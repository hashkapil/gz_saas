/**start delete request**/
jQuery(document).on('click', ".reddelete", function (e) {
    e.preventDefault();
    var url = jQuery(this).attr('data-url');
    jQuery('.btn-ydelete').attr('href', url);
    jQuery('#delete').trigger('click');
});
/**end delete request**/

/**start duplicate request**/
jQuery(document).on('click', ".copydup", function (e) {
    e.preventDefault();
    var url = jQuery(this).attr('href');
    jQuery('.btn-ndelete').attr('href', url);
    jQuery('#procopy').trigger('click');
});
/**end duplicate request**/


/**help popup for subuser**/
jQuery(document).on('click', "#contactwithagency", function () {
    jQuery('#dontaccesstosubuser').hide();
    jQuery(".modal-backdrop.fade.in").remove();
    jQuery('#contactwithagencyusers').trigger("click");
});

jQuery(document).on('click', ".trail-expire", function (e) {
    e.preventDefault();
    jQuery(this).attr('data-url');
    jQuery('#Select_PackForUP').trigger("click");

});

$('.subs_plan').on('change', function () {
    var sub_val = $(this).val();
    if (sub_val == 'A2870G') {
        $('.plans_table_sec').show();
        $('.plans_table').hide();
        $('.plans_table_logo').hide();
    } else if (sub_val == 'M299G') {
        $('.plans_table_sec').hide();
        $('.plans_table_logo').hide();
        $('.plans_table').show();
    }
});

/** After windod Load */
$(window).bind("load", function () {
    var timeout = 3000; // in miliseconds (3*1000)
    $('.alert-dismissable').delay(timeout).fadeOut(300);
});

var optionDiv='';
$(document).on('click', '.show_trans', function(){
    var id = $(this).attr('data-id');
    optionDiv = id;
 //   $('#trans_'+id).css('display','block');
    $('#trans_'+id).slideToggle();
});

$(document).mouseup(function(e) 
{
    var container = $('#trans_'+optionDiv);
    var classnm = $('.show_trans');
    if (!classnm.is(e.target) && classnm.has(e.target).length === 0) 
    {
        container.hide();
    }
});


/*********************Request listing start***********************************************/
function load_more_customer(is_loaded,viewtype,currentstatus,brand_id,status_cls){
    if(is_loaded != ''){
        var dataloaded = is_loaded.attr('data-isloaded');
    }else{
        dataloaded = '';
    }
    var html_data = '';
    var tabid = $(is_loaded).attr('href');
    var u_role = $("#status_check").attr("data-u_role");
    var activeTabPane = $('.tab-pane.active');
    var client_id = $client_id;
    var status_active = $('#status_check li.active a').attr('data-status');
    var view_active = is_loaded != "" ? $(is_loaded).attr('data-view'):$('#status_check li.active a').attr('data-view');
    var status_scroll = (currentstatus != '') ? currentstatus : status_active ;
    var searchval = $('.search_text').val();
    if(dataloaded != 1 || view_active != viewtype){
        $('.loader_tab').css('display','block');
        $('.product-list-show .row').css('display','none');
        $('.load_more').css('display','none');
        
        $.post(baseUrl+'account/request/load_more_customer',{'group_no': 0, 'status': status_scroll, 'search': searchval, 'brand_id': brand_id,'viewtype':viewtype,'client_id':client_id},
            function (data) {
                var newTotalCount = 0;
                if (viewtype != "grid") {
                var column="",v_column="",ex_heading="";
                    if(u_role != "designer"){
                        column = "<th>designer</th>";
                    }
                    if(u_role != "customer"){
                        v_column = "<th>verified</th>";
                    }
                    if(u_role == "s_user"){
                        ex_heading = '<th><div class="cli-ent-col td"><div class="sound-signal"><div class="form-radion2"><label class="containerr"><input type="checkbox" class="checkAll" data-attr="'+status_cls+'" name="project"><span class="checkmark"></span></label></div></div></div></th>';
                    }
                    html_data += '<div class="col-md-12"><div class="list-view-table"><table><thead><tr>'+ex_heading+'<th>project name</th><th>client</th>'+column+'<th>project status</th><th>project time</th>'+v_column+'<th>action</th></tr></thead><tbody>';
                }
                if (data != '') {
                    if(is_loaded != ''){
                        is_loaded.attr('data-isloaded','1');
                        is_loaded.attr('data-view',viewtype);
                    }
                    $('.loader_tab').css('display','none');
                    $('.product-list-show .row').css('display','block');
                    if (status_scroll == "draft") {
                        html_data += data;
                        $(".ajax_searchload").fadeOut(500);
                        $("#inprogressrequest .product-list-show .row").html(html_data);
                        newTotalCount = $("#inprogressrequest .product-list-show .row").find("#loadingAjaxCount").attr('data-value');
                        $("#inprogressrequest .product-list-show .row").find("#loadingAjaxCount").remove();
                    } else if (status_scroll == "assign") {
                        html_data += data;
                        $(".ajax_searchload").fadeOut(500);
                        $("#in_queue_tab .product-list-show .row").html(html_data);
                        newTotalCount = $("#in_queue_tab .product-list-show .row").find("#loadingAjaxCount").attr('data-value');
                        $("#in_queue_tab .product-list-show .row").find("#loadingAjaxCount").remove();
                    }else if (status_scroll == "approved") {
                        html_data += data;
                        $(".ajax_searchload").fadeOut(500);
                        $("#approved_designs_tab .product-list-show .row").html(html_data);
                        newTotalCount = $("#approved_designs_tab .product-list-show .row").find("#loadingAjaxCount").attr('data-value');
                        $("#approved_designs_tab .product-list-show .row").find("#loadingAjaxCount").remove();
                    } else if (status_scroll == "cancel") {
                        html_data += data;
                        $(".ajax_searchload").fadeOut(500);
                        $("#cancel_designs_tab .product-list-show .row").html(html_data);
                        newTotalCount = $("#cancel_designs_tab .product-list-show .row").find("#loadingAjaxCount").attr('data-value');
                        $("#cancel_designs_tab .product-list-show .row").find("#loadingAjaxCount").remove();
                    }else if (status_scroll == "hold") {
                        html_data += data;
                        console.log(html_data,"html_data");
                        $(".ajax_searchload").fadeOut(500);
                        $("#hold_designs_tab .product-list-show .row").html(html_data);
                        newTotalCount = $("#hold_designs_tab .product-list-show .row").find("#loadingAjaxCount").attr('data-value');
                        $("#hold_designs_tab .product-list-show .row").find("#loadingAjaxCount").remove();
                    }else if (status_scroll == "pendingrevision") {
                        html_data += data;
                        $(".ajax_searchload").fadeOut(500);
                        $("#pending_review_tab .product-list-show .row").html(html_data);
                        newTotalCount = $("#pending_review_tab .product-list-show .row").find("#loadingAjaxCount").attr('data-value');
                        $("#pending_review_tab .product-list-show .row").find("#loadingAjaxCount").remove();
                    }else if (status_scroll == "checkforapprove") {
                        html_data += data;
                        $(".ajax_searchload").fadeOut(500);
                        $("#pendingapproval_designs_tab .product-list-show .row").html(html_data);
                        newTotalCount = $("#pendingapproval_designs_tab .product-list-show .row").find("#loadingAjaxCount").attr('data-value');
                        $("#pendingapproval_designs_tab .product-list-show .row").find("#loadingAjaxCount").remove();
                    }else {
                        html_data += data;
                        $(".ajax_searchload").fadeOut(500);
                        $('#designs_request_tab' + " #prior_data").html(html_data);
                        newTotalCount = $(tabid + " #prior_data").find("#loadingAjaxCount").attr('data-value');
                        $(tabid + " #prior_data").find("#loadingAjaxCount").remove();
                    }
                    activeTabPane.attr("data-loaded", $rowperpage);
                    activeTabPane.attr("data-group", 1);
                } else {
                    activeTabPane.attr("data-loaded", 0);
                    activeTabPane.attr("data-group", 1);
//                    if (status_scroll == "draft") {
//                        $("#inprogressrequest .product-list-show .row").html("");
//                    } else if (status_scroll == "approved") {
//                        $("#approved_designs_tab .product-list-show .row").html("");
//                    } else {
//                        $(tabid + " #prior_data").html("");
//                    }
                    $(".loader_tab").css("display","none");
                    $('.product-list-show .row').css('display','block');
                    $(tabid + " .product-list-show .row").html(html_data);
                    
                }
                if ($rowperpage >= newTotalCount) {
                    activeTabPane.find('.load_more').css("display", "none");
                } else{
                    delay(function(){
                        $('.load_more').css('display','inline-block');
                        activeTabPane.find('.load_more').css("display", "inline-block");
                    }, 1000 );
                }
            });
    }
    else{
      $('.product-list-show .row').css('display','block');
      $('.ajax_loader').css('display','none');
      $('.load_more').css('display','inline-block');
  }
}

/*********start Search *******************/
var clickBtn = false;
$('.search_data_ajax').on('click', function(e) {
    var currentview = $('.view_class.active').data('view');
    e.preventDefault();
    if(clickBtn == false){
    }else{
        load_more_customer('',currentview,'',$brand_id);
    } 
    clickBtn = true;

});

//$('.close-search').click(function (e) { 
$(document).on('click', '.close-search', function () {
    $(this).parent().closest('.search-box ').removeClass('magic_search');
});

//$('.searchdata').focus(function () { 
$(document).on("focus", ".searchdata", function () {
    $('.search-box').addClass('magic_search');
}).blur(function () {
    if($('.search_text').val() == ''){
        $('.search-box').removeClass('magic_search');
    }
});
$(document).on("keyup", ".searchdata", function (e) {
//$('.searchdata').keyup(function (e) {
    var currentview = $('.view_class.active').data('view');
    e.preventDefault();
    $(".ajax_searchload").fadeIn(500);
    delay(function () {
        load_more_customer('',currentview,'',$brand_id);
    }, 1000);
});

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();
/*********end Search *******************/


/**************on page load and load more button click*****************/
var status = $('#status_check li.active a').attr('data-status');
var id = $('#status_check li.active a').attr('href');
var view = $('.search_text').attr('data-view','grid');

$(document).on("click", ".load_more", function () {
    $(".ajax_loader").css("display", "block");
    $(".load_more").css("display", "none");
    var row = parseInt($(this).attr('data-row'));
   // var viewType = $('.view_class.active').attr('data-view');
    var viewType = "list";
    row = row + $rowperpage;
    var activeTabPane = $('.s_projct_lst .tab-pane.active');
    var client_id = $client_id;
    var searchval = activeTabPane.find('.search_text').val();
    var allcount = parseInt(activeTabPane.attr("data-total-count"));
    var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
    var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
    var status_scroll = $('#status_check li.active a').attr('data-status');
    var tabid = $('#status_check li.active a').attr('href');
    if (allLoaded < allcount) {
        $.post(baseUrl+'account/request/load_more_customer', {'group_no': activeTabPaneGroupNumber, 'status': status_scroll, 'search': searchval,'viewtype':viewType,'client_id':client_id},
            function (data) {
                if (data != "") {
                    var apendclass;
                    if(viewType == "list"){
                        apendclass = ".product-list-show .row table tbody";
                    }else{
                        apendclass = ".product-list-show .row";
                    }
                    row = row + $rowperpage;
                    activeTabPane.find('.load_more').attr('data-row', row);
                    if (status_scroll == "draft") {
                        $("#inprogressrequest "+apendclass).append('<tr>'+data+'</tr>');
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more").css("display", "inline-block");
                    }else if (status_scroll == "assign") {
                        $("#in_queue_tab "+apendclass).append('<tr>'+data+'</tr>');
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more").css("display", "inline-block");
                    }
                    else if (status_scroll == "approved") {
                        $("#approved_designs_tab "+apendclass).append('<tr>'+data+'</tr>');
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more").css("display", "inline-block");
                    }
                    else if(status_scroll == "cancel"){
                        $("#cancel_designs_tab "+apendclass).append('<tr>'+data+'</tr>');
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more").css("display", "inline-block");
                    }else if(status_scroll == "hold"){
                        $("#hold_designs_tab "+apendclass).append('<tr>'+data+'</tr>');
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more").css("display", "inline-block");  
                    }else if (status_scroll == "pendingrevision") {
                        $("#pending_review_tab "+apendclass).append('<tr>'+data+'</tr>');
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more").css("display", "inline-block");  
                    }else if (status_scroll == "checkforapprove") {
                        $("#pendingapproval_designs_tab "+apendclass).append('<tr>'+data+'</tr>');
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more").css("display", "inline-block");
                    }else {
                        if(viewType == "list"){
                            console.log("currenttab - ",tabid + " .product-list-show .row table tbody")
                            $(tabid + " .product-list-show .row table tbody").append('<tr>'+data+'</tr>');
                        }else{
                            $(tabid + " #prior_data").append(data);
                        }
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more").css("display", "inline-block");
                    }
                    activeTabPaneGroupNumber++;
                    activeTabPane.attr('data-group', activeTabPaneGroupNumber);
                    allLoaded = allLoaded + $rowperpage;
                    activeTabPane.attr('data-loaded', allLoaded);
                    if (allLoaded >= allcount) {
                        activeTabPane.find('.load_more').css("display", "none");
                    } else {
                        activeTabPane.find('.load_more').css("display", "inline-block");
                    }
                }
            });
    }
});
/**************End on page load and load more button click*****************/

/**************start list/grid*****************/
$(document).on('click', '.view_class', function(){
//    alert(2);
   var view_type = $(this).data('view');
   $(this).addClass('active').siblings().removeClass('active');
   $('#status_check li.active a').attr('data-view',view_type);;
   if(view_type == 'list'){
       jQuery('.list-add-project').css('display','block');
       jQuery('.add_profordesign').attr('data-step','');
       jQuery('.list_view_d').attr('data-step','1');
       jQuery('.grid_add').css('display','none');
   }else{
    jQuery('.list-add-project').css('display','none');
    jQuery('.list_view_d').attr('data-step','');
    jQuery('.add_profordesign').attr('data-step','1');
    jQuery('.grid_add').css('display','block');
    
}
load_more_customer('',view_type,'',$brand_id);
});


function check_activeClass(){
 var id = $(".list-header-blog").attr("id");
    if($('#'+id+' li:first-child').hasClass("active")==false){ 
      $('#'+id+' li:first-child a').trigger("click"); 
       setTimeout(function() { 
         introJs().start();
      }, 700);
    }else{
        introJs().start();
    }
}
/**************end list/grid*****************/

/**************start on tab change*****************/
$(document).on('click', '#status_check li a', function(){
    var currentstatus = $(this).data('status');
    var is_loaded = $(this).data('isloaded');
    var status_cls = $(this).data('s_class');
    var currentview = $('.view_class.active').data('view');
    load_more_customer($(this),currentview,currentstatus,$brand_id,status_cls);
});
/**************end on tab change*****************/

/*********listing end***********************************************/


/**************priority change***************/
function prioritize (priorityto, priorityfrom, id,usertype) {
    $('#confirmation').click();
    $('#priorityFrom').val(priorityfrom);
    $('#priorityto').val(priorityto);
    $('#priority_user_type').val(usertype);
    $('#id').val(id);
}
//$('.btn-y').click(function () {
$('.btn-y-priority').click(function () {
    var priorityfrom = $('#priorityFrom').val();
    var priorityto = $('#priorityto').val();
    var priority_user_type = $('#priority_user_type').val();
    var id = $('#id').val();
    $.ajax({
        type: 'GET',
        url: baseUrl+"customer/request/set_priority",
        data: {priorityfrom: priorityfrom, priorityto: priorityto, id: id, priority_user_type: priority_user_type},
        success: function (response) {
            location.reload();
        }
    });
});

/**************add new request****************/
function design(str) {
    document.getElementById("design_patterns").innerHTML = ""
    document.getElementById("design_patterns").innerHTML = document.getElementById(str).innerHTML
}

function formatFileSize(bytes, decimalPoint) {
    if (bytes == 0)
        return '0 B';
    var k = 1000,
    dm = decimalPoint || 2,
    sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
    i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

var $droparea = $('.project-file-drop-area');
var storedFiles = [];
storedFiles['logo_upload'] = [];
storedFiles['materials_upload'] = [];
storedFiles['additional_upload'] = [];
var deletedFiles = [];
    // highlight drag area
    $('#file_input').on('dragenter focus click', function () {
        $droparea.addClass('is-active');
    });

// back to normal state
$('#file_input').on('dragleave blur drop', function () {
    $droparea.removeClass('is-active');
});
//$(document).ready(function () {
$(document).on("change","#file_input",function (){

//$('#file_input').change(function () {
   
var from_file = $("input[name=from_file]").val();
var $fileListContainer = $(".uploadFileListContain");
var loadingImg = $assets_path+'img/ajax-loader.gif';
$fileListContainer.append('<div class="ajax-img-loader"><img src="'+loadingImg+'" height="150"/></div>');
$('#request_submit').prop('disabled', true);
var fileInput = $('#file_input')[0];

if (fileInput.files.length > 0) {
    console.log("ajax working");
    var formData = new FormData();
    $.each(fileInput.files, function (k, file) {
        var fileName = file.name;
        var type =  fileName.split('.').pop();
        var name = fileName.split('.');
        var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,Math.floor(Math.random() * 6) + 1);
        var newFileName = name_file + '.'+type;
        formData.append('file-upload[]', file,newFileName);
    });
    var $url =  baseUrl+"account/request/process";
$.ajax({
    method: 'post',
    url: $url,
    data: formData,
    dataType: 'json',
    contentType: false,
    processData: false,
    success: function (response) {
      //  console.log(response);
        if (response.status == '1') {
            if (response.files.length > 0) {
                $.each(response.files, function (k, v) {
                    storedFiles.push(v[0]);
                    $fileListContainer.html(createStoredFilesHtml());
                    $('#request_submit').prop('disabled', false);
                });
            }
        }
    }
});
}
});
//});

$(document).on("click", ".delete_file", function () {
    var file_index = $(this).data("file-index");
    deletedFiles.push($(this).data("id"));
    storedFiles.splice(file_index, 1);
    var file_name = $(this).data("file-name");
    var folderPath = $(".upload_path").val();
    var dataString = 'file_name=' + file_name + '&folderPath=' + folderPath;
    $("#file" + file_index).remove();
    var $fileListContainer = $(".uploadFileListContain");
    $fileListContainer.append('<div class="ajax-img-loader"><img src="'+$assets_path+'"img/default-img/Loading_icon.gif" height="150"/></div>');
    $fileListContainer.html(createStoredFilesHtml());
    $.ajax({
        method: 'POST',
        url: baseUrl+"account/request/delete_file_from_folder",
        data: dataString,
        success: function (response) {

        }
    });
});

function createStoredFilesHtml(){
    var ouptput = "<div class='attached-files'><h3>Attachments</h3><div class='row'>";
    //console.log("storedFiles",storedFiles);
    $.each(storedFiles,function (key, value) {
        var imageLink ="";
        if (value.error == false) {
        
            $('.ajax-img-loader').css('display', 'none');
            if(value.image_link != ""){
            imageLink = value.image_link; 
            
            ouptput += '<div  class="uploadFileRow " id="file' + key + '"><div class="col-md-6"><div class="extnsn-lst">\n\
            <p class="text-mb">' + value.file_name + '<strong> (' + formatFileSize(value.file_size, 2) + ') </strong></p>\n\
            <p class="cross-btnlink">\n\
            <a href="javascript:void(0)" class="delete_file" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
            <i class="fa fa-times"></i>\n\
            </a>\n\
            <input type="hidden" value="' + value.file_name + '" name="delete_file[]" class="delete_file"/><input type="hidden" value="" name="upload_path" class="upload_path" /><input type="hidden" value="' +imageLink+ '" name="image_link[]"/>\n\
            \n\
            \n\
            \n\
            </p>\n\
            </div></div></div>';
         }

        } else {
            ouptput += '<div  class="uploadFileRow" id="file' + key + '"><div class="col-md-6" ><div class="extnsn-lst error-list">\n\
            <p class="text-mb">' + value.file_name + '<br/><strong>' + value.error_msg + '</strong></p><p class="cross-btnlink">\n\
            <a href="javascript:void(0)" class="delete_file" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
            <i class="fa fa-times"></i>\n\
            </a>\n\
            \n\
            \n\
            \n\
            \n\
            </p>\n\
            </div></div></div>';
        }
    });
    ouptput += "</div></div>";
    return ouptput;
}
/***************modal popup****************/
$(document).ready(function(){
    $(".s_projct_lst .tab-pane").attr("data-loaded", $rowperpage);
    var last_selected = $("input[name='logo-brand']:checked").val();
    $("#SelectPackForUP").focusout(function() {
        $("input[name='logo-brand'][value='"+last_selected+"']").prop("checked", true);
        $(".modal-backdrop.fade.in").remove();
    });
});

$(document).on('change','#design_patterns',function(){
    var is_trial = $('#for_trial').val();
    var logo_brand = $("input[name='logo-brand']:checked").val();
    if(is_trial == 1 && logo_brand == 'Logo'){
        $('#Select_PackForUP').trigger("click");
        $('#SelectPackForUP .iflogo_selected').html("<p><strong class='UpgrdSubs'>Logos</strong> are premium designs.<br/> Upgrade your plan to submit a logo request</p>");
    }
});

$("#submit_requests_form").submit(function (e) { 
    var count = $('#request_submit').attr('data-count');
    var without_pay_user = $without_pay_user;
    var session = $session;
    var subcategory_class = $("input[name='subcategory_id']:checked").val();
    var title = $('.title').val();
    var dimension = $('.dimension').val();
    var comment = $('#comment').val();
    var sample = [];
    var sample_isset = $('.sampleexist').attr('data-isset');
    if(sample_isset == '1'){
       // console.log("submit_not_suceess");
        $("input[name='sample_subcat[]']:checked").each(function() {
            sample.push($(this).val());
        });
        var minval = $('.min_val').html();
        if(sample.length < minval){
           $('.sample .error_msg').html("<span class='validation'>Please choose atleast "+minval+" sample.</span>");
            return false; 
        }else{
          $('.sample .error_msg').html("");   
        }
//    }
//    if (subcategory_class == '' || typeof subcategory_class == 'undefined') {
//        $('.catagory .error_msg').html("<span class='validation'>Please choose atleast one subcategory</span>");
//        return false;
//    } else if (title == '' || typeof title == 'undefined') {
//        $('.title_class .error_msg').html("<span class='validation'>Please enter title</span>");
//        $('.catagory .error_msg').html("");
//        return false;
//    }else if (dimension == '' || typeof dimension == 'undefined') {
//        $('.dim_class .error_msg').html("<span class='validation'>Please enter dimension</span>");
//        $('.catagory .error_msg').html("");
//        $('.title_class .error_msg').html("");
//        return false;
//    } else if (comment == '' || typeof comment == 'undefined') {
//        $('.com_class .error_msg').html("<span class='validation'>Please enter description</span>");
//        $('.catagory .error_msg').html("");
//        $('.title_class .error_msg').html("");
//        $('.dim_class .error_msg').html("");
//        return false;
    } else {
        //console.log("submit_suceess");
        if (without_pay_user == 1) {
            e.preventDefault();
            if (session == '' && count == '1') {
                var formData = $(this).serialize();
                $.ajax({
                    method: 'POST',
                    url: baseUrl + "account/request/add_request_todraft_withoutpayuser",
                    data: formData,
                    dataType: 'json',
                    success: function (response) {
                    }
                });
            }
            var session = $session = 'yes';
            $('#Select_PackForUP').trigger('click');
            var click_count = parseInt(count) + 1;
            $('#request_submit').attr('data-count', click_count);
            return false;
        } else {
            delay(function () {
              //  console.log($assets_path + 'img/ajax-loader.gif');
                $('.imgloader_loader').attr('src', $assets_path + 'img/ajax-loader.gif');
                $('#request_submit').prop('disabled', true);
            }, 1);
        }
        //return true;
    }
});
/*******************category bucket************************/
    $(document).ready(function(){
        var url = window.location.href;     
        var arr = url.split('/');
        var myoutput=arr[5];  
        // console.log(arr);
        var value = $('select#cat_page option:selected').val();
        $('#child_'+value).css('display','block');
        if(myoutput == 'add_new_request'){
        $('#child_'+value+' > .row > div:first-child input.subcategory_class').trigger("click");
    }
    });

function getval(sel)
{
    var catID = sel.value;
    $('option:selected', '#cat_page').attr('selected',true).siblings().removeAttr('selected');
    $('#child_'+sel.value).siblings().css('display','none');
    $('#child_'+sel.value).css('display','block');
    getCatQuest(catID);
    getSamples(catID);
    var url = window.location.href;     
    var arr = url.split('/');
    var myoutput=arr[6];    
    
    if(myoutput == 'add_new_request'){
    $('#child_'+sel.value+' > .row > div:first-child input.subcategory_class').trigger("click"); 
    }
}
    
$(document).ready(function(){
    var value = $('select#cat_page option:selected').val();
    $('#child_'+value).css('display','block');
});

function labelactive() {
    $(document).on('focus', '.input', function () {
        //$('.input').focus(function () {
        $(this).parent().find(".label-txt").addClass('label-active');
    });
    $(document).on('focusout', '.input', function () {
        //$(".input").focusout(function () {
        if ($(this).val() == '') {
            $(this).parent().find(".label-txt").removeClass('label-active');
        }
        ;
    });
}

/*********** all js of document ready **********/
$(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip(); 
    
    labelactive();
    $(".tab-toggle").click(function () {
        $(".account-tab ul.nav.nav-tabs").slideToggle();
    });
    if(is_logged_in == 0){
    //javascript:introJs().start();
    }
    /**************setting js on page load****************/
    /**editor setting**/
    var settings = {
            'indent': false,
            'outdent': false,
            'strikeout': false,
            'block_quote': false,
            'hr_line': false,
            'splchars': false,
            'ol': false,
            'ul': false
    }
    /**end editor setting**/
    var url = window.location.href;
    var url_divide = url.split("/");
    var pieces = url.split("#");
    var activebar = pieces[1];
    if ($.inArray('user_setting', url_divide) != -1 || $.inArray('user_setting#'+activebar, url_divide) != -1){
        var user_setting = 1;
        $('#brandcheck').chosen();
        $(function () {
            $('.sel_colorpicker').colorpicker( {
                customClass: 'colorpicker-2x',
                sliders: {
                    saturation: {
                        maxLeft: 150,
                        maxTop: 150
                    },
                    hue: {
                        maxTop: 150
                    },
                    alpha: {
                        maxTop: 150
                    }
                }

            });
        });
         
        if (is_smtp_saved == 1) {
            $("#is_smtp_enable").prop('checked', true);
            $(".port_settings").css('display', 'block');
        }
        /**editor js**/
        $("#txtEditor").Editor(settings);
        $("#txtEditor").Editor('createMenuItem', {
            "text": "Personalize", //Text replaces icon if its not available
            "tooltip": "Personalize",
            "custom": function (button, parameters) {
                $('#identifierDetail').modal('show');
            },
            "params": {'button': "Button Name", 'color': "Button Color", 'font': "Font Size"} //Any custom parameters you want to pass
        });
        var content = $('#txtEditor').val();
        $('.Editor-editor').html(content);
        /**end editor js**/
    }else if($.inArray('setting-view', url_divide) != -1 || $.inArray('setting-view#'+activebar, url_divide) != -1){
        var user_setting = 0;
        $('#brandcheck').chosen();
    }
    $('#' + activebar).addClass('active');
    $('#' + activebar).siblings().removeClass('active');
    $('[href*="#' + activebar + '"]').closest('li').siblings().removeClass('active');
    $('[href*="#' + activebar + '"]').closest('li').addClass('active');

    $(document).on('click', '.account-tab ul.list-header-blog', function (e) {
        var activetab = $('.account-tab ul.list-header-blog .active > a').attr('href');
        if (user_setting === 1) {
            window.location.replace(baseUrl + 'account/user_setting' + activetab);
        }else if(user_setting === 0){
            window.location.replace(baseUrl + 'account/setting-view' + activetab);
        }
    });
    
    $(".togglemenuforres").click(function () {
        $(".account-tab ul.list-header-blog").slideToggle();
    });
    
    setTimeout(function(){
        $("div.alert").remove();
    }, 10000); // 5 secs
    if ($('.new_plan_billingfr_chng').length > 0){
        $('.new_plan_billingfr_chng').owlCarousel({
              autoplayHoverPause: true,
              loop:true,
              autoplay:true,
              margin:20,
              dots:false,
              nav:true,
              items:3,
              responsive: {
                0: {
                  items: 1
                },
                600: {
                  items: 2
                },
                992: {
                  items: 3  
                }
              }
        });
    }
    /**************end setting js on page load****************/
    
    /**************start agency setting js on page load****************/
     make_swatchs();
     /************Get domain value*****************/
        var domain_or_subdomain = ($(".switch-custom-usersetting-check .slct_domain_subdomain").is(':checked'))?1:0;
        if (domain_or_subdomain === 1) {
            $(".domain_nm").show();
//            $(".chkdomain_ssl").show();
            $(".subdomain_nm").hide();
        } else {
            $(".subdomain_nm").show();
//            $(".chkdomain_ssl").hide();
            $(".domain_nm").hide();
        }

    /***********End Get domain value*****************/
    var is_checked1 = $('#switch_access').is(":checked");
    brandsShow(is_checked1);
    $(document).on('change', '#switch_access', function () {
        var is_checked = $('#switch_access').is(":checked");
        brandsShow(is_checked)
    });

   /**************end agency setting js on page load****************/
   /************brand profile js on page load *************/
    $(document).on('click', ".delete_brands", function () {
        var id = $(this).attr('data-id');
        jQuery('.brand_ids').val(id);
        $('#delete').trigger('click');

    });
    $(document).on('change', '.brand_radio', function () {
    var brand_id = jQuery('.brand_ids').val();
    var is_checked = $('#reassign_req').is(":checked");
    var brandstoassign = '';
    // console.log('brand_profiles_data',brand_profiles_data);
    if (is_checked == true) {
    $('.assign_brand_to_req').css('display', 'inline-block');
    brandstoassign += "<select name='assign_brand'><option value=''>Select Brand</option>";
    $.each(brand_profiles_data, function (k, v) {
    if (v.id != brand_id) {
    brandstoassign += "<option value=" + v.id + ">" + v.brand_name + "</option>";
    }
    });
    brandstoassign += "</select>";
            $('.assign_brand_to_req').html(brandstoassign);
    }else {
    $('.assign_brand_to_req').css('display', 'none');
    }

    });
   /************end brand profile js on page load *************/
   /**********add brand profile js on page load*********/
    var max_fields_limit = 10; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $('.add_more_button').click(function (e) { //click event on add more fields button having class add_more_button
        e.preventDefault();
        var name = $(this).parent().find(".f_cls_nm").attr('name');
        if (x < max_fields_limit) { //check conditions
            x++; //counter increment
            $('.input_fields_container').append('<div><div class="remove-pre"><input type="text" class="input" name="'+name+'"/><div class="line-box"><div class="line"></div></div><a href="#" class="remove_field"><i class="icon-gz_plus_icon"></i></a></div></div></div>'); //add input field
        }
    });
    $('.add_more_script').click(function (e) { //click event on add more fields button having class add_more_button
        e.preventDefault();
        var name = $(this).parent().find(".f_cls_nm").attr('name');
        var tracking_nm = $(this).parent().find(".f_trkngcls_nm").attr('name');
        var pos_nm = $(this).parent().find(".f_scrpt_pos_nm").attr('name');
        var pg_nm = $(this).parent().find(".f_scrpt_pg_nm").attr('name');
        if (x < max_fields_limit) { //check conditions
            x++; //counter increment
            $('.input_fields_container').append('<div><div class="remove-pre"><div class="col-lg-12 col-md-12"><label class="form-group"><p class="label-txt">Name</p><input name="'+name+'" class="input"/><div class="line-box"><div class="line"></div></div></label></div><div class="col-lg-12 col-md-12"><label class="form-group"><p class="label-txt">Paste your tracking code here</p><textarea class="input" name="'+tracking_nm+'"></textarea><div class="line-box"><div class="line"></div></div></label></div><div class="col-lg-6 col-md-6"><label class="form-group"><p class="label-txt label-active">Position inside the code</p><select name="'+pos_nm+'" class="input"><option value="before_head">Before < /HEAD ></option><option value="before_body">Before < /BODY ></option></select></label></div><div class="col-lg-6 col-md-6"><label class="form-group"><p class="label-txt label-active">Show only on specific page</p><select name="'+pg_nm+'" class="input"><option value="login">Login</option><option value="signup">Signup</option><option value="cus_portl">Customer Portal</option><option value="all_pages">All Pages</option></select></label></div><a href="#" class="remove_field"><i class="icon-gz_plus_icon"></i></a></div></div></div>'); //add input field
        }
    });
    $('.input_fields_container').on("click", ".remove_field", function (e) { //user click on remove text links
        e.preventDefault();
        jQuery(this).parent('div').remove();
        x--;
    });

    //delete brand file materials in edit case
    $(document).on("click", ".edt_brnd_prfl", function () {
        var flpath = $(this).data("flpath");
        var delfile = $(this).data("delfile");
        var fileid = $(this).data("fileid");
        var fieldname = $(this).data("fieldname");
        var parent_sec = '#file' + fileid;
        //console.log('id',parent_sec);
        $.ajax({
            method: 'POST',
            url: baseUrl+"account/BrandProfile/del_brandprofile_files",
            data: {flpath: flpath, delfile: delfile, fileid: fileid, fieldname: fieldname},
            success: function (response) {
                //console.log('response1',response)
                if (response == 1) {
                    $(parent_sec).remove();
                }
            }
        });
    });
    /***********upload logo files*****************/

    $('#logo_file_input').change(function () {
        var $fileListContainer = $(".logo_uploadFileListContain");
        $fileListContainer.append('<div class="ajax-img-loader"><img src="'+$assets_path+'img/ajax-loader.gif"/></div>');

        var fileInput = $('#logo_file_input')[0];
        if (fileInput.files.length > 0) {

            var formData = new FormData();
            $.each(fileInput.files, function (k, file) {
                var fileName = file.name;
                var type = fileName.split('.').pop();
                var name = fileName.split('.');
                var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, Math.floor(Math.random() * 6) + 1);
                var newFileName = name_file + '.' + type;
                formData.append('logo_upload[]', file, newFileName);
            });

            call_ajax_to_upload(formData, $fileListContainer);
        } else {
        //    console.log('No Files Selected');
        }
    });
    $('#materials_file_input').change(function () {
        var $fileListContainer = $(".materials_uploadFileListContain");
        $fileListContainer.append('<div class="ajax-img-loader"><img src="'+$assets_path+'img/ajax-loader.gif"/></div>');

        var fileInput = $('#materials_file_input')[0];
        if (fileInput.files.length > 0) {

            var formData = new FormData();
            $.each(fileInput.files, function (k, file) {
                var fileName = file.name;
                var type = fileName.split('.').pop();
                var name = fileName.split('.');
                var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, Math.floor(Math.random() * 6) + 1);
                var newFileName = name_file + '.' + type;
                formData.append('materials_upload[]', file, newFileName);
            });

            call_ajax_to_upload(formData, $fileListContainer);

        } else {
         //   console.log('No Files Selected');
        }
    });

    $('#additional_file_input').change(function () {
        var $fileListContainer = $(".additional_uploadFileListContain");
        $fileListContainer.append('<div class="ajax-img-loader"><img src="'+$assets_path+'img/ajax-loader.gif"/></div>');

        var fileInput = $('#additional_file_input')[0];
        if (fileInput.files.length > 0) {

            var formData = new FormData();
            $.each(fileInput.files, function (k, file) {
                var fileName = file.name;
                var type = fileName.split('.').pop();
                var name = fileName.split('.');
                var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, Math.floor(Math.random() * 6) + 1);
                var newFileName = name_file + '.' + type;

                formData.append('additional_upload[]', file, newFileName);
            });

            call_ajax_to_upload(formData, $fileListContainer);
        } else {
        //    console.log('No Files Selected');
        }
    });

    $(document).on("click", ".logo_delete_file", function () {
        var file_index = $(this).data("file-index");
        var file_upload = $(this).data("upload");
        deletedFiles.push($(this).data("id"));
        //console.log('check',file_upload);
        storedFiles[file_upload].splice(file_index, 1);

        var file_name = $(this).data("file-name");
        var folderPath = $(".logo_upload_path").val();
        var dataString = 'file_name=' + file_name + '&folderPath=' + folderPath;
        if (file_upload == 'logo_upload') {
            var $fileListContainer = $(".logo_uploadFileListContain");
        }
        if (file_upload == 'materials_upload') {
            var $fileListContainer = $(".materials_uploadFileListContain");
        }
        if (file_upload == 'additional_upload') {
            var $fileListContainer = $(".additional_uploadFileListContain");
        }

        $fileListContainer.append('<div class="ajax-img-loader"><img src="'+$assets_path+'img/ajax-loader.gif" height="150"/></div>');
        $fileListContainer.html(createlogoStoredFilesHtml(file_upload));
        $.ajax({
            method: 'POST',
            url: baseUrl+"account/BrandProfile/delete_brand_file_from_folder",
            data: dataString,
            success: function (response) {

            }
        });
    });

    function createlogoStoredFilesHtml(upload) {
        var ouptput = "";
        $.each(storedFiles[upload], function (key, value) {
            if (value.error == false) {
                $('.ajax-img-loader').css('display', 'none');
                ouptput += '<div  class="uploadFileRow " id="logo_file' + key + '"><div class="col-md-12"><div class="extnsn-lst">\n\
        <p class="text-mb">' + value.file_name + '<strong> (' + formatFileSize(value.file_size, 2) + ') </strong></p>\n\
        <p class="cross-btnlink">\n\
        <a href="javascript:void(0)" class="logo_delete_file" data-upload="' + upload + '" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
        <span>x</span>\n\
        </a>\n\
        <input type="hidden" value="' + value.file_name + '" name="upload_file_name[]" class="logo_delete_file"/><input type="hidden" value="' + upload + '" name="upload_type[]" class="upload_type_file"/><input type="hidden" value="" name="logo_upload_path" class="logo_upload_path" />\n\
        \n\
        \n\
        \n\
        </p>\n\
        </div></div></div>';
            } else {
                ouptput += '<div  class="uploadFileRow" id="logo_file' + key + '"><div class="col-md-12" ><div class="extnsn-lst error-list">\n\
        <p class="text-mb">' + value.file_name + '<br/><strong>' + value.error_msg + '</strong></p><p class="cross-btnlink">\n\
        <a href="javascript:void(0)" class="logo_delete_file" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
        <span>x</span>\n\
        </a>\n\
        \n\
        \n\
        \n\
        \n\
        </p>\n\
        </div></div></div>';
            }
        });
        return ouptput;
    }

    function call_ajax_to_upload(formData, $fileListContainer) {

        $.ajax({
            method: 'post',
            url: baseUrl+"account/BrandProfile/upload_logo_process",
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (response) {
                //   console.log(response);
                if (response.status == '1') {
                    if (response.type == 'logo_upload') {

                        if (response.logo_upload.length > 0) {
                            $.each(response.logo_upload, function (k, v) {
                                storedFiles['logo_upload'].push(v[0]);
                                $fileListContainer.html(createlogoStoredFilesHtml('logo_upload'));
                            });
                        }
                    }
                    if (response.type == 'materials_upload') {
                        if (response.materials_upload.length > 0) {

                            $.each(response.materials_upload, function (k, v) {
                                storedFiles['materials_upload'].push(v[0]);
                                $fileListContainer.html(createlogoStoredFilesHtml('materials_upload'));
                            });
                        }
                    }
                    if (response.type == 'additional_upload') {
                        if (response.additional_upload.length > 0) {

                            $.each(response.additional_upload, function (k, v) {
                                storedFiles['additional_upload'].push(v[0]);
                                $fileListContainer.html(createlogoStoredFilesHtml('additional_upload'));
                            });
                        }
                    }
                }
            }
        });
    }
   /**********end add brand profile js on page load*********/
   /*************footer js on page load*******************/

//    if ($('.pricebasedquantity').length > 0 && total_inprogress_req > 0) {
//        $(".chooseplan.f_mainusr_pln.active .pricebasedquantity").val(total_inprogress_req).find("option[value=" + total_inprogress_req + "]").attr('selected', true);
//        var price_activepln = $(".chooseplan.f_mainusr_pln.active .pricebasedquantity option:selected").attr('data-amount');
//        $(".chooseplan.f_mainusr_pln.active").find('.agency_price').html(price_activepln);
//    }
    $pln_name = "";
    if($('.pricebasedquantity').length > 0 && total_inprogress_req > 0){ 
      if (typeof client_inprogress_req !== 'undefined'){
        $(".f_clnt_actv_pln.active .pricebasedquantity").val(client_inprogress_req).find("option[value=" + client_inprogress_req +"]").attr('selected', true);
        var price_active_clntpln = $(".f_clnt_actv_pln .f_actv_pln").parents(".chooseplan").find(".pricebasedquantity option[value=" + client_inprogress_req +"]").attr('data-amount');
        $(".f_clnt_actv_pln.active").find('.agency_price').html(price_active_clntpln);
        $(".f_clnt_actv_pln .currentPlan").parents(".chooseplan").find(".pricebasedquantity").val(client_inprogress_req).find("option[value=" + client_inprogress_req +"]").attr('selected', true);
        $(".f_clnt_actv_pln .f_actv_pln").parents(".chooseplan").find('.agency_price').html(price_active_clntpln);
      }
      $(".chooseplan.active.f_main_clnt .pricebasedquantity").val(total_inprogress_req).find("option[value=" + total_inprogress_req +"]").attr('selected', true);
      var price_activepln = $(".chooseplan.active .pricebasedquantity option:selected").attr('data-amount');
      var annual_activepln = $(".chooseplan.active .pricebasedquantity option:selected").attr('data-annual_price');
      $(".chooseplan.active.f_main_clnt").find('.agency_price').html(price_activepln);
      $(".f_main_clnt .currentPlan").parents(".chooseplan").find(".pricebasedquantity").val(total_inprogress_req).find("option[value=" + total_inprogress_req +"]").attr('selected', true);
      $(".f_main_clnt .f_actv_pln").parents(".chooseplan").find('.agency_price').html(annual_activepln);
      $(".f_main_clnt .f_actv_pln").parents(".chooseplan").find('.f_pln_totl_amnt').html(price_activepln);
    }
    $(document).on('change', '.pricebasedquantity', function () {
        var Subs_price = $('option:selected', this).attr('data-amount');
        var annual_price = $('option:selected', this).attr('data-annual_price');
        var f_pln_name = $(this).parents(".chooseplan").find("a").data("value");
        var subs_quantity = this.value;
        if(annual_price == "" || annual_price == "0" || typeof annual_price === 'undefined'){
            var final_SubPrice = Subs_price;
        }else{
            var final_SubPrice = annual_price;
        }
        if($(this).parents(".chooseplan").find("a").hasClass("currentPlan")){
           if(subs_quantity != total_inprogress_req){
               $(this).parents(".chooseplan").find("a").removeClass("currentPlan f_actv_pln");
               $(this).parents(".chooseplan").find("a").addClass("ChngPln");
               $(this).parents(".chooseplan").find("a").html("Change Plan");
               $(this).parents(".chooseplan").find("a").prop('disabled', false);
               $(this).parents(".chooseplan").find("a").attr("data-target","#CnfrmPopup");
               $pln_name = $(this).parents(".chooseplan").find("a").data("value");
           }
        }
        if(f_pln_name == $pln_name && subs_quantity == total_inprogress_req){
                $(this).parents(".chooseplan").find("a").removeClass("ChngPln");
                $(this).parents(".chooseplan").find("a").addClass("currentPlan f_actv_pln");
                $(this).parents(".chooseplan").find("a").html("Current Plan");
                $(this).parents(".chooseplan").find("a").attr("data-target","#currentplan");
        }
        jQuery(this).closest('.price-card').find('.upgrd_agency_pln').attr('data-price', final_SubPrice);
        jQuery('.toatal-bill-p.text-right').html('<span class="appened_price">$' + final_SubPrice + '</span>');
        jQuery('.chooseplan').find('h3.agency_updated').html('<font><span class="currency-sign">$</span>' + final_SubPrice + '/Month</font>');
        jQuery(this).closest('.price-card').find('.upgrd_agency_pln').attr('data-inprogress', subs_quantity);
        jQuery(this).closest('.price-card').find('.agency_price').html(Subs_price);
        if($('.f_pln_totl_amnt').length > 0){
            jQuery(this).closest('.price-card').find('.f_pln_totl_amnt').html(annual_price);
        }
        jQuery('.disc-code').val('');
        jQuery('.ErrorMsg').css('display', 'none');
        jQuery('.coupon-des-newsignup').css('display', 'none');
    });
//    $(document).on('change', '.pricebasedquantity', function () {
//   // jQuery('.pricebasedquantity').on('change', function () {
//        var Subs_price = $('option:selected', this).attr('data-amount');
//        var subs_quantity = this.value;
//        var final_SubPrice = Subs_price;
//        console.log('Subs_price',Subs_price);
//        jQuery(this).closest('.price-card').find('.upgrd_agency_pln').attr('data-price', final_SubPrice);
//        jQuery('.toatal-bill-p.text-right').html('<span class="appened_price">$' + final_SubPrice + '</span>');
//        jQuery('.chooseplan').find('h3.agency_updated').html('<font><span class="currency-sign">$</span>' + final_SubPrice + '/Month</font>');
//        jQuery(this).closest('.price-card').find('.upgrd_agency_pln').attr('data-inprogress', subs_quantity);
//        jQuery(this).closest('.price-card').find('.agency_price').html(final_SubPrice);
//        jQuery('.disc-code').val('');
//        jQuery('.ErrorMsg').css('display', 'none');
//        jQuery('.coupon-des-newsignup').css('display', 'none');
//    });
    $('#lightgallery').lightGallery({
      thumbnail: false,
      zoom: false,
      mousewheel: false,
      fullScreen: false,
      width: "70%"
    });
    
    /** term conditions tab **/    
    if($('#term_conditions').length > 0){
        $("#term_conditions").Editor(settings);
        $("#privacy_policy").Editor(settings);
        var term_conditions = $('#term_conditions').val();
        var privacy_policy = $('#privacy_policy').val();
        $("#term_conditions").next(".Editor-container").find(".Editor-editor").html(term_conditions);
        $("#privacy_policy").next(".Editor-container").find(".Editor-editor").html(privacy_policy);
  }
    $(document).on("click","#savebtntc",function(e){
        
        var tc = $('#term_conditions').Editor("getText");
        var pp = $('#privacy_policy').Editor("getText");
        $('#term_conditions').text(tc);
        $('#privacy_policy').text(pp);
        tc = tc.replace(/&nbsp;/g," ");
        pp = pp.replace(/&nbsp;/g," ");
        
        /** remove html tag from string **/
        var is_tc_html = $(tc).is('*');
        var is_pp_html = $(pp).is('*');
        if(is_tc_html === true){
            tc = $(tc).text();
        }
        if(is_pp_html === true){
           pp = $(pp).text();
        }
        if(tc != "" && tc.length < 500){
            e.preventDefault();
            $(".term_conditions_error").html("Please enter atleast 500 words");
            return false;
        }else{
            $(".term_conditions_error").html("");
        }
        if(pp != "" && pp.length < 500){
            e.preventDefault();
            $(".privacy_policy_error").html("Please enter atleast 500 words");
            return false;
        }else{
            $(".privacy_policy_error").html("");
        }
    });
    $(document).on("change",".f_term_privacy_toggle input[type='checkbox']",function(){
      var ischecked = ($(this).is(':checked')) ? 1 : 0;
      if(ischecked == 1){
          $(".term_pryc_scton").css("display","block");
      }else{
          $(".term_pryc_scton").css("display","none"); 
      }
});
/** end term conditions tab **/ 
    
   /*************end footer js on page load****************/
   
/*********** end all js of document ready **********/
});

/*********************setting page js*****************************/
$('#about_info_form').submit(function (e) {
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    $.ajax({
        type: 'POST',
        url: baseUrl+'account/ChangeProfile/customer_edit_profile',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            location.reload(true);
        }
    });
});
$(document).on('change', '.switch-custom-usersetting-check.remind_mail input[type="checkbox"]', function () {
    var status = $(this).prop('checked');
    var key = $(this).attr('data-key');
    var data_id = $(this).attr('data-cid');
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    if (ischecked) {
        $(".checkuserstatus_" + data_id).text('Active');
    } else {
        $(".checkuserstatus_" + data_id).text('Inactive');
    }
    $.ajax({
        type: 'POST',
        url: baseUrl+"account/ChangeProfile/change_status_usersettings",
        data: {status: status, data_id: data_id, data_key: key},
        success: function (data) {
        }
    });
});
$('#edit_form_data').submit(function (e) {
    e.preventDefault();
    // console.log("i am here"); 
    var formData = new FormData($(this)[0]);
     $(".ajax_loader").show();
    $.ajax({
        type: 'POST',
        url: baseUrl+"account/ChangeProfile/edit_profile_image_form",
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {  
         $(".ajax_loader").hide();            
            location.reload(true);
        }
    });
});
$('#edit_button').click(function () {
    $('.dp').hide();
    $('.changeprofilepage').css('display', 'block');
});
$(document).on('click', '#about_infoform', function () {
    $('.settingedit-box').css('display', 'none');
    $('.carddetailsedit').css('display', 'block');
});

$(document).on('click','#cancelcard',function(){
    $('.settingedit-box').css('display', 'block');
    $('.carddetailsedit').css('display', 'none');
});
$(document).on('click', '.chossbpanss', function () {
    $('.billing_plan_view').css('display', 'none');
    $('.f_chng_ugrd_blpln').css('display', 'block');
    $('.new_plan_billingfr_chng').css('display', 'flex');
    $('.purchase_req').css('display', 'block');
    $('.bckbtntoback').css('display', 'block');
    $('.slctpln_subs').css('display', 'none');
});
$(document).on('click', '.backfromnewplan', function () {
    $('.billing_plan_view').css('display', 'block');
    $('.f_chng_ugrd_blpln').css('display', 'none');
    $('.new_plan_billingfr_chng').css('display', 'none');
    $('.purchase_req').css('display', 'none');
    $('.bckbtntoback').css('display', 'none');
    $('.slctpln_subs').css('display', 'block');
});
$('.stayhere').click(function (e) {
    var x = window.pageXOffset,
    y = window.pageYOffset;
    $(window).one('scroll', function () {
        window.scrollTo(x, y);
    })
});
function validateAndUpload(input) {
//    alert("In Function"); 
    var URL = window.URL || window.webkitURL;
    var file = input.files[0];
    var filetype = $(input).attr('data-file');
  //  console.log('filetype',filetype);
   var file_val = $(input).val();
   var result = file_val.substring(file_val.lastIndexOf('\\')+1);
   if(filetype == 'logo'){
     $('#changelogovalue').html(result);
   }else if(filetype == 'favicon'){
      $('#changefaviconvalue').html(result); 
   }
    if (file) { 
        //console.log(URL.createObjectURL(file));
        // console.log(file);
        $(".imagemain").hide();

        $(".imagemain2").show();
        $(".imagemain3").show();
        $("#image3").attr('src', URL.createObjectURL(file));

        $(".dropify-wrapper").show();
    }
}

/*********************end setting page js*****************************/

/*********************agency setting page js*****************************/
$(document).on('click', '.email_preview', function () {
    var email_id = $(this).attr('data-id');
    var email_header = $(this).find('.inneremail_header_temp_cont').html();
    var email_body = $('#txtEditor').Editor("getText");
    var email_footer = $(this).find('.inneremail_footer_temp_cont').html();
    var res = email_body.replace("{{MESSAGE}}", "<div class='left_message_padding'>{{MESSAGE}}</div>");
    $('.preview_section').html("");
    if (email_body != '') {
        $('.preview_section').html(email_header + res + "<div class='email_footer_padding'>" + email_footer + "</div>");
    }
});
function make_swatchs() {

        $(".colorpicker").append('<div class="swatches_container"><div class="swatches_row"></div></div>');
        for (var i = 0; i < 7; i++) {
            $(".swatches_container .swatches_row").append('<div class="color_cell"><div class="color_box color_' + i + '"></div></div>')
        }
}
$('#edit_button').click(function () {
    $('.change_logo').hide();
    $('.changeprofilepage').css('display', 'block');
});
$(document).on('click', '.edit_email_temp', function () {

    var editid = $(this).attr('data-id');
    $(".email_preview").attr('data-id', editid);
    $(".send_test_email").attr('data-id', editid);
    $('.reset_email_template').html("");
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: baseUrl+"account/Emailcontent/getemail_content",
        data: {"id": editid},
        success: function (data) {
            var email_info = data[0];
            if (email_info.email_slug == 'email_header' || email_info.email_slug == 'email_footer') {
                $(".single_edit_action_btns").hide();
                $(".hide_subjecttoheaderfooter").hide();
            } else {
                $(".single_edit_action_btns").show();
                $(".hide_subjecttoheaderfooter").show();
            }
            $('#email_id').val(email_info.id);
            $('#user_id').val(email_info.user_id);
            $('#email_name').val(email_info.email_name);
            $('#identifier').val(email_info.identifier);
            $('#subject').val(email_info.email_subject);
            $('#txtEditor').text(email_info.email_text);

            $('.email_preview_lst').hide();
            $('.edit_single_email_temp').modal('show');
            $('html, body').animate({scrollTop: $('#Admin_Email').offset().top - 20}, 'slow');

            var content = $('#txtEditor').val();
            $('.Editor-editor').html(content);
            if (email_info.user_id != 0) {
                $('.reset_email_template').html('<a class="reset_default" href="'+baseUrl+'account/Emailcontent/delete_emailtemp/' + email_info.id + '">Reset Default</a>');
            }
            var identifier = JSON.parse(email_info.identifier_description);
            $('.identifier_description').html('');
            var i = 1;
            $.each(identifier, function (key, value) {
                $('.identifier_description').append('<div class="identifier_single"><h3 id="slct_identifiers_' + i + '">' + key + "</h3><p>" + value + '</p><a href="#" name="copy_pre" id="slct_identifiers_' + i + '">copy</a></div>');
                i++;
            });
            if ($('.identifier_description').html() == '') {
                $('.identifier_description').html('There is no identifier for this template.');
            }
        }
    });
});
$(document).on('change', '.switch-custom-usersetting-check.activate-emails input[type="checkbox"]', function () {
    var status = $(this).prop('checked');
    var data_id = $(this).attr('data-id');
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    if (ischecked) {
        $(".checkstatus_" + data_id).text('Enable');
    } else {
        $(".checkstatus_" + data_id).text('Disable');
    }
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: baseUrl+"account/Emailcontent/editemail",
        data: {"id": data_id, "enable_disable": 1, "status": status},
        success: function (data) {
            location.reload();
        }
    });
});
$(document).on('click', '.emailPreview', function () {
    var email_id = $(this).attr('data-id');
    //console.log(email_id);
    var email_header = $(this).find('.email_header_temp_cont').html();
    var email_body = $(this).find('.email_body_cont').html();
 //   console.log(email_body);
    var email_footer = $(this).find('.email_footer_temp_cont').html();
    var res = email_body.replace("{{MESSAGE}}", "<div class='left_message_padding'>{{MESSAGE}}</div>");


    $('.email_preview_section').html("");
    if (email_body != '') {
        $('.email_preview_section').html(email_header + res + "<div class='email_footer_padding'>" + email_footer + "</div>");
    }
});
$(document).on('click', '.contact_preview', function () {
    $('#contact_detail').modal('show');
});
$(document).on('change', '#primary_color', function () {
    var primary_color = $('#primary_color').val();
    $('.bgcolor_primary').css('background', '' + primary_color + '');
});
var success_msg_color = $('#success_msg_color').val();
$('#success_msg_color').change(function () {
    success_msg_color = $(this).val();
     $('.success_msg').css('background', '' + success_msg_color + '');
     $('.alert-success').css('color', '' + success_msg_color + '');
});

var success_msg_bg_color = $('#success_msg_bg_color').val();
$('#success_msg_bg_color').change(function () {
    success_msg_bg_color = $(this).val();
    $('.success_bg_msg').css('background', '' + success_msg_bg_color + '');
    $('.alert-success').css('background', '' + success_msg_bg_color + '');

});
var error_msg_color = $('#error_msg_color').val();
$('#error_msg_color').change(function () {
    var error_msg_color = $(this).val();
    $('.error_msges').css('background', '' + error_msg_color + '');
    $('.alert-danger').css('color', '' + error_msg_color + '');
});
var error_msg_bg_color = $('#error_msg_bg_color').val();
$('#error_msg_bg_color').change(function () {
    var error_msg_bg_color = $(this).val();
    $('.error_bg_msg').css('background', '' + error_msg_bg_color + '');
    $('.alert-danger').css('background', '' + error_msg_bg_color + '');
});
$('.alert-danger').css('background', '' + error_msg_color + '');

$(document).on('change', '#setting_tab_bkg_color', function () {
    var setting_tab_bkg_color = $('#setting_tab_bkg_color').val();
    $('.setting_tab').css('background', '' + setting_tab_bkg_color + '');
});

$(document).on('change', '#secondary_color', function () {
    var secondary_color = $('#secondary_color').val();
    $('.bgcolor_secondary').css('background', '' + secondary_color + '');
});
$(document).on('change', '#inprog_font_color,#inprog_bkg_color', function () {
    var inprog_font_color = $('#inprog_font_color').val();
    var inprog_bkg_color = $('#inprog_bkg_color').val();
    $('.agency_inprogress').css('color', '' + inprog_font_color + '');
    $('.agency_inprogress').css('background', '' + inprog_bkg_color + '');
    $('.font_inprogress').css('background', '' + inprog_font_color + '');
    $('.bgcolor_inprogress').css('background', '' + inprog_bkg_color + '');
});

$(document).on('change', '#revsion_font_color,#revsion_bkg_color', function () {
    var revsion_font_color = $('#revsion_font_color').val();
    var revsion_bkg_color = $('#revsion_bkg_color').val();
    $('.agency_revision').css('color', '' + revsion_font_color + '');
    $('.agency_revision').css('background', '' + revsion_bkg_color + '');
    $('.font_revision').css('background', '' + revsion_font_color + '');
    $('.bgcolor_revision').css('background', '' + revsion_bkg_color + '');
});

$(document).on('change', '#review_font_color,#review_bkg_color', function () {
    var review_font_color = $('#review_font_color').val();
    var review_bkg_color = $('#review_bkg_color').val();
   // console.log(review_bkg_color);
    $('.agency_review').css('color', '' + review_font_color + '');
    $('.agency_review').css('background', '' + review_bkg_color + '');
    $('.font_review').css('background', '' + review_font_color + '');
    $('.bgcolor_review').css('background', '' + review_bkg_color + '');
});

$(document).on('change', '#inqueue_font_color,#inqueue_bkg_color', function () {
    var inqueue_font_color = $('#inqueue_font_color').val();
    var inqueue_bkg_color = $('#inqueue_bkg_color').val();
    $('.inqueue_status_button').css('color', '' + inqueue_font_color + '');
    $('.inqueue_status_button').css('background', '' + inqueue_bkg_color + '');
    $('.font_review').css('background', '' + inqueue_font_color + '');
    $('.bgcolor_review').css('background', '' + inqueue_bkg_color + '');
});

$(document).on('change', '#hold_font_color,#hold_bkg_color', function () {
    var hold_font_color = $('#hold_font_color').val();
    var hold_bkg_color = $('#hold_bkg_color').val();
    $('.hold_status_button').css('color', '' + hold_font_color + '');
    $('.hold_status_button').css('background', '' + hold_bkg_color + '');
    $('.font_review').css('background', '' + hold_font_color + '');
    $('.bgcolor_review').css('background', '' + hold_bkg_color + '');
});

$(document).on('change', '#cancel_font_color,#cancel_bkg_color', function () {
    var cancel_font_color = $('#cancel_font_color').val();
    var cancel_bkg_color = $('#cancel_bkg_color').val();
    // console.log(review_bkg_color);
    $('.cancel_status_button').css('color', '' + cancel_font_color + '');
    $('.cancel_status_button').css('background', '' + cancel_bkg_color + '');
    $('.font_review').css('background', '' + cancel_font_color + '');
    $('.bgcolor_review').css('background', '' + cancel_bkg_color + '');
});

$(document).on('change', '#draft_font_color,#draft_bkg_color', function () {
    var draft_font_color = $('#draft_font_color').val();
    var draft_bkg_color = $('#draft_bkg_color').val();
    $('.draft_status_button').css('color', '' + draft_font_color + '');
    $('.draft_status_button').css('background', '' + draft_bkg_color + '');
    $('.font_review').css('background', '' + draft_font_color + '');
    $('.bgcolor_review').css('background', '' + draft_bkg_color + '');
});


$(document).on('change', '#complete_font_color,#complete_bkg_color', function () {
    var complete_font_color = $('#complete_font_color').val();
    var complete_bkg_color = $('#complete_bkg_color').val();
    $('.agency_completed').css('color', '' + complete_font_color + '');
    $('.agency_completed').css('background', '' + complete_bkg_color + '');
    $('.font_completed').css('background', '' + complete_font_color + '');
    $('.bgcolor_completed').css('background', '' + complete_bkg_color + '');
});

$(document).on('change', '#approve_button_color,#approve_font_color', function () {
    var approve_font_color = $('#approve_font_color').val();
    var approve_button_color = $('#approve_button_color').val();
    $('.agency_approve').css('background', '' + approve_button_color + '');
    $('.agency_approve').css('color', '' + approve_font_color + '');
    $('.font_approve').css('background', '' + approve_font_color + '');
    $('.bgcolor_approve').css('background', '' + approve_button_color + '');
});

$(document).on('click', '.identifiers_popup', function (e) {
    e.preventDefault();
    $('#identifierDetail').modal('show');
});
$(document).on('click', '.identifier_close', function (e) {
    e.preventDefault();
    $('#identifierDetail').modal('hide');
});
function brandsShow(is_checked) {
    if (is_checked == false) {
        $('#brandshowing').css('display', 'block');
    } else {
        $('#brandshowing').css('display', 'none');
    }
}
$(document).on('change', '#view_only', function () {
    var is_view = $(this).is(":checked");
    if (is_view == true) {
        $('#add_requests').attr('checked', false);
        $('#comnt_requests').attr('checked', false);
        $('#del_requests').attr('checked', false);
        $('#billing_module').attr('checked', false);
        $('#approve_reject').attr('checked', false);
        $('#show_all_project').attr('checked', false);
        $('#add_brand_pro').attr('checked', false);
        $('#upload_draft').attr('checked', false);
        $('#white_label').attr('checked', false);
        $('#file_management').attr('checked', false);
        $('#assign_designer_to_client').attr('checked', false);
        $('#assign_designer_to_project').attr('checked', false);
    }
});

$(document).on('change', '.uncheckview', function () {
    var is_view = $('#view_only').is(":checked");
    if (is_view == true) {
        $('#view_only').attr('checked', false);
    }
});

$(document).on('change', '.switch-custom-usersetting-check .slct_domain_subdomain', function () {
    var domain_or_subdomain = ($(this).is(":checked"))?1:0;
   // console.log(domain_or_subdomain);
    if (domain_or_subdomain === 1) {
        $(".domain_nm").show();
//        $(".chkdomain_ssl").show();
        $(".subdomain_nm").hide();
        $("#setmaindomain_url").trigger("click");
    } else {
        $(".subdomain_nm").show();
//        $(".chkdomain_ssl").hide();
        $(".domain_nm").hide();
    }
});

//$(document).on('change', '.ssl_domains', function () {
//    var ssl_or_not = $(this).val();
//   // console.log('ssl_or_not', ssl_or_not);
//    if (ssl_or_not === "1") {
//        $("#sslmsg_sec").trigger("click");
//    }
//});
$(document).on('change', '.is_smtp_enabele input[type="checkbox"]', function () {
        var ischecked = ($(this).is(':checked')) ? 1 : 0;
        if (ischecked) {
            $(".port_settings").css('display', 'block');
            $("#email_setting").attr('disabled', false);
            $("#from_name").removeAttr("readonly");
            $("#from_email").removeAttr("readonly");
            $("#reply_name").removeAttr("readonly");
            $("#reply_email").removeAttr("readonly");
        } else {
            $(".port_settings").css('display', 'none');
}
});
$("#agency_user_email_setting").submit(function () {
    var ischecked = ($('.is_smtp_enabele input[type="checkbox"]').is(':checked')) ? 1 : 0;
    if (ischecked == '1') {
        var port = $('#port').val();
        var host_name = $('#host_name').val();
        var host_username = $('#host_username').val();
        var host_password = $('#host_password').val();
        var is_password = $('#host_password').attr("data-is_password");
        var email_secure = $('#email_secure').val();
        var from_email = $('#from_email').val();
        if (from_email == '') {
            $('.from_email_text .error_msg').html('<span class="validation">Please enter from email.</span>');
            return false;
        } else {
            $('.from_email_text .error_msg').html('');
        }
        if (port == '') {
            $('.port_text .error_msg').html('<span class="validation">Please enter port number.</span>');
            return false;
        } else {
            $('.port_text .error_msg').html('');
        }
        if (host_name == '') {
            $('.host_name .error_msg').html('<span class="validation">Please enter host name.</span>');
            return false;
        } else {
            $('.host_name .error_msg').html('');
        }
        if (host_username == '') {
            $('.host_username .error_msg').html('<span class="validation">Please enter host username.</span>');
            return false;
        } else {
            $('.host_username .error_msg').html('');
        }
        if (host_password == "" && is_password == "") {
            $('.host_password .error_msg').html('<span class="validation">Please enter host password.</span>');
            return false;
        } else {
            $('.host_password .error_msg').html('');
        }
        if (email_secure == '') {
            $('.email_secure .error_msg').html('<span class="validation">Please enter email secure.</span>');
            return false;
        } else {
            $('.email_secure .error_msg').html('');
        }
        return true;
    }
});
$(document).on('click', '.send_test_email', function () {
    $('#user_email_adrs').val(loggedinemail);
    var template_id = $(this).attr('data-id');
    $('#template_id').val(template_id);
});
/**********Live chat restriction***************/
$('#save_livechat').submit(function () {
    var livescript = $("textarea#live_script").val();
  //  console.log(livescript);
    var str2 = "<" + "?php";
    var str3 = "ajax";
    var str4 = "<script>";
    var str5 = "</" + "script>";
   // console.log(str2);
    if (livescript.indexOf(str2) != -1) {
        $(".scriptvalidation_error").html("<p>You cann't use php tag. please use only script here for live chat");
        return false;
    } else if (livescript.indexOf(str3) != -1) {
        $(".scriptvalidation_error").html("<p>You cann't use ajax code. please use only script here for live chat");
        return false;
    } else if (livescript.indexOf(str4) != -1 && livescript.indexOf(str5) == -1) {
        $(".scriptvalidation_error").html("<p>Your script tag is started but not closed. please correct it</p>");
        return false;
    } else {
        $(".scriptvalidation_error").html("");
        return true;
    }

});
/**********End live chat restriction***************/
/********************copy identifiers*****************/
$(document).on("click", "a[name=copy_pre]", function () {
    var id = $(this).attr('id');
    var el = document.getElementById(id);
    var range = document.createRange();
    range.selectNodeContents(el);
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
    document.execCommand('copy');
    return false;
});
/********************end copy identifiers*****************/
/*********************end agency setting page js*****************************/
/***************footer js****************/
$(document).on('click', ".ChngPln", function (e) {
  //  console.log('test');
    e.preventDefault();
    var data_id = $(this).attr('data-value');
    var data_price = $(this).attr('data-price');
    var data_inprogress = $(this).attr('data-inprogress');
    var display_name = $(this).attr('data-display_name');
    jQuery('.display_namepln').val(display_name);
    jQuery('.plan_price').val(data_price);
    jQuery('.in_progress_request').val(data_inprogress);
    $(".plan_name_to_upgrade").val(function () {
        return data_id;
    });
    $("#ChangePlan").modal('hide');
    $("#SelectPackForUP").modal('hide');
});

$(document).on("click", "ul.list-unstyled.list-notificate li a", function (e) {
    $('.modal-backdrop.in').css('opacity', '0');
});

//$(document).on('click', ".Select_PackageForUP", function (e) {
//    e.preventDefault();
//    $(this).attr('data-url');
//    $('#Select_PackForUP').trigger('click');
//});

$(document).on('click', ".UpgrdSubs", function (e) {
    e.preventDefault();
    $(this).attr('data-url');
    $("#TrialExpire").css("display", "none");
    $(".modal-backdrop.fade.in").remove();
    $('#Select_PackForUP').trigger('click');
});

$(document).on('click', ".upgrd", function (e) {
    e.preventDefault();
    var data_id = $(this).attr('data-value');
    var data_price = $(this).attr('data-price');
    var data_inprogress = $(this).attr('data-inprogress');
	var applycoupn = $(this).attr('data-applycoupn');
	console.log("applycoupn",applycoupn);
    jQuery('.plan_price').val(data_price);
    jQuery('#final_plan_price').val(data_price);
    jQuery('#subs_plan_price').val(data_price);
    jQuery('.in_progress_request').val(data_inprogress);
	jQuery('.couponinserted_ornot').val(applycoupn);
    $(this).attr('data-url');
    $(".plan_name").val(function () {
        return data_id;
    });
    jQuery('.disc-code').val('');
    jQuery('.ErrorMsg').css('display', 'none');
    jQuery('.coupon-des-newsignup').css('display', 'none');
    jQuery('.total-pPrice').html('<div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p total-biled text-left">Total Billed</div><div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p text-right"><span class="appened_price">$' + data_price + '</span></div><div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p g-totl text-left">Grand Total</div><div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p g-totl-prc text-right"><span class="appened_price">$' + data_price + '</span></div>');
//    console.log("upgrd_account");
   // $("#SelectPackForUP").css("display", "none");
    $("#SelectPackForUP").modal('hide');
   //$(".modal-backdrop.fade.in").remove();
    jQuery('#upgrade_account').trigger('click');

});
function dateFormat(val, ths) {
    if (val.length === 2) {
        if (ths !== "") {
            $(ths).val($(ths).val() + '/');
        } else {
            $('.expir_date').val($('.expir_date').val() + '/');
        }
    }
}
$("#upgrade_useraccount").submit(function (e) {
    e.preventDefault();
    $('.bill-infoo .error-msg').html("");
    var customer_id = $('#customer_id').val();
    var plan_name = $('.plan_name').val();
    var in_progress_request = $('.in_progress_request').val();
    var card_number = $('#card_number').val();
    var plan_price = $('.plan_price').val();
    var final_plan_price = $('#final_plan_price').val();
    var expir_date = $('#expir_date').val();
    var state_tax = $('#state_tax').val();
    var state = $('#state_upgrd').val();
    var cvc = $('#cvc').val();
    var term = $('#term').val();
    var email = $('#email').val();
    var discount = $('#discCode').val();
    var url;
    $('.loading_account_procs').html('<span class="loading_process" style="text-align:center;"><img src="'+$assets_path+'img/ajax-loader.gif" alt="loading"/ style="margin: auto;display: block;"></span>');
    $('#payndupgrade').prop('disabled', false);
    if (is_trail == 1 || is_without_pay_user == 1) {
        url = baseUrl+"account/request/upgrADE_account";
    } else {
        url = baseUrl+"account/ChangeProfile/change_current_plan";
    }
    $.ajax({
        type: 'POST',
        url: url,
        dataType: "json",
        data: {
            customer_id: customer_id,
            plan_name: plan_name,
            card_number: card_number,
            expir_date: expir_date,
            cvc: cvc,
            term: term,
            discount: discount,
            email: email,
            in_progress_request: in_progress_request,
            plan_price: plan_price,
            final_plan_price: final_plan_price,
            state_tax: state_tax,
            state: state
        },
        success: function (response) {
            $('.loading_account_procs').html('');
            $('#payndupgrade').prop('disabled', false);
            if (response.success) {
                $("#Upgradeacoount").css("display", "none");
                $(".modal-backdrop.fade.in").remove();
                $('#subscribed_USR').trigger('click');
            } else {
                $('.bill-infoo .error-msg').html(response.error);
            }
        },
        error: function (response) {
            $('.loading_account_procs').html('');
            $('#payndupgrade').prop('disabled', false);
            $('.bill-infoo .error-msg').html('Error occurred while upgrading account, please try again or contact us!');
        }
    });
    return false;
});

$(document).on('blur', '.disc-code', function (e) {
    e.preventDefault();
    
    var dicountCode = $(this).val();
    var setSelected = $(this).closest(".contactForm").find(".subs_plan_price").val();
    var cust_id = $(this).closest(".contactForm").find(".subuser_id").val();
    var apply_couponor_not = $(this).closest(".contactForm").find(".couponinserted_ornot").val();
//    var selectedplan = $("#plan_name").val();
//    var orginal_price = setSelected.strike();
    $("span.CouponSucc_code, span.CouponErr_code").remove();
    if (dicountCode == '') {
        $('.ErrorMsg').css('display', 'none');
        $('.coupon-des-newsignup').css({"display": "none"});
        $('.toatal-bill-p.text-right').html(' $' + setSelected);
        $discount_amount = 0;
        $discount_notes = '';
        return;
    } else {
        $('.ErrorMsg').css('display', 'block');
    }
    
    $('.ErrorMsg.epassErrorSuccess.discount-code').html('<span class="CouponSucc_code"><img src="'+$assets_path+'img/ajax-loader.gif" alt="loading"/></span>');
    delay(function () {
        $.ajax({
            type: 'POST',
            url: baseUrl+'welcome/CheckCouponValidation',
            data: {dicountCode: dicountCode, apply_couponor_not: apply_couponor_not, userid: cust_id},
            dataType: 'json',
            success: function (data) {
                $('.ErrorMsg').css('display', 'block');
                $('.couponinserted_ornot').val(data.inertcoupon);
                if (data.status == '1') {
                    if (data.return_data.valid == '1') {
                        if (data.return_data.amount_off !== null) {
                            var str = data.return_data.amount_off;
                            var resStr = str / 100;
                            var finalAmount = setSelected - resStr;
                            $discount_amount = resStr;
                        } else {
                            var TotalPercent = data.return_data.percent_off;
                            var calcPrice = setSelected * (TotalPercent / 100);
                            var finalAmount = setSelected - calcPrice;
                            $discount_amount = calcPrice;
                        }
                        var total_finalAmountt = finalAmount.toFixed(2);
                       // $('.toatal-bill-p.text-right').html(' $' + orginal_price + ' $' + total_finalAmountt);
                        jQuery('#final_plan_price').val(total_finalAmountt);
                        jQuery('#s_final_plan_price').val(total_finalAmountt);
                        $('.CouponErr_code').css({"display": "none"});
                        $('.coupon-des-newsignup').css('display', 'block');
                        $('.ErrorMsg.epassErrorSuccess.discount-code').html('<span class="CouponSucc_code"><i class="fa fa-check-circle"></i> Coupon Applied</span>');
                        if (data.return_data.duration == 'forever') {
                            discount_notes = '<p>Forever</p>';
                         //   $('.coupon-des-newsignup').html('<p>Its for forever</p>');
                        } else if (data.return_data.duration == 'once') {
                            discount_notes = '<p>One Time Only</p>';
                           // $('.coupon-des-newsignup').html('<p>Its for once</p>');
                        } else {
                            discount_notes = '<p>First ' + data.return_data.duration_in_months + ' Months Only</p>';
                          //  $('.coupon-des-newsignup').html('<p>Its for ' + data.return_data.duration_in_months + ' months</p>');
                        }
                        $discount_notes = discount_notes;
                        Gettaxbasisonstate('',setSelected);
                    }
                } else {
                   // $('.toatal-bill-p.text-right').html(' $' + setSelected);
                    $('.CouponSucc_code').css({"display": "none"});
                    $('.ErrorMsg.epassErrorSuccess.discount-code').html('<span class="CouponErr_code"><i class="fa fa-times-circle"></i> ' + data.message + '</span>');
                    $('.coupon-des-newsignup').css({"display": "none"});
                    $discount_amount = 0;
                    $discount_notes = '';
                    Gettaxbasisonstate('',setSelected);
                }
            }
        });
    }, 1000);
    Gettaxbasisonstate('',setSelected);

});
/***************State Tax Script*************************/
jQuery('#state_upgrd').on('change', function () {
    var statename = this.value;
    var plnprc = $(this).closest(".contactForm").find(".subs_plan_price").val();
    if (statename == '---') {
        jQuery('.ErrorMsg.state_validation').css('display', 'block');
        jQuery('.ErrorMsg.state_validation').html('<span class="validation">Please select any other state</span>')
        jQuery('#state').val("");
    } else {
        jQuery('.ErrorMsg.state_validation').html('')
    }
    Gettaxbasisonstate(statename,plnprc);
});

function Gettaxbasisonstate(statename,plnprc) {
    if(statename == '' || typeof statename == 'undefined'){
        var statename = jQuery('#state_upgrd').val();
    } else {
        var statename = statename;
    }
//    var grand_total = jQuery('#appened_price').html();
    $plan_price = plnprc;
//    console.log("hello");
//    console.log('$plan_price',$plan_price);
//    console.log('statename',statename);
    var final_price = parseFloat($plan_price) - parseFloat($discount_amount);
    if ((statename in jqueryarray)) {
        var tax = jqueryarray[statename];
        var total = parseFloat(final_price) * (parseFloat(tax) / 100);
        var tax_rounded = total.toFixed(2);
        var percent_amount = parseFloat(final_price) + total;
        var total_percent_amount = percent_amount.toFixed(2);
        $tax_amount = total;
        $tax_value = tax;
//        console.log('$tax_amount',$tax_amount);
        jQuery('#tax_amount').val(total);
        jQuery('.tax_prc_for_texas').css("display", "block");
//        jQuery('.tax_prc_for_texas').html('Sales Tax(' + tax + '%) :<span id="tax_prc" class="box item12"><span>$' + tax_rounded + '</span></span>');
//        jQuery('.toatal-bill-p.text-right').html('<span class="appened_price">$' + total_percent_amount + '</span>');
        jQuery('.plan_price').val(total_percent_amount);
        jQuery('#state_tax').val(tax);

    } else {
        jQuery('.tax_prc_for_texas').css("display", "none");
        jQuery('#tax_amount').val("0");
//        jQuery('.toatal-bill-p.text-right').html('<span class="appened_price">$' + final_price + '</span>');
//        jQuery('.plan_price').val(final_price);
        jQuery('#state_tax').val("0");
        $tax_amount = 0;
        $tax_value = 0;
}
showPriceSectionHTML();
}

function showPriceSectionHTML(){
    $plan_price = parseFloat($plan_price);
    $tax_amount = parseFloat($tax_amount);
    $discount_amount = parseFloat($discount_amount);
    var total_price = (($plan_price +  $tax_amount) - $discount_amount);
    if(total_price % 1 !== 0){
        total_price = total_price.toFixed(2);
    }
    var htmll = '<div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p total-biled text-left">Total Billed:</div><div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p text-right">$<span>'+$plan_price+'</span></div></p>';
    if(parseFloat($discount_amount) > 0){
        htmll += '<div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p discounted_pric text-left">Discount: </div><div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p discount-p text-right"><span>- $'+$discount_amount.toFixed(2)+'</span></div>';	
    }
    if(parseFloat($tax_amount) > 0){
            htmll += '<div class="col-lg-12 toatal-bill-p tax_prc_for_texas">Sales Tax ('+$tax_value.toFixed(2)+'%) :<span id="tax_prc" class="box item12"><span>$'+$tax_amount.toFixed(2)+'</span></span></div>';
    }
    htmll += '<hr>';
    htmll += '<div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p g-totl text-left">Grand Total: </div><div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p g-totl-prc text-right"><span>';
    if(parseFloat($discount_amount) > 0){
        var strikeamount = parseFloat($plan_price) + parseFloat($tax_amount);
            htmll += '<strike>$';
            htmll += Math.round(strikeamount * 100) / 100;
            htmll += '</strike> $'+total_price;
    } else{
            htmll += '$'+total_price;
    }
    htmll += '</span></div>';
    if(parseFloat($discount_amount).toFixed(2) > 0){
            htmll += '<span class="coupon-des-newsignup">'+$discount_notes+'</span>';
    }
    $('.total-pPrice').html(htmll);
}
/***************End State Tax Script******************/
/***************requests_signup*************************/
$(document).on('click', '.upgrd_fortynine_pln', function () {
    var plan_name = $('.plan_name').val();
    var plan_price = $('.plan_price').val();
    var display_name;
    if (plan_price == '99') {
        display_name = '3 active requests';
    } else if (plan_price == '149') {
        display_name = '5 active requests';
    } else {
        display_name = 'unlimited requests';
    }
    if (jQuery.inArray(plan_name, new_plans)) {
        $('.update_text').text('By purchasing $' + plan_price + ' plan, You will get ' + display_name);
    }
    $("#upgradefortynineplan").modal('hide');
    // $(".modal-backdrop.fade.in").remove();
});
/***************end requests_signup*******************/
$(document).on('click', '.help_user', function () {
    $('#contact_detail').show();
});
$(document).on('click', '.contact_popup', function () {
    $('#contact_detail').hide();
    $('.sharing-popup').addClass('in');
});
// $(document).on('click', '.upgrd_fortynine_pln', function () {
//     $("#upgradefortynineplan").css("display", "none");
//     $(".modal-backdrop.fade.in").remove();
// });
$(function ()
{
    $('#save').click(function ()
    {
        $('#txtEditor').text($('#txtEditor').Editor("getText"));
    });
});
$(document).on('click', '.remove_selected', function () {
  var r = confirm('Are you sure you want to delete this file?');
  if (r == true) {
    var reqid = $(this).attr('data-id');
    var filename = $(this).attr('data-name');
    $.ajax({
      type: "POST",
      url: baseUrl+"account/Emailcontent/delete_emailtemp_file",
      data: {"request_id": reqid, "filename": filename},
      success: function (data) {
        window.location.reload();
      }
    });
  } else {
  }
});
$inprogress_reqofuser = "";
$active_reqofuser = "";
$edituser = 0;
$(document).ready(function(){
$(document).on("click", ".add_subuser_main", function () {
    // alert("hello");
    if($("#is_all").data("isall") == 1){
        $(".step1").hide(); 
    }
    $(".EmailAlert").hide(); 
    $(".sub_user_form").attr("id","add_designer_saas");
    $("#formOpen").val("add");
    if ($('#brandcheck') > 0) {
        $('#brandcheck').chosen();
    }
    var slctplan = $("#requests_type").val();
    var slctplan_name = $("#requests_type option[value='"+slctplan+"']").attr("data-payment");
    $("#sub-userlist").css("display", "none");
    $("#new-subuser").css("display", "block");
    $(".password_toggle").css("display", "block");
    $('#switch_access').prop('checked', true);
    $('#brandshowing').css('display', 'none');
    var user_flag = $(this).data("user_type");
    $("#user_flag").val(user_flag);

    $(".view_only_prmsn").show();
    $('.permissions_for_subuser_client').show();
    $(".access-brand").show();
    $(".requests_limit_toclient").hide();
    if(slctplan_name == 1){
        $(".show_bypss_sec").css("display", "block");
        $(".f_card_number").prop("required", true);
        $(".f_expir_date").prop("required", true);
        $(".f_cvv").prop("required", true);
    }
});
});
$(document).on('click', '.edit_subuser', function (e) {
    if($("#is_all").data("isall") == 1){
        $(".step1").show(); 
    }
    $("#password").attr("disabled",true);
    $("#tabidentity").val($(".TeamManagement").find("li.active").find("a").attr("id")); 
    if($(this).attr("data-user_type")=="designer"){
        $(".step1").find(".next-form").hide(); 
    }
    $(".user_role_main").hide(); 
    $(".sub_user_form").attr("id","edit_designer_saas");   
    $("#formOpen").val("edit");   
    $("#cid").attr("value",$(this).data("id"));  
    $(".main-info-heading").css("opacity", "0");
    $(".bypaspayment").css("opacity", "1");
    $(".fill-sub").css("opacity", "0");
    $("#sub-userlist").css("display", "none");
    $("#new-subuser").css("display", "block");
    $edituser = 1;
    var editid = $(this).attr('data-id');
    var user_type = $(this).attr('data-user_type');
       if (user_type == 'client') {
           $(".access-brand").hide();
           $(".requests_limit_toclient").show();
           $('.permissions_for_subuser_client').hide();
           $(".view_only_prmsn").hide();
           $('#add_requests').attr({'checked': true, 'disabled': true});
           $('#comnt_requests').attr({'checked': true, 'disabled': true});
           $('#del_requests').attr({'checked': true, 'disabled': true});
           $('#billing_module').attr({'checked': false, 'disabled': true});
           $('#app_requests').attr({'checked': true, 'disabled': true});
           $('#downld_requests').attr({'checked': true, 'disabled': true});
           $('#add_brand_pro').attr({'checked': true, 'disabled': true});
           $('#manage_priorities').attr({'checked': false, 'disabled': true});
       } else {
        $(".access-brand").show();
        $('.permissions_for_subuser_client').show();
        $(".view_only_prmsn").show();
        $(".requests_limit_toclient").hide();
    }
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: baseUrl + 'account/ChangeProfile/getSubuserData',
        data: { 'data': editid },
        success: function (data) {
            var subuserdata = data.sub_user_data[0];
            var sub_user_permissions = data.sub_user_permissions[0];
            var brandIDs = data.brandIDs;
            $('#cust_id').val(subuserdata.id);
            $('#fname').val(subuserdata.first_name);
            $('#user_flag').val(user_type);
            $('#lname').val(subuserdata.last_name);
            $('#user_email').val(subuserdata.email);
            $('#phone').val(subuserdata.phone);
            var paymentMode = $('#option_'+subuserdata.plan_name).data("paymentmode");
            if(paymentMode==1){
                $(".show_bypss_sec").show();
            }else{
                $(".show_bypss_sec").hide();
            }
            $('#option_'+subuserdata.plan_name).attr("selected",true);
            
            if ($('#fname').val() != "") {
                $('#fname').parent().children('p').addClass("label-active");
            }
            if ($('#lname').val() != "") {
                $('#lname').parent().children('p').addClass("label-active");
            }
            if ($('#user_email').val() != "") {
                $('#user_email').parent().children('p').addClass("label-active");
            }
            if ($('#phone').val() != "") {
                $('#phone').parent().children('p').addClass("label-active");
            }

                $("#edit_designer_saas").append("<input type='hidden' name='user_role' value='"+subuserdata.role+"'> ");
            if(subuserdata.role=="customer"){
                $(".step1").find(".next").show(); 
                $("#Client").prop("checked",true);
            }else if(subuserdata.role=="designer"){
                $(".step1").find(".next").hide(); 
                $(".step1").find(".submit-form").show(); 
                $("#designer").prop("checked",true);
                enableDisable(1);
            }else{
                $(".step1").find(".next").show(); 
                $("#member").prop("checked",true);
            }
            
            // if (sub_user_permissions.brand_profile_access == 1) {
            //     $('#switch_access').prop('checked', true);
            //     $('#brandshowing').css('display', 'none');
            // }
            // if (sub_user_permissions.brand_profile_access == 0) {
            //     $('#switch_access').prop('checked', false);
            //     $('#brandshowing').css('display', 'block');
            // }
            
            var arr = [];
            $(brandIDs).each(function (i, v) {
                arr.push(v.brand_id);
            });
            $('#brandcheck').chosen();
            $('#brandcheck').val(arr);
            $('#brandcheck').trigger("chosen:updated");

            if (sub_user_permissions.comment_on_req == 1) {
                $('#comnt_requests').prop('checked', true);
            }
            if (sub_user_permissions.add_brand_pro == 1) {
                $('#add_brand_pro').prop('checked', true);
            }
            if (sub_user_permissions.billing_module == 1) {
                $('#billing_module').prop('checked', true);
            }
            if (sub_user_permissions['approve/revision_requests'] == 1) {
                $('#app_requests').prop('checked', true);
            }
            if (sub_user_permissions.add_requests == 1) {
                $('#add_requests').prop('checked', true);
            }
            if (sub_user_permissions.delete_req == 1) {
                $('#del_requests').prop('checked', true);
            }
            if (sub_user_permissions.download_file == 1) {
                $('#downld_requests').prop('checked', true);
            }
            if (sub_user_permissions.view_only == 1) {
                $('#view_only').prop('checked', true);
            }
            if (sub_user_permissions.manage_priorities == 1) {
                $('#manage_priorities').prop('checked', true);
            }
            if (sub_user_permissions.file_management == 1) {
                $('#file_management').prop('checked', true);
            }
            if(sub_user_permissions.white_label == 1){
                $('#white_label').prop('checked', true);
            }
            if(sub_user_permissions.upload_draft == 1){
                $('#upload_draft').prop('checked', true);
            }
            if(sub_user_permissions.approve_reject == 1){
                $('#approve_reject').prop('checked', true);
            }
            if(sub_user_permissions.show_all_project == 1){
                $('#show_all_project').prop('checked', true);
            }
            if(sub_user_permissions.assign_designer_to_client == 1){
                $('#assign_designer_to_client').prop('checked', true);
            }
            if(sub_user_permissions.assign_designer_to_project == 1){
                $('#assign_designer_to_project').prop('checked', true);
            }
        }
    });
});
$(document).on('change', '#password_ques', function () {
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    if(ischecked === 0){
        $('.create_password').css("display","block");
        $('.create_password #password').prop("required",true);
        $('.create_password #password').attr("disabled",false)
    }else{
        $('.create_password').css("display","none");
        $('.create_password #password').prop("required",false);
        $('.create_password #password').attr("disabled",true);
    }
});
$(document).on('change', '#requests_type', function () {
    var values = $(this).val();
    var subuser_reqdata = jQuery.parseJSON(subusers_reqdata);
    var pending_requests;
    var pending_req = (subuser_reqdata.main_inprogress_req) - (subuser_reqdata.subtotal_inprogress_req);
    if (pending_req > 0) {
        pending_requests = pending_req;
    } else {
        pending_requests = 0;
    }
    var slctplan_name =  $('option:selected', this).attr('data-payment');
    if(slctplan_name == 1){
        $(".show_bypss_sec").css("display", "block");
        $(".f_card_number").prop("required", true);
        $(".f_expir_date").prop("required", true);
        $(".f_cvv").prop("required", true);
    }else{
        $(".show_bypss_sec").css("display", "none");
        $(".f_card_number").prop("required", false);
        $(".f_expir_date").prop("required", false);
        $(".f_cvv").prop("required", false);
    }
    $('#total_requests').prop('required', false);
    if (values == 'per_month') {

        $('.billing_cycle_req').show();
        $('#billing_cycle_requests').prop('required', true);
        $('#total_requests').prop('required', false);
        $('#total_active_req').prop('required', true);
        $('.total_one_time_req').hide();
        $('.total_active_req_sec').show();

    } else if (values == 'one_time') {

        $('.billing_cycle_req').hide();
        $('.total_one_time_req').show();
        $('#total_requests').prop('required', true);
        $('#billing_cycle_requests').prop('required', false);
        $('#total_active_req').prop('required', false);
        $('.total_inprogress_req_sec').hide();
        $('.total_active_req_sec').hide();

    } else {
        $('.billing_cycle_req').hide();
        $('.total_one_time_req').hide();
        $('#billing_cycle_requests').prop('required', false);
        $('#total_requests').prop('required', false);
        $('#total_active_req').prop('required', true);
        $('.total_active_req_sec').show();
        $('.total_active_req_sec .error_msg').html("<span class='validation'>As per you plan,  " + subuser_reqdata.main_inprogress_req + " dedicated designers assigned to your account. Out of which you already assigned "+ subuser_reqdata.subtotal_inprogress_req +" designers to your existing clients. So you can assign "+ pending_requests +" more designers to new customers. <a href='"+baseUrl+"account/setting-view#billing' target='_blank'>click here</a> to purchase more requests.</span>");
    }
});

$(document).on('change', '.switch-custom-usersetting-check.activate-user input[type="checkbox"]', function () {
    var status = $(this).prop('checked');
    var data_id = $(this).attr('data-userid');
    var data_email = $(this).attr('data-email');
    var data_name = $(this).attr('data-name');
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    if (ischecked) {
        $(".checkstatus_" + data_id).text('Active');
    } else {
        $('#disablecustomer_acc').click(); 
        $(".checkstatus_" + data_id).text('Inactive');
    }
    $.ajax({
        type: 'POST',
        url: baseUrl+"account/ChangeProfile/enable_disable_subuser_account",
        data: {status: status, data_id: data_id, data_email: data_email, data_name: data_name},
        success: function (data) {
            // window.location.reload();
        }
    });
});
$(document).on('change', '#switch_access', function () {
    var is_checked = $('#switch_access').is(":checked");
    brandsShow(is_checked)
});
//$(document).on('change', '#view_only', function () {
//    var is_view = $('#view_only').is(":checked");
//    if (is_view == true) {
//        $('#add_requests').attr('checked', false);
//        $('#comnt_requests').attr('checked', false);
//        $('#del_requests').attr('checked', false);
//        $('#billing_module').attr('checked', false);
//        $('#app_requests').attr('checked', false);
//        $('#downld_requests').attr('checked', false);
//        $('#add_brand_pro').attr('checked', false);
//        $('#manage_priorities').attr('checked', false);
//    }
//});

$(document).on('change', '.uncheckview', function () {
    var is_view = $('#view_only').is(":checked");
    if (is_view == true) {
        $('#view_only').attr('checked', false);
    }
});
/***reactivate user plan***/
$( "#reActivate" ).submit(function() {
  $('#cancel_subscription_pln').prop('disabled',true);
  $('.loaderimg_forreactivate').attr('src', ''+$assets_path+'img/ajax-loader.gif');

});
/***end reactivate user plan***/

/****change current plan***/
$( "#ChangeCurrent_Plan" ).submit(function() {
  $('#chng_currnt_pln').prop('disabled',true);
  $('.loaderimg_forreactivate').attr('src', ''+$assets_path+'img/ajax-loader.gif');

});
/****change saas current plan***/
$( "#s_changeCurrent_Plan" ).submit(function() {
  $('#s_chng_currnt_pln').prop('disabled',true);
  $('.loaderimg_forreactivate').attr('src', ''+$assets_path+'img/ajax-loader.gif');

});
/***end change current plan***/
$("#sub_user_sbmt").submit(function () {
    var subuser_reqdata = jQuery.parseJSON(subusers_reqdata);
    var user_flag = $("#user_flag").val();
    var total_active_req = $("#total_active_req").val();
    var total_requests = $("#total_requests").val();
    var requests_type = $("#requests_type").val();
    if (user_flag === 'client' && requests_type !== 'one_time') {
        if ($edituser === 0) {
            var inprogress_reqsum = parseInt(subuser_reqdata.subtotal_inprogress_req) + parseInt(total_active_req);
        } else {
            var inprogress_reqsum = parseInt($inprogress_reqofuser) + parseInt(total_active_req);
        }
        if (inprogress_reqsum > subuser_reqdata.main_inprogress_req) {
            $('.total_active_req_sec .error_msg').html('');
            return false;
        }
        else {
            return true;
        }
    }else if(requests_type == 'one_time'){
        if (total_requests > 0) {
            return true;
        }else{
            $('.total_one_time_req .error_msg').html("<span class='validation'>Please enter total number of requests user can add</span>");
            $('.total_inprogress_req_sec .error_msg').html('');
            $('.total_active_req_sec .error_msg').html('');
            return false;
        }
    }else {
        return true;
    }


});
$(document).on('click','.backlist',function(){
  window.location.reload();
});

$('input:radio[name="reason"]').change(function(){
  var others =   $(this).val();
  if(others == 'others'){
    $('.give_reason').slideDown( "slow" );
  }else{
    $('.give_reason').slideUp( "slow" );
    $('#reason_desc').val('');
  }
});
/*************end footer js*******************/
/**************project-info page js*********************/
$(document).on('click','.read_more_btn',function (){
    $(".read_more").toggle();
});
var $fileInput = $('.file-input');
var $droparea = $('.file-drop-area');

$(document).ready(function () {
    $(".curnt_req_status").click(function () {
        $(".mark_cmplt_rq").toggleClass('show_cmpt_btn');
    });

    

    $('#message_container .msgk-chatrow.clearfix').each(function (e) {
        if ($('figure', this).length > 0) {
            $(this).addClass('margin_top');
        }
    });
});

$('.h-c-r li.dropdown a').on('click',function(){
        $(this).parent().find('ul').first().toggle(300);
        $(this).parent().siblings().find('ul').hide(200);
    //Hide menu when clicked outside
    $(this).parent().find('ul').mouseleave(function(){  
      var thisUI = $(this);
      $('html').click(function(){
        thisUI.hide();
        $('html').unbind('click');
    });
  });
});

/****************link share************************/
$(document).on('click', ".share, .share_draft", function () {
   // alert("hello share");
    var pro_id = $(this).attr('data-proid');
    var pro_name = $(this).attr('data-proname');
    var draftid = $(this).attr('data-draftid');
    var draftname = $(this).attr('data-draftname');
    if(pro_id){
        $('#project_name').text('Project Name');
        $("input[type=text][name=pro_name]").val(pro_name);
    }
    if(draftid){
        $('#project_name').text('Draft Name');
        $("input[type=text][name=pro_name]").val(draftname);
    }
    $("input[type=hidden][name=draft_id]").val(draftid); 
    $.ajax({
        method: 'POST',
        url: baseUrl+"account/request/getallprivateaccount",
        data: {share_type: 'private', reqid: pro_id, draftid: draftid},
        success: function (response) {
            $('#shareduser').html('');
            jQuery.each(jQuery.parseJSON(response), function (i, val) {
                if (val.mark_as_completed == '1') {
                    var selectedapproved = 'selected';
                } else {
                    var selectedapproved = '';
                }
                if (val.download_source_file == '1') {
                    var selecteddownload = 'selected';
                } else {
                    var selecteddownload = '';
                }
                if (val.commenting == '1') {
                    var selectedcomment = 'selected';
                } else {
                    var selectedcomment = '';
                }
                var html = '<tr class="shared_user_'+val.id+'">\n\
                <td><span>'+val.user_name+'</span>'+val.email+'</td>\n\
                <td style="text-align:right"><a href="javascript:void(0)" class="del_shareduser" data-id="'+val.id+'"><i class="fa fa-trash" aria-hidden="true"></i></a>\n\<button class="change_permisn" data-toggle="modal" data-target="#change_permissions" type="button" data-shareID="'+val.id+'"><span><i class="fas fa-ellipsis-v"></i></span></button></td>\n\\n\
                </tr>';
                $('#shareduser').append(html);
            });
        }
    });
});
$(document).on('click','.del_shareduser',function(){
    var shared_id = $(this).attr('data-id');
    var req_id = $(this).attr('data-req_id');
    var confrm = confirm("Are you sure to delete this user's permissions ?");
    if (confrm == true) {
        $.ajax({
            method: 'POST',
            url: baseUrl+"account/request/deleteShareduser",
            data: {shared_id: shared_id,req_id: req_id},
            success: function (response) {
                if(response){
                    $('.sharedbymail_block_'+shared_id).html('');
                }
            }
        });
    }
});

$(document).on('click','#public_share,#people_share',function (){
   var pro_id = $(this).attr('data-proid');
   var draftid = $(this).attr('data-draftid');
   $('#public_cmnt,#public_dwnld,#public_cmpld').prop('checked', true);
   $.ajax({
       method: 'POST',
       url: baseUrl+"account/request/publiclinksave",
       data: {proID: pro_id,draftid: draftid},
       success: function (response) {
        var data = JSON.parse(response);
        var permissions = data.permissions[0];
        var publiclinkexists = data.publiclinkexists;
        var is_disabled = data.is_disabled;
       // console.log("is_disabled",is_disabled);
        $("#disabeled_link_pop").attr("data-reqid",pro_id);
        $("#disabeled_link_pop").attr("data-draftid",draftid);
        if(is_disabled == 1){
            $('#disabeled_link_pop').prop('checked', false);
            $('.overhelm').addClass('disbledtoggl');
            $('.label_case').text('Disabled');
        }else{
            $('#disabeled_link_pop').prop('checked', true);
            $('.overhelm').removeClass('disbledtoggl');
            $('.label_case').text('Enabled');
        }
        $("input[type=text][class=public_link]").val(publiclinkexists); 
        $("input[type=text][class=show_publink]").val(publiclinkexists); 
        if(permissions.download_source_file == '1'){
            $('#public_dwnld').prop('checked', true);
             $('.pub_dwn').text('on');
        }else{
            $('#public_dwnld').prop('checked', false); 
            $('.pub_dwn').text('off');
        }
        if(permissions.commenting == '1'){
          $('#public_cmnt').prop('checked', true);
           $('.pub_cmnt').text('on');
      }else{
        $('#public_cmnt').prop('checked', false);
        $('.pub_cmnt').text('off');
    }
    if(permissions.mark_as_completed == '1'){
        $('#public_cmpld').prop('checked', true);
        $('.pub_cmpl').text('on');
    }else{
        $('#public_cmpld').prop('checked', false); 
        $('.pub_cmpl').text('off');
    }
}
             
});
             sharing_permision_ajaxCall(pro_id,draftid); 
             $("#public_"+draftid).css("display","block");
});

$(function() {
  $('.copy-to-clipboard input').click(function() {
    $(this).focus();
    $(this).select();
    document.execCommand('copy');
    $(".copied").text("Copied to clipboard").show().fadeOut(1200);
});
});

$(document).on('click','.copy_public_link',function(){
    $('.show_publink').focus();
    $('.show_publink').select();
    document.execCommand('copy');
    $(".copiedone").text("Link copied").show().fadeOut(1200);
});

$(document).on('click', '.change_permisn,.change_pubpermision', function () {
    var shareID = $(this).attr('data-shareID');
    var reqID = $(this).attr('data-req_id');
    $("input[type=hidden][class=shareID]").val(shareID); 
   $("#private_permisn_"+shareID).slideToggle();
    $this = $(this); 
        //if($this.parents('div').hasClass("ShareLinkHtml")){
//            console.log("has");
            $(this).next(".permision-here").slideToggle();
        //}else{
//               console.log("not_has");
        //}
    $.ajax({
        method: 'POST',
        url: baseUrl+"account/request/getAllselectedpermissions",
        data: {shareID: shareID},
        success: function (response) {
            var data = JSON.parse(response);
            var record = data[0];
            if(record.email != ''){
                $('.user_email').html(record.email);
            }if(record.user_name != ''){
                $('.user_name').html(record.user_name);
            }
            if(record.commenting == '1'){
                $('#switch_Comments').prop('checked', true);
                $('#switch_Comments_'+shareID).prop('checked', true);
                 $('.pri_com').html('On');
            }else{
             $('#switch_Comments').prop('checked', false);
             $('#switch_Comments_'+shareID).prop('checked', false);
              $('.pri_com').html('Off');
            }if(record.mark_as_completed == '1'){
               $('#switch_Completed').prop('checked', true);
               $('#switch_Completed_'+shareID).prop('checked', true);
                $('.pri_compl').html('On');
            }else{
            $('#switch_Completed').prop('checked', false);
            $('#switch_Completed_'+shareID).prop('checked', false);
            $('.pri_compl').html('Off');
            }if(record.download_source_file == '1'){
               $('#switch_downloading').prop('checked', true);
               $('#switch_downloading_'+shareID).prop('checked', true);
               $('.pri_dwn').html('On');
           }else{
            $('#switch_downloading').prop('checked', false);
            $('#switch_downloading_'+shareID).prop('checked', false);
             $('.pri_dwn').html('Off');
        }
    }
    });
});
$(document).on('change', '.edit_permission', function () {
    var shareID = $('.shareID').val();
    var value = [];
    $.each($("input[name='link_permissions']:checked"), function(){            
        value.push($(this).val());
    });
    $.ajax({
        method: 'POST',
        url: baseUrl+"account/request/editsharerequest",
        data: {shareID: shareID,sharekeys: value},
        success: function (response) {
    if(jQuery.inArray("download_source_file", value) == -1){
                 $('.pri_dwn').text('off');
            }else{
                $('.pri_dwn').text('on');
            }
            
            if(jQuery.inArray("mark_as_completed", value) == -1){
                 $('.pri_compl').text('off');
            }else{
                $('.pri_compl').text('on');
            }
            
            if(jQuery.inArray("comment_revision", value) == -1){
                 $('.pri_com').text('off');
            }else{
                $('.pri_com').text('on');
            }
}
});
});
function changePublicpermissions(publink,permisn,reqID=''){
    $.ajax({
        method: 'POST',
        url: baseUrl+"account/request/changepublicpermissions",
        data: {publink: publink,permissions: permisn,reqID:reqID},
        success: function (response) {
            var data = JSON.parse(response);
            var record = data[0];
            if(jQuery.inArray("download_source_file", permisn) == -1){
                 $('.pub_dwn').text('off');
            }else{
                $('.pub_dwn').text('on');
            }
            
            if(jQuery.inArray("mark_as_completed", permisn) == -1){
                 $('.pub_cmpl').text('off');
            }else{
                $('.pub_cmpl').text('on');
            }
            
            if(jQuery.inArray("comment_revision", permisn) == -1){
                 $('.pub_cmnt').text('off');
            }else{
                $('.pub_cmnt').text('on');
            }
        }
    });
}

$(document).on('change', '.public_permsn', function () {
    var publink = $("input[type=text][class=public_link]").val();
    var value = [];
    $.each($("input[name='publiclink_permissions[]']:checked"), function(){            
        value.push($(this).val());
    });
    changePublicpermissions(publink,value);
});

$(".sharemail").keyup(function(){
   $('.show_permisionbox').css('display','block');
});

//Project hold unhold script    
$(document).on('click', '.project_on_hold', function () {
    var status = $(this).attr('data-status');
    var data_id = $(this).attr('data-request_id');
    $.ajax({
        type: 'POST',
        url: baseUrl+"account/Request/hold_unhold_project_status",
        data: {status: status, data_id: data_id},
        success: function (data) {
          window.location.reload();
      }
  });
});

$(document).on('click','.deletemsg',function(){
  var delreq_id = $(this).data('req_id');
    var r = confirm('Are you sure you want to delete this message?');
    if(r == true){
        var delid = $(this).data('delid');
        $.ajax({
            type: 'POST',
            url: baseUrl+"account/Request/Deletemsg/"+delreq_id,
            data: {delid: delid},
            success: function (data) {
                if(data){
                //console.log(delid);
                $('.editdeletetoggle_'+delid).css('display','none');
                $('.took_'+delid).html('<div class="edited_msg deleted_msg"><i class="fa fa-ban" aria-hidden="true"></i> You have deleted this message.</div>');
            }
        }
    });
    }
});


$(document).on('click','.editmsg',function(){
 var editid = $(this).data('editid'); 
 $('.msg_desc_'+editid).css('display','none');
 $('.open-edit_'+editid).css('display','none');
 $('.edit_main_'+editid).css('display','block');
});

$(document).on('click','.cancel_main',function(){
 var msgid = $(this).data('msgid'); 
 $('.edit_main_'+msgid).css('display','none');
 $('.msg_desc_'+msgid).css('display','block');
});

$(document).on('click','.edit_save',function(){
    var editid = $(this).data('id');
    var editreq_id = $(this).data('req_id');
    var msg = $("#edit_main_msg_"+editid).val();
    $.ajax({
        type: 'POST',
        url: baseUrl+"account/Request/Editmsg/"+editreq_id,
        data: {editid: editid,msg:msg},
        success: function (data) {
            if(data){
                //console.log('.msg_desc_' + editid + ' .msgk-umsgbox .edit_icon');
                $('.edit_icon_'+editid).html('<i class="fas fa-pen"></i>');
                $('.took_'+editid).html('<div class="edited_msg">'+msg+'</div>');
                $('.msg_desc_'+editid).css('display','block');
                $('.edit_main_'+editid).css('display','none');
            }
        }
    });
});

function message(obj) {
    var request_id = obj.getAttribute('data-requestid');
    var sender_type = obj.getAttribute('data-senderrole');
    var sender_id = obj.getAttribute('data-senderid');
    var reciever_id = obj.getAttribute('data-receiverid');
    var reciever_type = obj.getAttribute('data-receiverrole');
    var text_id = 'text_' + request_id;
    var message = $('.' + text_id).val();
    if (message.indexOf("http://") >= 0){
        var link_http_string = (message.substr(message.indexOf("http://") + 0));
        var link = link_http_string.split(' ')[0];
    }if(message.indexOf("https://") >= 0){
        var link_https_string = (message.substr(message.indexOf("https://") + 0));
        var link = link_https_string.split(' ')[0];
    }
    if(message.includes(link)){
        message = message.replace(link, '<a target="_blank" style="color:#fff" href="'+link+'">'+link+'</a>');
    }else{
        message = message;
    }
    var verified_by_admin = "1";
    var profile_image = obj.getAttribute('data-profile_pic');
    var customer_name = obj.getAttribute('data-sendername');
    if (message != "") {
        $.ajax({
            type: "POST",
            url: baseUrl+"account/Request/send_message_request",
            data: {"request_id": request_id,
            "sender_type": sender_type,
            "sender_id": sender_id,
            "reciever_id": reciever_id,
            "reciever_type": reciever_type,
            "message": message},
            success: function (data) {
//                console.log("data",data);
                var res = data.split("_");
                var update = $.trim(res[0]);
                data = $.trim(res[1]);
                $('.text_' + request_id).val("");
                if ($('#message_container > div.msgk-chatrow:last-child > div').hasClass("msgk-right") === true) {
                    $( "#message_container > div.msgk-chatrow:nth-last-child(1)" ).find('.just_now').css('display','none');
                    $('#message_container').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + 'del_'+data+'"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_'+data+'"><i class="fas fa-ellipsis-v openchange" data-chatid="'+data+'"></i><div class="open-edit open-edit_'+data+'" style="display: none;"><a href="javascript:void(0)" class="editmsg" data-editid="'+data+'">edit</a><a href="javascript:void(0)" class="deletemsg" data-delid="'+data+'">delete</a></div></div></div><div class="msgk-mn-edit edit_main_'+data+'" style="display:none"><form method="post" id="editMainmsg"><textarea class="pstcmm sendtext" id="edit_main_msg_'+data+'">'+message+'</textarea><a href="javascript:void(0)" class="edit_save"  data-id="'+data+'">Save</a><a href="javascript:void(0)" class="cancel_main" data-msgid="'+data+'">Cancel</a></form></div><div class="msgk-mn msg_desc_'+data+'"><div class="msgk-umsgbox"><pre><span class="edit_icon_'+data+' edit_icon_msg"></span><span class="msgk-umsxxt took_'+data+'>' + message + '</span></pre></div></div></div></div>');
                } else {
                    $('#message_container').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + ' del_'+data+'"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_'+data+'"><i class="fas fa-ellipsis-v openchange" data-chatid="'+data+'"></i><div class="open-edit open-edit_'+data+'" style="display: none;"><a href="javascript:void(0)" class="editmsg" data-editid="'+data+'">edit</a><a href="javascript:void(0)" class="deletemsg" data-delid="'+data+'">delete</a></div></div></div><div class="msgk-mn-edit edit_main_'+data+'" style="display:none"><form method="post" id="editMainmsg"><textarea class="pstcmm sendtext" id="edit_main_msg_'+data+'">'+message+'</textarea><a href="javascript:void(0)" class="edit_save"  data-id="'+data+'">Save</a><a href="javascript:void(0)" class="cancel_main" data-msgid="'+data+'">Cancel</a></form></div><div class="msgk-mn msg_desc_'+data+'"><div class="msgk-umsgbox"><pre><span class="edit_icon_'+data+' edit_icon_msg"></span><span class="msgk-umsxxt took_'+data+'">' + message + '</span></pre></div></div></div></div>');
                }
                if (update == 'yes') {
//                    $.noConflict();
                    $(".curnt_req_status").addClass("agency_revision");
                    $(".curnt_req_status a").html("Revision");
                   // $("#review_reqid").val(request_id);
                   // $("#f_reviewpopup").modal("show");
                }
            }
        });
    }
//    $('#message_container').stop().animate({
//        scrollTop: $('#message_container')[0].scrollHeight
//    },0);
}

$(document).on('click', '.delete_file_req', function () {
    var r = confirm('Are you sure you want to delete this file?');
    if(r == true){
        var reqid = $(this).attr('data-id');
        var filename = $(this).attr('data-name');
        $.ajax({
            type: "POST",
            url: baseUrl+"account/Request/delete_file_req",
            data: {"request_id": reqid, "filename": filename},
            success: function (data) {
                window.location.reload();
            }
        });
    }else{}
});

/********disable public link from project-info people tab******/
$(document).on('change','#disabeled_link',function(){
    var is_disabled = $(this).is(':checked');
    var reqid = $(this).data('reqid');
   // console.log('is_disabled',is_disabled);
    if(is_disabled == true){
        is_disabled = 0;
    }else{
        is_disabled = 1;
    }
    $("#mainpubliclinkshr").slideToggle();
    $.ajax({
        method: 'POST',
        url:baseUrl+"account/request/isDisabledlink",
        data: {'reqid':reqid,'is_disabled':is_disabled},
        success: function(response){}
    });
});

$(document).on('click','.sharebyemail_link',function(){
    var id = $(this).attr('data-id');
     $(".share_emailby_link_frm_"+id).show();
});
$(document).on('click','.cancelshareitbymail',function(){
    var id = $(this).attr('data-id');
     $(".share_emailby_link_frm_"+id).hide();
});
/********change public permisssion from project-info people tab******/
$(document).on('change', '.people_public_permsn', function () {
    var publink = $('.show_publink').val();
    var reqID = $('.change_pubpermision').attr('data-req_id');
    console.log("reqID",reqID);
  //  console.log(publink);
    var value = [];
    $.each($("input[name='peoplepubliclink_permissions[]']:checked"), function(){            
        value.push($(this).val());
    });
    changePublicpermissions(publink,value,reqID);
});
$(document).on('change', '.chang_prmsn_publink', function () {
    var share_id = $(this).attr('data-id');
    var req_id = $(this).attr('data-reqid');
    var share_value = $(this).val();
    var share_checked = ($(this).prop("checked"))?1:0;
    $.ajax({
        method: 'POST',
        url: baseUrl+"account/request/changepublicpermissionsforpeople",
        data: {share_id: share_id,share_value: share_value,share_checked:share_checked,req_id:req_id},
        success: function (response) {
        }
    });
});
$(document).on('click', '.openchange', function (e) {
  var chatid = $(this).data('chatid');
  $(".open-edit_" + chatid).slideToggle()
});

    
/************dot comment js************/
$(document).on('click','.disabled_draft_pub',function(){
    var reqid = $(this).data('reqid');
    var draftid = '';
    if($(this).data('draftid') != 0){
        draftid = $(this).data('draftid');
    }else{
       draftid = 0; 
    }
    var is_disabled = $(this).is(':checked');
    if(is_disabled == true){
        is_disabled = 0;
    }else{
        is_disabled = 1;
    }
    $("#mainpubliclinkshr_"+draftid).slideToggle();
    $.ajax({
        method: 'POST',
        url:baseUrl+"account/request/isDisabledlink",
        data: {'reqid':reqid,'draftid':draftid,'is_disabled':is_disabled},
        success: function(response){}
    });
});


/************disable link from link share popup js************/
$(document).on('click','#disabeled_link_pop',function(){
    var reqid = $(this).data('reqid');
    var draftid = '';
    if($(this).data('draftid') != 0){
        draftid = $(this).data('draftid');
    }else{
       draftid = 0; 
    }
    var is_disabled = $(this).is(':checked');
    if(is_disabled == true){
        is_disabled = 0;
        $('.overhelm').removeClass('disbledtoggl');
         $('.label_case').text('Enabled');
    }else{
        is_disabled = 1;
         $('.overhelm').addClass('disbledtoggl');
         $('.label_case').text('Disabled');
    }
    $.ajax({
        method: 'POST',
        url:baseUrl+"account/request/isDisabledlink",
        data: {'reqid':reqid,'draftid':draftid,'is_disabled':is_disabled},
        success: function(response){}
    });
});    
//$('#request_submit').on('click',function(){
//    var subcategory_class = $("input[name='subcategory_id']:checked").val();
//    var title = $('.title').val();
//    var dimension = $('.dimension').val();
//    var comment = $('#comment').val();
//        if (subcategory_class == '' || typeof subcategory_class == 'undefined') {
//            $('.catagory .error_msg').html("<span class='validation'>Please choose atleast one subcategory</span>");
//            return false;
//        } else if (title == '' || typeof title == 'undefined') {
//            $('.title_class .error_msg').html("<span class='validation'>Please enter title</span>");
//            $('.catagory .error_msg').html("");
//            return false;
//        }else if(dimension == '' || typeof dimension == 'undefined'){
//             $('.dim_class .error_msg').html("<span class='validation'>Please enter dimension</span>");
//             $('.catagory .error_msg').html("");
//             $('.title_class .error_msg').html("");
//             return false;
//        }else if(comment == '' || typeof comment == 'undefined'){
//            $('.com_class .error_msg').html("<span class='validation'>Please enter description</span>");
//            $('.catagory .error_msg').html("");
//            $('.title_class .error_msg').html("");
//            $('.dim_class .error_msg').html("");
//            return false;
//        }else{
//            return true;
//        }
//});

var optionDiv = ''
$(document).on('click', '.see_subuser_req', function () {
    optionDiv = $(this).attr('data-subID');
    $(".client_projects_counting_" + optionDiv).slideToggle();
});
$(document).mouseup(function (e) {
    var container = $('.client_projects_counting_' + optionDiv);
    var classnm = $('.see_subuser_req');
    if (!classnm.is(e.target) && classnm.has(e.target).length === 0) {
        container.hide();
    }
});

/****sitetour update value of is_logged_in on done click*******/
//$(document).on('click','.introjs-donebutton',function(){
//   //console.log("on click event"); 
//    $.ajax({
//        type: 'POST',
//        url: baseUrl+'account/request/closeSitetour',
//        data: {'is_logged_in': 1},
//        dataType: "json",
//        success: function (data) {}
//    });
//});

$(document).on('click','.livechatpopup',function(){
   $('#livechatpopup').modal('show'); 
});



$(document).on('click','#people_shares',function(){
    $this =  $(this); 
    var issaved = $this.data("issaved");
    var reqid = $(this).data('proid');
    if(issaved !="1"){
        $.ajax({
            type: 'POST',
            url: baseUrl+'account/request/publiclinksave',
            data: {'proID': reqid,'draftid':0},
            dataType: "json",
            success: function (data) {
                 console.log(data);
               //toastr.success('Public link has been successfully saved.', 'Success', {timeOut: 4000})
       //        console.log(data.issaved); 
                if(data.issaved == "1"){
                    $this.attr("data-issaved",data.issaved);
                }else{
                   $this.attr("data-issaved","0"); 
                }
                $('.show_publink').val(data.publiclinkexists);
                if(data.is_disabled == 1){
                    $('#mainpubliclinkshr').css('display','none');
                } 
            }
        });
    }else{
        //toastr.warning('Public Link already Saved', 'Warning', {timeOut: 5000})
       
    } 

     
});


function sharing_permision_ajaxCall(pro_id,draftid){
    $("#loader_peoples_tab").show();
            $.ajax({
                    method: 'POST',
                    url: baseUrl+"account/request/sharing_permision_ajaxCall",
                    data: {request_id: pro_id,draftid: draftid},
                    success: function (res) {
                        $("#loader_peoples_tab").hide();
                       $(".ShareLinkHtml").html(res);
                    }
            }); 
        }
        
$(document).on('click','.domain_inst',function(){
    var iframe = $("#myiFrame");
    var url = iframe.data("src");
    var frame = '<iframe src="https://docs.google.com/viewer?url='+url+'&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
    $.fancybox.open(frame); 	
});

/**************samples & questions*************/
$(document).on('change','.subcategory_class',function(){
    var subcat = $("input[name='subcategory_id']:checked").val();
    var cat = $('select#cat_page option:selected').val();
    getCatQuest(cat,subcat);
    getSamples(cat,subcat);
});

function getCatQuest(catID,subcat){
    var req,valreq;
    $('.for_loading_while').css('display','none');
    $('.imgloader_questt').css("display","block");
    $('.categoryqust').html('');
    var searchParams = new URLSearchParams(window.location.search);
    var edit_id = searchParams.get('reqid');
    $.ajax({
        method: 'POST',
        url: baseUrl+"account/Request/getCategoryquestions",
        data: {'catID':catID,'subcat':subcat,'edit_id':edit_id},
        dataType: "json",
        success: function (response) {
            $('.for_loading_while').css("display","block");
            $('.imgloader_questt').css("display","none");
            $('.frstappnd .categoryqust').html('');
            $('.secondappnd .categoryqust').html('');
            if(response.status == 'success'){
               var brkgpnt;
               var datalen = ((response.data).length);
               var halfcnt = parseInt((datalen+2)/ 2);
               var dynamicquestionscount = parseInt(datalen+2);
//               console.log('dynamicquestionscount',dynamicquestionscount);
//                console.log('datalen',datalen);
               var questionssection = (datalen >= 4) ? 'twosection' : 'onesection';
                if(questionssection == 'twosection'){
                    brkgpnt = (parseInt)(halfcnt-2);
//                    brkgpnt = (parseInt)(halfcnt);
                }else{
                     brkgpnt = datalen;
                }
                if(dynamicquestionscount/2 != 0){
                    brkgpnt = brkgpnt+1; 
                }
                for (i = 0; i < brkgpnt; i++) {
                    var htmlData = questionData(response.data[i]);
                    $('.frstappnd .categoryqust').append(htmlData);
                    $('.delvrb').addClass('rytbtm');
                    $('.secondappnd').css('display','none');
                    $('.colrpref').addClass('ryttop');
                    $('.colors-row').addClass('rightcolclass');
                }
                for (j = brkgpnt; j < datalen; j++) {
                    var htmldata = questionData(response.data[j]);
                    $('.secondappnd').css('display','block');
                    $('.secondappnd .categoryqust').append(htmldata);
                    $('.delvrb').removeClass('rytbtm');
                    $('.colrpref').removeClass('ryttop');
                    $('.colors-row').removeClass('rightcolclass');
                }
        }else{
            $('#categoryqustns').css('display','none');
        }
    }
    });
}


function getSamples(catID,subcat){
   var req,valreq,samplesfetch;
    $('.categoryqust').html('');    
    $('#samples_categories').html('');
    var searchParams = new URLSearchParams(window.location.search);
    var edit_id = searchParams.get('reqid');
    $.ajax({
        method: 'POST',
        url: baseUrl+"account/Request/getSamples",
        data: {'catID':catID,'subcat':subcat,'edit_id':edit_id},
        dataType: "json",
        success: function (response) {
              if(response.status == 'success'){
                $.each(response.data, function (nameArr_k, nameArr_v){
                var maxval =  nameArr_v.maximum_sample;
                var minval =  nameArr_v.minimum_sample;
                //console.log("minval",minval);
                $('.maximumselection').attr('data-maxsel',maxval);
                sendMax(maxval);
                samplesfetch +=  '<div class="sample_div"><label class="radio-box-xx2"><input name="sample_subcat[]" class="sample_subcat" data-minchk='+minval+' data-maxchk='+maxval+' value="'+nameArr_v.material_id+'" type="checkbox"><span class="checkmark"></span><div class="check-main-xx3"><figure class="chkimg"><img src="'+$uploadPath+'samples/'+nameArr_v.sample_id+'/'+nameArr_v.sample_material+'" class="img-responsive"></figure></div></label></div>';
                $('.sampleexist').css('display','block');
                $('.sampleexist').attr('data-isset','1');
                $('.min_val').text(nameArr_v.minimum_sample);
                $('.max_val').text(nameArr_v.maximum_sample);
                });
                var samplesfetch = samplesfetch.replace("undefined", '');
                $('#samples_categories').html(samplesfetch);
                $('.sample_label').css('display','flex');
                $('.step_qus').text('Step 3.');
                $('.step_upl').text('Step 4.');
                $("#samples_categories").owlCarousel('destroy'); 
                samplecarousel();
            }else{
                $('.sampleexist').attr('data-isset','0');
               $('.sampleexist').css('display','none');
               $('.sample_label').css('display','none');
               $('.step_qus').text('Step 2.');
               $('.step_upl').text('Step 3.');
            }
        }
    });
}

$(document).on('change','.sample_subcat',function(evt){
     var limitval = (parseInt($('#samples_categories').attr('data-maxchk')) + 1);
    if ($('input[name="sample_subcat[]"]:checked').length  >= limitval) {
     this.checked = false;
    } 
});

 function sendMax(limit){
     $('#samples_categories').attr('data-maxchk',limit);
 }

function check_activeClass(){
 var id = $(".list-header-blog").attr("id");
    if($('#'+id+' li:first-child').hasClass("active")==false){ 
      $('#'+id+' li:first-child a').trigger("click"); 
       setTimeout(function() { 
         introJs().start();
      }, 400);
    }else{
        introJs().start();
    }
}

$(document).ready(function(){
  samplecarousel();  
});
 $countSamples ='';
 
function samplecarousel(){
  var totalItems = $countSamples.length;
  if(totalItems < 4){
      var isLooped = false;
  }
$("#samples_categories").owlCarousel({
    
    loop: isLooped,
    mouseDrag:false,
    nav:true,
    margin:10,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
    });
}

/*********question append**********/
function questionData(value){
   var answer = value.answer;
    var html_data = val_ans = label_class = '';
    if(answer === 'null' || typeof answer === 'undefined' || answer === null){
         val_ans = '';
         label_class = '';
    }else{
       val_ans =  value.answer;
       label_class = 'label-active';
    }
    if(value.is_required == 1){
         req = '*';
         valreq = 'required';
    }else{
        req = '';
        valreq = '';
    }
    if(value.question_type == 'text'){
        html_data += '<div class="title_class_1"><label class="form-group"><p class="label-txt '+label_class+'">'+value.question_label+''+req+'</p><input type="text" class="input title" name="quest['+value.id+']"  value="'+val_ans+'" '+valreq+'/><div class="line-box"><div class="line"></div></div><p class="discrption">'+value.question_placeholder+'</p></label></div>';
    }else if(value.question_type == 'textarea'){
        html_data += '<div class="com_class"><label class="form-group"><p class="label-txt '+label_class+'">'+value.question_label+''+req+'</p><textarea class="input" rows="5" name="quest['+value.id+']" id="comment" placeholder="" '+valreq+'>'+val_ans+'</textarea><div class="line-box"><div class="line"></div></div><p class="discrption">'+value.question_placeholder+'</p><div class="error_msg"></div></label></div>';
    }
    else if(value.question_type == 'radio'){
        var res = value.question_options.split(",");
        var slrad = value.answer;
        html_data += '<label class="form-group"><div class="color-inside catagry-heading"><h3>'+value.question_label+''+req+'</h3><div class="sound-signal">'
        $.each(res, function (kk, val) {
            if(val == slrad || (kk == 0 && value.is_required == 1)){
                var resrad = "checked";
            }else{
                var resrad = ""; 
            }
            html_data += '<div class="form-radion radio_qust"><input type="radio" name="quest['+value.id+']" class="preference" id="soundsignalredio_'+val+'" value="'+val+'" '+resrad+'><label for="soundsignalredio_'+val+'">'+val+'</label></div>';
        });
        html_data += '</div></label>';
    }
    else if(value.question_type == 'checkbox'){
        var res_chck = value.question_options.split(",");
        html_data += '<label class="form-group"><div class="color-inside catagry-heading"><h3>'+value.question_label+''+req+'</h3>'
        $.each(res_chck, function (res_chck_k, res_chck_val) {
            if(res_chck_k == 0 && value.is_required == 1){
                var chckrad = "checked";
            }else{
                 var chckrad = "";
            }
            html_data += '<div class="form-radion2"><label class="containerr" for="soundsignal_'+res_chck_val+'">'+res_chck_val+'<input type="checkbox" name="quest['+value.id+'][]" class="preference" id="soundsignal_'+res_chck_val+'" value="'+res_chck_val+'" '+chckrad+'><span class="checkmark"></span></label></div>';
        });
        html_data += '</div></label>';
    }else{
        var res_sel = value.question_options.split(",");
        var ans_sel = answer;
        html_data += '<label class="form-group"><p class="label-txt">'+value.question_label+' '+req+'</p><select name="quest['+value.id+']" class="input select-a" '+valreq+'><option value=""></option>'
        $.each(res_sel, function (res_sel_k, res_sel_val) {
            if(res_sel_val == ans_sel) {
                var ressel = "selected";
            }else{
                var ressel = ""; 
            }
            html_data += '<option value="'+res_sel_val+'" '+ressel+'>'+res_sel_val+'</option>';
        });
        html_data += '</select><div class="line-box"><div class="line"></div></div><p class="discrption">'+value.question_placeholder+'</p></label>';
    }
    return html_data;
}



//  pixabay and unsplash code start from here 

$(document).ready(function() {
$(".serach_btn").click(function(e) {
//            loader_show();
            $(".ajax_searchload").show();
            $(".serach_btn").removeClass("active");
            $(this).addClass("active");
            var search_keyword = $("#search").val();
            var filter = $("#image_filter").val();
            var filter_unsplash;
            filter_unsplash = orientation_filter(filter,"");
            var default_text = $("#image_filter").val();
            default_text = orientation_filter("", default_text);
            search_images(search_keyword, load_more = "1", type = "sr", filter,filter_unsplash,default_text);
    });
});

function search_images(search_keyword, load_more, type, filter,filter_unsplash,default_text) {
    if ($.trim(search_keyword)!= "" && $.trim(search_keyword) != null) {
       $.ajax({
            type: "POST",
            url:  baseUrl+"account/request/get_all_imagesPost",
            data: {"keyword":search_keyword,"filter":filter,"load_more":load_more,"filter_un":filter_unsplash,"filter":filter},
            dataType:'json',
            success: function (res) {
                 
            if (parseInt(res[0].totalHits) > 0) {
                $("#image_gallery").removeClass("custom-error");
                
                $(".search_head").show();
                $("#search").css("border-color", "#dddddd");
                if (parseInt(res[0].total) > 20 && parseInt(res[0].totalHits) <= parseInt(res[0].total)) {
                    $("#loadMore").show();
                    $("#loadMore").attr("data-count", load_more);
                } else if (parseInt(res[1].total) > 20) {
                    $("#loadMore").show();
                    $("#loadMore").attr("data-count", load_more);
                }else{
                    $("#loadMore").hide();
                }
                var html = '';
                var html2 = '';
                var error = '';
             //   var loaderHtml  = '<div class="loader_button"><div class="ajax_se_loaderbtnn" style="display:none;"> <img src="<?php echo base_url(); ?>public/assets/img/ajax-loader.gif"></div> <a href="#" id="loadMore" style="display:none;" onclick="load_more_images($(this))">Load More </a></div>'; 
                $("#search").css("border-color", "#dddddd");
                $(".errorSpan").hide();
              //  result from pixabay                
                $.each(res[0].hits, function(i, original_data) { 
                     var count = i + 1;
                     html += '<li class="item thumb"><div class="content box-overflow"> <a data-href="'+original_data.largeImageURL+'" data-from="pixabay" data-author="'+original_data.user+'" data-likes="'+original_data.likes+'" data-id="'+original_data.id+'" data-pageUrl ="'+original_data.pageURL+'"></a><img id="img_gallary_px' + load_more + count + '" src=' + original_data.webformatURL + ' alt=' + search_keyword + ' style="width:100%;opacity:0; "class="gallary_thumb"><i class="checkmark_pop" aria-hidden="true"></i></div><div class="credit-area" style="opacity:0;" id="img_gallary_px' + load_more + count + 'cr"><a href="'+original_data.pageURL+'" target="_blank"><img src="https://pixabay.com/favicon-32x32.png"></a></div></li>'; 
                });
                //  result from unsplash
                $.each(res[1].results, function(i, original_data) { 
                 var count = i + 1;
                 html2 += '<li class="item thumb"><div class="content box-overflow"> <a data-href="'+original_data.urls.regular+'" data-from="unsplash"  data-authorlink = "'+original_data.user.links.html+'" data-author="'+original_data.user.name+'" data-likes="'+original_data.likes+'" data-id="'+original_data.id+'" data-pageUrl ="'+original_data.links.html+'"></a><img id="img_gallary_un' + load_more + count + '" src=' + original_data.urls.regular + ' alt=' + original_data.alt_description + ' style="width:100%; opacity:0;" class="gallary_thumb"><i class="checkmark_pop" aria-hidden="true"></i></div><div class="credit-area" style="opacity:0;" id="img_gallary_un' + load_more + count + 'cr"><a href="'+original_data.links.html+'" target="_blank"><img src="https://unsplash.com/favicon-32x32.png"></a></div></li>';
                });
            } else {
                $(".search_head").hide();
                $(".errorSpan").hide();
                html = '';
                $("#image_gallery").addClass("custom-error");
                html += '<h4 class="no_data"> No Data Found for Search <b>"' + search_keyword + '"</b> </h4>';
                loader_hide();
                $("#loadMore").hide();
            }
            if (type != "lm") {
             // loader_show();
             
                $(".search_head").html("<h2> Result For your search keyword <b>"+search_keyword+"</b> & search type <b>"+default_text+"</b></h2>");
                $("#image_gallery").html(html);
                $("#image_gallery").append(html2);
               CheckImageLoad('gallary_thumb'); 
               
                 
                // make_gridForImages(); 
            } else {
               
              //loader_show();
               $(".search_head").html("<h2> Result For your search keyword <b>"+search_keyword+"</b> & search type <b>"+default_text+"</b></h2>");
               $("#image_gallery").append(html);
               $("#image_gallery").append(html2);
                 CheckImageLoad('gallary_thumb'); 
                 
                 
 }
             $(".ajax_searchload").hide();
             $(".ajax_se_loaderbtnn").hide();
//            loader_hide();
        }
    });
    } else {
         $(".ajax_searchload").hide();
//        loader_hide();
        $(".search_head").hide();
        $("#search").css("border-color", "red");
        $(".errorSpan").show();
    }
}


function load_more_images(event) {
    $(".ajax_se_loaderbtnn").show();
//    $(".ajax_loader").show();
    var search_keyword = $("#search").val();
    var filter = $("#image_filter").val();
    var filter_unsplash;
    filter_unsplash = orientation_filter(filter,"");
    var default_text = $("#image_filter").val();
    default_text = orientation_filter("", default_text);
    var current_count = event.attr("data-count");
    var nextLoad = 1;
    var load_more = parseInt(current_count) + parseInt(nextLoad);
    search_images(search_keyword, load_more, type = "lm", filter,filter_unsplash,default_text);
}

$(document).on("click","#image_gallery li",function(){
       var downloaded_url,targetid,get_method,currnt_state,pageUrl,markpoint;
       var status_str;
       var search_keyword = $("#search").val();
       $("#search_keyword_hid").val(search_keyword);
        
       $this = $(this);
       $checkmark = $(".checkmark_pop");
       $this.toggleClass("selected-li");
       $this.find("i").toggleClass("selected");
       $this.find("i").toggleClass("fa fa-check");
//       $this.parents("li").find(".checkmark_pop").toggleClass("selected");
//       $this.parents("li").find(".checkmark_pop").toggleClass("fa fa-check");
//       $this.parents("li").find("a").toggleClass("selected_img");
         
//      $this.find(".checkmark_pop").toggleClass("selected");
//      $this.find(".checkmark_pop").toggleClass("fa fa-check");
      $this.find("a").toggleClass("selected_img");
       downloaded_url = $this.find("a").attr("data-href");
       targetid = $this.find("a").attr("data-id");
       pageUrl = $this.find("a").attr("data-pageurl");
       if($this.find("i").hasClass("selected")){
        var text = downloaded_url;
        var text2 = pageUrl;
        downloaded_url = text.replace("https://", "");
        pageUrl = text2.replace("https://", "");
        $(".selected_images").append('<input type="hidden" id="'+targetid+'pgurl" name="pageurl[]" value="'+pageUrl+'">');
        $(".selected_images").append('<input type="hidden" multiple="" name="file-upload[]" class="multiImage" id="'+targetid+'" value="'+downloaded_url+'">');
        $(".selected_images").append('<input type="hidden" id="'+targetid+'ids" name="ids[]" value="'+targetid+'">');
        status_str = $(".selected_images").html();
       }else{
          $("#"+targetid).remove(); 
          $("#"+targetid+"ids").remove(); 
          $("#"+targetid+"pgurl").remove(); 
           status_str =  $(".selected_images").html();
       }
       currnt_state= status_str.replace(/\s/g, '');
      if(currnt_state!=""){   
          $(".DownloadBtn").fadeIn();
          $(".upload_btn").fadeIn();
         }else{
          $(".DownloadBtn").fadeOut();
          $(".upload_btn").fadeOut();
         } 
      });

    $(document).on("click",".image_open",function(){
        $this = $(this); 
        $(".fancybox-button--download").attr("href","")
        var url = $this.parents("li").find("a").attr("data-href");
        var author = $this.parents("li").find("a").attr("data-author");
        var likes = $this.parents("li").find("a").attr("data-likes");
        
          $.fancybox.open([
            {
              src  : url,
              opts : {
                caption : 'Likes <span class="fa fa-thumbs-up"></span> '+likes+' <br> Photo By <b> '+author+' </b>',
              }
            } 
                ], {
            loop : true,
            thumbs : {
              autoStart : true
            },buttons: [
              "zoom",
              "share",
              "slideShow",
              "fullScreen",
              //"download",
              //"thumbs",
              "close"
            ], });
      });
   $(".upload_btn").click(function(){
      $this = $(this);
      storedFiles['file_name']= [];
      $this.button('loading');
      $.ajax({
          type:"POST",
          url:  baseUrl+"account/request/download_remote_server",
          data:$('#uploadForm').serialize(),
          dataType: 'json',
          success: function (response) {
           // $this.button('reset');
            var count = ''; 
            if (response.status == '1') {
                if (response.files.length > 0) {

                    //console.log("123231",response.files.length);
                    var files ='';
                    count =  response.img_link.length; 
                    $.each(response.files,function (k, v) {  
                         storedFiles.push({
                          "file_name":v,
                          "file_size":response.file_size[k],
                          "error":false,
                          "image_link":response.img_link[k],
                         });
                        $(".uploadFileListContain").html(createStoredFilesHtml());
                        
//                        $(".content_Container").html(createStoredFilesHtml());
                        $(".content_Container").html(" ");
                            if(count == 1){
                                 files = "file";
                            }else{
                                  files = "files";
                            }
                             
//                        $(".content_Container").html("<span>" +count+" "+files+"  uploaded</span>");
                        
                        $(".ad_new_reqz").show();
                        $('#request_submit').prop('disabled', false);
                    });
                    //toastr.success("<span>" +count+" "+files+"  uploaded</span>");
                    toastr.success("<span>" +count+" "+files+"  uploaded</span>", 'Uploaded Items',{ 
                        showDuration: 5000,
                        positionClass: 'toast-top-right',
                        progressBar: true
                        });

                     attach_ment(); 
                }
            }else{
                 var html = ''; 
                 var msg = ''; 
                 var length  = response.filename.length; 
                 if(length == 1){
                            var Efiles = "File";
                        }else{
                            Efiles  = "Files";
                        }
                $.each(response.filename,function (k, value) {
                    
                   html += "<a href='"+response.path[k]+value+"' target='_blank'>"+value+"</a> &nbsp; ";
                 });
                 $(".uploaded-items").addClass("custom_outer");
                  $(".ad_new_reqz").hide();
                   toastr.error("<span class='custom_error'>"+Efiles+" already exist on server " + html+"</span>");    
             //$(".content_Container").html("<span class='custom_error'>"+Efiles+" already exist on server " + html+"</span>");
            }
        }
      });


    }); 

function loader_hide() {
    $(".ajax_loader").hide();
}

function loader_show() {
    $(".ajax_loader").show();
}

function orientation_filter(filter = "", default_text = "") {
    var return_type;
    if (filter != "") {
        if (filter == "horizontal") {
            return_type = "landscape";
        } else if (filter == "vertical") {
            return_type = "portrait";
        } else {
            return_type = "portrait";
        }

    } else if (default_text != "") {
        if (default_text == "horizontal") {
            return_type = "landscape";
        } else if (default_text == "vertical") {
            return_type = "portrait";
        } else {
            return_type = "all";
        }
    }
    return return_type;
}

function attach_ment(){
    $(".upload_btn").button('reset');
    setTimeout(function(){  

    if(get_method == "add_new_request"){
         $("#uploadModel").modal("hide");
         //$("#request_submit").trigger("click"); 
        }else{
          $("#"+get_method+"_btnid").trigger("click");    
        }
 }, 1000);
}

/**-- ends here  **/
//  pixabay and unsplash code Ends here  

/* client management page js */

/**editor setting**/
var settings1 = {
    'indent': false,
    'outdent': false,
    'strikeout': false,
    'block_quote': false,
    'hr_line': false,
    'splchars': false,
    'ol': false,
    'redo': false,
    'undo': false,
    'insert_link': false,
    'unlink': false,
    'insert_img': false,
    'print': false,
    'insert_table': false,
    'rm_format': false,
    'select_all': false,
    'togglescreen': false,
    'ul': true
};
/**editor setting**/

$(document).on('click', '.backtoclntlistng', function () {
    $("#sub-userlist").css("display", "block");
    $("#new-subuser").css("display", "none");
});
$(document).on('click', '.livechatpopup', function () {
    $('#livechatpopup').modal('show');
});

$(document).on('click', '.f_add_subscription', function () {
    labelactive();
    $(".clint_subs").css("display", "none");
    $(".subs_hdr").css("display", "none");
    $(".add_edt_clntsubs").css("display", "block");
    $(".fadd_subs").css("display", "block");
    $(".fedit_subs").css("display", "none");
    $("#f_subs_plan_features").Editor(settings1);
});
$(document).on('click', '.f_bk_subs', function () {
    $("#subscription_management").load(" #subscription_management > *");
});

//$(document).on("click","#save_clntsubs",function(e){
//    var features = $('#f_subs_plan_features').Editor("getText");
//    $("#f_subs_plan_features").val(features);
//});
$(document).on("click", ".delete_clients", function (e) {
    var id = $(this).data("id");
     var idntfy = $(".TeamManagement").find("li.active").find("a").attr("id")
     $("#deletesubuser a.btn-ydelete").attr("data-id",id); 
    //$("#deletesubuser a.btn-ydelete").attr("href", baseUrl + "account/MultipleUser/delete_sub_user_designer/"+idntfy+"/" + id);
});

$(document).on('click','.edit_clntsubs',function(){
    var id = $(this).data("id");
    $(".clint_subs").css("display","none");
    $(".subs_hdr").css("display","none");
    $(".fadd_subs").css("display","none");
    $(".fedit_subs").css("display","block");
    
    $(".add_edt_clntsubs").css("display","block");
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: baseUrl+'account/AgencyClients/getclientsubscription',
        data: {'id': id},
        success: function (data) {
            var plantype = data.plan_type_name;
            var features = data.features;
            $("#f_subs_id").val(data.id);
            $("#f_subs_plan_id").val(data.plan_id);
            $("#f_subs_plan_name").val(data.plan_name);
            $("#f_subs_plan_price").val(data.plan_price);
            $("#f_subs_plan_type").val(data.plan_type);
            $("#f_subs_plan_type_name").val(plantype);
            if(features != ""){
                features_html = "";
                var strArray = features.split("_");
                $.each(strArray, function (k, val) {
                    if (val != "") {
                        features_html += '<input type="text" name="subs_plan_features[]" class="input subs_plan_features f_cls_nm" value="' + val + '"/><div class="line-box"><div class="line"></div></div>';
                    }
                });
                var f_html = $('.f_subs_plan_features_blok');
                f_html.html(features_html);
            }
            if(data.payment_mode == 1){
                $('#f_subs_payment_mode').prop('checked', true);
                $('.payment_mod_txt').html("Online Payment");
            }else{
                $('.payment_mod_txt').html("Offline Payment");
            }
            if(data.apply_coupon == 1){
                $('#f_apply_coupon').prop('checked', true);
            }
            if(data.file_management == 1){
                $('#f_file_management').prop('checked', true);
            }
            if(data.file_sharing == 1){
                $('#f_file_sharing').prop('checked', true);
            }
            if(data.is_active == 1){
                $('#f_subs_plan_active').prop('checked', true);
            }
            if(plantype == "one_time"){
                $(".onetimepln").css("display","block");
                $(".subscriptionbased").css("display","none");
                $("#f_subs_numof_req").val(data.global_inprogress_request);
                $('#f_subs_numof_req').prop('required', true);
                $('#f_subs_active_req').prop('required', false);
                $("#f_subs_plan_id").prop('required', false);
                $(".f_amount_cycle").css("display","none");
                $(".f_pln_user_type").css("display","none");
                $(".stripe_is_sec").css("display","none");
                $("#subscrptn_frm").addClass("request_mode");
                $("#subscrptn_frm").removeClass("subs_mode");
                
            }else{
                $("#f_subs_user_type").val(data.shared_user_type);
                $(".stripe_is_sec").css("display","block");
                $(".onetimepln").css("display","none");
                $(".subscriptionbased").css("display","block");
                $('#f_subs_numof_req').prop('required', false);
                $('#f_subs_active_req').prop('required', true);
                $("#f_subs_plan_id").prop('required', true);
                $(".f_amount_cycle").css("display","block");
                $(".f_pln_user_type").css("display","block");
                $('#f_subs_plan_id').parent().children('p').addClass("label-active");
                $("#subscrptn_frm").addClass("subs_mode");
                $("#subscrptn_frm").removeClass("request_mode");
               
            }
            
            $('#f_subs_plan_name').parent().children('.label-txt').addClass("label-active");
            $('#f_subs_plan_price').parent().children('p').addClass("label-active");
            $('#f_subs_active_req').parent().children('p').addClass("label-active");
            $('#f_subs_turn_around_days').parent().children('p').addClass("label-active");
            $('#f_subs_numof_req').parent().children('.label-txt').addClass("label-active");
        }
    });
});

$(document).on('change', '#f_subs_plan_type_name', function () {
    var plantype = $(this).val();
    var paymode = ($("#f_subs_payment_mode").is(':checked')) ? 1 : 0;
    //console.log("paymode",paymode);
    if(plantype === "one_time"){
        $(".onetimepln").css("display","block");
        $(".stripe_is_sec").css("display","none");
        $("#f_subs_plan_id").prop('required', false);
        $("#f_subs_planid").prop('required', false);
        $('#f_subs_numof_req').prop('required', true);
        $(".f_amount_cycle").css("display","none");
        $(".f_pln_user_type").css("display","none");
        $("#subscrptn_frm").addClass("request_mode");
        $("#subscrptn_frm").removeClass("subs_mode");
    }else{
        $(".onetimepln").css("display","none");
        $(".stripe_is_sec").css("display","block");
        $('#f_subs_numof_req').prop('required', false);
        $(".f_amount_cycle").css("display","block");
        $(".f_pln_user_type").css("display","block");
        $("#subscrptn_frm").removeClass("request_mode");
        $("#subscrptn_frm").addClass("subs_mode");
        if(paymode == 1){
            $("#f_subs_planid").prop('required', true);
            $("#f_subs_plan_id").prop('required', false);
        }else{
            $("#f_subs_plan_id").prop('required', true);
            $("#f_subs_planid").prop('required', false);
        }
            
    }
    
});
$(document).on('change', '.f_bypss_payment input[type="checkbox"]', function () {
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    if (ischecked == 1) {
        $(".f_card_details").css("display", "none");
        $(".f_card_number").prop("required", false);
        $(".f_expir_date").prop("required", false);
        $(".f_cvv").prop("required", false);
    } else {
        $(".f_card_details").css("display", "block");
        $(".f_card_number").prop("required", true);
        $(".f_expir_date").prop("required", true);
        $(".f_cvv").prop("required", true);
    }
});

$("#addclientwithsubs").submit(function (e) {
    e.preventDefault();
    var subuser_reqdata = jQuery.parseJSON(subusers_reqdata);
    var shareduser = $(this).find("#requests_type").find(':selected').attr('data-shared');
    var inprogress_req = $(this).find("#requests_type").find(':selected').attr('data-inprogress');
    var formData = $(this).serialize();
    var assigned, reqcount, allow_req, pending_req, error_msg, main_dedicated_designr;
    error_msg = "You can't add more shared user";

    if (shareduser == 0) {
        inprogress_req = parseInt(inprogress_req) * parseInt(subuser_reqdata.shared_ratio);
        error_msg = "You can't add more dedicated user";
    }
    assigned = parseInt(subuser_reqdata.addedsbrqcount);
    reqcount = parseInt(assigned) + parseInt(inprogress_req);
    allow_req = parseInt(subuser_reqdata.main_inprogress_req);
    pending_req = parseInt(subuser_reqdata.pending_req);
    main_dedicated_designr = parseInt(subuser_reqdata.main_dedicated_designr);
    var allslot = parseInt(subuser_reqdata.allslot);

    var plan_type = $(this).find("#requests_type").find(':selected').attr('data-plan_type');

    if (plan_type !== "one_time" && main_dedicated_designr == 1 && shareduser != 1) {
        $('.clienterror_msg').html("<span class='validation'> You can't add dedicated designer, you can add only shared user.</span>");
        $(".clienterror_msg").css("display", "block");
        $(".clntajax_loader").css("display", "none");
    } else if(plan_type !== "one_time" && allslot == -1){
        $('.clienterror_msg').html("<span class='validation'>You have assigned all slots to yourself, so you don't have access to add client. <a class='demo_stripe_link' href = '"+baseUrl+"account/client_management#addi_setting' target='_blank'>Click here</a> to adjust slot for add client. </span>");
        $(".clienterror_msg").css("display", "block");
        $(".clntajax_loader").css("display", "none");
    }else if (plan_type !== "one_time" && reqcount > allow_req) {
        $('.clienterror_msg').html("<span class='validation'>" + error_msg + "</span>");
        $(".clienterror_msg").css("display", "block");
        $(".clntajax_loader").css("display", "none");
    } else {
        //return false;
        $(".clienterror_msg").css("display", "none");
        $(".clntajax_loader").css("display", "block");

        $.ajax({
            type: 'POST',
            url: baseUrl + "account/AgencyClients/addclientwithsubscription",
            dataType: "json",
            data: formData,
            success: function (data) {
                if (data.status == 1) {
                    var url = window.location.href;
                    var chnage = baseUrl + "account/client_management/#client_management";
                    if (url != chnage) {
                        window.location.href = baseUrl + "account/client_management#client_management";
                    }
                    window.location.reload();
                } else {
                    $(".clienterror_msg").html(data.msg);
                    $(".clienterror_msg").css("display", "block");
                    $('html, body').animate({
                        scrollTop: $("#addclientwithsubs").offset().top
                    }, 2000);
                $(".clntajax_loader").css("display", "none");
                }
                
            }
        });
    }
});

$("#subscrptn_frm").submit(function (e) {
    e.preventDefault();
    var formData = $(this).serialize();
    $(".subsajax_loader").css("display", "block");
    $(".subserror_msg").css("display", "none");
    $.ajax({
        type: 'POST',
        url: baseUrl + "account/AgencyClients/addclientsubscription",
        dataType: "json", 
        data: formData,
        success: function (data) {
            if (data.status == 1) {
                var url = window.location.href;
                var chnage = baseUrl + "account/client_management#subscription_management";
                if (url != chnage) {
                    window.location.href = baseUrl + "account/client_management#subscription_management";
                }
                window.location.reload();
            } else {
                $(".subserror_msg").html(data.msg);
                $(".subserror_msg").css("display", "block");
                $('html, body').animate({
                    scrollTop: $("#subscrptn_frm").offset().top
                }, 2000);
            }
            $(".subsajax_loader").css("display", "none");
        }
    });
});

//$(document).on('change', '#password_ques', function () {
//    var ischecked = ($(this).is(':checked')) ? 1 : 0;
//    if (ischecked === 0) {
//        $('.create_password').css("display", "block");
//    } else {
//        $('.create_password').css("display", "none");
//    }
//});

$(document).on('click', ".ChngclntPln", function (e) {
    //    console.log('test');
    e.preventDefault();
    var data_id = $(this).attr('data-value');
    var data_price = $(this).attr('data-price');
    var data_inprogress = $(this).attr('data-inprogress');
    var data_active = $(this).attr('data-totlactive');
    var display_name = $(this).attr('data-display_name');
    var clientid = $(this).attr('data-clientid');
    jQuery('.display_namepln').val(display_name);
    jQuery('.plan_price').val(data_price);
    jQuery('.in_progress_request').val(data_inprogress);
    jQuery('.totl_active_request').val(data_active);
    jQuery('.plnclient_id').val(clientid);
    $(".plan_name_to_upgrade").val(function () {
        return data_id;
    });
});

$(document).on('click', ".upgrdclntacnt", function () {
    var data_id = $(this).attr('data-value');
    var data_price = $(this).attr('data-price');
    var clientid = $(this).attr('data-clientid');
    var applycoupn = $(this).attr('data-applycoupn');
    jQuery('#subsplanprice').val(data_price);
    jQuery('#clientfinalplan_price').val(data_price);
    jQuery('#clientplanprice').val(data_price);
    jQuery('#subuser_id').val(clientid);
    jQuery('#client_plan').val(data_id);
    jQuery('#clientcouponinserted_ornot').val(applycoupn);
    $(this).attr('data-url');
    jQuery('.disc-code').val('');
    jQuery('.ErrorMsg').css('display', 'none');
    jQuery('.coupon-des-newsignup').css('display', 'none');
    jQuery('.total-pPrice').html('<div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p total-biled text-left">Total Billed</div><div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p text-right"><span class="appened_price">$' + data_price + '</span></div><div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p g-totl text-left">Grand Total</div><div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p g-totl-prc text-right"><span class="appened_price">$' + data_price + '</span></div>');


});

$("#f_clntupgrade_account").submit(function (e) {
    e.preventDefault();
    $('.bill-infoo .error-msg').html("");
    var formData = $(this).serialize();
    $('.loading_account_procs').html('<span class="loading_process" style="text-align:center;"><img src="' + $assets_path + 'img/ajax-loader.gif" alt="loading"/ style="margin: auto;display: block;"></span>');
    $('#clntpayndupgrade').prop('disabled', true);
    $.ajax({
        type: 'POST',
        url: baseUrl + "account/AgencyClients/upgradeclient_plan",
        dataType: "json",
        data: formData,
        success: function (response) {
            $('.loading_account_procs').html('');
            $('#clntpayndupgrade').prop('disabled', false);
            if (response.success) {
                $("#Upgrdsubuseracount").css("display", "none");
                $(".modal-backdrop.fade.in").remove();
                $('#f_clientsubscribed').trigger('click');
                $("#f_clientsubscribed").css("display", "block");
            } else {
                $('.bill-infoo .error-msg').html(response.error);
            }
        },
        error: function () {
            $('.loading_account_procs').html('');
            $('#clntpayndupgrade').prop('disabled', false);
            $('.bill-infoo .error-msg').html('Error occurred while upgrading account, please try again or contact us!');
        }
    });
    return false;
});

$(document).on("click",".active_blling_acont",function(){
    var client_id = $(this).data("id");
    $("#f_billing_client_id").val(client_id);
    var plan_type = $("#f_selct_clintsubs").find(':selected').attr('data-plan_type');
    if(plan_type == "one_time"){
        $(".datepicker_billing_sec").css("display","none");
    }else{
        $(".datepicker_billing_sec").css("display","block");
    }
    $("#f_active_plan_type").val(plan_type);
});

$(document).on('change', "#f_selct_clintsubs", function () {
    var selected_plan = $(this).find(':selected').attr('data-plan_type');
    if(selected_plan == "one_time"){
        $(".datepicker_billing_sec").css("display","none");
    }else{
        $(".datepicker_billing_sec").css("display","block");
    }
    $("#f_active_plan_type").val(selected_plan);
});

$(document).on('click', ".cantaddclnt", function () {
    var reserveslot = $(this).data("reserveslot");
    if(reserveslot == -1){
        $(".f_reserveallslot").html("You have assigned all slots to yourself, so you don't have access to upgrade plan. <a class='demo_stripe_link' href = '"+baseUrl+"account/client_management#addi_setting' target='_blank'>Click here</a> to adjust slot for add client.");
    }else{
        $(".f_reserveallslot").html("You already assigned all slot to users, so you don't have access to upgrade plan.");
    }
});

$(document).on('change', '.switch-custom-usersetting-check.f_active_subs_toggle input[type="checkbox"]', function () {
    var data_id = $(this).attr('data-subsid');
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    $.ajax({
        type: 'POST',
        url: baseUrl + "account/AgencyClients/enable_disable_subscription",
        data: { status: ischecked, data_id: data_id },
        success: function (data) {
            var url = window.location.href;
            var chnage = baseUrl + "account/client_management#subscription_management";
            if (url != chnage) {
                window.location.href = baseUrl + "account/client_management#subscription_management";
            }
            window.location.reload();
        }
    });
});

/** Client additional settings **/
$(document).on('change','#online_payment',function(){
     
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    var msg = "";
        if(ischecked === 1){
            $("#isokyAttr").attr("data-prop",false);
            msg = "<h3 class='head-c text-center'>Please follow the below steps for accepting payments online:</h3><ul><li>Firstly, connect your Stripe account with GraphicsZoo. You can connect your existing account or create a new one.</li><li>Create subscriptions on Stripe and Sync with the GraphicsZoo Subscriptions. (You will get the guidelines in the subscription module.)</li><li>Sync your existing customers (if any) account with your Stripe account by clicking the Sync button in the client's management section.</li></ul>";
            if($("#connect_withstripe").length > 0){
                 $("#connect_withstripe").css("display","inline-block");
            }
        }else{
            $("#isokyAttr").attr("data-prop",true);
            msg = "<h3 class='head-c text-center caution_icn'><i class='fas fa-exclamation-triangle'></i> You will have to manage your payments manually. No payment process will take place on the website for the new users as well for the existing customers.</h3><p><strong>Note:</strong> System will disconnect your stripe account from GraphicsZoo automatically but you will have to cancel your customers' subscription from stripe yourself.</p></li></ul>";
            if($("#connect_withstripe").length > 0){
                $("#connect_withstripe").css("display","none");
            }
        }
    $(".f_payment_mode").html(msg);
    
   var  chk_prop = $("#isokyAttr").attr("data-prop");
   if(chk_prop == "true"){
       chk_prop = true;
   }else{
       chk_prop = false; 
   }
    $('#online_payment').prop("checked", chk_prop);
});

$(document).on('click','#change_paymnt_flg',function(){
    var check_prop = "";
    var prop = $("#isokyAttr").attr("data-prop");
    if(prop == "false"){
        check_prop = true;
    }else{
        check_prop = false;
        $("#auto_upgrade").prop("checked",false);
    }
    $('#online_payment').prop("checked", check_prop);
    $('#change_paymentflag').modal('hide');
});
$(document).on('change','#f_subs_payment_mode',function(){
    var payment_mode = $(this).data("payment_mode");
    var stripe_link = $(this).data("stripe_link");
    var select_mode = ($(this).is(':checked')) ? 1 : 0;
    if(payment_mode == 0 && select_mode == 1 && stripe_link != ""){
        $("#m_connectwithstripe").modal("show");
        $(this).prop("checked",false);
    }
    if(select_mode == 1 && stripe_link == ""){
        $(".stripe_pln_sec").css("display","block");
        $("#f_subs_planid").prop("required",true);
        $('.payment_mod_txt').html("Online Payment");
    }else{
        $(".stripe_pln_sec").css("display","none");
        $("#f_subs_planid").prop("required",false);
        $('.payment_mod_txt').html("Offline Payment");
    }
});
$(document).on('change','#shared_designer',function(){
    var shared = ($(this).is(':checked')) ? 1 : 0;
    if(shared == 0){
     $("#ratio_numbr").val("1").find("option[value='1']").attr('selected', true);
     $("#ratio_numbr").prop("disabled",true);
    }else{
     $("#ratio_numbr").prop("disabled",false); 
    }
});

$(document).on('change','#f_reserve_count',function(){
    var reserve = $(this).val();
    var main_user_req = $(this).data("total_req");
    if(reserve == '-1'){
        reserve = main_user_req;
    }
    var resell = main_user_req - reserve;
    $(".f_resell_count").html(resell);
});
/** End client additional setting **/
/* end client management page js */


function make_gridForImages(imgid){
      $this = $(this); 
      setTimeout(function(){ 
            var grid = $(".grid");
            var getItem = grid.children('li');
            getItem.each(function(){
                 var getItem_height = 0;
                getItem_height = $(this).find('div').children("img").height(); 

               var rowHeight =  parseInt($(grid).css( "grid-auto-rows" ));
               var rowGap = parseInt($(grid).css('grid-row-gap'));
               var rowSpan = Math.ceil((getItem_height + rowGap) / (rowHeight + rowGap))
                     // $(this).css( "grid-row-end", "span " + rowSpan  );
                     $(this).css( "grid-row-end", "span " + (rowSpan - 1) );
                // if(rowSpan > 15){
                    
                //     $(this).css( "grid-row-end", "span  15");
                // }
                // else{
                     
                //     $(this).css( "grid-row-end", "span " + (rowSpan - 1) );
                // }
                $("#"+imgid).css("opacity","1");
                $("#"+imgid+"cr").css("opacity","1");
            });
        },1000);
        
         
         
        }
        

 function CheckImageLoad(imageClass){
    $('.'+imageClass)
        .load(function(){
            var imgId = $(this).attr("id");
              make_gridForImages(imgId); 
         })
        .error(function(){
                console.log("Found Error in loading image");
        });
        return true; 
    }
    
     $(document).on('change','.prvt_links',function(){
    var array = []; 
    $(".prvt_links:checkbox:checked").each(function() { 
        array.push($(this).val()); 
    });
    if(jQuery.inArray("download_source_file", array) == -1){
        $('.privt_dwnld').text('off');
        }else{
            $('.privt_dwnld').text('on');
        }

        if(jQuery.inArray("mark_as_completed", array) == -1){
             $('.privt_cmplt').text('off');
        }else{
            $('.privt_cmplt').text('on');
        }

        if(jQuery.inArray("comment_revision", array) == -1){
             $('.privt_cmnt').text('off');
        }else{
            $('.privt_cmnt').text('on');
        }
 });
 
 $(document).on('change', '#f_update_psswrd', function () {
        var ischecked = ($(this).is(':checked')) ? 1 : 0;
        if(ischecked === 1){
            $('#host_password').prop("disabled", false);
            $('#host_password').attr("data-is_password","");
        }else{
            $('#host_password').prop("disabled", true);
            $('#host_password').attr("data-is_password","yes");
            $('.host_password .error_msg').html('');
        }
});
/************cancel subscription********/
$(document).on('change','#change_reason',function(){
   $('.error_sel').html("");
   $('.reason_submit').removeClass('reason_submit_disable');
var reason = $( "#change_reason option:selected" ).val();
if(reason == 'Designs are taking too long to complete' || reason == 'Designers are not understanding my request' || reason == 'The design quality is not good'){
    $('.switch_designer_box').slideDown( "slow" );
    $(".yes_rad").prop("checked", true);
}else if(reason == 'We are going with another service provider or in-house designer'){
    $('.give_reason').slideDown( "slow" );
    $('.switch_designer_box').slideUp( "slow" );
    $(".yes_rad").prop("checked", false);
    $(".no_rad").prop("checked", false);
}else{
    $('.switch_designer_box').slideUp("slow");
    $('.give_reason').slideUp( "slow" );
    $('#reason_desc').val('');
    $(".yes_rad").prop("checked", false);
    $(".no_rad").prop("checked", false);
}
});
$(document).on('click','.reason_submit',function(e){
    e.preventDefault();
    var is_already_cancelled = $(this).data('is_already_cancelled');
    var is_coupn_applied = $(this).data('is_coupn_applied');
    var reason = $( "#change_reason option:selected" ).val();
    var feedback = $('#cancel_feebback').val();
    var why_reason = $('#reason_desc').val();
    var switchdesigner = $("input[name='radio_des']:checked").val();
   if(is_already_cancelled == 1 && is_coupn_applied == ''){
       $('#sorry_popup').modal('show');  
    }else{
    if((reason && typeof(switchdesigner) == 'undefined') || (reason && switchdesigner == 'no') && is_coupn_applied == ''){
        $('#prompt_popup').modal('show'); 
        $('.reason_val').val(reason);
        $('.feedback_val').val(feedback);
        $('.is_switch_designer').val(switchdesigner);
        $('.why_reason').val(why_reason);
    }else if(reason && switchdesigner == 'yes' && is_coupn_applied == ''){
         $('#switch_popup').modal('show'); 
         $('.reason_val').val(reason);
        $('.feedback_val').val(feedback);
        $('.is_switch_designer').val(switchdesigner);
    }else if(is_coupn_applied){
        $('#form_cancel_pop').submit();
    }else{
      $('.error_sel').html("<span class='validation' style=color:red;font-size:13px;>Please choose one reason.</span>");
    }
    }
});

/******file sharing in main chat******/
$(document).on('click', '.attchmnt', function () {
    $("input[id='shre_file']").click();
});

$(".f_upgrade-link").click(function(){
    location.href= baseUrl+"account/setting-view#billing"; 
    if(get_method == "setting_view"){
        location.reload();
    }
});

/*******add new updates in color preferences request*********/
$(document).on('change','.color_codrr',function(){
    var color_type = $("input[name='color_pref[]']:checked").val();
    if(color_type == "color_code"){
        $('.choose_select_pref').css('display','block');
    }else{
       $('.choose_select_pref').css('display','none'); 
    }
});


$(document).on('change','.choose_design_pref',function(){
     var color_ref = $( ".choose_design_pref option:selected" ).val();
     if(color_ref == 'pantone' || color_ref == 'hex'){
         $('.color_choice').css('display','block');
     }else{
         $('.color_choice').css('display','none');
     }
});

var max_fields_limit = 10; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $('.add_more_colors').click(function (e) { //click event on add more fields button having class add_more_button
        e.preventDefault();
        var name = $(this).parent().find(".f_color_nm").attr('name');
        if (x < max_fields_limit) { //check conditions
            x++; //counter increment
            $('.input_color_container').append('<div><div class="remove-pre"><input type="text" class="input" name="'+name+'"/><div class="line-box"><div class="line"></div></div><a href="#" class="remove_field">-</a></div></div></div>'); //add input field
        }
    });
    
    $('.input_color_container').on("click", ".remove_field", function (e) { //user click on remove text links
        e.preventDefault();
        jQuery(this).fadeOut('slow', function(){ jQuery(this).parent('div').remove(); });
        // jQuery(this).parent('div').remove();
        x--;
    });
    
    
/*********cancel popup on mark as comnpleted***/
//$(document).on('click','.checkfeedbacksubmit',function(){
//    var current_status = $(this).data('current_status');
//    var request_id = $(this).data('reqid');
//    var from_markascomplete =  $('input[name="from_markascomplete"]').val('1');
////    console.log("current_status",from_markascomplete);
////    console.log("request_id",request_id);
//    if(current_status == 'checkforapprove'){
//        $("#review_reqid").val(request_id);
//        $("#f_reviewpopup").modal("show");
//        return false;
//    }
//});

/***********submit feedback popup using ajax**********/
//$(document).on('submit','.customer_feeds',function(){
//  $.ajax({
//        type: 'POST',
//        url: baseUrl+'admin/Dashboard/design_review_frm',
//        data: $('#f_feedback_frm').serialize(),
//        dataType: "json",
//        success: function (res) {
//            var file_id = res.file_id;
////            console.log("res",res);
//            if(res.status == 'success'){
//                $('#f_reviewpopup').modal('hide');
//                $('#f_revw_feedback_'+file_id).css('display','block');
//            }
//        }
//    });  
//});

/*********affiliate start***********/
$(document).on('click','.copy_affiliated_link',function(){
    $('.show_aff').focus();
    $('.show_aff').select();
    document.execCommand('copy');
    $(".copiedaffone").text("Link copied").show().fadeOut(1200);
});

$(".f_upgrade-link").click(function(){
    location.href= baseUrl+"account/setting-view#billing"; 
    if(get_method == "setting_view"){
        location.reload();
    }
});
$(document).on('click','.copy_short_affiliated_link',function(){
    $('.show_aff_short').focus();
    $('.show_aff_short').select();
    document.execCommand('copy');
    $(".copiedshortaff").text("Link copied").show().fadeOut(1200);
});

$(document).on('click','.generate_shorten',function(){
    $('.generate_shorten').css('display','none');
    $('.generate_loader').css('display','block');
    var full_url = $(this).attr('data-url');
    $.ajax({
        method: 'POST',
        url: baseUrl+"account/Affiliate/generateshortenUrl",
        data: {'full_url':full_url},
        dataType: "json",
        success: function (response) {
            if(response.status == "success"){
              $('.generate_shorten').css('display','none');
              $('.generate_loader').css('display','none');
              $('.shortn_url').css('display','block');
              $('.show_aff_short').val(response.short_url);
            }else{
              $('.generate_shorten').css('display','block');
              $('.generate_loader').css('display','none');
            }
        }
    });
});
/*********affiliate end***********/

/*****************Add Request for SAAS ********************/
$(document).on('change','.is_transfer',function(){
   var is_transfer = $("input[name='is_transfer']:checked").val();
//   console.log(is_transfer);
    
});

/********Add/Edit designer SAAS*************/
$(document).on("submit", "#add_designer_saas",function(e){
    e.preventDefault();
    var formData = $(this).serialize();
    FormEDitADD(formData,from="save"); 
});

$(document).on("submit", "#edit_designer_saas",function(e){
 e.preventDefault();
 var formData = $(this).serialize();
 FormEDitADD(formData,from="edit"); 
});

function FormEDitADD(formData,from){
     var tabidentity =  $("#tabidentity").val(); 
    $(".ajax_loader").show();
    var pageURL = $(location).attr("href");
     
    $.ajax({
        type: 'POST',
        url: baseUrl + "account/DesignerManagement/add_designer/"+from,
        dataType: "json",
        data: formData,
        success: function (data) {
          $(".ajax_loader").hide();
            // console.log("data",data);
            $(".ajax_loader").hide();
            if(data.status == 1){
                $(".designrerror_msg").addClass("alert-success").css("display", "block");
                $(".designrerror_msg").find(".head-c").text(data.msg);
                 window.location.href =  baseUrl+"account/setting-view"+"?"+tabidentity; 
                 // location.reload();   
                // location.reload(true);
            }else{
               $(".designrerror_msg").addClass("alert-danger").css("display", "block");
               $(".designrerror_msg").find(".head-c").text(data.msg);   
                // location.reload(true);    
           }

       }
       
   });
}

$(document).on('change','#manual_status',function(){
    var manual_status = $("input[name='manual_status']:checked").val();
    if(manual_status == 'on'){
     $('.status_val').css('display','block');
    }else{
      $('.status_val').css('display','none');   
    }
});

$(".permanent_assign_design").click(function(){
    $("#assign_customer_id").val($(this).attr("data-customerid"));
});
$(".assign_saasVa").click(function(){
   
    $("#allcustomers_id").val($(this).attr("data-requestid"));
});

$(document).on("click",function(){
 $("#assign_vatocus").show(); 
});

$('#assign_vatocus').click(function (e) {
    e.preventDefault();
    var assign_va = $('input[name=assign_va]:checked').val();
    var customer_id = $('#allcustomers_id').val();
   var designer_name = $('input[name=assign_va]:checked').attr('data-name');
    $.ajax({
        url: baseUrl + 'account/ChangeProfile/assign_va_tocustomer',
        type: 'POST',
        data: {
            'assign_va': assign_va,
            'customer_id': customer_id
        },
        success: function (data) {
             $(".ajax_loader").hide();
            var returnedData = JSON.parse(data);
            //         console.log('returnedData',returnedData);
            if (returnedData.status == 'success') {
                $('#close-pop').click();
                $(".assign_saasVa").hide();
                $("." + customer_id + " p.text-h").html('<a href="#" class="assign_va assign_saasVa" data-toggle="modal" data-target="#Addva"  data-requestid="'+customer_id+'"  data-designerid=""><img src="'+$assets_path+'img/gz_icons/assign_designer_edit.svg"></a>'+designer_name);
                //                 console.log("."+customer_id+" p.text-h",designer_name);
                //  $("."+customer_id+" a.adddesinger").attr('data-designerid',designer_id);
            } else {
                $('#show_error').html(returnedData.msg);
                //             console.log(returnedData.msg);
                $('.alert-dismissable.error').css('display', 'block');
            }
        }
    });
});
/** end assign VA js**/

$(document).on('change', '.is_verified input[type="checkbox"]', function (e) {
    var data_id = $(this).attr('data-pid');
    var data_value = $(this).attr('data-value');
    var ischecked = ($(this).is(':checked')) ? data_value : 0;
    if (ischecked == 1) {
        $('#verified_' + data_id).addClass('verified_by_admin');
        if ($(this).closest('label').find('i').hasClass("fa-circle")) {
            $(this).closest('label').find('i').removeClass('fa-circle');
            $(this).closest('label').find('i').addClass('fa-check-circle');
            $(this).closest('label').find('.vrfy_txt').html('Verified');
        }
    } else if (ischecked == 2) {
        $('#verified_' + data_id).addClass('verified_by_designer');
        $("#verified_" + data_id + " .verified").prop("disabled", true);
        if ($(this).closest('label').find('i').hasClass("fa-circle")) {
            $(this).closest('label').find('i').removeClass('fa-circle');
            $(this).closest('label').find('i').addClass('fas fa-check-circle');
            $(this).closest('label').find('.vrfy_txt').html('Verified');
        } else if ($(this).closest('label').find('i').hasClass("fa-check-circle")) {
            $(this).closest('label').find('i').removeClass('fa-check-circle');
            $(this).closest('label').find('i').addClass('fas fa-check-circle');
            $(this).closest('label').find('.vrfy_txt').html('Verify');
        }
    } else {
        $('#verified_' + data_id).removeClass('verified_by_admin');
        $('#verified_'+data_id).removeClass('verified_by_designer'); 
        if ($(this).closest('label').find('i').hasClass("fa-check-circle")) {
            $(this).closest('label').find('i').removeClass('fa-check-circle');
            $(this).closest('label').find('i').addClass('fa-circle');
            $(this).closest('label').find('.vrfy_txt').html('Verify');
        }
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + "account/request/request_verified_by_admin",
        data: { data_id: data_id, ischecked: ischecked },
        success: function (data) {
        }
    });
});

//$(".checkAll").change(function () {
$(document).on('change', '.checkAll', function () {
    var attribute = $(this).attr('data-attr');
    if ($(this).is(':checked')) {
        $('.' + attribute).prop("checked", true);
    } else {
        $('.' + attribute).prop("checked", false);
    }
    var requestIDs = [];
    $.each($("input[name='project[]']:checked"), function () {
        if ($(this).val() != 'on') {
            requestIDs.push($(this).val());
        }
    });
    $('.adddesinger').attr('data-requestid', requestIDs);
    
    if(requestIDs.length >= '1'){
         $('.adddesinger').addClass('relate_check');
    }else{
        $('.adddesinger').removeClass('relate_check');
    }
});

$(document).on('change', '.selected_pro', function () {
    var requestIDs = [];
    $.each($("input[name='project[]']:checked"), function () {
        if ($(this).val() != 'on') {
            requestIDs.push($(this).val());
        }
    });
    $('.adddesinger').attr('data-requestid', requestIDs);
});

$(document).on('click', ".adddesinger", function () {
    var request_id = $(this).attr('data-requestid');
    var designer_id = $(this).attr('data-designerid');
    $('#AddDesign #request_id').val(request_id);
    if (designer_id !== "") {
    $('#AddDesign input#' + designer_id).click();
    }
});

$(document).on('click', "#assign_desig", function (e) {
    e.preventDefault();
//    console.log("assign_designer");
    var designer_id = $('input[name=assign_designer]:checked').val();
//    console.log("designer_id",designer_id);
    var request_id = $('#request_id').val();
    var designer_name = $('input[name=assign_designer]:checked').attr('data-name');
    if (typeof designer_id === "undefined" || designer_id == "") {
        $('#show_error').html("Please Select Any Designer");
        $('.alert-dismissable.error').css('display', 'block');
    }else{
    $.ajax({
        url: baseUrl + 'account/Request/assign_designer_ajax',
        type: 'POST',
        data: {
            'assign_designer': designer_id,
            'request_id': request_id
        },
        success: function (data) {
            var returnedData = JSON.parse(data);
            if (returnedData.status == 'success') {
                $('.alert-dismissable.error').css('display', 'none');
            var result = (request_id).split(',');
            if(result.length > 1){
            $.each(result,function(i){
                $("." + result[i] + " p.text-h").html(designer_name);
                $("." + result[i] + " a.adddesinger").attr('data-designerid', designer_id);
             });
         }else{
             $("." + request_id + " p.text-h").html(designer_name);
             $("." + request_id + " a.adddesinger").attr('data-designerid', designer_id);
         }
            } else {
                $('#show_error').html(returnedData.msg);
                $('.alert-dismissable.error').css('display', 'block');
            }
        }
    });
}
});


 

$(".designer_del").click(function(){
   $(".ajax_loader").show();
    $("#tabidentity").val($(".TeamManagement").find("li.active").find("a").attr("id"));
     var tabidentity =  $("#tabidentity").val();
    var id = $(this).data("id"); 
     $.ajax({
        type: 'POST',
        url: baseUrl + "account/MultipleUser/delete_sub_user_designer",
        data: { "id": id,"tab": tabidentity},
        success: function (data) {
            $(".ajax_loader").hide();
           // window.location.href=data;   
            window.location.reload();
             
        }
    }); 
});


function statuschange(request_id, value) {
    $('#myModal').modal('show');
    $('#request_id_status').val(request_id);
    var status = $(value).val();
    $('#valuestatus').val(status);
    if (status === "hold" || status === "unhold") {
        var status_flag = $(value).find(':selected').attr('data-status');
    } else {
        var status_flag = '0';
    }
//        console.log("request_id",request_id);
//        console.log("status_flag",status_flag);
//        console.log("status",status);
    $('#status_flag').val(status_flag);
    $('.msgal strong').html(status);
}

$('.btn-y').click(function () {
    var request_id = $('#request_id_status').val();
    var valueSelected = $('#valuestatus').val();
    var status = $('#status_flag').val();
//        console.log("request_id",request_id);
//        console.log("valueSelected",valueSelected);
//        console.log("status",status);
    if (status === "0") {
        $.ajax({
            type: "POST",
            url: baseUrl + "account/request/change_project_status",
            dataType: "json",
            data: {
                'request_id': request_id,
                'value': valueSelected,
            },
            success: function (response) {
                if (response == '1') {
                    $('#statuspass').css('display', 'block');
                    setTimeout(function () {
                        $('#statuspass').fadeOut();
                        location.reload();
                    }, 2000);
                } else {
                    $('#statusfail').css('display', 'block');
                    setTimeout(function () {
                        $('#statusfail').fadeOut();
                        location.reload();
                    }, 2000);
                }
            }
        });
    } else {
        $.ajax({
            type: 'POST',
            url: baseUrl + "account/request/hold_unhold_project_status",
            data: { status: status, data_id: request_id },
            success: function (data) {
                window.location.reload();
            }
        });
    }
});
/**update saas subscription**/
$(document).on('click', ".s_upgrd", function (e) {
    e.preventDefault();
    var data_id = $(this).attr('data-value');
    var data_price = $(this).attr('data-price');
    var data_inprogress = $(this).attr('data-inprogress');
	var applycoupn = $(this).attr('data-applycoupn');
	console.log("applycoupn",applycoupn);
    jQuery('.plan_price').val(data_price);
    jQuery('#s_final_plan_price').val(data_price);
    jQuery('#s_subs_plan_price').val(data_price);
    jQuery('.in_progress_request').val(data_inprogress);
	jQuery('.couponinserted_ornot').val(applycoupn);
    $(this).attr('data-url');
    $(".plan_name").val(function () {
        return data_id;
    });
    jQuery('.disc-code').val('');
    jQuery('.ErrorMsg').css('display', 'none');
    jQuery('.coupon-des-newsignup').css('display', 'none');
    jQuery('.total-pPrice').html('<div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p total-biled text-left">Total Billed</div><div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p text-right"><span class="appened_price">$' + data_price + '</span></div><div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p g-totl text-left">Grand Total</div><div class="col-lg-6 col-md-6 col-xs-6 toatal-bill-p g-totl-prc text-right"><span class="appened_price">$' + data_price + '</span></div>');
//    console.log("upgrd_account");
   // $("#SelectPackForUP").css("display", "none");
    $("#SelectPackForUP").modal('hide');
   //$(".modal-backdrop.fade.in").remove();
    jQuery('#s_upgrade_account').trigger('click');

});

$("#s_upgrade_useraccount").submit(function (e) {
    e.preventDefault();
    $('.bill-infoo .error-msg').html("");
    var customer_id = $('#s_customer_id').val();
    var plan_name = $('.plan_name').val();
    var in_progress_request = $('.in_progress_request').val();
    var card_number = $('#s_card_number').val();
    var plan_price = $('.plan_price').val();
    var final_plan_price = $('#s_final_plan_price').val();
    var expir_date = $('#s_expir_date').val();
    var state_tax = $('#s_state_tax').val();
    var state = $('#s_state_upgrd').val();
    var cvc = $('#s_cvc').val();
    var term = $('#s_term').val();
    var email = $('#s_email').val();
    var discount = $('#s_discCode').val();
    var url;
    $('.loading_account_procs').html('<span class="loading_process" style="text-align:center;"><img src="'+$assets_path+'img/ajax-loader.gif" alt="loading"/ style="margin: auto;display: block;"></span>');
    $('#s_payndupgrade').prop('disabled', false);
        url = baseUrl+"account/ChangeProfile/createsaaspln";
    $.ajax({
        type: 'POST',
        url: url,
        dataType: "json",
        data: {
            customer_id: customer_id,
            plan_name: plan_name,
            card_number: card_number,
            expir_date: expir_date,
            cvc: cvc,
            term: term,
            discount: discount,
            email: email,
            in_progress_request: in_progress_request,
            plan_price: plan_price,
            final_plan_price: final_plan_price,
            state_tax: state_tax,
            state: state
        },
        success: function (response) {
            $('.loading_account_procs').html('');
            $('#s_payndupgrade').prop('disabled', false);
            if (response.success) {
                $("#s_upgradeacoount").css("display", "none");
                $(".modal-backdrop.fade.in").remove();
                $('#subscribed_USR').trigger('click');
            } else {
                $('.bill-infoo .error-msg').html(response.error);
            }
        },
        error: function (response) {
            $('.loading_account_procs').html('');
            $('#s_payndupgrade').prop('disabled', false);
            $('.bill-infoo .error-msg').html('Error occurred while upgrading account, please try again or contact us!');
        }
    });
    return false;
});
/**end update saas subscription**/
$(document).on('click','.teamMangmnt-back',function(){
 $("#sub-userlist").show(); 
 $("#new-subuser").hide(); 
});
$(document).on('click','.TeamManagement li a',function(){
 $("#customer_editForm").hide(); 
 $("#client-tab").removeAttr("style"); 
 // $("#client-tab").show(); 
});
 
 
$(".edit_subuser-op").click(function(){
    var id = $(this).data("id");
  $(".ajax_loader").show();
    $.ajax({
            type: 'POST',
            url: baseUrl + "account/AgencyClients/index/"+id,
            data: {"request": "ajax"},
            cache: false,
            success: function (res) {
               $(".ajax_loader").hide();
                // console.log("res",res);
                $("#client-tab").hide(); 
                $("#customer_editForm").show(); 
                $("#customer_editForm").html(res); 
                //window.location.reload();
            }
        });

});
