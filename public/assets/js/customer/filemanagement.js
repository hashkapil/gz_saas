     /* ps custom file management code start from here */
/* ##### File Management Code Start From here ##########*/
function array_values(arrayObj,type) {
 tempObj = [];
  Object.keys(arrayObj).forEach((prop) => {
    if (arrayObj[prop][type]) { tempObj[prop] = arrayObj[prop][type]; }
  });
  return tempObj;
};
 $(document).on('click','.sub_directory',function(e){
       var bRCName = $('.breadcrumb_name li').html()
        var data_isb = $(this).attr("data-isb");
        var status = $(this).data('status');
        var id = $(this).data('reqid');
        var title = $(this).data('title');
        var foldid = $(this).data('foldid');
        var className = $(this).attr('class');
         $(".selected_folder").val(title);
        if(foldid != null){
            var folderId = "data-foldid='"+foldid+"'"; 
        }else{
             folderId = id; 
        }
        
        $(".upload_btn_cst").html('<a href="javascript:void(0)" data-toggle="modal" data-target="#f_uploadfiles_custom" class="create_directory upldflsfrmcstom"><i class="fas fa-upload"></i><span>Files</span>');
             
        if(data_isb == "1" ){ 
            $(".customer_designer_directory").find("a.active").data("isloaded","0"); 
           $(this).nextAll().remove(); 
            var bUpdate =  $('.breadcrumb_name li').html();
            addbreadcrumb(bUpdate);
   
        }else{ 
           addbreadcrumb("<a href='javascript:void(0);' onclick='active_trigger()' data-isb='1' class='three3'> My Folders</a><a href='javascript:void(0);' data-isb='1' class='three3C "+className+"' "+folderId+">/ "+title+'</a>');
        }
         $(".customer_directory").attr("isloaded","0");
        
 });
 $(document).on('click', '.customer_directory', function () {
        $(this).addClass('active');
        $(this).siblings().removeClass('active').attr("isloaded","0");
        $(this).addClass('selected_folder');
        $(this).siblings().removeClass('selected_folder');   
        $('.from').val('main');
        $('.requests_files').css('display', 'block');    
        $('.go_back_file').css('display', 'block');
        $('.go_back_file').prop('disabled', false);
        $('.customer_designer_requests').css('display', 'none');
        $('.requests_all_files').css('display', 'block');
        var role = $(this).data('role');  
        var title = $(this).data('title'); 
        var data_bc = $(this).data('bc'); 
        var status = $(this).data('status'); 
        if(title == null){
           var text = $(this).text(); 
            title = $.trim(text.replace("/", " "));
         } 
         var mytext ="";
         if($(this).parents("div").hasClass('customer_designer_directory')){
           mytext = $(this).text();  
         } else if($(this).hasClass('sub_directory')){
             mytext = "My Folders";
         } 
//            if(role !="brands" || role != "custom_structure"){
//               $(".upload_btn_cst").html('');
//            }

        var self_id = $(this).data('foldid');
        $('.go_back_file').attr('data-url',role);
     
         $isloaded = $(this).attr("isloaded");
         
         if($isloaded !=1){
             
             Ajax_for_getAllRequestsUser(role,self_id,title,mytext);
         }
    });
  
  function Ajax_for_getAllRequestsUser(role,self_id,title="",mytext=""){
     
      //show_loader();
      $.ajax({
            type: "POST",
            dataType: 'json',
            url: baseUrl+"customer/Filemanagement/getAllRequestsUser",
            data: {"role": role, "self_id": self_id},
            success: function (data) {
                $(".title h3").text($.trim(mytext));
                
                $(".folder_id").val(self_id);
                //hide_loader();
                $(this).attr("isloaded","1");
                $(".copy_directory").hide();
                var html = '';
                var files_html = '';
                var count = 10;
               var uploadcst = $(".upload_btn_cst").html();
//             console.log("data_uu",data); 
             if(data.folder != ""  || data.files != null ) {
                if (role == "projects") {
                    $(".upload_btn_cst").html('');
                    addbreadcrumb("<a href='javascript:void(0);'>"+role + '</a>');
                    $.each(data, function (key, value) {
                      var name = value.name;
                        if(value.request_id !=null){
                        html += '<li class="copyfolder_or_files three"><a href="JavaScript:void(0);" class="projects_list" data-status="projects" data-reqid=' + value.request_id + ' data-title="' + name + '"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"><div class="default_filename"><h2>'+name+ '</h2><p class="in-des">'+data.count[key]+ ' Designs</p></div></a> <span class="rq_status '+data.status[key].color+'">'+data.status[key].status+'</span></li>';
                    }
                    });
                } else if (role == "brands") {
                    if(uploadcst !=""){
                        $(".upload_btn_cst").html('');
                    }
                    addbreadcrumb("<a onclick='active_trigger()' class='b1'>Brands Profile</a>");
                    $.each(data, function (key, value) {
                      var name = value.brand_name;
                         if(value.id !=null){
                        html += '<li class="copyfolder_or_files four"><a href="JavaScript:void(0);" class="request_file" data-url="'+value.brand_name+'" data-status="brands" data-reqid=' + value.id + ' data-title="' + name + '"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"><div class="default_filename"><h2>'+name+ '</h2><p class="in-des">'+data.count[key]+ ' Designs</p></div></a></li>';
                    }
                    });
                }  
                else if (role == "default_folder") {
                  $(".upload_btn_cst").html('');
                    $.each(data.folder, function (key, value) {
                        var name = value.folder_name;
                        var type = value.folder_type;
                        var user_id = data.user_id;
                        addbreadcrumb("<a onclick='active_trigger()' >Default Folders</a>");
                        html += '<li class="copyfolder_or_files five"><a href="JavaScript:void(0);" class="request_file" data-url="'+value.folder_name+'" data-status='+type+' data-foldid="'+ value.id +'" data-reqid=' + value.id + ' data-user-id='+user_id+' data-title="' + name + '"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"><div class="default_filename"><h2>'+name+ '</h2><p class="in-des">'+data.count[key]+ ' Designs</p></div></a></li>';
                    });
                }
                else if (role == "custom_structure") {
                    if(uploadcst !=""){
                        $(".upload_btn_cst").html('');
                    }
                    addbreadcrumb("<a onclick='active_trigger()' class='cust_bg'>My Folders</a>"); 
                    $name = $(this).text();
                    var creatFolderHtm,lastKey,folderCount,fileCount,totalCount,commas,filetext,foldertext;
                    if (data.folder!= null && data.folder.length != "0") {
                         
                    $.each(data.folder, function (key, value) {
                        if(data.foldercount != null){ folderCount = data.foldercount[value.folder_id];
                          }else{ folderCount = 0;    } 
                        
                        if(data.filecount[value.folder_id]!=null){ fileCount = data.filecount[value.folder_id];  
                        }else{ fileCount = 0;    }
                        
                        if(folderCount ==  null || folderCount ==  0 ){
                            folderCount = ''; 
                            foldertext =''; 
                             
                        }else{
                             if(folderCount > 1){foldertext = 'Folders'; }else{   foldertext = 'Folder';} } 
                                if(fileCount ==  null  || fileCount ==  0){
                                     fileCount = ' '; 
                                     filetext =' '; 
                                      
                                 }else{
                                    
                                     if(fileCount > 1 ){  filetext = 'Files'; }else {  filetext = 'File'; }
                                  } 
                                   
                         if(fileCount == 0 && folderCount == 0){
                             foldertext='Empty folder';
                             filetext ='';
                             
                           }  
                        lastKey =  data.length;
                        $d ='<div class="dropdown"><div class="simple-option_del dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-v" data-id="#custom_action_id_'+key+'"></i></div><div class="custom_action_dropdown dropdown-menu" role="menu" id="custom_action_id_'+key+'"><ul><li><a class="rename" data-id="#ur_id_'+key+'" data-foldid="'+value.folder_id+'" data-name="'+value.folder_name+'"><i class="fas fa-edit"></i>Rename</a></li><li> <a class="DelSampLOption" data-foldid="'+value.folder_id+'" data-name="'+value.folder_name+'"><i class="fas fa-trash-alt"></i>Delete </a></li></ul></div></div>';
                         if(value.folder_type != "default" && value.folder_type != "brand_default"){
                             html += '<li class="copyfolder_or_files 6"><a href="javascript:void(0)" id="ur_id_'+key+'" class="customer_directory sub_directory" data-foldid="'+value.folder_id+'" data-role="'+role+'/'+value.folder_name+'" data-title="' + value.folder_name + '"> <img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"><div class="folder_details default_filename"><h2>'+value.folder_name+'</h2><p class="in-des">'+$.trim(folderCount)+' '+foldertext+' '+$.trim(fileCount)+' '+filetext+'</p></div></a>'+$d+'</li>';
                         }
                    });
                }
                //console.log("data.files.length",data.files.length); 
                if (data.files!= null && data.files.length != "0") {
                     
                    files_html += "<div class='section-hddng'><h4>Files</h4><span></span></div>";
                    
                    $.each(data.files, function (key, value) {
                        if (value.image || value.img_name) {
                            var flname,url,image_name,project_url,dropdwn_li,folder_id,mark_as_fav;
                            if(value.mark_as_fav ==1){
                            IconHTMl =  '<i class="fas fa-heart active"></i> Remove from Favorite';
                            }
                            else{
                                IconHTMl =  '<i class="far fa-heart"></i> Mark as Favorite'; 
                            }
                            if(value.reqid == 0){
                                 flname = files_array_toshow(value.img_name, $uploadcustom_files + value.folder_id + '/' + value.img_name);
                                 url = $uploadcustom_files + value.folder_id + '/' + value.img_name;
                                 image_name = value.img_name;
                                 mark_as_fav = value.markasfav;
                                 dropdwn_li = "";
                                 folder_id = value.folder_id;
                            }else{
                                
                                 flname = files_array_toshow(value.image, $uploadAssestPath + value.reqid + '/' + value.image);
                                 
                                 url = $uploadAssestPath + value.reqid + '/' + value.image;
                                 folder_id = 0;
                                 project_url = baseUrl + 'customer/request/project-info/' + value.image;   
                                 dropdwn_li = '<li><a target="_blank" href="'+project_url+'"><i class="fas fa-link"></i> Go to Project</a></li>';
                                 mark_as_fav = value.mark_as_fav;
                                 image_name = value.image;
                            }
                            files_html += '<li class="copyfolder_or_files eight1"><a href="JavaScript:void(0);" class="viewBox" data-from="'+url+'" data-status="files" data-id='+ value.reqid +' data-file=' + image_name + ' data-reqid=' + value.reqid + ' data-title="' + image_name + '" data-projid="'+value.file_id+'"><img src="'+ flname + '"><h3>' + image_name + '</h3></a><div class="dropdown"><div class="simple-option dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-h" data-id="#action_id_'+key+'"></i></div><div class="action_dropdown dropdown-menu" role="menu" id="action_id_'+key+'"><ul><li> <a class="action_mAf" id="custm_idLast_'+key+' " data-id="'+value.file_id+'" data-requested-id="'+value.reqid+'" data-isfav="'+mark_as_fav+'" data-folder_id="'+folder_id+'" data-file="' + image_name + '">'+IconHTMl+' </a></li><li class="download_image"> <a href="'+ url + '" download><img src="'+baseUrl+'public/assets/img/download-form.svg" class="img-responsive"> Download File </a></li>'+dropdwn_li+'<li><a class="CopYfileToFolder"><i class="far fa-copy"></i> Copy to Folder</a></li><li><a target="_blank" class="deletefolderfiles" data-folderid="'+value.id+'" data-imgnm= "' + image_name + '"><i class="fas fa-trash"></i> Delete</a></li></ul></div></div></li>';
                        }
                    });
                }

                }
                else {
                    $.each(data.folder, function (key, value) {
                        if (value.folder_name) { 
                            //console.log("folder",value); 
                            var dl ='<div class="dropdown"><div class="simple-option_del dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-v" data-id="#custom_action_id_'+key+'"></i></div><div class="custom_action_dropdown dropdown-menu" role="menu" id="custom_action_id_'+key+'"><ul><li><a class="rename" data-id="#ur_id_'+key+'" data-foldid="'+value.id+'" data-name="'+value.folder_name+'"><i class="fas fa-edit"></i>Rename</a></li><li> <a class="DelSampLOption" data-foldid="'+value.id+'" data-name="'+value.folder_name+'"> <i class="fas fa-trash-alt" ></i>Delete</a></li></ul></div></div>';
                            var name = value.folder_name;
                            $("#caseForCustomFolder").val("2");
                            //name.substr(0, count) + (name.length > count ? "..." : '');
                            html += '<li class="copyfolder_or_files 7"><a href="JavaScript:void(0);" id="ur_id_'+key+'" class="request_file" data-url="'+name+'" data-status=' + value.parent_name + ' data-reqid=' + value.id + ' data-title="' + name + '"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg">' + name+'</a>'+dl+'</li>';
                            
                        }
                    });
                    if (data.files.length !== 0) {
                    files_html += "<div class='section-hddng'><h4>Files</h4><span></span></div>";
                    $.each(data.files, function (key, value) {
                        if (value.image || value.img_name) {
                            var flname,url,image_name,project_url,dropdwn_li,folder_id,mark_as_fav;
                            if(value.mark_as_fav ==1){
                            IconHTMl =  '<i class="fas fa-heart active"></i> Remove from Favorite';
                            }
                            else{
                                IconHTMl =  '<i class="far fa-heart"></i> Mark as Favorite'; 
                            }
                            if(value.reqid == 0){
                                 flname = files_array_toshow(value.img_name, $uploadcustom_files + value.folder_id + '/' + value.img_name);
                                 url = $uploadcustom_files + value.folder_id + '/' + value.img_name;
                                 image_name = value.img_name;
                                 folder_id = value.folder_id;
                                 mark_as_fav = value.markasfav;
                                 dropdwn_li = "";
                            }else if(value.file_id == 0){
                                 flname = files_array_toshow(value.img_name, baseUrl +"public/uploads/custom_files/"+ value.reqid + '/' + value.img_name);
                                 url = baseUrl +"public/uploads/custom_files/"+ value.reqid + '/' + value.img_name;
                                 project_url = baseUrl + 'customer/request/project-info/' + value.image;   
                                 dropdwn_li = '';
                                 folder_id = 0;
                                 image_name = value.img_name;
                                 mark_as_fav = value.mark_as_fav;
                            }else{
                                 flname = files_array_toshow(value.image, $uploadAssestPath + value.reqid + '/' + value.image);
                                 url = $uploadAssestPath + value.reqid + '/' + value.image;
                                 project_url = baseUrl + 'customer/request/project-info/' + value.image;   
                                 dropdwn_li = '<li><a target="_blank" href="'+project_url+'"><i class="fas fa-link"></i> Go to Project</a></li>';
                                 folder_id = 0;
                                 image_name = value.image;
                                 mark_as_fav = value.mark_as_fav;
                            }
                            
                            files_html += '<li class="copyfolder_or_files eight"><a href="JavaScript:void(0);" class="viewBox" data-from="'+url+'" data-status="files" data-id='+ value.reqid +' data-file=' + image_name + ' data-projid=' + value.file_id + ' data-title="' + image_name + '"><img src="'+ flname + '"><h3>' + image_name + '</h3></a><div class="dropdown"><div class="simple-option dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-h" data-id="#action_id_'+key+'"></i></div><div class="action_dropdown dropdown-menu" role="menu" id="action_id_'+key+'"><ul><li> <a class="action_mAf" id="custm_idLast_'+key+' " data-id="'+value.file_id+'" data-requested-id="'+value.reqid+'" data-isfav="'+mark_as_fav+'"  data-folder_id="'+folder_id+'" data-file="' + image_name + '">'+IconHTMl+' </a></li>'+ dropdwn_li +'<li><a class="CopYfileToFolder"><i class="far fa-copy"></i> Copy to Folder</a></li><li><a target="_blank" class="deletefolderfiles" data-folderid="'+value.id+'" data-imgnm= "' + image_name + '"><i class="fas fa-trash"></i> Delete</a></li></ul></div></div></li>';
                        }
                    });
                }
                }
            } 
            var ErRorHtml  ='<div class="main-conintainer"><div class="inner-html"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"><div class="inner-msg"><h3><strong>Folder is Empty</strong></h3></div></div></div>'; 
             if(files_html!="" ||  html!=""){
                 $('.all_request_files').html(files_html);
                 $('.all_request_file').html(html);
             }else{
                $('.all_request_file').html(ErRorHtml);
             }
         }
        });
  }
    $(document).on('click', '.projects_list', function () {
        $("#selected_folder").val("My Folders");
        $(".folder_id").val("");
        $(".customer_directory").attr("isloaded","0");
        var reqid = $(this).data('reqid');
        var title = $(this).data('title');
        var status = $(this).data('status');
        $('.customer_files').attr('data-id', reqid);
        $('.customer_files').attr('data-title', title);
        addbreadcrumb("<a class='one'>"+status +"</a>"+ '/' + "<a href='javascript:void(0);'>"+title+"</a>");
        $('.go_back_file').attr('data-url', status + '/');
        $('.go_back_file').attr('data-count', '2');
        $('.go_back_file').attr('data-sbfolder', '2');
        $('.go_back_file').prop('disabled', false);
        //$('.customer_designer_requests').css('display', 'block');
          getallfiles($(this)); 
        $('.requests_files').css('display', 'none');
        $('.requests_all_files').css('display', 'block');
        
        
    });

    function getallfiles(click) {
        //show_loader();
        var role = $(click).attr('data-role');
        var id = $(click).attr('data-id');
        var reqid = $(click).attr('data-reqid');
        var title = $(click).attr('data-title');
        $('.go_back_file').attr('data-url', 'projects/' + title + '/');
        $('.go_back_file').attr('data-count', '2');
        $('.go_back_file').attr('data-sbfolder', '3');
        $('.customer_designer_requests').css('display', 'none');
        $('.requests_files').css('display', 'none');
        $('.requests_all_files').css('display', 'block');
        if(title == null){
           var text = $(this).text(); 
           title = $.trim(text.replace("/", " "));
            
        }
        
       
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: baseUrl+"customer/Filemanagement/getAllRequestFiles",
            data: {"reqid": id, "type": "projects", "role": role, "reqid": reqid },
            success: function (data) {
             
                //hide_loader(); 
                        addbreadcrumb('<a href="javascript:void(0);"  class="2" onclick="active_trigger()"> projects /</a><a class="projects_list" data-reqid='+reqid+' data-title="'+title+'">'+ title + '</a>');
//                        addbreadcrumb('<a href="javascript:void(0);" class="projects_list" data-status="projects" data-reqid="7898"> /' + title + '</a>');

                $(".copy_directory").hide();
                var image = '';
                var Main_name,image1; 
                  image +='<div class="main_project">';

                    var conditoinal  = data.files[0].user_type;
                    if(conditoinal != null){
                     Main_name ='<div class="inner_structure"><div class="section-hddng"><h4>'+data.files[0].user_type+'</h4><span></span></div><div class="inner_data_'+data.files[0].user_type+'">';
                   } 
 
                    var allValues = array_values(data.files,$type ="user_type");
                    var CountFileTypes = allValues.reduce(function(prev, cur) {
                     prev[cur] = (prev[cur] || 0) + 1;
                     return prev;
                   }, []);
                $i = 0;
                $.each(data.files, function (key,value) {
                    $i++;
                    image1 ='';
                    var flname = files_array_toshow(value.file_name,$uploadAssestPath + value.request_id + '/' + value.file_name);
                    var url =  $uploadAssestPath + value.request_id + '/' + value.file_name;
                    var project_url =  baseUrl+'customer/request/project-info/'+value.request_id;
                    if(CountFileTypes[value.user_type] == $i){
                             image1 ='</div></div>';
                              
                        }

                        if(value.favStatus ==1){
                            IconHTMl =  '<i class="fas fa-heart active"></i> Remove from Favorite';
                        }
                        else{
                            IconHTMl =  '<i class="far fa-heart"></i> Mark as Favorite'; 
                        }
                    if(value.user_type == "customer"){
                    image += Main_name+'<li class="copyfolder_or_files elvn"><a class="viewBox" data-isfav="'+value.mark_as_fav+'" data-from = "'+url+'" data-id='+value.request_id+' data-file='+value.file_name+' data-from = "'+url+'" data-projid ="'+ value.id + '" > <img src="'+ flname + '"> <h3>'+value.file_name+'</h3></a><div class="dropdown"><div class="simple-option dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-h" data-id="#action_id_'+key+'"></i></div><div class="action_dropdown dropdown-menu" role="menu" id="action_id_'+key+'"><ul><li> <a class="action_mAf" id="custm_idLast_'+key+' " data-id="'+value.id+'" data-requested-id="'+value.request_id+'" data-isfav="'+value.mark_as_fav+'">'+IconHTMl+' </a></li><li class="download_image"> <a href="'+ url + '" download><img src="'+baseUrl+'public/assets/img/download-form.svg" class="img-responsive"> Download File </a></li><li><a class="CopYfileToFolder myiconCstm_copy"><i class="far fa-copy"></i> Copy to Folder</a></li><li><a target="_blank" href="'+project_url+'"><i class="fas fa-link"></i> Go to Project</a></li></ul></div></div></li>'+image1;    
                    }else if(value.user_type == "designer"){
                      image += Main_name+'<li class="copyfolder_or_files twlv"><a class="viewBox" data-from = "'+url+'" data-id='+value.request_id+' data-file='+value.file_name+'> <img src="'+ flname + '"><h3>'+value.file_name+'</h3></a><div class="dropdown"><div class="simple-option dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-h" data-id="#action_id_'+key+'"></i></div><div class="action_dropdown dropdown-menu" role="menu" id="action_id_'+key+'"><ul><li class="download_image"> <a href="'+ url + '" download><img src="'+baseUrl+'public/assets/img/download-form.svg" class="img-responsive"> Download File </a></li><li><a target="_blank" href="'+project_url+'"><i class="fas fa-link"></i> Go to Project</a></li></ul></div></div></li>'+image1;  
                    }else{
                        image += '<li class="copyfolder_or_files thirtn"><img src="'+ flname + '"><h3>'+value.file_name+'</h3></li>';
                    }
                    
                    Main_name = "";
                        if(CountFileTypes[value.user_type] == $i){
                      
                            var k = key;
                            if(key < data.files.length - 1){
                             k = Number(key+1);
                            }
                             image1 ='</div></div>';

                            conditoinal = data.files[k]['user_type'];
                            Main_name ='<div class="inner_structure"><div class="section-hddng"><h4>'+conditoinal+'</h4><span></span></div><div class="inner_data_'+conditoinal+'">';
                            $i = 0;
                        }
                    
                });
                var ErRorHtml  ='<div class="main-conintainer"><div class="inner-html"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"><div class="inner-msg"><h3><strong>Folder is Empty</strong></h3></div></div></div>'; 

                if(data.files.length > 0){
                    $('.all_request_files').html(image);
                }else{
                    //$('.all_request_files').html(ErRorHtml);
                }

                //$('.all_request_files').html(image);
            }
        });
    }
    $("body").click(function (e){
        if (e.target.className !== "copyfolder_or_files")
        {
            $(".copy_directory").hide();
        }
    });

    $(document).on('click','.requests_all_files a',function(){
        $('.go_back_file').attr('data-sbfolder','3');
        //alert(1);
    });
 var count = 0;
    $(document).on('click', '.copyfolder_or_files', function (e) {
          //e.stopPropagation();
         $(".copy_directory").show();
        var status = $(this).find('a').data('status');
        var id = $(this).find('a').data('reqid');
        var title = $(this).find('a').data('title');
        var foldid = $(this).find('a').data('foldid');
        var className = $(this).find('a').attr('class');
         if(foldid != null){
            var folderId = "data-foldid='"+foldid+"'"; 
        }else{
             folderId = id; 
        }
        
      
        $(".customer_designer_directory").find("a.active").attr("isloaded" ,"0"); 
        var idtrigger = $(".customer_designer_directory").find("a.active").attr("id");
         

       // addbreadcrumb("<a href='javascript:void(0);' onclick='active_trigger()' data-isb='1' class='three3'> Custom Folder</a><a href='javascript:void(0);' data-isb='1' class='three3C "+className+"' "+folderId+">/ "+title+'</a>');
        
        $('.go_back_file').attr('data-count',"2");
       // $('.folder_id').val(id);
        $("#fromid").val(id);
        $("#fromtype").val(status);
        $("#fromtitle").val(title);
        if(status == "custom"){
           $(".upload_btn_cst").html('<a href="javascript:void(0)" data-toggle="modal" data-target="#f_uploadfiles_custom" class="create_directory upldflsfrmcstom right-side-btn"><i class="fas fa-file-upload"></i><span>Files</span>'); 
        }
         //$(this).siblings().removeClass('selected_folder');
         //$(this).addClass('selected_folder');
    });

    $(document).on('click', '.request_file', function (e) {
        e.stopPropagation(); 
        var breadcrumb_name = $('.breadcrumb_name li').html();
        var reqid = $(this).data('reqid');
        var type = $(this).data('status');
        var title = $(this).data('title');
        var url = $(this).data('url');
        var user_id = $(this).data('user-id');
        var folderID = $(this).data('foldid');
         if($("#custom_structure").hasClass('active')){
            $('.folder_id').val(reqid);
            $(".selected_folder").val(title)
        }else{
             $(".folder_id").val("");
             $(".selected_folder").val("My folders")
        }
        
        $('.from').val('sub');
       
        $('.requests_files').css('display', 'none');
         $('.requests_all_files').css('display', 'block');

        $('.go_back_file').attr('data-count',"2");
        $('.go_back_file').attr('data-sbfolder',"2");
        $('.go_back_file').prop('disabled',false);
        $('.go_back_file').attr('data-url',breadcrumb_name);
        
        
        var data_isb = $(this).attr("data-isb");
        if(data_isb == "1" ){ 
            $(".customer_designer_directory").find("a.active").data("isloaded","0"); 
           $(this).nextAll().remove(); 
            var bUpdate =  $('.breadcrumb_name li').html();
            Ajax_for_getAllRequestFiles(reqid,type,title,user_id,bUpdate,folderID,'',data_isb);
        }else{ 
            Ajax_for_getAllRequestFiles(reqid,type,title,user_id,breadcrumb_name);  
        }
  
    });

 function Ajax_for_getAllRequestFiles(reqid,type,title,user_id,breadcrumb_name,folderID,issettb){
     
     $.ajax({
            type: "POST",
            dataType: 'json',
            url: baseUrl+"customer/Filemanagement/getAllRequestFiles",
            data: {"reqid": reqid, "type": type,"title":title,"user_id":user_id},

            success: function (data) { 
                 if(title !=null && issettb==null){
                    addbreadcrumb(breadcrumb_name+"<a href='javascript:void(0);' data-isb ='1' data-reqid='"+reqid+"' data-foldid='"+folderID+"' data-user-id='"+user_id+"' data-status="+type+" data-title='"+title+"' class='request_file 11'>/ "+title+'</a>');
                }else{
                    addbreadcrumb(breadcrumb_name);
  
                }
                //console.log('data -1',data);
                $(".copy_directory").hide();
                $('.all_request_files').html('');
                $('.all_request_file').html('');
                
                var image = '';
                var html = '';
                var count = 10;
                 
                if(data.folder_typ == 'brand'){
                 $(".upload_btn_cst").html('<a  class="upld_brand" data-toggle="modal" data-target="#f_uploadbrand" data-id="'+reqid+'"><i class="fas fa-upload upload_brand_icon"></i>Upload</a>');
                }
                if(data.folder !=null && data.folder.length > 0){
                    
                    // $(".upload_btn_cst").html('');
                $.each(data.folder, function (key, value) {
                   //addbreadcrumb("<a href='javascript:void(0);'>"+breadcrumb_name+"</a><a href='javascript:void(0);'>"+url+'</a>/'); 
                    var name = value.folder_name;
                    var dl ='<div class="dropdown"><div class="simple-option_del dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-v" data-id="#custom_action_id_'+key+'"></i></div><div class="custom_action_dropdown dropdown-menu" role="menu" id="custom_action_id_'+key+'"><ul><li><a class="rename"  data-foldid="'+value.id+'" data-id="#ur_id_'+key+'" data-foldid="'+value.id+'"  data-name="'+value.folder_name+'"><i class="fas fa-edit"></i>Rename</a></li><li><a class="DelSampLOption" data-foldid="'+value.id+'" data-name="'+value.folder_name+'"><i class="fas fa-trash-alt" ></i>Delete</a></li></ul></div></div>';
                    $uploadHtm=''; 
                    $str ="brands";
                    if(key == 0 && breadcrumb_name.indexOf($str) != -1){
                       $data_status = "brand_sub";
                       $dataparent =data.parent_id;
                       $uploadHtm = '<a  class="upld_brand" data-toggle="modal" data-target="#f_uploadbrand" data-id="'+$dataparent+'"><i class="fas fa-upload upload_brand_icon"></i>Upload</a>'; 
                    }else{
                        $dataparent = value.id;
                        $data_status = "custom";
                    }          
                    $("#caseForCustomFolder").val("3");
                    html += '<li class="copyfolder_or_files nin"><a href="JavaScript:void(0);" id="ur_id_'+key+'" class="request_file" data-url="'+name+'" data-status="'+$data_status+'" data-reqid=' + $dataparent + ' data-title="' + name + '"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg">' + name+'</a>'+dl+'</li>';
                });
                $('.requests_files').css('display', 'block');
                $('.requests_all_files').css('display', 'none');
                $('.all_request_file').html(html);
            }
            if(data.files !=null && data.files.length > 0 && data.folder_typ == 'brand'){
                
                var Main_name,image1; 
                  image +='<div class="main_brand">';

                    var conditoinal  = data.files[0].file_type;
                    
                    if(conditoinal == "logo_upload"){
                            conditoinal = "Brand Logo"; 
                           }else if(conditoinal == "materials_upload"){
                               conditoinal ="Marketing Material";
                           }else if(conditoinal == "additional_upload"){
                            conditoinal = "Additional Images"; 
                        }
                      
                    if(conditoinal != null){
                     Main_name ='<div class="inner_structure"><div class="section-hddng"><h4>'+conditoinal+'</h4><span></span></div><div class="inner_data_'+data.files[0].file_type+'">';
                   } 
 
                    var allValues = array_values(data.files,$type ="file_type");
                    var CountFileTypes = allValues.reduce(function(prev, cur) {
                     prev[cur] = (prev[cur] || 0) + 1;
                     return prev;
                   }, []);
                $i = 0;
                $.each(data.files, function (key, value) {
                    $i++;
                   
                     if(value.brand_id !=null){
                        image1 ='';
                        if(CountFileTypes[value.file_type] == $i){
                             image1 ='</div></div>';
                              
                        }
                        var flname = files_array_toshow(value.filename,$uploadbrandPath + value.brand_id + '/' + value.filename);
                        var url =  $uploadbrandPath + value.brand_id + '/' + value.filename;
                        var project_url =  baseUrl+'customer/add_brand_profile?editid='+value.brand_id;
                        var mark_as_fav = value.mark_as_fav;
                        if(mark_as_fav ==1){
                            IconHTMl =  '<i class="fas fa-heart active"></i> Remove from Favorite';
                        }
                        else{
                            IconHTMl =  '<i class="far fa-heart"></i> Mark as Favorite'; 
                        }
                        if(value.file_type == "logo_upload"){
                             
                            image +=Main_name+'<li class="copyfolder_or_files frtn" ><a href="JavaScript:void(0);" class="viewBox" data-status="files" data-from="'+url+'" data-id='+value.brand_id+' data-file='+value.filename+' data-reqid=' + value.id + ' data-title="' + value.filename + '" data-projid="'+ value.id+'" data-file='+value.filename+' data-role="brand"><img src="'+ flname + '"><h3> '+value.filename+'</h3></a><div class="dropdown"><div class="simple-option dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-h" data-id="#action_id_'+key+'"></i></div><div class="action_dropdown dropdown-menu" role="menu" id="action_id_'+key+'"><ul><li> <a class="action_mAf" id="custm_idLast_'+key+' " data-id="'+value.brand_id+'" data-isfav="'+mark_as_fav+'" data-role="brand" data-file="'+value.filename+'">'+IconHTMl+' </a></li><li class="download_image"> <a href="'+ url + '" download><img src="'+baseUrl+'public/assets/img/download-form.svg" class="img-responsive"> Download File </a></li><li><a target="_blank" href="'+project_url+'"><i class="fas fa-link"></i> Go to Brand</a></li></ul></div></div></li>'+image1;  
                            }  
                        else if(value.file_type == "materials_upload"){
                               
                            image += Main_name+'<li class="copyfolder_or_files fftn" ><a href="JavaScript:void(0);" class="viewBox" data-status="files" data-from="'+url+'" data-id='+value.brand_id+' data-file='+value.filename+' data-reqid=' + value.id + ' data-title="' + value.filename + '" data-projid="'+ value.id+'" data-file='+value.filename+' data-role="brand" ><img src="'+ flname + '"><h3> '+value.filename+'</h3></a><div class="dropdown"><div class="simple-option dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-h" data-id="#action_id_'+key+'"></i></div><div class="action_dropdown dropdown-menu" role="menu" id="action_id_'+key+'"><ul><li> <a class="action_mAf" id="custm_idLast_'+key+' " data-id="'+value.brand_id+'" data-isfav="'+mark_as_fav+'" data-role="brand" data-file="'+value.filename+'" >'+IconHTMl+' </a></li><li class="download_image"> <a href="'+ url + '" download><img src="'+baseUrl+'public/assets/img/download-form.svg" class="img-responsive"> Download File </a></li><li><a target="_blank" href="'+project_url+'"><i class="fas fa-link"></i> Go to Brand</a></li></ul></div></div></li>'+image1;
                        }else if(value.file_type == "additional_upload"){
                          
                            image += Main_name+'<li class="copyfolder_or_files sxtn" ><a href="JavaScript:void(0);" class="viewBox" data-status="files" data-from="'+url+'" data-id='+value.brand_id+' data-file='+value.filename+' data-reqid=' + value.id + ' data-title="' + value.filename + '" data-projid="'+ value.id+'" data-file='+value.filename+' data-role="brand"><img src="'+ flname + '"> <h3> '+value.filename+'</h3></a><div class="dropdown"><div class="simple-option dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-h" data-id="#action_id_'+key+'"></i></div><div class="action_dropdown dropdown-menu" role="menu" id="action_id_'+key+'"><ul><li> <a class="action_mAf" id="custm_idLast_'+key+' " data-id="'+value.brand_id+'" data-isfav="'+mark_as_fav+'" data-role="brand" data-file="'+value.filename+'">'+IconHTMl+' </a></li><li class="download_image"> <a href="'+ url + '" download><img src="'+baseUrl+'public/assets/img/download-form.svg" class="img-responsive"> Download File </a></li><li><a target="_blank" href="'+project_url+'"><i class="fas fa-link"></i> Go to Brand</a></li></ul></div></div></li>'+image1;
                        }
                     
                        Main_name = "";
                        if(CountFileTypes[value.file_type] == $i){
                      
                            var k = key;
                            var cls;
                            if(key < data.files.length - 1){
                             k = Number(key+1);
                            }
                             image1 ='</div></div>';

                            conditoinal = data.files[k]['file_type'];
                           if(conditoinal == "logo_upload"){
                            conditoinal = "Brand Logo"; 
                           }else if(conditoinal == "materials_upload"){
                            conditoinal ="Marketing Material";
                           }else if(conditoinal == "additional_upload"){
                            conditoinal = "Additional Images"; 
                            }
                           
                             
                            Main_name ='<div class="inner_structure"><div class="section-hddng"><h4>'+conditoinal+'</h4><span></span></div><div class="inner_data_'+data.files[k]['file_type']+'">';
                            $i = 0;
                        }
                        
                        
                        $('.go_back_file').attr('data-sbfolder',"3");
                    //image += '<li class="copyfolder_or_files" ><a href="JavaScript:void(0);" class="request_file" data-status="files" data-reqid=' + value.id + ' data-title="' + value.filename + '"><img src="'+ $assets_path + value.brand_id + '/' + value.filename + '"> <p> '+value.file_type+'</p></a></li>';
                    }else{
//                        console.log("else");
                       // $(".upload_btn_cst").html('');
                        addbreadcrumb(breadcrumb_name+"<a href='javascript:void(0);' class='request_file tw' data-status='files' data-reqid='"+ value.reqid +"'>/ "+title+'</a>');
                        var flname = files_array_toshow(value.image,$assets_path + value.reqid + '/' + value.image);
                        image += '<li class="copyfolder_or_files ten" ><a href="JavaScript:void(0);" class="request_file" data-status="files" data-reqid=' + value.reqid + ' data-title="' + value.img_name + '"><img src="'+ flname + '"></a></li>';
                    }
                    
                 });
                 image +='</div>';


                 $('.requests_all_files').css('display', 'block');
                 var ErRorHtml  ='<div class="main-conintainer"><div class="inner-html"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"><div class="inner-msg"><h3><strong>Folder is Empty</strong></h3></div></div></div>'; 
                 if(data.files.length > 0){

                    $('.all_request_files').html(image);
                }else{
                    $('.all_request_files').html(ErRorHtml);
                }
                 //$('.all_request_files').html(image);
                 

                 
             }
             else if(data.folder_typ == 'other'){
               // console.log("1"); 
                  image += "<div class='section-hddng'><h4>Files</h4><span></span></div>";
                 $.each(data.files, function (key, value) {
                     var path,id,image_nm,project_url,dropdwn_li,folder_id,mark_as_fav,flname,url;
                     if(value.reqid == 0){
                          path = $uploadcustom_files;
                          id = 0;
                          image_nm = value.img_name;
                          folder_id = value.folder_id;
                          mark_as_fav = value.mark_as_fav;
                          dropdwn_li = ""; 
                          flname = files_array_toshow(image_nm,path + value.folder_id + '/' + image_nm);
                          url =  path + value.folder_id + '/' + image_nm;
                      }else if(value.file_id == "0"){
                                 flname = files_array_toshow(value.img_name, baseUrl +"public/uploads/custom_files/"+ value.reqid + '/' + value.img_name);
                                 url = baseUrl +"public/uploads/custom_files/"+ value.reqid + '/' + value.img_name;
                                 project_url = baseUrl + 'customer/request/project-info/' + value.image;   
                                 dropdwn_li = ' ';
                                 folder_id = 0;
                                 image_name = value.img_name;
                                 mark_as_fav = value.mark_as_fav;
                      }else{
                          path = $uploadAssestPath;
                          image_nm = value.image;
                          id = value.reqid;
                          folder_id = 0;
                          mark_as_fav = value.markasfav;
                          project_url = baseUrl+'customer/request/project-info/'+value.reqid;
                          dropdwn_li = '<li><a target="_blank" href="'+project_url+'"><i class="fas fa-link"></i> Go to Project</a></li>';
                          flname = files_array_toshow(image_nm,path + value.reqid + '/' + image_nm);
                          url =  path + value.reqid + '/' + image_nm;
                     }
                     
                    var IconHTMl;
                    if(value.favStatus ==1){
                            IconHTMl =  '<i class="fas fa-heart active"></i> Remove from Favorite';
                        }
                        else{
                            IconHTMl =  '<i class="far fa-heart"></i> Mark as Favorite'; 
                        }
                    image += '<li class="copyfolder_or_files one-1"><a data-id="'+id+'" data-file="'+image_nm+'" class="viewBox" data-from = "'+url+'" data-projid ="'+ value.file_id + '"> <img src="'+ flname + '"><h3>'+image_nm+'</h3></a><div class="dropdown"><div class="simple-option dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-h" data-id="#action_id_'+key+'"></i></div><div class="action_dropdown dropdown-menu" role="menu" id="action_id_'+key+'"><ul><li> <a class="action_mAf" id="custm_idLast_'+key+' " data-id="'+value.file_id+'" data-requested-id="'+value.reqid+'" data-isfav="'+mark_as_fav+'" data-folder_id="'+folder_id+'" data-file="' + image_nm + '">'+IconHTMl+' </a></li><li class="download_image"> <a href="'+ url + '" download><img src="'+baseUrl+'public/assets/img/download-form.svg" class="img-responsive"> Download File </a></li>'+dropdwn_li+'<li><a class="CopYfileToFolder"><i class="far fa-copy"></i> Copy to Folder</a></li><li><a target="_blank" class="deletefolderfiles" data-folderid="'+value.id+'" data-imgnm= "' + image_nm + '"><i class="fas fa-trash"></i> Delete</a></li></ul></div></div></li>';
                 });
               
              var ErRorHtml  ='<div class="main-conintainer"><div class="inner-html"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"><div class="inner-msg"><h3><strong>Folder is Empty</strong></h3></div></div></div>'; 
                if(data.files.length > 0){
                    $('.requests_all_files').css('display', 'block');
                   $('.all_request_files').html(image);
                }else{ 
                 
                    $('.all_request_files').html(ErRorHtml);
                }
                  
//                $('.all_request_files').html(image);
             }

             else if(data.folder_typ == 'default' && title == 'Favorite'){
              
                 $.each(data.image, function (key, value) {
                     var IconHTMl,path,filename,projecturl,projectid,id,attr,attr1,markasfav,proid,dropdwnli;
                      
                     if(value.from_data == "brands"){
                         filename = value.filename;
                         path = $uploadbrandPath + value.brand_id + '/' + filename;
                         projecturl = baseUrl+'customer/add_brand_profile?editid='+value.brand_id;
                         projectid = 0;
                         dropdwnli='';
                         id = value.brand_id;
                         proid = value.brand_id;
                         attr = "data-role = 'brand'";
                         attr1 = "data-role = 'brand' data-file='"+filename+"'";
                         markasfav = value.mark_as_fav;
                         
                     }else if(value.from_data == "custom"){
                         filename = value.file_name;
                         path = $uploadcustom_files + value.folder_id + '/' + value.file_name;
                         projecturl = "";
                         projectid = 0;
                         id = value.folder_id;
                         dropdwnli='';
                         proid = "";
                         attr = "";
                         attr1 = "data-folder_id = '"+id+"' data-file='"+filename+"'";
                         markasfav = value.mark_as_fav;
                         
                     }else{
                         filename = value.file_name;
                         path = $uploadAssestPath + value.request_id + '/' + value.file_name;
                         projecturl = baseUrl+'customer/request/project-info/'+value.request_id;
                         projectid = value.request_id;
                         id = value.request_id;
                         proid = value.project_id;
                         dropdwnli ='<li><a target="_blank" href="'+project_url+'"><i class="fas fa-link"></i> Go to Draft</a></li>';
                         attr = "";
                         attr1 = "";
                         markasfav = value.favStatus;
                     }
                    var flname = files_array_toshow(filename,path);
                    var url =  path;
                    var project_url = projecturl;
                    
                    if(markasfav == 1){
                            IconHTMl =  '<i class="fas fa-heart active"></i> Remove from Favorite';
                        }
                        else{
                            IconHTMl =  '<i class="far fa-heart"></i> Mark as Favorite'; 
                        }
                    image += '<li class="copyfolder_or_files one"><a data-id="'+id+'" data-file="'+filename+'" data-projUrl= '+project_url+' data-requested-id="'+id+'" data-isfav="'+markasfav+'" class="viewBox" data-from = "'+url+'" data-projid ="'+ projectid + '" '+attr+'> <img src="'+ flname + '"><h3>'+filename+'</h3></a><div class="dropdown"><div class="simple-option dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-h" data-id="#action_id_'+key+'"></i></div><div class="action_dropdown dropdown-menu" role="menu" id="action_id_'+key+'"><ul><li> <a class="action_mAf" id="custm_idLast_'+key+' " data-id="'+proid+'" data-requested-id="'+projectid+'" data-isfav="'+markasfav+'" '+attr1+'>'+IconHTMl+' </a></li><li class="download_image"> <a href="'+ url + '" download><img src="'+baseUrl+'public/assets/img/download-form.svg" class="img-responsive"> Download File </a></li><li><a class="CopYfileToFolder"><i class="far fa-copy"></i> Copy to Folder</a></li>'+dropdwnli+'</ul></div></div></li>';
                });
                var ErRorHtml  ='<div class="main-conintainer"><div class="inner-html"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"><div class="inner-msg"><h3><strong>Folder is Empty</strong></h3></div></div></div>'; 
                if (data.image != null) {
                    if (data.image.length > 0) {
                        $('.all_request_files').html(image);
                    } else {
                        $('.all_request_files').html(ErRorHtml);
                    }
                } else {
                    $('.all_request_files').html(ErRorHtml);
                }
             }
             else{
                $.each(data.image, function (key, value) {
                    var flname = files_array_toshow(value.file_name,$uploadAssestPath + value.request_id + '/' + value.file_name);
                    var url =  $uploadAssestPath + value.request_id + '/' + value.file_name;
                    var project_url = baseUrl+'customer/request/project-info/'+value.request_id;
                    var IconHTMl;
                    if(value.favStatus ==1){
                            IconHTMl =  '<i class="fas fa-heart active"></i> Remove from Favorite';
                        }
                        else{
                            IconHTMl =  '<i class="far fa-heart"></i> Mark as Favorite'; 
                        }
                    image += '<li class="copyfolder_or_files one"><a data-id="'+value.request_id+'" data-file="'+value.file_name+'" data-projUrl= '+project_url+' data-requested-id="'+value.request_id+'" data-isfav="'+value.favStatus+'" class="viewBox" data-from = "'+url+'" data-projid ="'+ value.project_id + '"> <img src="'+ flname + '"><h3>'+value.file_name+'</h3></a><div class="dropdown"><div class="simple-option dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-h" data-id="#action_id_'+key+'"></i></div><div class="action_dropdown dropdown-menu" role="menu" id="action_id_'+key+'"><ul><li> <a class="action_mAf" id="custm_idLast_'+key+' " data-id="'+value.project_id+'" data-requested-id="'+value.request_id+'" data-isfav="'+value.favStatus+'">'+IconHTMl+' </a></li><li class="download_image"> <a href="'+ url + '" download><img src="'+baseUrl+'public/assets/img/download-form.svg" class="img-responsive"> Download File </a></li><li><a class="CopYfileToFolder"><i class="far fa-copy"></i> Copy to Folder</a></li><li><a target="_blank" href="'+project_url+'"><i class="fas fa-link"></i> Go to Draft</a></li></ul></div></div></li>';
                });
                var ErRorHtml  ='<div class="main-conintainer"><div class="inner-html"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"><div class="inner-msg"><h3><strong>Folder is Empty</strong></h3></div></div></div>'; 
                if (data.image != null) {
                    if (data.image.length > 0) {
                        $('.all_request_files').html(image);
                    } else {
                        $('.all_request_files').html(ErRorHtml);
                    }
                } else {
                    $('.all_request_files').html(ErRorHtml);
                }
            
            }
            }
        });
 }
    $(document).on('click', '.simple-option i,.simple-option_del i', function (e) {
        $this = $(this); 
        $post = $(".action_mAf");
          var user_id, projectID,isfav,urL,filename; 
          user_id = $this.parents("li").find("a.viewBox").attr("data-id");
          projectID = $this.parents("li").find("a.viewBox").attr("data-projid");
          urL = $this.parents("li").find("a.viewBox").attr("data-from");
          if(urL!=null){
          filename = urL.substring(urL.lastIndexOf('/')+1); 
            }
          isfav = $post.attr("data-isfav");
          
            $(".myiconCstm_copy").attr("data-projid",projectID);
            $(".myiconCstm_copy").attr("data-id",user_id);
            $(".myiconCstm_copy").attr("data-file",filename);
          
           if(isfav == "1"){
            $post.find("i").removeClass('far')
            $post.find("i").addClass('fas');
            $post[0].lastChild.nodeValue  =" Remove from Favorite";
            
          }else{
           // $this.parents("li").find("a.viewBox").attr("data-isfav","1");
            $post[0].lastChild.nodeValue  =" Mark as Favorite";
            $post.find("i").addClass('far');
            $post.find("i").removeClass('fas');
          }
         parent_name = $('.breadcrumb_name li').text();
         if(parent_name == "Favorite"){
           $('.action_mAf').find('i.active').addClass('fas fa-heart');
         }
    });

    $(document).on('click', '.DelSampLOption', function (e) {
        $this = $(this);
         $folderID=  $(this).data('foldid');
         $folderName=  $(this).data('name');
//         getting folder sturute 
                //show_loader();
                   $.ajax({
                        type: "POST",
                        url: baseUrl+"customer/Filemanagement/checkingForChildOrSubchild",
                        data: {"folderid": $folderID},
                        success: function (folRes) {
                            //hide_loader();
                            if(folRes != ""){
                                var folder_exist ="These Sub Folder '"+folRes+"' Will also Delete with this action";
                            }else{
                                folder_exist ="";
                            }
                            swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this "+$folderName+" file/Folder! "+folder_exist+" ",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if(willDelete){
                      //show_loader();
                    $.ajax({
                        type: "POST",
                        url: baseUrl+"customer/Filemanagement/Custom_folder_delete",
                        data: {"folderid": $folderID,"confirmDelete":1},

                        success: function (res) {
                            //hide_loader();
                            if(res == 1){
                                swal(""+$folderName+" folder has been deleted!", {
                                      icon: "success",
                                    }).then(function(){
                                        $this.parents('li').fadeOut();
                                    });
                            }

                            }
                        });       
                  } else {
                    swal(""+$folderName+" folder is safe!");
                  }
                });

                            }
                        }); 
                 
// ending
           
    });

  $(document).on('click','.CopYfileToFolder',function(e){
      //$(".filetree").hide(); 
      $this = $(this); 
        var orignal_file,request_id,projid;
        $(".breadCrumCopYFolder a").trigger("click");
        $('#CopYfileToFolder').modal('show');
        orignal_file= $(this).closest('.copyfolder_or_files').find('a').data('file');
        request_id= $(this).closest('.copyfolder_or_files').find('a').data('id');
        projid= $(this).closest('.copyfolder_or_files').find('a').data('projid');  
        
        if(orignal_file ==null && request_id ==null && projid==null ){
            orignal_file= $this.attr('data-file');
            request_id= $this.attr('data-id');
            projid= $this.attr('data-projid'); 
        }
       
        $('#original_file').val(orignal_file);
        $('#req_id').val(request_id);
        $('#file_id').val(projid);
    });

    // $(document).on('click','.CoPyOrMove',function(e){
        $('.CoPyOrMove').click(function(){
        $this = $(this); 
        var orignal_file,request_id,projid;
        orignal_file= $this.data('file');
        request_id= $this.data('id');
        projid= $this.data('projid');
        $('#CopYfileToFolder').modal('show');
        $('#original_file').val(orignal_file);
        $('#req_id').val(request_id);
        $('#file_id').val(projid);
        
    });

    $('#copy_file').click(function(){
    var original_file,folder_id,file_id,file_name,copy_folder_url,req_id;
        original_file  = $('#original_file').val();
        req_id   = $('#req_id').val();
        file_id     = $('#file_id').val(); 
        if(original_file.length == 0 && req_id.length == 0 && file_id.length == 0){
           original_file =  $(".myiconCstm_copy").attr("data-file");
           file_id = $(".myiconCstm_copy").attr("data-projid");
           req_id = $(".myiconCstm_copy").attr("data-id");
       }
         
       folder_id     = $('.folder_id').val();
       if(folder_id !=""){
           folder_id     = $('.folder_id').val();
       }else{
           folder_id     = "0";
       }
       copy_folder_url   = $('#copy_folder_path').val();
       
       if(copy_folder_url !=""){
            copy_folder_url   = $('#copy_folder_path').val();
       }else{
            copy_folder_url   = "My Folders/";
       }
       
       //show_loader();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: baseUrl+"customer/Filemanagement/copy_file_to_folder",
            data: {"req_id": req_id,"folder_id":folder_id,"file_name":original_file , "file_id":file_id},
            success: function (data) {
                //hide_loader();
                if(data == "1"){
                    swal("File is copied!"," "+original_file+" successfully copied to "+copy_folder_url+"", "success").then(function(){
                        $("#CopYfileToFolder").modal("hide");
                    });
                }else{
                    swal("File is not copied!", ""+original_file+" already  exist in  "+copy_folder_url+"", "error").then(function(){
                        $("#CopYfileToFolder").modal("hide");
                    });
                   
                }
            }
        });

    });
    $(document).on('click','.create_folder',function(e){
        setTimeout(function(){
        $('#CopYfileToFolder').modal('hide');
        },200);

        setTimeout(function(){
           $('#DirectorySave').modal('show');
        },300);
        
    });

   
    $(document).on("click","#DirectorySaveClose",function(){
    //    $(".close").click(function(){
         // alert(2);   
         $("#CopYfileToFolder").modal("show");
      });
    $('.list-header-blog li.active').click(function(){
        $("#default_folder").trigger('click');
    }); 
 
    function goback(e) {
        var bread_lits =  $('.breadcrumb_name li').text();
        $str ="custom_structure";
          if(bread_lits.indexOf($str) != -1){
          var sbfolder = $(e).attr('data-sbfolder');
         }else{
           var count = $(e).attr('data-count');
         }
        var url = $(e).attr('data-url');
        addbreadcrumb("<a href='javascript:void(0);'>" +bread_lits+ "</a>");
        if (count == "1") {
            $(".breadcrumb_name").text(bread_lits); 
            $(e).prop('disabled', true);
        } else if (count == "2") {
              var root = $('.customer_designer_directory a.active').text();  
              addbreadcrumb("<a  onclick='active_trigger()' class='8ignht'>"+root+'</a>');
              active_trigger(); 
            $('.requests_files').css('display', 'block');
            $('.requests_all_files').css('display', 'none');
            $('.customer_designer_requests').css('display', 'none');
            $(e).attr('data-count', '1');
        } else if (count == "3") {
             if(sbfolder == 3){
               $('.requests_files').css('display','block'); 
               $('.requests_all_files').css('display', 'none');
           }else{

            $('.customer_designer_requests').css('display', 'block');
           }
            $('.requests_all_files').css('display', 'none');
            $(e).attr('data-count', '2');
        }else if(sbfolder == 1){
            $('.customer_designer_directory').find('a.active').trigger('click');
            $(e).prop('disabled', true);
        }
        else if(sbfolder == 2){
           $(e).prop('disabled', false);
            $('.requests_files').css('display','block');
            $('.requests_all_files').css('display','none');
            $(e).attr('data-sbfolder',"1");
        }
        else if(sbfolder == 3){
         $(e).attr('data-sbfolder',"2");
         $(e).prop('disabled', false);
           
        }else if(count == 0){
         $(e).prop('disabled', true);
           
        }
         
    }

     function addbreadcrumb(url) {
        $(".breadcrumb_name").show();
        $(".breadcrumb_name ").html("<li class='menu_list'>" + url + "</li>");
        var text = $(".breadcrumb_name li a").text(); 
        $('#current_active_folder').val($(url).text());
        $('.folder_nest').val("My Folders/");
    //   file upload path set from here
            $str ="My Folders";
              if(text.indexOf($str) != -1){ 
                  $(".FolderPathForUpload span").text(text); 
                }else{
                    $(".FolderPathForUpload span").text($str); 
                }
       
       // $('.folder_id').val("0");
     }


    $(document).on('click', '.folder_nest', function (e) {
        $("#copy_file").attr("disabled",true);
        var dataid= $(this).data('id');  
        $("#copy_file").addClass("disable"); 
        $(this).nextAll().remove(); 
        var main_folderli;
        //show_loader();
         $.ajax({   

                url: baseUrl+"customer/Filemanagement/getting_main_folder",
                dataType: 'json',
                type: "post",
                data: {'ddd':"dd"},
                success: function(data) 
                {   
                    main_folderli=''; 
                $(".folder_id").val("0");
                $("#copy_folder_path").val("My Folder");
                    if(data != null){
                    $.each(data, function (key, value) {
                      
                        main_folderli +='<li class="tree-title get_sub_folder_main" data-id="'+value.folder_id+'" data-url="My Folders/'+value.folder_name+'/"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg">'+value.folder_name+'<div class="right-arrow" title="Go to '+value.folder_name+' folder"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M7.33 24l-2.83-2.829 9.339-9.175-9.339-9.167 2.83-2.829 12.17 11.996z"/></svg></div></li><ul class="tree projects_tree" id="'+dataid+'_under_a'+key+'" style="display: none;"></ul>';
                     });      
                        $("#copy_file_mainTree").html(main_folderli);
                    }else{
                        $("#copy_file_mainTree").html(ErRorHtml); 
                     }
                 } 
      });
        $(".filetree").slideDown();
        $('.filetree').css('display', 'block');
        $("#"+dataid+'_filetree').slideDown();
        $("#"+dataid+'_filetree').css('display', 'block');
    });

    $(document).on('click', '.tree-title_sub', function (e) {
        var dataID = $(this).attr('data-id');
        var dataName = $(this).attr('data-name');
        $('#req_' + dataID).html('<li class="cust" data-url="/projects/' + dataName + '/Customer">Customer</li><li class="designer" data-url="projects/' + dataName + '/Designer">Designer</li>');
    });

    $(document).on('click', '.create_folderst', function (e) {
        e.preventDefault();
        var folder_name = $('.folder_name').val();
        var folder_nest = $('.folder_nest').val();
        var folder_id = $('.folder_id').val();
        var under = $('#selected_folder').val();
        if(folder_name!=""){
             $("#errorSpan").hide();
             //show_loader();
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: baseUrl+"customer/Filemanagement/createFolder",
            data: {"folder_name": folder_name, "folder_nest": folder_nest, "folder_id": folder_id},
            success: function (data) {
                //hide_loader(); 
            swal("Folder is successfully created !", ""+folder_name+" successfully created  inside "+under+"", "success").then(function(){
                       $('.menu_list a:last-child').trigger('click');
                    });
                if (folder_nest == '/') {
                    $('.customer_designer_directory').append("<a href='JavaScript:void(0);' class='customer_directory' data-foldid=" + data.folder_id + " data-role=" + data.folder_name + "><img src='"+$assets_path+"img/file-managment/defult-folder-icon.svg'>" + data.folder_name + "</a>");
                } else {
                    $('.all_request_file').append("<li class='copyfolder_or_files two'><a href='JavaScript:void(0);' class='request_file' data-url='"+folder_name+"' data-status='' data-reqid=" + data.folder_id + " data-title='"+ data.folder_name +"' ><img src='"+$assets_path+"img/file-managment/defult-folder-icon.svg'>" + data.folder_name + "</a></li>");
                }
                $('#DirectorySave').hide();
                $('.modal-backdrop').remove();
                $('.folder_name').val('');
            }
        });
    }else{

         $("#errorSpan").text('Please Enter Folder Name').css('color','red');
    }

    });

    /******copy files********/
    $("#copy_form").submit(function () {
        var formdata = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: baseUrl+"customer/File_management/copy_filesor_folder",
            data: {formdata: formdata},
            success: function (data) {
            }
        });
        return false;
    });

/* view files */
$(document).on('click','.viewBox',function(e) {
    var breadcrumb_name = $('.breadcrumb_name li').html();
    $this =  $(this); 
            var user_id,file,url,projid,projUrl,isfav,role,folder_id;
            user_id     =   $this.attr('data-id');
            file        =   $this.attr('data-file');
            url         =   $this.attr('data-from');
            projid      =   $this.attr('data-projid');
            projUrl     =   $this.attr('data-projurl');
            isfav       =   $this.attr('data-isfav');
            role       =    $this.attr('data-role');
            folder_id  =    $this.next('.dropdown').find('.action_mAf').data('folder_id');
            if(role=="brand"){
                 $(".myiconCstm_copy").hide();
            }
            if(projid == "0"){
                $(".myiconCstm_gtD").hide(); 
            }else{
                $(".myiconCstm_gtD").show();
            }
           
                    
                    ajaxFor_openFileType(url,file,user_id,breadcrumb_name);
                    var filename = url.substring(url.lastIndexOf('/')+1);
                    
                     if(role=="brand"){
                          $("#myiconCstm_mAf").attr("data-id",user_id);
                        // $("#myiconCstm_mAf").attr("data-requested-id",user_id);
                     }else{
                      $("#myiconCstm_mAf").attr("data-id",projid); 
                      $("#myiconCstm_mAf").attr("data-requested-id",user_id);
                     }
                   
                    $("#myiconCstm_mAf").attr("data-folder_id",folder_id);
                    $("#myiconCstm_mAf").attr("data-role",role);
                    $("#myiconCstm_mAf").attr("data-file",file);
                    $(".myiconCstm_copy").attr("data-projid",projid);
                   
                    $(".myiconCstm_copy").attr("data-id",user_id);
                    $(".myiconCstm_copy").attr("data-file",filename);
                    $("#myiconCstm_dwnld").attr("href",url);
                    var gtDurl = baseUrl+"customer/request/project-info/"+user_id;
                    $(".myiconCstm_gtD").attr("href",gtDurl);
                    checkForFavOrUnFav(projid,user_id,filename,role);
                    $(".myiconCstm_gtD").attr("href",projUrl);
                    
                    
          
        });
        
        
/*  open file type function start from here */
function ajaxFor_openFileType(url=null,file=null,user_id=null,breadcrumb_name=null){
    $.ajax({
              type: 'POST',
              url: baseUrl+"customer/Request/open_file_type/",
              data: {url: url,url: url,file:file,user_id:user_id},
              success:function(result){
                  //hide_loader();
                addbreadcrumb(breadcrumb_name);
                if(result != 0){
                    $("#openfileType_content").html(result);
                    $("#openfileType").modal("show");
                 // $.fancybox.open(result);       
                } 
          }
          });
}
/*  open file type function End here */

/* Mark As Favorout */
$(document).on('click','.action_mAf',function(e) {
         e.stopPropagation();
        $post = $(this); 
        var id,project_id,user_id,isfav,folder_id,orignal_file,role,parent_name,brlisting; 
        id= $post.attr('id');
        project_id= $post.attr('data-id');
        user_id= $post.attr('data-requested-id');
        folder_id= $post.attr('data-folder_id');
        isfav= $post.attr('data-isfav');
        orignal_file= $post.attr('data-file');
        role= $post.attr('data-role');
        parent_name = $('#current_active_folder').val();
        brlisting = $(".menu_list").html(); 
         $str ="Favorite";
          if(parent_name.indexOf($str) != -1){ 
            $post.parents('li').fadeOut();  
         }
         
        if(isfav == '1'){
            $post.attr('data-isfav','0');

            $.ajax({
                        url: baseUrl+"customer/Request/mark_as_favorite",
                        type: "post",
                        data: {'projectID':project_id,'user_id':user_id,'mark':'unfav','folder_id':folder_id,'filename':orignal_file,'role':role},
                        success: function(res) {
                            addbreadcrumb(brlisting);
                                $post.removeClass('fav-active');
                                $post.find('i.active').removeClass('active');
                                $post.find('i').removeClass('fas fa-heart');
                                $post.find('i').addClass('far fa-heart');
                                $(".action_mAf").find("i").removeClass('fas fa-heart');
                                $(".action_mAf").find("i").addClass('far fa-heart');
                                $post[0].lastChild.nodeValue  =" Mark as Favorite";
                        }
                    });
         }else{
            $post.attr('data-isfav','1');  
                    $.ajax({
                        url: baseUrl+"customer/Request/mark_as_favorite",
                        type: "post",
                        data: {'projectID':project_id,'user_id':user_id,'mark':'fav','folder_id':folder_id,'filename':orignal_file,'role':role},
                        success: function(res) { 
                              addbreadcrumb(brlisting);
                            $post.addClass('fav-active');
                            $post.find('i').addClass('active');
                            $post.find('i').removeClass('far fa-heart');
                            $post.find('i').addClass('fas fa-heart');
                            $(".action_mAf").find("i").removeClass('far fa-heart');
                            $(".action_mAf").find("i").addClass('fas fa-heart');
                            $post[0].lastChild.nodeValue = "   Remove from Favorite";
                            
                        }
                    });
        }            
});

$('.MarkasFavClick , .click ').click(function() {
        var id,project_id,user_id; 
        $this = $(this);
        id= $this.attr('id');
        project_id= $this.attr('data-id');
        user_id= $this.attr('data-requested-id');
        if($this.hasClass('active')){
            $.ajax({
                        url: baseUrl+"customer/Request/mark_as_favorite",
                        type: "post",
                        data: {'projectID':project_id,'user_id':user_id,'mark':'unfav'},
                        success: function(res) {
                                    $('.tooltiptext').text('Mark as favorite');
                                    $('#'+id).removeClass('active');
                                setTimeout(function() {
                                    $('#'+id).removeClass('active-2');
                                }, 30);
                                    $('#'+id).removeClass('active-3');
    
                                setTimeout(function() {
                                    $('#'+id+' span').removeClass('fas');
                                    $('#'+id+' span').addClass('far');
                                }, 15);
        
                        }
                    });
         }else{
                    $.ajax({
                        url: baseUrl+"customer/Request/mark_as_favorite",
                        type: "post",
                        data: {'projectID':project_id,'user_id':user_id,'mark':'fav'},
                        success: function(res) {

                             $('.tooltiptext').text('Marked as favorite ');
                             setTimeout(function() {
                                $('.tooltiptext').text('Already marked');
                                }, 2000);
                            $('#'+id).addClass('active');
                            $('#'+id).addClass('active-2');
                            setTimeout(function() {
                                $('#'+id+' span').addClass('fas');
                                $('#'+id+' span').removeClass('far');
                            }, 150);
                            setTimeout(function() {
                               $('#'+id).addClass('active-3');
                            }, 150);
                            $('#'+id+' .info').addClass('info-tog');
                            setTimeout(function(){
                                $('#'+id+' .info').removeClass('info-tog');
                            },1000);
                        }
                    });
        }            
});

/* rename and delete folder */

$(document).on("click",".rename",function(){
        $this =  $(this);
        $folderID=  $(this).data('foldid');
        $folderName=  $(this).data('name');
         $(".swal-content__input").val($folderName);
    swal("Enter your folder name ",{
        content: {
          element: "input",
          attributes: {
            value: $folderName,
            type: "text",
            id:"rename_text"
             
             
          },
        },
      }).then((value) => {
        if(value !=null && value!="" ){
           
            $(".customer_designer_directory a.active").attr("isloaded","0"); 
            $.ajax({
                        url: baseUrl+"customer/Filemanagement/rename_files",
                        type: "post",
                        data: {'folderID':$folderID,'newName':value},
                        success: function(res) {
                            if(res==1){
                                   swal("Folder has been renamed successfully !", ""+value+" is successfully updated", "success").then(function(){
                                    //$(".customer_designer_directory a.active").trigger("click"); 
                                    var currentID =  $this.attr('data-id');
                                    var design = $(currentID).find("div").children("p").text();
                                    $(currentID).html('<img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"><div class="folder_details default_filename"><h2>'+value+'</h2><p class="in-des">'+design+'</p></div>'); 
                                     });
                            }
                        
                        }
                    });
        
    } 
    });
});
         
function active_trigger(){
    $isloaded =  $(".customer_designer_directory").find("a.active").attr("data-isloaded");
    if($isloaded !="1"){
     $(".customer_designer_directory").find("a.active").trigger("click").attr("isloaded","0");
     $url =$(".customer_designer_directory").find("a.active").text();
     addbreadcrumb("<a onclick='active_trigger()' class='active_triger'>"+$url+"</a>"); 
    }else {
         $(".customer_designer_directory").find("a.active").trigger("click").attr("isloaded","0");
    }
    
}
function files_array_toshow(file,path){
    if(file !=null && path !=null){
    var exten = file.split('.').pop();
    var file_ext = exten.toLowerCase();
    var imgext = ['jpg', 'jpeg', 'png', 'gif'];
    var imgnm = '';
    if($.inArray(file_ext,imgext) !== -1){
        imgnm = path;
    }else{
        imgnm = $assets_path+'img/default-img/defult-file-demo.svg';
    }
    return imgnm;
}
}

 $(".customer_directory").click(function(){
   if ( $(this).is("#custom_structure") ){
                         
                       $(".go_back_file").addClass("hide");
                       $(".go_back_file_cst").css("display","block");
                    }else{
                          
                         $(".go_back_file_cst").css("display","none");
                         $(".go_back_file").removeClass("hide");
                        
                    } 
 });
 
// $(docuemnt).on("click",".go_back_file_cst",function(){
$(".go_back_file_cst").click(function(){
    var find_length = $(".breadcrumb_name li").children();  
    var length_a = find_length.length;
    var last =  length_a -1 ; 
    var selector = $( ".breadcrumb_name li a:nth-child("+last+")" );
    $(document).find(selector).trigger("click");
     
 });
 
 $(document).on("click",".get_sub_folder_main,.get_sub_folder ",function(){
     
    $(this).addClass("mlactive"); 
    $("#copy_file").removeAttr("disabled"); 
    $("#copy_file").removeClass("disable");
    var folder_path = $(this).attr("data-url"); 
    var folderId = $(this).data("id");
    $('.folder_id').val(folderId);
    $('#copy_folder_path').val(folder_path);

 });

$(document).on("click",".right-arrow svg",function(e){
     e.stopPropagation();
     var click = $(this).parents("li");
     
     $(".get_sub_folder_main").not($(this)).removeClass("mlactive");
   //  $(this).parent("li").addClass("mlactive"); 
     var folderId = click.data("id");
     var popup =  click.data("popup");
     var idLoaded =  click.attr("data-isloaded");
     var data_uid = click.data("uid");
     var url = click.attr('data-url');      
     var text = click.text();     

     $('.folder_id').val(folderId);
     $('.projects_tree').not(data_uid).hide();
     
      if(idLoaded != "1")
      {
         
       Add_path_copy(url); 
       get_popupFolder_Ajax(click,popup); 
       }else{
        //click.effect("shake");
            $(this).addClass("mlactive");
            Add_path_copy(url); 
            $(data_uid).toggle("slide");
            $(this).attr("data-isloaded", "0");
       }
    });
    $(document).on("click",".brclClick",function(){ 
        $("#copy_file").attr("disabled",true);
        $("#copy_file").addClass("disable"); 
        get_popupFolder_Ajax($(this),"","brl"); 
    });
    function get_popupFolder_Ajax(click,popup,from=""){
        $("#copy_file").attr("disabled",true);
        $("#copy_file").addClass("disable");
        
        $(".get_sub_folder").not(click).removeClass("ulactive");
        var folderId = click.data("id");
         
        var data_uid = click.data("uid");
        var popup =  click.data("popup");
        var idLoaded =  click.attr("data-isloaded");
        var url = click.attr('data-url');
        var myTExt = click.text();
          
      
        $('.folder_id').val(folderId);
         
          
        if(idLoaded != "1"){ 
        var ErRorHtml  ='<div class="main-conintainer"><div class="inner-html"><img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"><div class="inner-msg"><h3><strong>Folder is Empty</strong></h3></div></div></div>'; 

                   $.ajax({
                            url: baseUrl+"customer/Filemanagement/getting_folder_dir",
                            dataType: 'json',
                            type: "post",
                            data: {'folderID':folderId},
                            success: function(data) 
                            {

                                 
                                    if(from != "brl"){
                                        // click.effect("shake");
                                         click.attr("data-isloaded", "1");
                                          
                                          addBreadCrubm_url("<a class='brclClick e' data-id="+folderId+" data-url='"+url+"'>/ "+$.trim(myTExt)+"</a>");
                                        
                                   }else{
                                      
                                        click.attr("data-isloaded", "0");
                                        click.nextAll().remove();
                                   }

                              
                                var folders ="";
                                if(data.folder != ""){
                             
                               $("#eRroSpan").hide().text(""); 
                                $.each(data.folder, function (key, value) {
                                    
                                         Add_path_copy(url);
                                      click.addClass("ulactive"); 
//                                      <ul id ="'+popup+value.id+'" class="under_sb_folder"></ul>
                                    folders +='<li class="get_sub_folder" data-id="'+value.id+'"   data-url="'+url+value.folder_name+'/"> <img src="'+$assets_path+'img/file-managment/defult-folder-icon.svg"> '+value.folder_name+' <div class="right-arrow"  title="Go to '+value.folder_name+' folder"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M7.33 24l-2.83-2.829 9.339-9.175-9.339-9.167 2.83-2.829 12.17 11.996z"/></svg></div></li>'; 
                                  });  
                                  $("#copy_file_mainTree").html(folders);
                                   
//                                  $(data_uid).hide().html(folders).fadeIn(300);
                                   
                                }else{
                                    $("#copy_file_mainTree").html(ErRorHtml);
                                    $("#copy_file").removeAttr("disabled");
                                    $("#copy_file").removeClass("disable");
                                     click.addClass("ulactive");
                                     Add_path_copy(url); 
                                     click.attr("data-isloaded", "1");      
                                     $(data_uid).addClass("hide"); 
                                    
                                } 
                               
                            }  
                        });
     
                    }else{ 
                         
                        //hide_loader(); 
//                        click.effect("shake");
                        click.addClass("ulactive"); 
                        click.addClass("mlactive");
                        Add_path_copy(url); 
                        $(data_uid).toggle("slide");
                    }
    }


        function show_loader(){
            $(".ajax_loader").show();
        }
        function hide_loader(){
            $(".ajax_loader").hide();
        }
        $(".create_directory").click(function(){
            $(".get_sub_folder").attr("data-isloaded","0");
        });
        function addBreadCrubm_url(url)
        { 
            $(".breadCrumCopYFolder").append(url);
        }
        function Add_path_copy(url,closed="",id=""){ 
            if(url !='/'){
            $('.folder_nest').val(url);     
             $('#copy_folder_path').val(url);   
           } 
       }
       
       $(".creat_btn_css").click(function(){
           $("#errorSpan").text("");
           $(".filetree").hide(); 
       });
       
  $(document).on('click', '.deletefolderfiles', function () {
      $this = $(this);
      var folderName = $this.data("imgnm");
// $(".deletefolderfiles").click(function(){
     var folderid = $(this).data("folderid");
     
         swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this " + folderName + " file!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
    .then((willDelete) => {
      if (willDelete) {
          $.ajax({
                    url: baseUrl+"customer/Filemanagement/deletefolderfiles",
                    dataType: 'json',
                    type: "post",
                    data: {'id':folderid},
                    success: function(data) {
                        swal("" + folderName + " file has been deleted!", {
                            icon: "success",
                        }).then(function(){
                           
                          $this.parents('li').fadeOut(); 
                        });

                    }  
             });
    
                } else {
                  swal("" + folderName + " file is safe!");
                }
    });
 });


  function checkForFavOrUnFav(dataId,reqid,filename,role){ 
            $("#project-pageinfo-id").attr("data-id",dataId);
            $.ajax({
           url: baseUrl+"customer/Filemanagement/checkForFavOrUnFav",
           type: "post",
           data: {'projectID':dataId,'user_id':reqid,'filename':filename,'role':role},
           success: function(res) {
               if(res == 1){
                   $("#people_share").data("proid",reqid);
                   $("#people_share").data("draftid",dataId);
                   trigger_sharing(dataId);
                   $('.tooltiptext').text('Marked as favorite ');
                setTimeout(function() {
                   $('.tooltiptext').text('Already marked');
                   }, 2000);
               $('#MarkasFavClick_'+dataId).addClass('active');
               $('#MarkasFavClick_'+dataId).addClass('active-2');
               setTimeout(function() {
                   $('#MarkasFavClick_'+dataId+' span').addClass('fas');
                   $('#myiconCstm_mAf i').addClass('fas');
                   $('#MarkasFavClick_'+dataId+' span').removeClass('far');
                   $('#myiconCstm_mAf i').removeClass('far');
                   $('#myiconCstm_mAf').attr('data-isfav',"1");
               }, 150);
               setTimeout(function() {
                  $('#MarkasFavClick_'+dataId).addClass('active-3');
               }, 150);
   
            }else{   
                   $("#people_share").data("proid",reqid);
                   $("#people_share").data("draftid",dataId);
                   trigger_sharing(dataId);
                $('.tooltiptext').text('Mark as favorite');
               $('#MarkasFavClick_'+dataId).removeClass('active');
               $('#MarkasFavClick_'+dataId).removeClass('active-2');
               $('#MarkasFavClick_'+dataId).removeClass('active-3');
               $('#MarkasFavClick_'+dataId+' span').removeClass('fas');
               $('#myiconCstm_mAf i').removeClass('fas');
               $('#MarkasFavClick_'+dataId+' span').addClass('far');
               $('#myiconCstm_mAf i').addClass('far');
               $('#myiconCstm_mAf').attr('data-isfav',"0");
   
            }
        }
       });
   
       }

        function trigger_sharing(draftid=""){
           if($("#people_share").parent("li").hasClass("active")){
               $("#people_share").trigger("click");
               $("#public_"+draftid).css("display","block"); 
            }
       }


/* update work on filemanagement 26-sep-19 */
/* search functionality start from here  */
            $("#fm_searchBox").bind('keyup',function(){
                
                $this = $(this); 
                var search_key,fileurl;
                search_key =  $this.val(); 
                 
                var serachHtml= '';
                $("#srch_spinner").show();
                if($.trim(search_key) !=""){
                    $(".search_result").show();

                  setTimeout(function(){  

                     $.ajax({
                                url: baseUrl+"customer/Filemanagement/search_fm",
                                dataType: 'json',
                                type: "post",
                                data: {'search_keyword':search_key},
                                success: function(data) 
                                {
                                    
                                    $("#srch_spinner").hide();
                                    if(data.fileName!="" || data.folderName!="" || data.fileName_ffs !=""){
                                        $(".search_result").show();
                                        $.each(data.folderName,function(key,value){
                                        if(value.folder_name !=""){
                                             serachHtml +='<li class="result_Folderlisting"><a class="afoldername" data-foldid="'+value.id+'" data-parentid="'+value.parent_id+'" data-type="'+value.type+'"><i class="fa fa-folder" aria-hidden="true"></i> '+value.folder_name+'</a></li>';
                                        }
                                     });
                                      
                                      $.each(data.fileName_ffs ,function(key,value){
                                         
                                        if(value.reqid!=0){
                                             fileurl = $uploadAssestPath+value.reqid+"/"+value.file_name;
                                         }else{
                                             fileurl = $uploadcustom_files+value.id+"/"+value.file_name;
                                         }
                                         // fileurl = baseUrl+"public/uploads/custom_files/"+value.id+"/"+value.file_name;
                                        

                                        if(value.file_name !=""){
                                            serachHtml +='<li class="result_Filelisting"><img src="'+fileurl+'"><a class="afile_name">'+value.file_name+'</a></li>';
                                        }
                                     });
                                   
                                    $.each(data.fileName,function(key,value){            
                                        //fileurl = baseUrl+"public/uploads/requests/"+value.request_id+"/"+value.file_name;
                                        fileurl = $uploadAssestPath+value.request_id+"/"+value.file_name;

                                        if(value.file_name !=""){
                                            serachHtml +='<li class="result_Filelisting"><img src="'+fileurl+'"><a class="afile_name">'+value.file_name+'</a></li>';

                                        }
                                        
                                      });
                                  }else{

                                serachHtml +='None of your files or folders matched this search.';       
                                //$(".search_result").hide();
                               }
                               $("#serach_resultListing").html(serachHtml);

                            }

                        });
                     },1000);
                 }else{
                    $(".search_result").hide();
                    $("#srch_spinner").hide();
                 }

            });
/* search functionality ends here  */
/* update work end  */



$(document).ready(function(){
     $('.customer_designer_directory').find('a.active').trigger('click');
 });
$(document).on("click",".upld_brand",function(e){
    var id = $(this).data("id");
    $("#f_uploadbrand #f_file_id").val(id);
    storedFiles = [];
    $('.uploadFileListContain').empty();
    $(".file_input").val("");
});
$(document).on("click",".upldflsfrmcstom",function(e){
    storedFiles = [];
    $('.fileuploads_customfolders').empty();
    $(".file_input").val("");
});
 $("#f_uplod_brnd_profl").submit(function (e) {
    e.preventDefault();
    var html = "";
    $this = $(this);
    var formData = $this.serialize();
    var file_type = $this.find(".file_type").val();
    var brand_id = $this.find(".f_file_id").val();
    var files = $('input[class^="delete_file"]').map(function () {
        return this.value;
    }).get();
    var name;
    if (file_type === "logo_upload") {
        name = "Brand Logo";
    } else if (file_type === "materials_upload") {
        name = "Marketing Material";
    } else if (file_type === "additional_upload") {
        name = "Additional Images";
    }
    if (files.length > 0) {
        $('.loading_file_procs').html('<span class="loading_process" style="text-align:center;"><img src="'+$assets_path+'img/ajax-loader.gif" alt="loading"/ style="margin: auto;display: block;"></span>');
        $.ajax({
            type: 'POST',
            url: baseUrl+"customer/Filemanagement/Uploadbrandprofile",
            dataType: "json",
            data: formData,
            success: function (response) {
                swal("File is uploaded successfully !", {
                    icon: "success",
                }).then(function () {
                    $('.loading_file_procs').html("");
                    $("#f_uploadbrand").modal("hide");
                    if($(".main_brand").length <= 0){
                        html += '<div class="main_brand"><div class="inner_structure"><div class="section-hddng"><h4>'+name+'</h4><span></span></div><div class="inner_data_'+file_type+'">';
                    }else if($(".inner_data_"+file_type).length <= 0){
                        html += '<div class="inner_structure"><div class="section-hddng"><h4>'+name+'</h4><span></span></div><div class="inner_data_'+file_type+'">';
                    }
                    $.each(files, function (k, file) {
                       var fileName = file;
                        var flname = files_array_toshow(fileName,$uploadbrandPath + brand_id + '/' + fileName);
                        var url =  $uploadbrandPath + brand_id + '/' + fileName;
                        html += '<li class="copyfolder_or_files sxtn" ><a href="JavaScript:void(0);" class="viewBox" data-status="files" data-from="'+url+'" data-id='+brand_id+' data-file='+fileName+' data-title="' + fileName + '" data-role="brand"><img src="'+ flname + '"> <h3> '+fileName+'</h3></a><div class="dropdown"><div class="simple-option dropdown-toggle" data-toggle="dropdown"> <i class="fas fa-ellipsis-h" data-id="#action_id_'+k+'"></i></div><div class="action_dropdown dropdown-menu" role="menu" id="action_id_'+k+'"><ul><li class="download_image"> <a href="'+ url + '" download><img src="'+baseUrl+'public/assets/img/download-form.svg" class="img-responsive"> Download File </a></li><li><a target="_blank" href="'+url+'"><i class="fas fa-link"></i> Go to Brand</a></li></ul></div></div></li>';

                   });
                   if($(".main_brand").length <= 0){
                       html += '</div></div></div>'; 
                      $(".all_request_files").html(html);
                   }else if($(".inner_data_"+file_type).length <= 0){
                      html += '</div></div>'; 
                      $(".main_brand").append(html);
                   }else{
                      $(".inner_data_"+file_type).append(html); 
                  }
                    $('.uploadFileListContain').empty();
                   // $('.uploadFileRow').html("");
                    $this.find(".file_input").val("");
                    $this.find(".delete_file").val("");
                     storedFiles = [];
                });
            }
        });
    }else{
        $(".err_msg_upldfl").html("Please Select Files to Upload");
    }
});

 $("#f_uploadcustomfile").submit(function (e) {
    e.preventDefault();
    $this = $(this);
    var formData = $this.serialize();
    var files = $('input[class^="delete_file"]').map(function () {
        return this.value;
    }).get();
    if (files.length > 0) {
        $('.loading_file_procs').html('<span class="loading_process" style="text-align:center;"><img src="'+$assets_path+'img/ajax-loader.gif" alt="loading"/ style="margin: auto;display: block;"></span>');
        $.ajax({
            type: 'POST',
            url: baseUrl+"customer/Filemanagement/upload_custom_files",
            dataType: "json",
            data: formData,
            success: function (response) {
                swal("File is uploaded successfully", {
                    icon: "success",
                }).then(function () {
                     $('.menu_list a:last-child').trigger('click');
                    $("#f_uploadfiles_custom").modal("hide");
                    $('.fileuploads_customfolders').empty();
                    $('.loading_file_procs').html("");
                    $this.find(".file_input").val("");
                    $this.find(".delete_file").val("");
                     storedFiles = [];
                });
            }
        });
    }else{
        $(".f_err_upld").html("Please Select Files to Upload");
    }
});

    $(document).on("click",".result_Filelisting",function(){
        $(".myiconCstm").hide(); 
            var url,file,user_id; 
            $this = $(this); 
            url =  $this.find("img").attr("src");
            file = url.substring(url.lastIndexOf('/')+1);
            ajaxFor_openFileType(url,file,"","");
    }); 
    
   $(document).on("click",".result_Folderlisting",function(){ 
       $this = $(this); 
       var is_active = $("#custom_structure").hasClass("active");
       var is_active2 = $("#default_folder").hasClass("active");
       var foldid =  $this.find("a").attr("data-foldid");
       var parentid =  $this.find("a").attr("data-parentid");
       var type =  $this.find("a").attr("data-type");
       
       if(is_active != true && type == null){
           $("#custom_structure").trigger("click");
        }
        if(type != null && type == "default" && is_active2 != true){
           $("#default_folder").trigger("click"); 
        }
      setTimeout(function(){

       $(".copyfolder_or_files").each(function(k,v)
         { 
            var findfoldid= $( this ).find("a").data("foldid"); 
            var findreqid= $( this ).find("a").data("reqid"); 
           if(parentid == "0" && foldid == findfoldid ){
              $(this).find("a.customer_directory").trigger("click");  
              $(this).find("a.request_file").trigger("click");  
              $(".search_result").hide(); 
           }
       });
     }, 1000);
   });


$(document).mouseup(function(e) 
{
    var container = $(".search_result");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        container.hide();
    }
});

$(".customer_designer_directory a").click(function(){
    var text = $(this).text(); 
    
    if(text == "My Folders"){
        $('.selected_folder').val(text);
    }else{
        $('.selected_folder').val("My folders");
    }
});
 

$(".creat_btn_cst, .creat_btn_css").hover(
  function () {
    $(this).parents("div").addClass("open")
  },
  function () {
    $(this).parents("div").removeClass("open");
  }
);

 $(document).on("keyup","#rename_text",function(e){
        $post = $(this);
        limitText($post, 20)   
    });
   
function limitText(field, maxChar){
    var ref = $(field),
        val = ref.val();
    if ( val.length >= maxChar ){
        ref.val(function() {
            return val.substr(0, maxChar);       
        });
    }
}
$(document).on("click","a",function(){
    $this = $(this); 
    if($this.get(0).hasAttribute("download") && $this.get(0).hasAttribute("onclick")!=true ){
       download_images_ajax($this);
      } 
    
});
function download_images_ajax(click){
    var url = click.attr("href"); 
      $.ajax({
            type: 'POST',
            url: baseUrl+"customer/Request/download_images", 
            data: {"file-upload[]": url},
            success: function (data) {
                 
            }
        });
 }


$('.f_custom_files').change(function () {
    var inputid = $(this).attr("id");
    var $fileListContainer = $(".fileuploads_customfolders");
    var loadingImg = $assets_path+'img/ajax-loader.gif';
    $fileListContainer.html('<div class="ajax-img-loader"><img src="'+loadingImg+'" height="150"/></div>');
    $('#f_uploadcustom_file').prop('disabled', true);
    var fileInput = $('#'+inputid)[0];
    if (fileInput.files.length > 0) {
    var formData = new FormData();
    $.each(fileInput.files, function (k, file) {
        var fileName = file.name;
        var type =  fileName.split('.').pop();
        var name = fileName.split('.');
        var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g,Math.floor(Math.random() * 6) + 1);
        var newFileName = name_file + '.'+type;
        formData.append('file-upload[]', file,newFileName);
    });
    var $url =  baseUrl+"customer/request/process";
    $.ajax({
        method: 'post',
        url: $url,
        data: formData,
        dataType: 'json',
        contentType: false,
        processData: false,
        success: function (response) {
            //console.log(response);
            if (response.status == '1') {
                if (response.files.length > 0) {
                    $.each(response.files, function (k, v) {
                        storedFiles.push(v[0]);
                        $fileListContainer.html(createStoredFilesHtml());
                        $('#f_uploadcustom_file').prop('disabled', false);
                    });
                }
            }
        }
    });
    }
});


 // this is code not releated to File management 
 
//function DrawALine(Start,EnD){
//         
//       var lineH = new LeaderLine(
//                        document.getElementById(Start),
//                        document.getElementById(EnD),
//                        {
//                          path: 'straight'
//                          // dash: {animation: true}
//                        }
//                        );
//        
//        $("#leader-line-"+lineH._id+"-plug-marker-1").addClass("custom-color"); 
//        $(".leader-line").hide();   
//        $("#"+EnD).draggable({
//
//                       drag: function(event, ui){
//                        $(".arrowdowns").addClass("hidenotch"); 
//                        $(".leader-line").show(); 
//                          lineH.position();
//                     }
//        });
//       
//
//    }

    //  not related to file management