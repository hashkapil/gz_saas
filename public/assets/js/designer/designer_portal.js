$(document).ready(function () {
    
    if ($(window).width() <= '767') {
        console.log("test"); 
        $(document).on('click','.arrow_btn',function(){
            console.log("test on btn"); 
            $('.admin-sidebar').toggleClass('open');
        });
    $(document).click(function(e) {
        var classnm = $('.arrow_btn');
        if (!classnm.is(e.target) && classnm.has(e.target).length === 0){
          $(".admin-sidebar").removeClass("open");
        }
    }); 
    }
    
    $('.input').focus(function () {
        $(this).parent().find(".label-txt").addClass('label-active');
    });
    $(".input").focusout(function () {
        if ($(this).val() == '') {
            $(this).parent().find(".label-txt").removeClass('label-active');
        };
    });
    $('.input').each(function () {
        if ($(this).val() != '') {
            $(this).parent().find(".label-txt").addClass('label-active');
        } else {
            $(this).parent().find(".label-txt").removeClass('label-active');
        }
    });
});


if(($('.chattype').length) > 0){
$(".chattype").on('change', function (e) {
    var valueSelected = this.value;
    $(".send_request_chat").attr('data-customerwithornot', valueSelected);
    $("#shre_file").attr('data-withornot', valueSelected);
    if (valueSelected == 0) {
        $('#message_container_without_cus').show();
        $('#message_container').hide();
//        $('#message_container_without_cus').stop().animate({
//            scrollTop: $('#message_container_without_cus')[0].scrollHeight
//        }, 1);
    } else if (valueSelected == 1) {
        $('#message_container_without_cus').hide();
        $('#message_container').show();
//        $('#message_container').stop().animate({
//            scrollTop: $('#message_container')[0].scrollHeight
//        }, 1);
    }
});
}

$(document).on('click', '.openchange', function (e) {
    var chatid = $(this).data('chatid');
    $(".open-edit_" + chatid).slideToggle()
});

$(document).on('click', '.editmsg', function () {
    var editid = $(this).data('editid');
    $('.msg_desc_' + editid).css('display', 'none');
    $('.open-edit_' + editid).css('display', 'none');
    $('.edit_main_' + editid).css('display', 'block');
});
$(document).on('click', '.edit_save', function () {
    var editid = $(this).data('id');
    var msg = $("#edit_main_msg_" + editid).val();
    $.ajax({
        type: 'POST',
        url: baseUrl + "admin/Request/Editmsg",
        data: { editid: editid, msg: msg },
        success: function (data) {
            if (data) {
                $('.edit_icon_' + editid).html('<i class="fas fa-pen"></i>');
                $('.took_' + editid).html('<div class="edited_msg">' + msg + '</div>');
                $('.msg_desc_' + editid).css('display', 'block');
                $('.edit_main_' + editid).css('display', 'none');
            }
        }
    });
});
$(document).on('click', '.cancel_main', function () {
    var msgid = $(this).data('msgid');
    $('.edit_main_' + msgid).css('display', 'none');
    $('.msg_desc_' + msgid).css('display', 'block');
});
$(document).on('click', '.deletemsg', function () {
    var r = confirm('Are you sure you want to delete this message?');
    if (r == true) {
        var delid = $(this).data('delid');
        $.ajax({
            type: 'POST',
            url: baseUrl + "admin/Request/Deletemsg",
            data: { delid: delid },
            success: function (data) {
                if (data) {
                    //console.log(delid);
                    $('.editdeletetoggle_' + delid).css('display', 'none');
                    $('.took_' + delid).html('<div class="edited_msg deleted_msg"><i class="fa fa-ban" aria-hidden="true"></i> You have deleted this message.</div>');
                }
            }
        });
    }
});

$(document).on('click','.clearall_msges',function(){
    var all_msg = $(this).data('allmsges');
    var vals=[];
    for(var i=0;i<all_msg.length;i++){
       vals.push(all_msg[i].id);
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + "designer/Request/clearAllmessages",
        data: { delid: vals },
        success: function (data) {
            if (data) {
                
            }
        }
    });
});
/******file sharing in chat******/
$(document).on('click', '.attchmnt', function () {
    $("input[id='shre_file']").click();
});
