$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip(); 
    if ($(window).width() <= '767') {
        console.log("test"); 
        $(document).on('click','.arrow_btn',function(){
            $('.admin-sidebar').toggleClass('open');
        });
    $(document).click(function(e) {
        var classnm = $('.arrow_btn');
        if (!classnm.is(e.target) && classnm.has(e.target).length === 0){
          $(".admin-sidebar").removeClass("open");
        }
    }); 
    }

    
    $(document).on('focus','.input',function(){
        $(this).parent().find(".label-txt").addClass('label-active');
    });
   $(document).on('focusout','.input',function(){
        if ($(this).val() == '') {
            $(this).parent().find(".label-txt").removeClass('label-active');
        };
    });
    $('.input').each(function () {
        if ($(this).val() != '') {
            $(this).parent().find(".label-txt").addClass('label-active');
        } else {
            $(this).parent().find(".label-txt").removeClass('label-active');
        }
    });
    if ($('#tax_datepicker_start').length > '0') {
        $("#tax_datepicker_start").datepicker({
            maxDate: new Date(),
            dateFormat: 'yy-mm-dd',
            onSelect: function (dateText, inst) {
                var date = $(this).val();
                $('#tax_datepicker_start').val(date);
            }
        });
    }
    if ($('#tax_datepicker_end').length > '0') {
        $("#tax_datepicker_end").datepicker({
            maxDate: new Date(),
            dateFormat: 'yy-mm-dd',
            onSelect: function (dateText, inst) {
                var date = $(this).val();
                $('#tax_datepicker_end').val(date);
            }
        });
    }
    $('.profile_pic_sec_btn').click(function (e) {
        e.preventDefault();
        $('.profile-pic').hide();
        $('.edit_profile_pic_sec').css('display', 'block');
    });
    
    Autoresizingcategory();

    var url = window.location.href;
    var pieces = url.split("#");
    var activebar = pieces[1];
    $('#' + activebar).addClass('active');
    $('#' + activebar).siblings().removeClass('active');
    $('[href*="#' + activebar + '"]').closest('li').siblings().removeClass('active');
    $('[href*="#' + activebar + '"]').closest('li').addClass('active');
    countTimer();
    var status = $('#status_check li.active a').attr('data-status');
    var id = $('#status_check li.active a').attr('href');
    $('.tab-pane').attr("data-loaded", $rowperpage);
    $(document).on("click", "#load_more", function () {
        countTimer();
        //if(BUCKET_TYPE != ''){
        var bucket_type = BUCKET_TYPE;
        //}
        var assign_project = ASSIGN_PROJECT;
        $(".ajax_loader").css("display", "flex");
        $(".load_more").css("display", "none");
        var row = parseInt($(this).attr('data-row'));
        ///var allcount = parseInt($(this).attr('data-count'));
        row = row + $rowperpage;
        var activeTabPane = $('.tab-pane.active');
        var searchval = activeTabPane.find('.search_text').val();
        var allcount = parseInt(activeTabPane.attr("data-total-count"));
        var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
        var client_id = dash_client_id;
        var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
        var status_scroll = $('#status_check li.active a').attr('data-status');
        var tabid = $('#status_check li.active a').attr('href');
        if (allLoaded < allcount) {
            $.post(BASE_URL + 'admin/dashboard/load_more', { 'group_no': activeTabPaneGroupNumber, 'status': status_scroll, 'search': searchval, 'bucket_type': bucket_type, 'assigned_project': assign_project, 'client_id': client_id, 'designer_id': dash_designer_id,'bkurl':comefrm },
                function (data) {
                    if (data != "") {
                        //                            console.log('data h');
                        $(tabid + " .product-list-show .row").append(data);
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more").css("display", "inline-block");
                        activeTabPane.find('.load_more').attr('data-row', row);
                        activeTabPaneGroupNumber++;
                        activeTabPane.attr('data-group', activeTabPaneGroupNumber);
                        allLoaded = allLoaded + $rowperpage;
                        activeTabPane.attr('data-loaded', allLoaded);
                        if (allLoaded >= allcount) {
                            activeTabPane.find('.load_more').css("display", "none");
                            $(".ajax_loader").css("display", "none");
                        } else {
                            activeTabPane.find('.load_more').css("display", "inline-block");
                            $(".ajax_loader").css("display", "none");
                        }
                    }
                });
        }
    });

    $(".read_more_btn").click(function () {
        $(".read_more").toggle();
    });

    $('#message_container .msgk-chatrow.clearfix').each(function (e) {
        if ($('figure', this).length > 0) {
            $(this).addClass('margin_top');
        }
    });

    $('#file_input').change(function () {
        var type = $(this).attr('data-type');
        if(type == 'sample'){
          type = 'sample';  
        }else{
            type = '';
        }
        var filesCount = $(this)[0].files.length;
        var $textContainer = $(this).prev();
        var imgpath =  $asset_path+'img/ajax-loader.gif';
        var $fileListContainer = $(".uploadFileListContainer");
        filesCount = storedFiles.length;
        if (filesCount === 1) {
            var fileName = $(this).val().split('\\').pop();
        } else if (filesCount >= 1) { }
        $fileListContainer.append('<div class="ajax-img-loader"><img src='+imgpath+' height="150"/></div>');
        var fileInput = $('#file_input')[0];
        if (fileInput.files.length > 0) {
            var formData = new FormData();
            $.each(fileInput.files, function (k, file) {
                var fileName = file.name;
                var type = fileName.split('.').pop();
                var name = fileName.split('.');
                var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, Math.floor(Math.random() * 6) + 1);
                var newFileName = name_file + '.' + type;
                formData.append('file_upload[]', file, newFileName);
            });
            $.ajax({
                method: 'post',
                url: BASE_URL + "admin/dashboard/attach_file_process/"+type,
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                success: function (response) {
                    if (response.status == '1') {
                        if (response.files.length > 0) {
                            $.each(response.files, function (k, v) {
                                storedFiles.push(v[0]);
                                $fileListContainer.html("");
                                $.each(storedFiles, function (key, value) {
                                    if (value.error == false) {
                                        $('.ajax-img-loader').css('display', 'none');
                                        $fileListContainer.append('<div  class="uploadFileRow " id="file' + key + '"><div class="col-md-6"><div class="extnsn-lst">\n\
                                        <p class="text-mb">' + value.file_name + '<strong> (' + formatFileSize(value.file_size, 2) + ') </strong></p>\n\
                                        <p class="cross-btnlink">\n\
                                        <a href="javascript:void(0)" class="delete_file" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
                                        <span>x</span>\n\
                                        </a>\n\
                                        <input type="hidden" value="' + value.file_name + '" name="delete_FL[]" class="delete_file"/><input type="hidden" value="" name="upload_path" class="upload_path" />\n\
                                        \n\
                                        \n\
                                        \n\
                                        </p>\n\
                                        </div></div></div>');
                                    } else {
                                        $fileListContainer.append('<div  class="uploadFileRow" id="file' + key + '"><div class="col-md-6" ><div class="extnsn-lst error-list">\n\
                                    <p class="text-mb">' + value.file_name + '<br/><strong>' + value.error_msg + '</strong></p><p class="cross-btnlink">\n\
                                    <a href="javascript:void(0)" class="delete_file" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
                                    <span>x</span>\n\
                                    </a>\n\
                                    \n\
                                    \n\
                                    \n\
                                    \n\
                                    </p>\n\
                                    </div></div></div>');
                                    }
                                });
                            });
                        }
                    } else {
                    }
                }
            });
        } else {
            //console.log('No Files Selected');
        }
    });

    var status = $('#status_check_client li.active a').attr('data-status');
    var id = $('#status_check_client li.active a').attr('href');
    $('.tab-pane').attr("data-loaded", $rowperpage);
    $(document).on("click", "#load_more_client", function () {
        $(".ajax_loader").css("display", "flex");
        $(".load_more").css("display", "none");
        var isonetime = $(this).data('is_single');
        var row = parseInt($(this).attr('data-row'));
        row = row + $rowperpage;
        var activeTabPane = $('.tab-pane.active');
        var searchval = $('.search_text').val();
        var assign_project = ASSIGN_PROJECT;
        var allcount = parseInt(activeTabPane.attr("data-total-count"));
        var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
        var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
        var status_scroll = $('#status_check_client li.active a').attr('data-status');
        var tabid = $('#status_check_client li.active a').attr('href');
        if (allLoaded < allcount) {
            $.post(BASE_URL + 'admin/dashboard/load_more_customer', { 'group_no': activeTabPaneGroupNumber, 'status': status_scroll, 'search': searchval, 'isonetime': isonetime, 'assign_project': assign_project },
                function (data) {
                    if (data != "") {
                        $(tabid + " .product-list-show .add-rows .two-can").append(data);
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more").css("display", "inline-block");
                        activeTabPane.find('.load_more').attr('data-row', row);
                        activeTabPaneGroupNumber++;
                        activeTabPane.attr('data-group', activeTabPaneGroupNumber);
                        allLoaded = allLoaded + $rowperpage;
                        activeTabPane.attr('data-loaded', allLoaded);
                        if (allLoaded >= allcount) {
                            activeTabPane.find('.load_more').css("display", "none");
                            $(".ajax_loader").css("display", "none");
                        } else {
                            activeTabPane.find('.load_more').css("display", "inline-block");
                            $(".ajax_loader").css("display", "none");
                        }
                    }
                });
        }
    });

    $('.switch').each(function () {
        var checkbox = $(this).children('input[type=checkbox]');
        var toggle = $(this).children('label.switch-toggle');
        if (checkbox.length) {
            checkbox.addClass('hidden');
            toggle.removeClass('hidden');
            if (checkbox[0].checked) {
                toggle.addClass('on');
                toggle.removeClass('off');
                toggle.text(toggle.attr('data-on'));
            }
            else {
                toggle.addClass('off');
                toggle.removeClass('on');
                toggle.text(toggle.attr('data-off'));
            }
            ;
        }
    });

    $(document).on('change', '.actClient', function () {
        var status = $(this).prop('checked');
        var data_id = $(this).attr('data-cid');
        var ischecked = ($(this).is(':checked')) ? 1 : 0;
        //        if (ischecked) {
        //            $(".checkstatus_" + data_id).text('Active');
        //        } else {
        //            $(".checkstatus_" + data_id).text('Inactive');
        //        }
        $.ajax({
            type: 'POST',
            url: BASE_URL + "admin/dashboard/change_customer_status_active_inactive",
            data: { status: status, data_id: data_id },
            success: function (data) {
            }
        });
    });

    $('.tab-pane').attr("data-loaded", $rowperpage);
    $(document).on("click", "#load_more_designer", function () {
        var row = parseInt($(this).attr('data-row'));
        row = row + $rowperpage;
        var activeTabPane = $('.tab-pane.active');
        activeTabPane.find('.ajax_loader').css("display", "flex");
        activeTabPane.find('.load_more').css("display", "none");
        var searchval = activeTabPane.find('.search_text').val();
        var allcount = parseInt(activeTabPane.attr("data-total-count"));
        var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
        var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
        if (allLoaded < allcount) {
            $.post(BASE_URL + 'admin/dashboard/load_more_designer', { 'group_no': activeTabPaneGroupNumber },
                function (data) {
                    if (data != "") {
                        activeTabPane.find('.ajax_loader').css("display", "none");
                        activeTabPane.find('.load_more').css("display", "inline-block");
                        $('#designerslist .two-can').append(data);
                        activeTabPaneGroupNumber++;
                        activeTabPane.attr('data-group', activeTabPaneGroupNumber);
                        allLoaded = allLoaded + $rowperpage;
                        activeTabPane.attr('data-loaded', allLoaded);
                        if (allLoaded >= allcount) {
                            activeTabPane.find('.load_more').css("display", "none");
                        } else {
                            activeTabPane.find('.load_more').css("display", "inline-block");
                        }
                    }
                });
        }
    });



    $(".change_tab").each(function (index) {
        if (($(this).hasClass('active')) == true) {
            var active_status = $(this).attr('data-status');
            read_unread_message(active_status);
        }
    });
    
    var position = 0;
    var val1 = $('#sckill_val1').val();
    var val2 = $('#sckill_val2').val();
    var val3 = $('#sckill_val3').val();
    var val4 = $('#sckill_val4').val();
    var val5 = $('#sckill_val5').val();
    var val6 = $('#sckill_val6').val();
    var val7 = $('#sckill_val7').val();
    var position1 = [val1, val2, val3, val4, val5, val6, val7];
    //if(val1 != '' || val2 != '' || val3 != '' || val4 != '' || val5 != '' || val6 != '' || val7 != ''){
    if ($('#sckill_val1').val() > '0') {
        $(position1).each(function (i) {
            $('#ui-slider-control' + i)
                .UISlider({
                    min: 1,
                    max: 101,
                    smooth: false,
                    value: position1[i]
                })

                .on('change thumbmove', function (event, value) {
                    var targetPath = $(event.target).data('target');
                    //console.log(targetPath,value);
                    $(targetPath).text(value);
                    $(targetPath).next('input.value_skill').val(value);
                    $('#dataaaaa').val(targetPath);
                })
                .on('start', function () {
                    $('.value').addClass('editing');
                })
                .on('end', function () {
                    $('.value').removeClass('editing');
                });
            $('input[type=checkbox]')
                .on('change', function () {

                    var name = $(this).prop('name'),
                        value = $(this).prop('checked');

                    if (name === 'popup') {

                        $('.popup-buttons').toggle(value);

                    } else {

                        $('.ui-slider-control').UISlider(name, value);
                    }
                });

        });
    }
    if (($('#datepicker_start').length) > '0' || ($('#datepicker_end').length) > '0' || ($('#taxes_start').length) > '0' || ($('#taxes_end').length) > '0' || ($('#msgstart_date').length) > '0' || ($('#endmsg_date').length) > '0') {
        $("#datepicker_start").datepicker({
            // maxDate: new Date(),
            dateFormat: 'yy-mm-dd',
            onSelect: function (dateText, inst) {
                var date = $(this).val();
                $('#datepicker_start').val(date);
            }
        });
        $("#datepicker_end").datepicker({
            //maxDate: new Date(),
            dateFormat: 'yy-mm-dd',
            onSelect: function (dateText, inst) {
                var date = $(this).val();
                $('#datepicker_end').val(date);
            }
        });

        $("#taxes_start").datepicker({
            // maxDate: new Date(),
            dateFormat: 'yy-mm-dd',
            onSelect: function (dateText, inst) {
                var date = $(this).val();
                $('#taxes_start').val(date);
            }
        });

        $("#taxes_end").datepicker({
            //maxDate: new Date(),
            dateFormat: 'yy-mm-dd',
            onSelect: function (dateText, inst) {
                var date = $(this).val();
                $('#taxes_end').val(date);
            }
        });

        $("#msgstart_date").datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function (dateText, inst) {
                var date = $(this).val();
                $('#msgstart_date').val(date);
            }
        });
        $("#endmsg_date").datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function (dateText, inst) {
                var date = $(this).val();
                $('#endmsg_date').val(date);
            }
        });
    }
});


//start project listing js
$(document).on('click', ".adddesinger", function () {
    var request_id = $(this).attr('data-requestid');
    var designer_id = $(this).attr('data-designerid');
    $('#AddDesign #request_id').val(request_id);
    if (designer_id !== "") {
    $('#AddDesign input#' + designer_id).click();
    }
});

$('#assign_desig').click(function (e) {
    e.preventDefault();
    var designer_id = $('input[name=assign_designer]:checked').val();
    var request_id = $('#request_id').val();
    var designer_name = $('input[name=assign_designer]:checked').attr('data-name');
    var designer_pic = $('input[name=assign_designer]:checked').attr('data-image-pic');
    if (typeof designer_id === "undefined" || designer_id == "") {
        $('#show_error').html("Please Select Any Designer");
        $('.alert-dismissable.error').css('display', 'block');
    }else{
    $.ajax({
        url: BASE_URL + 'admin/Dashboard/assign_designer_ajax',
        type: 'POST',
        data: {
            'assign_designer': designer_id,
            'request_id': request_id
        },
        success: function (data) {
            var returnedData = JSON.parse(data);
//             console.log('succ',returnedData.status);
            if (returnedData.status == 'success') {
                $('.alert-dismissable.error').css('display', 'none');
//                console.log('yes',returnedData.status);
            var result = (request_id).split(',');
            //console.log(result.length);
            if(result.length > 1){
            $.each(result,function(i){
                $("." + result[i] + " p.text-h").html(designer_name);
                $("." + result[i] + " a.adddesinger").attr('data-designerid', designer_id);
             });
         }else{
             $("." + request_id + " p.text-h").html(designer_name);
             $("." + request_id + " a.adddesinger").attr('data-designerid', designer_id);
         }
            } else {
                //console.log('no',returnedData.status);
                $('#show_error').html(returnedData.msg);
                $('.alert-dismissable.error').css('display', 'block');
            }
        }
    });
}
});

function load_more_admin(is_loaded, currentstatus, tabActive, search, activetabid, client_id, dash_designer_id) {
    if (is_loaded != '') {
        var dataloaded = is_loaded.attr('data-isloaded');
    } else {
        dataloaded = '';
    }
    if (client_id != '') {
        client_id = client_id;
    } else {
        client_id = '';
    }
    var bucket_type = BUCKET_TYPE;
    var assign_project = ASSIGN_PROJECT;
    var status_Active = $('#status_check li.active a').attr('data-status');
    $("input:hidden.bucket_type").val(bucket_type);
    var activetab = $('#status_check li.active a').attr('href');
    var tabid;
    if (tabActive != '') {
        tabid = tabActive;
    } else {
        tabid = activetab;
    }
    delay(function () {
        $('.ajax_loader').css('display', 'none');
    }, 5000);
    var activeTabPane = $('.tab-pane.active');
    var status_scroll = (currentstatus != '') ? currentstatus : status_Active;
    var searchval = $('.search_text').val();
    if (dataloaded != '1' || search == '2') {
        $.post(BASE_URL + 'admin/dashboard/load_more', { 'group_no': 0, 'status': status_scroll, 'search': searchval, 'bucket_type': bucket_type, 'assigned_project': assign_project, 'activetabid': activetabid, 'client_id': client_id, 'designer_id': dash_designer_id,'bkurl':comefrm},
            function (data) {
                var newTotalCount = 0;
                if (data != '') {
                    if (is_loaded != '') {
                        is_loaded.attr('data-isloaded', '1');
                    }
                    $('.ajax_loader').css('display', 'none');
                    $(".ajax_searchload").fadeOut(500);
                    $(tabid + " .product-list-show .row").fadeOut(200, function () {
                        $(tabid + " .product-list-show .row").html(data);
                        $(tabid + " .product-list-show .row").fadeIn(800);
                    });
                    var tabname = tabid.replace('#','');
                    //newTotalCount = $(tabid + " .product-list-show .row").find("#loadingAjaxCount").attr('data-value');
                    newTotalCount = $('#'+tabname).attr('data-total-count');
                    //console.log("newTotalCount",newTotalCount);
                    $(tabid + " .product-list-show .row").find("#loadingAjaxCount").remove();
                    activeTabPane.attr("data-total-count", newTotalCount);
                    activeTabPane.attr("data-loaded", $rowperpage);
                    activeTabPane.attr("data-group", 1);
                } else {
                    activeTabPane.attr("data-total-count", 0);
                    activeTabPane.attr("data-loaded", 0);
                    activeTabPane.attr("data-group", 1);
                    $(tabid + " .product-list-show .row").html("<div class='no_data_ajax'>No record found</div>");
                }
                if ($rowperpage >= newTotalCount || newTotalCount == undefined) {
                    activeTabPane.find('.load_more').css("display", "none");
                } else {
                    delay(function () {
                        $('.load_more').css('display', 'inline-block');
                        activeTabPane.find('.load_more').css("display", "inline-block");
                    }, 1000);
                }
            });
    } else {
        $('.ajax_loader').css('display', 'none');
        $('.load_more').css('display', 'block');
    }
    }
//}
$('.close-search').click(function (e) { 
    $(this).parent().closest('.search-box ').removeClass('magic_search');
});
$('.search_data_ajax').click(function (e) {
    e.preventDefault();
    load_more_admin('', '', '', '2', '', dash_client_id, dash_designer_id);
});

$('.searchdata_admin').keyup(function (e) {
    //console.log("xcvxcv");
    e.preventDefault();
    delay(function () {
        $('.ajax_loader').css('display', 'flex');
        $(".ajax_searchload").fadeIn(500);
        load_more_admin('', '', '', '2', '', dash_client_id, dash_designer_id);
    }, 1000);
});

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

function countTimer() {
    $('.expectd_date').each(function () {
        var expected_date = $(this).attr('data-date');
        var user_datetimezone = timezone;
//        console.log(user_datetimezone);
        var data_id = $(this).attr('data-id');
        // Set the date we're counting down to
        var countDownDate = new Date(expected_date).getTime();
        // Update the count down every 1 second
        var x = setInterval(function () {
            // Get todays date and time
            var nowdate = new Date().toLocaleString('en-US', { timeZone: user_datetimezone });

            if (user_datetimezone != '' || user_datetimezone != null) {
                var now = new Date(nowdate).getTime();
            } else {
                var now = new Date().getTime();
            }
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            if (minutes > 0) {
               // console.log("inside",minutes);
                var timer = ("0" + days).slice(-2) + " : " + ("0" + hours).slice(-2) + " : "
                    + ("0" + minutes).slice(-2) + " : " + ("0" + seconds).slice(-2) + " ";
                $('#demo_' + data_id).text(timer);
            } else {
                //console.log("outside",minutes);
                $('#demo_' + data_id).text("EXPIRED");
            }
        }, 1000);
    });
}

/********load more using ajax on tab change**********/
$('.admin_pro_listing li a').click(function (e) {
//    console.log("test");
    $('.ajax_loader').css('display', 'flex');
    //$('.load_more').css('display', 'none');
    var currentstatus = $(this).data('status');
    var activetabid = $(this).parent().attr('id');
    var bucket_type = BUCKET_TYPE;
    var status = "";
    if (currentstatus == "active,disapprove") {
        status = "active-disapprove";
    } else {
        status = currentstatus;
    }
    var url = BASE_URL + "admin/Dashboard/downloadrequest_accstatus/" + status + "/" + bucket_type;
    $('#download_rquests').attr('href', url);
    var tab = $(this).attr('href');
    var is_loaded = $(this).data('isloaded');
    //console.log(is_loaded);
    e.preventDefault();
    load_more_admin($(this), currentstatus, tab, "", activetabid, dash_client_id, dash_designer_id);
});

$(document).on('change', '.is_verified input[type="checkbox"]', function (e) {
    var data_id = $(this).attr('data-pid');
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    if (ischecked == 1) {
        $('#verified_' + data_id).addClass('verified_by_admin');
        if ($(this).closest('label').find('i').hasClass("fa-circle")) {
            $(this).closest('label').find('i').removeClass('fa-circle');
            $(this).closest('label').find('i').addClass('fa-check-circle');
            $(this).closest('label').find('.vrfy_txt').html('Verified');
        }
    } else {
        $('#verified_' + data_id).removeClass('verified_by_admin');
        if ($(this).closest('label').find('i').hasClass("fa-check-circle")) {
            $(this).closest('label').find('i').removeClass('fa-check-circle');
            $(this).closest('label').find('i').addClass('fa-circle');
            $(this).closest('label').find('.vrfy_txt').html('Verify');        }
    }
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/dashboard/request_verified_by_admin",
        data: { data_id: data_id, ischecked: ischecked },
        success: function (data) {
        }
    });
});

//add designer button movement
//$(document).on('change', '.selected_pro', function () {
//    var checked = $("input[type='checkbox']:checked").length;
//    //    console.log(checked);
//    //    console.log($(window).scrollTop());
//    if (checked >= 1 && $(window).scrollTop() > '215') {
//        $('.adddesinger').addClass('relate_check');
//    } else {
//        $('.adddesinger').removeClass('relate_check');
//    }
//});

/***************Edit Expected****************/
$(document).on('click', '.editexpected', function () {
    var data_expected = $(this).attr('date-expected');
    var data_reqid = $(this).attr('date-reqid');
    // console.log(data_expected);
    $('#editExpected').modal();
    $('input[name=edit_expected]').val(data_expected);
    $('input[name=edit_reqid]').val(data_reqid);
});

$(document).on('click', '.edit_expected', function () {
    var date = jQuery(this).datetimepicker();
});

function check(val, attr) {
    // var status = $(this).val();
    var status = val;
    // var request_id = $(this).attr('data-request');
    var request_id = attr;
    $.ajax({
        url: BASE_URL + "admin/dashboard/designer_skills_matched",
        type: 'POST',
        data: {
            'designer_skills_matched': status,
            'request_id': request_id
        },
        success: function (data) {
            if (data == 1) {
                setTimeout(function () {
                    $('#' + request_id).hide();
                }, 1000);
            }
        }
    });
}

$(".checkAll").change(function () {
    var attribute = $(this).attr('data-attr');
    //console.log(attribute);
    if ($(this).is(':checked')) {
        $('.' + attribute).prop("checked", true);
    } else {
        $('.' + attribute).prop("checked", false);
    }
    var requestIDs = [];
    $.each($("input[name='project']:checked"), function () {
        if ($(this).val() != 'on') {
            requestIDs.push($(this).val());
        }
    });
    $('.adddesinger').attr('data-requestid', requestIDs);
    //console.log(requestIDs);
});

$(document).on('change', '.selected_pro', function () {
    var requestIDs = [];
    $.each($("input[name='project[]']:checked"), function () {
        if ($(this).val() != 'on') {
            requestIDs.push($(this).val());
        }
    });
    $('.adddesinger').attr('data-requestid', requestIDs);
});

//start project overview page
$(".chattype").on('change', function (e) {
    var valueSelected = this.value;
    $(".send_request_chat").attr('data-customerwithornot', valueSelected);
    $("#shre_file").attr('data-withornot', valueSelected);
    if (valueSelected == 0) {
        $('#message_container_without_cus').show();
        $('#message_container').hide();
//        $('#message_container_without_cus').stop().animate({
//            scrollTop: $('#message_container_without_cus')[0].scrollHeight
//        }, 1);
    } else if (valueSelected == 1) {
        $('#message_container_without_cus').hide();
        $('#message_container').show();
//        $('#message_container').stop().animate({
//            scrollTop: $('#message_container')[0].scrollHeight
//        }, 1);
    }
});

$(document).on('click', '.copy_public_link', function () {
    $('.show_publink').focus();
    $('.show_publink').select();
    document.execCommand('copy');
    $(".copiedone").text("Link copied").show().fadeOut(1200);
});

$(document).on('click', '.send_request_chat', function () {
    var designer_id = $(this).attr("data-designerid");
    var request_id = $(this).attr("data-requestid");
    var sender_type = $(this).attr("data-senderrole");
    var sender_id = $(this).attr("data-senderid");
    var reciever_id = $(this).attr("data-receiverid");
    var reciever_type = $(this).attr("data-receiverrole");
    var sender_name = $(this).attr('data-sendername');
    var text_id = 'text_' + request_id;
    var message = $('.' + text_id).val();
    if (message.indexOf("http://") >= 0) {
        var link_http_string = (message.substr(message.indexOf("http://") + 0));
        var link = link_http_string.split(' ')[0];
    }
    if (message.indexOf("https://") >= 0) {
        var link_https_string = (message.substr(message.indexOf("https://") + 0));
        var link = link_https_string.split(' ')[0];
    }
    if (message.includes(link)) {
        message = message.replace(link, '<a target="_blank" href="' + link + '">' + link + '</a>');
    } else {
        message = message;
    }
    var verified_by_admin = "1";
    var customer_name = $(this).attr("data-sendername");
    var customer_withornot = $(this).attr("data-customerwithornot");
    var profilepic = $(this).attr("data-profilepic");
    if (message != "") {
        $('.text_' + request_id).val("");
        $.ajax({
            type: "POST",
            url: BASE_URL + "admin/dashboard/send_message_request",
            data: {
                "request_id": request_id,
                "designer_id": designer_id,
                "sender_type": sender_type,
                "sender_id": sender_id,
                "reciever_id": reciever_id,
                "reciever_type": reciever_type,
                "message": message,
                "with_or_not_customer": customer_withornot,
                "admin_seen": "1",
            },
            success: function (data) {
                if (customer_withornot == 0) {
                    //                    console.log("hello",message);
                    if ($('#message_container_without_cus > div.msgk-chatrow:last-child > div').hasClass("msgk-right") === true) {
                        $("#message_container > div.msgk-chatrow:nth-last-child(1)").find('.just_now').css('display', 'none');
                        $('#message_container_without_cus').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + 'del_' + data + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + data + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + data + '"></i><div class="open-edit open-edit_' + data + '" style="display: none;"><a href="javascript:void(0)" class="editmsg" data-editid="' + data + '">edit</a><a href="javascript:void(0)" class="deletemsg" data-delid="' + data + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + data + '" style="display:none"><form method="post" id="editMainmsg"><textarea class="pstcmm sendtext" id="edit_main_msg_' + data + '">' + message + '</textarea><a href="javascript:void(0)" class="edit_save"  data-id="' + data + '">Save</a><a href="javascript:void(0)" class="cancel_main" data-msgid="' + data + '">Cancel</a></form></div> <div class="msgk-mn msg_desc_' + data + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + data + ' edit_icon_msg"></span><span class="msgk-umsxxt took_' + data + '">' + message + '</span></pre></div></div></div></div>');
                    } else {
                        $('#message_container_without_cus').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + 'del_' + data + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + data + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + data + '"></i><div class="open-edit open-edit_' + data + '" style="display: none;"><a href="javascript:void(0)" class="editmsg" data-editid="' + data + '">edit</a><a href="javascript:void(0)" class="deletemsg" data-delid="' + data + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + data + '" style="display:none"><form method="post" id="editMainmsg"><textarea class="pstcmm sendtext" id="edit_main_msg_' + data + '">' + message + '</textarea><a href="javascript:void(0)" class="edit_save"  data-id="' + data + '">Save</a><a href="javascript:void(0)" class="cancel_main" data-msgid="' + data + '">Cancel</a></form></div> <div class="msgk-mn msg_desc_' + data + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + data + ' edit_icon_msg"></span><span class="msgk-umsxxt took_' + data + '">' + message + '</span></pre></div></div></div></div>');
                    }
                } else if (customer_withornot == 1) {
                    //            console.log("hii",message);
                    if ($('#message_container > div.msgk-chatrow:last-child > div').hasClass("msgk-right") === true) {
                        $("#message_container > div.msgk-chatrow:nth-last-child(1)").find('.just_now').css('display', 'none');
                        $('#message_container').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + 'del_' + data + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + data + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + data + '"></i><div class="open-edit open-edit_' + data + '" style="display: none;"><a href="javascript:void(0)" class="editmsg" data-editid="' + data + '">edit</a><a href="javascript:void(0)" class="deletemsg" data-delid="' + data + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + data + '" style="display:none"><form method="post" id="editMainmsg"><textarea class="pstcmm sendtext" id="edit_main_msg_' + data + '">' + message + '</textarea><a href="javascript:void(0)" class="edit_save"  data-id="' + data + '">Save</a><a href="javascript:void(0)" class="cancel_main" data-msgid="' + data + '">Cancel</a></form></div> <div class="msgk-mn msg_desc_' + data + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + data + ' edit_icon_msg"></span><span class="msgk-umsxxt took_' + data + '">' + message + '</span></pre></div></div></div></div>');
                    } else {
                        $('#message_container').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + 'del_' + data + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + data + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + data + '"></i><div class="open-edit open-edit_' + data + '" style="display: none;"><a href="javascript:void(0)" class="editmsg" data-editid="' + data + '">edit</a><a href="javascript:void(0)" class="deletemsg" data-delid="' + data + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + data + '" style="display:none"><form method="post" id="editMainmsg"><textarea class="pstcmm sendtext" id="edit_main_msg_' + data + '">' + message + '</textarea><a href="javascript:void(0)" class="edit_save"  data-id="' + data + '">Save</a><a href="javascript:void(0)" class="cancel_main" data-msgid="' + data + '">Cancel</a></form></div> <div class="msgk-mn msg_desc_' + data + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + data + ' edit_icon_msg"></span><span class="msgk-umsxxt took_' + data + '">' + message + '</span></pre></div></div></div></div>');
                    }
                }
            }
        });
    }
});

$(document).on("click", ".delete_file", function () {
    var file_index = $(this).data("file-index");
    deletedFiles.push($(this).data("id"));
    storedFiles.splice(file_index, 1);
    var file_name = $(this).data("file-name");
    var folderPath = $(".upload_path").val();
    var dataString = 'file_name=' + file_name + '&folderPath=' + folderPath;
    $("#file" + file_index).remove();
    var filesCount = storedFiles.length;
    if (filesCount === 1) {
    } else {
    }
    $.ajax({
        method: 'POST',
        url: BASE_URL + "admin/dashboard/delete_attachfile_from_folder",
        data: dataString,
        success: function (response) {

        }
    });
});

$(document).on('click', '.delete_file_req', function () {
    var r = confirm('Are you sure you want to delete this file?');
    if (r == true) {
        var reqid = $(this).attr('data-id');
        var filename = $(this).attr('data-name');
        $.ajax({
            type: "POST",
            url: BASE_URL + "admin/dashboard/delete_file_req",
            data: { "request_id": reqid, "filename": filename },
            success: function (data) {
                window.location.reload();
            }
        });
    } else {

    }
});

function statuschange(request_id, value) {
    $('#myModal').modal('show');
    $('#request_id_status').val(request_id);
    var status = $(value).val();
    $('#valuestatus').val(status);
    if (status === "hold" || status === "unhold") {
        var status_flag = $(value).find(':selected').attr('data-status');
    } else {
        var status_flag = '0';
    }
    //    console.log("request_id",request_id);
    //    console.log("status_flag",status_flag);
    //    console.log("status",status);
    $('#status_flag').val(status_flag);
    $('.msgal strong').html(status);
}

$('.btn-y').click(function () {
    var request_id = $('#request_id_status').val();
    var valueSelected = $('#valuestatus').val();
    var status = $('#status_flag').val();
    //    console.log("request_id",request_id);
    //    console.log("valueSelected",valueSelected);
    //    console.log("status",status);
    if (status === "0") {
        $.ajax({
            type: "POST",
            url: BASE_URL + "admin/dashboard/change_project_status",
            dataType: "json",
            data: {
                'request_id': request_id,
                'value': valueSelected,
            },
            success: function (response) {
                if (response == '1') {
                    $('#statuspass').css('display', 'block');
                    setTimeout(function () {
                        $('#statuspass').fadeOut();
                        location.reload();
                    }, 2000);
                } else {
                    $('#statusfail').css('display', 'block');
                    setTimeout(function () {
                        $('#statusfail').fadeOut();
                        location.reload();
                    }, 2000);
                }
            }
        });
    } else {
        $.ajax({
            type: 'POST',
            url: BASE_URL + "admin/Dashboard/hold_unhold_project_status",
            data: { status: status, data_id: request_id },
            success: function (data) {
                window.location.reload();
            }
        });
    }
});

//Project hold unhold script    
$(document).on('change', '.switch-custom-usersetting-check.project_on_hold input[type="checkbox"]', function () {
    var status = $(this).prop('checked');
    var data_id = $(this).attr('data-request_id');
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    if (ischecked) {
        $(".checkstatus_" + data_id).text('Hold');
    } else {
        $(".checkstatus_" + data_id).text('Hold');
    }
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/Dashboard/hold_unhold_project_status",
        data: { status: status, data_id: data_id },
        success: function (data) {
            window.location.reload();
        }
    });
});


$(document).on('click', ".remove_selected", function (e) {
    e.preventDefault();
    $(this).closest('.dropify-preview').css('display', 'none');
    $(this).closest('.file-drop-area').find('span').show();
    $(this).closest('.file-drop-area').find('input').val('');
    $(this).closest('.file-drop-area').find('.file-msg').html('Drag and drop file here or <span class="nocolsl">Click Here</span>');
});

function validateAndUploadS(input) {
    var URL = window.URL || window.webkitURL;
    var file = input.files[0];
    var exten = $(input).val().split('.').pop();
    var imgext = ['jpg', 'jpeg', 'png', 'gif', 'PNG', 'JPG', 'JPEG'];

    if (exten == "zip" || exten == "rar") {
        $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-archive-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
        $('.goup_1 .file-drop-area .dropify-preview').show();
        $('.goup_1 .file-drop-area span').hide();
    } else if (exten == "docx" || exten == "doc") {
        $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-word-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
        $('.goup_1 .file-drop-area .dropify-preview').show();
        $('.goup_1 .file-drop-area span').hide();
    } else if (exten == "pdf") {
        $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-pdf-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
        $('.goup_1 .file-drop-area .dropify-preview').show();
        $('.goup_1 .file-drop-area span').hide();
    } else if (exten == "txt") {
        $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-text-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
        $('.goup_1 .file-drop-area .dropify-preview').show();
        $('.goup_1 .file-drop-area span').hide();
    } else if (exten == "psd") {
        $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
        $('.goup_1 .file-drop-area .dropify-preview').show();
        $('.goup_1 .file-drop-area span').hide();
    } else if (exten == "ai") {
        $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
        $('.goup_1 .file-drop-area .dropify-preview').show();
        $('.goup_1 .file-drop-area span').hide();
    } else if (jQuery.inArray(exten, imgext) != '-1') {
        console.log(URL.createObjectURL(file));
        $(".goup_1 .file-drop-area .dropify-preview").html('<img src="' + URL.createObjectURL(file) + '" class="img_dropify" style="height: 150px; width: 100%;"><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
        $('.goup_1 .file-drop-area .dropify-preview').show();
        $('.goup_1 .file-drop-area span').hide();
    } else {
        $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg">You have selected <strong>' + exten + '</strong> file</h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
        $('.goup_1 .file-drop-area .dropify-preview').show();
        $('.goup_1 .file-drop-area span').hide();
    }
}
function validateAndUploadP(input) {
    var URL = window.URL || window.webkitURL;
    var file = input.files[0];
    if (file) {
        $(".goup_2 .file-drop-area span").hide();
        $(".goup_2 .file-drop-area .dropify-preview").css('display', 'block');
        $(".goup_2 .img_dropify").attr('src', URL.createObjectURL(file));
    }
}
// multiple file Upload script
function formatFileSize(bytes, decimalPoint) {
    if (bytes == 0)
        return '0 Bytes';
    var k = 1000,
        dm = decimalPoint || 2,
        //sizes = ['Bytes','KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
         sizes = ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
        i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}
var $droparea = $('.project-file-drop-area');
var storedFiles = [];
var deletedFiles = [];
// highlight drag area
$('#file_input').on('dragenter focus click', function () {
    $droparea.addClass('is-active');
});
// back to normal state
$('#file_input').on('dragleave blur drop', function () {
    $droparea.removeClass('is-active');
});


//--------Client listing page------------
function ValidDate(d) {
    d = new String(d)
    var re1 = new RegExp("[0|1][0-9]\/[1-2][0-9]{3}");
    var re2 = new RegExp("^[1[3-9]]");
    var re3 = new RegExp("^00");
    var expir_date = re1.test(d) && (!re2.test(d)) && (!re3.test(d));
    if (expir_date === true) {
        $('#expir_date').val(d);
    } else {
        $('#expir_date').val("");
    }
}
$(function () {
    $("#bypasspayment").click(function () {
        if ($(this).is(":checked")) {
            $(this).val(1);
            $("#div_bypass").hide();
            $('#expir_date').removeAttr('required');
            $('#card_number').removeAttr('required');
            $('#cvc').removeAttr('required');
        } else {
            $("#div_bypass").show();
            $(this).val(0);
            $('#expir_date').attr('required', 'required');
            $('#card_number').attr('required', 'required');
            $('#cvc').attr('required', 'required');
        }
    });
});

$(document).on('click', ".delete_customer", function () {
    var customer_id = $(this).attr('data-customerid');
    $('#id').val(customer_id);
    $('#confirmation').click();
});

$('.btn-yes').click(function () {
    var customer_id = $('#id').val();
    $.ajax({
        type: "POST",
        url: BASE_URL + "admin/dashboard/deletecustomer",
        data: {
            id: customer_id
        },
        success: function (data) {
            //            if (data == 1) {
            window.location.reload();
            //}
        }
    });
});

$(document).on('click', ".permanent_assign_design", function () {
    var customer_id = $(this).attr('data-customerid');
    $('#AddPermaDesign #assign_customer_id').val(customer_id);
});

//add customer validation 
function validate() {
    var email = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
    var fname = new RegExp('^[A-Za-z ]+$');
    var lname = new RegExp('^[A-Za-z ]+$');
    var mob = new RegExp('^[0-9]+$');
    var passwords = new RegExp('^[A-Za-z0-9@#]+$');
    //    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    if (!fname.test($('.firstname').val())) {

        $(".firstname");
        $(".firstname").focus();
        $('.epassError.frstname').css({ "display": "block" });
        $('.epassError.frstname .alert').css({ "display": "block" });
        return false;
    }
    else {

        $(".firstname");
        $('.epassError.frstname').css({ "display": "none" });
        $('.epassError.frstname .alert').css({ "display": "none" });
    }
    if (!lname.test($('.lastname').val())) {

        $('.lastname');
        $(".lastname").focus();
        $('.epassError.lstname').css({ "display": "block" });
        $('.epassError.lstname .alert').css({ "display": "block" });
        return false;
    }
    else {

        $(".lastname");
        $('.epassError.lstname').css({ "display": "none" });
        $('.epassError.lstname .alert').css({ "display": "none" });
    }
    if (!email.test($('.emailid').val())) {

        $(".emailid");
        $(".emailid").focus();
        $('.epassError.mailadrs').css({ "display": "block" });
        $('.epassError.mailadrs .alert').css({ "display": "block" });
        return false;
    }
    else {

        $(".emailid");
        $('.epassError.mailadrs').css({ "display": "none" });
        $('.epassError.mailadrs .alert').css({ "display": "none" });
    }
    if (!mob.test($('.phone').val())) {

        $(".phone").focus();
        $('.phone');
        $('.epassError.phnnmr').css({ "display": "block" });
        $('.epassError.phnnmr .alert').css({ "display": "block" });
        return false;
    }
    else {

        $(".phone");
        $('.epassError.phnnmr').css({ "display": "none" });
        $('.epassError.phnnmr .alert').css({ "display": "none" });
    }
    if (!passwords.test($('.password').val())) {

        $(".password").focus();
        $('.password');
        return false;
    }
    else {

        $(".password");
    }
    if (!passwords.test($('.cpassword').val())) {
        $(".cpassword").focus();
        $('.cpassword');
        return false;
    }
    else {
        if ($('.password').val() != $('.cpassword').val()) {
            $(".password").focus();
            $('.password');
            $('.cpassword');
            $('.passError').css({ "display": "block" });
            $('.passError .alert').css({ "display": "block" });
            return false;
        }
        else {
            $(".password");
            $(".cpassword");
            $('.passError').css({ "display": "none" });
            $('.passError .alert').css({ "display": "none" });
        }
    }

    return true;
}

function EditFormSQa(id) {
    var email = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
    var fname = new RegExp('^[A-Za-z ]+$');
    var lname = new RegExp('^[A-Za-z ]+$');
    var mob = new RegExp('^[0-9]+$');
    var password = new RegExp('^[A-Za-z0-9@#]+$');
    //var password = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    if (!fname.test($('#' + id + ' .efirstname').val())) {
        $('#' + id + ' .efirstname');
        $(".efirstname").focus();
        return false;
    }
    else {
        $('#' + id + ' .efirstname');
    }
    if (!lname.test($('#' + id + ' .elastname').val())) {
        $('#' + id + ' .elastname');
        $('#' + id + ' .elastname').focus();
        return false;
    }
    else {
        $('#' + id + ' .elastname');
    }
    if (!email.test($('#' + id + ' .eemailid').val())) {
        $('#' + id + ' .eemailid');
        $('#' + id + ' .eemailid').focus();
        return false;
    }
    else {
        $('#' + id + ' .eemailid');
    }
    if (!mob.test($('#' + id + ' .ephone').val())) {
        $('#' + id + ' .ephone').focus();
        $('#' + id + ' .ephone');
        return false;
    }
    else {
        $('#' + id + ' .ephone');
    }
    if ($('#' + id + ' .epassword').val() != "") {
        if (!password.test($('#' + id + ' .epassword').val())) {
            $('#' + id + ' .epassword').focus();
            $('#' + id + ' .epassword');
            return false;
        }
        else {
            $('#' + id + ' .epassword');
        }
    }
    if ($('#' + id + ' .eCpassword').val() != "") {
        if ($('#' + id + ' .epassword').val() != $('.ecpassword').val()) {
            $('#' + id + ' .epassword').focus();
            $('#' + id + ' .epassword');
            $('#' + id + ' .ecpassword');
            $('#' + id + ' .epassError').css({ "display": "block" });
            $('.epassError .alert').css({ "display": "block" });
            return false;
        }
        else {
            $('#' + id + ' .epassword');
            $('#' + id + ' .ecpassword');
        }
    }
    return true;
}

function load_more_clientlist(is_loaded, currentstatus, tabActive, isonetime, search, activetabid) {
    if (is_loaded != '') {
        var dataloaded = is_loaded.attr('data-isloaded');
    } else {
        dataloaded = '';
    }
    //if(dataloaded){
    var assign_project = ASSIGN_PROJECT;
    var activetab = $('#status_check_client li.active a').attr('href');
    var status_Active = $('#status_check_client li.active a').attr('data-status');
    var tabid;
    if (tabActive != '') {
        tabid = tabActive;
    } else {
        tabid = activetab;
    }
    var activeTabPane = $('.tab-pane.active');
    var status_scroll = (currentstatus != '') ? currentstatus : status_Active;
    var searchval = $('.search_text').val();
    //console.log(search);
    //console.log(dataloaded);
    if (dataloaded != '1' || search == '2') {
        $.post(BASE_URL + 'admin/dashboard/load_more_customer', { 'group_no': 0, 'status': status_scroll, 'search': searchval, 'isonetime': isonetime, 'assign_project': assign_project, 'activetabid': activetabid },
            function (data) {
                $('.ajax_loader').css('display', 'none');
                var newTotalCount = 0;
                if (data != '') {
                    if (is_loaded != '') {
                        is_loaded.attr('data-isloaded', '1');
                    }
                    
                    $(".ajax_searchload").fadeOut(500);
                    $(tabid + " .product-list-show .add-rows .two-can").html(data);
                    newTotalCount = $(tabid + " .product-list-show .add-rows").find("#loadingAjaxCount").attr('data-value');
                    $(tabid + " .product-list-show .add-rows").find("#loadingAjaxCount").remove();
                    activeTabPane.attr("data-total-count", newTotalCount);
                    activeTabPane.attr("data-loaded", $rowperpage);
                    activeTabPane.attr("data-group", 1);
                } else {
                    activeTabPane.attr("data-total-count", 0);
                    activeTabPane.attr("data-loaded", 0);
                    activeTabPane.attr("data-group", 1);
                    $(tabid + " .product-list-show .add-rows .two-can").html("<div class='no_data_ajax'>No record found</div>");
                }
                //        console.log("$rowperpage",$rowperpage);
                //        console.log("newTotalCount",newTotalCount);
                if ($rowperpage >= newTotalCount) {
                    //activeTabPane.find('.load_more').css("display", "none");
                    $('.load_more').css('display', 'none');
                } else {
                    //console.log("aayga");
                    //activeTabPane.find('.load_more').css("display", "inline-block");
                    $('.load_more').css('display', 'block');
                }
            });
    }
    else {
        $('.ajax_loader').css('display', 'none');
        $('.load_more').css('display', 'block');
    }
    //}
}

$('.searchdata_client').keyup(function (e) {
    var onetime = $('#status_check_client li.active a').data('is_single');
    e.preventDefault();
    delay(function () {
        $('.ajax_loader').css('display', 'flex');
        $(".ajax_searchload").fadeIn(500);
        load_more_clientlist('', '', '', onetime, '2');
    }, 1000);
});

$('.search_client_ajax').click(function (e) {
    e.preventDefault();
    var onetime = $('#status_check_client li.active a').data('is_single');
    load_more_clientlist('', '', '', onetime, '2');
});

$(document).on('change', '.cust_status', function () {
    // $('.cust_status').on('change',function() {
    var status = $(this).prop('checked');
    var data_id = $(this).attr('data-id');
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/dashboard/change_customer_status_active_inactive",
        data: { status: status, data_id: data_id },
        success: function (data) {
        }
    });
});

$(document).on('click', '.see_subuser', function () {
    var custID = $(this).attr('data-custid');
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/dashboard/getSubusers",
        data: { cust_id: custID },
        success: function (data) {
            $('#ShowSubusers').html(data);
        }
    });

});

$('#status_check_client li a').click(function (e) {
    $('.ajax_loader').css('display', 'flex');
    $('.load_more').css('display', 'none');
    var currentstatus = $(this).data('status');
    var activetabid = $(this).parent().attr('id');
    var isonetime;
    if (($(this).data('is_single')) == '1') {
        isonetime = '1';
    } else {
        isonetime = '0';
    }
    var tab = $(this).attr('href');
    load_more_clientlist($(this), currentstatus, tab, isonetime, activetabid);
});

//---------------Designer listing------------
function valid_Date() {
    var email = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
    var fname = new RegExp('^[A-Za-z ]+$');
    var lname = new RegExp('^[A-Za-z ]+$');
    var mob = new RegExp('^[0-9]+$');
    //var password = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    var password = new RegExp('^[A-Za-z0-9@#]+$');
    if (!fname.test($('.firstname').val())) {
        $(".firstname");
        $(".firstname").focus();
        $('.epassError.frstname').css({ "display": "block" });
        $('.epassError.frstname .alert').css({ "display": "block" });
        return false;
    }
    else {
        $(".firstname");
        $('.epassError.frstname').css({ "display": "none" });
        $('.epassError.frstname .alert').css({ "display": "none" });
    }
    if (!lname.test($('.lastname').val())) {
        $('.lastname');
        $(".lastname").focus();
        $('.epassError.lstname').css({ "display": "block" });
        $('.epassError.lstname .alert').css({ "display": "block" });
        return false;
    }
    else {
        $(".lastname");
        $('.epassError.lstname').css({ "display": "none" });
        $('.epassError.lstname .alert').css({ "display": "none" });
    }
    if (!email.test($('.emailid').val())) {
        $(".emailid");
        $(".emailid").focus();
        $('.epassError.mailadrs').css({ "display": "block" });
        $('.epassError.mailadrs .alert').css({ "display": "block" });
        return false;
    }
    else {
        $(".emailid");
        $('.epassError.mailadrs').css({ "display": "none" });
        $('.epassError.mailadrs .alert').css({ "display": "none" });
    }
    if (!mob.test($('.phone').val())) {
        $(".phone").focus();
        $('.phone');
        $('.epassError.phnnmr').css({ "display": "block" });
        $('.epassError.phnnmr .alert').css({ "display": "block" });
        return false;
    }
    else {
        $(".phone");
        $('.epassError.phnnmr').css({ "display": "none" });
        $('.epassError.phnnmr .alert').css({ "display": "none" });
    }
    if (!password.test($('.password').val())) {
        $(".password").focus();
        $('.password');
        return false;
    }
    else {
        $(".password");
    }
    if ($('.password').val() != $('.cpassword').val()) {
        $(".password").focus();
        $('.password');
        $('.cpassword');
        $('.passError').css({ "display": "block" });
        $('.passError .alert').css({ "display": "block" });
        return false;
    }
    else {
        $(".password");
        $(".cpassword");
        $('.passError').css({ "display": "none" });
        $('.passError .alert').css({ "display": "none" });
    }
    return true;
}
$(document).on('click', '.confirmationdelete', function (e) {
    e.preventDefault();
    thisbtn = $(this);
    designerid = $(this).attr('data-designerid');
    $(document).on('click', '#confirmationdelete .btn-yd', function (e) {
        $.ajax({
            type: "POST",
            url: BASE_URL + "admin/dashboard/delete_designer",
            data: {
                'designerid': designerid
            },
            success: function (response) {
                //            console.log("deleted");
                //            window.location.reload();
                location.reload();
            }
        });
    });
});

$(document).on('change', '.en_dis_designer', function () {
    var designer_id = $(this).attr('data-designerID');
    var status = '';
    if ($('.ena_dis_' + designer_id).is(":checked")) {
        status = 'enable';
    } else {
        status = 'disable';
    }
    if (status) {
        $.ajax({
            type: "POST",
            url: BASE_URL + "admin/dashboard/enabledisable_designer",
            data: {
                'designerid': designer_id,
                'status': status
            },
            success: function (response) {
                if (response == 1) {
                    //                    $('#disable' + designerid + '.confirmation').show();
                    //                    $('#enable' + designerid + '.confirmation').hide();
                }
            }
        });
    }
});

$(document).on('change', '.en_dis_qa', function () {
    var qa_id = $(this).attr('data-qaid');
    //  console.log('qa_id',qa_id);
    var is_active;
    if ($('.ena_dis_' + qa_id).is(":checked")) {
        is_active = '1';
    } else {
        is_active = '0';
    }
    if (is_active) {
        $.ajax({
            type: "POST",
            url: BASE_URL + "admin/dashboard/activateQa",
            data: {
                'qa_id': qa_id,
                'is_active': is_active
            },
            success: function (response) {
                if (response == 1) {
                    //                    $('#disable' + designerid + '.confirmation').show();
                    //                    $('#enable' + designerid + '.confirmation').hide();
                }
            }
        });
    }
});

//var status;
//var designerid;
//var thisbtn;
//$(document).on('click', '.confirmation', function (e) {
//    e.preventDefault();
//    thisbtn = $(this);
//    status = $(this).attr('data-status');
//    designerid = $(this).attr('data-designerid');
//    $('#confirmation #message').html("Are you sure " + status + " this designer");
//});
//$(document).on('click', '#confirmation .btn-yy', function (e) {
//    if (status == "enable") {
//        $.ajax({
//            type: "POST",
//            url: BASE_URL+"admin/dashboard/enable_designer",
//            data: {
//                'designerid': designerid
//            },
//            success: function (response) {
//                if (response == 1) {
//                    $('#disable' + designerid + '.confirmation').show();
//                    $('#enable' + designerid + '.confirmation').hide();
//                }
//            }
//        });
//    } else if (status == "disable") {
//        $.ajax({
//            type: "POST",
//            url: BASE_URL+"admin/dashboard/disable_designer",
//            data: {
//                'designerid': designerid
//            },
//            success: function (response) {
//                if (response == 1) {
//                    $('#enable' + designerid + '.confirmation').show();
//                    $('#disable' + designerid + '.confirmation').hide();
//                }
//            }
//        });
//    }
//});

$('.search_designer_ajax').click(function (e) {
    e.preventDefault();
    load_more_designerlist();
});

$('.searchdata_designer').keyup(function (e) {
    e.preventDefault();
    delay(function () {
        $('.ajax_loader').css('display', 'flex');
        $(".ajax_searchload").fadeIn(500);
        load_more_designerlist();
    }, 1000);
});

function load_more_designerlist() {
    var activeTabPane = $('.tab-pane.active');
    var searchval = activeTabPane.find('.search_text').val();
    $.post(BASE_URL + 'admin/dashboard/load_more_designer', { 'group_no': 0, 'search': searchval },
        function (data) {
            $('.ajax_loader').css('display', 'none');
            var newTotalCount = 0;
            if (data != '') {
                //$('.ajax_loader').css('display', 'none');
                $(".ajax_searchload").fadeOut(500);
                $("#designerslist .two-can").html(data);
                newTotalCount = $('#designerslist').find("#loadingAjaxCount").attr('data-value');
                $("#designerslist").find("#loadingAjaxCount").remove();
                activeTabPane.attr("data-total-count", newTotalCount);
                activeTabPane.attr("data-loaded", $rowperpage);
                activeTabPane.attr("data-group", 1);
            } else {
                activeTabPane.attr("data-total-count", 0);
                activeTabPane.attr("data-loaded", 0);
                activeTabPane.attr("data-group", 1);
                $('#designerslist').html("");
            }
            if ($rowperpage >= newTotalCount) {
                activeTabPane.find('.load_more').css("display", "none");
            } else {
                activeTabPane.find('.load_more').css("display", "inline-block");
            }
        });
}
var optionDiv = ''
$(document).on('click', '.on-dot', function () {
    optionDiv = $(this).attr('data-clientID');
    $(".client_projects_counting_" + optionDiv).slideToggle();
});
$(document).mouseup(function (e) {
    var container = $('.client_projects_counting_' + optionDiv);
    var classnm = $('.on-dot');
    if (!classnm.is(e.target) && classnm.has(e.target).length === 0) {
        container.hide();
    }
});

$(document).on('click', '.confirmationqadelete', function (e) {
    e.preventDefault();
    thisbtn = $(this);
    designerid = $(this).attr('data-qaid');
    $(document).on('click', '#confirmationqadelete .btn-yq', function (e) {
        $.ajax({
            type: "POST",
            url: BASE_URL + "admin/dashboard/delete_designer",
            data: {
                'designerid': designerid
            },
            success: function (response) {
                //            console.log("deleted");
                //            window.location.reload();
                location.reload();
            }
        });
    });
});

/** user management js**/
$(document).on('click', '.delete_subuserdata', function (e) {
    var clientid = $(this).data('clientid');
    //    console.log("clientid",clientid);
    $("#subusersid").val(clientid);
});
$(document).on('click', '.backlist-go', function () {
    $("#new-subuser").css("display", "none");
    $("#new-subuser-client").css("display", "none");
    $("#sub-userlist").css("display", "block");
});

$inprogress_reqofuser = "";
$active_reqofuser = "";
$edituser = 0;

$(document).on("click", ".add_subuser_main", function () {
    //console.log("abc");
    $('#brandcheck').chosen();
    $("#sub-userlist").css("display", "none");
    $('.switch_access').prop('checked', true);
    $('#brandshowing').css('display', 'none');
    var user_flag = $(this).data("user_type");
    $(".user_flag").val(user_flag);
    if (user_flag == 'client') {
       // console.log(user_flag);
        $("#new-subuser-client").css("display", "block");
        $(".access-brand").hide();
        $(".requests_limit_toclient").show();
        $('.permissions_for_subuser_client').hide();
        $(".view_only_prmsn").hide();
        $('#add_requests').attr({ 'checked': true, 'disabled': true });
        $('#comnt_requests').attr({ 'checked': true, 'disabled': true });
        $('#del_requests').attr({ 'checked': true, 'disabled': true });
        $('#billing_module').attr({ 'checked': false, 'disabled': true });
        $('#app_requests').attr({ 'checked': true, 'disabled': true });
        $('#downld_requests').attr({ 'checked': true, 'disabled': true });
        $('#add_brand_pro').attr({ 'checked': true, 'disabled': true });
        $('#manage_priorities').attr({ 'checked': false, 'disabled': true });
    } else {
        //console.log("mgr",user_flag);
//        $('#sub-userlist').css('display','none');
        $("#new-subuser").css("display", "block");
        $(".view_only_prmsn").show();
        $('.permissions_for_subuser_client').show();
        $(".access-brand").show();
        $(".requests_limit_toclient").hide();
    }
});

$(document).on('click', '.edit_subuser', function (e) {
    $("#sub-userlist").css("display", "none");
    $edituser = 1;
    var editid = $(this).attr('data-id');
    var user_type = $(this).attr('data-user_type');
    if (user_type == 'client') {
        $("#new-subuser-client").css("display", "block");
        $(".access-brand").hide();
        $(".requests_limit_toclient").show();
        $('.permissions_for_subuser_client').hide();
        $(".view_only_prmsn").hide();
        $('#add_requests').attr({ 'checked': true, 'disabled': true });
        $('#comnt_requests').attr({ 'checked': true, 'disabled': true });
        $('#del_requests').attr({ 'checked': true, 'disabled': true });
        $('#billing_module').attr({ 'checked': false, 'disabled': true });
        $('#app_requests').attr({ 'checked': true, 'disabled': true });
        $('#downld_requests').attr({ 'checked': true, 'disabled': true });
        $('#add_brand_pro').attr({ 'checked': true, 'disabled': true });
        $('#manage_priorities').attr({ 'checked': false, 'disabled': true });
    } else {
        $("#new-subuser").css("display", "block");
        $(".access-brand").show();
        $('.permissions_for_subuser_client').show();
        $(".view_only_prmsn").show();
        $(".requests_limit_toclient").hide();
    }
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: baseUrl + '/customer/ChangeProfile/getSubuserData',
        data: { 'data': editid },
        success: function (data) {
            var subuserdata = data.sub_user_data[0];
            // console.log(subuserdata,'subuserdata');
            var sub_user_permissions = data.sub_user_permissions[0];
            var brandIDs = data.brandIDs;
            var totaladded_req = data.totaladded_req;
            var pendingreq = data.pendingreq;
            var requests_type = subuserdata.requests_type;
            console.log('id', 'option_id_' + subuserdata.total_active_req);
            // console.log(sub_user_permissions,'sub_user_permissions');
            var subuser_reqdata = jQuery.parseJSON(subusers_reqdata);
            $('.cust_id').val(subuserdata.id);
            $('.fname').val(subuserdata.first_name);
            $('.user_flag').val(user_type);
            $('.lname').val(subuserdata.last_name);
            $('.user_email').val(subuserdata.email);
            $('.phone').val(subuserdata.phone);
            $('.requests_type').val(subuserdata.requests_type);
            $('.total_requests').val(subuserdata.total_requests);
            $('.total_active_req').val(subuserdata.total_active_req);
            $('#option_id_' + subuserdata.total_active_req).attr('selected', true);
            if ($('.fname').val() != "") {
                $('.fname').parent().children('p').addClass("label-active");
            }
            if ($('.lname').val() != "") {
                $('.lname').parent().children('p').addClass("label-active");
            }
            if ($('.user_email').val() != "") {
                $('.user_email').parent().children('p').addClass("label-active");
            }
            if ($('.phone').val() != "") {
                $('.phone').parent().children('p').addClass("label-active");
            }
            if ($('.requests_type').val() != "") {
                $('.requests_type').parent().find('.label-txt').addClass("label-active");
            }
            if ($('.total_requests').val() != "") {
                $('.total_requests').parent().find('.label-txt').addClass("label-active");
            }
            if ($('.option_id_' + subuserdata.total_active_req).val() != "") {
                $('.option_id_' + subuserdata.total_active_req).parent().find('.label-txt').addClass("label-active");
            }


            $('.total_one_time_req .tool-text').append('<br><span>Total Requests: ' + subuserdata.total_requests + '<br>Active Requests: ' + totaladded_req + '<br>Pending Request: ' + pendingreq + '</span>');

            //deduct requests of this user from all sub user requests
            $inprogress_reqofuser = parseInt(subuser_reqdata.subtotal_inprogress_req) - parseInt(subuserdata.total_inprogress_req);
            $active_reqofuser = parseInt(subuser_reqdata.subtotal_active_req) - parseInt(subuserdata.total_active_req);

            if (sub_user_permissions.brand_profile_access == 1) {
                $('.switch_access').prop('checked', true);
                $('#brandshowing').css('display', 'none');
            }
            if (sub_user_permissions.brand_profile_access == 0) {
                $('.switch_access').prop('checked', false);
                $('#brandshowing').css('display', 'block');
            }
            if (requests_type === 'per_month') {
                $('.requests_type').append('<option value="per_month" selected>Billing subscription based</option>');
                $('.billing_cycle_requests').val(subuserdata.billing_cycle_request);
                if ($('.billing_cycle_requests').val() != "") {
                    $('.billing_cycle_requests').parent().find('.label-txt').addClass("label-active");
                }
                $('.billing_cycle_req').show();
                $('.billing_cycle_requests').prop('required', true);
                $('.total_requests').prop('required', false);
                $('.total_active_req').prop('required', true);
                $('.total_one_time_req').hide();
                $('.total_active_req_sec').show();

            } else if (requests_type === 'one_time') {

                $('.billing_cycle_req').hide();
                $('.total_one_time_req').show();
                $('.total_requests').prop('required', true);
                $('.billing_cycle_requests').prop('required', false);
                $('.total_active_req').prop('required', false);
                $('.total_active_req_sec').hide();

            } else {
                $('.billing_cycle_req').hide();
                $('.total_one_time_req').hide();
                $('.total_requests').prop('required', false);
                $('.billing_cycle_requests').prop('required', false);
                $('.total_active_req').prop('required', true);
                $('.total_active_req_sec').show();
            }
            var arr = [];
            $(brandIDs).each(function (i, v) {
                arr.push(v.brand_id);
            });
            $('#brandcheck').chosen();
            $('#brandcheck').val(arr);
            $('#brandcheck').trigger("chosen:updated");

            if (sub_user_permissions.comment_on_req == 1) {
                $('#comnt_requests').prop('checked', true);
            }
            if (sub_user_permissions.add_brand_pro == 1) {
                $('#add_brand_pro').prop('checked', true);
            }
            if (sub_user_permissions.billing_module == 1) {
                $('#billing_module').prop('checked', true);
            }
            if (sub_user_permissions['approve/revision_requests'] == 1) {
                $('#app_requests').prop('checked', true);
            }
            if (sub_user_permissions.add_requests == 1) {
                $('#add_requests').prop('checked', true);
            }
            if (sub_user_permissions.delete_req == 1) {
                $('#del_requests').prop('checked', true);
            }
            if (sub_user_permissions.download_file == 1) {
                $('#downld_requests').prop('checked', true);
            }
            if (sub_user_permissions.view_only == 1) {
                $('#view_only').prop('checked', true);
            }
            if (sub_user_permissions.manage_priorities == 1) {
                $('#manage_priorities').prop('checked', true);
            }
            if(sub_user_permissions.file_management == 1){
                $('#file_management').prop('checked', true);
            }
            if(sub_user_permissions.white_label == 1){
                $('#white_label').prop('checked', true);
            }
        }
    });
});

$(document).on('change', '.requests_type', function () {
    var values = $(this).val();
    $('.total_requests').prop('required', false);
    if (values == 'per_month') {

        $('.billing_cycle_req').show();
        $('.billing_cycle_requests').prop('required', true);
        $('.total_requests').prop('required', false);
        $('.total_active_req').prop('required', true);
        $('.total_one_time_req').hide();
        $('.total_active_req_sec').show();

    } else if (values == 'one_time') {

        $('.billing_cycle_req').hide();
        $('.total_one_time_req').show();
        $('.total_requests').prop('required', true);
        $('.billing_cycle_requests').prop('required', false);
        $('.total_active_req').prop('required', false);
        $('.total_inprogress_req_sec').hide();
        $('.total_active_req_sec').hide();

    } else {
        $('.billing_cycle_req').hide();
        $('.total_one_time_req').hide();
        $('.billing_cycle_requests').prop('required', false);
        $('.total_requests').prop('required', false);
        $('.total_active_req').prop('required', true);
        $('.total_active_req_sec').show();
    }
});

$(document).on('change', '.switch-custom-usersetting-check.activate-user input[type="checkbox"]', function () {
    var status = $(this).prop('checked');
    var data_id = $(this).attr('data-userid');
    var data_email = $(this).attr('data-email');
    var data_name = $(this).attr('data-name');
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    if (ischecked) {
        $(".checkstatus_" + data_id).text('Active');
    } else {
        $(".checkstatus_" + data_id).text('Inactive');
    }
    $.ajax({
        type: 'POST',
        url: baseUrl + "customer/ChangeProfile/enable_disable_subuser_account",
        data: { status: status, data_id: data_id, data_email: data_email, data_name: data_name },
        success: function (data) {
            // window.location.reload();
        }
    });
});
$(document).on('change', '.switch_access', function () {
    var is_checked = $('.switch_access').is(":checked");
    brandsShow(is_checked)
});

function brandsShow(is_checked) {
    if (is_checked == false) {
        $('#brandshowing').css('display', 'block');
    } else {
        $('#brandshowing').css('display', 'none');
    }
}
$(document).on('change', '#view_only', function () {
    var is_view = $('#view_only').is(":checked");
    if (is_view == true) {
        $('#add_requests').attr('checked', false);
        $('#comnt_requests').attr('checked', false);
        $('#del_requests').attr('checked', false);
        $('#billing_module').attr('checked', false);
        $('#app_requests').attr('checked', false);
        $('#downld_requests').attr('checked', false);
        $('#add_brand_pro').attr('checked', false);
        $('#manage_priorities').attr('checked', false);
    }
});

$(document).on('change', '.uncheckview', function () {
    var is_view = $('#view_only').is(":checked");
    if (is_view == true) {
        $('#view_only').attr('checked', false);
    }
});
$(document).on('submit', '.sub_user_sbmt', function () {
    e.preventDefault();
    var subuser_reqdata = jQuery.parseJSON(subusers_reqdata);
    var user_flag = $("#user_flag").val();
    var total_inprogress_req = $("#total_inprogress_req").val();
    var total_active_req = $("#total_active_req").val();
    var total_requests = $("#total_requests").val();
    var requests_type = $("#requests_type").val();
    //    console.log('total_requests',total_requests);
    //    console.log('requests_type',requests_type);
    //    console.log('total_inprogress_req',total_inprogress_req);
    //    console.log('total_active_req',total_active_req);
    if (user_flag === 'client' && requests_type !== 'one_time') {
        if ($edituser === 0) {
            var inprogress_reqsum = parseInt(subuser_reqdata.subtotal_inprogress_req) + parseInt(total_inprogress_req);
            var active_reqsum = parseInt(subuser_reqdata.subtotal_active_req) + parseInt(total_active_req);
        } else {
            var inprogress_reqsum = parseInt($inprogress_reqofuser) + parseInt(total_inprogress_req);
            var active_reqsum = parseInt($active_reqofuser) + parseInt(total_active_req);
        }
        if (inprogress_reqsum > subuser_reqdata.main_inprogress_req) {
            $('.total_active_req_sec .error_msg').html('');
            $('.total_inprogress_req_sec .error_msg').html("<span class='validation'>You can't assign more than " + subuser_reqdata.main_inprogress_req + " dedicated requests, <a href='" + baseUrl + "customer/setting-view#billing' target='_blank'>click here</a> to purchase more requests.</span>");
            return false;
        } else if (active_reqsum > subuser_reqdata.main_active_req) {
            $('.total_active_req_sec .error_msg').html("<span class='validation'>You can't assign more than " + subuser_reqdata.main_active_req + " dedicated requests, <a href='" + baseUrl + "customer/setting-view#billing' target='_blank'>click here</a> to purchase more requests.</span>");
            $('.total_inprogress_req_sec .error_msg').html('');
            return false;
        } else {
            return true;
        }
    } else if (requests_type == 'one_time') {
        if (total_requests > 0) {
            return true;
        } else {
            $('.total_one_time_req .error_msg').html("<span class='validation'>Please enter total number of requests user can add</span>");
            $('.total_inprogress_req_sec .error_msg').html('');
            $('.total_active_req_sec .error_msg').html('');
            return false;
        }
    } else {
        return true;
    }


});
/** end user management js**/

/** assign VA js**/
$(document).on('click', ".assign_va", function () {
    var request_id = $(this).attr('data-requestid');
    $('#Addva #allcustomers_id').val(request_id);
});
$(document).on('change', '.selected_custmr', function () {
    var requestIDs = [];
    $.each($("input[name='customer']:checked"), function () {
        if ($(this).val() != 'on') {
            requestIDs.push($(this).val());
        }
    });
    //    console.log("requestIDs",requestIDs);
    $('.assign_va').attr('data-requestid', requestIDs);
});
$(".checkallcus").change(function () {
    var attribute = $(this).attr('data-attr');
    //console.log("attribute",attribute);
    if ($(this).is(':checked')) {
        $('.' + attribute).prop("checked", true);
    } else {
        $('.' + attribute).prop("checked", false);
    }
    var requestIDs = [];
    $.each($("input[name='customer']:checked"), function () {
        if ($(this).val() != 'on') {
            requestIDs.push($(this).val());
        }
    });
    //console.log("requestIDs",requestIDs);
    $('.assign_va').attr('data-requestid', requestIDs);
});

$('#assign_vatocus').click(function (e) {
    e.preventDefault();
    var assign_va = $('input[name=assign_va]:checked').val();
    var customer_id = $('#allcustomers_id').val();
    var designer_name = $('input[name=assign_va]:checked').attr('data-name');
    $.ajax({
        url: BASE_URL + 'admin/Dashboard/assign_va_tocustomer',
        type: 'POST',
        data: {
            'assign_va': assign_va,
            'customer_id': customer_id
        },
        success: function (data) {
            var returnedData = JSON.parse(data);
            //         console.log('returnedData',returnedData);
            if (returnedData.status == 'success') {
                $('#close-pop').click();
                $("." + customer_id + " p.text-h").html(designer_name);
                //                 console.log("."+customer_id+" p.text-h",designer_name);
                //  $("."+customer_id+" a.adddesinger").attr('data-designerid',designer_id);
            } else {
                $('#show_error').html(returnedData.msg);
                //             console.log(returnedData.msg);
                $('.alert-dismissable.error').css('display', 'block');
            }
        }
    });
});
/** end assign VA js**/


/******start Client profile js**********/
//$(document).on('submit','#about_info_client_form',function(e){
//    e.preventDefault();
//    var user_id = $('#user_id').val();
//    var formData = new FormData($(this)[0]);
////    console.log(user_id);
////    console.log(formData);
//    $.ajax({
//        type: 'POST',
//        url: BASE_URL+'admin/accounts/customer_edit_profile/' + user_id,
//        data: formData,
//        async: false,
//        cache: false,
//        contentType: false,
//        processData: false,
//        success: function (data) {
//        //alert(data);
//       // location.reload(true);
//    }
//});
//});

$(document).on('change', '.switch-custom_toggle input[type="checkbox"]', function () {
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    $(this).val(ischecked);
});

$(document).on('change', '.f_filesharing input[type="checkbox"]', function () {
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    $(this).val(ischecked);
});

$(document).on('submit', '#custom_active_req', function (e) {
    e.preventDefault();
    var user_id = $('#curuser_id').val();
    var formData = new FormData($(this)[0]);
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'admin/accounts/customer_active_requests/' + user_id,
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            //alert(data);
            location.reload(true);
        }
    });
});
//console.log(total_inprogress_req);
//if(typeof total_inprogress_req != 'undefined' || total_inprogress_req != ''){
//if(subscription_plan){
//     subscription_plan = jQuery.parseJSON(subscription_plan);
// }
// 
// jQuery(document).ready(function ($) {
//    var agencyPlanOptions = '';
//    $.each(subscription_plan,function(k,v){
//        var value = parseInt(k)*parseInt(total_active_requests);
//        if (k == total_inprogress_req)
//        {
//         var selected = 'selected';
//                // var selected_price = parseInt(v.amount)/100 *parseInt(k);
//                var selected_price = parseInt(v.amount)/100;
//                $('.agency_price.change').html(selected_price);
//            }else{
//                selected = '';
//                //$('.agency_price').html(<?php echo AGENCY_PLAN_PRICE ?>);
//            }
//            agencyPlanOptions += '<option value="'+k+'" data-amount="'+v.amount+'"'+selected+'>'+value+'</option>';
//        });
//
//    $('.pricebasedquantity').html(agencyPlanOptions);
//    jQuery('.pricebasedquantity').on('change', function () {
//        var  subscription_plan_detail = subscription_plan[this.value];
//        var Subs_price = subscription_plan_detail.amount/100;
//        var subs_quantity = this.value;
//                //var final_SubPrice = Subs_price*subs_quantity;
//                var final_SubPrice = Subs_price; 
//                jQuery('.upgrd_agency_pln').attr('data-price', final_SubPrice);
//                jQuery('.toatal-bill-p.text-right').html('<span class="appened_price">$' + final_SubPrice +'</span>');
//                jQuery('.upgrd_agency_pln').attr('data-inprogress', subs_quantity);
//                jQuery('span.agency_price').html(final_SubPrice);
//                jQuery('.disc-code').val('');  
//                jQuery('.ErrorMsg').css('display','none');
//                jQuery('.coupon-des-newsignup').css('display','none');
//            });
//});
//}
//$(document).on('change', '.switch-brandprofile input[type="checkbox"]', function () {
//    var status = $(this).prop('checked');
//    if(status !== false){
//        console.log("brand true",status);
//        $(".input_sizeofbrand").css('display','block');
//    }else{
//        console.log("brand false",status);
//        $(".input_sizeofbrand").css('display','none');
//    }
//});
// $(document).on('change', '.switch-custom_default input[type="checkbox"]', function () {
//    var status = $(this).prop('checked');
//   // var data_id = $(this).attr('data-cid');
//    var check_showhide = $('.switch-custom_toggle input[type="checkbox"]').prop('checked');
//    if(check_showhide !== false){
//        console.log("check_showhide",check_showhide);
//        $(".input_sizeofuser").css('display','block');
//    }
//    if(status === false){
//        $(".change_default_setting").css('display','inline-block');
//        console.log("show");
//    }else{
//        $(".change_default_setting").css('display','none');
//        $(".input_sizeofuser").css('display','none');
//        console.log("hide");
//    }
//    $.ajax({
//        type: 'POST',
//        url: BASE_URL+"admin/accounts/show_hide_user_managemnt",
//        data: {status: status, data_id: data_id,setting:'default'},
//        success: function (data) {
//        }
//    });
//});

//$(document).on('change', '.switch-custom_toggle input[type="checkbox"]', function () {
//  //  var status = $(this).prop('checked');
//    var data_id = $(this).attr('data-cid');
//    var ischecked = ($(this).is(':checked')) ? 1 : 0;
//    if (ischecked) {
//        $(".checkstatus_" + data_id).text('Show');
//        $(".input_sizeofuser").css('display','block');
//    } else {
//        $(".checkstatus_" + data_id).text('Hide');
//         $(".input_sizeofuser").css('display','none');
//    }
//    $.ajax({
//        type: 'POST',
//        url: BASE_URL+"admin/accounts/show_hide_user_managemnt",
//        data: {status: status, data_id: data_id,setting:'custom'},
//        success: function (data) {
//        }
//    });
//});

/******** Cancel Subscription **********/
$(document).on('change', '.cancel_subs_toggle input[type="checkbox"]', function () {
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    var user_id = $(this).attr('data-userid');
    var subscription_id = userCurrentplan;
    $(".error_msg_section").html("");
    if (ischecked == '1') {
        var cnfrm = confirm("Are you sure, you want to cancel subscription for this user ?");
        if (cnfrm == true) {
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: BASE_URL + 'admin/accounts/cancel_subscription',
                data: { "user_id": user_id, "subscription_id": subscription_id },
                success: function (data) {
                    console.log(data);
                    if (data.status == 1) {
                        $('.cancel_subs_toggle span').html('Cancelled');
                        $('#switch_can').prop('disabled', true);
                    } else {
                        $(".error_msg_section").html(data.msg);
                    }
                }
            });
        }else{
             $(this).attr('checked', false);
        }
    }
});

$(document).on('click', ".ChngPln", function (e) {
    e.preventDefault();
    var data_id = $(this).attr('data-value');
    var data_price = $(this).attr('data-price');
    var data_inprogress = $(this).attr('data-inprogress');
    jQuery('.plan_price').val(data_price);
    jQuery('.in_progress_request').val(data_inprogress);
    $(".plan_name_to_upgrade").val(function () {
        return data_id;
    });
});
/******end Client profile js**********/
/********start message list**************/
$(document).on('click', '.change_tab1', function () {
    //console.log(this);
    $(this).addClass('active');
    $(this).siblings().removeClass('active');
});

$(document).on('click', '.mark_messages', function () {
    var status = $(this).attr('data-status');
    var project_id = $(this).attr('data-id');
    var active_project = $('.open').attr('data-id');
    var active_chat = $('.open').attr('data-chat');
    $('.loading_msg').html('<div class="loading"><span class="loading_process"><img src="' + $asset_path + 'img/gz-ajax-loader.gif" alt="loading"/></span></div>');
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/messages/mark_read_unread",
        data: { status: status, 'project_id': project_id, active_project: active_project, active_chat: active_chat },
        success: function (data) {
            if (data == 1) {
                if (status == 'mark_read') {
                    $('#list_' + project_id).removeClass('unread_msg');
                    $('#list_' + project_id).addClass('read');
                    $('.chatbox-add .numcircle-box-01').css("display", "none");
                    $(".open").addClass('read');
                    $(".open").removeClass('unread');
                } else if (status == 'mark_unread') {
                    $('#list_' + project_id).removeClass('read');
                    $('#list_' + project_id).addClass('unread_msg');
                    $(".open").addClass('unread');
                    $(".open.unread .chatbox-add").html("<p class='unread_circle'></p>");
                    $(".open").removeClass('read');
                } else if (status == 'mark_star') {
                    $("#list_" + project_id + " .my_fvrt").addClass('star_project');
                    $("#list_" + project_id + " .my_fvrt").attr('data-status', 'remove_star');
                    $(".add_star").css('display', 'none');
                    $(".remove_star").css('display', 'block');
                } else if (status == 'remove_star') {
                    $("#list_" + project_id + " .my_fvrt").removeClass('star_project');
                    $("#list_" + project_id + " .my_fvrt").attr('data-status', 'mark_star');
                    $(".add_star").css('display', 'block');
                    $(".remove_star").css('display', 'none');
                }
            }
            $('.loading_msg').html('');
        }
    });
});
$(document).on('click', '.project_id_get_msg', function () {
    var project_id = $(this).attr('data-id');
    $(this).closest('.project_sectionfr_msg ').addClass('active');
    $(this).closest('.project_sectionfr_msg ').siblings().removeClass('active');
    $('.loading_msg').html('<div class="loading"><span class="loading_process"><img src="' + $asset_path + 'img/gz-ajax-loader.gif" alt="loading"/></span></div>');
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/messages/message_chat",
        data: { project_id: project_id, 'type': 1 },
        success: function (data) {
            $("#allChats").html(data);
            $('.loading_msg').html('');
        }
    });
});

$(document).on('click', '.get_comment_chat', function () {
    var project_id = $(this).attr('data-id');
    $(this).closest('li').addClass('open');
    $(this).closest('li').siblings().removeClass('open');
    $('.loading_msg').html('<div class="loading"><span class="loading_process"><img src="' + $asset_path + 'img/gz-ajax-loader.gif" alt="loading"/></span></div>');
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/messages/project_comment_chat",
        data: { project_id: project_id },
        success: function (data) {
            $("#message_chat_area").html(data);
            $('.loading_msg').html('');
        }
    });
});

$(document).on('click', '.show_main_chat', function () {
    var project_id = $(this).attr('data-id');
    $(this).closest('li').addClass('open');
    $(this).closest('li').siblings().removeClass('open');
    //    console.log(project_id);
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/messages/message_chat",
        data: { project_id: project_id, 'type': 2 },
        success: function (data) {
            $("#message_chat_area").html(data);
        }
    });
});

$(document).on('click', '.show_trans', function (e) {
    $(".transcopy").slideToggle()
});
function read_unread_message(active_status) {
    if (active_status == 'read') {
        $('.project_sectionfr_msg.read').css('display', 'flex');
        $('.project_sectionfr_msg.unread_msg').css('display', 'none');
    } else {
        $('.project_sectionfr_msg.unread_msg').css('display', 'flex');
        $('.project_sectionfr_msg.read').css('display', 'none');
    }
}
$(".msg-list").scroll(function () {
    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
        var data_count = parseInt($("#load_more_messages").attr("data-count"));
        var data_seen = $("#load_more_messages").attr("data-seen");
        $("#load_more_messages").attr("data-count", data_count + 1);
        $.ajax({
            type: 'POST',
            url: BASE_URL + "admin/messages/load_more_messages",
            data: { data_count: data_count, data_seen: data_seen },
            success: function (data) {
                $(".msg-list").append(data);
            }
        });
    }
});
/********end message list**************/

$(document).on('click', '.openchange', function (e) {
    var chatid = $(this).data('chatid');
    $(".open-edit_" + chatid).slideToggle()
});

$(document).on('click', '.editmsg', function () {
    var editid = $(this).data('editid');
    $('.msg_desc_' + editid).css('display', 'none');
    $('.open-edit_' + editid).css('display', 'none');
    $('.edit_main_' + editid).css('display', 'block');
});

$(document).on('click', '.edit_save', function () {
    var editid = $(this).data('id');
    var msg = $("#edit_main_msg_" + editid).val();
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/Request/Editmsg",
        data: { editid: editid, msg: msg },
        success: function (data) {
            if (data) {
                $('.edit_icon_' + editid).html('<i class="fas fa-pen"></i>');
                $('.took_' + editid).html('<div class="edited_msg">' + msg + '</div>');
                $('.msg_desc_' + editid).css('display', 'block');
                $('.edit_main_' + editid).css('display', 'none');
            }
        }
    });
});
$(document).on('click', '.cancel_main', function () {
    var msgid = $(this).data('msgid');
    $('.edit_main_' + msgid).css('display', 'none');
    $('.msg_desc_' + msgid).css('display', 'block');
});

$(document).on('click', '.deletemsg', function () {
    var r = confirm('Are you sure you want to delete this message?');
    if (r == true) {
        var delid = $(this).data('delid');
        $.ajax({
            type: 'POST',
            url: BASE_URL + "admin/Request/Deletemsg",
            data: { delid: delid },
            success: function (data) {
                if (data) {
                    //console.log(delid);
                    $('.editdeletetoggle_' + delid).css('display', 'none');
                    $('.took_' + delid).html('<div class="edited_msg deleted_msg"><i class="fa fa-ban" aria-hidden="true"></i> You have deleted this message.</div>');
                }
            }
        });
    }
});


/********start admin profile***********/
$(document).on('change', '.enable_emails input[type="checkbox"]', function () {
    var status = $(this).prop('checked');
    var key = $(this).attr('data-key');
    var data_id = $(this).attr('data-cid');
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/dashboard/change_customer_status_settings",
        data: { status: status, data_id: data_id, data_key: key },
        success: function (data) {
        }
    });
});
$('#about_info_form').submit(function (e) {
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'admin/dashboard/customer_edit_profile',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            window.location.replace(BASE_URL + "admin/dashboard/adminprofile");
        }
    });
});

$('#edit_form_data').submit(function (e) {
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'admin/dashboard/edit_profile_image_form',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            window.location.replace(BASE_URL + "admin/dashboard/adminprofile?status=1");
        }
    });
});

/********end admin profile***********/

/***********admin reporting start*********/
var forEach = function (array, callback, scope) {
    for (var i = 0; i < array.length; i++) {
        callback.call(scope, i, array[i]);
    }
};
window.onload = function () {
    var max = -219.99078369140625;
    forEach(document.querySelectorAll('.progress'), function (index, value) {
        percent = value.getAttribute('data-progress');
        value.querySelector('.fill').setAttribute('style', 'stroke-dashoffset: ' + ((100 - percent) / 100) * max);
        value.querySelector('.value').innerHTML = percent + '%';
    });
}

$('#requests_category_count').on('change', function () {
    var value = $(this).val();
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'admin/report/dashboard',
        dataType: "json",
        async: false,
        data: { "value": value, "category_flag": 1 },
        success: function (response) {
            var categorydata = '';
            $.each(response, function (k, v) {
                categorydata += '<li>' + k + '<span>' + v + '</span></li>';
            });
            $('.catgory_based_count').html(categorydata);
        },
        error: function (response) {
        }
    });
});
$(document).on('change', '.filter_chart', function () {
    var start_date = $(this).val();
    var end_date = $('option:selected', this).attr('data-date');
    if (start_date == 'range') {
        $('.datepicker_tax_sec').show();
        $('#tax_datepicker_start').val('');
        $('#tax_datepicker_end').val('');
    } else {
        $('.datepicker_tax_sec').hide();
        $.ajax({
            type: 'POST',
            url: BASE_URL + 'admin/report/tax_report',
            dataType: "json",
            async: false,
            data: { "start_date": start_date, "end_date": end_date },
            success: function (response) {
                if (response.status == '1') {
                    var taxreport = new CanvasJS.Chart("Tax_report", {
                        animationEnabled: true,
                        theme: "light2",
                        title: {
                            text: ""
                        },
                        axisY: {
                            includeZero: false
                        },
                        data: [{
                            type: "spline",
                            dataPoints: response.tax_report
                        }]
                    });
                    taxreport.render();
                } else {
                    $('#Tax_report').html('<div class="taxt_error_msg">Data Not Found</div>');
                }
            },
            error: function (response) {
                console.log('error response', response);
            }
        });
    }
});

$('#datepicker_tax').on('submit', function (e) {
    e.preventDefault();
    var datepicker_start = $('#tax_datepicker_start').val();
    var datepicker_end = $('#tax_datepicker_end').val();
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'admin/report/tax_report',
        dataType: "json",
        data: { "start_date": datepicker_start, "end_date": datepicker_end },
        success: function (response) {
            if (response.status == '1') {
                var taxreport = new CanvasJS.Chart("Tax_report", {
                    animationEnabled: true,
                    theme: "light2",
                    title: {
                        text: ""
                    },
                    axisY: {
                        includeZero: false
                    },
                    data: [{
                        type: "spline",
                        dataPoints: response.tax_report
                    }]
                });
                taxreport.render();
            } else {
                //console.log('kdfgidfiuyd');  
                $('#Tax_report').html('<div class="taxt_error_msg">Data Not Found</div>');
            }
        },
        error: function (response) {
        }
    });
});

var selectCurrentWeek = function () {
    window.setTimeout(function () {
        $('.ui-datepicker-calendar > tr').find('.ui-datepicker-current-day > a').addClass('ui-state-active');
    }, 10000);
}
if ($('.week-picker').length == '1') {
    var startDate;
    var endDate;
    $('.week-picker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        onSelect: function (dateText, inst) {
            var date = $(this).datepicker('getDate');
            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
            var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
            $('#startDate').text($.datepicker.formatDate(dateFormat, startDate, inst.settings));
            $('#endDate').text($.datepicker.formatDate(dateFormat, endDate, inst.settings));
            $('.hiphen_hide').css('display', 'inline-block');
            var start = $('#startDate').text();
            var end = $('#endDate').text();
            var selected_pro_type = $('.new-project').val();
            selectCurrentWeek();
            getWeekdate(start, end, selected_pro_type);
        },
        beforeShowDay: function (date) {
            var cssClass = '';
            if (date >= startDate && date <= endDate)
                cssClass = 'ui-datepicker-current-day';
            return [true, cssClass];
        },
        onChangeMonthYear: function (year, month, inst) {
            selectCurrentWeek();
        }
    });
}

if ($('.week-picker_customers').length > '0') {
    var startDate;
    var endDate;
    $('.week-picker_customers').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        onSelect: function (dateText, inst) {
            var date = $(this).datepicker('getDate');
            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
            var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
            $('#users_startDate').text($.datepicker.formatDate(dateFormat, startDate, inst.settings));
            $('#users_endDate').text($.datepicker.formatDate(dateFormat, endDate, inst.settings));
            $('.users_hiphen_hide').css('display', 'inline-block');
            var start = $('#users_startDate').text();
            var end = $('#users_endDate').text();
            selectCurrentWeek();
            getWeeklyusers(start, end);
        },
        beforeShowDay: function (date) {
            var cssClass = '';
            if (date >= startDate && date <= endDate)
                cssClass = 'ui-datepicker-current-day';
            return [true, cssClass];
        },
        onChangeMonthYear: function (year, month, inst) {
            selectCurrentWeek();
        }
    });
}

if ($('.week-picker_utmusers').length > '0') {
    var startDate;
    var endDate;
    $('.week-picker_utmusers').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        onSelect: function (dateText, inst) {
            var date = $(this).datepicker('getDate');
            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
            var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
            $('#utmusers_startDate').text($.datepicker.formatDate(dateFormat, startDate, inst.settings));
            $('#utmusers_endDate').text($.datepicker.formatDate(dateFormat, endDate, inst.settings));
            $('.utmusers_hiphen_hide').css('display', 'inline-block');
            var start = $('#utmusers_startDate').text();
            var end = $('#utmusers_endDate').text();
            selectCurrentWeek();
            getWeeklyutmusers(start, end);
        },
        beforeShowDay: function (date) {
            var cssClass = '';
            if (date >= startDate && date <= endDate)
                cssClass = 'ui-datepicker-current-day';
            return [true, cssClass];
        },
        onChangeMonthYear: function (year, month, inst) {
            selectCurrentWeek();
        }
    });
}

$(document).on('change', '.new-project', function () {
    var selected_pro_type = $(this).val();
    var start_date = $('#startDate').text();
    var endDate = $('#endDate').text();
    getWeekdate(start_date, endDate, selected_pro_type);
});

function getWeeklyusers(start, end) {
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'admin/report/getweeklyusers',
        dataType: "json",
        async: false,
        data: { "start": start, "end": end },
        success: function (response) {
            if (response.status == '1') {
                var chart = new CanvasJS.Chart("Newusers", {
                    animationEnabled: true,
                    theme: "light2",
                    title: {
                        text: ""
                    },
                    axisY: {
                        includeZero: false
                    },
                    data: [{
                        type: "spline",
                        dataPoints: response.linechart
                    }]
                });
                chart.render();
            } else {
                $('#Newusers').html('<div class="taxt_error_msg">Data Not Found</div>');
            }
        },
        error: function (response) {
            console.log('error response', response);
        }
    });
}

function getWeeklyutmusers(start, end) {
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'admin/report/getWeeklyutmusers',
        dataType: "json",
        async: false,
        data: { "start": start, "end": end },
        success: function (response) {
            if (response.status == '1') {
                var chart = new CanvasJS.Chart("Utmusers", {
                    animationEnabled: true,
                    theme: "light2",
                    title: {
                        text: ""
                    },
                    axisY: {
                        includeZero: false
                    },
                    data: [{
                        type: "column",
                        dataPoints: response.linechart
                    }]
                });
                chart.render();
            } else {
                $('#Utmusers').html('<div class="taxt_error_msg">Data Not Found</div>');
            }
        },
        error: function (response) {
            console.log('error response', response);
        }
    });
}

function getWeekdate(start, end, selected_pro_type) {
    if (selected_pro_type != '') {
        selected_pro_type = selected_pro_type;
    } else {
        selected_pro_type = '';
    }
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'admin/report/getlinechartData',
        dataType: "json",
        async: false,
        data: { "start": start, "end": end, 'selected_pro_type': selected_pro_type },
        success: function (response) {
            if (response.status == '1') {
                var chart = new CanvasJS.Chart("myChart", {
                    animationEnabled: true,
                    theme: "light2",
                    title: {
                        text: ""
                    },
                    axisY: {
                        includeZero: false
                    },
                    data: [{
                        type: "spline",
                        dataPoints: response.linechart
                    }]
                });
                chart.render();
            } else {
                $('#myChart').html('<div class="taxt_error_msg">Data Not Found</div>');
            }
        },
        error: function (response) {
            console.log('error response', response);
        }
    });
}

function dateFormatset(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = year + "-" + month + "-" + day;

    return date;
};

$(document).on('submit', '#datepicker_range', function (e) {
    e.preventDefault();
    var start_date = $('#datepicker_start').val();
    var end_date = $('#datepicker_end').val();
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'admin/report/designer_overview_DateRange',
        dataType: "json",
        data: { "start_date": start_date, "end_date": end_date },
        success: function (response) {
            var avgdayscompletion, avgperday, avgdraft, avg_quality;
            $.each(response, function (k, v) {
                if (v.avgperday == undefined) {
                    avgperday = 0;
                } else {
                    avgperday = v.avgperday;
                }
                $('.avgperday_' + v.id).text(avgperday);
                if (v.avgdaysforcompletion == undefined) {
                    avgdayscompletion = 0;
                } else {
                    avgdayscompletion = v.avgdaysforcompletion;
                }
                $('.avgdaysforcompletion_' + v.id).text(avgdayscompletion);
                if (v.avgdraft == undefined) {
                    avgdraft = 0;
                } else {
                    avgdraft = v.avgdraft;
                }
                $('.avgdraft_' + v.id).text(avgdraft);
                if (v.average_total_quality == undefined) {
                    avg_quality = 0;
                } else {
                    avg_quality = (v.average_total_quality) + '%';
                }
                $('.avg_quality_' + v.id).text(avg_quality);
            });
        },
        error: function (response) {
            // console.log('error response', response);
        }
    });
});

$(document).on('submit', '#taxes_datepicker_range', function (e) {
    e.preventDefault();
    var start_date = $('#taxes_start').val();
    var end_date = $('#taxes_end').val();
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'admin/report/getTotalTaxpaid',
        dataType: "json",
        data: { "start_date": start_date, "end_date": end_date },
        success: function (response) {
            if (response.status == 'success') {
                var html = '',statename;
                $.each(response.data, function (k, v) {
                    if(v.state != null || v.state != ''){
                        statename = v.state;
                    }else{
                        statename = 'N/A';
                    }
                    html += "<li class='state_name'>" + statename + "<span class='tax_count'>" + v.totaltax + "</span></li>";
                });
                $('.state_based_taxcount').html(html);
            } else {
                $('.state_based_taxcount').html('No data found');
            }
        },
        error: function (response) {
        }
    });
});

$(document).on('submit', '#msg_datepicker_range', function (e) {
    e.preventDefault();
    var start_date = $('#msgstart_date').val();
    var end_date = $('#endmsg_date').val();
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'admin/report/messages_report',
        dataType: "json",
        data: { "start_date": start_date, "end_date": end_date },
        success: function (response) {
            if (response) {
                $('.all_msg h4').html(response.all);
                $('.customer_msg h4').html(response.customer);
                $('.team_msg h4').html(response.team);
                $('.discuss_msg h4').html(response.discussion);
            }
        },
        error: function (response) {
        }
    });
});
/********* admin reporting end ***********/

/********* admin contentmanagement start ***********/
$('.searchdata_blog').keyup(function (e) {
    ajax_call();
});

$('.search_data_blog').click(function (e) {
    e.preventDefault();
    ajax_call();
});

function ajax_call() {
    var search = $('#search_text').val();
    var status = $('#status').val();
    if (status == "blog") {
        $.get(BASE_URL + "admin/contentmanagement/search_ajax_blog?title=" + search, function (data) {
            var content_id = $('a[data-status="' + status + '"]').attr('href');
            $('#blog_list').html(data);
        });
    } else if (status == "portfolio") {
        $.get(BASE_URL + "admin/contentmanagement/search_ajax_portfolio?title=" + search, function (data) {
            var content_id = $('a[data-status="' + status + '"]').attr('href');
            $('#portfolio_list').html(data);
        });
    }
}
$(document).on('click', '.delete_blog', function () {
    var delID = $(this).attr('data-delID');
    var cnfrm = confirm('Are you sure you want to delete this blog ?');
    if (cnfrm == true) {
        window.location.href = BASE_URL + "admin/contentmanagement/delete_blog/" + delID;
    }
});
function validateAndUploadblog(input) {
    var URL = window.URL || window.webkitURL;
    var file = input.files[0];
    if (file.type.indexOf('image/') !== 0) {
        this.value = null;
        //console.log("invalid");
    }
    else {
        if (file) {
            console.log(URL.createObjectURL(file));
            $(".blogimgadd").hide();
            $(".blogPic").css('display', 'block');
            $(".blogPic .blogimage").attr('src', URL.createObjectURL(file));
            $('.blogimg_hidden').val(URL.createObjectURL(file));
        }
    }
}
$('.remove_selected_blogimg').click(function (e) {
    e.preventDefault();
    $(this).closest('.blogPic').hide();
    $('.blogimgadd').show();
    $('.blogimgadd input').val('');
});
$(document).on('click', '.delete_portfolio', function () {
    var delID = $(this).attr('data-delID');
    var cnfrm = confirm('Are you sure you want to delete this portfolio ?');
    if (cnfrm == true) {
        window.location.href = BASE_URL + "admin/contentmanagement/delete_portfolio/" + delID;
    }
});
/***category listing******/
$(document).on('click', '.show_child', function () {
    var id = $(this).attr('data-child_id');
    $('.plus_' + id).html('-');
    $(this).addClass('minus');
    //  $("child_"+id).slideToggle();
    $('.child_' + id).css('display', 'table-row');
});
$(document).on('click', '.delete_cat', function () {
    var r = confirm("Are you sure you want to delete this ?");
    var id = $(this).attr('data-id');
    if (r == true) {
        $.ajax({
            type: "POST",
            url: BASE_URL + "admin/categories/delete_category",
            data: { "category_id": id },
            success: function (data) {
                // if(data){
                location.reload(true);
                // }
            }
        });
    }
});
$(document).on('click', '.edit_cat', function (e) {
    var id = $(this).attr('data-id');
    $("#file-uploadNameEdit").hide(); 
    $('#cat_id').val(id);
 $.ajax({
        type: "POST",
        url: BASE_URL + "admin/categories/edit_detailcat",
        data: { "category_id": id },
        success: function (data) {
            var returnedData = JSON.parse(data);
            console.log("returnedData",returnedData);   
            if (returnedData) {
                $.each(returnedData, function (index, value) {
                    var targetid =value.parent_id;
                     $("#EditCategoryForm .showimg").show();
                     $("#EditCategoryForm .name").parents("label").show();
                    if(value.parent_id == 0){
                        $("#EditCategoryForm .showimg").hide();
                        $("#EditCategoryForm .name").parents("label").hide();
                        targetid =value.id;
                    }
                    if(value.agency_only == 1){
                      $('.showimg').addClass('agency_file');  
                    }else{
                     $('.showimg').removeClass('agency_file');     
                    }
                    $('#EditCategoryForm .name').val(value.name);
                    $('#EditCategoryForm .name').parent().find(".label-txt").addClass('label-active');
                    $('#EditCategoryForm .position').val(value.position);
                    $('#EditCategoryForm .position').parent().find(".label-txt").addClass('label-active');
                    $('#EditCategoryForm .timeline').val(value.timeline);
                    $('#EditCategoryForm .timeline').parent().find(".label-txt").addClass('label-active');
                     
                    $('#option-id-'+ targetid).siblings().prop('selected',false); 
                    $('#option-id-'+ targetid).prop("selected", true);
                    $('#Editparent_cat').parents("label").find('.label-txt').addClass("label-active");
                    $('#EditCategoryForm .parent_cat').parent().find(".label-txt").addClass('label-active');
                    $('#EditCategoryForm select[name^="bucket_type"] option[value="' + value.bucket_type + '"]').attr("selected", "selected");
                    $('#EditCategoryForm .bucket_type').parent().find(".label-txt").addClass('label-active');
                    if (value.image_url) {
                        
                        $("#EditCategoryForm .showimg").attr("src", $asset_path + 'img/customer/customer-images/' + value.image_url);
                    }
                    if (value.is_active == '1') {
                        $('#EditCategoryForm .active').prop('checked', true);
                    }else{
                        $('#EditCategoryForm .active').prop('checked', false);
                    }
                });
            }
        }
    });
});
$(document).on('click', '.update_cat', function (e) {
    var id = $(this).attr('data-id');
    $.ajax({
        type: "POST",
        url: BASE_URL + "admin/categories/update_category",
        data: { "category_id": id },
        success: function (data) {
        }
    });
});
$(document).on('click', '.minus', function () {
    var id = $(this).attr('data-child_id');
    $('.plus_' + id).html('+');
    $('.child_' + id).css('display', 'none');
    $(this).removeClass('minus');
});

$(document).on('click', '.notifi_loading', function (e) {
     $(".ajax_loader").css("display","flex");
    var row = parseInt($(this).attr('data-row'));
    row = parseInt(row) + parseInt($rowperpage);
    var activeTabPaneGroupNumber = $(this).attr('data-group');
    var allcount = $(this).attr("data-count");
    var allLoaded = $(this).attr("data-loaded");
    //    console.log("row",row);
    //    console.log("activeTabPaneGroupNumber",activeTabPaneGroupNumber);
    //    console.log("allcount",allcount);
    //    console.log("allLoaded",allLoaded);
    //if (allLoaded < allcount) {
        $.post(BASE_URL + 'admin/Contentmanagement/load_more_notifications', { 'group_no': activeTabPaneGroupNumber },
            function (data) {
                if (data != "") {
                    $(".ajax_loader").css("display", "none");
                    $(".list-notificate").append(data);
                    row = parseInt(row) + parseInt($rowperpage);
                    $(".notifi_loading").css("display", "inline-block");
                    $('.notifi_loading').attr('data-row', row);
                    activeTabPaneGroupNumber++;
                    $('.notifi_loading').attr('data-group', activeTabPaneGroupNumber);
                    allLoaded = parseInt(allLoaded) + parseInt($rowperpage);
                    //console.log('$rowperpage',$rowperpage);
                    //console.log('row',row);
                    // console.log('allLoaded',allLoaded);
                    //console.log('allLoaded',allcount);
                    $('.notifi_loading').attr('data-loaded', allLoaded);
                    if (allLoaded >= allcount) {
                        $(this).css("display", "none");
                        $(".ajax_loader").css("display", "none");
                    } else {
                        $(this).css("display", "inline-block");
                        $(".ajax_loader").css("display", "none");
                    }
                }
            });
   // }
});
//$('#usermanagemnt_setting').submit(function (e) {
//    e.preventDefault();
//    var default_setting = $('.switch-custom_default input[type="checkbox"]').prop('checked');
//    var custom_setting = $('.switch-custom_toggle input[type="checkbox"]').prop('checked');
//    var usersize = $('#users_size').val();
//    if(default_setting !== false){
//       var setting = 'default';
//       var status = default_setting;
//    }else{
//       var setting = 'custom'; 
//       var status = custom_setting;
//    }
//    var data_id = $("#settinguser_id").val();
//     $.ajax({
//        type: 'POST',
//        url: BASE_URL+"admin/accounts/show_hide_user_managemnt",
//        data: {status: status, data_id: data_id,setting:setting,usersize:usersize},
//        success: function (data) {
//        }
//    });
//    
//});
//$(document).on('change', '#showhide_usrmanagement', function () {
//   var value = $(this).val();
//   if(value === "1"){
//       $('.isadd_subuser').css("display","block");
//   }else{
//       $('.isadd_subuser').css("display","none");
//   }
//});
//$(document).on('change', '#showhide_brandprofile', function () {
//   var value = $(this).val();
//   if(value === "1"){
//       $('.isadd_brand_profile').css("display","block");
//   }else{
//       $('.isadd_brand_profile').css("display","none");
//   }
//});
/********* admin contentmanagement end ***********/
$(document).ready(function () {
    $(".filter-site").click(function () {
        $(".gz-filter").addClass("show");
    });
    $(".close-filter").click(function () {
        $(".gz-filter").removeClass("show");
    });
});
/*******new***********/

//$(document).on('click', '.delete_cust_request', function () {
//    var delID = $(this).data('req_id');
//    var con = confirm("Are you sure, you want to delete this request ?");
//    if (con == true) {
//        $.ajax({
//            type: 'POST',
//            url: BASE_URL + "admin/dashboard/deleteRequest",
//            data: { id: delID },
//            success: function (data) {
//                if (data == 'success') {
//                    $('#remove_' + delID).hide();
//                }
//            }
//        });
//    }
//});
/****filters**********/
$(document).on('change', '.designers_show', function () {
    var selectedval = $(this).val();
    if (selectedval == 'designers') {
        $('.designers_list').css('display', 'block');
    } else {
        $('.designers_list').css('display', 'none');
    }
});

$(document).on('change', '.des_name', function () {
    var val = [];
    $.each($("input[name='des_name']:checked"), function () {
        val.push($(this).val());
    });
});

$('#search_client').keyup(function () {
    var clientname = $(this).val();
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/dashboard/filter_clients_show",
        data: { search_client: clientname },
        success: function (data) {
        }
    });
});

$(document).ready(function () {
    $("#search_client").keyup(function () {
        var clientname = $(this).val();
        $.ajax({
            type: 'POST',
            url: BASE_URL + "admin/dashboard/filter_clients_show",
            dataType: JSON,
            data: { search_client: clientname },
            success: function (data) {
                console.log(data);
                var returnedData = JSON.parse(data);
                $.each(returnedData, function (i, val) {
                    var html = '<li><input type="checkbox" class="cli_name" name="cli_name[]" value=' + val.id + '/>' + val.first_name + '</li>';
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(html);
                    $("#search-box").css("background", "#FFF");
                });
            }
        });
    });
});

$(window).resize(function(){
    Autoresizingcategory();     
});


$(document).on('change', '.add_des_scrl,.checkAll', function () {  
    var requestIDs = [];
    //if($(window).scrollTop() > '370'){
    $.each($("input[name='project[]']:checked"), function () {
        if ($(this).val() != 'on') {
            requestIDs.push($(this).val());
        }
    });
    if(requestIDs.length >= '1'){
         $('.adddesinger').addClass('relate_check');
    }else{
        $('.adddesinger').removeClass('relate_check');
    }
});

function Autoresizingcategory(){
if ($(window).width() <= 800) { 
     $(document).on('click', '.show_child', function () {
        var id = $(this).attr('data-child_id');
        console.log("show id on less than 800",id);
        $('.plus_' + id).html('-');
        $(this).addClass('minus');
        //  $("child_"+id).slideToggle();
        $('.child_' + id).css('display', 'block');
    });
    
    $(document).on('click', '.minus', function () {
        var id = $(this).attr('data-child_id');
        $('.plus_' + id).html('+');
        $('.child_' + id).css('display', 'none');
        $(this).removeClass('minus');
    });
    }
}

function dateFormat(ths) {
    var val = $(ths).val();
    if (val.length == 2) {
        $(ths).val($(ths).val() + '/');
    }
}
$("#load_more_aboanded").click(function () {
    $this = $(this);
    var limit = $(this).attr("data-limit");
    var start = $(this).attr("data-row");
    var count = $(this).attr("data-count");
    $(".ajax_loader").show();
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'admin/Contentmanagement/abandoned_loadmore',
        data: {"limit": limit, "start": start},
        dataType: "json",
        success: function (responce) {
            $(".ajax_loader").hide();
            var html = "";
            var total_load = "";
            if (responce != "") {

                $.each(responce.data, function (k, value) {

                    var firstname = "", lastname = "", email = "", state = "", created = "", modified = "";
                    var index = parseInt(start) + parseInt(k + 1);
                    total_load = responce.data.length;

                    if (value.first_name != null) {
                        firstname = value.first_name;
                    }
                    if (value.last_name != null) {
                        lastname = value.last_name;
                    }
                    if (value.email != null) {
                        email = value.email;
                    }
                    if (value.state != null) {
                        state = value.state;
                    }
                    if (value.state != null) {
                        state = value.state;
                    }

                    html += '<div class="row two-can"><div class="cli-ent-row tr brdr"> <div class="cli-ent-col td" style="width: 30%">' + firstname + " " + lastname + '</div><div class="cli-ent-col td" style="width: 30%">' + email + '</div><div class="cli-ent-col td" style="width: 20%">' + state + '</div><div class="cli-ent-col td" style="width: 20%">' + responce.timezone[k].created_at + '</div></div></div>';
                });

                var new_row = parseInt(start) + parseInt(total_load);

                $this.attr("data-row", new_row);
                if (count == new_row) {
                    $this.fadeOut();
                }
                $("#abond_data").append(html);


            }
        }

    });
});

/**************Reporting section *******************/
$(document).on("click", "#load_more_reports", function () {
        countTimer();
        $(".ajax_loader").css("display", "flex");
        $(".load_more_rep").css("display", "none");
        var row = parseInt($(this).attr('data-row'));
        row = row + $rowperpage;
        var searchval = $('.search_text').val();
        var allcount = parseInt($('.data_total_count').attr('data-total-count'));
        var slug = $('.slug').attr('slug');
        var num = parseInt($('.num').attr('num'));
        var allLoaded = parseInt($('.data_loaded').attr('data-loaded'));
        var activeTabPaneGroupNumber = $('.data_group').attr('data-group');
//        console.log("allcount",allcount);
//        console.log("allLoaded",allLoaded);
        if (allLoaded < allcount) {
            $.post(BASE_URL + 'admin/Contentmanagement/load_more_reports', { 'slug': slug,'num': num,'group_no': activeTabPaneGroupNumber, 'search': searchval},
                function (data) {
                    if (data != "") {
                        //console.log('data h');
                        $(".product-list-show .row").append(data);
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more_rep").css("display", "inline-block");
                        $('.load_more_rep').attr('data-row', row);
                        activeTabPaneGroupNumber++;
                        $('.data_group').attr('data-group', activeTabPaneGroupNumber);
                        allLoaded = allLoaded + $rowperpage;
                        $('.data_loaded').attr('data-loaded', allLoaded);
                        if (allLoaded >= allcount) {
                            $('.load_more_rep').css("display", "none");
                            $(".ajax_loader").css("display", "none");
                        } else {
                            $('.load_more_rep').css("display", "inline-block");
                            $(".ajax_loader").css("display", "none");
                        }
                    }
                });
        }
    });
    
    
$('.searchdata_reports').keyup(function (e) {
    //console.log("xcvxcv");
    e.preventDefault();
    delay(function () {
        $('.ajax_loader').css('display', 'flex');
        $(".ajax_searchload").fadeIn(500);
        load_more_reports();
    }, 1000);
});

function load_more_reports() {
    countTimer();
    var row = parseInt($(this).attr('data-row'));
    row = row + $rowperpage;
    var searchval = $('.search_text').val();
    var allcount = parseInt($('.data_total_count').attr('data-total-count'));
    var slug = $('.slug').attr('slug');
    var num = parseInt($('.num').attr('num'));
    var allLoaded = parseInt($('.data_loaded').attr('data-loaded'));
    var activeTabPaneGroupNumber = $('.data_group').attr('data-group');
    delay(function () {
        $('.ajax_loader').css('display', 'none');
    }, 5000);
    var searchval = $('.search_text').val();
    //if (dataloaded != '1' || search == '2') {
        $.post(BASE_URL + 'admin/Contentmanagement/load_more_reports', { 'slug': slug,'num': num,'group_no': activeTabPaneGroupNumber, 'search': searchval},
            function (data) {
                var newTotalCount = 0;
                if (data != '') {
                    $('.ajax_loader').css('display', 'none');
                    $(".ajax_searchload").fadeOut(500);
                    $(".product-list-show .row").fadeOut(200, function () {
                        $(".product-list-show .row").html(data);
                        $(".product-list-show .row").fadeIn(800);
                    });
                    newTotalCount = $('.data_total_count').attr('data-total-count');
                    $(".product-list-show .row").find("#loadingAjaxCount").remove();
                    $('.data_total_count').attr("data-total-count", newTotalCount);
                    $('.data_loaded').attr("data-loaded", $rowperpage);
                    $('.data_group').attr("data-group", 1);
                } else {
                    $('.data_total_count').attr("data-total-count", 0);
                    $('.data_loaded').attr("data-loaded", 0);
                    $('.data_group').attr("data-group", 1);
                    $(".product-list-show .row").html("<div class='no_data_ajax'>No record found</div>");
                }
                if ($rowperpage >= newTotalCount || newTotalCount == undefined) {
                    $('.load_more_rep').css("display", "none");
                } else {
                    delay(function () {
                        $('.load_more_rep').css('display', 'inline-block');
                    }, 1000);
                }
            });
//    } else {
//        $('.ajax_loader').css('display', 'none');
//        $('.load_more').css('display', 'block');
//    }
    }
/***********category questions***********/
$(document).on('change','#quest_category',function(){
    var cat_id = $( "#quest_category option:selected" ).val();
     $('.multicat').val(cat_id);
    //$('#selected_subcat_id').val('');
    $.ajax({
        type: 'POST',
        url: BASE_URL+'admin/contentmanagement/get_subcategorybasedoncat/'+cat_id,
        data: {"cat_id":cat_id},
        dataType: "json",
        success: function (responce) {
            var $select = $('#quest_subcategories');
            $select.find('option').remove();
            $select.append('<option value="" selected="">Select option</option>'); 
            $.each(responce,function(key, value){
                  $select.append('<option value=' + value.id + '>' + value.name + '</option>'); // return empty
            });  
        }
    });
});

$(document).on('change','#quest_subcategories',function(){
    var subcat_id = $( "#quest_subcategories option:selected" ).val();
    $('.multisubcat').val(subcat_id);
});

$(document).on('click','.delete_quest',function(){
var delID = $(this).data('questid');
    var con = confirm("Are you sure, you want to delete this question ?");
    if (con == true) {
        window.location.href = BASE_URL + "admin/contentmanagement/deleteQuest/"+delID; 
    }
});

$(document).on('change','.quest_type',function(){
    var question_type = $(this).find('option:selected').val();
    if(question_type == 'selectbox' || question_type == 'radio' || question_type == 'checkbox'){
         $(this).closest('.questions_data').find('.question_options').css('display','block');
    }else{
        $(this).closest('.questions_data').find('.question_options').css('display','none');
    }
});

$(document).on('blur','#question_options',function(){
    var options = $(this).val();
    $('#question_options').val(options);
});

$('.searchdata_questions').keyup(function (e) {
    e.preventDefault();
    delay(function () {
        $('.ajax_loader').css('display', 'flex');
        $(".ajax_searchload").fadeIn(500);
        load_more_questions();
    }, 1000);
});

$(document).on("click", "#load_more_questions", function () {
        countTimer();
        $(".ajax_loader").css("display", "flex");
        $(".load_more_questions").css("display", "none");
        var row = parseInt($(this).attr('data-row'));
        row = row + $rowperpage;
        var searchval = $('.search_text').val();
        var allcount = parseInt($('.data_total_count').attr('data-total-count'));
        var slug = $('.slug').attr('slug');
        var num = parseInt($('.num').attr('num'));
        var allLoaded = parseInt($('.data_loaded').attr('data-loaded'));
        var activeTabPaneGroupNumber = $('.data_group').attr('data-group');
//        console.log("allcount",allcount);
//        console.log("allLoaded",allLoaded);
        if (allLoaded < allcount) {
            $.post(BASE_URL + 'admin/Contentmanagement/load_more_questions', { 'group_no': activeTabPaneGroupNumber, 'search': searchval},
                function (data) {
                    if (data != "") {
                        //console.log('data h');
                        $(".product-list-show .row").append(data);
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more_questions").css("display", "inline-block");
                        $('.load_more_questions').attr('data-row', row);
                        activeTabPaneGroupNumber++;
                        $('.data_group').attr('data-group', activeTabPaneGroupNumber);
                        allLoaded = allLoaded + $rowperpage;
                        $('.data_loaded').attr('data-loaded', allLoaded);
                        if (allLoaded >= allcount) {
                            $('.load_more_questions').css("display", "none");
                            $(".ajax_loader").css("display", "none");
                        } else {
                            $('.load_more_questions').css("display", "inline-block");
                            $(".ajax_loader").css("display", "none");
                        }
                    }
                });
        }
    });

 function load_more_questions(){
    var row = parseInt($(this).attr('data-row'));
    row = row + $rowperpage;
    var allcount = parseInt($('.data_total_count').attr('data-total-count'));
    var allLoaded = parseInt($('.data_loaded').attr('data-loaded'));
    var activeTabPaneGroupNumber = $('.data_group').attr('data-group');
    delay(function () {
        $('.ajax_loader').css('display', 'none');
    }, 5000);
    var searchval = $('.search_text').val();
    //if (dataloaded != '1' || search == '2') {
        $.post(BASE_URL + 'admin/Contentmanagement/load_more_questions', { 'group_no': activeTabPaneGroupNumber, 'search': searchval},
            function (data) {
                var newTotalCount = 0;
                if (data != '') {
                    $('.ajax_loader').css('display', 'none');
                    $(".ajax_searchload").fadeOut(500);
                    $(".product-list-show .row").fadeOut(200, function () {
                        $(".product-list-show .row").html(data);
                        $(".product-list-show .row").fadeIn(800);
                    });
                    newTotalCount = $('.data_total_count').attr('data-total-count');
                    $(".product-list-show .row").find("#loadingAjaxCount").remove();
                    $('.data_total_count').attr("data-total-count", newTotalCount);
                    $('.data_loaded').attr("data-loaded", $rowperpage);
                    $('.data_group').attr("data-group", 1);
                } else {
                    $('.data_total_count').attr("data-total-count", 0);
                    $('.data_loaded').attr("data-loaded", 0);
                    $('.data_group').attr("data-group", 1);
                    $(".product-list-show .row").html("<div class='no_data_ajax'>No record found</div>");
                }
                if ($rowperpage >= newTotalCount || newTotalCount == undefined) {
                    $('.load_more_questions').css("display", "none");
                } else {
                    delay(function () {
                        $('.load_more_questions').css('display', 'inline-block');
                    }, 1000);
                }
            });
}

$(document).on('change','#quest_default',function(){
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
//    console.log(ischecked);
    if(ischecked == 1){
        $(".labels_up").parents("label").addClass("feild_disable");
        //$('#quest_category').val('0');
        //$('#quest_subcategories').val('0');
        $(".make_default").prop('disabled',true);
        $("#quest_category").find('.labels_up').removeClass('labels_up');
        
    }else{
         $(".make_default").prop('disabled',false);
         $(".labels_up").parents("label").removeClass("feild_disable");
    }
});
/*********sample work********/

$(document).on("click", "#load_more_samples", function () {
        countTimer();
        $(".ajax_loader").css("display", "flex");
        $(".load_more_samples").css("display", "none");
        var row = parseInt($(this).attr('data-row'));
        row = row + $rowperpage;
        var searchval = $('.search_text').val();
        var allcount = parseInt($('.data_total_count').attr('data-total-count'));
        var slug = $('.slug').attr('slug');
        var num = parseInt($('.num').attr('num'));
        var allLoaded = parseInt($('.data_loaded').attr('data-loaded'));
        var activeTabPaneGroupNumber = $('.data_group').attr('data-group');
//        console.log("allcount",allcount);
//        console.log("allLoaded",allLoaded);
        if (allLoaded < allcount) {
            $.post(BASE_URL + 'admin/Contentmanagement/load_more_samples', { 'group_no': activeTabPaneGroupNumber, 'search': searchval},
                function (data) {
                    if (data != "") {
                        //console.log('data h');
                        $(".product-list-show .row").append(data);
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more_samples").css("display", "inline-block");
                        $('.load_more_samples').attr('data-row', row);
                        activeTabPaneGroupNumber++;
                        $('.data_group').attr('data-group', activeTabPaneGroupNumber);
                        allLoaded = allLoaded + $rowperpage;
                        $('.data_loaded').attr('data-loaded', allLoaded);
                        if (allLoaded >= allcount) {
                            $('.load_more_samples').css("display", "none");
                            $(".ajax_loader").css("display", "none");
                        } else {
                            $('.load_more_samples').css("display", "inline-block");
                            $(".ajax_loader").css("display", "none");
                        }
                    }
                });
        }
    });
    /**************delete sample*******************/
$(document).on('click','.delsamplemat',function(){
       var matid = $(this).closest('.samplematid').data('sample');
       var con = confirm("Are you sure, you want to delete this sample material ?");
        if (con == true) {
            $.ajax({
                type: 'POST',
                url: BASE_URL + "admin/contentmanagement/deleteSamplemateial",
                data: { id: matid },
                success: function (data) {
                    if (data == 'success') {
                        $('#file'+matid).hide();
                    }
                }
            });  
        }
    });

$(document).on('change', '.sample_active', function () {
    var status = $(this).prop('checked');
    var data_id = $(this).attr('data-sid');
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/contentmanagement/change_sample_status",
        data: { status: status, data_id: data_id },
        success: function (data) {}
    });
});

/*********add multiple questions*********/
    var max_fields_limit = 25; //set limit for maximum input fields
    var x = 1; //initialize counter for text box
    $('.add_more_quest').click(function (e) { //click event on add more fields button having class add_more_button
        e.preventDefault();
        var data = $('.questions_div').html();
        if (x < max_fields_limit) { //check conditions
            x++;
            var updated_class = 'inner_ques_'+ x;
            $('.input_quest_container').append('<div class="'+ updated_class +'">'+data+'</div>');//add input field
            $('.input_quest_container .'+updated_class).find('span.number').html(x);
            $('.input_quest_container .'+updated_class).find('.is_active').attr('id','is_active_'+x);
            $('.input_quest_container .'+updated_class).find('.is_active').next('label').attr('for','is_active_'+x);
            $('.input_quest_container .'+updated_class).find('.is_required').attr('id','is_required_'+x);
            $('.input_quest_container .'+updated_class).find('.is_required').next('label').attr('for','is_required_'+x);
            $('.input_quest_container .'+updated_class).append('<a href="#" class="remove_field"><span>-</span>Remove Question</a>');
        }
    });
    
    $('.input_quest_container').on("click", ".remove_field", function (e) { //user click on remove text links
        e.preventDefault();
        jQuery(this).parent('div').remove();
        x--;
    });
    
    /*******cancel request by admin************/
$(document).on('change','.closecancel',function(){
    var data_cid = $(this).attr('data-cid');
    var status = $('#switch_'+data_cid).prop('checked');
    if(status == true){
        $('.action_lab_'+data_cid).html('open');
    }else{
         $('.action_lab_'+data_cid).html('closed');
    }
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/dashboard/change_cancel_status_by_admin",
        data: { status: status, cancel_id: data_cid },
        success: function (data) {
        }
    });
}); 

/*********file sharing in main chat******/

   $(document).on('click', '.attchmnt', function () {
    $("input[id='shre_file']").click();
});

    
/**********afffiliated**********/
$(document).on('change', '.affactive', function () {
    var status = $(this).prop('checked');
    var data_id = $(this).attr('data-cid');
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    $.ajax({
        type: 'POST',
        url: BASE_URL + "admin/contentmanagement/changeaffstatus",
        data: { status: status, data_id: data_id },
        success: function (data) {
        }
    });
});

function load_more_affclientlist(is_loaded, currentstatus, tabActive, isonetime, search, activetabid) {
    if (is_loaded != '') {
        var dataloaded = is_loaded.attr('data-isloaded');
    } else {
        dataloaded = '';
    }
    var activetab = $('#status_check_client li.active a').attr('href');
    var status_Active = $('#status_check_client li.active a').attr('data-status');
    var tabid;
    if (tabActive != '') {
        tabid = tabActive;
    } else {
        tabid = activetab;
    }
    var activeTabPane = $('.tab-pane.active');
    var status_scroll = (currentstatus != '') ? currentstatus : status_Active;
    var searchval = $('.search_text').val();
    if (dataloaded != '1' || search == '2') {
        $.post(BASE_URL + 'admin/contentmanagement/load_more_affclientlist', { 'group_no': 0, 'status': status_scroll, 'search': searchval, 'activetabid': activetabid },
            function (data) {
                var newTotalCount = 0;
                if (data != '') {
                    if (is_loaded != '') {
                        is_loaded.attr('data-isloaded', '1');
                    }
                    $('.ajax_loader').css('display', 'none');
                    $(".ajax_searchload").fadeOut(500);
                    $(".product-list-show .add-rows .two-can").html(data);
                    newTotalCount = $(tabid + " .product-list-show .add-rows").find("#loadingAjaxCount").attr('data-value');
                    $(tabid + " .product-list-show .add-rows").find("#loadingAjaxCount").remove();
                    activeTabPane.attr("data-total-count", newTotalCount);
                    activeTabPane.attr("data-loaded", $rowperpage);
                    activeTabPane.attr("data-group", 1);
                } else {
                    activeTabPane.attr("data-total-count", 0);
                    activeTabPane.attr("data-loaded", 0);
                    activeTabPane.attr("data-group", 1);
                    $(".product-list-show .add-rows .two-can").html("<div class='no_data_ajax'>No record found</div>");
                }
                //        console.log("$rowperpage",$rowperpage);
                //        console.log("newTotalCount",newTotalCount);
                if ($rowperpage >= newTotalCount) {
                    //activeTabPane.find('.load_more').css("display", "none");
                    $('.load_more').css('display', 'none');
                } else {
                    //console.log("aayga");
                    //activeTabPane.find('.load_more').css("display", "inline-block");
                    $('.load_more').css('display', 'block');
                }
            });
    }
    else {
        $('.ajax_loader').css('display', 'none');
        $('.load_more').css('display', 'block');
    }
    //}
}

$('.searchdata_affclient').keyup(function (e) {
    e.preventDefault();
    delay(function () {
        $(".ajax_searchload").fadeIn(500);
        load_more_affclientlist('', '', '', '', '2');
    }, 1000);
});

$('.search_aff_data_ajax').click(function (e) {
    e.preventDefault();
    load_more_affclientlist('', '', '', '', '2');
});

 $(document).on("click", "#load_more_aff_client", function () {
        $(".ajax_loader").css("display", "flex");
        $(".load_more").css("display", "none");
        var row = parseInt($(this).attr('data-row'));
        row = row + $rowperpage;
        var activeTabPane = $('.tab-pane.active');
        var searchval = $('.search_text').val();
        var allcount = parseInt(activeTabPane.attr("data-total-count"));
        var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
        var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
        var status_scroll = $('#status_check_client li.active a').attr('data-status');
        if (allLoaded < allcount) {
            $.post(BASE_URL + 'admin/contentmanagement/load_more_affclientlist', { 'group_no': activeTabPaneGroupNumber, 'status': status_scroll, 'search': searchval},
                function (data) {
                    if (data != "") {
                        $(".product-list-show .add-rows .two-can").append(data);
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more").css("display", "inline-block");
                        activeTabPane.find('.load_more').attr('data-row', row);
                        activeTabPaneGroupNumber++;
                        activeTabPane.attr('data-group', activeTabPaneGroupNumber);
                        allLoaded = allLoaded + $rowperpage;
                        activeTabPane.attr('data-loaded', allLoaded);
                        if (allLoaded >= allcount) {
                            activeTabPane.find('.load_more').css("display", "none");
                            $(".ajax_loader").css("display", "none");
                        } else {
                            activeTabPane.find('.load_more').css("display", "inline-block");
                            $(".ajax_loader").css("display", "none");
                        }
                    }
                });
        }
    });
    
/****aff amnt*****/
$(document).on('click','.pay_comm',function(){
    var avail_amt = $(this).data('available');
    $('.payable_amnt').val(avail_amt);
});

 