/*********start global variables*********/
var DocArr = ['ai', 'pdf', 'ppt', 'pptx', 'pps', 'ppsx', 'tiff', 'xxx', 'eps', 'doc', 'docx', 'odt', 'ods', 'xlsx', 'xls', 'txt'];
var imgArr = ['jpg', 'png', 'gif', 'jpeg', 'bmp'];
var font_file = ['ttf', 'otf', 'woff', 'eot', 'svg'];
var vedioArr = ['mp4', 'Mov'];
var audioArr = ['mp3'];
var zipArr = ['zip', 'rar'];
/*********end global variables******/

$(".pro-circle-img-xxs").click(function () {
    $("ul#right_nav").toggleClass("yes")
});

$(".navbar-toggle").click(function () {
    $(".small-menu").toggleClass("spring");
});

$('.small-menu').on('click', 'li', function () {
    $('.navbar-nav li.active').removeClass('active');
    $(this).addClass('active');
});

$(".mobile-project-view").click(function () {
    $(".view-commentBox").slideToggle();
});

$(document).ready(function () {
    $('.mydivposition').each(function () {
        var outer_main = $(this);
        var main_img = outer_main.find('.slid').find('img');
        outer_main.find('.abcabcabc').each(function () {
            getScaledPosition($(this),outer_main,main_img);
        });
    });
});

function getScaledPosition(thiss,outer_main,main_img){
    var outerDivWidth = outer_main.outerWidth();
    var outerDivHeight = outer_main.outerHeight();
    var actualimgWidth = main_img.get(0).naturalWidth;
    var actualimgHeight = main_img.get(0).naturalHeight;
    var getImgRatioW = actualimgWidth / outerDivWidth;
    var getImgRatioH = actualimgHeight / outerDivHeight ;
    var actualTop = thiss.attr("data-top");
    var actualLeft = thiss.attr("data-left");
    var exactTop = actualTop / getImgRatioH;
    var exactLeft = actualLeft / getImgRatioW;
    thiss.css('left', exactLeft - 17);
    thiss.css('top', exactTop - 17);
    thiss.css('opacity', 1);
}

function setAllDotsOnImage(outer_main,main_img){
    outer_main.find('.abcabcabc').each(function () {
        getScaledPosition($(this),outer_main,main_img);
    });
}

function setAllDotsOnImageopen(outer_main,main_img){
    outer_main.find('.customer_chat').each(function () {
        getScaledPosition($(this),outer_main,main_img);
    });
}

function refreshDotsActiveTab(){
    var outer_main = $('#cro .active .mydivposition');
    var main_img = outer_main.find('.slid').find('img');
    setAllDotsOnImage(outer_main,main_img);
}
$(".cmmbxonline").click(function(){
   $(".posone1").removeAttr("style");
   $(".posone2abs").removeAttr("style");
   $(".posone3abs").removeAttr("style");
});

function printMousePos(event, id) {

     if ($(window).width() <= '767') {
        return false;
    } else {
        
        var AnyoPen =  $("#OpenpopUp").attr("data-open");
 
       
        if(AnyoPen !="2" ){
            
            $(".arrowdowns").removeClass("hidenotch");
            $(".posone1").removeAttr("style");
            $(".posone2abs").removeAttr("style");
            $(".posone3abs").removeAttr("style");
            $(".text_write").removeClass("wide_text"); 
            $(".leader-line").remove(); 
            $("#OpenpopUp").attr("data-open",1);
            $(".openchat").css("z-index", "0");
            $('.text_' + id).val('');
            $('.slick-active .openchat').css("z-index", "9999999999999999999");
            $(".customer_chatimage1").show();
            $(".customer_chatimage2").hide();
            //$('.customer_chat').hide();
            $('.showclicktocomment').remove();
            var outer_main = $('#cro .active .mydivposition');

            var main_img = outer_main.find('.slid').find('img');
            setAllDotsOnImageopen(outer_main, main_img);
            var outerDivElement = $("#mydivposition_" + id);
            var position = outerDivElement.offset();
            var outerDivWidthimg = $("#my-img-"+id).width();
            var outerDivHeightimg = $("#my-img-"+id).height();

            var outerDivWidth = outerDivElement.outerWidth();
            var outerDivHeight = outerDivElement.outerHeight();
             
            var image = outerDivElement.find('.slid').find('img');
            var actualimgWidth = image.get(0).naturalWidth;
            var actualimgHeight = image.get(0).naturalHeight;
 
            var getImgRatioW = (actualimgWidth / outerDivWidth);
            var getImgRatioH = (actualimgHeight / outerDivHeight);
   
            var posX = position.left;
            var posY = position.top;
            var posR = ($(window).width() + $(window).scrollLeft()) - (position.left + $('#whatever').outerWidth(true));

            var ActualLeftpos = event.pageX - posX - 17;
            var ActualToppos = event.pageY - posY - 17;
            var ActualLeft = event.pageX - posX;
            var ActualTop = event.pageY - posY;
             
            var posxx = getImgRatioW * ActualLeft;
            var posyy = getImgRatioH * ActualTop;
            
            var pointhegih = outerDivHeight - posyy;  
            var pointwidth = outerDivWidth - posxx;  


            var diff_x = outerDivWidth - ActualLeftpos;
            var diff_y = outerDivHeight - ActualToppos;
            //console.log("diff_x = ",diff_x);
           
            //console.log("ActualLeftpos",ActualLeftpos);
            // debugger;
            if(ActualLeftpos < 350 ){
                $('.mydivposition_' + id + ' .posone3abs').addClass('topposition');
                if(ActualToppos < 172){
                    $('.mydivposition_' + id + ' .posone3abs').addClass('topposition');
                    $('.mydivposition_' + id + ' .posone3abs').removeClass('leftposition');
 
                }
                if(diff_y < 172){
                    $('.mydivposition_' + id + ' .posone3abs').removeClass('topposition');
                    $('.mydivposition_' + id + ' .posone3abs').removeClass('leftposition');

                     
                }else{

                    $('.mydivposition_' + id + ' .posone3abs').removeClass('leftposition');
                     
                }
                
            }
            else{
                if(diff_x < 350){
                    $('.mydivposition_' + id + ' .posone3abs').addClass('leftposition');
                    $('.mydivposition_' + id + ' .posone3abs').addClass('topposition');
                    if(diff_y < 172){
                        $('.mydivposition_' + id + ' .posone3abs').removeClass('topposition');
                        
                    }
                }
                else{
                    if(diff_y < 172){
                        $('.mydivposition_' + id + ' .posone3abs').removeClass('leftposition');
                        $('.mydivposition_' + id + ' .posone3abs').removeClass('topposition');

                         
                    }
                    else{
                        $('.mydivposition_' + id + ' .posone3abs').addClass('topposition');
                        
                        //$('.mydivposition_' + id + ' .posone3abs').removeClass('leftposition');

                    }
                    
                }
                
            }

            
            // if(pointhegih > 212){
            //   // alert("have space"); 
            //     $('.mydivposition_' + id + ' .posone2abs').removeClass('alternate');
            //     $('.mydivposition_' + id + ' .posone3abs').removeClass('alternate');
            //     $('.mydivposition_' + id + ' .posone3abs').addClass('topposition');
            //     $('.mydivposition_' + id + ' .posone2abs').addClass('topposition');
                
            // }else{
            //    // alert("less space"); 

            //     $('.mydivposition_' + id + ' .posone2abs').addClass('alternate');
            //     $('.mydivposition_' + id + ' .posone3abs').addClass('alternate');
            //     $('.mydivposition_' + id + ' .posone3abs').removeClass('topposition');
            //     $('.mydivposition_' + id + ' .posone2abs').removeClass('topposition');
            // }

            //  if(pointwidth < 322 ){
            //     $('.mydivposition_' + id + ' .posone3abs').addClass('leftposition');
            //     $('.mydivposition_' + id + ' .posone2abs').addClass('leftposition');
            // } else {
            //       //alert("2" );
            //     $('.mydivposition_' + id + ' .posone3abs').removeClass('leftposition');
            //     $('.mydivposition_' + id + ' .posone2abs').removeClass('leftposition');
            // }
              
            var myposition = Math.round(posX + posY);
            $('.openchat').css("left", ActualLeftpos);
            $('.openchat').css("top", ActualToppos);
            $(".openchat").show();
            
            $('.send_text' + id).attr("data-yco", posyy);
            $('.send_text' + id).attr("data-xco", posxx);
            $('.send_text' + id).attr("data-cancel-false", myposition)
            $('.mycancellable').attr("data-cancel", myposition);
            $(".mydiv" + myposition).remove();
           
            $(".dotComment").remove(); 
            $('.appenddot_' + id).append('<div data-id="'+myposition+'"  id="dot-comment-'+ myposition +'" class="mydiv' + myposition + ' mycancel dotComment" style="z-index:999;position:absolute;left:' + ActualLeftpos + 'px;top:' + ActualToppos + 'px;width:30px;height:30px;" onclick="show_customer_chat(' + myposition + ',event,$(this));"></div>\<div id="dot-drag-'+myposition+'" class="customer_chat customer_chat' + myposition + '" style="display:none;background:#fff;position:absolute;left:' + posxx + 'px;top:' + posyy + 'px;padding: 5px;border-radius: 5px;">\<label></label>\</div>');
            $(".leader-line").remove(); 
             var Attrid = $("#project-pageinfo-id").attr("data-id");  
             DrawALine("dot-comment-"+myposition,"dot-drag-"+Attrid);
         }
     }
}

$(document).on('click','#zoomIn',function(){
     refreshDotsActiveTab();
    $('.customer_chat').hide();
//$('img.customer_chatimage1').css('transform','scale(1)');
    count = $('#zoomIn').attr('data-count');
    $('#zoomOut').css('pointer-events', 'auto');
    $('#zoomOut').css('cursor', 'pointer');
    $('#zoomOut').css('opacity', '1');
    clickcount = (parseInt(count) + 1);
    $('#zoomOut').attr('data-zoomoutcount', 0);
    $('#zoomIn').attr('data-count', clickcount);
    var Outerdivwidth = $('.outer-post').width();
    var Outerdivheight = $('.outer-post').height();
    var outer_main = $('#cro .active .mydivposition');
    var Divwidth = outer_main.width();
    //var Divheight = outer_main.height();
    var main_img = outer_main.find('.slid').find('img');
    //var Imgwidth = $('#cro .active .mydivposition .slid img').width();
    //var Imgheight = $('#cro .active .mydivposition .slid img').height();
   // var image = $('#cro > .active > .mydivposition > .slid').find('img');
    var actualimgWidth = main_img.get(0).naturalWidth;
    var actualimgHeight = main_img.get(0).naturalHeight;
//    console.log("outerwidth"+Outerdivwidth);
//    console.log("outerheight"+Outerdivheight);
//    console.log("actualimgWidth"+actualimgWidth);
//    console.log("actualimgHeight"+actualimgHeight);
    
    if (clickcount == 1) {
        $('.customer_loader').css('display','block');
        $('.slid').css('display','none');
        $('.abcabcabc').css('display','none');
    setTimeout(function () {
        $('.customer_loader').css('display','none');
        $('.slid').css('display','block');
        $('.abcabcabc').css('display','block');
    }, 2000);
        if ((actualimgWidth > Outerdivwidth) || (actualimgHeight > Outerdivheight)) {
//             console.log("greater");
//            $('#cro .active .mydivposition').css('width', Outerdivwidth);
//            $('#cro .active .mydivposition .slid img').css('width', Outerdivwidth);
//            $('#cro .active .mydivposition .slid img').css('max-height', 'inherit');
            $('#cro .active .mydivposition').css('width', actualimgWidth);
            $('#cro .active .mydivposition').css('height', actualimgHeight);
            $('#cro .active .mydivposition .slid img').css('width', actualimgWidth);
            $('#cro .active .mydivposition .slid img').css('max-height', actualimgHeight);
        }else if (actualimgWidth < Outerdivwidth || actualimgHeight < Outerdivheight) {
             //console.log('greaterouterwidth'+Outerdivwidth);
//            $('#cro .active .mydivposition .slid img').css('width', actualimgWidth);
//            $('#cro .active .mydivposition .slid img').css('height', actualimgHeight);
//            $('#cro .active .mydivposition').css('width', actualimgWidth);
//            $('#cro .active .mydivposition').css('height', actualimgHeight);
            $('#zoomIn').css('cursor', 'no-drop');
            $('#zoomIn').css('opacity', '0.5');
        }
//        if (Divwidth == Outerdivwidth) {
////            console.log("equal");
//            $('#cro .active .mydivposition .slid img').css('width', actualimgWidth);
//            $('#cro .active .mydivposition .slid img').css('height', actualimgHeight);
//            $('#cro .active .mydivposition').css('width', actualimgWidth);
//            $('#cro .active .mydivposition').css('height', actualimgHeight);
//            //console.log("Current Width",actualimgWidth);
//        }
    }
//    else if (clickcount == 2) {
//        $('div#myCarousel').css('filter','blur(3px)');
//        $('.abcabcabc').css('display','none');
//    setTimeout(function () {
//        $('div#myCarousel').css('filter','blur(0)');
//        $('.abcabcabc').css('display','block');
//    }, 2000);
//        if (Outerdivwidth < actualimgWidth || Outerdivheight < actualimgHeight) {
//            console.log("outless");
//            $('#cro .active .mydivposition').css('width', actualimgWidth);
//            $('#cro .active .mydivposition').css('height', actualimgHeight);
//            $('#cro .active .mydivposition .slid img').css('width', actualimgWidth);
//            $('#cro .active .mydivposition .slid img').css('max-height', actualimgHeight);
//        }
//        else if (actualimgWidth < Outerdivwidth || actualimgHeight < Outerdivheight) {
//             console.log("actusless");
////            $('#zoomIn').css('pointer-events', 'none');
//                $('#zoomIn').css('cursor', 'no-drop');
//                $('#zoomIn').css('opacity', '0.5');
//        }
//    }
    if (clickcount > 1) {
        //$('#zoomIn').css('pointer-events', 'none');
        $('#zoomIn').css('cursor', 'no-drop');
        $('#zoomIn').css('opacity', '0.5');
        //$('#zoomOut').css('pointer-events', '');
        $('#zoomOut').css('cursor', 'pointer');
        $('#zoomOut').css('opacity', '1');
    }
    setTimeout(
        function() 
        {
        setAllDotsOnImage(outer_main,main_img);
       }, 2000);
});

$(document).on('click','#zoomOut',function(){
     refreshDotsActiveTab();
//    alert("zoomout");
    $('.customer_chat').hide();
    //$('img.customer_chatimage1').css('transform','scale(1)');
    countzoomout = $('#zoomOut').attr('data-zoomoutcount');
    clickcount = (parseInt(countzoomout) + 1);
    $('#zoomOut').attr('data-zoomoutcount', clickcount);
    $('#zoomIn').attr('data-count', 0);
    $('#zoomIn').css('pointer-events', 'auto');
    $('#zoomIn').css('cursor', 'pointer');
    $('#zoomIn').css('opacity', '1');
    var Outerdivwidth = $('.outer-post').width();
    var Outerdivheight = $('.outer-post').height();
    var Divwidth = $('#cro .active .mydivposition').width();
    var Divheight = $('#cro .active .mydivposition').height();
    var outer_main = $('#cro .active .mydivposition');
    var main_img = outer_main.find('.slid').find('img');
    //var Imgwidth = $('#cro .active .mydivposition .slid img').width();
    //var Imgheight = $('#cro .active .mydivposition .slid img').height();
    //var image = $('#cro > .active > .mydivposition > .slid').find('img');
    var actualimgWidth = main_img.get(0).naturalWidth;
    var actualimgHeight = main_img.get(0).naturalHeight;
//console.log("outerwidth"+Outerdivwidth);
//    console.log("outerheight"+Outerdivheight);
//    //console.log("divwidth"+Divwidth);
//    console.log("actualimgWidth"+actualimgWidth);
//    console.log("actualimgHeight"+actualimgHeight);
    if (clickcount == 1) {
         $('.customer_loader').css('display','block');
        $('.slid').css('display','none');
         $('.abcabcabc').css('display','none');
    setTimeout(function () {
        $('.customer_loader').css('display','none');
            $('.slid').css('display','block');
        $('.abcabcabc').css('display','block');
    }, 2000);
//        if (Divwidth == Outerdivwidth || Divheight == Outerdivheight) {
//            //console.log("equalouter" + Outerdivwidth);
//            $('#cro .active .mydivposition .slid img').css('max-height', '');
//            $('#cro .active .mydivposition').css('width', 'auto');
//            $('#cro .active .mydivposition .slid img').css('width', 'auto');
//            $('#cro .active .mydivposition .slid img').css('max-height', "calc(100vh - 93px)");
//        }
        if (Divwidth == actualimgWidth || Divheight == actualimgHeight) {
            //console.log("equalactual"+actualimgWidth);
            $('#cro .active .mydivposition').css('width', Outerdivwidth);
            $('#cro .active .mydivposition').css('height', 'auto');
             $('#cro .active .mydivposition .slid img').css('width', Outerdivwidth);
            $('#cro .active .mydivposition .slid img').css('height', 'auto');
            if (actualimgWidth < Outerdivwidth || actualimgHeight < Outerdivheight) {
                //console.log("greaterouter" + Outerdivwidth);
                //$('img.customer_chatimage1').css('transform','scale(.6)');
                $('#cro .active .mydivposition .slid img').css('width', 'auto');
                $('#cro .active .mydivposition').css('width', 'auto');
                $('#cro .active .mydivposition .slid img').css('max-height', "calc(100vh - 93px)");
            }
        }
    }
//    if (clickcount == 2) {
//         $('div#myCarousel').css('filter','blur(3px)');
//         $('.abcabcabc').css('display','none');
//    setTimeout(function () {
//        $('div#myCarousel').css('filter','blur(0)');
//        $('.abcabcabc').css('display','block');
//    }, 2000);
//        //$('img.customer_chatimage1').css('transform','scale(.6)');
//        if (Divwidth == Outerdivwidth || Divheight == Outerdivheight) {
//            //console.log("divequalouter" + actualimgWidth);
//            $('#cro .active .mydivposition .slid img').css('max-height', '');
//            $('#cro .active .mydivposition').css('width', 'auto');
//            $('#cro .active .mydivposition .slid img').css('width', 'auto');
//            $('#cro .active .mydivposition .slid img').css('max-height', "calc(100vh - 93px)");
//        }
//    }
    if (clickcount > 1) {
        //$('#zoomOut').css('pointer-events', 'none');
        $('#zoomOut').css('cursor', 'no-drop');
        $('#zoomOut').css('opacity', '0.5');
        //$('#zoomIn').css('pointer-events', '');
        $('#zoomIn').css('cursor', 'pointer');
        $('#zoomIn').css('opacity', '1');
    }
    setTimeout(
        function() 
        {
    setAllDotsOnImage(outer_main,main_img);
         }, 2000);
});

$(document).on('click', '#thub_nails li', function () {
    var id = $(this).attr("id");
    $('#zoomIn').attr('data-count', 0);
    $('#zoomOut').attr('data-zoomoutcount', 0);
    $('#zoomOut').css('pointer-events', 'none');
    //$('#zoomOut').css('pointer-events','');
    $('#zoomIn').css('pointer-events', '');
   // $('#cro .active .mydivposition .slid img').css('max-height', "calc(100vh - 93px)");
    $('#cro .active .mydivposition').css('width', "auto");
    $('#cro .active .mydivposition .slid img').css('width', "auto");
    //$('img.customer_chatimage1').css('transform','scale(.6)');
    $(".f_revw_feedback").attr("id","f_revw_feedback_"+id);
    $(".f_revw_feedback").attr("data-id",id);
});

$(document).ready(function () {
//     $('div#myCarousel').css('filter','blur(3px)');
//    setTimeout(function () {
//        $('div#myCarousel').css('filter','blur(0)');
//    }, 2000);
    //$('#zoomOut').css('pointer-events', 'none');
    $('#zoomOut').css('cursor', 'no-drop');
    $('#zoomOut').css('opacity', '0.5');
});



/**************reply messages*************************/
  function replyMessages(id,pos){
      //console.log("instant",pos);
      if(pos == true){
          // console.log("posbbb",pos,"id",id);
          jQuery('.reply_msg_ryt_'+id).css('display','block');
      }else{
        jQuery('.reply_msg_'+id).css('display','block');
      }
  }
  
  function editMessages(id,msgposition){
      //console.log(msgposition);
      /*if comment is from dot */
    if(msgposition == false){
       $('.comment-submit_'+id).css('display','block');
       $('.edit_to_'+id).css('display','none');
       $('.distxt-cmm_'+id).css('display','none');
    }else if(msgposition == true){
        //console.log("ryt");
        $('.comment-submit_ryt_'+id).css('display','block');
        $('#editryt_'+id).css('display','block');
        $('#editdot_'+id).css('display','block');
        $('.edit_to_ryt_'+id).css('display','none');
       $('.distxt-cmm_ryt_'+id).css('display','none');
    }
  }
  
  $( window ).load(function() {
    var url = window.location.href;
    var url_divide = url.split("/");
    var pieces = url.split("#");
    var activebar = pieces[1];
	if($('.outer-post').length > 0){
    //if ($.inArray('project_image_view', url_divide) != -1){
      //console.log("onload");
    var Outerdivwidth = $('.outer-post').width();
    var Outerdivheight = $('.outer-post').height();
    var outer_main = $('#cro .active .mydivposition');
    var outer_main_width = outer_main.width();
    var outer_main_height = outer_main.height();
    var main_img = outer_main.find('.slid').find('img');
    var actualimgWidth = main_img.get(0).naturalWidth;
    var actualimgHeight = main_img.get(0).naturalHeight;
    var calcval = (Outerdivheight - outer_main_height)/2;
//    console.log("Outerdivheight",Outerdivheight);
//    console.log("outer_main_width",outer_main_height);
//    console.log("calcval",calcval);
    if(Outerdivheight > outer_main_height){
      //$('#cro .active .mydivposition').css('margin-top',calcval+"px");
    }
    if (actualimgWidth < Outerdivwidth || actualimgHeight < Outerdivheight) {
        $('#zoomOut').css('cursor', 'no-drop');
        $('#zoomOut').css('opacity', '0.5');
        $('#zoomIn').css('cursor', 'no-drop');
        $('#zoomIn').css('opacity', '0.5');
        $('#zoomOut').prop('disabled', true);
        $('#zoomIn').prop('disabled', true);
    }
    setTimeout(function () {
//    $('.slid img').css({
//        "-webkit-filter": "blur(0px)",
//        "filter": "blur(0px)"
//    });
    $('.slid').css("display", "block");
    $('.customer_loader').css("display", "none");
    $('.abcabcabc').css("display", "block");
    refreshDotsActiveTab();
    }, 500);
    }
});


$(".change_pubpermision").click(function(){
  $(".permision-here").slideToggle();
});

//$(".change_permisn").click(function(){
//  $(".private_permisn").slideToggle();
//});

$(".sidebar-toggle").click(function () {
$(".pro-ject-wrapper").toggleClass("sidebar-in");
$(".pro-ject-rightbar").toggleClass("sidebar-in");
//$('.loader').css("display", "flex");
$('.customer_loader').css("display", "flex");
$('.slid').css("display", "none");
$('.abcabcabc').css("display", "none");
setTimeout(function () {
$('.customer_loader').css("display", "none");
$('.slid').css("display", "block");
$('.abcabcabc').css("display", "block");
refreshDotsActiveTab();
}, 2000);
});

function CheckForFeedBack(draftid) {
    $("#project-pageinfo-id").attr("data-id", draftid);
    $.ajax({
        type: 'POST',
        url: baseUrl+"admin/dashboard/CheckForFeedBack",
        data: {"draftid": draftid},
        dataType: "JSON",
        success: function (res) {

            if (res[0].status == "pending") {
                $(".review-option-main").fadeIn();
                $(".project-details-bar").removeClass("hide");
            } else {

                $(".review-option-main").fadeOut();
                $(".project-details-bar").addClass("hide");
            }
        }
    });
}
    
$('.next,.prev').on('click',function(){
    var dataId,datatitle,active_thumbnail;
    var outer_main = $('#cro .active .mydivposition');
    if($(this).hasClass('prev')){
        dataId = $('.outer-post.active').prev().attr('id');
        datatitle = $('.outer-post.active').prev().data('name');
        active_thumbnail = $('.outer-post.active').prev().find('.slid').data('id');
        if(typeof dataId == 'undefined'){
            dataId = $('.outer-post:last').attr('id');
            datatitle = $('.outer-post:last').data('name');
            active_thumbnail = $('.outer-post:last').find('.slid').data('id');
        }
    }else{
        dataId = $('.outer-post.active').next().attr('id');
        datatitle = $('.outer-post.active').next().data('name');
        active_thumbnail = $('.outer-post.active').next().find('.slid').data('id');
        if(typeof dataId == 'undefined'){
            dataId = $('.outer-post:first').attr('id');
            datatitle = $('.outer-post:first').data('name');
            active_thumbnail = $('.outer-post:first').find('.slid').data('id');
        }
    }
//    console.log("dataId",dataId);
    // $("#people_share").data("proid",reqid);
    $("#people_share").data("draftid",dataId);
    trigger_sharing(dataId);
    if(dataId){
        $('.comment_sec').css('display','none');
        $('.hdr-buttons').css('display','none')
        $('.butt_'+dataId).css('display','block');
        $('.file_title').html(datatitle);
        $('#thub_nails li figure').removeClass('active');
        $('#thub_nails #'+active_thumbnail+' figure').addClass('active');
        $('.comment-sec-'+dataId).css('display','block');
    }
    $('.abcabcabc').css("display", "none");
     $('.customer_loader').css('display','block');
    $('.slid').css('display','none');
//    $('.slid img').css({
//        "-webkit-filter": "blur(8px)",
//        "filter": "blur(8px)"
//});
    setTimeout(function () {
    $('.loader').css("display", "none");
     $('.customer_loader').css('display','none');
            $('.slid').css('display','block');
    //$('.slid img').css({
    //        "-webkit-filter": "blur(0px)",
    //        "filter": "blur(0px)"
    //    });
    $('.abcabcabc').css("display", "block");
    refreshDotsActiveTab();
    }, 2000);
    /** review feedback js**/
    $(".f_revw_feedback").attr("id", "f_revw_feedback_" + dataId);
    $(".f_revw_feedback").attr("data-id", dataId);
    $("#project-pageinfo-id").attr("data-id", dataId);
    $(".response_request").attr("data-fileid",dataId);
    CheckForFeedBack(dataId);
});

function replyButton(id, msgposition) {
    if (msgposition == false) {
        $('.reply_msg_' + id).css('display', 'inline-block');
        $('.reply_to_' + id).css('display', 'none');
    } else if (msgposition == true) {
        $('.reply_msg_ryt_' + id).css('display', 'inline-block');
        $('.reply_to_ryt_' + id).css('display', 'none');

    }
}
$(".active1").click(function(){
        var dataid=  $(this).attr('id');
        var req_id=  $(this).attr('request_id');
        $("#people_share").data("proid",req_id);
        $("#people_share").data("draftid",dataid);
        $("#loader_peoples_tab").show();
        setTimeout(function(){ trigger_sharing(dataid);  }, 1000);
         
    });

      

//function editMessages(id, msgposition) {
///*if comment is from dot */
//if (msgposition == false) {
//    $('.comment-submit_' + id).css('display', 'block');
//    $('.edit_to_' + id).css('display', 'none');
//    $('.distxt-cmm_' + id).css('display', 'none');
//} else if (msgposition == true) {
//    $('.comment-submit_ryt_' + id).css('display', 'block');
//     $('.edit_to_ryt_' + id).css('display', 'none');
//    $('#editryt_' + id).css('display', 'block');
//    $('.distxt-cmm_ryt_' + id).css('display', 'none');
//}
//}

function cnfrm_delete() {
  return confirm("Are you sure you want to delete?");
}
function goBack() {
    window.history.back();
}
function wideText(el) {
     
    $('.cmmtype-row').removeClass('wide_text');
    $(el).addClass('wide_text');
     
     
}

$(document).on('click', 'ul#comment_ul li', function () {
    var itemType = $('ul#comment_ul').find('li').find('.active').parent().attr('id');
// alert(itemType);
});
    
function cancelButton(cancelid) {
    $('.comment-submit_' + cancelid).css('display', 'none');
    $('.distxt-cmm_' + cancelid).css('display', 'block');
    $('.edit_to_' + cancelid).css('display', 'inline-block');
    $('.comment-submit_ryt_' + cancelid).css('display', 'none');
    $('.distxt-cmm_ryt_' + cancelid).css('display', 'block');
    $('.edit_to_ryt_' + cancelid).css('display', 'inline-block');
}
/* add notes for user */
$(document).on('click','.add_notes',function(){
var id = $(this).attr('data-id');
var user_name = $(this).attr('data-name');
var from = $(this).attr('data-from');
$("#add_notes_btn").attr('data-user_id',id);
$(".notetitle span").text(user_name);
var msgdata = "";
    $.ajax({
        type:"POST",
        dataType: 'json',
        url:baseUrl+"/admin/Accounts/get_usernotes",
        data:{user_id:id},
        success: function(response){
            if(response.notes != 0 || from == 'admin'){
                if(response.notes == 0){
                    $(".main_classnotes").html("");
                }
                $.each(response.notes, function (key, value) {
                    //console.log(value.message);
                    //console.log(value);
                    msgdata += "<div class='msg'><span>"+value.created+"</span>"+value.message+"</div>";
                });
             $(".usernotes_list").html(msgdata);
             
            }else{
                $(".usernotes_list").html("No Record Found"); 
            }
            
        }
    });
});
$(document).on('click','#add_notes_btn',function(){
    var sender_id = $(this).attr('data-sender_id');
    var user_id = $(this).attr('data-user_id');
    var notes = $("#notes_txtmsg").val();
    // console.log('sender_id',sender_id);
    // console.log('user_id',user_id);
    // console.log('notes',notes);
    $.ajax({
        type:"POST",
       // dataType: 'json',
        url:baseUrl+"admin/Accounts/add_usernotes",
        data:{sender_id:sender_id,user_id:user_id,notes:notes},
        success: function(response){
            //console.log(response);
            if(response){
                $("#notes_txtmsg").val("");
                $(".usernotes_list").prepend("<div class='msg'><span>Just Now</span>"+notes+"</div>");
            }  
        }
    });
});

$('.searchdata').focus(function () { 
    $('.search-box').addClass('magic_search');
}).blur(function () {
    if($('.search_text').val() == ''){
        $('.search-box').removeClass('magic_search');
    }
});

$(document).ready(function(){
    var dataid =  $(".active1").find("figure.active").parents("li").attr('id');
    var req_id=  $(".active1").find("figure.active").parents("li").attr('request_id');
    $("#people_share").data("proid",req_id);
    $("#people_share").data("draftid",dataid);
    
    setTimeout(function(){
        trigger_sharing(dataid);
    },1000);
});
function trigger_sharing(draftid){
    
    if($("#people_share").parents("li").hasClass("active")){
        jQuery("#people_share")[0].click();
    } 
}

function DrawALine(Start,EnD){
    console.log("start",Start); 
    console.log("end",EnD); 
    if($("#"+EnD).hasClass("topposition")){
        $("#"+EnD).css("top","0");
        if($("#"+EnD).hasClass("leftposition")){
            $("#"+EnD).removeAttr("style");
        }
    }
    var lineH = new LeaderLine(
        document.getElementById(Start),
        document.getElementById(EnD),
        {
          path: 'straight'
        }
        );
    $("#leader-line-"+lineH._id+"-plug-marker-1").addClass("custom-color"); 
    $(".leader-line").hide();   
    $("#"+EnD).draggable({
             containment: $('#cro'),
                drag: function(event, ui){ 
                    //debugger;
                    if($("#"+EnD).hasClass("posone2abs")){
                        $("#"+EnD).css({"height":"auto","min-height":"90px","bottom":"unset"}); 
                    }
//                    else{
//                        $("#"+EnD).css({"height":"172px","width":"350px"}); 
//                    }
                    var  lineWidth = parseInt($(".leader-line").css("width"));
                    var widthLine = parseInt($(".leader-line").css('width'));

                    if(CheckElementIstouched('#'+EnD,'#'+Start)){
                        $(".leader-line").hide(); 
                    }else if(lineWidth < 30){
                        $(".leader-line").hide(); 
                    }else{
                        $(".leader-line").show(); 
                    }
                    
                    lineH.position();
                    $(".arrowdowns").addClass("hidenotch"); 
                }
 });
}

function CheckElementIstouched(rectone, recttwo){
    // console.log("Start",Start); 
    var r1 = $(rectone);
    var r2 = $(recttwo);
    
    var r1x = r1.offset().left+50;
    var r1w = r1.width();
    var r1y = r1.offset().top+50;
    var r1h = r1.height();
    
    var r2x = r2.offset().left+50;
    var r2w = r2.width();
    var r2y = r2.offset().top+50;
    var r2h = r2.height();
    
    if( r1y+r1h < r2y || r1y > r2y+r2h || r1x > r2x+r2w || r1x+r1w < r2x ){
        return false;
    }else{
        return true;   
    }
    
} 

    
    $(".send_text").click(function(){
        $(".leader-line").remove();
    });

    $(document).on('click','.CoPyOrMove',function(e){
      //$(".filetree").hide(); 
      $this = $(this); 
        var orignal_file,request_id,projid;
        $(".breadCrumCopYFolder a").trigger("click");
    });
    




    function replyButton(id, msgposition) {
    if (msgposition == false) {
        $('.reply_msg_' + id).css('display', 'inline-block');
        $('.reply_to_' + id).css('display', 'none');
    } else if (msgposition == true) {
        $('.reply_msg_ryt_' + id).css('display', 'inline-block');
        $('.reply_to_ryt_' + id).css('display', 'none');

    }
}
function editMessages(id, msgposition) {
    /*if comment is from dot */
    if (msgposition == false) {
        $('.comment-submit_' + id).css('display', 'block');
        $('.edit_to_' + id).css('display', 'none');
        $('.distxt-cmm_' + id).css('display', 'none');
    } else if (msgposition == true) {
        $('.comment-submit_ryt_' + id).css('display', 'block');
        $('.edit_to_ryt_' + id).css('display', 'none');
        $('.distxt-cmm_ryt_' + id).css('display', 'none');

    }
}
function cnfrm_delete() {
    return confirm("Are you sure you want to delete?");
}
function goBack() {
    window.history.back();
}
function wideText(el) {
    $('.cmmtype-row').removeClass('wide_text');
    $(el).addClass('wide_text');
}

$(document).on('click', 'ul#comment_ul li', function () {
    var itemType = $('ul#comment_ul').find('li').find('.active').parent().attr('id');
    // alert(itemType);
});

function cancelButton(cancelid) {
    $('.comment-submit_' + cancelid).css('display', 'none');
    $('.distxt-cmm_' + cancelid).css('display', 'block');
    $('.edit_to_' + cancelid).css('display', 'inline-block');
    $('.comment-submit_ryt_' + cancelid).css('display', 'none');
    $('.distxt-cmm_ryt_' + cancelid).css('display', 'block');
    $('.edit_to_ryt_' + cancelid).css('display', 'inline-block');
}

/* add notes for user */
$(document).on('click', '.add_notes', function () {
    var id = $(this).attr('data-id');
    var user_name = $(this).attr('data-name');
    var from = $(this).attr('data-from');
    $("#add_notes_btn").attr('data-user_id', id);
    $(".notetitle span").text(user_name);
    var msgdata = "";
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: baseUrl + "/admin/Accounts/get_usernotes",
        data: { user_id: id },
        success: function (response) {
            if (response.notes != 0) {
                $.each(response.notes, function (key, value) {
                    msgdata += "<div class='msg'><span>" + value.created + "</span>" + value.message + "</div>";
                });
                $(".usernotes_list").html(msgdata);
            } else {
                $(".usernotes_list").html("<span>No Record Found</span>");
            }

        }
    });
});
$(document).on('click', '#add_notes_btn', function () {
    var sender_id = $(this).attr('data-sender_id');
    var user_id = $(this).attr('data-user_id');
    var notes = $("#notes_txtmsg").val();
//    console.log('sender_id', sender_id);
//    console.log('user_id', user_id);
//    console.log('notes', notes);
    $.ajax({
        type: "POST",
        url: baseUrl + "admin/Accounts/add_usernotes",
        data: { sender_id: sender_id, user_id: user_id, notes: notes },
        success: function (response) {
            // console.log(response);
            if (response) {
                $("#notes_txtmsg").val("");
                $(".usernotes_list span").html("");
                $(".usernotes_list").prepend("<div class='msg'><span>Just Now</span>" + notes + "</div>");
            }
        }
    });
});
$('.searchdata').focus(function () {
    $('.search-box').addClass('magic_search');
}).blur(function () {
    if ($('.search_text').val() == '') {
        $('.search-box').removeClass('magic_search');
    }
});

/**********upload main chat file**********/
function uploadChatfile(e, reqID, sender_role, senderid, rcvrid, rcvrole, sendername, profile, seenby) {
    var reqid = reqID;
    var fileInput = e.target;
    var withornot = $(fileInput).attr("data-withornot");
    if (fileInput.files.length > 0) {
        var formData = new FormData();
        $.each(fileInput.files, function (k, file) {
            var fileName = file.name;
           // console.log("fileName",fileName); 
            var type = fileName.split('.').pop();
            var name = fileName.split('.');
            if($.inArray(type,imgArr) !== -1){
                  var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, Math.floor(Math.random() * 6) + 1);
                    name_file = name_file+'.'+type;
                }else{
                    name_file = fileName; 
                }
            // console.log("newFileName",name_file);
            var newFileName = name_file;
            formData.append('shre_file[]', file, newFileName);
            formData.append('saved_file[]', newFileName);
            formData.append("data[request_id]", reqID);
            formData.append("data[sender_role]", sender_role);
            formData.append("data[sender_id]", senderid);
            formData.append("data[reciever_id]", rcvrid);
            formData.append("data[reciever_role]", rcvrole);
            formData.append("data[sendername]", sendername);
            formData.append("data[withornot]", withornot);
            formData.append("data[profile]", profile);
            formData.append("data[seenby]", seenby);
        });
        $(".ajax_searchload").show();
        $.ajax({
            type: 'post',
            url: baseUrl + "admin/dashboard/uploadmainChatfiles/" + reqid,
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                $(".ajax_searchload").hide();
                $(data).each(function (index, value) {
                    //console.log("main_chat",value);
                    if(value.status == 'success'){
                    var html = ""; 
                    var defaultFile;
                    var fileimg;
                    var fileExt = (get_extension(value.file)).toLowerCase();
                    var newFileName = fileInput.files[index].name;
                    var filenAmeThumb =value.file.replace(" ", "_");
                    var newFileNametitle = newFileName.substr(0, 20) + (newFileName.length > 20 ? "..."+fileExt :'');
                    var downld = $assets_path+'img/customer/dwnlod-img.svg';
                    var downldimg = '<img src="'+downld+'">';
                    if(value.file_status == 0){
                     fileimg = requestmainchatimg + value.req_id + "/" +filenAmeThumb;
                    }else{
                      fileimg = requestmainchatimg + value.req_id + "/_thumb/" +filenAmeThumb;  
                    }
//                    console.log("fileimg",fileimg);
                    
                if($.inArray(fileExt,DocArr) !== -1){
                     defaultFile =  $assets_path+'img/default-img/chat-file.svg';
                     html = '<div class="contain-info"><a class="open-file-chat doc-type-file " data-ext="'+fileExt+'" data-src="' + requestmainchatimg + value.req_id + "/" + filenAmeThumb + '"><img src="'+defaultFile+'"><span class="file-ext">'+fileExt+'</span><div class="file-name"><h4><b>'+newFileNametitle+'</b></h4></div></a><div class="download-file"> <a href="'+baseUrl+'customer/request/download_stuffFromChat?ext='+fileExt+'&file-upload='+requestmainchatimg + value.req_id + '/' + newFileName+'" target="_blank">'+downldimg+'</a></div>'; 

                }else if($.inArray(fileExt,imgArr) !== -1){
                      html = '<div class="contain-info"><a class="open-file-chat doc-type-image" data-ext="'+fileExt+'" data-src="' + fileimg + '"><img src="' + fileimg + '"><div class="file-name"><h4><b>'+newFileNametitle+'</b></h4></div></a><div class="download-file"> <a href="'+baseUrl+'customer/request/download_stuffFromChat?ext='+fileExt+'&file-upload='+requestmainchatimg + value.req_id + '/' + newFileName+'" target="_blank">'+downldimg+'</a></div>'; 
                }else if($.inArray(fileExt,font_file) !== -1){ 
                     defaultFile =  $assets_path+'img/default-img/chat-file.svg';
                     html = '<div class="contain-info"><a class="open-file-chat doc-type-file " data-ext="'+fileExt+'" data-src="' + requestmainchatimg + value.req_id + "/" + filenAmeThumb + '"><img src="'+defaultFile+'"><span class="file-ext">'+fileExt+'</span><div class="file-name"><h4><b>'+newFileNametitle+'</b></h4></a><div class="download-file"> <a href="'+baseUrl+'customer/request/download_stuffFromChat?ext='+fileExt+'&file-upload='+requestmainchatimg + value.req_id + '/' + newFileName+'" target="_blank">'+downldimg+'</a></div>'; 
                }else if($.inArray(fileExt,vedioArr) !== -1){
                     
                  html = '<div class="contain-info"><a class="open-file-chat doc-type-file " data-ext="'+fileExt+'" data-src="' + requestmainchatimg + value.req_id + "/" + filenAmeThumb + '"><img src="'+defaultFile+'"><span class="file-ext">'+fileExt+'</span><div class="file-name"><h4><b>'+newFileNametitle+'</b></h4></a><div class="download-file"> <a href="'+baseUrl+'customer/request/download_stuffFromChat?ext='+fileExt+'&file-upload='+requestmainchatimg + value.req_id + '/' + newFileName+'" target="_blank">'+downldimg+'</a></div>'; 
                }else if($.inArray(fileExt,audioArr) !== -1){ 
                     html = '<div class="contain-info"><a class="open-file-chat doc-type-file " data-ext="'+fileExt+'" data-src="' + requestmainchatimg + value.req_id + "/" + filenAmeThumb + '"><img src="'+defaultFile+'"><span class="file-ext">'+fileExt+'</span><div class="file-name"><h4><b>'+newFileNametitle+'</b></h4></a><div class="download-file"> <a href="'+baseUrl+'customer/request/download_stuffFromChat?ext='+fileExt+'&file-upload='+requestmainchatimg + value.req_id + '/' + newFileName+'" target="_blank">'+downldimg+'</a></div>'; 
                }else if($.inArray(fileExt,zipArr) !== -1){ 

                     defaultFile =  $assets_path+'img/default-img/chat-zip.svg';
                     html = '<div class="contain-info"><a class="open-file-chat doc-type-zip" data-ext="'+fileExt+'" data-src="' + requestmainchatimg + value.req_id + "/" + filenAmeThumb + '"><img src="'+defaultFile+'"><span class="file-ext">'+fileExt+'</span><div class="file-name"><h4><b>'+newFileNametitle+'</b></h4></a><div class="download-file"> <a href="'+baseUrl+'customer/request/download_stuffFromChat?ext='+fileExt+'&file-upload='+requestmainchatimg + value.req_id + '/' + newFileName+'" target="_blank">'+downldimg+'</a></div>'; 
                } 
                if (value.status == 'success') {
                        if (withornot == 0) {
                            if ($('#message_container_without_cus > div.msgk-chatrow:last-child > div').hasClass("msgk-right") === true) {
                                $("#message_container > div.msgk-chatrow:nth-last-child(1)").find('.just_now').css('display', 'none');
                                $('#message_container_without_cus').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_role + 'del_' + value.id + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + value.id + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + value.id + '"></i><div class="open-edit open-edit_' + value.id + '" style="display: none;"><a href="javascript:void(0)" class="deletemsg" data-delid="' + value.id + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + value.id + '" style="display:none"></div> <div class="msgk-mn msg_desc_' + value.id + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + value.id + ' edit_icon_msg"></span><span class="msgk-umsxxt took_' + value.id + '">' + html + '</span></pre></div></div></div></div>');
                            } else {
                                $('#message_container_without_cus').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_role + 'del_' + value.id + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + value.id + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + value.id + '"></i><div class="open-edit open-edit_' + value.id + '" style="display: none;"><a href="javascript:void(0)" class="deletemsg" data-delid="' + value.id + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + value.id + '" style="display:none"></div> <div class="msgk-mn msg_desc_' + value.id + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + value.id + ' edit_icon_msg"></span><span class="msgk-umsxxt took_' + value.id + '">' + html + '</span></pre></div></div></div></div>');
                            }
                        } else if (withornot == 1) {
                            if ($('#message_container > div.msgk-chatrow:last-child > div').hasClass("msgk-right") === true) {
                                $("#message_container > div.msgk-chatrow:nth-last-child(1)").find('.just_now').css('display', 'none');
                                $('#message_container').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_role + 'del_' + value.id + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + value.id + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + value.id + '"></i><div class="open-edit open-edit_' + value.id + '" style="display: none;"><a href="javascript:void(0)" class="deletemsg" data-delid="' + value.id + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + value.id + '" style="display:none"></div> <div class="msgk-mn msg_desc_' + value.id + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + value.id + ' edit_icon_msg"></span><span class="msgk-umsxxt took_' + value.id + '">' + html + '</span></pre></div></div></div></div>');
                            } else {
                                $('#message_container').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_role + 'del_' + value.id + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + value.id + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + value.id + '"></i><div class="open-edit open-edit_' + value.id + '" style="display: none;"><a href="javascript:void(0)" class="deletemsg" data-delid="' + value.id + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + value.id + '" style="display:none"></div> <div class="msgk-mn msg_desc_' + value.id + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + value.id + ' edit_icon_msg"></span><span class="msgk-umsxxt took_' + value.id + '">' + html + '</span></pre></div></div></div></div>');
                            }
                        }
                    }
                }else{
                    if (withornot == 0) {
                        $('#message_container_without_cus').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_role + 'del_' + value.id + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + value.id + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + value.id + '"></i><div class="open-edit open-edit_' + value.id + '" style="display: none;"><a href="javascript:void(0)" class="deletemsg" data-delid="' + value.id + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + value.id + '" style="display:none"></div> <div class="msgk-mn msg_desc_' + value.id + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + value.id + ' edit_icon_msg"></span><span class="msgk-umsxxt took_' + value.id + '" style="color:red">' + value.error_msg + '</span></pre></div></div></div></div>');
                    }
                    else if (withornot == 1) {
                        $('#message_container').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_role + 'del_' + value.id + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + value.id + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + value.id + '"></i><div class="open-edit open-edit_' + value.id + '" style="display: none;"><a href="javascript:void(0)" class="deletemsg" data-delid="' + value.id + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + value.id + '" style="display:none"></div> <div class="msgk-mn msg_desc_' + value.id + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + value.id + ' edit_icon_msg"></span><span class="msgk-umsxxt took_' + value.id + '" style="color:red">' + value.error_msg + '</span></pre></div></div></div></div>');
                    }
                }
                });
            }
        });
    }
}

//  view file  for chatModule
$(document).on("click ",".open-file-chat",function(){
    $this = $(this); 
     var ext = $this.attr("data-ext");
     var fileurl = $this.attr("data-src"); 
    if($this.hasClass("doc-type-zip")!= true){
        $.ajax({ 
                type: 'post',
                url: baseUrl + "customer/request/open_file_type",
                data: {"ext":'', "url":fileurl},
                success: function (res) { 
                        $.fancybox.open(res);
                }
            });
    }
});

/**********start draft file upload********/
$(document).on('click', '.darftattchmnt', function () {
    var draftid = $(this).data('draft-id');
    var id = 'shre_file_'+draftid;
    $("input[id='"+id+"']").click();
});

function uploadDraftchatfile(e, sender_role,reqID, senderid, rcvrole,rcvrid, sendername, profile, parentid, seenby){
    var reqid = reqID;
    var fileInput = e.target;
    var withornot = $(fileInput).attr("data-withornot");
    var reqfileid = $(fileInput).attr('data-draftid');
    if (fileInput.files.length > 0) {
        var formData = new FormData();
        $.each(fileInput.files, function (k, file) {
            var fileName = file.name;
            var type = fileName.split('.').pop();
            var name = fileName.split('.');
            if($.inArray(type,imgArr) !== -1){
                  var name_file = name[0].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, Math.floor(Math.random() * 6) + 1);
                    name_file = name_file+'.'+type;
                }else{
                    name_file = fileName; 
                }
//             console.log("newFileName",name_file);
            var newFileName = name_file;
            formData.append('shre_main_draft_file[]', file, newFileName);
            formData.append('saved_draft_file[]', newFileName);
            formData.append("data[request_file_id]", reqfileid);
            formData.append("data[request_id]", reqID);
            formData.append("data[sender_role]", sender_role);
            formData.append("data[sender_id]", senderid);
            formData.append("data[reciever_id]", rcvrid);
            formData.append("data[reciever_role]", rcvrole);
            formData.append("data[sendername]", sendername);
            formData.append("data[withornot]", withornot);
            formData.append("data[profile]", profile);
            formData.append("data[parentid]", parentid);
            formData.append("data[seenby]", seenby);
//            console.log("formData",formData);
        });
        $(".ajax_searchload").show();
        $.ajax({
            type: 'post',
            url: baseUrl + "admin/dashboard/uploaddraftChatfiles",
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                $(data).each(function (index, value) {
//                    console.log("draft",value);
                    $(".ajax_searchload").hide();
                    if(value.status == 'success'){
                    var left = 'left';
                    var right = 'right';
                    var appendright = 'rightreply';
                    var html = ""; 
                    var defaultFile;
                    var fileExt = (get_extension(value.file)).toLowerCase();
                    //console.log("inside",fileExt);
                    var newFileName = fileInput.files[index].name;
                    var filenAmeThumb =value.file.replace(" ", "_");
                    var newFileNametitle = newFileName.substr(0, 20) + (newFileName.length > 20 ? "..."+fileExt :'');
                    var downld = $assets_path+'img/customer/dwnlod-img.svg';
                    var downldimg = '<img src="'+downld+'">';
                if($.inArray(fileExt,DocArr) !== -1){
                     defaultFile =  $assets_path+'img/default-img/chat-file.svg';
                     html = '<div class="contain-info"><a class="open-file-chat doc-type-file " data-ext="'+fileExt+'" data-src="' + requestdraftchatimg + value.req_id + "/" + value.file_id + "/" + filenAmeThumb + '"><img src="'+defaultFile+'"><span class="file-ext">'+fileExt+'</span><div class="file-name"><h4><b>'+newFileNametitle+'</b></h4></div></a><div class="download-file"> <a href="'+baseUrl+'customer/request/download_stuffFromChat?ext='+fileExt+'&file-upload='+requestmainchatimg + value.req_id + '/' + value.file_id + '/' + newFileName+'" target="_blank">'+downldimg+'</a></div>'; 
                }else if($.inArray(fileExt,imgArr) !== -1){
                      html = '<div class="contain-info"><a class="open-file-chat doc-type-image" data-ext="'+fileExt+'" data-src="' + requestdraftchatimg + value.req_id + "/" + value.file_id + "/" + "_thumb/" + filenAmeThumb + '"><img src="' + requestdraftchatimg + value.req_id + "/" + value.file_id + "/" + "_thumb/" +filenAmeThumb+ '"><div class="file-name"><h4><b>'+newFileNametitle+'</b></h4></div></a><div class="download-file"> <a href="'+baseUrl+'customer/request/download_stuffFromChat?ext='+fileExt+'&file-upload='+requestmainchatimg + value.req_id + '/' + value.file_id + '/' + newFileName+'" target="_blank">'+downldimg+'</a></div>'; 
                }else if($.inArray(fileExt,font_file) !== -1){ 
                     defaultFile =  $assets_path+'img/default-img/chat-file.svg';
                     html = '<div class="contain-info"><a class="open-file-chat doc-type-file " data-ext="'+fileExt+'" data-src="' + requestdraftchatimg + value.req_id + "/" + value.file_id + "/" + + filenAmeThumb + '"><img src="'+defaultFile+'"><span class="file-ext">'+fileExt+'</span><div class="file-name"><h4><b>'+newFileNametitle+'</b></h4></a><div class="download-file"> <a href="'+baseUrl+'customer/request/download_stuffFromChat?ext='+fileExt+'&file-upload='+requestmainchatimg + value.req_id + '/' + value.file_id + '/' + newFileName+'" target="_blank">'+downldimg+'</a></div>'; 
                }else if($.inArray(fileExt,vedioArr) !== -1){
                  html = '<div class="contain-info"><a class="open-file-chat doc-type-file " data-ext="'+fileExt+'" data-src="' + requestdraftchatimg + value.req_id + "/"  + value.file_id + "/" + + filenAmeThumb + '"><img src="'+defaultFile+'"><span class="file-ext">'+fileExt+'</span><div class="file-name"><h4><b>'+newFileNametitle+'</b></h4></a><div class="download-file"> <a href="'+baseUrl+'customer/request/download_stuffFromChat?ext='+fileExt+'&file-upload='+requestmainchatimg + value.req_id + '/' + value.file_id + '/' + newFileName+'" target="_blank">'+downldimg+'</a></div>'; 
                }else if($.inArray(fileExt,audioArr) !== -1){ 
                     html = '<div class="contain-info"><a class="open-file-chat doc-type-file " data-ext="'+fileExt+'" data-src="' + requestdraftchatimg + value.req_id + "/" + value.file_id + "/" + + filenAmeThumb + '"><img src="'+defaultFile+'"><span class="file-ext">'+fileExt+'</span><div class="file-name"><h4><b>'+newFileNametitle+'</b></h4></a><div class="download-file"> <a href="'+baseUrl+'customer/request/download_stuffFromChat?ext='+fileExt+'&file-upload='+requestmainchatimg + value.req_id + '/' + value.file_id + '/' + newFileName+'" target="_blank">'+downldimg+'</a></div>'; 
                }else if($.inArray(fileExt,zipArr) !== -1){ 
                     defaultFile =  $assets_path+'img/default-img/chat-zip.svg';
                     html = '<div class="contain-info"><a class="open-file-chat doc-type-zip" data-ext="'+fileExt+'" data-src="' + requestdraftchatimg + value.req_id + "/" + value.file_id + "/" + + filenAmeThumb + '"><img src="'+defaultFile+'"><span class="file-ext">'+fileExt+'</span><div class="file-name"><h4><b>'+newFileNametitle+'</b></h4></a><div class="download-file"> <a href="'+baseUrl+'customer/request/download_stuffFromChat?ext='+fileExt+'&file-upload='+requestmainchatimg + value.req_id + '/' + value.file_id + '/' + newFileName+'" target="_blank">'+downldimg+'</a></div>'; 
                } 
                  $(".messagediv_" +reqfileid + "").prepend('<li><div class="comment-box-es new-patrn"><div class="comments_link"><div class="cmmbx-row"><div class="all-sub-outer"><div class="cmmbx-col left "><div class="clickable" id=' + value.id + '><div class="cmmbxcir"><figure class="pro-circle-img-xxs"><img src="' + profile_path + $designer_img + '" class="mCS_img_loaded"></figure></div><div class="cmmbx-col"><div class="ryt_header"><span class="cmmbxtxt">' + $customer_name + ' </span><span class="kevtxt">Just Now</span></div><span class="msgk-umsxxt took_'+value.id+'"> '+html+' </span><div class="edit_del"><a href="javascript:void(0)" onclick="replyButton(' + value.id + ', true)" class="reply_to_' + value.id + '"><i class="fas fa-reply-all"></i></a><a href="javascript:void(0)" onclick="editMessages(' + value.id + ', true)" class="edit_to_' + value.id + '"><i class="icon-gz_edit_icon"></i></a><a href="javascrip:void(0)" onclick="deleteMsg(' + value.id + ');" class="del_msg"><i class="icon-gz_delete_icon"></i></a></div></div></div><div class="msgk-mn-edit comment-submit_ryt_' + value.id + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editryt_' + value.id + '">' + value.file + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + value.id + ',\'' + right + '\')" data-id="' + value.id + '">Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + value.id + ')" class="cancel">Cancel</a></form></div></div><div class="closeoncross reply_msg_ryt_' + value.id + '" style="display:none;"><form action="" method="post"><div class="cmmtype-row" onclick="wideText(this)"><textarea class="pstcmm replying sendtext text1_' + reqfileid + '" id="text2_' + value.id + '" placeholder="Post Comment" autocomplete="off" data-reqid="' + reqfileid + '"></textarea><a class="cmmsendbtn send_comment_without_img send_' + reqfileid + '" onclick="send_comment_without_img(' + reqfileid + ',\'' + sender_role + '\',\'' + value.id + '\',\'' + appendright + '\',\'' + value.id + '\');"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive"></a></div></form></div></div></div></div></div></li>');
                }else{
                    $(".messagediv_" +reqfileid + "").prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_role + 'del_' + value.id + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + value.id + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + value.id + '"></i><div class="open-edit open-edit_' + value.id + '" style="display: none;"><a href="javascript:void(0)" class="deletemsg" data-delid="' + value.id + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + value.id + '" style="display:none"></div> <div class="msgk-mn msg_desc_' + value.id + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + value.id + ' edit_icon_msg"></span><span class="msgk-umsxxt took_' + value.id + '" style="color:red">' + value.error_msg + '</span></pre></div></div></div></div>');
                }
                });
            }
        });
}
}
/**********end draft file upload********/

function get_extension(file ) {
    var extension = file.replace(/^.*\./, '');
    return extension;
}
$(".outer-post").on("scroll",function() {
  $(".leader-line").hide();

});

$(document).on('click','.expand_txt',function (){
    if($(this).hasClass("read_more")){
        $(this).addClass("hide"); 
        $(this).next(".read_more_txt").removeClass("hide").next(".expand_txt").removeClass("hide"); 

    }else{
        $(this).addClass("hide"); 
        $(this).prev(".read_more_txt").addClass("hide").prev(".expand_txt").removeClass("hide"); 
    }
});    

$(document).on("click ",".attch-icon .view-file",function(){
    $this = $(this); 
     var ext = $this.attr("data-ext");
     var fileurl = $this.attr("data-href"); 
    $(".ajax_loader").show();
        $.ajax({ 
                type: 'post',
                url:  baseUrl +"customer/request/open_file_type",
                data: {"ext":'', "url":fileurl},
                success: function (res) { 
                    $(".ajax_loader").hide();
                        $.fancybox.open(res);
                }
            });
    
});