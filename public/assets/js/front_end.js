jQuery(document).ready(function ($) {
    jQuery('.logs-slider').owlCarousel({
        margin: 20,
        loop: true,
        lazyLoad: true,
        autoplay: true,
        smartSpeed: 200,
        slideTransition: 'linear',
        autoplaySpeed: 1000,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            },
            992: {
                items: 4
            },
            1100: {
                items: 6
            }
        }
    });
    checkClasses();
    checkaboutClasses();
    var names = $('.logs-slider').find('.firstActiveItem > .item').attr('data-src');
    $("#hero").css("background-image", "url('" + names + "')");

    jQuery('.logs-slider').on('translated.owl.carousel', function (event) {
        checkClasses();
        var names = $(this).find('.firstActiveItem > .item').attr('data-src');
        $("#hero").css("background-image", "url('" + names + "')");
    });

    jQuery('.logs-slider').on('click', '.item', function () {
        $('.owl-item').removeClass('firstActiveItem');
//                $(this).parent().prevAll().removeClass('active');
//                $(this).parent().nextAll().addClass('active');
        $(this).parent().addClass('firstActiveItem');
        var names = $(this).attr('data-src');
        $("#hero").css("background-image", "url('" + names + "')");
    });
    function checkClasses() {
        var total = $('.logs-slider .owl-stage .owl-item.active').length;
        $('.logs-slider .owl-stage .owl-item').removeClass('firstActiveItem');
        $('.logs-slider .owl-stage .owl-item.active').each(function (index) {
            if (index === 0) {
                // this is the first one
                $(this).addClass('firstActiveItem');
            }
        });
    }
    
    jQuery('.features_slider').owlCarousel({
          autoplayHoverPause: true,
          margin: 20,
          lazyLoad: true,
          loop:true,
          autoplay:true,
          dots:false,
          responsive: {
            0: {
              items: 1
            },
            600: {
              items: 2
            },
            992: {
              items: 4  
            }
          }
        });
        
        jQuery('.testomonial').owlCarousel({
        loop: true,
        margin: 20,
        items:3,
        lazyLoad: true,
        nav: true,
        dots:false,
        autoplay:true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items:2
          },
          992: {
            items: 3
          }
        }
      });

jQuery('.logs-about-slider').owlCarousel({
          slideSpeed : 1000,
          margin: 20,
          lazyLoad: true,
          items: 4,
          loop:true,
          autoplay:true,
          nav: true,
          responsive: {
            0: {
              items: 1
            },
            600: {
              items: 2
            },
            992: {
              items: 4
            }
          }

        });
        
jQuery('.logs-about-slider').on('translated.owl.carousel', function(event) {
    checkaboutClasses();
});
function checkaboutClasses(){
          var total = $('.logs-about-slider .owl-stage .owl-item.active').length;
          $('.logs-about-slider .owl-stage .owl-item').removeClass('firstActiveItem');
          $('.logs-about-slider .owl-stage .owl-item.active').each(function(index){
            if (index === 0) {
                // this is the first one
                $(this).addClass('firstActiveItem');
              }
            });
        }
        
if($('.pricebasedquantity').length > 0){ 
    var setSelected = jQuery("#priceselector").val();
    var setAmount = jQuery("#priceselector").find(':selected').data('amount');
    Setselectedvalue(setSelected,setAmount); 
}
});


/******portfolio js******/
// function make_grid(){
//    var grid = $(".grid");
//    var getItem = grid.children('li');
//    getItem.each(function(){
//        var getItem_height = 0;
//        getItem_height = $(this).find('img').height();
//        var rowHeight =  parseInt($(grid).css( "grid-auto-rows" ));
//        var rowGap = parseInt($(grid).css('grid-row-gap'));
//        if(getItem_height == 0){
//            getItem_height = 363; //static height if height is 0
//        }
//        var rowSpan = Math.ceil((getItem_height + rowGap) / (rowHeight + rowGap));
//        //console.log("rowSpan",rowSpan);
//        if(rowSpan > 15){
//            $(this).css( "grid-row-end", "span 15");
//        }
//        else{
//            $(this).css( "grid-row-end", "span " + rowSpan );
//        }
//    });
//}
//
//$(window).load(function(){
//     make_grid();
// });
 
/**************start on load & tab change get all records with load more button*****************/
/*
$(document).ready(function () {
    $('.tab-pane').attr("data-loaded", $rowperpage);
     $(".ajax_loader").css("display","block");
    $(document).on("click", "#load_more", function (e) {
        e.preventDefault();
        $(".ajax_loader").css("display","block");
        $(".load_more").css("display","none");
        var row = parseInt($(this).attr('data-row'));
        row = row + $rowperpage;
        var activeTabPane = $('.tab-pane.active');
        var allcount = parseInt(activeTabPane.attr("data-total-count"));
        var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
        var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
        var status_scroll = $('#tabsJustified li a.active').attr('data-id');
        var tabid = $('#tabsJustified li a.active').attr('href');
        if (allLoaded < allcount) {
                $.post(BASE_URL+'welcome/load_more_portfolio', {'group_no': activeTabPaneGroupNumber, 'id': status_scroll},
                function (data) {
                    if (data != "") {
                        $(tabid).append(data);
                        //$(tabid+" .product-list-show .row").append(data);
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display","none");
                        $(".load_more").css("display","inline-block");
                        activeTabPane.find('.load_more').attr('data-row', row);
                        activeTabPaneGroupNumber++;
                        activeTabPane.attr('data-group', activeTabPaneGroupNumber);
                        allLoaded = allLoaded + $rowperpage;
                        activeTabPane.attr('data-loaded', allLoaded);
                        make_grid();
                        if (allLoaded >= allcount) {
                            $('#load_more').css("display", "none");
                            $(".ajax_loader").css("display","none");
                        }else{
                            activeTabPane.find('.load_more').css("display", "inline-block");
                            $(".ajax_loader").css("display","none");
                        }
                    }
                });
            }
    });
});
/**************End on load & tab change get all records with load more button*****************/
/***********portfolio active tab from home page***************/
//var url = window.location.href;
//var activeTab = url.substring(url.indexOf("#") + 1);
//var hash = window.location.href.indexOf('#');
//if(activeTab != '' && hash > 0){
//    $(".tab-pane").removeClass("active");
//    $('#' + activeTab).addClass("active show");
//    make_grid();
//    var id = $('.show_loading').data("target");
//    var list = $("" + id + " li");
//    var button = $(".load_more");
//    //loadMore(list, button);
//     $('#tabsJustified').find('li a').each(function(e) {
//        var dataattr = $(this).data('target');
//        var fromurl = ("#" + activeTab);
//        if(dataattr == fromurl){
//            $(this).addClass('active');
//            $(this).closest('li').siblings().find('a').removeClass('active');
//            return false;
//        }
//   });
//}
//
//$(document).on('click','#tabsJustified li a',function(e){
//$(".ajax_loader").css("display","inline-block");
//$(".load_more").css("display","none");
//var hrefdata = $(this).attr('href');$('.tab-pane').removeClass('active show');
//$(hrefdata).parent('.tab-pane').addClass('active show');
//var currentstatus = $(this).data('id');
//var tab = $(this).attr('href');
//e.preventDefault();
//    load_more_portfolio(currentstatus,tab);
//});
//
//function load_more_portfolio(currentstatus,tabActive){
//    var activetab = $('#tabsJustified li a.active').attr('href');
//    var tabid = (tabActive != '') ? tabActive : activetab ;
//    var activeTabPane = $('.tab-pane.active');
//    var status_Active = $('#tabsJustified li a.active').attr('data-id');
//    var id   = currentstatus;
//    if(currentstatus !== ''){
//       id =  currentstatus;
//    }else{
//       id =  status_Active;
//    }
//    $.post(BASE_URL+'welcome/load_more_portfolio', {'group_no': 0, 'id': id},
//        function (data){
//            var newTotalCount = 0;
//            if(data != ''){
//                 $(".ajax_loader").css("display","none");
////                console.log(tabid,data);
//                $(".ajax_searchload").fadeOut(500);
//                $(tabid).html(data);
//                 make_grid();
//                newTotalCount = $(tabid).closest('.tab-pane').find("#loadingAjaxCount").attr('data-value');
//                $(tabid+" .product-list-show .row").find("#loadingAjaxCount").remove();
//                activeTabPane.attr("data-total-count",newTotalCount);
//                activeTabPane.attr("data-loaded",$rowperpage);
//                activeTabPane.attr("data-group",1);
//            }else{
//                activeTabPane.attr("data-total-count",0);
//                activeTabPane.attr("data-loaded",0);
//                activeTabPane.attr("data-group",1);
//                $(tabid).html("");
//            }
//            if ($rowperpage >= newTotalCount) {
//                $('#load_more').css("display", "none");
//            } else{
//                $('#load_more').css("display", "inline-block");
//            }
//        });
//}
/********pricing************/
//jQuery(document).ready(function () {
//    console.log("data",subscription_plan);
//    if (subscription_plan) {
//        //subscription_plan = jQuery.parseJSON(subscription_plan);
//    }
//    var agencyPlanOptions = '';
//    $.each(subscription_plan, function (k, v) {
//        var value = parseInt(k) * parseInt(total_active_requests);
//        agencyPlanOptions += '<option value="' + k + '" data-amount="' + v.amount + '">' + value + '</option>';
//    });
//    $('#pricebasedquantity').html(agencyPlanOptions);
//
//    jQuery('#pricebasedquantity').on('change', function () {
//        var subscription_plan_detail = subscription_plan[this.value];
//        var Subs_price = subscription_plan_detail.amount / 100;
//        var subs_quantity = this.value;
//        var final_SubPrice = Subs_price;
//        jQuery('.agency_prc').html(final_SubPrice);
//        jQuery('.btn-plan_pricing').data('value', this.value);
//    });
//});
//
//$(document).on('click', '.btn-plan_pricing', function () {
//    var planname = $(this).data("id");
//    var value = $(this).data("value");
//    if (value == 0) {
//        var url = BASE_URL+'signup?pln=' + planname;
//    } else {
//        var url = BASE_URL+'signup?pln=' + planname + '&val=' + value;
//    }
////    console.log("planname: " + planname, "value: " + value);
//    window.location.href = url;
//
//});

/********signup start********/

$(document).on('change', '#priceselector', function() {
    var getSelectedPrice = this.value;
    var getAmount = $(this).data("amount");
  Setselectedvalue(getSelectedPrice,getAmount);
});

function Setselectedvalue(selectedval,setAmount) {
        $plan_price = setAmount;
        jQuery('#annual_price').val(selectedval);
        jQuery('#selected_price').val(setAmount);
        jQuery('#final_plan_price').val(setAmount);
        jQuery('#change_prc span').html(setAmount);
        jQuery('#grand_total span').html(setAmount);
        jQuery('.disc-code').val('');  
        jQuery('.ErrorMsg').css('display','none');
        jQuery('.coupon-des-newsignup').css('display','none');
        $.ajax({
            type: 'POST',
            url: BASE_URL+'welcome/getfraturesofplan',
            data: {planid: selectedval},
            dataType: 'json',
            success: function (data) {
//                console.log(data);
                if(data[0].features){
                    jQuery('.monthly-activate').html(data[0].features);
                        showquantityselector(selectedval,data[0].plan_type);
			jQuery('#couponinserted_ornot').val(data[0].apply_coupon);
                        jQuery('#in_progress_request').val(data[0].global_inprogress_request);
                        jQuery('.pricebasedquantity').attr('data-key',data[0].plan_type);
                        jQuery('.pricebasedquantity').attr('data-planname',selectedval);
                }
            }
        });
    var discount_empty =  jQuery('.disc-code').val();
        if(discount_empty == ''){
       $('.ErrorMsg').css('display','none');
        $discount_amount = 0;
        $discount_notes = '';
        Gettaxbasisonstate();
       return; 
    }else{
        Gettaxbasisonstate();
    }
}

function showquantityselector(val,type){
    
    var total_inprogress_req = total_inprogress; 
    var agencyPlanOptions = '';
        $.each(allplans_price[val],function(k,v){
              if(type == 'yearly'){
                   var selected_price = v.annual_price;
               }else{
                  var selected_price = v.amount;
               }
            if (v.quantity == total_inprogress_req){
                var selected = "selected";
                   $('#change_prc span').html(selected_price);
                   $('#grand_total span').html('$'+selected_price);
                   $('#selected_price').val(selected_price);
                   $('#final_plan_price').val(selected_price);
                   $plan_price = selected_price;
               }else{
                var selected = "";
            }
            var value = v.quantity;
            agencyPlanOptions += '<option value="'+v.quantity+'" data-amount="'+selected_price+'" '+selected+'>'+value+'<span> &nbsp;Dedicated Designers</span></option>';
        });
        $('.pricebasedquantity').html(agencyPlanOptions); 
}

$(document).ready(function () {
    $("button.menu-toggle").click(function () {
        $(".mobile-class").slideToggle();
    });
});

function showPriceSectionHTML(){
    //console.log("testing...");
    $plan_price = parseFloat($plan_price);
    $tax_amount = parseFloat($tax_amount);
    $discount_amount = parseFloat($discount_amount);
//    console.log('plan_price',$plan_price);
//    console.log('tax_amount',$tax_amount);
//    console.log('discount_amount',$discount_amount);
    var total_price = (($plan_price +  $tax_amount) - $discount_amount);
    if(total_price % 1 !== 0){
        total_price = total_price.toFixed(2);
    }
    var htmll = '<p class="plan-price">Plan Price:<span id="change_prc" class="box item1">$<span>'+$plan_price+'</span></span></p>';
    if(parseFloat($discount_amount) > 0){
        htmll += '<p class="plan-price">Discount: <span class="box item12"><span>- $'+$discount_amount.toFixed(2)+'</span></span></p>';	
    }
    if(parseFloat($tax_amount) > 0){
            htmll += '<p class="plan-price tax_prc_for_texas">Sales Tax ('+$tax_value.toFixed(2)+'%) :<span id="tax_prc" class="box item12"><span>$'+$tax_amount.toFixed(2)+'</span></span></p>';
    }
    htmll += '<hr>';
    htmll += '<div class="g-total"><h3>Grand Total <span id="grand_total"><span>';
    if(parseFloat($discount_amount) > 0){
        var strikeamount = parseFloat($plan_price) + parseFloat($tax_amount);
            htmll += '<strike>$';
            htmll += Math.round(strikeamount * 100) / 100;
            htmll += '</strike> $'+total_price;
    } else{
            htmll += '$'+total_price;
    }
    htmll += '</span></span></h3></div>';
    if(parseFloat($discount_amount).toFixed(2) > 0){
            htmll += '<span class="coupon-des-newsignup">'+$discount_notes+'</span>';
    }
    $('.total-pPrice').html(htmll);
}

//function showPriceSectionHTML(){
//    $plan_price = parseFloat($plan_price);
//    $tax_amount = parseFloat($tax_amount);
//    $discount_amount = parseFloat($discount_amount);
//       var total_price = (($plan_price +  $tax_amount) - $discount_amount); 
//    if(total_price % 1 !== 0){
//        total_price = total_price.toFixed(2);
//    }
//    var htmll = '<p class="plan-price">Plan Price:<span id="change_prc" class="box item1">$<span>'+$plan_price+'</span></span></p>';
//    if(parseFloat($discount_amount) > 0){
//        htmll += '<p class="plan-price">Discount: <span class="box item12"><span>- $'+$discount_amount.toFixed(2)+'</span></span></p>';	
//    }
//    if(parseFloat($tax_amount) > 0){
//            htmll += '<p class="plan-price tax_prc_for_texas">Sales Tax ('+$tax_value.toFixed(2)+'%) :<span id="tax_prc" class="box item12"><span>$'+$tax_amount.toFixed(2)+'</span></span></p>';
//    }
//    htmll += '<hr>';
//    htmll += '<div class="g-total"><h3>Grand Total <span id="grand_total"><span>';
//    if(parseFloat($discount_amount) > 0){
//            htmll += '<strike>$';
//            htmll += parseFloat($plan_price) + parseFloat($tax_amount);
//            htmll += '</strike> $'+total_price;
//    } else{
//            htmll += '$'+total_price;
//    }
//    htmll += '</span></span></h3></div>';
//    if(parseFloat($discount_amount).toFixed(2) > 0){
//            htmll += '<span class="coupon-des-newsignup">'+$discount_notes+'</span>';
//    }
//    $('.total-pPrice').html(htmll);
//}
    
function Gettaxbasisonstate(statename){
if(statename == '' || typeof statename == 'undefined'){
    var statename = jQuery('#state').val();
}else{
    var statename = statename;
}
var jqueryarray = sales_tax;
var final_price = parseFloat($plan_price) - parseFloat($discount_amount);
if((statename in jqueryarray)){ 
    var tax = jqueryarray[statename];
                //console.log('tax from array',tax);
                //console.log(parseFloat(tax));
                var total = parseFloat(final_price) * (parseFloat(tax)/100);
                var tax_rounded = total.toFixed(2); 
                var percent_amount = parseFloat(final_price) + total;
                var total_percent_amount = percent_amount.toFixed(2); 
                //console.log(total);
                //console.log(grand_total);
                //jQuery('.tax_prc_for_texas').css("display","block");
                //jQuery('.tax_prc_for_texas').html('Sales Tax ('+tax+'%) :<span id="tax_prc" class="box item12"><span>$'+tax_rounded+'</span></span>');
                //jQuery('#grand_total span').html(total_percent_amount);
                jQuery('#selected_price').val(total_percent_amount);
                jQuery('#tax_amount').val(total);
                jQuery('#state_tax').val(tax);
                $tax_amount = total;
                $tax_value = tax;
        }else{
                //jQuery('.tax_prc_for_texas').css("display","none");
                //jQuery('#grand_total span').html(final_price);
                jQuery('#tax_amount').val("0");
                jQuery('#selected_price').val(final_price);
                jQuery('#state_tax').val("0");
                $tax_amount = 0;
                $tax_value = 0;
        }
        //$discount_amount = 0;
        //$('.disc-code').value('');
        showPriceSectionHTML();
}
$(document).on('change','#state',function(){    
    var statename = this.value;
    if(statename == '---'){
       jQuery('.ErrorMsg.state_validation').css('display','block');
       jQuery('.ErrorMsg.state_validation').html('<span class="validation">Please select any other state</span>')
       jQuery('#state').val("");
   }else{
    jQuery('.ErrorMsg.state_validation').html('');
}
Gettaxbasisonstate(statename);
});

$(document).on('change','.pricebasedquantity',function(){
    
    var selectedplan = $('option:selected', this).parent().attr('data-planname');
    var plantype = $('option:selected', this).parent().attr('data-key');
    var subscription_plan_detail = allplans_price[selectedplan];  
    var subs_quantity = this.value;
    if(plantype == 'yearly'){
        var Subs_price = subscription_plan_detail[subs_quantity].annual_price;
    }else{
        var Subs_price = subscription_plan_detail[subs_quantity].amount;
    }
    
    var final_SubPrice = Subs_price;
    $plan_price = Subs_price;
     jQuery('#selected_price').val(final_SubPrice);
     jQuery('#final_plan_price').val(final_SubPrice);
     jQuery('#change_prc span').html(final_SubPrice);
     jQuery('#grand_total span').html(final_SubPrice);
     jQuery('#in_progress_request').val(subs_quantity);
     jQuery('.disc-code').val('');  
     jQuery('.ErrorMsg').css('display','none');
     jQuery('.coupon-des-newsignup').css('display','none');
     var discount_empty =  jQuery('.disc-code').val();
      if(discount_empty == ''){
     $('.ErrorMsg').css('display','none');
      $discount_amount = 0;
      $discount_notes = '';
      Gettaxbasisonstate();
     return; 
  }else{
      Gettaxbasisonstate();
  }
});
       

 
function closeAllSelect(elmnt) {
            /*a function that will close all select boxes in the document,
            except the current select box:*/
            var x, y, i, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            for (i = 0; i < y.length; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i)
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < x.length; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        }
        /*if the user clicks anywhere outside the select box,
        then close all select boxes:*/
        document.addEventListener("click", closeAllSelect);
 
$(document).on('click', '.select-items div', function () {
    Setselectedvalue($(this).attr('value'),$(this).attr('data-amount'));
    // 
});

function dateFormat(current,val) {
    if (val.length == 2) {
        $(current).val($(current).val() + '/');
    }
}

//$( "#normal_signup" ).submit(function() {
$(document).on('submit', '#normal_signup', function () {
        $('.loaderimg').attr('src', assetspath+'img/ajax-loader.gif');
        $('#submit').prop('disabled',true);
   });
   
$(document).on('blur','.disc-code',function(e){
            e.preventDefault();
            var dicountCode = $('.disc-code').val();
            var setSelected = $plan_price;
            var apply_couponor_not = $("#couponinserted_ornot").val();
            var selectedplan = $("#annual_price").val();
           // var setSelected = $("#selected_price").val();
            //var orginal_price = setSelected.strike();
        $("span.CouponSucc_code, span.CouponErr_code").remove();
        if(dicountCode == ''){
            $('.ErrorMsg').css('display','none');
            //$('.coupon-des-newsignup').css({"display": "none"});
           // $('#selected_price').val(setSelected);
           //jQuery('#grand_total span').html(setSelected);
            $discount_amount = 0;
            $discount_notes = '';
            Gettaxbasisonstate();
           return;
       }
       else{
        $('.ErrorMsg').css('display','block');
    }
    $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponSucc_code"><img src="'+assetspath+'img/ajax-loader.gif" alt="loading"/></span>');
    delay(function(){
        $.ajax({
            type: 'POST',
            url: BASE_URL+'welcome/CheckCouponValidation',
            data: {dicountCode: dicountCode,apply_couponor_not:apply_couponor_not},
            dataType: 'json',
            success: function (data) {
                  //console.log(data);
                  $('.ErrorMsg').css('display','block');
                  $('.couponinserted_ornot').val(data.inertcoupon);
                  if (data.status == '1') {
                      if(data.return_data.valid == '1'){
                        if(data.return_data.amount_off !== null){
                            var str = data.return_data.amount_off;
                            var resStr = str/100;
                            $discount_amount = resStr;
                            //var finalAmount = setSelected - resStr;                              
                        } else{
                            var TotalPercent = data.return_data.percent_off;
                            var calcPrice  =  setSelected * (TotalPercent / 100 );
                            $discount_amount = calcPrice;
                            //var finalAmount = setSelected - calcPrice;
                           // console.log(finalAmount);
                       }
                       //var total_finalAmountt = finalAmount.toFixed(2); 
                      // $('#grand_total span').html(orginal_price + ' $'+total_finalAmountt);
                      // jQuery('#final_plan_price').val(total_finalAmountt);
                       $('.CouponErr_code').css({"display": "none"});
                      // $('.coupon-des-newsignup').css('display','block');   
                       $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponSucc_code"><i class="fas fa-check-circle"></i> Coupon Applied</span>');
                    var discount_notes = '';                  
                    if(data.return_data.duration == 'forever'){
                        discount_notes = '<p>Forever</p>';
                        //$('.coupon-des-newsignup').html('<p>Forever</p>');
                    }else if(data.return_data.duration == 'once'){
                        discount_notes = '<p>One Time Only</p>';
//                       $('.coupon-des-newsignup').html('<p>1st Month Only</p>');
                   } else{
                        discount_notes = '<p>First ' + data.return_data.duration_in_months + ' Months Only</p>';
//                       $('.coupon-des-newsignup').html('<p>First ' + data.return_data.duration_in_months + ' Months Only</p>');
                   }
                   $discount_notes = discount_notes;
                    Gettaxbasisonstate();
               }
           }
           else{
               //$('#grand_total span').html(setSelected);
                $('.CouponSucc_code').css({"display": "none"});
                $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponErr_code"><i class="fas fa-times-circle"></i> ' + data.message + '</span>');
                //$('.coupon-des-newsignup').css({"display": "none"});
                $discount_amount = 0;
                $discount_notes = '';
                Gettaxbasisonstate();
           }
       }
   });  
    }, 1000 );
    Gettaxbasisonstate();

});

 var delay = (function(){
          var timer = 0;
          return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();
    
//$( "#signupnewfrm" ).submit(function() {
//    console.log("tester");
//    $('.loaderimg').attr('src', '<?php echo FS_PATH_PUBLIC_ASSETS ?>img/ajax-loader.gif');
//    $('#submit').prop('disabled',true);
//});    

//   abonded signup jquery code start from here ps
function Capture_values(fire,alternate="",ip="",from_page="",from_page_plan=""){

    var current_plan,plan_fname,ip,from_page,from_page_plan,fval,fname,sess;
     current_plan =  $("#annual_price").val();
     plan_fname =  $("#annual_price").attr("name");
     ip =  $("#current_ip").val();
     from_page = $("#from_page").val(); 
     from_page_plan = $("#from_page_plan").val(); 

    if(fire!="0" && alternate ==""){
         fval = fire.val(); 
         fname = fire.attr("name"); 


    }else{
         fval = alternate; 
         fname = "first_name";
         sess = "0";
         current_plan = from_page_plan;

    } 
    $.ajax({
          type: 'POST',
          url: BASE_URL+"welcome/Capture_cart_entry_moments",
          data: {"filed_value":fval,"filed_name":fname,"sess":sess,"plan":current_plan,"plan_field":plan_fname,"ip":ip,"from_page":from_page},
          success: function (data) {
             console.log(data); 
          }
      });

}
// ends here 
/*******signup end***********/
 
