<!DOCTYPE html>
<html>
    <head>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <title>Email Template</title>
        <style type='text/css'>
              .header_section{
                background: #102132;
                padding: 25px;
            }
            .container{
                width: 50%;
                margin: 0 auto;
            }
            .title{
                font:normal 30px/30px 'GothamPro-Medium', sans-serif;
                font-weight: 800;
                color: #e8304d;
                margin: 50px 35px 30px 35px;
            }
            .content_text{
                font:normal 15px/30px 'GothamPro-Medium', sans-serif;
                color: #20364c;
                margin: 0px 26px 30px 35px;
            }
            a.verify_btn {
                background: #e8304d;
                color: #fff;
                text-decoration: none;
                font: normal 16px/30px 'GothamPro-Medium',sans-serif;
                padding: 7px 36px;
                border-radius: 25px;
                text-transform: uppercase;
                font-weight: bold;
                margin-top: 20px;
                display: inline-block;
            }
            .header_img{
                width: 100%;
            }
            .button_verify{
                margin: 0px 35px 30px 35px;
            }
            a.expire_count {
                color: #e8304d;
                text-decoration: none;
            }
            .content_sec {
                margin-top: 67px;
            }
            .footer_section{
                background-image: url('<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/emailtemplate_img/footer.png');
                background-size: 100% 100%;
                background-repeat: no-repeat;
                display: flex;
            }
            .footer_sec {
                width: 23%;
            }
            .footer_inner{
                width:100%;
                padding: 25px 0px;
            }
            .footer_inner ul {
                display: inline-flex;
                list-style: none;
                padding-left: 0px;
            }
            .footer_inner ul li{
                margin-left: 8px;
                font: 13px/15px 'GothamPro-Medium', sans-serif;
                color: #91accf;
                font-weight: 600;
            }
            .footer_sec ul {
                display: inline-flex;
                list-style: none;
                padding-left: 0px;
            }
            .footer_sec ul li{
                margin-left: 8px;
            }
            .footer_sec p{
                font: 13px/15px 'GothamPro-Medium', sans-serif;
                color: #fff;
                margin-bottom: 0px;
                font-weight: 600;
            }
            .content_info{
                margin: 0px 26px 15px 35px;
                font: normal 15px/30px 'GothamPro-Medium',sans-serif;
    color: #20364c;
    margin: 0px 26px 15px 35px;
            }
            @media (max-width: 768px){
                .container{
                    width: 70%;
                }
                .footer_section {
                    display: block;
                }
                .footer_inner {
                    width: 100%;
                    padding: 25px 0px;
                }
                .footer_sec {
                    width: 100%;
                    text-align: center;
                }
            }
            @media (max-width: 425px){
                .container{
                    width: 100%;
                }
                .footer_inner {
                    padding:0px;
                }
                
            }
        </style>
    </head>
    <body>
        <?php //echo "<pre/>";print_r($key);exit;?>
        <div class='main_div'>
            <div class='container'>
               <div class='header_section'>
                            <img style="width: 100%;max-width: 250px;" class='header_img' src='<?php echo FS_PATH_PUBLIC_ASSETS;?>img/emailtemplate_img/logo.png'>
			</div>
                <div class='body_Section'>
                    <div class='content'>
                        <h1 class='title'>
                            Hi <?php echo ucfirst($user_name); ?>
                        </h1>
                        <h3 class='content_text'>
                            <?php echo ucfirst($sendername); ?> just shared the following project with you. Click the button below to view the project                       
                        </h3>
                        <div style="text-align:center;">
                            <p class="content_info" style="font-weight: bold;"> <?php echo $title; ?></p>
                                <div class='button_verify'>
                                    <a href="<?php echo $link;?>" class='verify_btn'>View Project</a>
                                </div>
                            </div>
                        <div class='content_sec'>
                                <h3 class='content_text'>
                                    Thank you,<br>
                                    Graphics Zoo Team
                                </h3>
                        </div>
                </div>
                <?php $this->load->view('emailtemplate/email_footer'); ?>
            </div>
        </div>
    </body>
</html>