<?php
foreach ($project_list as $project_msg) {
    if ($project_msg['request_id'] != '' || $project_msg['request_id'] != 0) {
        ?>
       <div id="list_<?php echo $project_msg['request_id']; ?>" class="project_sectionfr_msg  <?php if($project_msg['rd_admin_seen'] != 1){ echo "unread_msg"; }else{ echo "read"; } ?> <?php if($firstrequest_id == $project_msg['request_id']){ echo "active"; } ?>">

            <div class="clnt_prfl_wth_nam">
                <div class="prjct_clnt_prfl">
                        <img src="<?php echo $project_msg['user_profile_picture']; ?>" class="img-responsive">
                </div>     
                <div class="pro_client_name"><?php echo $project_msg['user_fname']; ?></div>
                
                
            </div>

            <div class="admin_prjct_clint">  
                
                    <div class="project_name_msg_list">
                      <a class="project_id_get_msg" data-id="<?php echo $project_msg['request_id']; ?>">  
                        <h4><p class="unread_circle"></p><?php echo $project_msg['request_title']; ?></h4>
                      </a>
                       <?php
                            if ($project_msg['request_status'] == "checkforapprove") {
                                $deliverdate = "Delivered on " . $project_msg['req_modified'];
                                $status = "Pending Approval";
                                $color = "bluetext";
                            } elseif ($project_msg['request_status'] == "active") {
                                $deliverdate = "Expected on " . date('M d, Y h:i A', strtotime($project_msg['req_last_update']));
                                $status = "In Progress";
                                $color = "green";
                            } elseif ($project_msg['request_status'] == "disapprove" && $project_msg['request_who_reject'] == 1) {
                                $deliverdate = "Expected on " . date('M d, Y h:i A', strtotime($project_msg['req_last_update']));
                                $status = "Revision";
                                $color = "orangetext";
                            } elseif ($project_msg['request_status'] == "disapprove" && $project_msg['request_who_reject'] == 0) {
                                $deliverdate = "Expected on " . date('M d, Y h:i A', strtotime($project_msg['req_last_update']));
                                $status = "Quality Revision";
                                $color = "red";
                            } elseif ($project_msg['request_status'] == "pending" || $project_msg['request_status'] == "assign") {
                                $status = "In Queue";
                                $color = "gray";
                            } elseif ($project_msg['request_status'] == "hold") {
                                $status = "On Hold";
                                $color = "gray";
                            } elseif ($project_msg['request_status'] == "draft") {
                                $status = "Draft";
                                $color = "gray";
                            } elseif ($project_msg['request_status'] == "approved") {
                                $deliverdate = "Approved on " . $project_msg['req_approvaldate'];
                                $status = "Completed";
                                $color = "green";
                            } elseif ($project_msg['request_status'] == "pendingrevision") {
                                $deliverdate = "Expected on " . $project_msg['req_modified'];
                                $status = "Pending Review";
                                $color = "lightbluetext";
                            } elseif ($project_msg['request_status'] == "cancel"){
                                $status = "Cancelled";
                                $color = "gray";
                            }else {
                                $status = "";
                                $color = "greentext";
                            }
                            ?>
                            
                            <?php
                                     if ($project_msg['is_star'] == "1") {
                                         $class = "star_project";
                                         $status_s = "remove_star";
                                     } else {
                                         $class = "gray_star";
                                         $status_s = "mark_star";
                                     }
                            ?>
                             <div class="comn-btn">
                                  <p> <span class="btn-d <?php echo $color; ?>" style="border-radius: 15px;"><?php echo $status; ?></span></p>
                                    <div class="view_req_frm_prjctlst">
                                          <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $project_msg['request_id']; ?>" target="_blank">
                                              <i class="fas fa-angle-right"></i>
                                          </a>
                                      </div>
                                      <div class="view_req_frm_prjctlst">
                                          <a href="<?php echo base_url() . "admin/accounts/view_client_profile/" . $project_msg['userid']; ?>" target="_blank">
                                              <i class="fas fa-user"></i>
                                          </a>
                                      </div>  
                                    <div class="my_fvrt mark_messages <?php  echo $class ?>" data-status="<?php echo $status_s; ?>"  data-id="<?php echo $project_msg['request_id']; ?>"> <p class="star_circle"><i class="fa fa-star"></i></p></div>
                            </div>
                        <div class="prjct_dlvrd_dt"><?php echo $deliverdate; ?></div>
                    </div>
                </a> 
                
            </div>

        </div>

    <?php }
}
?>