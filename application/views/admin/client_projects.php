<!-- <style type="text/css">
ul.list-header-blog li.active a{
    color: #e52344;
    border-bottom: 2px solid #e52344;
}
.list-header-blog.small li {
    margin-right: 0px;
}
.search-box {
position: absolute;
top: -95px;
right: -91px;
}
.ajax_searchload {
    position: absolute;
    top: 21px;
    right: 95px;
    
}
.ajax_loader{
    margin-top:20px;
}
a.load_more.button {
    background-color: #e8304d;
    border: none;
    border-radius: 50px;
    color: white!important;
    padding: 8px 45px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 17px;
    margin: -5px 0;
    cursor: pointer;
    text-decoration: none !important;
    margin-top: 20px;
    font-family: 'Montserrat', sans-serif;
}
</style> -->
<section class="con-b">
    <div class="container-fluid">
        <div class="header-blog">
            <div class="row flex-show">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="client-leftshows">
                                <input type="hidden" id="userid" value="<?php echo $custid; ?>">
                                <?php if ($userdata[0]['profile_picture']){ ?>
                                    <figure class="cli-ent-img circle one">
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$userdata[0]['profile_picture'];?>" class="img-responsive one">
                                    </figure>
                                <?php }else{ ?>
                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 20px; font-family: GothamPro-Medium; color: #fff;padding-top: 20px;">
                                <?php echo ucwords(substr($userdata[0]['first_name'],0,1)) .  ucwords(substr($userdata[0]['last_name'],0,1)); ?>
                                    </figure>
                                <?php } ?>
                                <h3 class="pro-head-q space-b"><a href="javascript:void(0)"><?php echo ucwords($userdata[0]['first_name']) . " " . ucwords($userdata[0]['last_name']); ?></a></h3>
                                <p class="neft text-left">
                                    <span class="blue text-uppercase">
                                        <?php if ($userdata[0]['plan_turn_around_days'] == "1") { 
                                                echo "Premium Member"; 
                                            }elseif ($userdata[0]['plan_turn_around_days'] == "3") {
                                                echo "Standard Member";
                                            }else{
                                                echo "No Member";
                                            } ?>
                                    </span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <ul id="status_check" class="list-unstyled list-header-blog small" role="tablist" style="border:none;">
                                <li class="active" id="1"><a data-status="active,disapprove,assign" data-toggle="tab" href="#Qa_client_active" role="tab">Active (<?php echo isset($count_active_project)?$count_active_project:'0';  ?>)</a></li>          
                                <li id="2"><a data-status="checkforapprove" data-toggle="tab" href="#Qa_client_pending" role="tab">Pending Approval (<?php echo isset($count_check_approve_project)?$count_check_approve_project:'0';?>)</a></li>
                                <li id="3"><a data-status="approved" data-toggle="tab" href="#Qa_client_completed" role="tab">Completed (<?php echo isset($count_complete_project)?$count_complete_project:'0'; ?>)</a></li>
                                <li id="4"><a data-status="hold" data-toggle="tab" href="#Qa_client_hold" role="tab">Hold (<?php echo isset($count_hold_project)?$count_hold_project:'0'; ?>)</a></li>
                                <li id="5"><a data-status="cancel" data-toggle="tab" href="#Qa_client_cancel" role="tab">Cancelled (<?php echo isset($count_cancel_project)?$count_cancel_project:'0'; ?>)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
        <p class="space-e"></p>
        <div class="cli-ent table">
            <div class="tab-content">
            <!--QA Active Section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $count_active_project; ?>" class="tab-pane active content-datatable datatable-width" id="Qa_client_active" role="tabpanel">
<!--                <div class="tab-pane active content-datatable datatable-width" id="Qa_client_active" role="tabpanel">-->
                    <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                <div class="ajax_searchload" style="display:none;">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                </div>
                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="product-list-show">
                    <div class="row">    
                        <?php 
//echo "<pre/>";print_R($active_project);exit;
                        for ($i = 0; $i < sizeof($active_project); $i++) { 
                            $data['client_projects'] = $active_project[$i];
                            $this->load->view('admin/client_projects_template',$data);
                        }?>
                    
                    </div>
                    <?php if ($count_active_project > LIMIT_ADMIN_LIST_COUNT) { ?>
                    <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                    <div class="" style="text-align:center">
                        <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_active_project; ?>" class="load_more button">Load more</a>
                    </div>
                    <?php } ?>
                    </div>
                </div>
            <!--QA Active Section End Here -->
            
            <!--QA Pending Approval Section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $count_check_approve_project; ?>" class="tab-pane content-datatable datatable-width" id="Qa_client_pending" role="tabpanel">
<!--                <div class="tab-pane content-datatable datatable-width" id="Qa_client_pending" role="tabpanel">-->
                    <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                <div class="ajax_searchload" style="display:none;">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                </div>
                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="product-list-show">
                    <div class="row">
                        <?php for ($i = 0; $i < sizeof($check_approve_project); $i++) { 
                        ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $check_approve_project[$i]['id']; ?>/2'"  style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">
                                    Delivered on 
                                </p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($check_approve_project[$i]['modified']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 40%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    
                                    <div class="cell-col">
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url()."admin/dashboard/view_request/".$check_approve_project[$i]['id']; ?>/2"><?php echo $check_approve_project[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($check_approve_project[$i]['description'], 0, 50); ?></p>
                                    </div>
                                    
                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="red bluetext text-uppercase text-wrap">Pending-approval</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="<?php echo base_url()."admin/dashboard/view_request/".$check_approve_project[$i]['id']; ?>/2">
                                        <?php if($check_approve_project[$i]['customer_profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$check_approve_project[$i]['customer_profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($check_approve_project[$i]['customer_first_name'],0,1)) .  ucwords(substr($check_approve_project[$i]['customer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?>
                                    </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $check_approve_project[$i]['customer_first_name']." ".$check_approve_project[$i]['customer_last_name'];?>">
                                            <?php echo $check_approve_project[$i]['customer_first_name'];?>
                                            <?php 
                                                if(strlen($check_approve_project[$i]['customer_last_name']) > 5){ 
                                                    echo ucwords(substr($check_approve_project[$i]['customer_last_name'],0,1)); 
                                                }else{ 
                                                    echo $check_approve_project[$i]['customer_last_name']; 
                                                } ?>
                                            </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="<?php echo base_url()."admin/dashboard/view_request/".$check_approve_project[$i]['id']; ?>/2">
                                        <?php if($check_approve_project[$i]['profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$check_approve_project[$i]['profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($check_approve_project[$i]['designer_first_name'],0,1)) .  ucwords(substr($check_approve_project[$i]['designer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $check_approve_project[$i]['designer_first_name']." ".$check_approve_project[$i]['designer_last_name'];?>">
                                             <?php
                                                if ($check_approve_project[$i]['designer_first_name']) {
                                                     echo $check_approve_project[$i]['designer_first_name'];
                                                    if (strlen($check_approve_project[$i]['designer_last_name']) > 5) {
                                                        echo ucwords(substr($check_approve_project[$i]['designer_last_name'], 0, 1));
                                                    } else {
                                                        echo $check_approve_project[$i]['designer_last_name'];
                                                    }
                                                }
                                                else {
                                                    echo "No Designer";
                                                }
                                                ?> 
                                            </p>
                                        <p class="pro-b">Designer</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url()."admin/dashboard/view_request/".$check_approve_project[$i]['id']; ?>/2">
                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                    <?php if($check_approve_project[$i]['total_chat'] + $check_approve_project[$i]['comment_count'] != 0){ ?>
                                            <span class="numcircle-box">
                                                <?php echo $check_approve_project[$i]['total_chat'] + $check_approve_project[$i]['comment_count']; ?>
                                            </span>
                                        <?php } ?></span></span>
                                <?php //echo $check_approve_project[$i]['total_chat_all'];?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url()."admin/dashboard/view_request/".$check_approve_project[$i]['id']; ?>/2">
                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                    <?php if(count($check_approve_project[$i]['total_files']) != 0){ ?>
                                        <span class="numcircle-box"><?php echo count($check_approve_project[$i]['total_files']); ?></span></span>
                                    <?php } ?>
                                    <?php echo count($check_approve_project[$i]['total_files_count']);?>
                                </a></p>
                            </div>
                        </div>
                    </div> <!-- End Row -->
                    <?php } ?>
                    </div>
                    <?php if ($count_check_approve_project > LIMIT_ADMIN_LIST_COUNT) { ?>
                    <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                    <div class="" style="text-align:center">
                        <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_check_approve_project; ?>" class="load_more button">Load more</a>
                    </div>
                    <?php } ?>
                    </div>
                </div>
                        
            <!--QA Pending Approval Section End Here -->
            
            <!--QA Completed Section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $count_complete_project; ?>" class="tab-pane content-datatable datatable-width" id="Qa_client_completed" role="tabpanel">
<!--                <div class="tab-pane content-datatable datatable-width" id="Qa_client_completed" role="tabpanel">-->
                <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                <div class="ajax_searchload" style="display:none;">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                </div>
                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="product-list-show">
                    <div class="row">    
                         <?php for ($i = 0; $i < sizeof($complete_project); $i++) { 
                        // echo "<pre>";
                        // print_r($complete_project);
                        // echo "</pre>";
                        ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $complete_project[$i]['id']; ?>/3'" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 13%;">
                            <div class="cli-ent-xbox">
                                <h3 class="app-roved green">Approved on</h3>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($complete_project[$i]['approvddate']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 37%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    
                                    <div class="cell-col">
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $complete_project[$i]['id']; ?>/3"><?php echo $complete_project[$i]['title'];?></a></h3>
                                        <p class="pro-b"><?php echo substr($complete_project[$i]['description'], 0, 50); ?></p>
                                    </div>
                                    
                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $complete_project[$i]['id']; ?>/3">
                                        <?php if($complete_project[$i]['customer_profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$complete_project[$i]['customer_profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($complete_project[$i]['customer_first_name'],0,1)) .  ucwords(substr($complete_project[$i]['customer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title=" <?php echo $complete_project[$i]['customer_first_name']." ".$complete_project[$i]['customer_last_name'];?>">
                                            <?php echo $complete_project[$i]['customer_first_name'];?>
                                            <?php 
                                                if(strlen($complete_project[$i]['customer_last_name']) > 5){ 
                                                    echo ucwords(substr($complete_project[$i]['customer_last_name'],0,1)); 
                                                }else{ 
                                                    echo $complete_project[$i]['customer_last_name']; 
                                                } ?>
                                           </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $complete_project[$i]['id']; ?>/3">
                                        <?php if($complete_project[$i]['profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$complete_project[$i]['profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($complete_project[$i]['designer_first_name'],0,1)) .  ucwords(substr($complete_project[$i]['designer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?></a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $complete_project[$i]['designer_first_name']." ".$complete_project[$i]['designer_last_name'];?>">
                                                <?php
                                                if ($complete_project[$i]['designer_first_name']) {
                                                    echo $complete_project[$i]['designer_first_name'];

                                                    if (strlen($complete_project[$i]['designer_last_name']) > 5) {
                                                        echo ucwords(substr($complete_project[$i]['designer_last_name'], 0, 1));
                                                    } else {
                                                        echo $complete_project[$i]['designer_last_name'];
                                                    }
                                                } else {
                                                    echo "No Designer";
                                                }
                                                ?>
                                            </p>
                                        <p class="pro-b">Designer</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $complete_project[$i]['id']; ?>/3">
                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                    <?php if($complete_project[$i]['total_chat'] + $complete_project[$i]['comment_count'] != 0){ ?>
                                            <span class="numcircle-box">
                                                <?php echo $complete_project[$i]['total_chat'] + $complete_project[$i]['comment_count']; ?>
                                            </span>
                                        <?php } ?></span>
                                <?php //echo $complete_project[$i]['total_chat_all'];?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $complete_project[$i]['id']; ?>/3">
                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                    <?php if(count($complete_project[$i]['total_files']) != 0){ ?>
                                        <span class="numcircle-box"><?php echo count($complete_project[$i]['total_files']); ?></span></span>
                                    <?php } ?>
                                    <?php echo count($complete_project[$i]['total_files_count']);?></a></p>
                            </div>
                        </div>
                    </div><!-- End Row -->
                    <?php } ?>
                    </div>
                        <?php if ($count_complete_project > LIMIT_ADMIN_LIST_COUNT) { ?>
                    <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                    <div class="" style="text-align:center">
                        <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_complete_project; ?>" class="load_more button">Load more</a>
                    </div>
                    <?php } ?>
                    </div>
                </div>
            <!--QA Completed Section End Here -->
            
            <!--QA Hold Section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $count_hold_project; ?>" class="tab-pane content-datatable datatable-width" id="Qa_client_hold" role="tabpanel">
                <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                <div class="ajax_searchload" style="display:none;">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                </div>
                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="product-list-show">
                    <div class="row">    
                         <?php for ($i = 0; $i < sizeof($hold_project); $i++) { 
                        ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $hold_project[$i]['id']; ?>/3'" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 13%;">
                            <div class="cli-ent-xbox">
                                <h3 class="app-roved green">Hold on</h3>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($hold_project[$i]['modified']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 37%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    
                                    <div class="cell-col">
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $hold_project[$i]['id']; ?>/3"><?php echo $hold_project[$i]['title'];?></a></h3>
                                        <p class="pro-b"><?php echo substr($hold_project[$i]['description'], 0, 50); ?></p>
                                    </div>
                                    
                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="gray text-uppercase text-wrap">On Hold</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $hold_project[$i]['id']; ?>/3">
                                        <?php if($hold_project[$i]['customer_profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$hold_project[$i]['customer_profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($hold_project[$i]['customer_first_name'],0,1)) .  ucwords(substr($hold_project[$i]['customer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title=" <?php echo $hold_project[$i]['customer_first_name']." ".$hold_project[$i]['customer_last_name'];?>">
                                            <?php echo $hold_project[$i]['customer_first_name'];?>
                                            <?php 
                                                if(strlen($hold_project[$i]['customer_last_name']) > 5){ 
                                                    echo ucwords(substr($hold_project[$i]['customer_last_name'],0,1)); 
                                                }else{ 
                                                    echo $hold_project[$i]['customer_last_name']; 
                                                } ?>
                                           </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $hold_project[$i]['id']; ?>/3">
                                        <?php if($hold_project[$i]['profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$hold_project[$i]['profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($hold_project[$i]['designer_first_name'],0,1)) .  ucwords(substr($hold_project[$i]['designer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?></a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $hold_project[$i]['designer_first_name']." ".$hold_project[$i]['designer_last_name'];?>">
                                                    <?php
                                                    if ($hold_project[$i]['designer_first_name']) {
                                                        echo $hold_project[$i]['designer_first_name'];
                                                        if (strlen($hold_project[$i]['designer_last_name']) > 5) {
                                                            echo ucwords(substr($hold_project[$i]['designer_last_name'], 0, 1));
                                                        } else {
                                                            echo $hold_project[$i]['designer_last_name'];
                                                        }
                                                    } else {
                                                        echo "No Designer";
                                                    }
                                                    ?>
                                            </p>
                                        <p class="pro-b">Designer</p>
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $hold_project[$i]['id']; ?>/3">
                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                    <?php if($hold_project[$i]['total_chat'] + $hold_project[$i]['comment_count'] != 0){ ?>
                                            <span class="numcircle-box">
                                                <?php echo $hold_project[$i]['total_chat'] + $hold_project[$i]['comment_count']; ?>
                                            </span>
                                        <?php } ?></span>
                                <?php //echo $complete_project[$i]['total_chat_all'];?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $hold_project[$i]['id']; ?>/3">
                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                    <?php if(count($hold_project[$i]['total_files']) != 0){ ?>
                                        <span class="numcircle-box"><?php echo count($hold_project[$i]['total_files']); ?></span></span>
                                    <?php } ?>
                                    <?php echo count($hold_project[$i]['total_files_count']);?></a></p>
                            </div>
                        </div>
                    </div><!-- End Row -->
                    <?php } ?>
                    </div>
                        <?php if ($count_hold_project > LIMIT_ADMIN_LIST_COUNT) { ?>
                    <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                    <div class="" style="text-align:center">
                        <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_hold_project; ?>" class="load_more button">Load more</a>
                    </div>
                    <?php } ?>
                    </div>
                </div>
            <!--QA Hold Section End Here -->
            
            <!--QA Cancel Section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $count_cancel_project; ?>" class="tab-pane content-datatable datatable-width" id="Qa_client_cancel" role="tabpanel">
                <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                <div class="ajax_searchload" style="display:none;">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                </div>
                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="product-list-show">
                    <div class="row">    
                         <?php for ($i = 0; $i < sizeof($cancel_project); $i++) { 
                        // echo "<pre>";
                        // print_r($complete_project);
                        // echo "</pre>";
                        ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $cancel_project[$i]['id']; ?>/3'" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 13%;">
                            <div class="cli-ent-xbox">
                                <h3 class="app-roved green">Cancelled on</h3>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($cancel_project[$i]['modified']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 37%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    
                                    <div class="cell-col">
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $cancel_project[$i]['id']; ?>/3"><?php echo $cancel_project[$i]['title'];?></a></h3>
                                        <p class="pro-b"><?php echo substr($cancel_project[$i]['description'], 0, 50); ?></p>
                                    </div>
                                    
                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="gray text-uppercase text-wrap">Cancelled</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $cancel_project[$i]['id']; ?>/3">
                                        <?php if($cancel_project[$i]['customer_profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$cancel_project[$i]['customer_profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($cancel_project[$i]['customer_first_name'],0,1)) .  ucwords(substr($cancel_project[$i]['customer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title=" <?php echo $cancel_project[$i]['customer_first_name']." ".$cancel_project[$i]['customer_last_name'];?>">
                                            <?php echo $cancel_project[$i]['customer_first_name'];?>
                                            <?php 
                                                if(strlen($cancel_project[$i]['customer_last_name']) > 5){ 
                                                    echo ucwords(substr($cancel_project[$i]['customer_last_name'],0,1)); 
                                                }else{ 
                                                    echo $cancel_project[$i]['customer_last_name']; 
                                                } ?>
                                           </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $cancel_project[$i]['id']; ?>/3">
                                        <?php if($cancel_project[$i]['profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$cancel_project[$i]['profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($cancel_project[$i]['designer_first_name'],0,1)) .  ucwords(substr($cancel_project[$i]['designer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?></a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $cancel_project[$i]['designer_first_name']." ".$cancel_project[$i]['designer_last_name'];?>">
                                                    <?php
                                                    if ($cancel_project[$i]['designer_first_name']) {
                                                        echo $cancel_project[$i]['designer_first_name'];
                                                        if (strlen($cancel_project[$i]['designer_last_name']) > 5) {
                                                            echo ucwords(substr($hold_project[$i]['designer_last_name'], 0, 1));
                                                        } else {
                                                            echo $cancel_project[$i]['designer_last_name'];
                                                        }
                                                    } else {
                                                        echo "No Designer";
                                                    }
                                                    ?>
                                            </p>
                                        <p class="pro-b">Designer</p>
                                    </div>
                                    
                                </div>
                                
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $cancel_project[$i]['id']; ?>/3">
                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                    <?php if($cancel_project[$i]['total_chat'] + $cancel_project[$i]['comment_count'] != 0){ ?>
                                            <span class="numcircle-box">
                                                <?php echo $cancel_project[$i]['total_chat'] + $cancel_project[$i]['comment_count']; ?>
                                            </span>
                                        <?php } ?></span>
                                <?php //echo $complete_project[$i]['total_chat_all'];?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $cancel_project[$i]['id']; ?>/3">
                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                    <?php if(count($cancel_project[$i]['total_files']) != 0){ ?>
                                        <span class="numcircle-box"><?php echo count($cancel_project[$i]['total_files']); ?></span></span>
                                    <?php } ?>
                                    <?php echo count($cancel_project[$i]['total_files_count']);?></a></p>
                            </div>
                        </div>
                    </div><!-- End Row -->
                    <?php } ?>
                    </div>
                        <?php if ($count_cancel_project > LIMIT_ADMIN_LIST_COUNT) { ?>
                    <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                    <div class="" style="text-align:center">
                        <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_cancel_project; ?>" class="load_more button">Load more</a>
                    </div>
                    <?php } ?>
                    </div>
                </div>
            <!--QA Cancel Section End Here -->
            </div>
         </div>
       </div>
</section>

<!-- Modal -->
<div class="modal fade" id="AddClient" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c">Add Client</h3>
                    </header>
                    <div class="fo-rm-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">First Name</label>
                                    <p class="space-a"></p>
                                    <input type="text" class="form-control input-c">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">Last Name</label>
                                    <p class="space-a"></p>
                                    <input type="text" class="form-control input-c">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">Email</label>
                                    <p class="space-a"></p>
                                    <input type="email" class="form-control input-c">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">Phone</label>
                                    <p class="space-a"></p>
                                    <input type="tel" class="form-control input-c">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group goup-x1">
                            <label class="label-x3">Password</label>
                            <p class="space-a"></p>
                            <input type="text" class="form-control input-c">
                        </div>
                        
                        <div class="form-group goup-x1">
                            <label class="label-x3">Confirm Password</label>
                            <p class="space-a"></p>
                            <input type="text" class="form-control input-c">
                        </div>
                        
                        <hr class="hr-b">
                        <p class="space-b"></p>
                        
                        <div class="form-group goup-x1">
                            <label class="label-x3">Select payment Method</label>
                            <p class="space-a"></p>
                            <select class="form-control select-c">
                                <option>Credit/Debit Card.</option>
                                <option>Credit/Debit Card.</option>
                                <option>Credit/Debit Card.</option>
                            </select>
                        </div>
                        
                        <div class="form-group goup-x1">
                            <label class="label-x3">Card Number</label>
                            <p class="space-a"></p>
                            <input type="text" class="form-control input-c" placeholder="xxxx-xxxx-xxxx">
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">Expiry Date</label>
                                    <p class="space-a"></p>
                                    <input type="text" class="form-control input-c" placeholder="22.05.2022">
                                </div>
                            </div>
                            
                            <div class="col-sm-3 col-md-offset-4">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">CVC</label>
                                    <p class="space-a"></p>
                                    <input type="text" class="form-control input-c" placeholder="xxx" maxlength="3">
                                </div>
                            </div>
                        </div>
                        
                        <p class="btn-x"><button class="btn-g">Add Customer</button></p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/jquery/dist/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/qa/bootstrap.min.js"></script>
<script type="text/javascript">     
// For Ajax Search
$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return decodeURI(results[1]) || 0;
    }
}
var status = $.urlParam('status');
$('#status_check #'+status+" a").click();
   

//     $('#status_check li a').click(function(){
//            var status = $(this).data('status');
//            $('#status').val(status);
//            $('#search_text').val("");
//            ajax_call();
//        });
//
//        $('.searchdata').keyup(function(e){
//            ajax_call();  
//        });
//
//        $('.search_data_ajax').click(function(e){
//            e.preventDefault();
//             ajax_call();
//        });
//        function ajax_call(){
//            var search = $('#search_text').val();
//            var status = $('#status').val();
//            var userid = $('#userid').val();
//            $.get( "<?php echo base_url();?>admin/dashboard/search_ajax_customer_project?title="+search+"&status="+status+"&id="+userid, function( data ){
//                var content_id = $('a[data-status="'+status+'"]').attr('href');
//               $( content_id ).html( data );  
//            });
//        }


function load_more_clientproject(){
    var tabid = $('#status_check li.active a').attr('href');
    var activeTabPane = $('.tab-pane.active');
    var status_scroll = $('#status_check li.active a').attr('data-status');
    var searchval = activeTabPane.find('.search_text').val();
    var userid = $('#userid').val();
    //activeTabPane.attr('data-search-text', searchval);
    $.post('<?php echo site_url() ?>admin/dashboard/load_more_clientprojects', {'group_no': 0, 'status': status_scroll,'search':searchval,'id':userid},
        function (data){
            var newTotalCount = 0;
            if(data != ''){
                $(".ajax_searchload").fadeOut(500);
                $(tabid+ " .product-list-show .row").html(data);
                newTotalCount = $(tabid+" .product-list-show .row").find("#loadingAjaxCount").attr('data-value');
               // console.log(newTotalCount);
                $(tabid+" .product-list-show .row").find("#loadingAjaxCount").remove();
                activeTabPane.attr("data-total-count",newTotalCount);
                activeTabPane.attr("data-loaded",$rowperpage);
                activeTabPane.attr("data-group",1);
            } else {
                activeTabPane.attr("data-total-count",0);
                activeTabPane.attr("data-loaded",0);
                activeTabPane.attr("data-group",1);
                $(tabid+ ".product-list-show .row").html("");
            }
            if ($rowperpage >= newTotalCount) {
                activeTabPane.find('.load_more').css("display", "none");
            } else{
                activeTabPane.find('.load_more').css("display", "inline-block");
            }
        });
}


/*********Search *******************/
$('.search_data_ajax').click(function (e) {
    //console.log($rowperpage);
    e.preventDefault();
    load_more_clientproject();
});
$('.searchdata').keyup(function (e) {
    e.preventDefault();
    delay(function(){
         $(".ajax_searchload").fadeIn(500);
        load_more_clientproject(); 
    }, 1000 );
});
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();


    $(document).ready(function () {
        var status = $('#status_check li.active a').attr('data-status');
        var id = $('#status_check li.active a').attr('href');
        var userid = $('#userid').val();
        //console.log(userid);
        //var total_record = 0;
        $rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
        $('.tab-pane').attr("data-loaded", $rowperpage);
       // var total_groups = total_pagin;
        //total_record++;
        //$(window).scroll(function () {
        $(document).on("click", "#load_more", function () {
            $(".ajax_loader").css("display","block");
            $(".load_more").css("display","none");
            var row = parseInt($(this).attr('data-row'));
            ///var allcount = parseInt($(this).attr('data-count'));
            row = row + $rowperpage;
            var activeTabPane = $('.tab-pane.active');
            var searchval = activeTabPane.find('.search_text').val();
            var allcount = parseInt(activeTabPane.attr("data-total-count"));
            var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
            var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
            var status_scroll = $('#status_check li.active a').attr('data-status');
            var tabid = $('#status_check li.active a').attr('href');
            if (allLoaded < allcount) {
                    $.post('<?php echo site_url() ?>admin/dashboard/load_more_clientprojects', {'group_no': activeTabPaneGroupNumber, 'status': status_scroll,'search':searchval,'id':userid},
                    function (data) {
                        if (data != "") {
                            $(tabid+" .product-list-show .row").append(data);
                            row = row + $rowperpage;
                            $(".ajax_loader").css("display","none");
                            $(".load_more").css("display","inline-block");
                            activeTabPane.find('.load_more').attr('data-row', row);
                            activeTabPaneGroupNumber++;
                            activeTabPane.attr('data-group', activeTabPaneGroupNumber);
                            allLoaded = allLoaded + $rowperpage;
                            activeTabPane.attr('data-loaded', allLoaded);
                            if (allLoaded >= allcount) {
                                activeTabPane.find('.load_more').css("display", "none");
                                $(".ajax_loader").css("display","none");
                            }else{
                                activeTabPane.find('.load_more').css("display", "inline-block");
                                $(".ajax_loader").css("display","none");
                            }
                        }
                    });
                }
            //} else {
            //}
            // }
        });
    });
  
//    $(document).on("click", "#right_nav > li", function (){
//          $(this).toggleClass('open');
//    });
</script>