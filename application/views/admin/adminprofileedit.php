<section class="con-b">
		<div class="container">
		
			<div class="row">
				<div class="col-md-4">
					<div class="project-row-qq1 settingedit-box">
						<div class="settrow">
							
							<div class="settcol right">
								<p class="btn-x"><a href="<?php echo base_url(); ?>admin/dashboard/adminprofile" class="btn-e">Save</a></p> 
							</div>
						</div>
						
						<div class="setimg-row33">
							<figure class="setimg-box33">
								<img src="<?php echo base_url();?>public/admin/images/xb3.jpg" class="img-responsive telset33">
								<a class="setimg-blog33" href="">
									<span class="setimgblogcaps">
										<img src="<?php echo base_url();?>public/admin/images/icon-camera.png" class="img-responsive"><br>
										<span class="setavatar33">Change <br>Avatar</span>
									</span>
								</a>
							</figure>
						</div>
						<p class="space-a"></p>
						<div class="form-group setinput">
							<input type="text" class="form-control input-b" placeholder="Shawn Alley">
						</div>
						
						<div class="form-group setinput">
							<input type="text" class="form-control input-b" placeholder="zishan@companyname.com">
							<p class="seter"><span>Administrative Email</span></p>
						</div>
						<p class="space-b"></p>
						<p class="chngpasstext"><a href=""><img src="<?php echo base_url();?>public/admin/images/change-password.png" class="img-responsive"> <span>Change Password</span></a></p>
					</div>	
				</div>
				
				<div class="col-md-4">
					<div class="project-row-qq1 settingedit-box">
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-b">About Info</h3>
							</div>
							<div class="settcol right">
								<p class="btn-x"><a href="<?php echo base_url(); ?>admin/dashboard/adminprofile" class="btn-e">Save</a></p> 
							</div>
						</div>
						<p class="space-d"></p>
						
							
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Company Name">
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Title">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Phone">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<textarea class="form-control textarea-b" placeholder="Address"></textarea>
								</div>
							</div>
							
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="City">
								</div>
							</div>
							
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="State">
								</div>
							</div>
							
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Zip">
								</div>
							</div>
							
						</div>
						
					</div>	
				</div>
				
				<div class="col-md-4">
					<div class="project-row-qq1 settingedit-box">
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-b">Billing and Subscription</h3>
							</div>
						</div>
						<p class="space-b"></p>
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-g">Current Plan</h3>
							</div>
							<div class="settcol right">
								<p class="setrightstst">Cycle: May 01,2018-June 01, 2018</p> 
							</div>
						</div>
						<p class="space-a"></p>
						
						<div class="cyclebox25">
							<div class="settrow">
								<div class="settcol left">
									<h3 class="sub-head-g">Monthly Golden Plan</h3>
									<p class="setpricext25">$259</p>
								</div>
								<div class="settcol right">
									<p class="btn-x"><button class="btn-f chossbpanss">Change Plan</button></p>
								</div>
							</div>
						</div>
						<p class="space-d"></p>
						
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-b">Paypal Id</h3>
							</div>
							<div class="settcol right">
								<p class="btn-x"><a href="<?php echo base_url(); ?>admin/dashboard/adminprofile" class="btn-e">Save</a></p>
							</div>
						</div>
						
						<p class="space-c"></p>
						
						<div class="form-group">
							<label class="label-x2">Card Number</label>
							<p class="space-a"></p>
							<div class="row">
								<div class="col-md-10">
									<input type="text" class="form-control input-c" placeholder="xxxx-xxxx-xxxx">
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Expiry Date">
								</div>
							</div>
							
							<div class="col-sm-4 col-sm-offset-1">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="CVC">
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Name">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Zip Code">
								</div>
							</div>
						</div>
						
					</div>	
				</div>
				
			</div>
			
			
			
		</div>
	</section>

    <!-- jQuery (necessary for JavaScript plugins) -->
    <script src="<?php echo base_url();?>public/admin/js/jquery.min.js"></script>
	<script>
		var $fileInput = $('.file-input');
		var $droparea = $('.file-drop-area');

		// highlight drag area
		$fileInput.on('dragenter focus click', function() {
		  $droparea.addClass('is-active');
		});

		// back to normal state
		$fileInput.on('dragleave blur drop', function() {
		  $droparea.removeClass('is-active');
		});

		// change inner text
		$fileInput.on('change', function() {
		  var filesCount = $(this)[0].files.length;
		  var $textContainer = $(this).prev();

		  if (filesCount === 1) {
			// if single file is selected, show file name
			var fileName = $(this).val().split('\\').pop();
			$textContainer.text(fileName);
		  } else {
			// otherwise show number of files
			$textContainer.text(filesCount + ' files selected');
		  }
		});
	</script>