<style>
h2.brnd-info-section {
    font: normal 21px/25px 'GothamPro-Medium', sans-serif;
    font-weight: 600;
    margin-bottom: 10px;
}
a.view-materials:hover{
	    color: #e73250;
}
.product-list-show h3 {
    color: #e52344;
    text-transform: uppercase;
}
.brand_profile_row .cell-row {
    font: normal 15px/25px 'GothamPro', sans-serif;
}
.brand_profile_row .cell-row a.view-materials {
    background: #e52344;
    color: #fff;
    width: 100px;
    display: block;
    text-align: center;
    line-height: 29px;
    border-radius: 30px;font-size: 13px;
    text-decoration: none;
}
.brand_info_not_found {
    background: #f5f5f5;
    padding: 80px;
    text-align: center;
    border: 1px solid #ccc;
    border-radius: 12px;
    font-family: GothamPro, sans-serif;
    color: #e73250;
    margin-top: 30px;
}
p.btn-x.add_subuser_admin .Change_Plan {
    display: inline-block;
    width: 170px;
    border-radius: 8px;
    text-transform: uppercase;
    color: #ffffff;
    outline: none;
    border: 0;
    transition: 0.26s;    background-color: #e73250;
    padding: 16px 0;
    margin-right: 10px;
    height: auto;
    font: normal 13px/20px 'GothamPro-Medium', sans-serif;
}
p.btn-x.add_subuser_admin .Change_Plan:hover{    box-shadow: 1px 7px 10px rgba(236, 42, 67, 0.40);
    color: #fff;text-decoration: none;}
p.btn-x.add_subuser_admin {
    float: right;
    transform: translateY(-50px);
}
</style>
<section class="con-b">
<div class="container">
    <div class="row">
    <?php if ($this->session->flashdata('message_error') != '') { ?>
        <div class="alert alert-danger alert-dismissable">
            <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
            <p class="head-c"><?php echo $this->session->flashdata('message_error'); ?></p>
        </div>
    <?php } ?>

    <?php if ($this->session->flashdata('message_success') != '') { ?>
        <div class="alert alert-success alert-dismissable">
            <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
            <p class="head-c"><?php echo $this->session->flashdata('message_success'); ?></ps>
        </div>
    <?php } ?>
 <h2 class="brnd-info-section"> Sub User List</h2>	
 <p class="fill-sub">Please find below Sub User information.</p>
 <?php if(($edit_profile[0]['full_admin_access'] != 0 || $edit_profile[0]['full_admin_access'] == NULL) && $edit_profile[0]['role'] == 'admin'){ ?>
 <p class="btn-x add_subuser_admin">
     <a href="<?php echo base_url(); ?>admin/dashboard/add_subuser/<?php echo $cust_id; ?>" class="btn-f Change_Plan chossbpanss">+ Add New User</a>
 </p>
 <?php } ?>
 </div>
<div class="product-list-show">
	<div class="row">  
   <?php if(sizeof($subuserlist) > 0){ ?>
		<!-- Start Row -->
		<div class="cli-ent-row tr brdr">
			<div class="cli-ent-col td" style="width: 20%;">
				<div class="cli-ent-xbox text-left">
				<div class="cell-row">

				<div class="cell-col">
				<h3 class="pro-head space-b">First Name</h3>
				
				</div>
				</div>
				</div>
			</div>
		<div class="cli-ent-col td" style="width: 20%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			<div class="cell-col">
			<h3 class="pro-head space-b">Last Name</h3>
			
			</div>
			</div>
			</div>
		</div>
               <?php if(($edit_profile[0]['full_admin_access'] != 0 || $edit_profile[0]['full_admin_access'] == NULL) && $edit_profile[0]['role'] == 'admin'){ ?>
		<div class="cli-ent-col td" style="width: 20%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			<div class="cell-col">
			<h3 class="pro-head space-b">Email</h3>
			
			</div>
			</div>
			</div>
		</div>
		<div class="cli-ent-col td" style="width: 20%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			<div class="cell-col">
			<h3 class="pro-head space-b">Phone</h3>
			
			</div>
			</div>
			</div>
		</div>
               
                 <div class="cli-ent-col td" style="width: 20%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			<div class="cell-col">
			<h3 class="pro-head space-b">Other Info</h3>
			
			</div>
			</div>
			</div>
		</div>   
                <?php } ?>
		</div> <!-- End Row -->
		<div class="brand_profile_row">
		<?php for($i=0; $i<sizeof($subuserlist);$i++){ ?>
		<!-- Start Row -->
		<div class="cli-ent-row tr brdr">
			<div class="cli-ent-col td" style="width: 20%;">
                            <div class="cli-ent-xbox text-left">
                            <div class="cell-row">

                            <?php echo $subuserlist[$i]['first_name']; ?>

                            </div>
                            </div>
			</div>
		<div class="cli-ent-col td" style="width: 20%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			<?php echo $subuserlist[$i]['last_name']; ?>
			
			</div>
			</div>
		</div>
                <?php if(($edit_profile[0]['full_admin_access'] != 0 || $edit_profile[0]['full_admin_access'] == NULL) && $edit_profile[0]['role'] == 'admin'){ ?>
		<div class="cli-ent-col td" style="width: 20%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			 
			<?php echo $subuserlist[$i]['email']; ?>
			
			</div>
			</div>
		</div>
                    
                <div class="cli-ent-col td" style="width: 20%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			 
			<?php echo $subuserlist[$i]['phone']; ?>
			
			</div>
			</div>
		</div>    
		<div class="cli-ent-col td" style="width: 20%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">
			<div class="cell-col">
                            <h3 class="pro-head space-b" style="cursor: pointer;"><a href="<?php echo base_url(); ?>admin/dashboard/editsub_user/<?php echo $subuserlist[$i]['id']; ?>" class="edit_subuser"><i class="fa fa-pencil"></i> </a><a onClick='javascript:return confirm("Are you sure to Delete?")' href="<?php echo base_url(); ?>admin/dashboard/delete_sub_user/<?php echo $subuserlist[$i]['id']; ?>" class="delete_sub_user"><i class="fa fa-trash-o"></i></a></h3>
			
			</div>
			</div>
			</div>
		</div>
                    <?php } ?>
		</div> <!-- End Row -->
		<?php } ?>

	</div>
        <?php } else { ?>
        <div class="brand_info_not_found">  
	<h3>Sub User Not Found</h3>
        </div>
	<?php } ?>
	</div>
</div>
</div>
</section>