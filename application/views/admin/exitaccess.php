<html>
    <head>
        <style>
            .error_wrapper {
                display: flex;
                align-items: center;
                justify-content: center;
                height: 96vh;
            }
            .error_message_dueto_prmsn {
                margin: auto;
                display: inline-block;
                font-family: 'Poppins', sans-serif;
                background: #fff;
                box-shadow: 0 0 60px 10px rgba(136, 136, 136, 0.25);
                border-radius: 42px;
                padding: 40px;
                text-align: center;
                max-width: 600px;
                width: 100%;
            }
            .error_message_dueto_prmsn h2 {
                margin: 0;
                font-size: 24px;
                color: #868686;
            }
            .error_message_dueto_prmsn h2 span {
                color: #000;
                text-transform: capitalize;
            }
            .error_message_dueto_prmsn a {
                display: block;
                border-radius: 4px;
                text-transform: uppercase;
                color: #ffffff;
                outline: none;
                border: 0;
                transition: 0.26s;
                padding: 0 13px;
                line-height: 50px;
                font-weight: 600;
                background: #ec2a43;
                max-width: 100px;
                text-decoration: none;
                margin: 25px auto 0;
                font-size: 13px;
            }
        </style>
        <title>Exit Access | Graphics Zoo</title>
    </head>
    <body>
        <div class="row">
            <div class="error_wrapper">
                <div class="error_message_dueto_prmsn">
                    <h2>You are logged in as : <span><?php echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name']; ?></span></h2>

                    <div class="logout-account">
                        <a href="<?php echo base_url(); ?>admin/accounts/backtoownaccount/<?php echo $_SESSION['switchfrom_id']; ?>/admin" class="backtoadmin">Exit Access</a>
                    </div>
                </div>
            </div>

        </div>
    </body>
</html>
