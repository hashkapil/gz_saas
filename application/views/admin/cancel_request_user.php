<section class="con-b">
    <div class="header-blog">
        <input type="hidden" value="" class="bucket_type">
        <div class="row flex-show">
            <div class="col-md-12">
                <div class="flex-this">
                    <h2 class="main_page_heading">Cancel Request Users</h2>
                </div>
            </div>
        </div>
    </div>


    <div class="product-list-show" id="Refund_taxes">
        <div class="cli-ent-row tr tableHead">
            <div class="cli-ent-col td" style="width: 20%">Customer Name</div>
            <div class="cli-ent-col td" style="width: 20%">Reason</div>
            <div class="cli-ent-col td" style="width: 10%">Switch designer</div>
            <div class="cli-ent-col td" style="width: 10%">Avail offer</div>
            <div class="cli-ent-col td" style="width: 10%">Feedback</div>
            <div class="cli-ent-col td" style="width: 20%">Request Date</div>
            <div class="cli-ent-col td" style="width: 20%">Action</div>
        </div>
        <?php
//                echo "<pre/>";print_R($cancl_req_user);
        if (count($cancl_req_user) > 0) {
            foreach ($cancl_req_user as $kk => $vv) {
                /*                 * ***request satatus**** */
                if ($vv['cancel_status'] == 'open') {
                    $cancel_status = 'Open';
                } else {
                    $cancel_status = 'Closed';
                }
                /*                 * ***designer switch or not** */
                if ($vv['is_designer_switch'] == 1) {
                    $switch_designer = 'Yes';
                } else {
                    $switch_designer = 'No';
                }
                /*                 * ****avail offer********* */
                if ($vv['coupon_code'] != '') {
                    $offeravail = 'Yes';
                } else {
                    $offeravail = 'No';
                }
                ?>
                <div class="row two-can">
                    <div class="cli-ent-row tr brdr">
                        <div class="cli-ent-col td cust-name-req" style="width: 20%">
                            <span class="cust_name_cancel"><?php echo $vv['first_name'] . ' ' . $vv['last_name']; ?></span>
                            <span class="email_cancel" data-toggle="tooltip" title="<?php echo $vv['email']; ?>"><?php echo (strlen($vv['email']) > 20 ) ? substr($vv['email'], 0, 20) . '..' : $vv['email']; ?></span>
                        </div>
                        <div class="cli-ent-col td" style="width: 20%"><p class="cancel_rsnn"><?php echo substr($vv['reason'], 0, 70);?></p><?php 
        echo ($vv['reason'] == 'We are going with another service provider or in-house designer') ? '<i class="far fa-comment-alt" aria-hidden="true" data-toggle="tooltip" title="' . $vv['why_reason'] . '"></i>' : ''; ?></div>
                        <div class="cli-ent-col td" style="width: 10%"><?php echo $switch_designer; ?></div>
                        <div class="cli-ent-col td" style="width: 10%"><?php echo $offeravail; ?></div>
                        <div class="cli-ent-col td" style="width: 10%"><p class="cancel_feedB"><?php echo substr($vv['cancel_feedback'], 0, 40);?></p><?php
        echo ($vv['cancel_feedback'] != '') ? '<i class="far fa-comment-alt" aria-hidden="true" data-toggle="tooltip" title="' . $vv['cancel_feedback'] . '"></i>' : ''; ?></div>
                        <div class="cli-ent-col td" style="width: 20%"><?php echo $vv['created']; ?></div>
                        <div class="cli-ent-col td" style="width: 20%">
                            <div class="notify-lines">
                                <div class="switch-custom switch-custom-usersetting-check for_disply">
                                    <span class="checkstatus checkstatus_<?php echo $vv['cid']; ?>"></span>
                                    <span class="action_lab_<?php echo $vv['cid']; ?>"><?php echo $cancel_status; ?></span>    
                                    <input type="checkbox" class="closecancel" data-cid="<?php echo $vv['cid']; ?>" id="switch_<?php echo $vv['cid']; ?>" <?php echo ($vv['cancel_status'] == 'open') ? 'checked' : ''; ?>>
                                    <label for="switch_<?php echo $vv['cid']; ?>"></label> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php
        } else {
            echo "<p style='text-align:center;padding:20px;'>Data Not Found</p>";
        }
        ?>
    </div>
</section>