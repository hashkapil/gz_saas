<?php //echo "<pre/>";print_R($clients); 
if ($clients['isfullaccess']) {
    $clientname = '<a href="'. base_url() .'admin/accounts/view_client_profile/'. $clients['id'] .'" title="View Client Details">
                    '. ucwords($clients['first_name'] . " " . $clients['last_name']) .'</a>';
}else{
    $clientname = ucwords($clients['first_name'] . " " . $clients['last_name']);
}
?>
<div class="cli-ent-row tr brdr ">  
    <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
    <div class="cli-ent-col td">
        <div class="cli-ent-xbox text-left">
            <div class="cell-row">
                <div class="cell-col">
                    <div class="sound-signal">
                        <div class="form-radion2">
                            <label class="containerr">
                                <input type="checkbox" name="customer" class="selected_custmr <?php echo $clients['addClass'];?>"  value="<?php echo $clients['id']; ?>"/> 
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="client-hd-listing" style="width:22%;">
        <div class="cli-ent-xbox text-left">
            <div class="cli-ent-col td flex-uses" style="">
                <figure class="cli-ent-img circle one">
                    <img src="<?php echo $clients['profile_picture']; ?>" class="img-responsive one">
                </figure>
                <h3 class="pro-head-q">
                    <?php echo $clientname; ?>
                    <?php if ($clients['isfullaccess']) { ?>
                        <p class="pro-a"> <?php echo (strlen($clients['email']) > 25) ? substr($clients['email'],0,20).'..' :$clients['email']; ?></p>
                    <?php } ?>
                    <p class="neft">
                        <span class="blue <?php echo ($clients['client_plan']!= '' || $clients['client_plan']!= NULL) ? 'set_plancolor' : '';?>">
                            <?php echo $clients['client_plan'];?>
                        </span>
                    </p>
                </h3>
            </div>
        </div>
    </div>
    <div class="cli-ent-col td text-center" style="width: 12%;">
        <div class="cli-ent-xbox">
            <p class="pro-b"><?php echo $clients['created']; ?></p>
        </div>
    </div>
    <div class="cli-ent-col td text-center" style="width: 12%;">
        <div class="cli-ent-xbox">
            <p class="pro-b"><?php echo $clients['billing_end_date']; ?></p>
        </div>
    </div>
    <div class="cli-ent-col td text-center toggle_status" style="width: 11%;">
        <?php echo $clients['total_request']; ?>&nbsp;
        <div class="on-dot" data-clientID="<?php echo $clients['id']; ?>"><i class="fas fa-ellipsis-h"></i></div>
        <div class="client_totaltab client_projects_counting_<?php echo $clients['id']; ?>" style="display: none;">
            <p>Active :<span> <?php echo $clients['active'];?></span></p>
        <p>In Queue :<span> <?php echo $clients['inque_request'];?></span></p>
        <p>Pending Review : <span><?php echo $clients['revision_request'];?></span></p>
        <p>Pending Approval : <span><?php echo $clients['review_request'];?></span></p>
        <p>Completed : <span><?php echo $clients['complete_request'];?></span></p>
        <p>Hold : <span><?php echo $clients['hold'];?></span></p>
        <p>Cancelled : <span><?php echo $clients['cancelled'];?></span></p>
    </div>
    </div>
    
    <div class="cli-ent-col td" style="width: 6%;">
        <div class="cli-ent-xbox text-center">
            <div class="notify-lines">
                <div class="switch-custom switch-custom-usersetting-check">
                    <span class="checkstatus checkstatus_<?php echo $clients['id']; ?>"></span>
                    <input type="checkbox" class="actClient" data-cid="<?php echo $clients['id']; ?>" id="switch_<?php echo $clients['id']; ?>" 
                    <?php
                    if ($clients['is_active'] == 1) {
                        echo 'checked';
                    } else {}
                    ?>/>
                    <label for="switch_<?php echo $clients['id']; ?>"></label> 
                </div>
            </div>
        </div>
    </div>
    <div class="cli-ent-col td" style="width: 15%">
        <?php if (!empty($clients['designer'])) { ?>
            <div class="cli-ent-xbox">
                <div class="cell-row">
                    <div class="cell-col">
                        <p class="text-h " title="<?php echo $clients['designer'][0]['first_name'] . " " . $clients['designer'][0]['last_name']; ?>
                           ">
                            <a href="#" class="addde-signersk1 permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $clients['id']; ?>">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/assign_designer_edit.svg"> 
                            </a>
                            <?php echo $clients['designer'][0]['first_name']; ?>
                            <?php
                            if (strlen($clients['designer'][0]['last_name']) > 5) {
                                echo ucwords(substr($clients['designer'][0]['last_name'], 0, 1));
                            } else {
                                echo $clients['designer'][0]['last_name'];
                            }
                            ?>

                        </p>

                    </div>
                </div> 
            </div>
        <?php } else { ?>
            <div class="cli-ent-xbox">
                <a href="#" class="upl-oadfi-le noborder permanent_assign_design assign_de" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $clients['id']; ?>">
                    <p class="attachfile"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/assign_designer_red.svg"> 
                        <span>Assign designer</span></p>
                </a>
            </div>
        <?php } ?>
    </div>
    <div class="cli-ent-col td assign_va text-center" style="width: 8%">
        <?php if (!empty($clients['va'])) { ?>
            <div class="cli-ent-xbox">
                <div class="cell-row">
                    <div class="cell-col <?php echo $clients['id']; ?>">
                        <p class="text-h" title="<?php echo $clients['va'][0]['first_name'] . " " . $clients['va'][0]['last_name']; ?>">
                            <?php echo $clients['va'][0]['first_name']; ?>
                            <?php
                            if (strlen($clients['va'][0]['last_name']) > 5) {
                                echo ucwords(substr($clients['va'][0]['last_name'], 0, 1));
                            } else {
                                echo $clients['va'][0]['last_name'];
                            }
                            ?>

                        </p>

                    </div>
                </div> 
            </div>
        <?php } else { ?>
            <div class="cli-ent-xbox">
                <p><span>N/A</span></p>
            </div>
        <?php } ?>
    </div>
    <div class="cli-ent-col td action-per text-center" style="width: 14%">
        <?php if ($clients['isfullaccess']) { ?>
            <a href="<?php echo base_url(); ?>admin/accounts/view_client_profile/<?php echo $clients['id']; ?>" title="View Client Details">
                <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/gz_icons/icon-user.svg">
            </a>
        <?php } ?>
        <a href="<?php echo base_url(); ?>admin/dashboard?client_id=<?php echo $clients['id']; ?>" title="View Projects"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/list_icon.svg" /></a>
        <a data-toggle="modal" data-target="#show_notepad_area" class="add_notes" title="Add Note" data-id="<?php echo $clients['id']; ?>" data-name="<?php echo ucwords($clients['first_name'] . " " . $clients['last_name']); ?>">
           <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/add-note-icon.svg" />
        </a>
        <?php if ($clients['isfullaccess']) { ?>
            <a href="javascript:void(0)" data-customerid="<?php echo $clients['id']; ?>" class="delete_customer">
                <i class="icon-gz_delete_icon"></i>
            </a>
        <?php } ?>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="viewinfo<?php echo $clients['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="cli-ent-model-box">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="cli-ent-model">
                        <header class="fo-rm-header">
                            <h3 class="head-c">View Information</h3>
                        </header>
                        <div class="fo-rm-body">
                            <div class="projects">
                                <ul class="projects_info_list">
                                    <li>
                                        <div class="starus"><b>Active Projects</b></div>
                                        <div class="value"><?php echo $clients['active']; ?></div>
                                    </li>
                                    <li>
                                        <div class="starus"><b>In -Queue Projects</b></div>
                                        <div class="value"><?php echo $clients['inque_request']; ?></div>
                                    </li>
                                    <li>
                                        <div class="starus"><b>Revision Projects</b></div>
                                        <div class="value"><?php echo $clients['revision_request']; ?></div>
                                    </li>
                                    <li>
                                        <div class="starus"><b>Review Projects</b></div>
                                        <div class="value"><?php echo $clients['review_request']; ?></div>
                                    </li>
                                    <li>
                                        <div class="starus"><b>Complete Projects</b></div>
                                        <div class="value"><?php echo $clients['complete_request']; ?></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ShowSubusers" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">

    </div>
    <!-- Modal -->
    <div class="modal fade" id="<?php echo $clients['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="cli-ent-model-box">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="cli-ent-model">
                        <header class="fo-rm-header">
                            <h3 class="head-c">Add Client</h3>
                        </header>
                        <div class="fo-rm-body">
                            <form onSubmit="return EditFormSQa(<?php echo $clients['id']; ?>)" method="post" action="<?php echo base_url(); ?>admin/dashboard/edit_client/<?php echo $clients['id']; ?>">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group goup-x1">
                                            <label class="label-x3">First Name</label>
                                            <p class="space-a"></p>
                                            <input type="text" name="first_name" required class="efirstname form-control input-c" value="<?php echo ucwords($clients['first_name']); ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group goup-x1">
                                            <label class="label-x3">Last Name</label>
                                            <p class="space-a"></p>
                                            <input type="text" name="last_name" required class="elastname form-control input-c" value="<?php echo ucwords($clients['last_name']); ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group goup-x1">
                                            <label class="label-x3">Email</label>
                                            <p class="space-a"></p>
                                            <input type="email" name="email" required  class="eemailid form-control input-c" value="<?php echo $clients['email']; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group goup-x1">
                                            <label class="label-x3">Phone</label>
                                            <p class="space-a"></p>
                                            <input type="tel" name="phone" required class="ephone form-control input-c" value="<?php echo $clients['phone']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group goup-x1">
                                    <label class="label-x3">State</label>
                                    <p class="space-a"></p>
                                    <input type="text" name="state"  required class="form-control input-c epassword" value="<?php echo ucwords($clients['state']); ?>">
                                </div>

                                <div class="form-group goup-x1">
                                    <label class="label-x3">Password</label>
                                    <p class="space-a"></p>
                                    <input type="password" name="password"   class="form-control input-c epassword" >
                                </div>

                                <div class="form-group goup-x1">
                                    <label class="label-x3">Confirm Password</label>
                                    <p class="space-a"></p>
                                    <input type="password" name="confirm_password"  class="form-control input-c ecpassword" >
                                </div>
                                <div class="epassError" style="display: none;">
                                    <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                </div>
                                <hr class="hr-b">
                                <p class="space-b"></p>

                                <div class="form-group goup-x1">
                                    <label class="label-x3">Select payment Method</label>
                                    <p class="space-a"></p>
                                    <select class="form-control select-c">
                                        <option>Credit/Debit Card.</option>
                                        <option>Credit/Debit Card.</option>
                                        <option>Credit/Debit Card.</option>
                                    </select>
                                </div>

                                <div class="form-group goup-x1">
                                    <label class="label-x3">Card Number</label>
                                    <p class="space-a"></p>
                                    <input type="text" class="form-control input-c" placeholder="xxxx-xxxx-xxxx" name="Card Number" required>
                                </div>

                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="form-group goup-x1">
                                            <label class="label-x3">Expiry Date</label>
                                            <p class="space-a"></p>
                                            <input type="text" name="Expiration Date" required class="form-control input-c" placeholder="22.05.2022">
                                        </div>
                                    </div>

                                    <div class="col-sm-3 col-md-offset-4">
                                        <div class="form-group goup-x1">
                                            <label class="label-x3">CVC</label>
                                            <p class="space-a"></p>
                                            <input type="text" name="CVC" required class="form-control input-c" placeholder="xxx" maxlength="4">
                                        </div>
                                    </div>
                                </div> 

                                <p class="space-b"></p>

                                <p class="btn-x"><button type="submit" class="btn-g">Add Customer</button></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- End Row -->

