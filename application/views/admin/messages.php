<section class="con-b">
    <div class="container-fluid">
        <?php if ($this->session->flashdata('message_error') != '') { ?>				
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>				
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-md-12">
                <h2 class="main_page_heading">Messages</h2>
            </div>

            <?php //if (!empty($data)) { ?>
                <div class="col-md-5 col-lg-4">
                    <div class="msz-sidebar">
                        <ul class="nav read_unread_tabb">
                            <li class="change_tab1 <?php
                            if ($seen == "unread") {
                                echo "active";
                            }
                            ?>" ><a href="<?php echo base_url(); ?>admin/messages/message_list/unread"> Unread (<?php echo ($count_unreadchat_msg > 0) ? $count_unreadchat_msg : '0'; ?>)</a></li>
                            <li class="change_tab1 <?php
                            if ($seen == "read") {
                                echo "active";
                            }
                            ?> "><a href="<?php echo base_url(); ?>admin/messages/message_list/read">  Read  </a></li>
                            <li class="change_tab1 <?php
                            if ($seen == "is_star") {
                                echo "active";
                            }
                            ?> "><a href="<?php echo base_url(); ?>admin/messages/message_list/is_star">  Starred  (<?php echo ($count_star_msg > 0) ? $count_star_msg : '0'; ?>)</a></li>
                        </ul>
                        <div class="two-can msg-list">
                            <?php
                            // echo $firstrequest_id;
                            foreach ($project_list as $project_msg) {
                                if ($project_msg['request_id'] != '') {
                                    ?>

                                    <div id="list_<?php echo $project_msg['request_id']; ?>" class="project_sectionfr_msg  <?php
                                    if ($project_msg['rd_admin_seen'] != 1) {
                                        echo "unread_msg";
                                    } else {
                                        echo "read";
                                    }
                                    ?> <?php
                                    if ($firstrequest_id == $project_msg['request_id']) {
                                        echo "active";
                                    }
                                    ?>">

                                        <div class="clnt_prfl_wth_nam">
                                            <div class="prjct_clnt_prfl">
                                                <img src="<?php echo $project_msg['user_profile_picture']; ?>" class="img-responsive">
                                            </div>     
                                            <div class="pro_client_name"><?php echo $project_msg['user_fname']; ?></div>


                                        </div>

                                        <div class="admin_prjct_clint">  

                                            <div class="project_name_msg_list">
                                                <a class="project_id_get_msg" data-id="<?php echo $project_msg['request_id']; ?>">
                                                    <h4>
                                                        <p class="unread_circle"></p>
                                                        <?php echo $project_msg['request_title']; ?>
                                                    </h4> </a>
                                                <?php
                                                if ($project_msg['request_status'] == "checkforapprove") {
                                                    $deliverdate = "Delivered on " . $project_msg['req_modified'];
                                                    $status = "Pending Approval";
                                                    $color = "bluetext";
                                                } elseif ($project_msg['request_status'] == "active") {
                                                    $deliverdate = "Expected on " . date('M d, Y h:i A', strtotime($project_msg['req_last_update']));
                                                    $status = "In Progress";
                                                    $color = "green";
                                                } elseif ($project_msg['request_status'] == "disapprove" && $project_msg['request_who_reject'] == 1) {
                                                    $deliverdate = "Expected on " . date('M d, Y h:i A', strtotime($project_msg['req_last_update']));
                                                    $status = "Revision";
                                                    $color = "orangetext";
                                                } elseif ($project_msg['request_status'] == "disapprove" && $project_msg['request_who_reject'] == 0) {
                                                    $deliverdate = "Expected on " . date('M d, Y h:i A', strtotime($project_msg['req_last_update']));
                                                    $status = "Quality Revision";
                                                    $color = "red";
                                                } elseif ($project_msg['request_status'] == "pending" || $project_msg['request_status'] == "assign") {
                                                    $status = "In Queue";
                                                    $color = "gray";
                                                } elseif ($project_msg['request_status'] == "hold") {
                                                    $status = "On Hold";
                                                    $color = "gray";
                                                } elseif ($project_msg['request_status'] == "draft") {
                                                    $status = "Draft";
                                                    $color = "gray";
                                                } elseif ($project_msg['request_status'] == "approved") {
                                                    $deliverdate = "Approved on " . $project_msg['req_approvaldate'];
                                                    $status = "Completed";
                                                    $color = "green";
                                                } elseif ($project_msg['request_status'] == "pendingrevision") {
                                                    $deliverdate = "Expected on " . $project_msg['req_modified'];
                                                    $status = "Pending Review";
                                                    $color = "lightbluetext";
                                                } elseif ($project_msg['request_status'] == "cancel"){
                                                    $status = "Cancelled";
                                                    $color = "gray";
                                                }else {
                                                    $status = "";
                                                    $color = "greentext";
                                                }
                                                ?>
                                                <div class="prjct_dlvrd_dt"><?php echo $deliverdate; ?></div>
                                                <div class="comn-btn">
                                                    <p>  <span class="btn-d <?php echo $color; ?>"><?php echo $status; ?></span>
                                                    </p>
                                                    <div class="view_req_frm_prjctlst">
                                                        <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $project_msg['request_id']; ?>" target="_blank">
                                                            <i class="fas fa-angle-right"></i>
                                                        </a>
                                                    </div>
                                                    <div class="view_req_frm_prjctlst">
                                                        <a href="<?php echo base_url() . "admin/accounts/view_client_profile/" . $project_msg['userid']; ?>" target="_blank">
                                                            <i class="fas fa-user"></i>
                                                        </a>
                                                    </div>  

                                                    <?php
                                                    if ($project_msg['is_star'] == "1") {
                                                        $class = "star_project";
                                                        $status_s = "remove_star";
                                                    } else {
                                                        $class = "gray_star";
                                                        $status_s = "mark_star";
                                                    }
                                                    ?>
                                                    <div class="my_fvrt mark_messages <?php echo $class ?>" data-status="<?php echo $status_s; ?>"  data-id="<?php echo $project_msg['request_id']; ?>"> <p class="star_circle"><i class="fa fa-star"></i></p></div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>

                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>

                        <div class="" style="text-align:center;float: left;width: 100%;display:none;">
                            <a id="load_more_messages" href="javascript:void(0)" data-row="0" data-count="1" data-seen="<?php echo $seen; ?>" class="load_more button">Load more</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-lg-8">
                    <div class="chat_area_with_draft">
                        <div class="loading_msg"> </div>
                        <div class="chat-box-row" id="allChats">

                            <?php
                            if (!empty($data)) { 
                            $this->load->view('admin/message_chat', array("chat_request" => $chat_request, "profile" => $profile, "data" => $data, "request_files" => $request_files, "file_chat_array" => $file_chat_array));
                            } else { ?>
                                <div class='no_found_pro'> No project found</div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php// } else { ?>
<!--                <div class="col-md-12">
                    <div class='no_found_pro'> No project found</div>
                </div>-->
            <?php //} ?>
        </div>
    </div>
</section>

<script type="text/javascript">
//    $( document ).ready(function() {
//        $( ".change_tab" ).each(function( index ) {
//            if(($(this).hasClass('active')) == true){
//                var active_status = $(this).attr('data-status');
//                read_unread_message(active_status);
//            }
//        });
//    });
//$(document).click(function(e) {
//   // console.log(e.target.id);
//  if( e.target.id != 'show_trans') {
//    $("#transcopy").hide();
//  }
//});

//$(document).on('click','.change_tab1',function(){
//    console.log(this);
//    $(this).addClass('active');
//    $(this).siblings().removeClass('active');
//    
//});
//$(document).on('click','.mark_messages',function(){
//    var status = $(this).attr('data-status');
//    var project_id = $(this).attr('data-id');
//    var active_project = $('.open').attr('data-id');
//    var active_chat = $('.open').attr('data-chat');
//    $('.loading_msg').html('<div class="loading"><span class="loading_process"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" alt="loading"/></span></div>');
//    $.ajax({
//        type: 'POST',
//        url: "<?php echo base_url(); ?>admin/messages/mark_read_unread",
//            //dataType:'json',
//            data: {status: status,'project_id':project_id,active_project:active_project,active_chat:active_chat},
//            success: function (data) {
//                if(data == 1){
//                    if(status == 'mark_read'){
//                        $('#list_'+project_id).removeClass('unread_msg');
//                        $('#list_'+project_id).addClass('read');
//                        $('.chatbox-add .numcircle-box-01').css("display","none");
//                        $(".open").addClass('read');
//                        $(".open").removeClass('unread');
//                    }else if(status == 'mark_unread'){
//                        $('#list_'+project_id).removeClass('read');
//                        $('#list_'+project_id).addClass('unread_msg');
//                        $(".open").addClass('unread');
//                        $(".open.unread .chatbox-add").html("<p class='unread_circle'></p>");
//                        $(".open").removeClass('read');    
//                    }else if(status == 'mark_star'){
//                       // $('#list_'+project_id).addClass('star_project');
//                       //console.log('add',"star");
//                       $("#list_"+project_id+ " .my_fvrt").addClass('star_project');
//                       $("#list_"+project_id+ " .my_fvrt").attr('data-status', 'remove_star'); 
//                       $(".add_star").css('display','none');
//                       $(".remove_star").css('display','block');
//                        //$(".open").addClass('is_star');
//                       // $(".open.is_star .star_chat").html('<p class="star_circle"><i class="far fa-star"></i></p>');
//                   }else if(status == 'remove_star'){
//                      //  console.log('remove',"star");
//                      $("#list_"+project_id+ " .my_fvrt").removeClass('star_project'); 
//                      $("#list_"+project_id+ " .my_fvrt").attr('data-status', 'mark_star'); 
//                      $(".add_star").css('display','block');
//                      $(".remove_star").css('display','none');
//                  }
//              }
//              $('.loading_msg').html('');
//          }
//      });
//});
//$(document).on('click','.project_id_get_msg',function (){
//    var project_id = $(this).attr('data-id');
//    $(this).closest('.project_sectionfr_msg ').addClass('active');
//  //$(this).closest('.project_sectionfr_msg ').removeClass('unread_msg');
//  //$(this).closest('.project_sectionfr_msg ').addClass('star_project');
//  $(this).closest('.project_sectionfr_msg ').siblings().removeClass('active');
//  $('.loading_msg').html('<div class="loading"><span class="loading_process"><img src="'+$asset_path+'img/ajax-loader.gif" alt="loading"/></span></div>');
////console.log(project_id);
//$.ajax({
//    type: 'POST',
//    url: "<?php echo base_url(); ?>admin/messages/message_chat",
//            //dataType:'json',
//            data: {project_id: project_id,'type':1},
//            success: function (data) {
//              //  console.log(data);
//              $("#allChats").html(data);
//              $('.loading_msg').html('');
//          }
//      });
//
//});
    $(document).on('click', '.send_request_chat_admin', function (e) {
        var designer_id = $(this).attr("data-designerid");
        var request_id = $(this).attr("data-requestid");
        var sender_type = $(this).attr("data-senderrole");
        var sender_id = $(this).attr("data-senderid");
        var reciever_id = $(this).attr("data-receiverid");
        var reciever_type = $(this).attr("data-receiverrole");
        var text_id = 'text_' + request_id;
        var message = $('.' + text_id).val();
        var verified_by_admin = "1";
        var customer_name = $(this).attr("data-sendername");
        var customer_withornot = $(this).attr("data-customerwithornot");
        var profilepic = $(this).attr("data-profilepic");
        //console.log('msg',customer_withornot);
        if (message != "") {
            //console.log($('.text_' + request_id).val(""));
            $('.text_' + request_id).val("");
            if (customer_withornot == 0) {
                console.log('msg test1', message);
                if ($('#message_container_without_cus > div.msgk-chatrow:last-child > div').hasClass("msgk-right") === true) {
                    $("#message_container > div.msgk-chatrow:nth-last-child(1)").find('.just_now').css('display', 'none');
                    $('#message_container_without_cus').append('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + '"> <div class="msgk-mn"><div class="msgk-umsgbox"><span class="msgk-umsxxt">' + message + '</span></div></div><p class="msgk-udate just_now">Just Now</p></div></div>');
                } else {
                    $('#message_container_without_cus').append('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + '"> <figure class="msgk-uimg right-img"><span class="cust_tl"><?php echo $profile[0]['first_name']; ?></span><a class="msgk-uleft" href="javascript:void(0)" title="<?php echo $profile[0]['first_name']; ?>"></a></figure><p class="msgk-udate just_now">Just Now</p><div class="msgk-mn"><div class="msgk-umsgbox"><pre><span class="msgk-umsxxt">' + message + '</span></pre></div></div></div></div>');
                }
                $('#message_container_without_cus').stop().animate({
                    scrollTop: $('#message_container_without_cus')[0].scrollHeight
                }, 1000);
            } else if (customer_withornot == 1) {
                // console.log('testfd admin');
                if ($('#message_container > div.msgk-chatrow:last-child > div').hasClass("msgk-right") === true) {
                    //console.log(' admin msg');
                    $("#message_container > div.msgk-chatrow:nth-last-child(1)").find('.just_now').css('display', 'none');
                    $('#message_container').append('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + '"> <div class="msgk-mn"><div class="msgk-umsgbox"><p class="msgk-udate just_now"> <?php echo $profile[0]['first_name']; ?> - Just Now</p><pre><span class="msgk-umsxxt">' + message + '</span></pre></div></div></div></div>');
                } else {
                    //console.log(' admin no msg');
                    $('#message_container').append('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + '"><p class="msgk-udate just_now"> <?php echo $profile[0]['first_name']; ?> - Just Now</p><div class="msgk-mn"><div class="msgk-umsgbox"><pre><span class="msgk-umsxxt">' + message + '</span></pre></div></div></div></div>');
                }
                $('#message_container').stop().animate({
                    scrollTop: $('#message_container')[0].scrollHeight
                }, 1000);
            }
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>admin/dashboard/send_message_request",
                data: {"request_id": request_id,
                    "designer_id": designer_id,
                    "sender_type": sender_type,
                    "sender_id": sender_id,
                    "reciever_id": reciever_id,
                    "reciever_type": reciever_type,
                    "message": message,
                    "with_or_not_customer": customer_withornot,
                    "admin_seen": "1",
                },
                success: function (data) {
                    if (!$(".markmsg_disabled").hasClass("mark_messages")) {
                        $(".markmsg_disabled").addClass('mark_messages reddelete');
                    }
                }
            });
        }
    });

//$(document).on('click','.get_comment_chat',function (){
//    var project_id = $(this).attr('data-id');
//    $(this).closest('li').addClass('open');
//    $(this).closest('li').siblings().removeClass('open');
//    $('.loading_msg').html('<div class="loading"><span class="loading_process"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" alt="loading"/></span></div>');
////console.log($(this).closest('li'));
//$.ajax({
//    type: 'POST',
//    url: "<?php echo base_url(); ?>admin/messages/project_comment_chat",
//            //dataType:'json',
//            data: {project_id: project_id},
//            success: function (data) {
//              //  console.log(data);
//              $("#message_chat_area").html(data);
//              $('.loading_msg').html('');
//          }
//      });
//
//});

//    $(document).on('click', '.show_main_chat', function () {
//        var project_id = $(this).attr('data-id');
//        $(this).closest('li').addClass('open');
//        $(this).closest('li').siblings().removeClass('open');
////var project_value = $(this).attr('data-value');
//        console.log(project_id);
//        $.ajax({
//            type: 'POST',
//            url: "<?php echo base_url(); ?>admin/messages/message_chat",
//            //dataType:'json',
//            data: {project_id: project_id, 'type': 2},
//            success: function (data) {
//                //  console.log(data);
//                $("#message_chat_area").html(data);
//                //$(".orb-xb").css("display","none");
//            }
//        });
//
//    });
    $(document).on('click', '.send_comment_without_img', function (e) {
        //$('.send_comment_without_img').click(function (e) {
        e.preventDefault();
        var request_id = $(this).data("requestid");
        var sender_type = $(this).data("senderrole");
        var sender_id = $(this).data("senderid");
        var reciever_id = $(this).data("receiverid");
        var reciever_type = $(this).data("receiverrole");
        var text_id = 'text1_' + request_id;
        var message = $('.' + text_id).val();
        var verified_by_admin = "1";
        var customer_name = $(this).data("sendername");
        var designer_img = $(this).data("designerpic");
        if (message != "") {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>admin/dashboard/send_message",
                data: {"request_file_id": request_id,
                    "sender_role": sender_type,
                    "sender_id": sender_id,
                    "receiver_role": reciever_type,
                    "receiver_id": reciever_id,
                    "message": message,
                    "customer_name": customer_name,
                    "admin_seen": 1,
                },
                success: function (data) {
                    // alert("---"+data);
                    //alert("Settings has been updated successfully.");
//                    console.log(data);
//                    if (!$(".markmsg_disabled").hasClass("mark_messages")) {
//                        $(".markmsg_disabled").addClass('mark_messages reddelete');
//                    }
                    $('.sendtext').val("");
                    $(".messagediv_" + request_id + "").append('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right admin"><p class="msgk-udate right">' + customer_name + ' - Just Now</p><div class="msgk-mn"><div class="msgk-umsgbox"><pre><span class="msgk-umsxxt">' + message + '</span></pre></div></div></div></div>');
                    $(".messagediv_" + request_id + "").stop().animate({
                        scrollTop: $(".messagediv_" + request_id + "")[0].scrollHeight
                    }, 1000);
                },
            });
        }
    });
//    $(document).on('click','#load_more_messages',function (e){   
//    var data_count = parseInt($(this).attr("data-count"));
//    var data_seen = $(this).attr("data-seen");
//    $(this).attr("data-count",data_count+1);
//    $.ajax({
//            type: 'POST',
//            url: "<?php echo base_url(); ?>admin/messages/load_more_messages",
//            //dataType:'json',
//            data: {data_count: data_count,data_seen: data_seen},
//            success: function (data) {
//                $(".msg-list").append(data);
//            }
//        });
//    });

//        $(document).on('click','#load_more_messages',function (e){   
//    $(".msg-list").scroll(function () {
////          console.log('box_hight ',$(this).scrollTop() + $(this).innerHeight());
////          console.log('scroll_hight ',$(this)[0].scrollHeight);
//        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
////            console.log('reached_bottom');
//            var data_count = parseInt($("#load_more_messages").attr("data-count"));
////            console.log(data_count);
//
//            var data_seen = $("#load_more_messages").attr("data-seen");
////            console.log(data_seen);
//            $("#load_more_messages").attr("data-count", data_count + 1);
//
//            $.ajax({
//                type: 'POST',
//                url: "<?php echo base_url(); ?>admin/messages/load_more_messages",
//                //dataType:'json',
//                data: {data_count: data_count, data_seen: data_seen},
//                success: function (data) {
//
//                    $(".msg-list").append(data);
////                    $( ".change_tab" ).each(function( index ) {
////                     if(($(this).hasClass('active')) == true){
////                         var active_status = $(this).attr('data-status');
////                         read_unread_message(active_status);
////                     }
////                     });
//                    //  console.log(data);
//                    //$("#message_chat_area").html(data);
//                    //$(".orb-xb").css("display","none");
//                }
//            });
//        }
//    });

//        $("a.show_trans").click(function () {
//    $(document).on('click', '.show_trans', function (e) {
//        $(".transcopy").slideToggle()
//    });

//    function read_unread_message(active_status) {
//        if (active_status == 'read') {
//            $('.project_sectionfr_msg.read').css('display', 'flex');
//            $('.project_sectionfr_msg.unread_msg').css('display', 'none');
//        } else {
//            $('.project_sectionfr_msg.unread_msg').css('display', 'flex');
//            $('.project_sectionfr_msg.read').css('display', 'none');
//        }
//    }
</script> 
