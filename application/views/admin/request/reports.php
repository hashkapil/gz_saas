<style type="text/css">
    .select-c::-ms-expand {
        display: none;
    }
    .search-box {
        position: absolute;
        top: -187px;
        right: -91px;
    }
    .ajax_searchload {
        position: absolute;
        top: 21px;
        right: 95px;

    }
    .ajax_loader{
        margin-top:20px;
    }
    div#chartContainer {
    float: left;
}
    a.load_more.button {
        background-color: #e8304d;
        border: none;
        border-radius: 50px;
        color: white!important;
        padding: 8px 45px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 17px;
        margin: -5px 0;
        cursor: pointer;
        text-decoration: none !important;
        margin-top: 20px;
        font-family: 'Montserrat', sans-serif;
    }
        form.date-report {
    font: normal 15px/20px 'GothamPro', sans-serif;margin-top: 12px;    float: right;
}
form.date-report p {
    display: inline-block;
}
form.date-report input[type="submit"] {
    background: #ec3d56;
    color: #fff;
    border: 0;
    padding: 8px 16px;
}
form.date-report p input {
    border-radius: 0;    margin-bottom: 0;
}
</style>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<section class="con-b">
    <?php //echo "<pre>";print_r($reports); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if ($this->session->flashdata('message_error') != '') { ?>    
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_error'); ?></strong>    
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('message_success') != '') { ?>    
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                    </div>
                <?php } ?>
                <div class="flex-this">
                    <h2 class="main_page_heading">Reports</h2>
                </div>
            </div>
        </div>
        <div class="cli-ent table">
                <div class="header-blog">
                    <div class="row flex-show">
                        <div class="col-md-5">
                            <ul class="list-unstyled list-header-blog" id="report_tabs">
                                <li class="active" id="Reports_sec">
                                    <a data-toggle="tab" href="#Active_reports" role="tab" class="show_loading">Reports</a>
                                </li>
                                <li class="">
                                    <a data-toggle="tab" href="#Active_state" role="tab" class="show_loading">States</a>
                                </li>
                                <li class="" id="refund_tab">
                                    <a data-toggle="tab" href="#Refund_taxes" role="tab" class="show_loading">Refund Taxes</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-7">
                             <form class="date-report" action="<?php echo base_url(); ?>admin/dashboard/reports" method="POST">
                                <p>Select Date: <input type="text" id="datepicker_start" name="start_date" class="form-control" autocomplete="off"></p>
                                <input type="submit" name="get_reports"/>
                            </form>
                        </div>
                    </div>
                </div>
                <p class="space-e"></p>
                <div class="tab-content">
                <div class="tab-pane product-list-show active" id="Active_reports" role="tabpanel">
<!--                <div class="product-list-show" id="Active_reports">-->
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 60%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head-q">New Customer in last 24 Hours</h3>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center"><span class="green text-uppercase"><?php echo $customercount; ?></span></p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 60%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head-q">Number of new trial users in last 24 hours</h3>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center"><span class="green text-uppercase"><?php echo $freecustcount; ?></span></p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <!--**********how many requests due today**********-->
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 60%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head-q">Requests added in last 24 hours</h3>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center"><span class="green text-uppercase"><?php echo $reqcount; ?></span></p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <!--**********- how many requests are over due?**********-->
                    <!--- how many requests an average customer submits per month?
                        - how many request an average customer approves per month?
                        - how many cancelled customers?-->
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 60%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head-q">Total requests submitted by trial users till now</h3>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center"><span class="green text-uppercase"><?php echo (isset($trailuserreqcount) && $trailuserreqcount != '') ? $trailuserreqcount : 'N/A'; ?></span></p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 60%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head-q">Total requests submitted by normal users till now</h3>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center"><span class="green text-uppercase"><?php echo $normaluserreqcount; ?></span></p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 60%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head-q">Total approved requests by trial users till now</h3>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center"><span class="green text-uppercase"><?php echo $trailapprovedreqcount; ?></span></p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 60%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head-q">Average requests by customers per month</h3>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center"><span class="green text-uppercase"><?php echo $avgreqcount; ?></span></p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 60%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head-q">Average approved requests by customer per month</h3>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center"><span class="green text-uppercase"><?php echo $getavgapprovedrequestcount; ?></span></p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 60%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head-q">Trial users received a design</h3>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center"><span class="green text-uppercase"><?php echo $gettrialdesigncount; ?></span></p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    
                </div>
                <div class="tab-pane product-list-show" id="Active_state" role="tabpanel">
                    <?php //echo "<pre/>";print_r($stateData);
                    foreach($stateData as $data){ 
                        //echo "<pre/>";print_r($data);
                        ?>
                     <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 60%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head-q"><?php echo (($data['state'] != '') || $data['state'] != NULL) ? $data['state']:'No state'; ?></h3>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center"><span class="green text-uppercase"><?php echo isset($data['count'])?$data['count']:''; ?></span></p>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <?php } ?>
               </div>
                    <div class="tab-pane product-list-show" id="Refund_taxes" role="tabpanel">
                      <?php //echo "<pre>";print_r($refund_tax_amount); ?>
                    <div class="col-md-12">
                    <div id="no-more-tables">
                        <?php 
                    if(count($refund_tax_amount)>0){ ?>
                    <table class="table">
                    <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Refund Amount</th>
                            <th>Type</th>
                            <th>Tax Amount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php 
                    foreach($refund_tax_amount as $refund_tax_amounts){ ?>
                    <tr>
                        <td data-title="Customer Name"><?php echo $refund_tax_amounts['first_name']; ?></td>
                        <td data-title="Refund Amount"><?php echo $refund_tax_amounts['actual_amount']; ?></td>
                        <td data-title="Type"><?php echo $refund_tax_amounts['type']; ?></td>
                        <td data-title="Tax Amount"><?php echo $refund_tax_amounts['tax_amount']; ?></td>
                        <td data-title="Action"> <a href="javascript:void(0)" class="refund_tax_popup" data-id="<?php echo $refund_tax_amounts['id']; ?>" data-toggle="modal" data-target="#Tax_popup">Add Tax Amount</a></td>
                    </tr>
                    <?php }
                     ?> 
                    <tbody>
                    </tbody>
                    </table>
                    <?php } else{
                        echo "Data Not Found";
                    }?>
               </div>
            </div>
        </div>
    </div>
                </div>
    </div>
</section>


<div class="modal fade similar-prop" id="Tax_popup" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                  <h2 class="popup_h2 del-txt">Add Tax Amount</h2>
                  <div class="close  edit_close" data-dismiss="modal" aria-label="Close"> x</div>
              </header>
                <div class="cli-ent-model">
                    <div class="fo-rm-body">
                        <form action="<?php echo base_url(); ?>admin/dashboard/add_taxesfor_refund" method="post" enctype="multipart/form-data">
                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="form-group">
                                        <p class="label-txt">Tax Amount</p>
                                        <input type="hidden" name="tax_id" required  class=" input tax_id">
                                        <input type="text" name="add_tax" required  class=" input name">
                                        <div class="line-box">
                                          <div class="line"></div>
                                      </div>
                                  </label>
                                </div>
                            </div>
                            <p class="space-b"></p>
                           <button type="submit" class="btn-g load_more button" name="edit" class="save_refund">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js');?>"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
$(document).ready(function () {
        $("#datepicker_start").datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function (dateText, inst) {
                var date = $(this).val();
                 $('#datepicker_start').val(date);
            }
        });
});
$(document).on('click','.refund_tax_popup',function(e){
var id = $(this).attr('data-id');
//console.log(id);
$('.tax_id').val(id);

});

var url = window.location.href;
        var activeTab = url.substring(url.indexOf("#") + 1);
        var hash = window.location.href.indexOf('#');
    console.log(activeTab);
   if(activeTab != '' && hash > 0){
       $('#Reports_sec').removeClass("active");
    $(".tab-pane").removeClass("active");
    $('#' + activeTab).addClass("active");
    $('#refund_tab').addClass("active");
    
    
    //make_grid();
    var id = $('.show_loading').data("target");
   // var list = $("" + id + " li");
    //var button = $(".load_more");
   // loadMore(list, button);
       // $('.loading').css('display','none');
       $('#report_tabs').find('li a').each(function(e) {
        var dataattr = $(this).data('target');
        var fromurl = ("#" + activeTab);
        if(dataattr == fromurl){
            $(this).addClass('active');
            $(this).closest('li').siblings().find('a').removeClass('active');
            return false;
        }
    });
   }
</script>
