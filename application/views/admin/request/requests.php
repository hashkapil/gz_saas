<?php //echo "<pre>"; print_r($active_project);exit; ?>
<h2 class="float-xs-left content-title-main" style="display:inline-block;padding-left:40px;">Projects</h2>
<!--<a href="<?php echo base_url(); ?>customer/request/view_designer" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Designer Profile</a>-->
</div>
<div class="col-md-12" style="background:white;">
<div class="">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147; } 
.pinktext { color: #ec4159; }
.weight600 { font-weight:600; }
.weight600 { font-weight:500; }
.font18 { font-size:18px; }
.font16 { font-size:12px; }
.font16 { font-size:16px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
	font-size: 25px !important;
    padding: 8px 0px !important;
    font-weight: 600 !important;
    letter-spacing: -3px !important;
	padding: 40px 0px 0px 0px !important;
}
.projecttitle{
	font-size: 16px;
	line-height:18px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: 1px solid #000;
    background: unset;
    
}
.trborder:hover{
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
	background-color: unset !important;
}
table {     border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}

/*
.content .nav-tab-pills-image .nav-item.active a {
border:unset !important;
}
.wrapper .nav-tab-pills-image ul li .nav-link:hover{
	color:#000;
}

.wrapper .nav-tab-pills-image ul li .nav-link{
	color: #2f4458;
	height:43px;
}
.nav-tab-pills-image ul li .nav-link:hover {
    border-bottom: none !important;
}
.wrapper .nav-tab-pills-image ul li .nav-link:focus
{	
	color:#fff;
	border:none;
}
.content .nav-tab-pills-image .nav-item.active a:hover{
	color:#fff;
}
.content .nav-tab-pills-image .nav-item.active dd {
	background:unset !important;
}
.nav-tab-pills-image ul li .nav-link:hover {
    border-bottom: none !important;
}
*/

.background_1st { background-color:#98d575; }
.background_2nd { background-color:#f7941f; }
.background_3rd { background-color:#409ae8; }
.projectstatus{ font-size:12px; }
.projectdesc{ font-size:12px; }
input.empty {
    font-family: FontAwesome;
    font-style: normal;
    font-weight: normal;
    text-decoration: inherit;
}
.projecttitle {
    font-size: 20px;
    line-height: 18px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
    text-transform: capitalize;
}

.design_complete .desc .projecttitle{text-transform: unset;}
</style>
    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">         
                <div class="nav-tab-pills-image mytab">

						<ul class="nav nav-tabs" role="tablist" style="border:none;">
							<li class="nav-item <?php if($heading == "incoming"){ echo "active"; } ?>">
								<a class="nav-link tabmenu" data-toggle="tab" href="#incoming_designs_tab" role="tab">
									<?php echo "Active"; ?>(<?php echo sizeof($active_project); ?>)
								</a>
							</li>
							<li class="nav-item <?php if($heading == "on_going"){ echo "active"; } ?>">
								<a class="nav-link tabmenu" data-toggle="tab" href="#on_going_request" role="tab">
									<?php echo "In-Queue"; ?>(<?php echo sizeof($pending_project); ?>)
								</a>
							</li>
							<li class="nav-item <?php if($heading == "pending"){ echo "active"; } ?>">
								<a class="nav-link tabmenu" data-toggle="tab" href="#pending_request" role="tab">
									<?php echo "Pending Approved"; ?>(<?php echo sizeof($check_approve_project); ?>)
								</a>
							</li>
							<li class="nav-item <?php if($heading == "approved"){ echo "active"; } ?>">
								<a class="nav-link tabmenu" data-toggle="tab" href="#approved_request" role="tab">
									<?php echo "Completed"; ?>(<?php echo sizeof($complete_project); ?>)
								</a>
							</li>

							<li class="nav-item float-right">
                                <input type="button" class="btn weight600" style="border-radius:5px;background:#d4d4d4;display:none;" value="Filter" />
								<p class="nav-link dd">
                                    <i class="fa fa-search onlysearch"></i>
                                    <input class="fbold" type="text" id="myInput" onkeyup="myFunction()" name="search_request" placeholder="Search" style="border:none;padding:5px;">
                                </p>
                            </li>
						</ul>


					<?php
					function ordinal($number) {
						$ends = array('th','st','nd','rd','th','th','th','th','th','th');
						if ((($number % 100) >= 11) && (($number%100) <= 13))
							return $number. 'th';
						else
							return $number. $ends[$number % 10];
					}
					?>
                    <div class="tab-content">
                        <div class="tab-pane  <?php if($heading == "incoming"){ echo "active"; } ?> content-datatable datatable-width" id="incoming_designs_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
									<div class="table_draft_sec table_draft_complete design_complete pending_approvel">
									<table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive dataTable" style=" border-collapse:separate; border-spacing: 0 1em;border:none; padding-top: 15px;" id="DataTables_Table_0">
									    <tbody class="">
									    <?php //echo "<pre>"; print_r(($active_project)); echo "<pre>"; 
									    for($i=0;$i<sizeof($active_project);$i++){ ?>
									        <?php
									        $class_name = ""; 
									        if ($active_project[$i]['status'] == "checkforapprove") {
									    		$status = "Review your design";
									    		$class_name = "";
									        } elseif ($active_project[$i]['status'] == "active") {
									    		$status = "In Progress";
									    		$class_name = "";
									        } elseif ($active_project[$i]['status'] == "disapprove") {   
									    		$status = "Revision";
									    		$class_name = "red";
									        } else {
									    		$status = "In-Queue";
									    		$class_name = "";
									        } 
									        ?>
									        <tr onclick="window.location.href='<?php echo base_url()."admin/request/view_request/".$active_project[$i]['id']; ?>'" class="trborder <?php echo $class_name; ?>" id="<?php echo $active_project[$i]['id'];  ?>" data-indexid="0">
									        	<?php 
									        	$duedate = "";
									        	
									        	if($active_project[$i]['plan_turn_around_days']){
									        	if($active_project[$i]['status_admin'] == "disapprove"){
									        		$active_project[$i]['dateinprogress'] = date("Y-m-d",strtotime($active_project[$i]['dateinprogress']." +1 day"));
									        	}
									        		$date = date("Y-m-d",strtotime($active_project[$i]['dateinprogress']));
									        		
									        		$duedate = date("d/m/Y",strtotime($date." ".$active_project[$i]['plan_turn_around_days']." weekdays "));
									        	} ?>
									            <td class="completed">
									                <span>Delivery</span>
									                <span><?php echo $duedate; ?></span>
									            </td>
									            <td class="desc desc_qa">
									                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $active_project[$i]['title']; ?></p>
									                <p class="small"><?php echo $active_project[$i]['work_type']; ?></p>
									                <p><span class="queue green"><?php echo $status; ?></span></p>
									            </td>
									            
									            <td class="designer_assig designer_des">
									                <div class="des">
									                    <div class="img float-left">
									                    <?php
									                    $profile_img = $active_project[$i]['customer_profile_picture'];
									                    if ($profile_img) { ?>
									                    <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $active_project[$i]['customer_profile_picture']; ?>" class="img-fluid">
									                    <?php	
									                    }
									                    else { ?>
									                     <img src="<?php echo base_url() . 'uploads/profile_picture';?>/13b2d60af0a45fefd0d0d0633339a453.jpeg" class="img-fluid">
									                    <?php
									                    }
									                    ?>
									                    </div>
									                    <p class="darkblacktext weight600 name"><?php if($active_project[$i]['customer_first_name']){ echo $active_project[$i]['customer_first_name']." ".$active_project[$i]['customer_last_name']; }else{ echo "Customer Not Available"; } ?></p>
									                    <p class="darkblacktext client">Client</p>
									                </div>
									            </td>
									            
									            <td class="designer_assig designer_des">
									                <div class="des" style="border-left: 1px solid #eceeef;">
									                    <div class="img float-left">
										                    <?php
										                    $profile_img = $active_project[$i]['profile_picture'];
										                    if ($profile_img) { ?>
										                    <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $active_project[$i]['profile_picture']; ?>" class="img-fluid">
										                    <?php	
										                    }
										                    else { ?>
										                     <img src="<?php echo base_url() . 'uploads/profile_picture';?>/13b2d60af0a45fefd0d0d0633339a453.jpeg" class="img-fluid">
										                    <?php
										                    }
										                    ?>
									                    </div>
									                    <p class="darkblacktext weight600 name"><?php if($active_project[$i]['designer_first_name']){ echo $active_project[$i]['designer_first_name']; }else{ echo "Unassignes"; } ?></p>
									                    <p class="darkblacktext client">Designer</p>
									                </div>
									            </td>

									            <td class="notification">
									                <p class="file">
									                    <span><i class="fa fa-file" aria-hidden="true"></i></span>
									                    <span class="num">0</span>
									                </p>
									                <p class="darkblacktext">
									                    <img src="http://backup.graphicszoo.com/public/img/img/messageicon.png" class="img-responsive imgauto" alt="">
									                    <span class="num"><?php echo $active_project[$i]['total_chat']; ?></span>
									                </p>
									            </td>
									        </tr>
									    </tbody>
									    <?php  } ?>
									</table>
									</div>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane  <?php if($heading == "on_going"){ echo "active"; } ?> content-datatable datatable-width" id="on_going_request" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
									<div class="table_draft_sec table_draft_complete design_complete in_queue_sec">
									<table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none; padding-top: 15px;">
									    <tbody class="">
									    <?php
									    //echo "<pre>"; print_r($pending_project); echo "<pre>" ; 
									    for($i=0;$i<sizeof($pending_project);$i++){ ?>
									    	<?php if ($pending_project[$i]['status'] == "checkforapprove") {
												$status = "Review your design";
										    } elseif ($pending_project[$i]['status'] == "active") {
										        
												$status = "Design In Progress";
										    } elseif ($pending_project[$i]['status'] == "disapprove") {
										        
												$status = "Revision In Progress";
										    } elseif ($pending_project[$i]['status'] == "assign") {
										        
												$status = "IN-Progress";
										    } else {
												$status = "In-Queue";
										    } ?>
									        <tr onclick="window.location.href='<?php echo base_url()."admin/request/view_request/".$pending_project[$i]['id']; ?>'" class="trborder" id="$pending_project[$i]['id']; " data-indexid="0">
										        <?php 
									        	$duedate = "";
									        	
									        	if($pending_project[$i]['plan_turn_around_days']){
									        	if($pending_project[$i]['status_admin'] == "disapprove"){
									        		$pending_project[$i]['dateinprogress'] = date("Y-m-d",strtotime($pending_project[$i]['dateinprogress']." +1 day"));
									        	}
									        		$date = date("Y-m-d",strtotime($pending_project[$i]['dateinprogress']));
									        		
									        		$duedate = date("d/m/Y",strtotime($date." ".$pending_project[$i]['plan_turn_around_days']." weekdays "));
									        	} ?>

									            <td class="completed">
									                <span>Delivery</span>
									                <span><?php echo $duedate; ?></span>
									            </td>
									            <td class="desc">
									                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $pending_project[$i]['title']; ?></p>
									                <p class="small"><?php echo($pending_project[$i]['work_type']); ?></p>
									                <p><span class="queue green"><?php echo $status; ?></span></p>
									            </td>
									            
									            <td class="designer_assig">
									                <div class="img float-left">
									               		<?php
									               		$profile_img = $pending_project[$i]['customer_profile_picture'];
									               		if ($profile_img) { ?>
									               		<img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $pending_project[$i]['customer_profile_picture']; ?>" class="img-fluid">
									               		<?php	
									               		}
									               		else { ?>
									               		 <img src="<?php echo base_url() . 'uploads/profile_picture';?>/13b2d60af0a45fefd0d0d0633339a453.jpeg" class="img-fluid">
									               		<?php
									               		}
									               		?>
									                </div>
									                <p class="darkblacktext weight600 name"><?php if($pending_project[$i]['customer_first_name']){ echo $pending_project[$i]['customer_first_name']." ".$pending_project[$i]['customer_last_name']; }else{ echo "Customer Not Available"; } ?></p>
									                <p class="darkblacktext client">Client</p>
									            </td>

									            <td class="notification">
									                <p class="file">
									                    <span><i class="fa fa-file" aria-hidden="true"></i></span>
									                    <span class="num">0</span>
									                </p>
									                <p class="darkblacktext">
									                    <img src="http://backup.graphicszoo.com/public/img/img/messageicon.png" class="img-responsive imgauto" alt="">
									                    <span class="num"><?php echo $pending_project[$i]['total_chat']; ?></span>
									                </p>
									            </td>
									        </tr>
									    <?php } ?>
									    </tbody>
									</table>
									</div>
  
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane  <?php if($heading == "pending"){ echo "active"; } ?> content-datatable datatable-width" id="pending_request" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
									<div class="table_draft_sec table_draft_complete design_complete pending_approvel">
									<table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none; padding-top: 15px;">
									    <tbody class="">
									    <?php for($i=0;$i<sizeof($check_approve_project);$i++){ ?>
									        <tr onclick="window.location.href='<?php echo base_url()."admin/request/view_request/".$check_approve_project[$i]['id']; ?>'" class="trborder" id="<?php echo $check_approve_project[$i]['id']; ?>" data-indexid="0">
									        	<?php if ($check_approve_project[$i]['status'] == "checkforapprove") {  
									        		$status = "IN-Progress";
									            } elseif ($check_approve_project[$i]['status'] == "active") {
									                
									        		$status = "Design In Progress";
									            } elseif ($check_approve_project[$i]['status'] == "disapprove") {
									                
									        		$status = "Revision In Progress";
									            } else {
									        		$status = "In-Queue";
									            } ?>

									            <td class="completed">
									            <?php 
									            $duedate = "";
									            
									            if($check_approve_project[$i]['plan_turn_around_days']){
									            if($check_approve_project[$i]['status_admin'] == "disapprove"){
									            	$check_approve_project[$i]['dateinprogress'] = date("Y-m-d ",strtotime($check_approve_project[$i]['dateinprogress']." +1 day"));
									            }
									            	$date = date("Y-m-d",strtotime($check_approve_project[$i]['dateinprogress']));
									            	
									            	$duedate = date("d/m/Y",strtotime($date." ".$check_approve_project[$i]['plan_turn_around_days']." weekdays "));
									            } ?>
									                <span>Delivery</span>
									                <span><?php echo $duedate; ?></span>
									            </td>
									            <td class="desc desc_qa">
									                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $check_approve_project[$i]['title']; ?></p>
									                <p class="small"><?php echo $check_approve_project[$i]['work_type']; ?></p>
									                <p><span class="queue green"><?php echo $status; ?></span></p>
									            </td>
									            
									            <td class="designer_assig designer_des">
									                <div class="des">
									                    <div class="img float-left">
										                <?php
									                    $profile_img = $check_approve_project[$i]['customer_profile_picture'];
									                    if ($profile_img) { ?>
									                    <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $check_approve_project[$i]['customer_profile_picture']; ?>" class="img-fluid">
									                    <?php	
									                    }
									                    else { ?>
									                     <img src="<?php echo base_url() . 'uploads/profile_picture';?>/13b2d60af0a45fefd0d0d0633339a453.jpeg" class="img-fluid">
									                    <?php
									                    }
									                    ?>
									                    </div>
									                    <p class="darkblacktext weight600 name"><?php if($check_approve_project[$i]['customer_first_name']){ echo $check_approve_project[$i]['customer_first_name']; }else{ echo "Customer Not Available"; } ?></p>
									                    <p class="darkblacktext client">Client</p>
									                </div>
									            </td>
									            
									            <td class="designer_assig designer_des">
									                <div class="des" style="border-left: 1px solid #eceeef;">
									                    <div class="img float-left">
									                    <?php
										                    $profile_img = $check_approve_project[$i]['profile_picture'];
										                    if ($profile_img) { ?>
										                    <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $check_approve_project[$i]['profile_picture']; ?>" class="img-fluid">
										                    <?php	
										                    }
										                    else { ?>
										                     <img src="<?php echo base_url() . 'uploads/profile_picture';?>/13b2d60af0a45fefd0d0d0633339a453.jpeg" class="img-fluid">
										                    <?php
										                    }
										                    ?>
									                    </div>
									                    <p class="darkblacktext weight600 name"><?php if($check_approve_project[$i]['designer_first_name']){ echo $check_approve_project[$i]['designer_first_name']; }else{ echo "No Designer"; } ?></p>
									                    <p class="darkblacktext client">Designer</p>
									                </div>
									            </td>

									            <td class="notification">
									                <p class="file">
									                    <span><i class="fa fa-file" aria-hidden="true"></i></span>
									                    <span class="num">0</span>
									                </p>
									                <p class="darkblacktext">
									                    <img src="http://backup.graphicszoo.com/public/img/img/messageicon.png" class="img-responsive imgauto" alt="">
									                    <span class="num"><?php echo $check_approve_project[$i]['total_chat']; ?></span>
									                </p>
									            </td>
									        </tr>
									    <?php  } ?>
									    </tbody>
									</table>
									</div>

                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane  <?php if($heading == "approved"){ echo "active"; } ?> content-datatable datatable-width" id="approved_request" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
									<div class="table_draft_sec table_draft_complete design_complete">
									<table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none; padding-top: 15px;">
									    <tbody class="">
									    <?php for($i=0;$i<sizeof($complete_project);$i++){ ?>
									        <tr onclick="window.location.href='<?php echo base_url()."admin/request/view_request/".$complete_project[$i]['id']; ?>'" class="trborder" id="<?php echo $complete_project[$i]['id']; ?>" data-indexid="0">
									        	<?php if ($complete_project[$i]['status'] == "checkforapprove") {
									        		$status = "Review your design";
									            } elseif ($complete_project[$i]['status'] == "active") {
									        		$status = "Design In Progress";
									            } elseif ($complete_project[$i]['status'] == "disapprove") {
									        		$status = "Revision In Progress";
									            } elseif ($complete_project[$i]['status'] == "approved") {
									                
									        		$status = "Completed";
									            } else {
									        		$status = "In-Queue"; 
									            } ?>
									            <td class="completed"><span>approved</span></td>
									            <td class="desc desc_qa">
									                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $complete_project[$i]['title']; ?></p>
									                <p class="small"><?php echo $complete_project[$i]['work_type']; ?></p>
									                <p><span class="queue"><?php echo $status; ?></span></p>
									            </td>

									            <td class="designer_assig designer_des">
									                <div class="des">
									                    <div class="img float-left">
									                    	<?php
									                    $profile_img = $complete_project[$i]['customer_profile_picture'];
									                    if ($profile_img) { ?>
									                    <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $complete_project[$i]['customer_profile_picture']; ?>" class="img-fluid">
									                    <?php	
									                    }
									                    else { ?>
									                     <img src="<?php echo base_url() . 'uploads/profile_picture';?>/13b2d60af0a45fefd0d0d0633339a453.jpeg" class="img-fluid">
									                    <?php
									                    }
									                    ?>
									                    </div>
									                    <p class="darkblacktext weight600 name"><?php if($complete_project[$i]['customer_first_name']){ echo $complete_project[$i]['customer_first_name']; }else{ echo "Customer Not Available"; } ?></p>
									                    <p class="darkblacktext client">Client</p>
									                </div>
									            </td>
									            
									            <td class="designer_assig designer_des">
									                <div class="des" style="border-left: 1px solid #eceeef;">
									                    <div class="img float-left">
									                    	<?php
										                    $profile_img = $complete_project[$i]['profile_picture'];
										                    if ($profile_img) { ?>
										                    <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $complete_project[$i]['profile_picture']; ?>" class="img-fluid">
										                    <?php	
										                    }
										                    else { ?>
										                     <img src="<?php echo base_url() . 'uploads/profile_picture';?>/13b2d60af0a45fefd0d0d0633339a453.jpeg" class="img-fluid">
										                    <?php
										                    }
										                    ?>
									                    </div>
									                    <p class="darkblacktext weight600 name"><?php if($complete_project[$i]['designer_first_name']){ echo $complete_project[$i]['designer_first_name']; }else{ echo "No Designer"; } ?></p>
									                    <p class="darkblacktext client">Designer</p>
									                </div>
									            </td>

									            <td class="notification">
									                <p class="file">
									                    <span><i class="fa fa-file" aria-hidden="true"></i></span>
									                    <span class="num">0</span>
									                </p>
									                <p class="darkblacktext">
									                    <img src="http://backup.graphicszoo.com/public/img/img/messageicon.png" class="img-responsive imgauto" alt="">
									                    <span class="num"><?php echo $complete_project[$i]['total_chat']; ?></span>
									                </p>
									            </td>
									        </tr>
									    <?php } ?>
									    </tbody>
									</table>
									</div>                
                                </div>
                            </div>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
</div>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>