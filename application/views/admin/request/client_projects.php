<h2 class="float-xs-left content-title-main" style="display:inline-block;">Projects</h2>
<a href="<?php echo base_url(); ?>customer/request/view_designer" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Designer Profile</a>
</div>
<div class="col-md-12" style="background:white;">
<div class="col-md-10 offset-md-1">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147; } 
.pinktext { color: #ec4159; }
.weight600 { font-weight:600; }
.font16 { font-size:18px; }
.font16 { font-size:16px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss {
    font-size: 25px !important;
    padding: 20px 13px !important;
    font-weight: 600 !important;
    letter-spacing: -3px !important;
    padding: 40px 17px 0px 11px !important;
    width: 70px;
}
.projecttitle{
	font-size: 20px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: none;
    background: unset;
    
}
.trborder:hover{
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
.table-hover tbody tr:hover{
	background-color:#fff;
}
table {     border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}
.nav-tab-pills-image ul li .nav-link {
    color:#1a3147;
	font-weight:600;
	padding: 7px 25px;
}
.content .nav-tab-pills-image .nav-item.active a {
border:unset !important;
}
.wrapper .nav-tab-pills-image ul li .nav-link:hover{
	color:#000;
}

.wrapper .nav-tab-pills-image ul li .nav-link{
	color: #2f4458;
	height:43px;
}
.nav-tab-pills-image ul li .nav-link:hover {
    border-bottom: none !important;
}
.wrapper .nav-tab-pills-image ul li .nav-link:focus
{	
	color:#fff;
	border:none;
}
.content .nav-tab-pills-image .nav-item.active a:hover{
	color:#fff;
}
.content .nav-tab-pills-image .nav-item.active dd {
	background:unset !important;
}
.nav-tab-pills-image ul li .nav-link:hover {
    border-bottom: none !important;
}
.background_1st { background-color:#98d575; }
.background_2nd { background-color:#f7941f; }
.background_3rd { background-color:#409ae8; }
</style>
    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">         
			
				<div class="col-sm-12" style="margin:40px 0px;">
					<div class="col-sm-1" style="padding:0px;">
						<center>
							 <div class="col-sm-2" style="padding:0px;width:100%;">
								 
								<?php
								if ($userdata[0]['profile_picture']) 
								{
								?>
								 <div class="myprofilebackground" ><p style="padding-top: 0px;">
								 <?php
									echo '<img src="' . base_url() . 'uploads/profile_picture/' . $userdata[0]['profile_picture'] . '" style="height:60px;width:60px; border-radius:50%;border: 3px solid #0190ff;">';
								
								?>
								 </p></div>         
								<?php  
								}
								else 
								{
								?>
								<div class="myprofilebackground" style="width:60px; height:60px;border-radius:50%; background: #0190ff;border: 3px solid #0190ff; text-align: center; font-size: 15px; color: #fff;"><p style="font-size: 22px;padding-top: 15px;">
										
								<?php 
									
									echo ucwords(substr($userdata[0]['first_name'],0,1)) .  ucwords(substr($userdata[0]['last_name'],0,1));
								?>
									</p></div>
								<?php
								}
								?>
							</div>
						</center>		
					</div>
					<div class="col-sm-9">
						<h5 class="weight600 darkblacktext" style="clear: both;"><?php echo $userdata[0]['first_name']." ".$userdata[0]['last_name']; ?></h5>
						<p class="greytext font16 pb0">Member since <?php echo date("M d, Y",strtotime($userdata[0]['created'])); ?></p>
					</div>
				</div>
                <div class="nav-tab-pills-image">
					<div class="col-sm-10">
						<ul class="nav nav-tabs" role="tablist" style="border-bottom: unset;padding:0px;">                      
							<li class="nav-item active" style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" data-toggle="tab" href="#incoming_designs_tab" role="tab">
									Active Projects(<?php echo sizeof($incoming_request); ?>)
								</a>
							</li>
							<li class="nav-item" style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" data-toggle="tab" href="#ongoing_designs_tab" role="tab">
									In-Queue(<?php echo sizeof($ongoing_request); ?>)
								</a>
							</li>
							 <li class="nav-item"style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" data-toggle="tab" href="#pendingforapproverequest" role="tab">
									Pending Approved(<?php echo sizeof($pending_request); ?>)
								</a>
							</li>
							<li class="nav-item"style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" data-toggle="tab" href="#approved_designs_tab" role="tab">
									Completed(<?php echo sizeof($approved_request); ?>)
								</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-2" style="padding:0px;">
						<!--<input type="button" class="btn weight600" style="border-radius:5px;background:#d4d4d4;" value="Filter" />-->
						
							<i class="fa fa-search onlysearch" style="padding-top:15px;"></i>
							<input class="fbold" type="text" id="myInput" onkeyup="myFunction()" name="search_request" placeholder="Search" style="border:none;">
						
					</div>
					<?php 
					function ordinal($number) {
						$ends = array('th','st','nd','rd','th','th','th','th','th','th');
						if ((($number % 100) >= 11) && (($number%100) <= 13))
							return $number. 'th';
						else
							return $number. $ends[$number % 10];
					}
					?>
                    <div class="tab-content">
                        <div class="tab-pane active content-datatable datatable-width" id="incoming_designs_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                     <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style="border:none;border-collapse:separate;">
                                        <!--<thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Project Name</th>
												<th>Designer Name</th>
												<th>Requested</th>
												<th>In-Progress</th>
												<th>Due Date</th>
												<th>Messages</th>
												<th>Star</th>
												<th>Icon</th>
												<th>Delete</th>
                                            </tr>
                                        </thead>-->
                                        <tbody>
										<?php for($i=0;$i<sizeof($incoming_request);$i++){ ?>
										   <tr class="trborder">
												<td class="<?php if($i+1 > 3){ echo "greybackground"; } else{ echo "background_".ordinal($i+1); } ?> whitetext numbercss"><?php echo ordinal($i+1); ?></td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $incoming_request[$i]['title']; ?></p>
													<p class="greentext weight600 textleft pl20 pb5">Waiting for approval</p>
													<p class="darkblacktext textleft pl20"><?php echo substr($incoming_request[$i]['description'], 0, 35);  ?></p>
												</td>
												<td style="padding-top:32px;">
													<p class="greytext pb0 weight600 font16">Designer</p>
													<p class="darkblacktext weight600"><?php if($incoming_request[$i]['designer_first_name']){ echo $incoming_request[$i]['designer_first_name']; }else{ echo "No Designer"; } ?></p>
												</td>
												<td style="padding-top:32px;">
													<p class="greytext pb0 weight600 font16">Requested</p>
													<p class="darkblacktext weight600"><?php echo date("d/m/y",strtotime($incoming_request[$i]['created'])); ?></p>
												</td>
												<td style="padding-top:32px;">
													<p class="greytext pb0 weight600 font16">In-Progress</p>
													<p class="darkblacktext weight600"><?php echo date("d/m/y",strtotime($incoming_request[$i]['dateinprogress'])); ?></p>
												</td>
												<td style="padding-top:32px;">
													<p class="greytext pb0 weight600 font16">Due Date</p>
													<?php 
													$duedate = "";
													
													if($userdata[0]['plan_turn_around_days']){
													if($incoming_request[$i]['status_admin'] == "disapprove"){
														$incoming_request[$i]['dateinprogress'] = date("Y-m-d",strtotime($incoming_request[$i]['dateinprogress']." +1 day"));
													}
														$date = date("Y-m-d",strtotime($incoming_request[$i]['dateinprogress']));
														$duedate = date("d-m-Y",strtotime($date." ".$userdata[0]['plan_turn_around_days']." weekdays "));
													} ?>
													<p class="darkblacktext weight600"><?php echo $duedate; ?></p>
												</td>
												<td style="padding-top:32px;">
													<p class="greytext pb0 weight600 font16">Messages</p>
													<p class="darkblacktext weight600"><svg style="vertical-align: sub;width: 0.5em;" aria-hidden="true" data-prefix="fas" data-icon="comment-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt fa-w-16 fa-2x" ><path fill="orange" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z" class=""></path></svg><span class="orangetext pl5">3</span></p>
												</td>
												<td style="width:8%;padding-top:50px;">
													
													<?php  
													//echo "<pre>";
													//print_r($incoming_request[$i]['total_rating']);
													//echo "</pre>";
													$ra= $incoming_request[$i]['total_rating'][0]['grade'];
													
                                                    for ($j = 1; $j <= 5; $j++) {
														if ($j <= $ra) {
															echo '<i class="fa fa-star pinktext"></i>';
														} else {
															echo '<i class="fa fa-star greytext"></i>';
														}
													}
                                                                
													?>
													
												</td>
												<td style="padding-top:35px;">
													<p class="darkblacktext weight600" style="transform: rotate(90deg);">
														<svg style="width:0.9em;" aria-hidden="true" data-prefix="fal" data-icon="exchange" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-exchange fa-w-16 fa-2x"><path fill="#cccccc" d="M508.485 184.485l-92.485 92c-4.687 4.686-12.284 4.686-16.97 0l-7.071-7.07c-4.687-4.686-4.687-12.284 0-16.971L452.893 192H12c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h440.905l-60.946-60.444c-4.687-4.686-4.687-12.284 0-16.971l7.07-7.07c4.687-4.686 12.284-4.686 16.971 0l92.485 92c4.687 4.686 4.686 12.284 0 16.97zm-504.97 160l92.485 92c4.687 4.686 12.284 4.686 16.971 0l7.07-7.07c4.687-4.686 4.687-12.284 0-16.971L59.095 352H500c6.627 0 12-5.373 12-12v-8c0-6.627-5.373-12-12-12H59.107l60.934-60.444c4.687-4.686 4.687-12.284 0-16.971l-7.071-7.07c-4.686-4.686-12.284-4.687-16.97 0l-92.485 92c-4.686 4.686-4.687 12.284 0 16.97z" class=""></path></svg>
													</p>
												</td>
												<td style="padding-top:47px;">
													<p><i class="fa fa-trash trash"></i></p>
												</td>
											</tr> 
										<?php } ?>
											<!--<tr class="trborder">
												<td class=" orangebackground whitetext numbercss">2nd</td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5">Project 2</p>
													<p class=" orangetext weight600 textleft pl20 pb5">Revision in progress</p>
													<p class="darkblacktext textleft pl20">This is a test Description</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Designer</p>
													<p class="darkblacktext weight600">Rey</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Requested</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">In-Progress</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Due Date</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Messages</p>
													<p class="darkblacktext weight600"><i class="fa fa-envelope orangetext"></i><span class="orangetext pl5">3</span></p>
												</td>
												<td>
													<p class="darkblacktext weight600"><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i></p>
												</td>
												<td>
													<p class="darkblacktext weight600">
														<i class="fa fa-arrow-up"></i>
														<i class="fa fa-arrow-down"></i>
													</p>
												</td>
												<td>
													<p><i class="fa fa-trash trash"></i></p>
												</td>
											</tr>

											<tr class="trborder">
												<td class=" bluebackground whitetext numbercss">3rd</td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5">Project 3</p>
													<p class=" bluetext weight600 textleft pl20 pb5">Design in progress</p>
													<p class="darkblacktext textleft pl20">This is a test Description</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Designer</p>
													<p class="darkblacktext weight600">Rey</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Requested</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">In-Progress</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Due Date</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Messages</p>
													<p class="darkblacktext weight600"><i class="fa fa-envelope orangetext"></i><span class="orangetext pl5">3</span></p>
												</td>
												<td>
													<p class="darkblacktext weight600"><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i></p>
												</td>
												<td>
													<p class="darkblacktext weight600">
														<i class="fa fa-arrow-up"></i>
														<i class="fa fa-arrow-down"></i>
													</p>
												</td>
												<td>
													<p><i class="fa fa-trash trash"></i></p>
												</td>
											</tr>

											<tr class="trborder">
												<td class="greybackground numbercss">4th</td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5">Project 4</p>
													<p class=" pinktext weight600 textleft pl20 pb5">In-Queue</p>
													<p class="darkblacktext textleft pl20">This is a test Description</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Designer</p>
													<p class="darkblacktext weight600">Rey</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Requested</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">In-Progress</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Due Date</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Messages</p>
													<p class="darkblacktext weight600"><i class="fa fa-envelope orangetext"></i><span class="orangetext pl5">3</span></p>
												</td>
												<td>
													<p class="darkblacktext weight600"><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i></p>
												</td>
												<td>
													<p class="darkblacktext weight600">
														<i class="fa fa-arrow-up"></i>
														<i class="fa fa-arrow-down"></i>
													</p>
												</td>
												<td>
													<p><i class="fa fa-trash trash"></i></p>
												</td>
											</tr>
											
											<tr class="trborder">
												<td class="greybackground numbercss">5th</td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5">Project 5</p>
													<p class=" pinktext weight600 textleft pl20 pb5">In-Queue</p>
													<p class="darkblacktext textleft pl20">This is a test Description</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Designer</p>
													<p class="darkblacktext weight600">Rey</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Requested</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">In-Progress</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Due Date</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font16">Messages</p>
													<p class="darkblacktext weight600"><i class="fa fa-envelope orangetext"></i><span class="orangetext pl5">3</span></p>
												</td>
												<td>
													<p class="darkblacktext weight600"><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i><i class="fa fa-star greytext"></i></p>
												</td>
												<td>
													<p class="darkblacktext weight600">
														<i class="fa fa-arrow-up"></i>
														<i class="fa fa-arrow-down"></i>
													</p>
												</td>
												<td>
													<p><i class="fa fa-trash trash"></i></p>
												</td>
											</tr>-->
										</tbody>
									</table>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="ongoing_designs_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style="border:none;border-collapse:separate;">
                                        <!--<thead>
                                            <tr>
                                                 <th>Id</th>
                                                <th>Project Name</th>
												<th>Designer Name</th>
												<th>Requested</th>
												<th>In-Progress</th>
												<th>Due Date</th>
												<th>Messages</th>
												<th>Star</th>
												<th>Icon</th>
												<th>Delete</th>           
                                            </tr>
                                        </thead>-->
                                        <tbody>
                                           <?php for($i=0;$i<sizeof($ongoing_request);$i++){ ?>
										   <tr class="trborder">
												<td class="<?php if($i+1 > 3){ echo "greybackground"; } else{ echo "background_".ordinal($i+1); } ?> whitetext numbercss"><?php echo ordinal($i+1); ?></td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $ongoing_request[$i]['title']; ?></p>
													<p class="greentext weight600 textleft pl20 pb5">Waiting for approval</p>
													<p class="darkblacktext textleft pl20"><?php echo substr($ongoing_request[$i]['description'], 0 ,30);  ?></p>
												</td>
												<td style="padding-top:28px;">
													<p class="greytext pb0 weight600 font16">Designer</p>
													<p class="darkblacktext weight600"><?php if($ongoing_request[$i]['designer_first_name']){ echo $ongoing_request[$i]['designer_first_name']; }else{ echo "No Designer"; } ?></p>
												</td >
												<td style="padding-top:28px;"> 
													<p class="greytext pb0 weight600 font16">Requested</p>
													<p class="darkblacktext weight600"><?php echo date("d-m-Y",strtotime($ongoing_request[$i]['created'])); ?></p>
												</td>
												<td style="padding-top:28px;">
													<p class="greytext pb0 weight600 font16">In-Progress</p>
													<p class="darkblacktext weight600"><?php echo date("d-m-Y",strtotime($ongoing_request[$i]['dateinprogress'])); ?></p>
												</td>
												<td style="padding-top:28px;">
													<p class="greytext pb0 weight600 font16">Due Date</p>
													<?php 
													$duedate = "";
													
													if($userdata[0]['plan_turn_around_days']){
													if($ongoing_request[$i]['status_admin'] == "disapprove"){
														$ongoing_request[$i]['dateinprogress'] = date("Y-m-d ",strtotime($ongoing_request[$i]['dateinprogress']." +1 day"));
													}
														$date = date("Y-m-d",strtotime($ongoing_request[$i]['dateinprogress']));
														$duedate = date("m-d-Y",strtotime($date." ".$userdata[0]['plan_turn_around_days']." weekdays "));
													} ?>
													<p class="darkblacktext weight600"><?php echo $duedate; ?></p>
												</td>
												<td style="padding-top:28px;">
													<p class="greytext pb0 weight600 font16">Messages</p>
													<p class="darkblacktext weight600"><svg style="vertical-align: sub;width: 0.5em;" aria-hidden="true" data-prefix="fas" data-icon="comment-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt fa-w-16 fa-2x" ><path fill="orange" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z" class=""></path></svg><span class="orangetext pl5">3</span></p>
												</td>
												<td style="padding-top:40px;">
													<?php 
													//print_r($ongoing_request[$i]['rate'][0]['grade']);
													$rating = $ongoing_request[$i]['rate'][0]['grade'];
													for ($j = 1; $j <= 5; $j++) {
														if ($j <= $rating) {
															echo '<i class="fa fa-star pinktext"></i>';
														} else {
															echo '<i class="fa fa-star greytext"></i>';
														}
													}
                                                      
													?>
													
												</td>
												<td style="padding-top:25px;">
													<p class="darkblacktext weight600" style="transform: rotate(90deg);">
														<svg style="width:0.9em;" aria-hidden="true" data-prefix="fal" data-icon="exchange" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-exchange fa-w-16 fa-2x"><path fill="#cccccc" d="M508.485 184.485l-92.485 92c-4.687 4.686-12.284 4.686-16.97 0l-7.071-7.07c-4.687-4.686-4.687-12.284 0-16.971L452.893 192H12c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h440.905l-60.946-60.444c-4.687-4.686-4.687-12.284 0-16.971l7.07-7.07c4.687-4.686 12.284-4.686 16.971 0l92.485 92c4.687 4.686 4.686 12.284 0 16.97zm-504.97 160l92.485 92c4.687 4.686 12.284 4.686 16.971 0l7.07-7.07c4.687-4.686 4.687-12.284 0-16.971L59.095 352H500c6.627 0 12-5.373 12-12v-8c0-6.627-5.373-12-12-12H59.107l60.934-60.444c4.687-4.686 4.687-12.284 0-16.971l-7.071-7.07c-4.686-4.686-12.284-4.687-16.97 0l-92.485 92c-4.686 4.686-4.687 12.284 0 16.97z" class=""></path></svg>
													</p>
												</td>
												<td style="padding-top:37px;">
													<p><i class="fa fa-trash trash"></i></p>
												</td>
											</tr> 
										<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="pendingforapproverequest" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style="border:none;border-collapse:separate;">						
                                        <!--<thead>
                                            <tr>
                                                 <th>Id</th>
                                                <th>Project Name</th>
												<th>Designer Name</th>
												<th>Requested</th>
												<th>In-Progress</th>
												<th>Due Date</th>
												<th>Messages</th>
												<th>Star</th>
												<th>Icon</th>
												<th>Delete</th>
                                            </tr>
                                        </thead>-->
                                        <tbody>
                                           <?php for($i=0;$i<sizeof($pending_request);$i++){ ?>
										   <tr class="trborder">
												<td class="<?php if($i+1 > 3){ echo "greybackground"; } else{ echo "background_".ordinal($i+1); } ?> whitetext numbercss"><?php echo ordinal($i+1); ?></td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $pending_request[$i]['title']; ?></p>
													<p class="greentext weight600 textleft pl20 pb5">Waiting for approval</p>
													<p class="darkblacktext textleft pl20"><?php echo substr($pending_request[$i]['description'],0,30); ?></p>
												</td>
												<td style="padding-top:30px;">
													<p class="greytext pb0 weight600 font16">Designer</p>
													<p class="darkblacktext weight600"><?php if($pending_request[$i]['designer_first_name']){ echo $pending_request[$i]['designer_first_name']; }else{ echo "No Designer"; } ?></p>
												</td>
												<td style="padding-top:30px;">
													<p class="greytext pb0 weight600 font16">Requested</p>
													<p class="darkblacktext weight600"><?php echo date("d/m/y",strtotime($pending_request[$i]['created'])); ?></p>
												</td>
												<td style="padding-top:30px;">
													<p class="greytext pb0 weight600 font16">In-Progress</p>
													<p class="darkblacktext weight600"><?php echo date("d/m/y",strtotime($pending_request[$i]['dateinprogress'])); ?></p>
												</td>
												<td style="padding-top:30px;">
													<p class="greytext pb0 weight600 font16">Due Date</p>
													<?php 
													$duedate = "";
													
													if($userdata[0]['plan_turn_around_days']){
													if($pending_request[$i]['status_admin'] == "disapprove"){
														$pending_request[$i]['dateinprogress'] = date("Y-m-d",strtotime($pending_request[$i]['dateinprogress']." +1 day"));
													}
														$date = date("Y-m-d",strtotime($pending_request[$i]['dateinprogress']));
														
														$duedate = date("m-d-Y",strtotime($date." ".$userdata[0]['plan_turn_around_days']." weekdays "));
													} ?>
													<p class="darkblacktext weight600"><?php echo $duedate; ?></p>
												</td>
												<td style="padding-top:30px;">
													<p class="greytext pb0 weight600 font16">Messages</p>
													<p class="darkblacktext weight600"><svg style="vertical-align: sub;width: 0.5em;" aria-hidden="true" data-prefix="fas" data-icon="comment-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt fa-w-16 fa-2x" ><path fill="orange" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z" class=""></path></svg><span class="orangetext pl5">3</span></p>
												</td>
												<td style="width:8%;padding-top:52px">
													<?php 
													//print_r($pending_request[$i]['rating_star'][0]['grade']);
													$rate_star = $pending_request[$i]['rating_star'][0]['grade'];
													for ($j = 1; $j <= 5; $j++) {
														if ($j <= $rate_star) {
															echo '<i class="fa fa-star pinktext"></i>';
														} else {
															echo '<i class="fa fa-star greytext"></i>';
														}
													}
                                                      
													?>
													
												</td>
												<td style="padding-top:35px;">
													<p class="darkblacktext weight600" style="transform: rotate(90deg);">
														<svg style="width:0.9em;" aria-hidden="true" data-prefix="fal" data-icon="exchange" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-exchange fa-w-16 fa-2x"><path fill="#cccccc" d="M508.485 184.485l-92.485 92c-4.687 4.686-12.284 4.686-16.97 0l-7.071-7.07c-4.687-4.686-4.687-12.284 0-16.971L452.893 192H12c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h440.905l-60.946-60.444c-4.687-4.686-4.687-12.284 0-16.971l7.07-7.07c4.687-4.686 12.284-4.686 16.971 0l92.485 92c4.687 4.686 4.686 12.284 0 16.97zm-504.97 160l92.485 92c4.687 4.686 12.284 4.686 16.971 0l7.07-7.07c4.687-4.686 4.687-12.284 0-16.971L59.095 352H500c6.627 0 12-5.373 12-12v-8c0-6.627-5.373-12-12-12H59.107l60.934-60.444c4.687-4.686 4.687-12.284 0-16.971l-7.071-7.07c-4.686-4.686-12.284-4.687-16.97 0l-92.485 92c-4.686 4.686-4.687 12.284 0 16.97z" class=""></path></svg>
													</p>
												</td>
												<td style="padding-top:46px;">
													<p><i class="fa fa-trash trash"></i></p>
												</td>
											</tr> 
										<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
						

                        <div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table  table-hover dt-responsive" style="border:none;border-collapse:separate;">
                                        <!--<thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Project Name</th>
												<th>Designer Name</th>
												<th>Requested</th>
												<th>In-Progress</th>
												<th>Due Date</th>
												<th>Messages</th>
												<th>Star</th>
												<th>Icon</th>
												<th>Delete</th>
                                            </tr>
                                        </thead>-->
                                        <tbody>
                                             <?php for($i=0;$i<sizeof($approved_request);$i++){ ?>
										   <tr class="trborder">
												<td class="<?php if($i+1 > 3){ echo "greybackground"; } else{ echo "background_".ordinal($i+1); } ?> whitetext numbercss"><?php echo ordinal($i+1); ?></td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $approved_request[$i]['title']; ?></p>
													<p class="greentext weight600 textleft pl20 pb5">Waiting for approval</p>
													<p class="darkblacktext textleft pl20"><?php echo substr($approved_request[$i]['description'],0,30);?></p>
												</td>
												<td style="padding-top:25px">
													<p class="greytext pb0 weight600 font16">Designer</p>
													<p class="darkblacktext weight600"><?php if($approved_request[$i]['designer_first_name']){ echo $approved_request[$i]['designer_first_name']; }else{ echo "No Designer"; } ?></p>
												</td>
												<td style="padding-top:25px">
													<p class="greytext pb0 weight600 font16">Requested</p>
													<p class="darkblacktext weight600"><?php echo date("m-d-Y",strtotime($approved_request[$i]['created'])); ?></p>
												</td>
												<td style="padding-top:25px">
													<p class="greytext pb0 weight600 font16">In-Progress</p>
													<p class="darkblacktext weight600"><?php echo date("m-d-Y",strtotime($approved_request[$i]['dateinprogress'])); ?></p>
												</td>
												<td style="padding-top:25px">
													<p class="greytext pb0 weight600 font16">Due Date</p>
													<?php 
													$duedate = "";
													
													if($userdata[0]['plan_turn_around_days']){
													if($approved_request[$i]['status_admin'] == "disapprove"){
														$approved_request[$i]['dateinprogress'] = date("Y-m-d",strtotime($approved_request[$i]['dateinprogress']." +1 day"));
													}
														$date = date("Y-m-d",strtotime($approved_request[$i]['dateinprogress']));
														
														$duedate = date("d-m-Y",strtotime($date." ".$userdata[0]['plan_turn_around_days']." weekdays "));
													} ?>
													<p class="darkblacktext weight600"><?php echo $duedate; ?></p>
												</td>
												<td style="padding-top:25px">
													<p class="greytext pb0 weight600 font16">Messages</p>
													<p class="darkblacktext weight600"><svg style="vertical-align: sub;width: 0.5em;" aria-hidden="true" data-prefix="fas" data-icon="comment-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt fa-w-16 fa-2x" ><path fill="orange" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z" class=""></path></svg><span class="orangetext pl5">3</span></p>
												</td>
												<td style="padding-top:45px">
													<?php 
													//print_r($approved_request[$i]['all_rating_star'][0]['grade']);
													$rate_star_gred = $approved_request[$i]['all_rating_star'][0]['grade'];
													for ($j = 1; $j <= 5; $j++) {
														if ($j <= $rate_star_gred) {
															echo '<i class="fa fa-star pinktext"></i>';
														} else {
															echo '<i class="fa fa-star greytext"></i>';
														}
													}
                                                      
													?>
													
												</td>
												<td style="padding-top:25px">
													<p class="darkblacktext weight600" style="transform: rotate(90deg);">
														<svg style="width:0.9em;" aria-hidden="true" data-prefix="fal" data-icon="exchange" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-exchange fa-w-16 fa-2x"><path fill="#cccccc" d="M508.485 184.485l-92.485 92c-4.687 4.686-12.284 4.686-16.97 0l-7.071-7.07c-4.687-4.686-4.687-12.284 0-16.971L452.893 192H12c-6.627 0-12-5.373-12-12v-8c0-6.627 5.373-12 12-12h440.905l-60.946-60.444c-4.687-4.686-4.687-12.284 0-16.971l7.07-7.07c4.687-4.686 12.284-4.686 16.971 0l92.485 92c4.687 4.686 4.686 12.284 0 16.97zm-504.97 160l92.485 92c4.687 4.686 12.284 4.686 16.971 0l7.07-7.07c4.687-4.686 4.687-12.284 0-16.971L59.095 352H500c6.627 0 12-5.373 12-12v-8c0-6.627-5.373-12-12-12H59.107l60.934-60.444c4.687-4.686 4.687-12.284 0-16.971l-7.071-7.07c-4.686-4.686-12.284-4.687-16.97 0l-92.485 92c-4.686 4.686-4.687 12.284 0 16.97z" class=""></path></svg>
													</p>
												</td>
												<td style="padding-top:37px">
													<p><i class="fa fa-trash trash"></i></p>
												</td>
											</tr> 
										<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
</div>
<script>
$(document).ready(function () {
	$("#myInput").on("keyup", function () {
		var value = $(this).val().toLowerCase();
		$(".active table tr").filter(function () {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		});
	});
});
</script>