<h2 class="float-xs-left content-title-main">Project Name</h2>
</div>
<div class="col-md-12" style="background:white;">
    <div class="col-md-11 offset-md-1">
        <section id="content-wrapper">
            <style>
                /* padding css start */
                .pb0{ padding-bottom:0px; }
                .pb5{ padding-bottom:5px; }
                .pb10{ padding-bottom:10px; }
                .pt0{ padding-top:0px; }
                .pt5{ padding-top:5px; }
                .pt10{ padding-top:10px; }
                .pl0{ padding-left:0px; }
                .pl5{ padding-left:5px; }
                .pl10{ padding-left:10px; }
                /* padding css end */
                .greenbackground { background-color:#98d575; }
                .greentext { color:#98d575; }
                .orangebackground { background-color:#f7941f; }
                .pinkbackground { background-color: #ec4159; }
                .orangetext { color:#f7941f; }
                .bluebackground { background-color:#409ae8; }
                .bluetext{ color:#409ae8; }
                .whitetext { color:#fff !important; }
                .blacktext { color:#000; }
                .greytext { color:#cccccc; }
                .greybackground { background-color:#ededed; }
                .darkblacktext { color:#1a3147; } 
                .darkblackbackground { background-color:#1a3147; } 
                .lightdarkblacktext { color:#b4b9be; }
                .lightdarkblackbackground { background-color:#f4f4f4; }
                .pinktext { color: #ec4159; }
                .weight600 { font-weight:600; }
                .font18 { font-size:18px; }
                .textleft { text-align:left; }
                .textright { text-align:right; }
                .textcenter { text-align:center; }
                .pl20 { padding-left:20px; }

                .numbercss{
                    font-size: 31px !important;
                    padding: 8px 0px !important;
                    font-weight: 600 !important;
                    letter-spacing: -3px !important;
                }
                .projecttitle{
                    font-size: 25px;
                    padding-bottom: 0px;
                    text-align: left;
                    padding-left: 20px;
                }
                .trborder{
                    border: 1px solid #000;
                    background: unset;
                    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                }
                table {     border-spacing: 0 1em; }


                .trash{     
                    color: #ec4159;
                    font-size: 25px; 
                }
                .nav-tab-pills-image ul li .nav-link{
                    background:#ededed;
                    font-weight: 600;
                    color: #2f4458;
                }

                .nav-link:hover {
                    color: #fff !important;border-bottom: unset !important;
                }
                .ls0{ letter-spacing: 0px; }


                .slick-slide.slick-current.slick-center .portfolio-items-wrapper:before {
                    content: "";
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    background: none;
                }

                .slick-next {
                    left: 53%;
                    z-index: 1000;
                    height: auto;
                    width: auto;
                    top:110px;
                }

                .slick-prev {
                    left: 40%;
                    z-index: 1000;
                    height: auto;
                    width: auto;
                    top:110px;
                }

                .slick-next::before {
                    font-family: FontAwesome;
                    content: "\f105";
                    font-size: 22px;
                    background: #FFFFFF;
                    opacity: 1;
                    border-radius: 30px;
                    color: #1a3147;
                    background-color: #e4e4e4;
                    width: 30px;
                    height: 30px;
                    line-height: 28px;
                    text-align: center;
                    display: inline-block;
                }

                .slick-prev::before {
                    font-family: FontAwesome;
                    content: "\f104";
                    font-size: 22px;
                    opacity: 1;
                    border-radius: 30px;
                    color: #1a3147;
                    background-color: #e4e4e4;
                    width: 30px;
                    height: 30px;
                    line-height: 28px;
                    text-align: center;
                    display: inline-block;
                }
                .pagin-one {
                    color: #1a3147;
                    font-size: 16px;
                    font-weight: bold;
                    left: 46%;
                    position: absolute;
                    top: 100px;
                }
                .nav-tab-pills-image ul li .nav-link:hover {
                    border-bottom: 0px solid #2f4458 !important;
                }
                .chatbox {
                    display: block;
                    margin-bottom: 10px;
                    margin-top: 5px;
                    margin-right: 20px;
                }
                .chatbox2 .description 
                {
                    max-width: 243px;
                }
                .chat-main
                {
                    padding: 20px 0px 20px 0px;
                }
            </style>
            <div class="content">

                <div class="row">
                    <?php if ($this->session->flashdata('message_error') != '') { ?>				
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('message_success') != '') { ?>				
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                        </div>
                    <?php } ?>

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;">                
                        <div class="nav-tab-pills-image">
                            <ul class="nav nav-tabs" role="tablist" style="border-bottom:unset !important;">                      
                                <li class="nav-item active">
                                    <a class="nav-link" style="" href="<?php echo base_url() . "admin/request/view_request/" . $_GET['id']; ?>" role="tab">
                                        Back to Files
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <span class="pagin-one">1 of <?php echo sizeof($designer_file); ?></span>
                                            <div class="portfolio-items center">
                                                <?php
                                                for ($i = 0; $i < sizeof($designer_file); $i++) {
                                                    if ($designer_file[$i]['id'] != $main_id) {
                                                        continue;
                                                    }
                                                    $created_date_time = strtotime($designer_file[$i]['created']);

                                                    $now_date_time = strtotime(date("Y-m-d H:i:s"));
                                                    $span = "";
                                                    if (($now_date_time - $created_date_time) < 5000) {
                                                        $span = '<span style="background:#ec445c;padding:5px;color:#fff;font-weight:600;position: absolute;top: 0px;">NEW</span>';
                                                    }
                                                    ?>
                                                    <div class="col-md-12" style="">
                                                        <div class="col-md-12"  style="border-right:1px solid #1a3147;margin-top:15px;">
                                                            <div class="col-md-12" style="padding-bottom: 50px;">

                                                                <div class="col-md-6" style="padding:0px;">
                                                                    <h5 class="darkblacktext weight600 ls0"><?php echo $designer_file[$i]['src_file']; ?></h5>
                                                                    <p class="lightdarkblacktext" style="font-size:16px;"><?php echo number_format(filesize("./uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']) / 1048576, 2) . " Mb"; ?> | 
                                                                        <?php
                                                                        $date = strtotime($designer_file[$i]['created']); //Converted to a PHP date (a second count)
                                                                        //Calculate difference
                                                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
                                                                        $minutes = floor($diff / 60);
                                                                        $hours = floor($diff / 3600);
                                                                        if ($hours == 0) {
                                                                            echo $minutes . " Minutes Ago";
                                                                        } else {
                                                                            $days = floor($diff / (60 * 60 * 24));
                                                                            if ($days == 0) {
                                                                                echo $hours . " Hours Ago";
                                                                            } else {
                                                                                echo $days . " Days Ago";
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>
                                                                <div class="col-md-6" style="padding:0px;text-align: right;">
                                                                    <a href="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>" download><p class="darkblacktext weight600" style="font-size: 16px;margin-top: 10px;padding-bottom: 0px !important"><i class="fa fa-download" style="padding-right: 10px;"></i>  Download</p></a>
                                                                    <?php
                                                                    $crate = $designer_file[$i]['customer_grade'];
                                                                    ?>
                                                                    <p class="weight600 pinktext" style="font-size:18px;display:inline-block;">Rating </p>
                                                                    <p style="display:inline-block;">
                                                                        <?php
                                                                        for ($j = 1; $j <= 5; $j++) {
                                                                            if ($j <= $crate) {
                                                                                ?>
                                                                                <i class="fa fa-star pl5 pinktext fa-star"></i>
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <i class="fa fa-star pl5 greytext fa-star"></i>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>

                                                            </div>
                                                            <div  style="padding:10px;width:100%;height:450px;clear:both;background-color: #cccccc;">
                                                                <image src="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" style="max-height:430px;width:100%;" />
                                                            </div>
                                                            <!--<div class="col-md-12"  style="clear:both;">
                                                                <div class="col-md-9" style="padding: 10px;">
                                                                    <p class="font18 pinktext weight600">GRADE DESIGN:</p>
                                                            <?php for ($k = 1; $k <= 10; $k++) { ?>
                                                                <?php
                                                                if ($designer_file[$i]['grade'] >= $k) {
                                                                    $grade = "e52344";
                                                                } else {
                                                                    $grade = "969191";
                                                                }
                                                                if ($designer_file[$i]['grade'] != 0) {
                                                                    $gradeclick = "";
                                                                } else {
                                                                    $gradeclick = "gradeclick";
                                                                }
                                                                ?>
                                                                                        <span class="whitetext weight600 <?php echo $gradeclick; ?> currentgrade_<?php echo $k; ?>" style="background:#<?php echo $grade; ?>;cursor:pointer;border-radius:5px;padding: 8px 12px;margin: 3px;" data-id="<?php echo $designer_file[$i]['id']; ?>"><?php echo $k; ?></span>
                                                            <?php } ?>
                                                                </div>
                                                                <div class="col-md-3" style="padding: 10px;margin-top: 30px;text-align:right;">
                                                            <?php if ($designer_file[$i]['grade'] == 0) { ?>
                                                                                        <form class="form<?php echo $designer_file[$i]['id']; ?>" method="post" action="" style="display:inline-block;">
                                                                                            <input type="hidden" name="grade" value="" class="gradeclick<?php echo $designer_file[$i]['id']; ?>" />
                                                                                            <input type="hidden" name="id" value="<?php echo $designer_file[$i]['id']; ?>" class="" />
                                                                                            <!--<input type="submit" class="btn btn-primary" value="Submit"/>
                                                                                        </form>
                                                                                        <span class="whitetext weight600 currentgrade_main" style="background:#969191;cursor:pointer;border-radius:5px;padding: 16px 20px;font-size: 20px;">0/10</span>
                                                            <?php } else { ?>
                                                                                        <span class="whitetext weight600 " style="background:#e52344;cursor:pointer;border-radius:5px;padding: 16px 20px;font-size: 20px;"><?php echo $designer_file[$i]['grade']; ?>/10</span>
                                                            <?php } ?>
                                                                </div>
                                                            </div>-->
                                                        </div>
                                                    </div>
                                                <?php } ?>


                                                <?php
                                                for ($i = 0; $i < sizeof($designer_file); $i++) {
                                                    if ($designer_file[$i]['id'] == $main_id) {
                                                        continue;
                                                    }
                                                    $created_date_time = strtotime($designer_file[$i]['created']);

                                                    $now_date_time = strtotime(date("Y-m-d H:i:s"));
                                                    $span = "";
                                                    if (($now_date_time - $created_date_time) < 5000) {
                                                        $span = '<span style="background:#ec445c;padding:5px;color:#fff;font-weight:600;position: absolute;top: 0px;">NEW</span>';
                                                    }
                                                    ?>
                                                    <div class="col-md-12">
                                                        <div class="col-md-12"  style="border-right:1px solid #1a3147;margin-top:15px;">
                                                            <div class="col-md-12" style="padding-bottom: 50px;">

                                                                <div class="col-md-6" style="padding:0px;">
                                                                    <h5 class="darkblacktext weight600 ls0"><?php echo $designer_file[$i]['src_file']; ?></h5>
                                                                    <p class="lightdarkblacktext" style="font-size:16px;"><?php echo number_format(filesize("./uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']) / 1048576, 2) . " Mb"; ?> | 
                                                                        <?php
                                                                        $date = strtotime($designer_file[$i]['created']); //Converted to a PHP date (a second count)
                                                                        //Calculate difference
                                                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
                                                                        $minutes = floor($diff / 60);
                                                                        $hours = floor($diff / 3600);
                                                                        if ($hours == 0) {
                                                                            echo $minutes . " Minutes Ago";
                                                                        } else {
                                                                            $days = floor($diff / (60 * 60 * 24));
                                                                            if ($days == 0) {
                                                                                echo $hours . " Hours Ago";
                                                                            } else {
                                                                                echo $days . " Days Ago";
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>
                                                                <div class="col-md-6" style="padding:0px;text-align: right;">
                                                                    <a href="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>" download><p class="darkblacktext weight600" style="font-size: 16px;margin-top: 10px;padding-bottom: 0px !important"><i class="fa fa-download" style="padding-right: 10px;"></i>  Download</p></a>
                                                                    <?php
                                                                    $crate = $designer_file[$i]['customer_grade'];
                                                                    ?>
                                                                    <p class="weight600 pinktext" style="font-size:18px;display:inline-block;">Rating </p>
                                                                    <p style="display:inline-block;">
                                                                        <?php
                                                                        for ($j = 1; $j <= 5; $j++) {
                                                                            if ($j <= $crate) {
                                                                                ?>
                                                                                <i class="fa fa-star pl5 pinktext fa-star"></i>
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <i class="fa fa-star pl5 greytext fa-star"></i>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>

                                                            </div>
                                                            <div  style="padding:10px;width:100%;height:450px;clear:both;background-color: #cccccc;">
                                                                <image src="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" style="max-height:430px;width:100%;" />
                                                            </div>
                                                            <!--<div class="col-md-12"  style="clear:both;">
                                                                <div class="col-md-9" style="padding: 10px;">
                                                                    <p class="font18 pinktext weight600">GRADE DESIGN:</p>
                                                            <?php for ($k = 1; $k <= 10; $k++) { ?>
                                                                <?php
                                                                if ($designer_file[$i]['grade'] >= $k) {
                                                                    $grade = "e52344";
                                                                } else {
                                                                    $grade = "969191";
                                                                }
                                                                if ($designer_file[$i]['grade'] != 0) {
                                                                    $gradeclick = "";
                                                                } else {
                                                                    $gradeclick = "gradeclick";
                                                                }
                                                                ?>
                                                                                        <span class="whitetext weight600 <?php echo $gradeclick; ?> currentgrade_<?php echo $k; ?>" style="background:#<?php echo $grade; ?>;cursor:pointer;border-radius:5px;padding: 8px 12px;margin: 3px;" data-id="<?php echo $designer_file[$i]['id']; ?>"><?php echo $k; ?></span>
                                                            <?php } ?>
                                                                </div>
                                                                <div class="col-md-3" style="padding: 10px;margin-top: 30px;text-align:right;">
                                                            <?php if ($designer_file[$i]['grade'] == 0) { ?>
                                                                                        <form class="form<?php echo $designer_file[$i]['id']; ?>" method="post" action="" style="display:inline-block;">
                                                                                            <input type="hidden" name="grade" value="" class="gradeclick<?php echo $designer_file[$i]['id']; ?>" />
                                                                                            <input type="hidden" name="id" value="<?php echo $designer_file[$i]['id']; ?>" class="" />
                                                                                            <!--<input type="submit" class="btn btn-primary" value="Submit"/>
                                                                                        </form>
                                                                                        <span class="whitetext weight600 currentgrade_main" style="background:#969191;cursor:pointer;border-radius:5px;padding: 16px 20px;font-size: 20px;">0/10</span>
                                                            <?php } else { ?>
                                                                                        <span class="whitetext weight600 " style="background:#e52344;cursor:pointer;border-radius:5px;padding: 16px 20px;font-size: 20px;"><?php echo $designer_file[$i]['grade']; ?>/10</span>
                                                            <?php } ?>
                                                                </div>
                                                            </div>-->
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="padding: 0px;">

                                            <div class="col-md-12 chat-main" style="padding-bottom:30px;margin-top:20px;">

                                                <h4 class="darkblacktext weight600 ls0">Messages</h4>
                                                <p class="orangetext weight600 font18"><i class="fa fa-envelope" style="margin-right:10px;"></i>Notes for New.png</p>

                                                <div class="myscroller messagediv_<?php echo $_GET['id']; ?>" style="padding:0px; overflow-y: auto; height:430px; margin-bottom:20px;">
                                                    <?php for ($j = 0; $j < sizeof($chat_request); $j++) { ?>
                                                        <?php if ($chat_request[$j]['sender_type'] == "designer") { ?>

                                                            <div class="chatbox2">

                                                                <div class="message-text-wrapper">
                                                                    <p class="title"><?php
                                                                        if ($j != 0) {
                                                                            if ($chat_request[$j]['sender_type'] != $chat_request[$j - 1]['sender_type']) {
                                                                                echo $data[0]['designer_name'];
                                                                            }
                                                                        } else {
                                                                            echo $data[0]['designer_name'];
                                                                        }
                                                                        ?>
                                                                    </p>

                                                                    <p class="description"><?php echo $chat_request[$j]['message']; ?></p>

                                                                    <p class="posttime">
                                                                        <?php
                                                                        $date = strtotime($chat_request[$j]['created']); //Converted to a PHP date (a second count)
                                                                        //Calculate difference
                                                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
                                                                        $minutes = floor($diff / 60);
                                                                        $hours = floor($diff / 3600);
                                                                        if ($hours == 0) {
                                                                            echo $minutes . " Minutes Ago";
                                                                        } else {
                                                                            $days = floor($diff / (60 * 60 * 24));
                                                                            if ($days == 0) {
                                                                                echo $hours . " Hours Ago";
                                                                            } else {
                                                                                echo $days . " Days Ago";
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </p>

                                                                </div>
                                                            </div>
                                                        <?php } else { ?>

                                                            <div class="chatbox">

                                                                <div class="avatar">
                                                                    <?php
                                                                    if ($data[0]['designer_image']) {
                                                                        echo '<img src="' . base_url() . 'uploads/profile_picture/' . $data[0]['designer_image'] . '" class="img-circle">';
                                                                    } else {
                                                                        ?>
                                                                        <div class="myprofilebackground" style="width:40px; height:40px; line-height: 40px; border-radius:50%; background: #0190ff; text-align: center; font-size: 15px; color: #fff;">
                                                                            <?php echo $data[0]['designer_sortname']; ?>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    //<img class="img-circle avatar" alt="chat avatar" src="https://bootdey.com/img/Content/avatar/avatar1.png">
                                                                    ?>
                                                                </div>

                                                                <div class="message-text-wrapper">

                                                                    <p class="title">
                                                                        <?php
                                                                        if ($j != 0) {
                                                                            if ($chat_request[$j]['sender_type'] != $chat_request[$j - 1]['sender_type']) {
                                                                                if ($chat_request[$j]['sender_type'] == "designer") {
                                                                                    echo $data[0]['designer_name'];
                                                                                } elseif ($chat_request[$j]['sender_type'] == "admin") {
                                                                                    echo "Admin";
                                                                                } elseif ($chat_request[$j]['sender_type'] == "qa") {
                                                                                    echo "QA";
                                                                                } elseif ($chat_request[$j]['sender_type'] == "va") {
                                                                                    echo "VA";
                                                                                }
                                                                            }
                                                                        } else {
                                                                            if ($chat_request[$j]['sender_type'] == "designer") {
                                                                                echo $data[0]['designer_name'];
                                                                            } elseif ($chat_request[$j]['sender_type'] == "admin") {
                                                                                echo "Admin";
                                                                            } elseif ($chat_request[$j]['sender_type'] == "qa") {
                                                                                echo "QA";
                                                                            } elseif ($chat_request[$j]['sender_type'] == "va") {
                                                                                echo "VA";
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                    <p class="description">
                                                                        <?php echo $chat_request[$j]['message']; ?>
                                                                    </p>
                                                                    <p class="posttime">
                                                                        <?php
                                                                        $date = strtotime($chat_request[$j]['created']); //Converted to a PHP date (a second count)
                                                                        //Calculate difference
                                                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
                                                                        $minutes = floor($diff / 60);
                                                                        $hours = floor($diff / 3600);
                                                                        if ($hours == 0) {
                                                                            echo $minutes . " Minutes Ago";
                                                                        } else {
                                                                            $days = floor($diff / (60 * 60 * 24));
                                                                            if ($days == 0) {
                                                                                echo $hours . " Hours Ago";
                                                                            } else {
                                                                                echo $days . " Days Ago";
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>

                                                </div>

                                                <input type='text' class='form-control text_<?php echo $_GET['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px;height: 44px;" placeholder="Reply To Client" />
                                                <span class="chatsendbtn send send_request_img_chat" style="float: right;margin-right: 6px;margin-top: -50px;position: relative;z-index: 2;color: white;"  
                                                      data-fileid="<?php echo $request[0]['id']; ?>" 
                                                      data-senderrole="qa" 
                                                      data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                                                      data-receiverid="<?php echo $request[0]['customer_id']; ?>" 
                                                      data-receiverrole="customer">Send
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- approve Modal -->
            <div id="approvemodal" class="modal fade" role="dialog" style="margin-top:10%;">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-body " style="background-color:#6ad154;text-align: center;font-size: 170px;">
                            <i class="fa fa-check whitetext"></i>
                            <h3 style="letter-spacing: -1px;font-size: 21px;" class="whitetext weight600">DESIGN APPROVED!</h3>
                        </div>
                        <div class="modal-footer" style="text-align:center;">
                            <h4 class="pinktext weight600" style="font-size:16px;letter-spacing: -1px;">RATE YOUR DESIGN</h4>
                            <p style="font-size:35px;">
                                <i class="fa fa-star pl5 greytext"></i>
                                <i class="fa fa-star pl5 greytext"></i>
                                <i class="fa fa-star pl5 greytext"></i>
                                <i class="fa fa-star pl5 greytext"></i>
                                <i class="fa fa-star pl5 greytext"></i>
                            </p>
                            <p style="padding-left:10%;padding-right:10%;margin-top:35px;">
                                Click "Download' button to access source files and hi-resolution version of your design.
                            </p>
                            <input type="button" class="btn whitetext weight600" style="background:#6ad154;padding:15px;border-radius: 5px;" value="DOWNLOAD SOURCE FILES" />
                            <p style="color:#6ad154;cursor:pointer;"  data-dismiss="modal">Thanks, I'll download this later.</p>
                            <p style="margin-top:25px;">Please be notified that all source files will be removed after 14 days upon approval</p>

                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
    <script>
        $(document).ready(function () {
            var total_design = <?php echo sizeof($designer_file); ?>;
            var current_design = 1;
            $(".slick-next").click(function () {
                current_design += 1;
                if (current_design > total_design) {
                    current_design = 1;
                }
                $(".pagin-one").html(current_design + " of " + total_design);
            });

            $(".slick-prev").click(function () {
                current_design -= 1;
                if (current_design == 0) {
                    current_design = total_design;
                }
                $(".pagin-one").html(current_design + " of " + total_design);
            });



            $(".myapprovestar").click(function () {
                $(".myapprovestar").removeClass("pinktext");
                for (var i = 1; i <= $(this).attr("data-id"); i++) {
                    $(".myapprovestar" + i).addClass("pinktext");
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>customer/request/designer_rating_ajax",
                    data: {"fileid": $("#myapprovedid").val(),
                        "value": $(this).attr("data-id")},
                    success: function (data) {
                        //alert("---"+data);
                        //alert("Settings has been updated successfully.");
                        if (data == 1) {
                            if (value == "Approve") {
                                //$('.approve_reject_div'+fileid).html('<p class="pinktext font18">Design is Approved Succesfully..!</p>');
                                $("#myapprovedid").val(fileid);
                                $('.approve_reject_new_div').html('Design is Approve Succesfully..!')
                                $('.design_download').attr('href', file);
                                $('#approvemodal').modal('show');
                            } else {
                                //$('.approve_reject_div'+fileid).html('<p class="darkblacktext font18">Design is Reject Succesfully..!</p>');
                                $('.approve_reject_new_div').html('Design is Reject Succesfully..!')
                            }
                        }
                    }
                });
                event.preventDefault();
            });
            $('.text_write').keypress(function (e)
            {
                if (e.which == 13) {
                    //$('.send_text').click();
                    //return false;  
                }
            });

        });
    </script>      
    <style>
        .chatbox .description{ width:180px; }
    </style> 






