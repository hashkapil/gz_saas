<div class="cli-ent-row tr brdr">
    <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
    <div class="cli-ent-col td flex-uses" style="width: 30%;">
        <figure class="cli-ent-img circle one">
            <img src="<?php echo $designers['profile_picture']; ?>" class="img-responsive one">
        </figure>
        <div class="cli-ent-xbox text-left">
            <h3 class="pro-head space-b">
                <!--<a href="<?php //echo base_url(); ?>admin/dashboard?designer_id=<?php //echo $designers['id']; ?>">-->
                    <?php echo ucwords($designers['first_name'] . " " . $designers['last_name']); ?>
                <!--</a>-->
            </h3>
            <p class="pro-b"><?php echo $designers['email']; ?></p>
            <p class="pro-b"><?php echo $designers['about_me']; ?></p>
        </div>
    </div>

    <div class="cli-ent-col td" style="width: 10%;">
        <div class="cli-ent-xbox">
            <p class="pro-b"><?php echo $designers['created']; ?></p>
        </div>
    </div>

    <div class="cli-ent-col td" style="width: 15%;">
        <div class="cli-ent-xbox text-center">
            <span>
                <?php echo $designers['pending_requests']; ?>
            </span>
        </div>
    </div>


    <div class="cli-ent-col td" style="width: 15%;">
        <div class="cli-ent-xbox text-center">
            <span>
                <?php echo $designers['active_requests']; ?>
            </span>
        </div>
    </div> 
    <div class="cli-ent-col td  text-center" style="width: 15%;" >
        <div class="notify-lines">
            <div class="switch-custom-usersetting-check en_dis_designer customtoggledesign" data-designerID="<?php echo $designers['id']; ?>">
                <span class="checkstatus"></span>
                <input class="ena_dis_<?php echo $designers['id']; ?>" type="checkbox" id="switch_<?php echo $designers['id']; ?>" <?php echo ($designers['is_active'] == '1')?'checked':'';?>>
                <label for="switch_<?php echo $designers['id']; ?>"></label> 
            </div>
        </div>
    </div>
    <div class="cli-ent-col td text-center" style="width: 15%;" >
        <div class="cli-ent-xbox action-per">
            <a href="<?php echo base_url(); ?>admin/dashboard?designer_id=<?php echo $designers['id']; ?>" title="View Projects"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/list_icon.svg" /></a>
            <a href="<?php echo base_url(); ?>admin/accounts/view_designer_profile/<?php echo $designers['id']; ?>" title="Edit Designer">
                <i class="icon-gz_edit_icon"></i>
            </a>
            <a title="delete" id="delete<?php echo $designers['id']; ?>" href="#" data-toggle="modal" class="confirmationdelete" data-target="#confirmationdelete" data-designerid="<?php echo $designers['id']; ?>">
                <i class="icon-gz_delete_icon"></i>
            </a>
        </div>
    </div>
</div> 