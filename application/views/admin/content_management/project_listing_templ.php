<?php //echo "<pre/>";print_R($projects);?>
<div class="cli-ent-row tr brdr <?php //if($projects['plan_color'] != ""){ echo "plancolor_".$projects['plan_color']; } ?>" id="remove_<?php echo $projects['id']; ?>">
    <!---Start Title---->
    <div class="cli-ent-col td client-project" style="width: 25%; cursor: pointer;">
        <div class="cli-ent-xbox text-left">
            <div class="cell-row">
                <div class="cell-col" >
                    <div class="name-disc">
                        <h3 class="pro-head space-b"><a href="<?php echo base_url().'admin/dashboard/view_request/'.$projects['id']; ?>"><?php echo $projects['title']; ?></a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---End Title---->

<!---Start Status---->
<div class="cli-ent-col td" style="width: 20%; cursor: pointer;">
        <div class="cell-col col-w1">
            <p class="neft"><span class="red <?php echo $projects['color']; ?> text-uppercase text-wrap"><?php echo $projects['status'];?></span></p>
        </div>
</div>
<!---End Status---->

<!---Start Status---->
<div class="cli-ent-col td" style="width: 20%; cursor: pointer;">
    <div class="cell-col col-w1">
        <?php echo (isset($projects['count_revision']) && $projects['count_revision'] != '') ? $projects['count_revision'] : 'N/A';?>
    </div>
</div>
<!---End Status---->

<!----Date tab start-->
<div class="cli-ent-col td" style="width: 25%;">
    <div class="cli-ent-xbox">
        <div class="dt-expire">
            <p id="demo_<?php echo $projects['id']; ?>" class="expectd_date" data-id="<?php echo $projects['id']; ?>" data-date="<?php echo date('M d, Y h:i:A', strtotime($projects['datetime'])); ?>" data-timezonedate="<?php echo $projects['usertimezone_date']; ?>"></p>
<!--            <a href="javascript:void(0)" class="editexpected" date-expected="<?php echo date('M d, Y h:i A', strtotime($projects['expected'])); ?>" date-reqid="<?php echo $projects['id']; ?>">
                <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/gz_icons/calebdar_edit.svg" />
            </a>-->
        </div>
        <?php echo $projects['deliverdate']; ?>
    </div>
</div>
<!----Date tab end-->

</div>



