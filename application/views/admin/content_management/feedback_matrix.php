
<?php $is_past_designer = array_search(0, array_column($designer_info, 'is_active'));
$is_past_cus = array_search(0, array_column($customer_info, 'is_active'));
// echo "<pre>"; print_r($designer_info); exit; ?> 
<link rel="stylesheet" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/admin/rangePicker.css');?>">
<section class="con-b">
    <div class="container-fluid">

        <div class="matrix-section feedback-matrix-rate">
          <div class="btn-matrix">
                    <a href="<?php echo base_url();?>admin/Contentmanagement/rating_details">Rating detail</a>
                    <a href="<?php echo base_url();?>admin/Contentmanagement/overall_rating">Overall Rating</a>
            </div>
            <div class="white-boundries">
                <div class="feedback-analysis">
                    <div class="rating-detailss">

                      <div class="custom-range-picker">
                        <div id="range1">
                         <input id="startrange" name="startrange" style="display: none;">
                       </div>
                       <span> Vs.</span>
                       <div id="range2">
                        <input id="endrange" name="endrange" style="display: none;">
                      </div>
                    </div>
                        
                    </div>
                    <h2 class="main-info-heading">Customer Feedback Analysis</h2>

                     <div class="row" id="TopNavForComapare" style="display: none; ">
                        
                       
                        <div class="col-md-4">
                          <div class="compare-row overall-compare">
                            <h2> <strong> Overall rating </strong> </h2>
                            <div class="compare_items"><h3>Customer Review</h3>
                              <span class="comare-rat" id="overall-custReview">0</span>
                            </div>
                            <div class="compare_items"><h3>Likes us</h3> 
                              <?php foreach ($Like as $ky => $like) {
                                     $sumOFAVg +=$like['average']*$like['total_design'];
                                     $total_design += $like['total_design']; 
                                  } 
                                    $avgOfLike = $sumOFAVg/$total_design; 
                                  ?>
                                  <span class="comare-rat"><?php echo count($Like);?></span></div>
                            <div class="compare_items"><h3>Dislike us</h3> 
                              <?php foreach ($dislIke as $ky => $dislike) {
                                     $sumOFAVgdislike +=$dislike['average']*$dislike['total_design'];
                                     $total_designdislike += $dislike['total_design']; 
                                  } ?>
                                  <span class="comare-rat"><?php echo count($dislIke);?></span></div>
                            <div class="compare_items">
                              <h3>Last 5 Ratings</h3>
                              <span class="comare-rat" id="overall-custReviewlast5">0</span></div>
                          </div>
                        </div>
                         <div class="col-md-4 " >
                            
                          <div class="compare-row left-compare">
                             
                          </div>
                        </div>
                        <div class="col-md-4">
                         
                          <div class="compare-row right-compare">
                             
                            
                          </div>
                        </div>
                       
                    </div>
                    <div class="row" id="TopNavDefault">
                        <div class="col-md-3">
                            <div class="analysis-item">
                                <h3>customer review</h3>
                                <div class="analysis-info" id="cust_review">
                                    <span >0</span>
                                    <img src="">
                                     
                                </div>
                            </div>
                        </div>

                       <div class="col-md-3">
                            <div class="analysis-item">
                                <h3> customers Love us</h3>
                                <div class="analysis-info ">
                                  <?php foreach ($Like as $ky => $like) {
                                     $sumOFAVg +=$like['average']*$like['total_design'];
                                     $total_design += $like['total_design']; 
                                  } 
                                    $avgOfLike = $sumOFAVg/$total_design; 
                                  ?>
                                  <span><?php echo count($Like);?></span>
                                     
                               
                                </div>
                            </div>
                        </div> 

                        <div class="col-md-3">
                            <div class="analysis-item">
                                <h3> customers dislike us</h3>
                                <div class="analysis-info ">
                                  <?php foreach ($dislIke as $ky => $dislike) {
                                     $sumOFAVgdislike +=$dislike['average']*$dislike['total_design'];
                                     $total_designdislike += $dislike['total_design']; 
                                  } ?>
                                  <span><?php echo count($dislIke);?></span>
                                     
                                </div>
                            </div>
                        </div>  

                         <div class="col-md-3">
                           <div class="analysis-item">
                               <h3>  last 5 designs </h3>
                               <div class="analysis-info " id="last5des">
                                 <!-- <span><?php echo $last5Avg['lastAvg'];?></span>
                                   <img src="<?php echo base_url();?>public/assets/img/customer/<?php echo $last5Avg["svg"];?>"> -->
                                   <span>0</span>
                                   <img src="">
                              
                               </div>
                           </div>
                       </div> 
                       
                    </div>
                </div>
            </div>
            
          
            <div class="all-feedback-colmn">
                <div class="row">
                    <div class="col-md-6">
                        <div class="rating-column">
                            <div class="feedback-item">
                            <div class="feedback-head">
                                <h2>Rating by Designer</h2>
                            </div>
                            <table class="feedback-col-table">
                                <thead>
                                    <tr>
                                        <th>Designer Name</th>
                                        <th>No of Drafts</th>
                                        <th>Avg. Rating</th>
                                        <th>Last 15 Design Ratings</th>
                                    </tr>
                                </thead>
                               <tbody style="max-height:626px;height:626px;">
                                   <tr class="cstm-desigr-rating actv_dsgnr"><th colspan="4">Current Designers</th><td><table><tbody>
                                   <?php
                                   foreach ($designer_info as $ky => $d_info) {
                                       if ($d_info['is_active'] == 1) {
                                           ?>
                                       <tr>
                                           <td><?php echo $d_info['name']; ?></td>
                                           <td><?php echo $d_info['total_design']; ?></td>
                                           <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $d_info["svg"]; ?>"> <?php echo $d_info['total_avg']; ?></td>
                                           <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $d_info['lastsvg']; ?>"> <?php echo $d_info['lastAvg']['avg']; ?></td>

                                       </tr>

                                       <?php
                                       $actvdis_totaldrafts += $d_info['total_design'];
                                       $actvdis_totalOfAvgRating += $d_info['total_avg'];
                                       $actvdis_totalavgRatingD += $d_info['total_design'] * $d_info['total_avg'];

                                       $actvdis_toal15Design += $d_info['lastAvg']['Avgtotal_design'];
                                       $actvdis_AvgWeighted += $d_info['lastAvg']['Avgtotal_design'] * $d_info['lastAvg']['avg'];

                                       $actvdis_totalAvgOF15 = $actvdis_AvgWeighted / $actvdis_toal15Design;
                                   }
                                }
                                ?>
                            </tbody>
                              <tfoot>
                                    <tr>
                                         
                                        <td><strong>Total</strong></td>
                                        <td><strong><?php echo $actvdis_totaldrafts; ?> </strong></td>
                                        <td id="totalAvgRating"><strong><?php echo number_format($actvdis_totalavgRatingD/$actvdis_totaldrafts,2,'.',''); ?></strong></td>
                                        <td><strong><?php echo number_format($actvdis_totalAvgOF15,2,'.',''); ?></strong></td>
                                         
                                    </tr>
                                </tfoot></table></td></tr>
                                   <?php if($is_past_designer){ ?>
                                   
                                   <tr class="cstm-desigr-rating dectv_dsgnr"><th class="accord-outer" data-toggle="collapse" colspan="4" data-target="#past_designers">Past Designers</th><td class="accordion-step collapse" id="past_designers"><table><tbody>
                                   <?php 
                                   foreach ($designer_info as $ky => $d_info) {
                                       if ($d_info['is_active'] == 0) {
                                           ?>
                                       <tr>
                                           <td><?php echo $d_info['name']; ?></td>
                                           <td><?php echo $d_info['total_design']; ?></td>
                                           <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $d_info["svg"]; ?>"> <?php echo $d_info['total_avg']; ?></td>
                                           <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $d_info['lastsvg']; ?>"> <?php echo $d_info['lastAvg']['avg']; ?></td>

                                       </tr>

                                       <?php
                                       $pastde_totaldrafts += $d_info['total_design'];
                                       $pastde_totalOfAvgRating += $d_info['total_avg'];
                                       $pastde_totalavgRatingD += $d_info['total_design'] * $d_info['total_avg'];

                                       $pastde_toal15Design += $d_info['lastAvg']['Avgtotal_design'];
                                       $pastde_AvgWeighted += $d_info['lastAvg']['Avgtotal_design'] * $d_info['lastAvg']['avg'];

                                       $pastde_totalAvgOF15 = $pastde_AvgWeighted / $pastde_toal15Design;
                                   }
                                } ?>
                            </tbody>
                                <tfoot>
                                    <tr>
                                         
                                        <td><strong>Total</strong></td>
                                        <td><strong><?php echo $pastde_totaldrafts; ?> </strong></td>
                                        <td id="totalAvgRating"><strong><?php echo number_format($pastde_totalavgRatingD/$pastde_totaldrafts,2,'.',''); ?></strong></td>
                                        <td><strong><?php echo number_format($pastde_totalAvgOF15,2,'.',''); ?></strong></td>
                                         
                                    </tr>
                                </tfoot>
                                </table></td></tr>
                            <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                         <?php 
                                              $final_drafts = $actvdis_totaldrafts+$pastde_totaldrafts;
                                              $final_totalavgRatingD = $actvdis_totalavgRatingD+$pastde_totalavgRatingD;
                                              $final_toal15Design += $actvdis_toal15Design+$pastde_toal15Design;
                                              $final_AvgWeighted += $pastde_AvgWeighted+$actvdis_AvgWeighted;
                                              $final_15avgrating = $final_AvgWeighted/$final_toal15Design;
                                          ?>
                                        <td><strong>Total</strong></td>
                                        <td><strong><?php echo $final_drafts; ?> </strong></td>
                                        <td id="totalAvgRating"><strong><?php echo number_format($final_totalavgRatingD/$final_drafts,2,'.',''); ?></strong></td>
                                        <td><strong><?php echo number_format($final_15avgrating,2,'.',''); ?></strong></td>
                                         
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="rating-column">
                            <div class="feedback-item">
                            <div class="feedback-head">
                                <h2>Rating by QA</h2>
                            
                            </div>
                            <table class="feedback-col-table">
                                <thead>
                                    <tr>
                                        <th>Qa Name</th>
                                        <th>No of Drafts</th>
                                        <th>Avg. Rating</th>
                                        <th>Last 50 Design Ratings</th>
                                    </tr>
                                </thead>
                                <tbody style="max-height:270px;height:270px;">
                                   <?php 
                                     foreach ($qa_info as $key => $q_info) { 
                                    

                                      ?> 
                                 
                                    <tr>
                                        <td><?php echo $q_info['name']; ?></td>
                                        <td><?php echo $q_info['total_design'];?></td>
                                        <td><img src="<?php echo base_url();?>public/assets/img/customer/<?php echo $q_info["svg"];?>"> <?php echo $q_info['total_avg']; ?></td>
                                        <td><img src="<?php echo base_url();?>public/assets/img/customer/<?php echo $q_info["lastsvg"];?>"><?php echo $q_info['lastAvg']['avg']; ?></td>

                                    </tr>
                                <?php 
                                //echo "<pre>"; print_r($q_info['lastAvg']); 
                                $Qatotaldrafts+= $q_info['total_design'];
                                $QatotalOfAvgRating+= $q_info['total_avg'];
                                $totalavgRatingqa += $q_info['total_design'] * $q_info['total_avg'];
                                $toal15Designqa +=  $q_info['lastAvg']['Avgtotal_design'];
                                $AvgWeightedqa +=  $q_info['lastAvg']['Avgtotal_design'] * $q_info['lastAvg']['avg'];

                                $totalAvgOF15qa = $AvgWeightedqa/$toal15Designqa;

                              } ?>
                                
                     
                                </tbody>
                               <tfoot>
                                    <tr>
                                        
                                        <td><strong>Total</strong></td>
                                        <td><strong><?php echo $Qatotaldrafts; ?> </strong></td>
                                        <td id="totalAvgRatingqa"><strong><?php echo number_format($totalavgRatingqa/$Qatotaldrafts,2,'.',''); ?></strong></td>
                                        <td><strong><?php echo number_format($totalAvgOF15qa,2,'.',''); ?> </strong></td>
                                         
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        </div>

                        <!--  rating by Plan name start  -->

                        <div class="rating-column">
                            <div class="feedback-item">
                            <div class="feedback-head">
                                <h2>Rating by Plan</h2>
                            
                            </div>
                            <table class="feedback-col-table">
                                <thead>
                                    <tr>
                                        <th>Plan Name</th>
                                        <th>No of Designs</th>
                                        <th>Avg. Rating</th> 
                                    </tr>
                                </thead>
                                <tbody style="max-height:200px;height:200px;">
                                   <?php 
                                     foreach ($ratingByplanName as $key => $plan_rating) { 
                                      $plnsvg =  $this->myfunctions->getfacesaccavg($plan_rating['avg_rating']);
                                    ?> 
                                 
                                    <tr>
                                        <td><?php echo $plan_rating['plan_name']; ?></td>
                                        <td><?php echo $plan_rating['total_designs'];?></td>
                                        <td><img src="<?php echo base_url();?>public/assets/img/customer/<?php echo $plnsvg;?>"> <?php echo $plan_rating['avg_rating']; ?></td>
                                    </tr>
                                <?php 
                                $Prtotaldrafts+= $plan_rating['total_designs'];
                                //$PrtotalOfAvgRating+= $plan_rating['avg_rating'];
                                $totalavgRatingPr += $plan_rating['total_designs'] * $plan_rating['avg_rating'];
                                 } ?>
                                
                     
                                </tbody>
                               <tfoot>
                                    <tr>
                                        
                                        <td><strong>Total</strong></td>
                                        <td><strong><?php echo $Prtotaldrafts; ?> </strong></td>
                                        <td id="totalAvgRatingc"><strong><?php echo number_format($totalavgRatingPr/$Prtotaldrafts,2,'.',''); ?></strong></td>
                                       
                                         
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                        <!--  rating by Plan name end  -->
                    </div>
                </div>
                <div class="row">
                   <div class="col-md-12">
                        <div class="feedback-item">
                            <div class="feedback-head">
                                <h2>Rating by Customer</h2>
                                
                            </div>
                            <table class="feedback-col-table">
                                <thead>
                                    <tr>
                                        <th>Client Name</th>
                                        <th>No of Drafts</th>
                                        <th>Avg. Rating</th>
                                        <th>Last 5 Design Ratings</th>
                                        <th>Customer Plan</th>
                                        <!-- <th>LTV</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="cstm-desigr-rating actv_dsgnr"><th colspan="4">Current Customers</th><td><table><tbody>
                                        <?php
                                        
                                        $i = 0;
                                        //echo "hey<pre>";print_r($customer_info);
                                        $subUserCal = []; 
                                        foreach ($customer_info as $k => $c_info) {




                                            if ($c_info["is_active"] == 1) {
                                              //  echo "active<pre>";print_r($c_info['sub']);exit;
                                                ?> 
                                                <tr>
                                                    <?php if (count($c_info['sub']) == 0) { ?>

                                                        <td data-id="<?php echo $c_info['customer_id']; ?>">
                                                            <?php echo $c_info['name']; ?></td>

                                                        <td><?php echo $c_info['total_design']; ?></td>
                                                        <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $c_info["svg"]; ?>"> <?php echo $c_info['total_avg']; ?></td>
                                                        <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $c_info["lastsvg"]; ?>"><?php echo $c_info['lastAvg']['avg']; ?></td>
                                                        <td>  <?php echo $c_info['plan_name']; ?>  </td>
                                                    <?php } else {

                                                           // echo "<pre>"; print_r($c_info['subuserTdesign']); 
                                                   $totalDesignOfSubMain = $c_info['main']['total_design'] + $c_info['subuserTdesign']; 
                                                    
                                                   $totalAVGOfSubMain = $c_info['main']['total_avg']*$c_info['main']['total_design'] + $c_info['subuserAvg']; 

                                                   $finalAvgToprint = $totalAVGOfSubMain/$totalDesignOfSubMain; 

                                                   
                                                    
                                         // last Design Calculations 

                                                   

                                                   $totalDesignOfSubMainForlast = $c_info['main']['lastAvg']['Avgtotal_design'] + $c_info['subuserLstTdesign']; 
                                                   
                                                   $totalAVGOfSubMainForlast = $c_info['main']['lastAvg']['avg']*$c_info['main']['lastAvg']['Avgtotal_design'] + $c_info['subuserlastAvg'];

                                                   
                                                   $finalAvgToprintForlast = $totalAVGOfSubMainForlast/$totalDesignOfSubMain;
                                                   echo $c_info['name'] ;  
  
                                                     echo "<pre>"; print_r($totalAVGOfSubMainForlast); 

                                                  //  echo "2avg".$finalAvgToprintForlast."<br>"; 
                                                   $mainUserSvg =  $this->myfunctions->getfacesaccavg( $finalAvgToprint ); 
                                                   $mainUserSvglast5 =  $this->myfunctions->getfacesaccavg($finalAvgToprintForlast); 

                                                                
                                                           // echo $c_info['name'] ."===" .$finalAvgToprint;  
                                                    
                                                     ?>
                                                        <td class="accordion"  style="cursor: pointer; " data-id="<?php echo $c_info['id']; ?>">
                                                            <i id="i_<?php echo $c_info['id']; ?>" class="fa fa-plus" aria-hidden="true"></i>
                                                            <?php echo $c_info['name']; ?>
                                                        </td> 

                                                        <!-- main user list data start -->
                                                        

                                                        <td><?php echo $totalDesignOfSubMain; ?></td>
                                                        <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $mainUserSvg; ?>"> <?php echo number_format($finalAvgToprint,2,'.',''); ?></td>
                                                        <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $mainUserSvglast5; ?>"><?php echo number_format($finalAvgToprintForlast, 2, '.', ''); ?></td>
                                                        <td>  <?php echo $c_info['plan_name']; ?>  </td>
                                                        <!-- main user list data ends -->


                                                    <tr id="target_<?php echo $c_info['id']; ?>" class="custmtoggle hide">
                                                        <td>
                                                            <!-- main user description  -->
                                                           
                                                            <table>
                                                                <tr>
                                                                    <td data-id="<?php echo $c_info['main'][$ky]['customer_id']; ?>">
                                                                        <?php echo $c_info['main']['name']; ?></td>

                                                                    <td><?php echo $c_info['main']['total_design']; ?></td>
                                                                    <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $c_info['main']["svg"]; ?>"> <?php echo number_format($c_info['main']['total_avg'],2,'.',''); ?></td>
                                                                    <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $c_info['main']["svg"]; ?>"> <?php echo number_format($c_info['main']['lastAvg']['avg'],2,'.',''); ?></td>
                                                                    <td>  <?php echo $c_info['main']['plan_name']; ?>  </td>
                                                                </tr>

                                                                <!-- main user desc ends  -->


                                                                <?php foreach ($c_info['sub'] as $ky => $sub_user) {
                                                                    ?>
                                                                    <tr>
                                                                        <td data-id="<?php echo $sub_user['customer_id']; ?>">
                                                                            <?php echo $sub_user['name']; ?></td>

                                                                        <td><?php echo $sub_user['total_design']; ?></td>
                                                                        <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $sub_user["svg"]; ?>"> <?php echo $sub_user['total_avg']; ?></td>
                                                                        <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $sub_user["lastsvg"]; ?>"><?php echo $sub_user['lastAvg']['avg']; ?></td>
                                                                        <td>  <?php echo $sub_user['plan_name']; ?>  </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </table>
                                                        </td>

                                                    </tr>
                                                <?php } ?>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                        </tbody>
                                        <?php if($is_past_cus){ ?>
                                        <tfoot class="left-css">
                                            <tr>
                                                <?php
                                                $actv_totalAvgOF5Cl = $totalCountCust['actv_AvgWeightedCl'] / $totalCountCust['actv_toal5DesignCl'];
                                                ?>
                                                <td><strong>Total</strong></td>
                                                <td><strong><?php echo $totalCountCust['actv_totaldraftscL']; ?> </strong></td>
                                                <td id="totalAvgRatingCl"><strong><?php echo number_format($totalCountCust['actv_totalavgRatingcl'] / $totalCountCust['actv_totaldraftscL'], 2, '.', ''); ?></strong></td>
                                                <td id="totalAvgRatingCl15"><strong><?php echo number_format($actv_totalAvgOF5Cl, 2, '.', ''); ?></strong></td>

                                                <td><strong>---</strong></td>
                                                <!-- <td><strong>---</strong></td> -->
                                            </tr>
                                        </tfoot>
                                        <?php } ?></table></td></tr>
                                  <?php if($is_past_cus){ ?>
                                    <tr class="cstm-desigr-rating dectv_dsgnr"><th class="accord-outer" data-toggle="collapse" colspan="4" data-target="#past_customers">Past Customers</th><td class="accordion-step collapse" id="past_customers"><table><tbody>
                                        <?php
                                        $i = 0;
//                                        echo "<pre>";print_r($customer_info);
                                        foreach ($customer_info as $k => $c_info) {
                                            if ($c_info["is_active"] == 0) {
                                              //  echo "testtt<pre>";print_r($c_info['sub']);exit;
                                                ?> 
                                                <tr>
                                                    <?php if (count($c_info['sub']) == 0) { ?>

                                                        <td data-id="<?php echo $c_info['customer_id']; ?>">
                                                            <?php echo $c_info['name']; ?></td>

                                                        <td><?php echo $c_info['total_design']; ?></td>
                                                        <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $c_info["svg"]; ?>"> <?php echo $c_info['total_avg']; ?></td>
                                                        <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $c_info["lastsvg"]; ?>"><?php echo $c_info['lastAvg']['avg']; ?></td>
                                                        <td>  <?php echo $c_info['plan_name']; ?>  </td>
                                                    <?php } else { ?>
                                                        <td class="accordion"  style="cursor: pointer; " data-id="<?php echo $c_info['id']; ?>">
                                                            <i id="i_<?php echo $c_info['id']; ?>" class="fa fa-plus" aria-hidden="true"></i>
                                                            <?php echo $c_info['name']; ?>
                                                        </td> 

                                                        <!-- main user list data start -->
                                                        <?php
                                                        $countOfsubUser = count($c_info['sub']);
                                                        for ($i = 0; $i < $countOfsubUser; $i++) {

                                                            $totalAvgMAinUser = $c_info['total_avg'] + $c_info['sub'][$i]['total_avg'];
                                                            $lastDesignAvg = $c_info['lastAvg']['avg'] + $c_info['sub'][$i]['lastAvg']['avg'];

                                                            $mainUsrDraft = $c_info['total_design'] + $countOfsubUser;

                                                            $mainUserSvg = $this->myfunctions->getfacesaccavg($totalAvgMAinUser / $mainUsrDraft);
                                                            $mainUserSvglast5 = $this->myfunctions->getfacesaccavg($lastDesignAvg / $mainUsrDraft);
                                                        }
                                                        ?>

                                                        <td><?php echo $mainUsrDraft; ?></td>
                                                        <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $mainUserSvg; ?>"> <?php echo $totalAvgMAinUser / $mainUsrDraft; ?></td>
                                                        <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $mainUserSvglast5; ?>"><?php echo number_format($lastDesignAvg / $mainUsrDraft, 2, '.', ''); ?></td>
                                                        <td>  <?php echo $c_info['plan_name']; ?>  </td>
                                                        <!-- main user list data ends -->


                                                    <tr id="target_<?php echo $c_info['id']; ?>" class="custmtoggle hide">
                                                        <td>
                                                            <!-- main user description  -->
                                                            <table>
                                                                <tr>
                                                                    <td data-id="<?php echo $c_info['main'][$ky]['customer_id']; ?>">
                                                                        <?php echo $c_info['main'][0]['name']; ?></td>

                                                                    <td><?php echo $c_info['main'][0]['total_design']; ?></td>
                                                                    <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $c_info['main'][0]["svg"]; ?>"> <?php echo number_format($c_info['main'][0]['total_avg'],2,'.',''); ?></td>
                                                                    <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $c_info['main'][0]["svg"]; ?>"> <?php echo number_format($c_info['main'][0]['lastAvg']['avg'],2,'.',''); ?></td>
                                                                    <td>  <?php echo $c_info['main'][0]['plan_name']; ?>  </td>
                                                                </tr>

                                                                <!-- main user desc ends  -->


                                                                <?php foreach ($c_info['sub'] as $ky => $sub_user) {
                                                                    ?>
                                                                    <tr>

                                                                        <td data-id="<?php echo $sub_user['customer_id']; ?>">
                                                                            <?php echo $sub_user['name']; ?></td>

                                                                        <td><?php echo $sub_user['total_design']; ?></td>
                                                                        <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $sub_user["svg"]; ?>"> <?php echo number_format($sub_user['total_avg'],2,'.',''); ?></td>
                                                                        <td><img src="<?php echo base_url(); ?>public/assets/img/customer/<?php echo $sub_user["lastsvg"]; ?>"><?php echo number_format($sub_user['lastAvg']['avg'],2,'.',''); ?></td>
                                                                        <td>  <?php echo $sub_user['plan_name']; ?>  </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </table>
                                                        </td>

                                                    </tr>
                                                <?php } ?>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot class="left-css">
                                            <tr>
                                                <?php
                                                $de_totalAvgOF5Cl = $totalCountCust['de_AvgWeightedCl'] / $totalCountCust['de_toal5DesignCl'];
                                                ?>
                                                <td><strong>Total</strong></td>
                                                <td><strong><?php echo $totalCountCust['de_totaldraftscL']; ?> </strong></td>
                                                <td id="totalAvgRatingCl"><strong><?php echo number_format($totalCountCust['de_totalavgRatingcl'] / $totalCountCust['de_totaldraftscL'], 2, '.', ''); ?></strong></td>
                                                <td id="totalAvgRatingCl15"><strong><?php echo number_format($de_totalAvgOF5Cl, 2, '.', ''); ?></strong></td>

                                                <td><strong>---</strong></td>
                                                <!-- <td><strong>---</strong></td> -->
                                            </tr>
                                        </tfoot></table></td></tr>
                                  <?php } ?>
                                </tbody>
                                  <tfoot>
                                    <tr>
                                        <?php 
                                         $totalAvgOF5Cl = $totalCountCust['AvgWeightedCl']/$totalCountCust['toal5DesignCl'];
                                        ?>
                                        <td><strong>Total</strong></td>
                                        <td><strong><?php echo $totalCountCust['totaldraftscL']; ?> </strong></td>
                                        <td id="totalAvgRatingCl"><strong><?php echo number_format($totalCountCust['totalavgRatingcl']/ $totalCountCust['totaldraftscL'],2,'.',''); ?></strong></td>
                                        <td id="totalAvgRatingCl15"><strong><?php echo number_format($totalAvgOF5Cl,2,'.',''); ?></strong></td>
                                           
                                        <td><strong>---</strong></td>
                                        <!-- <td><strong>---</strong></td> -->
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/jquery.min.js');?>"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/2.3.1/moment.min.js"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/rangePicker.js');?>"></script>
<script type="text/javascript">
      var base_url = "<?php echo base_url();?>"; 
    $(window).on("load",function(){
        var Average = $("#totalAvgRatingCl").text();
        var Average5 = $("#totalAvgRatingCl15").text();
         
        var svg,img;
        if(isNaN(Average)!=true){
            if(Average > 0 && Average < 3 ){
                   svg ="sad-red.svg";
                }else if(Average >= 3 && Average <= 4.24){
                    
                   svg ="happiness-yellow.svg";
                }else{
                   svg ="happy-green.svg"; 
                }
        }else{
            Average = 0;
            svg ="happy-green.svg"; 
             
        }

        if(isNaN(Average5)!=true){
            if(Average5 > 0 && Average5 < 3 ){
                   svg5 ="sad-red.svg";
                }else if(Average >= 3 && Average <= 4.24){
                    
                   svg5 ="happiness-yellow.svg";
                }else{
                   svg5 ="happy-green.svg"; 
                }
        }else{
            Average5 = 0;
            svg5 ="happy-green.svg"; 
             
        }
        img = base_url+"public/assets/img/customer/"+svg; 
        img5 = base_url+"public/assets/img/customer/"+svg5; 
       $("#cust_review span").text(Average); 
       $("#last5des span").text(Average5); 
       $("#cust_review img").attr("src",img);  
       $("#last5des img").attr("src",img5);  
    });
   $(".Sorting_area i").click(function(){
      if($(this).hasClass('up-desc')){
        $(this).removeClass('fa-angle-up up-desc').addClass('fa-angle-down down-Asc');
         var order = 'ASC';
      }else{
        $(this).removeClass('fa-angle-down down-Asc').addClass('fa-angle-up up-desc');
         order ='DESC'; 
      }

      SortingAjax(order,controller="feedback_matrix","designer");
   });
    function SortingAjax(order,controller,type){
    var html;
    $(".ajax_loader").show();
    $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>admin/Contentmanagement/'+controller,
                data: {"sort":order,"type":type},  
                dataType:"json",  
                success: function(data) {
                    if(data!=''){
                       var resultLength = data.length; 
                       var numberofPagnation =  Math.round(resultLength/50);
                         
                       for(i = 1; i<= numberofPagnation; i++)
                       {
                         $("#pegid_"+i).show();
                       }
                  $(".ajax_loader").hide();
                  $.each(data, function (key, value) {
                    //console.log("sss",data.lastAvg); 
                        html+= '<tr><td>'+value.name+'</td><td>'+value.total_design+'</td><td>'+value.total_avg+'</td><td>'+value.total_avg+'</td></tr>';
                     

                   });
              }else{
                $(".ajax_loader").hide();
                html+= '<tr><td colspan="10" class="NoRecord">No Record found </td></tr>';
              }
                  $("#rating_details").html(html);
                  $('[data-toggle="tooltip"]').tooltip();
                } 
            });
  }
 
</script>

<script>
  $(".accordion").click(function(){

    $(".accordion").not(this).removeClass("active"); 
    
 
    $(this).addClass('active');
    var id = $(this).data('id'); 
    //AjaxForSubCustomer(id);

    if($("#i_"+id).hasClass("fa-plus")){
      $("#i_"+id).removeClass("fa-plus").addClass("fa-minus");
    }else{
      $("#i_"+id).removeClass("fa-minus").addClass("fa-plus");
    } $("#target_"+id).toggleClass('hide');
  });
 

 // function AjaxForSubCustomer(id){
 //  $.ajax({
 //                type: "POST",
 //                url: '<?php echo base_url();?>admin/Contentmanagement/AjaxForSubCustomer',
 //                data: {"parentid":id},  
 //                dataType:"json",  
 //                success: function(data) {
 //                  console.log("data",data); 
                    
 //                }
 //              });      
 // }

 function ajaxForTopNav(one,two){
  $("#TopNavDefault").hide();  
  $("#TopNavForComapare").show();  
  // TopNavForComapare
  var htmlleft='';
  var htmlright='';
  $("#TopNavForComapare").find(".compare-row").addClass("loading");
  $.ajax({
                type: "POST",
                url: '<?php echo base_url();?>admin/Contentmanagement/ajaxForTopNav',
                data: {"start":one,"end":two},  
                dataType:"json",  
                success: function(data) {
                  $("#TopNavForComapare").find(".compare-row").removeClass("loading");
                  
                    $.each(data.customerRatingOne, function (key, value) {
                     
                    htmlleft+='<div class="left-area"><h2><strong>'+one+'</strong></h2></div><div class="compare_items"><h3>Customer Review</h3><span class="comare-rat">'+data.customerRatingOne[0].customer_rating+'</span></div><div class="compare_items"><h3>Like us</h3><span class="comare-rat">'+data.likeOne.total_count+'</span></div><div class="compare_items"><h3>Dislike us</h3><span class="comare-rat">'+data.dislIkeOne.total_count+'</span></div><div class="compare_items"><h3>Last 5 Ratings</h3><span class="comare-rat">'+data.last5avgOne.avg+'</span></div>'; 

                    htmlright+='<div class="right-area"><h2><strong>'+two+'</strong></h2></div><div class="compare_items"><h3>Customer Review</h3><span class="comare-rat">'+data.customerRatingtwo[0].customer_rating+'</span></div><div class="compare_items"><h3>Like us</h3><span class="comare-rat">'+data.likeTwo.total_count+'</span></div><div class="compare_items"><h3>Dislike us</h3><span class="comare-rat">'+data.dislIkeTwo.total_count+'</span></div><div class="compare_items"><h3>Last 5 Ratings</h3><span class="comare-rat">'+data.last5avgTwo.avg+'</span></div>'; 
                 });
                    var overAllRating = $("#totalAvgRatingCl").text();
                    var overAllRatinglast5 = $("#totalAvgRatingCl15").text();
          
                    $("#TopNavForComapare .left-compare").html(htmlleft);
                    $("#TopNavForComapare .right-compare").html(htmlright);
                    $("#overall-custReview").text(overAllRating);
                    $("#overall-custReviewlast5").text(overAllRatinglast5);
                 }
              });  
                 
 }
 
 // using callbacks
  $("#endrange").daterangepicker({
    dateFormat: "yy/mm/dd",
    datepickerOptions : {
         numberOfMonths : 1
         
     },
       initialText : 'Select',
       presetRanges: false,
       applyOnMenuSelect: true,
    });
 // using events
 $("#endrange").on('daterangepickerclose', function(event, data) {   });
 // a change event is also triggered on the original element
 $("#endrange").on('change', function(event,data) {  

var daterange1  = $("#range1").find(".ui-button-text").text();
var daterange2  = $("#range2").find(".ui-button-text").text();
 if(daterange1!="Select"){
  $("#range1").find("button").removeAttr("style"); 
 ajaxForTopNav(daterange1,daterange2);
}else{
  $("#range1").find("button").css("border","1px solid #f92141"); 
}
 });
 // using callbacks
 $("#startrange").daterangepicker({
  dateFormat: "yy/mm/dd",
    datepickerOptions : {
     numberOfMonths : 1,
     
     },
     presetRanges: true,
     initialText : 'Select',
    applyOnMenuSelect: false,
 
 });
 // using events
 $("#startrange").on('daterangepickerclose', function(event, data) {   });
 // a change event is also triggered on the original element
 $("#startrange").on('change', function(event) {   });
 </script>