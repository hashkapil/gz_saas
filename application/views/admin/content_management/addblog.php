<section class="con-b">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?php if ($this->session->flashdata('message_error') != '') { ?>    
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_error'); ?></strong>    
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('message_success') != '') { ?>    
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                    </div>
                <?php } ?>
                <div class="flex-this">
                    <h2 class="main_page_heading">Blog</h2>
                </div>
                <br/>
            </div>
        </div>
        <div class="white-boundries">
            <div class="row">
                <div class="col-md-12">
                    <div class="headerWithBtn">
                        <h2 class="main-info-heading">Add Blog</h2>
                        <p class="fill-sub">Please fill the form below.</p>
                        <div class="addPlusbtn">
                            <a href="<?php echo base_url();?>admin/Contentmanagement/blog" class="back-link-xx0">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png">
                            Back</a>
                        </div>
                    </div>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="fo-rm-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="form-group">
                                        <p class="label-txt">Add Title</p>
                                        <input type="text" name="title" class="input input-d" id="title" required>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-sm-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Category</p>
                                        <select id="category" name="category" class="inputselect-d input">
                                            <option value="Web">Web</option>
                                            <option value="recomended">Recommended</option>
                                        </select>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-sm-12">
                                    <label class="form-group">
                                        <p class="label-txt">Tags</p>
                                        <textarea name="tags" id="tags" class="input textarea-c"></textarea>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group goup-x1">
                                        <textarea name="body" id="txtEditor" class="input">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Recommand blog</label>
                                        <p class="space-a"></p>
                                        <?php //echo "<pre/>";print_R($allblogs);?>
<!--                                        <select name="recomended_blog[]" id="recom_blog"  class="chosen-select" style='display:none' data-live-search="true" multiple>-->
                                        <select name="recomended_blog[]" class="recom_add" multiple required>
                                         <?php 
                                         
                                         foreach ($allblogs as $bk => $bv) { ?>
                                                <option value="<?php echo $bv['id']; ?>"><?php echo $bv['title']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">featured image</label>
                                        <p class="space-b"></p>
                                        <div class="cell-row">
                                            <div class="cell-col">
                                                <div class="blogPic" style="display: none;">
                                                    <div class="accimgbx33">
                                                        <img class="blogimage" src="" style="object-fit: unset;">
                                                    </div>
                                                    <a class="icon-crss-2 remove_selected_blogimg"><i class="icon-gz_delete_icon"></i></a>
                                                </div>
                                                <div class="bl-ogaddt-est blogimgadd">
                                                    <span class="bl-ogaddt-est-a2">
                                                        <span class="bl-ogaddt-est-a33">+</span>
                                                    </span>
                                                    Add Image
                                                    <input class="file-input" id="blog_image" name="blog_file" type="file" required="required" onchange="validateAndUploadblog(this);" accept="image/*">
                                                </div>
                                                <input id="pic" type="hidden" class="blogimg_hidden">
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <p class="btn-x bl-ogbtn text-right">
                                        <button type="submit" id="save" class="load_more button">Publish</button> 
                                    </p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/chosen.css');?>" rel="stylesheet">
    <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/jquery.min.js');?>"></script>
    <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/bootstrap.min.js');?>"></script>
    <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/chosen.jquery.js');?>"></script>
    <script type="text/javascript">
//        $(document).on('click', "#menuBarDiv_txtEditor .btn-group a", function () {
//            $(this).closest('.btn-group').toggleClass('open');
//        });
//        function validateAndUploadblog(input) {
//            var URL = window.URL || window.webkitURL;
//            var file = input.files[0];
//            if (file.type.indexOf('image/') !== 0) {
//                this.value = null;
//                console.log("invalid");
//            }
//            else {
//                if (file) {
//                    console.log(URL.createObjectURL(file));
//                    $(".blogimgadd").hide();
//                    $(".blogPic").css('display', 'block');
//                    $(".blogPic .blogimage").attr('src', URL.createObjectURL(file));
//                    $('.blogimg_hidden').val(URL.createObjectURL(file));
//                }
//            }
//        }

//        $('.remove_selected_blogimg').click(function (e) {
//            e.preventDefault();
//            $(this).closest('.blogPic').hide();
//            $('.blogimgadd').show();
//            $('.blogimgadd input').val('');
//        });
//        $('#preview').click(function (e) {
//            e.preventDefault();
//            $('#txtEditor').text($('#txtEditor').Editor("getText"));
//            var title = $("#title").val();
//            var cate = $("#category").val();
//            var tags = $("#tags").val();
//            var content = $("#txtEditor").val();
//            var image = $("#pic").val();
//            $("#heading_title").html(title);
//            $('.blogimg').html('<img src="' + image + '">');
//            $('.blogcontent p').html(content);
//            $('.pretags_in').html(tags);
//            $('.precate_in').html(cate);
//        });

//        $(document).ready(function () {
//            $('#recom_blog').chosen();
//        });
    </script>