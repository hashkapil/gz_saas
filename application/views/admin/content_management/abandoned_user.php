<?php // echo "<pre>"; print_r($timezone); ?>
<div class="header_searchbtn abond_user">
<a class="dwl-oadfi-le" id="download_rquests" data-status="active,disapprove" href="<?php echo base_url();?>admin/Contentmanagement/download_abonededform">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 12 15" enable-background="new 0 0 12 15" xml:space="preserve">
                                <g>
                                <g>
                                <path fill="#7f7f7f" d="M5.3,11.4C5.3,11.5,5.4,11.5,5.3,11.4c0.5,0.4,1,0.4,1.4,0c0.9-1,1.8-2,2.7-3c0,0,0.1-0.1,0.1-0.1
                                      c0.2-0.3,0.3-0.6,0.2-1c-0.2-0.7-1.1-1-1.6-0.4C7.8,7.3,7.4,7.7,7,8.2c0,0,0,0-0.1,0.1V8.1c0-2.3,0-4.7,0-7C7,1.1,7,1,7,1
                                      c0-0.4-0.2-0.7-0.6-0.9C6.3,0,6.2,0,6.1,0H5.9C5.8,0,5.6,0.1,5.5,0.2C5.2,0.4,5,0.7,5,1.1c0,2.3,0,4.7,0,7v0.1c0,0,0,0,0,0
                                      c0,0,0,0,0-0.1C4.6,7.7,4.2,7.3,3.8,6.9C3.5,6.6,3.1,6.5,2.7,6.7C2.1,7,2,7.9,2.5,8.4C3.4,9.4,4.4,10.4,5.3,11.4z M12,10.5
                                      c0-0.6-0.6-1.1-1.2-1c-0.4,0.1-0.7,0.5-0.7,1c0,0.7,0,1.5,0,2.2v0.1H1.9v-0.1c0-0.7,0-1.5,0-2.2c0,0,0-0.1,0-0.1
                                      c0-0.5-0.4-1-0.9-1c-0.6,0-1,0.4-1,1c0,1.1,0,2.3,0,3.4c0,0.1,0,0.1,0,0.2C0.1,14.7,0.5,15,1,15c3.3,0,6.7,0,10,0
                                      c0.6,0,1-0.4,1-1.1C12,12.8,12,11.7,12,10.5C12,10.6,12,10.5,12,10.5z"></path>
                                </g>
                                </g>
                                </svg>
                            </a>
</div>
<section class="con-b">
    <div class="header-blog">
        <input type="hidden" value="" class="bucket_type">
        <div class="row flex-show">
            <div class="col-md-12">
                <div class="flex-this">
                    <h2 class="main_page_heading">Abandoned Users List</h2>
                </div>
            </div>
        </div>
    </div>


    <div class="product-list-show" id="abond_data">
            <div class="cli-ent-row tr tableHead">
                <div class="cli-ent-col td" style="width: 30%">User Name</div>
                <div class="cli-ent-col td" style="width: 30%">Email</div>
                <div class="cli-ent-col td" style="width: 20%">State</div>
                <div class="cli-ent-col td" style="width: 20%">Created At</div>
            </div>
            <?php
             
            $count_project_a = count($data["users"]);      
            if (!empty($data)) 
                {
                $i=1;
                foreach ($data["users"] as $key=>$user) {
                    if($i <=LIMIT_ADMIN_LIST_COUNT ){ 
                    ?>
                     
                      <div class="row two-can">
                        <div class="cli-ent-row tr brdr">
                            <div class="cli-ent-col td" style="width: 30%"><?php echo $user['first_name'] . ' ' . $user['last_name']; ?></div>
                            <div class="cli-ent-col td" style="width: 30%"><?php echo $user['email']; ?></div>
                            <div class="cli-ent-col td" style="width: 20%"><?php echo $user['state']; ?></div>
                            <div class="cli-ent-col td" style="width: 20%"><?php echo $timezone[$key]['created_at']; ?></div>
                        </div> 
                      </div>
                <?php $i++; } } ?>
                <?php
            } else {
                echo "<p style='text-align:center;padding:20px;'>Data Not Found</p>";
            }
            ?>
        </div>
    <?php if ($count_project_a > LIMIT_ADMIN_LIST_COUNT) { ?>
        <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" />
        </div>
        <div class="" style="text-align:center">
            <a id="load_more_aboanded" href="javascript:void(0)" data-limit="<?php echo LIMIT_ADMIN_LIST_COUNT; ?>" data-row="<?php echo LIMIT_ADMIN_LIST_COUNT; ?>" data-count="<?php echo $count_project_a; ?>" class="load_more button">Load more</a>
        </div>
    <?php } ?>
</section>
<style>
    .abond_user{
        float:right;
        margin:10px; 
    }
</style>