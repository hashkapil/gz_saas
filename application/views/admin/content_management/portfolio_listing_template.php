<?php //echo "<pre/>";print_R($portfolio); ?>
<div class="col-md-3 col-sm-6">
    <figure class="por-tfolioimg">
        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PORTFOLIOS . $portfolio['image']; ?>" class="img-responsive">
    </figure>
    <div class="bl-ogbox por-tfol-io">
        <div class="postion_count"><?php echo $portfolio['position']; ?></div>
        <h3 class="bl-oghead" title="<?php echo $portfolio['title']; ?>">
            <?php
            if (strlen($portfolio['title']) > 25) {
                echo $name = substr($portfolio['title'], 0, 25) . " ...";
            } else {
                echo $name = $portfolio['title'];
            }
            ?>
        </h3>
        <span class="bl-ogaction">
            <a href="<?php echo base_url() ?>admin/contentmanagement/portfolio_edit/<?php echo $portfolio['id']; ?>">
                <i class="icon-gz_edit_icon"></i>
            </a>
            <a href="javascript:void(0)" class="delete_portfolio" data-delID="<?php echo $portfolio['id']; ?>"><i class="icon-gz_delete_icon"></i></a>
        </span>
<!--        <div class="category_nm">Category: <span><?php //echo $portfolio['category_name']; ?></span></div>-->
        <p class="pro-b mintwo"><?php echo substr(strip_tags($portfolio['body']), 0, 220); ?></p>
        <div class="cell-row">
            <p class="pro-b"><?php echo date("M /d /Y", strtotime($portfolio['created'])); ?></p>
        </div>
    </div>
</div>