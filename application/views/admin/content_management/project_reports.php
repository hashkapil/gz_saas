<section class="con-b">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="flex-this">
                    <h2 class="main_page_heading perfect_space">Project Reports</h2>
                </div>
            </div>
        </div>
        <div class="cli-ent table">
            <p class="space-e"></p>
            <div class="tab-content">
                <div class="tab-pane product-list-show active" id="Active_reports" role="tabpanel">
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 77%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head space-b">Average number of revisions per design</h3>
                                    <p class="descr_reports">Sum of total revisions / Total number of projects from beginning to till date (Excluding queue, draft, hold, cancel projects)</p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center"><span class="green text-uppercase"><?php echo $averagerevisionperdesign; ?></span></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 14%;"></div>
                            
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 77%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head space-b">Projects currently have more than one revision count</h3>
                                    <p class="descr_reports">Count of projects gone to in-revision by client more than <?php echo AVG_REVISION_NUMBER - 1; ?> time</p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center">
                                        <span class="green text-uppercase"><?php echo $countrev; ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 14%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <a href="<?php echo base_url();?>admin/Contentmanagement/view_projects_basedonslug/revisions_count/<?php echo AVG_REVISION_NUMBER;?>"><p class="neft text-center"><span class="green text-uppercase">View Projects</span></p></a>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 77%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head space-b">Projects goes into quality revision more than one time</h3>
                                    <p class="descr_reports">Count of projects quality failed by admin/QA more than <?php echo AVG_REVISION_NUMBER - 1; ?> time</p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center">
                                        <span class="green text-uppercase"><?php echo $countqualityrev; ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 14%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <a href="<?php echo base_url();?>admin/Contentmanagement/view_projects_basedonslug/quality_revision_count/<?php echo AVG_REVISION_NUMBER;?>"><p class="neft text-center"><span class="green text-uppercase">View Projects</span></p></a>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 77%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head space-b">How many projects got delayed today</h3>
                                    <p class="descr_reports">Undelivered expired projects of today + delivered projects today after expired datetime</p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center">
                                        <span class="green text-uppercase"><?php echo $delayedprojectcount; ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 14%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <a href="<?php echo base_url();?>admin/Contentmanagement/view_projects_basedonslug/delayed_project"><p class="neft text-center"><span class="green text-uppercase">View Projects</span></p></a>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 77%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head space-b">How many projects are currently late</h3>
                                    <p class="descr_reports">Active projects having expected date greater than current datetime (<?php echo $currentdate; ?>)</p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center">
                                        <span class="green text-uppercase"><?php echo $lateProjectsCount; ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 14%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <a href="<?php echo base_url();?>admin/Contentmanagement/view_projects_basedonslug/late_project"><p class="neft text-center"><span class="green text-uppercase">View Projects</span></p></a>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 77%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head space-b">How many projects are unverified</h3>
                                    <p class="descr_reports">Active projects that are not verified</p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center">
                                        <span class="green text-uppercase"><?php echo $unverifiedProjectsCount; ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 14%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <a href="<?php echo base_url();?>admin/Contentmanagement/view_projects_basedonslug/unverified"><p class="neft text-center"><span class="green text-uppercase">View Projects</span></p></a>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 77%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head space-b">How many projects are active today</h3>
                                    <p class="descr_reports">Total number of projects having today's expected date (<?php echo $date = date('M d, Y',strtotime($currentdate)); ?>).</p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center">
                                        <span class="green text-uppercase"><?php echo $activeprojectscount; ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 14%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <a href="<?php echo base_url();?>admin/Contentmanagement/view_projects_basedonslug/activepro"><p class="neft text-center"><span class="green text-uppercase">View Projects</span></p></a>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="row">
                        <div class="cli-ent-row tr brdr fullheight">
                            <div class="cli-ent-col td" style="width: 77%;">
                                <div class="cli-ent-xbox text-left">
                                    <h3 class="pro-head space-b">How many projects are completed today</h3>
                                    <p class="descr_reports">Total number of projects with today's expected date and delivered today (<?php echo $date = date('M d, Y',strtotime($currentdate)); ?>).</p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <p class="neft text-center">
                                        <span class="green text-uppercase"><?php echo $activenotapprovedProjectsCount; ?></span>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 14%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="space-a"></p>
                                    <a href="<?php echo base_url();?>admin/Contentmanagement/view_projects_basedonslug/activebutcompleted"><p class="neft text-center"><span class="green text-uppercase">View Projects</span></p></a>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

