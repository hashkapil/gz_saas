<?php //echo "<pre/>";print_R($editdata);  ?>
<section class="con-b">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?php if ($this->session->flashdata('message_error') != '') { ?>    
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_error'); ?></strong>    
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('message_success') != '') { ?>    
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                    </div>
                <?php } ?>
                <div class="flex-this">
                    <h2 class="main_page_heading">Add/Edit Sample</h2>
                </div>
            </div>
        </div>
        <br/>
        <div class="white-boundries">
            <div class="row">
                <div class="col-md-12">
                    <div class="headerWithBtn">
                        <h2 class="main-info-heading">Add/Edit Sample</h2>
                        <p class="fill-sub">Add and Update sample for request based on categories & subcategories</p>
                        <div class="addPlusbtn">
                            <a href="<?php echo base_url(); ?>admin/Contentmanagement/category_based_samples" class="back-link-xx0">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png">
                                Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <form method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt label-active labels_up">Category*</p>
                            <select id="quest_category" name="category" class="input select-d make_default" required="">
                                <option value="" selected="">Select option</option>
                                <?php for ($i = 0; $i < sizeof($categories); $i++) { ?>
                                    <option value="<?php echo $categories[$i]['id'] ?>"
                                            <?php echo ($categories[$i]['id'] == $editdata[0]['category_id']) ? 'selected' : ''; ?>>
                                        <?php echo $categories[$i]['name'] ?></option>
                                <?php } ?>
                            </select>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt label-active labels_up">Subcategory*</p>
                            <select id="quest_subcategories" name="subcategories" class="input select-d make_default" required>
                                <option value="" selected="">Select option</option>
                                <?php
                                if (!empty($sub_categories)) {
                                    for ($i = 0; $i < sizeof($sub_categories); $i++) {
                                        ?>
                                        <option value="<?php echo $sub_categories[$i]['id'] ?>"
                                            <?php echo ($sub_categories[$i]['id'] == $editdata[0]['subcategory_id']) ? 'selected' : ''; ?>>
                                        <?php echo $sub_categories[$i]['name'] ?></option>
                                    <?php }
                                }
                                ?>
                            </select>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt">MINIMUM SAMPLE SELECTION REQUIRED BY USER*</p>
                            <input type="number" id="min_sample" min="1" name="min_sample" value="<?php echo (isset($editdata[0]['minimum_sample'])) ? $editdata[0]['minimum_sample'] : ''; ?>" class="form-control input" required>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt">MAXIMUM SAMPLE SELECTION REQUIRED BY USER*</p>
                            <input type="number" id="max_sample" min="1" name="max_sample" value="<?php echo (isset($editdata[0]['maximum_sample'])) ? $editdata[0]['maximum_sample'] : ''; ?>" class="form-control input" required>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt">Sample Name*</p>
                            <input type="text" id="question_options" name="sample_name" value="<?php echo (isset($editdata[0]['name']) && $editdata[0]['name'] != '') ? $editdata[0]['name'] : '';   ?>" class="form-control input" required>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>

                    </div>
                     <div class="col-md-6 default_questions actv_sm">
                            <label class="form-group">Active</label>
                            <div class="notify-lines">
                                <div class="switch-custom switch-custom-usersetting-check">
                                    <span class="checkstatus"></span>
                                    <input name="quest_active" type="checkbox" <?php echo (isset($editdata[0]['is_active']) && $editdata[0]['is_active'] == '1') ? 'checked' : ''; ?> id="is_active" value="" >
                                    <label for="is_active"></label> 
                                </div>
                            </div>
                        </div>
                    
                    <!--
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt">Sample's position*</p>
                            <input type="number" id="position" min="1" name="position" value="<?php //echo (isset($editdata[0]['position'])) ? $editdata[0]['position'] : '';   ?>" class="form-control input" required>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                    </div>  
                </div>-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group goup-x1"> 
                                <div class="file-drop-area file-upload">
                                    <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or 
                                        <span class="nocolsl">Click Here</span>
                                    </span>
                                    <input type="file" class="file-input project-file-input" multiple="" name="sample-upload[]" id="file_input" data-type="sample">
                                </div>
                                <p class="text-g">Example: Jpg, Png, Jpeg </p>
                            </div>
                            <hr/>
                            <div class="uploadFileListContainer row">  </div>
                                <?php if (!empty($getsamplematerial)) { ?>
                                <hr/>
                                <div class="">   
    <?php for ($i = 0; $i < count($getsamplematerial); $i++) { ?>
                                        <div class="uploadFileRow samplematid" id="file<?php echo $getsamplematerial[$i]['id']; ?>" data-sample="<?php echo $getsamplematerial[$i]['id']; ?>">
                                            <div class="col-md-6">
                                                <div class="extnsn-lst">
                                                    <p class="text-mb">
        <?php echo $getsamplematerial[$i]['sample_material']; ?>
                                                    </p>
                                                    <a href="javascript:void(0)"><span class="delsamplemat">x</span></a>
                                                </div>
                                            </div>
                                        </div>
                                <?php } ?>
                                </div>
<?php } ?>
                        </div>
                    </div>
<!--                    <div class="row">
                        <div class="col-md-6 default_questions">
                            <label class="form-group">Active</label>
                            <div class="notify-lines">
                                <div class="switch-custom switch-custom-usersetting-check">
                                    <span class="checkstatus"></span>
                                    <input name="quest_active" type="checkbox" <?php //echo (isset($editdata[0]['is_active']) && $editdata[0]['is_active'] == '1') ? 'checked' : ''; ?> id="is_active" value="" >
                                    <label for="is_active"></label> 
                                </div>
                            </div>
                        </div>
                    </div>-->

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" id="save" class="load_more button">Publish</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
</div>
</section>
