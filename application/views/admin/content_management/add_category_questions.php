<?php //echo "<pre/>";print_R($edit_id);?>
<section class="con-b">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?php if ($this->session->flashdata('message_error') != '') { ?>    
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_error'); ?></strong>    
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('message_success') != '') { ?>    
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                    </div>
                <?php } ?>
                <div class="flex-this">
                    <h2 class="main_page_heading">Add/Edit Question</h2>
                </div>
            </div>
        </div>
        <br/>
        <div class="white-boundries">
            <div class="row">
                <div class="col-md-12">
                    <div class="headerWithBtn">
                        <h2 class="main-info-heading">Add/Edit Question</h2>
                        <p class="fill-sub">Add/Update question for request</p>
                        <div class="addPlusbtn">
                            <a href="<?php echo base_url(); ?>admin/Contentmanagement/category_based_questions" class="back-link-xx0">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png">
                                Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <form method="post" enctype="multipart/form-data" class="questions-from">
                <input type="hidden" name="multicat[]" class="multicat" value="">
                <input type="hidden" name="multisubcat[]" class="multisubcat" value="">
                <div class='row'>
                    <div class="col-md-6 default_questions">
                    <label class="form-group">Make this question as default</label>
                        <div class="notify-lines">
                        <div class="switch-custom switch-custom-usersetting-check">
                            <span class="checkstatus"></span>
                            <input name="quest_default" type="checkbox" <?php echo (isset($editdata[0]['is_default']) && $editdata[0]['is_default'] == '1') ? 'checked' : ''; ?> id="quest_default" value="" >
                            <label for="quest_default"></label> 
                        </div>
                        </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt label-active labels_up">Category</p>
                            <select id="quest_category" name="category[]" class="input select-d make_default" <?php echo (isset($editdata[0]['is_default']) && $editdata[0]['is_default'] == '1') ? 'disabled' : ''; ?> required>
                                <option value="" selected="">Select option</option>
                                <?php for ($i = 0; $i < sizeof($categories); $i++) { ?>
                                    <option value="<?php echo $categories[$i]['id'] ?>"
                                    <?php echo ($categories[$i]['id'] == $editdata[0]['category_id']) ? 'selected' : ''; ?>>
                                    <?php echo $categories[$i]['name'] ?></option>
                                <?php } ?>
                            </select>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
<!--                            <p class="fill-sub">Please select atleast one category, if you don't want to make this question as default question.</p>-->
                        </label>
                    </div>
<!--                    <input type="hidden" name="selected_subcat_id" id="selected_subcat_id" value=""/>-->
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt label-active labels_up">Subcategory</p>
                            <select id="quest_subcategories" name="subcategories[]" class="input select-d make_default" <?php echo (isset($editdata[0]['is_default']) && $editdata[0]['is_default'] == '1') ? 'disabled' : ''; ?>>
                                <option value="" selected="">Select option</option>
                                <?php if(!empty($sub_categories)){ 
                                for ($i = 0; $i < sizeof($sub_categories); $i++) { ?>
                                <option value="<?php echo $sub_categories[$i]['id'] ?>"
                                <?php echo ($sub_categories[$i]['id'] == $editdata[0]['subcategory_id']) ? 'selected' : ''; ?>>
                                <?php echo $sub_categories[$i]['name'] ?></option>
                                 <?php }} ?>
                            </select>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="inner_ques_1">
                <div class="questions_div">
                <div class="questions_data">
                <?php if($edit_id == ''){ ?>
                <h3 class="question_count">Question No. <span class="number">1</span></h3>
                <?php } ?>
                <div class="row">
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt">Type*</p>
                            <select id="quest_type" name="question_type[]" class="input select-d quest_type" required>
                                <option value="text" <?php echo (isset($editdata[0]['question_type']) && $editdata[0]['question_type'] == 'text') ? 'selected' : ''; ?>>Text</option>
                                <option value="textarea" <?php echo (isset($editdata[0]['question_type']) && $editdata[0]['question_type'] == 'textarea') ? 'selected' : ''; ?>>Textarea</option>
                                <option value="selectbox" <?php echo (isset($editdata[0]['question_type']) && $editdata[0]['question_type'] == 'selectbox') ? 'selected' : ''; ?>>Selectbox</option>
                                <option value="radio" <?php echo (isset($editdata[0]['question_type']) && $editdata[0]['question_type'] == 'radio') ? 'selected' : ''; ?>>Radio</option>
                                <option value="checkbox" <?php echo (isset($editdata[0]['question_type']) && $editdata[0]['question_type'] == 'checkbox') ? 'selected' : ''; ?>>Checkbox</option>
                            </select>
                            <div class="line-box">
                            <div class="line"></div>
                            </div>
                        </label>
                    </div>
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt">QUESTION TEXT*</p>
                            <input type="text" id="question_label" name="question_label[]" value="<?php echo (isset($editdata[0]['question_label'])) ? $editdata[0]['question_label'] : ''; ?>" class="form-control input" required>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="row">
                   <div class="col-md-12 question_options" style="<?php echo (isset($editdata[0]['question_options']) && $editdata[0]['question_options'] != '0') ? "display:block" : 'display:none';?>">
                        <label class="form-group">
                            <p class="label-txt">ANSWER OPTIONS*</p>
                            <input type="text" id="question_options" name="question_options[]" value="<?php echo (isset($editdata[0]['question_options']) && $editdata[0]['question_options'] != '0') ? $editdata[0]['question_options'] : ''; ?>" class="form-control input">
                        </label>
                       <div class="line-box">
                            <div class="line"></div>
                        </div>
                        <p class="fill-sub">Please enter your options here by comma separate like(demo,test,help).</p>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt">position*</p>
                            <input type="number" id="position" name="position[]" value="<?php echo (isset($editdata[0]['position'])) ? $editdata[0]['position'] : ''; ?>" class="form-control input" min="1" required>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                    </div>  
                
                <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt">QUESTION HELP TEXT</p>
                            <input type="text" id="question_placeholder" value="<?php echo (isset($editdata[0]['question_placeholder'])) ? $editdata[0]['question_placeholder'] : ''; ?>" name="question_placeholder[]" class="form-control input">
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 default_questions">
                    <label class="form-group">Active</label>
                        <div class="notify-lines">
                        <div class="switch-custom switch-custom-usersetting-check">
                            <span class="checkstatus"></span>
                            <input name="quest_active[]" type="checkbox" class="is_active" <?php echo (isset($editdata[0]['is_active']) && $editdata[0]['is_active'] == '1') ? 'checked' : ''; ?> id="is_active_1" value="" >
                            <label for="is_active_1"></label> 
                        </div>
                        </div>
                    </div>
                    <?php //echo "<pre/>";print_R($editdata);?>
                    <div class="col-md-6 default_questions required_flag" style="<?php //echo (empty($editdata) || $editdata[0]['question_type'] == 'text' || $editdata[0]['question_type'] == 'textarea') ? 'display:block' : 'display:none'; ?>">
                    <label class="form-group">Required</label>
                        <div class="notify-lines">
                        <div class="switch-custom switch-custom-usersetting-check">
                            <span class="checkstatus"></span>
                            <input name="quest_reqrd[]" type="checkbox" class="is_required" <?php echo (isset($editdata[0]['is_required']) && $editdata[0]['is_required'] == '1') ? 'checked' : ''; ?> id="is_required" value="" >
                            <label for="is_required"></label> 
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                
                </div>
                </div>
                
                <div class="input_quest_container"></div>
                <?php if($edit_id == ''){ ?>
                <a class="add_more_quest" style="text-align:right;"> <span>+</span> Add question</a>
                <?php } ?>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" id="save" class="load_more button">Publish</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
