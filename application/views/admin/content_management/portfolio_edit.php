<section class="con-b">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?php if ($this->session->flashdata('message_error') != '') { ?>    
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_error'); ?></strong>    
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('message_success') != '') { ?>    
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                    </div>
                <?php } ?>
                <div class="flex-this">
                    <h2 class="main_page_heading">Portfolio</h2>
                </div>
                <br/>
            </div>
        </div>
        <div class="white-boundries">
            <div class="row">
                <div class="col-md-12">
                    <div class="headerWithBtn">
                        <h2 class="main-info-heading">Edit Portfolio</h2>
                        <p class="fill-sub">Edit and Update additional users for your account.</p>
                        <div class="addPlusbtn">
                            <a href="<?php echo base_url(); ?>admin/Contentmanagement/portfolio" class="back-link-xx0">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png">
                                Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <form method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt">Title</p>
                            <input type="text" id="title" name="title" class="input" value="<?php echo $portfoliodata[0]['title']; ?>" required>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                        <label class="form-group">
                            <p class="label-txt">Category</p>
                            <select id="category" name="category" class="input select-d" required>
                                <option value="">Select option</option>
                                <?php for ($i = 0; $i < sizeof($all_portfolio_category); $i++) { ?>
                                    <option value="<?php echo $all_portfolio_category[$i]['id'] ?>" <?php echo ($selected_portfolio_category['id'] == $all_portfolio_category[$i]['id']) ? 'selected' : ''; ?>><?php echo $all_portfolio_category[$i]['name'] ?></option>
                                <?php } ?>
                            </select>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                        <label class="form-group">
                            <p class="label-txt">Position</p>
                            <input type="number" name="position" class="input select-d" value="<?php echo isset($selected_portfolio_category['position']) ? $selected_portfolio_category['position'] : ''; ?>" required/>
                            <div class="line-box">
                                <div class="line"></div>
                            </div>
                        </label>
                        <!-- <div class="form-group goup-x1">
                                <textarea name="body" id="txtEditor"><?php echo $portfoliodata[0]['body']; ?></textarea>
                        </div> -->
                    </div>
                    <div class="col-md-6">
                        <div class="form-group goup-x1">
                            <label class="form-group">
                                <p class="label-txt">Blog Thumbnail</p>
                            </label>
                        </div>
                        <div class="blogPic" >
                            <div class="accimgbx33">
                                <img class="blogimage" src="<?php echo FS_PATH_PUBLIC_UPLOADS_PORTFOLIOS . $portfoliodata[0]['image']; ?>">
                            </div>
                            <a class="icon-crss-2 remove_selected_blogimg"><i class="icon-gz_delete_icon"></i></a>
                        </div>
                        <div class="bl-ogaddt-est blogimgadd" style="display: none;">
                            <span class="bl-ogaddt-est-a2">
                                <span class="bl-ogaddt-est-a33">+</span>
                            </span>
                            Add Image
                            <input class="file-input" name="protfolio_file" type="file" onchange="validateAndUpload(this);" accept="image/*">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input id="pic" type="hidden" class="blogimg_hidden">
                        <p class="btn-x bl-ogbtn text-right"><button type="submit" id="save" class="load_more button">Publish</button> <!-- <button id="preview" data-toggle="modal" data-target="#portfoliopreview" class="btn-g text-uppercase">preview</button> --></p>
                    </div>
                </div>	
            </form>
        </div>

    </div>
</div>
</div>

</div>
</div>
</div>
</section>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/bootstrap.min.js');?>"></script>
<script type="text/javascript">
//$(document).on('click', "#menuBarDiv_txtEditor .btn-group a", function () {
//    $(this).closest('.btn-group').toggleClass('open');
//});
//function validateAndUpload(input) {
//    var URL = window.URL || window.webkitURL;
//    var file = input.files[0];
//    if (file.type.indexOf('image/') !== 0) {
//        this.value = null;
//        console.log("invalid");
//    }
//    else {
//        if (file) {
//            console.log(URL.createObjectURL(file));
//            $(".blogimgadd").hide();
//            $(".blogPic").css('display', 'block');
//            $(".blogPic .blogimage").attr('src', URL.createObjectURL(file));
//            $('.blogimg_hidden').val(URL.createObjectURL(file));
//        }
//    }
//}
//$('.remove_selected').click(function (e) {
//    e.preventDefault();
//    $(this).closest('.blogPic').hide();
//    $('.blogimgadd').show();
//    $('.blogimgadd input').val('');
//    $('.blogimgadd input').attr('required', "required");
//});
//$('#preview').click(function (e) {
//    e.preventDefault();
//    $('#txtEditor').text($('#txtEditor').Editor("getText"));
//    var title = $("#title").val();
//    var cate = $("#category").val();
//    var content = $("#txtEditor").val();
//    var image = $("#pic").val();
//    $("#heading_title").html(title);
//    if (image != "") {
//        $('.blogimg').html('<img src="' + image + '">');
//    }
//    $('.blogcontent p').html(content);
//    $('.precate_in').html(cate);
//});
</script>