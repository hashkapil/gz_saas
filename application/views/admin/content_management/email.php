<section class="con-b">
    <div class="container-fluid">
        <?php if ($this->session->flashdata('message_error') != '') { ?>    
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>    
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>    
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
            </div>
        <?php } ?>
        <div class="header-blog">
            <div class="row flex-show">
                <div class="col-md-12">
                    <div class="flex-this">
                        <h2 class="main_page_heading">Email Setting</h2>
                        <div class="header_searchbtn">
                            <a href="javascript:void(0)" class="edit_email_temp header_footer_btn adddesinger" data-id="<?php echo $email_header_footer_sec['header_part_id']; ?>"> Header</a>
                            <a href="javascript:void(0)" class="edit_email_temp header_footer_btn addderequesr" data-id="<?php echo $email_header_footer_sec['footer_part_id']; ?>"> Footer</a>
                            <!--                        <div class="search-first">
                                                         <div class="focusout-search-box">
                                                     <div class="search-box">
                                                        <form method="post" class="search-group clearfix">
                                                            <input type="text" placeholder="Search here..." class="form-control searchdata" id="search_text">
                                                            <input type="hidden" name="status" id="status" value="blog">
                                                            <button type="submit" class="search-btn search search_data_ajax">
                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                                    </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="Admin_Email">
            <div class="" id="email_setting_list">
                <div class="row">
                    <div class="col-md-12">
                        <div class="managment-list">
                            <?php for ($i = 0; $i < sizeof($emailslist); $i++) {
                                ?>
                                <ul>
                                    <li class="usericon">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/msg-icon-bg.svg" class="img-responsive">
                                    </li>
                                    <li class="username">
                                        <div class="email_title">
                                            <label class="title"><?php echo $emailslist[$i]['email_name']; ?></label>
                                            <p class="brief_intro"> <?php echo $emailslist[$i]['email_description']; ?></p>
                                        </div>
                                    </li>
                                    <li class="three-icon">
                                        <div class="c">
                                            <h3 class="pro-head space-b" style="cursor: pointer; text-align: right">
                                                <a href="javascript:void(0)" class="edit_email_temp" data-id="<?php echo $emailslist[$i]['id']; ?>"> 
                                                    <span><i class="icon-gz_edit_icon"></i></span>
                                                </a>
                                            </h3>
                                        </div>
                                        <div class="email_preview">
                                            <div data-toggle="modal" data-target="#emailPreview" class="emailPreview" data-id="<?php echo $emailslist[$i]['id']; ?>">
                                                <div class="email_header_temp_cont" style="display:none"><?php echo $email_header_footer_sec['header_part']; ?></div>
                                                <div class="email_body_cont" style="display:none"><?php echo $emailslist[$i]['email_text']; ?></div>
                                                <div class="email_footer_temp_cont" style="display:none"><?php echo $email_header_footer_sec['footer_part']; ?></div>
                                                <span><i class="fas fa-eye"></i></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="modal fade new-test-email  edit_single_email_temp in" id="email_edit_temp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
                    <!-- <div class="edit_single_email_temp" style="display:none"> -->
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>Edit Email Template</h3>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="email_preview single_edit_action_btns">
                                <div data-toggle="modal" data-target="#Preview" class="emailPreview" data-id="">
                                    <div class="inneremail_header_temp_cont" style="display:none"><?php echo $email_header_footer_sec['header_part']; ?></div>
                                    <div class="email_body_cont" style="display:none"></div>
                                    <div class="inneremail_footer_temp_cont" style="display:none"><?php echo $email_header_footer_sec['footer_part']; ?></div>
                                </div>
                            </div>
                            <form action="<?php echo base_url(); ?>admin/Emailcontent/editemail" method="post" enctype="multipart/form-data">
                                <div class="fo-rm-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label class="form-group">
                                                <p class="label-txt label-active">Title</p>
                                                <input type="hidden" class="input input-d" name="id" id="email_id" value="">
                                                <input type="hidden" class="input input-d" name="user_id" id="user_id" value="">
                                                <input type="text" class="input input-d" name="email_name" id="email_name" value="">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                            <label class="form-group hide_subjecttoheaderfooter">
                                                <p class="label-txt label-active">Subject</p>
                                                <input type="text" class="input input-d" id="subject" name="subject" value="">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>

                                            <div class="form-group goup-x1">
                                                <textarea id="txtEditor" name="description"></textarea>
                                            </div>

                                            <div class="form-group goup-x1">
                                                <div class="email-footer-btn">
                                                    <button type="submit" name="submit" id="save"  class="save-publish btn-red">Publish</button>
                                                    <h3 class="space-b reset_email_template " style="cursor: pointer; text-align: center"></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>





        <!-- Model for email preview -->
        <div class="modal fade slide-3 email-popups model-close-button in" id="emailPreview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

                    </div>
                    <div class="email_preview_section">
                        testing area 
                    </div>

                    <p class="space-b"></p>

                </div>

            </div>
        </div>
    </div>
</section>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/jquery/dist/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/qa/bootstrap.min.js"></script>
<script type="text/javascript">

    /**************code for edit email templates*******************/

    $(document).on('click', '.edit_email_temp', function (e) {
        e.preventDefault();
        $('.input').parent().find(".label-txt").addClass('label-active');
        var editid = $(this).attr('data-id');
        $('.email_preview_lst').hide();
        $('.edit_single_email_temp').modal('show');
        $('.reset_email_template').html("");
        $('html, body').animate({
            scrollTop: $('#Admin_Email').offset().top - 20
        }, 'slow');
//        $('.input').each(function () {
//            if ($(this).val() != '') {
//                $(this).parent().find(".label-txt").addClass('label-active');
//            } else {
//                $(this).parent().find(".label-txt").removeClass('label-active');
//            }
//        });
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "<?php echo base_url(); ?>admin/Emailcontent/editemail",
            data: {"id": editid},
            success: function (data) {
                var email_info = data[0];
                // console.log('return',email_info);
                //console.log('user_id', email_info.user_id);
                $('#email_id').val(email_info.id);
                $('#user_id').val(email_info.user_id);
                $('#email_name').val(email_info.email_name);
                $('#identifier').val(email_info.identifier);
                $('#subject').val(email_info.email_subject);
                $('#txtEditor').text(email_info.email_text);
                var content = $('#txtEditor').val();
                $('.Editor-editor').html(content);
                if (email_info.user_id != 0) {
                    $('.reset_email_template').html('<a class="reset_default" href="<?php echo base_url(); ?>customer/Emailcontent/delete_emailtemp/' + email_info.id + '">Reset Default</a>');
                }
            }
        });
    });



    $(document).on('click', '.emailPreview', function () {
        var email_id = $(this).attr('data-id');
        var email_header = $(this).find('.email_header_temp_cont').html();
        var email_body = $(this).find('.email_body_cont').html();
        var email_footer = $(this).find('.email_footer_temp_cont').html();
        var res = email_body.replace("{{MESSAGE}}", "<div class='left_message_padding'>{{MESSAGE}}</div>");
        console.log('email_id', email_id);
        console.log('email_body', res);
        $('.email_preview_section').html("");
        if (email_body != '') {
            $('.email_preview_section').html(email_header + res + "<div class='email_footer_padding'>" + email_footer + "</div>");
        }

    });
    $(document).on('click', '.close_edit_email_templt', function () {
        $('.email_preview_lst').show();
        $('.edit_single_email_temp').hide();
        $('.header_footer_btn').removeClass('active');

    });
    $('.header_footer_btn').click(function () {
        $(this).addClass('active').siblings().removeClass('active');
    });
    /**************end code for edit email templates*******************/
</script>
