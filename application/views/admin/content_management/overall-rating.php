<?php //echo "<prE>"; print_r($info); exit; ?> 
<section class="con-b">
    <div class="container-fluid">

        <div class="matrix-section overall-sorting-table">

            <div class="all-feedback-colmn">
                <div class="overall-heading">
                        <h2>overall customers rating</h2>  
                        <a class="backbtn" href="<?php echo base_url();?>admin/Contentmanagement/feedback_matrix"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                        <div class="overall_date_pick">
                          
                            <div class='input-group date' id='datetimepicker1'>
                               
                              <span id="dateRange1"  data-day="<?php echo date('d', strtotime("last week monday"));  ?>" data-start="<?php echo date("Y-m-d", strtotime("last week monday"));  ?>"> <?php echo date('M d', strtotime("last week monday"));  ?></span>
                              <span class='space'> - </span>
                              <span id="dateRange2" data-day="<?php echo date('d', strtotime("last sunday"));  ?>" data-end="<?php echo date("Y-m-d", strtotime("last sunday"));  ?>"><?php echo date('M d', strtotime("last sunday"));  ?></span>
                              
                            </div>
                            <div class="week-picker hide"></div>
                        </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="feedback-item">
                            
                            <table class="feedback-col-table">
                                <thead>
                                    <tr>
                                        <th>Design Category</th>
                                        <th>Design Sub-category</th>
                                        <th>No Of Designs </th>
                                        <th>Avg. rating</th>
                                         
                                    </tr>
                                </thead>
                                <tbody id="OverAll" data-drafts="<?php echo count($info);?>">
                                   <?php foreach ($info as $k => $user_info) { ?> 
                                    <tr>
                                        <td> <?php echo $user_info['parent_cat_name'];?></td>
                                        <td> <?php echo $user_info['sub_cat_name'];?></td>
                                        <td> <?php echo $user_info['total_drafts'];?></td>
                                        <td><img src="<?php echo base_url();?>public/assets/img/customer/<?php echo $user_info["svg"];?>">  <?php echo number_format($user_info['total_rating'],2,'.','');?></td> 
                                    </tr>
                                 
                                    <?php
                                        $totl += $user_info['total_drafts'];
                                        $avgRating += $user_info['total_rating'] * $user_info['total_drafts'];
                                        $resultLoad = $k;
                                    }
                                    ?>
                                </tbody>
                                  <tfoot>
                                    <tr>
                                         <td id="totalCount" data-count="<?php echo $totl; ?>"><strong>Total &nbsp;</strong></td>
                                         <td></td>
                                         <td id="totalCounts"><strong> <?php echo $totl; ?></strong></td>
                                         <td id="totalRating"><strong><?php echo number_format($avgRating/$totl,2,'.','');?> </strong></td>
                                        
                                    </tr>
                                </tfoot>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo base_url();?>public/assets/img/gz-ajax-loader.gif" />
 </div>
    <!--div class="" style="text-align:center">
      <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $resultLoad; ?>" class=" load_more button">Load more</a>
    </div-->
    
</section>
 
 
