<section class="con-b billing_subs">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?php if ($this->session->flashdata('message_error') != '') { ?>    
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_error'); ?></strong>    
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('message_success') != '') { ?>    
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                    </div>
                <?php } ?>
               <div class="flex-this">
                    <h2 class="main_page_heading">Billing and Subscription</h2>
                </div>
                <br/>
            </div>
        </div>
        
        <br/>
        <div class="product-list-show">
            <div class="cli-ent-row tr tableHead">
                <div class="cli-ent-col td" style="width: 20%;">
                    NAME
                </div>
                <div class="cli-ent-col td" style="width: 10%;">
                    Price
                </div>
                <div class="cli-ent-col td text-center" style="width: 15%;">
                    Active Request
                </div>
                <div class="cli-ent-col td text-center" style="width: 15%;">
                    Request per month
                </div>
                <div class="cli-ent-col td text-center"style="width: 15%;">
                    turnaround days
                </div>
                <div class="cli-ent-col td"style="width: 15%;">
                    Active/Inactive
                </div>
                <div class="cli-ent-col td"style="width: 10%;">
                    Action
                </div>
            </div>

            <?php foreach ($subscriptionplan as $plans) { ?>
                <div class="cli-ent-row tr brdr">
                    <div class="mobile-visibles">
                        <i class="fa fa-plus-circle"></i>
                    </div>
                    <!---Start Title---->
                    <div class="cli-ent-col td client-project" style="width: 20%; ">
                        <div class="cli-ent-xbox text-left">
                            <div class="cell-row">
                                <div class="cell-col" >
                                    <div class="name-discp">
                                        <h3 class="pro-head space-b">
                                            <?php echo $plans['plan_name']; ?>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!---End Title---->

                    <!---Start Client name---->
                    <div class="cli-ent-col td" style="width: 10%;">
                        <span class="small">&nbsp;$&nbsp;</span>
                        <p><?php echo $plans['plan_price']; ?></p>
                        <span class="small">&nbsp;
                            <?php
                            if ($plans['plan_type'] == "Annual") {
                                echo "/year";
                            } elseif ($plans['plan_type'] == "month") {
                                echo "/month";
                            }
                            ?>
                        </span>
                    </div>
                    <!---End Client---->

                    <!---Start Designer name---->
                    <div class="cli-ent-col td text-center" style="width: 15%;">
                        <p><?php echo $plans['global_active_request']; ?></p>
                    </div>
                    <!---End Designer name---->

                    <!---Start Status---->
                    <div class="cli-ent-col td text-center" style="width: 15%;">
                        <p><?php echo $plans['active_request']; ?></p>
                    </div>
                    <!---End Status---->



                    <div class="cli-ent-col td text-center" style="width: 15%;">
                        <p><?php echo $plans['turn_around_days']; ?></p>
                    </div>
                    
                    <div class="cli-ent-col td text-center" style="width: 15%;">
                        <p><?php if($plans['is_active'] == 1){
                            echo "Active";
                        }else{
                            echo "Inactive";
                        }
                        ?></p>
                        
                    </div>
                    <!----verified tab end-->

                    <!----Action tab start-->
                    <div class="cli-ent-col td" style="width: 10%;">
                        <div class="cli-ent-xbox action-per">
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#edit_plan<?php echo $plans['id']; ?>"><i class="icon-gz_edit_icon"></i></a>
                        </div>
                    </div>
                    <!----Action tab End-->

                </div>
                <!-- Modal -->
                <div class="modal similar-prop nonflex fade" id="edit_plan<?php echo $plans['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="cli-ent-model-box">
                                <form method="post" action="<?php echo base_url(); ?>admin/contentmanagement/edit_plan/<?php echo $plans['id']; ?>" onsubmit="return confirm('Do you really want to submit the form?');">
                                    <header class="fo-rm-header">
                                        <h2 class="popup_h2 del-txt">Edit Billing and Subscription Plan</h2>
                                        <div class="close" data-dismiss="modal" aria-label="Close"> x </div>
                                    </header>
                                     <input name="plan_id" type="hidden" class="input plan_title" value="<?php echo $plans['plan_id']; ?>">
                                    <div class="cli-ent-model">
                                        <div class="fo-rm-body">                        
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php echo ($plans['plan_type'] != '') ? 'label-active' : ''; ?>">Plan Title</p>
                                                        <input name="plan_name" type="text" class="input plan_title" value="<?php echo $plans['plan_name']; ?>" readonly="">
                                                        <div class="line-box"><div class="line"></div></div>
                                                    </label>
                                                    <label class="form-group">
                                                        <p class="label-txt <?php echo ($plans['plan_type'] != '') ? 'label-active' : ''; ?>">Select subscription plan</p>
                                                        <input name="plan_type" type="text" class="input plan_price" value="<?php echo $plans['plan_type']; ?>" readonly="">
                                                        <div class="line-box"><div class="line"></div></div>
                                                    </label>
                                                    <label class="form-group">
                                                        <p class="label-txt <?php echo ($plans['plan_price'] != '') ? 'label-active' : ''; ?>">Price</p>
                                                        <input name="plan_price" type="text" class="input plan_price" value="<?php echo $plans['plan_price']; ?>" readonly="">
                                                        <div class="line-box"><div class="line"></div></div>
                                                    </label>
                                                    <label class="form-group">
                                                        <p class="label-txt <?php echo ($plans['global_active_request'] != '') ? 'label-active' : ''; ?>">Active Request</p>
                                                        <input type="number" min="1" class="input" name="global_active_request" value="<?php echo isset($plans['global_active_request']) ? $plans['global_active_request'] : ''; ?>"/>
                                                        <div class="line-box"><div class="line"></div></div>
                                                    </label>
                                                    <label class="form-group">
                                                        <p class="label-txt <?php echo ($plans['global_inprogress_request'] != '') ? 'label-active' : ''; ?>">In Progress Request</p>
                                                        <input type="number" min="1" class="input" name="global_inprogress_request" value="<?php echo isset($plans['global_inprogress_request']) ? $plans['global_inprogress_request'] : ''; ?>"/>
                                                        <div class="line-box"><div class="line"></div></div>
                                                    </label>
                                                    <label class="form-group text-left">
                                                        <p class="label-txt <?php echo ($plans['active_request'] != '') ? 'label-active' : ''; ?>">Request per month</p>
                                                        <input name="active_request" type="number" min="-1" class="input plan_title" value="<?php echo $plans['active_request']; ?>" <?php echo (in_array(($plans['plan_id']), UNLIMITED_REQUESTS_PLANS)) ? 'readonly' : ''; ?>>
                                                        <div class="line-box"><div class="line"></div></div>
                                                        <p class="note_req"><strong>Note:</strong> <span>Keep -1</span>, To allow user to add unlimited requests.</p>
                                                    </label>
                                                    <label class="form-group">
                                                        <p class="label-txt <?php echo ($plans['turn_around_days'] != '') ? 'label-active' : ''; ?>">Turn around days</p>
                                                        <input type="number" class="input" min="1" name="turn_around_days" value="<?php echo isset($plans['turn_around_days']) ? $plans['turn_around_days'] : ''; ?>"/>
                                                        <div class="line-box"><div class="line"></div></div>
                                                    </label>
                                                    <label class="form-group isadd_subuser">
                                                            <p class="label-txt <?php echo ($plans['total_sub_user'] != '') ? 'label-active' : ''; ?>">User Management Setting</p>
                                                            <input type="number" class="input" min="-1" name="how_many_user" value="<?php echo isset($plans['total_sub_user']) ? $plans['total_sub_user'] : ''; ?>"/>
                                                            <div class="line-box"><div class="line"></div></div>
                                                            <p class="note_req"><strong>Note:</strong> <span>Keep -1</span>, To allow user to add unlimited users.
                                                                <span>Keep 1 to n number</span>, To allow user to add specific number of users.
                                                                <span>Keep 0</span>, To hide user management section to users.</p>
                                                    </label>
                                                    <label class="form-group isadd_brand_profile">
                                                            <p class="label-txt <?php echo ($plans['brand_profile_count	'] != '') ? 'label-active' : ''; ?>">Brand Profile Setting</p>
                                                            <input type="number" class="input" min="-1" name="how_many_brandpro" value="<?php echo isset($plans['brand_profile_count']) ? $plans['brand_profile_count'] : ''; ?>"/>
                                                            <div class="line-box"><div class="line"></div></div>
                                                            <p class="note_req"><strong>Note:</strong> <span>Keep -1</span>, To allow user to add unlimited brand profile.
                                                                <span>Keep 1 to n number</span>, To allow user to add specific number of brand profile.
                                                                    <span>Keep 0</span>, To not allow to users to add brand profile.</p>
                                                    </label>
                                                    <label class="form-group file_management">
                                                            <p class="label-txt label-active">File Management</p>
                                                            <select class="input select-c" name="file_management">
                                                                    <option value="1" <?php echo ($plans['file_management'] == 1) ? 'selected' : ''; ?>>Show</option>
                                                                    <option value="0" <?php echo ($plans['file_management'] == 0) ? 'selected' : ''; ?>>Hide</option>
                                                            </select>
                                                            <div class="line-box"><div class="line"></div></div>
                                                    </label>
                                                    <label class="form-group file_management">
                                                            <p class="label-txt label-active">File Sharing</p>
                                                            <select class="input select-c" name="file_sharing">
                                                                    <option value="1" <?php echo ($plans['file_sharing'] == 1) ? 'selected' : ''; ?>>Show</option>
                                                                    <option value="0" <?php echo ($plans['file_sharing'] == 0) ? 'selected' : ''; ?>>Hide</option>
                                                            </select>
                                                            <div class="line-box"><div class="line"></div></div>
                                                    </label>
                                                    <label class="form-group agency_setting">
                                                            <p class="label-txt label-active">Agency Setting</p>
                                                            <select class="input select-c" name="is_agency">
                                                                    <option value="1" <?php echo ($plans['is_agency'] == 1) ? 'selected' : ''; ?>>Show</option>
                                                                    <option value="0" <?php echo ($plans['is_agency'] == 0) ? 'selected' : ''; ?>>Hide</option>
                                                            </select>
                                                            <div class="line-box"><div class="line"></div></div>
                                                    </label>
                                                    <label class="form-group">
                                                        <p class="label-txt label-active">Overwirte User Level Setting </p>
                                                        <select class="input select-c" name="overwite_setting">
                                                            <option value="0">No</option>
                                                            <option value="1">Yes</option>
                                                        </select>
                                                        <div class="line-box"><div class="line"></div></div>
                                                    </label>
                                                    
                                                </div>
                                            </div>
                                            <p class="space-b"></p>
                                            <p class="btn-x">
                                                <button type="submit" class="load_more button">Edit Plan</button>
                                            </p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
</div>
</section>