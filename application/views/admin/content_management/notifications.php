<?php //echo "<pre/>";print_R($notifications);   ?>
<section class="con-b">
    <div class="container-fluid">
        <div class="flex-this">
            <h2 class="main_page_heading">Notifications</h2>
        </div>
        <div class="notification-box">
            <ul class="list-unstyled list-notificate">
                <?php foreach ($notifications as $nk => $nv) {
//                    echo "<pre/>";print_R($nv);
                    ?>
                    <li>
                        <a href="<?php echo base_url().$nv['url']; ?>">
                            <div class="setnoti-fication">
                                <figure class="pro-circle-k1">
                                    <img src="<?php echo $nv['profile_picture']; ?>" class="img-responsive">
                                </figure>
                                <div class="notifitext">
                                    <p>
                                        <strong><?php echo $nv['first_name'] . ' ' . $nv['last_name']; ?><?php if($nv['shown'] == 0){ ?><span class="unread-noti">New</span><?php } ?></strong>
                                        <span class="time-date">
                                            <?php echo date('M d, Y H:i A', strtotime($nv['created'])); ?> 
                                        </span>  
                                    </p>
                                    <p><?php echo $nv['title']; ?></p>
                                </div>
                            </div>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <div class="col-md-12">
            <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" />
            </div>
            <div class="" style="text-align:center">
                <a href="javascript:void(0)" data-row='0' data-group='1' data-loaded="<?php echo LIMIT_ADMIN_LIST_COUNT; ?>" data-count="<?php echo $countnotifications; ?>" class="load_more notifi_loading button mb-20"> Load More</a>
            </div>
        </div>
    </div>
</section>