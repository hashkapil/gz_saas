<?php //echo "<pre/>";print_r($selecteddata); ?>
<section class="con-b">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="cli-ent-model-box">
                    <div class="cli-ent-model">
                        <p class="savecols-col left"><a href="<?php echo base_url(); ?>admin/Contentmanagement" class="back-link-xx0 text-uppercase">Back</a></p>
                        <p class="space-d"></p>
                        <header class="fo-rm-header">
                            <h3 class="sub-head-i">Edit Email Template</h3>
                        </header>
                        <form action="" method="post" enctype="multipart/form-data">
                        <div class="fo-rm-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Title</label>
                                        <p class="space-a"></p>
                                        <input type="text" class="form-control input-d" name="email_name" value="<?php echo isset($selecteddata[0]['email_name'])?$selecteddata[0]['email_name']:'';?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group goup-x1">
                                <label class="label-x3">Subject</label>
                                <p class="space-a"></p>
                                <textarea class="form-control textarea-c" name="subject"><?php echo isset($selecteddata[0]['email_subject'])?$selecteddata[0]['email_subject']:'';?></textarea>
                            </div>

                            <div class="form-group goup-x1">
                                <textarea id="txtEditor" name="description"><?php echo isset($selecteddata[0]['email_text'])?$selecteddata[0]['email_text']:'';?></textarea>
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x3">Email image</label>
                                <p class="space-b"></p>
                                <div class="cell-row">
                                    <div class="cell-col">
                                        <?php if($selecteddata[0]['email_img']){ ?>
                                        <div class="blogPic" >
                                                <div class="accimgbx33">
                                                        <img class="blogimage" src="<?php echo FS_PATH_PUBLIC_UPLOADS.'emailtemplate_img/'.$selecteddata[0]['email_img'];?>" style="object-fit: unset;">
                                                        <a href="javascript:void(0)" class="icon-crss-2 remove_selected" data-id="<?php echo $selecteddata[0]['id'];?>"  data-name="<?php echo $selecteddata[0]['email_img'];?>"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/admin/icon-crss-2.png" class="img-responsive"></a>
                                                </div>
                                        </div>
                                        <?php }else{ ?>    
                                        <div class="bl-ogaddt-est blogimgadd">
                                                <span class="bl-ogaddt-est-a2">
                                                        <span class="bl-ogaddt-est-a33">+</span>
                                                </span>
                                                Add Image
                                                <input class="file-input" name="blog_file" type="file" onchange="validateAndUpload(this);" accept="image/*">
                                        </div>
                                        <?php } ?>
                                </div>
                                    <div class="cell-col">
                                        <p class="btn-x bl-ogbtn text-right">
                                        <button type="submit" name="submit" id="save"  class="btn-g text-uppercase">Publish</button></p>
                                    </div>
                                </div>
                            </div>

                            <p class="space-b"></p>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>