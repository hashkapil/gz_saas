<?php
if($blog['title'] != ''){
    if (strlen($blog['title']) > 25) {
        $title = substr($blog['title'], 0, 15) . " ...";
    } else {
       $title = $blog['title'];
    }
?>
<div class="col-lg-3 col-md-4 col-sm-6 blog_grid">
    <figure class="por-tfolioimg">
        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_BLOGS . $blog['image']; ?>" class="img-responsive">
    </figure>
    <div class="bl-ogbox por-tfol-io">
        <h3 class="bl-oghead" title="<?php echo $blog['title']; ?>"><?php echo $title; ?></h3>
        <span class="bl-ogaction">
            <a href="<?php echo base_url(); ?>admin/contentmanagement/blog_edit/<?php echo $blog['id']; ?>"><i class="icon-gz_edit_icon"></i></a>
            <a href="javascript:void(0)" class="delete_blog" data-delID="<?php echo $blog['id']; ?>"><i class="icon-gz_delete_icon"></i></a>
        </span>
        <?php if ($blog['tags'] != '') { ?>
            <p class="text-b tags" title="<?php echo $blog['tags']; ?>">
                Tags : <?php
                if (strlen($blog['tags']) > 25) {
                    echo $name = substr($blog['tags'], 0, 20) . " ...";
                } else {
                    echo $name = $blog['tags'];
                }
                ?>
            </p>
        <?php } ?>
        <?php if ($blog['category'] != '') { ?>
            <p class="category_nm">Category : <span class="catname"> <?php echo ($blog['category'] != "") ? $blog['category'] : ''; ?></span></p>  
        <?php } ?>
<!--        <p class="text-b minone">
            <?php 
            //if(strlen($blog['body']) > 250){
              //echo substr(strip_tags($blog['body']),0,10);
            //}else{
             // echo strip_tags($blog['body']);
            //}?>
        </p>-->
        <div class="cell-row">
            <p class="pro-b"><?php echo date("M /d /Y ", strtotime($blog['created'])); ?></p>
        </div>
    </div>
</div>
<?php } ?>