	<?php /* ?>
	<a href="<?php echo base_url()."admin/accounts/view_designers";?>"><h2 class="float-xs-left content-title-main" style="color:#cac8c8;display:inline-block;">Designers</h2></a>
	<h2 class="float-xs-left content-title-main" style="padding-left:10%;display:inline-block;">QA / VA</h2>
	<?php */ ?>

</div>
<div class="col-md-12" style="background:white;">
<div class="">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147 !important; } 
.darkblackbackground { background-color:#1a3147; }
.pinktext { color: #ec4159; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.font16 { font-size:16px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
	font-size: 31px !important;
    padding: 8px 0px !important;
    font-weight: 600 !important;
    letter-spacing: -3px !important;
}
.projecttitle{
	font-size: 25px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: none;
    background: unset;
    
}
.trborder:hover{
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
.table-hover tbody tr:hover{
	background-color:#fff;
}
table {border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}

#content-wrapper .content{padding-top: 0;}

/*
.nav-tab-pills-image ul li .nav-link {
    color:#1a3147;
	font-weight:600;
	padding: 5px 35px;
}
.content .nav-tab-pills-image .nav-item.active a {
border:unset !important;
}
.wrapper .nav-tab-pills-image ul li .nav-link:hover{
	color:#000;
}

.wrapper .nav-tab-pills-image ul li .nav-link{
	color: #2f4458;
	height:43px;
}
.nav-tab-pills-image ul li .nav-link:hover {
    border-bottom: none !important;
}
.wrapper .nav-tab-pills-image ul li .nav-link:focus
{	
	color:#fff;
	border:none;
}
.content .nav-tab-pills-image .nav-item.active a:hover{
	color:#fff;
}
.content .nav-tab-pills-image .nav-item.active dd {
	background:unset !important;
}
.nav-tab-pills-image ul li .nav-link:hover {
    border-bottom: none !important;
}
*/

.wrapper .nav-tab-pills-image ul li .nav-link {
    padding: 30px 15px;
    font-size: 18px;
}
.wrapper .nav-tab-pills-image ul li.float-right .nav-link{
	padding: 24px 15px;
}

.font14{ font-size:14px; }
</style>
    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="nav-tab-pills-image mytab">
					
						<ul class="nav nav-tabs" role="tablist" style="border:none;">
							<li class="nav-item active">
								<a class="nav-link" data-toggle="tab" href="#qa" role="tab">
									Quality Assurance
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#va" role="tab">
									Virtual Assistant
								</a>
							</li>

							<li class="nav-item float-right">
								<p class="nav-link dd">
									<i class="fa fa-search onlysearch" style="padding-left:12px;padding-top:8px;"></i>
									<input class="fbold" type="text" id="myInput" onkeyup="myFunction()" name="search_request" placeholder="Search" style="border:none;">
								</p>
							</li>

							<li class="nav-item float-right">
								<p class="nav-link dd">
									<button class="add_custom_btn" data-toggle="modal" data-target="#add_designer">+ Add QA/VA</button>									
								</p>
							</li>
						</ul>

						<!-- <input type="button" class="btn weight600 darkblacktext" style="border-radius:5px;background:#fff;float: right;padding:5px 20px;margin-left: 20px;border:2px solid" value="+ Add QA/VA"  data-toggle="modal" data-target="#add_designer"/> -->
											
                    <div class="tab-content">
                        <div class="tab-pane active content-datatable datatable-width" id="qa" role="tabpanel">
                            <div class="row">
								  <div class="col-md-10 offset-md-1">
								 <?php if($this->session->flashdata('message_error') != '') {?>				
							   <div class="alert alert-danger alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
									<strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
								</div>
							   <?php }?>
							   <?php if($this->session->flashdata('message_success') != '') {?>				
							   <div class="alert alert-success alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
									<strong><?php echo $this->session->flashdata('message_success');?></strong>
								</div>
							   <?php }?>

								<div class="table_draft_sec table_draft_complete customers_table_sec qa_client_table">
								<table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive dataTable" style=" border-collapse:separate; border-spacing: 0 1em;border:none; padding-top: 15px;" id="DataTables_Table_0">
								    <tbody class="">
								    <?php
								    //echo "<pre>"; print_r($qa); echo "</pre>"; 
								    for($i=0;$i<sizeof($qa);$i++){ ?>            
								        <tr onclick="" class="trborder" id="40" data-indexid="0">
								            <td class="completed image_dv image_dv1">
									            <?php
									            if ($qa[$i]['profile_picture']) { ?>
									            <div class="img">
								                  <img class="image_circle" src="<?php echo base_url(); ?>public/profile/<?php echo $qa[$i]['profile_picture']; ?>">
								                </div>         
									            <?php }
									            else {  ?>
									            <div class="img">
								                  <img class="image_circle" src="<?php echo base_url(); ?>uploads/profile_picture/13b2d60af0a45fefd0d0d0633339a453.jpeg">
								                </div>
									            <?php
									            }
									            ?>
								            </td>
								            <td class="desc">
								                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $qa[$i]['first_name']." ".$qa[$i]['last_name']; ?></p>
								                <p><span class="">Member since <?php echo date("F Y",strtotime($qa[$i]['created'])); ?></span></p>
								            </td>
								                        
								            
								            <td class="requests">
								                <div class="text">No. of Requests</div>
								                <div class="num"><?php  echo $qa[$i]['total_clients']; ?></div>
								            </td>

								            <td class="requests active_req">
								                <div class="text">Active Requests</div>
								                <div class="num"><?php  echo $qa[$i]['total_designer']; ?></div>
								            </td>
								        </tr>
								    <?php } ?>
								    </tbody>
								</table>
								</div>
									<?php /*
                                     <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive">
                                       
                                        <tbody>
										<?php for($i=0;$i<sizeof($qa);$i++){ ?>
										   <tr class="trborder">
												<td style="width: 500px;">
													<center>
														 <div class="col-sm-2" style="padding:0px;">
															 
															<?php
															if ($qa[$i]['profile_picture']) 
															{
															?>
															 <div class="myprofilebackground" ><p style="padding-top: 13px;">
															 <?php
																echo '<img src="' . base_url() . 'public/profile/' . $qa[$i]['profile_picture'] . '" style=" height:60px;width:60px; border-radius:50%;border: 3px solid #0190ff;margin-left:0px;">';
															
															?>
															 </p></div>         
															<?php  
															}
															else 
															{
															?>
															<div class="myprofilebackground" style="width:60px; height:60px;border-radius:50%; background: #0190ff;border: 3px solid #0190ff; text-align: center; font-size: 15px; color: #fff;"><p style="font-size: 22px;padding-top: 15px;">
																	
															<?php 
																
																echo ucwords(substr($qa[$i]['first_name'],0,1)) .  ucwords(substr($qa[$i]['last_name'],0,1));
															?>
																</p></div>
															<?php
															}
															?>
														</div>
                                                    </center>
													<div style="display:inline-block;width:80%;text-align: left;">
														<h4 class="darkblacktext" style="display: inline-block;font-size: 17px;font-weight: 600;padding-left: 20px;"><?php echo $qa[$i]['first_name']." ".$qa[$i]['last_name']; ?> </h4></br>
														<p class="greytext" style="display: inline-block;font-size: 12px;font-weight: 100;padding-left: 20px;">Member since <?php echo date("M d, Y",strtotime($qa[$i]['created'])); ?></p>
													</div>													
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Lorem Ipsum</p>
													<p class="darkblacktext weight600">12</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Rating</p>
													<i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star"></i>
												</td>
												<!--<td>
													<p class="greytext pb0 weight600 font18 directmessage" style="display: inline-block;margin-left: 5px;" data-clientname="James Franco" data-clientid="3" data-imageurl="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;">Direct Message</p>
													<p><svg style="vertical-align: sub;width: 0.5em;" aria-hidden="true" data-prefix="fas" data-icon="comment-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt fa-w-16 fa-2x" ><path fill="orange" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z" class=""></path></svg></p>
												</td>-->
											</tr> 
										<?php } ?>
											
										</tbody>
									</table>
									*/ ?>
									 
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="va" role="tabpanel">
                            <div class="row">

                                <div class="col-md-10 offset-md-1">
									<div class="table_draft_sec table_draft_complete customers_table_sec qa_client_table">
									<table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive dataTable" style=" border-collapse:separate; border-spacing: 0 1em;border:none; padding-top: 15px;" id="DataTables_Table_0">
									    <tbody class=""> 
									    <?php for($i=0;$i<sizeof($va);$i++){ ?>           
									        <tr onclick="" class="trborder" id="40" data-indexid="0">
									            <td class="completed image_dv image_dv1">
									                <?php
										            if ($va[$i]['profile_picture']) { ?>
										            <div class="img">
									                  <img class="image_circle" src="<?php echo base_url(); ?>public/profile/<?php echo $va[$i]['profile_picture']; ?>">
									                </div>         
										            <?php }
										            else {  ?>
										            <div class="img">
									                  <img class="image_circle" src="<?php echo base_url(); ?>uploads/profile_picture/13b2d60af0a45fefd0d0d0633339a453.jpeg">
									                </div>
										            <?php
										            }
										            ?>
									            </td>
									            <td class="desc">
									                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $va[$i]['first_name']." ".$va[$i]['last_name']; ?></p>
									                <p><span class="">Member since <?php echo date("F Y",strtotime($va[$i]['created'])); ?></span></p>
									            </td>
									                        
									            
									            <td class="requests">
									                <div class="text">No. of Requests</div>
									                <div class="num"><?php  echo $va[$i]['total_clients']; ?></div>
									            </td>

									            <td class="requests active_req">
									                <div class="text">Active Requests</div>
									                <div class="num"><?php  echo $va[$i]['total_designer']; ?></div>
									            </td>
									        </tr>
									    <?php } ?>
									    </tbody>
									</table>
									</div>
									<?php /*
                                     <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive">
                                        
                                        <tbody>
										<?php for($i=0;$i<sizeof($va);$i++){ ?>
										   <tr class="trborder">
												<td style="width: 500px;">
													<center>
														 <div class="col-sm-2" style="padding:0px;">
															 
															<?php
															if ($va[$i]['profile_picture']) 
															{
															?>
															 <div class="myprofilebackground" ><p style="padding-top: 0px;">
															 <?php
																echo '<img src="' . base_url() . 'public/profile/' . $va[$i]['profile_picture'] . '" style=" height:60px;width:60px; border-radius:50%;border: 3px solid #0190ff;margin-left:0px;">';
															
															?>
															 </p></div>         
															<?php  
															}
															else 
															{
															?>
															<div class="myprofilebackground" style="width:60px; height:60px;border-radius:50%; background: #0190ff;border: 3px solid #0190ff; text-align: center; font-size: 15px; color: #fff;"><p style="font-size: 22px;padding-top: 15px;">
																	
															<?php 
																
																echo ucwords(substr($va[$i]['first_name'],0,1)) .  ucwords(substr($va[$i]['last_name'],0,1));
															?>
																</p></div>
															<?php
															}
															?>
														</div>
                                                    </center>
													<div style="display:inline-block;width:80%;text-align: left;">
														<h4 class="darkblacktext" style="display: inline-block;font-size: 17px;font-weight: 600;padding-left: 20px;"><?php echo $va[$i]['first_name']." ".$va[$i]['last_name']; ?> </h4></br>
														<p class="greytext" style="display: inline-block;font-size: 12px;font-weight: 100;padding-left: 20px;">Member since <?php echo date("M d, Y",strtotime($va[$i]['created'])); ?></p>
													</div>													
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Lorem Ipsum</p>
													<p class="darkblacktext weight600">12</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Rating</p>
													<i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star"></i>
												</td>
												<!--<td>
													<p class="greytext pb0 weight600 font18 directmessage" style="display: inline-block;margin-left: 5px;" data-clientname="James Franco" data-clientid="3" data-imageurl="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;">Direct Message</p>
													<svg style="vertical-align: sub;width: 0.5em;" aria-hidden="true" data-prefix="fas" data-icon="comment-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt fa-w-16 fa-2x" ><path fill="orange" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z" class=""></path></svg>
												</td>-->
											</tr> 
										<?php } ?>
											
										</tbody>
									</table>
									*/?>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="pendingforapproverequest" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
</div>

<form method="post" action="">
<div id="add_designer" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top: 10%;">

    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header" style="display:none;">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modal Header</h4>
		</div>
		<div class="modal-body" style="padding:0px;">
			<div class="col-sm-12" style="padding:30px;">
				<div class="col-sm-12" style="padding: 0;">
					<div class="col-sm-12">
						<h3 class="darkblacktext weight600 ls0" style="margin-bottom: 20px;">Add QA/VA</h3>
					</div>
					
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="fname" class="darkblacktext weight600"><strong>Select Position</strong></label>
						<select class="form-control " id="sel1" style="background-color: #ededed  !important;border-radius: 6px !important;border:unset !important;" name="role">
							<option value="qa">Quality Assurance Person</option>
							<option value="va">Virtual Assistant Person</option>
						</select>
					</div>	
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">First Name</label>
						<input type="text" class="form-control" name="first_name" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Last Name</label>
						<input type="text" class="form-control" name="last_name" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Email</label>
						<input type="text" class="form-control" name="email" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Phone</label>
						<input type="text" class="form-control" name="phone" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-12" style="padding-bottom:20px; padding-top:20px;">
						<p style="border-bottom:1px solid #c7c9cc; padding-bottom:0;"></p>
					</div>
				</div>
			
				<div class="col-sm-12" style="padding:0;">		  
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="fname" class="darkblacktext weight600"><strong>Password</strong></label>
						<input type="password" class="form-control" name="password" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>	
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Confirm Password</label>
						<input type="password" class="form-control" name="confirm_password" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
				</div>
				
				<div class="col-sm-12">
					<button type="submit" class="btn pinkbackground weight600" style="margin:  auto;height: 60px;font-size:20px;margin-top: 20px;width: 100%;">Add Person</button>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" style="display:none;">Close</button>
		</div>
	</div>
			
   </div>
	
</div>
</form>

<div id="directmessage" class="modal fade" role="dialog" style="margin-top:10%;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body " style="font-size: 170px;height: 270px;">
			
			<h3 style="letter-spacing: -1px;font-size: 21px;display:inline-block;" class="darkblacktext weight600">Direct Message to </h3>
			<h3 style="letter-spacing: -1px;font-size: 21px;display:inline-block;width:50%;" class="darkblacktext weight600 image_name">Direct Message to </h3>
			
			<div class="col-sm-10">
				<textarea class="form-control" style="height: 125px;margin-top: 10px;border: 1px solid !important;"></textarea>
			</div>
			<div class="col-sm-2">
				<i class="fa fa-send pinktext" style="font-size: 35px;"></i>
			</div>
      </div>
    </div>

  </div>
</div>
<script>
$(document).ready(function () {
	$("#myInput").on("keyup", function () {
		var value = $(this).val().toLowerCase();
		$(".active table tr").filter(function () {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		});
	});
});
</script>