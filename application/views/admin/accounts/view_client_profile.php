<?php
//echo "<pre/>";print_R($data);exit;
if (isset($data[0]['first_name'])) {
    $first_name = $data[0]['first_name'];
}
if (isset($data[0]['last_name'])) {
    $last_name = $data[0]['last_name'];
}
?>
<section class="con-b">
    <div class="container-fluid">
        <?php if ($this->session->flashdata('message_error') != '') { ?>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c"><?php echo $this->session->flashdata('message_error'); ?></p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c"><?php echo $this->session->flashdata('message_success'); ?></p>
            </div>
        <?php } ?>
        <div class="flex-this flex-end">
            <div class="header_searchbtn">
                <a class="adddesinger" href="<?php echo base_url(); ?>admin/dashboard?client_id=<?php echo $data[0]['id']; ?>">Projects</a>

                <?php //if(($edit_profile[0]['full_admin_access'] == 1) && $edit_profile[0]['role'] == 'admin'){  ?>
                <a class="switchtouseraccount adddesinger" target="_blank"  href="<?php echo base_url(); ?>admin/accounts/switchtouseraccount/<?php echo $data[0]['id']; ?>">Switch To User Account</a>
                <?php //}  ?>

            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-4">
                <div class="client-profile">
                    <div class="profile-pic">
                        <figure>
                            <img src="<?php echo $data[0]['profile_picture']; ?>" class="img-responsive telset33">
                        </figure>
                    </div>
                    <div class="main-discp">
                        <h3>Client</h3>
                        <h2>
                            <?php echo $first_name . ' ' . $last_name; ?>
                        </h2>
                        <p>
                            <?php if (isset($data[0]['email'])): ?>
                                <?php echo $data[0]['email']; ?>
                            <?php endif ?>
                        </p>
                        <p class="chngpasstext">
                            <a href="" class="change_pass" name="savebtn" value="Change Password" data-toggle="modal" data-target="#myModal">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/change-password.png" class="img-responsive"> 
                            <span>Change Password</span></a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="detailed-item client_info">
                    <ul>
                        <li><label for="">Current Plan</label><span><?php echo ($plan_data[0]['plan_name'] != "") ? $plan_data[0]['plan_name'] : "No Member" ?></span></li>
                        <li><label for=""><?php echo 'Billing Start'; ?></label><span><?php echo ($data[0]['billing_start_date'] != '') ? $data[0]['billing_start_date'] : 'N/A'; ?></span></li>
                        <li><label for=""><?php echo 'Billing End'; ?></label><span><?php echo ($data[0]['billing_end_date'] != '') ? $data[0]['billing_end_date'] : 'N/A'; ?></span></li>
                        <li><label for=""><?php echo 'Company Name'; ?></label><span><?php echo ($data[0]['company_name'] != '') ? $data[0]['company_name'] : 'N/A'; ?></span></li>
                        <li><label for=""><?php echo 'Address'; ?></label><span><?php echo ($data[0]['address_line_1'] != '') ? $data[0]['address_line_1'] : 'N/A'; ?></span></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <ul class="list-header-blog client-inform">
                    <li class="active"><a href="#general" class="stayhere" data-toggle="tab">Profile Info</a></li>
                    <li><a href="#subUser" class="stayhere" data-toggle="tab">Sub Users</a></li>
                    <li><a href="#brandProfile" class="stayhere" data-toggle="tab">Brand Profiles</a></li>
                    <li><a href="#billingPlan" class="stayhere" data-toggle="tab">Billing</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">
                    <div class="tab-pane active" id="general">   
                        <div class="tabs-card">
                            <form method="post" id="about_info_client_form" action="<?php echo base_url() . 'admin/accounts/customer_edit_profile/' . $data[0]['id']; ?>">
                                <div class="settingedit-box">
                                    <div class="settrow">
                                        <h2 class="main-info-heading">Profile Information</h2>
                                        <p class="fill-sub">Please fill your Profile Information</p>
                                        <input type="hidden" id="user_id" name="userid" value="<?php echo $data[0]['id']; ?>">
                                        <div class="settcol right">
                                            <p class="btn-x">
                                                <input type="submit" id="about_info_btn" name="savebtn" class="btn-e save_info" value="Save"/>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($data[0]['first_name']) && $data[0]['first_name'] != "") ? "label-active" : ""; ?>"> First Name </p>
                                                <input type="text" name="first_name" class="input" value="<?php
                                                if (isset($data[0]['first_name'])) {
                                                    echo $data[0]['first_name'];
                                                }
                                                ?>" id="user_name" required/>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($data[0]['last_name']) && $data[0]['last_name'] != "") ? "label-active" : ""; ?>"> Last Name </p>
                                                <input type="text" name="last_name" class="input" value="<?php
                                                if (isset($data[0]['last_name'])) {
                                                    echo $data[0]['last_name'];
                                                }
                                                ?>" id="last_name" required/>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($data[0]['email']) && $data[0]['email'] != "") ? "label-active" : ""; ?>"> Email </p>
                                                <input type="email" class="input" value="<?php echo $data[0]['email']; ?>" name="notification_email" required/>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($data[0]['company_name']) && $data[0]['company_name'] != "") ? "label-active" : ""; ?>"> Company Name </p>
                                                <input type="text" class="input" value="<?php
                                                if (isset($data[0]['company_name'])) {
                                                    echo $data[0]['company_name'];
                                                }
                                                ?>" name="company_name">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($data[0]['title']) && $data[0]['title'] != "") ? "label-active" : ""; ?>"> Title </p>
                                                <input type="text" class="input" value="<?php
                                                if (isset($data[0]['title'])) {
                                                    echo $data[0]['title'];
                                                }
                                                ?>" name="title">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($data[0]['phone']) && $data[0]['phone'] != "") ? "label-active" : ""; ?>"> phone </p>
                                                <input type="text" class="input" value="<?php
                                                if (isset($data[0]['phone'])) {
                                                    echo $data[0]['phone'];
                                                }
                                                ?>" name="phone">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-12">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($data[0]['address_line_1']) && $data[0]['address_line_1'] != "") ? "label-active" : ""; ?>"> Address </p>
                                                <textarea class="input" name="address_line_1" placeholder="<?php echo $data[0]['address_line_1']; ?>"><?php
                                                    if (isset($data[0]['address_line_1'])) {
                                                        echo $data[0]['address_line_1'];
                                                    }
                                                    ?></textarea>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($data[0]['city']) && $data[0]['city'] != "") ? "label-active" : ""; ?>"> City </p>
                                                <input type="text" class="input" value="<?php
                                                if (isset($data[0]['city'])) {
                                                    echo $data[0]['city'];
                                                }
                                                ?>" name="city">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($data[0]['state']) && $data[0]['state'] != "") ? "label-active" : ""; ?>"> state </p>
                                                <input type="text" class="input" value="<?php
                                                if (isset($data[0]['state'])) {
                                                    echo $data[0]['state'];
                                                }
                                                ?>" name="state">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>

                                        <div class="col-sm-4">
                                            <label class="form-group">
                                                <p class="label-txt <?php echo (isset($data[0]['zip']) && $data[0]['zip'] != "") ? "label-active" : ""; ?>"> zip Code </p>
                                                <input type="text" class="input" value="<?php
                                                if (isset($data[0]['zip'])) {
                                                    echo $data[0]['zip'];
                                                }
                                                ?>" name="zip">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div> 
                            </form>
                        </div>
                        <!--survey info-->

                        <?php if ($data[0]['is_trail'] != 1) { ?>
                            <?php if ($data[0]['parent_id'] == 0 && $data[0]['plan_name'] != "") { ?>
                                <div class="tabs-card">
                                    <form class="user_level_set" id="custom_active_req" method="POST">
                                        <div class="settrow">
                                            <h2 class="main-info-heading">User Setting</h2>
                                            <p class="fill-sub"><?php echo ($data[0]['overwrite'] == 0) ? "These are default plan settings,You can override these from here." : "Following data is the override plan settings,You can change these from here."; ?></p>
                                            <div class="settcol right">
                                                <p class="btn-x">
                                                    <input type="submit" id="req_info_btn" name="savebtn" class="btn-e save_info" value="Save"/>
                                                </p>
                                            </div>
                                        </div>
                                        <input type="hidden" id="curuser_id" name="curuser_id" value="<?php echo $data[0]['id']; ?>">
                                        <div class="row">
                                            <?php if (in_array($data[0]['plan_name'], NEW_PLANS)) { ?>
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php echo (isset($data[0]['total_requests']) && $data[0]['total_requests'] != "") ? "label-active" : ""; ?>"> How many requests</p>
                                                        <input type="number" min="-1" class="total_requests input" id="usertotalreq" name="total_requests" value="<?php
                                                        if (isset($data[0]['total_requests'])) {
                                                            echo $data[0]['total_requests'];
                                                        }
                                                        ?>">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php echo (isset($data[0]['total_active_req']) && $data[0]['total_active_req'] != "") ? "label-active" : ""; ?>">Total Active Requests</p>
                                                        <input type="number" min="1" class="total_req_count input" id="useractivereq" name="total_req_count" value="<?php
                                                        if (isset($data[0]['total_active_req'])) {
                                                            echo $data[0]['total_active_req'];
                                                        }
                                                        ?>">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php echo (isset($data[0]['total_inprogress_req']) && $data[0]['total_inprogress_req'] != "") ? "label-active" : ""; ?>">Inprogress Requests</p>
                                                        <input type="number" min="1" class="input" id="userinprogreq" name="inprogess_req_count" value="<?php
                                                        if (isset($data[0]['total_inprogress_req'])) {
                                                            echo $data[0]['total_inprogress_req'];
                                                        }
                                                        ?>">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <?php if ($data[0]['plan_name'] == SUBSCRIPTION_99_PLAN) { ?>
                                                    <div class="col-sm-4">
                                                        <label class="form-group">
                                                            <p class="label-txt <?php echo (isset($data[0]['total_inprogress_req']) && $data[0]['total_inprogress_req'] != "") ? "label-active" : ""; ?>">Request per month</p>
                                                            <input type="number" min="1" class="total_requests input" id="userbillingreq" name="billing_cycle_request" value="<?php
                                                            if (isset($data[0]['billing_cycle_request'])) {
                                                                echo $data[0]['billing_cycle_request'];
                                                            }
                                                            ?>">
                                                            <div class="line-box">
                                                                <div class="line"></div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            <div class="col-sm-4">
                                                <label class="form-group">
                                                    <p class="label-txt <?php echo (isset($data[0]['turn_around_days']) && $data[0]['turn_around_days'] != "") ? "label-active" : ""; ?>">Turn around days</p>
                                                    <input type="number" min="1" class="turn_around_days input" id="userturnday" name="turn_around_days" value="<?php
                                                    if (isset($data[0]['turn_around_days'])) {
                                                        echo $data[0]['turn_around_days'];
                                                    }
                                                    ?>">
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="form-group">
                                                    <div class="label-txt <?php echo (isset($data[0]['sub_user_count']) && $data[0]['sub_user_count'] != "") ? "label-active" : ""; ?>"> How many users
                                                        <div class="tool-hover">
                                                            <i class="fas fa-info-circle"></i>
                                                            <div class="tool-text"><strong>Note:</strong><span> Keep -1</span>, To allow user to add unlimited users.
                                                                <span> Keep 1 to n number</span>, To allow user to add specific number of users.
                                                                <span> Keep 0</span>, To hide user management section to users.
                                                            </div>
                                                        </div></div>
                                                    <input type="number" name="users_size" min="-1" class="users_size input" id="users_size" value="<?php echo isset($data[0]['sub_user_count']) ? $data[0]['sub_user_count'] : ""; ?>">
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="form-group">
                                                    <div class="label-txt <?php echo (isset($data[0]['brand_profile_count']) && $data[0]['brand_profile_count'] != "") ? "label-active" : ""; ?>"> How many brand profile
                                                        <div class="tool-hover">
                                                            <i class="fas fa-info-circle"></i>
                                                            <div class="tool-text"><strong>Note:</strong> <span>Keep -1</span>, To allow user to add unlimited brand profile.
                                                                <span>Keep 1 to n number</span>, To allow user to add specific number of brand profile.
                                                                <span>Keep 0</span>, To not allow to users to add brand profile.
                                                            </div>
                                                        </div></div>
                                                    <input type="number" name="brand_size" min="-1" class="brand_size input" id="brand_size" value="<?php echo isset($data[0]['brand_profile_count']) ? $data[0]['brand_profile_count'] : ""; ?>">
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label class="form-group">
                                                    <div class="label-txt label-active">File Management
                                                    </div>
                                                    <div class="switch-custom-usersetting-check switch-custom_toggle">
                                                        <input type="checkbox" name="file_management" data-cid="<?php echo $data[0]['id']; ?>" id="switch_toggle_<?php echo $data[0]['id']; ?>" <?php echo ($data[0]['file_management'] == 1) ? 'checked' : ''; ?> value="<?php echo $data[0]['file_management']; ?>" />
                                                        <label for="switch_toggle_<?php echo $data[0]['id']; ?>"></label> 
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-sm-4">
                                                <label class="form-group">
                                                    <div class="label-txt label-active">File Sharing
                                                    </div>
                                                    <div class="switch-custom-usersetting-check f_filesharing">
                                                        <input type="checkbox" name="file_sharing" data-cid="<?php echo $data[0]['id']; ?>" id="switch_file_<?php echo $data[0]['id']; ?>" <?php echo ($data[0]['file_sharing'] == 1) ? 'checked' : ''; ?> value="<?php echo $data[0]['file_sharing']; ?>" />
                                                        <label for="switch_file_<?php echo $data[0]['id']; ?>"></label> 
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            <?php } ?>
                            <div class="tabs-card">
                                <div class="about_info_survey row">
                                    <div class="col-md-12">
                                        <div class="settingedit-box">
                                            <div class="settrow">
                                                <div class="settcol left">
                                                    <h2 class="main-info-heading">Survey Info</h2>
                                                    <p class="fill-sub"></p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="label-x2">Which Best Describes Your Business?</label>
                                                        <p class="space-a"></p>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p class="text-f">
                                                                    <?php echo (isset($data[0]['business_detail']) && $data[0]['business_detail'] != '') ? $data[0]['business_detail'] : '<p class="text-f" style="color:#e73250">Not Filled</p>'; ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="label-x2">What Is Your Role At The Company?</label>
                                                        <p class="space-a"></p>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p class="text-f">
                                                                    <?php echo (isset($data[0]['company_role']) && $data[0]['company_role'] != '') ? $data[0]['company_role'] : '<p class="text-f" style="color:#e73250">Not Filled</p>'; ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="label-x2">How Many Designs Do You Need Per Month?</label>
                                                        <p class="space-a"></p>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p class="text-f">
                                                                    <?php echo (isset($data[0]['design_count']) && $data[0]['design_count'] != '') ? $data[0]['design_count'] : '<p class="text-f" style="color:#e73250">Not Filled</p>'; ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="label-x2">What Kind Of Designs Do You Usually Need?</label>
                                                        <p class="space-a"></p>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p class="text-f">
                                                                    <?php echo (isset($data[0]['design_type']) && $data[0]['design_type'] != '') ? $data[0]['design_type'] : '<p class="text-f" style="color:#e73250">Not Filled</p>'; ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="tabs-card">
                            <div class="utm_track row">
                                <div class="col-md-12">
                                    <div class="settingedit-box">
                                        <div class="settrow">
                                            <div class="settcol left">
                                                <h2 class="main-info-heading">UTM Tracking Info</h2>
                                                <p class="fill-sub"></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="label-x2">Source Info</label>
                                                    <p class="space-a"></p>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="text-f">
                                                                <?php echo (isset($data[0]['YOUR_SOURCE_FIELD']) && $data[0]['YOUR_SOURCE_FIELD'] != '') ? $data[0]['YOUR_SOURCE_FIELD'] : '<p class="text-f" style="color:#e73250">NA</p>'; ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="label-x2">Medium info</label>
                                                    <p class="space-a"></p>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="text-f">
                                                                <?php echo (isset($data[0]['YOUR_MEDIUM_FIELD']) && $data[0]['YOUR_MEDIUM_FIELD'] != '') ? $data[0]['YOUR_MEDIUM_FIELD'] : '<p class="text-f" style="color:#e73250">NA</p>'; ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="label-x2">Campaign Info</label>
                                                    <p class="space-a"></p>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="text-f">
                                                                <?php echo (isset($data[0]['YOUR_CAMPAIGN_FIELD']) && $data[0]['YOUR_CAMPAIGN_FIELD'] != '') ? $data[0]['YOUR_CAMPAIGN_FIELD'] : '<p class="text-f" style="color:#e73250">NA</p>'; ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="label-x2">Content Info</label>
                                                    <p class="space-a"></p>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="text-f">
                                                                <?php echo (isset($data[0]['YOUR_CONTENT_FIELD']) && $data[0]['YOUR_CONTENT_FIELD'] != '') ? $data[0]['YOUR_CONTENT_FIELD'] : '<p class="text-f" style="color:#e73250">NA</p>'; ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="label-x2">Term Info</label>
                                                    <p class="space-a"></p>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="text-f">
                                                                <?php echo (isset($data[0]['YOUR_TERM_FIELD']) && $data[0]['YOUR_TERM_FIELD'] != '') ? $data[0]['YOUR_TERM_FIELD'] : '<p class="text-f" style="color:#e73250">NA</p>'; ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="label-x2">Visits</label>
                                                    <p class="space-a"></p>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <p class="text-f">
                                                                <?php echo (isset($data[0]['VISITS']) && $data[0]['VISITS'] != '') ? $data[0]['VISITS'] : '<p class="text-f" style="color:#e73250">NA</p>'; ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane" id="subUser"> 
                        <div class="row">
                            <div class="col-md-12">
                                <div class="white-boundries" id="sub-userlist">
                                    <div class="headerWithBtn">
                                        <h2 class="main-info-heading">User Management</h2>
                                        <p class="fill-sub">Add and Update additional users for your account.</p>
                                        <div class="addPlusbtn">
                                            <a href="javascript:void(0)" class="new-subuser add_subuser_main" id="add_subuser_mains" data-user_type="manager">+ Add Management User</a>
                                        </div>
                                    </div>
                                    <div class="account-tab" style="margin-top: 0">
                                        <div id="no-more-tables">
                                            <div class="managment-list">   
                                                <?php
                                                if (!empty($user_management)) {
                                                    foreach ($user_management as $users) {
                                                        ?>
                                                        <ul>
                                                            <li class="usericon">
                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/user-bg.svg" class="img-responsive">
                                                            </li>
                                                            <li class="name"><?php echo $users['first_name'] . ' ' . $users['last_name']; ?> </li>
                                                            <li class="email"><?php echo $users['email']; ?></li>
                                                            <li class="lbl">
                                                                <div class="switch-custom-usersetting-check activate-user">
                                                                    <input type="checkbox" name="enable_disable_set" data-userid="<?php echo $users['id']; ?>" data-email="<?php echo $users['email']; ?>" data-name="<?php echo $users['first_name']; ?>" id="enable_disable_set_<?php echo $users['id']; ?>" checked="">
                                                                    <label for="enable_disable_set_<?php echo $users['id']; ?>"></label> 
                                                                    <span class="checkstatus_<?php echo $users['id']; ?>">
                                                                        Active</span>
                                                                </div>
                                                            </li>
                                                            <li class="action">
                                                                <a href="javascript:void(0)" data-id="<?php echo $users['id']; ?>" class="edit_subuser" data-user_type="<?php echo $users['user_flag']; ?>">
                                                                    <i class="fas fa-pen"></i></a>
                                                                <a data-clientid="<?php echo $users['id']; ?>" class="delete_subuserdata" data-toggle="modal" data-target="#Deletesubusers">
                                                                    <i class="icon-gz_delete_icon"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <?php
                                                    }
                                                } else {
                                                    echo "<h3>No Record Found</h3>";
                                                }
                                                ?>
                                            </div>
                                        </div>                              
                                    </div>  
                                </div> 
                                <div id="new-subuser" style="display: none;">

                                    <div class="edit-submt">

                                        <form action="<?php echo base_url(); ?>admin/Subusers/sub_user" method="post" role="form" class="sub_user_form">
                                            <div class="white-boundries">
                                                <div class="headerWithBtn">
                                                    <h2 class="main-info-heading">New User</h2>
                                                    <p class="fill-sub">Complete the form below to add new user.</p>
                                                    <div class="addPlusbtn">
                                                        <a class="backlist backlist-go">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive">Back
                                                        </a>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="cust_id" value="" class="cust_id">
                                                <input type="hidden" name="main_id" value="<?php echo $data[0]['id']; ?>" class="main_id">
                                                <input type="hidden" name="user_flag" value="" class="user_flag">
                                                <input type="hidden" name="cust_url" value="admin/accounts/view_client_profile/<?php echo $data[0]['id']; ?>#subUser" class="cust_url">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label class="form-group">
                                                            <p class="label-txt ">First Name <span>*</span></p>
                                                            <input type="text" name="first_name" class="input fname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="" required="">
                                                            <div class="line-box">
                                                                <div class="line"></div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="form-group">
                                                            <p class="label-txt ">Last Name <span>*</span></p>
                                                            <input type="text" name="last_name" class="input lname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="" required="">
                                                            <div class="line-box">
                                                                <div class="line"></div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="form-group">
                                                            <p class="label-txt ">Email Address <span>*</span></p>
                                                            <input type="email" class="input user_email" name="email" data-rule="email" data-msg="Please enter a valid email" value="" required="">
                                                            <div class="line-box">
                                                                <div class="line"></div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label class="form-group">
                                                            <p class="label-txt ">Phone Number <span>*</span></p>
                                                            <input type="tel" name="phone" class="input phone" value="" required="">
                                                            <div class="line-box">
                                                                <div class="line"></div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="requests_limit_toclient">
                                                        <div class="col-md-6">
                                                            <label class="form-group">
                                                                <p class="label-txt">Total Requests Type</p>
                                                                <select class="input requests_type" name="requests_type" id="requests_type">
                                                                    <option value="all">Subscription based</option>
                                                                    <option value="per_month">Billing subscription based</option>
                                                                    <option value="one_time">One time requests</option>
                                                                </select>
                                                                <div class="line-box">
                                                                    <div class="line"></div>
                                                                </div>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6 total_one_time_req" style="display:none">
                                                            <label class="form-group">
                                                                <p class="label-txt ">Total Add Requests</p><div class="tool-hover">
                                                                    <i class="fas fa-info-circle"></i>
                                                                    <div class="tool-text">Total Number of requests user can add</div>
                                                                </div><p></p>
                                                                <input type="text" name="total_requests" class="input total_requests" value="" id="total_requests">
                                                                <div class="line-box">
                                                                    <div class="line"></div>
                                                                </div>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6 billing_cycle_req" style="display:none">
                                                            <label class="form-group">
                                                                <p class="label-txt ">Billing Cycle Requests</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Total Number of requests user can add in current billing cycle</div></div><p></p>
                                                                <input type="text" name="billing_cycle_requests" class="input billing_cycle_requests" value="" id="billing_cycle_requests">

                                                                <div class="line-box">
                                                                    <div class="line"></div>
                                                                </div>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group total_inprogress_req_sec">
                                                                <label class="form-group">
                                                                    <div class="label-txt ">In-Progress Requests<div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Total Number of active requests</div></div></div>
                                                                    <input type="text" name="total_inprogress_req" class="input total_inprogress_req" value="" id="total_inprogress_req">
                                                                    <div class="line-box">
                                                                        <div class="line"></div>
                                                                    </div>
                                                                </label>
                                                                <div class="error_msg"></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group total_active_req_sec">
                                                                <label class="form-group">
                                                                    <div class="label-txt ">Active Requests<div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Total Number of active,revision and pending approval requests</div></div></div>
                                                                    <input type="text" name="total_active_req" class="input total_active_req" value="" id="total_active_req">
                                                                    <div class="line-box">
                                                                        <div class="line"></div>
                                                                    </div>
                                                                </label>
                                                                <div class="error_msg"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="white-boundries">
                                                        <div class="access-brand">
                                                            <h3>Brands Selection</h3>
                                                            <div class="notify-lines finish-line">
                                                                <label class="switch-custom-usersetting-check">
                                                                    <div class="switch-custom-usersetting-check">
                                                                        <input type="checkbox" name="access_brand_pro" class="switch_access" id="switch_access">
                                                                        <label for="switch_access"></label> 
                                                                    </div>
                                                                </label>
                                                                <h3>Access All brands</h3>
                                                            </div>
                                                        </div>
                                                        <div class="" id="brandshowing">
                                                            <label>Select Brands <span>*</span></label>
                                                            <select name="brandids[]" id="brandcheck"  class="chosen-select" style='display:none' data-live-search="true" multiple>
                                                                <?php ?>
                                                                <option value="" disabled>Select Brand</option>
                                                                <?php
                                                                foreach ($brandprofile as $brand) { ?> 
                                                                    <option value="<?php echo $brand['id'] ?>" <?php echo in_array($brand['id'], $selectedbrandIDs) ? 'selected' : '' ?>><?php echo $brand['brand_name'] ?></option>
                                                                <?php } ?>
                                                            </select>                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="white-boundries">
                                                        <div class="view_only_prmsn">
                                                            <div class="access-brand">
                                                                <h3>Permissions</h3>
                                                                <div class="notify-lines finish-line">
                                                                    <label class="switch-custom-usersetting-check">
                                                                        <input type="checkbox" name="view_only" class="form-control" id="view_only">
                                                                        <label for="view_only"></label> 
                                                                    </label>
                                                                    <h3>View Only</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row permissions_for_subuser_client">
                                                            <div class="col-md-4">
                                                                <div class="notify-lines">
                                                                    <label class="switch-custom-usersetting-check uncheckview">
                                                                        <input type="checkbox" name="add_requests" class="form-control" id="add_requests">
                                                                        <label for="add_requests"></label> 
                                                                    </label>
                                                                    <p>Add Request</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="notify-lines">
                                                                    <label class="switch-custom-usersetting-check uncheckview">
                                                                        <input type="checkbox" name="comnt_requests" class="form-control" id="comnt_requests">
                                                                        <label for="comnt_requests"></label> 
                                                                    </label>
                                                                    <p>Comment on Request</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="notify-lines">
                                                                    <label class="switch-custom-usersetting-check uncheckview">
                                                                        <input type="checkbox" name="add_brand_pro" class="form-control" id="add_brand_pro">
                                                                        <label for="add_brand_pro"></label> 
                                                                    </label>
                                                                    <p>Add/Edit Brand Profile</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="notify-lines">
                                                                    <label class="switch-custom-usersetting-check uncheckview">
                                                                        <input type="checkbox" name="del_requests" class="form-control" id="del_requests">
                                                                        <label for="del_requests"></label> 
                                                                    </label>
                                                                    <p>Delete Request</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="notify-lines">
                                                                    <label class="switch-custom-usersetting-check uncheckview">
                                                                        <input type="checkbox" name="billing_module" class="form-control" id="billing_module">
                                                                        <label for="billing_module"></label> 
                                                                    </label>
                                                                    <p>Billing Module</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="notify-lines">
                                                                    <label class="switch-custom-usersetting-check uncheckview">
                                                                        <input type="checkbox" name="manage_priorities" class="form-control" id="manage_priorities">
                                                                        <label for="manage_priorities"></label> 
                                                                    </label>
                                                                    <p>Manage Request Priorities<br><span style="font-size:12px;display:none;font-style: italic;margin-top: 5px;">*Applicable only if all brand access is enable</span></p>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="notify-lines">
                                                                    <label class="switch-custom-usersetting-check uncheckview">
                                                                        <input type="checkbox" name="app_requests" class="form-control" id="app_requests">
                                                                        <label for="app_requests"></label> 
                                                                    </label>
                                                                    <p>Approve/Revision Request</p>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="notify-lines">
                                                                    <label class="switch-custom-usersetting-check uncheckview">
                                                                        <input type="checkbox" name="downld_requests" class="form-control" id="downld_requests">
                                                                        <label for="downld_requests"></label> 
                                                                    </label>
                                                                    <p>Download File</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="notify-lines">
                                                                    <label class="switch-custom-usersetting-check uncheckview">
                                                                        <input type="checkbox" name="file_management" class="form-control" id="file_management" <?php echo ($sub_user_permsion['file_management'] == 1) ? "checked" : ""; ?>>
                                                                        <label for="file_management"></label> 
                                                                    </label>
                                                                    <p>File Management</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="notify-lines">
                                                                    <label class="switch-custom-usersetting-check uncheckview">
                                                                        <input type="checkbox" name="white_label" class="form-control" id="white_label" <?php echo ($sub_user_permsion['white_label'] == 1) ? "checked" : ""; ?>>
                                                                        <label for="white_label"></label> 
                                                                    </label>
                                                                    <p>White Label</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-12 mb-t30">
                                                    <div class="btn-here">
                                                        <input type="submit" name="save_subuser" class="btn-red center-block" value="SAVE">
                                                    </div>
                                                </div>
                                            </div>        
                                        </form>
                                    </div>
                                </div>                        
                            </div>
                        </div>
                        <div class="sub-userlist white-boundries" id="sub-userlist">
                            <div class="headerWithBtn">
                                <h2 class="main-info-heading">Client Management</h2>
                                <p class="fill-sub">Add and Update additional users for your account.</p>
                                <div class="addPlusbtn">
                                    <a href="javascript:void(0)" class="new-subuser add_subuser_main" id="add_subuser_main" data-user_type="client">+ Add New Customer</a>
                                </div>
                            </div>

                            <div id="no-more-tables">
                                <div class="managment-list">
                                    <?php
                                    if (!empty($client_management)) {
                                        foreach ($client_management as $clients) {
                                            ?>
                                            <ul>
                                                <li class="usericon">
                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/user-bg.svg" class="img-responsive">
                                                </li>
                                                <li class="name"><?php echo $clients['first_name'] . ' ' . $clients['last_name']; ?></li>
                                                <li class="email"><?php echo $clients['email']; ?></li>
                                                <li class="lbl">
                                                    <div class="switch-custom-usersetting-check activate-user">
                                                        <input type="checkbox" name="enable_disable_set" data-userid="<?php echo $clients['id']; ?>" data-email="<?php echo $clients['email']; ?>" data-name="<?php echo $clients['first_name']; ?>" id="enable_disable_set_<?php echo $clients['id']; ?>" checked="">
                                                        <label for="enable_disable_set_<?php echo $clients['id']; ?>"></label> 
                                                        <span class="checkstatus_<?php echo $clients['id']; ?>">Active</span>
                                                    </div>
                                                </li>
                                                <li class="action">
                                                    <a class="switchtouseraccount direction-re" target="_blank"  href="<?php echo base_url(); ?>admin/accounts/switchtouseraccount/<?php echo $clients['id']; ?>" title="Switch to User Account"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/switch_user.png" class="img-responsive"></a>
                                                    <a href="javascript:void(0)" data-id="<?php echo $clients['id']; ?>" class="edit_subuser" data-user_type="<?php echo $clients['user_flag']; ?>" title="Edit">
                                                        <i class="fas fa-pen"></i></a>
                                                    <a data-clientid="<?php echo $users['id']; ?>" class="delete_subuserdata" data-toggle="modal" data-target="#Deletesubusers" title="Delete">
                                                        <i class="icon-gz_delete_icon"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                            <?php
                                        }
                                    } else {
                                        echo "<h3>No Record Found";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="white-boundries" id="new-subuser-client" style="display: none;">
                            <div class="headerWithBtn">
                                <?php ?>
                                <h2 class="main-info-heading">New User</h2>
                                <?php ?>
                                <p class="fill-sub">Complete the form below to add new user.</p>
                                <div class="addPlusbtn"><a class="backlist backlist-go"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive">Back
                                    </a>
                                </div>
                            </div>
                            <form action="<?php echo base_url(); ?>admin/Subusers/sub_user" method="post" role="form" class="sub_user_form" id="sub_user_sbmt">
                                <input type="hidden" name="cust_id" value="" class="cust_id"/>
                                <input type="hidden" name="main_id" value="<?php echo $data[0]['id']; ?>" class="main_id">
                                <input type="hidden" name="cust_url" value="admin/accounts/view_client_profile/<?php echo $data[0]['id']; ?>#subUser" class="cust_url"/>
                                <input type="hidden" name="user_flag" value="<?php echo isset($edit_sub_user['user_flag']) ? $edit_sub_user['user_flag'] : ""; ?>" class="user_flag"/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="form-group">
                                            <p class="label-txt">First Name <span>*</span></p>
                                            <input type="text" name="first_name" class="input fname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="" required="">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-group">
                                            <p class="label-txt">Last Name <span>*</span></p>
                                            <input type="text" name="last_name" class="input lname" data-rule="minlen:4" data-msg="Please enter at least 4 chars" value="" required="">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-group">
                                            <p class="label-txt">Email Address <span>*</span></p>
                                            <input type="email" class="input user_email" name="email" data-rule="email" data-msg="Please enter a valid email" value="" required="">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-group">
                                            <p class="label-txt">Phone Number <span>*</span></p>
                                            <input type="tel" name="phone" class="input phone" value=""required="">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="requests_limit_toclient">
                                        <div class="col-md-6">
                                            <label class="form-group">
                                                <p class="label-txt label-active">Total Requests Type</p>
                                                <select class="input requests_type" name="requests_type">
                                                    <option value="all">Subscription based</option>
                                                    <option value="one_time">One time requests</option>
                                                </select>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-md-6 total_one_time_req" style="display:none">
                                            <label class="form-group">
                                                <div class="label-txt">How many requests <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Total Number of requests user can add</div></div></div>
                                                <input type="text" name="total_requests" class="input total_requests" id="" value="" required="">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                                <div class="error_msg"></div>
                                            </label>
                                        </div>
                                        <div class="col-md-6 billing_cycle_req"  style="display:none">
                                            <label class="form-group">
                                                <div class="label-txt label-active">Max number of requests per month <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Total Number of requests user can add in current billing cycle</div></div></div>
                                                <input type="text" name="billing_cycle_requests" class="input billing_cycle_requests" id="" value="">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="col-md-6 total_active_req_sec">
                                            <label class="form-group">
                                                <div class="label-txt label-active">Active Requests <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Your users can have up to the number of Active projects worked on simultaneously. Half will be new projects we work on and the other half can be Revisions needed.</div></div></div>
                                                <select id="" class="input total_active_req" name="total_active_req">
                                                    <?php
                                                    $i = 2;
                                                    $j = 1;
                                                    while ($j <= 10) {
                                                        ?>
                                                        <option id="option_id_<?php echo $i * $j; ?>" value='<?php echo $i * $j; ?> <?php
                                                        if ($edit_sub_user['total_active_req'] == $i * $j) {
                                                            echo "selected";
                                                        }
                                                        ?>'><?php echo $i * $j; ?></option>
                                                                <?php
                                                                $j = $j + 1;
                                                            }
                                                            ?>
                                                </select>
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                                <div class="error_msg"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div> 
                                <div class="btn-here">
                                    <input type="submit" name="save_subuser" class="btn-red" value="SAVE">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="tab-pane" id="brandProfile">
                        <div class="catagry-heading">
                            <br/>
                            <div class="row">
                                <div class="col-md-8">
                                    <h2 class="main-info-heading">Brand Profile List</h2>
                                    <p class="fill-sub">Please find below Brand Profiles information.</p>
                                </div>
                                <div class="col-md-4 text-right">
                                    <a href="<?php echo base_url(); ?>customer/add_brand_profile" class="new-brand addderequesr">+ Add New Brand Profile</a>
                                </div>
                            </div>
                            <?php //echo "<pre/>";print_R($brandprofile); ?>
                            <div class="row">
                                <?php 
                                if(!empty($brandprofile)){
                                foreach($brandprofile as $bk => $bv){ ?>
                                <div class="col-md-3 col-sm-6">
                                    <div class="brand-card">
                                        <div class="bCard-header">
                                            <a href="javascript:void(0)">
                                                <i class="icon-gz_edit_icon"></i>
                                            </a>
                                            <a data-id="<?php echo $bv['id'];?>" class="delete_brands">
                                                <i class="icon-gz_delete_icon"></i>
                                            </a>
                                        </div>
                                        <div class="user-image">
                                            <!--<img class="cross_popup" data-dismiss="modal" src="<?php //echo FS_PATH_PUBLIC_UPLOADS.'brand_profile'.$bv['id'].$bv['brand_name']; ?>">-->
                                        </div>
                                        <div class="name-info"><a href="javascript:void(0)">
                                                <h3> <?php echo $bv['brand_name'];?></h3></a>
                                            <a href="javascript:void(0)" target="_blank"><p><?php echo $bv['website_url'];?></p></a>
                                        </div>
                                    </div>
                                </div>
                                <?php }}else{ echo "<p style='text-align:center'>No Data Found</p>";} ?>
                            </div>
                        </div>
                    </div>
                   <div class="tab-pane" id="billingPlan">
                        <div class="white-boundries">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="settrow">
                                        <h2 class="main-info-heading">Billing and Subscription</h2>
                                        <p class="fill-sub">Update Billing or Change user's Subscription Plan below.</p>
                                    </div>
                                </div>
                                <?php
                                if ($data[0]['plan_name'] != '') {
                                    ?>
                                    <div class="col-md-4">
                                        <div class="switch-custom-usersetting-check cancel_subs_toggle">
                                            <span><?php echo ($data[0]['is_cancel_subscription'] == '1') ? 'Cancelled' : 'Cancel Subscription'; ?></span>
                                            <input type="checkbox" id="switch_can" data-userid="<?php echo $data[0]['id']; ?>" <?php echo ($data[0]['is_cancel_subscription'] == '1') ? 'checked' : ''; ?>  <?php echo ($data[0]['is_cancel_subscription'] == '1') ? 'disabled' : ''; ?>>
                                            <label for="switch_can"></label> 
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <form class="card-verifection" action="<?php echo base_url(); ?>customer/ChangeProfile/change_plan" method="post" accept-charset="utf-8">
                                <div class="card-js">
                                    <?php if ($card) { ?>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="form-group">
                                                    <p class="label-txt <?php echo ($card['0']->last4 != '') ? 'label-active' : ''; ?>">Card Number <span>*</span></p>
                                                    <input type="text" name="card-number" class="card-number input" value='************<?php echo $card['0']->last4; ?>' readonly maxlength="16">
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="form-group">
                                                    <p class="label-txt label-active fixedlabelactive">Expiry Date <span>*</span></p>
                                                    <input type="text" class="input expiry-month" value="<?php echo $card['0']->exp_month; ?>/<?php echo $card['0']->exp_year; ?>" name="expiry-month-date" id="card_expir_date" readonly placeholder="MM  /  YY" onkeyup="dateFormat(this.value);">
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="form-group">
                                                    <p class="label-txt label-active">CVV <span>*</span></p>
                                                    <input type="text" name="cvc" value="***" id="cvc_detail" class="input" maxlength="3" readonly>
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="btn-here">
                                                        <input type="hidden" name="current_plan" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </form>
                        </div>
                        <div class="white-boundries">
                            <div class="client_billin_secton">
                                <div class="settrow">
                                    <h2 class="main-info-heading">Billing Information</h2>
                                    <p class="fill-sub">Please change your billing plan here</p>
                                    <input type="hidden" id="user_id" name="userid" value="1883">
                                    <div class="settcol right">
                                        <p class="btn-x">
                                        <p class="btn-x">
                                            <a href="javascript:void(0)" class="btn-f Change_Plan chossbpanss"><?php echo ($data[0]['plan_name'] != '') ? "Change plan" : "Select plan"; ?></a>
                                        </p>
                                        <p class="btn-x bckbtntoback" style="display:none">
                                            <a href="javascript:void(0)" class="btn-f Change_Plan backfromnewplan">Back</a>
                                        </p>
                                        </p>
                                    </div>
                                </div>

                                <div class="row billing_plan_view">
                                    <div class="col-md-6 col-sm-6 current_actv_pln billing_plan">
                                        <div class="flex-grid">
                                            <div class="price-card best-offer">
                                                <div class ="chooseplan active">
                                                    <?php
                                                    if ($data[0]['plan_name'] != '') {
                                                        $agency_priceclass = "<span class='" . $plan_data[0]['plan_type'] . " agency_price'> " . $plan_data[0]['plan_price'] . "</span>";
                                                        ?>
                                                        <h2><?php echo $plan_data[0]['plan_name']; ?></h2>
                                                        <h3><font>$</font><?php echo $agency_priceclass; ?><span>/<?php echo $plan_data[0]['plan_type']; ?></span></h3>
                                                        <div class="p-benifite">
                                                            <?php if ($type_ofuser == 'fortynine_plans') { ?>
                                                                <ul>
                                                                    <li><p>Total Requests: <?php echo $data[0]['total_requests']; ?></p></li>
                                                                    <li><p>Added Requests: <?php echo $request['added_request']; ?></p></li>
                                                                    <li><p>Pending Requests: <?php echo $request['pending_req']; ?></p></li>
                                                                </ul>
                                                            <?php } else { 
                                                            if(!empty($plan_data[0]['tier_prices'])){
                                                                        if ($plan_data[0]['plan_type'] == 'yearly') {
                                                                            $amount = 'annual_price';
                                                                        } else {
                                                                            $amount = 'amount';
                                                                        }
                                                                        $options = "";
                                                                        foreach ($plan_data[0]['tier_prices'] as $tier_prices) {
                                                                            $options .= '<option value ="' . $tier_prices['quantity'] . '" data-amount="' . $tier_prices[$amount] . '">' . $tier_prices['quantity'] . ' Dedicated Designer</option>';
                                                                        }
                                                                        $features = str_replace('{{QUANTTY}}', $options, $plan_data[0]['features']);
                                                                            echo $features; 
                                                                        }else{
                                                                            echo $plan_data[0]['features'];
                                                                        }
                                                             } ?>
                                                        </div>
                                                        <div class="price-sign-up">
                                                            <a type="button" class="ud-dat-p currentPlan" disabled>
                                                                Current Plan
                                                            </a>
                                                        </div>
                                                        <div class="active-check" style="display: none"><i class="fas fa-check"></i></div>
                                                        <?php
                                                    } else {
                                                        echo "no selected Plan";
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row billing_plan new_plan_billingfr_chng two-can owl-carousel" style="display:none">

                                    <?php
                                    foreach ($userplans as $userplan) {
                                        $agencyclass = $userplan['plan_type'] . " upgrd_agency_pln";
                                        $agency_priceclass = "<span class='" . $userplan['plan_type'] . " agency_price'> " . $userplan['plan_price'] . "</span>";
                                        ?>
                                        <div class="flex-grid">
                                            <div class="price-card">
                                                <div class="chooseplan">
                                                    <h2><?php echo $userplan['plan_name']; ?></h2>
                                                    <h3><font>$</font><?php echo $agency_priceclass; ?><span>/<?php echo $userplan['plan_type']; ?></span></h3>
                                                    <div class="p-benifite">
                                                        <div class="price-details">
                                                            <?php 
                                                            if($userplan['plan_type'] == 'yearly'){
                                                                $amount = 'annual_price';
                                                            }else{
                                                                $amount = 'amount';
                                                            }
                                                            $options = "";
                                                             foreach ($userplan['tier_prices'] as $tier_prices){
                                                                 $options .= '<option value ="'.$tier_prices['quantity'].'" data-amount="'.$tier_prices[$amount].'">'.$tier_prices['quantity'].' Dedicated Designer</option>';
                                                             }
                                                            $features = str_replace('{{QUANTTY}}', $options, $userplan['features']);
                                                            echo $features; 
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="price-sign-up">
                                                            <?php 
                                                            if($plan_data[0]['plan_type'] == 'yearly' && $userplan['plan_type'] == 'monthly'){ ?>
                                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#planpermissionspopup" type="button" class="ChngPln ud-dat-p <?php echo $agencyclass; ?>">
                                                                    Change Plan
                                                                </a>
                                                            <?php }else{
                                                            if ($data[0]['is_cancel_subscription'] == 1 && $data[0]['parent_id'] == 0 && $data[0]['plan_name'] == "") { ?> 
                                                                <a href="javascript:void(0)" data-value="<?php echo $userplan['plan_id']; ?>" data-price="<?php echo $userplan['plan_price']; ?>" data-inprogress="<?php echo $userplan['in_progress_request']; ?>" data-display_name="<?php echo $userplan['plan_name']; ?>" data-toggle="modal" data-target="#CnfrmPopup" type="button" class="ChngPln ud-dat-p <?php echo $agencyclass; ?>">
                                                                    Choose Plan
                                                                </a>
                                                            <?php } else if($data[0]['plan_name'] == ""){ ?>
                                                                <a href="javascript:void(0)" data-value="<?php echo $userplan['plan_id']; ?>" data-price="<?php echo $userplan['plan_price']; ?>" data-inprogress="<?php echo $userplan['in_progress_request']; ?>" data-display_name="<?php echo $userplan['plan_name']; ?> "type="button" class="upgrd ud-dat-p <?php echo $agencyclass; ?>">
                                                                    Choose Plan
                                                                </a>
                                                            <?php }else { ?> 
                                                                <a href="javascript:void(0)" data-value="<?php echo $userplan['plan_id']; ?>" data-price="<?php echo $userplan['plan_price']; ?>" data-inprogress="<?php echo $userplan['in_progress_request']; ?>"  data-display_name="<?php echo $userplan['plan_name']; ?>" data-toggle="modal" data-target="#CnfrmPopup" type="button" class="ChngPln ud-dat-p <?php echo $agencyclass; ?>">
                                                                    Change Plan
                                                                </a>
                                                            <?php } 
                                                            } ?>
                                                    </div>
                                                    <div class="active-check" style="display: none"><i class="fas fa-check"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</section>

<!-- Modal -->
<div class="modal similar-prop fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Change Password</h2>
                    <div class="cross_popup" data-dismiss="modal"> x</div>
                </header>
                <div class="cli-ent-model">
                    <div class="fo-rm-body">
                        <form method="post" action="<?php echo base_url(); ?>admin/accounts/change_password_front/<?php echo $data[0]['id'] ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="form-group">
                                        <p class="label-txt">New Password</p>
                                        <input type="password" required class="input"  name="new_password">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-sm-12">
                                    <label class="form-group">
                                        <p class="label-txt">Confirm Password</p>
                                         <input type="password" required class="input" name="confirm_password">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <button class="btn-g btn btn-ydelete" name="savebtn">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Confirm popup for change plan -->
<div class="modal fade similar-prop" id="CnfrmPopup" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Change Current Plan</h2>
                <div class="cross_popup" data-dismiss="modal">x</div>
            </header>
            <div class="cli-ent-model-box">

                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/change_plan.svg">
                    <h3 class="head-c text-center">Are you sure you want to Change Current Plan? </h3>
                    <form action="<?php echo base_url(); ?>admin/dashboard/change_current_plan" method="post" role="form" class="ChangeCurrent_Plan" id="ChangeCurrent_Plan">
                        <input type="hidden" class="form-control plan_name_to_upgrade plan_name" name="plan_name" id="plan_name" value="" >
                        <input type="hidden" class="form-control plan_price" name="plan_price" value="" >
                        <input type="hidden" class="form-control plan_customer_id" name="plan_customer_id" value="<?php echo $data[0]['id']; ?>" >
                        <input type="hidden" class="form-control in_progress_request" name="in_progress_request" id="in_progress_request" value="1" >
                        <div class="confirmation_btn text-center">
                            <button class="btn btn-CnfrmChange btn-ndelete" type="submit">Yes</button>
                            <button class="btn btn-ydelete" data-dismiss="modal" aria-label="Close">No</button>

                        </div>  

                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal popup -->
<!-- Access denied popup for change plan -->
  <div class="modal fade similar-prop" id="planpermissionspopup" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <header class="fo-rm-header">
          <h2 class="popup_h2 del-txt">Access Denied</h2>
          <div class="cross_popup" data-dismiss="modal"> x</div>
        </header>
        <div class="cli-ent-model-box">
          <div class="cli-ent-model">
            <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
              <div class="fo-rm-header">
                <br>
                  <h3 class="head-c text-center">You can't move  directly 'Annually to Monthly Plan'. Please contact us for more info. </h3>
              </div>
             </div>
         </div>
       </div>
     </div>
   </div>
<!-- End Modal popup -->
<!-- Model for sub project -->
<div class="modal fade similar-prop" id="Deletesubusers" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">


            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Delete User</h2>
                <div class="cross_popup" data-dismiss="modal"> x </div>
            </header>
            <div class="cli-ent-model">
                <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/delete-bg.svg">
                <h3 class="head-c text-center">Are you sure you want to delete this user? </h3>

                <div class="confirmation_btn text-center">
                    <form action="<?php echo base_url(); ?>admin/Subusers/delete_sub_user" method="post">
                        <input type="hidden" class="mainuseris" name="mainuseris" value="<?php echo $data[0]['id']; ?>">
                        <input type="hidden" class="subusersid" name="subusersid" id="subusersid" value="">
                        <button type="submit" class="btn btn-ydelete">Delete</button>
                    </form>
                    <button class="btn btn-ndelete" data-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- load agency quantity value -->
<div class="first-Ul" style="display:none">
    <span>Up to</span>
    <select class="pricebasedquantity">
        <option value="2" >2</option>
    </select>
    <select class="agencypricebasedquanity"><option value="2" >2</option></select>
    <span>active requests</span>
</div>
<!-- end load agency quantity value -->
<!--------------end requests upgrade popup------------->
<!-- jQuery (necessary for JavaScript plugins) -->
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js');?>"></script>
<link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'front_end/Updated_Design/owl/owl.theme.default.min.css');?>" rel = 'stylesheet'>
<link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'front_end/Updated_Design/owl/owl.carousel.css');?>" rel="stylesheet">
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'front_end/Updated_Design/owl/owl.carousel.js');?>"></script>
<script>
var userCurrentplan = "<?php echo $data[0]['current_plan']; ?>";
var total_inprogress_req = '<?php echo $data[0]['total_inprogress_req']; ?>';

    $(".chooseplan.active .pricebasedquantity").val(total_inprogress_req).find("option[value=" + total_inprogress_req +"]").attr('selected', true);
      var price_activepln = $(".chooseplan.active .pricebasedquantity option:selected").attr('data-amount');
      $(".chooseplan.active").find('.agency_price').html(price_activepln);
      
      $(document).on('change', '.pricebasedquantity', function () {
    //jQuery('.pricebasedquantity').on('change', function () {
        var Subs_price = $('option:selected', this).attr('data-amount');
        var subs_quantity = this.value;
        var final_SubPrice = Subs_price;
        jQuery(this).closest('.price-card').find('.upgrd_agency_pln').attr('data-price', final_SubPrice);
        jQuery('.toatal-bill-p.text-right').html('<span class="appened_price">$' + final_SubPrice + '</span>');
        jQuery('.chooseplan').find('h3.agency_updated').html('<font><span class="currency-sign">$</span>' + final_SubPrice + '/Month</font>');
        jQuery(this).closest('.price-card').find('.upgrd_agency_pln').attr('data-inprogress', subs_quantity);
        jQuery(this).closest('.price-card').find('.agency_price').html(final_SubPrice);
        jQuery('.disc-code').val('');
        jQuery('.ErrorMsg').css('display', 'none');
        jQuery('.coupon-des-newsignup').css('display', 'none');
    });
    
    var subusers_reqdata = '<?php echo json_encode($checksubuser_requests) ?>';
    $(document).on('click', '.chossbpanss', function () {
        $(this).css('display', 'none');
        $('.billing_plan_view').css('display', 'none');
        $('.new_plan_billingfr_chng').css('display', 'flex');
        $('.purchase_req').css('display', 'block');
        $('.bckbtntoback').css('display', 'block');
    });
    $(document).on('click', '.backfromnewplan', function () {
        $('.billing_plan_view').css('display', 'block');
        $('.Change_Plan').css('display', 'block');
        $('.new_plan_billingfr_chng').css('display', 'none');
        $('.purchase_req').css('display', 'none');
        $('.bckbtntoback').css('display', 'none');
    });

     $('.new_plan_billingfr_chng').owlCarousel({
              autoplayHoverPause: true,
              loop:true,
              autoplay:true,
              margin:20,
              dots:false,
              nav:true,
              items:3,
              responsive: {
                0: {
                  items: 1
                },
                600: {
                  items: 2
                },
                992: {
                  items: 3  
                }
              }
    });
</script>
</body>
</html>
