<section class="con-b">
    <div class="container-fluid">
        <?php if ($this->session->flashdata('message_error') != '') { ?>				
            <div class="alert alert-danger alert-dismissable">
                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>				
            <div class="alert alert-success alert-dismissable">
                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
            </div>
        <?php } ?>
        <div class="flex-this">
            <h2 class="main_page_heading">QA Profile</h2>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <div class="client-profile">
                    <div class="profile-pic">
                            <?php if ($edit_profile[0]['profile_picture'] != "") { ?>
                                <figure class="setimg-box33">
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $edit_profile[0]['profile_picture']; ?>" class="img-responsive telset33">
                                </figure>
                            <?php } else { ?>
                                <figure class="setimg-box33" style="background: #0190ff; text-align: center; font-size: 30px; color: #fff;padding-top: 57px;font-family: GothamPro-Medium;">
                                    <?php echo ucwords(substr($edit_profile[0]['first_name'], 0, 1)) . ucwords(substr($edit_profile[0]['last_name'], 0, 1)); ?>
                                </figure>
                            <?php } ?>
                    </div>
                    <div class="main-discp">
                        <h3>QA</h3>
                        <h2>
                            <?php echo $edit_profile[0]['first_name'] . " " . $edit_profile[0]['last_name'] ?>
                        </h2>
                        <p>
                            <?php echo $edit_profile[0]['email']; ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="list-header-blog client-inform">
                    <li class="active"><a href="#general" class="stayhere" data-toggle="tab">Profile Info</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">
                    <div class="tab-pane active" id="general"> 
                        <div class="tabs-card">
                            <form method="post" action="<?php echo base_url(); ?>admin/accounts/editqadetaiL/<?php echo $edit_profile[0]['id']; ?>" id="withoutpic">
                                <div class="settingedit-box">
                                    <div class="settrow">
                                        <h2 class="main-info-heading">Profile Information</h2>
                                        <p class="fill-sub">Please fill your Profile Information</p>
                                        <div class="settcol right">
                                            <p class="btn-x">
                                                <input type="submit" class="btn-e" value="Save" name="saveother">
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($edit_profile[0]['first_name']) && $edit_profile[0]['first_name'] != "") ? "label-active" : ""; ?>"> First Name</p>
                                            <input type="text" class="input" value="<?php echo $edit_profile[0]['first_name']; ?>" name="first_name" required>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($edit_profile[0]['last_name']) && $edit_profile[0]['last_name'] != "") ? "label-active" : ""; ?>"> Last Name</p>
                                            <input type="text" class="input" value="<?php echo $edit_profile[0]['last_name']; ?>" name="last_name" required>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($edit_profile[0]['email']) && $edit_profile[0]['email'] != "") ? "label-active" : ""; ?>"> Email</p>
                                            <input type="email" class="input" value="<?php echo $edit_profile[0]['email']; ?>" name="email" required>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($edit_profile[0]['paypal_email_address']) && $edit_profile[0]['paypal_email_address'] != "") ? "label-active" : ""; ?>"> Paypal Id</p>
                                            <input type="text" class="input" value="<?php echo $edit_profile[0]['paypal_email_address']; ?>" name="paypalid">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($edit_profile[0]['title']) && $edit_profile[0]['title'] != "") ? "label-active" : ""; ?>"> Title</p>
                                            <input type="text" class="input" value="<?php echo $edit_profile[0]['title']; ?>" name="title" >
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($edit_profile[0]['phone']) && $edit_profile[0]['phone'] != "") ? "label-active" : ""; ?>"> Phone</p>
                                            <input type="text" class=" input"value="<?php echo $edit_profile[0]['phone']; ?>" name="phone">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($edit_profile[0]['address_line_1']) && $edit_profile[0]['address_line_1'] != "") ? "label-active" : ""; ?>"> Address</p>
                                            <textarea class="input textarea-b" name="address"><?php echo $edit_profile[0]['address_line_1']; ?></textarea>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($edit_profile[0]['city']) && $edit_profile[0]['city'] != "") ? "label-active" : ""; ?>"> City</p>
                                            <input type="text" class="input" value="<?php echo $edit_profile[0]['city']; ?>" name="city">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>

                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($edit_profile[0]['state']) && $edit_profile[0]['state'] != "") ? "label-active" : ""; ?>"> State</p>
                                            <input type="text" class="input" placeholder="" value="<?php echo $edit_profile[0]['state']; ?>" name="state">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>

                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($edit_profile[0]['zip']) && $edit_profile[0]['zip'] != "") ? "label-active" : ""; ?>"> Zip</p>
                                            <input type="text" class="input" placeholder="" value="<?php echo $edit_profile[0]['zip']; ?>" name="zip">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="ChangePasswordModel" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c">Change Password</h3>
                    </header>
                    <div class="fo-rm-body">
                        <form action="<?php echo base_url(); ?>admin/accounts/change_password_old/<?php echo $edit_profile[0]['id']; ?>" method="post">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">New Password</label>
                                        <p class="space-a"></p>
                                        <input type="password" required class="form-control input-c" name="new_password">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Confirm Password</label>
                                        <p class="space-a"></p>
                                        <input type="password" required class="form-control input-c" name="confirm_password">
                                    </div>
                                </div>
                            </div>

                            <p class="btn-x"><button type="submit" class="btn-g">Save</button></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script type="text/javascript">
//    $(document).ready(function () {
//        $('.editprofile-info-pic').click(function (e) {
//            e.preventDefault();
//            $('.profile_pic_sec').hide();
//            $('.edit_profile_pic_sec').css('display', 'block');
//        });
//
//        $('.other_profile_info_btn').click(function (e) {
//            e.preventDefault();
//            $('.other_profile_info').hide();
//            $('.edit_other_profile_info').css('display', 'block');
//        });
//    });

//    function validateAndUpload(input) {
//        var URL = window.URL || window.webkitURL;
//        var file = input.files[0];
//        if (file) {
//            console.log(URL.createObjectURL(file));
//            $(".imagemain").hide();
//            $(".imagemain3").show();
//            $("#image3").attr('src', URL.createObjectURL(file));
//            $(".dropify-wrapper").show();
//        }
//    }
//    $(document).on('click', '.box-setting ul#right_nav li', function () {
//        $(this).toggleClass('open');
//    });
</script>

