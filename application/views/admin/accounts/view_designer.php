	<?php /* ?>
	<h2 class="float-xs-left content-title-main" style="display:inline-block;">Designers</h2>
	<a href="<?php echo base_url()."admin/accounts/view_qa_va";?>"><h2 class="float-xs-left content-title-main" style="padding-left:10%;display:inline-block;color:#cac8c8;">QA / VA</h2></a>
	<?php */ ?>

</div>

<div class="col-md-12" style="background:white;">
<div class="">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147 !important; } 
.darkblackbackground { background-color:#1a3147; }
.pinktext { color: #ec4159; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.font16 { font-size:16px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
	font-size: 31px !important;
    padding: 8px 0px !important;
    font-weight: 600 !important;
    letter-spacing: -3px !important;
}
.projecttitle{
	font-size: 25px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: 1px solid #000;
    background: unset;
    
}
.trborder:hover{
	box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
	background-color:#fff;
}
.table-hover tbody tr:hover{
background-color:#fff;
}
table {border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}

#content-wrapper .content{padding-top: 0;}

/*
.nav-tab-pills-image ul li .nav-link {
    color:#1a3147;
	font-weight: 600;
    padding: 7px 30px;
    font-size: 13px;
}
.content .nav-tab-pills-image .nav-item.active a {
border:unset !important;
}
.wrapper .nav-tab-pills-image ul li .nav-link:hover{
	color:#000;
}

.wrapper .nav-tab-pills-image ul li .nav-link{
	color: #2f4458;
	height:43px;
}
.nav-tab-pills-image ul li .nav-link:hover {
    border-bottom: none !important;
}
.wrapper .nav-tab-pills-image ul li .nav-link:focus
{	
	color:#fff;
	border:none;
}
.content .nav-tab-pills-image .nav-item.active a:hover{
	color:#fff;
}
.content .nav-tab-pills-image .nav-item.active dd {
	background:unset !important;
}
.nav-tab-pills-image ul li .nav-link:hover {
    border-bottom: none !important;
}
*/

.wrapper .nav-tab-pills-image ul li .nav-link {
    padding: 30px 15px;
    font-size: 18px;
}
.wrapper .nav-tab-pills-image ul li.float-right .nav-link{
	padding: 24px 15px;
}
</style>

    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="nav-tab-pills-image mytab">
					
						<ul class="nav nav-tabs" role="tablist" style="border:none;">
							<li class="nav-item active">
								<a class="nav-link" data-toggle="tab" href="#current_designer" role="tab">
									Current Designer
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#designer_application" role="tab">
									Designer Application
								</a>
							</li>

							<li class="nav-item float-right">
								<p class="nav-link dd">
									<i class="fa fa-search onlysearch" style="padding-left:12px; padding-top:8px;"></i>
									<input class="fbold" type="text" id="myInput" onkeyup="myFunction()" name="search_request" placeholder="Search" style="border:none;">
								</p>
							</li>
							<li class="nav-item float-right">
								<p class="nav-link dd">
									<button class="add_custom_btn" data-toggle="modal" data-target="#add_designer">+ Add Designer</button>									
								</p>
							</li>
						</ul>
					

                    <div class="tab-content">
                        <div class="tab-pane active content-datatable datatable-width" id="current_designer" role="tabpanel">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
                                	<?php if($this->session->flashdata('message_error') != '') {?>
                                	   <div class="alert alert-danger alert-dismissable">
                                			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                			<strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
                                		</div>
                                	   <?php }?>
                                	   <?php if($this->session->flashdata('message_success') != '') {?>				
                                	   <div class="alert alert-success alert-dismissable">
                                			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                			<strong><?php echo $this->session->flashdata('message_success');?></strong>
                                		</div>
                                    <?php }?>
										<div class="table_draft_sec table_draft_complete customers_table_sec qa_client_table">
										<table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive dataTable" style=" border-collapse:separate; border-spacing: 0 1em;border:none; padding-top: 15px;" id="DataTables_Table_0">
										    <tbody class="">
										    <?php for($i=0;$i<sizeof($designers);$i++){ ?>            
										        <tr onclick="" class="trborder" id="<?php echo $designers[$i]['id'] ?>" data-indexid="0">
										            <td class="completed image_dv image_dv1">
										            <?php
										            if ($designers[$i]['profile_picture']) 
										            {
										            ?>
										            <div class="img">
										              <img class="image_circle" src="<?php echo base_url() . 'uploads/profile_picture/' . $designers[$i]['profile_picture'] ; ?>">
										            </div>
										      
										            <?php  
										            }
										            else {   ?>
										            <div class="img">
										              <img class="image_circle" src="<?php echo base_url() . 'uploads/profile_picture';?>/13b2d60af0a45fefd0d0d0633339a453.jpeg">
										            </div>
										            <?php
										            }
										            ?>
										            </td>
										            <td class="desc">
										                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $designers[$i]['first_name']." ".$designers[$i]['last_name']?> </p>
										                <p><span class="">Member since <?php echo date("F Y",strtotime($designers[$i]['created']))?></span></p>
										            </td>
										                        
										            
										            <td class="requests">
										                <div class="text">No. of Requests</div>
										                <div class="num"><?php echo $designers[$i]['handled']; ?></div>
										            </td>

										            <td class="requests active_req">
										                <div class="text">Active Requests</div>
										                <div class="num"><?php echo $designers[$i]['active_request']; ?></div>
										            </td>
										        </tr>
										    <?php } ?>
										    </tbody>
										</table>
										</div>

								 	<?php /*
                                     <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none;">
                                        
                                        <tbody>
										<?php for($i=0;$i<sizeof($designers);$i++){ ?>
										   <tr class="trborder">
												<td style="width: 500px;">
													<center>
														 <div class="col-sm-2" style="padding:0px;">
															 
															<?php
															if ($designers[$i]['profile_picture']) 
															{
															?>
															 <div class="myprofilebackground" ><p style="padding-top: 13px;">
															 <?php
																echo '<img src="' . base_url() . 'uploads/profile_picture/' . $designers[$i]['profile_picture'] . '" style=" height:60px;width:60px; border-radius:50%;border: 3px solid #0190ff;margin-left:0px;">';
															
															?>
															 </p></div>         
															<?php  
															}
															else 
															{
															?>
															<div class="myprofilebackground" style="width:60px; height:60px;border-radius:50%; background: #0190ff;border: 3px solid #0190ff; text-align: center; font-size: 15px; color: #fff;"><p style="font-size: 22px;padding-top: 15px;">
																	
															<?php 
																
																echo ucwords(substr($designers[$i]['first_name'],0,1)) .  ucwords(substr($designers[$i]['last_name'],0,1));
															?>
																</p></div>
															<?php
															}
															?>
														</div>
                                                    </center>
													<div style="display:inline-block;width:70%;text-align: left;padding-left: 20px;">
														<h4 class="darkblacktext" style="display: inline-block;font-size: 18px;font-weight: 600;"><?php echo $designers[$i]['first_name']." ".$designers[$i]['last_name']?> | </h4>
														<p class="greytext" style="display: inline-block;font-size: 13px;font-weight: 100;padding: 0px;"><?php echo $designers[$i]['email']; ?></p></br>
														<p class="greytext" style="display: inline-block;font-size: 13px;font-weight: 100;">Member since <?php echo date("M d, Y",strtotime($designers[$i]['created']))?></p>
													</div>													
												</td>
												<td>
													<p class="greytext pb0 weight600 ">Requests being handled</p>
													<p class="darkblacktext weight600"><?php echo $designers[$i]['handled']; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 ">Active Requests</p>
													<p class="darkblacktext weight600"><?php echo $designers[$i]['active_request']; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 ">Rating</p>
													<?php  
													$rate= $designers[$i]['total_rating'][0]['grade'];
													
                                                    for ($j = 1; $j <= 5; $j++) {
														if ($j <= $rate) {
															echo '<i class="fa fa-star pinktext"></i>';
														} else {
															echo '<i class="fa fa-star greytext"></i>';
														}
													}
                                                                
													?>
													
												</td>
												<!--<td>
													<p class="greytext pb0 weight600  directmessage" style="display: inline-block;margin-left: 5px;" data-clientname="James Franco" data-clientid="3" data-imageurl="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;">Direct Message</p>
													<a href="<?php echo base_url()."admin/accounts/client_info/".$designers[$i]['id']; ?>"><p><svg style="vertical-align: sub;width: 0.5em;" aria-hidden="true" data-prefix="fas" data-icon="comment-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt fa-w-16 fa-2x" ><path fill="orange" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z" class=""></path></p></svg></a>
												</td>-->
											</tr> 
										<?php } ?>
											 
										</tbody>
									</table>

									*/?>
									 
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="designer_application" role="tabpanel">
                            <div class="row">
								

                                <div class="col-md-12" style="margin-top: 40px;">
                                    <div class="col-sm-3" style="padding: 8px;">
										<div class="col-sm-12 portfolio_bx">
											<div style="width:100%;height:150px;" class="greybackground"></div>
											<h5 class="darkblacktext weight600">PORTFOLIO IMAGE 1</h5>
											<p class="darkblacktext">Web</p>
											<p class="greytext">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac felis rhoncus, posuere est eget, pretium felis. Ut lacinia condimentum consequat. Phasellus non tempor</p>
											<div class="row">
											<div class="col-sm-6">
												<p class="darkblacktext weight600 font16">02/23/17</p>
											</div>
											<div class="col-sm-6" style="float:right;">
												<i class="fa fa-trash pinktext font16" style="float:right;"></i>
												<p class="greytext font16" style="display:inline-block;float: right;margin-right: 10px;">EDIT</p>
											</div>
											</div><!-- row -->
										</div>
									 </div>
									 <div class="col-sm-3" style="padding: 8px;">
										<div class="col-sm-12 portfolio_bx">
											<div style="width:100%;height:150px;" class="greybackground"></div>
											<h5 class="darkblacktext weight600">PORTFOLIO IMAGE 1</h5>
											<p class="darkblacktext">Web</p>
											<p class="greytext">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac felis rhoncus, posuere est eget, pretium felis. Ut lacinia condimentum consequat. Phasellus non tempor</p>
											<div class="row">
											<div class="col-sm-6">
												<p class="darkblacktext weight600 font16">02/23/17</p>
											</div>
											<div class="col-sm-6" style="float:right;">
												<i class="fa fa-trash pinktext font16" style="float:right;"></i>
												<p class="greytext font16" style="display:inline-block;float: right;margin-right: 10px;">EDIT</p>
											</div>
											</div><!-- row -->
										</div>
									 </div>
									 <div class="col-sm-3" style="padding: 8px;">
										<div class="col-sm-12 portfolio_bx">
											<div style="width:100%;height:150px;" class="greybackground"></div>
											<h5 class="darkblacktext weight600">PORTFOLIO IMAGE 1</h5>
											<p class="darkblacktext">Web</p>
											<p class="greytext">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac felis rhoncus, posuere est eget, pretium felis. Ut lacinia condimentum consequat. Phasellus non tempor</p>
											<div class="row">
											<div class="col-sm-6">
												<p class="darkblacktext weight600 font16">02/23/17</p>
											</div>
											<div class="col-sm-6" style="float:right;">
												<i class="fa fa-trash pinktext font16" style="float:right;"></i>
												<p class="greytext font16" style="display:inline-block;float: right;margin-right: 10px;">EDIT</p>
											</div>
											</div><!-- row -->
										</div>
									 </div>
									 <div class="col-sm-3" style="padding: 8px;">
										<div class="col-sm-12 portfolio_bx">
											<div style="width:100%;height:150px;" class="greybackground"></div>
											<h5 class="darkblacktext weight600">PORTFOLIO IMAGE 1</h5>
											<p class="darkblacktext">Web</p>
											<p class="greytext">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac felis rhoncus, posuere est eget, pretium felis. Ut lacinia condimentum consequat. Phasellus non tempor</p>
											<div class="row">
											<div class="col-sm-6">
												<p class="darkblacktext weight600 font16">02/23/17</p>
											</div>
											<div class="col-sm-6" style="float:right;">
												<i class="fa fa-trash pinktext font16" style="float:right;"></i>
												<p class="greytext font16" style="display:inline-block;float: right;margin-right: 10px;">EDIT</p>
											</div>
											</div><!-- row -->
										</div>
									 </div>
									 
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="pendingforapproverequest" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
</div>

<form method="post" action="">
<div id="add_designer" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top: 10%;">

    <!-- Modal content-->
    <div class="modal-content add_client_model">
		<div class="modal-header" style="display:none;">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modal Header</h4>
		</div>
		<div class="modal-body" style="padding:0px;">
			<div class="col-sm-12" style="padding:30px;">
				
				<div class="col-sm-12" style="padding:0;">
					<div class="col-sm-12">
						<h3 class="darkblacktext weight600 ls0" style="margin-bottom: 20px;">Add Designer</h3>
					</div>
					
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="fname" class="darkblacktext weight600"><strong>First Name</strong></label>
						<input type="text" class="form-control" name="first_name" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>	
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Last Name</label>
						<input type="text" class="form-control" name="last_name" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Email</label>
						<input type="text" class="form-control" name="email" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Phone</label>
						<input type="text" class="form-control" name="phone" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-12" style="padding-bottom:20px; padding-top:20px;">
						<p style="border-bottom:1px solid #c7c9cc; padding-bottom:0;"></p>
					</div>
				</div>
			
				<div class="col-sm-12" style="padding:0;">
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="fname" class="darkblacktext weight600"><strong>Password</strong></label>
						<input type="password" class="form-control" name="password" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>	
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Confirm Password</label>
						<input type="password" class="form-control" name="confirm_password" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
				</div>
				
				<div class="col-sm-12">
					<button type="submit" class="btn pinkbackground weight600" style="margin:  auto;height: 60px;font-size:20px;margin-top: 20px;width: 100%;">Add Designer</button>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" style="display:none;">Close</button>
		</div>
	</div>
			
   </div>
	
</div>
</form>

<div id="add_portflio" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top: 10%;">

    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header" style="display:none;">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modal Header</h4>
		</div>
		<div class="modal-body" style="padding:0px;">
			<div class="col-sm-12" style="padding:30px;">
				<div class="col-sm-12">
					<h3 class="darkblacktext weight600 ls0" style="margin-bottom: 20px;">Add Portfolio</h3>
					<div class="col-sm-4 greybackground">
						<a href="#" style="border: 3px solid;border-radius: 73px;padding: 29px 28px 32px 36px;;margin: auto;display: block;width: 100px;margin-top: 14px;" class="greytext">
							<span class="fa fa-plus" style="font-size: 30px;"></span>
						</a>
						<p style="text-align:center;margin-top:10px;font-size: 18px;margin-bottom: 30px;">Add Image</p>
					</div>
					<div class="col-sm-8" style="margin-bottom:10px;">
						<div class="col-sm-12" style="margin-bottom:10px;">
							<label for="fname" class="darkblacktext weight600"><strong>Portfolio Category</strong></label>
							<select class="form-control" name="portfolio_category" style="background-color: #ededed  !important;border-radius: 6px !important;border:unset !important;">
								<option>Web</option>
							</select>
						</div>
						<div class="col-sm-12" style="margin-bottom:10px;">
							<label for="fname" class="darkblacktext weight600"><strong>Title</strong></label>
							<input type="text" class="form-control" name="portfolio_title" style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;" />
						</div>
						<div class="col-sm-12" style="margin-bottom:10px;">
							<label for="fname" class="darkblacktext weight600"><strong>Caption</strong></label>
							<textarea  style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;" name="portfolio_caption" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<div class="col-sm-12">		  
					<div class="col-sm-10" style="margin-bottom:10px;">
						<button type="button" class="btn pinkbackground weight600" style="margin:  auto;width: 50%;height: 50px;font-size:20px;margin-top: 20px;">Publish</button>
						<button type="button" class="btn darkblacktext weight600" style="margin:  auto;width: 40%;height: 50px;font-size:20px;margin-top: 20px;background:#fff;">Preview</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" style="display:none;">Close</button>
		</div>
	</div>
			
   </div>
	
</div>

<div id="directmessage" class="modal fade" role="dialog" style="margin-top:10%;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body " style="font-size: 170px;height: 270px;">
			
			<h3 style="letter-spacing: -1px;font-size: 21px;display:inline-block;" class="darkblacktext weight600">Direct Message to </h3>
			<h3 style="letter-spacing: -1px;font-size: 21px;display:inline-block;width:50%;" class="darkblacktext weight600 image_name">Direct Message to </h3>
			
			<div class="col-sm-10">
				<textarea class="form-control" style="height: 125px;margin-top: 10px;border: 1px solid !important;"></textarea>
			</div>
			<div class="col-sm-2">
				<i class="fa fa-send pinktext" style="font-size: 35px;"></i>
			</div>
      </div>
    </div>

  </div>
</div>
<script>
$(document).ready(function () {
	$("#myInput").on("keyup", function () {
		var value = $(this).val().toLowerCase();
		$(".active table tr").filter(function () {
			$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
		});
	});
});
</script>