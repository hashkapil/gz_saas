<h2 class="float-xs-left content-title-main" style="display:inline-block;">Client Info</h2>
</div>
<div class="col-md-12" style="background:white;">
<div class="col-md-10 offset-md-1">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147; } 
.darkblackbackground { background-color:#1a3147; }
.pinktext { color: #ec4159; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.font16 { font-size:16px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
	font-size: 18px !important;
    padding: 8px 0px !important;
    font-weight: 600 !important;
    line-height: 31px;
    text-align: left !important;
    padding-left: 10px !important;
}
.projecttitle{
	font-size: 25px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: 1px solid #000;
    background: unset;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
table {     border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}
.nav-tab-pills-image ul li .nav-link {
    color:#1a3147;
	font-weight:600;
	padding: 7px 25px;
}
</style>
    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;padding-bottom: 40px;border-bottom: 1px solid;">                
				<div class="col-sm-2">
					<h4 class="weight600 darkblacktext">Information</h4>
				</div>
				<div class="col-sm-5">
					<div class="col-sm-3" style="padding:0px;">
						<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;" />
					</div>
					<div class="col-sm-9">
						<h5 class="weight600 darkblacktext">+ Upload New Photo</h5>
					</div>
				</div>
				<div class="col-sm-5">
					<?php if($data[0]['plan_turn_around_days'] == "1"){ ?>
					<h5 class="weight600 darkblacktext" style="display:inline-block;">Subscription</h5>
					<button class="btn weight600 darkblackbackground whitetext" style="border-radius:5px;padding:15px;font-size:20px;">Premium Member</button>
					<?php }elseif($data[0]['plan_turn_around_days'] == "3"){ ?>
					<h5 class="weight600 darkblacktext" style="display:inline-block;">Subscription</h5>
					<button class="btn weight600 pinkbackground whitetext" style="border-radius:5px;padding:15px;font-size:20px;">Standard Member</button>
					<?php }else{ ?>
					<h5 class="weight600 darkblacktext" style="display:inline-block;">Subscription</h5>
					<button class="btn weight600 greybackground whitetext" style="border-radius:5px;padding:15px;font-size:20px;">No Plan</button>
					<?php } ?>
				</div>
				<div class="col-sm-2" style="clear:both;">&nbsp;</div>
				<div class="col-sm-5">
					<h5 class="weight600 darkblacktext" style="clear: both;padding-top: 20px;">Client Name</h5>
					<p class="greytext font16"><?php echo $data[0]['first_name']." ".$data[0]['last_name']; ?></p>
					<h5 class="weight600 darkblacktext">Email Address</h5>
					<p class="greytext font16"><?php echo $data[0]['email']; ?></p>
					<h5 class="weight600 darkblacktext">Contact Number</h5>
					<p class="greytext font16"><?php echo $data[0]['phone']; ?></p>
					<h5 class="weight600 darkblacktext">Password<span class="pinktext" style="font-size: 14px;font-weight: 100;padding-left: 10px;">Change Password</span></h5>
					<p class="greytext font16"><?php echo $data[0]['new_password']; ?></p>
				</div>
				<div class="col-sm-5">
					<p>
						<i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star"></i>
					</p>
					<h5 class="weight600 darkblacktext" style="clear: both;padding-top: 20px;">Billing Information<span class="pinktext" style="font-size: 14px;font-weight: 100;padding-left: 10px;cursor:pointer;" data-toggle="modal" data-target="#edit_billing">Edit Billing Information</span></h5>
					<p class="greytext font16 pb0"><?php echo $data[0]['first_name']." ".$data[0]['last_name']; ?></p>
					<p class="greytext font16 pb0"><?php echo $data[0]['email']; ?></p>
					<p class="greytext font16 pb0">Address Line 1</p>
					<p class="greytext font16 pb0">Address Line 2</p>
					<p class="greytext font16 pb0">City, Province, Zip Code</p>
					<p class="greytext font16 pb0">Country</p>
					<h5 class="weight600 darkblacktext pb0" style="clear: both;padding-top: 20px;">VISA - <span class="" style="font-size: 16px;font-weight: 100;">ending in 0123</span></h5>
				</div>
            </div>
			
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;">
				<div class="col-sm-2" style="clear:both;">
					<h4 class="weight600 darkblacktext">Requests</h4>
				</div>
				<div class="col-sm-10" style="text-align:center;">
					<div class="col-sm-3">
						<p class="greytext font18">No. of Requests</p>
						<h5 class="weight600 darkblacktext"><?php echo $data[0]['handled']; ?></h5>
					</div>
					<div class="col-sm-3">
						<p class="greytext font18">Active Requests</p>
						<h5 class="weight600 darkblacktext"><?php echo $data[0]['active_request']; ?></h5>
					</div>
					<div class="col-sm-3">
						<p class="greytext font18">Pending Requests</p>
						<h5 class="weight600 darkblacktext"><?php echo $data[0]['pending_request']; ?></h5>
					</div>
				</div>
			</div>
        </div>
    </div>

</section>
</div>

<div id="edit_billing" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top: 10%;">

    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header" style="display:none;">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modal Header</h4>
		</div>
		<div class="modal-body" style="padding:0px;">
			<div class="col-sm-12" style="padding:30px;">
				<div class="col-sm-12">
					<h3 class="darkblacktext weight600 ls0" style="margin-bottom: 20px;">Edit Billing Information</h3>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="fname" class="darkblacktext weight600"><strong>First Name:</strong></label>
						<input type="text" class="form-control" id="example1" name="First Name" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>	
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Last Name:</label>
						<input type="text" class="form-control" id="example1" name="Last Name" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="Email" class="darkblacktext weight600">Email:</label>
						<input type="text" class="form-control" id="Email" name="Email" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="Phone" class="darkblacktext weight600">Phone:</label>
						<input type="number" class="form-control" id="Phone" name="Phone" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="Email" class="darkblacktext weight600">Address Line 1:</label>
						<input type="text" class="form-control" id="address_1" name="address_1" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="Phone" class="darkblacktext weight600">Address Line 2:</label>
						<input type="number" class="form-control" id="address_2" name="address_2" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
				</div>
				<div class="col-sm-12">		  
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="Confirm Password" class="darkblacktext weight600">Select Payment Method:</label>
						<select class="form-control " id="sel1" style="background-color: #ededed  !important;border-radius: 6px !important;border:unset !important;">
							<option>Credit/Debit Card</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
						</select>
					</div>
				  
					<div class="col-sm-7" style="margin-bottom:10px;">
						<label for="Card Number" class="darkblacktext weight600">Card Number:</label>
						<input type="number" class="form-control" id="Card Number" name="Card Number" required style=" border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
				  
					<div class="col-sm-5" style="margin-bottom:10px;">
						<label for="edate" class="darkblacktext weight600">Expiration Date:</label>
						<input type="date" class="form-control" id="edate" name="Expiration Date" required style="border: 2px solid #c7c9cc !important;border-radius: 6px !important;">
					</div>
					<div class="col-sm-3" style="margin-bottom:10px;">
						<label for="Cvs" class="darkblacktext weight600">CVC:</label>
						<input type="text" class="form-control" id="CVC" name="CVC" required style="border: 2px solid #c7c9cc !important;border-radius: 6px !important;">
					</div>
				  
					
					<div class="col-sm-10" style="margin-bottom:10px;">
						<button type="button" class="btn pinkbackground weight600" style="margin:  auto;width: 80%;height: 60px;font-size:20px;margin-top: 20px;">Update Billing Information</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" style="display:none;">Close</button>
		</div>
	</div>
			
   </div>
	
</div>