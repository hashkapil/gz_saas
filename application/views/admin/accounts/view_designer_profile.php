<link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
<section class="con-b">
        <div class="container-fluid">
            <?php if ($this->session->flashdata('message_error') != '') { ?>				
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('message_success') != '') { ?>				
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                </div>
            <?php } ?>
            <div class="flex-this">
                <h2 class="main_page_heading">Designer Profile</h2>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-4">
                    <div class="client-profile">
                        <div class="profile-pic">
                            <figure>
                                <img src="<?php echo $data[0]['profile_picture']; ?>" class="img-responsive telset33">
                        </figure>
                    </div>
                    <div class="main-discp">
                        <h3>Designer</h3>
                        <h2>
                         <?php echo $data[0]['first_name'] . " " . $data[0]['last_name']; ?>
                        </h2>
                     <p>
                        <?php if (isset($data[0]['email'])): ?>
                            <?php echo $data[0]['email']; ?>
                        <?php endif ?>
                    </p>
                    <p class="chngpasstext">
                        <a href="" class="change_pass" name="savebtn" value="Change Password" data-toggle="modal" data-target="#myModal">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/customer/change-password.png" class="img-responsive"> 
                            <span>Change Password</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="detailed-item">
                <ul>
                    <li><label for=""><?php echo 'Member Since';?></label><span><?php echo ($data[0]['created']!= '') ? date("F Y", strtotime($data[0]['created'])) :'No Member'; ?></span></li>
                    <li><label for=""><?php echo 'Phone';?></label><span><?php echo ($data[0]['phone'] != '') ? $data[0]['phone']:'N/A'; ?></span></li>
                    <li><label for=""><?php echo 'Address';?></label><span><?php echo ($data[0]['address_line_1'] != '')?$data[0]['address_line_1']:'N/A'; ?></span></li>
                    <li><label for=""><?php echo 'State';?></label><span><?php echo ($data[0]['state'] != '') ? $data[0]['state']: 'N/A'; ?></span></li>
                    <li><label for=""><?php echo 'Zip Code';?></label><span><?php echo ($data[0]['zip'] != '')?$data[0]['zip']:'N/A'; ?></span></li>
                    <li><label for=""><?php echo 'City';?></label><span><?php echo ($data[0]['city']!='')?$data[0]['city']:'N/A'; ?></span></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="list-header-blog client-inform">
                <li class="active"><a href="#general" class="stayhere" data-toggle="tab">Profile Info</a></li>
                <li><a href="#skills" class="stayhere" data-toggle="tab">Skills</a></li>
            </ul>
        </div>
    </div>
    <div class="">
        <div class="tab-content">
            <div class="tab-pane active" id="general"> 
               <div class="tabs-card">
                <div class="project-row-qq1 profielveiwee-box">
                    <form method="post" action="<?php echo base_url(); ?>admin/accounts/designer_edit_profile/<?php echo $data[0]['id']; ?>">
                        <div class="settrow">
                            <h2 class="main-info-heading">Profile Information</h2>
                            <p class="fill-sub">Please fill your Profile Information</p>
                            <div class="settcol right">
                                <p class="btn-x">
                                    <button name="submit" type="submit" class="btn-e text-wrap">Save</button>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="form-group">
                                    <p class="label-txt <?php echo (isset($data[0]['first_name']) && $data[0]['first_name'] != "") ? "label-active" : ""; ?>">First Name</p>
                                    <input class="input" type="text" name="first_name" value="<?php echo $data[0]['first_name']; ?>" required>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>

                            <div class="col-sm-4">
                                <label class="form-group">
                                    <p class="label-txt <?php echo (isset($data[0]['last_name']) && $data[0]['last_name'] != "") ? "label-active" : ""; ?>">Last Name</p>
                                    <input class="input" type="text" name="last_name" value="<?php echo $data[0]['last_name']; ?>" required>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>
                            <div class="col-sm-4">
                                <label class="form-group">
                                    <p class="label-txt <?php echo (isset($data[0]['paypal_email_address']) && $data[0]['paypal_email_address'] != "") ? "label-active" : ""; ?>">Paypal id</p>
                                    <input class="input" placeholder="" type="text" name="paypal_id" value="<?php echo $data[0]['paypal_email_address']; ?>">
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <label class="form-group">
                        <p class="label-txt <?php echo (isset($data[0]['email']) && $data[0]['email'] != "") ? "label-active" : ""; ?>">Email</p>
                        <input class=" input" type="text" name="email" value="<?php echo $data[0]['email']; ?>" required>
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    </label>
                </div>

                <div class="col-sm-4">
                    <label class="form-group">
                        <p class="label-txt <?php echo (isset($data[0]['phone']) && $data[0]['phone'] != "") ? "label-active" : ""; ?>">Phone</p>
                        <input class="input"  type="text" name="phone_number" value="<?php echo $data[0]['phone']; ?>">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    </label>
                </div>
                <div class="col-sm-4">
                    <label class="form-group">
                        <p class="label-txt <?php echo (isset($data[0]['address_line_1']) && $data[0]['address_line_1'] != "") ? "label-active" : ""; ?>">Address Line 1</p>
                        <input class="input" type="text" name="address" value="<?php echo $data[0]['address_line_1']; ?>">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label class="form-group">
                        <p class="label-txt <?php echo (isset($data[0]['city']) && $data[0]['city'] != "") ? "label-active" : ""; ?>">City</p>
                        <input class="input" type="text" name="city" value="<?php echo $data[0]['city']; ?>">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    </label>
                </div>

                <div class="col-sm-4">
                    <label class="form-group">
                        <p class="label-txt <?php echo (isset($data[0]['state']) && $data[0]['state'] != "") ? "label-active" : ""; ?>">State</p>
                        <input class="input"  type="text" name="state" value="<?php echo $data[0]['state']; ?>">
                        <div class="line-box">
                            <div class="line"></div>
                        </div>
                    </label>
                </div>
                <div class="col-sm-4">
                   <label class="form-group">
                    <p class="label-txt <?php echo (isset($data[0]['zip']) && $data[0]['zip'] != "") ? "label-active" : ""; ?>">Zip</p>
                    <input class="input" type="text" name="zip_code" value="<?php echo $data[0]['zip']; ?>">
                    <div class="line-box">
                        <div class="line"></div>
                    </div>
                </label>
            </div>
        </div>
        <div class="row">
         <div class="col-md-12">
            <label class="form-group">
                <p class="label-txt <?php echo (isset($skills[0]['hobbies']) && $skills[0]['hobbies'] != "") ? "label-active" : ""; ?>">Hobbies</p>
                <textarea class="input"  name="hobbies"><?php echo (isset($skills[0]['hobbies']) && $skills[0]['hobbies']!='') ? $skills[0]['hobbies']:''; ?></textarea>
                <div class="line-box">
                    <div class="line"></div>
                </div>
            </label>
        </div>
        <div class="col-md-12">
            <label class="form-group">
                <p class="label-txt <?php echo (isset($skills[0]['about']) && $skills[0]['about'] != "") ? "label-active" : ""; ?>">About</p>
                <textarea class="input" name="about"><?php echo (isset($skills[0]['about']) && $skills[0]['about']!='')?$skills[0]['about']:'';?></textarea>
                <div class="line-box">
                    <div class="line"></div>
                </div>
            </label>
        </div>
    </div>
</form>
</div>
</div>  
</div>
<div class="tab-pane" id="skills">
    <div class="tabs-card">
        <div class="row">
            <div class="col-md-12">
                <div class="project-row-qq1 profielveiwee-box">
                    <form method="post" action="<?php echo base_url(); ?>admin/accounts/addskills/<?php echo $data[0]['id']; ?>">  
                        <div class="settrow">
                            <h2 class="main-info-heading">Skills</h2>
                            <p class="fill-sub">Please add Designer's skills</p>
                            <div class="settcol right">
                                <p class="btn-x">
                                    <button id="skills_submit" class="btn-e text-wrap">Save</button>
                                </p>
                            </div>
                        </div>
                        <?php if (!empty($skills[0])) {
                           // echo "<pre/>";print_R($skills[0]['brandin_logo']);
                            ?>
                            <div class="ui-slid-errow">
                                <div class="pro-gressbar">
                                    <h3 class="progress-title">Brandig & Logos</h3>
                                    <div class="progress-value-1">
                                        <span class="value value0" id="value1"><?php
                                        if ($skills[0]['brandin_logo']) {
                                            echo $skills[0]['brandin_logo'];
                                        } else {
                                            echo '0';
                                        }
                                        ?></span>%
                                        <input type="hidden" class="value_skill" name="brandin_logo" value="<?php
                                        if ($skills[0]['brandin_logo']) {
                                            echo $skills[0]['brandin_logo'];
                                            } else {
                                                echo '0';
                                            }
                                            ?>" id="sckill_val1">
                                        </div>
                                    </div>

                                    <div data-target="#value1" class="ui-slider-control" id="ui-slider-control0"></div>
                                </div>

                                <div class="ui-slid-errow">
                                    <div class="pro-gressbar">
                                        <h3 class="progress-title">Ads & Banners</h3>
                                        <div class="progress-value-1">
                                            <span class="value value1" id="value2"><?php
                                            if ($skills[0]['ads_banner']) {
                                                echo $skills[0]['ads_banner'];
                                            } else {
                                                echo '0';
                                            }
                                            ?></span>%
                                            <input type="hidden" class="value_skill" name="ads_banner" value="<?php
                                            if ($skills[0]['ads_banner']) {
                                                echo $skills[0]['ads_banner'];
                                                } else {
                                                    echo '0';
                                                }
                                                ?>" id="sckill_val2">
                                            </div>
                                        </div>

                                        <div data-target="#value2" class="ui-slider-control" id="ui-slider-control1"></div>
                                    </div>

                                    <div class="ui-slid-errow">
                                        <div class="pro-gressbar">
                                            <h3 class="progress-title">Email & Marketing</h3>
                                            <div class="progress-value-1">
                                                <span class="value value2" id="value3"><?php
                                                if ($skills[0]['email_marketing']) {
                                                    echo $skills[0]['email_marketing'];
                                                } else {
                                                    echo '0';
                                                }
                                                ?></span>%
                                                <input type="hidden" class="value_skill" name="email_marketing" value="<?php
                                                if ($skills[0]['email_marketing']) {
                                                    echo $skills[0]['email_marketing'];
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?>" id="sckill_val3">
                                                </div>
                                            </div>

                                            <div data-target="#value3" class="ui-slider-control" id="ui-slider-control2"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Web Design</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value3" id="value4"><?php
                                                    if ($skills[0]['webdesign']) {
                                                        echo $skills[0]['webdesign'];
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?></span>%
                                                    <input type="hidden" class="value_skill" name="webdesign" value="<?php
                                                    if ($skills[0]['webdesign']) {
                                                        echo $skills[0]['webdesign'];
                                                        } else {
                                                            echo '0';
                                                        }
                                                        ?>" id="sckill_val4">
                                                    </div>
                                                </div>

                                                <div data-target="#value4" class="ui-slider-control" id="ui-slider-control3"></div>
                                            </div>

                                            <div class="ui-slid-errow">
                                                <div class="pro-gressbar">
                                                    <h3 class="progress-title">App Design</h3>
                                                    <div class="progress-value-1">
                                                        <span class="value value4" id="value5"><?php
                                                        if ($skills[0]['appdesign']) {
                                                            echo $skills[0]['appdesign'];
                                                        } else {
                                                            echo '0';
                                                        }
                                                        ?></span>%
                                                        <input type="hidden" class="value_skill" name="appdesign" value="<?php
                                                        if ($skills[0]['appdesign']) {
                                                            echo $skills[0]['appdesign'];
                                                            } else {
                                                                echo '0';
                                                            }
                                                            ?>" id="sckill_val5">
                                                        </div>
                                                    </div>

                                                    <div data-target="#value5" class="ui-slider-control" id="ui-slider-control4"></div>
                                                </div>

                                                <div class="ui-slid-errow">
                                                    <div class="pro-gressbar">
                                                        <h3 class="progress-title">Graphics Design</h3>
                                                        <div class="progress-value-1">
                                                            <span class="value value5" id="value6"><?php
                                                            if ($skills[0]['graphics']) {
                                                                echo $skills[0]['graphics'];
                                                            } else {
                                                                echo '0';
                                                            }
                                                            ?></span>%
                                                            <input type="hidden" class="value_skill" name="graphics" value="<?php
                                                            if ($skills[0]['graphics']) {
                                                                echo $skills[0]['graphics'];
                                                                } else {
                                                                    echo '0';
                                                                }
                                                                ?>" id="sckill_val6">
                                                            </div>
                                                        </div>

                                                        <div data-target="#value6" class="ui-slider-control" id="ui-slider-control5"></div>
                                                    </div>

                                                    <div class="ui-slid-errow">
                                                        <div class="pro-gressbar">
                                                            <h3 class="progress-title">Illustrations</h3>
                                                            <div class="progress-value-1">
                                                                <span class="value value6" id="value7"><?php
                                                                if ($skills[0]['illustration']) {
                                                                    echo $skills[0]['illustration'];
                                                                } else {
                                                                    echo '0';
                                                                }
                                                                ?></span>%
                                                                <input type="hidden" class="value_skill" name="illustration" value="<?php
                                                                if ($skills[0]['illustration']) {
                                                                    echo $skills[0]['illustration'];
                                                                    } else {
                                                                        echo '0';
                                                                    }
                                                                    ?>" id="sckill_val7">
                                                                </div>
                                                            </div>

                                                            <div data-target="#value7" class="ui-slider-control" id="ui-slider-control6"></div>
                                                        </div>
                                                    <?php } else { ?>
                                                        <div class="ui-slid-errow">
                                                            <div class="pro-gressbar">
                                                                <h3 class="progress-title">Brandig & Logos</h3>
                                                                <div class="progress-value-1">
                                                                    <span class="value value0" id="value1">0</span>%
                                                                    <input type="hidden" class="value_skill" name="brandin_logo" value="0" id="sckill_val1">
                                                                </div>
                                                            </div>

                                                            <div data-target="#value1" class="ui-slider-control" id="ui-slider-control0"></div>
                                                        </div>

                                                        <div class="ui-slid-errow">
                                                            <div class="pro-gressbar">
                                                                <h3 class="progress-title">Ads & Banners</h3>
                                                                <div class="progress-value-1">
                                                                    <span class="value value1" id="value2">0</span>%
                                                                    <input type="hidden" class="value_skill" name="ads_banner" value="0" id="sckill_val2">
                                                                </div>
                                                            </div>

                                                            <div data-target="#value2" class="ui-slider-control" id="ui-slider-control1"></div>
                                                        </div>

                                                        <div class="ui-slid-errow">
                                                            <div class="pro-gressbar">
                                                                <h3 class="progress-title">Email & Marketing</h3>
                                                                <div class="progress-value-1">
                                                                    <span class="value value2" id="value3">0</span>%
                                                                    <input type="hidden" class="value_skill" name="email_marketing" value="0" id="sckill_val3">
                                                                </div>
                                                            </div>

                                                            <div data-target="#value3" class="ui-slider-control" id="ui-slider-control2"></div>
                                                        </div>

                                                        <div class="ui-slid-errow">
                                                            <div class="pro-gressbar">
                                                                <h3 class="progress-title">Web Design</h3>
                                                                <div class="progress-value-1">
                                                                    <span class="value value3" id="value4">0</span>%
                                                                    <input type="hidden" class="value_skill" name="webdesign" value="0" id="sckill_val4">
                                                                </div>
                                                            </div>
                                                            <div data-target="#value4" class="ui-slider-control" id="ui-slider-control3"></div>
                                                        </div>
                                                        <div class="ui-slid-errow">
                                                            <div class="pro-gressbar">
                                                                <h3 class="progress-title">App Design</h3>
                                                                <div class="progress-value-1">
                                                                    <span class="value value4" id="value5">0</span>%
                                                                    <input type="hidden" class="value_skill" name="appdesign" value="0" id="sckill_val5">
                                                                </div>
                                                            </div>
                                                            <div data-target="#value5" class="ui-slider-control" id="ui-slider-control4"></div>
                                                        </div>
                                                        <div class="ui-slid-errow">
                                                            <div class="pro-gressbar">
                                                                <h3 class="progress-title">Graphics Design</h3>
                                                                <div class="progress-value-1">
                                                                    <span class="value value5" id="value6">0</span>%
                                                                    <input type="hidden" class="value_skill" name="graphics" value="0" id="sckill_val6">
                                                                </div>
                                                            </div>

                                                            <div data-target="#value6" class="ui-slider-control" id="ui-slider-control5"></div>
                                                        </div>

                                                        <div class="ui-slid-errow">
                                                            <div class="pro-gressbar">
                                                                <h3 class="progress-title">Illustrations</h3>
                                                                <div class="progress-value-1">
                                                                    <span class="value value6" id="value7">0</span>%
                                                                    <input type="hidden" class="value_skill" name="illustration" value="0" id="sckill_val7">
                                                                </div>
                                                            </div>
                                                            <div data-target="#value7" class="ui-slider-control" id="ui-slider-control6"></div>
                                                        </div>
                                                    <?php } ?>
                                                </form>
                                            </div>  
                                        </div>
                                    </div>
                                </div> 
                            </div>
</div>
</div>
</section>
<!-- Modal -->
<div class="modal similar-prop fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="cli-ent-model-box">
               <header class="fo-rm-header">
                                    <h2 class="popup_h2 del-txt">Change Password</h2>
                                    <div class="cross_popup" data-dismiss="modal"> x</div>
                                </header>
                <div class="cli-ent-model">
                    <div class="fo-rm-body">
                        <form method="post" action="<?php echo base_url(); ?>admin/accounts/designer_change_password_profile/<?php echo $data[0]['id']; ?>">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="form-group">
                                        <p class="label-txt">New Password</p>
                                        <input type="password" required class="input"  name="new_password">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-sm-12">
                                    <label class="form-group">
                                        <p class="label-txt">Confirm Password</p>
                                         <input type="password" required class="input" name="confirm_password">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <button class="btn-g btn btn-ydelete" name="savebtn">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/ui-slider/jquery.ui-slider.js');?>"></script>

<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/dropify/dist/js/dropify.min.js');?>"></script>
<script type="text/javascript">
//$(document).ready(function () {
//        $('.profile_pic_sec_btn').click(function (e) {
//            e.preventDefault();
//            $('.profile_pic_sec').hide();
//            $('.edit_profile_pic_sec').css('display', 'block');
//        });
//        $('.abt_sec_btn').click(function (e) {
//            e.preventDefault();
//            $('.about_info_sec').hide();
//            $('.edit_abt_sec').css('display', 'block');
//        });
//        $('.skills_btn').click(function (e) {
//            e.preventDefault();
//            $('.skills_sec').hide();
//            $('.edit_skills_sec').css('display', 'block');
//        });
//        $('.other_profile_btn').click(function (e) {
//            e.preventDefault();
//            $('.other_profile_sec').hide();
//            $('.edit_other_profile_sec').css('display', 'block');
//        });
//        $('#changepass').click(function (e) {
//            e.preventDefault();
//        });
//    });
//function validateAndUpload(input) {
//    //alert("In Function"); 
//    var URL = window.URL || window.webkitURL;
//    var file = input.files[0];
//
//    if (file) {
//        console.log(URL.createObjectURL(file));
//        // console.log(file);
//        $(".imagemain").hide();
//
//        $(".imagemain2").show();
//        $(".imagemain3").show();
//        $("#image3").attr('src', URL.createObjectURL(file));
//
//        $(".dropify-wrapper").show();
//    }
//}
                                
//$(document).ready(function (event) {
//    var position = 0;
//    var val1 = $('#sckill_val1').val();
//    var val2 = $('#sckill_val2').val();
//    var val3 = $('#sckill_val3').val();
//    var val4 = $('#sckill_val4').val();
//    var val5 = $('#sckill_val5').val();
//    var val6 = $('#sckill_val6').val();
//    var val7 = $('#sckill_val7').val();
//    var position1 = [val1, val2, val3, val4, val5, val6, val7];
//    $(position1).each(function (i) {
//        $('#ui-slider-control' + i)
//        .UISlider({
//        min: 1,
//        max: 101,
//        smooth: false,
//        value: position1[i]
//        })
//    
//        .on('change thumbmove', function (event, value) {
//            var targetPath = $(event.target).data('target');
//            //console.log(targetPath,value);
//            $(targetPath).text(value);
//            $(targetPath).next('input.value_skill').val(value);
//            $('#dataaaaa').val(targetPath);
//        })
//        .on('start', function () {
//            $('.value').addClass('editing');
//        })
//        .on('end', function () {
//            $('.value').removeClass('editing');
//        });
//
////        $('.value').text(position);
//
//
//        $('input[type=checkbox]')
//        .on('change', function () {
//
//            var name = $(this).prop('name'),
//            value = $(this).prop('checked');
//
//            if (name === 'popup') {
//
//                $('.popup-buttons').toggle(value);
//
//            } else {
//
//                $('.ui-slider-control').UISlider(name, value);
//            }
//        });
//        });
//    });
    
//$(document).ready(function () {
//    $('.progress-value > span').each(function () {
//        $(this).prop('Counter', 0).animate({
//            Counter: $(this).text()
//        }, {
//            duration: 1500,
//            easing: 'swing',
//            step: function (now) {
//                $(this).text(Math.ceil(now));
//            }
//        });
//    });
//});
</script>