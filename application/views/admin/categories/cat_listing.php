<section class="con-b">
    <div class="container-fluid">
        <?php if($this->session->flashdata('message_error') != '') { ?>				
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
            </div>
        <?php }?>
        <?php if($this->session->flashdata('message_success') != '') {?>				
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_success');?></strong>
            </div>
        <?php } ?>
        <div class="row flex-show">
            <div class="col-md-12">
                <div class="flex-this">
                    <h2 class="main_page_heading">Request Categories</h2>
                    <div class="header_searchbtn">
                        <a href="javascript:void(0)" class="addderequesr" data-toggle="modal" data-target="#AddCategory">+ Add New Category</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="cat-bucket">
            <div id="no-more-tables">
                <table class="table">
                    <thead>
                        <tr>
                            <th> </th>
                            <th colspan="2">Name</th>
                            <th>Image</th>
                            <th>Position</th>
                            <th>Bucket</th>
                            <th>Timeline</th>
                            <th>Active</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($cat_data as $kk => $vv) { //echo "<pre/>";print_r($vv);?>
                        <tr class="parent" data-id="<?php echo $vv['id']; ?>">
                            <td class="text-center"><a class="show_child plus_<?php echo $vv['id']; ?>" data-child_id="<?php echo $vv['id']; ?>">+</a> </td>
                            <td colspan="2" data-title="Name" class="title"><?php echo $vv['name']; ?>
                        </td>
                        <td data-title="Image">
                            <?php 
                            if($vv['is_active'] == 1){
                            if ($vv['image_url'] != '') { ?>
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS . 'img/customer/customer-images/' . $vv['image_url']; ?>" alt="<?php echo $vv['name']; ?>"/>
                            <?php }} ?>
                            <br/>
                        </td>
                        <td data-title="Position"><?php echo $vv['position']; ?></td>
                        <td data-title="Bucket"></td>
                        <td data-title="Timeline"><?php echo $vv['timeline']; ?></td>
                        <td data-title="Active">
<!--                            <div class="notify-lines">
                                <div class="switch-custom switch-custom-usersetting-check">
                                    <span class="checkstatus checkstatus_<?php echo $vv['is_active']; ?>"></span>
                                    <input type="checkbox" data-cid="<?php echo ($vv['is_active'] == "1") ? "Active" : "Inactive"; ?>" 
                                    <?php
//                                    if ($vv['is_active'] == 1) {
//                                        echo 'checked';
//                                    } else {}
                                    ?>/>
                                    <label for="switch_<?php //echo $vv['is_active']; ?>"></label> 
                                </div>
                            </div>-->
                            <?php echo ($vv['is_active'] == "1") ? "Active" : "Inactive"; ?>
                        </td>
                        <td data-title="Action" class="action-per">
                            <a href="javascript:void(0)" data-id="<?php echo $vv['id'];?>" class="edit_cat" data-toggle="modal" data-target="#EditCategory"><i class="icon-gz_edit_icon"></i></a>
                            <a href="javascript:void(0)" class="delete_cat" data-id="<?php echo $vv['id']; ?>"> <i class="icon-gz_delete_icon"></i>
                            </a>
                        </td>
                    </tr>
                    <?php if (isset($vv['child']) && !empty($vv['child'])) { ?>
                        <?php foreach ($vv['child'] as $k => $v) { ?>
                            <tr class="child_<?php echo $v['parent_id'];?>" style="display:none;">
                                <td class="text-center "></td>
                                <td data-title="Name" colspan="2"><div class="dot"></div><?php echo $v['name']; ?></td>
                                <?php 
                                if($v['agency_only'] == 1){
                                    $gifclass = 'allowed_agency_file';
                                 }else{
                                     $gifclass = ''; 
                                 }
                                ?>
                                <td data-title="Image" class="<?php echo $gifclass; ?>">
                                    <?php 
                                    if($v['is_active'] == 1){ 
                                    if ($v['image_url'] != '') { ?>
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS . 'img/customer/customer-images/' . $v['image_url']; ?>" alt="<?php echo $v['name']; ?>"/>
                                    <?php }} ?>
                                    <br/>
                                </td>
                                <td data-title="Position"><?php echo $v['position']; ?></td>
                                <td data-title="Bucket">
                                    <?php echo ($v['bucket_type'] == "2") ? "Creative" : ""; ?> 
                                </td>
                                <td data-title="Timeline"><?php echo $v['timeline']; ?></td>
                                <td data-title="Active">
                                    <?php echo ($v['is_active'] == "1") ? "Active" : "Inactive"; ?> 
                                </td>
                                <td data-title="Action" class="action-per">
                                    <a href="javascript:void(0)" class="edit_cat" data-id="<?php echo $v['id']; ?>" data-toggle="modal" data-target="#EditCategory">
                                        <i class="icon-gz_edit_icon"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="delete_cat" data-id="<?php echo $v['id']; ?>">
                                        <i class="icon-gz_delete_icon"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
</div>
</section>




