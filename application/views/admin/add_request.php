<?php
// $editID = isset($_GET['reqid']) ? $_GET['reqid']:'';
// if($canuseradd == 0 || $useraddrequest != 2 && $editID == ''){
//     redirect(base_url().'customer/request/design_request');
// }
?>
<section id="add-categeory">
	<div class="container-fluid">
		<div class="header-blog">
			<?php if ($this->session->flashdata('message_error') != '') { ?>
				<div id="message" class="alert alert-danger alert-dismissable">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<p class="head-c">
						<?php echo $this->session->flashdata('message_error'); ?>
					</p>
				</div>
			<?php } ?>
			<?php if ($this->session->flashdata('message_success') != '') { ?>
				<div id="message" class="alert alert-success alert-dismissable">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<p class="head-c">
						<?php echo $this->session->flashdata('message_success'); ?>
					</p>
				</div>
			<?php }
			?>
		</div>
		<form method="post" id="submit_requests_form">
			<div class="row">
				<?php
				$is_trial = $profile_data[0]['is_trail'];
				$editvar = '';
				$editID = isset($_GET['reqid']) ? $_GET['reqid'] : '';
				$dupID = isset($_GET['did']) ? $_GET['did'] : '';
				if ($editID && $editID != '') {
					$editvar = $existeddata[0];
				}if ($dupID && $dupID != '') {
					$editvar = $duplicatedata[0];
				}if ($editID == '' && $dupID == '') {
					$editvar = '';
				}
				$category = array('Popular Design' => 'Popular Design', 'Branding & Logos' => 'Branding & Logos', 'Social Media' => 'Social Media', 'Marketing' => 'Marketing', 'UI/UX' => 'UI/UX', 'Artwork' => 'Artwork', 'Custom' => 'Custom');
                //echo "<pre>";print_r($editvar);
				?>
				<input type="hidden" id="for_trial" value="<?php echo $is_trial; ?>">
				<div class="col-lg-12 catagry-heading" data-step="8" data-intro="Use this form to add details about the design you need created." data-position='right' data-scrollTo='tooltip'>
					<h3>Add New Project<span>Please find below Brand Profiles information.</span></h3>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="catagory">
						<div class="form-group">
							<select id="cat_page" class="form-control select-a" required name="category_id" onchange="getval(this);">
								<?php foreach ($allcategories as $key => $value) { ?>
									<option value="<?php echo $value['id']; ?>" 
										<?php echo ($value['id'] == $editvar['category_id'] || $value['name'] == $editvar['category']) ? 'selected' : ''; ?>>
										<?php echo $value['name']; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="error_msg"></div>
							<div class="plan-boxex-xx5 clearfix two-can" id="boxes">
								<div class="form-group" id="child_1" style="display: block;">
									<div class="row">
										<div class="col-md-4 col-lg-3">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="15" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Logo-944.jpg" class="img-responsive">
													</figure>										
													<h3>Logo</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4 col-lg-3">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="16" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Business Card-918.jpg" class="img-responsive">
													</figure>										
													<h3>Business Card</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4 col-lg-3">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="17" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/T-Shirts-865.jpg" class="img-responsive">
													</figure>										
													<h3>T-Shirt</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4 col-lg-3">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="18" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Facebook Ads-937.jpg" class="img-responsive">
													</figure>										
													<h3>Facebook Ad</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4 col-lg-3">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="19" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Instagram Post-983.jpg" class="img-responsive">
													</figure>										
													<h3>Instagram Post</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4 col-lg-3">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="20" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Flyers-102.jpg" class="img-responsive">
													</figure>										
													<h3>Flyers</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4 col-lg-3">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="21" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/brochure-810.png" class="img-responsive">
													</figure>										
													<h3>Brochure</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4 col-lg-3">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="22" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Landing Page-57.jpg" class="img-responsive">
													</figure>										
													<h3>Landing Page</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4 col-lg-3">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="23" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Custom Request-901.jpg" class="img-responsive">
													</figure>										
													<h3>Custom Request</h3>
												</div>
											</label>
										</div>
									</div>
								</div>

								<div class="form-group" id="child_2" style="display:none">
									<div class="row">
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="24" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Logo-874.jpg" class="img-responsive">
													</figure>										
													<h3>Logo</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="25" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Business Card-306.jpg" class="img-responsive">
													</figure>										
													<h3>Business Card</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="26" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/email-signature-222.png" class="img-responsive">
													</figure>										
													<h3>Email signature</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="27" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Email Templates-490.jpg" class="img-responsive">
													</figure>										
													<h3>Email Templates</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="29" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Letterhead-670.jpg" class="img-responsive">
													</figure>										
													<h3>Letterhead</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="30" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/T-Shirts-222.jpg" class="img-responsive">
													</figure>										
													<h3>T-Shirts</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="31" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Custom Request-778.jpg" class="img-responsive">
													</figure>										
													<h3>Custom Request</h3>
												</div>
											</label>
										</div>
									</div>
								</div>

								<div class="form-group" id="child_3" style="display:none">
									<div class="row">
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="32" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Instagram-410.png" class="img-responsive">
													</figure>										
													<h3>Instagram</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="33" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Facebook Ads-786.jpg" class="img-responsive">
													</figure>										
													<h3>Facebook Ad</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="34" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Facebook post-211.jpg" class="img-responsive">
													</figure>										
													<h3>Facebook Post</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="35" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Facebook Cover Photo-637.jpg" class="img-responsive">
													</figure>										
													<h3>Facebook Cover Photo</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="36" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Pinterest-469.jpg" class="img-responsive">
													</figure>										
													<h3>Pinterest</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="37" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Twitter Post-860.jpg" class="img-responsive">
													</figure>										
													<h3>Twitter Post</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="38" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Twitter Header-87.jpg" class="img-responsive">
													</figure>										
													<h3>Twitter Header</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="39" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Youtube Channel Art-562.jpg" class="img-responsive">
													</figure>										
													<h3>Youtube Channel Art</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="40" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/linkedIn-header-840.png" class="img-responsive">
													</figure>										
													<h3>Linkedin header</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="41" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Linkedin Banner-298.jpg" class="img-responsive">
													</figure>										
													<h3>Linkedin Banner</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="42" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/custom-banner-981.png" class="img-responsive">
													</figure>										
													<h3>Custom banner</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="43" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Custom Request-435.jpg" class="img-responsive">
													</figure>										
													<h3>Custom Request</h3>
												</div>
											</label>
										</div>
									</div>
								</div>

								<div class="form-group" id="child_4" style="display:none">
									<div class="row">
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="44" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Posters-924.jpg" class="img-responsive">
													</figure>										
													<h3>Posters</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="45" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Flyers-703.jpg" class="img-responsive">
													</figure>										
													<h3>Flyers</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="46" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/brochure-565.png" class="img-responsive">
													</figure>										
													<h3>Brochure</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="47" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/billboard-ad-342.png" class="img-responsive">
													</figure>										
													<h3>banner Ads</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="48" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/billboard-ad1-248.png" class="img-responsive">
													</figure>										
													<h3>Billboard Ads</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="49" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Presentations-542.jpg" class="img-responsive">
													</figure>										
													<h3>Presentations</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="50" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Sales Sheet-363.jpg" class="img-responsive">
													</figure>										
													<h3>Sales Sheet</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="51" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Custom Request-667.jpg" class="img-responsive">
													</figure>										
													<h3>Custom Request</h3>
												</div>
											</label>
										</div>
									</div>
								</div>

								<div class="form-group" id="child_5" style="display:none">
									<div class="row">
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="52" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Landing Page-484.jpg" class="img-responsive">
													</figure>										
													<h3>Landing Page</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="53" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Multipage Website-872.jpg" class="img-responsive">
													</figure>										
													<h3>Multipage Website</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="54" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/favicon-509.png" class="img-responsive">
													</figure>										
													<h3>favicon</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="55" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Web Forms-221.jpg" class="img-responsive">
													</figure>										
													<h3>Web Forms</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="56" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/web.banners-163.png" class="img-responsive">
													</figure>										
													<h3>web banner</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="57" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/App Design-576.jpg" class="img-responsive">
													</figure>										
													<h3>App Design</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="58" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/App Icon-214.jpg" class="img-responsive">
													</figure>										
													<h3>App Icon</h3>
												</div>
											</label>
										</div>
									</div>
								</div>

								<div class="form-group" id="child_6" style="display:none">
									<div class="row">
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="59" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/color-separation-909.png" class="img-responsive">
													</figure>										
													<h3>Color Separation</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="60" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/vectorize-885.png" class="img-responsive">
													</figure>										
													<h3>Vectorize</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="61" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/digitize-758.png" class="img-responsive">
													</figure>										
													<h3>Digitize/Embroidery</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="62" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/mock-up-253.png" class="img-responsive">
													</figure>										
													<h3>Mock-up/Proof</h3>
												</div>
											</label>
										</div>
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="63" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Custom Request-997.jpg" class="img-responsive">
													</figure>										
													<h3>Custom Request</h3>
												</div>
											</label>
										</div>
									</div>
								</div>

								<div class="form-group" id="child_7" style="display:none">
									<div class="row">
										<div class="col-md-4">
											<label class="radio-box-xx2"> 
												<input name="subcategory_id" class="subcategory_class" value="64" type="radio">
												<span class="checkmark"></span>
												<div class="check-main-xx3">
													<figure class="chkimg">
														<img src="https://www.graphicszoo.com/public/assets/img/customer/customer-images/Custom Request-526.jpg" class="img-responsive">
													</figure>										
													<h3>Custom Request</h3>
												</div>
											</label>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6">
						<div class="catagory-right">
							<div class="title_class">
								<label class="form-group">
									<p class="label-txt <?php echo isset($editvar['title']) ? "label-active" : ""; ?>"> Project Title * </p>
									<input type="text" class="input title" name="title"  value="<?php echo isset($editvar['title']) ? $editvar['title'] : ""; ?>"/>
									<div class="line-box">
										<div class="line"></div>
									</div>
									<div class="error_msg"></div>
								</label>
							</div>
							<label class="form-group">
								<p class="label-txt <?php echo isset($editvar['brand_id']) ? "label-active" : ""; ?>">Select Brand Profile</p>
								<select name="brand_name" class="input select-a">
									<option value=""></option>
									<?php
									if ($user_profile_brands != '') {
										for ($i = 0; $i < sizeof($user_profile_brands); $i++) {
											?>
											<option value="<?php echo $user_profile_brands[$i]['id']; ?>" <?php
											if ($user_profile_brands[$i]['id'] == $editvar['brand_id']) {
												echo "selected";
											}
											?>><?php echo $user_profile_brands[$i]['brand_name']; ?></option>
											<?php
										}
									}
									?>
								</select>
								<div class="line-box">
									<div class="line"></div>
								</div>
							</label>
							<div class="dim_class">
								<label class="form-group">
									<p class="label-txt <?php echo isset($editvar['design_dimension']) ? "label-active" : ""; ?>">Design Dimensions * </p>
									<input type="text" class="input dimension" name="dimension" id="exampleInputPassword1" placeholder="" value="<?php echo isset($editvar['design_dimension']) ? $editvar['design_dimension'] : ""; ?>">
									<div class="line-box">
										<div class="line"></div>
									</div>
									<div class="error_msg"></div>
								</label>

							</div>
							<div class="com_class">  
								<label class="form-group">
									<p class="label-txt <?php echo isset($editvar['description']) ? "label-active" : ""; ?>">Description *  </p>
									<textarea class="input" rows="5" name="description" id="comment" placeholder=""><?php echo isset($editvar['description']) ? $editvar['description'] : ""; ?></textarea>
									<div class="line-box">
										<div class="line"></div>
									</div>
									<p class="discrption">
									Please provide us as much detail as possible. Include things such as color preferences, styles you like, text you want written on the graphic, and anything else that may help the designer.</p>
									<div class="error_msg"></div>
								</label>

							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<section class="color-sec">
						<div class="col-md-6">
							<div class="color-combination">
								<div class="color-inside catagry-heading">
									<h3>Color preference <span>Choose color or let designer choose.</span></h3>
									<div class="sound-signal">
										<div class="form-radion">
											<input type="radio" name="color_pref[]" class="preference" id="soundsignal1" <?php echo ($editvar['color_pref'] == "Let Designer Choose" || $editvar['color_pref'] == '') ? 'checked' : ''; ?> value="Let Designer Choose">
											<label for="soundsignal1">Let Designer Choose for me.</label>
										</div>
										<div class="form-radion">
											<input type="radio" name="color_pref[]" id="soundsignal2" class="preference" value="hex code" <?php echo ($editvar['color_pref'] !== "Let Designer Choose" && $editvar['color_pref'] != '') ? 'checked' : '' ?>>
											<label for="soundsignal2">Enter Hex Code</label>
											<label class="form-group">
												<p class="label-txt <?php echo (isset($editvar['color_pref']) && $editvar['color_pref'] !== "Let Designer Choose") ? "label-active" : ""; ?>">color code </p>
												<input type="text" name="HexCodeVal" class="input" id="hexa-color2" placeholder="" value="<?php echo (isset($editvar['color_pref']) && $editvar['color_pref'] !== "Let Designer Choose") ? $editvar['color_pref'] : '' ?>">
												<div class="line-box">
													<div class="line"></div>
												</div>
											</label>
										</div>
									</div>
								</div>
								<div class="color-inside catagry-heading">
									<h3>Deliverables <span>What would you like your designer to deliver?</span></h3>
									<div class="sound-signal2">
										<?php $deliverable = explode(',', $editvar['deliverables']); ?>
										<div class="form-radion2">
											<label class="containerr">Source Files
												<input type="checkbox" name="filetype[]" value="SourceFiles" checked="checked" <?php echo in_array('SourceFiles', $deliverable) ? 'checked' : ''; ?>>
												<span class="checkmark"></span>
											</label>
										</div>
										<div class="form-radion2">
											<label class="containerr">JPEG
												<input type="checkbox" name="filetype[]" value="jpeg" <?php echo in_array('jpeg', $deliverable) ? 'checked' : ''; ?>>
												<span class="checkmark"></span>
											</label>
										</div>

										<div class="form-radion2">
											<label class="containerr">PNG
												<input type="checkbox" name="filetype[]" value="png" <?php echo in_array('png', $deliverable) ? 'checked' : ''; ?>>
												<span class="checkmark"></span>
											</label>
										</div>

										<div class="form-radion2">
											<label class="containerr">PDF
												<input type="checkbox" name="filetype[]" value="pdf" <?php echo in_array('pdf', $deliverable) ? 'checked' : ''; ?>>
												<span class="checkmark"></span>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="drop-file">
								<div class="form-group goup-x1"> 
									<div class="file-drop-area file-upload">
										<span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cloud.png" class="img-responsive"></span>
										<span class="file-msg">Drag and drop file here or 
											<span class="nocolsl">Click Here</span>
										</span>
										<input type="file" class="file-input project-file-input" multiple="" name="file-upload[]" id="file_input">
									</div>
									<p class="text-g">Allowed file types : Doc, Docx, Odt, Pdf, Jpg, Png, Jpeg, Psd, Ai, Zip, Mp4, Mov, tiff, ttf, eps </p>

								</div>
								<?php if (isset($editvar['customer_attachment'])) { ?>
									<hr/>
									<div class="uploadFileListContainer row">   
										<?php
										for ($i = 0; $i < count($editvar['customer_attachment']); $i++) {
											$type = substr($editvar['customer_attachment'][$i]['file_name'], strrpos($editvar['customer_attachment'][$i]['file_name'], '.') + 1);
											?>
											<div  class="uploadFileRow" id="file<?php echo $editvar['customer_attachment'][$i]['id']; ?>">
												<div class="col-md-6">
													<div class="extnsn-lst">
														<p class="text-mb">
															<?php echo $editvar['customer_attachment'][$i]['file_name']; ?>
														</p>
													</div>
												</div>
											</div>
										<?php } ?>
									</div>
								<?php } ?>
							</div>
						</div>
					</section>
					<div class="col-md-12">
						<div class="uploadFileListContainer  uploadFileListContain">   

						</div>
					</div>
					<div style="display: flex; justify-content: center;width: 100%;padding-bottom: 15px;">
						<img class="imgloader_loader" src="">
					</div>
					<div class="col-lg-12 ad_new_reqz text-center">
						<input type="submit" class=" theme-save-btn" id="request_submit" data-count="1" name="request_submit"/>
						<input type="submit" class="theme-cancel-btn" id="request_exit" value="Save in Draft" name="request_exit"/>
					</div>

				</div>
			</form>
		</div>
	</section>
	<button style="display: none;" id="trial_expire" data-toggle="modal" data-target="#TrialExpire">click here</button>
	<button style="display: none;" id="Select_PackForUP" data-toggle="modal" data-target="#SelectPackForUP">click here</button>
<!--<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/jquery.min.js"></script>
	<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/bootstrap.min.js"></script>-->
	<script type="text/javascript">
		$session = "<?php echo isset($_SESSION['click_count']) ? $_SESSION['click_count'] : ''; ?>";
		$without_pay_user = "<?php echo $profile_data[0]['without_pay_user'] ?>";
	</script>