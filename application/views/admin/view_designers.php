<section class="con-b">
    <div class="container-fluid">
        <?php if ($this->session->flashdata('message_error') != '') { ?>				
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>				
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
            </div>
        <?php } ?>
        <div class="tab-content">
            <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $designerscount; ?>" class="tab-pane active view_clnts content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                <div class="header-blog">
                    <div class="row flex-show">
                        <div class="col-md-12">
                            <div class="flex-this">
                                <h2 class="main_page_heading">Designers</h2>
                                <div class="header_searchbtn">
                                    <a href="#" class="upl-oadfi-le adddesinger" data-toggle="modal" data-target="#AddDesigners">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/assign_designer.svg"">
                                        <span class="">Add Designer</span>
                                    </a>
                                    <div class="search-first">
                                        <div class="focusout-search-box">
                                            <div class="search-box">
                                                <form method="post" class="search-group clearfix">
                                                    <input type="text" placeholder="Search here..." class="form-control searchdata searchdata_designer search_text">
                                                    <div class="ajax_searchload" style="display:none;">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                                    </div>
                                                    <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                                    <div class="close-search"><i class="far fa-eye-slash"></i></div>
                                                    <button type="submit" class="search-btn search search_designer_ajax">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_search_icon.svg" class="img-responsive">
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="filter-site">
                                        <i class="fas fa-sliders-h"></i>
                                    </div>
                                    <div class="gz-filter">
                                        <div class="filter-header">
                                            <h2>Filter</h2>
                                            <a href="#" class="close-filter">x</a>
                                        </div>
                                        <div class="filter-body">
                                            <select name="" id="">
                                                <option value="">select client </option>
                                                <option value="">select Deisgner</option>
                                            </select>
                                            <select name="" id="">
                                                <option value="">select client </option>
                                                <option value="">select Deisgner</option>
                                            </select>
                                            <div class="status-filter">
                                                <h4>Project Status</h4>
                                                <div class="sound-signal2">
                                                    <div class="form-radion2">
                                                        <label class="containerr">iN-PROGRESS
                                                            <input type="checkbox" name="filetype[]" value="SourceFiles" checked="checked">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="form-radion2">
                                                        <label class="containerr">PENDING
                                                            <input type="checkbox" name="filetype[]" value="jpeg">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>

                                                    <div class="form-radion2">
                                                        <label class="containerr">ON-HOLD
                                                            <input type="checkbox" name="filetype[]" value="png">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="status-filter date-filter">
                                                <h4>Task Time</h4>
                                                <div class="date-stand">
                                                    <div class="date-from">
                                                        <label>From</label>
                                                        <input type="date" class="week-picker hasDatepicker">
                                                    </div>
                                                    <div class="date-to">
                                                        <label>To</label>
                                                        <input type="date" class="week-picker hasDatepicker">
                                                    </div>
                                                </div>
                                                <div class="sound-signal2">
                                                    <div class="form-radion2">
                                                        <label class="containerr">EXPRIED
                                                            <input type="checkbox" name="filetype[]" value="SourceFiles" checked="checked">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="status-filter verify-option">
                                                <h4>VERIFY OPTION</h4>
                                                <div class="sound-signal2">
                                                    <div class="form-radion2">
                                                        <label class="containerr">VERIFY
                                                            <input type="checkbox" name="filetype[]" value="SourceFiles" checked="checked">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="form-radion2">
                                                        <label class="containerr">VERIFIED
                                                            <input type="checkbox" name="filetype[]" value="jpeg">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="filter_button">
                                                <a href="#" class="more_load">Apply Filter</a>
                                                <a href="#" class="reset">Reset</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-blog">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="designer-count">Current Designers  &nbsp;  <?php echo $designerscount;?></p>
                        </div>
                    </div>
                </div>
                <div class="product-list-show">
                    <div class="cli-ent-row tr tableHead">
                        <div class="cli-ent-col td" style="width: 30%;">
                            Designer NAME
                        </div>
                        <div class="cli-ent-col td" style="width: 10%;">
                         Sign Up Date
                     </div>
                     <div class="cli-ent-col td  text-center" style="width: 15%;">
                        pending requests
                    </div>
                    <div class="cli-ent-col td  text-center" style="width: 15%;">
                        Active requests
                    </div>
                    <div class="cli-ent-col td  text-center"style="width: 15%;">
                     Enable/Disable
                 </div>
                 <div class="cli-ent-col td text-center"style="width:15%;">
                     Action
                 </div>
             </div>
             <div class="cli-ent" id="designerslist">
                <div class="row two-can">
                    <?php for ($i = 0; $i < sizeof($allDesigners); $i++): 
                        $data['designers'] = $allDesigners[$i];
                        $this->load->view('admin/view_designers_template', $data);
                    endfor; ?>
                </div>	
            </div>
            <?php if ($designerscount > LIMIT_ADMIN_LIST_COUNT) { ?>
                <div class="" style="text-align:center">
                    <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" /></div>
                    <a id="load_more_designer" href="javascript:void(0)" data-row="0" data-count="<?php echo $designerscount; ?>" class="load_more button">Load more</a>
                </div>

            <?php } ?>
        </div>
    </div>
</div>
</div>
</section>
<!-- Desable designer confirmation modal -->
<!--<div class="modal fade" id="confirmation" tabindex="-1" role="dialog" aria-labelledby="confirmation" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c text-center" id="message">Are you sure disable this designer?</h3>
                    </header>
                    <div class="fo-rm-body">
                        <div class="confirmation_btn text-center">
                            <button class="btn btn-yy" data-dismiss="modal" aria-label="Close">Yes</button>
                            <button class="btn btn-nn" data-dismiss="modal" aria-label="Close">No</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>-->
<!-- Modal -->
<div class="modal fade similar-prop" id="confirmationdelete" tabindex="-1" role="dialog" aria-labelledby="confirmationdelete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Delete Designer</h2>
                    <div class="cross_popup" data-dismiss="modal"> x</div>
                </header>
                <div class="cli-ent-model">
                   <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/delete-bg.svg">
                        <h3 class="head-c text-center">Are you sure you want to delete this designer?</h3>
                    <div class="confirmation_btn text-center">
                         <a href="javascript:voild(0)" class="btn btn-yd btn-ydelete" data-dismiss="modal" aria-label="Close">Delete</a>
                        <button class="btn btn-nd btn-ndelete" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal similar-prop nonflex fade" id="AddDesigners" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
              <h2 class="popup_h2 del-txt">Add Designer</h2>
              <div class="close" data-dismiss="modal" aria-label="Close"> x</div>
          </header>
          <div class="cli-ent-model-box">
              <div class="cli-ent-model">
                <div class="fo-rm-body">
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-group">
                                    <p class="label-txt">First Name</p>
                                    <input name="first_name" type="text" required class="input">
                                    <div class="line-box"><div class="line"></div></div>
                                </label>
                                <div class="epassError frstname" style="display: none;">
                                    <p class="alert alert-danger">Enter First Name</p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="form-group">
                                    <p class="label-txt">Last Name</p>
                                    <input type="text" name="last_name" required  class=" input lastname">
                                    <div class="line-box"><div class="line"></div></div>
                                </label>
                                <div class="epassError lstname" style="display: none;">
                                    <p class="alert alert-danger">Enter Last Name</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="form-group">
                                    <p class="label-txt">Email</p>
                                    <input type="email" name="email" required class="input emailid">
                                    <div class="line-box"><div class="line"></div></div>
                                </label>
                                <div class="epassError mailadrs" style="display: none;">
                                    <p class="alert alert-danger">Enter Valid Email Address</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="form-group">
                                    <p class="label-txt">Phone</p>
                                    <input type="tel" name="phone" required class="input phone">
                                    <div class="line-box"><div class="line"></div></div>
                                </label>
                                <div class="epassError phnnmr" style="display: none;">
                                    <p class="alert alert-danger">Enter Phone Number</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="form-group text-left">
                                    <p class="label-txt">Password</p>
                                    <input type="password" name="password" required  class="input password">
                                    <div class="line-box"><div class="line"></div></div>
                                    <!--<i>Only alphanumeric, @ and # allowed</i>-->
                                </label>
                            </div>
                            <div class="col-sm-6">
                                <label class="form-group">
                                    <p class="label-txt">Confirm Password</p>
                                    <input type="password" name="confirm_password" required class="input cpassword">
                                    <div class="line-box"><div class="line"></div></div>
                                    <div class="passError" style="display: none;">
                                        <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="form-group">
                                    <p class="label-txt">About</p>
                                    <textarea name="about" class="input lastname"></textarea>
                                    <div class="line-box"><div class="line"></div></div>
                                </label>
                            </div>
                            <div class="col-sm-12">
                                <label class="form-group">
                                    <p class="label-txt">Hobbies</p>
                                    <textarea name="hobbies" class="input"></textarea>
                                    <div class="line-box"><div class="line"></div></div>
                                </label>
                            </div>
                        </div>

                        <p class="btn-x">
                            <button type="submit" class="load_more button">Add Designer</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js');?>"></script>
<script type="text/javascript">
   var $rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
//    function load_more_designerlist(){
//   // var tabid = $('#status_check li.active a').attr('href');
//   var activeTabPane = $('.tab-pane.active');
//    //var status_scroll = $('#status_check li.active a').attr('data-status');
//    var searchval = activeTabPane.find('.search_text').val();
//    //activeTabPane.attr('data-search-text', searchval);
//    $.post('<?php echo site_url() ?>admin/dashboard/load_more_designer', {'group_no': 0, 'search':searchval},
//        function (data){
//            var newTotalCount = 0;
//            if(data != ''){
//                $(".ajax_searchload").fadeOut(500);
//                $("#designerslist .two-can").html(data);
//                newTotalCount = $('#designerslist').find("#loadingAjaxCount").attr('data-value');
//                $("#designerslist").find("#loadingAjaxCount").remove();
//                activeTabPane.attr("data-total-count",newTotalCount);
//                activeTabPane.attr("data-loaded",$rowperpage);
//                activeTabPane.attr("data-group",1);
//            } else {
//                activeTabPane.attr("data-total-count",0);
//                activeTabPane.attr("data-loaded",0);
//                activeTabPane.attr("data-group",1);
//                $('#designerslist').html("");  
//            }
//            if ($rowperpage >= newTotalCount) {
//                activeTabPane.find('.load_more').css("display", "none");
//            } else{
//                activeTabPane.find('.load_more').css("display", "inline-block");
//            }
//        });
//}
//$('.searchdata_designer').keyup(function (e) {
//    e.preventDefault();
//    delay(function(){
//        $(".ajax_searchload").fadeIn(500);
//        load_more_designerlist(); 
//    }, 1000 );
//});
//var delay = (function(){
//  var timer = 0;
//  return function(callback, ms){
//    clearTimeout (timer);
//    timer = setTimeout(callback, ms);
//};
//})();


//$('.search_designer_ajax').click(function (e) {
//    e.preventDefault();
//    load_more_designerlist();
//});
//function valid_Date() {
//    var email = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
//    var fname = new RegExp('^[A-Za-z ]+$');
//    var lname = new RegExp('^[A-Za-z ]+$');
//    var mob = new RegExp('^[0-9]+$');
//        //var password = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
//        var password = new RegExp('^[A-Za-z0-9@#]+$');
//        if (!fname.test($('.firstname').val())) {
//            $(".firstname").css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//            $(".firstname").focus();
//            $('.epassError.frstname').css({"display": "block"});
//            $('.epassError.frstname .alert').css({"display": "block"});
//            return false;
//        }
//        else {
//            $(".firstname").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//            $('.epassError.frstname').css({"display": "none"});
//            $('.epassError.frstname .alert').css({"display": "none"});
//        }
//        if (!lname.test($('.lastname').val())) {
//            $('.lastname').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//            $(".lastname").focus();
//            $('.epassError.lstname').css({"display": "block"});
//            $('.epassError.lstname .alert').css({"display": "block"});
//            return false;
//        }
//        else {
//            $(".lastname").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//            $('.epassError.lstname').css({"display": "none"});
//            $('.epassError.lstname .alert').css({"display": "none"});
//        }
//        if (!email.test($('.emailid').val())) {
//            $(".emailid").css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//            $(".emailid").focus();
//            $('.epassError.mailadrs').css({"display": "block"});
//            $('.epassError.mailadrs .alert').css({"display": "block"});
//            return false;
//        }
//        else {
//            $(".emailid").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//            $('.epassError.mailadrs').css({"display": "none"});
//            $('.epassError.mailadrs .alert').css({"display": "none"});
//        }
//        if (!mob.test($('.phone').val())) {
//            $(".phone").focus();
//            $('.phone').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//            $('.epassError.phnnmr').css({"display": "block"});
//            $('.epassError.phnnmr .alert').css({"display": "block"});
//            return false;
//        }
//        else {
//            $(".phone").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//            $('.epassError.phnnmr').css({"display": "none"});
//            $('.epassError.phnnmr .alert').css({"display": "none"});
//        }
//        if (!password.test($('.password').val())) {
//            $(".password").focus();
//            $('.password').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//            return false;
//        }
//        else {
//            $(".password").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//        }
//        if ($('.password').val() != $('.cpassword').val())
//        {
//            $(".password").focus();
//            $('.password').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//            $('.cpassword').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//            $('.passError').css({"display": "block"});
//            $('.passError .alert').css({"display": "block"});
//            return false;
//        }
//        else {
//            $(".password").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//            $(".cpassword").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//            $('.passError').css({"display": "none"});
//            $('.passError .alert').css({"display": "none"});
//        }
//        return true;
//    }

//    function EditFormSQa(id) {
//        var email = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
//        var fname = new RegExp('^[A-Za-z ]+$');
//        var lname = new RegExp('^[A-Za-z ]+$');
//        var mob = new RegExp('^[0-9]+$');
//        var password = new RegExp('^[A-Za-z0-9@#]+$');
//    //var password = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
//    if (!fname.test($('#' + id + ' .efirstname').val())) {
//        $('#' + id + ' .efirstname').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//        $(".efirstname").focus();
//        return false;
//    }
//    else {
//        $('#' + id + ' .efirstname').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//    }
//    if (!lname.test($('#' + id + ' .elastname').val())) {
//        $('#' + id + ' .elastname').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//        $('#' + id + ' .elastname').focus();
//        return false;
//    }
//    else {
//        $('#' + id + ' .elastname').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//    }
//    if (!email.test($('#' + id + ' .eemailid').val())) {
//        $('#' + id + ' .eemailid').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//        $('#' + id + ' .eemailid').focus();
//        return false;
//    }
//    else {
//        $('#' + id + ' .eemailid').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//    }
//    if (!mob.test($('#' + id + ' .ephone').val())) {
//        $('#' + id + ' .ephone').focus();
//        $('#' + id + ' .ephone').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//        return false;
//    }
//    else {
//        $('#' + id + ' .ephone').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//    }
//    if ($('#' + id + ' .epassword').val() != "") {
//        if (!password.test($('#' + id + ' .epassword').val())) {
//            $('#' + id + ' .epassword').focus();
//            $('#' + id + ' .epassword').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//            return false;
//        }
//        else {
//            $('#' + id + ' .epassword').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//        }
//    }
//    if ($('#' + id + ' .ecpassword').val() != "") {
//        if ($('#' + id + ' .epassword').val() != $('.ecpassword').val())
//        {
//            $('#' + id + ' .epassword').focus();
//            $('#' + id + ' .epassword').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//            $('#' + id + ' .ecpassword').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
//            $('#' + id + ' .epassError').css({"display": "block"});
//            $('.epassError .alert').css({"display": "block"});
//            return false;
//        }
//        else {
//            $('#' + id + ' .epassword').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//            $('#' + id + ' .ecpassword').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
//        }
//    }
//    return true;
//}

//$(document).ready(function () {
//  $rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
//  $('.tab-pane').attr("data-loaded", $rowperpage);
//  $(document).on("click", "#load_more_designer", function () {
//    var row = parseInt($(this).attr('data-row'));
//    row = row + $rowperpage;
//    var activeTabPane = $('.tab-pane.active');
//    var searchval = activeTabPane.find('.search_text').val();
//    var allcount = parseInt(activeTabPane.attr("data-total-count"));
//    var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
//    var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
//    if (allLoaded < allcount) {
//        $.post('<?php echo site_url() ?>admin/dashboard/load_more_designer', {'group_no': activeTabPaneGroupNumber},
//            function (data) {
//                if (data != "") {
//                        //console.log(allLoaded);
//                        $('#designerslist .two-can').append(data);
//                        activeTabPaneGroupNumber++;
//                        activeTabPane.attr('data-group', activeTabPaneGroupNumber);
//                        allLoaded = allLoaded + $rowperpage;
//                        activeTabPane.attr('data-loaded', allLoaded);
//                        if (allLoaded >= allcount) {
//                            activeTabPane.find('.load_more').css("display", "none");
//                        } else{
//                            activeTabPane.find('.load_more').css("display", "inline-block");
//                        }
//                    }
//                });
//    }
//});
//});

//var status;
//var designerid;
//var thisbtn;
//$(document).on('click', '.confirmation', function (e) {
//    e.preventDefault();
//    thisbtn = $(this);
//    status = $(this).attr('data-status');
//    designerid = $(this).attr('data-designerid');
//    $('#confirmation #message').html("Are you sure " + status + " this designer");
//});
//$(document).on('click', '#confirmation .btn-y', function (e) {
//    if (status == "enable") {
//        $.ajax({
//            type: "POST",
//            url: "<?php echo base_url(); ?>admin/dashboard/enable_designer",
//            data: {
//                'designerid': designerid
//            },
//            success: function (response) {
//                if (response == 1) {
//                    $('#disable' + designerid + '.confirmation').show();
//                    $('#enable' + designerid + '.confirmation').hide();
//                }
//            }
//        });
//    } else if (status == "disable") {
//        $.ajax({
//            type: "POST",
//            url: "<?php echo base_url(); ?>admin/dashboard/disable_designer",
//            data: {
//                'designerid': designerid
//            },
//            success: function (response) {
//                if (response == 1) {
//                    $('#enable' + designerid + '.confirmation').show();
//                    $('#disable' + designerid + '.confirmation').hide();
//                }
//            }
//        });
//    }
//});

//$(document).on('click', '.confirmationdelete', function (e) {
//    e.preventDefault();
//    thisbtn = $(this);
//    designerid = $(this).attr('data-designerid');
//});
//$(document).on('click', '#confirmationdelete .btn-y', function (e) {
//    $.ajax({
//        type: "POST",
//        url: "<?php echo base_url(); ?>admin/dashboard/delete_designer",
//        data: {
//            'designerid': designerid
//        },
//        success: function (response) {
//            alert();
//        }
//    });
//});
</script>
<script>
//    $(document).ready(function(){
//      $(".filter-site").click(function(){
//        $(".gz-filter").addClass("show");
//    });
//      $(".close-filter").click(function(){
//        $(".gz-filter").removeClass("show");
//    });
//  });
</script>