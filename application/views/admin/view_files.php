<?php 
$id = $this->input->get("id");
$draftid = $this->uri->segment(4);
$request = $this->Request_model->get_request_by_id_admin($id);
$cat_data = $this->Category_model->get_category_byID($request[0]['category_id']);
$cat_name = $cat_data[0]['name'];
$subcat_data = $this->Category_model->get_category_byID($request[0]['subcategory_id']);
$subcat_name = $subcat_data[0]['name'];
$request['cat_name'] = $cat_name;
$request['subcat_name'] = $subcat_name;
$branddata = $this->Request_model->get_brandprofile_by_id($request[0]['brand_id']);
$branddata = $this->Request_model->get_brandprofile_by_id($request[0]['brand_id']);
$brand_materials_files = $this->Request_model->get_brandprofile_materials_files($branddata[0]['id']);
for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
    if ($brand_materials_files[$i]['file_type'] == 'logo_upload') {
        $brand_materials_files['latest_logo'] = $brand_materials_files[$i]['filename'];
        break;
    }
}  
$qustArr  = array(
        'q_instructions' => "Did you follow all the instructions given by the customer in the product descriptions or the latest revision comments?",
        'q_brand_guidelines' => "Is the design created according to the brand guidelines and information provided in the brand profile?",
        'q_attachments' =>  "Did you review all the attachments sent by the customer and the designer included them, as needed?",
        'q_dimensions' =>   "Is the design created according to the dimensions given by the customer?",
        'q_color_preferences' =>    "Is the design matching the color preferences given by the customer? (if provided)",
        'q_required_text' =>    "Did the designer include all the required text?",
        'q_source_files' => "Did the designer attach the correct source files as requested by the customer?",
        'q_font_files' =>   "Did the designer include the font files in the source folder if required? ",
        'q_quality_review' =>   "Are you satisfied with the quality of your design you are reviewing? (Does it look good?)"
 ); 
//  echo "<pre/>";print_R($designer_file);      
?> 


<style type="text/css" media="screen">
header.nav-section {
    display: none;
}
.mydivposition {
    cursor: crosshair;
    padding: 20px;
    display: inline-block;
    width: auto;
    margin: auto;
    float: none;
    transform-origin: top;
}


.loading{
      font-size:0;
      width:30px;
      height:30px;
      margin-top:5px;
      border-radius:15px;
      padding:0;
      border:3px solid #FFFFFF;
      border-bottom:3px solid rgba(255,255,255,0.0);
      border-left:3px solid rgba(255,255,255,0.0);
      background-color:transparent !important;
      animation-name: rotateAnimation;
      -webkit-animation-name: wk-rotateAnimation;
      animation-duration: 1s;
      -webkit-animation-duration: 1s;
      animation-delay: 0.2s;
      -webkit-animation-delay: 0.2s;
      animation-iteration-count: infinite;
      -webkit-animation-iteration-count: infinite;
    }

    @keyframes rotateAnimation {
        0%   {transform: rotate(0deg);}
        100% {transform: rotate(360deg);}
    }
    @-webkit-keyframes wk-rotateAnimation {
        0%   {-webkit-transform: rotate(0deg);}
        100% {-webkit-transform: rotate(360deg);}
    }

    .hide-loading{
      opacity:0;
      -webkit-transform: rotate(0deg) !important;
      transform: rotate(0deg) !important;
      -webkit-transform:scale(0) !important;
      transform:scale(0) !important;
    }
    button.nav-menu-itm{
        margin-left: 5px; 
    }


    header.nav-section {
   display: none;
   }
   .mydivposition {
   cursor: crosshair;
   padding: 20px;
   display: inline-block;
   width: auto;
   margin: auto;
   float: none;
   transform-origin: top;
   }
   body, .project-info-page .side_bar {
   padding-top: 0px; 
   }
   
   .posone1,.posone2{
   cursor: move;
   
   }

   svg.leader-line {
   z-index: 999;
   /*position: absolute;*/
   }
   svg.leader-line use {
   stroke: #dc223c;
   }
   .custom-color use{
   fill:#dc223c !important;
   }
   .hidenotch:after{
      opacity: 0 !important;
   }
   .MarkasFav {
   display: inline-flex;
   padding: 0px;
   margin: 0px;
   }
   .MarkasFav .click{
   position: relative;
   }
   .MarkasFav .CoPyOrMove img {
   width: 45%;
   cursor: pointer;
   }
   div#CopYfileToFolder,div#DirectorySave {
   margin-top: 40px;
   }
   .fa-heart-o:before {
   content: "\f004";
   }
   span.cstm_span.fa-heart.fas {
   color: #f90500;
   }
</style>


<?php 
$_SESSION['designerpic'] = $user[0]['profile_picture']; 
?>
<div class="dot-header">

    <span id="OpenpopUp" data-open="0"></span>
    <span id="project-pageinfo-id"></span>

    <div class="custom-container">
        <div class="flex-headers">
            <p class="savecols-col left">
                <a  href="/admin/dashboard/view_request/<?php echo $designer_file[0]['request_id']; ?>" class="back-link-xx0 text-uppercase"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive"></a>
            </p>
            <div class="middle-tital">
                <div class="zoom-btns">
                    <a id="zoomOut" href="javascript:void(0)" data-zoomoutcount='0'><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/icon-zoom-out.svg" class="img-responsive"></a>
                    <a id="zoomIn" href="javascript:void(0)" data-count='0'><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/icon-zoom-in.svg" class="img-responsive"></a>
                </div>
                <div class="despeginate-box">
                    <a class="despage prev" href="#myCarousel" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
                    <span class="file_title"><?php echo $designer_file[0]['file_name']; ?></span>
                    <a class="despage next" href="#myCarousel" data-slide="next"><i class="fas fa-chevron-right"></i></a>
                </div>
                <div class="feedbk_data">
                    <div class="f_revw_feedback"  id="" data-id="">
                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve">
                            <g>
                                <g>
                                    <g>
                                        <path d="M460.1,62.5c-0.6,0-1.2,0.2-1.8,0.2v-0.2c0-23-18.7-41.7-41.7-41.7h-375C18.7,20.8,0,39.5,0,62.5v406.3
                                              c0,4.2,2.5,8,6.4,9.6c1.3,0.5,2.6,0.8,4,0.8c2.7,0,5.4-1.1,7.4-3.1L118.9,375h297.8c23,0,41.7-18.7,41.7-41.7V160.6l30-30
                                              c0,0,0,0,0,0c7.4-7.4,11.7-17.7,11.7-28.2C500,80.4,482.1,62.5,460.1,62.5z M437.5,333.3c0,11.5-9.3,20.8-20.8,20.8H114.6
                                              c-2.8,0-5.4,1.1-7.4,3.1l-86.4,86.4V62.5c0-11.5,9.3-20.8,20.8-20.8h375c11.5,0,20.8,9.3,20.8,20.8v7.1c-2,1.4-3.9,2.9-5.6,4.6
                                              L318.6,187.5h-204c-5.8,0-10.4,4.7-10.4,10.4c0,5.8,4.7,10.4,10.4,10.4h187.3l-10,50c-0.7,3.4,0.4,6.9,2.8,9.4
                                              c2,2,4.6,3.1,7.4,3.1c0.7,0,1.4-0.1,2-0.2l52.1-10.4c2-0.4,3.9-1.4,5.3-2.8l76-76V333.3z M349,240.4l-33.7,6.7l6.7-33.7
                                              l94.6-94.6l26.9,26.9L349,240.4z M473.6,115.8l-15.3,15.3l-26.9-26.9l15.3-15.3c3.5-3.5,8.4-5.6,13.5-5.6c10.5,0,19,8.5,19,19
                                              C479.2,107.4,477.1,112.3,473.6,115.8z"/>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </div>
                </div>
                <div class="sidebar-toggle">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/toggle.svg" class="img-responsive">
                </div>
            </div>
            <div class="down-approv">
            <?php for ($i = 0; $i < sizeof($designer_file); $i++) { ?>
            <div class="hdr-buttons butt_<?php echo $designer_file[$i]['id'];?>"
                <?php if ($main_id == $designer_file[$i]['id']) {
                    echo "style='display: block;'";
                } else {
                    echo "style='display:none;'";
                }
            ?>>
            <div class="">
            <div class="buttons_quality">
                <?php if ($designer_file[$i]['status'] == "pending") { ?>
                    
                    <div id="status">
                    <p class="qua-litypas-sed approved app_quanlity_psd" style="display: none;"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-check-ap.svg" class="img-responsive"> Quality Passed </p>
                        <h3 class="rejected notapprove rej_desgn_prjct" style="display: none;">NOT APPROVED</h3>
                    </div>
                    <?php } elseif ($designer_file[$i]['status'] == "Reject") { ?>
                        <div id="status">
                        <h3 class="rejected">Not Approved</h3>
                        </div>
                    <?php } elseif ($designer_file[$i]['status'] == "customerreview") { ?>
                        <div id="status">
                        <p class="qua-litypas-sed approved"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-check-ap.svg" class="img-responsive"> Quality Passed</p>
                       </div>
                    <?php } elseif ($designer_file[$i]['status'] == "Approve") { ?>
                        <div id="status">
                        <p class="qua-litypas-sed approved"> <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-check-ap.svg" class="img-responsive">Approved</p>
                        </div>
                    <?php } ?>
                    <div class="approvedate admin-del">  
                    <div class="downld_del_sec">
                        <form action="<?php echo base_url(); ?>qa/dashboard/downloadzip" method="post">
                            <input type="hidden" name="srcfile" value="<?php echo "public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>">
                            <input type="hidden" name="prefile" value="<?php echo "public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>">
                            <input type="hidden" name="project_name" value="<?php echo isset($data[0]['title']) ? $data[0]['title'] : 'Graphicszoo'; ?>">
                            <button type="submit" name="submit" style="border:none"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </form>
                        <form action="<?php echo base_url(); ?>/admin/dashboard/gz_delete_files" method="post">
                            <input type="hidden" name="draft_count" value="<?php echo $designer_file[$i]['draft_count']; ?>">
                            <input type="hidden" name="srcfile" value="<?php echo "public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>">
                            <input type="hidden" name="prefile" value="<?php echo "public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>">
                            <input type="hidden" name="thumb_prefile" value="<?php echo "public/uploads/requests/" . $_GET['id'] . "/_thumb/" . $designer_file[$i]['file_name']; ?>">
                            <input type="hidden" name="project_name" value="<?php echo $data[0]['title']; ?>">
                            <input type="hidden" name="requests_files" value="<?php echo $designer_file[$i]['id']; ?>">
                            <input type="hidden" name="request_id" value="<?php echo $_GET['id']; ?>">
                            <button type="submit" name="gz_delete_files" onclick="return cnfrm_delete()" style="border:none"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </form>
                        <button class="nav-menu-itm" type="button" name="bars" style="border:none"><i class="fa fa-bars" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
            </div> 
            </div>
            <?php } ?>
        </div>
             </div>
    </div>
</div>
<div class="main-three-outer">
    <div class="left-slide-sidebar">
        <div class="pro-ject-leftbar two-can">
            <div class="orb-xbin one-can slick_thumbs">
                <ul class="list-unstyled orbxbin-list" id="thub_nails">
                    <?php for ($i = 0; $i < sizeof($designer_file); $i++) { ?>
                            <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="active1 <?php echo $designer_file[$i]['id']; ?>" id="<?php echo $designer_file[$i]['id']; ?>">
                            <figure class="imgobxbins leftnav_<?php echo $designer_file[$i]['id']; ?>" style="background-color:#fff;">
                        <?php
                            $type = substr($designer_file[$i]['file_name'], strrpos($designer_file[$i]['file_name'], '.') + 1);
                        ?>
                        <img src="<?php echo imageUrl($type, $_GET['id'], $designer_file[$i]['file_name']); ?>" class="img-responsive">
                        </figure>
                        </li>
                    <?php } ?>
                </ul>
            </div>
    </div>
    </div>
    <div class="pro-ject-wrapper dot-comment">
        <div class="view-commentBox">
       <div class="row">
        <div class="col-md-12">
            <div id="myCarousel" class="carousel fade-carousel carousel-fade slide" data-ride="carousel" data-interval="false">
               <div class="carousel-inner" id="cro" style="z-index: 99;overflow: unset;">
                        <?php
                        //echo "<pre/>";print_r($designer_file);
                        for ($i = 0; $i < sizeof($designer_file); $i++) {
                            ?>
                            <div class="item slides two-can outer-post <?php if ($designer_file[$i]['id'] == $main_id) { echo 'active';}?>" id="<?php echo $designer_file[$i]['id']; ?>" data-name="<?php echo $designer_file[$i]['file_name']; ?>">
                                <div class="customer_loader loading_<?php echo $designer_file[$i]['id']; ?>" style="display:none;">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/gz_customer_loader.gif">
                                </div>
                                <div class="col-md-12 mydivposition mydivposition_<?php echo $designer_file[$i]['id']; ?>"  onclick="printMousePos(event,<?php echo $designer_file[$i]['id']; ?>);" id="mydivposition_<?php echo $designer_file[$i]['id']; ?>" onmouseout="hideclicktocomment();" onmouseover="showclicktocomment(event,<?php echo $designer_file[$i]['id']; ?>);" onmousemove="showclicktocomment(event,<?php echo $designer_file[$i]['id']; ?>);" >
                                    <div class="gz ajax_loader loader loading_<?php echo $designer_file[$i]['id']; ?>" style="display:none;">
                                       <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif">
                                    </div>
                                    <div class="slid" data-id="<?php echo $designer_file[$i]['id']; ?>">
                                        <?php
                                        $type = substr($designer_file[$i]['file_name'], strrpos($designer_file[$i]['file_name'], '.') + 1);
                                        if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                            ?>
                                            <img class="img-responsive" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" style="/*max-height:calc(100vh - 80px);*/margin: auto;display: block;"/>
                                            <p class="extension_name"><?php echo $type; ?></p>
                                        <?php } else { ?>
                                            <img class="img-responsive" src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" style="/*webkit-filter:blur(8px);filter:blur(8px);*/" />
                                        <?php }
                                        ?>
                                    </div>
                                    <!-- Chat toolkit start-->
                                    <div class="appenddot_<?php echo $designer_file[$i]['id']; ?> remove_dot DotAppenD">
                                        <?php
                                        $total = array_diff(array_column($designer_file[$i]['chat'],'xco'),['']);
                                        $counttotaldotleft = count(array_values($total));
                                        for ($j = 0; $j < sizeof($designer_file[$i]['chat']); $j++) {
                                            if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                                $background = "pinkbackground";
                                            } else {
                                                $background = "bluebackground";
                                            }
                                            ?>
                                            <?php
                                            if ($designer_file[$i]['chat'][$j]['xco'] != "") {
                                                if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                                    $image1 = "dot_image1.png";
                                                    $image2 = "dot_image2.png";
                                                } else {
                                                    $image1 = "designer_image1.png";
                                                    $image2 = "designer_image2.png";
                                                }
                                                ?>
                                                <?php if (is_numeric($designer_file[$i]['chat'][$j]['xco'])) {
                                                    
                                                     //echo $counttotaldotleft;
                                                    ?>
                                                    <div id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" class="abcabcabc" style="display:none;position:absolute;opacity:0;width:30px;z-index:0;height:30px;" data-dot="<?php echo $counttotaldotleft; ?>" data-left="<?php echo $designer_file[$i]['chat'][$j]['xco']; ?>" data-top="<?php echo $designer_file[$i]['chat'][$j]['yco']; ?>" onclick='show_customer_chat(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, event,$(this));'>
                                                        <div  id="dot-comment-<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" class="customer_chatimage1 beatHeart customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><span class="numberdot" id="dotnum_<?php echo $designer_file[$i]['id']; ?>"><?php echo $counttotaldotleft; ?></span></div>
                                                        <div style="display:none;" class="customer_chatimage2 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"></div>
                                                    </div>
                                                    <div class="customer_chat customer_chat<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" data-left="<?php echo $designer_file[$i]['chat'][$j]['xco']; ?>" data-top="<?php echo $designer_file[$i]['chat'][$j]['yco']; ?>" style="display:none;position:absolute;left:<?php echo $designer_file[$i]['chat'][$j]['xco']; ?>;top:<?php echo $designer_file[$i]['chat'][$j]['yco']; ?>;padding: 5px;border-radius: 5px;">
                                                         <div class="posone2" style="z-index: 99999999999999999999999;">
                                                                <div class="posonclick-add" id="posonclick-add<?php echo $designer_file[$i]['chat'][$j]['id']; ?>">
                                                                    <div class="posone2abs arrowdowns"  id="dotnum_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>_Next" onclick="return avoidClick(event);" >
                                                                        <a href="javascrip:void(0)" onclick="return closeChatBox(event);" class="crossblink"><i class="fa fa-times"></i></a>
                                                                        <div class="comment-box-es new-patrn">
                                                                            <div class="cmmbx-col left">
                                                                                <div class="cmmbxcir">
                                                                                    <figure class="pro-circle-img-xxs t1">
                                                                                        <img src="<?php echo $designer_file[$i]['chat'][$j]['profile_picture']; ?>">
                                                                                    </figure>
                                                                                </div>
                                                                                <div class='new_msg_des'>
                                                                                    <div class="ryt_header">
                                                                                        <span class="cmmbxtxt">
                                                                                            <?php echo isset($designer_file[$i]['chat'][$j]['first_name'])?$designer_file[$i]['chat'][$j]['first_name']:$designer_file[$i]['chat'][$j]['customer_name']; ?>
                                                                                        </span>
                                                                                        <span class="kevtxt" title='<?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?>'><?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?></span>
                                                                                    </div>
                                                                                    <pre><p class="distxt-cmm distxt-cmm_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></p></pre>
                                                                                        <div class="edit_del">
                                                                                            <a href="javascript:void(0)" onclick="replyButton(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, false)" class="reply_to_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><i class="fas fa-reply-all"></i></a>
                                                                                            <?php if ($_SESSION['user_id'] == $designer_file[$i]['chat'][$j]['sender_id']) { ?>    
                                                                                            <a href="javascript:void(0)" onclick='editMessages(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, false)'  class="edit_to_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><i class="fas fa-pen"></i></a> 
                                                                                            <a href="javascrip:void(0)" onclick="deleteMsg(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>);" class="del_msg"><i class="fas fa-trash"></i></a>                                                                               
                                                                                        <?php } ?>
                                                                                            </div>
                                                                                        <div class="msgk-mn-edit comment-submit_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none">
                                                                                            <form action="" method="post">
                                                                                                <textarea class="pstcmm sendtext" id="editdot_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, 'left')" data-id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>">Save</a> <a href="javascript:void(0)" class="cancel" onclick="cancelButton(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>)">Cancel</a>
                                                                                            </form>
                                                                                        </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="allreplyy two-can">
                                                                            <?php
                                                                            foreach ($designer_file[$i]['chat'][$j]['replies'] as $key => $val) {
                                                                                //echo "<pre/>";print_r($val);
                                                                                ?>
                                                                                <div class="cmmbx-col left reply reply_<?php echo $val['id']; ?>">
                                                                                    <div class="cmmbxcir">
                                                                                        <figure class="pro-circle-img-xxs t5">
                                                                                            <img src="<?php echo $val['profile_picture']; ?>">
                                                                                        </figure>
                                                                                    </div>
                                                                                    <div class="msg_sect">
                                                                                        <div class="name_edit_del">
                                                                                           <span class="cmmbxtxt"><?php echo isset($val['first_name'])?$val['first_name']:$val['customer_name']; ?></span>
                                                                                           <pre><p class="distxt-cmm distxt-cmm_<?php echo $val['id']; ?>"><?php echo $val['message']; ?></p></pre>

                                                                                           <?php 
                                                                                           //echo $_SESSION['user_id'] ."==". $val['sender_id'];
                                                                                           if ($_SESSION['user_id'] == $val['sender_id']) { ?> 
                                                                                            <div class="edit_del">
                                                                                                <a href="javascript:void(0)" onclick='editMessages(<?php echo $val['id']; ?>, false)' class="edit_to_<?php echo $val['id']; ?>"><i class="fas fa-pen"></i></a>
                                                                                                <a href="javascrip:void(0)" onclick="deleteMsg(<?php echo $val['id']; ?>);" class="del_msg"><i class="fas fa-trash"></i></a>                                                                               
                                                                                            </div>
                                                                                        <?php } ?>
                                                                                        <div class="msgk-mn-edit comment-submit_<?php echo $val['id']; ?>" style="display:none">
                                                                                            <form action="" method="post">
                                                                                                <textarea class="pstcmm sendtext" id="editdot_<?php echo $val['id']; ?>"><?php echo $val['message']; ?></textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(<?php echo $val['id']; ?>, 'left')" data-id="<?php echo $val['id']; ?>">Save</a> <a href="javascript:void(0)" onclick="cancelButton(<?php echo $val['id']; ?>)" class="cancel">Cancel</a>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <?php } ?>
                                                                        </div>
                                                                        <div class="closeoncross reply_msg_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none">
                                                                            <form action="" method="post">
                                                                                <div class="cmmtype-row wide_text">
                                                                                    <textarea class="pstcmm sendtext text1_<?php echo $designer_file[$i]['id']; ?>" id="text1_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" Placeholder="Post Comment" autocomplete="off" data-reqid="<?php echo $designer_file[$i]['id']; ?>"></textarea>
                                                                                    <span class="cmmsend">
                                                                                        <a class="cmmsendbtn send_comment_without_img send_<?php echo $designer_file[$i]['id']; ?>"
                                                                                            onclick="send_comment_without_img('<?php echo $designer_file[$i]['id'] ?>', 'customer', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>', 'leftreply', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>');">
                                                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive">
                                                                                        </a>
                                                                                    </span>
                                                                                </div>
                                                                            </form> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                                }
                                            }
                                            ?>
                                    <?php 
                                     if(is_numeric($designer_file[$i]['chat'][$j]['xco'])){
                                        $counttotaldotleft--;
                                        }} ?>
                                    </div>
                                    <div class="openchat" style="width:35%;position:absolute;display:none;padding:10px 20px;">
                                        <div class="posone1">
                                            <div class="posonclick-add" id="posonclick-add-inner<?php echo $designer_file[$i]['id']; ?>">
                                                <div class="posone3abs arrowdowns">
                                                    <div class="cmmbxpost">
                                                        <p class="leave-commt">Post a comment</p>
                                                        <textarea type="text" placeholder="Write Comment here" class="tff-con text_write text_<?php echo $designer_file[$i]['id']; ?>" onclick="wideText(this)" placeholder="Write Comment here"></textarea>
                                                    </div>
                                                    <div class="postrow12">
                                                        <span class="pstcancel"><a class="mycancellable" href="javascript:void(0);">Cancel</a></span>
                                                        <span class="pstbtns">
                                                            <button class="pstbtns-a send_request_img_chat send_text send_text<?php echo $designer_file[$i]['id']; ?>" style="border: none;border-radius: 5px;padding: 3px 10px 4px;" 
                                                                    data-fileid="<?php echo $designer_file[$i]['id']; ?>"
                                                                    data-senderrole="admin" 
                                                                    data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                                                                    data-designerpic="<?php echo $user[0]['profile_picture']; ?>"
                                                                    data-receiverid="<?php echo $request[0]['customer_id']; ?>" 
                                                                    data-receiverrole="customer"
                                                                    data-parentid = '0'
                                                                    data-customername="<?php echo $user[0]['first_name']; ?>"
                                                                    data-xco="" data-yco="">Send</button>
                                                        </span> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="showclicktocomment" style="display:none;">
                                        Click To Comment
                                    </div>
                                    <!-- Chat Toolkit end -->
                                </div>
                                <?php if ($designer_file[$i]['status'] != "Reject") { ?>
                                    <!--<div class="overlay_comment"></div>-->
                            <?php } ?>
                            </div>
                        <?php $counttotaldots--;} ?>
                    </div>
            </div>
        </div>
   </div> 
</div>

    <div class="qa-project-review">
            <span id="onchangeDraftid"></span>

            <form  id="collectionFeedback-form" >
                <input type="hidden" name="project-id" value="<?php echo $id; ?>">
                <input type="hidden" id="draft-id" name="draft-id" value="<?php echo $draftid; ?>">
                <input type="hidden" name="designer-id" value="<?php echo $designer_file[0]['user_id']; ?>">

            <div id="collectionFeedback" class="hide"></div>
             <span id='counter'></span>

             <div class="review-option-main" style="display: none;">
                            <div class="backToprevQuestion" style="display: none; ">
                                                            <a href="javascript:void(0); " class="angle-left-ancor" >
                                                                <i class="fas fa-angle-left"></i>
                                                            </a>
                                                     </div> 
                                                      <?php  $lastvar=""; $i=1; foreach ($qustArr as $k => $Ques) {  
                                                    $icon_like= ''; 
                                                    $icon_dislike = ''; 
                                                    $icon_dactive="";
                                                    $icon_active="";
                                                    if($i ==1){
                                                        $active ="active";
                                                     }else{ 
                                                        $active ="hide";
                                                     }
                                                    if($i == count($qustArr)){
                                                         
                                                        $lastvar = $i; 
                                                     }
                                             ?>
                                            <div class="review-option-outer <?php echo $active; ?> " data-id="<?php echo $i; ?>" id="fds<?php echo $i; ?>">
                                                <div class="review-option">  
                                                         <p class="">  <?php echo $Ques; ?></p>
                                                        <div class="like-dislike">
                                                           <a href="#" data-like="1" class="<?php echo !empty($icon_active) ? $icon_active : ""; ?>" data-count="<?php echo $i; ?>" ><i class="<?php echo !empty($icon_like) ? $icon_like : "far"; ?> fa-thumbs-up"></i> Yes</a>
                                                           <a href="#" data-like="2" class="<?php echo !empty($icon_dactive) ? $icon_dactive : ""; ?>"   data-count="<?php echo $i; ?>" ><i class="<?php echo !empty($icon_dislike) ? $icon_dislike : "far"; ?> fa-thumbs-down"></i> No</a>
                                                </div>
                                            </div>
                                         </div>
                                        <?php $i++; } ?>
                                         </div>
                                     
                                           
                                            <span id="totalQuestion" data-last="<?php echo $lastvar; ?>"></span>
                                            
                                            <div class="submit-draft" style="display: none; ">
                                                <div class="warningMsg">
                                                    <span id="warningMsg"></span>
                                                </div>

                                       


                                                <input id="sub-button" type="button" class="btn-c greenbtn approve response_request load_more" value="Quality Pass" data-fileid="<?php echo $main_id; ?>" data-status="customerreview"  data-requestid="<?php echo $_GET['id']; ?>">
                                                <input id="cancel-button" type="button" class="btn-c redbtn approve response_request default-btn" value="Quality Fail" data-fileid="<?php echo $main_id; ?>" data-status="Reject"  data-requestid="<?php echo $_GET['id']; ?>">     
                                            </div>

                        </form>
        </div>
</div>
<div class="pro-ject-rightbar" id="comment_list_all"> 

    <div class="project-details-bar">
        <h2 class="">project details</h2>
        <div class="project-info-list">
            <div class="info-items">
                    <h3>project title</h3>
                    <div class="p_ryt_info">
                    <span><?php echo $request[0]['title'];?> </span>
                    </div>
            </div>
            <div class="info-items">
                    <h3>Color Preference</h3>
                    <div class="p_ryt_info">
                    <span> <?php echo $request[0]['color_pref'];?> </span>
                    </div>
            </div>
            <div class="info-items">
                    <h3>Deliverables</h3>
                    <div class="p_ryt_info">
                    <span><?php echo $request[0]['deliverables'];?>  </span>
                    </div>
            </div>
            <div class="info-items">
                    <h3>Category</h3>
                    <div class="p_ryt_info Category">
                    <span>
                        <?php echo isset($request['cat_name']) ? $request['cat_name'].'<p> > </p>' : $request[0]['Category'].'<p> > </p>'; 
                            if ($request[0]['logo_brand'] != '') { ?> 
                       
                     </span>
                            <span><?php
                                echo $request[0]['logo_brand'];
                            } elseif ($request['subcat_name'] != '') {
                                ?></span><span><?php
                                echo $request['subcat_name'];
                            }
                            ?></span>
                    </div>
            </div>
            <div class="info-items">
                    <h3>brand profile</h3>
                    <div class="p_ryt_info">
                    <?php if ($branddata[0]['brand_name'] != '') { ?>
                        <a class="showbrand_profile"  data-id="<?php echo $branddata[0]['id']; ?>"><i class="fa fa-user"></i> <?php echo $branddata[0]['brand_name']; ?></a>
                    <?php } else { ?>
                        No brand profile selected
                    <?php } ?>
                    </div>
            </div> 

            <?php
            if (!empty($request_meta)) {
                foreach ($request_meta as $qkey => $qval) {
                    if ($qval['question_label'] == 'DESIGN DIMENSIONS' || $qval['question_label'] == 'DESCRIPTION') {
                        if ($qval['question_label'] == 'DESIGN DIMENSIONS') {
                            if ($qval['answer'] != '') {
                                $qval['answer'] = $qval['answer'];
                            } else {
                                $qval['answer'] = $request[0]['design_dimension'];
                            }
                        }
                        if ($qval['question_label'] == 'DESCRIPTION') {
                            if ($qval['answer'] != '') {
                                $qval['answer'] = $qval['answer'];
                            } else {
                                $qval['answer'] = $request[0]['description'];
                            }
                        }
                    } else {
                        $qval['answer'] = $qval['answer'];
                    }

                    if(isset($qval['answer']) && $qval['answer'] != ''){
                      if(strlen(htmlspecialchars($qval['answer'])) > 100){
                        $answer = substr(htmlspecialchars($qval['answer']),0,97).'... <a class="expand_txt read_more" href="javascript:void(0);">Read More</a><div class="read_more_txt hide">'.htmlspecialchars($qval['answer']).'</div><a class="expand_txt read_less hide" href="javascript:void(0);">Read Less</a>';
                      }else{
                        $answer = htmlspecialchars($qval['answer']);
                      }
                    }else{
                      $answer = "N/A";
                    }
                    ?>

                    <?php
                    if ($qval['question_label'] != 'DESCRIPTION') {
                        ?>
                        <div class="info-items">
                            <h3> <?php echo $qval['question_label']; ?></h3>
                            <div class="p_ryt_info">
                                  <?php echo $answer; ?>
                            </div>
                        </div>      
                    <?php } else { ?>
                        <div class="info-items d_descrptn">
                            <h3> <?php echo $qval['question_label']; ?></h3>
                            <div class="p_ryt_info">

                                 <?php echo $answer; ?>
                            </div>
                        </div> 
                        <?php
                    }
                }
            } else {
                ?>
                <div class="info-items">
                    <h3> Design Dimension</h3>
                    <div class="p_ryt_info">
                        <?php echo isset($request[0]['design_dimension']) ? $request[0]['design_dimension'] : 'N/A'; ?>
                    </div>
                </div>
                <div class="info-items d_descrptn">
                    <h3> Description</h3>
                    <div class="p_ryt_info">
                       <?php echo isset($request[0]['description']) ? (strlen(htmlspecialchars($request[0]['description'])) > 100)?substr(htmlspecialchars($request[0]['description']),0,97).'... <a class="expand_txt read_more" href="javascript:void(0);">Read More</a><div class="read_more_txt hide">'.htmlspecialchars($request[0]['description']).'</div> <a class="expand_txt read_less hide" href="javascript:void(0);">Read Less</a> ':htmlspecialchars($request[0]['description']) : 'N/A'; ?>
                    </div>
                </div>
            <?php } ?>
            <?php if(!empty($request[0]['customer_attachment'])){ ?>
            <div class="info-items">
                    <h3>Attachments</h3>
                    <div class="p_ryt_info">
                        <?php for ($i = 0; $i < count($request[0]['customer_attachment']); $i++) { 
                            $imgName    =   $request[0]['customer_attachment'][$i]['file_name']; 
                            $imgArr     =   array('png','jpeg','gif','jpg','bmp','PNG','BMP','JPEG','JPG','GIF');
                            $ext        =   pathinfo($imgName, PATHINFO_EXTENSION);
                            
                            if(in_array($ext,$imgArr)) { //echo "here"; ?>
                               <a href="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $request[0]['id'] . "/" . $request[0]['customer_attachment'][$i]['file_name']; ?>" data-fancybox="images"> <i class="fas fa-eye"></i> </a>
                            <?php } else { //echo "1";?> 
                                 <a href="<?php echo base_url() . "admin/Dashboard/download_projectfile/" . $request[0]['id'] . "?imgpath=" . $request[0]['customer_attachment'][$i]['file_name'] ?>"><i class="fas fa-download"></i></a>
                         <?php   } ?>
                          
                        
                     
                    
                 <?php } ?>                                             
                    </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="sidebar-header">
        <ul id="comment-people" class="list-header-blog" role="tablist" style="border:none;">
        <li class="active" id="1"><a data-toggle="tab" href="#comments_tab" role="tab"><i class="icon-gz_message_icon"></i> Comments </a></li>          
        <li class="" id="2" ><a class="nav-link tabmenu" data-toggle="tab" href="#peoples_tab" role="tab">
            <i class="icon-gz_share"></i> Share</a></li>
        </ul>
    </div>
    <div class="tab-content">
    <div data-group="1" data-loaded=""  class="tab-pane active content-datatable datatable-width" id="comments_tab" role="tabpanel">
    <?php for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['src_file'] = str_replace(' ', '+', $designer_file[$i]['src_file']); 
            $designer_file[$i]['file_name'] = str_replace(' ', '+', $designer_file[$i]['file_name']); ?>
    <div class="comment_sec comment-sec-<?php echo $designer_file[$i]['id']; ?>" id="<?php echo $designer_file[$i]['id']; ?>" 
        <?php
        if ($main_id == $designer_file[$i]['id']) {
            echo "style='display: block;'";
        } else {
            echo "style='display:none;'";
        }
        ?>>
    <div class=""> 
        <div class="cmmtype-box">
            <form action="" method="post">
                <div class="cmmtype-row">
                    <textarea class="pstcmm sendtext abc_<?php echo $designer_file[$i]['id']; ?>" Placeholder="Post Comment" id="sendcom_<?php echo $designer_file[$i]['id']; ?>" autocomplete="off" data-reqid="<?php echo $designer_file[$i]['id']; ?>"></textarea>
                     <div class="send-attech">
                        <label for="image">
                            <input type="file" name="shre_main_draft_file[]" id="shre_file_<?php echo $designer_file[$i]['id']; ?>" class="shre_file" data-withornot="1" data-draftid="<?php echo $designer_file[$i]['id']; ?>" style="display:none;" multiple onchange="uploadDraftchatfile(event, 'admin', '<?php echo $request[0]['id']; ?>', '<?php echo $_SESSION['user_id']; ?>', 'customer', '<?php echo $request[0]['customer_id']; ?>', '<?php echo $_SESSION['first_name']; ?>', '<?php echo $request[0]['profile_picture']; ?>', '0', 'admin_seen')"/>
                            <span class="darftattchmnt" data-draft-id="<?php echo $designer_file[$i]['id']; ?>" title="Add Files"><i class="fa fa-paperclip" aria-hidden="true"></i></span>
                            <input type="hidden" value="" name="saved_draft_file[]" class="delete_file"/>
                        </label>
                        <span class="cmmsend">
                            <a class="cmmsendbtn send_comment_without_img send_<?php echo $designer_file[$i]['id']; ?>"
                                onclick="send_comment_without_img('<?php echo $designer_file[$i]['id'] ?>', 'admin', '0', '', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>');">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive">
                            </a>
                        </span>
                    </div>
                </div>
            </form> 
        </div>
        <div class="ajax_searchload chat-align" style="display:none; text-align:center;">
                             <img src="<?php echo base_url();?>public/assets/img/customer/gz_customer_loader.gif" />
                    </div>
    <ul class="two-can list-unstyled list-comment-es comment_list_view one-can messagediv_<?php echo $designer_file[$i]['id']; ?>" id="comment_list">
   <?php
      //echo "<pre/>";print_R($designer_file);
        $total = array_diff(array_column($designer_file[$i]['chat'], 'xco'), ['']);
        $counttotaldots = count(array_values($total));
        for ($j = 0; $j < sizeof($designer_file[$i]['chat']); $j++) {
            ?>
            <?php if ($designer_file[$i]['chat'][$j]['xco'] != 'NaN' || $designer_file[$i]['chat'][$j]['yco'] != 'NaN'): ?>
                <li class="<?php echo (isset($designer_file[$i]['chat'][$j]['xco']) && $designer_file[$i]['chat'][$j]['xco'] != '') ? "dotlist_" . $designer_file[$i]['id'] . "" : ''; ?>">
                    <div class="comment-box-es new-patrn">
                        <div class="comments_link">
                         <div class="cmmbx-row">
                            <div class="all-sub-outer">
                                <div class="cmmbx-col left">
                                    <div class="clickable" id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" data-dot="<?php echo $counttotaldots; ?>">
                                        <div class="cmmbxcir">
                                            <figure class="pro-circle-img-xxs t2">
                                                <img src="<?php echo $designer_file[$i]['chat'][$j]['profile_picture']; ?>">
                                            </figure>
                                            <?php if ($designer_file[$i]['chat'][$j]['xco'] != "" && $designer_file[$i]['chat'][$j]['yco'] != "") { ?>
                                                <span class="cmmbxonline" onclick="show_customer_chat(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>,event,$(this))">
                                                   <span class="numberdot" id="dotnum_<?php echo $designer_file[$i]['id']; ?>"><?php echo $counttotaldots; ?></span>
                                               </span>
                                           <?php } ?>
                                       </div>
                                       <div class="cmmbx-col">
                                         <div class="ryt_header">
                                            <span class="cmmbxtxt">
                                                <?php
                                                if ($designer_file[$i]['chat'][$j]['sender_role'] == 'designer') {
                                                    echo $designer_file[$i]['chat'][$j]['first_name'];
                                                } else if ($designer_file[$i]['chat'][$j]['sender_role'] == 'customer' && $designer_file[$i]['chat'][$j]['sender_id'] != 0) {
                                                    echo $designer_file[$i]['chat'][$j]['first_name'];
                                                } else if ($designer_file[$i]['chat'][$j]['sender_role'] == 'customer' && $designer_file[$i]['chat'][$j]['sender_id'] == 0) {
                                                    echo $designer_file[$i]['chat'][$j]['customer_name'];
                                                } else if ($designer_file[$i]['chat'][$j]['sender_role'] == 'admin') {
                                                    echo $designer_file[$i]['chat'][$j]['first_name'];
                                                } else if ($designer_file[$i]['chat'][$j]['sender_role'] == 'qa') {
                                                  echo $designer_file[$i]['chat'][$j]['first_name'];
                                                } else if ($designer_file[$i]['chat'][$j]['sender_role'] == 'va') {
                                                   echo $designer_file[$i]['chat'][$j]['first_name'];
                                                }
                                                ?>
                                            </span>
                                            <span class="kevtxt" title='<?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?>'><?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?></span>
                                            <?php if ($_SESSION['user_id'] == $designer_file[$i]['chat'][$j]['sender_id']) { ?>

                                            <?php } ?>
                                        </div>
                                        <?php if($designer_file[$i]['chat'][$j]['is_filetype'] != 1){ ?>
                                        <pre><p class="distxt-cmm distxt-cmm_ryt_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></p></pre>
                                        <?php } else{
                                            $type = strtolower(substr($designer_file[$i]['chat'][$j]['message'], strrpos($designer_file[$i]['chat'][$j]['message'], '.') + 1));
                                            $updateurl = '';
                                            if (in_array($type, DOCARROFFICE)) {
                                                $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                                                $class = "doc-type-file";
                                                $exthtml = '<span class="file-ext">' . $type . '</span>';
                                            } elseif (in_array($type, FONT_FILE)) {
                                                $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                                                $class = "doc-type-file";
                                                $exthtml = '<span class="file-ext">' . $type . '</span>';
                                            } elseif (in_array($type, ZIPARR)) {
                                                $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-zip.svg';
                                                $class = "doc-type-zip";
                                                $exthtml = '<span class="file-ext">' . $type . '</span>';
                                            } elseif (in_array($type, IMAGEARR)) {
                                                $imgVar = imageRequestDraftchatUrl($type, $designer_file[0]['request_id'] . '/' .$designer_file[$i]['chat'][$j]['request_file_id'], $designer_file[$i]['chat'][$j]['message']);
                                                $updateurl = '/_thumb';
                                                $class = "doc-type-image";
                                                $exthtml = '';
                                            }
                                            $basename = $designer_file[$i]['chat'][$j]['message'];
                                            $basename = strlen($basename) > 20 ? substr($basename, 0, 20) . "..." . $type : $basename;
                                            $data_src = FS_PATH_PUBLIC_UPLOADS . 'requestdraftmainchat/' . $designer_file[0]['request_id'] . '/' .$designer_file[$i]['chat'][$j]['request_file_id'].'/'. $designer_file[$i]['chat'][$j]['message'];?>
                                           <span class="msgk-umsxxt took_<?php echo $chatdata['id']; ?>">
                                                <div class="contain-info <?php echo (!in_array($type, IMAGEARR)) ? 'only_file_class' : ''; ?>" >
                                                    <a class="open-file-chat <?php echo $class; ?>" data-ext="<?php echo $type; ?>" data-src="<?php echo $data_src; ?>">
                                                        <img src="<?php echo $imgVar; ?>"> <?php echo $exthtml; ?>
                                                    </a>
                                                    <div class="download-file">
                                                        <div class="file-name">
                                                            <h4><b><?php echo $basename; ?></b></h4>
                                                        </div>
                                                        <a href="<?php echo base_url() ?>customer/request/download_stuffFromChat?ext=<?php echo $type; ?>&file-upload=<?php echo $data_src; ?>" target="_blank"> 
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/dwnlod-img.svg" />
                                                        </a>
                                                    </div>
                                                </div>
                                            </span>
                                            <?php } ?>
                                        <div class="edit_del"> 
                                           <a href="javascript:void(0)" onclick='replyButton(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, true)' class="reply_to_ryt_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><i class="fas fa-reply-all"></i></a>
                                           <?php if ($_SESSION['user_id'] == $designer_file[$i]['chat'][$j]['sender_id']) { 
                                            if($designer_file[$i]['chat'][$j]['is_filetype'] != 1){   
                                            ?> 
                                           <a href="javascript:void(0)" onclick='editMessages(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, true)' class="edit_to_ryt_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><i class="fas fa-pencil-alt"></i></a>
                                            <?php } ?>
                                           <a href="javascript:void(0)" onclick="deleteMsg('<?php echo $designer_file[$i]['chat'][$j]['id']; ?>')" class="del_msg"><i class="far fa-trash-alt"></i></a>
                                           <?php } ?> 
                                        </div>
                                   </div>
                               </div>
                               <div class="msgk-mn-edit comment-submit_ryt_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none">
                                   <form action="" method="post">
                                       <textarea class="pstcmm sendtext" id="editryt_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, 'right')" data-id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>">Save</a> <a href="javascript:void(0)" class="cancel" onclick="cancelButton(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>)">Cancel</a>
                                   </form>
                               </div>
                           </div>
                           <?php foreach ($designer_file[$i]['chat'][$j]['replies'] as $key => $val) { ?>
                            <div class="cmmbx-col left reply clearfix reply_<?php echo $val['id']; ?>">
                                <div class="cmmbxcir">
                                    <figure class="pro-circle-img-xxs t5">
                                        <img src="<?php echo $val['profile_picture']; ?>">
                                    </figure>
                                </div>
                                <div class="msg_sect">
                                    <div class="name_edit_del">
                                        <span class="cmmbxtxt">
                                            <?php echo isset($val['first_name']) ? $val['first_name'] : $val['customer_name']; ?>
                                        </span>
                                        <?php $currentTime = new DateTime(date("Y-m-d h:i:s")); ?>

                                        <div class="msgk-mn-edit comment-submit_ryt_<?php echo $val['id']; ?>" style="display:none">
                                            <form action="" method="post">
                                                <textarea class="pstcmm sendtext" id="editryt_<?php echo $val['id']; ?>"><?php echo $val['message']; ?></textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(<?php echo $val['id']; ?>, 'right')" data-id="<?php echo $val['id']; ?>">Save</a> <a href="javascript:void(0)" onclick="cancelButton(<?php echo $val['id']; ?>)" class="cancel">Cancel</a>
                                            </form>
                                        </div>
                                    </div>
                                    <pre><p class="distxt-cmm distxt-cmm_ryt_<?php echo $val['id']; ?>"><?php echo $val['message']; ?></p></pre>
                                    <?php if($_SESSION['user_id'] == $val['sender_id']){?>
                                    <div class="edit_del"> 
                                        <a href="javascript:void(0)" onclick='editMessages(<?php echo $val['id']; ?>, true)' class="edit_to_ryt_<?php echo $val['id']; ?>"><i class="fas fa-pencil-alt"></i></a>
                                        <a href="javascrip:void(0)" onclick="deleteMsg(<?php echo $val['id']; ?>);" class="del_msg"><i class="far fa-trash-alt"></i></a>                                                                               
                                    </div>
                           <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="closeoncross reply_msg_ryt_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none">
                            <form action="" method="post">
                                <div class="cmmtype-row wide_text">
                                    <textarea class="pstcmm sendtext text1_<?php echo $designer_file[$i]['id']; ?>" id="text2_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" Placeholder="Post Comment" autocomplete="off" data-reqid="<?php echo $designer_file[$i]['id']; ?>"></textarea>
                                    <span class="cmmsend">
                                        <a class="cmmsendbtn send_comment_without_img send_<?php echo $designer_file[$i]['id']; ?>"
                                            onclick="send_comment_without_img('<?php echo $designer_file[$i]['id'] ?>', 'admin', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>', 'rightreply', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>');">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive">
                                        </a>
                                    </span>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
            </div>                     
        </div>
    </li>
    <?php
endif;
if (is_numeric($designer_file[$i]['chat'][$j]['xco'])) {
    $counttotaldots--;
}
}
        ?>   
    </ul>

</div>

    </div>
<?php } ?>
    </div>
   <div data-group="1" data-loaded=""  class="tab-pane content-datatable datatable-width" id="peoples_tab" role="tabpanel">
    <div class="sharedBy two-can">
        <div class="team_member_sec">
            <h3>Team
                    <div class="right-inst">
                        <a class="vwwv" href="javascript:void(0);">

                            <span class="vewviewer-cicls">
                                <img src="<?php echo $data[0]['customer_image']; ?>" class="img-responsive"/>
                            </span>
                            <span class="hoversetss"><?php echo $data[0]['customer_name']; ?>
                            </span>
                        </a>
                        <a class="vwwv" href="javascript:void(0);">
                            <span class="vewviewer-cicls">
                                <img src="<?php echo $data[0]['designer_image']; ?>" class="img-responsive"/>
                            </span>
                            <span class="hoversetss"><?php echo $data[0]['designer_name']; ?> (Designer)
                            </span>
                        </a>
                        <a class="vwwv" href="javascript:void(0);">
                            <span class="vewviewer-cicls">
                                <img src="<?php echo $data[0]['qa_image']; ?>" class="img-responsive"/>
                            </span>
                            <span class="hoversetss"><?php echo $data[0]['qa_name']; ?> (QA)
                            </span>
                        </a>
                    </div>
            </h3>

        </div>
        <?php
        if (!empty($alldraftpubliclinks) || !empty($shareddraftUser)) {
            foreach ($alldraftpubliclinks as $pubkey => $pubval) {
               if($pubval['public_link'] != '' && $pubval['is_disabled'] != 1){
                ?>
                <div class="public_link" id="public_<?php echo $pubval['draft_id']; ?>"
                    <?php
                    if ($main_id == $pubval['draft_id']) {
                        echo "style='display: block;'";
                    } else {
                        echo "style='display:none;'";
                    }
                    ?>>
                        <h3>Share via Link</h3>
                        <div class="share-people" id="mainpubliclinkshr_<?php echo $pubval['draft_id']; ?>" <?php echo (isset($pubval['is_disabled']) && $pubval['is_disabled'] == 1)? 'style="display:none"' :'test'; ?>>
                            <span class="copiedone"></span>   
                            <input type="text" readonly class="show_publink" name="linkshared" value="<?php echo (isset($pubval['public_link']) && $pubval['public_link'] != '') ? $pubval['public_link'] : ''; ?>"/>
                            <span class="copy_public_link" ><i class="far fa-clone"></i></span>
                            <div class="permision-here"></div>
                        </div>
                </div>
                <?php }
                } if(!empty($sharedreqUser) || !empty($shareddraftUser)){ ?>
                <h3>Shared Via Email</h3>
                <!--requests shared people-->
                <div id="sharedpeople" class="shareduser">
                    <?php foreach ($shareddraftUser as $sdk => $sdv) { ?>
                        <?php //echo $main_id.' - '.$sdv['draft_id']; ?>
                        <div class="sharedbymail_block_<?php echo $sdv['id']; ?>">
                            <ul class="shared_project_class shared_draft_user_<?php echo $sdv['draft_id']; ?>"
                            <?php
                            if ($main_id == $sdv['draft_id']) {
                                echo "style='display:flex;'";
                            } else {
                                echo "style='display:none;'";
                            }
                            ?>>
                                <li><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/share-bg.svg" class="img-responsive"></li>
                                <li><span><?php echo $sdv['user_name']; ?></span><?php echo $sdv['email']; ?></li>
                            </ul>
                        </div>

                    <?php } ?>
                </div>
              <?php  }
            } else {
                echo "<h3 class='no-data' style='text-transform: capitalize;font-weight: 500;letter-spacing: 0;color: #db203c'>There is no shared user for this request.</h3>";
            }
            ?>

</div>
</div>
</div>     
        
</div>
</div>
</div>
<div class="modal fade slide-3 in" id="qa_custmr_fdbk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="popup_h2 del-txt dgn_revw">Feedback</h2>
                <div class="cross_popup" data-dismiss="modal"> x</div>
               </div>
            <div class="modal-body">
                <ul class="list-header-blog feedbk_li">
                    <li class="active"> <a href="#customer_fdbk" class="stayhere" data-toggle="tab">Customer Review</a></li></ul>
              <div class="tab-content">
                <div class="tab-pane active" id="customer_fdbk">
                    <div class="design_review_form">
                        <div class="text-aprt">
                            <form class="f_feedback_frm" method="post" id="f_feedback_customrfrm">
                                <input type="hidden" name="review_draftid" class="review_draftid" value="">
                                <input type="hidden" name="review_reqid" class="review_reqid" value="">
                                <label class="form-group">
                                    <p class="choose-feedbk"> How satisfied are you with the designers updates to this design?</p>
                                    <div class="sound-signal">
                                        <div class="form-radion">
                                            <input type="radio" name="satisfied_with_design" class="preference" id="soundsignal12" value="5" required="">
                                            <label for="soundsignal12">
                                                <i class="far fa-thumbs-up"></i> Love it!</label>
                                        </div>
                                        <div class="form-radion">
                                            <input type="radio" name="satisfied_with_design" id="soundsignal23" class="preference" value="3" required="">
                                            <label for="soundsignal23">
                                                <i class="far fa-grin"></i> Neutral</label>
                                        </div>
                                        <div class="form-radion">
                                            <input type="radio" name="satisfied_with_design" id="soundsignal34" class="preference" value="1" required="">
                                            <label for="soundsignal34">
                                                <i class="far fa-thumbs-down"></i>Dissatisfied</label>
                                        </div>
                                    </div>
                                </label>
                                <label class="form-group">
                                    <p class="label-txt">Additional Note</p>
                                    <textarea name="additional_notes" class="review_addtional_note input"></textarea>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </form>
                        </div>
                    </div>
                    <div class='no_feedbk_cus' style="display:none">No Customer Feedback Found</div>
                </div>
         </div>
     </div>

 </div>
</div>
</div>
<div class="modal fade" id="brand_profile" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        </div>
    </div>
</div>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/admin/bootstrap.min.js');?>"></script>
<script type="text/javascript">
var $rowperpage = '';
var $assets_path = '<?php echo FS_PATH_PUBLIC_ASSETS; ?>';
var requestmainchatimg = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTMAINCHATFILES; ?>";
var requestdraftchatimg = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES; ?>";
var $customer_name = '<?php echo $_SESSION['first_name'] ?>';
var $designer_img = '<?php echo $_SESSION['designerpic'] ?>';
var profile_path = "<?php  echo FS_PATH_PUBLIC_UPLOADS_PROFILE ?>";
function cnfrm_delete() {
    return confirm("Are you sure you want to delete?");
}
function goBack() {
    window.history.back();
}
$('#myCarousel').carousel({
    interval: false,
    cycle: true
});
$(document).on('click','.showbrand_profile',function(){
    var brandid = $(this).data("id");
    $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>admin/dashboard/get_brand_details",
        data: {brandid: brandid},
        success: function (data) {
            $("#brand_profile .modal-content").html(data);
            $("#brand_profile").modal("show");
        }
    });
    
});
$('.btn_reset').click(function () {
    $('#cro .active .mydivposition .slid img').css('transform', 'scale(1)');
    $(this).css('display', 'none');
    $('#cro .active .overlay_comment1').css('display', 'none');
});
//$('#cro .item:first-child').addClass('active');
$('#thub_nails li').click(function () {
    var id = $(this).attr('id');
    $(".comment_sec").hide();
    $('.openchat').hide();
    $(".pro-ject-rightbar").find("#" + id).show();
    $(".pro-ject-rightbar").find('.shared_project_class').hide();
    $(".public_link").hide();
    $(".shared_draft_class").hide();
    $('#people_share').attr('data-draftid',id)
    $(".pro-ject-rightbar").find('#public_'+id).show();
    $(".pro-ject-rightbar").find('.shared_draft_user_'+id).show();
    
});
var total_design1 = <?php echo sizeof($designer_file); ?>;
var ind_id = $('.<?php echo $main_id; ?>').index();
$(".pagin-one").html(ind_id + 1 + " of " + total_design1);
setTimeout(function () {
    $('#thub_nails .<?php echo $main_id; ?>').click();
}, 200);

function send_comment_without_img(request_id, reciever_type, parent_id, replyfrom, requstfileid) {
        //console.log(parent_id);
        var sender_type = '<?php echo $_SESSION['role'] ?>';
        var sender_id = '<?php echo $_SESSION['user_id'] ?>';
        var reciever_id = '<?php echo $request[0]['customer_id'] ?>';
        if (parent_id == 0) {
            var text_id = '.abc_' + request_id;
        } else if (replyfrom == 'rightreply') {
            //console.log(requstfileid);
            var text_id = '#text2_' + requstfileid;
        } else if (replyfrom == 'appendleft') {
            //console.log('appendleft');
            var text_id = '#text3_' + requstfileid;
        } else {
            var text_id = '#text1_' + requstfileid;
        }
        var message = $(text_id).val();
        var verified_by_admin = "1";
        var customer_name = '<?php echo $_SESSION['first_name'] ?>';
        var designer_img = '<?php echo $_SESSION['designerpic'] ?>';
        if (message != "") {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>admin/dashboard/send_message",
                data: {"request_file_id": request_id,
                "sender_role": sender_type,
                "sender_id": sender_id,
                "receiver_role": reciever_type,
                "receiver_id": reciever_id,
                "message": message,
                "customer_name": customer_name,
                "parent_id": parent_id,
                "admin_seen": 1,
            },
            success: function (data) {
                    //console.log(data);
                    var left = 'left';
                    var right = 'right';
                    var appendright = 'rightreply';
                    $('.sendtext').val("");
                    if (parent_id == 0) {
                        $(".messagediv_" + request_id + "").prepend('<li><div class="comment-box-es new-patrn"><div class="comments_link"><div class="cmmbx-row"><div class="all-sub-outer"><div class="cmmbx-col left "><div class="clickable" id=' + data + '><div class="cmmbxcir"><figure class="pro-circle-img-xxs"><img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>' + designer_img + '" class="mCS_img_loaded"></figure></div><div class="cmmbx-col"><div class="ryt_header"><span class="cmmbxtxt">' + customer_name + ' </span><span class="kevtxt">Just Now</span></div><pre><p class="distxt-cmm distxt-cmm_ryt_' + data + '">' + message + ' </p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="replyButton(' + data + ', true)" class="reply_to_' + data + '"><i class="fas fa-reply-all"></i></a><a href="javascript:void(0)" onclick="editMessages(' + data + ', true)" class="edit_to_' + data + '"><i class="fas fa-pencil-alt"></i></a><a href="javascrip:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="far fa-trash-alt"></i></a></div></div></div><div class="msgk-mn-edit comment-submit_ryt_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editryt_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + right + '\')" data-id="' + data + '">Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div><div class="closeoncross reply_msg_ryt_' + data + '" style="display:none;"><form action="" method="post"><div class="cmmtype-row" onclick="wideText(this)"><textarea class="pstcmm replying sendtext text1_' + request_id + '" id="text2_' + data + '" placeholder="Post Comment" autocomplete="off" data-reqid="' + request_id + '"></textarea><span class="cmmsend"><a class="cmmsendbtn send_comment_without_img send_' + request_id + '" onclick="send_comment_without_img(' + request_id + ',\'' + sender_type + '\',\'' + data + '\',\'' + appendright + '\',\'' + data + '\');"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive"></a></span></div></form></div></div></div></div></div></li>');
                    } else {
                        $('.reply_msg_' + parent_id).before('<div class="cmmbx-col left reply reply_' + data + '"><div class="cmmbxcir"><figure class="pro-circle-img-xxs t5"><img src="' + designer_img + '" class="mCS_img_loaded"></figure></div><div class="msg_sect"><div class="name_edit_del"><span class="cmmbxtxt">' + customer_name + '</span><pre><p class="distxt-cmm distxt-cmm_ryt_' + data + '">' + message + '</p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="editMessages(' + data + ', false)" class="edit_to_' + data + '"><i class="fas fa-pencil-alt"></i></a><a href="javascript:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="far fa-trash-alt"></i></a></div><div class="msgk-mn-edit comment-submit_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editdot_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + left + '\')" data-id=' + data + '>Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div></div></div>');
                        $('.reply_msg_ryt_' + parent_id).before('<div class="cmmbx-col left reply reply_' + data + '"><div class="cmmbxcir"><figure class="pro-circle-img-xxs t5"><img src="' + designer_img + '" class="mCS_img_loaded"></figure></div><div class="msg_sect"><div class="name_edit_del"><span class="cmmbxtxt">' + customer_name + '</span><pre><p class="distxt-cmm distxt-cmm_ryt_' + data + '">' + message + '</p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="editMessages(' + data + ', false)" class="edit_to_' + data + '"><i class="fas fa-pencil-alt"></i></a><a href="javascript:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="far fa-trash-alt"></i></a></div><div class="msgk-mn-edit comment-submit_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editdot_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + left + '\')" data-id=' + data + '>Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div></div></div>');
                        //$('.dottt_' + parent_id).append('<div class="cmmbx-col left"><div class="cmmbxcir"><figure class="pro-circle-img-xxs t5"><img src="' + designer_img + '" class="mCS_img_loaded"></figure></div><div class="msg_sect"><div class="name_edit_del"><span class="cmmbxtxt">' + customer_name + '</span><pre><p class="distxt-cmm_ryt_' + data + '">' + message + '</p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="editMessages(' + data + ', false)" class="edit_to_' + data + '"><i class="fas fa-pencil-alt"></i></a><a href="javascrip:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="far fa-trash-alt"></i></a></div><div class="comment-submit_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editdot_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + left + '\')" data-id=' + data + '>Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div></div></div>');
                        $('.reply_msg_ryt_' + parent_id).css('display', 'none');
                        $('.reply_to_ryt_' + parent_id).css('display', 'inline-block');
                    }
                },
            });
}
}

$('.send_request_img_chat').click(function (event) {
    event.stopPropagation();
    var request_file_id = $(this).attr("data-fileid");
    var sender_role = $(this).attr("data-senderrole");
    var sender_id = $(this).attr("data-senderid");
    var receiver_role = $(this).attr("data-receiverrole");
    var receiver_id = $(this).attr("data-receiverid");
    var text_id = 'text_' + request_file_id;
    var message = $('.' + text_id).val();
    var customer_name = $(this).attr("data-customername");
    var xco = $(this).attr("data-xco");
    var yco = $(this).attr("data-yco");
    var designer_img = $(this).data("designerpic");
    if (message != "") {
         $(this).prop('disabled', true);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>admin/dashboard/send_message",
            data: {"request_file_id": request_file_id,
                "sender_role": sender_role,
                "sender_id": sender_id,
                "receiver_role": receiver_role,
                "receiver_id": receiver_id,
                "message": message,
                "customer_name": customer_name,
                "xco": xco,
                "yco": yco,
                "admin_seen": 1,
            },
            success: function (data) {
            $('.send_request_img_chat').prop('disabled', false);
            if (xco != '') {
                var li_class = 'dotlist_' + request_file_id + ' ';
                li_class += 'reply_' + data;
            } else {
                li_class = '';
            }
            var numItems = $('.dotlist_' + request_file_id + '').length;
            var numDots = numItems + 1;
            var left = 'left';
            var appendleft = 'appendleft';
            var appendright = 'rightreply';
            var right = 'right';
            var left_cord = $('.openchat').css('left');
            var top_cord = $('.openchat').css('top');
                $('.text_' + request_file_id).val("");
                $(".messagediv_" + request_file_id + "").prepend('<li class="' + li_class + '"><div class="comment-box-es new-patrn"><div class="cmmbx-row"><div class="all-sub-outer"><div class="cmmbx-col left"><div class="cmmbxcir"><figure class="pro-circle-img-xxs"><img src="' + designer_img + '" class="mCS_img_loaded"></figure><span class="cmmbxonline" onclick="show_customer_chat('+data+',event,$(this))"><span class="numberdot" id="dotnum_' + request_file_id + '">' + numDots + '</span></span></div><div class="cmmbx-col"><div class="ryt_header"><span class="cmmbxtxt test2">' + customer_name + ' </span><span class="kevtxt">Just Now</span></div><pre><p class="distxt-cmm distxt-cmm_ryt_' + data + '">' + message + ' </p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="replyButton(' + data + ',true)" class="reply_to_ryt_' + data + '"><i class="fas fa-reply-all"></i></a><a href="javascript:void(0)" onclick="editMessages(' + data + ',true)" class="edit_to_ryt_' + data + '"><i class="fas fa-pen"></i></a><a href="javascrip:void(0)" onclick="deleteMsg(' + data + ')" class="del_msg"><i class="fas fa-trash"></i></a></div></div><div class="msgk-mn-edit comment-submit_ryt_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editryt_' + data + '">' + message + '</textarea><a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + right + '\')" data-id=' + data + '>Save</a><a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div></div></div><div class="closeoncross reply_msg_ryt_' + data + '" style="display:none"><form action="" method="post"><div class="cmmtype-row" onclick="wideText(this)"><textarea class="pstcmm sendtext text1_' + request_file_id + '" id="text2_' + data + '" placeholder="Post Comment" autocomplete="off" data-reqid="' + request_file_id + '"></textarea><span class="cmmsend"></span><a class="cmmsendbtn send_comment_without_img send_' + request_file_id + '" onclick="send_comment_without_img(' + request_file_id + ',\'' + sender_role + '\',\'' + data + '\',\'' + appendright + '\',\'' + data + '\');"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive"></a></div></form></div></div></div></div></div></li>');
                $('.appenddot_' + request_file_id).append('<div id="' + data + '" class="abcabcabc" style="position:absolute;left:' + left_cord + ';top:' + top_cord + ';width:30px;z-index:99999999;height:30px;" onclick="show_customer_chat(' + data + ', event,$(this));"><div class="customer_chatimage1 beatHeart customer_chatimage' + data + '" ><span class="numberdot" id="dotnum_' + request_file_id + '">' + numDots + '</span></div><div style="display:none;" class="customer_chatimage2 customer_chatimage' + data + '"></div></div>');
                $('.appenddot_' + request_file_id).append('<div class="customer_chat customer_chat' + data + '" style="position: absolute; left: ' + xco + '; top: ' + yco + '; padding: 5px; border-radius: 5px; display: none;"><div class="posone2" style="z-index: 99999999999999999999999;top:0px; "><div class="posonclick-add"><div class="posone2abs arrowdowns" onclick="return avoidClick(event);"><a href="javascrip:void(0)" onclick="return closeChatBox(event);" class="crossblink">×</a><div class="comment-box-es new-patrn two-can dotreply dottt_' + data + '"><div class="cmmbx-col left reply_' + data + '"><div class="cmmbxcir"><figure class="pro-circle-img-xxs t4"><img src="' + designer_img + '"></figure></div><div class="new_msg_des"><div class="ryt_header"><span class="cmmbxtxt">' + customer_name + '</span><span class="kevtxt">just now</span></div><pre><p class="distxt-cmm distxt-cmm_' + data + '">' + message + '</pre><div class="edit_del"><a href="javascript:void(0)" onclick="replyButton(' + data + ',false)" class="edit_to_' + data + '"><i class="fas fa-reply-all"></i></a><a href="javascript:void(0)" onclick="editMessages(' + data + ',false)" class="edit_to_' + data + '"><i class="fas fa-pen"></i></a><a href="javascrip:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="fas fa-trash"></i></a></div><div class="msgk-mn-edit comment-submit_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editdot_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + left + '\')" data-id="' + data + '" >Save</a><a href="javascript:void(0)" class="cancel" onclick="cancelButton(' + data + ')">Cancel</a></p></form></div></div></div><div class="closeoncross reply_msg_' + data + '" style="display:none"><form action="" method="post"><div class="cmmtype-row wide_text" onclick="wideText(this)"><textarea class="pstcmm sendtext text1_" id="text3_' + data + '" Placeholder="Post Comment" autocomplete="off"></textarea><span class="cmmsend"><a class="cmmsendbtn send_comment_without_img" onclick="send_comment_without_img(' + request_file_id + ',\'' + receiver_role + '\',\'' + data + '\',\'' + appendleft + '\',\'' + data + '\');"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive"></a></span></div></form></div></div></div></div></div></div>');
                $(".mycancel").remove();
                $('.openchat').hide();
            }
        });
    }
});

$(document).ready(function () {
    if ($(window).width() <= '767') {
        $('.remove_dot').css('display', 'none');
    }
    $dataid=  $(".slick_thumbs li").find("figure.active").parents("li").attr('id');
    $('.customer_loader').css('display','block');
    $('.slid').css('display','none');
    $('#thub_nails li figure').removeClass('active');
    $('#thub_nails .<?php echo $main_id; ?> figure').addClass('active');
//    console.log("$dataid",$dataid);
     /** review feedback js**/
    $(".f_revw_feedback").attr("id","f_revw_feedback_"+$dataid);
    $(".f_revw_feedback").attr("data-id",$dataid);
    
    $('.response_request').click(function () {
        var btn = this;
        var parent = $(this).closest('.comment_sec').attr('id');
        var fileid = $(this).attr('data-fileid');
        var value = $(this).attr("data-status");
        var file = $(this).attr('data-file');
        var request_id = $(this).attr('data-requestid');
        var approve_date = $(this).attr('data-approve-date');
         var qUestionCollection = $("#collectionFeedback-form").serializeArray();
         $(".review_draftid").val(fileid); 
         $(this).addClass("loading");
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>admin/dashboard/request_response",
            dataType: "json",
            data: {"fileid": fileid,
                "value": value,
                "request_id": request_id,
                "form-input":qUestionCollection
            },
            success: function (data) {
              $("#collectionFeedback").html('');
              $(this).removeClass("loading");
                setTimeout(function() {
                $(this).addClass("hide-loading");
               }, 1000);
                 $(this).removeClass("loading");
                  $(this).removeClass("hide-loading");
                if (data.success == true) {
                    if (value == "customerreview") {
                        $('.butt_' + fileid).find('.app_quanlity_psd').css('display', 'block');
                        $(btn).parent().css('display', 'none');
                        $(btn).parent().next('.approved').css('display', 'block');
                        $(btn).parent().prev('.approvedate').html("<p class='dateApp'>" + data.modified + "</p>");
                    } else if (value == "Reject") {
                        $('.butt_' + fileid).find('.rej_desgn_prjct').css('display', 'block');
                        $(btn).parent().css('display', 'none');
                        $(btn).parent().siblings().eq(1).find('h3').css('display', 'block');
                        $("#" + fileid + " .overlay_comment").remove();
                    }
                    $(".review_draftid").val(fileid);
                    $(".review_reqid").val(request_id);
                    //$("#f_reviewpopup").modal("show");
                     //location.reload(); 
                    //window.location.reload();
                }
            }
        });
    });
    
   


    // Slick Slider script Start here 
    $('#changepass').click(function (e) {
        e.preventDefault();
    });

});

    $(document).on('click','.slick_thumbs ul li',function(e){
    var dataId = $(this).attr('id');
    var Outerdivwidth = $('.outer-post').width();
    var Outerdivheight = $('.outer-post').height();
    var outer_main = $('.mydivposition_'+dataId);
    var main_img = $(this).find('img');
    var actualimgWidth = main_img.get(0).naturalWidth;
    var actualimgHeight = main_img.get(0).naturalHeight;
    var outer_main_height = outer_main.height();
    var calcval = (Outerdivheight - outer_main_height)/2;
    if(Outerdivheight > outer_main_height){
        $(outer_main).css('margin-top',calcval+"px");
    }
    if(actualimgWidth > Outerdivwidth || actualimgHeight > Outerdivheight) {
        $('#zoomIn').css('cursor', 'pointer');
        $('#zoomIn').css('opacity', '1');
        $('#zoomOut').prop('disabled', false);
        $('#zoomIn').prop('disabled', false);
    }else{
        $('#zoomOut').css('cursor', 'no-drop');
        $('#zoomOut').css('opacity', '0.5');
        $('#zoomIn').css('cursor', 'no-drop');
        $('#zoomIn').css('opacity', '0.5');
        $('#zoomOut').prop('disabled', true);
        $('#zoomIn').prop('disabled', true);
    }
    $('#zoomOut').attr('data-zoomoutcount','0');
    $('#zoomIn').attr('data-count','0');
    $('.slick_thumbs ul li').css('pointer-events','auto');
    
    if(dataId){
        $('.hdr-buttons').css('display','none')
        $('.butt_'+dataId).css('display','block');
    }
    var url = $(this).find('img').attr('src');
    var parts = url.split("/");
    var last_part = parts[parts.length-1];
    $('.file_title').html(last_part);
//    $('.loading_' + dataId).css("display", "flex");
//    $('.slid').css("display", "none");
//    $('.abcabcabc').css("display", "none");
    $('.abcabcabc').css("display", "none");
    $('.customer_loader').css('display','block');
    $('.slid').css('display','none');
//    $('.slid img').css({
//            "-webkit-filter": "blur(8px)",
//            "filter": "blur(8px)"
//    });
    setTimeout(function () {
        $('.loading_' + dataId).css("display", "none");
//        $('.slid img').css({
//            "-webkit-filter": "blur(0px)",
//            "filter": "blur(0px)"
//        });
        $('.slick_thumbs ul li').css('pointer-events','auto');
        $('.abcabcabc').css("display", "block");
        $('.customer_loader').css('display','none');
        $('.slid').css('display','block');
        refreshDotsActiveTab();
    }, 2000);
    $(".slick_thumbs ul li").find('figure').removeClass('active');
    $(this).find('figure').addClass('active');
    var thumb_index = $(this).index();
    $('ul.slick-dots').find('li:eq(' + thumb_index + ')').trigger('click');
    var dot_length = $('ul.slick-dots').children().length;
    var count_slid = thumb_index;
    var current_design = count_slid;
    var total_design = <?php echo sizeof($designer_file); ?>;
    current_design += 1;
    if (current_design > total_design) {
        current_design = 1;
    }
    $(".pagin-one").html(current_design + " of " + total_design);
    $(".response_request").attr("data-fileid",dataId);
    $("#draft-id").val(dataId); 
});

function showclicktocomment(event, id) {
    if ($(window).width() <= '767') {
        return false;
    }
    var position = $("#mydivposition_" + id).offset();
    var posX = position.left;
    var posY = position.top;
    var left = event.pageX - posX;
    var top = event.pageY - posY + +10;
    var window_height = $(window).height();
    var window_width = $("#mydivposition_" + id).width();
    if (top > window_height - 100) {
        top = top - 40;
    }
    if (left > window_width - 200) {
        left = left - 130;
    }
    if ($(event.target).is(".posone2abs") || $(event.target).parents('div.posone2abs').length
            || $(event.target).is(".customer_chatimage1") || $(event.target).is(".customer_chatimage2")) {
        return false;
    }
    /*
     if( $(event.target).is(".customer_chatimage1") || $(event.target).is(".posone2abs")
     || $(event.target).is(".comment-box-es") || $(event.target).is(".cmmbx-col")
     || $(event.target).is(".space-a") || $(event.target).is(".distxt-cmm") || $(event.target).is(".crossblink")) {
     return false;
     }
     */
    $('.showclicktocomment').remove();
    if ($(".openchat").css("display") != "block") {
        $('.openchat').after('<div class="showclicktocomment" style="left: ' + left + 'px; top: ' + top + 'px; display: none;">\Click To Comment\</div>');
        $("#mydivposition_" + id + ' .showclicktocomment').show();
        //event.stopPropaganation();
        // event.preventDefault();
        return false;
    }

}
function hideclicktocomment()
{
    $('.showclicktocomment').remove();
}
function closeChatBox(e) {
    $("#OpenpopUp").attr("data-open",0);
    $(".leader-line").remove(); 
    
    //var data = $(this).attr('data-close');
    //$('.customer_chat customer_chat'+data).find('posone2abs').hide();
    $('.customer_chat').hide();
    e.preventDefault();
    e.stopPropagation();
    return false;
}
function avoidClick(e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
}

function show_customer_chat(id, event,click) {

    $this = click; 
    var datadot=  $this.attr("data-dot"); 
     $(".arrowdowns").removeClass("hidenotch");
      
    if(datadot > 0 || $this.find("span").hasClass("numberdot")){
       $("#OpenpopUp").attr("data-open", "2");
    }
    $(".posone1").removeAttr("style");
    $(".posone2abs").removeAttr("style");
    $(".leader-line").remove();
    
    targetid = id; 
    
    var left_cord = $('#' + id).css('left');
    var top_cord = $('#' + id).css('top');
    $('.customer_chat' + id).css('left', left_cord);
    $('.customer_chat' + id).css('top', top_cord);
    var topPosition = $("#" + id + "").css('top');
    var newtopPosition = topPosition.substring(0, topPosition.length - 2); //string 800
    newtopPosition = parseFloat(newtopPosition) || 0;
    if (newtopPosition < 200) {
        $('.customer_chat' + id + ' .posone2abs').toggleClass('topposition');
    } else {
        $('.customer_chat' + id + ' .posone2abs').removeClass('topposition');
    }


   // $(".customer_chat" + id).show().siblings('.customer_chat').hide();
   // $(".customer_chat").hide();
   // $(".customer_chat" + id).show();
    setTimeout(function () {
        $('.myallremove').remove();
        $(".openchat").hide();
        $(".customer_chat").each(function () {
            if ($(this).html() == "<label></label>") {
                $(this).remove();
            }
            if ($(this).text() == "") {
                $(this).remove();
            }
        })
//        $(".customer_chat" + id).show();
//        $(".customer_chatimage" + id).toggle();
        
        $('.showclicktocomment').remove();
        $('.mycancel').remove();
        event.preventDefault();
    }, 1);
    
    $(".customer_chat" + id).show().siblings('.customer_chat').hide();
    var Attrid = $("#project-pageinfo-id").attr("data-id"); 
    $(".leader-line").remove(); 
    DrawALine(targetid,"dotnum_"+targetid+"_Next"); 
    return false;
}

$('.mycancellable').click(function (event) {
    event.stopPropagation();
    $('.openchat').hide();
    var mycancel = $(this).attr('data-cancel');
    $('.mydiv' + mycancel).hide();
});

function mycancellable(id)
{
    $('.openchat').hide();
    var mycancel = $(".mycancellable" + id).attr('data-cancel');
    $('.mydiv' + mycancel).hide();
}
$('.text_write').click(function (event) {
    event.stopPropagation();
});


var total_design = <?php echo sizeof($designer_file); ?>;
var current_design = 1;
//$(".next").click(function () {
//    var index = $('#thub_nails li figure.active').closest('.active1').index();
//    if (index + 1 == total_design) {
//        $('#thub_nails li:first-child').click();
//    } else {
//        $('#thub_nails li figure.active').closest('.active1').next().click();
//    }
//    $('.slick-current .mydivposition .slid img').css('transform', 'scale(1)');
//    setTimeout(function () {
//        var chat_id = $('#cro .active').attr('id');
//        $('.comment_sec').css('display', 'none');
//        $('#comment_list_all #' + chat_id).css('display', 'block');
//        $(".messagediv_" + chat_id).stop().animate({
//            scrollTop: $(".messagediv_" + chat_id)[0].scrollHeight
//        }, 1);
//    }, 1000);
//});
//$(".prev").click(function () {
//    var index = $('#thub_nails li figure.active').closest('.active1').index();
//    if (index == 0) {
//        $('#thub_nails li:last-child').click();
//    } else {
//        $('#thub_nails li figure.active').closest('.active1').prev().click();
//    }
//    $('.slick-current .mydivposition .slid img').css('transform', 'scale(1)');
//    setTimeout(function () {
//        var chat_id = $('#cro .active').attr('id');
//        $('.comment_sec').css('display', 'none');
//        $('#comment_list_all #' + chat_id).css('display', 'block');
//    }, 1000);
//});

$(document).on('change', '.switch-custom input[type="checkbox"]', function () {
    var status = $(this).prop('checked');
    var data_id = $(this).attr('data-cid');
    var ischecked = ($(this).is(':checked')) ? 1 : 0;
    $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>admin/dashboard/change_can_trialdownload",
        data: {status: status, data_id: data_id},
        success: function (data) {
        }
    });
});

function EditClick(data_id, flag) {
        if (flag == 'left') {
            var updated_msg = $('#editdot_' + data_id).val();
        } else {
            var updated_msg = $('#editryt_' + data_id).val();
        }
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: "<?php echo base_url(); ?>admin/dashboard/editComment",
            data: {"request_file_id": data_id,
            "updated_msg": updated_msg,
        },
        success: function (data) {
            if (flag == 'left') {
                $('.distxt-cmm_' + data_id).css('display', 'block');
                $('.distxt-cmm_' + data_id).text(data.message);
                $('.distxt-cmm_ryt_' + data_id).text(data.message);
                $('.comment-submit_' + data_id).css('display', 'none');
                $('.edit_to_' + data_id).css('display', 'inline-block');
            } else {
                $('.distxt-cmm_' + data_id).text(data.message);
                $('.distxt-cmm_ryt_' + data_id).css('display', 'block');
                $('.distxt-cmm_ryt_' + data_id).text(data.message);
                $('.edit_to_ryt_' + data_id).css('display', 'inline-block');
                $('.comment-submit_ryt_' + data_id).css('display', 'none');
            }
        },
    });
    }
    
     function deleteMsg(id) {
        var cnfrm = cnfrm_delete();
        var mainid = '<?php echo $main_id; ?>';
        var numItems = $('.dotlist_' + mainid + '').length;
//        console.log("mainid", mainid);
//        console.log("numItems", numItems);
if (cnfrm == true) {
    $.ajax({
        type: "POST",
        dataType: "JSON",
        url: "<?php echo base_url(); ?>admin/dashboard/deleteRequestmsg",
        data: {"request_file_id": id, },
        success: function (data) {
            if (data.status == 'success') {
                var outer_main = $('#cro .active .mydivposition');
                outer_main.find('.abcabcabc').each(function () {
                    var deldot = $('#' + id).data('dot');
                    var currentdots = $(this).data('dot');
                    if (currentdots > deldot) {
                        $(this).data('dot', currentdots - 1);
                        $(this).find('.numberdot').text(currentdots - 1);
                    }
                });

                $('.dotlist_' + mainid).each(function () {
                    var deldot = $('#' + id).data('dot');
                    var currentdots = $(this).find('.clickable').data('dot');
                    // console.log("currentdots", currentdots);
                    // console.log("deldot", deldot);
                    if (currentdots > deldot) {
                        $(this).data('dot', currentdots - 1);
                        $(this).find('.numberdot').text(currentdots - 1);
                    }
                });
                $('#' + id).remove();
                $('.customer_chat' + id).remove();
                $('#' + id).closest('li').remove();
                $('.reply_' + id).remove();
            }
        },
    });
}
}
 function cancelButton(cancelid) {
        $('.comment-submit_' + cancelid).css('display', 'none');
        $('.distxt-cmm_' + cancelid).css('display', 'block');
        $('.edit_to_' + cancelid).css('display', 'inline-block');
        $('.comment-submit_ryt_' + cancelid).css('display', 'none');
        $('.distxt-cmm_ryt_' + cancelid).css('display', 'block');
        $('.edit_to_ryt_' + cancelid).css('display', 'inline-block');
    }
</script>
<!--  for  Question Feedback  -->
 <script>
    $(document).ready(function(){
       $("#next_project").click(function(){

        if (jQuery('#sour').val() == '') {
             $("#sour").parents(".file-drop-area").addClass("red-alert"); 
             $('#warningError').fadeIn().css("color","red").html("Please Attach Source File");
             setTimeout(function(){ $('#warningError').fadeOut().text('');  }, 3000);
             
         }else{
             if (jQuery('#preview_image').val() == '') {
                $("#preview_image").parents(".file-drop-area").addClass("red-alert"); 
                $('#warningError').fadeIn().css("color","red").html("Please Attach Preview File");
                setTimeout(function(){ $('#warningError').fadeOut().text('');  }, 3000);
             }else{
                var project_title,color_pref,category=[],brand_profile,Attachments;
                project_title =  $(".project-title").text();
                color_pref    =  $(".color-pref").text();
                $(".category span").each(function(k,v){
                       category.push(v);   
                       $(".ct-info p").html(category);
                });  
                brand_profile =  $(".brand-profile").html();
                if(brand_profile == " "){
                    brand_profile = "No brand profile selected"; 
                }
                $(".attch-icon .img-link").each(function(k,v){
                             var imageArr = ['jpeg', 'gif', 'bmp', 'png',"jpg"];
                             var url = $(this).attr("href");
                             console.log("vs",url); 
                             var baseName = getPageName(url);
                             ext = baseName.substring(baseName.lastIndexOf('.') + 1);
                              
                             if (jQuery.inArray(ext, imageArr)!='-1') {
                                $(".atm-info").append("<div class='images-gallary' data-fancybox  href='"+$(this).attr("href")+"' ><img src='"+$(this).attr("href")+"'></div>");
                                } else {
                                    $(".atm-info").append(v); 
                                }

         
                });  

                

                $(".pt-info span").text(project_title);
                $(".cp-info span").text(color_pref);
                $(".bp-info").html(brand_profile);

                $("#selectimagespopup").hide();
                $("#project-prew-detail").show(); 
                var preview_image = $(".preview_file").attr("src"); 
                $(".preview-image").find("img").attr("src",preview_image); 
                $(".overlay-icon").attr("href",preview_image); 
             }
         }
 
       });

       $("#back_to_draft a").click(function(){
        $("#project-prew-detail").hide();
        $("#selectimagespopup").show();
       }); 
       $(".like-dislike a").click(function(){
       var StroeArrA= {}; 
       var Counter = $("#Counter").attr("data-slide");
            
            var totalQuestion;
             $(this).siblings().removeClass("a-active").find("i").removeClass("fas").addClass("far");    
             $(this).addClass("a-active").find("i").removeClass("far").addClass("fas");
             var like_Check = $(this).attr("data-like"); 
             var lastCheck =  $(".review-option-outer").last().hasClass("active");

             $(".review-option-main").find(".active").removeClass("active").addClass("hide").fadeIn("slow").next(".review-option-outer").addClass("active").fadeIn("slow").removeClass("hide");
                 var quesNumber =  $(this).data("count");
                totalQuestion = $("#totalQuestion").data("last");
              StroeArrA[quesNumber]  = like_Check; 
              $("#collection-q"+quesNumber).remove(); 
              $("#collectionFeedback").append("<input type='hidden' class='updating' id='collection-q"+quesNumber+"' name='feedbackColl["+quesNumber+"]' value='"+like_Check+"'>");
              $("#Counter").attr("data-slide",quesNumber); 
              $(".backToprevQuestion").show();
               if(quesNumber == totalQuestion){   
                  $(".review-option-main").hide();  
                  var NumberArr = [];
                  $( ".updating" ).each(function( index,val ) {
                    NumberArr.push($( this ).val()); 
                });
                   

                  if(jQuery.inArray("2", NumberArr) !== -1){
                    
                    $("#warningMsg").css('color',"#fff").text("As per your answers, it seems that you have missed some instructions from customer"); 
                    $("#sub-button").removeClass("load_more").val("Approve Anyway");
                    $("#sub-button").addClass("default-btn");
                    $("#cancel-button").addClass("load_more").val("Reject");
                    $("#cancel-button").removeClass("default-btn");
                     $(".submit-draft").fadeIn(); 
                   }else{
                      
                     $("#warningMsg").css('color',"#fff").text("As per answers, you have followed all the required instructions carefully, Approve the design now."); 
                    $("#sub-button").addClass("load_more").val("Approve");
                    $("#sub-button").removeClass("default-btn");
                    $("#cancel-button").removeClass("load_more").val("Reject");
                    $("#cancel-button").addClass("default-btn");
                         $(".submit-draft").fadeIn(); 
                 
                   }
               }
        });
      
    });
  
    $(".backToprevQuestion a").click(function(){
        var Counter = $("#Counter").attr("data-slide");
        var newCounter = Counter - 1; 
        $("#Counter").attr("data-slide",newCounter);
        if(Counter==1){
            $(".backToprevQuestion").hide();
        }else{
            $(".backToprevQuestion").show();
        }
        $(".review-option-main").find(".active").removeClass("active").addClass("hide").fadeIn("slow").prev(".review-option-outer").addClass("active").fadeIn("slow").removeClass("hide");
    });
    
 $("#cancel-button").click(function(){
    $("#Addfiles").modal("hide");
     location.reload(); 
 }); 


 

function getPageName(url) {
    var index = url.lastIndexOf("/") + 1;
    var filenameWithExtension = url.substr(index);
    var filename = filenameWithExtension.split(".")[0]; // <-- added this line
    return filename;                                    // <-- added this line
}

function CheckForFeedBack(draftid) {
    $("#project-pageinfo-id").attr("data-id", draftid);
    $.ajax({
        type: 'POST',
        url: baseUrl+"admin/dashboard/CheckForFeedBack",
        data: {"draftid": draftid},
        dataType: "JSON",
        success: function (res) {

            if (res[0].status == "pending") {
                $("#fds1").removeClass('hide');
                $(".review-option-main").show();
                $(".like-dislike").find("a").removeClass("a-active");
                $(".project-details-bar").removeClass("hide");
            } else {
                $(".like-dislike").find("a").removeClass("a-active");
                $(".review-option-main").hide();
                $(".project-details-bar").addClass("hide");
            }
        }
    });
}

$(document).ready(function(){
       var draftid  = $("#thub_nails li").find(".imgobxbins.active").parent("li").attr("id"); 
       CheckForFeedBack(draftid);
});
$("#thub_nails li").click(function(){
    var draftid  =  $(this).attr("id"); 
  //console.log("ssasd",draftid); 
    CheckForFeedBack(draftid);
}); 
$(".nav-menu-itm").click(function(){
   $(".project-details-bar").toggleClass("hide");
   $("#2").removeClass("active"); 
});

$(document).on("click", ".f_revw_feedback", function () {
       var id = $(this).attr("data-id");
       $.ajax({
        method: 'POST',
        url: "<?php echo base_url(); ?>customer/request/get_designfeedback",
        data: {id:id,role:"admin"},
        dataType: 'json',
        success: function (response) {
            if((response.customer).length > '0'){
                var value = response.customer[0].satisfied_with_design;
                $(".review_addtional_note").val(response.customer[0].additional_notes);
                $(".review_addtional_note").prev(".label-txt").addClass("label-active");
                $("input[name=satisfied_with_design][value='"+value+"']").prop("checked",true);
                $(".design_review_form").css("display","block");
                $(".no_feedbk_cus").css("display","none");
            }else{
                $(".design_review_form").css("display","none");
                $(".no_feedbk_cus").css("display","block");
            }
            $("#qa_custmr_fdbk").modal("show");
     }
   });


   });

 $(window).on("load",function(){ 
       $dataid=  $(".slick_thumbs li").find("figure.active").parents("li").attr('id');
       $request_id=  $(".slick_thumbs li").find("figure.active").parents("li").attr('request_id');
       console.log("sasd",$dataid);
       console.log("request_id",$request_id);
       $("#posonclick-add-inner"+$dataid).find(".posone3abs").attr("id","dot-drag-"+$dataid);

 });

 $(".active1").click(function(){
        $(".leader-line").remove();
       var dataid=  $(this).attr('id');
       var req_id=  $(this).attr('request_id');
       var file_title=  $(".file_title").text();
       $("#posonclick-add-inner"+dataid).find(".posone3abs").attr("id","dot-drag-"+dataid);
        $("#OpenpopUp").attr("data-open","1");
       
   });
 $('.mycancellable').click(function () {
  $(".leader-line").remove();
  $('.openchat').hide();
  var mycancel = $(this).attr('data-cancel');
  $('.mydiv' + mycancel).hide();
});

 $(".next,.prev ").on("click",function(){
      var dataId,datatitle,reqid;
      if($(this).hasClass('prev')){
           dataId = $('.outer-post.active').prev().attr('id');
           datatitle = $('.outer-post.active').prev().data('name');
           reqid = $('.outer-post.active').prev().data('req-id');
           if(typeof dataId == 'undefined'){
               dataId = $('.outer-post:last').attr('id');
               reqid = $('.outer-post:last').data('req-id');
               datatitle = $('.outer-post:last').data('name');
           }
       }else{
           dataId = $('.outer-post.active').next().attr('id');
           datatitle = $('.outer-post.active').next().data('name');
           reqid = $('.outer-post.active').next().data('req-id');
           if(typeof dataId == 'undefined'){
               dataId = $('.outer-post:first').attr('id');
               reqid = $('.outer-post:first').data('req-id');
               datatitle = $('.outer-post:first').data('name');
           }
       }
      $("#project-pageinfo-id").attr("data-id",dataId);
      $(".leader-line").hide(); 
      $('.posone3abs').hide();
      $("#posonclick-add-inner"+dataId).find(".posone3abs").attr("id","dot-drag-"+dataId);
 });

 </script>