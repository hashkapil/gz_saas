<?php 
//echo "<pre/>";print_R($projects);
//if(isset($client_url) && $client_url != ''){
//    $comefrm = $client_url;
//}elseif(isset($designer_url) && $designer_url != ''){
//     $comefrm = $designer_url;
//}else{
//     $comefrm = '';
//}
?>
<div class="cli-ent-row tr brdr <?php if($projects['plan_color'] != ""){ echo "plancolor_".$projects['plan_color']; } ?>" id="remove_<?php echo $projects['id']; ?>">
    <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
    <div class="cli-ent-col td">
        <div class="cli-ent-xbox text-left">
            <div class="cell-row">
                <div class="cell-col">
                    <div class="sound-signal">
                        <div class="form-radion2">
                            <label class="containerr">
                                <input type="checkbox" name="project[]" class="selected_pro <?php echo $projects['addClass'];?> add_des_scrl"  value="<?php echo $projects['id']; ?>" data-id="<?php echo $projects['id']; ?>" id="<?php echo $projects['id']; ?>"/> 
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!---Start Title---->
    <div class="cli-ent-col td client-project" style="width: 30%; cursor: pointer;">
        <?php if ($projects['is_trail'] == 1 && $projects['status_admin'] == 'assign') { ?>
            <div class="promo-req inqueue"><?php echo PROMO_TEXT; ?></div>
        <?php } ?>
        <div class="cli-ent-xbox text-left">
            <div class="cell-row">
                <div class="cell-col" >
                    <div class="name-disc">
                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $projects['id'].$projects['bkurl']; ?>"><?php echo $projects['title']; ?></a>
                            <?php if ((($projects['total_chat'] + $projects['comment_count']) != 0)) { ?>
                                <span class="numcircle-box">
                                    <i class="fas fa-comment-dots"></i>
                                </span>
                            <?php } ?>
                        </h3>
                        <?php if ($projects['category_name'] != '' || $projects['subcategory_name'] != '') { ?>
                            <p class="pro-b"><span><?php echo $projects['category_name']; ?></span> <span><?php echo isset($projects['subcategory_name']) ? "> " . $projects['subcategory_name'] : ''; ?></span></p>
                        <?php } ?>
                        <?php ?>
                        <?php ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!---End Title---->

    <!---Start Client name---->
    <div class="cli-ent-col td" style="width: 10%; cursor: pointer;">
        <div class="cli-ent-xbox text-left">
            <div class="cell-row">
                <div class="cell-col">
                    <p class="text-h text-wrap" title="<?php echo $projects['customer_first_name'] . " " . $projects['customer_last_name']; ?>
                    ">
                    <?php echo $projects['customer_first_name']; ?>
                    <?php
                    if (strlen($projects['customer_last_name']) > 5) {
                       echo ucwords(substr($projects['customer_last_name'], 0, 1));
                   } else {
                       echo $projects['customer_last_name'];
                   }
                   ?>
               </p>
               <div class="sub_user_req_info">
                <p class="text-h text-wrap subUsr">
                <?php
                    if ($projects['sub_first_name'] != '') { ?>
                    <i class="fas fa-user-friends"></i>
                <?php } 
                    echo $projects['sub_first_name']; 
                    if (strlen($projects['sub_last_name']) > 5) {
                        echo ucwords(substr($projects['sub_last_name'], 0, 1));
                    } else {
                        echo $projects['sub_last_name'];
                    }
                    ?>
                </p>
                
            </div>
        </div>
    </div>

</div>
</div>
<!---End Client---->

<!---Start Designer name---->
<div class="cli-ent-col td" style="width: 10%;">
    <div class="cli-ent-xbox text-left">
        <div class="cell-row">
            <?php if ($projects['designer_id'] != "" && $projects['designer_id'] != 0) { ?>
                <div class="cell-col <?php echo $projects['id']; ?>">
                    <p class="text-h text-wrap">
                        <?php echo ((strlen($projects['designer_first_name']) < 6) ? $projects['designer_first_name'] : substr($projects['designer_first_name'],0,6)); ?>
                        <?php
                        if (strlen($projects['designer_last_name']) > 5) {
                            echo ucwords(substr($projects['designer_last_name'], 0, 1));
                        } else {
                            echo $projects['designer_last_name'];
                        }
                        ?>
                    </p>
                    <?php ?>
                </div>
            <?php } else { ?>
                <div class="cell-col <?php echo $projects['id']; ?>"><p class="text-h text-wrap"></p></div>
            <?php } ?>
        </div>
    </div>
</div>
<!---End Designer name---->

<!---Start Status---->
<div class="cli-ent-col td" style="width: 16%; cursor: pointer;">
    <?php if ($projects['status_admin'] == "active") { ?>
        <div class="cell-col col-w1">
            <p class="neft"><span class="green text-uppercase text-wrap">In-Progress</span></p>
        </div>
    <?php } elseif ($projects['status_admin'] == "disapprove" && $projects['who_reject'] == 1) { ?>
        <div class="cell-col col-w1">
            <p class="neft"><span class="red orangetext text-uppercase text-wrap">REVISION</span></p>
        </div>
    <?php } elseif ($projects['status_admin'] == "disapprove" && $projects['who_reject'] == 0) { ?>
        <div class="cell-col col-w1">
            <p class="neft"><span class="red  text-uppercase text-wrap">Quality Revision</span></p>
        </div>
    <?php } elseif ($projects['status_admin'] == 'assign') { ?>
        <p class="neft"><span class="gray text-uppercase">IN-queue</span></p>
        <div class="cell-col priority-sec"><?php echo isset($projects['priority']) ? $projects['priority'] : ''; ?></div>
    <?php } elseif ($projects['status_admin'] == 'pendingrevision') { ?>
        <p class="neft"><span class="red lightbluetext text-uppercase text-wrap">Pending Review</span></p>
    <?php } elseif($projects['status_admin'] == "checkforapprove"){ ?>
        <div class="cell-col col-w1">
           <p class="neft"><span class="red bluetext text-uppercase text-wrap">Pending-Approval</span></p>
       </div>
    <?php }elseif($projects['status_admin'] == "approved"){ ?>
        <div class="cell-col col-w1">
            <p class="neft"><span class="green text-uppercase text-wrap">COMPLETED</span></p>
        </div>
    <?php }elseif($projects['status_admin'] == 'hold'){ ?>
        <div class="cell-col col-w1">
            <p class="neft"><span class="hold-color text-uppercase text-wrap">On Hold</span></p>
        </div>
    <?php }elseif($projects['status_admin'] == 'cancel'){ ?>
        <div class="cell-col col-w1">
            <p class="neft"><span class="cancelcolor text-uppercase text-wrap">Cancelled</span></p>
        </div>
    <?php } ?>
</div>
<!---End Status---->


<!----verified tab start-->
<?php
if ($projects['verified'] == '1') {
    $class = 'verified_by_admin';
    $icon = '<i class="far fa-check-circle"></i> <span class="vrfy_txt"></span>Verified';
    $disable = '';
} elseif ($projects['verified'] == '2') {
    $class = 'verified_by_designer';
    $icon = '<i class="fas fa-check-circle"></i> <span class="vrfy_txt"></span>Verified';
    $disable = 'disabled';
} else {
    $class = '';
    $icon = '<i class="far fa-circle"></i> <span class="vrfy_txt">Verify</span>';
    $disable = '';
}
?>
<div class="cli-ent-col td" style="width: 9%;">
    <div class="cli-ent-xbox">
        <div class="cell-row">
            <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $projects['id']; ?>">
                <label>
                    <input type="checkbox" name="project" class="verified" data-pid="<?php echo $projects['id']; ?>" <?php echo ($projects['verified'] == 1 || $projects['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable; ?>/>
                    <?php echo $icon; ?>
                </label>
            </div>
        </div>
    </div>
</div>
<!----verified tab end-->

<!----Date tab start-->
<div class="cli-ent-col td" style="width: 15%;">
    <?php if ($projects['status_admin'] == 'assign') { ?>
        <div class="cli-ent-xbox">
            <?php echo date('M d, Y h:i:A', strtotime($projects['created'])); ?>
        </div>
    <?php } elseif ($projects['status_admin'] == 'active' || $projects['status_admin'] == 'disapprove') { ?>
        <?php if ($projects['is_trail'] == 1) { ?>
            <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
        <?php } ?>
        <div class="cli-ent-xbox">
            <div class="dt-expire">
                <p id="demo_<?php echo $projects['id']; ?>" class="expectd_date" data-id="<?php echo $projects['id']; ?>" data-date="<?php echo date('M d, Y h:i:A', strtotime($projects['expected'])); ?>" data-timezonedate="<?php echo $projects['usertimezone_date']; ?>"></p>
                <a href="javascript:void(0)" class="editexpected" date-expected="<?php echo date('M d, Y h:i A', strtotime($projects['expected'])); ?>" date-reqid="<?php echo $projects['id']; ?>">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/gz_icons/calebdar_edit.svg" />
                </a>
            </div>
            <?php echo date('M d, Y h:i:A', strtotime($projects['expected'])); ?>
        </div>
    <?php }elseif($projects['status_admin'] == 'pendingrevision'){ //print_R($projects);?>
        <div class="cli-ent-xbox">
            <p class="app-roved pro-a">Delivered on</p>
            <p class="space-a"></p>
            <p class="pro-b">
                <?php echo date('M d, Y h:i A', strtotime($projects['modified']));
                ?>
            </p>
        </div>
    <?php } elseif($projects['status_admin'] == 'checkforapprove'){ ?>
        <div class="cli-ent-xbox">
            <p class="app-roved pro-a">Delivered on</p>
            <p class="space-a"></p>
            <p class="pro-b">
                <?php echo date('M d, Y h:i A', strtotime($projects['reviewdate']));
                ?>
            </p>
        </div>
    <?php }elseif($projects['status_admin'] == 'approved'){ ?>
      <div class="cli-ent-xbox">
        <h3 class="app-roved green">Approved on</h3>
        <p class="space-a"></p>
        <p class="pro-b">
            <?php echo date('M d, Y h:i A', strtotime($projects['approvddate']));
            ?>
        </p>
    </div>  
<?php }elseif($projects['status_admin'] == 'hold'){ ?>
    <div class="cli-ent-xbox">
        <h3 class="app-roved holdcolors">Hold on</h3>
        <p class="space-a"></p>
        <p class="pro-b">
            <?php echo date('M d, Y h:i A', strtotime($projects['modified']));
            ?>
        </p>
    </div>
<?php }elseif($projects['status_admin'] == 'cancel'){ ?>
    <div class="cli-ent-xbox">
        <h3 class="app-roved holdcolors">Cancelled on</h3>
        <p class="space-a"></p>
        <p class="pro-b">
            <?php echo date('M d, Y h:i A', strtotime($projects['modified']));
            ?>
        </p>
    </div>
<?php } ?>
</div>
<!----Date tab end-->

<!----Action tab start-->
<div class="cli-ent-col td" style="width: 10%; cursor: pointer;">
    <div class="cli-ent-xbox action-per">
        <a href="javascript:void(0)"><i class="icon-gz_edit_icon"></i></a>
        <a href="javascript:void(0)" class='delete_cust_request' data-req_id='<?php echo $projects['id'];?>'><i class="icon-gz_delete_icon"></i></a>
    </div>
</div>
<!----Action tab End-->

</div>



