<?php
if (isset($data[0]['first_name'])) {
    $first_name = $data[0]['first_name'];
}
if (isset($data[0]['last_name'])) {
    $last_name = $data[0]['last_name'];
}
?>
<section class="con-b">
    <div class="container-fluid">
        <?php if ($this->session->flashdata('message_error')) { ?>
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c"><?php echo $this->session->flashdata('message_error'); ?></ps>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('message_success')) { ?>
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c"><?php echo $this->session->flashdata('message_success'); ?></p>
                </div>
            <?php } ?>
            <div class="flex-this">
                <h2 class="main_page_heading"><?php echo $first_name . ' ' . $last_name; ?></h2>
                <div class="header_searchbtn">
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-4">
                    <div class="client-profile">
                        <div class="profile-pic">
                            <figure class="setimg-box33 big-cirlce">
                                <img src="<?php echo $data[0]['profile_picture']; ?>" class="img-responsive telset33">
                            </figure>
                            <a class="setimg-blog33" href="javascript:void(0)">
                                <span class="setimgblogcaps profile_pic_sec_btn">
                                    <i class="icon-gz_edit_icon"></i>
                                </span>
                            </a>
                        </div>
                        <div class="edit_profile_pic_sec"  style="display: none;">
<!--                            <figure class="setimg-box33 big-cirlce">
                                <img src="<?php echo $data[0]['profile_picture']; ?>" class="img-responsive telset33">
                            </figure>-->
                            <form action="<?php echo base_url(); ?>admin/dashboard/edit_profile_image_form" method="post" enctype="multipart/form-data">
                                <div class="settrow">
                                    <div class="settcol right">
                                        <p class="btn-x"><button type="submit" class="btn-e" name="submit_pic">Save</button></p> 
                                    </div>
                                </div>
                                <div class="setimg-row33">
                                    <div class="imagemain2" style="padding-bottom: 20px; display: none;">
                                        <input type="file" onChange="validateAndUpload(this);" class="form-control dropify waves-effect waves-button" name="profile" style="display:none;" id="inFile"/>
                                    </div>
                                    <div class="imagemain">
                                        <figure class="setimg-box33 big-cirlce">
                                            <img src="<?php echo $data[0]['profile_picture']; ?>" class="img-responsive telset33">
                                            <a class="setimg-blog33" href="#" onclick="$('.dropify').click();">
                                                <span class="setimgblogcaps">
                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-camera.png" class="img-responsive">
                                                   
                                                </span>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="imagemain3" style="display: none;">
                                        <figure class="setimg-box33 big-cirlce">
                                            <img src="" id="image3" class="img-responsive telset33">
                                            <a class="setimg-blog33" href="#" onclick="$('.dropify').click();">
                                                <span class="setimgblogcaps">
                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-camera.png" class="img-responsive">
                                                   
                                                </span>
                                            </a>
                                        </figure>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="main-discp">
                            <h3>Admin</h3>
                            <h2>
                                <?php echo $first_name . ' ' . $last_name; ?>
                            </h2>
                            <p>
                                <?php if (isset($data[0]['notification_email'])): ?>
                                    <?php echo $data[0]['notification_email']; ?>
                                <?php endif ?>
                            </p>
                            <p class="chngpasstext">
                                <a href="" class="change_pass" name="savebtn" value="Change Password" data-toggle="modal" data-target="#myModal"  ><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/change-password.png" class="img-responsive"> <span>Change Password</span></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="detailed-item">
                            <ul>
                                <li><label for=""><?php echo ($data[0]['company_name'] != '') ? 'Company Name' : ''; ?></label><span><?php echo $data[0]['company_name']; ?></span></li>
                                <li><label for=""><?php echo ($data[0]['phone'] != '') ? 'Phone' : ''; ?></label><span><?php echo $data[0]['phone']; ?></span></li>
                                <li><label for=""><?php echo ($data[0]['address_line_1'] != '') ? 'Address' : ''; ?></label><span><?php echo $data[0]['address_line_1']; ?></span></li>
                                <li><label for=""><?php echo ($data[0]['city'] != '') ? 'City' : ''; ?></label><span><?php echo $data[0]['city']; ?></span></li>
                                <li><label for=""><?php echo ($data[0]['state'] != '') ? 'State' : ''; ?></label><span><?php echo $data[0]['state']; ?></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row plan_row">
                    <div class="col-md-8 col-sm-12">
                        <ul class="list-header-blog client-inform" id="tab_menu">
                            <li class="active" id="1">
                                <a data-toggle="tab" href="#myprofile" role="tab">My Profile</a>
                            </li>         
<!--                    <li id="2">
                        <a data-toggle="tab" href="#billing" role="tab">Billing and Subscription </a>
                    </li> -->
                    <li id="3">
                        <a data-toggle="tab" href="#enableEmail" role="tab">Enable emails </a>
                    </li> 
                </ul>
            </div>
        </div>
        <div class="cli-ent table">
            <div class="tab-content">
                <div class="tab-pane active content-datatable datatable-width" id="myprofile" role="tabpanel"> 
                    <div class="tabs-card">
                        <div class="row" id="profile_form">
                            <div class="about_info_edit">
                                <form method="post" id="about_info_form">
                                    <div class="col-md-12">
                                        <div class="settingedit-box">
                                            <div class="settrow">
                                                <h2 class="main-info-heading">Profile Information</h2>
                                                <p class="fill-sub">Please fill your Profile Information</p>
                                                <div class="settcol right">
                                                    <p class="btn-x">
                                                        <input type="submit" id="about_info_btn" name="savebtn" class="btn-e save_info" value="Save"/>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                 <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php //echo (isset($data[0]['title']) && $data[0]['title'] != "") ? "label-active" : "";  ?>"> First Name </p>
                                                        <input type="text" class="input" value="<?php
                                                        if (isset($data[0]['first_name'])) {
                                                            echo $data[0]['first_name'];
                                                        }
                                                        ?>" name="first_name" required>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                 <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php //echo (isset($data[0]['title']) && $data[0]['title'] != "") ? "label-active" : "";  ?>"> Last Name </p>
                                                        <input type="text" class="input" value="<?php
                                                        if (isset($data[0]['last_name'])) {
                                                            echo $data[0]['last_name'];
                                                        }
                                                        ?>" name="last_name" required>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php //echo (isset($data[0]['title']) && $data[0]['title'] != "") ? "label-active" : "";  ?>"> Email </p>
                                                        <input type="email" class="input" value="<?php
                                                        if (isset($data[0]['email'])) {
                                                            echo $data[0]['email'];
                                                        }
                                                        ?>" name="email" required>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php //echo (isset($data[0]['company_name']) && $data[0]['company_name'] != "") ? "label-active" : "";  ?>"> Company Name </p>
                                                        <input type="text" class="input" value="<?php
                                                        if (isset($data[0]['company_name'])) {
                                                            echo $data[0]['company_name'];
                                                        }
                                                        ?>" placeholder="" name="company_name" required>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php //echo (isset($data[0]['title']) && $data[0]['title'] != "") ? "label-active" : "";  ?>"> Title </p>
                                                        <input type="text" class="input" value="<?php
                                                        if (isset($data[0]['title'])) {
                                                            echo $data[0]['title'];
                                                        }
                                                        ?>" name="title" required>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php //echo (isset($data[0]['phone']) && $data[0]['phone'] != "") ? "label-active" : "";  ?>"> Phone </p>
                                                        <input type="text" class="input" value="<?php
                                                        if (isset($data[0]['phone'])) {
                                                            echo $data[0]['phone'];
                                                        }
                                                        ?>" name="phone">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-sm-12">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php //echo (isset($data[0]['address_line_1']) && $data[0]['address_line_1'] != "") ? "label-active" : "";  ?>"> Address </p>
                                                        <textarea class="textarea-b input" name="address_line_1" ><?php
                                                        if (isset($data[0]['address_line_1'])) {
                                                            echo $data[0]['address_line_1'];
                                                        }
                                                        ?></textarea>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php //echo (isset($data[0]['city']) && $data[0]['city'] != "") ? "label-active" : "";  ?>"> City </p>
                                                        <input type="text" class="input" value="<?php
                                                        if (isset($data[0]['city'])) {
                                                            echo $data[0]['city'];
                                                        }
                                                        ?>" name="city">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>

                                                <div class="col-sm-3">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php //echo (isset($data[0]['state']) && $data[0]['state'] != "") ? "label-active" : "";  ?>"> State </p>
                                                        <input type="text" class="input" value="<?php
                                                        if (isset($data[0]['state'])) {
                                                            echo $data[0]['state'];
                                                        }
                                                        ?>" name="state" placeholder="">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>

                                                <div class="col-sm-3">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php //echo (isset($data[0]['zip']) && $data[0]['zip'] != "") ? "label-active" : "";  ?>"> Zip </p>
                                                        <input type="text" class="input" value="<?php
                                                        if (isset($data[0]['zip'])) {
                                                            echo $data[0]['zip'];
                                                        }
                                                        ?>" name="zip">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label class="form-group">
                                                        <p class="label-txt label-active"> Timezone </p>
                                                        <select class="input" name="timezone" required>
                                                            <?php for ($i = 0; $i < sizeof($timezone); $i++) { ?>
                                                                <option value="<?php echo $timezone[$i]['zone_name']; ?>" <?php
                                                                if ($data[0]['timezone'] == $timezone[$i]['zone_name']) {
                                                                    echo 'selected';
                                                                }
                                                                ?> ><?php echo $timezone[$i]['zone_name']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>	
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="tab-pane content-datatable datatable-width" id="enableEmail" role="tabpanel">
                    <div class="tabs-card">
                        <div class="row">
                            <div class="about_info">
                                <div class="col-md-12">
                                    <div class="settingedit-box">
                                        <div class="settrow">
                                            <h2 class="main-info-heading">Enable emails</h2>
                                            <p class="fill-sub">Please edit your Email Notification</p>
                                        </div>
                                        <?php foreach ($setting_fields as $kk => $vv) { ?>
                                            <div class="cli-ent-xbox">
                                                 <div class="switch-custom-usersetting-check enable_emails">
<!--                                                    <span class="checkstatus_<?php //echo $vv['id']; ?>"><?php // echo ($vv['value'] == 1) ? 'Active' : 'Inactive';    ?></span>-->
                                                    <input type="checkbox" data-cid="<?php echo $vv['id']; ?>" data-key="<?php echo $vv['key']; ?>" id="switch_<?php echo $vv['id']; ?>" 
                                                    <?php
                                                    if ($vv['value'] == 1) {
                                                        echo 'checked';
                                                    } else {

                                                    }
                                                    ?>/>
                                                    <label for="switch_<?php echo $vv['id']; ?>"></label> 

                                                </div>
                                                <label class="label-x2"><?php echo ucwords($vv['title']); ?></label>
                                            </div>
                                        <?php } ?>
                                    </div>  
                                </div>
                            </div>  
                        </div>
                    </div>

                </div>

                <!-- Modal -->
                <div class="modal fade similar-prop" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="cli-ent-model-box">
                                <header class="fo-rm-header">
                                    <h2 class="popup_h2 del-txt">Change Password</h2>
                                    <div class="cross_popup" data-dismiss="modal"> x</div>
                                </header>
                                <div class="cli-ent-model">
                                    <div class="fo-rm-body">
                                        <form method="post" action="<?php echo base_url(); ?>admin/dashboard/change_password_front">
                                            <div class="row">
                                               <div class="col-sm-12">
                                                <label class="form-group">
                                                    <p class="label-txt <?php  ?>">Current Password </p>
                                                    <input type="password" class="input" name="old_password" required>
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                                <label class="form-group">
                                                    <p class="label-txt <?php  ?>">New Password</p>
                                                    <input type="password" class="input"  name="new_password" required>
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                                <label class="form-group">
                                                    <p class="label-txt <?php  ?>">Confirm Password</p>
                                                    <input type="password" class="input" name="confirm_password" required>
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                            </div>
                                            <button class="btn-g btn btn-ydelete" name="savebtn">Save</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="addbillandsub" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <form method="post">
                    <div class="cli-ent-model">
                        <header class="fo-rm-header">
                            <h3 class="head-c">Add Billing and Subscription Plan</h3>
                        </header>
                        <div class="fo-rm-body">						
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Plan Title</label>
                                        <p class="space-a"></p>
                                        <input name="plan_title" type="text" class="form-control input-c plan_title">
                                    </div>
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Select subscription plan</label>
                                        <p class="space-a"></p>
                                        <select class="form-control select-c" name="plan_type">
                                            <option>Please select a subscription plan</option>
                                            <option value="Annual">Annual Plan</option>
                                            <option value="Monthly">Monthly Plan</option>
                                        </select>
                                    </div>
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Price</label>
                                        <p class="space-a"></p>
                                        <input name="plan_price" type="text" class="form-control input-c plan_price">
                                    </div>
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Active Request</label>
                                        <p class="space-a"></p>
                                        <select class="form-control select-c" name="active_request">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">In Progress Request</label>
                                        <p class="space-a"></p>
                                        <select class="form-control select-c" name="in_progress_request">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                        </select>
                                    </div>
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Request per month</label>
                                        <p class="space-a"></p>
                                        <input name="request_per_month" type="text" class="form-control input-c plan_title">
                                    </div>
                                    <div class="form-group goup-x1">
                                        <label class="label-x3">Credit Card Required </label>
                                        <p class="space-a"></p>
                                        <select class="form-control select-c" name="creditcard_required">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <p class="space-b"></p>
                            <p class="btn-x">
                                <button type="submit" onclick="return validate();" class="btn-g submit">Add Plan</button>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- jQuery (necessary for JavaScript plugins) -->
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js');?>"></script>
<script type="text/javascript">
//$.urlParam = function (name) {
//    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
//    if (results == null) {
//        return null;
//    }
//    else {
//        return decodeURI(results[1]) || 0;
//    }
//}
//var status = $.urlParam('status');
//$('#tab_menu #' + status + " a").click();
//$(document).ready(function () {
//    $('#edit_form_data').submit(function (e) {
//        e.preventDefault();
//        var formData = new FormData($(this)[0]);
//        $.ajax({
//            type: 'POST',
//            url: '<?php echo base_url(); ?>admin/dashboard/edit_profile_image_form',
//            data: formData,
//            async: false,
//            cache: false,
//            contentType: false,
//            processData: false,
//            success: function (data) {
//                window.location.replace("<?php echo base_url(); ?>admin/dashboard/adminprofile?status=1");
//                // location. reload(true);
//            }
//        });
//    });

//$('#about_info_form').submit(function (e) {
//    e.preventDefault();
//    var formData = new FormData($(this)[0]);
//    $.ajax({
//        type: 'POST',
//        url: '<?php echo base_url(); ?>admin/dashboard/customer_edit_profile',
//        data: formData,
//        async: false,
//        cache: false,
//        contentType: false,
//        processData: false,
//        success: function (data) {
//            //alert(data);
//            window.location.replace("<?php echo base_url(); ?>admin/dashboard/adminprofile");
//            //location. reload(true);
//        }
//    });
//});

//$('#togal_form').click(function () {
//    $("#password_form").toggle();
//});
//$('#edit_button').click(function () {
//    $('.saved_form').hide();
//    $('.edit_form').css('display', 'block');
//});
//
//$('#about_infoform').click(function () {
//    $('.about_info').hide();
//    $('.about_info_edit').css('display', 'block');
//});
//});



var $fileInput = $('.file-input');
var $droparea = $('.file-drop-area');

// highlight drag area
$fileInput.on('dragenter focus click', function () {
    $droparea.addClass('is-active');
});

// back to normal state
$fileInput.on('dragleave blur drop', function () {
    $droparea.removeClass('is-active');
});

// change inner text
$fileInput.on('change', function () {
    var filesCount = $(this)[0].files.length;
    var $textContainer = $(this).prev();

    if (filesCount === 1) {
                                            // if single file is selected, show file name
                                            var fileName = $(this).val().split('\\').pop();
                                            $textContainer.text(fileName);
                                        } else {
                                            // otherwise show number of files
                                            $textContainer.text(filesCount + ' files selected');
                                        }
                                    });
function validateAndUpload(input) {
                                        //alert("In Function"); 
                                        var URL = window.URL || window.webkitURL;
                                        var file = input.files[0];

                                        if (file) {
                                            console.log(URL.createObjectURL(file));
                                            // console.log(file);
                                            $(".imagemain").hide();

                                            $(".imagemain2").show();
                                            $(".imagemain3").show();
                                            $("#image3").attr('src', URL.createObjectURL(file));

                                            $(".dropify-wrapper").show();
                                        }
                                    }
//$(document).on('change', '.enable_emails input[type="checkbox"]', function () {
//    var status = $(this).prop('checked');
//    var key = $(this).attr('data-key');
//    var data_id = $(this).attr('data-cid');
//    var ischecked = ($(this).is(':checked')) ? 1 : 0;
////    if (ischecked) {
////        $(".checkuserstatus_" + data_id).text('Active');
////    } else {
////        $(".checkuserstatus_" + data_id).text('Inactive');
////    }
//    $.ajax({
//        type: 'POST',
//        url: "<?php echo base_url(); ?>admin/dashboard/change_customer_status_settings",
//        data: {status: status, data_id: data_id, data_key: key},
//        success: function (data) {
//            //window.location.reload();
//        }
//    });
//});
</script>
</body>
</html>