<section id="content-wrapper">

    <div class="site-content-title">
        <h2 class="float-xs-left content-title-main">View Designer</h2>

        <ol class="breadcrumb float-xs-right">
            <li class="breadcrumb-item">
                <span class="fs1" aria-hidden="true" data-icon="?"></span>
                <a href="#">Main Menu</a>
            </li>
            <li class="breadcrumb-item active">View Users</li>
        </ol>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4 class="page-content-title">Designer</h4>
                <div class="divider15"></div>
                <div class="nav-tab-pills-image">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item active">
                            <a class="nav-link" data-toggle="tab" href="#personal_info_tab" role="tab">
                                <i class="icon icon_house_alt"></i>Info
                            </a>
                        </li>                        
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#designs_queue_tab" role="tab">
                                <i class="icon icon_cog"></i>Designs Queue
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#approved_designs_tab" role="tab">
                                <i class="icon icon_cog"></i>Approved Designs
                            </a>
                        </li>
						<li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#assigned_customer" role="tab">
                                <i class="icon icon_cog"></i>Assigned Customer
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="personal_info_tab" role="tabpanel">
                            <div class="content content-datatable datatable-width">
								
								<div class="all-form-section">
									<div class="row">        
					<?php if($this->session->flashdata('message_error') != '') {?>				
				   <div class="alert alert-danger alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
						<strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
					</div>
				   <?php }?>
				   <?php if($this->session->flashdata('message_success') != '') {?>				
				   <div class="alert alert-success alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
						<strong><?php echo $this->session->flashdata('message_success');?></strong>
					</div>
				   <?php }?>									
										<form enctype="multipart/form-data" method="post">

										<div class="element-form">
											<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
												<label>First Name</label>
											</div>
											<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
												<div class="form-group">
													<input type="text" class="form-control" value="<?php echo $designer[0]['first_name']; ?>" name="first_name">
												</div>
											</div>
										</div>

										<div class="element-form">
											<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
												<label>Last Name</label>
											</div>
											<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
												<div class="form-group">
													<input type="text" class="form-control" value="<?php echo $designer[0]['last_name']; ?>" name="last_name">
												</div>
											</div>
										</div>

										<div class="element-form">
											<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
												<label>Phone</label>
											</div>
											<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
												<div class="form-group">
													<input type="text" class="form-control numericOnly" value="<?php echo $designer[0]['phone']; ?>" name="phone">
												</div>
											</div>
										</div>

										<div class="element-form">
											<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
												<label>Email</label>
											</div>
											<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
												<div class="form-group">
													<input type="email" class="form-control" value="<?php echo $designer[0]['email']; ?>" name="email">
												</div>
											</div>
										</div>

										<div class="element-form">
											<div class="col-xl-2 col-lg-3 col-md-3 col-sm-12 col-xs-12 text-xs-right">
												<label>Profile Picture</label>
											</div>
											<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-xs-12">
												<div class="form-group">
													<div class="row">
														<div class="col-md-4">
															<?php
															if ($designer[0]['profile_picture']):
																//echo $this->Html->image($designer->profile_picture_url, ['class' => 'img img-responsive img-medium']);
																?>
																<img width="200" src="<?php echo base_url()."public/profile/".$designer[0]['profile_picture']; ?>" />
																<?php
																echo '<div class="divider15"></div>';
															endif;
															?>
															<input type="file" name="profile_picture1" data-height="200" data-plugin="dropify" class="dropify">
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="element-form">
											<div class="col-xl-2 col-lg-3 col-sm-12 col-md-3 text-xs-right"></div>
											<div class="col-xl-9 col-lg-9 col-sm-12 col-md-9 col-xs-12">
												<input type="submit" value="Edit Designer" class="btn btn-success flat-buttons waves-effect waves-button">
											</div>
										</div>
										</form>
									</div>
								</div>
							</div>
                        </div>

                        <div class="tab-pane content-datatable datatable-width" id="designs_queue_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive" style="border:none;">
                                        <!--<thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Title</th>
                                                <th>Description</th>                           
                                                <th>Customer Id</th>                           
                                                <th>Customer Name</th>                           
                                                <th>Designer Assigned</th>                           
                                                <th>Design Preview</th>
                                                <th>Design Status</th>
                                                <th>Date Requested</th>
                                                <th>Action</th>                            
                                            </tr>
                                        </thead>-->
                                        <tbody>
                                            <?php for ($i=0;$i<sizeof($designs_queue);$i++){ ?>
                                                <tr>
													 <td><?= $designs_queue[$i]['id'] ?></td>
                                                    <td><?= $designs_queue[$i]['title'] ?></td>
                                                    <td><?= $designs_queue[$i]['description'] ?></td>  
                                                    <td><?php if(isset($designs_queue[$i]['customer'][0])) { echo $designs_queue[$i]['customer'][0]['id']; } ?></td>  
                                                    <td><?php if(isset($designs_queue[$i]['customer'][0])) {  echo $designs_queue[$i]['customer'][0]['first_name']. ' ' . $designs_queue[$i]['customer'][0]['last_name']; } ?></td>  
                                                    <td></td>  
                                                    <td>
                                                        <?php
                                                        /*if ($request->attachment):
                                                            $img_path = REQUEST_IMG_PATH . DS . $request->id . DS . $request->profile_picture;
//                                        echo $this->Html->image($img_path, ['class' => 'img img-responsive img-small']);
                                                        endif;*/
                                                        ?>
                                                    </td>
													<td><?= ($designs_queue[$i]['status']) ? $designs_queue[$i]['status'] : '' ?></td>
                                                    <td><?= ($designs_queue[$i]['created']) ? date("Y-m-d",strtotime($designs_queue[$i]['created'])) : "" ?></td>  
                                                    <td>
                                                        <a href="<?php echo base_url(); ?>admin/view_request/<?= $designs_queue[$i]['id'] ?>">
                                                             <button class="btn-view btn-primary"><i class="fa fa-eye" aria-hidden="true"></i>View</button>
                                                        </a>
                                                        <?php
                                                        //echo $this->Form->postLink('<button class="btn-cancel btn-danger"><i class="fa fa-times" style="margin-right: 5px;font-size: 15px;"></i></button>', '#', ['escape' => FALSE, 'confirm' => 'Are you sure want to delete?']);
                                                        ?>
                                                    </td>
                                                </tr>        
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Title</th>
                                                <th>Description</th>                           
                                                <th>Customer Id</th>                           
                                                <th>Customer Name</th>                           
                                                <th>Designer Assigned</th>                           
                                                <th>Design Preview</th>
                                                <th>Date Requested</th>
                                                <th>Action</th>                            
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i=0;$i<sizeof($designs_approved);$i++){ ?>
                                                <tr>
                                                    <td><?= $designs_approved[$i]['id'] ?></td>
                                                    <td><?= $designs_approved[$i]['title'] ?></td>
                                                    <td><?= $designs_approved[$i]['description'] ?></td>  
                                                     <td><?php if(isset($designs_approved[$i]['customer'][0])) { echo $designs_approved[$i]['customer'][0]['id']; } ?></td>  
                                                     <td><?php if(isset($designs_approved[$i]['customer'][0])) {  echo $designs_approved[$i]['customer'][0]['first_name']. ' ' . $designs_approved[$i]['customer'][0]['last_name']; } ?></td> 
                                                    <td></td>  
                                                    <td>
                                                        <?php
                                                        /*if ($request->attachment):
                                                            $img_path = REQUEST_IMG_PATH . DS . $request->id . DS . $request->profile_picture;
//                                        echo $this->Html->image($img_path, ['class' => 'img img-responsive img-small']);
                                                        endif;*/
                                                        ?>
                                                    </td>
                                                    <td><?= ($designs_approved[$i]['created']) ? date("Y-m-d",strtotime($designs_approved[$i]['created'])) : "" ?></td>  
                                                    <td>
                                                        <a href="<?php echo base_url(); ?>admin/view_request/<?= $designs_approved[$i]['id'] ?>">
                                                             <button class="btn-view btn-primary"><i class="fa fa-eye" aria-hidden="true"></i>View</button>
                                                        </a>

                                                        <?php
                                                        //echo $this->Form->postLink('<button class="btn-cancel btn-danger"><i class="fa fa-times" style="margin-right: 5px;font-size: 15px;"></i></button>', '#', ['escape' => FALSE, 'confirm' => 'Are you sure want to delete?']);
                                                        ?>


                                                    </td>

                                                </tr>        
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="assigned_customer" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Customer Name</th>               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php for ($i=0;$i<sizeof($customer);$i++){ ?>
                                                <tr>
                                                    <td><?php echo $customer[$i]['first_name'] . ' ' . $customer[$i]['last_name']; ?></td>  
                                                </tr>        
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/qa/js/bootstrap.min.js"></script>