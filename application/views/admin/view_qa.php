<section class="con-b">
    <div class="container-fluid">
        <?php if ($this->session->flashdata('message_error') != '') { ?>				
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>				
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
            </div>
        <?php } ?>
        <div class="header-blog">
            <input type="hidden" value="" class="bucket_type"/>
            <div class="row flex-show">
                <div class="col-md-12">
                    <div class="flex-this">
                        <h2 class="main_page_heading">QA/VA Listing</h2>
                        <div class="header_searchbtn">
                            <a href="#" class="adddesinger" data-toggle="modal" data-target="#AddDesigners">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/assign_designer.svg">
                                <span> Add QA</span>
                            </a>
                            <div class="search-first">
                                <div class="focusout-search-box">
                                    <div class="search-box">
                                        <form method="post" class="search-group clearfix">
                                            <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                            <div class="ajax_searchload" style="display:none;">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" />
                                            </div>
                                            <input type="hidden" name="status" class="status" value="qa">
                                            <div class="close-search"><i class="far fa-eye-slash"></i></div>
                                            <button type="submit" class="search-btn search search_data_ajax">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_search_icon.svg" class="img-responsive">
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="filter-site">
                                <i class="fas fa-sliders-h"></i>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cli-ent table">
            <div class="tab-content">
                <!--Admin QA Section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $countqa; ?>" class="tab-pane active content-datatable datatable-width" id="Admin_QA" role="tabpanel">
                    <div class="product-list-show">
                        <div class="cli-ent-row tr tableHead">
                            <div class="cli-ent-col td" style="width: 25%;">QA NAME</div>
                            <div class="cli-ent-col td" style="width: 15%;">Sign Up Date</div>
                            <div class="cli-ent-col td  text-center" style="width: 15%;">pending requests</div>
                            <div class="cli-ent-col td  text-center" style="width: 15%;">Active requests</div>
                            <div class="cli-ent-col td  text-center"style="width: 9%;">Active</div>
                            <div class="cli-ent-col td"style="width:15%;">Action</div>
                        </div>
                        <div class="add-rows">    
                            <div class="row two-can">
                                <?php
                                for ($i = 0; $i < sizeof($allQa); $i++):
                                    $data['allqa'] = $allQa[$i];
                                    $this->load->view('admin/view_qa_template', $data);
                                endfor;
                                ?>
                            </div>
                        </div>
                        <?php if ($countqa > LIMIT_ADMIN_LIST_COUNT) { ?>
                            <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>gz-ajax-loader.gif"/></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_a; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <p class="space-d"></p>			
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal similar-prop nonflex fade" id="AddDesigners" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Add QA</h2>
                <div class="close" data-dismiss="modal" aria-label="Close"> x</div>
            </header>
            <div class="cli-ent-model-box">
                <form method="post">
                    <div class="cli-ent-model">
                        <div class="fo-rm-body">
                            <input name="role" type="hidden" value="admin">
                            <input name="full_admin_access" type="hidden" value="0">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">First Name</p>
                                        <input name="first_name" type="text" class="input firstname" required>
                                        <div class="line-box"><div class="line"></div></div>
                                        <div class="epassError frstname" style="display: none;">
                                            <p class="alert alert-danger">Enter First Name</p>
                                        </div>
                                    </label>

                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">Last Name</p>
                                        <input type="text" name="last_name" class="input lastname" required>
                                        <div class="line-box"><div class="line"></div></div>
                                        <div class="epassError lstname" style="display: none;">
                                            <p class="alert alert-danger">Enter Last Name</p>
                                        </div>
                                    </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">Email</p>
                                        <input type="email" name="email" class="input emailid" required>
                                        <div class="line-box"><div class="line"></div></div>
                                        <div class="epassError mailadrs" style="display: none;">
                                            <p class="alert alert-danger">Enter Valid Email Address</p>
                                        </div>
                                    </label>                                    
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">Phone</p>
                                        <input type="tel" name="phone"  class="input phone" required>
                                        <div class="line-box"><div class="line"></div></div>
                                        <div class="epassError phnnmr" style="display: none;">
                                            <p class="alert alert-danger">Enter Phone Number</p>
                                        </div>
                                    </label>   
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="form-group text-left">
                                        <p class="label-txt">Password</p>
                                        <input type="password" name="password" class="input password" required>
                                        <div class="line-box"><div class="line"></div></div>
                                        <!--<i>Only alphanumeric, @ and # allowed</i>-->
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">Confirm Password</p>
                                        <input type="password" name="confirm_password"  class="input cpassword" required>
                                        <div class="line-box"><div class="line"></div></div>
                                        <div class="passError" style="display: none;">
                                            <p class="alert alert-danger">Password and confirm password does not match!</p>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <p class="btn-x"><button type="submit" class="load_more button">Add</button></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade similar-prop" id="confirmationqadelete" tabindex="-1" role="dialog" aria-labelledby="confirmationqadelete" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                    <header class="fo-rm-header">
                        <h2 class="popup_h2 del-txt">Delete QA</h2>
                        <div class="cross_popup" data-dismiss="modal"> x</div>
                    </header>
                    <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/delete-bg.svg">
                        <h3 class="head-c text-center">Are you sure you want to delete this QA?</h3>
                    <div class="confirmation_btn text-center">
                        <a href="javascript:voild(0)" class="btn btn-yq btn-ydelete" data-dismiss="modal" aria-label="Close">Delete</a>
                        <button class="btn btn-nq btn-ndelete" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js');?>"></script>
<script type="text/javascript">
//function validate(){
//    var email = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
//    var fname = new RegExp('^[A-Za-z ]+$');
//    var lname = new RegExp('^[A-Za-z ]+$');
//    var mob = new RegExp('^[0-9]+$');
//    var password = new RegExp('^[A-Za-z0-9@#]+$');
//                //var password = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
//                if (!fname.test($('.firstname').val())) {
//                    $(".firstname").css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//                    $(".firstname").focus();
//                    $('.epassError.frstname').css({"display": "block"});
//                    $('.epassError.frstname .alert').css({"display": "block"});
//                    return false;
//                }
//                else{
//                    $(".firstname").css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//                    $('.epassError.frstname').css({"display": "none"});
//                    $('.epassError.frstname .alert').css({"display": "none"});
//                }
//                if (!lname.test($('.lastname').val())) {
//                    $('.lastname').css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//                    $(".lastname").focus();
//                    $('.epassError.lstname').css({"display": "block"});
//                    $('.epassError.lstname .alert').css({"display": "block"});
//                    return false;
//                }
//                else{
//                    $(".lastname").css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//                    $('.epassError.lstname').css({"display": "none"});
//                    $('.epassError.lstname .alert').css({"display": "none"});
//                }
//                if (!email.test($('.emailid').val())) {
//                    $(".emailid").css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//                    $(".emailid").focus();
//                    $('.epassError.mailadrs').css({"display": "block"});
//                    $('.epassError.mailadrs .alert').css({"display": "block"});
//                    return false;
//                }
//                else{
//                    $(".emailid").css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//                    $('.epassError.mailadrs').css({"display": "none"});
//                    $('.epassError.mailadrs .alert').css({"display": "none"});
//                }	        
//                if (!mob.test($('.phone').val())) {
//                    $(".phone").focus();
//                    $('.phone').css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//                    $('.epassError.phnnmr').css({"display": "block"});
//                    $('.epassError.phnnmr .alert').css({"display": "block"});
//                    return false;
//                }
//                else{
//                    $(".phone").css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//                    $('.epassError.phnnmr').css({"display": "none"});
//                    $('.epassError.phnnmr .alert').css({"display": "none"});
//                }	        
//                if (!password.test($('.password').val())) {
//                    $(".password").focus();
//                    $('.password').css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//                    return false;
//                }
//                else{
//                    $(".password").css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//                }
//                if($('.password').val() != $('.cpassword').val())
//                {
//                    $(".password").focus();
//                    $('.password').css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//                    $('.cpassword').css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//                    $('.passError').css({"display": "block"});
//                    $('.passError .alert').css({"display": "block"});
//                    return false;
//                }
//                else{
//                    $(".password").css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//                    $(".cpassword").css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//                    $('.passError').css({"display": "none"});
//                    $('.passError .alert').css({"display": "none"});
//                }
//                return true;     		
//            }
//            function EditFormSQa(id){
//               var email = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
//               var fname = new RegExp('^[A-Za-z ]+$');
//               var lname = new RegExp('^[A-Za-z ]+$');
//               var mob = new RegExp('^[0-9]+$');
//               var password = new RegExp('^[A-Za-z0-9@#]+$');
//			//var password = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
//			if (!fname.test($('#'+id+' .efirstname').val())) {
//             $('#'+id+' .efirstname').css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//             $(".efirstname").focus();
//             return false;
//         }
//         else{
//          $('#'+id+' .efirstname').css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//      }
//      if (!lname.test($('#'+id+' .elastname').val())) {
//         $('#'+id+' .elastname').css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//         $('#'+id+' .elastname').focus();
//         return false;
//     }
//     else{
//      $('#'+id+' .elastname').css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//  }
//  if (!email.test($('#'+id+' .eemailid').val())) {
//     $('#'+id+' .eemailid').css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//     $('#'+id+' .eemailid').focus();
//     return false;
// }
// else{
//  $('#'+id+' .eemailid').css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//}	        
//if (!mob.test($('#'+id+' .ephone').val())) {
//    $('#'+id+' .ephone').focus();
//    $('#'+id+' .ephone').css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//    return false;
//}
//else{
//  $('#'+id+' .ephone').css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//}
//if($('#'+id+' .epassword').val() != ""){
//  if (!password.test($('#'+id+' .epassword').val())) {
//     $('#'+id+' .epassword').focus();
//     $('#'+id+' .epassword').css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//     return false;
// }
// else{
//   $('#'+id+' .epassword').css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//}
//}
//if($('#'+id+' .ecpassword').val() != ""){
//  if($('#'+id+' .epassword').val() != $('.ecpassword').val())
//  {
//   $('#'+id+' .epassword').focus();
//   $('#'+id+' .epassword').css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//   $('#'+id+' .ecpassword').css({"box-shadow": "0px 1px 6px #f00","border":"1px solid #f00"});
//   $('#'+id+' .epassError').css({"display": "block"});
//   $('.epassError .alert').css({"display": "block"});
//   return false;
//}
//else{
//   $('#'+id+' .epassword').css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//   $('#'+id+' .ecpassword').css({"box-shadow": "0 0 2px #d7d8d8","border":"1px solid #d7d8d8"});
//}
//}
//return true;  
//}

/**********************************my-load-more*****************************/

function load_more_qavalist() {
    var status_scroll = 'qa';
    var searchval = $('.search_text').val();
    $.post('<?php echo site_url() ?>admin/dashboard/load_more_qa_va', {'group_no': 0, 'search': searchval, 'status': status_scroll},
        function (data) {
            var newTotalCount = 0;
            if (data != '') {
                //console.log(data);
                $(".ajax_searchload").fadeOut(500);
                $(".product-list-show .row").html(data);
                // newTotalCount = $(tabid).find("#loadingAjaxCount").attr('data-value');
                newTotalCount = $("#loadingAjaxCount").attr('data-value');
                $("#loadingAjaxCount").remove();
                //activeTabPane.attr("data-total-count",newTotalCount);
                //activeTabPane.attr("data-loaded",$rowperpage);
                //activeTabPane.attr("data-group",1);
            } else {
                activeTabPane.attr("data-total-count", 0);
                activeTabPane.attr("data-loaded", 0);
                activeTabPane.attr("data-group", 1);
                $(tabid + " .product-list-show .row").html("");
            }
            if ($rowperpage >= newTotalCount) {
                activeTabPane.find('.load_more').css("display", "none");
            } else {
                activeTabPane.find('.load_more').css("display", "inline-block");
            }
        });
}

$('.searchdata').keyup(function (e) {
    e.preventDefault();
    delay(function () {
         $('.ajax_loader').css('display', 'flex');
        $(".ajax_searchload").fadeIn(500);
        load_more_qavalist();
    }, 1000);
});
var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

$('.search_data_ajax').click(function (e) {
    e.preventDefault();
    load_more_qavalist();
});


$(document).ready(function () {
    var status = $('#status_check li.active a').attr('data-status');
    $rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
    $('.tab-pane').attr("data-loaded", $rowperpage);
    $(document).on("click", "#load_more", function () {
        $(".ajax_loader").css("display", "block");
        $(".load_more").css("display", "none");
        var row = parseInt($(this).attr('data-row'));
        row = row + $rowperpage;
        var activeTabPane = $('.tab-pane.active');
        var searchval = activeTabPane.find('.search_text').val();
        var allcount = parseInt(activeTabPane.attr("data-total-count"));
        var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
        var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
        var status_scroll = $('#status_check li.active a').attr('data-status');
        var tabid = $('#status_check li.active a').attr('href');
        if (allLoaded < allcount) {
            $.post('<?php echo site_url() ?>admin/dashboard/load_more_qa_va', {'group_no': activeTabPaneGroupNumber, 'status': status_scroll, 'search': searchval},
                function (data) {
                    if (data != "") {
                        $(".product-list-show .row").append(data);
                        row = row + $rowperpage;
                        $(".ajax_loader").css("display", "none");
                        $(".load_more").css("display", "inline-block");
                        activeTabPane.find('.load_more').attr('data-row', row);
                        activeTabPaneGroupNumber++;
                        activeTabPane.attr('data-group', activeTabPaneGroupNumber);
                        allLoaded = allLoaded + $rowperpage;
                        activeTabPane.attr('data-loaded', allLoaded);
                        if (allLoaded >= allcount) {
                            activeTabPane.find('.load_more').css("display", "none");
                            $(".ajax_loader").css("display", "none");
                        } else {
                            activeTabPane.find('.load_more').css("display", "inline-block");
                            $(".ajax_loader").css("display", "none");
                        }
                    }
                });
        }
    });
});
</script>
