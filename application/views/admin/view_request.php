<?php //echo "<pre>";print_r($request);exit; ?>
<link rel="stylesheet" type="text/css" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/jquery.fancybox.min.css">
<section class="con-b project-info-page">
    <div class="content_section">
        <div style="display: none;" id="statusfail" class="alert alert-danger alert-dismissable">
            <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
            <p class="head-c">
                Something went wrong.
            </p>
        </div>
        <div style="display: none;" id="statuspass" class="alert alert-success alert-dismissable">
            <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
            <p class="head-c">
                Status successfully changed.
            </p>
        </div>
        <?php if ($this->session->flashdata('message_error') != '') { ?>
            <div id="message" class="alert alert-danger alert-dismissable">
                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_error'); ?>
                </p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>
            <div id="message" class="alert alert-success alert-dismissable">
                <a href="javascript:void(0)" class="close" data-dismiss = "alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_success'); ?>
                </p>
            </div>
        <?php }
        ?>
        <div class="row">
            <?php ?>   
            <div class="content_wrapper">
                <div class="header-blog">
                    <div class="row">
                        <div class="col-md-12">


                            <div class="rheight-xt d-progress">
                    <h3 class="head-b"><p class="savecols-col left"><a href="<?php echo base_url().'admin/dashboard'.$bckurl;?>" class="backbtns"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive"></a></p> <?php echo $request[0]['title']; ?>
                                </h3>
                                <div class="flex-this">
                                    <?php if ($request[0]['status'] != "approved" && $request[0]['status'] != "hold") { ?>
                                        <a href="javascript:void(0)" class="adddesinger upload" data-toggle="modal" data-target="#Addfiles">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/trail.svg"><span> Upload Draft</span>
                                        </a>
                                        <?php //if ($request[0]['designer_assign_or_not'] == 0) { ?>
                                                <a href="javascript:void(0)" class="adddesinger mrl_10" data-toggle="modal" data-target="#AddDesigner" data-requestid="<?php echo $request[0]['id']; ?>" data-designerid= "<?php echo $request[0]['designer_id']; ?>">
                                                   <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/assign_designer.svg" class="img-responsive"> <span>Assign Designer</span>
                                               </a>
                                        <?php //} ?>
                                    <?php } ?>
                                    <div class="srd-col status_opt">
                                        <input type="hidden" value="<?php echo $request[0]['id']; ?>" id="request_id_status">
                                        <div class="tool-hover">
                                            <select class="form-control statusselect select-b" onchange="statuschange(<?php echo $request[0]['id']; ?>, this)" <?php echo ($cancel_subscription == 1) ? "disabled" : ""; ?>>
                                                <option disabled value="" selected>Change Status</option>
                                                <option value="assign"<?php
                                                if ($request[0]['status_admin'] == "assign") {
                                                    echo 'disabled ' . ' selected';
                                                }
                                                ?>>In Queue</option>
                                                <option value="active" <?php
                                                if ($request[0]['status_admin'] == "active") {
                                                    echo 'disabled ' . ' selected';
                                                }
                                                ?>>Design In Progress</option>
                                                <option value="disapprove" <?php
                                                if ($request[0]['status_admin'] == "disapprove") {
                                                    echo 'disabled ' . ' selected';
                                                }
                                                ?>>Revision In Progress</option>
                                                        <?php if ($request[0]['status_admin'] == "hold") { ?>
                                                    <option value="unhold" data-status="false">Unhold</option>   
                                                <?php } ?>
                                                <option value="hold" <?php
                                                if ($request[0]['status_admin'] == "hold") {
                                                    echo 'disabled ' . ' selected';
                                                }
                                                ?> data-status="true">Hold</option> 
                                                <option value="pendingrevision" <?php
                                                if ($request[0]['status_admin'] == "pendingrevision") {
                                                    echo 'disabled ' . ' selected';
                                                }
                                                ?> >Pending Review</option>
                                                <option value="checkforapprove" <?php
                                                if ($request[0]['status_admin'] == "checkforapprove") {
                                                    echo 'disabled ' . ' selected';
                                                }
                                                ?> >Review your design</option>
                                                <option value="approved" <?php
                                                if ($request[0]['status_admin'] == "approved") {
                                                    echo 'disabled ' . ' selected';
                                                }
                                                ?>>Completed</option>
                                                <option value="cancel" <?php
                                                if ($request[0]['status_admin'] == "cancel") {
                                                    echo 'disabled ' . ' selected';
                                                }
                                                ?>>Cancelled</option>
                                            </select>
                                            <?php if ($cancel_subscription == 1) { ?>
                                                <div class="tool-text">This customer subscription is cancelled, You need to reactivate it to change project status.</div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-blog">
                    <div class="row">
                        <div class="col-sm-12">
                            <ul id="status_check" class="list-unstyled list-header-blog project-information" role="tablist" style="border:none;">
                                <li class="active" id="1"><a data-toggle="tab" href="#designs_request_tab" role="tab">Project Drafts </a></li>
                                <li class="" id="2"><a class="nav-link tabmenu" data-toggle="tab" href="#inprogressrequest" role="tab">Project Information </a></li>
                                <li class="" id="3"><a class="nav-link tabmenu" data-toggle="tab" href="#activity_timeline" role="tab">Activity Timeline </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="orb-xb-col right">
                    <div class="tab-content">
                        <div data-group="1" data-loaded="" class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                            <div class="orbxb-img-xx3s two-can">
                                <div class="row-designdraftnew">
                                    <?php if ($request_files) { ?>
                                        <ul class="list-unstyled clearfix list-designdraftnew">
                                            <?php
                                            $divide = (count($request_files)) % 3;
                                            for ($i = 0; $i < count($request_files); $i++) {
                                                if ($divide == 2) {
                                                    if ($i <= 1) {
                                                        $class = "col-md-6 big_box";
                                                    } else {
                                                        $class = "col-lg-4 col-md-6";
                                                    }
                                                } else {
                                                    $class = "col-lg-4 col-md-6";
                                                }
                                                ?>
                                                <li class="<?php echo $class; ?>">
                                                    <div class="figimg-one">
                                                        <div class="draft-go">
                                                            <?php if ($request_files[$i]['admin_seen'] == 0) { ?>
                                                                <div class="status_label">
                                                                    <svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve">
                                                                    <g>
                                                                    <path fill-rule="evenodd" fill="#A02302" d="M1.9,71.1l4.5,6.7V60.6L1.9,71.1z" />
                                                                    <path fill-rule="evenodd" fill="#A02302" d="M71.4,2.2l6.7,4.5H60.9L71.4,2.2z" />
                                                                    <path fill-rule="evenodd" fill="#F15A29" d="M2,46.5L47,2.2l24.5,0L1.9,70.9L2,46.5z" />
                                                                    </g>
                                                                    </svg>
                                                                    <span class="adnewpp">New Upload</span>
                                                                </div>
                                                            <?php } ?>
                                                            <?php if ($request_files[$i]['status'] == "pending") { ?>
                                                                <div class="status_label">
                                                                    <svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve">
                                                                    <g>
                                                                    <path fill-rule="evenodd" fill="#310000" d="M1.9,71.1l4.5,6.7V60.6L1.9,71.1z" />
                                                                    <path fill-rule="evenodd" fill="#310000" d="M71.4,2.2l6.7,4.5H60.9L71.4,2.2z" />
                                                                    <path fill-rule="evenodd" fill="#510000" d="M2,46.5L47,2.2l24.5,0L1.9,70.9L2,46.5z" />
                                                                    </g>
                                                                    </svg>
                                                                    <span class="adnewpp">Pending review</span>
                                                                </div>

                                                            <?php } elseif ($request_files[$i]['status'] == "Reject") { ?>
                                                                <div class="status_label">
                                                                    <svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve">
                                                                    <g>
                                                                    <path fill-rule="evenodd" fill="#715a5a" d="M1.9,71.1l4.5,6.7V60.6L1.9,71.1z" />
                                                                    <path fill-rule="evenodd" fill="#715a5a" d="M71.4,2.2l6.7,4.5H60.9L71.4,2.2z" />
                                                                    <path fill-rule="evenodd" fill="#866c6c" d="M2,46.5L47,2.2l24.5,0L1.9,70.9L2,46.5z" />
                                                                    </g>
                                                                    </svg>
                                                                    <span class="adnewpp">Reject</span>
                                                                </div>
                                                            <?php } elseif ($request_files[$i]['status'] == "Approve" && $request[0]['status'] == "approved") { ?>
                                                                <div class="status_label">
                                                                    <svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve">
                                                                    <g>
                                                                    <path fill-rule="evenodd" fill="#14a853" d="M1.9,71.1l4.5,6.7V60.6L1.9,71.1z" />
                                                                    <path fill-rule="evenodd" fill="#14a853" d="M71.4,2.2l6.7,4.5H60.9L71.4,2.2z" />
                                                                    <path fill-rule="evenodd" fill="#37c473" d="M2,46.5L47,2.2l24.5,0L1.9,70.9L2,46.5z" />
                                                                    </g>
                                                                    </svg>
                                                                    <span class="adnewpp">Approved</span>
                                                                </div> 
                                                            <?php } 
                                                            $type = substr($request_files[$i]['file_name'], strrpos($request_files[$i]['file_name'], '.') + 1); ?>
                                                            <div class="draft_img_wrapper">
                                                                <img src="<?php echo imageUrl($type, $request_files[$i]['request_id'], $request_files[$i]['file_name']); ?>" class="img-responsive">
                                                            </div>

                                                            <div class="view-share">
                                                                <a class="view-draft" href="<?php echo base_url() . "admin/dashboard/view_files/" . $request_files[$i]['id'] . "?id=" . $request[0]['id']; ?>" >
                                                                   
                                                                    <i class="fas fa-eye"></i> View
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="tm-heading">
                                                            <div class="iconwithtitle">
                                                                <a href="<?php echo base_url() . "admin/dashboard/view_files/" . $request_files[$i]['id'] . "?id=" . $request[0]['id']; ?>">    
                                                                    <?php
                                                                    $filename = $request_files[$i]['file_name'];
                                                                    $imagePreFix = substr($filename, 0, strpos($filename, "."));
                                                                    ?>
                                                                    <h3><?php echo $imagePreFix; ?></h3>
                                                                </a> 
                                                            </div>


                                                            <div class="chatbox-add">
                                                                <p><?php echo $request_files[$i]['created']; ?></p>
                                                                <a href="<?php echo base_url() . "admin/dashboard/view_files/" . $request_files[$i]['id'] . "?id=" . $request[0]['id']; ?>">
                                                                    <i class="icon-gz_message_icon"></i>
                                                                    <?php for ($j = 0; $j < count($file_chat_array); $j++) { ?>
                                                                        <?php if ($file_chat_array[$j]['id'] == $request[0]['designer_attachment'][$i]['id']) { ?>
                                                                            <span class="numcircle-box-01"><?php echo $file_chat_array[$j]['count']; ?></span>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </a>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } else { ?>
                                        <div class="figimg-one no_img">
                                            <?php if ($request[0]['status'] == 'draft') { ?>
                                                <h3 class="head-b draft_no">
                                                    Design Project is in Draft
                                                </h3>
                                                <a class="btn btn-x draft_btn1" href="<?php echo base_url(); ?>customer/request/new_request_brief/<?php echo $request[0]['id']; ?>?rep=1">Edit</a>
                                                <a class="btn btn-x draft_btn2" href="<?php echo base_url(); ?>customer/request/new_request_review/<?php echo $request[0]['id']; ?>?rep=1">Publish</a>
                                            <?php } elseif ($request[0]['status'] == 'assign') { ?>
                                                <h3 class="head-b draft_no">
                                                    Design Project is in queue
                                                </h3>
                                            <?php } elseif ($request[0]['status'] == 'active') { ?>
                                                <h3 class="head-b draft_no">
                                                    Designer is working on the designs
                                                </h3>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <!-- 2 tab  -->
                        <div data-group="1" data-loaded="" data-search-text="" class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">
                            <div class="review-textcolww abler">
                                <a data-toggle="modal" title="Add Note" data-target="#show_notepad_area" class="add_notes adddesinger" data-id="<?php echo $request[0]['customer_id']; ?>" data-name="<?php echo $request[0]['customer_first_name']; ?>">
                                    Add Note
                                </a>                                        
                            </div>
                            <div class="right-ins-edit">
                                <a class="" href="javascript:void(0)">
                                    <i class="icon-gz_edit_icon"></i> Edit
                                </a>
                            </div>
                            <div class="project-row-qq1">
                                <div class="designdraft-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="designdraft-views">
                                                <div class="form-group goup-x1">
                                                    <label class="label-x2"><span class="prowest"></span> Project Title</label>
                                                    <p class="space-a"></p>
                                                    <div class="review-textcolww"><?php echo $request[0]['title']; ?></div>
                                                </div>
                                                <?php if (isset($request[0]['business_industry']) && $request[0]['business_industry'] != '') { ?>
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x2"><span class="prowest"></span> Target Market</label>
                                                        <p class="space-a"></p>
                                                        <div class="review-textcolww"><?php echo $request[0]['business_industry']; ?></div>
                                                    </div>
                                                <?php } ?>
                                                <?php if (!empty($request[0]['design_colors'])) { ?>
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x2"><span class="prowest"></span> Color Pallete</label>
                                                        <div class="form-group">
                                                            <?php
                                                            if ($request[0]['design_colors']) {
                                                                $color = explode(",", $request[0]['design_colors']);
                                                            }
                                                            ?>
                                                            <p class="space-a"></p>
                                                            <?php if ($request[0]['design_colors'] && in_array("Let Designer Choose", $color)) { ?>
                                                                <div class="radio-check-kk1">
                                                                    <label> 
                                                                        <div class="">
                                                                            <span class="review-colww left">
                                                                                <span class="review-textcolww">
                                                                                    Let Designer Choose for me.</span>
                                                                            </span>
                                                                        </div>
                                                                    </label>
                                                                </div>
                                                                <?php
                                                            } else {
                                                                $flag = 0;
                                                                ?>
                                                                <div class="lessrows">
                                                                    <div class="right-lesscol noflexes">
                                                                        <div class="plan-boxex-xx6 clearfix">
                                                                            <?php
                                                                            if ($request[0]['design_colors'] && in_array("blue", $color)) {
                                                                                $flag = 1;
                                                                                ?>
                                                                                <label for="id1" class="radio-box-xx2"> 
                                                                                    <div class="check-main-xx3">
                                                                                        <figure class="chkimg">
                                                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-1.png" class="img-responsive">
                                                                                        </figure>                                       
                                                                                        <h3 class="sub-head-c text-center">Blue
                                                                                        </h3>
                                                                                    </div>
                                                                                </label>
                                                                            <?php } ?>
                                                                            <?php
                                                                            if ($request[0]['design_colors'] && in_array("aqua", $color)) {
                                                                                $flag = 1;
                                                                                ?>
                                                                                <label for="id2" class="radio-box-xx2"> 
                                                                                    <span class="checkmark"></span>
                                                                                    <div class="check-main-xx3">
                                                                                        <figure class="chkimg">
                                                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-2.png" class="img-responsive">
                                                                                        </figure>                                       
                                                                                        <h3 class="sub-head-c text-center">Auqa</h3>
                                                                                    </div>
                                                                                </label>
                                                                            <?php } ?>
                                                                            <?php
                                                                            if ($request[0]['design_colors'] && in_array("green", $color)) {
                                                                                $flag = 1;
                                                                                ?>
                                                                                <label for="id3" class="radio-box-xx2"> 
                                                                                    <div class="check-main-xx3">
                                                                                        <figure class="chkimg">
                                                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-3.png" class="img-responsive">
                                                                                        </figure>                                       
                                                                                        <h3 class="sub-head-c text-center">Greens</h3>
                                                                                    </div>
                                                                                </label>
                                                                            <?php } ?>
                                                                            <?php
                                                                            if ($request[0]['design_colors'] && in_array("purple", $color)) {
                                                                                $flag = 1;
                                                                                ?>
                                                                                <label for="id4" class="radio-box-xx2"> 
                                                                                    <div class="check-main-xx3">
                                                                                        <figure class="chkimg">
                                                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-4.png" class="img-responsive">
                                                                                        </figure>                                       
                                                                                        <h3 class="sub-head-c text-center">Purple</h3>
                                                                                    </div>
                                                                                </label>
                                                                            <?php } ?>
                                                                            <?php
                                                                            if ($request[0]['design_colors'] && in_array("pink", $color)) {
                                                                                $flag = 1;
                                                                                ?>
                                                                                <label for="id5" class="radio-box-xx2"> 
                                                                                    <div class="check-main-xx3" onclick="funcheck()">
                                                                                        <figure class="chkimg">
                                                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-5.png" class="img-responsive">
                                                                                        </figure>                                       
                                                                                        <h3 class="sub-head-c text-center">Pink</h3>
                                                                                    </div>
                                                                                </label>
                                                                            <?php } ?>

                                                                        </div>  
                                                                    </div>
                                                                </div>
                                                            <?php }
                                                            ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if (!empty($request[0]['designer'])) { ?>
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x2"><span class="prowest"></span> Design Software Requirement</label>
                                                        <p class="space-a"></p>
                                                        <div class="review-textcolww"><?php echo $request[0]['designer']; ?></div>
                                                    </div>
                                                <?php } ?>
                                                <?php if (!empty($request[0]['design_dimension'])) { ?>
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x2"><span class="prowest"></span> Design Dimension</label>
                                                        <p class="space-a"></p>
                                                        <div class="review-textcolww"><?php echo $request[0]['design_dimension']; ?>                                        
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <?php if (!empty($request[0]['color_pref'])) { ?>
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x2"><span class="prowest"></span> Color Preference</label>
                                                        <p class="space-a"></p>
                                                        <div class="review-textcolww">
                                                            <?php 
                                                            echo ucfirst($request[0]['color_pref'])."<br/>"; 
                                                            if($request[0]['color_values']){?>
                                                             <span class="color_theme">
                                                               <?php echo $request[0]['color_values']; } ?>
                                                             </span>
                                                         </div>
                                                    </div>
                                                <?php }if($request[0]['deliverables'] != ''){ ?>
                                                <div class="form-group goup-x1">
                                                    <label class="label-x2"><span class="prowest"></span> Deliverables</label>
                                                    <p class="space-a"></p>
                                                    <div class="review-textcolww"><?php echo $request[0]['deliverables']; ?></div>
                                                </div>
                                                <?php } ?>
                                                <div class="form-group  goup-x1">
                                                    <label class="label-x2"><span class="prowest"></span> Category</label>
                                                    <div class="review-textcolww">
                                                        <span><?php
                                                            echo isset($request['cat_name']) ? $request['cat_name'] : $request[0]['category'];
                                                            if ($request[0]['logo_brand'] != '') {
                                                                ?>  
                                                            </span>
                                                            <span><?php
                                                                echo $request[0]['logo_brand'];
                                                            } elseif ($request['subcat_name'] != '') {
                                                                ?></span><span><?php
                                                                echo $request['subcat_name'];
                                                            }
                                                            ?></span></div>
                                                </div>
                                                <div class="form-group goup-x1">
                                                    <label class="label-x2"><span class="prowest"></span> Brand Profile</label>
                                                    <p class="space-a"></p>
                                                    <div class="review-textcolww">  
                                                        <?php if ($branddata[0]['brand_name'] != '') { ?>
                                                            <a class="showbrand_profile" data-toggle="modal" data-target="#brand_profile"><i class="fa fa-user"></i> <?php echo $branddata[0]['brand_name']; ?></a>
                                                        <?php } else { ?>
                                                            No brand profile selected
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <!-- <div class="form-group goup-x1">
                                                    <label class="label-x2"><span class="prowest"></span> Client Notes</label>
                                                    <p class="space-a"></p>
                                                    <div class="review-textcolww">
                                                        <a data-toggle="modal" data-target="#show_notepad_area" class="add_notes" data-id="<?php echo $request[0]['customer_id']; ?>" data-name="<?php echo $request[0]['customer_first_name']; ?>">
                                                            <i class="fas fa-book-open"></i>
                                                        </a>                                        
                                                    </div>
                                                </div> -->
<!--******questions & answer*******-->
                                                    
                                                    
                                                   <?php
                                                    if (!empty($request_meta)) { 
                                                        foreach ($request_meta as $qkey => $qval) {
                                                            if($qval['question_label'] == 'DESIGN DIMENSIONS' || $qval['question_label'] == 'DESCRIPTION'){
                                                            if($qval['question_label'] == 'DESIGN DIMENSIONS'){
                                                            if($qval['answer'] != ''){
                                                               $qval['answer'] = $qval['answer'];
                                                            }else{
                                                               $qval['answer'] = $request[0]['design_dimension']; 
                                                            }
                                                            }
                                                            if($qval['question_label'] == 'DESCRIPTION'){
                                                            if($qval['answer'] != ''){
                                                               $qval['answer'] = $qval['answer'];
                                                            }else{
                                                               $qval['answer'] = $request[0]['description'];
                                                            }
                                                            }
                                                            }else{
                                                              $qval['answer'] = $qval['answer'];  
                                                            }
                                                            $ahNSq = substr($qval['answer'],97); 
                                                            // echo $ahNSq; 
                                                            if(isset($qval['answer']) && $qval['answer'] != ''){
                                                                if(strlen(htmlspecialchars($qval['answer'])) > 100){
                                                                    $answer = substr(htmlspecialchars($qval['answer']),0,97).'... <a class="expand_txt read_more" href="javascript:void(0);">Read More</a><div class="read_more_txt hide">'.htmlspecialchars($qval['answer']).'</div><a class="expand_txt read_less hide" href="javascript:void(0);">Read Less</a>';
                                                                }else{
                                                                    $answer = htmlspecialchars($qval['answer']);
                                                                }
                                                            }else{
                                                                $answer = "N/A";
                                                            }
                                                           // echo $answer;exit;
                                                           /******************Description excluded***************************/
                                                            if($qval['question_label'] != 'DESCRIPTION'){
                                                            ?>
                                                            <div class="form-group goup-x1">
                                                                <label class="label-x2"> <?php echo $qval['question_label']; ?></label>
                                                                <div class="review-textcolww">
                                                                    <?php echo $answer; ?>
                                                                </div>
                                                            </div>      
                                                            <?php } else { ?>
                                                            <div class="form-group goup-x1" style="width:100%;">
                                                                <label class="label-x2"> <?php echo $qval['question_label']; ?></label>
                                                                <div class="review-textcolww">
                                                                    <pre><?php echo $answer; ?></pre>
                                                                </div>
                                                            </div> 
                                                            <?php }
                                                        }
                                                    }else{ ?>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"> Design Dimension</label>
                                                            <div class="review-textcolww">
                                                                <?php echo isset($request[0]['design_dimension']) ? $request[0]['design_dimension']: 'N/A'; ?>
                                                            </div>
                                                        </div>
                                                    <div class="form-group goup-x1" style="width:100%;">
                                                            <label class="label-x2"> Description</label>
                                                            <div class="review-textcolww">
                                                                <pre> 
                                                                <?php echo isset($request[0]['description']) ? (strlen(htmlspecialchars($request[0]['description'])) > 100)?substr(htmlspecialchars($request[0]['description']),0,97).'... <a class="expand_txt read_more" href="javascript:void(0);">Read More</a><div class="read_more_txt hide">'.htmlspecialchars($request[0]['description']).'</div> <a class="expand_txt read_less hide" href="javascript:void(0);">Read Less</a> ':htmlspecialchars($request[0]['description']) : 'N/A'; ?>
                                                                    

                                                                </pre>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                    ?>
                                                    
                                                    <!--*********samples************-->
                                                    <?php if (!empty($sampledata)) { ?>
                                                    <div class="clearfix"></div>
                                                    <div class="design-ch-pre">
                                                    <h3 class="head-b base-heading request_heading">Design choice preferences</h3>
                                                    <ul class="list-unstyled list-accessimg">
                                                    <?php //echo "<pre/>";print_R($sampledata);  
                                                    foreach($sampledata as $skey => $sval){ ?>
                                                    <li>
                                                        <div class="accimgbx33">
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS.'samples/'.$sval['sample_id'].'/'.$sval['sample_material']; ?>" />
<!--                                                            <div class="viewDelete">
                                                                <a href="<?php //echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" data-fancybox="images">
                                                                    View
                                                                </a>
                                                                <a href="<?php //echo base_url() . "customer/Request/download_projectfile/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name'] ?>"> Download
                                                                </a>
                                                                <p class="cross-btnlink">
                                                                    <a href="javascript:void(0)" class="delete_file_req" data-id="<?php //echo $data[0]['id']; ?>" data-name="<?php //echo $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                        <span>Delete</span>
                                                                    </a>
                                                                </p>
                                                            </div>-->
                                                        </div>
                                                    </li>
                                                    <?php } ?>
                                                    </ul>
                                                    </div>
                                                    <?php } ?>
                                                <div class="light-box" id="imgrmv">
                                                    <h3 class="base-heading">Attachments</h3>
                                                    <ul class="list-unstyled list-accessimg">
                                                        <li>
                                                            <a class="accimgbx33 add attach_file_dv" data-toggle="modal" data-target="#attach_file">
                                                                <span class="icon-crss-3">
                                                                    <i class="fas fa-paperclip"></i>
                                                                    <span class="icon-circlxx55">+</span>
                                                                    <p class="attachfile">Add attachments</p>
                                                                </span>
                                                                <input class="file-input" multiple="" type="file">
                                                            </a>
                                                        </li>
                                                        <?php for ($i = 0; $i < count($request[0]['customer_attachment']); $i++) { 
                                                            $str1 = 'https://pixabay.com/';
                                                            $str2 = 'https://unsplash.com/';
                                                            $imageLink = $request[0]['customer_attachment'][$i]['image_link'];
                                                            if (strpos($imageLink, $str1) !== false)  {
                                                                    $imageLinkFrom ="https://pixabay.com/favicon-32x32.png";
                                                                    $imgLink = $imageLink;
                                                                }else if (strpos($imageLink, $str2) !== false) {
                                                                    $imageLinkFrom ="https://unsplash.com/favicon-32x32.png";
                                                                    $imgLink = $imageLink;
                                                                }else{
                                                                    $imageLinkFrom ="";
                                                                    $imgLink = "";
                                                                }
                                                            $type = substr($request[0]['customer_attachment'][$i]['file_name'], strrpos($request[0]['customer_attachment'][$i]['file_name'], '.') + 1);
                                                            if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                                ?>
                                                                <li>
                                                                    <div class="accimgbx33">
                                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150"/>
                                                                        <p class="extension_name"><?php echo $type; ?></p>

                                                                        <div class="viewDelete">
                                                                            <div class="attch-icon">
                                                                            <a data-href="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $request[0]['id'] . "/" . $request[0]['customer_attachment'][$i]['file_name']; ?>" class="view-file">
                                                                                 <i class="fas fa-eye"></i>
                                                                            </a>
                                                                            <a href="<?php echo base_url() . "admin/Dashboard/download_projectfile/" . $request[0]['id'] . "?imgpath=" . $request[0]['customer_attachment'][$i]['file_name'];?>"> <i class="fas fa-download"></i></a>
                                                                            <p class="cross-btnlink">
                                                                                <a href="javascript:void(0)" class="delete_file_req" data-id="<?php echo $request[0]['id']; ?>" data-name="<?php echo $request[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                                     <span><i class="far fa-trash-alt"></i></span>
                                                                                </a>
                                                                            </p>
                                                                             </div>
                                                                            </div> 
                                                                    </div>
                                                                </li>

                                                            <?php } else { ?>
                                                                <li>
                                                                    <div class="accimgbx33">
                                                                        <?php if(!empty($imgLink)){?>
                                                                        <div class="img_from icon-tip"><a href="<?php echo $imgLink; ?>" target="_blank"><img src="<?php echo $imageLinkFrom;?>"></a></div>
                                                                        <?php } ?>
                                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $request[0]['id'] . "/" . $request[0]['customer_attachment'][$i]['file_name']; ?>" />
                                                                        <div class="viewDelete">
                                                                           <div class="attch-icon">
                                                                            <a href="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $request[0]['id'] . "/" . $request[0]['customer_attachment'][$i]['file_name']; ?>" data-fancybox="images">
                                                                               <i class="fas fa-eye"></i>
                                                                            </a>
                                                                        <a href="<?php echo base_url() . "admin/Dashboard/download_projectfile/" . $request[0]['id'] . "?imgpath=" . $request[0]['customer_attachment'][$i]['file_name']; ?>"><i class="fas fa-download"></i></a>
                                                                            <p class="cross-btnlink">
                                                                                <a href="javascript:void(0)" class="delete_file_req" data-id="<?php echo $request[0]['id']; ?>" data-name="<?php echo $request[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                                    <span><i class="far fa-trash-alt"></i></span>
                                                                                </a>
                                                                            </p>
                                                                        </div>
                                                                           <?php if(!empty($imgLink)){?>
                                                                             <div class="img_from"><a href="<?php echo $imgLink; ?>" target="_blank"><img src="<?php echo $imageLinkFrom;?>"></a></div>
                                                                           <?php } ?>
                                                                        </div>
                                                                       
                                                                    </div>
                                                                </li>
                                                                <?php
                                                            }
                                                        }
                                                        ?>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-group="1" data-loaded="" data-search-text="" class="tab-pane content-datatable datatable-width" id="activity_timeline" role="tabpanel">
                            <div class="white-boundries activity-boundry">
                                <div class="headerWithBtn">
                                    <h2 class="main-info-heading">Activity</h2>
                                    <?php
//                                    echo "<pre>";print_r($activities);exit;
                                    if (sizeof($activities) > 0) { ?>
                                        <div class="addPlusbtn">
                                            <a href="javascript:void(0)" class="totl-activity" ><?php echo sizeof($activities); ?></a>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php if (!empty($activities)) { ?>
                                    <ul class="activities two-can">
                                        <?php foreach ($activities as $activity) { ?>
                                        <li class="cap_activits"><img src="<?php echo $activity['profile_picture']; ?>" data-toggle="tooltip" title="<?php echo $activity['user_fisrtname']; ?>"><?php echo $activity['msg']; ?><span class="activity_dt" data-toggle="tooltip" title="<?php echo $activity['created_dt']; ?>"><?php echo $activity['created']; ?> <i class="far fa-clock"></i></span></li>
                                        <?php } ?>
                                    </ul>
                                    <?php
                                } else {
                                    echo "<div class='figimg-one no_img'><h3 class='head-b draft_no'>There is no activity</h3></div>";
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="side_bar">
                    <div class="gz-sidebar">
                        <div class="sidebar-header">
                            <ul id="comment-people" class="list-header-blog sidebar_style" role="tablist" style="border:none;">
                                <li class="active" id="1">
                                    <a data-toggle="tab" href="#comments_tab" role="tab">
                                        <i class="icon-gz_message_icon"></i> Comments 
                                    </a>
                                </li>
                                <li class="" id="2">
                                    <a class="nav-link tabmenu" id="people_shares" data-proid="<?php echo isset($data[0]['id']) ? $data[0]['id'] : ''; ?>" data-isloaded="0" data-view="" data-status="draft" data-toggle="tab" href="#peoples_tab" role="tab" data-step="5" data-intro="See who you have shared this project with add more people as needed." data-position='right' data-scrollTo='tooltip'>
                                        <i class="icon-gz_share"></i>
                                        Share
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div data-group="1" data-loaded="" class="tab-pane active content-datatable datatable-width" id="comments_tab" role="tabpanel">
                                <div class="sidebar-outer">
                                    <div class="with-withoiut">
                                        <div class="sound-signal">
                                            <div class="form-radion">
                                                <input type="radio" name="chat_type" class="chattype" id="soundsignal1" checked="" value="1">
                                                <label for="soundsignal1">With Customer</label>
                                            </div>
                                            <div class="form-radion">
                                                <input type="radio" name="chat_type" id="soundsignal2" class="chattype" value="0">
                                                <label for="soundsignal2">Without Customer</label>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if ($request[0]['status_designer'] == "checkforapprove") {
                                        $status = "Pending Approval";
                                        $color = "bluetext";
                                    } elseif ($request[0]['status_designer'] == "active") {
                                        $status = "In Progress";
                                        $color = "green";
                                    } elseif ($request[0]['status'] == "hold") {
                                        $status = "On Hold";
                                        $color = "holdcolor";
                                    } elseif ($request[0]['status'] == "cancel") {
                                        $status = "Canceled";
                                        $color = "holdcolor";
                                    } elseif ($request[0]['status_designer'] == "disapprove" && $request[0]['who_reject'] == 1) {
                                        $status = "Revision";
                                        $color = "orangetext";
                                    } elseif ($request[0]['status_designer'] == "disapprove" && $request[0]['who_reject'] == 0) {
                                        $status = "Quality Revision";
                                        $color = "red";
                                    } elseif ($request[0]['status_designer'] == "pending" || $request[0]['status_designer'] == "assign") {
                                        $status = "In Queue";
                                        $color = "gray";
                                    } elseif ($request[0]['status_designer'] == "draft") {
                                        $status = "Draft";
                                        $color = "gray";
                                    } elseif ($request[0]['status_designer'] == "approved") {
                                        $status = "Completed";
                                        $color = "green";
                                    } elseif ($request[0]['status_designer'] == "pendingrevision") {
                                        $status = "Pending Review";
                                        $color = "lightbluetext";
                                    } else {
                                        $status = "";
                                        $color = "greentext";
                                    }
                                    ?>

                                    <div class="project-row-qq2 chat_box">
                                        <div class="cmmtype-row">
                                            <textarea 
                                            <?php
                                            if ($request[0]['status'] == "assign") {
                                                echo "disabled";
                                                $place = "Messaging is not enabled for designs in queue";
                                            } else {
                                                $place = "Type a message...";
                                            }
                                            ?> type="text" class="pstcmm t_w text_<?php echo $request[0]['id']; ?>" Placeholder="<?php echo $place; ?>"></textarea>
                                            <div class="send-attech">
                                                <label for="image">
                                                    <input type="file" name="shre_file[]" data-withornot="1" id="shre_file" data-reqID="<?php echo $request[0]['id']; ?>" style="display:none;" multiple onchange="uploadChatfile(event,'<?php echo $request[0]['id']; ?>','admin','<?php echo $_SESSION['user_id']; ?>','<?php echo $request[0]['customer_id']; ?>','customer','<?php echo $edit_profile[0]['first_name']; ?>','<?php echo $edit_profile[0]['profile_picture']; ?>','admin_seen')"/>
                                                    <span class="attchmnt" title="Add Files"><i class="fa fa-paperclip" aria-hidden="true"></i></span>
                                                    <input type="hidden" value="" name="saved_file[]" class="delete_file"/>
                                                </label>
                                                <span class="cmmsend">
                                                    <button class="cmmsendbtn send_request_chat"
                                                            data-designerid="<?php echo $request[0]['designer_id']; ?>"
                                                            data-requestid="<?php echo $request[0]['id']; ?>"  
                                                            data-senderrole="admin" 
                                                            data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                                                            data-receiverid="<?php echo $request[0]['customer_id']; ?>" 
                                                            data-receiverrole="customer"
                                                            data-sendername="<?php echo $edit_profile[0]['first_name']; ?>"
                                                            data-customerwithornot="1" 
                                                            data-profilepic="<?php echo $edit_profile[0]['profile_picture']; ?>">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat-send.png" class="img-responsive">
                                                    </button>
                                                </span>
                                                
                                             </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="chat-box-row">
                                    <div class="ajax_searchload chat-align" style="display:none; text-align:center;">
                                        <img src="<?php echo base_url();?>public/assets/img/customer/gz_customer_loader.gif" />
                                    </div>
                                    <div class="read-msg-box two-can message_container_without_cus" id="message_container_without_cus" style="display: none;">
                                        <!-- Start Chat without customer-->  
                                        <?php
                                        for ($j = 0; $j < count($chat_request_without_customer); $j++) {
                                            $data['chat_without_customer'] = $chat_request_without_customer[$j];
                                            if ($chat_request_without_customer[$j]['chat_created_datee'] != "") {
                                                ?>
                                                <div class="date_print msgdate_cre"><span class="msgk-udate t4">
                                                        <?php echo $chat_request_without_customer[$j]['chat_created_datee']; ?>
                                                    </span></div>
                                                <?php
                                            }
                                            if ($chat_request_without_customer[$j]['sender_id'] != $login_user_id) {
                                                $this->load->view('admin/main_left_chat_without_customer', $data);
                                            } else {
                                                $this->load->view('admin/main_right_chat_without_customer', $data);
                                            }
                                        }
                                        ?>
                                        <!-- End Chat without customer-->  
                                    </div>

                                    <!-- Start Chat with customer-->
                                    <div class="read-msg-box two-can" id="message_container" style="padding:0px; height:320px; overflow-y: scroll;width: 99%;overflow-x: hidden;">
                                        <?php
                                        for ($j = 0; $j < count($chat_request); $j++) {
                                            $data['chat_with_customer'] = $chat_request[$j];
                                            if ($chat_request[$j]['chat_created_datee'] != "") {
                                                ?>
                                                <div class="date_print msgdate_cre"><span class="msgk-udate t4">
                                                        <?php echo $chat_request[$j]['chat_created_datee']; ?>
                                                    </span></div>
                                                <?php
                                            }
                                            if ($chat_request[$j]['sender_id'] != $login_user_id) {
                                                $this->load->view('admin/main_left_chat_with_customer', $data);
                                            } else {
                                                $this->load->view('admin/main_right_chat_with_customer', $data);
                                            }
                                        }
                                        ?>
                                    </div>
                                    <!-- End Chat with customer-->
                                </div>
                            </div>

                            <div data-group="1" data-loaded="" class="tab-pane content-datatable datatable-width" id="peoples_tab" role="tabpanel">
                                <div class="sharedBy two-can">
                                    <div class="team_member_sec">
                                        <h3>Team
                                            <div class="right-inst">
                                                <a class="vwwv" href="javascript:void(0);">

                                                    <span class="vewviewer-cicls">
                                                        <img src="<?php echo $data[0]['customer_image']; ?>" class="img-responsive"/>
                                                    </span>
                                                    <span class="hoversetss"><?php echo $data[0]['customer_name']; ?>
                                                    </span>
                                                </a>
                                                <a class="vwwv" href="javascript:void(0);">
                                                    <span class="vewviewer-cicls">
                                                        <img src="<?php echo $data[0]['designer_image']; ?>" class="img-responsive"/>
                                                    </span>
                                                    <span class="hoversetss"><?php echo $data[0]['designer_name']; ?> (Designer)
                                                    </span>
                                                </a>
                                                <a class="vwwv" href="javascript:void(0);">
                                                    <span class="vewviewer-cicls">
                                                        <img src="<?php echo $data[0]['qa_image']; ?>" class="img-responsive"/>
                                                    </span>
                                                    <span class="hoversetss"><?php echo $data[0]['qa_name']; ?> (QA)
                                                    </span>
                                                </a>
                                            </div>
                                        </h3>

                                    </div>
                                    <?php
                                    if ($publiclink['public_link'] != '' || (!empty($sharedUser))) {
                                        if ($publiclink['public_link'] != '' && $publiclink['is_disabled'] != 1) {
                                            ?>
                                            <h3>Public Link</h3>
                                            <div class="share-people" id="mainpubliclinkshr" <?php echo (isset($publiclink['is_disabled']) && $publiclink['is_disabled'] == 1 ) ? 'style="display:none"' : ''; ?>>
                                                <span class="copiedone"></span>
                                                <input type="text" readonly class="show_publink" name="linkshared" value="<?php echo (isset($publiclink['public_link']) && $publiclink['public_link'] != '') ? $publiclink['public_link'] : ''; ?>" />
                                                <span class="copy_public_link" title="Copy link"><i class="far fa-clone"></i></span>
                                            </div>
                                        <?php } ?>
                                        <?php if (!empty($sharedUser)) { ?>
                                            <h3>Shared People</h3>
                                            <div id="sharedpeople" class="shareduser">
                                                <?php
                                                foreach ($sharedUser as $sk => $sv) {
                                                    ?>
                                                    <div class="sharedbymail_block_<?php echo $sv['id']; ?>">
                                                        <ul class="shared_project_class shared_user_<?php echo $sv['id']; ?>">
                                                            <li><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/share-bg.svg" class="0"></li>
                                                            <li><span>
                                                                    <?php echo $sv['user_name']; ?></span>
                                                                <?php echo $sv['email']; ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        echo "<h3 class='no-data' style='text-transform: capitalize;font-weight: 500;letter-spacing: 0;color: #db203c'>There is no shared user for this request.</h3>";
                                    }
                                    ?>
                                </div>
                                <div class="show_error"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></div>

    </div>
</section>
<!-- Multiple file upload Modal -->
<div class="modal similar-prop  fade" id="attach_file" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Add File</h2>
                    <div class="cross_popup close edit_close" data-dismiss="modal" aria-label="Close">x</div>
                </header>

                <div class="cli-ent-model">

                    <div class="selectimagespopup">
                        <form method="post" action="" enctype="multipart/form-data">
                            <div class="form-group goup-x1 goup_1">
                                <p class="space-a"></p>
                                <div class="file-drop-area">
                                    <span class="fake-img"><img src="<?php echo FS_PATH; ?>public/assets/img/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>

                                    <input type="file" class="file-input project-file-input" multiple="" name="file_upload[]" id="file_input"/>

                                </div>
                                <p class="allowed-type  text-left">Allowed file types : Doc, Docx, Odt, Pdf, Jpg, Png, Jpeg, Psd, Ai, Zip, Mp4, Mov,Ppt, Pptx </p>
                                <p class="space-e"></p>
                                <div class="uploadFileListContainer row">                      
                                </div>
                            </div>
                            <p class="btn-x"><input type="submit" value="Submit" name="attach_file" class="load_more button" style="border-radius: 7px;margin: auto;display: block;margin-top: 10px;"/></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal similar-prop nonflex fade" id="Addfiles" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Add File</h2>
                <div id="close-d" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <div class="selectimagespopup">
                        <form method="post" action="" enctype="multipart/form-data">
                            <div class="form-group goup-x1 goup_1 text-left">
                                <label class="label-x3">Source File:</label>
                                <p class="space-a"></p>
                                <div class="file-drop-area">
                                    <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                    <input type="hidden" name="tab_status" value="<?php echo $matches[0]; ?>"/>
                                    <input class="file-input" type="file" name="src_file" onchange="validateAndUploadS(this);">
                                    <div class="dropify-preview" style="display: none; width: 100%;">
                                        <img src="" class="img_dropify" style="height: 150px; width: 100%;">
                                        <div class="overlay">
                                            <div class="rem_btn">
                                                <button class="remove_selected">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group goup-x1 goup_2 text-left">
                                <label class="label-x3 ">Preview File:</label>
                                <p class="space-a"></p>
                                <div class="file-drop-area">
                                    <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                    <input class="file-input" type="file" name="preview_file"  onchange="validateAndUploadP(this);" accept="image/*">
                                    <div class="dropify-preview" style="display: none; width: 100%;">
                                        <img src="" class="img_dropify" style="height: 150px; width: 100%;">
                                        <div class="overlay">
                                            <div class="rem_btn">
                                                <button class="remove_selected">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p class="allowed-type  text-left"> <i>Allowed file types : jpg, jpeg, png, gif</i></p>
                            </div>
                            <p class="btn-x"><input type="submit" value="Submit" class="load_more button" /></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal similar-prop nonflex fade" id="AddDesigner" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Assign Designer</h2>
                <div id="close-d" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <div class="noti-listpopup">
                        <div class="newsetionlist">
                            <div class="cli-ent-row tr notificate">
                                <div class="cli-ent-col td" style="width: 33%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Designer</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 45%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Skill</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 22%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b text-center">Active Requests</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="" method="post">
                            <ul class="list-unstyled list-notificate two-can">
                                <?php foreach ($designer_list as $designers) { ?>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <div class="cli-ent-row tr notificate">
                                                <div class="cli-ent-col td" >
                                                    <div class="sound-signal">
                                                        <div class="form-radion">
                                                            <input class="selected_btn" type="radio" value="<?php echo $designers['id']; ?>" name="assign_designer" id="<?php echo $designers['id']; ?>" data-image-pic="<?php echo $designers['profile_picture'] ?>" data-name="<?php echo $designers['first_name'] . " " . $designers['last_name']; ?>">
                                                            <label for="<?php echo $designers['id']; ?>" data-image-pic="<?php echo $designers['profile_picture'] ?>"></label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 30%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="setnoti-fication">
                                                            <figure class="pro-circle-k1">
                                                                <img src="<?php echo $designers['profile_picture'] ?>" class="img-responsive">
                                                            </figure>

                                                            <div class="notifitext">
                                                                <p class="ntifittext-z1">
                                                                    <strong>
                                                                        <?php echo $designers['first_name'] . " " . $designers['last_name']; ?>

                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 45%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <p class="pro-a">UX Designer, Landing page, Mobile App  UX Designer, Landing page, Mobile App<span class="sho-wred">+2</span></p>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 25%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="cli-ent-xbox text-center">

                                                            <p class="neft text-center"><span class="red text-uppercase"><?php echo $designers['active_request']; ?></span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>  
                                <?php } ?>                          
                            </ul>
                            <input type="hidden" name="request_id" id="request_id">
                            <p class="space-c"></p>
                            <p class="btn-x text-center">
                                <button name="adddesingerbtn" type="submit" id="assign_desigertoreq" class="load_more button">Assign Designer</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<button style="display: none;" id="confirmation" data-toggle="modal" data-target="#myModal">click here</button>
<!-- Modal -->
<div class="modal fade similar-prop" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" name="request_id_status" id="request_id_status" value=""/>
            <input type="hidden" name="valuestatus" id="valuestatus" value=""/>
            <input type="hidden" name="status_flag" id="status_flag" value=""/>
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Change  Status</h2>
                <div class="cross_popup" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">
                <!--                <img class="cross_popup" data-dismiss="modal" src="<?php //echo FS_PATH_PUBLIC_ASSETS;       ?>img/default-img/cross.png">-->

                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/change_plan.svg">
                    <h3 class="head-c text-center msgal">Are you sure you want to change the status to <strong></strong></h3>
                    <div class="confirmation_btn text-center">
                        <button class="btn btn-y btn-ndelete" data-dismiss="modal" aria-label="Close">Yes Change It</button>
                        <button class="btn btn-n btn-ydelete" data-dismiss="modal" aria-label="Close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="brand_profile" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h2 class="popup_h2 del-txt">
                            <?php echo $branddata[0]['brand_name']; ?>
                        </h2>
                        <div class="cross_popup" data-dismiss="modal" aria-label="Close">
                            x
                        </div>
                    </header>
                    <?php if ($branddata[0]['id'] != '') { ?>
                        <div class="displayed-pic">
                            <div class="dp-here">
                                <?php
                                $type = substr($brand_materials_files['latest_logo'], strrpos($brand_materials_files['latest_logo'], '.') + 1);
                                $allow_type = array("jpg", "jpeg", "png", "gif");
                                if (isset($brand_materials_files['latest_logo']) && in_array(strtolower($type), $allow_type)) {
                                    ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files['latest_logo']; ?>" height="150">
                                <?php } elseif (isset($brand_materials_files['latest_logo']) && !in_array(strtolower($type), $allow_type)) { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/all-file.jpg" height="150">
                                    <!-- <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files['latest_logo']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div> -->
                                <?php } else { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/logo-brand-defult.jpg" height="150" />
                                <?php } ?>
                            </div>
                            <div class="profile_info">
                                <div class="profile colm">
                                    <div class="form-group">
                                        <div class="brand_labal">Fonts Size</div>
                                        <div class="brand_info">
                                            <?php echo $branddata[0]['fonts']; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile colm">
                                    <div class="form-group">
                                        <div class="brand_labal">Color Preferences</div>
                                        <div class="brand_info">
                                            <?php echo $branddata[0]['color_preference']; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile colm">
                                    <div class="form-group">
                                        <div class="brand_labal">Website URL </div>
                                        <div class="brand_info">
                                            <?php echo $branddata[0]['website_url']; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if (!empty($branddata[0]['google_link'])) { ?>
                                    <div class="profile colm" style="width:100%">
                                        <div class="form-group">
                                            <div class="brand_labal">Addition Reference Link</div>
                                            <div class="brand_info">
                                                <?php
                                                $brandlinks = explode(',', $branddata[0]['google_link']);
                                                foreach ($brandlinks as $bk => $bv) {
                                                    echo $bv . "<br/>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="profile colm">
                                    <div class="form-group">
                                        <div class="brand_labal">Brand Description </div>
                                        <div class="brand_info discrptions two-can">
                                            <?php echo $branddata[0]['description']; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="all-brand-outer">
                            <div class="profile colm materials-sp">
                                <div class="brand_labal">Brand Logo </div>
                                <div class="brand_info row">
                                    <?php
                                    for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                                        if ($brand_materials_files[$i]['file_type'] == 'logo_upload') {
                                            $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                            if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150" />
                                                            <p class="extension_name">
                                                                <?php echo $type; ?>
                                                            </p>
                                                        </a>
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="profile colm materials-sp">
                                <div class="brand_labal">Market Materials </div>
                                <div class="brand_info row">
                                    <?php
                                    for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                                        if ($brand_materials_files[$i]['file_type'] == 'materials_upload') {
                                            $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                            if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150" />
                                                            <p class="extension_name">
                                                                <?php echo $type; ?>
                                                            </p>
                                                        </a>
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                                        <div class="bottom-icon">
                                                            <a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="profile colm materials-sp">
                                <div class="brand_labal">Additional Images </div>
                                <div class="brand_info row">
                                    <?php
                                    for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                                        if ($brand_materials_files[$i]['file_type'] == 'additional_upload') {
                                            $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                            if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150" />
                                                            <p class="extension_name">
                                                                <?php echo $type; ?>
                                                            </p>
                                                        </a>
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<!-- jQuery (necessary for JavaScript plugins) -->
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/jquery.fancybox.min.js');?>"></script>
<script type="text/javascript">
    var $assets_path = '<?php echo FS_PATH_PUBLIC_ASSETS; ?>';
$('[data-fancybox="images"]').fancybox({
    buttons: [
        'download',
        'thumbs',
        'close'
    ]
});
</script>
<?php
if (!empty($chat_request)) {
    $prjtid = $chat_request[0]['request_id'];
    $customerid = $chat_request[0]['reciever_id'];
    $designerid = $chat_request[0]['sender_id'];
}
?>
<script type="text/javascript">
</script>
