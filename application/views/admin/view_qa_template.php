<!-- Start Row -->
<div class="cli-ent-row tr brdr fullheight">
    <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
    <div class="cli-ent-col td flex-uses" style="width: 25%;">
        <div class="cell-col">
                <figure class="cli-ent-img circle one">
                    <img src="<?php echo $allqa['profile_picture']; ?>" class="img-responsive">
                </figure>
        </div>
        <div class="cell-col">
            <h3 class="pro-head">
                <a href="javascript:void(0)"><?php echo ucwords($allqa['first_name'] . " " . $allqa['last_name']); ?>
            </a>

        </h3>
        <p class="pro-b"><?php echo $allqa['email'];?></p>
    </div>
</div>
<div class="cli-ent-col td" style="width: 15%;">
    <p class="text-a"><?php echo $allqa['created']; ?></p>
</div>
<div class="cli-ent-col td text-center" style="width: 15%;">
    <div class="cli-ent-xbox">
        <span><?php echo $allqa['count_pending_project']; ?></span>
    </div>
</div>
<div class="cli-ent-col td text-center" style="width: 15%;">
    <div class="cli-ent-xbox text-center">
        <span><?php echo $allqa['count_active_project']; ?></span>
    </div>
</div>
<div class="cli-ent-col td  text-center" style="width: 9%;" >
    <div class="notify-lines">
        <div class="switch-custom-usersetting-check en_dis_qa" data-qaid="<?php echo $allqa['id']; ?>">
            <input class="ena_dis_<?php echo $allqa['id']; ?>" type="checkbox" id="switch_<?php echo $allqa['id']; ?>" <?php echo ($allqa['is_active'] == '1')?'checked':''; ?>>
            <label for="switch_<?php echo $allqa['id']; ?>"></label> 
        </div>
    </div>
</div>
<div class="cli-ent-col td" style="width: 15%; cursor: pointer;">
    <div class="cli-ent-xbox action-per">
        <a href="<?php echo base_url(); ?>admin/accounts/view_qa_profile/<?php echo $allqa['id']; ?>" title="View Qa">
            <i class="icon-gz_edit_icon"></i>
        </a>
<!--        <a href="#" data-toggle="modal" data-target="#<?php //echo $allqa['id'];?>">
            <i class="icon-gz_edit_icon"></i>
        </a>-->
        <a title="delete" id="delete_<?php echo $allqa['id']; ?>" href="#" data-toggle="modal" class="confirmationqadelete" data-target="#confirmationqadelete" data-qaid="<?php echo $allqa['id']; ?>">
                <i class="icon-gz_delete_icon"></i>
        </a>
    </div>
</div>
</div>
<!-- Edit QA Modal -->
<!--<div class="modal similar-prop fade" id="<?php echo $allqa['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <form onSubmit="return EditFormSQa(<?php echo $allqa['id']; ?>)" method="post" action="<?php echo base_url(); ?>admin/accounts/edit_qa/<?php echo $allqa['id']; ?>">
                 <div class="modal-content">
                     <header class="fo-rm-header">
                        <h2 class="popup_h2 del-txt">Edit QA</h2>
                        <div class="close" data-dismiss="modal" aria-label="Close"> x</div>
                    </header>
                    <div class="cli-ent-model">
                        <div class="fo-rm-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt <?php echo ($allqa['first_name'] != '')?'label-active':'';?>">First Name</p>
                                        <input name="first_name" type="text" class="input efirstname" value="<?php echo $allqa['first_name']; ?>">
                                        <div class="line-box"><div class="line"></div></div>
                                    </label>
                                </div>
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt <?php echo ($allqa['last_name']!='')?'label-active':'';?>">Last Name</p>
                                        <input type="text" name="last_name" class="elastname input" value="<?php echo $allqa['last_name']; ?>">
                                        <div class="line-box"><div class="line"></div></div>
                                    </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                   <label class="form-group">
                                    <p class="label-txt <?php echo ($allqa['email']!='')?'label-active':'';?>">Email</p>
                                    <input type="email" name="email" class="eemailid input" value="<?php echo $allqa['email']; ?>">
                                    <div class="line-box"><div class="line"></div></div>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="form-group">
                                    <p class="label-txt <?php echo ($allqa['phone']!='')?'label-active':'';?>">Phone</p>
                                    <input type="tel" name="phone"  class="ephone input" value="<?php echo $allqa['phone']; ?>">
                                    <div class="line-box"><div class="line"></div></div>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                               <label class="form-group">
                                <p class="label-txt">Password</p>
                                <input type="password" name="password" class="input epassword">
                                <div class="line-box"><div class="line"></div></div>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label class="form-group">
                                <p class="label-txt">Confirm Password</p>
                                <input type="password" name="confirm_password"  class="input ecpassword">
                                <div class="line-box"><div class="line"></div></div>
                            </label>
                            <div class="epassError" style="display: none;">
                                <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                            </div>
                        </div>
                    </div>
                    <p class="btn-x"><button  type="submit" class="load_more button">Save</button></p>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>
</div> -->
<!-- End Row -->
