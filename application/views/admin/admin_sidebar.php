<?php 
$url = current_url();
$messageurl = explode('/', $url);
$pagename = end($messageurl);
$count = count($messageurl);
if($count > 1){
 $is_last_idexist = $messageurl[$count - 2];
}
//echo "<pre/>";print_R($messageurl);exit;
?>
<div class="site-logo">
    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/GraphicsZooLogo-Icon.svg" class="close-nav"/>
    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/logo.svg" class="open-nav"/>
    <span class="arrow_btn"></span>
</div>
<ul class="side-nav">
    <li class="<?php echo (($pagename == 'dashboard' && $is_last_idexist != 'report' && empty($_GET['client_id']) && empty($_GET['designer_id'])) || $is_last_idexist == 'view_request') ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>admin/dashboard"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/projuct-icon.svg" /><span>Projects</span></a></li>
    <li class="<?php echo ($pagename == 'view_clients' || $is_last_idexist == 'view_client_profile' || ((isset($_GET['client_id'])) && (!in_array('view_request',$messageurl)))) ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>admin/dashboard/view_clients"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/client-icon.svg" /><span>Clients</span></a></li>
    <li class="<?php echo ($pagename == 'view_designers' || $is_last_idexist == 'view_designer_profile' || ((isset($_GET['designer_id'])) && (!in_array('view_request',$messageurl)))) ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>admin/dashboard/view_designers"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/designer-icon.svg" /><span>Designers</span></a></li>
    <?php if(($profileinfo[0]['full_admin_access'] != 0 || $profileinfo[0]['full_admin_access'] == NULL) && $profileinfo[0]['role'] == 'admin'){ ?> 
    <li class="<?php echo ($pagename == 'view_qa' || $is_last_idexist == 'view_qa_profile') ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>admin/dashboard/view_qa"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/qa-icon.svg" /><span>QA/VA</span></a></li>
    <?php } ?>
    <li class="<?php echo ($pagename == 'message_list' || $pagename == 'read' || $pagename == 'is_star') ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>admin/messages/message_list"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/sidebar_message_icon.svg" /><span>Messages</span></a></li>
    <?php if(($profileinfo[0]['full_admin_access'] != 0 || $profileinfo[0]['full_admin_access'] == NULL) && $profileinfo[0]['role'] == 'admin'){ ?> 
    <li class='<?php echo ($pagename == 'projectReports' || (in_array('view_projects_basedonslug',$messageurl))) ? 'active' : ''; ?>'><a href="<?php echo base_url(); ?>admin/Contentmanagement/projectReports"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/reporting-icon.svg" /><span>Reports</span></a></li>
    <li class="<?php echo ($pagename == 'index' || $pagename == 'blog' || $pagename == 'addblog' || $is_last_idexist == 'blog_edit' || $pagename == 'portfolio' || $pagename == 'email' || 
            $pagename == 'addportfolio' || $is_last_idexist == 'portfolio_edit' || $pagename == 'billing_subscription' ||
            $pagename == 'cat_listing' || $pagename == 'notifications' || ($pagename == 'dashboard' && $is_last_idexist == 'report') 
            || $pagename == 'category_based_questions' || $pagename == 'add_category_questions' ||
            in_array('add_category_questions',$messageurl) || $pagename == 'add_samples' || $pagename == 'getAffiateUsers' || $pagename == 'category_based_samples' || (in_array('add_samples',$messageurl))) ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>admin/Contentmanagement/index"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/content-managment-icon.svg" /><span>Additional Modules</span></a></li> 
    
    <li class="<?php echo (in_array('cancel_request_users',$messageurl)) ? 'active' : ''; ?>"><a href="<?php echo base_url(); ?>admin/dashboard/cancel_request_users"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/cancel-request.svg" /><span>Cancel Request Users</span></a></li>
        <?php } ?>
</ul>

