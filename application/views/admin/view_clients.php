<section class="con-b">
    <div class="container-fluid">
        <?php
        if ($this->session->flashdata('message_error') != '') { ?>				
            <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>				
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
            </div>
        <?php } ?>
        <div class="alert alert-danger alert-dismissable error" style="display:none">
            <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">×</a>
            <p class="head-c" id="show_error"></p>
        </div>
        <div class="header-blog">
            <div class="row flex-show">
                <div class="col-md-12">
                    <div class="flex-this">
                        <h2 class="main_page_heading">Client</h2>
                        <div class="header_searchbtn">
                            <?php if(!$isfullaccess && $_GET['assign'] == 1){?>
                                <a href="<?php echo base_url();?>admin/dashboard/view_clients" class="addderequesr">
                                    <span class="">All Customer</span>
                                </a>
                            <?php }else if(!$isfullaccess){ ?>
                                <a href="<?php echo base_url();?>admin/dashboard/view_clients?assign=1" class="addderequesr">
                                    <span class="">Customer assign to me </span>
                                </a>
                            <?php } ?>
                            <a href="#" class="addderequesr" data-toggle="modal" data-target="#AddClient">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/add_client.svg"><span><span>Add Client</span>
                            </a>
                            <?php 
                            if($isfullaccess) { 
                                if( $edit_profile[0]['full_admin_access'] != 2){
                                ?>
                                <a class="adddesinger" href="<?php echo base_url(); ?>admin/dashboard/downloadTrialusers">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/trail.svg"><span class="">Trial</span>
                                </a>
                                <a class="adddesinger" href="<?php echo base_url(); ?>admin/dashboard/downloadActiveusers"> 
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/trail.svg"><span> Active</span>
                                </a>
                            <?php } ?>
                                <a href="#" class="assign_va" data-toggle="modal" data-target="#Addva" data-requestid="" data-designerid= "">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/assign_designer.svg" class="img-responsive"><span>Add VA</span>
                                </a>
                            <?php } ?>
                            <div class="search-first">
                                <div class="focusout-search-box">
                                    <div class="search-box view-client">
                                        <form method="post" class="search-group clearfix">
                                            <input type="text" placeholder="Search here..." class="form-control searchdata searchdata_client search_text">
                                            <div class="ajax_searchload" style="display:none;">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" />
                                            </div>
                                            <input type="hidden" name="status"  value="active,disapprove,assign,pending,checkforapprove">
                                            <div class="close-search"><i class="far fa-eye-slash"></i></div>
                                            <button type="submit" class="search-btn search search_data_ajax">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_search_icon.svg" class="img-responsive">
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="filter-site">
                                <i class="fas fa-sliders-h"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-blog">
            <div class="row flex-show">
                <div class="col-lg-12">
                    <ul id="status_check_client" class="list-unstyled list-header-blog" role="tablist" style="border:none;">
                        <li class="active">
                            <a data-toggle="tab" href="#Admin_active" data-is_single="0" data-isloaded=0 data-status="paid" role="tab" data-pagin ="<?php echo $count_customer; ?>">Active &nbsp; <?php echo $count_customer; ?></a>
                        </li>			
                        <li>
                            <a data-toggle="tab" href="#Admin_free" data-is_single="0" data-isloaded=0 data-status="free" role="tab" data-pagin="<?php echo $counttrailcustomer; ?>">Trial Accounts &nbsp; <?php echo $counttrailcustomer; ?></a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#Admin_request" data-is_single="1" data-isloaded=0 data-status="request_based" role="tab" data-pagin="<?php echo $countrequestcustomer; ?>">Requests Based &nbsp; <?php echo $countrequestcustomer; ?></a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#Cancel_subscription" data-is_single="0" data-isloaded=0 data-status="cancel_subs" role="tab" data-pagin="<?php echo $countcancelsubscription; ?>">Deactivate Users &nbsp; <?php echo isset($countcancelsubscription) ? $countcancelsubscription : 0; ?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-content">
            <div data-group="1" data-loaded=""  data-total-count="<?php echo $count_customer; ?>" class="tab-pane active view_clnts content-datatable datatable-width" id="Admin_active" role="tabpanel">
                <div class="product-list-show">
                    <div class="cli-ent-row tr tableHead">
                        <div class="cli-ent-col td">
                            <div class="sound-signal">
                                <div class="form-radion2">
                                    <label class="containerr">
                                        <input type="checkbox" class="checkallcus" data-attr="paid" name="customer">  
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 22%;">Client NAME</div>
                        <div class="cli-ent-col td text-center" style="width: 12%;">Sign Up Date</div>
                        <div class="cli-ent-col td  text-center" style="width: 12%;">Billing end date</div>
                        <div class="cli-ent-col td  text-center" style="width: 11%;">Total project</div>
                        <div class="cli-ent-col td  text-center"style="width: 6%;">Active</div>
                        <div class="cli-ent-col td"style="width:15%;">Assign designer</div>
                        <div class="cli-ent-col td"style="width:8%;">Assign va</div>
                        <div class="cli-ent-col td text-center"style="width:14%;">Action</div>
                    </div>
                    <div class="add-rows">    
                        <div class="row two-can">
                            <?php
                            for ($i = 0; $i < sizeof($activeClients); $i++) {
                                $data['clients'] = $activeClients[$i];
                                $this->load->view('admin/view_clients_template', $data);
                            }
                            ?>
                        </div>			
                    </div>
                    <?php if ($count_customer > LIMIT_ADMIN_LIST_COUNT) { ?>
                        <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif"/></div>
                        <div class="" style="text-align:center">
                            <a id="load_more_client" href="javascript:void(0)" data-is_single="0" data-row="0" data-count="<?php echo $count_project_a; ?>" class="load_more button">Load more</a>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div data-group="1" data-loaded=""  data-total-count="<?php echo $counttrailcustomer; ?>" class="tab-pane view_clnts view_clnts content-datatable datatable-width" id="Admin_free" role="tabpanel">
                <div class="product-list-show">
                    <div class="cli-ent-row tr tableHead">
                        <div class="cli-ent-col td">
                            <div class="sound-signal">
                                <div class="form-radion2">
                                    <label class="containerr">
                                        <input type="checkbox" class="checkallcus" data-attr="free" name="customer">  
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 22%;">Client NAME</div>
                        <div class="cli-ent-col td text-center" style="width: 12%;">Sign Up Date</div>
                        <div class="cli-ent-col td  text-center" style="width: 12%;">Billing end date</div>
                        <div class="cli-ent-col td  text-center" style="width: 11%;">Total project</div>
                        <div class="cli-ent-col td  text-center"style="width: 6%;">Active</div>
                        <div class="cli-ent-col td"style="width:15%;">Assign designer</div>
                        <div class="cli-ent-col td"style="width:10%;">Assign va</div>
                        <div class="cli-ent-col td"style="width:12%;">Action</div>
                    </div>
                    <div class="add-rows">    
                        <div class="row two-can"></div>
                    </div>
                    <?php if ($counttrailcustomer > LIMIT_ADMIN_LIST_COUNT) { ?>
                        <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" /></div>
                        <div class="" style="text-align:center">
                            <a id="load_more_client" href="javascript:void(0)" data-is_single="0" data-row="0" data-count="<?php echo $counttrailcustomer; ?>" class="load_more button">Load more</a>
                        </div>
                    <?php } ?>
                </div>
            </div>    

            <div data-group="1" data-loaded=""  data-total-count="<?php echo $countrequestcustomer; ?>" class="tab-pane view_clnts content-datatable datatable-width" id="Admin_request" role="tabpanel">
                <div class="product-list-show">
                    <div class="cli-ent-row tr tableHead">
                        <div class="cli-ent-col td">
                            <div class="sound-signal">
                                <div class="form-radion2">
                                    <label class="containerr">
                                        <input type="checkbox" class="checkallcus" data-attr="request_based" name="customer">  
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 22%;">Client NAME</div>
                        <div class="cli-ent-col td text-center" style="width: 12%;">Sign Up Date</div>
                        <div class="cli-ent-col td  text-center" style="width: 12%;">Billing end date</div>
                        <div class="cli-ent-col td  text-center" style="width: 11%;">Total project</div>
                        <div class="cli-ent-col td  text-center"style="width: 6%;">Active</div>
                        <div class="cli-ent-col td"style="width:15%;">Assign designer</div>
                        <div class="cli-ent-col td"style="width:10%;">Assign va</div>
                        <div class="cli-ent-col td"style="width:12%;">Action</div>
                    </div>
                    <div class="add-rows">    
                        <div class="row two-can">  

                        </div>
                    </div>
                    <?php if ($countrequestcustomer > LIMIT_ADMIN_LIST_COUNT) { ?>
                        <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" /></div>
                        <div class="" style="text-align:center">
                            <a id="load_more_client" href="javascript:void(0)" data-is_single="1" data-row="0" data-count="<?php echo $countrequestcustomer; ?>" class="load_more button">Load more</a>
                        </div>
                    <?php } ?>
                </div>
            </div>  
            <div data-group="1" data-loaded=""  data-total-count="<?php echo $countcancelsubscription; ?>" class="tab-pane view_clnts content-datatable datatable-width" id="Cancel_subscription" role="tabpanel">
                <div class="product-list-show">
                    <div class="cli-ent-row tr tableHead">
                        <div class="cli-ent-col td">
                            <div class="sound-signal">
                                <div class="form-radion2">
                                    <label class="containerr">
                                        <input type="checkbox" class="checkallcus" data-attr="cancel_subs" name="customer">  
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 22%;">Client Name</div>
                        <div class="cli-ent-col td text-center" style="width: 12%;">Sign Up Date</div>
                        <div class="cli-ent-col td  text-center" style="width: 12%;">Billing end date</div>
                        <div class="cli-ent-col td  text-center" style="width: 11%;">Total project</div>
                        <div class="cli-ent-col td  text-center"style="width: 6%;">Active</div>
                        <div class="cli-ent-col td"style="width:15%;">Assign designer</div>
                        <div class="cli-ent-col td"style="width:10%;">Assign va</div>
                        <div class="cli-ent-col td"style="width:12%;">Action</div>
                    </div>
                    <div class="add-rows">    
                        <div class="row two-can">  
                        </div>
                    </div>
                    <?php if ($countcancelsubscription > LIMIT_ADMIN_LIST_COUNT) { ?>
                        <div class="gz ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif" /></div>
                        <div class="" style="text-align:center">
                            <a id="load_more_client" href="javascript:void(0)" data-is_single="0" data-row="0" data-count="<?php echo $countcancelsubscription; ?>" class="load_more button">Load more</a>
                        </div>
                    <?php } ?>
                </div>
            </div>     
        </div>
    </div>
    </section>
    <!-- Modal -->
    <div class="modal similar-prop nonflex fade" id="AddClient" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <header class="fo-rm-header">
                  <h2 class="popup_h2 del-txt">Add Client</h2>
                  <div class="close" data-dismiss="modal" aria-label="Close"> x</div>
              </header>
              <div class="cli-ent-model-box">
                  <div class="cli-ent-model">
                    <div class="fo-rm-body">
                        <form method="post" action="<?php echo base_url(); ?>admin/accounts/create_customer">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt">First Name</p>
                                        <input type="text" name="first_name" required class="input firstname">
                                        <div class="line-box">
                                          <div class="line"></div>
                                      </div>
                                  </label>
                              </div>
                              <div class="col-md-6">
                                <label class="form-group">
                                    <p class="label-txt">Last Name</p>
                                    <input type="text" name="last_name" required class="input lastname">
                                    <div class="line-box">
                                      <div class="line"></div>
                                  </div>
                              </label>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                            <label class="form-group">
                                <p class="label-txt">Email</p>
                                <input type="email" name="email" required  class="input emailid">
                                <div class="line-box">
                                  <div class="line"></div>
                              </div>
                          </label>
                      </div>
                      <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt">Phone</p>
                            <input type="tel" name="phone" required class="input phone">
                            <div class="line-box">
                              <div class="line"></div>
                          </div>
                      </label>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                    <label class="form-group">
                        <p class="label-txt">Password</p>
                        <input type="password" name="password" required  class="input password">
                        <div class="line-box">
                          <div class="line"></div>
                      </div>
                      <i>Only alphanumeric, @ and # allowed</i>
                  </label>                                    
              </div>
              <div class="col-md-6">
                <label class="form-group">
                    <p class="label-txt">Confirm Password</p>
                    <input type="password" name="confirm_password" required class="input cpassword">
                    <div class="line-box">
                      <div class="line"></div>
                  </div>
                  <div class="passError" style="display: none;">
                    <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                </div>
            </label>  
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label class="form-group">
                <p class="label-txt label-active">State</p>
                <input type="text" name="state"  required class="input" value="<?php echo ucwords($activeClients[$i]['state']); ?>">
                <div class="line-box">
                  <div class="line"></div>
              </div>
          </label>
      </div>
  </div>
  <div class="row">
    <div class="col-md-12">
        <label class="form-group">
            <p class="label-txt label-active">Select payment Method</p>
            <select class="input select-c" name="plan_name">
                <?php for ($i = 0; $i < sizeof($planlist); $i++) { ?>
                    <option value="<?php echo $planlist[$i]['plan_id']; ?>">
                        <?php echo $planlist[$i]['plan_name']; ?>
                    </option>';
                <?php } ?>
            </select>
            <div class="line-box">
              <div class="line"></div>
          </div>
      </label>
  </div>
</div>
<div class="form-group goup-x1">
    <div class="sound-signal text-left">
        <div class="form-radion2">
            <label class="containerr">
               <input value="0" type="checkbox" name="bypasspayment" class="form-check-input" id="bypasspayment">
               <span class="checkmark"></span> Bypass payment
           </label>
       </div>
   </div>
</div>
<div id="div_bypass">
   <label class="form-group">
    <p class="label-txt">Card Number</p>
    <input id="card_number" type="text" pattern="\d*" class="input" name="card_number" maxlength="16" required>
    <div class="line-box">
      <div class="line"></div>
  </div>
</label>
<div class="row">
    <div class="col-sm-6">
        <label class="form-group">
            <p class="label-txt">Expiry Date</p>
            <input id="expir_date" type="text" name="expir_date" class="input" onkeyup="dateFormat(this);" required>
            <div class="line-box">
              <div class="line"></div>
          </div>
      </label>
  </div>
  <div class="col-sm-6">
    <label class="form-group">
        <p class="label-txt">CVV</p>
        <input id="cvc" type="text" name="CVC" pattern="\d*" class="input" maxlength="4" required>
        <div class="line-box">
          <div class="line"></div>
      </div>
  </label>
</div>
</div> 
</div>
<p class="btn-x">
 <button type="submit" class="load_more button">Add Customer</button></p>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal similar-prop fade" id="AddPermaDesign" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Add Designer</h2>
                <div id="close-d" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">

                <div class="cli-ent-model">

                    <div class="noti-listpopup">
                        <div class="newsetionlist">
                            <div class="cli-ent-row tr notificate">
                                <div class="cli-ent-col td" style="width: 32%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Designer</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 43%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Skill</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 25%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b text-center">Active Requests</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="<?php echo base_url(); ?>admin/dashboard/assign_designer_for_customer" method="post">
                            <ul class="list-unstyled list-notificate two-can">
                                <?php foreach ($data['all_designers'] as $value): ?>
                                    <li>
                                        <a href="#">
                                            <div class="cli-ent-row tr notificate">
                                                <div class="cli-ent-col td" >
                                                    <div class="sound-signal">
                                                        <div class="form-radion">
                                                           <input class="selected_btn" type="radio" value="<?php echo $value['id']; ?>" name="assign_designer" id="<?php echo $value['id']; ?>" data-image-pic="<?php echo $value['profile_picture']?>" data-name="<?php echo $value['first_name'] ." ". $value['last_name'];?>">
                                                           <label for="<?php echo $value['id'];?>" data-image-pic="<?php echo $value['profile_picture']?>"></label>
                                                       </div>
                                                   </div>
                                               </div>

                                               <div class="cli-ent-col td" style="width: 30%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <div class="setnoti-fication">
                                                        <figure class="pro-circle-k1">
                                                            <img src="<?php echo $value['profile_picture'] ?>" class="img-responsive">
                                                        </figure>

                                                        <div class="notifitext">
                                                            <p class="ntifittext-z1"><strong><?php echo $value['first_name'] . " " . $value['last_name']; ?></strong></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="cli-ent-col td" style="width: 45%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <p class="pro-a">UX Designer, Landing page, Mobile App  UX Designer, Landing page, Mobile App<span class="sho-wred">+2</span></p>
                                                </div>
                                            </div>

                                            <div class="cli-ent-col td" style="width: 25%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <div class="cli-ent-xbox text-center">

                                                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $value['active_request']; ?></span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <input type="hidden" name="customer_id" id="assign_customer_id">
                        <p class="space-c"></p>
                        <p class="btn-x text-center"><button name="submit" type="submit" class="red_gz_btn button" id="assign_designer_per" >Assign Designer</button></p>
						<!--<p class="btn-x text-center"><button name="submit" type="submit" class="load_more button" id="assign_designer_per" >Assign Designer</button></p>-->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal fade similar-prop" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" name="id" id="id" value=""/>
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                        <h2 class="popup_h2 del-txt">Delete Customer</h2>
                        <div class="cross_popup" data-dismiss="modal"> x</div>
                    </header>
                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/delete-bg.svg">
                        <h3 class="head-c text-center">Are you sure you want to delete this customer?</h3>
                    <div class="confirmation_btn text-center">
                       <a href="javascript:voild(0)" class="btn btn-yes btn-ydelete" data-dismiss="modal" aria-label="Close">Delete</a>
                        <button class="btn btn-nq btn-ndelete" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<!-- Modal -->
<div class="modal similar-prop fade" id="Addva" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
              <h2 class="popup_h2 del-txt">Add VA</h2>
              <div id="close-pop" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
          </header>
          <div class="cli-ent-model-box">
            <div class="cli-ent-model">
                <div class="noti-listpopup">
                    <form action="" method="post">
                        <ul class="list-unstyled list-notificate two-can">
                            <?php foreach($data['all_va'] as $va){ ?>
                                <li>
                                    <a href="#">
                                        <div class="cli-ent-row tr notificate va-adder">
                                            <div class="cli-ent-col td" >
                                                <div class="sound-signal">
                                                    <div class="form-radion">
                                                       <input class="selected_btn" type="radio" value="<?php echo $va['id'];?>" name="assign_va" id="<?php echo $va['id'];?>" data-image-pic="<?php echo $va['profile_picture']?>" data-name="<?php echo $va['first_name'] ." ". $va['last_name'];?>">
                                                       <label for="<?php echo $va['id'];?>" data-image-pic="<?php echo $va['profile_picture']?>"></label>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="cli-ent-col td" style="width: 30%;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="setnoti-fication">
                                                    <figure class="pro-circle-k1">
                                                        <img src="<?php echo $va['profile_picture']?>" class="img-responsive">
                                                    </figure>

                                                    <div class="notifitext">
                                                        <p class="ntifittext-z1">
                                                            <strong>
                                                                <?php echo $va['first_name'] ." ". $va['last_name'];?>

                                                            </strong>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </a>
                            </li>  
                        <?php } ?>                          
                    </ul>
                    <input type="hidden" name="allcustomers_id" id="allcustomers_id">
                    <p class="space-c"></p>
                    <p class="btn-x text-center">
                        <button name="submit" type="submit" id="assign_vatocus" class="load_more button" data-dismiss="modal" aria-label="Close">Assign Va</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<button style="display: none;" id="confirmation" data-toggle="modal" data-target="#myModal">click here</button>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js');?>"></script>
<script type="text/javascript">
    var ASSIGN_PROJECT = '<?php echo isset($_GET['assign'])?$_GET['assign']:""; ?>';
            //    function ValidDate(d) {
            //        d = new String(d)
            //        var re1 = new RegExp("[0|1][0-9]\/[1-2][0-9]{3}");
            //        var re2 = new RegExp("^[1[3-9]]");
            //        var re3 = new RegExp("^00");
            //        var expir_date = re1.test(d) && (!re2.test(d)) && (!re3.test(d));
            //        if (expir_date === true) {
            //            $('#expir_date').val(d);
            //        } else {
            //            $('#expir_date').val("");
            //        }
            //    }

            //    $(function () {
            //        $("#bypasspayment").click(function () {
            //            if ($(this).is(":checked")) {
            //                $(this).val(1);
            //                $("#div_bypass").hide();
            //                $('#expir_date').removeAttr('required');
            //                $('#card_number').removeAttr('required');
            //                $('#cvc').removeAttr('required');
            //            } else {
            //                $("#div_bypass").show();
            //                $(this).val(0);
            //                $('#expir_date').attr('required', 'required');
            //                $('#card_number').attr('required', 'required');
            //                $('#cvc').attr('required', 'required');
            //            }
            //        });
            //    });

            //    $(document).on('click', ".delete_customer", function () {
            //        var customer_id = $(this).attr('data-customerid');
            //        $('#id').val(customer_id);
            //        $('#confirmation').click();
            //    });

            //    $('.btn-yes').click(function () {
            //        var customer_id = $('#id').val();
            //        $.ajax({
            //            type: "POST",
            //            url: "<?php echo base_url(); ?>admin/dashboard/deletecustomer",
            //            data: {
            //                id: customer_id
            //            },
            //            success: function (data) {
            //                if (data == 1) {
            //                    location.reload();
            //                }
            //            }
            //        });
            //    });

            //    $(document).on('click', ".permanent_assign_design", function () {
            //        var customer_id = $(this).attr('data-customerid');
            //        $('#AddPermaDesign #assign_customer_id').val(customer_id);
            //    });

            //    function validate() {
            //        var email = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
            //        var fname = new RegExp('^[A-Za-z ]+$');
            //        var lname = new RegExp('^[A-Za-z ]+$');
            //        var mob = new RegExp('^[0-9]+$');
            //        var passwords = new RegExp('^[A-Za-z0-9@#]+$');
            //        //    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
            //        if (!fname.test($('.firstname').val())) {
            //
            //            $(".firstname").css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //            $(".firstname").focus();
            //            $('.epassError.frstname').css({"display": "block"});
            //            $('.epassError.frstname .alert').css({"display": "block"});
            //            return false;
            //        }
            //        else {
            //
            //            $(".firstname").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //            $('.epassError.frstname').css({"display": "none"});
            //            $('.epassError.frstname .alert').css({"display": "none"});
            //        }
            //        if (!lname.test($('.lastname').val())) {
            //
            //            $('.lastname').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //            $(".lastname").focus();
            //            $('.epassError.lstname').css({"display": "block"});
            //            $('.epassError.lstname .alert').css({"display": "block"});
            //            return false;
            //        }
            //        else {
            //
            //            $(".lastname").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //            $('.epassError.lstname').css({"display": "none"});
            //            $('.epassError.lstname .alert').css({"display": "none"});
            //        }
            //        if (!email.test($('.emailid').val())) {
            //
            //            $(".emailid").css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //            $(".emailid").focus();
            //            $('.epassError.mailadrs').css({"display": "block"});
            //            $('.epassError.mailadrs .alert').css({"display": "block"});
            //            return false;
            //        }
            //        else {
            //
            //            $(".emailid").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //            $('.epassError.mailadrs').css({"display": "none"});
            //            $('.epassError.mailadrs .alert').css({"display": "none"});
            //        }
            //        if (!mob.test($('.phone').val())) {
            //
            //            $(".phone").focus();
            //            $('.phone').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //            $('.epassError.phnnmr').css({"display": "block"});
            //            $('.epassError.phnnmr .alert').css({"display": "block"});
            //            return false;
            //        }
            //        else {
            //
            //            $(".phone").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //            $('.epassError.phnnmr').css({"display": "none"});
            //            $('.epassError.phnnmr .alert').css({"display": "none"});
            //        }
            //        if (!passwords.test($('.password').val())) {
            //
            //            $(".password").focus();
            //            $('.password').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //            return false;
            //        }
            //        else {
            //
            //            $(".password").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //        }
            //        if (!passwords.test($('.cpassword').val())) {
            //            $(".cpassword").focus();
            //            $('.cpassword').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //            return false;
            //        }
            //        else {
            //            if ($('.password').val() != $('.cpassword').val())
            //            {
            //                $(".password").focus();
            //                $('.password').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //                $('.cpassword').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //                $('.passError').css({"display": "block"});
            //                $('.passError .alert').css({"display": "block"});
            //                return false;
            //            }
            //            else {
            //                $(".password").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //                $(".cpassword").css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //                $('.passError').css({"display": "none"});
            //                $('.passError .alert').css({"display": "none"});
            //            }
            //        }
            //
            //        return true;
            //    }

            //                                function EditFormSQa(id) {
            //                                    var email = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
            //                                    var fname = new RegExp('^[A-Za-z ]+$');
            //                                    var lname = new RegExp('^[A-Za-z ]+$');
            //                                    var mob = new RegExp('^[0-9]+$');
            //                                    var password = new RegExp('^[A-Za-z0-9@#]+$');
            //                                    //var password = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
            //                                    if (!fname.test($('#' + id + ' .efirstname').val())) {
            //                                        $('#' + id + ' .efirstname').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //                                        $(".efirstname").focus();
            //                                        return false;
            //                                    }
            //                                    else {
            //                                        $('#' + id + ' .efirstname').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //                                    }
            //                                    if (!lname.test($('#' + id + ' .elastname').val())) {
            //                                        $('#' + id + ' .elastname').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //                                        $('#' + id + ' .elastname').focus();
            //                                        return false;
            //                                    }
            //                                    else {
            //                                        $('#' + id + ' .elastname').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //                                    }
            //                                    if (!email.test($('#' + id + ' .eemailid').val())) {
            //                                        $('#' + id + ' .eemailid').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //                                        $('#' + id + ' .eemailid').focus();
            //                                        return false;
            //                                    }
            //                                    else {
            //                                        $('#' + id + ' .eemailid').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //                                    }
            //                                    if (!mob.test($('#' + id + ' .ephone').val())) {
            //                                        $('#' + id + ' .ephone').focus();
            //                                        $('#' + id + ' .ephone').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //                                        return false;
            //                                    }
            //                                    else {
            //                                        $('#' + id + ' .ephone').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //                                    }
            //                                    if ($('#' + id + ' .epassword').val() != "") {
            //                                        if (!password.test($('#' + id + ' .epassword').val())) {
            //                                            $('#' + id + ' .epassword').focus();
            //                                            $('#' + id + ' .epassword').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //                                            return false;
            //                                        }
            //                                        else {
            //                                            $('#' + id + ' .epassword').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //                                        }
            //                                    }
            //                                    if ($('#' + id + ' .eCpassword').val() != "") {
            //                                        if ($('#' + id + ' .epassword').val() != $('.ecpassword').val())
            //                                        {
            //                                            $('#' + id + ' .epassword').focus();
            //                                            $('#' + id + ' .epassword').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //                                            $('#' + id + ' .ecpassword').css({"box-shadow": "0px 1px 6px #f00", "border": "1px solid #f00"});
            //                                            $('#' + id + ' .epassError').css({"display": "block"});
            //                                            $('.epassError .alert').css({"display": "block"});
            //                                            return false;
            //                                        }
            //                                        else {
            //                                            $('#' + id + ' .epassword').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //                                            $('#' + id + ' .ecpassword').css({"box-shadow": "0 0 2px #d7d8d8", "border": "1px solid #d7d8d8"});
            //                                        }
            //                                    }
            //                                    return true;
            //                                }


            /**************************my-load-more**********************************/



            //                                function load_more_clientlist(isonetime = NULL) {
            //                                    var tabid = $('#status_check li.active a').attr('href');
            //                                    var activeTabPane = $('.tab-pane.active');
            //                                    var status_scroll = $('#status_check li.active a').attr('data-status');
            //                                    var searchval = $('.search_text').val();
            //                                    //activeTabPane.attr('data-search-text', searchval);
            //                                    $.post('<?php echo site_url() ?>admin/dashboard/load_more_customer', {'group_no': 0, 'status': status_scroll, 'search': searchval,'isonetime':isonetime},
            //                                        function (data) {
            //                                            var newTotalCount = 0;
            //                                            if (data != '') {
            //                                                $(".ajax_searchload").fadeOut(500);
            //                                                $(tabid + " .product-list-show .add-rows .two-can").html(data);
            //                                                newTotalCount = $(tabid + " .product-list-show .add-rows").find("#loadingAjaxCount").attr('data-value');
            //                                                $(tabid + " .product-list-show .add-rows").find("#loadingAjaxCount").remove();
            //                                                activeTabPane.attr("data-total-count", newTotalCount);
            //                                                activeTabPane.attr("data-loaded", $rowperpage);
            //                                                activeTabPane.attr("data-group", 1);
            //                                            } else {
            //                                                activeTabPane.attr("data-total-count", 0);
            //                                                activeTabPane.attr("data-loaded", 0);
            //                                                activeTabPane.attr("data-group", 1);
            //                                                $(tabid + ".product-list-show .add-rows").html("");
            //                                            }
            //                                            if ($rowperpage >= newTotalCount) {
            //                                                activeTabPane.find('.load_more').css("display", "none");
            //                                            } else {
            //                                                activeTabPane.find('.load_more').css("display", "inline-block");
            //                                            }
            //                                        });
            //                                }


            //                                $('.searchdata_client').keyup(function (e) {
            //                                 var onetime = $('.tab-content .active').find('.search_text').data('isonetime');
            //                                 //console.log(onetime)
            //                                 e.preventDefault();
            //                                 delay(function () {
            //                                    $(".ajax_searchload").fadeIn(500);
            //                                        //console.log(onetime);
            //                                        if(onetime == 1){
            //                                            load_more_clientlist(onetime);
            //                                        }else{
            //                                            load_more_clientlist('');
            //                                        }
            //                                    }, 1000);
            //                             });
            //                                var delay = (function () {
            //                                    var timer = 0;
            //                                    return function (callback, ms) {
            //                                        clearTimeout(timer);
            //                                        timer = setTimeout(callback, ms);
            //                                    };
            //                                })();

            //$('.search_client_ajax').click(function (e) {
            //    e.preventDefault();
            //    var onetime = $('.tab-content .active').find('.search_text').data('isonetime');
            //   // console.log(onetime);
            //   if(onetime == 1){
            //    load_more_clientlist(onetime);
            //}else{
            //   load_more_clientlist('');
            //}
            //});

            //$(document).ready(function () {
            //    var status = $('#status_check_client li.active a').attr('data-status');
            //    var id = $('#status_check_client li.active a').attr('href');
            //    //var total_pagin = $('#status_check_client li.active a').attr('data-pagin');
            //    //var total_record = 0;
            //    $rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
            //    $('.tab-pane').attr("data-loaded", $rowperpage);
            //    // var total_groups = total_pagin;
            //    //total_record++;
            //    //$(window).scroll(function () {
            //        $(document).on("click", "#load_more_client", function () {
            //            $(".ajax_loader").css("display", "block");
            //            $(".load_more").css("display", "none");
            //            var isonetime;
            //            if(($(this).data('is_single')) == '1'){
            //                isonetime = '1';
            //            }else{
            //                isonetime = '0';
            //            }
            //    var row = parseInt($(this).attr('data-row'));
            //        ///var allcount = parseInt($(this).attr('data-count'));
            //        row = row + $rowperpage;
            //        var activeTabPane = $('.tab-pane.active');
            //        var searchval = activeTabPane.find('.search_text').val();
            //        var allcount = parseInt(activeTabPane.attr("data-total-count"));
            //        var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
            //        var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
            //        var status_scroll = $('#status_check_client li.active a').attr('data-status');
            //        var tabid = $('#status_check_client li.active a').attr('href');
            //        if (allLoaded < allcount) {
            //            $.post('<?php echo site_url() ?>admin/dashboard/load_more_customer', {'group_no': activeTabPaneGroupNumber, 'status': status_scroll, 'search': searchval,'isonetime':isonetime},
            //                function (data) {
            //                    if (data != "") {
            //                        console.log(data);
            //                        $(tabid + " .product-list-show .add-rows .two-can").append(data);
            //                        row = row + $rowperpage;
            //                        $(".ajax_loader").css("display", "none");
            //                        $(".load_more").css("display", "inline-block");
            //                        activeTabPane.find('.load_more').attr('data-row', row);
            //                        activeTabPaneGroupNumber++;
            //                        activeTabPane.attr('data-group', activeTabPaneGroupNumber);
            //                        allLoaded = allLoaded + $rowperpage;
            //                        activeTabPane.attr('data-loaded', allLoaded);
            //                        if (allLoaded >= allcount) {
            //                            activeTabPane.find('.load_more').css("display", "none");
            //                            $(".ajax_loader").css("display", "none");
            //                        } else {
            //                            activeTabPane.find('.load_more').css("display", "inline-block");
            //                            $(".ajax_loader").css("display", "none");
            //                        }
            //                    }
            //                });
            //        }
            //        //} else {
            //        //}
            //        // }
            //    });
            //});

            //$(document).on('change', '.cust_status', function () {
            //                                    // $('.cust_status').on('change',function() {
            //                                        var status = $(this).prop('checked');
            //                                        var data_id = $(this).attr('data-id');
            //                                        $.ajax({
            //                                            type: 'POST',
            //                                            url: "<?php echo base_url(); ?>admin/dashboard/change_customer_status_active_inactive",
            //                                            data: {status: status, data_id: data_id},
            //                                            success: function (data) {
            //                                            }
            //                                        });
            //                                    });

            //$(document).ready(function () {
            //    $('.switch').each(function () {
            //        var checkbox = $(this).children('input[type=checkbox]');
            //        var toggle = $(this).children('label.switch-toggle');
            //        if (checkbox.length) {
            //            checkbox.addClass('hidden');
            //            toggle.removeClass('hidden');
            //            if (checkbox[0].checked) {
            //                toggle.addClass('on');
            //                toggle.removeClass('off');
            //                toggle.text(toggle.attr('data-on'));
            //            }
            //            else {
            //                toggle.addClass('off');
            //                toggle.removeClass('on');
            //                toggle.text(toggle.attr('data-off'));
            //            }
            //            ;
            //        }
            //    });      
            //    $(document).on('change', '.switch-custom input[type="checkbox"]', function () {
            //        var status = $(this).prop('checked');
            //        var data_id = $(this).attr('data-cid');
            //        var ischecked = ($(this).is(':checked')) ? 1 : 0;
            //        if (ischecked) {
            //            $(".checkstatus_" + data_id).text('Active');
            //        } else {
            //            $(".checkstatus_" + data_id).text('Inactive');
            //        }
            //        $.ajax({
            //            type: 'POST',
            //            url: "<?php echo base_url(); ?>admin/dashboard/change_customer_status_active_inactive",
            //            data: {status: status, data_id: data_id},
            //            success: function (data) {
            //            }
            //        });
            //    });
            //});

            //$(document).on('click','.see_subuser',function (){
            //    var custID = $(this).attr('data-custid');
            //    $.ajax({
            //        type: 'POST',
            //        url: "<?php echo base_url(); ?>admin/dashboard/getSubusers",
            //        data: {cust_id: custID},
            //        success: function (data) {
            //            $('#ShowSubusers').html(data);
            //        }
            //    });
            //
            //});

            //$(document).on('change','.show_onetime_client',function(){
            //var is_checked;
            //if($(this).is(':checked')){
            //    is_checked = '1';
            //}else{
            //    is_checked = '0';
            //}
            //load_more_clientlist(is_checked);
            //});
        </script> 
