<!-- <style type="text/css" media="screen">
/*****calender*****/
.taxt_error_msg {
    text-align: center;
    font-size: 20px;
    font-weight: 700;
    margin-top: 172px;
}
.tp-amount p {
    display: inline-block;
}
#Tax_report .taxt_error_msg {
    text-align: center;
    font-size: 24px;
    margin-top: 70px;
}
.graph-header input {
    margin-bottom: 0;
    padding: 4px;
    border: 1px solid #ccc;
    border-radius: 4px;
}
/*****calender*****/

.tool-hover {
    display: inline-block;
    margin-left: 4px;
    position: relative;
}
.tool-hover:hover .tool-text {
    display: block !important;
}
.tool-hover .tool-text {
    display: none;
    position: absolute;
    background: rgb(37, 56, 88);
    color: #fff;
    font-size: 12px;
    min-width: 240px;
    border-radius: 12px;
    padding: 12px;
    right: -20px;
    top: 40px;
    text-align: center;
    border: 1px solid #253858;
    z-index: 999;
}
.tool-hover .tool-text::after {
    content: "";
    position: absolute;
    top: -28px;
    margin-left: -5px;
    border-width: 15px;
    border-style: solid;
    border-color: transparent transparent #253858 transparent;
    right: 14px;
}

#Tax_report{
    overflow: visible !important;
    height: 387px;
    /* margin-top: 100px; */
    margin-right: 100px;
}
div#myChart-license-text {
    display: none;
}
/*===== main CSS =====*/

.active-designer {
    background: #fff;
    padding: 15px;
    text-align: center;
    border-radius: 8px;
}
.active-designer h3 {
    margin: 0;
    font-size: 50px;
    font-family: 'GothamPro-Medium', sans-serif;
    font-weight: 900;
    color: #000;
}

/*===== main CSS =====*/

.center {
    left: 50%;
    -webkit-transform: translate( -50% );
    -ms-transform: translate( -50% );
    transform: translate( -50% );
}

/*===== The CSS =====*/
.progress{
    width: 200px;
    height: 280px;
}
.progress .track, .progress .fill{
    stroke-width: 6;
    transform: rotate(90deg)translate(0px, -80px);
}
.progress .track{
    stroke: rgb(226, 226, 226);fill: transparent;
}
.progress .fill {
    stroke: rgb(255, 255, 255);
    stroke-dasharray: 219.99078369140625;
    stroke-dashoffset: -219.99078369140625;
    transition: stroke-dashoffset 1s;
}
.progress.blue .fill {
    stroke: #0e3087;
    fill: transparent;
}
.progress.green .fill {
    stroke: #1ab125;
    fill: transparent;
}
.progress.red .fill {
    stroke: #e52445;
    fill: transparent;
}
.progress.orange .fill {
    stroke: #ff5400;
    fill: transparent;
}
.progress .value, .progress .text {
    fill:rgb(103, 214, 198);
    text-anchor: middle;
}
.graph-subheader {
    float: right;
    width: 100%;
}
.progress.green .value {fill:#1ab125;}
.progress.red .value {fill:#e52445;}
.progress.orange .value {fill:#ff5400;}
.progress.blue .value {fill:#0e3087;}
.progress .text {	font-size: 14px;}
.noselect {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    cursor: default;
}
</style> -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php 
$today = date('m/d/Y');
    //$sevendayback = date('m/d/Y', strtotime('-6 days'));
$ts = strtotime($today);
$start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
$start_date = date('m/d/Y', $start);
$end_date = date('m/d/Y', strtotime('next saturday', $start));
?>
<div class="report-dashboard">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="flex-this">
                    <h2 class="main_page_heading">Reporting</h2>
                </div>
                <br/>
                <ul class="nav nav-tabs tabtop  tabsetting">
                    <li class="active"> <a href="#tab_default_1" data-toggle="tab"> Project Overview </a> </li>
                    <li> <a href="#tab_default_2" data-toggle="tab"> Designer Overview</a> </li>
                    <li class=""><a data-toggle="tab" href="#Tax_report_section" role="tab">Taxes Report</a></li>
                    <li class=""><a data-toggle="tab" href="#Messages" role="tab">Messages</a></li>
                </ul>

            </div>
            <div class="tab-content">
                <div class="tab-pane active fade in" id="tab_default_1">

                    <div class="col-md-12">
                        <div class="tabs-card">
                            <h3>Analytics Overview</h3>
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="analytic-box bgk-green">
                                        <h4><?php echo $all_project_count['active_disapprove_project']; ?></h4>
                                        <div class="tp-amount"><p>Active Projects</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> all active and revision status</div></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="analytic-box bgk-red">
                                        <h4><?php echo $all_project_count['late_project']; ?></h4>
                                        <div class="tp-amount"><p>Late Projects</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> Expired estimated date and project still in active stage</div></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="analytic-box bgk-orange">
                                        <h4><?php echo $all_project_count['pending_project']; ?></h4>
                                        <div class="tp-amount"><p>Pending Review</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> Designer have upload designs but approval pending from admin</div></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="analytic-box bgk-blue">
                                        <h4><?php echo $all_project_count['review_project']; ?></h4>
                                        <div class="tp-amount"><p>Pending Approval</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> all project that's approval pending from customer</div></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tabs-card">
                            <h3>Project Status Overview</h3>
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="project-stat green">
                                        <div class="stat-cont">
                                            <div class="icon-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/reporting_tick.svg">
                                            </div>
                                            <div class="data">
                                                <h5 class="mck-green"><?php echo $all_project_count['approved_project']; ?></h5>
                                                <div class="tp-amount"><p>Completed Projects</p>
                                                    <div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> all project status exclude draft</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="range-stst">
                                      <div class="progress">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0"
                                        aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $all_project_count['completed_percent'].'%'; ?>">
                                          <?php echo $all_project_count['completed_percent'].'%'; ?>
                                        </div>
                                      </div>
                                   </div>
                               </div>
                           </div>
                           <div class="col-sm-6 col-md-3">
                            <div class="project-stat purple">
                                <div class="stat-cont">
                                    <div class="icon-img">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/reporting_progress.svg">
                                    </div>
                                    <div class="data">
                                        <h5 class="mck-red"><?php echo $all_project_count['active_project']; ?></h5>
                                        <div class="tp-amount"><p>In Progress Projects</p>
                                            <div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> all project status exclude draft,assign and approved</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="range-stst">
                               <div class="progress">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0"
                                aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $all_project_count['active_project_percent'].'%'; ?>">
                                  <?php echo $all_project_count['active_project_percent'].'%'; ?>
                                </div>
                              </div>
                           </div>
                       </div>
                   </div>
                   <div class="col-sm-6 col-md-3">
                    <div class="project-stat orange">
                        <div class="stat-cont">
                            <div class="icon-img">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/reporting_revision.svg">
                            </div>
                            <div class="data">
                                <h5 class="mck-orange"><?php echo $all_project_count['disapprove_project']; ?></h5>
                                <div class="tp-amount"><p>In Revision</p>
                                    <div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> all project status exclude draft,assign and approved</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="range-stst">
                        <div class="progress">
                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0"
                            aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $all_project_count['disapprove_project_percent'].'%'; ?>">
                              <?php echo $all_project_count['disapprove_project_percent'].'%'; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="project-stat blue">
                    <div class="stat-cont">
                        <div class="icon-img">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/reporting_quality_revision.svg">
                        </div>
                        <div class="data">
                            <h5 class="mck-blue"><?php echo $all_project_count['quality_revision_project']; ?></h5>
                            <div class="tp-amount"><p>Quality Revision</p>
                                <div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> all project status exclude draft,assign and approved</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="range-stst">
                  <div class="progress">
                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0"
                    aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $all_project_count['late_project_percent'].'%'; ?>">
                      <?php echo $all_project_count['late_project_percent'].'%'; ?>
                    </div>
                    </div>
               </div>
           </div>
       </div>
   </div>
</div>


<div class="tabs-card">
    <div class="row">
       <div class="col-sm-4">
        <ul class="project-designer">
            <li>
                <span><?php echo $all_project_count['customer_avg']; ?></span>
                <p>Avg. Customers assigned to Designer</p>
            </li>
            <li>
                <span><?php echo $all_project_count['designer_avg']; ?></span>
                <p>Avg. Designs Per Designer</p>
            </li>
            <li>
                <span><?php echo $all_project_count['avgdaysforcompletion']; ?></span>
                <p>Avg. Days for completion of Request</p>
            </li>
        </ul>
    </div>
    <div class="col-sm-8">
        <div class="heading-header">
            <select class="new-project">
                <option value="new_project">New Project</option> 
                <option value="in_progress">In Progress</option>
            </select>
            <div class="graph-header">
                <span>Project Date </span>
                <input type="text" class="week-picker" value="<?php echo $today; ?>"/>
                <span id="startDate"><?php echo $start_date; ?></span><span class='hiphen_hide'> - </span><span id="endDate"><?php echo $end_date; ?></span>
            </div>
        </div>
        <div id="myChart" style="height: 300px; width: 100%;"></div>
    </div>
</div>
</div>
<div class="tabs-card">
    <div class="row">
        <div class="col-sm-12">
            <div class="graph-header">
                <h3>Project Categories</h3>
                <select id='requests_category_count'>
                    <option value='active,disapprove,checkforapprove'>Active Projects</option>
                    <option value='all'>Submitted Projects</option>
                </select>
            </div>
            <div class="graph-sec">
                <ul class="designer-graph catgory_based_count">
                    <?php foreach ($req_based_oncategory as $categoryname => $categoryvalsue) { ?>
                        <li><?php echo $categoryname; ?> <span><?php echo $categoryvalsue; ?></span></li>
                    <?php } ?>
                </ul>
            </div>

        </div>
    </div>
</div>
<div class="tabs-card">
    <div class="row">
        <div class="col-sm-6">
            <div class="heading-header">
             <h3>New Users</h3>
             <div class="graph-subheader">
                <span>Users</span>
                <input type="text" class="week-picker_customers" value="<?php echo $today; ?>"/>
                <span id="users_startDate"><?php echo $start_date; ?></span><span class='users_hiphen_hide'>-</span><span id="users_endDate"><?php echo $end_date; ?></span>
            </div>
        </div>
        <div id="Newusers"></div>
    </div>
    <div class="col-sm-6">
        <div class="heading-header">
         <h3>UTM Users</h3>
         <div class="graph-subheader">
            <span>Users</span>
            <input type="text" class="week-picker_utmusers" value="<?php echo $today; ?>"/>

            <span id="utmusers_startDate"><?php echo $start_date; ?></span><span class='utmusers_hiphen_hide'>-</span><span id="utmusers_endDate"><?php echo $end_date; ?></span>
        </div>
    </div>
    <div id="Utmusers"></div>
</div>
</div>
</div>

</div>
</div>
<div class="tab-pane fade" id="tab_default_2">
    <div class="col-md-12">	
        <div class="tabs-card">
            <h3>Designer List</h3>
            <div class="datepicker_tax_sec">
                <form class="date-report" id="datepicker_range" action="" method="POST">
                    <p>Start Date: <input type="text" id="datepicker_start" readonly name="start_date" class="form-control"></p>
                    <p>End Date: <input type="text" id="datepicker_end" readonly name="end_date" class="form-control"></p>
                    <input type="submit" name="get_designer_average" id="get_designer_average">
                </form>
            </div>
        </div>
        <div class="row">
            <?php 
            foreach($designerdetail as $dkey => $dval){
                if($dval['profile_picture'] != NULL){
                if(file_get_contents(FS_PATH. FS_UPLOAD_PUBLIC_UPLOADS_PROFILE ."_thumb/". $dval['profile_picture'])){
                    $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE ."_thumb/".$dval['profile_picture'];
                }else{
                    $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE .$dval['profile_picture'];
                }
                }else{
                    $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE ."user-admin.png";
                }
                ?>
                <div class="col-sm-12">
                    <div class="designer-list-here">
                        <div class="img-header">
                                <img src="<?php echo $path; ?>" class="img-responsive">
                            <h3>Average quality reviewed <span>0%</span></h3>
                        </div>
                        <div class="designer-data">
                           <h4><?php echo $dval['first_name']. ' '.$dval['last_name']  ?></h4>
                           <ul>
                            <li>Active Projects<span><?php echo (!empty($dval['total']))? $dval['total']: '0';?></span></li>
                            <li>In Progress<span><?php echo (!empty($dval['active']))?$dval['active']:'0';?></span></li>
                            <li>In Revision<span><?php echo (!empty($dval['disapprove']))?$dval['disapprove']:'0';?></span></li>
                            <li>Late Projects<span><?php echo (!empty($dval['lateprojectCount']))?$dval['lateprojectCount']:'0';?></span></li>
                            <li>Pending Approval<span><?php echo (!empty($dval['checkforapprove']))?$dval['checkforapprove']:'0';?></span></li>
                            <li>Completed Designs<span><?php echo (!empty($dval['approved']))?$dval['approved']:'0';?></span></li>
                            <li>Time Quality Reviewed<span><?php echo (!empty($dval['gettimeReviews']))?$dval['gettimeReviews']:'0';?></span></li>
                            <li>Curr. assigned requests count<span><?php echo (!empty($dval['currentproject']))?$dval['currentproject']:'0';?></span></li>
                            <li>Total assign customer<span><?php echo (!empty($dval['permamnentCustomercount']))?$dval['permamnentCustomercount']:'0';?></span></li>
                            <li>Avg. designs per day<span class="avgperday_<?php echo $dval['id'];?>"><?php echo (!empty($dval['avgperday']))?$dval['avgperday']:'0';?></span></li>
                            <li>Average days to complete design<span class="avgdaysforcompletion_<?php echo $dval['id'];?>">
                             <?php
                             if(!empty($dval['avgdaysforcompletion']) && is_finite($dval['avgdaysforcompletion']) && $dval['avgdaysforcompletion'] >= 1){
                                 echo $dval['avgdaysforcompletion'];
                             }else{
                                echo 0; 
                            }?>
                        </span></li>
                        <li>Avg. number of design drafts<span class="avgdraft_<?php echo $dval['id'];?>"><?php echo (!empty($dval['avgdraft']))?$dval['avgdraft']:'0';?></span></li>
                        <li>Average quality reviewed<span class="avg_quality_<?php echo $dval['id'];?>"><?php echo (!empty($dval['average_total_quality']) && is_finite($dval['average_total_quality']))?$dval['average_total_quality'].'%':'0';?></span></li>
                    </ul>
                </div>
            </div>
        </div>

    <?php } ?>
</div>
</div>
</div>
<div class="tab-pane" id="Tax_report_section" role="tabpanel">
    <div class="col-md-12">
        <div class="tabs-card">
            <h3>Taxes Overview</h3>
            <div class="row custom-stock">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="analytic-box bgk-darkblue">
                        <h4><?php echo $allusers; ?></h4>
                        <div class="tp-amount"> <p>All Users</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> All customer</div></div></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="analytic-box bgk-green">
                        <h4><?php echo $taxinternationalUsers; ?></h4>
                        <div class="tp-amount"><p>International Users</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> All customers excluded USA customers</div></div></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="analytic-box bgk-red">
                        <h4><?php echo $usausers; ?></h4>
                        <div class="tp-amount"><p>USA Users</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> All USA customers</div></div></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="analytic-box bgk-orange">
                        <h4><?php echo $taxusers; ?></h4>
                        <div class="tp-amount"> <p>Texas Users</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> Customers from texas</div></div></div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="analytic-box bgk-blue">
                        <h4><?php echo round($taxamount); ?></h4>
                        <div class="tp-amount"><p>Tax Amount</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> Total Tax amount from texas</div></div></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tabs-card">
            <div class="row">
                <div class="">
                    <div class="graph-header">
                        <h3>Total Tax Paid</h3>
                        <div class="datepicker_tax_sec">
                            <form class="date-report" id="taxes_datepicker_range" action="" method="POST">
                                <p>Start Date: <input type="text" id="taxes_start" readonly name="start_date" class="form-control"></p>
                                <p>End Date: <input type="text" id="taxes_end" readonly name="end_date" class="form-control"></p>
                                <input type="submit" name="get_designer_average" id="get_taxes_amount">
                            </form>
                        </div>
                    </div>
                    <div class="txable-amt">
                        <ul class="designer-graph state_based_taxcount">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="tabs-card">
            <h3>Taxes Line Chart</h3>
            <?php
            $today = date('Y-m-d');
            $currennt_week = date('Y-m-d', strtotime('monday this week'));

                    //Current month
                    $first_day_this_month = date('Y-m-01'); // hard-coded '01' for first day
                    $last_day_this_month = date('Y-m-t');

                    //Last Week
                    $previous_week = strtotime("-1 week +1 day");
                    $start_week = strtotime("last monday", $previous_week);
                    $end_week = strtotime("next sunday", $start_week);
                    $start_week_date = date('Y-m-d', $start_week);
                    $end_week_date = date('Y-m-d', $end_week);

                    //Last Month
                    $last_month_start = date('Y-m-d', strtotime('first day of last month'));
                    $last_month_end = date('Y-m-d', strtotime('last day of last month'));
                    $currentMonth = date('M Y');
                    $last_month_name = Date('M Y', strtotime($currentMonth . " last month"));

                    ?>
                    <div class="date-select datepicker">
                        <select class="filter_chart" id="filter_chart" name="filter_chart">
                            <option value="<?php echo $currennt_week; ?>" data-date="<?php echo $today; ?>">Current Week</option>
                            <option value="<?php echo $start_week_date; ?>" data-date="<?php echo $end_week_date; ?>">Last Week</option>
                            <option value="<?php echo $first_day_this_month; ?>" data-date="<?php echo $last_day_this_month; ?>">Current Month</option>
                            <option value="<?php echo $last_month_start; ?>" data-date="<?php echo $last_month_end; ?>">Last Month(<?php echo $last_month_name; ?>)</option>
                            <option value="range">Date Range</option>
                        </select>
                        <div class="datepicker_tax_sec" style="display:none">
                            <form class="date-report" id="datepicker_tax" action="" method="POST">
                                <p>Start Date: <input type="text" id="tax_datepicker_start" readonly name="start_date" class="form-control"></p>
                                <p>End Date: <input type="text" id="tax_datepicker_end" readonly name="end_date" class="form-control"></p>
                                <input type="submit" name="get_reports">
                            </form>
                        </div>
                    </div>
                    <div id="Tax_report" style="height: 300px; width: 100%;">
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="Messages" role="tabpanel">
            <div class="col-md-12">
                <div class="tabs-card">
                    <h3>Messages Overview</h3>
                    <div class="row">
                     <div class="datepicker_tax_sec">
                        <form class="date-report" id="msg_datepicker_range" action="" method="POST">
                            <p>Start Date: <input type="text" id="msgstart_date" readonly name="msgstart_date" class="form-control" value="<?php echo date("Y-m-d"); ?>"></p>
                            <p>End Date: <input type="text" id="endmsg_date" readonly name="endmsg_date" class="form-control" value="<?php echo date("Y-m-d"); ?>"></p>
                            <input type="submit" name="get_msg_range" id="get_msg_range">
                        </form>
                    </div>
                </div>
            </div>
            <div class="tabs-card marge-it">
             <div class="row custom-stock">
                <div class="col-md-4 col-sm-6">
                    <div class="analytic-box bgk-blue all_msg">
                        <h4><?php echo $messages['all']; ?></h4>
                        <div class="tp-amount"> <p>Total Messages</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> All messages</div></div></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="analytic-box bgk-green customer_msg">
                        <h4><?php echo $messages['customer']; ?></h4>
                        <div class="tp-amount"><p>Total Client Messages</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> All messages of customers</div></div></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="analytic-box bgk-red team_msg">
                        <h4><?php echo $messages['team']; ?></h4>
                        <div class="tp-amount"><p>Total GZ Team Messages</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> All messages of GZ team</div></div></div>
                    </div>
                </div> 
                <div class="col-md-4 col-sm-6">
                    <div class="analytic-box bgk-orange respond_msg">
                        <h4><?php echo $messages['team']; ?></h4>
                        <div class="tp-amount"> <p>Average time to respond on messages</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> Average time to respond on messages</div></div></div>
                    </div>
                </div>
                <div class="col-md-4  col-sm-6">
                    <div class="analytic-box bgk-orange discuss_msg">
                        <h4><?php echo $messages['discussion']; ?></h4>
                        <div class="tp-amount"> <p>Total Discussed projects</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> All discussed projects</div></div></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="analytic-box bgk-red delay_msg">
                        <h4><?php echo $messages['team']; ?></h4>
                        <div class="tp-amount"><p>Number of delays</p><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text"> Number of delays</div></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/jquery/dist/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/qa/bootstrap.min.js');?>"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'js/admin/admin_portal.js'); ?>"></script>
<script>
//    var forEach = function (array, callback, scope) {
//        for (var i = 0; i < array.length; i++) {
//            callback.call(scope, i, array[i]);
//        }
//    };
//    window.onload = function () {
//        var max = -219.99078369140625;
//        forEach(document.querySelectorAll('.progress'), function (index, value) {
//            percent = value.getAttribute('data-progress');
//            value.querySelector('.fill').setAttribute('style', 'stroke-dashoffset: ' + ((100 - percent) / 100) * max);
//            value.querySelector('.value').innerHTML = percent + '%';
//        });
//    }


</script>

<script>
    var chart = new CanvasJS.Chart("myChart", {
     animationEnabled: true,
     theme: "light2",
     title:{
      text: ""
  },
  axisY:{
      includeZero: false
  },
  data: [{        
      type: "spline",       
      dataPoints: <?php echo json_encode($getLinechartdata, JSON_NUMERIC_CHECK); ?>
  }]
});
    chart.render();

    var taxreport = new CanvasJS.Chart("Tax_report", {
     animationEnabled: true,
     theme: "light2",
     title:{
      text: ""
  },
  axisY:{
      includeZero: false
  },
  data: [{        
      type: "spline",       
      dataPoints: <?php echo json_encode($tax_report, JSON_NUMERIC_CHECK); ?>
  }]
});
    taxreport.render();


    var weeklyusers = new CanvasJS.Chart("Newusers", {
     animationEnabled: true,
     theme: "light2",
     title:{
      text: ""
  },
  axisY:{
      includeZero: false
  },
  data: [{        
      type: "spline",       
      dataPoints: <?php echo json_encode($getusersweekdata, JSON_NUMERIC_CHECK); ?>
  }]
});
    weeklyusers.render();

    var weeklyutmusers = new CanvasJS.Chart("Utmusers", {
     animationEnabled: true,
     theme: "light2",
     title:{
      text: ""
  },
  axisY:{
      includeZero: false
  },
  data: [{        
      type: "column",       
      dataPoints: <?php echo json_encode($getutmusersweekdata, JSON_NUMERIC_CHECK); ?>
  }]
});
    weeklyutmusers.render();

//    $('#requests_category_count').on('change', function () {
//        var value = $(this).val();
//        //console.log(value);
//        $.ajax({
//            type: 'POST',
//            url: '<?php echo base_url(); ?>admin/report/dashboard',
//            dataType: "json",
//            async: false,
//            data: {"value": value, "category_flag": 1},
//            success: function (response) {
//                console.log(response);
//                var categorydata = '';
//                $.each(response, function (k, v) {
//                    categorydata += '<li>' + k + '<span>' + v + '</span></li>';
//                });
//                $('.catgory_based_count').html(categorydata);
//            },
//            error: function (response) {
//                console.log('error response', response);
//            }
//        });
//
//
//    });
    
//    $('.filter_chart').on('change', function () {
//        var start_date = $(this).val();
//        var end_date = $('option:selected', this).attr('data-date');
//        //console.log(start_date);
//        if (start_date == 'range') {
//            $('.datepicker_tax_sec').show();
//            $('#tax_datepicker_start').val('');
//            $('#tax_datepicker_end').val('');
//        } else {
//            $('.datepicker_tax_sec').hide();
//            $.ajax({
//                type: 'POST',
//                url: '<?php echo base_url(); ?>admin/report/tax_report',
//                dataType: "json",
//                async: false,
//                data: {"start_date": start_date, "end_date": end_date},
//                success: function (response) {
//                    if (response.status == '1') {
//                        var taxreport = new CanvasJS.Chart("Tax_report", {
//                            animationEnabled: true,
//                            theme: "light2",
//                            title:{
//                                text: ""
//                            },
//                            axisY:{
//                                includeZero: false
//                            },
//                            data: [{        
//                                type: "spline",       
//                                dataPoints: response.tax_report
//                            }]
//                        });
//                        taxreport.render();
//                    } else {
//                        $('#Tax_report').html('<div class="taxt_error_msg">Data Not Found</div>');
//                    }
//                },
//                error: function (response) {
//                    console.log('error response', response);
//                }
//            });
//        }
//    });
//    $('#datepicker_tax').on('submit', function (e) {
//        e.preventDefault();
//        var datepicker_start = $('#tax_datepicker_start').val();
//        var datepicker_end = $('#tax_datepicker_end').val();
//        $.ajax({
//            type: 'POST',
//            url: '<?php echo base_url(); ?>admin/report/tax_report',
//            dataType: "json",
//            data: {"start_date": datepicker_start, "end_date": datepicker_end},
//            success: function (response) {
//               if (response.status == '1') {
//                var taxreport = new CanvasJS.Chart("Tax_report", {
//                    animationEnabled: true,
//                    theme: "light2",
//                    title:{
//                        text: ""
//                    },
//                    axisY:{
//                        includeZero: false
//                    },
//                    data: [{        
//                        type: "spline",       
//                        dataPoints: response.tax_report
//                    }]
//                });
//                taxreport.render();
//            } else {
//                    //console.log('kdfgidfiuyd');  
//                    $('#Tax_report').html('<div class="taxt_error_msg">Data Not Found</div>');
//                }
//            },
//            error: function (response) { 
//            }
//        });
//    });
//    $(document).ready(function () {
//        $("#tax_datepicker_start").datepicker({
//            maxDate: new Date(),
//            dateFormat: 'yy-mm-dd',
//            onSelect: function (dateText, inst) {
//                var date = $(this).val();
//                $('#tax_datepicker_start').val(date);
//            }
//        });
//        $("#tax_datepicker_end").datepicker({
//            maxDate: new Date(),
//            dateFormat: 'yy-mm-dd',
//            onSelect: function (dateText, inst) {
//                var date = $(this).val();
//                $('#tax_datepicker_end').val(date);
//            }
//        });
//    });
    
    
//    var selectCurrentWeek = function () {
//        window.setTimeout(function () {
//            $('.ui-datepicker-calendar > tr').find('.ui-datepicker-current-day > a').addClass('ui-state-active');
//        }, 10000);
//    }

//    $(function () {
//        var startDate;
//        var endDate;
//        $('.week-picker').datepicker({
//            showOtherMonths: true,
//            selectOtherMonths: true,
//            onSelect: function (dateText, inst) {
//                var date = $(this).datepicker('getDate');
//                startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
//                endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
//                var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
//                $('#startDate').text($.datepicker.formatDate(dateFormat, startDate, inst.settings));
//                $('#endDate').text($.datepicker.formatDate(dateFormat, endDate, inst.settings));
//                $('.hiphen_hide').css('display','inline-block');
//                var start = $('#startDate').text();
//                var end = $('#endDate').text();
//                var selected_pro_type = $('.new-project').val();
//                selectCurrentWeek();
//                getWeekdate(start,end,selected_pro_type);
//            },
//            beforeShowDay: function (date) {
//                var cssClass = '';
//                if (date >= startDate && date <= endDate)
//                    cssClass = 'ui-datepicker-current-day';
//                return [true, cssClass];
//            },
//            onChangeMonthYear: function (year, month, inst) {
//                selectCurrentWeek();
//            }
//        });
//
//    });
    
//    $(function () {
//        var startDate;
//        var endDate;
//        $('.week-picker_customers').datepicker({
//            showOtherMonths: true,
//            selectOtherMonths: true,
//            onSelect: function (dateText, inst) {
//                var date = $(this).datepicker('getDate');
//                startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
//                endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
//                var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
//                $('#users_startDate').text($.datepicker.formatDate(dateFormat, startDate, inst.settings));
//                $('#users_endDate').text($.datepicker.formatDate(dateFormat, endDate, inst.settings));
//                $('.users_hiphen_hide').css('display','inline-block');
//                var start = $('#users_startDate').text();
//                var end = $('#users_endDate').text();
//                selectCurrentWeek();
//                getWeeklyusers(start,end);
//            },
//            beforeShowDay: function (date) {
//                var cssClass = '';
//                if (date >= startDate && date <= endDate)
//                    cssClass = 'ui-datepicker-current-day';
//                return [true, cssClass];
//            },
//            onChangeMonthYear: function (year, month, inst) {
//                selectCurrentWeek();
//            }
//        });
//
//    });
    
//    $(function () {
//        var startDate;
//        var endDate;
//        $('.week-picker_utmusers').datepicker({
//            showOtherMonths: true,
//            selectOtherMonths: true,
//            onSelect: function (dateText, inst) {
//                var date = $(this).datepicker('getDate');
//                startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
//                endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
//                var dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
//                $('#utmusers_startDate').text($.datepicker.formatDate(dateFormat, startDate, inst.settings));
//                $('#utmusers_endDate').text($.datepicker.formatDate(dateFormat, endDate, inst.settings));
//                $('.utmusers_hiphen_hide').css('display','inline-block');
//                var start = $('#utmusers_startDate').text();
//                var end = $('#utmusers_endDate').text();
//                selectCurrentWeek();
//                getWeeklyutmusers(start,end);
//            },
//            beforeShowDay: function (date) {
//                var cssClass = '';
//                if (date >= startDate && date <= endDate)
//                    cssClass = 'ui-datepicker-current-day';
//                return [true, cssClass];
//            },
//            onChangeMonthYear: function (year, month, inst) {
//                selectCurrentWeek();
//            }
//        });
//
//    });

//    $(document).on('change','.new-project',function(){
//        var selected_pro_type = $(this).val();
//        var start_date = $('#startDate').text();
//        var endDate = $('#endDate').text();
//        getWeekdate(start_date,endDate,selected_pro_type);
//    });
    
//    function getWeeklyusers(start,end){
//        $.ajax({
//            type: 'POST',
//            url: '<?php echo base_url(); ?>admin/report/getweeklyusers',
//            dataType: "json",
//            async: false,
//            data: {"start": start, "end": end},
//            success: function (response) {
//                if (response.status == '1') {
//                 var chart = new CanvasJS.Chart("Newusers", {
//                    animationEnabled: true,
//                    theme: "light2",
//                    title:{
//                        text: ""
//                    },
//                    axisY:{
//                        includeZero: false
//                    },
//                    data: [{        
//                        type: "spline",       
//                        dataPoints: response.linechart
//                    }]
//                });
//                 chart.render();
//             }else{
//                $('#Newusers').html('<div class="taxt_error_msg">Data Not Found</div>');
//            }
//        },
//        error: function (response) {
//            console.log('error response', response);
//        }
//    });
//    }
    
//    function getWeeklyutmusers(start,end){
//        $.ajax({
//            type: 'POST',
//            url: '<?php echo base_url(); ?>admin/report/getWeeklyutmusers',
//            dataType: "json",
//            async: false,
//            data: {"start": start, "end": end},
//            success: function (response) {
//                if (response.status == '1') {
//                 var chart = new CanvasJS.Chart("Utmusers", {
//                    animationEnabled: true,
//                    theme: "light2",
//                    title:{
//                        text: ""
//                    },
//                    axisY:{
//                        includeZero: false
//                    },
//                    data: [{        
//                        type: "column",       
//                        dataPoints: response.linechart
//                    }]
//                });
//                 chart.render();
//             }else{
//                $('#Utmusers').html('<div class="taxt_error_msg">Data Not Found</div>');
//            }
//        },
//        error: function (response) {
//            console.log('error response', response);
//        }
//    });
//    }
    
//    function getWeekdate(start,end,selected_pro_type = ''){
//        $.ajax({
//            type: 'POST',
//            url: '<?php echo base_url(); ?>admin/report/getlinechartData',
//            dataType: "json",
//            async: false,
//            data: {"start": start, "end": end,'selected_pro_type':selected_pro_type},
//            success: function (response) {
//                if (response.status == '1') {
//                 var chart = new CanvasJS.Chart("myChart", {
//                    animationEnabled: true,
//                    theme: "light2",
//                    title:{
//                        text: ""
//                    },
//                    axisY:{
//                        includeZero: false
//                    },
//                    data: [{        
//                        type: "spline",       
//                        dataPoints: response.linechart
//                    }]
//                });
//                 chart.render();
//             }else{
//                $('#myChart').html('<div class="taxt_error_msg">Data Not Found</div>');
//            }
//        },
//        error: function (response) {
//            console.log('error response', response);
//        }
//    });
//    }
    
//    function dateFormatset(dateObject) {
//        var d = new Date(dateObject);
//        var day = d.getDate();
//        var month = d.getMonth() + 1;
//        var year = d.getFullYear();
//        if (day < 10) {
//            day = "0" + day;
//        }
//        if (month < 10) {
//            month = "0" + month;
//        }
//        var date = year + "-" + month + "-" + day;
//
//        return date;
//    };

    /*************************Reporting designer average using date range****************************************************/
//    $('#datepicker_range').on('submit', function(e){
//        e.preventDefault();
//        var start_date = $('#datepicker_start').val();
//        var end_date = $('#datepicker_end').val();
//        $.ajax({
//            type: 'POST',
//            url: '<?php echo base_url(); ?>admin/report/designer_overview_DateRange',
//            dataType: "json",
//            data: {"start_date": start_date, "end_date": end_date},
//            success: function (response) {
//                var avgdayscompletion,avgperday,avgdraft,avg_quality;
//                $.each(response, function (k, v) {
//                    if(v.avgperday == undefined){
//                        avgperday = 0;
//                    }else{
//                        avgperday = v.avgperday;
//                    }
//                    $('.avgperday_'+v.id).text(avgperday);
//                    if(v.avgdaysforcompletion == undefined){
//                        avgdayscompletion = 0;
//                    }else{
//                        avgdayscompletion = v.avgdaysforcompletion;
//                    }
//                    $('.avgdaysforcompletion_'+v.id).text(avgdayscompletion);
//                    if(v.avgdraft == undefined){
//                        avgdraft = 0;
//                    }else{
//                        avgdraft = v.avgdraft;
//                    }
//                    $('.avgdraft_'+v.id).text(avgdraft);
//                    if(v.average_total_quality == undefined){
//                        avg_quality = 0;
//                    }else{
//                        avg_quality = (v.average_total_quality)+'%';
//                    }
//                    $('.avg_quality_'+v.id).text(avg_quality);
//                }); 
//            },
//            error: function (response) {
//                   // console.log('error response', response);
//               }
//           });
//        //}
//    });
    
//    $(document).ready(function () {
//       $("#datepicker_start").datepicker({
//           // maxDate: new Date(),
//           dateFormat: 'yy-mm-dd',
//           onSelect: function (dateText, inst) {
//            var date = $(this).val();
//            $('#datepicker_start').val(date);
//        }
//    });
//       $("#datepicker_end").datepicker({
//            //maxDate: new Date(),
//            dateFormat: 'yy-mm-dd',
//            onSelect: function (dateText, inst) {
//                var date = $(this).val();
//                $('#datepicker_end').val(date);
//            }
//        });
//       $("#taxes_start").datepicker({
//           // maxDate: new Date(),
//           dateFormat: 'yy-mm-dd',
//           onSelect: function (dateText, inst) {
//            var date = $(this).val();
//            $('#taxes_start').val(date);
//        }
//    });
//       $("#taxes_end").datepicker({
//            //maxDate: new Date(),
//            dateFormat: 'yy-mm-dd',
//            onSelect: function (dateText, inst) {
//                var date = $(this).val();
//                $('#taxes_end').val(date);
//            }
//        });
//
//
//       $("#msgstart_date").datepicker({
//        dateFormat: 'yy-mm-dd',
//        onSelect: function (dateText, inst) {
//            var date = $(this).val();
//            $('#msgstart_date').val(date);
//        }
//    });
//       $("#endmsg_date").datepicker({
//        dateFormat: 'yy-mm-dd',
//        onSelect: function (dateText, inst) {
//            var date = $(this).val();
//            $('#endmsg_date').val(date);
//        }
//    });
//   });
    
//    $('#taxes_datepicker_range').on('submit', function(e){
//        e.preventDefault();
//        var start_date = $('#taxes_start').val();
//        var end_date = $('#taxes_end').val();
//        $.ajax({
//            type: 'POST',
//            url: '<?php echo base_url(); ?>admin/report/getTotalTaxpaid',
//            dataType: "json",
//            data: {"start_date": start_date, "end_date": end_date},
//            success: function (response) {
//                if(response.status == 'success'){
//                    var html = '';
//                    $.each(response.data, function (k, v) {
//                      html  += "<li class='state_name'>"+v.state+"<span class='tax_count'>"+v.totaltax+"</span></li>";
//                  });
//                    $('.state_based_taxcount').html(html);
//                }else{
//                  $('.state_based_taxcount').html('No data found');  
//              }
//          },
//          error: function (response) {
//          }
//      });
//    });
    
//    $('#msg_datepicker_range').on('submit', function(e){
//        e.preventDefault();
//        var start_date = $('#msgstart_date').val();
//        var end_date = $('#endmsg_date').val();
//        $.ajax({
//            type: 'POST',
//            url: '<?php echo base_url(); ?>admin/report/messages_report',
//            dataType: "json",
//            data: {"start_date": start_date, "end_date": end_date},
//            success: function (response) {
//                if(response){
//                   $('.all_msg h4').html(response.all);
//                   $('.customer_msg h4').html(response.customer);
//                   $('.team_msg h4').html(response.team);
//                   $('.discuss_msg h4').html(response.discussion);
//
//               }
//           },
//           error: function (response) {
//           }
//       });
//    });
    

</script>