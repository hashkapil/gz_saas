<!-- Start Row -->
<div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $client_projects['id']; ?>/1'" style="cursor: pointer;">
    <div class="cli-ent-col td" style="width: 12%;">
        <div class="cli-ent-xbox">
            <p class="pro-a">
                <?php if($client_projects['status'] == "active") { 
                    echo "Expected on";
                }elseif ($client_projects['status'] == "disapprove") {
                    echo "Expected on";
                }elseif ($client_projects['status'] == "assign") {
                    echo "In-Queue";
                } ?>
            </p>
            <p class="space-a"></p>
            <p class="pro-b">
                <?php if($client_projects['status'] == "active") { 
                    echo date('M d, Y h:i A', strtotime($client_projects['expected']));
                }elseif ($client_projects['status'] == "disapprove") {
                    echo date('M d, Y h:i A', strtotime($client_projects['expected']));
                } ?>
            </p>
        </div>
    </div>
    <div class="cli-ent-col td" style="width: 48%;">
        <div class="cli-ent-xbox text-left">
            <div class="cell-row">
                
                <div class="cell-col" >
                    <h3 class="pro-head space-b"><a href="<?php echo base_url()."admin/dashboard/view_request/".$client_projects['id']; ?>/1"><?php echo $client_projects['title']; ?></a></h3>
                    <p class="pro-b"><?php echo substr($client_projects['description'], 0, 50); ?></p>
                </div>
                <!--                                    <div class="cell-col priority-sec"><?php// echo isset($client_projects['priority']) ? $client_projects['priority'] : ''; ?></div>-->
                <div class="cell-col col-w1">
                    <p class="neft text-center">
                        <?php if($client_projects['status'] == "active" || $client_projects['status'] ==  "checkforapprove" ){ ?>
                            <span class="green text-uppercase">In-Progress</span>
                        <?php }elseif ($client_projects['status'] == "disapprove") { ?>
                            <span class="red orangetext text-uppercase">REVISION</span>
                        <?php } elseif ($client_projects['status'] == "assign") { ?>
                            <span class="gray text-uppercase">In-Queue</span>
                        <?php } ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    
    <div class="cli-ent-col td" style="width: 20%;">
        <div class="cli-ent-xbox text-left p-left1">
            <div class="cell-row">
                <div class="cell-col" style="width: 36px; padding-right: 15px;">
                    <a href="<?php echo base_url()."admin/dashboard/view_request/".$client_projects['id']; ?>/1">
                        <?php if($client_projects['profile_picture'] != "" ){ ?>
                            <figure class="pro-circle-img">
                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$client_projects['profile_picture'];?>" class="img-responsive">
                            </figure>
                        <?php }else{ ?>
                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                <?php echo ucwords(substr($client_projects['designer_first_name'],0,1)) .  ucwords(substr($client_projects['designer_last_name'],0,1)); ?>
                            </figure>
                        <?php } ?>
                    </a>
                </div>
                <div class="cell-col">
                    <p class="text-h">
                        <?php
                        if ($client_projects['designer_first_name']) {
                            echo $client_projects['designer_first_name'];
                        } else {
                            echo "No Designer";
                        }
                        ?>  
                    </p>
                    <p class="pro-b">Designer</p>
                </div>
            </div>
            
        </div>
    </div>
    <div class="cli-ent-col td" style="width: 10%;">
        <div class="cli-ent-xbox text-center">
            <p class="pro-a inline-per"><a href="<?php echo base_url()."admin/dashboard/view_request/".$client_projects['id']; ?>/1">
                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                 <?php if($client_projects['total_chat'] + $client_projects['comment_count'] != 0){ ?>
                    <span class="numcircle-box">
                        <?php echo $client_projects['total_chat'] + $client_projects['comment_count']; ?>
                    </span>
                    <?php } ?></span></a></p>
                </div>
            </div>
            <div class="cli-ent-col td" style="width: 10%;">
                <div class="cli-ent-xbox text-center">
                    <p class="pro-a inline-per"><a href="<?php echo base_url()."admin/dashboard/view_request/".$client_projects['id']; ?>/1">
                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                            <?php if(count($client_projects['total_files']) != 0){ ?>
                                <span class="numcircle-box"><?php echo count($client_projects['total_files']); ?></span></span>
                            <?php } ?>
                            <?php echo count($client_projects['total_files_count']);?></a></p>
                        </div>
                    </div>
                </div> <!-- End Row -->
                