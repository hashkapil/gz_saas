<!DOCTYPE html>
<html lang="en">
    <head>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo FAV_ICON_PATH; ?>">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'front_end/Updated_Design/css/style.css');?>">
    <link rel="stylesheet" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'front_end/Updated_Design/css/custom.css');?>">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>404 Page Not Found</title>
        <style type="text/css">
            body{ margin:0; padding:0; }

            h1 {
                font-size: 23vh;  
                margin-bottom: 0;font-weight: 600;
                color: #e52344; margin-top:60px;
            }
            h2 {color: #253858;
                font-size: 50px;
                margin-top: 0;
            }
            .back-to {
                font-family: 'Montserrat', sans-serif;
                text-transform: uppercase;
                font-weight: 400;
                font-size: 20px;
                letter-spacing: 1px;
                display: inline-block;
                padding: 11px 36px;
                border-radius: 50px;
                transition: 0.5s;
                margin-bottom: 30px;
                background: #e8304d;
                color: #ffffff !important;
                text-decoration: none;
                box-shadow: 5px 6px 3px 0px #F4E1E5;
            }
            .back-to:hover{box-shadow: 0px 15px 18px 0px #EBEBEB; text-decoration: none;}

            @media (max-width:992px) {
                .four-four {
                    margin-top: 100px;
                }
            }
            @media (max-width:767px) {
                .four-four {
                    margin-top: 30px;
                    text-align: center;
                }
                h1 {
                    font-size: 13vh;
                }
                h2 {
                    font-size: 30px;
                }
                .back-to {
                    font-size: 16px;
                }
            }

        </style>
    </head>
    <?php $this->load->view('front_end/variable_css'); ?>
    <body>
        <div class="four-four four-bg">
            <div class="container">
            <div id="logo" class="pull-center">
                <a href="<?php echo config_item('base_url'); ?>"><img src="<?php echo $custom_logo; ?>" alt="<?php echo $page_title; ?>" title="<?php echo $page_title; ?>" /></a>
            </div>
            
                <div class="row">
                    <div class="col-sm-12">
                        <div class="error-info">
                        <h1>404</h1>
                        <h2>we are sorry, but the page you requested was not found</h2>
                        <div class="site-log-btn">
                            <a class="back-to" href="<?php echo config_item('base_url'); ?>">go to login</a>
                            <a class="back-to" href="<?php echo config_item('base_url').'/user_signup';?>">go to signup</a>
                        </div>
                        </div>
                    </div>
<!--                    <div class="col-sm-5">
                        <div class="error-image">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/404-error.png" class="img-fluid"> 
                        </div>
                    </div>-->
                </div>
            </div>
        </div>

    </body>
</html>