<?php
//die("xcvxcv");
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>404 Page Not Found</title>
<style type="text/css">
body{ margin:0; padding:0; }

h1 {
    font-size: 23vh;  
    margin-bottom: 0;font-weight: 600;
    color: #e52344; margin-top:60px;
}
h2 {color: #253858;
    font-size: 50px;
    margin-top: 0;
}
.back-to {
    font-family: 'Montserrat', sans-serif;
    text-transform: uppercase;
    font-weight: 400;
    font-size: 20px;
    letter-spacing: 1px;
    display: inline-block;
    padding: 11px 36px;
    border-radius: 50px;
    transition: 0.5s;
    margin-bottom: 30px;
    background: #e8304d;
    color: #ffffff !important;
    text-decoration: none;
    box-shadow: 5px 6px 3px 0px #F4E1E5;
}
.back-to:hover{box-shadow: 0px 15px 18px 0px #EBEBEB; text-decoration: none;}

@media (max-width:992px) {
.four-four {
    margin-top: 100px;
}
}
@media (max-width:767px) {
	.four-four {
    margin-top: 30px;
    text-align: center;
}
h1 {
    font-size: 13vh;
}
h2 {
    font-size: 30px;
}
.back-to {
    font-size: 16px;
}
}

</style>
</head>
<body>

<div class="four-four">
<div class="container">
<div class="row">
<div class="col-sm-6">

<h1>404</h1>
<h2>PAGE NOT FOUND</h2>
<a class="back-to" href="<?php echo config_item('base_url'); ?>">Go to home </a>

</div>
<div class="col-sm-6">
    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/404-error.png" alt="404-error" class="img-fluid"> 
</div>
</div>
</div>
</div>
	
</body>
</html>