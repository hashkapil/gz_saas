<section class="con-b">
		<div class="container">
			<div class="header-blog">
				<div class="row flex-show">
					<div class="col-md-8">
						<ul class="list-unstyled list-header-blog">
							<li><a href="<?php echo base_url();?>designer/request">Active (<?php echo sizeof($in_queue_project); ?>)</a></li>			
							<li class="acitve"><a href="<?php echo base_url();?>designer/request/designer_queue">In Queue(<?php echo sizeof($in_queue_project); ?>)</a></li>
							<li><a href="<?php echo base_url();?>designer/request/designer_pending_approval">Pending Approveal (<?php echo sizeof($pending_approval_project); ?>)</a></li>			
							<li><a href="<?php echo base_url();?>designer/request/designer_dashboard_completed">Completed (<?php echo sizeof($approved_project); ?>) </a></li>
						</ul>
					</div>
					<div class="col-md-4">
						<div class="search-box">
							<form method="post" class="search-group clearfix">
								<input type="text" placeholder="Search here..." class="form-control">
								<button type="submit" class="search-btn">
									<svg class="icon">
										<use xlink:href="<?php echo base_url();?>theme/designer_css/images/symbol-defs.svg#icon-magnifying-glass"></use>
									</svg>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			
			<p class="space-c"></p>
			
			<div class="pro-deshboard-list">
				<!-- <div class="cli-ent-add-row">
					<a href="category.html" class="cli-ent-add-col" >
						<span class="cli-ent-add-icon">+</span>
					</a>
				</div> -->
			
				<!-- List Desh Product -->

				<?php for ($i = 0; $i < sizeof($in_queue_project); $i++) { 
                        $colorclass= "";
                         if ($in_queue_project[$i]['status'] == "checkforapprove") {
                            $status = "Review your design";
                            $colorclass=" ";
                            //echo "greentext";
                        } elseif ($in_queue_project[$i]['status'] == "active") {
                            
                            $status = "IN-Progress";
                            $colorclass="green";
                            //echo "bluetext";
                        } elseif ($in_queue_project[$i]['status'] == "disapprove") {        
                            $status = "Revision";
                            $colorclass="red";
                            //echo "orangetext";
                        } else {  
                            $status = "In-Queue";
                            $colorclass="";
                           // echo "greytext";
                        } 
                ?>
				<div class="pro-desh-row" onclick="window.location.href = '<?php echo base_url();?>designer/request/project_info/<?php echo $in_queue_project[$i]['id']?>'">
					<?php
                        if (checkdate(date("m", strtotime($in_queue_project[$i]['dateinprogress'])), date("d", strtotime($in_queue_project[$i]['dateinprogress'])), date("Y", strtotime($in_queue_project[$i]['dateinprogress'])))) {
                            $over_date = "";
                            if ($in_queue_project[$i]['plan_turn_around_days']) {
                                //$in_queue_project[$i]['dateinprogress'] = "2018-04-12";
                                $date = date("Y-m-d", strtotime($in_queue_project[$i]['dateinprogress']));
                                $time = date("g:i:s", strtotime($in_queue_project[$i]['dateinprogress']));

                                if ($in_queue_project[$i]['status_designer'] == "disapprove") {
                                    $in_queue_project[$i]['plan_turn_around_days'] = $in_queue_project[$i]['plan_turn_around_days'] + 1;
                                }
                                $in_queue_project[$i]['plan_turn_around_days'] = "2";
                                $date = date("m/d/Y g:i a", strtotime($date . " " . $in_queue_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                //$date = date("m/d/Y g:i a",strtotime($date." 6 weekdays ".$time)); 
                                $diff = date_diff(date_create(date("Y-m-d", strtotime($in_queue_project[$i]['dateinprogress']))), date_create($date));
                                //print_r($diff);
                                //echo $in_queue_project[$i]['dateinprogress'];
                                //echo $date;
                            }
                        }
                        // echo "<pre>";
                        // print_r($in_queue_project);
                        // die;
                        ?>
					<div class="pro-desh-box dcol-1">
						<div class="desh-head-wwq">
							<div class="desh-inblock">
								<h3 class="pro-head space-b"><a href="<?php echo base_url();?>designer/request/project_info/<?php echo $in_queue_project[$i]['id']?>"><?php echo $in_queue_project[$i]['title']; ?></a></h3>
								<p class="pro-b"><?php echo $in_queue_project[$i]['category']; ?></p>
							</div>
							<div class="desh-inblock col-w1">
								<p class="neft" ><span class="gray text-uppercase"><?php echo $status; ?></span></p>
							</div>
						</div>
					</div>
					
					<div class="pro-desh-box dcol-3">
						<div class="pro-circle-list clearfix pad-w1">
							<p class="pro-a">Delivery</p>
							<p class="space-a"></p>
							<div class="pro-circle-box">
								<a href="">
								 <?php if ($in_queue_project[$i]['profile_picture']) { ?>
									<figure class="pro-circle-img">
										<img src="<?php echo base_url();?>uploads/profile_picture/<?php echo $in_queue_project[$i]['profile_picture']; ?>" class="img-responsive">
									</figure>
								<?php }else{ ?>
									<figure class="pro-circle-img">
										<img src="<?php echo base_url();?>uploads/profile_picture/user-admin.png" class="img-responsive">
									</figure>
								<?php } ?>
								<p class="pro-circle-txt text-center"><?php echo $in_queue_project[$i]['customer_first_name'] . " " . $in_queue_project[$i]['customer_last_name']; ?></p></a>
							</div>
							
							<!-- <div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							-->
							<!-- <div class="pro-circle-box">
								<a href=""><figure class="pro-circle-plus">
									<span class="plus-text"><span class="plus-sizebig">+</span> <br> 05</span>
								</figure></a>
							</div>  -->
							
						</div>
					</div>
					
					<div class="pro-desh-box">
						<div class="pro-desh-r1">
							<div class="pro-inleftbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
									<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21"><span class="numcircle-box">5</span></span>
								<?php echo $in_queue_project[$i]['total_chat']; ?></a></p>
							</div>
							<div class="pro-inrightbox">
								<p class="pro-a inline-per">
								<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13"> <span class="numcircle-box"><?php echo $in_queue_project[$i]['total_files'][0]['fileCount']; ?></span></span>
								</p>
							</div>
						</div>
					</div>
				</div>
				<?php } ?> 
				<!-- -->
				
				<!-- List Desh Product -->
				<!-- <div class="pro-desh-row">
					
					<div class="pro-desh-box dcol-1">
						<div class="desh-head-wwq">
							<div class="desh-inblock">
								<h3 class="pro-head space-b"><a href="<?php echo base_url();?>designer/request/project_info">Design a logo for merchant Processing Company</a></h3>
								<p class="pro-b">Identity and Barnding</p>
							</div>
							<div class="desh-inblock col-w1">
								<p class="neft" ><span class="gray text-uppercase">IN-queue</span></p>
							</div>
						</div>
					</div>
					
					<div class="pro-desh-box dcol-3">
						<div class="pro-circle-list clearfix pad-w1">
							<p class="pro-a">Delivery</p>
							<p class="space-a"></p>
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-plus">
									<span class="plus-text"><span class="plus-sizebig">+</span> <br> 05</span>
								</figure></a>
							</div>
							
						</div>
					</div>
					
					<div class="pro-desh-box">
						<div class="pro-desh-r1">
							<div class="pro-inleftbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
									<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21"><span class="numcircle-box">5</span></span>
								8</a></p>
							</div>
							<div class="pro-inrightbox">
								<p class="pro-a inline-per">
								<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13"> <span class="numcircle-box">2</span></span>
								</p>
							</div>
						</div>
					</div>
				</div>  -->
				<!-- -->
				
				<!-- List Desh Product -->
				<!-- <div class="pro-desh-row">
					
					
					<div class="pro-desh-box dcol-1">
						<div class="desh-head-wwq">
							<div class="desh-inblock">
								<h3 class="pro-head space-b"><a href="<?php echo base_url();?>designer/request/project_info">Design a logo for merchant Processing Company</a></h3>
								<p class="pro-b">Identity and Barnding</p>
							</div>
							<div class="desh-inblock col-w1">
								<p class="neft" ><span class="gray text-uppercase">IN-queue</span></p>
							</div>
						</div>
					</div>
					
					<div class="pro-desh-box dcol-3">
						<div class="pro-circle-list clearfix pad-w1">
							<p class="pro-a">Delivery</p>
							<p class="space-a"></p>
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-plus">
									<span class="plus-text"><span class="plus-sizebig">+</span> <br> 05</span>
								</figure></a>
							</div>
							
						</div>
					</div>
					
					<div class="pro-desh-box">
						<div class="pro-desh-r1">
							<div class="pro-inleftbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
									<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21"><span class="numcircle-box">5</span></span>
								8</a></p>
							</div>
							<div class="pro-inrightbox">
								<p class="pro-a inline-per">
								<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13"></span>
								</p>
							</div>
						</div>
					</div>
				</div>  -->
				<!-- -->
				
				<!-- List Desh Product -->
				<!-- <div class="pro-desh-row">
					
					
					<div class="pro-desh-box dcol-1">
						<div class="desh-head-wwq">
							<div class="desh-inblock">
								<h3 class="pro-head space-b"><a href="<?php echo base_url();?>designer/request/project_info">Design a logo for merchant Processing Company</a></h3>
								<p class="pro-b">Identity and Barnding</p>
							</div>
							<div class="desh-inblock col-w1">
								<p class="neft" ><span class="gray text-uppercase">IN-queue</span></p>
							</div>
						</div>
					</div>
					
					<div class="pro-desh-box dcol-3">
						<div class="pro-circle-list clearfix pad-w1">
							<p class="pro-a">Delivery</p>
							<p class="space-a"></p>
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-plus">
									<span class="plus-text"><span class="plus-sizebig">+</span> <br> 05</span>
								</figure></a>
							</div>
							
						</div>
					</div>
					
					<div class="pro-desh-box">
						<div class="pro-desh-r1">
							<div class="pro-inleftbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
									<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21"><span class="numcircle-box">5</span></span>
								8</a></p>
							</div>
							<div class="pro-inrightbox">
								<p class="pro-a inline-per">
								<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13"> <span class="numcircle-box">2</span></span>
								</p>
							</div>
						</div>
					</div>
				</div>  -->
				<!-- -->
				
				
			</div>

			
			<p class="space-e"></p>
			
			<?php if(sizeof($in_queue_project) >= 20){ ?>
			<div class="loader">
				<a class="loader-link text-uppercase" href=""><img src="<?php echo base_url();?>theme/designer_css/images/icon-loader.png" class="img-responsive" width="28"> Loading</a>
			</div>
			<?php } ?>
			
		</div>
	</section>