<h2 class="float-xs-left content-title-main" style="display:inline-block;padding-left:40px">Projects</h2>
</div>
<div class="col-md-12" style="background:white;">
    <div class="col-md-10 offset-md-1">
        <section id="content-wrapper">
            <style>
                /* padding css start */
                .pb0{ padding-bottom:0px; }
                .pb5{ padding-bottom:5px; }
                .pb10{ padding-bottom:10px; }
                .pt0{ padding-top:0px; }
                .pt5{ padding-top:5px; }
                .pt10{ padding-top:10px; }
                .pl0{ padding-left:0px; }
                .pl5{ padding-left:5px; }
                .pl10{ padding-left:10px; }
                /* padding css end */
                .greenbackground { background-color:#98d575; }
                .greentext { color:#98d575; }
                .orangebackground { background-color:#f7941f; }
                .pinkbackground { background-color: #ec4159; }
                .orangetext { color:#f7941f; }
                .bluebackground { background-color:#409ae8; }
                .bluetext{ color:#409ae8; }
                .whitetext { color:#fff !important; }
                .blacktext { color:#000; }
                .greytext { color:#cccccc; }
                .greybackground { background-color:#ededed; }
                .darkblacktext { color:#1a3147; } 
                .pinktext { color: #ec4159; }
                .weight600 { font-weight:600; }
                .font18 { font-size:18px; }
                .textleft { text-align:left; }
                .textright { text-align:right; }
                .textcenter { text-align:center; }
                .pl20 { padding-left:20px; }

                .numbercss{
                    font-size: 20px !important;
                    padding: 8px 0px !important;
                    font-weight: 600 !important;
                    letter-spacing: 0px !important;
                    padding: 40px 0px 0px 0px !important;
                }
                .table-hover tbody tr:hover {
                    background-color: unset !important;
                }
                .projecttitle{
                    font-size: 16px;
                    line-height:18px;
                    padding-bottom: 0px;
                    text-align: left;
                    padding-left: 20px;
                }
                .trborder{
                    border: 1px solid #000;
                    background: unset;

                }
                .trborder:hover{
                    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                }
                table {     border-spacing: 0 1em; }
                .trash{     
                    color: #ec4159;
                    font-size: 25px; 
                }

                .nav-tab-pills-image ul li .nav-link {
                    color:#1a3147;
                    font-weight:600;
                    padding: 7px 25px;
                }

                .content .nav-tab-pills-image .nav-item.active a {

                    border:unset !important;
                }
                .wrapper .nav-tab-pills-image ul li .nav-link:hover{
                    color:#000;

                }

                .wrapper .nav-tab-pills-image ul li .nav-link{
                    color: #2f4458;
                    height:43px;
                }
                .nav-tab-pills-image ul li .nav-link:hover {
                    border-bottom: none !important;
                }
                .wrapper .nav-tab-pills-image ul li .nav-link:focus
                {
                    color:#fff;
                    border:none;
                }
                .content .nav-tab-pills-image .nav-item.active a:hover{
                    color:#fff;

                }
                .content .nav-tab-pills-image .nav-item.active dd {
                    background:unset !important;
                }
                .nav-tab-pills-image ul li .nav-link:hover {
                    border-bottom: none !important;
                }
                table.custom-table.table-striped tbody tr:nth-of-type(even) {
                    background-color: unset;
                }
            </style>
            <div class="content">

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">  

                        <div class="col-md-12" style="margin-bottom:20px;">
                            <div class="col-md-8">
                               <center>
									 <div class="col-sm-2">
										 
										<?php
										if ($client_details[0]['profile_picture']) 
										{
										?>
										 <div class="myprofilebackground" ><p style="padding-top: 13px;">
										 <?php
											echo '<img src="' . base_url() . 'uploads/profile_picture/' . $client_details[0]['profile_picture'] . '" style="width:60px; height:50px; border-radius:50%;border: 3px solid #0190ff;">';
										
										?>
										 </p></div>         
										<?php  
										}
										else 
										{
										?>
										<div class="myprofilebackground" style="width:60px; height:50px;border-radius:50%; background: #0190ff;border: 3px solid #0190ff; text-align: center; font-size: 15px; color: #fff;"><p style="font-size: 22px;padding-top: 10px;">
												
										<?php 
											
											echo ucwords(substr($client_details[0]['first_name'],0,1)) .  ucwords(substr($client_details[0]['last_name'],0,1));
										?>
											</p></div>
										<?php
										}
										?>
									</div>
										</center>
                                <div class="col-md-6">
                                    <h3>
                                        <?php
                                        echo $client_details[0]['first_name'] . " " . $client_details[0]['last_name'];
                                        ?>
                                    </h3>
                                    <h5 style="font-size:12px;">Member Since <?php echo date("M d,Y", strtotime($client_details[0]['first_name'])); ?></h5>
                                </div>
                            </div>
                        </div>
                        <div class="nav-tab-pills-image">
                            <ul class="nav nav-tabs" role="tablist" style="border:none;">                      
                                <li class="nav-item active" style="background: #ededed;border-radius: 12px 0px 0px 12px;">
                                    <a class="nav-link" data-toggle="tab" href="#designs_request_tab" role="tab" style="width:245px;border-radius: 12px 0px 0px 12px;">
                                        Active Projects (<?php echo sizeof($active_project); ?>)
                                    </a>
                                </li>
                                <li class="nav-item" style="background: #ededed;margin-left:6px;">
                                    <a class="nav-link" data-toggle="tab" href="#inprogressrequest" role="tab">
                                        In-Queue (<?php echo sizeof($in_queue_project); ?>)
                                    </a>
                                </li>
                                <li class="nav-item" style="background: #ededed;margin-left:6px;">
                                    <a class="nav-link" data-toggle="tab" href="#pending_designs_tab" role="tab">
                                        Pending Approval (<?php echo sizeof($pending_approval_project); ?>)
                                    </a>
                                </li>
                                <li class="nav-item" style="background: #ededed;margin-left:6px;border-radius: 0px 12px 12px 0px;">
                                    <a class="nav-link" data-toggle="tab" href="#approved_designs_tab" role="tab" style="border-radius: 0px 12px 12px 0px;">
                                        Completed (<?php echo sizeof($approved_project); ?>)
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none;">
                                               <!--<thead>
                                                   <tr>
                                                       <th>Id</th>
                                                       <th>Project Name</th>
                                                                                                       <th>Customer Name</th>
                                                                                                       <th>Latest Update</th>
                                                                                                       <th>Due Date</th>
                                                                                                       <th>Messages</th>
                                                                                                       <th>Rating</th>
                                                   </tr>
                                               </thead>-->
                                                <tbody>

                                                    <?php for ($i = 0; $i < sizeof($active_project); $i++) { ?>
                                                        <tr class="trborder" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_request/<?php echo $active_project[$i]['id']; ?>'">
                                                            <?php
                                                            $date = "";
                                                            if (checkdate(date("m", strtotime($active_project[$i]['dateinprogress'])), date("d", strtotime($active_project[$i]['dateinprogress'])), date("Y", strtotime($active_project[$i]['dateinprogress'])))) {
                                                                $over_date = "";

                                                                if ($active_project[$i]['plan_turn_around_days']) {
                                                                    //$active_project[$i]['dateinprogress'] = "2018-04-12";
                                                                    $date = date("Y-m-d", strtotime($active_project[$i]['dateinprogress']));
                                                                    $time = date("h:i:s", strtotime($active_project[$i]['dateinprogress']));

                                                                    if ($active_project[$i]['status_designer'] == "disapprove") {
                                                                        $active_project[$i]['plan_turn_around_days'] = $active_project[$i]['plan_turn_around_days'] + 1;
                                                                    }
                                                                    $active_project[$i]['plan_turn_around_days'] = "2";
                                                                    $date = date("m/d/Y g:i a", strtotime($date . " " . $active_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                                                    //$date = date("m/d/Y g:i a",strtotime($date." 6 weekdays ".$time)); 
                                                                    $diff = date_diff(date_create(date("Y-m-d", strtotime($active_project[$i]['dateinprogress']))), date_create($date));
                                                                    //print_r($diff);
                                                                    //echo $active_project[$i]['dateinprogress'];
                                                                    //echo $date;
                                                                }
                                                            }
                                                            $current_date = strtotime(date("Y-m-d"));
                                                            $now = time(); // or your date as well
                                                            $expiration_date = strtotime($date);
                                                            $datediff = $now - $expiration_date;
                                                            $date_due_day = round($datediff / (60 * 60 * 24));
                                                            //echo $date_due_day;
                                                            $color = "";
                                                            $text_color = "white";
                                                            if ((round($datediff / (60 * 60 * 24))) == 0) {
                                                                $date_due_day = "Due  </br>Today";
                                                                $color = "#f7941f";
                                                            } else
                                                            if (round(($datediff / (60 * 60 * 24))) == (-1)) {
                                                                $date_due_day = "Due  </br>Tomorrow";
                                                                $color = "#98d575";
                                                            } else
                                                            if (round(($datediff / (60 * 60 * 24))) > 0) {
                                                                $date_due_day = "Due from  </br>" . number_format($datediff / (60 * 60 * 24)) . " days";
                                                                $color = "red";
                                                            } else if (round(($datediff / (60 * 60 * 24))) < 0) {
                                                                $date_due_day = "Due from </br>" . number_format($datediff / (60 * 60 * 24)) . " days";
                                                                $text_color = "black";
                                                            }
                                                            //echo round($datediff / (60 * 60 * 24));
                                                            ?>
                                                            <td class="whitetext numbercss" style="color:<?php echo $text_color; ?>width: 100px;padding:5px;background:<?php echo $color; ?>"><span style="color:<?php echo $text_color; ?>"><?php echo $date_due_day; ?></span><p class="whitetext" style="font-size: 16px;">

                                                                    <?php
                                                                    if (checkdate(date("m", strtotime($active_project[$i]['dateinprogress'])), date("d", strtotime($active_project[$i]['dateinprogress'])), date("Y", strtotime($active_project[$i]['dateinprogress'])))) {
                                                                        $over_date = "";
                                                                        if ($active_project[$i]['plan_turn_around_days']) {
                                                                            $date = date("Y-m-d", strtotime($active_project[$i]['dateinprogress']));
                                                                            $time = date("h:i:s", strtotime($active_project[$i]['dateinprogress']));

                                                                            if ($active_project[$i]['status_designer'] == "disapprove") {
                                                                                $active_project[$i]['plan_turn_around_days'] = $active_project[$i]['plan_turn_around_days'] + 1;
                                                                            }

                                                                            $date = date("m/d/Y g:i a", strtotime($date . " " . $active_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                                                            $diff = date_diff(date_create(date("Y-m-d", strtotime($active_project[$i]['dateinprogress']))), date_create($date));
                                                                            //print_r($diff);
                                                                            //echo $active_project[$i]['dateinprogress'];
                                                                            //echo $date;
                                                                        }
                                                                    }
                                                                    ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $active_project[$i]['title']; ?></p>
                                                                <p class="greentext weight600 textleft pl20 pb5"><?php echo $active_project[$i]['status']; ?></p>
                                                                <p class="darkblacktext textleft pl20"><?php echo substr($active_project[$i]['description'], 0, 50); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Client</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo $active_project[$i]['customer_first_name'] . " " . $active_project[$i]['customer_last_name']; ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Latest Update</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo date("d/m/Y", strtotime($active_project[$i]['dateinprogress'])); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Due Date</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo $date; ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Messages</p>
                                                                <p class="darkblacktext weight600"><svg style="vertical-align: sub;width: 0.5em;" aria-hidden="true" data-prefix="fas" data-icon="comment-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt fa-w-16 fa-2x" ><path fill="orange" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z" class=""></path></svg><span class="orangetext pl5"><?php echo $active_project[$i]['total_chat']; ?></span></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font14">Rating</p>
                                                                <?php
                                                                $rat = round($active_project[$i]['total_request_grade']);
                                                                for ($j = 1; $j <= 5; $j++) {
                                                                    if ($j <= $rat) {
                                                                        ?>

                                                                        <i class="fa fa-star pinktext"></i>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <i class="fa fa-star"></i>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr> 
                                                            <?php } ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane  content-datatable datatable-width" id="inprogressrequest" role="tabpanel"> 
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none;">
                                                <!--<thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Project Name</th>
                                                                                                        <th>Customer Name</th>
                                                                                                        <th>In-progress</th>
                                                                                                        <th>Messages</th>
                                                                                                        <th>Icon</th>
                                                                                   
                                                    </tr>
                                                </thead>-->

                                                <tbody>
<?php for ($i = 0; $i < sizeof($in_queue_project); $i++) { ?>
                                                        <tr class="trborder" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_request/<?php echo $in_queue_project[$i]['id']; ?>'">

    <td class="whitetext numbercss greybackground" style="width: 100px;">In </br>Queue<p class="whitetext" style="font-size: 16px;">

    <?php
    if (checkdate(date("m", strtotime($in_queue_project[$i]['dateinprogress'])), date("d", strtotime($in_queue_project[$i]['dateinprogress'])), date("Y", strtotime($in_queue_project[$i]['dateinprogress'])))) {
        $over_date = "";
        if ($in_queue_project[$i]['plan_turn_around_days']) {
            $date = date("Y-m-d", strtotime($in_queue_project[$i]['dateinprogress']));
            $time = date("h:i:s", strtotime($in_queue_project[$i]['dateinprogress']));

            if ($in_queue_project[$i]['status_designer'] == "disapprove") {
                $in_queue_project[$i]['plan_turn_around_days'] = $in_queue_project[$i]['plan_turn_around_days'] + 1;
            }

            $date = date("m/d/Y g:i a", strtotime($date . " " . $in_queue_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
            $diff = date_diff(date_create(date("Y-m-d", strtotime($in_queue_project[$i]['dateinprogress']))), date_create($date));
            //print_r($diff);
            //echo $in_queue_project[$i]['dateinprogress'];
            //echo $date;
        }
    }
    ?></p>
    </td>
                                                            <td>
                                                                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $in_queue_project[$i]['title']; ?></p>
                                                                <p class="greentext weight600 textleft pl20 pb5"><?php echo $in_queue_project[$i]['status']; ?></p>
                                                                <p class="darkblacktext textleft pl20"><?php echo substr($in_queue_project[$i]['description'], 0, 50); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Client</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo $in_queue_project[$i]['customer_first_name'] . " " . $in_queue_project[$i]['customer_last_name']; ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">In-progress</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo date("d/m/Y", strtotime($in_queue_project[$i]['dateinprogress'])); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Messages</p>
                                                                <p class="darkblacktext weight600"><svg style="vertical-align: sub;width: 0.5em;" aria-hidden="true" data-prefix="fas" data-icon="comment-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt fa-w-16 fa-2x" ><path fill="orange" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z" class=""></path></svg><span class="orangetext pl5"><?php echo $in_queue_project[$i]['total_chat']; ?></span></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font14">Rating</p>
                                                                <?php
                                                                $ra = round($in_queue_project[$i]['total_request_grade']);
                                                                for ($j = 1; $j <= 5; $j++) {
                                                                    if ($j <= $ra) {
                                                                        ?>

                                                                        <i class="fa fa-star pinktext"></i>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <i class="fa fa-star"></i>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr> 
<?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane content-datatable datatable-width" id="pending_designs_tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none;">						
                                                <!--<thead>
                                                    <tr>
                                                       <th>Id</th>
                                                        <th>Project Name</th>
                                                                                                        <th>Customer Name</th>
                                                                                                        <th>In-progress</th>
                                                                                                        <th>Messages</th>
                                                                                                        <th>Icon</th>
                                                    </tr>
                                                </thead>-->
                                                <tbody>
<?php for ($i = 0; $i < sizeof($pending_approval_project); $i++) { ?>
                                                        <tr class="trborder" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_request/<?php echo $pending_approval_project[$i]['id']; ?>'">

                                                            <td class="greenbackground whitetext numbercss" style="width: 100px;">Due </br>Today<p class="whitetext" style="font-size: 16px;">

    <?php
    if (checkdate(date("m", strtotime($pending_approval_project[$i]['dateinprogress'])), date("d", strtotime($pending_approval_project[$i]['dateinprogress'])), date("Y", strtotime($pending_approval_project[$i]['dateinprogress'])))) {
        $over_date = "";
        if ($pending_approval_project[$i]['plan_turn_around_days']) {
            $date = date("Y-m-d", strtotime($pending_approval_project[$i]['dateinprogress']));
            $time = date("h:i:s", strtotime($pending_approval_project[$i]['dateinprogress']));

            if ($pending_approval_project[$i]['status_designer'] == "disapprove") {
                $pending_approval_project[$i]['plan_turn_around_days'] = $pending_approval_project[$i]['plan_turn_around_days'] + 1;
            }

            $date = date("m/d/Y g:i a", strtotime($date . " " . $pending_approval_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
            $diff = date_diff(date_create(date("Y-m-d", strtotime($pending_approval_project[$i]['dateinprogress']))), date_create($date));
            //print_r($diff);
            //echo $pending_approval_project[$i]['dateinprogress'];
            //echo $date;
        }
    }
    ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $pending_approval_project[$i]['title']; ?></p>
                                                                <p class="greentext weight600 textleft pl20 pb5"><?php echo $pending_approval_project[$i]['status_designer']; ?></p>
                                                                <p class="darkblacktext textleft pl20"><?php echo substr($pending_approval_project[$i]['description'], 0, 50); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Client</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo $pending_approval_project[$i]['customer_first_name'] . " " . $pending_approval_project[$i]['customer_last_name']; ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">In-progress</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo date("d/m/Y", strtotime($pending_approval_project[$i]['dateinprogress'])); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Messages</p>
                                                                <p class="darkblacktext weight600"><svg style="vertical-align: sub;width: 0.5em;" aria-hidden="true" data-prefix="fas" data-icon="comment-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt fa-w-16 fa-2x" ><path fill="orange" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z" class=""></path></svg><span class="orangetext pl5"><?php echo $pending_approval_project[$i]['total_chat']; ?></span></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font14">Rating</p>
                                                            <?php
                                                                $r = round($pending_approval_project[$i]['total_request_grade']);
                                                                for ($j = 1; $j <= 5; $j++) {
                                                                    if ($j <= $r) {
                                                                        ?>

                                                                        <i class="fa fa-star pinktext"></i>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <i class="fa fa-star"></i>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </td>            
                                                        </tr> 
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>



                                <div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none;">
                                                <!--<thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Project Name</th>
                                                                                                        <th>Customer Name</th>
                                                                                                        <th>In-progress</th>
                                                                                                        <th>Messages</th>
                                                                                                        <th>Icon</th>
                                                    </tr>
                                                </thead>-->
                                                <tbody>
                                                    <?php for ($i = 0; $i < sizeof($approved_project); $i++) { ?>
                                                        <tr class="trborder" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_request/<?php echo $approved_project[$i]['id']; ?>'">

    	<td class="greybackground whitetext numbercss" style="width: 100px;">Complete<p class="whitetext" style="font-size: 16px;">

                                                            <?php
                                                            if (checkdate(date("m", strtotime($approved_project[$i]['dateinprogress'])), date("d", strtotime($approved_project[$i]['dateinprogress'])), date("Y", strtotime($approved_project[$i]['dateinprogress'])))) {
                                                                $over_date = "";
                                                                if ($approved_project[$i]['plan_turn_around_days']) {
                                                                    $date = date("Y-m-d", strtotime($approved_project[$i]['dateinprogress']));
                                                                    $time = date("h:i:s", strtotime($approved_project[$i]['dateinprogress']));

                                                                    if ($approved_project[$i]['status_designer'] == "disapprove") {
                                                                        $approved_project[$i]['plan_turn_around_days'] = $approved_project[$i]['plan_turn_around_days'] + 1;
                                                                    }

                                                                    $date = date("m/d/Y g:i a", strtotime($date . " " . $approved_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                                                    $diff = date_diff(date_create(date("Y-m-d", strtotime($approved_project[$i]['dateinprogress']))), date_create($date));
                                                                    //print_r($diff);
                                                                    //echo $approved_project[$i]['dateinprogress'];
                                                                    //echo $date;
                                                                }
                                                            }
                                                            ?></p>
    </td>
                                                            <td>
                                                                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $approved_project[$i]['title']; ?></p>
                                                                <p class="greentext weight600 textleft pl20 pb5"><?php echo $approved_project[$i]['status_designer']; ?></p>
                                                                <p class="darkblacktext textleft pl20"><?php echo substr($approved_project[$i]['description'], 0, 50); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Client</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo $approved_project[$i]['customer_first_name'] . " " . $approved_project[$i]['customer_last_name']; ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">In-progress</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo date("d/m/Y", strtotime($approved_project[$i]['dateinprogress'])); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Messages</p>
                                                                <p class="darkblacktext weight600"><svg style="vertical-align: sub;width: 0.5em;" aria-hidden="true" data-prefix="fas" data-icon="comment-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt fa-w-16 fa-2x" ><path fill="orange" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z" class=""></path></svg><span class="orangetext pl5"><?php echo $approved_project[$i]['total_chat']; ?></span></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font14">Rating</p>
                                                            <?php
                                                                $r = round($approved_project[$i]['total_request_grade']);
                                                                for ($j = 1; $j <= 5; $j++) {
                                                                    if ($j <= $r) {
                                                                        ?>

                                                                        <i class="fa fa-star pinktext"></i>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <i class="fa fa-star"></i>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr> 
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>

    <script>

        $(document).ready(function () {
            $(".more_less_Click").click(function () {
                $(".more_additional").toggle();
            });
            $(".read_more_btn").click(function () {
                $(".read_more").toggle();
            });
            $(".more_text_data_more_btn").click(function () {
                $(".more_text_data_more").toggle();
            });


        });
    </script>