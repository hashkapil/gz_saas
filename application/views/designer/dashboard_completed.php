<section class="con-b">
		<div class="container">
			<div class="header-blog">
				<div class="row flex-show">
					<div class="col-md-8">
						<ul class="list-unstyled list-header-blog">
							<li><a class="nav-link tabmenu" data-toggle="tab" href="#designs_request_tab" role="tab" >Active (<?php echo sizeof($active_project); ?>)</a></li>			
							<li><a href="<?php echo base_url();?>designer/request/designer_queue">In Queue(<?php echo sizeof($in_queue_project); ?>)</a></li>
							<li><a href="<?php echo base_url();?>designer/request/designer_pending_approval">Pending Approveal (<?php echo sizeof($approved_project); ?>)</a></li>			
							<li class="acitve"><a href="<?php echo base_url();?>designer/request/designer_dashboard_completed">Completed (<?php echo sizeof($approved_project); ?>) </a></li>
						</ul>
					</div>
					<div class="col-md-4">
						<div class="search-box">
							<form method="post" class="search-group clearfix">
								<input type="text" placeholder="Search here..." class="form-control">
								<button type="submit" class="search-btn">
									<svg class="icon">
										<use xlink:href="<?php echo base_url();?>theme/designer_css/images/symbol-defs.svg#icon-magnifying-glass"></use>
									</svg>
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			
			<p class="space-c"></p>
			
			<div class="pro-deshboard-list">
				<!-- <div class="cli-ent-add-row">
					<a href="category.html" class="cli-ent-add-col" >
						<span class="cli-ent-add-icon">+</span>
					</a>
				</div> -->
			
				<!-- List Desh Product -->
				<?php //echo "<pre>"; print_r($approved_project); echo "</pre>";
                    for ($i = 0; $i < sizeof($approved_project); $i++) { ?>
                        <?php
                        $colorclass= "";
                         if ($approved_project[$i]['status'] == "checkforapprove") {
                            $status = "Review your design";
                            $colorclass=" ";
                            //echo "greentext";
                        } elseif ($approved_project[$i]['status'] == "active") {
                            
                            $status = "IN-Progress";
                            $colorclass="green";
                            //echo "bluetext";
                        } elseif ($approved_project[$i]['status'] == "disapprove") {        
                            $status = "Revision";
                            $colorclass="red";
                            //echo "orangetext";
                        } elseif ($approved_project[$i]['status'] == "approved") {
                        	$status = "Completed";
                        } else {  
                            $status = "In-Queue";
                            $colorclass="";
                           // echo "greytext";
                        } ?>
				<div class="pro-desh-row" onclick="window.location.href = '<?php echo base_url();?>designer/request/project_info/<?php echo $approved_project[$i]['id']?>'">
					<?php
                        if (checkdate(date("m", strtotime($approved_project[$i]['dateinprogress'])), date("d", strtotime($approved_project[$i]['dateinprogress'])), date("Y", strtotime($approved_project[$i]['dateinprogress'])))) {
                            $over_date = "";
                            if ($approved_project[$i]['plan_turn_around_days']) {
                                //$approved_project[$i]['dateinprogress'] = "2018-04-12";
                                $date = date("Y-m-d", strtotime($approved_project[$i]['dateinprogress']));
                                $time = date("g:i:s", strtotime($approved_project[$i]['dateinprogress']));

                                if ($approved_project[$i]['status_designer'] == "disapprove") {
                                    $approved_project[$i]['plan_turn_around_days'] = $approved_project[$i]['plan_turn_around_days'] + 1;
                                }
                                $approved_project[$i]['plan_turn_around_days'] = "2";
                                $date = date("m/d/Y g:i a", strtotime($date . " " . $approved_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                //$date = date("m/d/Y g:i a",strtotime($date." 6 weekdays ".$time)); 
                                $diff = date_diff(date_create(date("Y-m-d", strtotime($approved_project[$i]['dateinprogress']))), date_create($date));
                                //print_r($diff);
                                //echo $approved_project[$i]['dateinprogress'];
                                //echo $date;
                            }
                        }
                        
                        ?>
					<div class="pro-desh-box delivery-desh">
						<p class="pro-a">Approved</p>
						<p class="pro-b">
							<?php
                                $created_date =  date($approved_project[$i]['dateinprogress']);
                                // $expected_date = date('m/d/Y H:i:s', strtotime($created_date . ' +1 day'));
                                $expected_date = date('M /d /Y', strtotime($created_date . ' +1 day'));
                                if ( $approved_project[ $i ][ 'modified' ] == "" ) {
                                    echo $expected_date;
                                } else {
                                    echo date( "M /d /Y", strtotime( $approved_project[ $i ][ 'modified' ] ) );
                                }
                                // echo date("m/d/Y",strtotime($date));
                            ?> 
						</p>
					</div>
					
					<div class="pro-desh-box dcol-1">
						<div class="desh-head-wwq">
							<div class="desh-inblock">
								<h3 class="pro-head space-b"><a href="<?php echo base_url();?>designer/request/project_info/<?php echo $approved_project[$i]['id']?>"><?php echo $approved_project[$i]['title']; ?></a></h3>
								<p class="pro-b"><?php echo $approved_project[$i]['category']; ?></p>
							</div>
							<div class="desh-inblock col-w1">
								<p class="neft"><span class="green text-uppercase"><?php echo $status; ?></span></p>
							</div>
						</div>
					</div>
					
					<div class="pro-desh-box dcol-2">
						<div class="pro-circle-list clearfix">
							<p class="pro-a">Delivery</p>
							<p class="space-a"></p>
							<div class="pro-circle-box">
								<a href="">
								 <?php if ($approved_project[$i]['profile_picture']) { ?>
									<figure class="pro-circle-img">
										<img src="<?php echo base_url();?>uploads/profile_picture/<?php echo $approved_project[$i]['customer_profile_picture'] ;?>" class="img-responsive">
									</figure>
								<?php }else{ ?>
									<figure class="pro-circle-img">
										<img src="<?php echo base_url();?>uploads/profile_picture/user-admin.png" class="img-responsive">
									</figure>
								<?php } ?>
								<p class="pro-circle-txt text-center"><?php echo $approved_project[$i]['customer_first_name'] . " " . $approved_project[$i]['customer_last_name']; ?></p></a>
							</div>
							
							<!-- <div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div> -->
							
							<!-- <div class="pro-circle-box">
								<a href=""><figure class="pro-circle-plus">
									<span class="plus-text"><span class="plus-sizebig">+</span> <br> 05</span>
								</figure></a>
							</div> -->
							
						</div>
					</div>
					
					<div class="pro-desh-box">
						<div class="pro-desh-r1">
							<div class="pro-inleftbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
									<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21"><span class="numcircle-box">5</span></span>
								<?php echo $approved_project[$i]['total_chat']; ?></a></p>
							</div>
							<div class="pro-inrightbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
								<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13"> <span class="numcircle-box">2</span></span>
								2</a></p>
							</div>
						</div>
					</div>
				</div> 
			<?php } ?>
				<!-- -->
				
				<!-- List Desh Product -->
				<!-- <div class="pro-desh-row">
					<div class="pro-desh-box delivery-desh">
						<p class="pro-a">Approved</p>
						<p class="pro-b">May /30 /2018</p>
					</div>
					
					<div class="pro-desh-box dcol-1">
						<div class="desh-head-wwq">
							<div class="desh-inblock">
								<h3 class="pro-head space-b"><a href="<?php echo base_url();?>designer/request/project_info">Design a logo for merchant Processing Company</a></h3>
								<p class="pro-b">Identity and Barnding</p>
							</div>
							<div class="desh-inblock col-w1">
								<p class="neft"><span class="green text-uppercase">Completed</span></p>
							</div>
						</div>
					</div>
					
					<div class="pro-desh-box dcol-2">
						<div class="pro-circle-list clearfix">
							<p class="pro-a">Delivery</p>
							<p class="space-a"></p>
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-plus">
									<span class="plus-text"><span class="plus-sizebig">+</span> <br> 05</span>
								</figure></a>
							</div>
							
						</div>
					</div>
					
					<div class="pro-desh-box">
						<div class="pro-desh-r1">
							<div class="pro-inleftbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
									<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21"><span class="numcircle-box">5</span></span>
								8</a></p>
							</div>
							<div class="pro-inrightbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
								<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13"> <span class="numcircle-box">2</span></span>
								2</a></p>
							</div>
						</div>
					</div>
				</div> --> 
				<!-- -->
				
				<!-- List Desh Product -->
				<!-- <div class="pro-desh-row">
					<div class="pro-desh-box delivery-desh">
						<p class="pro-a">Approved</p>
						<p class="pro-b">May /30 /2018</p>
					</div>
					
					<div class="pro-desh-box dcol-1">
						<div class="desh-head-wwq">
							<div class="desh-inblock">
								<h3 class="pro-head space-b"><a href="<?php echo base_url();?>designer/request/project_info">Design a logo for merchant Processing Company</a></h3>
								<p class="pro-b">Identity and Barnding</p>
							</div>
							<div class="desh-inblock col-w1">
								<p class="neft"><span class="green text-uppercase">Completed</span></p>
							</div>
						</div>
					</div>
					
					<div class="pro-desh-box dcol-2">
						<div class="pro-circle-list clearfix">
							<p class="pro-a">Delivery</p>
							<p class="space-a"></p>
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-plus">
									<span class="plus-text"><span class="plus-sizebig">+</span> <br> 05</span>
								</figure></a>
							</div>
							
						</div>
					</div>
					
					<div class="pro-desh-box">
						<div class="pro-desh-r1">
							<div class="pro-inleftbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
									<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21"><span class="numcircle-box">5</span></span>
								8</a></p>
							</div>
							<div class="pro-inrightbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
								<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13"> <span class="numcircle-box">2</span></span>
								2</a></p>
							</div>
						</div>
					</div>
				</div>  -->
				<!-- -->
				
				<!-- List Desh Product -->
				<!-- <div class="pro-desh-row">
					<div class="pro-desh-box delivery-desh">
						<p class="pro-a">Approved</p>
						<p class="pro-b">May /30 /2018</p>
					</div>
					
					<div class="pro-desh-box dcol-1">
						<div class="desh-head-wwq">
							<div class="desh-inblock">
								<h3 class="pro-head space-b"><a href="<?php echo base_url();?>designer/request/project_info">Design a logo for merchant Processing Company</a></h3>
								<p class="pro-b">Identity and Barnding</p>
							</div>
							<div class="desh-inblock col-w1">
								<p class="neft"><span class="green text-uppercase">Completed</span></p>
							</div>
						</div>
					</div>
					
					<div class="pro-desh-box dcol-2">
						<div class="pro-circle-list clearfix">
							<p class="pro-a">Delivery</p>
							<p class="space-a"></p>
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-plus">
									<span class="plus-text"><span class="plus-sizebig">+</span> <br> 05</span>
								</figure></a>
							</div>
						</div>
					</div>
					
					<div class="pro-desh-box">
						<div class="pro-desh-r1">
							<div class="pro-inleftbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
									<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21"><span class="numcircle-box">5</span></span>
								8</a></p>
							</div>
							<div class="pro-inrightbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
								<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13"> <span class="numcircle-box">2</span></span>
								2</a></p>
							</div>
						</div>
					</div>
				</div> --> 
				<!-- -->
				
				<!-- List Desh Product -->
				<!-- <div class="pro-desh-row">
					<div class="pro-desh-box delivery-desh">
						<p class="pro-a">Approved</p>
						<p class="pro-b">May /30 /2018</p>
					</div>
					
					<div class="pro-desh-box dcol-1">
						<div class="desh-head-wwq">
							<div class="desh-inblock">
								<h3 class="pro-head space-b"><a href="<?php echo base_url();?>designer/request/project_info">Design a logo for merchant Processing Company</a></h3>
								<p class="pro-b">Identity and Barnding</p>
							</div>
							<div class="desh-inblock col-w1">
								<p class="neft"><span class="green text-uppercase">Completed</span></p>
							</div>
						</div>
					</div>
					
					<div class="pro-desh-box dcol-2">
						<div class="pro-circle-list clearfix">
							<p class="pro-a">Delivery</p>
							<p class="space-a"></p>
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-img">
									<img src="<?php echo base_url();?>theme/designer_css/images/pro1.png" class="img-responsive">
								</figure>
								<p class="pro-circle-txt text-center">Gabriel</p></a>
							</div>
							
							<div class="pro-circle-box">
								<a href=""><figure class="pro-circle-plus">
									<span class="plus-text"><span class="plus-sizebig">+</span> <br> 05</span>
								</figure></a>
							</div>
							
						</div>
					</div>
					
					<div class="pro-desh-box">
						<div class="pro-desh-r1">
							<div class="pro-inleftbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
									<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21"><span class="numcircle-box">5</span></span>
								8</a></p>
							</div>
							<div class="pro-inrightbox">
								<p class="pro-a inline-per"><a href="<?php echo base_url();?>designer/request/project_info">
								<span class="inline-imgsssx"><img src="<?php echo base_url();?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13"> <span class="numcircle-box">2</span></span>
								2</a></p>
							</div>
						</div>
					</div>
				</div>  -->
				<!-- -->
				
			</div>

			
			<p class="space-e"></p>
			<?php if(sizeof($approved_project) >= 20){ ?>
			<div class="loader">
				<a class="loader-link text-uppercase" href=""><img src="<?php echo base_url();?>theme/designer_css/images/icon-loader.png" class="img-responsive" width="28"> Loading</a>
			</div><?php } ?>
			
		</div>
	</section>