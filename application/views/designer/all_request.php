<!-- <h2 class="float-xs-left content-title-main" style="display:inline-block;padding-left:40px">Projects</h2> -->
</div>
<div class="col-md-12" style="background:white;">
    <div class="">
        <section id="content-wrapper">
            <style>
                /* padding css start */
                .pb0{ padding-bottom:0px; }
                .pb5{ padding-bottom:5px; }
                .pb10{ padding-bottom:10px; }
                .pt0{ padding-top:0px; }
                .pt5{ padding-top:5px; }
                .pt10{ padding-top:10px; }
                .pl0{ padding-left:0px; }
                .pl5{ padding-left:5px; }
                .pl10{ padding-left:10px; }
                /* padding css end */
                .greenbackground { background-color:#98d575 !important; }
                .greentext { color:#98d575; }
                .orangebackground { background-color:#f7941f; }
                .pinkbackground { background-color: #ec4159; }
                .orangetext { color:#f7941f; }
                .bluebackground { background-color:#409ae8; }
                .bluetext{ color:#409ae8; }
                .whitetext { color:#fff !important; }
                .blacktext { color:#000; }
                .greytext { color:#cccccc; }
                .greybackground { background-color:#ededed; }
                .darkblacktext { color:#1a3147; } 
                .pinktext { color: #ec4159; }
                .weight600 { font-weight:600; }
                .font18 { font-size:18px; }
                .textleft { text-align:left; }
                .textright { text-align:right; }
                .textcenter { text-align:center; }
                .pl20 { padding-left:20px; }

                .numbercss{
                    font-size: 20px !important;
                    padding: 8px 0px !important;
                    font-weight: 600 !important;
                    letter-spacing: 0px !important;
                    padding: 40px 0px 0px 0px !important;
                }
                .table-hover tbody tr:hover {
                    background-color: unset !important;
                }
                .projecttitle{
                    font-size: 16px;
                    line-height:18px;
                    padding-bottom: 0px;
                    text-align: left;
                    padding-left: 20px;
                }
                .trborder{
                    border: 1px solid #000;
                    background: unset;

                }
                .trborder:hover{
                    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                }
                table {     border-spacing: 0 1em; }
                .trash{     
                    color: #ec4159;
                    font-size: 25px; 
                }

/*
                .nav-tab-pills-image ul li .nav-link {
                    color:#1a3147;
                    font-weight:600;
                    padding: 7px 25px;
                }

                .content .nav-tab-pills-image .nav-item.active a {

                    border:unset !important;
                }
                .wrapper .nav-tab-pills-image ul li .nav-link:hover{
                    color:#000;

                }

                .wrapper .nav-tab-pills-image ul li .nav-link{
                    color: #2f4458;
                    height:43px;
                }
                .nav-tab-pills-image ul li .nav-link:hover {
                    border-bottom: none !important;
                }
                .wrapper .nav-tab-pills-image ul li .nav-link:focus
                {
                    color:#fff;
                    border:none;
                }
                .content .nav-tab-pills-image .nav-item.active a:hover{
                    color:#fff;

                }
                .content .nav-tab-pills-image .nav-item.active dd {
                    background:unset !important;
                }
                .nav-tab-pills-image ul li .nav-link:hover {
                    border-bottom: none !important;
                }
*/

                table.custom-table.table-striped tbody tr:nth-of-type(even) {
                    background-color: unset;
                }
                input.empty {
                    font-family: FontAwesome;
                    font-style: normal;
                    font-weight: normal;
                    text-decoration: inherit;
                }
				.projectstatus {
					font-size: 15px;
					text-transform: capitalize;
				}
                .table_draft_sec .desc .queue{
                    display: inline-block;
                    padding: 2px 0 0 !important;
                    min-width: 70px;
                    text-align: center;
                    line-height: 14px;
                }
            </style>
            <div class="content">

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="nav-tab-pills-image mytab">
                            <ul class="nav nav-tabs" role="tablist" style="border:none;">                      
                                <li class="nav-item active">
                                    <a class="nav-link tabmenu" data-toggle="tab" href="#designs_request_tab" role="tab">
                                        Active (<?php echo sizeof($active_project); ?>)
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tabmenu" data-toggle="tab" href="#inprogressrequest" role="tab">
                                        In-Queue (<?php echo sizeof($in_queue_project); ?>)
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tabmenu" data-toggle="tab" href="#pending_designs_tab" role="tab">
                                        Pending Approval (<?php echo sizeof($pending_approval_project); ?>)
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link tabmenu" data-toggle="tab" href="#approved_designs_tab" role="tab">
                                        Completed (<?php echo sizeof($approved_project); ?>)
                                    </a>
                                </li>
                                <li class="nav-item float-right">
                                    <p class="nav-link dd">
                                        <i class="fa fa-search onlysearch"></i>
                                        <input class="fbold" type="text" id="myInput" onkeyup="myFunction()" name="search_request" placeholder="Search" style="border:none;">
                                    </p>
                                </li>
                            </ul>

                        <!--
                            <ul class="nav nav-tabs" role="tablist" style="border:none;">                      
                                <li class="nav-item active" style="background: #ededed;border-radius: 12px 0px 0px 12px;">
                                    <a class="nav-link" data-toggle="tab" href="#designs_request_tab" role="tab" style="width:245px;border-radius: 12px 0px 0px 12px;">
                                        Active Projects (<?php echo sizeof($active_project); ?>)
                                    </a>
                                </li>
                                <li class="nav-item" style="background: #ededed;margin-left:6px;">
                                    <a class="nav-link" data-toggle="tab" href="#inprogressrequest" role="tab">
                                        In-Queue (<?php echo sizeof($in_queue_project); ?>)
                                    </a>
                                </li>
                                <li class="nav-item" style="background: #ededed;margin-left:6px;">
                                    <a class="nav-link" data-toggle="tab" href="#pending_designs_tab" role="tab">
                                        Pending Approval (<?php echo sizeof($pending_approval_project); ?>)
                                    </a>
                                </li>
                                <li class="nav-item" style="background: #ededed;margin-left:6px;border-radius: 0px 12px 12px 0px;">
                                    <a class="nav-link" data-toggle="tab" href="#approved_designs_tab" role="tab" style="border-radius: 0px 12px 12px 0px;">
                                        Completed (<?php echo sizeof($approved_project); ?>)
                                    </a>
                                </li>
                                <li class="weight600 darkblacktext font18" style="display:inline-block;float: right;">
                                    <p class="nav-link dd">
                                        <i class="fa fa-search onlysearch"></i>
                                        <input class="fbold" type="text" id="myInput" onkeyup="myFunction()" name="search_request" placeholder="Search" style="border:none;">
                                    </p>
                                </li>
                            </ul>
                        -->

                            <div class="tab-content">
                                <div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="table_draft_sec table_draft_complete design_complete pending_approvel">
                                            <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none; padding-top: 15px;">
                                                <tbody class="">
                                                <?php //echo "<pre>"; print_r($active_project); echo "</pre>";
                                                for ($i = 0; $i < sizeof($active_project); $i++) { ?>
                                                    <?php
                                                    $colorclass= "";
                                                     if ($active_project[$i]['status'] == "checkforapprove") {
                                                        $status = "Review your design";
                                                        $colorclass=" ";
                                                        //echo "greentext";
                                                    } elseif ($active_project[$i]['status'] == "active") {
                                                        
                                                        $status = "IN-Progress";
                                                        $colorclass="";
                                                        //echo "bluetext";
                                                    } elseif ($active_project[$i]['status'] == "disapprove") {        
                                                        $status = "Revision";
                                                        $colorclass="red";
                                                        //echo "orangetext";
                                                    } else {  
                                                        $status = "In-Queue";
                                                        $colorclass="";
                                                       // echo "greytext";
                                                    } ?>
                                                    <tr onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_request/<?php echo $active_project[$i]['id']; ?>'" class="trborder <?php echo $colorclass; ?>" id="<?php echo $active_project[$i]['id']; ?>" data-indexid="0">
                                                        <?php
                                                        if (checkdate(date("m", strtotime($active_project[$i]['dateinprogress'])), date("d", strtotime($active_project[$i]['dateinprogress'])), date("Y", strtotime($active_project[$i]['dateinprogress'])))) {
                                                            $over_date = "";
                                                            if ($active_project[$i]['plan_turn_around_days']) {
                                                                //$active_project[$i]['dateinprogress'] = "2018-04-12";
                                                                $date = date("Y-m-d", strtotime($active_project[$i]['dateinprogress']));
                                                                $time = date("g:i:s", strtotime($active_project[$i]['dateinprogress']));

                                                                if ($active_project[$i]['status_designer'] == "disapprove") {
                                                                    $active_project[$i]['plan_turn_around_days'] = $active_project[$i]['plan_turn_around_days'] + 1;
                                                                }
                                                                $active_project[$i]['plan_turn_around_days'] = "2";
                                                                $date = date("m/d/Y g:i a", strtotime($date . " " . $active_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                                                //$date = date("m/d/Y g:i a",strtotime($date." 6 weekdays ".$time)); 
                                                                $diff = date_diff(date_create(date("Y-m-d", strtotime($active_project[$i]['dateinprogress']))), date_create($date));
                                                                //print_r($diff);
                                                                //echo $active_project[$i]['dateinprogress'];
                                                                //echo $date;
                                                            }
                                                        }
                                                        
                                                        ?>
                                                        <td class="completed">
                                                            <span>Delivery</span>
                                                            <span>
                                                            <?php
                                                                $created_date =  date($active_project[$i]['dateinprogress']);
                                                                // $expected_date = date('m/d/Y H:i:s', strtotime($created_date . ' +1 day'));
                                                                $expected_date = date('m/d/Y', strtotime($created_date . ' +1 day'));
                                                                if ( $active_project[ $i ][ 'modified' ] == "" ) {
                                                                    echo $expected_date;
                                                                } else {
                                                                    echo date( "m/d/Y", strtotime( $active_project[ $i ][ 'modified' ] ) );
                                                                }
                                                                // echo date("m/d/Y",strtotime($date));
                                                            ?>
                                                           </span>
                                                        </td>
                                                        
                                                        <td class="desc">
                                                            <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $active_project[$i]['title']; ?></p>
                                                            <p class="small">Shift Design</p>
                                                            <p><span class="queue green"><?php echo $status; ?></span></p>
                                                        </td>
                                                        
                                                        <td class="designer_assig">
                                                            <?php if ($active_project[$i]['profile_picture']) { ?>
                                                             <div class="img float-left"><img src="<?php echo base_url() . 'uploads/profile_picture/' . $active_project[$i]['profile_picture'] ;?>" class="img-fluid"></div>
                                                            <?php }  else { ?>
                                                             <div class="img float-left"><img src="http://backup.graphicszoo.com/uploads/profile_picture/13b2d60af0a45fefd0d0d0633339a453.jpeg" class="img-fluid"></div>
                                                            <?php } ?>
                                                            <p class="darkblacktext weight600 name"><?php echo $active_project[$i]['customer_first_name'] . " " . $active_project[$i]['customer_last_name']; ?></p>
                                                            <p class="darkblacktext client">Client</p>
                                                        </td>

                                                        <td class="notification">
                                                            <p class="file">
                                                                <span><i class="fa fa-file" aria-hidden="true"></i></span>
                                                                <span class="num">0</span>
                                                            </p>
                                                            <p class="darkblacktext">
                                                                <img src="http://backup.graphicszoo.com/public/img/img/messageicon.png" class="img-responsive imgauto" alt="">
                                                                <span class="num"><?php echo $active_project[$i]['total_chat']; ?></span>
                                                            </p>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                            </div><!-- table_draft_sec -->

                                            <?php /*
                                            <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none;">
                                               <!--<thead>
                                                   <tr>
                                                       <th>Id</th>
                                                       <th>Project Name</th>
                                                                                                       <th>Customer Name</th>
                                                                                                       <th>Latest Update</th>
                                                                                                       <th>Due Date</th>
                                                                                                       <th>Messages</th>
                                                                                                       <th>Rating</th>
                                                   </tr>
                                               </thead>-->
                                                <tbody>

                                                    <?php for ($i = 0; $i < sizeof($active_project); $i++) { ?>
                                                        <tr class="trborder" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_request/<?php echo $active_project[$i]['id']; ?>'">
                                                            <?php
                                                            if (checkdate(date("m", strtotime($active_project[$i]['dateinprogress'])), date("d", strtotime($active_project[$i]['dateinprogress'])), date("Y", strtotime($active_project[$i]['dateinprogress'])))) {
                                                                $over_date = "";
                                                                if ($active_project[$i]['plan_turn_around_days']) {
                                                                    //$active_project[$i]['dateinprogress'] = "2018-04-12";
                                                                    $date = date("Y-m-d", strtotime($active_project[$i]['dateinprogress']));
                                                                    $time = date("g:i:s", strtotime($active_project[$i]['dateinprogress']));

                                                                    if ($active_project[$i]['status_designer'] == "disapprove") {
                                                                        $active_project[$i]['plan_turn_around_days'] = $active_project[$i]['plan_turn_around_days'] + 1;
                                                                    }
                                                                    $active_project[$i]['plan_turn_around_days'] = "2";
                                                                    $date = date("m/d/Y g:i a", strtotime($date . " " . $active_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                                                    //$date = date("m/d/Y g:i a",strtotime($date." 6 weekdays ".$time)); 
                                                                    $diff = date_diff(date_create(date("Y-m-d", strtotime($active_project[$i]['dateinprogress']))), date_create($date));
                                                                    //print_r($diff);
                                                                    //echo $active_project[$i]['dateinprogress'];
                                                                    //echo $date;
                                                                }
                                                            }
                                                            $current_date = strtotime(date("Y-m-d"));
                                                            $now = time(); // or your date as well

                                                            $expiration_date = strtotime($date);
                                                            $datediff = $now - $expiration_date;
                                                            //echo "now: ".$now."</br>";
                                                            //echo "now: ".$now."</br>";//
                                                            //echo $datediff; exit;
                                                            $date_due_day = round($datediff / (60 * 60 * 24));
                                                            //echo $date_due_day;
                                                            $color = "";
                                                            $text_color = "white";
                                                            if ($date_due_day == 0) {
                                                                $date_due_day = "Due  </br>Today";
                                                                $color = "#f7941f";
                                                            } else
                                                            if ($date_due_day == (-1)) {
                                                                $date_due_day = "Due  </br>Tomorrow";
                                                                $color = "#98d575";
                                                            } else
                                                            if ($date_due_day > 0) {
                                                                $date_due_day = "Due from  </br>" . number_format($date_due_day) . " days";
                                                                $color = "red";
                                                            } else if ($date_due_day < 0) {
                                                                $date_due_day = "Due from </br>" . number_format($date_due_day) . " days";
                                                                $text_color = "black";
                                                            }
                                                            //echo round($datediff / (60 * 60 * 24));
                                                            ?>
                                                            <td class="whitetext numbercss" style="color:<?php echo $text_color; ?>;width: 100px;padding:5px;background:<?php echo $color; ?>"><span><?php echo $date_due_day; ?></span><p class="whitetext" style="font-size: 16px;">

                                                                    <?php
                                                                    if (checkdate(date("m", strtotime($active_project[$i]['dateinprogress'])), date("d", strtotime($active_project[$i]['dateinprogress'])), date("Y", strtotime($active_project[$i]['dateinprogress'])))) {
                                                                        $over_date = "";
                                                                        if ($active_project[$i]['plan_turn_around_days']) {
                                                                            $date = date("Y-m-d", strtotime($active_project[$i]['dateinprogress']));
                                                                            $time = date("g:i:s", strtotime($active_project[$i]['dateinprogress']));

                                                                            if ($active_project[$i]['status_designer'] == "disapprove") {
                                                                                $active_project[$i]['plan_turn_around_days'] = $active_project[$i]['plan_turn_around_days'] + 1;
                                                                            }

                                                                            $date = date("m/d/Y g:i a", strtotime($date . " " . $active_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                                                            $diff = date_diff(date_create(date("Y-m-d", strtotime($active_project[$i]['dateinprogress']))), date_create($date));
                                                                            //print_r($diff);
                                                                            //echo $active_project[$i]['dateinprogress'];
                                                                            //echo $date;
                                                                        }
                                                                    }
                                                                    ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="darkblacktext weight600 projecttitle pb0 pt5" style="text-transform: capitalize;"><?php echo $active_project[$i]['title']; ?></p>
                                                                <p style="text-align:left;padding-left:20px;" class="<?php if ($active_project[$i]['status'] == "checkforapprove") {
                                                                        
                                                                		$status = "Review your design";
                                                                		echo "greentext";
                                                                    } elseif ($active_project[$i]['status'] == "active") {
                                                                        
                                                                		$status = "Design In Progress";
                                                                		echo "bluetext";
                                                                    } elseif ($active_project[$i]['status'] == "disapprove") {
                                                                        
                                                                		$status = "Revision In Progress";
                                                                		echo "orangetext";
                                                                    } else {
                                                                        
                                                                		$status = "In-Queue";
                                                                		echo "greytext";
                                                                    } ?>">
																	<?php echo $status; ?>
																</p>
                                                                <p class="darkblacktext textleft pl20"><?php echo substr($active_project[$i]['description'], 0, 45); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Client</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo $active_project[$i]['customer_first_name'] . " " . $active_project[$i]['customer_last_name']; ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Latest Update</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo date("m/d/Y", strtotime($active_project[$i]['dateinprogress'])); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Due Date</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo date("m/d/Y",strtotime($date)); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Messages</p>
                                                                <p class="darkblacktext weight600">
																<img src="<?php echo base_url(); ?>public/img/img/messageicon.png" class="img-responsive imgauto" alt="">
																<span class="orangetext pl5"><?php echo $active_project[$i]['total_chat']; ?></span></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Rating</p>
                                                                <?php
																for($j=1;$j<=5;$j++){
																	if($j <= $active_project[$i]['total_grade'][0]['grade']){ ?>
																			<i class="fa fa-star pinktext"></i>
																<?php	}else{ ?>
																		<i class="fa fa-star greytext"></i>
																<?php	}
																}
																?>
                                                            </td>
                                                        </tr> 
                                                    <?php } ?>

                                                </tbody>
                                            </table> */?>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane  content-datatable datatable-width" id="inprogressrequest" role="tabpanel"> 
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table_draft_sec table_draft_complete design_complete in_queue_sec">
                                            <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none; padding-top: 15px;">
                                                <tbody class="">
                                                <?php for ($i = 0; $i < sizeof($in_queue_project); $i++) { ?>
                                                    <tr onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_request/<?php echo $in_queue_project[$i]['id']; ?>'" class="trborder" id="<?php echo $in_queue_project[$i]['id']; ?>" data-indexid="0">
                                                        <?php
                                                            if (checkdate(date("m", strtotime($in_queue_project[$i]['dateinprogress'])), date("d", strtotime($in_queue_project[$i]['dateinprogress'])), date("Y", strtotime($in_queue_project[$i]['dateinprogress'])))) {
                                                                $over_date = "";
                                                                if ($in_queue_project[$i]['plan_turn_around_days']) {
                                                                    $date = date("Y-m-d", strtotime($in_queue_project[$i]['dateinprogress']));
                                                                    $time = date("g:i:s", strtotime($in_queue_project[$i]['dateinprogress']));

                                                                    if ($in_queue_project[$i]['status_designer'] == "disapprove") {
                                                                        $in_queue_project[$i]['plan_turn_around_days'] = $in_queue_project[$i]['plan_turn_around_days'] + 1;
                                                                    }

                                                                    $date = date("m/d/Y g:i a", strtotime($date . " " . $in_queue_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                                                    $diff = date_diff(date_create(date("Y-m-d", strtotime($in_queue_project[$i]['dateinprogress']))), date_create($date));
                                                                    //print_r($diff);
                                                                    //echo $in_queue_project[$i]['dateinprogress'];
                                                                    //echo $date;
                                                                }
                                                            }
                                                            ?>
                                                        <td class="completed">
                                                            <span>Delivery</span>
                                                            <span>MM/DD/YY</span>
                                                        </td>
                                                        <?php if ($in_queue_project[$i]['status'] == "checkforapprove") {
                                                                
                                                                $status = "Review your design";
                                                                //echo "greentext";
                                                            } elseif ($in_queue_project[$i]['status'] == "active") {
                                                                
                                                                $status = "Design In Progress";
                                                               // echo "bluetext";
                                                            } elseif ($in_queue_project[$i]['status'] == "disapprove") {
                                                                
                                                                $status = "Revision In Progress";
                                                               // echo "orangetext";
                                                            } else {
                                                                
                                                                $status = "In-Queue";
                                                               // echo "greytext";
                                                            } ?>
                                                        <td class="desc">
                                                            <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $in_queue_project[$i]['title']; ?></p>
                                                            <p class="small">Shift Design</p>
                                                            <p><span class="queue green"><?php echo $status; ?></span></p>
                                                        </td>
                                                        
                                                        <td class="designer_assig">
                                                            <div class="img float-left"><img src="http://backup.graphicszoo.com/uploads/profile_picture/13b2d60af0a45fefd0d0d0633339a453.jpeg" class="img-fluid"></div>
                                                            <p class="darkblacktext weight600 name"><?php echo $in_queue_project[$i]['customer_first_name'] . " " . $in_queue_project[$i]['customer_last_name']; ?></p>
                                                            <p class="darkblacktext client">Client</p>
                                                        </td>

                                                        <td class="notification">
                                                            <p class="file">
                                                                <span><i class="fa fa-file" aria-hidden="true"></i></span>
                                                                <span class="num">0</span>
                                                            </p>
                                                            <p class="darkblacktext">
                                                                <img src="http://backup.graphicszoo.com/public/img/img/messageicon.png" class="img-responsive imgauto" alt="">
                                                                <span class="num"><?php echo $in_queue_project[$i]['total_chat']; ?></span>
                                                            </p>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                            </div><!-- table_draft_sec -->

                                           <?php /* <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none;">
                                                <!--<thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Project Name</th>
                                                                                                        <th>Customer Name</th>
                                                                                                        <th>In-progress</th>
                                                                                                        <th>Messages</th>
                                                                                                        <th>Icon</th>
                                                                                   
                                                    </tr>
                                                </thead>-->

                                                <tbody>
                                                    <?php for ($i = 0; $i < sizeof($in_queue_project); $i++) { ?>
                                                        <tr class="trborder" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_request/<?php echo $in_queue_project[$i]['id']; ?>'">

                                                            <td class="whitetext numbercss greybackground" style="width: 100px;">In </br> Queue<p class="whitetext" style="font-size: 16px;">

                                                            <?php
                                                            if (checkdate(date("m", strtotime($in_queue_project[$i]['dateinprogress'])), date("d", strtotime($in_queue_project[$i]['dateinprogress'])), date("Y", strtotime($in_queue_project[$i]['dateinprogress'])))) {
                                                                $over_date = "";
                                                                if ($in_queue_project[$i]['plan_turn_around_days']) {
                                                                    $date = date("Y-m-d", strtotime($in_queue_project[$i]['dateinprogress']));
                                                                    $time = date("g:i:s", strtotime($in_queue_project[$i]['dateinprogress']));

                                                                    if ($in_queue_project[$i]['status_designer'] == "disapprove") {
                                                                        $in_queue_project[$i]['plan_turn_around_days'] = $in_queue_project[$i]['plan_turn_around_days'] + 1;
                                                                    }

                                                                    $date = date("m/d/Y g:i a", strtotime($date . " " . $in_queue_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                                                    $diff = date_diff(date_create(date("Y-m-d", strtotime($in_queue_project[$i]['dateinprogress']))), date_create($date));
                                                                    //print_r($diff);
                                                                    //echo $in_queue_project[$i]['dateinprogress'];
                                                                    //echo $date;
                                                                }
                                                            }
                                                            ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $in_queue_project[$i]['title']; ?></p>
                                                                <p style="text-align:left;padding-left:20px;" class="<?php if ($in_queue_project[$i]['status'] == "checkforapprove") {
        
		$status = "Review your design";
		echo "greentext";
    } elseif ($in_queue_project[$i]['status'] == "active") {
        
		$status = "Design In Progress";
		echo "bluetext";
    } elseif ($in_queue_project[$i]['status'] == "disapprove") {
        
		$status = "Revision In Progress";
		echo "orangetext";
    } else {
        
		$status = "In-Queue";
		echo "greytext";
    } ?>"><?php echo $status; ?></p>
                                                                <p class="darkblacktext textleft pl20"><?php echo substr($in_queue_project[$i]['description'], 0, 50); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Client</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo $in_queue_project[$i]['customer_first_name'] . " " . $in_queue_project[$i]['customer_last_name']; ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">In-progress</p>
                                                                <p class="darkblacktext weight600 font12"><?php if(!empty($in_queue_project[$i]['dateinprogress'])){ echo date("m/d/Y", strtotime($in_queue_project[$i]['dateinprogress']));}else{echo "Not Inprogress ";} ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Messages</p>
                                                                <p class="darkblacktext weight600">
																<img src="<?php echo base_url(); ?>public/img/img/messageicon.png" class="img-responsive imgauto" alt="">
																<span class="orangetext pl5"><?php echo $in_queue_project[$i]['total_chat']; ?></span></p>
                                                            </td>
                                                            <td>
                                                               <?php
																for($j=1;$j<=5;$j++){
																	if($j <= $in_queue_project[$i]['total_grade'][0]['grade']){ ?>
																			<i class="fa fa-star pinktext"></i>
																<?php	}else{ ?>
																		<i class="fa fa-star greytext"></i>
																<?php	}
																}
																?>
                                                            </td>
                                                        </tr> 
<?php } ?>
                                                </tbody>
                                            </table> */?>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane content-datatable datatable-width" id="pending_designs_tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">
<div class="table_draft_sec table_draft_complete design_complete pending_approvel">
<table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none; padding-top: 15px;">
    <tbody class="">
    <?php //echo "<pre>"; print_r($pending_approval_project); echo "</pre>";
    for ($i = 0; $i < sizeof($pending_approval_project); $i++) { ?>
        <tr onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_request/<?php echo $pending_approval_project[$i]['id']; ?>'" class="trborder" id="<?php echo $pending_approval_project[$i]['id']; ?>" data-indexid="0">
            <?php
            if (checkdate(date("m", strtotime($pending_approval_project[$i]['dateinprogress'])), date("d", strtotime($pending_approval_project[$i]['dateinprogress'])), date("Y", strtotime($pending_approval_project[$i]['dateinprogress'])))) {
                $over_date = "";
                if ($pending_approval_project[$i]['plan_turn_around_days']) {
                    //$active_project[$i]['dateinprogress'] = "2018-04-12";
                    $date = date("Y-m-d", strtotime($pending_approval_project[$i]['dateinprogress']));
                    $time = date("g:i:s", strtotime($pending_approval_project[$i]['dateinprogress']));

                    if ($pending_approval_project[$i]['status_designer'] == "disapprove") {
                        $pending_approval_project[$i]['plan_turn_around_days'] = $pending_approval_project[$i]['plan_turn_around_days'] + 1;
                    }
                    $pending_approval_project[$i]['plan_turn_around_days'] = "2";
                    $date = date("m/d/Y g:i a", strtotime($date . " " . $pending_approval_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                    //$date = date("m/d/Y g:i a",strtotime($date." 6 weekdays ".$time)); 
                    $diff = date_diff(date_create(date("Y-m-d", strtotime($pending_approval_project[$i]['dateinprogress']))), date_create($date));
                    //print_r($diff);
                    //echo $active_project[$i]['dateinprogress'];
                    //echo $date;
                }
            }
            $current_date = strtotime(date("Y-m-d"));
            $now = time(); // or your date as well

            $expiration_date = strtotime($date);
            $exitdate = date("d-m-Y" , $expiration_date);
            $datediff = $now - $expiration_date;
            //echo "now: ".$now."</br>";
            //echo "now: ".$now."</br>";//
            //echo $datediff; exit;
            $date_due_day = round($datediff / (60 * 60 * 24));
            //echo $date_due_day;
            $color = "";
            $text_color = "white";
            if ($date_due_day == 0) {
                $date_due_day = "Today";
                $color = "#f7941f";
            } else
            if ($date_due_day == (-1)) {
                $date_due_day = "Tomorrow";
                $color = "#98d575";
            } else
            if ($date_due_day > 0) {
                $date_due_day =  number_format($date_due_day) . " days";
                $color = "red";
            } else if ($date_due_day < 0) {
                $date_due_day =  number_format($date_due_day) . " days";
                $text_color = "black";
            }
            //echo round($datediff / (60 * 60 * 24));
            ?>
            <td class="completed">
                <span>Delivery</span>
                <span><?php echo $date_due_day; ?></span>
            </td>
            <?php if ($pending_approval_project[$i]['status_designer'] == "checkforapprove") {
                $status = "In-Progress";
            } elseif ($pending_approval_project[$i]['status_designer'] == "active") {
                $status = "Design In Progress";
            } elseif ($pending_approval_project[$i]['status_designer'] == "disapprove") {
                $status = "Revision In Progress";
            } else {
                $status = "In-Queue";
            } ?>
            <td class="desc">
                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $pending_approval_project[$i]['title']; ?></p>
                <p class="small">Shift Design</p>
                <p><span class="queue green"><?php echo $status; ?></span></p>
            </td>
            
            <td class="designer_assig">
                <?php if ($pending_approval_project[$i]['profile_picture']) { ?>
                 <div class="img float-left"><img src="<?php echo base_url() . 'uploads/profile_picture/' . $pending_approval_project[$i]['profile_picture'] ;?>" class="img-fluid"></div>
                <?php }  else { ?>
                 <div class="img float-left"><img src="http://backup.graphicszoo.com/uploads/profile_picture/13b2d60af0a45fefd0d0d0633339a453.jpeg" class="img-fluid"></div>
                <?php } ?>
                <p class="darkblacktext weight600 name"><?php echo $pending_approval_project[$i]['customer_first_name'] . " " . $pending_approval_project[$i]['customer_last_name']; ?></p>
                <p class="darkblacktext client">Client</p>
            </td>

            <td class="notification">
                <p class="file">
                    <span><i class="fa fa-file" aria-hidden="true"></i></span>
                    <span class="num">0</span>
                </p>
                <p class="darkblacktext">
                    <img src="http://backup.graphicszoo.com/public/img/img/messageicon.png" class="img-responsive imgauto" alt="">
                    <span class="num"><?php echo $pending_approval_project[$i]['total_chat']; ?></span>
                </p>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
</div><!-- table_draft_sec -->

                                        <?php /*
                                            <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none;">						
                                                <!--<thead>
                                                    <tr>
                                                       <th>Id</th>
                                                        <th>Project Name</th>
                                                                                                        <th>Customer Name</th>
                                                                                                        <th>In-progress</th>
                                                                                                        <th>Messages</th>
                                                                                                        <th>Icon</th>
                                                    </tr>
                                                </thead>-->
                                                <tbody>
                                                        <?php for ($i = 0; $i < sizeof($pending_approval_project); $i++) { ?>
                                                        <tr class="trborder" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_request/<?php echo $pending_approval_project[$i]['id']; ?>'">

                                                            <?php
                                                            if (checkdate(date("m", strtotime($pending_approval_project[$i]['dateinprogress'])), date("d", strtotime($pending_approval_project[$i]['dateinprogress'])), date("Y", strtotime($pending_approval_project[$i]['dateinprogress'])))) {
                                                                $over_date = "";
                                                                if ($pending_approval_project[$i]['plan_turn_around_days']) {
                                                                    //$active_project[$i]['dateinprogress'] = "2018-04-12";
                                                                    $date = date("Y-m-d", strtotime($pending_approval_project[$i]['dateinprogress']));
                                                                    $time = date("g:i:s", strtotime($pending_approval_project[$i]['dateinprogress']));

                                                                    if ($pending_approval_project[$i]['status_designer'] == "disapprove") {
                                                                        $pending_approval_project[$i]['plan_turn_around_days'] = $pending_approval_project[$i]['plan_turn_around_days'] + 1;
                                                                    }
                                                                    $pending_approval_project[$i]['plan_turn_around_days'] = "2";
                                                                    $date = date("m/d/Y g:i a", strtotime($date . " " . $pending_approval_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                                                    //$date = date("m/d/Y g:i a",strtotime($date." 6 weekdays ".$time)); 
                                                                    $diff = date_diff(date_create(date("Y-m-d", strtotime($pending_approval_project[$i]['dateinprogress']))), date_create($date));
                                                                    //print_r($diff);
                                                                    //echo $active_project[$i]['dateinprogress'];
                                                                    //echo $date;
                                                                }
                                                            }
                                                            $current_date = strtotime(date("Y-m-d"));
                                                            $now = time(); // or your date as well

                                                            $expiration_date = strtotime($date);
                                                            $datediff = $now - $expiration_date;
                                                            //echo "now: ".$now."</br>";
                                                            //echo "now: ".$now."</br>";//
                                                            //echo $datediff; exit;
                                                            $date_due_day = round($datediff / (60 * 60 * 24));
                                                            //echo $date_due_day;
                                                            $color = "";
                                                            $text_color = "white";
                                                            if ($date_due_day == 0) {
                                                                $date_due_day = "Due  </br>Today";
                                                                $color = "#f7941f";
                                                            } else
                                                            if ($date_due_day == (-1)) {
                                                                $date_due_day = "Due  </br>Tomorrow";
                                                                $color = "#98d575";
                                                            } else
                                                            if ($date_due_day > 0) {
                                                                $date_due_day = "Due from  </br>" . number_format($date_due_day) . " days";
                                                                $color = "red";
                                                            } else if ($date_due_day < 0) {
                                                                $date_due_day = "Due from </br>" . number_format($date_due_day) . " days";
                                                                $text_color = "black";
                                                            }
                                                            //echo round($datediff / (60 * 60 * 24));
                                                            ?>
                                                            <td class="whitetext numbercss greenbackground" style="color:<?php echo $text_color; ?>width: 100px;padding:5px;background:<?php echo $color; ?>"><span style="<?php //echo $text_color; ?>">Pending</br> Approval<?php //echo $date_due_day; ?></span>
                                                                <p class="whitetext" style="font-size: 16px;"></p>
                                                            </td>
                                                            <td>
                                                                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $pending_approval_project[$i]['title']; ?></p>
                                                                <p style="text-align:left;padding-left:20px;" class="<?php if ($pending_approval_project[$i]['status_designer'] == "checkforapprove") {
        
		$status = "Review your design";
		echo "greentext";
    } elseif ($pending_approval_project[$i]['status_designer'] == "active") {
        
		$status = "Design In Progress";
		echo "bluetext";
    } elseif ($pending_approval_project[$i]['status_designer'] == "disapprove") {
        
		$status = "Revision In Progress";
		echo "orangetext";
    } else {
        
		$status = "In-Queue";
		echo "greytext";
    } ?>"><?php echo $status; ?></p>
                                                                <p class="darkblacktext textleft pl20"><?php echo substr($pending_approval_project[$i]['description'], 0, 50); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Client</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo $pending_approval_project[$i]['customer_first_name'] . " " . $pending_approval_project[$i]['customer_last_name']; ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">In-progress</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo date("m/d/Y", strtotime($pending_approval_project[$i]['dateinprogress'])); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Messages</p>
                                                                <p class="darkblacktext weight600">
																<img src="<?php echo base_url(); ?>public/img/img/messageicon.png" class="img-responsive imgauto" alt="">
																<span class="orangetext pl5"><?php echo $pending_approval_project[$i]['total_chat']; ?></span></p>
                                                            </td>
                                                            <td>
																<?php
																for($j=1;$j<=5;$j++){
																	if($j <= $pending_approval_project[$i]['total_grade'][0]['grade']){ ?>
																			<i class="fa fa-star pinktext"></i>
																<?php	}else{ ?>
																		<i class="fa fa-star greytext"></i>
																<?php	}
																}
																?>
                                                            </td>
                                                        </tr> 
<?php } ?>
                                                </tbody>
                                            </table> */ ?>
                                        </div>
                                    </div>
                                </div>



                                <div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">

<div class="table_draft_sec table_draft_complete design_complete">
<table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none; padding-top: 15px;">
    <tbody class="">
    <?php for ($i = 0; $i < sizeof($approved_project); $i++) { ?>
        <tr onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_request/<?php echo $approved_project[$i]['id']; ?>'" class="trborder" id="742" data-indexid="0">
            <td class="completed"><span>approved</span></td>
            <?php if ($approved_project[$i]['status_designer'] == "checkforapprove") {
                    
                    $status = "Review your design";
                   // echo "greentext";
                } elseif ($approved_project[$i]['status_designer'] == "active") {
                    
                    $status = "Design In Progress";
                   // echo "bluetext";
                } elseif ($approved_project[$i]['status_designer'] == "disapprove") {
                    
                    $status = "Revision In Progress";
                   // echo "orangetext";
                } elseif($approved_project[$i]['status_designer'] == "approved") {
                    
                    $status = "Completed";
                    //echo "greytext";
                }else{
                    
                    $status = "In-Queue";
                    //echo "greytext";
                } ?>
            <td class="desc">
                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $approved_project[$i]['title']; ?></p>
                <p class="small"><?php echo $approved_project[$i]['category']; ?></p>
                <p><span class="queue"><?php echo $status ?></span></p>
            </td>
            
            <td class="designer_assig">
                <?php if ($approved_project[$i]['profile_picture']) { ?>
                 <div class="img float-left"><img src="<?php echo base_url() . 'uploads/profile_picture/' . $approved_project[$i]['profile_picture'] ;?>" class="img-fluid"></div>
                <?php }  else { ?>
                 <div class="img float-left"><img src="http://backup.graphicszoo.com/uploads/profile_picture/13b2d60af0a45fefd0d0d0633339a453.jpeg" class="img-fluid"></div>
                <?php } ?>
                <p class="darkblacktext weight600 name"><?php echo $approved_project[$i]['customer_first_name'] . " " . $approved_project[$i]['customer_last_name']; ?></p>
                <p class="darkblacktext client">Client</p>
            </td>

            <td class="notification">
                <p class="file">
                    <span><i class="fa fa-file" aria-hidden="true"></i></span>
                    <span class="num">0</span>
                </p>
                <p class="darkblacktext">
                    <img src="http://backup.graphicszoo.com/public/img/img/messageicon.png" class="img-responsive imgauto" alt="">
                    <span class="num"><?php echo $approved_project[$i]['total_chat']; ?></span>
                </p>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
</div><!-- table_draft_sec -->

<?php /*
                                            <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none;">
                                                <!--<thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Project Name</th>
                                                                                                        <th>Customer Name</th>
                                                                                                        <th>In-progress</th>
                                                                                                        <th>Messages</th>
                                                                                                        <th>Icon</th>
                                                    </tr>
                                                </thead>-->
                                                <tbody>
<?php for ($i = 0; $i < sizeof($approved_project); $i++) { ?>
                                                        <tr class="trborder" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_request/<?php echo $approved_project[$i]['id']; ?>'">

                                                            <td class="whitetext numbercss greybackground" style="width: 100px;">Complete<p class="whitetext" style="font-size: 16px;">
<!--                                                            <td><p class="whitetext" style="font-size: 16px;">-->

                                                                    <?php
                                                                    if (checkdate(date("m", strtotime($approved_project[$i]['dateinprogress'])), date("d", strtotime($approved_project[$i]['dateinprogress'])), date("Y", strtotime($approved_project[$i]['dateinprogress'])))) {
                                                                        $over_date = "";
                                                                        if ($approved_project[$i]['plan_turn_around_days']) {
                                                                            $date = date("Y-m-d", strtotime($approved_project[$i]['dateinprogress']));
                                                                            $time = date("g:i:s", strtotime($approved_project[$i]['dateinprogress']));

                                                                            if ($approved_project[$i]['status_designer'] == "disapprove") {
                                                                                $approved_project[$i]['plan_turn_around_days'] = $approved_project[$i]['plan_turn_around_days'] + 1;
                                                                            }

                                                                            $date = date("m/d/Y g:i a", strtotime($date . " " . $approved_project[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                                                            $diff = date_diff(date_create(date("Y-m-d", strtotime($approved_project[$i]['dateinprogress']))), date_create($date));
                                                                            //print_r($diff);
                                                                            //echo $approved_project[$i]['dateinprogress'];
                                                                            //echo $date;
                                                                        }
                                                                    }
                                                                    ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $approved_project[$i]['title']; ?></p>
                                                                <p style="text-align:left;padding-left:20px;" class="<?php if ($approved_project[$i]['status_designer'] == "checkforapprove") {
        
		$status = "Review your design";
		echo "greentext";
    } elseif ($approved_project[$i]['status_designer'] == "active") {
        
		$status = "Design In Progress";
		echo "bluetext";
    } elseif ($approved_project[$i]['status_designer'] == "disapprove") {
        
		$status = "Revision In Progress";
		echo "orangetext";
    } elseif($approved_project[$i]['status_designer'] == "approved") {
        
		$status = "Complete";
		echo "greytext";
    }else{
        
		$status = "In-Queue";
		echo "greytext";
    } ?>"><?php echo $status; ?></p>
                                                                <p class="darkblacktext textleft pl20"><?php echo substr($approved_project[$i]['description'], 0, 50); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Client</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo $approved_project[$i]['customer_first_name'] . " " . $approved_project[$i]['customer_last_name']; ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">In-progress</p>
                                                                <p class="darkblacktext weight600 font12"><?php echo date("m/d/Y", strtotime($approved_project[$i]['dateinprogress'])); ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font12">Messages</p>
                                                                <p class="darkblacktext weight600">
																<img src="<?php echo base_url(); ?>public/img/img/messageicon.png" class="img-responsive imgauto" alt="">
																<span class="orangetext pl5"><?php echo $approved_project[$i]['total_chat']; ?></span></p>
                                                            </td>
                                                            <td>
                                                                <?php
																for($j=1;$j<=5;$j++){
																	if($j <= $approved_project[$i]['total_grade'][0]['grade']){ ?>
																			<i class="fa fa-star pinktext"></i>
																<?php	}else{ ?>
																		<i class="fa fa-star greytext"></i>
																<?php	}
																}
																?>
                                                            </td>
                                                        </tr> 
<?php } ?>
                                                </tbody>
                                            </table> */?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>

    <script>

        $(document).ready(function () {
            $(".more_less_Click").click(function () {
                $(".more_additional").toggle();
            });
            $(".read_more_btn").click(function () {
                $(".read_more").toggle();
            });
            $(".more_text_data_more_btn").click(function () {
                $(".more_text_data_more").toggle();
            });


        });
        $(document).ready(function ()
    {
        $("#myInput").on("keyup", function ()
        {
            var value = $(this).val().toLowerCase();
            $(".active table tr").filter(function ()
            {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
$('#iconified').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
        input.addClass('empty');
    } else {
        input.removeClass('empty');
    }
});
    </script>