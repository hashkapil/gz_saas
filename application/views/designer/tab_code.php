<div class="pro-desh-row">
    <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
    <div class="pro-desh-box dcol-1"  data-proid ='<?php echo $projects['id']; ?>' style="width: 30%;">
        <div class="desh-head-wwq">
            <div class="desh-inblock">
                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>designer/request/project_info/<?php echo $projects['id'] ?>/1"><?php echo $projects['title']; ?></a></h3>
                <?php if ($projects['category_name'] != '' || $projects['subcategory_name'] != '') { ?>
                    <p class="pro-b"><?php echo $projects['category_name']; ?><?php echo isset($projects['subcategory_name']) ? " > " . $projects['subcategory_name'] : ''; ?></p>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="desh-inblock col-w1 text-right pro-desh-box"  style="width: 20%;">
        <p class="neft text-center">
                <span class="<?php echo $projects['designerVariables']['colorclass']; ?> text-uppercase"><?php echo $projects['designerVariables']['status'] ?></span>
        </p>
    </div>
    <div class="pro-desh-box" style="width: 20%;" data-proid ='<?php echo $projects['id']; ?>'    >
        <div class=""><!-- pro-circle-list clearfix -->
            <div class="pro-circ-le">
               
                    <p class="pro-circle-txt text-center">
                        <?php echo $projects['customer_first_name'] . " " . $projects['customer_last_name']; ?>
                    </p>
                <div class="sub_user_req_info designer-sub-user">
                    <p class="text-h text-wrap subUsr">
                    <?php
                    if ($projects['sub_first_name'] != '') { ?>
                    <i class="fas fa-user-friends"></i>
                    <?php }
                    echo $projects['sub_first_name']; 
                        if (strlen($projects['sub_last_name']) > 5) {
                            echo ucwords(substr($projects['sub_last_name'], 0, 1));
                        } else {
                            echo $projects['sub_last_name'];
                        }
                        ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="pro-desh-box pro-b"  style="width: 20%;">
        <div class="cli-ent-xbox">
            <p class="pro-a">
                <?php echo $projects['designerVariables']['datelabel']; ?>
            </p>
            <?php if ($projects['status_designer'] == 'active' || $projects['status_designer'] == 'disapprove') { ?>
                <p id="demo_<?php echo $projects['id']; ?>" class="expectd_date" data-id="<?php echo $projects['id']; ?>" data-date="<?php echo date('M d, Y h:i:A', strtotime($projects['expected'])); ?>" data-timezonedate="<?php echo $projects['usertimezone_date']; ?>"></p>
            <?php } echo $projects['designerVariables']['date']; ?>          
        </div>
    </div>
    <?php
    if ($projects['verified'] == '1') {
        $class = 'verified_by_admin';
        $disable = '';
    } elseif ($projects['verified'] == '2') {
        $class = 'verified_by_designer';
        $disable = 'disabled';
    } else {
        $class = '';
        $disable = '';
    }
    ?>
    <!----verified tab start-->
    <?php
    if ($projects['verified'] == '1') {
        $class = 'verified_by_admin';
        $icon = '<i class="far fa-check-circle"></i> <span class="vrfy_txt">Verified</span>';
        $disable = '';
    } elseif ($projects['verified'] == '2') {
        $class = 'verified_by_designer';
        $icon = '<i class="fas fa-check-circle"></i> <span class="vrfy_txt">Verified</span>';
        $disable = 'disabled';
    } else {
        $class = '';
        $icon = '<i class="far fa-circle"></i> <span class="vrfy_txt">Verify</span>';
        $disable = '';
    }
    ?>
    <div class="pro-desh-box" style="width: 10%;">
        <div class="cell-row">
            <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $projects['id']; ?>">
                <label>
                    <input type="checkbox" name="project" class="verified" data-pid="<?php echo $projects['id']; ?>" <?php echo ($projects['verified'] == 1 || $projects['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable; ?>/>
                    <?php echo $icon; ?>
                </label>
            </div>
        </div>
    </div>
    <!----verified tab end-->
</div>


