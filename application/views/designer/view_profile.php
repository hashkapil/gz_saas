<a href="#" class="float-xs-left content-title-main darkblacktext" style="padding-left: 5%;">Designer Profile</a>
</div>
<div class="col-md-12" style="background:white;">
<div class="col-md-10 offset-md-1" style="margin-top:50px;">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #e52344; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147; } 
.darkblackbackground { background-color:#1a3147; } 
.pinktext { color: #e52344; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
	font-size: 31px !important;
    padding: 8px 0px !important;
    font-weight: 600 !important;
    letter-spacing: -3px !important;
}
.projecttitle{
	font-size: 25px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: 1px solid #000;
    background: unset;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
table {     border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}
</style>
    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">                
                <div class="nav-tab-pills-image">
                    
                    <div class="tab-content">
                        <div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                            <div class="row" style="border-bottom: 1px solid #000;">
                                <div class="col-md-12" style="margin-bottom:30px;">
                                     <div class="col-md-5">
										<div class="col-md-3">
											<img src="<?php echo base_url(); ?>public/img/designer_1.png" width="100%" style="border-radius:50%;"/>
										</div>
										<div class="col-sm-9">
											<h3 class="darkblacktext">Rey Jugalbot</h3>
											<p class="greytext" style="font-size: 14px;margin: 0px;padding: 0px;line-height: 19px;">Designer since August 08, 2017</p>
											
											<!--<h3 class="darkblacktext" style="margin-top:15px;">About Me</h3>
											<p class="greytext" style="font-size: 14px;margin: 0px;padding: 0px;line-height: 19px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>-->
										</div>
									 </div>
									 <div class="col-md-4">
										<h3 class="greytext weight600" style="letter-spacing: -1px;">Specializations:</h3>
										<h3 class="darkblacktext weight600" style="font-size: 23px;margin: 0px;padding: 0px;line-height: 32px;padding-right: 130px;">Identity / Branding Web Design Illustration</h3>
										
										<h3 class="greytext weight600" style="letter-spacing: -1px;margin-top:15px;">Requests Completed</h3>
										<h3 class="darkblacktext weight600" style="font-size: 23px;margin: 0px;padding: 0px;line-height: 32px;padding-right: 130px;">120</h3>
										
										<h3 class="greytext weight600" style="letter-spacing: -1px;margin-top:15px;">Current Active Clients</h3>
										<h3 class="darkblacktext weight600" style="font-size: 23px;margin: 0px;padding: 0px;line-height: 32px;padding-right: 130px;">7</h3>
									 </div>
									 <div class="col-md-3">
										<h2 class="greytext weight600">Designer Rating</h2>
										<h1 class="orangetext">4.1</h1>
										<i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star"></i>
									 </div>
                                </div>
                            </div>
							<input type="button" value="EDIT PROFILE" class="weight600 btn darkblackbackground" style="padding:13px 45px;font-size:16px;letter-spacing:0px;border-radius:5px;margin-top:30px;float:right;">
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
</div>