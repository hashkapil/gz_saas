<section class="con-b">
    <?php foreach ($edit_profile as $data): ?>
        <div class="container-fluid">
            <?php if ($this->session->flashdata('message_error') != '') { ?>				
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('message_success') != '') { ?>				
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                </div>
            <?php } ?>
            <div class="flex-this">
                <h2 class="main_page_heading"><?php echo $data['first_name'] . " " . $data['last_name']; ?></h2>
                <div class="header_searchbtn">
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <div class="client-profile">
                        <div class="profile-pic">
                            <figure class="setimg-box33 big-cirlce">
                                <img src="<?php echo $data['profile_picture']; ?>" class="img-responsive telset33">
                            </figure>
                            <a class="setimg-blog33" href="javascript:void(0)">
                                <span class="setimgblogcaps profile_pic_sec_btn">
                                    <i class="icon-gz_edit_icon"></i>
                                </span>
                            </a>
                        </div>
                        <div class="edit_profile_pic_sec"  style="display: none;">
<!--                            <figure class="setimg-box33 big-cirlce">
                                <img src="<?php echo $data['profile_picture']; ?>" class="img-responsive telset33">
                            </figure>-->
                            <form action="<?php echo base_url(); ?>designer/profile/designer_edit_profile_pic" method="post" enctype="multipart/form-data">
                                <div class="settrow">
                                    <div class="settcol right">
                                        <p class="btn-x"><button type="submit" class="btn-e" name="submit_pic">Save</button></p> 
                                    </div>
                                </div>
                                <div class="setimg-row33">
                                    <div class="imagemain2" style="padding-bottom: 20px; display: none;">
                                        <input type="file" onChange="validateAndUpload(this);" class="form-control dropify waves-effect waves-button" name="profile" style="display:none;" id="inFile"/>
                                    </div>
                                    <div class="imagemain">
                                        <figure class="setimg-box33 big-cirlce">
                                            <img src="<?php echo $data['profile_picture']; ?>" class="img-responsive telset33">
                                            <a class="setimg-blog33" href="#" onclick="$('.dropify').click();">
                                                <span class="setimgblogcaps">
                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-camera.png" class="img-responsive">
                                                   
                                                </span>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="imagemain3" style="display: none;">
                                        <figure class="setimg-box33 big-cirlce">
                                            <img src="" id="image3" class="img-responsive telset33">
                                            <a class="setimg-blog33" href="#" onclick="$('.dropify').click();">
                                                <span class="setimgblogcaps">
                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-camera.png" class="img-responsive">
                                                   
                                                </span>
                                            </a>
                                        </figure>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="main-discp">
                            <h3>Designer</h3>
                            <h2>
                                <?php echo $data['first_name'] . " " . $data['last_name']; ?>
                            </h2>
                            <p>
                                Member Since <?php echo date("F Y", strtotime($data['created'])) ?>
                            </p>
                            <p class="chngpasstext">
                                <a href="javascript:voil(0)" data-toggle="modal" data-target="#myModal" class="btn-e text-wrap"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/change-password.png" class="img-responsive"> <span>Change Password</span></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row plan_row">
                <div class="col-md-8 col-sm-12">
                    <ul class="list-header-blog client-inform" id="tab_menu">
                        <li class="active" id="1">
                            <a data-toggle="tab" href="#myprofile" role="tab">My Profile</a>
                        </li>    
                        <li id="3">
                            <a data-toggle="tab" href="#enableEmail" role="tab">Skills </a>
                        </li> 
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <div class="tab-pane active content-datatable datatable-width" id="myprofile" role="tabpanel"> 
                    <div class="tabs-card">
                        <div class="row">

                            <div class="edit_other_profile_sec" >
                                <div class="col-md-12">
                                    <div class="project-row-qq1 pr-ofile-desinger">
                                        <form method="post" action="<?php echo base_url(); ?>designer/profile/designer_edit_profile">
                                            <div class="settrow">
                                                <h2 class="main-info-heading">Profile Information</h2>
                                                <p class="fill-sub">Please fill your Profile Information</p>
                                                <div class="settcol right">
                                                    <p class="btn-x">
                                                        <button name="submit" type="submit" class="btn-e text-wrap">Save</button>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php ?>"> First Name </p>
                                                        <input class="input" type="text" name="first_name" value="<?php echo $data['first_name']; ?>" required>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php ?>"> Last Name </p>
                                                        <input class=" input" type="text" name="last_name" value="<?php echo $data['last_name']; ?>" required>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php ?>">Paypal id</p>
                                                        <input class="input" type="text" name="paypal_id" value="<?php echo $data['paypal_email_address']; ?>">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php ?>">Email</p>
                                                        <input class="input" type="text" name="email" value="<?php echo $data['email']; ?>" required>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php ?>">Phone</p>
                                                        <input class="input" type="text" name="phone_number" value="<?php echo $data['phone']; ?>">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php ?>">Address</p>
                                                        <input class="input" type="text" name="address" value="<?php echo $data['address_line_1']; ?>">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php ?>">City</p>
                                                        <input class="input" placeholder="" type="text" name="city" value="<?php echo $data['city']; ?>">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php ?>">State</p>
                                                        <input class="input" type="text" name="state" value="<?php echo $data['state']; ?>">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>  
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php ?>">Zip</p>
                                                        <input class="input" type="text" name="zip_code" value="<?php echo $data['zip']; ?>">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label class="form-group">
                                                        <p class="label-txt label-active">Timezone</p>
                                                        <select class="input" name="timezone" placeholder="" required>
                                                            <?php for ($i = 0; $i < sizeof($timezone); $i++) { ?>
                                                                <option value="<?php echo $timezone[$i]['zone_name']; ?>" <?php
                                                                if ($data['timezone'] == $timezone[$i]['zone_name']) {
                                                                    echo 'selected';
                                                                }
                                                                ?> ><?php echo $timezone[$i]['zone_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php ?>">Hobbies</p>
                                                        <textarea class="input"  name="hobbies"><?php echo (isset($skills[0]['hobbies']) && $skills[0]['hobbies'] != '') ? $skills[0]['hobbies'] : ''; ?></textarea>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label class="form-group">
                                                        <p class="label-txt <?php ?>">About</p>
                                                        <textarea class="input" name="about"><?php echo (isset($skills[0]['about']) && $skills[0]['about'] != '') ? $skills[0]['about'] : ''; ?></textarea>
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>


                                        <!--   <form action="<?php echo base_url(); ?>designer/profile/about_info" method="post"> -->
                                            <!-- <p class="btn-x"><button type="submit" class="btn-e" name="submit_abt">Save</button></p>  -->
                                            <!-- </form>   -->

                                        </form>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



                <div class="tab-pane content-datatable datatable-width" id="enableEmail" role="tabpanel">
                    <div class="tabs-card">
                        <div class="edit_skills_sec">
                            <div class="project-row-qq1 profielveiwee-box">
                                <form method="post" action="<?php echo base_url(); ?>designer/profile/addskills">  
                                    <div class="settrow">
                                        <h2 class="main-info-heading">Skills</h2>
                                        <p class="fill-sub">Please add Designer's skills</p>
                                        <div class="settcol right">
                                            <p class="btn-x">
                                                <button id="skills_submit" class="btn-e text-wrap">Save</button>
                                            </p>
                                        </div>
                                    </div> 
                                    <?php if (!empty($skills)) { ?>
                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Brandig & Logos</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value0" id="value1"><?php
                                                        if ($skills[0]['brandin_logo']) {
                                                            echo $skills[0]['brandin_logo'];
                                                        } else {
                                                            echo '0';
                                                        }
                                                        ?></span>%
                                                    <input type="hidden" class="value_skill" name="brandin_logo" value="<?php
                                                    if ($skills[0]['brandin_logo']) {
                                                        echo $skills[0]['brandin_logo'];
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?>" id="sckill_val1">
                                                </div>
                                            </div>

                                            <div data-target="#value1" class="ui-slider-control" id="ui-slider-control0"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Ads & Banners</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value1" id="value2"><?php
                                                        if ($skills[0]['ads_banner']) {
                                                            echo $skills[0]['ads_banner'];
                                                        } else {
                                                            echo '0';
                                                        }
                                                        ?></span>%
                                                    <input type="hidden" class="value_skill" name="ads_banner" value="<?php
                                                    if ($skills[0]['ads_banner']) {
                                                        echo $skills[0]['ads_banner'];
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?>" id="sckill_val2">
                                                </div>
                                            </div>

                                            <div data-target="#value2" class="ui-slider-control" id="ui-slider-control1"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Email & Marketing</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value2" id="value3"><?php
                                                        if ($skills[0]['email_marketing']) {
                                                            echo $skills[0]['email_marketing'];
                                                        } else {
                                                            echo '0';
                                                        }
                                                        ?></span>%
                                                    <input type="hidden" class="value_skill" name="email_marketing" value="<?php
                                                    if ($skills[0]['email_marketing']) {
                                                        echo $skills[0]['email_marketing'];
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?>" id="sckill_val3">
                                                </div>
                                            </div>

                                            <div data-target="#value3" class="ui-slider-control" id="ui-slider-control2"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Web Design</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value3" id="value4"><?php
                                                        if ($skills[0]['webdesign']) {
                                                            echo $skills[0]['webdesign'];
                                                        } else {
                                                            echo '0';
                                                        }
                                                        ?></span>%
                                                    <input type="hidden" class="value_skill" name="webdesign" value="<?php
                                                    if ($skills[0]['webdesign']) {
                                                        echo $skills[0]['webdesign'];
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?>" id="sckill_val4">
                                                </div>
                                            </div>

                                            <div data-target="#value4" class="ui-slider-control" id="ui-slider-control3"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">App Design</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value4" id="value5"><?php
                                                        if ($skills[0]['appdesign']) {
                                                            echo $skills[0]['appdesign'];
                                                        } else {
                                                            echo '0';
                                                        }
                                                        ?></span>%
                                                    <input type="hidden" class="value_skill" name="appdesign" value="<?php
                                                    if ($skills[0]['appdesign']) {
                                                        echo $skills[0]['appdesign'];
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?>" id="sckill_val5">
                                                </div>
                                            </div>

                                            <div data-target="#value5" class="ui-slider-control" id="ui-slider-control4"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Graphics Design</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value5" id="value6"><?php
                                                        if ($skills[0]['graphics']) {
                                                            echo $skills[0]['graphics'];
                                                        } else {
                                                            echo '0';
                                                        }
                                                        ?>                                                                
                                                    </span>%
                                                    <input type="hidden" class="value_skill" name="graphics" value="<?php
                                                    if ($skills[0]['graphics']) {
                                                        echo $skills[0]['graphics'];
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?>" id="sckill_val6">
                                                </div>
                                            </div>

                                            <div data-target="#value6" class="ui-slider-control" id="ui-slider-control5"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Illustrations</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value6" id="value7">
                                                        <?php
                                                        if ($skills[0]['illustration']) {
                                                            echo $skills[0]['illustration'];
                                                        } else {
                                                            echo '0';
                                                        }
                                                        ?>                                                                
                                                    </span>%
                                                    <input type="hidden" class="value_skill" name="illustration" value="<?php
                                                    if ($skills[0]['illustration']) {
                                                        echo $skills[0]['illustration'];
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?>" id="sckill_val7">
                                                </div>
                                            </div>

                                            <div data-target="#value7" class="ui-slider-control" id="ui-slider-control6"></div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Brandig & Logos</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value0" id="value1">0</span>%
                                                    <input type="hidden" class="value_skill" name="brandin_logo" value="0" id="sckill_val1">
                                                </div>
                                            </div>

                                            <div data-target="#value1" class="ui-slider-control" id="ui-slider-control0"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Ads & Banners</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value1" id="value2">0</span>%
                                                    <input type="hidden" class="value_skill" name="ads_banner" value="0" id="sckill_val2">
                                                </div>
                                            </div>

                                            <div data-target="#value2" class="ui-slider-control" id="ui-slider-control1"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Email & Marketing</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value2" id="value3">0</span>%
                                                    <input type="hidden" class="value_skill" name="email_marketing" value="0" id="sckill_val3">
                                                </div>
                                            </div>

                                            <div data-target="#value3" class="ui-slider-control" id="ui-slider-control2"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Web Design</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value3" id="value4">0</span>%
                                                    <input type="hidden" class="value_skill" name="webdesign" value="0" id="sckill_val4">
                                                </div>
                                            </div>

                                            <div data-target="#value4" class="ui-slider-control" id="ui-slider-control3"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">App Design</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value4" id="value5">0</span>%
                                                    <input type="hidden" class="value_skill" name="appdesign" value="0" id="sckill_val5">
                                                </div>
                                            </div>

                                            <div data-target="#value5" class="ui-slider-control" id="ui-slider-control4"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Graphics Design</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value5" id="value6">0</span>%
                                                    <input type="hidden" class="value_skill" name="graphics" value="0" id="sckill_val6">
                                                </div>
                                            </div>

                                            <div data-target="#value6" class="ui-slider-control" id="ui-slider-control5"></div>
                                        </div>

                                        <div class="ui-slid-errow">
                                            <div class="pro-gressbar">
                                                <h3 class="progress-title">Illustrations</h3>
                                                <div class="progress-value-1">
                                                    <span class="value value6" id="value7">0</span>%
                                                    <input type="hidden" class="value_skill" name="illustration" value="0" id="sckill_val7">
                                                </div>
                                            </div>

                                            <div data-target="#value7" class="ui-slider-control" id="ui-slider-control6"></div>
                                        </div>
                                    <?php } ?>
                                </form>
                            </div>  
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php endforeach; ?>
</section>
<!-- Modal -->
<div class="modal fade similar-prop" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Change Password</h2>
                    <div class="cross_popup" data-dismiss="modal"> x</div>
                </header>
                <div class="cli-ent-model">
                    <div class="fo-rm-body">
                        <form method="post" action="<?php echo base_url(); ?>designer/profile/designer_change_password_profile">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="form-group">
                                        <p class="label-txt <?php ?>">Current Password</p>
                                        <input type="password" required class="input" name="old_password">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-sm-12">
                                    <label class="form-group">
                                        <p class="label-txt <?php ?>">New Password</p>
                                        <input type="password" required class="input"  name="new_password">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-sm-12">
                                    <label class="form-group">
                                        <p class="label-txt <?php ?>">Confirm Password</p>
                                        <input type="password" required class="input" name="confirm_password">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <button class="btn-g load_more button" name="savebtn">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<script type="text/javascript" src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/designer/jquery.min.js');?>"></script>
<script type="text/javascript">
        $(document).ready(function () {
        $('.profile_pic_sec_btn').click(function (e) {
        e.preventDefault();
        $('.profile-pic').hide();
        $('.edit_profile_pic_sec').css('display', 'block');
        });
        $('.abt_sec_btn').click(function (e) {
        e.preventDefault();
$('.about_info_sec').hide();
    $('.edit_abt_sec').css('display', 'block');
    });
    $('.skills_btn').click(function (e) {
    e.preventDefault();
    $('.skills_sec').hide();
    $('.edit_skills_sec').css('display', 'block');
    });
    $('.other_profile_btn').click(function (e) {
    e.preventDefault();
    $('.other_profile_sec').hide();
    $('.edit_other_profile_sec').css('display', 'block');
    });
    $('#changepass').click(function (e) {
    e.preventDefault();
    });
    });
    function validateAndUpload(input) {
    //alert("In Function"); 
    var URL = window.URL || window.webkitURL;
    var file = input.files[0];

    if (file) {
    console.log(URL.createObjectURL(file));
    // console.log(file);
    $(".imagemain").hide();

    $(".imagemain2").show();
    $(".imagemain3").show();
    $("#image3").attr('src', URL.createObjectURL(file));

    $(".dropify-wrapper").show();
    }
    }
</script>