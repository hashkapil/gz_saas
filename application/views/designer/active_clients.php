<!-- <h2 class="float-xs-left content-title-main" style="display:inline-block;padding-left:40px;">Clients</h2> -->

<div class="setting_tabs_link">
    <a href="<?php echo base_url(); ?>designer/profile/edit_profile" class="float-xs-left content-title-main" style="display:inline-block; padding-left:33px; color: #1a3147;">
        My Profile
    </a>
    <a href="<?php echo base_url(); ?>designer/request/active_clients" class="float-xs-left content-title-main active" style="color: #9da0a3; padding-left: 5%;">
        Customers
    </a>
</div><!-- setting_tabs_link -->

</div>
<div class="col-md-12" style="background:white;">
    <div class="col-md-10 offset-md-1">
        <section id="content-wrapper">
            <style>
                /* padding css start */
                .pb0{ padding-bottom:0px; }
                .pb5{ padding-bottom:5px; }
                .pb10{ padding-bottom:10px; }
                .pt0{ padding-top:0px; }
                .pt5{ padding-top:5px; }
                .pt10{ padding-top:10px; }
                .pl0{ padding-left:0px; }
                .pl5{ padding-left:5px; }
                .pl10{ padding-left:10px; }
                /* padding css end */
                .greenbackground { background-color:#98d575; }
                .greentext { color:#98d575; }
                .orangebackground { background-color:#f7941f; }
                .pinkbackground { background-color: #ec4159; }
                .orangetext { color:#f7941f; }
                .bluebackground { background-color:#409ae8; }
                .bluetext{ color:#409ae8; }
                .whitetext { color:#fff !important; }
                .blacktext { color:#000; }
                .greytext { color:#cccccc; }
                .greybackground { background-color:#ededed; }
                .darkblacktext { color:#1a3147; } 
                .darkblackbackground { background-color:#1a3147; }
                .pinktext { color: #ec4159; }
                .weight600 { font-weight:600; }
                .font18 { font-size:18px; }
                .textleft { text-align:left; }
                .textright { text-align:right; }
                .textcenter { text-align:center; }
                .pl20 { padding-left:20px; }

                .numbercss{
                    font-size: 18px !important;
                    padding: 8px 0px !important;
                    font-weight: 600 !important;
                    line-height: 31px;
                    text-align: left !important;
                    padding-left: 10px !important;
                }
                .projecttitle{
                    font-size: 20px;
                    padding-bottom: 0px;
                    text-align: left;
                    padding-left: 20px;
                }
                .trborder{
                    border: 1px solid #000;
                    background: unset;

                }
                .trborder:hover{
                    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                    background-color:unset;
                }
                .table-hover tbody tr:hover{
                    background-color:unset;
                }
                table {     border-spacing: 0 1em; }
                .trash{     
                    color: #ec4159;
                    font-size: 25px; 
                }
                .nav-tab-pills-image ul li .nav-link {
                    color:#1a3147;
                    font-weight:600;
                    padding: 7px 25px;

                }
                .nav-tabs {
                    border-bottom: unset;
                }

                .nav-tab-pills-image ul li .nav-link:hover
                {
                    border-bottom:unset !important;
                    color:#2f4458 !important;
                }
                .nav-tab-pills-image ul li .nav-link active a:hover{
                    color:#fff !important;
                }
                .nav-tab-pills-image ul li .nav-link active a{
                    color:#fff !important;
                }

                .hovmein a:hover {
                    color: #f7941f !important;

                }	
                .wrapper .nav-tab-pills-image ul .active .nav-link:hover {
                    color: #fff !important;
                }
                .wrapper .nav-tab-pills-image ul li .nav-link:focus
                {
                    color:#fff;
                    border:none;
                }
                .content .nav-tab-pills-image .nav-item.active a:hover{
                    color:#fff;

                }
                .content .nav-tab-pills-image .nav-item.active dd {
                    background:unset !important;
                }
                .nav-tab-pills-image ul li .nav-link:hover {
                    border-bottom: none !important;
                }
                .content .nav-tab-pills-image .nav-item.active a {

                    border:unset !important;
                }
                .content .nav-tab-pills-image .nav-item.active a {
                    /* border-bottom: 3px solid #ec1c41 !important; */
                    color: #fff;
                    background: #2f4458;
                }
            </style>

            <div class="content">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
<!-- table_draft_sec new one -->
<div class="table_draft_sec table_draft_complete customers_table_sec">
<table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style=" border-collapse:separate; border-spacing: 0 1em;border:none; padding-top: 15px;">
    <tbody class="">
     <?php for ($i = 0; $i < sizeof($data); $i++) { ?>
        <tr onclick="window.location.href = '<?php echo base_url(); ?>designer/request/view_clients_request/<?php echo $data[$i]['id'] ?>'" class="trborder" id="<?php echo $data[$i]['id'] ?>" data-indexid="0">
            <?php if ($data[$i]['profile_picture']) { ?>
            <td class="completed image_dv">
                <div class="img"><img class="image_circle" src="<?php echo base_url() . 'uploads/profile_picture/' . $data[$i]['profile_picture'] ;?>"></div>
            </td>
            <?php }  else { ?>
            <td class="completed image_dv">
                <div class="img placeholder_img">
                  <span><?php  echo ucwords(substr($data[$i]['first_name'],0,1)) .  ucwords(substr($data[$i]['last_name'],0,1)); ?></span>
                </div>
            </td>
            <?php } ?>
            <?php  if ($data[$i]['plan_turn_around_days'] == 3) { ?>
            <td class="desc">
                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $data[$i]['first_name']; ?> <?php echo $data[$i]['last_name']; ?></p>
                <p><span class="queue">Premium Member</span></p>
            </td>
            <?php } else { ?>
            <td class="desc">
                <p class="darkblacktext weight600 projecttitle pb0 pt5"><?php echo $data[$i]['first_name']; ?> <?php echo $data[$i]['last_name']; ?></p>
                <p><span class="queue">Standard Member</span></p>
            </td>
            <?php  } ?>
            
            
            <td class="requests">
                <div class="text">No. of Requests</div>
                <div class="num"><?php echo $data[$i]['total_requests']; ?></div>
            </td>

            <td class="requests active_req">
                <div class="text">Active Requests</div>
                <div class="num"><?php echo $data[$i]['total_active']; ?></div>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
</div><!-- table_draft_sec -->



                        <?php /* <div class="nav-tab-pills-image">
                            <?php /* ?>
                            <ul class="nav nav-tabs" role="tablist">                      
                                <li class="nav-item active" style="background: #ededed;border-radius: 7px;margin-left: 1px;height:40px;">
                                    <a class="nav-link" data-toggle="tab" href="#active_clients" role="tab">
                                        Active Clients (<?php echo sizeof($data); ?>)
                                    </a>
                                </li>
                                <!--<li class="nav-item" style="background: #ededed;border-radius: 7px;margin-left: 1px;height:40px;">
                                    <a class="nav-link" data-toggle="tab" href="#previous_clients" role="tab">
                                        Previous Clients (0)
                                    </a>
                                </li>-->
                            </ul>
                            <?php */ ?>

                            
                          <?php /*    <div class="tab-content">
                                <div class="tab-pane active content-datatable datatable-width" id="active_clients" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive" style="border:none;">
                                               <!--<thead>
                                                   <tr>
                                                       <th>Id</th>
                                                       <th>Project Name</th>
                                                                                                       <th>Customer Name</th>
                                                                                                       <th>In-progress</th>
                                                                                                       <th>Messages</th>
                                                                                                       <th>Icon</th>
                                                   </tr>
                                               </thead>-->
                                                <tbody>
                                                    <?php for ($i = 0; $i < sizeof($data); $i++) { ?>
                                                        <tr class="trborder">

                                                            <?php
                                                            if ($data[$i]['plan_turn_around_days'] == 3) {
                                                                ?>
                                                                <td class="darkblackbackground whitetext numbercss" style="width: 100px;">
                                                                    <p style="font-size:12px;">Premium Member </p><p class="whitetext" style="font-size: 14px;">1-Day Turnaround</p>
                                                                </td>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <td class="pinkbackground whitetext numbercss" style="width: 100px;">
                                                                    <p style="font-size:12px;">Standard Member</p> <p class="whitetext" style="font-size: 14px;">3-Day Turnaround</p>
                                                                </td>
                                                                <?php
                                                            }
                                                            ?>

                                                            <td>
                                                                <div class="col-sm-3">
                                                                   <center>
                                                                     
                                                                    <?php
                                                                    if ($data[$i]['profile_picture']) 
                                                                    {
                                                                    ?>
                                                                     <div class="myprofilebackground" ><p style="padding-top: 13px;">
                                                                     <?php
                                                                        echo '<img src="' . base_url() . 'uploads/profile_picture/' . $data[$i]['profile_picture'] . '" style="width:60px; height:50px; border-radius:50%;border: 3px solid #0190ff;">';
                                                                    
                                                                    ?>
                                                                     </p></div>         
                                                                    <?php  
                                                                    }
                                                                    else 
                                                                    {
                                                                    ?>
                                                                    <div class="myprofilebackground" style="width:60px; height:50px;border-radius:50%; background: #0190ff;border: 3px solid #0190ff; text-align: center; font-size: 15px; color: #fff;"><p style="font-size: 22px;padding-top: 13px;">
                                                                            
                                                                    <?php 
                                                                        
                                                                        echo ucwords(substr($data[$i]['first_name'],0,1)) .  ucwords(substr($data[$i]['last_name'],0,1));
                                                                    ?>
                                                                        </p></div>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                
                                                                    </center>
                                                                </div>
                                                                <div class="col-sm-6" style="padding-top:19px;text-align:left;">
                                                                    <a href="<?php echo base_url(); ?>designer/request/view_clients_request/<?php echo $data[$i]['id'] ?>" class="darkblacktext" style="font-size: 14px;text-align: left;font-weight: 600;padding-top: 10px;"><span class="darkblacktext weight600 projecttitle pb0 pt5" style="padding:0px;"><?php echo $data[$i]['first_name']; ?></span><span class="darkblacktext weight600 projecttitle pb0 pt5" style="padding-left:8px;"><?php echo $data[$i]['last_name']; ?></span></a>
                                                                    <p class="greytext weight600" style=""><?php if ($data[$i]['modified'] == "") {
                                                            echo date("m/d/Y", strtotime($data[$i]['created']));
                                                        } else {
                                                            echo date("m/d/Y", strtotime($data[$i]['modified']));
                                                        } ?></p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font16" style="padding-top:15px;">No. of Requests</p>
                                                                <p class="darkblacktext weight600"><?php echo $data[$i]['total_requests']; ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font16" style="padding-top:15px;">Active Requests</p>
                                                                <p class="darkblacktext weight600"><?php echo $data[$i]['total_active']; ?></p>
                                                            </td>
                                                            <td>
                                                                <p class="greytext pb0 weight600 font16" style="padding-top:15px;">Rating</p>
                                                                <?php
                                                                 $rate =  round($data[$i]['total_grade']);
                                                                 for($j=1;$j<=5;$j++)
                                                                 {
                                                                     if($j <= $rate)
                                                                     {
                                                                ?>
                                                                    
                                                                    <i class="fa fa-star pinktext"></i>
                                                                <?php
                                                                     }
                                                                     else 
                                                                     {
                                                                ?>
                                                                    <i class="fa fa-star"></i>
                                                                <?php
                                                                     }
                                                                 }
                                                                ?>
<!--                                                                <p class="greytext pb0 weight600 font14" style="padding-top:15px;">Rating</p>
                                                                <i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star"></i>-->
                                                                
                                                    <?php /*        </td>
<!--                                                            <td>
                                                                <svg style="vertical-align: sub;width: 0.5em;" aria-hidden="true" data-prefix="fas" data-icon="comment-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-comment-alt fa-w-16 fa-2x" ><path fill="orange" d="M448 0H64C28.7 0 0 28.7 0 64v288c0 35.3 28.7 64 64 64h96v84c0 9.8 11.2 15.5 19.1 9.7L304 416h144c35.3 0 64-28.7 64-64V64c0-35.3-28.7-64-64-64z" class=""></path></svg>
                                                                <p class="greytext pb0 weight600 font18 directmessage" style="display: inline-block;margin-left: 5px;padding-top: 25px;font-size: 15px;" data-clientname="<?php echo "<span>" . $data[$i]['first_name'] . "</span>";
                                                        echo "<span style='padding-left:8px;'>" . $data[$i]['last_name'] . "</span>" ?>" data-clientid="<?php echo $data[$i]['id'] ?>" data-imageurl="<?php if ($data[$i]['profile_picture'] == "") {
                                                        echo base_url(); ?>public/img/img/logo.png" <?php } else {
                                                    echo $data[$i]['profile_picture'];
                                                } ?> style="border-radius:25px;">Direct Message</p>
                                                            </td>-->
                                                        </tr> 
    <?php
}
?>
                                                    <!--<tr class="trborder"">
                                                            <td class=" pinkbackground whitetext numbercss">Standard Member </br><p class="whitetext" style="font-size: 14px;">3-Day Turnaround</p></td>
                                                            <td>
                                                                    <div class="col-sm-3">
                                                                            <img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;" />
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                            <h4 class="darkblacktext">Clayton Call</h4>
                                                                            <p class="greytext">Member since January 5, 2017</p>
                                                                    </div>
                                                            </td>
                                                            <td>
                                                                    <p class="greytext pb0 weight600 font18">No. of Requests</p>
                                                                    <p class="darkblacktext weight600">12</p>
                                                            </td>
                                                            <td>
                                                                    <p class="greytext pb0 weight600 font18">Active Requests</p>
                                                                    <p class="darkblacktext weight600">3</p>
                                                            </td>
                                                            <td>
                                                                    <p class="greytext pb0 weight600 font18">Rating</p>
                                                                    <i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star"></i>
                                                            </td>
                                                            <td>
                                                                    <i class="fa fa-envelope"></i><p class="greytext pb0 weight600 font18 directmessage" style="display: inline-block;margin-left: 5px;" data-clientname="Clayton Call" data-clientid="2" data-imageurl="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;">Direct Message</p>
                                                            </td>
                                                    </tr>

                                                    <tr class="trborder">
                                                            <td class=" pinkbackground whitetext numbercss">Standard Member </br><p class="whitetext" style="font-size: 14px;">3-Day Turnaround</p></td>
                                                            <td>
                                                                    <div class="col-sm-3">
                                                                            <img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;" />
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                            <h4 class="darkblacktext">James Franco</h4>
                                                                            <p class="greytext">Member since August 24, 2017</p>
                                                                    </div>
                                                            </td>
                                                            <td>
                                                                    <p class="greytext pb0 weight600 font18">No. of Requests</p>
                                                                    <p class="darkblacktext weight600">12</p>
                                                            </td>
                                                            <td>
                                                                    <p class="greytext pb0 weight600 font18">Active Requests</p>
                                                                    <p class="darkblacktext weight600">3</p>
                                                            </td>
                                                            <td>
                                                                    <p class="greytext pb0 weight600 font18">Rating</p>
                                                                    <i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star"></i>
                                                            </td>
                                                            <td>
                                                                    <i class="fa fa-envelope"></i><p class="greytext pb0 weight600 font18 directmessage" style="display: inline-block;margin-left: 5px;" data-clientname="James Franco" data-clientid="3" data-imageurl="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;">Direct Message</p>
                                                            </td>
                                                    </tr>-->


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane content-datatable datatable-width" id="previous_clients" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                                                <!--<thead>
                                                    <tr>
                                                        <th>Id</th>
                                                                                                        <th>Design Status</th>
                                                                                                        <th>Customer Name</th> 
                                                                                                        <th>Designer Name</th>
                                                                                                        <th>Title</th>
                                                        <th>Date Requested</th>
                                                                                                        <th>Messages()</th>
                                                                                                        <th>Action</th>
                                                                                   
                                                    </tr>
                                                </thead>-->
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        */?>
                    </div>
                </div>
            </div>
            <div id="directmessage" class="modal fade" role="dialog" style="margin-top:10%;">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-body " style="font-size: 170px;height: 270px;">

                            <h3 style="letter-spacing: -1px;font-size: 21px;display:inline-block;" class="darkblacktext weight600">Direct Message to </h3>
                            <h3 style="letter-spacing: -1px;font-size: 21px;display:inline-block;width:50%;" class="darkblacktext weight600 image_name">Direct Message to </h3>
                            <p id="textarea_text" style="padding-bottom: 0px;font-size: 15px;color: #000;"></p>
                            <form method="post" action="" >

                                <div class="col-sm-10">

                                    <textarea  id="text_text" class="form-control" style="height: 125px;margin-top: 10px;border: 1px solid !important;"></textarea>
                                </div>
                                <div class="col-sm-2">
                                    <i class="fa fa-send pinktext send_clients" style="font-size: 35px;"></i>
                                </div>

                            </form>

                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
