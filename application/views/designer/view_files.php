<h2 class="float-xs-left content-title-main"><?php echo $request[0]['title']; ?></h2>
</div>
<div class="col-md-12" style="background:white;">
    <div class="col-md-11 offset-md-1">
        <section id="content-wrapper">
            <style>
                /* padding css start */
                .pb0{ padding-bottom:0px; }
                .pb5{ padding-bottom:5px; }
                .pb10{ padding-bottom:10px; }
                .pt0{ padding-top:0px; }
                .pt5{ padding-top:5px; }
                .pt10{ padding-top:10px; }
                .pl0{ padding-left:0px; }
                .pl5{ padding-left:5px; }
                .pl10{ padding-left:10px; }
                /* padding css end */
                .greenbackground { background-color:#98d575; }
                .greentext { color:#98d575; }
                .orangebackground { background-color:#f7941f; }
                .pinkbackground { background-color: #ec4159; }
                .orangetext { color:#f7941f; }
                .bluebackground { background-color:#409ae8; }
                .bluetext{ color:#409ae8; }
                .whitetext { color:#fff !important; }
                .blacktext { color:#000; }
                .greytext { color:#cccccc; }
                .greybackground { background-color:#ededed; }
                .darkblacktext { color:#1a3147; } 
                .darkblackbackground { background-color:#1a3147; } 
                .lightdarkblacktext { color:#b4b9be; }
                .lightdarkblackbackground { background-color:#f4f4f4; }
                .pinktext { color: #ec4159; }
                .weight600 { font-weight:600; }
                .font18 { font-size:18px; }
                .textleft { text-align:left; }
                .textright { text-align:right; }
                .textcenter { text-align:center; }
                .pl20 { padding-left:20px; }
                .chetcolor { color: #464646; }

                .numbercss{
                    font-size: 31px !important;
                    padding: 8px 0px !important;
                    font-weight: 600 !important;
                    letter-spacing: -3px !important;
                }
                .projecttitle{
                    font-size: 25px;
                    padding-bottom: 0px;
                    text-align: left;
                    padding-left: 20px;
                }
                .trborder{
                    border: 1px solid #000;
                    background: unset;
                    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
                }
                table {     border-spacing: 0 1em; }
                .trash{     
                    color: #ec4159;
                    font-size: 25px; 
                }
                .nav-tab-pills-image ul li .nav-link{
                    background:#ededed;
                    font-weight: 600;
                    color: #2f4458;
                }
                .ls0{ letter-spacing: 0px; }


                .slick-slide.slick-current.slick-center .portfolio-items-wrapper:before {
                    content: "";
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    background: none;
                }

                .slick-next {
                    left: 53%;
                    z-index: 1000;
                    height: auto;
                    width: auto;
                    top:95px;
                }

                .slick-prev {
                    left: 40%;
                    z-index: 1000;
                    height: auto;
                    width: auto;
                    top:95px;
                }

                .slick-next::before {
                    font-family: FontAwesome;
                    content: "\f105";
                    font-size: 22px;
                    background: #FFFFFF;
                    opacity: 1;
                    border-radius: 30px;
                    color: #1a3147;
                    background-color: #e4e4e4;
                    width: 30px;
                    height: 30px;
                    line-height: 28px;
                    text-align: center;
                    display: inline-block;
                }

                .slick-prev::before {
                    font-family: FontAwesome;
                    content: "\f104";
                    font-size: 22px;
                    opacity: 1;
                    border-radius: 30px;
                    color: #1a3147;
                    background-color: #e4e4e4;
                    width: 30px;
                    height: 30px;
                    line-height: 28px;
                    text-align: center;
                    display: inline-block;
                }
                .wrapper .nav-tab-pills-image ul .active .nav-link:hover {
                    color: #fff !important;
                }
                .wrapper .nav-tab-pills-image ul li .nav-link:focus
                {
                    color:#fff;
                    border:none;
                }
                .content .nav-tab-pills-image .nav-item.active a:hover{
                    color:#fff;

                }
                .content .nav-tab-pills-image .nav-item.active dd {
                    background:unset !important;
                }
                .nav-tab-pills-image ul li .nav-link:hover {
                    border-bottom: none !important;
                }
                .content .nav-tab-pills-image .nav-item.active a {

                    border:unset !important;
                }
                .content .nav-tab-pills-image .nav-item.active a {
                    /* border-bottom: 3px solid #ec1c41 !important; */
                    color: #fff;
                    background: #2f4458;
                }
                .pagin-one {
                    color: #1a3147;
                    font-size: 16px;
                    font-weight: bold;
                    left: 45%;
                    position: absolute;
                    top: 85px;
                }
            </style>


            <div class="content">

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;">
                        <div class="nav-tab-pills-image">
                            <ul class="nav nav-tabs" role="tablist" style="border-bottom:unset !important;">                      
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo base_url() . "designer/request/view_request/" . $_GET['id']; ?>" role="tab">
                                        Back to Files
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">

                                <div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <span class="pagin-one">1 of <?php echo sizeof($designer_file); ?></span>
                                            <div class="portfolio-items center">
                                                <?php
                                                for ($i = 0; $i < sizeof($designer_file); $i++) {
                                                    if ($designer_file[$i]['id'] != $main_id) {
                                                        continue;
                                                    }
                                                    ?>
                                                    <div class="col-md-12">
                                                        <div class="row">

                                                            <div class="col-md-12" style="padding-bottom:15px; border-right:1px solid;">

    <!--  <p style="text-align:center;font-size:12px;" class="pinktext font18 approve_reject_new_div" >
                                                                <?php
                                                                if ($designer_file[$i]['status'] == "Approve") {
                                                                    echo 'Design is Approved.';
                                                                } elseif ($designer_file[$i]['status'] == "Reject") {
                                                                    echo 'Design is Rejected.';
                                                                }
                                                                ?>
    </p>-->


                    <div class="col-md-12" style="padding:0px;">
                        <div class="cusslidetitmain" style="padding:0px;">
                            <div class="col-md-6" style="padding:0px;">
                                <h5 class="darkblacktext weight600 ls0"><?php echo $designer_file[$i]['src_file']; ?>
                                    <?php
                                    $created_date_time = strtotime($designer_file[$i]['created']);

                                    $now_date_time = strtotime(date("Y-m-d H:i:s"));
                                    $span = "";
                                    if (($now_date_time - $created_date_time) < 5000) {
                                        $span = '<span class="redlabel">NEW</span>';
                                    }
                                    ?>
                                </h5>
<!--<h5 class="darkblacktext weight600 ls0"><?php echo $designer_file[$i]['src_file']; ?> <span class="redlabel">NEW</span></h5>-->
                                <p class="lightdarkblacktext" style="font-size:16px;"><?php echo number_format(filesize("./uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']) / 1048576, 2) . " Mb"; ?> |
                                    <?php
                                    $date = strtotime($designer_file[$i]['created']); //Converted to a PHP date (a second count)
                                    //Calculate difference
                                    $diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
                                    $minutes = floor($diff / 60);
                                    $hours = floor($diff / 3600);
                                    if ($hours == 0) {
                                        echo $minutes . " Minutes Ago";
                                    } else {
                                        $days = floor($diff / (60 * 60 * 24));
                                        if ($days == 0) {
                                            echo $hours . " Hours Ago";
                                        } else {
                                            echo $days . " Days Ago";
                                        }
                                    }
                                    ?>
                                </p>
                            </div>

                            <div class="col-md-4" style="float:right;padding:0px;">
                                <a href="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>" download style="text-align:right;">
                                    <p class="darkblacktext weight600" style="font-size:16px;margin-top:10px; padding-bottom: 0px !important;"><i class="fa fa-download" style="padding-right: 10px;"></i>Download</p></a>
                                <form method="post" class="myform234" action="<?php echo base_url(); ?>customer/request/design_rating" style="padding-top:5px;float:right;display:inline-block;">
                                    <?php
                                    for ($j = 1; $j <= 5; $j++) {
                                        if ($designer_file[$i]['customer_grade'] >= $j) {
                                            echo '<i class="fa fa-star pl5 pinktext fa-star' . $j . $i . '" data-index="' . $j . '" data-id=""></i>';
                                        } else {
                                            echo '<i class="fa fa-star pl5 greytext fa-star' . $j . $i . '" data-index="' . $j . '" data-id=""></i>';
                                        }
                                        ?>

                                    <?php } ?>
                                    <input type="hidden" name="customer_rating" id="customer_rating<?php echo $i; ?>" value="<?php echo $designer_file[$i]['customer_grade']; ?>" />
                                    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" id="myrequest_id"/>
                                    <input type="hidden" name="id" value="<?php echo $designer_file[$i]['id']; ?>" id="myfilerequest_id"/>
                                    <input type="hidden" name="request_id" value="<?php echo $designer_file[$i]['request_id']; ?>" />
                                </form>
                                <p class="weight600 pinktext" style="font-size:18px;padding:0px;float:right;display:inline-block;">Rating</p>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                <div class="row">

                    <div class="col-md-12 mydivposition mydivposition_<?php echo $designer_file[$i]['id']; ?>"  style="margin-top:40px;padding-left: 0px;background-color:#ccc;height: 625px;max-height: 625px;" onclick="printMousePos(event,<?php echo $designer_file[$i]['id']; ?>);" id="mydivposition_<?php echo $designer_file[$i]['id']; ?>" onmouseout="hideclicktocomment();" onmouseover="showclicktocomment(event,<?php echo $designer_file[$i]['id']; ?>);" onmousemove="showclicktocomment(event,<?php echo $designer_file[$i]['id']; ?>);" >
                        <div class="col-md-12 " style="padding:10px;">
                            <image class="img-responsive" src="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" style="width:100%;     max-height: 600px; margin: auto; display: block;"  />
                         <!--   <div onclick="$('.open_<?php echo $designer_file[$i]['id']; ?>').toggle();" style="height:20px;width:20px;border-radius:50%;    background: red;position: absolute;top: 50px;left: 50px;"></div>-->
                        </div>
                        <!-- Chat toolkit start-->
                        <div class="appenddot_<?php echo $designer_file[$i]['id']; ?>">
                            <?php
                            for ($j = 0; $j < sizeof($designer_file[$i]['chat']); $j++) {
                                if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                    $background = "pinkbackground";
                                } else {
                                    $background = "bluebackground";
                                }
                                ?>
                                <?php
                                if ($designer_file[$i]['chat'][$j]['xco'] != "") {
                                    if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                        $image1 = "dot_image1.png";
                                        $image2 = "dot_image2.png";
                                    } else {
                                        $image1 = "designer_image1.png";
                                        $image2 = "designer_image2.png";
                                    }
                                    ?>
                                    <div class="<?php //echo $background;    ?>" style="position:absolute;left:<?php echo $designer_file[$i]['chat'][$j]['xco']; ?>px;top:<?php echo $designer_file[$i]['chat'][$j]['yco']; ?>px;width:25px;z-index:99999999999999;height:25px;" onclick='show_customer_chat(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, event);'>
                                        <img src="<?php echo base_url() . "public/" . $image1; ?>" class="customer_chatimage1 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"/>
                                        <img style="display:none;" src="<?php echo base_url() . "public/" . $image2; ?>" class="customer_chatimage2 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"/>
                                    </div>
                                    <div class="customer_chat customer_chat<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none;background:#fff;position:absolute;left:<?php echo $designer_file[$i]['chat'][$j]['xco'] - 3; ?>px;top:<?php echo $designer_file[$i]['chat'][$j]['yco'] + 25; ?>px;padding: 5px;border-radius: 5px;">
                                        <label><?php echo $designer_file[$i]['chat'][$j]['message']; ?></label>
                                    </div>
                                <?php }
                                ?>
                            <?php } ?>
                        </div>
                        <div class="openchat" style="width:35%;position:absolute;display:none;padding:10px 20px;background:#fff;">
                            <textarea class='form-control text_write text_<?php echo $designer_file[$i]['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px;margin-bottom:5px;" placeholder="Leave a comment" onkeydown="javascript: if (event.keyCode == 13) {
                                            $('.send_text<?php echo $designer_file[$i]['id']; ?>').click();
                                        }"></textarea>
                            <button class="pinkbackground send_request_img_chat send_text send_text<?php echo $designer_file[$i]['id']; ?>" style="border: none;border-radius: 5px;padding: 1px 10px;" data-fileid="<?php echo $designer_file[$i]['id']; ?>"
                                    data-senderrole="designer" 
                                    data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                                    data-receiverid="<?php echo $request[0]['customer_id']; ?>" 
                                    data-receiverrole="customer"
                                    data-customername="<?php echo $user[0]['first_name'] . " " . $user[0]['last_name']; ?>"
                                    data-xco="" data-yco=""><i class="fa fa-send whitetext"></i></button>
                            <span class="mycancellable  greytext" style="font-size:10px;cursor:pointer;">Cancel</span>
                        </div>
                        <div class="showclicktocomment" style="z-index: 9999999999999999999999999999999999999;color: #fff;background: #c7c1c1;padding: 5px;font-weight: 600;position:absolute;display:none;">
                            Click To Comment
                        </div>
                        <!-- Chat Toolkit end -->
                    </div>
                </div>

        </div>





        <div class="col-md-12" style="padding:0px;padding-left:40px;">
            <?php
            if ($designer_file[$i]['grade'] != 0) {
                for ($k = 1; $k <= 10; $k++) {
                    ?>
                    <?php
                    if ($designer_file[$i]['grade'] >= $k) {
                        $grade = "e52344";
                    } else {
                        $grade = "969191";
                    }
                    if ($designer_file[$i]['grade'] != 0) {
                        $gradeclick = "";
                    } else {
                        $gradeclick = "gradeclick";
                    }
                    ?>
                    <span class="whitetext weight600 <?php echo $gradeclick; ?> currentgrade_<?php echo $k; ?>" style="background:#<?php echo $grade; ?>;border-radius:5px;padding: 8px 12px;margin: 3px;height: 50px;" data-id="<?php echo $designer_file[$i]['id']; ?>"><?php echo $k; ?></span>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>

<?php } ?>


                                                <?php
                                                for ($i = 0; $i < sizeof($designer_file); $i++) {
                                                    if ($designer_file[$i]['id'] == $main_id) {
                                                        continue;
                                                    }
                                                    $created_date_time = strtotime($designer_file[$i]['created']);

                                                    $now_date_time = strtotime(date("Y-m-d H:i:s"));
                                                    $span = "";
                                                    if (($now_date_time - $created_date_time) < 5000) {
                                                        $span = '<span style="background:#ec445c;padding:5px;color:#fff;font-weight:600;position: absolute;top: 0px;">NEW</span>';
                                                    }
                                                    ?>
                                                    <div class="col-md-12" stle="padding:0px;">
                                                        <div class="col-md-12" style="padding:0px;padding-bottom:15px;padding-left:40px;border-right:1px solid;">

    <!--   <p style="text-align:center;font-size:12px;" class="pinktext font18 approve_reject_new_div" >
                                                            <?php
                                                            if ($designer_file[$i]['status'] == "Approve") {
                                                                echo 'Design is Approved.';
                                                            } elseif ($designer_file[$i]['status'] == "Reject") {
                                                                echo 'Design is Rejected.';
                                                            }
                                                            ?>
    </p>-->

                                                            <div class="col-md-12" style="padding:0px;">

                                                                <div class="col-md-6" style="padding:0px;">
                                                                    <h5 class="darkblacktext weight600 ls0"><?php echo $designer_file[$i]['src_file']; ?></h5>
                                                                    <p class="lightdarkblacktext" style="font-size:16px;margin-top:10px;"><?php echo number_format(filesize("./uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']) / 1048576, 2) . " Mb"; ?> | 
                                                                        <?php
                                                                        $date = strtotime($designer_file[$i]['created']); //Converted to a PHP date (a second count)
                                                                        //Calculate difference
                                                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
                                                                        $minutes = floor($diff / 60);
                                                                        $hours = floor($diff / 3600);
                                                                        if ($hours == 0) {
                                                                            echo $minutes . " Minutes Ago";
                                                                        } else {
                                                                            $days = floor($diff / (60 * 60 * 24));
                                                                            if ($days == 0) {
                                                                                echo $hours . " Hours Ago";
                                                                            } else {
                                                                                echo $days . " Days Ago";
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>
                                                                <div class="col-md-4" style="float:right;padding:0px;">
                                                                    <a href="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>" download style="text-align:right;"><p class="darkblacktext weight600" style="font-size:16px;margin-top:10px;"><i class="fa fa-download" style="padding-right: 10px;"></i>Download</p></a>

                                                                    <form method="post" action="<?php echo base_url(); ?>customer/request/design_rating" style="padding-top:5px;float:right;display:inline-block;">
                                                                        <?php
                                                                        for ($j = 1; $j <= 5; $j++) {
                                                                            if ($designer_file[$i]['customer_grade'] >= $j) {
                                                                                echo '<i class="fa fa-star pl5 pinktext fa-star' . $j . $i . '" data-index="' . $j . '" data-id="' . $i . '"></i>';
                                                                            } else {
                                                                                echo '<i class="fa fa-star pl5 greytext fa-star' . $j . $i . '" data-index="' . $j . '" data-id="' . $i . '"></i>';
                                                                            }
                                                                            ?>

                                                                        <?php } ?>
                                                                        <input type="hidden" name="customer_rating" id="customer_rating<?php echo $i; ?>" value="<?php echo $designer_file[$i]['customer_grade']; ?>" />
                                                                        <input type="hidden" name="id" value="<?php echo $designer_file[$i]['id']; ?>" />
                                                                        <input type="hidden" name="request_id" value="<?php echo $designer_file[$i]['request_id']; ?>" />
                                                                    </form>
                                                                    <p class="weight600 pinktext" style="font-size:18px;padding:0px;float:right;display:inline-block;">Rating
                                                                    </p>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12 mydivposition mydivposition_<?php echo $designer_file[$i]['id']; ?>"  style="padding-left: 0px;background-color:#ccc;height: 625px;max-width: 99%;max-height: 625px;margin-top:40px;" onclick="printMousePos(event,<?php echo $designer_file[$i]['id']; ?>);" id="mydivposition_<?php echo $designer_file[$i]['id']; ?>" onmouseout="hideclicktocomment();" onmouseover="showclicktocomment(event,<?php echo $designer_file[$i]['id']; ?>);" onmousemove="showclicktocomment(event,<?php echo $designer_file[$i]['id']; ?>);" >
                                                                <div class="col-md-12 "  style="padding:10px;">
                                                                    <image src="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" style="max-height:100%;width:100%;max-height: 600px;margin: auto;display: block;"  />
                                                                 <!--   <div onclick="$('.open_<?php echo $designer_file[$i]['id']; ?>').toggle();" style="height:20px;width:20px;border-radius:50%;    background: red;position: absolute;top: 50px;left: 50px;"></div>-->
                                                                </div>
                                                                <div class="appenddot_<?php echo $designer_file[$i]['id']; ?>">
                                                                    <!-- Chat toolkit start-->
                                                                    <?php
                                                                    for ($j = 0; $j < sizeof($designer_file[$i]['chat']); $j++) {
                                                                        if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                                                            $background = "pinkbackground";
                                                                        } else {
                                                                            $background = "bluebackground";
                                                                        }
                                                                        ?>
                                                                        <?php
                                                                        if ($designer_file[$i]['chat'][$j]['xco'] != "") {
                                                                            if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                                                                $image1 = "dot_image1.png";
                                                                                $image2 = "dot_image2.png";
                                                                            } else {
                                                                                $image1 = "designer_image1.png";
                                                                                $image2 = "designer_image2.png";
                                                                            }
                                                                            ?>
                                                                            <div class="<?php //echo $background;     ?>" style="position:absolute;left:<?php echo $designer_file[$i]['chat'][$j]['xco']; ?>px;top:<?php echo $designer_file[$i]['chat'][$j]['yco']; ?>px;width:25px;z-index:99999999999999999;height:25px;" onclick='show_customer_chat(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, event);'>

                                                                                <img src="<?php echo base_url() . "public/" . $image1; ?>" class="customer_chatimage1 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"/>
                                                                                <img style="display:none;" src="<?php echo base_url() . "public/" . $image2; ?>" class="customer_chatimage2 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"/>
                                                                            </div>
                                                                            <div class="customer_chat customer_chat<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none;background:#fff;position:absolute;left:<?php echo $designer_file[$i]['chat'][$j]['xco'] + 25; ?>px;top:<?php echo $designer_file[$i]['chat'][$j]['yco'] + 10; ?>px;padding: 5px;border-radius: 5px;">
                                                                                <label><?php echo $designer_file[$i]['chat'][$j]['message']; ?></label>
                                                                            </div>
                                                                        <?php }
                                                                        ?>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="openchat" style="width:35%;position:absolute;display:none;padding:10px 20px;background:#fff;">
                                                                    <textarea class='form-control text_write text_<?php echo $designer_file[$i]['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px;margin-bottom:5px;" placeholder="Leave a comment" onkeydown="javascript: if (event.keyCode == 13) {
                                                                                    $('.send_text<?php echo $designer_file[$i]['id']; ?>').click();
                                                                                }"></textarea>
                                                                    <button class="pinkbackground send_request_img_chat send_text send_text<?php echo $designer_file[$i]['id']; ?>" style="border: none;border-radius: 5px;padding: 1px 10px;" data-fileid="<?php echo $designer_file[$i]['id']; ?>" 
                                                                            data-senderrole="designer" 
                                                                            data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                                                                            data-receiverid="<?php echo $request[0]['customer_id']; ?>" 
                                                                            data-receiverrole="customer"
                                                                            data-customername="<?php echo $user[0]['first_name'] . " " . $user[0]['last_name']; ?>"
                                                                            data-xco="" data-yco=""><i class="fa fa-send whitetext"></i></button>
                                                                    <span class="mycancellable greytext" style="font-size:10px;cursor:pointer;">Cancel</span>
                                                                </div>

                                                                <!-- Chat Toolkit end -->
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12" style="padding:0px;padding-left:40px;">
                                                            <?php
                                                            if ($designer_file[$i]['grade'] != 0) {
                                                                for ($k = 1; $k <= 10; $k++) {
                                                                    ?>
                                                                    <?php
                                                                    if ($designer_file[$i]['grade'] >= $k) {
                                                                        $grade = "e52344";
                                                                    } else {
                                                                        $grade = "969191";
                                                                    }
                                                                    if ($designer_file[$i]['grade'] != 0) {
                                                                        $gradeclick = "";
                                                                    } else {
                                                                        $gradeclick = "gradeclick";
                                                                    }
                                                                    ?>
                                                                    <span class="whitetext weight600 <?php echo $gradeclick; ?> currentgrade_<?php echo $k; ?>" style="background:#<?php echo $grade; ?>;border-radius:5px;padding: 8px 12px;margin: 3px;height: 50px;" data-id="<?php echo $designer_file[$i]['id']; ?>"><?php echo $k; ?></span>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>

                                                <?php } ?>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="chat-main">
                                                <h3 style="margin-top:20px;">Leave a comment</h3>
                                                <div class="col-md-12 myscroller messagediv_<?php echo $_GET['id']; ?>" style="padding:0px;overflow-y:scroll;height:600px;margin-bottom:20px;">
                                                    <?php for ($j = 0; $j < sizeof($chat_request); $j++) { ?>
                                                        <?php if ($chat_request[$j]['sender_type'] == "designer") { ?>

                                                            <div class="chatbox2">
                                                                <div class="message-text-wrapper">
                                                                    <p class="title">
                                                                        <?php
                                                                        if ($j != 0) {
                                                                            if ($chat_request[$j]['sender_type'] != $chat_request[$j - 1]['sender_type']) {
                                                                                echo $user[0]['first_name'] . " " . $user[0]['last_name'];
                                                                            }
                                                                        } else {
                                                                            echo $user[0]['first_name'] . " " . $user[0]['last_name'];
                                                                        }
                                                                        ?>
                                                                    </p>

                                                                    <p class="description">
                                                                        <?php echo $chat_request[$j]['message']; ?>
                                                                    </p>

                                                                    <p class="posttime">
                                                                        <?php
                                                                        $date = strtotime($chat_request[$j]['created']); //Converted to a PHP date (a second count)
                                                                        //Calculate difference
                                                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
                                                                        $minutes = floor($diff / 60);
                                                                        $hours = floor($diff / 3600);
                                                                        if ($hours == 0) {
                                                                            echo $minutes . " Minutes Ago";
                                                                        } else {
                                                                            $days = floor($diff / (60 * 60 * 24));
                                                                            if ($days == 0) {
                                                                                echo $hours . " Hours Ago";
                                                                            } else {
                                                                                echo $days . " Days Ago";
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        <?php } else { ?>

                                                            <div class="chatbox">

                                                                <div class="avatar">
                                                                    <?php
                                                                    if ($data[0]['customer_image']) {
                                                                        echo '<img src="' . base_url() . 'uploads/profile_picture/' . $data[0]['customer_image'] . '" class="img-circle">';
                                                                    } else {
                                                                        ?>
                                                                        <div class="myprofilebackground" style="width:50px;height:50px;border-radius:50%;background: #e6e2e2;text-align: center;padding-top: 15px;font-size: 15px;color: #fff;">
                                                                            <?php echo $data[0]['customer_sortname']; ?>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    //<img class="img-circle avatar" alt="chat avatar" src="https://bootdey.com/img/Content/avatar/avatar1.png">
                                                                    ?>
                                                                </div>

                                                                <div class="message-text-wrapper">

                                                                    <p class="title">
                                                                        <?php
                                                                        if ($j != 0) {
                                                                            if ($chat_request[$j]['sender_type'] != $chat_request[$j - 1]['sender_type']) {
                                                                                if ($chat_request[$j]['sender_type'] == "designer") {
                                                                                    echo $data[0]['designer_name'];
                                                                                } elseif ($chat_request[$j]['sender_type'] == "admin") {
                                                                                    echo "Admin";
                                                                                } elseif ($chat_request[$j]['sender_type'] == "qa") {
                                                                                    echo "QA";
                                                                                } elseif ($chat_request[$j]['sender_type'] == "va") {
                                                                                    echo "VA";
                                                                                }
                                                                            }
                                                                        } else {
                                                                            if ($chat_request[$j]['sender_type'] == "designer") {
                                                                                echo $data[0]['designer_name'];
                                                                            } elseif ($chat_request[$j]['sender_type'] == "admin") {
                                                                                echo "Admin";
                                                                            } elseif ($chat_request[$j]['sender_type'] == "qa") {
                                                                                echo "QA";
                                                                            } elseif ($chat_request[$j]['sender_type'] == "va") {
                                                                                echo "VA";
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </p>

                                                                    <p class="description">
                                                                        <?php echo $chat_request[$j]['message']; ?>
                                                                    </p>
                                                                    <p class="posttime">
                                                                        <?php
                                                                        $date = strtotime($chat_request[$j]['created']); //Converted to a PHP date (a second count)
                                                                        //Calculate difference
                                                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
                                                                        $minutes = floor($diff / 60);
                                                                        $hours = floor($diff / 3600);
                                                                        if ($hours == 0) {
                                                                            echo $minutes . " Minutes Ago";
                                                                        } else {
                                                                            $days = floor($diff / (60 * 60 * 24));
                                                                            if ($days == 0) {
                                                                                echo $hours . " Hours Ago";
                                                                            } else {
                                                                                echo $days . " Days Ago";
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        <?php } ?>
                                                    <?php } ?>

                                                </div>
                                                <input  type='text' class='form-control sendtext text_<?php echo $request[0]['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px; height: 44px;" placeholder="Reply To Client" />
                                                <span class="chatsendbtn send_request_chat send" style="bottom: 25px;top: unset;right: 14px;" 
                                                      data-requestid="<?php echo $request[0]['id']; ?>" 
                                                      data-senderrole="designer" 
                                                      data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                                                      data-receiverid="<?php echo $request[0]['customer_id']; ?>" 
                                                      data-receiverrole="customer"
                                                      data-sendername="<?php echo $user[0]['first_name'] . " " . $user[0]['last_name']; ?>">Send
                                                </span>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- approve Modal -->
            <div id="approvemodal" class="modal fade" role="dialog" style="margin-top:10%;">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-body " style="background-color:#6ad154;text-align: center;font-size: 170px;">
                            <i class="fa fa-check whitetext"></i>
                            <h3 style="letter-spacing: -1px;font-size: 21px;" class="whitetext weight600">DESIGN APPROVED!</h3>
                        </div>
                        <div class="modal-footer" style="text-align:center;">
                            <h4 class="pinktext weight600" style="font-size:16px;letter-spacing: -1px;">RATE YOUR DESIGN</h4>
                            <p style="font-size:35px;">
                                <i class="fa fa-star pl5 greytext myapprovestar myapprovestar1"  data-id="1"></i>
                                <i class="fa fa-star pl5 greytext myapprovestar myapprovestar2" data-id="2"></i>
                                <i class="fa fa-star pl5 greytext myapprovestar myapprovestar3" data-id="3"></i>
                                <i class="fa fa-star pl5 greytext myapprovestar myapprovestar4" data-id="4"></i>
                                <i class="fa fa-star pl5 greytext myapprovestar myapprovestar5" data-id="5"></i>
                            </p>
                            <input type="hidden" id="myapprovedid" />
                            <p style="padding-left:10%;padding-right:10%;margin-top:35px;">
                                Click "Download' button to access source files and hi-resolution version of your design.
                            </p>
                            <a type="button" class="btn whitetext weight600 design_download" style="background:#6ad154;padding:15px;border-radius: 5px;" download>DOWNLOAD SOURCE FILES</a>
                            <p style="color:#6ad154;cursor:pointer;"  data-dismiss="modal">Thanks, I'll download this later.</p>
                            <p style="margin-top:25px;">Please be notified that all source files will be removed after 14 days upon approval</p>

                        </div>
                    </div>

                </div>
            </div>
            <!-- reject_modal-->
            <div class="modal fade" id="reject_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Please Suggest Why You Reject?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="<?php echo base_url(); ?>/customer/request/suggest_view" method="post">
                            <div class="modal-body rejectbody">
                                <input type="text" name="suggest" class="form-control" placeholder="Please Suggest Why You Reject?" required/>

                                <input type="hidden" name="requestid" id="reject_requestid" />
                                <input type="hidden" name="fileid" id="reject_fileid" />
                                <input type="hidden" name="senderrole" value="customer"/>
                                <input type="hidden" name="senderid" value="<?php echo $_SESSION['user_id']; ?>"/>
                                <input type="hidden" name="receiverid" value="<?php echo $request[0]['designer_id']; ?>"/>
                                <input type="hidden" name="receiverrole" value="designer"/>
                            </div>
                            <div class="modal-footer">

                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>	
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>
        .openchat :before{
            content: "\A";
            border-style: solid;
            border-width: 0px 15px 15px 0;
            border-color: transparent #fff transparent transparent;
            position: absolute;
            left: -15px;
            left: 40%;
            transform: rotate(-47deg);
            top: -5px;
        }
        .fa-send:before, .fa-paper-plane:before {
            content: "\f1d8";
            position:unset;
            border-style: unset;
        }
        .customer_chat {
            width: 200px;
            z-index: 99999999999999999999999999999999999;
        }
        .customer_chat label {
            width: 180px;
        }
    </style>
    <script>

        $('.respose_reject').click(function () {
            var id = $(this).attr("data-requestid");
            var fileid = $(this).attr("data-fileid");
            $("#reject_fileid").val(fileid);
            $("#reject_requestid").val(id);
        });

        function show_customer_chat(id, event) {

            $(".customer_chat").hide();
            $(".customer_chat" + id).toggle();
            setTimeout(function () {
                $('.myallremove').remove();
                $(".openchat").hide();
                $(".customer_chat").each(function () {
                    if ($(this).html() == "<label></label>") {
                        $(this).remove();
                    }
                    if ($(this).text() == "") {
                        $(this).remove();
                    }
                })
                $(".customer_chatimage" + id).toggle();
                $(".customer_chat" + id).toggle();

                $('.showclicktocomment').remove();
                $('.mycancel').remove();
                event.preventDefault();
            }, 1);
            event.stopPropogation();
            return false;
        }

        $(".openchat").click(function () {
            $('.showclicktocomment').remove();
            return false;
            event.preventDefaukt();
        })

        function myFunc(event)
        {
            $(".openchat").remove();
        }
        function showclicktocomment(event, id) {
            var position = $("#mydivposition_" + id).offset();
            var posX = position.left;
            var posY = position.top;
            var left = event.pageX - posX;
            var top = event.pageY - posY + +10;
            $('.showclicktocomment').remove();
            if ($(".openchat").css("display") != "block") {
                $('.openchat').after('<div class="showclicktocomment" style="z-index: 2147483647; color: rgb(255, 255, 255); background: rgb(199, 193, 193); padding: 5px; font-weight: 600; position: absolute; left: ' + left + 'px; top: ' + top + 'px; display: none;">\Click To Comment\</div>');
                $("#mydivposition_" + id + ' .showclicktocomment').show();

                //event.stopPropaganation();
                // event.preventDefault();
                return false;
            }

        }

        function hideclicktocomment()
        {
            $('.showclicktocomment').remove();
        }
        function printMousePos(event, id) {
            // var a=0;
            // $(".customer_chat").each(function(){
            // if($(this).css("display")=="none"){
            // a=1;
            // }
            // })
            // if(a==0){
            // alert("hello");
            $(".openchat").css("z-index", "0");
            $('.slick-active .openchat').css("z-index", "9999999999999999999");
            $(".customer_chatimage1").show();
            $(".customer_chatimage2").hide();
            $('.customer_chat').hide();
            $('.showclicktocomment').remove();
            var position = $("#mydivposition_" + id).offset();
            var posX = position.left;
            var posY = position.top;
            //alert((event.pageX - posX)+ ' , ' + (event.pageY - posY));
            $('.openchat .greentext').hide();
            $('.openchat').css("left", event.pageX - posX + +7);
            $('.openchat').css("top", event.pageY - posY + +7);
            $(".openchat").show();
            $('.send_text' + id).attr("data-yco", event.pageY - posY);
            $('.send_text' + id).attr("data-xco", event.pageX - posX);


            var posx = Math.round(event.pageX - posX - 10);
            var posy = Math.round(event.pageY - posY - 10);
            var posx_ = Math.round(event.pageX - posX);
            var posy_ = Math.round(event.pageY - posY);

            var myposition = Math.round(posX + posY);
            $('.send_text' + id).attr("data-cancel-false", myposition)
            $('.mycancellable').attr("data-cancel", myposition);
            $(".mydiv" + myposition).remove();
            $('.appenddot_' + id).append('<div class="mydiv' + myposition + ' mycancel" style="position:absolute;left:' + posx + 'px;top:' + posy + 'px;width:25px;height:25px;" onclick="show_customer_chat(' + myposition + ',event);">\<img src="<?php echo base_url(); ?>public/designer_image1.png">\</div>\<div class="customer_chat customer_chat' + myposition + '" style="display:none;background:#fff;position:absolute;left:' + posx_ + 'px;top:' + posy_ + 'px;padding: 5px;border-radius: 5px;">\<label></label>\</div>');
            // // alert(event.clientX+ ' '+event.clientY)
            // // var position = $("#mydivposition").position();
            // // alert(position.left+ ' '+position.top)
            //}
        }
        $(document).ready(function () {
            var total_design = <?php echo sizeof($designer_file); ?>;
            var current_design = 1;
            $(".slick-next").click(function () {
                current_design += 1;
                if (current_design > total_design) {
                    current_design = 1;
                }
                $(".pagin-one").html(current_design + " of " + total_design);
            });

            $(".slick-prev").click(function () {
                current_design -= 1;
                if (current_design == 0) {
                    current_design = total_design;
                }
                $(".pagin-one").html(current_design + " of " + total_design);
            });



            /*$(".myapprovestar").click(function () {
             $(".myapprovestar").removeClass("pinktext");
             for (var i = 1; i <= $(this).attr("data-id"); i++) {
             $(".myapprovestar" + i).addClass("pinktext");
             }
             $.ajax({
             type: "POST",
             url: "<?php echo base_url(); ?>customer/request/designer_rating_ajax",
             data: {"fileid": $("#myapprovedid").val(),
             "value": $(this).attr("data-id")},
             success: function (data) {
             //alert("---"+data);
             //alert("Settings has been updated successfully.");
             if (data == 1) {
             if (value == "Approve") {
             //$('.approve_reject_div'+fileid).html('<p class="pinktext font18">Design is Approved Succesfully..!</p>');
             $("#myapprovedid").val(fileid);
             $('.approve_reject_new_div').html('Design is Approve Succesfully..!')
             $('.design_download').attr('href', file);
             $('#approvemodal').modal('show');
             } else {
             //$('.approve_reject_div'+fileid).html('<p class="darkblacktext font18">Design is Reject Succesfully..!</p>');
             $('.approve_reject_new_div').html('Design is Reject Succesfully..!')
             }
             }
             }
             });
             event.preventDefault();
             });*/
            $('.text_write').keypress(function (e) {
                if (e.which == 13) {
                    //$('.send_text').click();
                    //return false;  
                }
            });

        });
    </script>
    <script>
        $(document).ready(function () {

            $('.t_w').keypress(function (e) {
                if (e.which == 13) {
                    $('.s_t').click();
                    return false;
                }
            });

            $('.sendtext').keypress(function (e) {
                if (e.which == 13) {
                    $('.send').click();
                    return false;
                }
            });


        });
    </script>

    <script>

        $('.mycancellable').click(function () {
            $('.openchat').hide();
            var mycancel = $(this).attr('data-cancel');
            $('.mydiv' + mycancel).hide();
        });

        function mycancellable(id)
        {
            $('.openchat').hide();
            var mycancel = $(".mycancellable" + id).attr('data-cancel');
            $('.mydiv' + mycancel).hide();
        }

        $(".fa-star").click(function () {
            /* if ($(this).hasClass("myapprovestar")) {
             return false;
             }
             var number = $(this).attr("data-index");
             var id = $(this).attr("data-id");
             
             for (var i = 1; i <= 5; i++) {
             var myid = "fa-star" + i + id;
             if (i <= number) {
             $("." + myid).css("color", "#ec4159");
             } else {
             $("." + myid).css("color", "unset");
             }
             }
             $("#customer_rating" + id).val(number);
             
             if (id != "")
             {
             $('.rating_btn_' + id).click();
             } else
             {
             
             var id = $("#myrequest_id").val();
             var file_id = $("#myfilerequest_id").val();
             window.location = "<?php echo base_url(); ?>customer/request/design_rating2/" + id + "/" + file_id + "/" + number;
             }*/

        });
    </script>
    <script>
        (function ($) {
            $(window).on("load", function () {

                $(".myscroller").mCustomScrollbar({
                    theme: "dark-3"
                });
                $(".myscroller").mCustomScrollbar("scrollTo", "bottom");
                $(".myscroller").css("overflow-y", "hidden");

            });
        })(jQuery);
    </script>
    <style>
        .chatbox .description{ width:180px; }
    </style>