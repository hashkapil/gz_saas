<style type="text/css">
	.setimgblogcaps > img {
	    display: block;
	    margin: 30px auto 10px;
	}
	.pad {
	    margin-bottom: 18px;
	}
</style>
<section class="con-b">
	<?php 	foreach ($edit_profile as $data): ?>
		<div class="container">
		
			<h2 class="head-d text-left">My Profile</h2>   
			
			<p class="space-d"></p>
		
			<div class="row">
				<div class="col-md-4">
					<div class="project-row-qq1 settingedit-box">
						<form action="<?php echo base_url(); ?>designer/profile/designer_edit_profile_pic" method="post" enctype="multipart/form-data">
							<div class="settrow">
								
								<div class="settcol right">
									<p class="btn-x"><button type="submit" class="btn-e" name="submit_pic">Save</button></p> 
								</div>
							</div>
							
							<div class="setimg-row33">
								<div class="imagemain2" style="padding-bottom: 20px; display: none;">
									<input type="file" onChange="validateAndUpload(this);" class="form-control dropify waves-effect waves-button" name="profile" style="display:none;" id="inFile"/>
								</div>
								<div class="imagemain">
									<figure class="setimg-box33 big-cirlce">
										<img src="<?php echo base_url()."uploads/profile_picture/".$data['profile_picture']; ?>" class="img-responsive telset33">
										<a class="setimg-blog33" href="#" onclick="$('.dropify').click();">
											<span class="setimgblogcaps">
												<img src="<?php echo base_url();?>theme/designer_css/images/icon-camera.png" class="img-responsive"><br>
												<span class="setavatar33">Change <br>Avatar</span>
											</span>
										</a>
									</figure>
							    </div>
							    <div class="imagemain3" style="display: none;">
							    	<figure class="setimg-box33 big-cirlce">
								        <img src="" id="image3" class="img-responsive telset33">
										<a class="setimg-blog33" href="#" onclick="$('.dropify').click();">
											<span class="setimgblogcaps">
												<img src="<?php echo base_url();?>theme/designer_css/images/icon-camera.png" class="img-responsive"><br>
												<span class="setavatar33">Change <br>Avatar</span>
											</span>
										</a>
								    </figure>
							    </div>
							</div>
						</form>
						<p class="space-a"></p>
						<div class="form-group setinput">
							<input class="form-control input-b" placeholder="<?php echo $data['first_name']." ".$data['last_name'];?>" type="text" value="<?php echo $data['first_name']." ".$data['last_name'];?>">
						</div>
						
					</div>	
				</div>
			
				<div class="col-md-4">
					<div class="project-row-qq1 profielveiwee-box">
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-b">Skills</h3>
							</div>
							<div class="settcol right">
								<p class="btn-x"><button class="btn-e text-wrap">Save</button></p>
							</div>
						</div>
						<p class="space-e"></p>
						
						<div class="ui-slid-errow">
							<div class="pro-gressbar">
								<h3 class="progress-title">Graphic Design</h3>
								<div class="progress-value-1"><span class="value" id="value1">0</span>%</div>
							</div>
						
							<div data-target="#value1" class="ui-slider-control"></div>
						</div>
						
						<div class="ui-slid-errow">
							<div class="pro-gressbar">
								<h3 class="progress-title">Web Design</h3>
								<div class="progress-value-1"><span class="value" id="value2">0</span>%</div>
							</div>
						
							<div data-target="#value2" class="ui-slider-control"></div>
						</div>
						
						<div class="ui-slid-errow">
							<div class="pro-gressbar">
								<h3 class="progress-title">Illustration</h3>
								<div class="progress-value-1"><span class="value" id="value3">0</span>%</div>
							</div>
						
							<div data-target="#value3" class="ui-slider-control"></div>
						</div>
						
						<div class="ui-slid-errow">
							<div class="pro-gressbar">
								<h3 class="progress-title">Logo Design</h3>
								<div class="progress-value-1"><span class="value" id="value4">0</span>%</div>
							</div>
						
							<div data-target="#value4" class="ui-slider-control"></div>
						</div>
						
						<p class="space-c"></p>
						
						<p class="ad-dskills"><a href="">+ Add Skill</a></p>
						
						
					</div>	
				</div>
				
				<div class="col-md-4">
					<div class="project-row-qq1 pr-ofile-desinger">
						<form method="post" action="<?php echo base_url(); ?>designer/profile/designer_edit_profile">
							<div class="settrow">
								<div class="settcol left">
									<h3 class="sub-head-b">Edit Profile</h3>
								</div>
								<div class="settcol right">
									<p class="btn-x"><button name="submit" type="submit" class="btn-e text-wrap">Save</button></p>
								</div>
							</div>
							<p class="space-c"></p>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group goup-x1">
										<input class="form-control input-c" placeholder="First Name" type="text" name="first_name" value="<?php echo $data['first_name']; ?>">
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group goup-x1">
										<input class="form-control input-c" placeholder="Last Name" type="text" name="last_name" value="<?php echo $data['last_name']; ?>">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-group goup-x1">
										<div class="settrow paypalinput">
											<div class="settcol left">
												<input class="form-control input-c" placeholder="Paypal id" type="text" name="paypal_id" value="<?php echo $data['paypal_email_address'];?>">
											</div>
											<div class="settcol right">
												<p class="btn-x"><button data-toggle="modal" data-target="#myModal" class="btn-e text-wrap" id="changepass">Change Password</button></p> 
											</div>
											<div id="myModal" class="modal fade" role="dialog">
											  <div class="modal-dialog">

											    <!-- Modal content-->
											    <div class="modal-content">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <h4 class="modal-title">Change Password</h4>
											      </div>
											      <div class="modal-body">
											        <form method="post" action="<?php echo base_url(); ?>designer/profile/designer_change_password__edit_profile">
														<div class="pad">
															<p class="colorblack">Old Password</p>
															<input type="password" placeholder="Old Password" name="old_password" class="form-control"/>
														</div>
														<div class="pad">
															<p class="colorblack">New Password</p>
															<input type="password" placeholder="New Password" name="new_password" class="form-control"/>
														</div>
														<div class="pad">
															<p class="colorblack">Confirm Password</p>
															<input type="password" placeholder="Confirm Password" name="confirm_password" class="form-control"/>
														</div>
														<div class="pad" >	
															<input  style="background-color:#ec1c41;color:#fff" type="submit" name="comformbtn" value="Submit" class="form-control"/>
														</div>
													</form>
											      </div>
											    </div>

											  </div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group goup-x1">
										<input class="form-control input-c" placeholder="Email" type="text" name="email" value="<?php echo $data['email']; ?>">
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group goup-x1">
										<input class="form-control input-c" placeholder="Phone" type="text" name="phone_number" value="<?php echo $data['phone']; ?>">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-group goup-x1">
										<input class="form-control input-c" placeholder="Address Line 1" type="text" name="address" value="<?php echo $data['address_line_1']; ?>">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group goup-x1">
										<input class="form-control input-c" placeholder="City" type="text" name="city" value="<?php echo $data['city']; ?>">
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group goup-x1">
										<input class="form-control input-c" placeholder="State" type="text" name="state" value="<?php echo $data['state']; ?>">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-group goup-x1">
										<div class="row">
											<div class="col-md-6">
												<input class="form-control input-c" placeholder="Zip" type="text" name="zip_code" value="<?php echo $data['zip']; ?>">
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>	
				</div>
				
			</div>
			
		</div>
	<?php endforeach; ?>
	</section>
