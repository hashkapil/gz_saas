<!-- jQuery (necessary for JavaScript plugins) -->
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/designer/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/all.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/designer/bootstrap.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/ui-slider/jquery.ui-slider.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/dropify/dist/js/dropify.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'js/designer/designer_portal.js'); ?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/jquery.fancybox.min.js');?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>


<script type="text/javascript">
    setTimeout(function () {
        $('.alert').fadeOut();
    }, 2000);
    var dzm = 1;
    zm = 0.3;
    $('.skills_btn').click(function () {
        var val1 = $('#sckill_val1').val();
        var val2 = $('#sckill_val2').val();
        var val3 = $('#sckill_val3').val();
        var val4 = $('#sckill_val4').val();
        var val5 = $('#sckill_val5').val();
        var val6 = $('#sckill_val6').val();
        var val7 = $('#sckill_val7').val();
        var position1 = [val1, val2, val3, val4, val5, val6, val7];
        var i = 0;
        $(position1).each(function (index) {
            //alert(position1[i]);
            $('#ui-slider-control' + i)
                    .UISlider({
                        min: 1,
                        max: 101,
                        smooth: false,
                        value: position1[i]
                    })
                    .on('change thumbmove', function (event, value) {
                        var targetPath = $(event.target).data('target');
                        $(targetPath).text(value);
                        $(targetPath).next('input.value_skill').val(value);
                        $('#dataaaaa').val(targetPath);
                    })
                    .on('start', function () {
                        $('.value').addClass('editing');
                    })
                    .on('end', function () {
                        $('.value').removeClass('editing');
                    });

            $('.value' + i).text(position1[i]);
            i = i + 1;
        });
    });
    
    $(document).ready(function () {
    var position = 0;
    var val1 = $('#sckill_val1').val();
    var val2 = $('#sckill_val2').val();
    var val3 = $('#sckill_val3').val();
    var val4 = $('#sckill_val4').val();
    var val5 = $('#sckill_val5').val();
    var val6 = $('#sckill_val6').val();
    var val7 = $('#sckill_val7').val();
    var position1 = [val1, val2, val3, val4, val5, val6, val7];
    if ($('#sckill_val1').val() > '0') {
        $(position1).each(function (i) {
            $('#ui-slider-control' + i)
                .UISlider({
                    min: 1,
                    max: 101,
                    smooth: false,
                    value: position1[i]
                })

                .on('change thumbmove', function (event, value) {
                    var targetPath = $(event.target).data('target');
                    //console.log(targetPath,value);
                    $(targetPath).text(value);
                    $(targetPath).next('input.value_skill').val(value);
                    $('#dataaaaa').val(targetPath);
                })
                .on('start', function () {
                    $('.value').addClass('editing');
                })
                .on('end', function () {
                    $('.value').removeClass('editing');
                });
            $('input[type=checkbox]')
                .on('change', function () {

                    var name = $(this).prop('name'),
                        value = $(this).prop('checked');

                    if (name === 'popup') {

                        $('.popup-buttons').toggle(value);

                    } else {

                        $('.ui-slider-control').UISlider(name, value);
                    }
                });

        });
    }
//        var position = 0;
//
//        $('.ui-slider-control')
//                .UISlider({
//                    min: 1,
//                    max: 101,
//                    smooth: false,
//                    value: position
//                })
//                .on('change thumbmove', function (event, value) {
//                    var targetPath = $(event.target).data('target');
//                    $(targetPath).text(value);
//                    $(targetPath).next('input.value_skill').val(value);
//                    $('#dataaaaa').val(targetPath);
//                })
//                .on('start', function () {
//                    $('.value').addClass('editing');
//                })
//                .on('end', function () {
//                    $('.value').removeClass('editing');
//                });
//
//        $('.value').text(position);
//
//
//        $('input[type=checkbox]')
//                .on('change', function () {
//
//                    var name = $(this).prop('name'),
//                            value = $(this).prop('checked');
//
//                    if (name === 'popup') {
//
//                        $('.popup-buttons').toggle(value);
//
//                    } else {
//
//                        $('.ui-slider-control').UISlider(name, value);
//                    }
//                });
    });
    var $fileInput = $('.file-input');
    var $droparea = $('.file-drop-area');

    // highlight drag area
    $fileInput.on('dragenter focus click', function () {
        $droparea.addClass('is-active');
    });

    // back to normal state
    $fileInput.on('dragleave blur drop', function () {
        $droparea.removeClass('is-active');
    });

    // change inner text
    $fileInput.on('change', function () {
        var filesCount = $(this)[0].files.length;
        var $textContainer = $(this).prev();

        if (filesCount === 1) {
            // if single file is selected, show file name
            var fileName = $(this).val().split('\\').pop();
            $textContainer.text(fileName);
        } else {
            // otherwise show number of files
            $textContainer.text(filesCount + ' files selected');
        }
    });

    function validateAndUpload(input) {
        var URL = window.URL || window.webkitURL;
        var file = input.files[0];
        if (file) {
            console.log(URL.createObjectURL(file));
            $(".imagemain").hide();
            $(".imagemain3").show();
            $("#image3").attr('src', URL.createObjectURL(file));
            $(".dropify-wrapper").show();
        }
    }

    $(document).ready(function () {
        $('.progress-value > span').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 1500,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    });

</script>
</body>
</html>