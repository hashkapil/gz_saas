<link rel="stylesheet" type="text/css" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/jquery.fancybox.min.css');?>">
<section class="con-b project-info-page">
    <div class="row">
        <?php preg_match("/[^\/]+$/", $_SERVER['REQUEST_URI'], $matches); ?>
        <?php
        if (isset($_SERVER['HTTP_REFERER'])) {
            if (strpos($_SERVER['HTTP_REFERER'], 'designer/request/project_info') == false) {
                $last_url = explode('?', $_SERVER['HTTP_REFERER'])[0];
                $_SESSION['lastUrl'] = $last_url;
            } else {
                if (isset($_SESSION['lastUrl'])) {
                    $last_url = $_SESSION['lastUrl'];
                } else {
                    $last_url = base_url() . '/qa/dashboard';
                }
            }
        } else {
            $last_url = isset($_SESSION['lastUrl']) ? $_SESSION['lastUrl'] : '';
        }
        ?>
        <div class="content_section">
            <?php if ($this->session->flashdata('message_error') != '') { ?>
                <div id="message" class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_error'); ?>
                    </p>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('message_success') != '') { ?>
                <div id="message" class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_success'); ?>
                    </p>
                </div>
            <?php }
            ?>
            <div class="rheight-xt d-progress">
                <h3 class="head-b"><p class="savecols-col left"><a href="<?php echo base_url(); ?>designer/request/" class="backbtns"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive"></a></p> <?php echo $data[0]['title']; ?></h3>

                <div class="flex-this">
                    <div class="descol right-instbox">
                        <?php if ($data[0]['status'] != "approved" && $data[0]['status'] != "hold") { ?>
                            <a href="#" class="upl-oadfi-le adddesinger" data-toggle="modal" data-target="#Addfiles">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/trail.svg"><span> Upload Draft</span>
                            </a>
                        <?php } ?>

                    </div>
                    <p class="btn-x text-right">
                        <?php
                        if ($data[0]['status_designer'] == "checkforapprove") {
                            $status = "Pending Approval";
                            $color = "bluetext";
                        } elseif ($data[0]['status_designer'] == "active") {
                            $status = "Design In Progress";
                            $color = "green";
                        } elseif ($data[0]['status_designer'] == "disapprove" && $data[0]['who_reject'] == 1) {
                            $status = "Revision";
                            $color = "orangetext";
                        } elseif ($data[0]['status_designer'] == "disapprove" && $data[0]['who_reject'] == 0) {
                            $status = "Quality Revision";
                            $color = "red";
                        } elseif ($data[0]['status_designer'] == "pending" || $data[0]['status_designer'] == "assign") {
                            $status = "In Queue";
                            $color = "gray";
                        } elseif ($data[0]['status_designer'] == "draft") {
                            $status = "Draft";
                            $color = "gray";
                        } elseif ($data[0]['status_designer'] == "hold") {
                            $status = "On Hold";
                            $color = "holdcolor";
                        } elseif ($data[0]['status_designer'] == "cancel") {
                            $status = "Cancelled";
                            $color = "holdcolor";
                        } elseif ($data[0]['status_designer'] == "approved") {
                            $status = "Completed";
                            $color = "green";
                        } elseif ($data[0]['status_designer'] == "pendingrevision") {
                            $status = "Pending Review";
                            $color = "lightbluetext";
                        } else {
                            $status = "";
                            $color = "greentext";
                        }
                        ?>
                        <a class="btn-d <?php echo $color; ?>" href="javascript:void(0)">
                            <?php echo $status; ?>
                        </a>
                        <!-- <a class="btn-d" href="javascript:void(0);" style="border-radius: 15px;"><?php
                        if ($data[0]['status_designer'] == "active") {
                            echo "IN-PROGRESS";
                        } elseif ($data[0]['status_designer'] == "assign") {
                            echo "IN-QUEUE";
                        } elseif ($data[0]['status_designer'] == "checkforapprove") {
                            echo "PENDING-APPROVAL";
                        } elseif ($data[0]['status_designer'] == "approved") {
                            echo "COMPLETED";
                        } elseif ($data[0]['status_designer'] == "disapprove") {
                            echo "REVISION";
                        } elseif ($data[0]['status_designer'] == "pendingrevision") {
                            echo "PENDING REVIEW";
                        }
                        ?></a> -->
                    </p>
                </div>
            </div>
            <div class="header-blog">
                <div class="row">
                    <div class="col-sm-12">
                        <ul id="status_check" class="list-unstyled list-header-blog project-information" role="tablist" style="border:none;">
                            <li class="active" id="1"><a data-toggle="tab" href="#designs_request_tab" role="tab">Project Drafts </a></li>
                            <li class="" id="2"><a class="nav-link tabmenu" data-toggle="tab" href="#inprogressrequest" role="tab">Project Information </a></li>
                            <li class="" id="3"><a class="nav-link tabmenu" data-toggle="tab" href="#activity_timeline" role="tab">Activity Timeline </a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="orb-xb-col right">
                <div class="tab-content">
                    <div data-group="1" data-loaded="" class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                        <div class="orb-xb">
                            <div class="orb-xb-col right">
                                <div class="orbxb-img-xx3s one-can">
                                    <div class="row-designdraftnew">
                                        <?php if ($data[0]['designer_attachment']) { ?>
                                            <ul class="list-unstyled clearfix list-designdraftnew">
                                                <?php
                                                for ($i = 0; $i < sizeof($data[0]['designer_attachment']); $i++) {
                                                    ?>
                                                    <li class="col-lg-4 col-md-6">
                                                        <div class="figimg-one">
                                                            <div class="draft-go">
                                                                <div class="status_label">
                                                                    <?php if ($data[0]['designer_attachment'][$i]['designer_seen'] == 0) { ?>
                                                                        <div class="status_label">
                                                                            <svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve">
                                                                                <g>
                                                                                    <path fill-rule="evenodd" fill="#A02302" d="M1.9,71.1l4.5,6.7V60.6L1.9,71.1z" />
                                                                                    <path fill-rule="evenodd" fill="#A02302" d="M71.4,2.2l6.7,4.5H60.9L71.4,2.2z" />
                                                                                    <path fill-rule="evenodd" fill="#F15A29" d="M2,46.5L47,2.2l24.5,0L1.9,70.9L2,46.5z" />
                                                                                </g>
                                                                            </svg>
                                                                            <span class="adnewpp">New</span>
                                                                        </div>
                                                                    <?php } ?>
                                                                    <?php if ($data[0]['designer_attachment'][$i]['status'] == "pending") { ?>
                                                                        <div class="status_label">
                                                                            <svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve">
                                                                                <g>
                                                                                    <path fill-rule="evenodd" fill="#310000" d="M1.9,71.1l4.5,6.7V60.6L1.9,71.1z" />
                                                                                    <path fill-rule="evenodd" fill="#310000" d="M71.4,2.2l6.7,4.5H60.9L71.4,2.2z" />
                                                                                    <path fill-rule="evenodd" fill="#510000" d="M2,46.5L47,2.2l24.5,0L1.9,70.9L2,46.5z" />
                                                                                </g>
                                                                            </svg>
                                                                            <span class="adnewpp">Pending review</span>
                                                                        </div>
                                                                    <?php } elseif ($data[0]['designer_attachment'][$i]['status'] == "Reject") { ?>
                                                                        <div class="status_label">
                                                                            <svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve">
                                                                                <g>
                                                                                    <path fill-rule="evenodd" fill="#715a5a" d="M1.9,71.1l4.5,6.7V60.6L1.9,71.1z" />
                                                                                    <path fill-rule="evenodd" fill="#715a5a" d="M71.4,2.2l6.7,4.5H60.9L71.4,2.2z" />
                                                                                    <path fill-rule="evenodd" fill="#866c6c" d="M2,46.5L47,2.2l24.5,0L1.9,70.9L2,46.5z" />
                                                                                </g>
                                                                            </svg>
                                                                            <span class="adnewpp">Rejected</span>
                                                                        </div>
                                                                    <?php } elseif ($data[0]['designer_attachment'][$i]['status'] == "Approve" && $data[0]['status'] == "approved") { ?>
                                                                        <div class="status_label">
                                                                            <svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve">
                                                                                <g>
                                                                                    <path fill-rule="evenodd" fill="#14a853" d="M1.9,71.1l4.5,6.7V60.6L1.9,71.1z" />
                                                                                    <path fill-rule="evenodd" fill="#14a853" d="M71.4,2.2l6.7,4.5H60.9L71.4,2.2z" />
                                                                                    <path fill-rule="evenodd" fill="#37c473" d="M2,46.5L47,2.2l24.5,0L1.9,70.9L2,46.5z" />
                                                                                </g>
                                                                            </svg>
                                                                            <span class="adnewpp">Approved</span>
                                                                        </div> 
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="draft_img_wrapper">
                                                                    <?php
                                                                    $type = substr($data[0]['designer_attachment'][$i]['file_name'], strrpos($data[0]['designer_attachment'][$i]['file_name'], '.') + 1);
                                                                    ?>
                                                                    <img src="<?php echo imageUrl($type, $data[0]['id'], $data[0]['designer_attachment'][$i]['file_name']); ?>" class="img-responsive" />
                                                                </div>
                                                                <div class="view-share">
                                                                    <a class="view-draft" href="<?php echo base_url() . "designer/request/project_image_view/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $request_id; ?>">
                                                                        <i class="fas fa-eye"></i> View
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="tm-heading">
                                                                <div class="iconwithtitle">
                                                                    <a href="<?php echo base_url() . "designer/request/project_image_view/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $request_id; ?>">    
                                                                        <?php
                                                                        $filename = $data[0]['designer_attachment'][$i]['file_name'];
                                                                        $imagePreFix = substr($filename, 0, strpos($filename, "."));
                                                                        ?>
                                                                        <h3><?php echo $imagePreFix; ?></h3>
                                                                    </a> 

                                                                </div>
                                                                <div class="chatbox-add">
                                                                    <p><?php echo $data[0]['designer_attachment'][$i]['created']; ?></p>
                                                                    <a href="<?php echo base_url() . "designer/request/project_image_view/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $request_id; ?>">
                                                                        <i class="icon-gz_message_icon"></i>
                                                                        <?php for ($j = 0; $j < count($file_chat_array); $j++): ?>
                                                                            <?php if ($file_chat_array[$j]['id'] == $data[0]['designer_attachment'][$i]['id']): ?>
                                                                                <span class="numcircle-box-01"><?php echo $file_chat_array[$j]['count']; ?></span>
                                                                            <?php endif; ?>
                                                                        <?php endfor; ?>
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php } else { ?>
                                            <div class="figimg-one no_img">
                                                <?php if ($data[0]['status'] == 'draft') { ?>
                                                    <h3 class="head-b draft_no">
                                                        Design Project is in Draft
                                                    </h3>
                                                    <a class="btn btn-x draft_btn1" href="<?php echo base_url(); ?>customer/request/new_request_brief/<?php echo $data[0]['id']; ?>?rep=1">Edit</a>
                                                    <a class="btn btn-x draft_btn2" href="<?php echo base_url(); ?>customer/request/new_request_review/<?php echo $data[0]['id']; ?>?rep=1">Publish</a>
                                                <?php } elseif ($data[0]['status'] == 'assign') { ?>
                                                    <h3 class="head-b draft_no">
                                                        Design Project is in queue
                                                    </h3>
                                                <?php } elseif ($data[0]['status'] == 'active') { ?>
                                                    <h3 class="head-b draft_no">
                                                        Working on the designs
                                                    </h3>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 2 tab  -->
                    <div data-group="1" data-loaded="" data-search-text="" class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">
                        <div class="project-row-qq1">
                            <?php if (!empty($notes)) { ?>
                                <div class="review-textcolww abler designer_notes">
                                    <a data-toggle="modal" title="View Notes" data-target="#show_notepad_area" class="add_notes adddesinger" data-id="<?php echo $data[0]['customer_id']; ?>" data-name="<?php echo $data[0]['customer_name']; ?>">
                                        View Notes
                                    </a>                                         
                                </div>
                            <?php } ?>
                            <div class="designdraft-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="designdraft-views">
                                            <div class="form-group goup-x1">
                                                <label class="label-x2"> Project Title</label>
                                                <p class="space-a"></p>
                                                <div class="review-textcolww project-title"><?php echo $data[0]['title']; ?></div>
                                            </div>
                                            <?php if (isset($data[0]['business_industry']) && $data[0]['business_industry'] != '') { ?>
                                                <div class="form-group goup-x1">
                                                    <label class="label-x2">Target Market</label>
                                                    <p class="space-a"></p>
                                                    <div class="review-textcolww"><?php echo $data[0]['business_industry']; ?> </div>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group goup-x1">
                                                <label class="label-x2"> Deliverables</label>
                                                <p class="space-a"></p>
                                                <div class="review-textcolww Deliverables"><?php echo $data[0]['deliverables']; ?></div>
                                            </div>
                                            <div class="form-group goup-x1">
                                                <label class="label-x2"> Category</label>
                                                <p class="space-a"></p>
                                                <div class="review-textcolww category"><span>
                                                    <?php
                                                        //echo "<pre/>";print_r($data);
                                                    echo isset($data['cat_name']) ? $data['cat_name'] : $data[0]['category'];
                                                    if ($data[0]['logo_brand'] != '') {
                                                        ?> </span><span> <?php
                                                        echo $data[0]['logo_brand'];
                                                    } elseif ($data['subcat_name'] != '') {
                                                        ?></span><span><?php
                                                        echo $data['subcat_name'];
                                                    }
                                                    ?></span>
                                                </div>
                                            </div>
                                            <div class="form-group goup-x1">
                                                <label class="label-x2">Brand Profile</label>
                                                <p class="space-a"></p>
                                                <div class="review-textcolww brand-profile"> 
                                                    <?php if ($branddata[0]['brand_name'] != '') { ?>
                                                        <a class="showbrand_profile" data-toggle="modal" data-target="#brand_profile"><i class="fa fa-user"></i> <?php echo $branddata[0]['brand_name']; ?></a>
                                                    <?php } else { ?>
                                                        No brand profile selected
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php if (!empty($data[0]['designer'])) { ?>
                                                <div class="form-group goup-x1">
                                                    <label class="label-x2">Design Software Requirement</label>
                                                    <p class="space-a"></p>
                                                    <div class="review-textcolww requirement"><?php echo $data[0]['designer']; ?></div>
                                                </div>
                                            <?php } ?>
                                            <?php //if (!empty($data[0]['design_dimension'])) { ?>
<!--                                                <div class="form-group goup-x1">
                                                    <label class="label-x2"> Design Dimension</label>
                                                    <p class="space-a"></p>
                                                    <div class="review-textcolww"><?php //echo $data[0]['design_dimension']; ?>										
                                                </div>
                                            </div>-->
                                        <?php //} ?>
                                       <?php if (!empty($data[0]['color_pref'])) { ?>
                                       <div class="form-group goup-x1">
                                          <label class="label-x2"><span class=""></span> Color pallete</label>
                                          <div class="review-textcolww">
                                             <?php 
                                             echo ucfirst($data[0]['color_pref'])."<br/>"; 
                                             if($data[0]['color_values']){?>
                                              <span class="color_theme">
                                                <?php echo $data[0]['color_values']; } ?>
                                              </span>
                                          </div>
                                       </div>
                                       <?php } ?>
                                    <!--******questions & answer*******-->
                                    <div class="main_divForQuestion">
                                    <?php
                                     if (!empty($request_meta)) { 
                                         foreach ($request_meta as $qkey => $qval) {
                                             if($qval['question_label'] == 'DESIGN DIMENSIONS' || $qval['question_label'] == 'DESCRIPTION'){
                                             if($qval['question_label'] == 'DESIGN DIMENSIONS'){
                                             if($qval['answer'] != ''){
                                                $qval['answer'] = $qval['answer'];
                                             }else{
                                                $qval['answer'] = $data[0]['design_dimension']; 
                                             }
                                             }
                                             if($qval['question_label'] == 'DESCRIPTION'){
                                             if($qval['answer'] != ''){
                                                $qval['answer'] = $qval['answer'];
                                             }else{
                                                $qval['answer'] = $data[0]['description'];
                                             }
                                             }
                                             }else{
                                               $qval['answer'] = $qval['answer'];  
                                             }


                                             if(isset($qval['answer']) && $qval['answer'] != ''){
                                                if(strlen(htmlspecialchars($qval['answer'])) > 100){
                                                    $answer = substr(htmlspecialchars($qval['answer']),0,97).'... <a class="expand_txt read_more" href="javascript:void(0);">Read More</a><div class="read_more_txt hide">'.htmlspecialchars($qval['answer']).'</div><a class="expand_txt read_less hide" href="javascript:void(0);">Read Less</a>';
                                                }else{
                                                    $answer = htmlspecialchars($qval['answer']);
                                                }
                                            }else{
                                                $answer = "N/A";
                                            }

                                             /******************Description excluded***************************/
                                                            if($qval['question_label'] != 'DESCRIPTION'){
                                                            ?>
                                                            <div class="form-group goup-x1 fp_ryt_info">
                                                                <label class="label-x2"> <?php echo $qval['question_label']; ?></label>
                                                                <div class="review-textcolww">
                                                                    <?php echo $answer; ?>
                                                                </div>
                                                            </div>      
                                                            <?php } else { ?>
                                                            <div class="form-group goup-x1 d_descrptn fp_ryt_info" style="width:100%;">
                                                                <label class="label-x2"> <?php echo $qval['question_label']; ?></label>
                                                                <div class="review-textcolww">
                                                                    <pre><?php echo $answer; ?></pre>
                                                                </div>
                                                            </div> 
                                                            <?php }
                                         }
                                     }else{ ?>
                                         <div class="form-group goup-x1 fp_ryt_info" >
                                             <label class="label-x2"> Design Dimension</label>
                                             <div class="review-textcolww d-dimension">
                                                 <?php echo isset($data[0]['design_dimension']) ? $data[0]['design_dimension']: 'N/A'; ?>

                                             </div>
                                         </div>
                                     <div class="form-group goup-x1 fp_ryt_info d_descrptn" style="width:100%;">
                                             <label class="label-x2"> Description</label>
                                             <div class="review-textcolww description">d
                                                 <pre><?php echo isset($data[0]['description']) ? (strlen(htmlspecialchars($data[0]['description'])) > 100)?substr(htmlspecialchars($data[0]['description']),0,97).'... <a class="expand_txt read_more" href="javascript:void(0);">Read More</a><div class="read_more_txt hide">'.htmlspecialchars($data[0]['description']).'</div> <a class="expand_txt read_less hide" href="javascript:void(0);">Read Less</a> ':htmlspecialchars($data[0]['description']) : 'N/A'; ?></pre>
                                             </div>
                                         </div>
                                     <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="designdraft-views">
                                     <!--*********samples************-->
                                                    <?php if (!empty($sampledata)) { ?>
                                                    <div class="clearfix"></div>
                                                    <div class="design-ch-pre">
                                                    <h3 class="head-b base-heading request_heading">Design choice preferences</h3>
                                                    <ul class="list-unstyled list-accessimg">
                                                    <?php //echo "<pre/>";print_R($sampledata);  
                                                    foreach($sampledata as $skey => $sval){ ?>
                                                    <li>
                                                        <div class="accimgbx33">
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS.'samples/'.$sval['sample_id'].'/'.$sval['sample_material']; ?>" />
<!--                                                            <div class="viewDelete">
                                                                <a href="<?php //echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" data-fancybox="images">
                                                                    View
                                                                </a>
                                                                <a href="<?php //echo base_url() . "customer/Request/download_projectfile/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name'] ?>"> Download
                                                                </a>
                                                                <p class="cross-btnlink">
                                                                    <a href="javascript:void(0)" class="delete_file_req" data-id="<?php //echo $data[0]['id']; ?>" data-name="<?php //echo $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                        <span>Delete</span>
                                                                    </a>
                                                                </p>
                                                            </div>-->
                                                        </div>
                                                    </li>
                                                    <?php } ?>
                                                    </ul>
                                                    </div>
                                                    <?php } ?>
                                 
                                    <div class="light-box" id="imgrmv">
                                        <h3 class="base-heading">Attachments</h3>
                                        <ul class="list-unstyled list-accessimg">
                                            <?php if(!empty($data[0]['customer_attachment'])){
                                             for ($i = 0; $i < count($data[0]['customer_attachment']); $i++) { ?>
                                                <?php
                                                
                                                $str1 = 'https://pixabay.com/';
                                                $str2 = 'https://unsplash.com/';
                                                $imageLink = $data[0]['customer_attachment'][$i]['image_link'];
                                                
                                                    if (strpos($imageLink, $str1) !== false)  {
                                                       // $imageLinkFrom =  base_url()."public/assets/img/pixabay.jpg";
                                                        $imageLinkFrom ="https://pixabay.com/favicon-32x32.png";
                                                        $imgLink = $imageLink;
                                                    }else if (strpos($imageLink, $str2) !== false) {
                                                       // $imageLinkFrom =base_url()."public/assets/img/unsplash.jpg";
                                                        $imageLinkFrom ="https://unsplash.com/favicon-32x32.png";
                                                        $imgLink = $imageLink;
                                                    }else{
                                                       $imageLinkFrom ="";
                                                       $imgLink = ""; 
                                                    }    
                                                $type = substr($data[0]['customer_attachment'][$i]['file_name'], strrpos($data[0]['customer_attachment'][$i]['file_name'], '.') + 1);
                                                if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                    ?>
                                                    <li>
                                                        <div class="accimgbx33">
                                                            
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150"/>
                                                            <p class="extension_name"><?php echo $type; ?></p>

                                                            <div class="viewDelete">
                                                                 <div class="attch-icon">
                                                                <a  data-href="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>"  class="view-file">
                                                                    <i class="fas fa-eye"></i>
                                                                </a>
                                                                <a class="img-link" href="<?php echo base_url()."designer/Request/download_projectfile/".$data[0]['id']."?imgpath=".$data[0]['customer_attachment'][$i]['file_name']?>">  <i class="fas fa-download"></i>
                                                                </a>
                                                                     
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php } else { ?>
                                                    <li>
                                                        <div class="accimgbx33">
                                                            <?php if(!empty($imgLink)){?>
                                                             <div class="img_from icon-tip"><a href="<?php echo $imgLink; ?>" target="_blank"><img src="<?php echo $imageLinkFrom;?>"></a></div>
                                                            <?php } ?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" />
                                                            <div class="viewDelete">
                                                                <div class="attch-icon">
                                                                <a  href="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" data-fancybox="images">
                                                                   <i class="fas fa-eye"></i>
                                                                </a>
                                                                <a class="img-link" href="<?php echo base_url()."designer/Request/download_projectfile/".$data[0]['id']."?imgpath=".$data[0]['customer_attachment'][$i]['file_name']?>">
                                                                    <i class="fas fa-download"></i>
                                                                </a>
                                                                    </div>
                                                                <?php if(!empty($imgLink)){?>
                                                                <div class="img_from"><a href="<?php echo $imgLink; ?>" target="_blank"><img src="<?php echo $imageLinkFrom;?>"></a></div>
                                                                <?php } ?>
                                                            </div>
                                                            
                                                        </div>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                        }else{
                                            echo "<li class='no_atchmnt'>There is no attachment.</li>";
                                        }
                                        ?>
                                    </ul>
                                </div>

                            </div>
                        </div>                                        
                    </div>

                </div>

            </div>
        </div>
        <!-- 3 tab-->
        <div data-group="1" data-loaded="" data-search-text="" class="tab-pane content-datatable datatable-width" id="activity_timeline" role="tabpanel">
            <div class="white-boundries activity-boundry">
                <div class="headerWithBtn">
                    <h2 class="main-info-heading">Activity</h2>
                    <?php if (sizeof($activities) > 0) { ?>
                        <div class="addPlusbtn">
                            <a href="javascript:void(0)" class="totl-activity" ><?php echo sizeof($activities); ?></a>
                        </div>
                    <?php } ?>
                </div>
                <?php if (!empty($activities)) { ?>
                    <ul class="activities two-can">
                        <?php foreach ($activities as $activity) { ?>
                            <li class="cap_activits">
                                <img src="<?php echo $activity['profile_picture']; ?>" data-toggle="tooltip" title="<?php echo $activity['user_fisrtname']; ?>">
                                <?php echo $activity['msg']; ?>
                                <span class="activity_dt" data-toggle="tooltip" title="<?php echo $activity['created_dt']; ?>"><?php echo $activity['created']; ?> <i class="far fa-clock"></i></span></li>
                            <?php } ?>
                        </ul>
                        <?php
                    } else {
                        echo "<div class='figimg-one no_img'><h3 class='head-b draft_no'>There is no activity</h3></div>";
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="side_bar">
            <div class="gz-sidebar">
                <div class="sidebar-header">
                    <ul id="comment-people" class="list-header-blog sidebar_style" role="tablist" style="border:none;">
                        <li class="active" id="1">
                            <a data-toggle="tab" href="#comments_tab" role="tab">
                                <i class="icon-gz_message_icon"></i> Comments 
                            </a>
                        </li>
                        <li style="width:50%">
                            <div class="right-inst">
                                <a class="vwwv" href="javascript:void(0);">
                                    <span class="vewviewer-cicls">
                                        <img src="<?php echo $data[0]['customer_image']; ?>" class="img-responsive"/>
                                    </span>
                                    <span class="hoversetss"><?php echo $data[0]['customer_name']; ?>
                                </span>
                            </a>
                            <a class="vwwv" href="javascript:void(0);">
                                <span class="vewviewer-cicls">
                                    <img src="<?php echo $data[0]['designer_image']; ?>" class="img-responsive"/>
                                </span>
                                <span class="hoversetss"><?php echo $data[0]['designer_name']; ?> (Designer)
                                </span>
                            </a>
                            <a class="vwwv" href="javascript:void(0);">
                                <span class="vewviewer-cicls">
                                    <img src="<?php echo $data[0]['qa_image']; ?>" class="img-responsive"/>
                                </span>
                                <span class="hoversetss"><?php echo $data[0]['qa_name']; ?> (QA)
                                </span>
                            </a>
                        </div>
                    </li>
                </ul>

            </div>
            <div class="tab-content">
                <div data-group="1" data-loaded="" class="tab-pane active content-datatable datatable-width" id="comments_tab" role="tabpanel">
                    <div class="sidebar-outer">
                        <div class="with-withoiut">
                            <div class="sound-signal">
                                <div class="form-radion">
                                    <input type="radio" name="chat_type" class="chattype" id="soundsignal1" checked="" value="1">
                                    <label for="soundsignal1">With Customer</label>
                                </div>
                                <div class="form-radion">
                                    <input type="radio" name="chat_type" id="soundsignal2" class="chattype" value="0">
                                    <label for="soundsignal2">Without Customer</label>
                                </div>
                            </div>
                        </div>
                        <div class="project-row-qq2 chat_box">
                            <div class="cmmtype-row">
                                <textarea class="pstcmm  text_<?php echo $data[0]['id']; ?>" Placeholder="Type a message..." wrap="hard"></textarea>
                                <?php if (($edit_profile[0]['profile_picture'] != "") || (file_exists(FS_PATH_PUBLIC_UPLOADS_PROFILE . $edit_profile[0]['profile_picture']))) { ?>
                                    <span class="cmmsend chatsendbtn send_request_chat send" data-requestid="<?php echo $data[0]['id']; ?>" data-profile_pic="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $edit_profile[0]['profile_picture']; ?>"
                                      data-senderrole="designer" 
                                      data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                                      data-receiverid="<?php echo $data[0]['customer_id']; ?>" 
                                      data-receiverrole="customer"
                                      data-sendername="<?php echo $data[0]['designer_name']; ?> " data-customerwithornot="1" onclick="message(this)"><button class="cmmsendbtn">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-chat-send.png" class="img-responsive"/></button>
                                    </span>    

                                <?php } else { ?>
                                <div class="send-attech">
                                    <span class="cmmsend chatsendbtn send_request_chat send" data-requestid="<?php echo $data[0]['id']; ?>" data-profile_pic="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png"
                                      data-senderrole="designer" 
                                      data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                                      data-receiverid="<?php echo $data[0]['customer_id']; ?>" 
                                      data-receiverrole="customer"
                                      data-sendername="<?php echo $data[0]['designer_name']; ?> " data-customerwithornot="1" onclick="message(this)"><button class="cmmsendbtn">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-chat-send.png" class="img-responsive"/></button>
                                    </span> 
                                <label for="image">
                                    <input type="file" name="shre_file[]" id="shre_file" data-withornot="1" data-reqID="<?php echo $data[0]['id']; ?>" style="display:none;" multiple onchange="uploadChatfile(event,'<?php echo $data[0]['id']; ?>','designer','<?php echo $_SESSION['user_id']; ?>','<?php echo $data[0]['customer_id']; ?>','customer','<?php echo $data[0]['designer_name']; ?>','<?php echo $data[0]['profile_picture']; ?>','designer_seen')"/>
                                    <span class="attchmnt" title="Add Files"><i class="fa fa-paperclip" aria-hidden="true"></i></span>
                                    <input type="hidden" value="" name="saved_file[]" class="delete_file"/>
                                </label>
                                <?php } ?>
                            </div>
                            </div>
                        </div></div>

                        <div class="chat-box-row" id="allChats">
                            <div class="ajax_searchload chat-align" style="display:none; text-align:center;">
                             <img src="<?php echo base_url();?>public/assets/img/customer/gz_customer_loader.gif" />
                         </div>
                            <div class="read-msg-box two-can message_container_without_cus" id="message_container_without_cus" style="padding:0px; height:320px; overflow-y: scroll;width: 99%;overflow-x: hidden; display: none;">
                                <?php
                                for ($j = 0; $j < count($chat_request_without_customer); $j++) {
                                    $data['chat_without_customer'] = $chat_request_without_customer[$j];
                                    if ($chat_request_without_customer[$j]['chat_created_datee'] != "") {
                                        ?>
                                        <div class="date_print msgdate_cre"><span class="msgk-udate t4">
                                            <?php echo $chat_request_without_customer[$j]['chat_created_datee']; ?>
                                        </span></div>
                                        <?php
                                    }
                                    if ($chat_request_without_customer[$j]['sender_id'] != $login_user_id) {
                                        $this->load->view('designer/main_left_chat_without_customer', $data);
                                    } else {
                                        $this->load->view('designer/main_right_chat_without_customer', $data);
                                    }
                                }
                                ?>
                            </div>
                            <div class="read-msg-box two-can" id="message_container" style="padding:0px; height:320px; overflow-y: scroll;width: 99%;overflow-x: hidden;">
                                <?php
                                for ($j = 0; $j < count($chat_request); $j++) {
                                    $data['chat_with_customer'] = $chat_request[$j];
                                    if ($chat_request[$j]['chat_created_datee'] != "") {
                                        ?>
                                        <div class="date_print msgdate_cre"><span class="msgk-udate t4">
                                            <?php echo $chat_request[$j]['chat_created_datee']; ?>
                                        </span></div>
                                        <?php
                                    }
                                    if ($chat_request[$j]['sender_id'] != $login_user_id) {
                                        $this->load->view('designer/main_left_chat_with_customer', $data);
                                    } else {
                                        $this->load->view('designer/main_right_chat_with_customer', $data);
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>                    
</section>
<!-- Modal -->
<div class="modal fade similar-prop nonflex" id="brand_profile" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">
                        <?php echo $branddata[0]['brand_name']; ?> </h2>
                        <div class="cross_popup" data-dismiss="modal" aria-label="Close">
                            x
                        </div>
                    </header>
                    <?php if($branddata[0]['id'] != ""){  ?>
                        <div class="displayed-pic">
                            <div class="dp-here">
                                <?php
                                $type = substr($brand_materials_files['latest_logo'], strrpos($brand_materials_files['latest_logo'], '.') + 1);
                                $allow_type = array("jpg", "jpeg", "png", "gif");
                                if (isset($brand_materials_files['latest_logo']) && in_array(strtolower($type), $allow_type)) {
                                    ?>
                                    <img src = "<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files['latest_logo']; ?>" height="150">
                                <?php } elseif (isset($brand_materials_files['latest_logo']) && !in_array(strtolower($type), $allow_type)) { ?>
                                    <img src = "<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/all-file.jpg" height="150">
                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files['latest_logo']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div>
                                <?php } else { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/logo-brand-defult.jpg" height="150"/>
                                <?php } ?>
                            </div>
                            <div class="profile_info">
                                <div class="profile colm">
                                    <div class="form-group">
                                        <div class="brand_labal">Fonts Size</div>
                                        <div class="brand_info"><?php echo $branddata[0]['fonts']; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="profile colm">
                                <div class="form-group">
                                    <div class="brand_labal">Color Preferences</div>
                                    <div class="brand_info"><?php echo $branddata[0]['color_preference']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="profile colm">
                            <div class="form-group">
                                <div class="brand_labal">Website URL </div>
                                <div class="brand_info"><?php echo $branddata[0]['website_url']; ?></div>
                            </div>
                        </div>
                        <?php if (!empty($branddata[0]['google_link'])) { ?>
                            <div class="profile colm">
                                <div class = "brand_labal">Google Link </div>
                                <div class = "brand_info">
                                    <?php
                                    $brandlinks = explode(',', $branddata[0]['google_link']);
                                    foreach ($brandlinks as $bk => $bv) {
                                        echo $bv . "<br/><br/>";
                                    }
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="profile colm">
                            <div class="form-group">
                                <div class="brand_labal">Brand Description </div>
                                <div class="brand_info discrptions two-can">
                                    <?php echo $branddata[0]['description']; ?>  </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="all-brand-outer">                  
                        <div class="profile colm materials-sp">
                            <div class = "brand_labal">Brand Logo </div>
                            <div class = "brand_info row">
                                <?php
                                for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                                    if ($brand_materials_files[$i]['file_type'] == 'logo_upload') {
                                        $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                        if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                            ?>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="bma-outer">
                                                    <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150"/>
                                                        <p class="extension_name"><?php echo $type; ?></p>
                                                    </a>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                </div></div>
                                            <?php } else { ?> 
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="bma-outer">
                                                        <img src = "<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="profile colm materials-sp">
                                <div class = "brand_labal">Market Materials </div>
                                <div class = "brand_info row">
                                    <?php
                                    for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                                        if ($brand_materials_files[$i]['file_type'] == 'materials_upload') {
                                            $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                            if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="bma-outer">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150"/>
                                                            <p class="extension_name"><?php echo $type; ?></p>
                                                        </a>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></div>
                                                </div></div>
                                            <?php } else { ?> 
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="bma-outer">
                                                        <img src = "<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="profile colm materials-sp">
                                <div class = "brand_labal">Additional Images </div>
                                <div class = "brand_info row">
                                    <?php
                                    for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                                        if ($brand_materials_files[$i]['file_type'] == 'additional_upload') {
                                            $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                            if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="bma-outer">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150"/>
                                                            <p class="extension_name"><?php echo $type; ?></p>
                                                        </a>
                                                    <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></div>
                                                </div></div>
                                            <?php } else { ?> 
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="bma-outer">
                                                        <img src = "<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a><!--a href="#"><i class="fa fa-eye" aria-hidden="true"></i><span>View</span></a--></div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade similar-prop nonflex" id="Addfiles" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="cli-ent-model-box">
                    <header class="fo-rm-header">
                        <h2 class="popup_h2 del-txt">Add File</h2>
                        <div id="close-d" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
                    </header>
                    <div class="cli-ent-model">
                        <fieldset id="selectimagespopup" style="display:block">
                        <div class="selectimagespopup">
                            <h2>Upload Draft</h2>
                            <form method="post" action="" enctype="multipart/form-data">
                                <input type="hidden" name="user-id" value="<?php echo  $login_user_id = $this->load->get_var('login_user_id');  ?>">
                                <div class="upload-draft-colm">
                                <div class="form-group goup-x1 goup_1 text-left">
                                    <label class="label-x3">Source File:</label>
                                    <p class="space-a"></p>
                                    <div class="file-drop-area">
                                        <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-cloud.png" class="img-responsive"></span>
                                        <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                        <input type="hidden" name="tab_status" value="<?php echo $matches[0]; ?>"/>
                                        <input id="sour" class="file-input dropify" data-plugin="dropify" multiple="" type="file" name="src_file" onchange="validateAndUploadS(this);" >
                                        <div class="dropify-preview" style="display: none; width: 100%;">
                                            <img src="" class="img_dropify" style="height: 150px; width: 100%;">
                                            <div class="overlay">
                                                <div class="rem_btn">
                                                    <button class="remove_selected">Remove</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                 <span id="collectionFeedback"></span>
                                 <span id="Counter"></span>

                                <div class="form-group goup-x1 goup_2  text-left">
                                    <label class="label-x3">Preview File:</label>
                                    <p class="space-a"></p>
                                    <div class="file-drop-area">
                                        <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-cloud.png" class="img-responsive"></span>
                                        <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                        <input id="preview_image" class="file-input dropify" data-plugin="dropify" multiple="" type="file" name="preview_file"  onchange="validateAndUploadP(this);" accept="image/*">
                                        <div class="dropify-preview " style="display: none; width: 100%;">
                                            <img src="" class="img_dropify preview_file" style="height: 150px; width: 100%;">
                                            <div class="overlay">
                                                <div class="rem_btn">
                                                    <button class="remove_selected">Remove</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="allowed-type"><i>Allowed file types : jpg, jpeg, png, gif</i></p>

                                </div>
                                     </div>
                                     <div id="warningError"></div>
                              <p class="btn-x">
                                  
                                     <input id="next_project" type="button" value="Next" class="load_more button" />
                              </p>
                            
                        </div>
                        </fieldset>
                        <fieldset id="project-prew-detail" style="display:none">
                            <div class="project-prew-detail">
                                <div class="row">
                                    <div class="col-md-4">
                                            <div class="design_preview">
                                                    <h2 class="main-info-heading">
                                                            design preview
                                                    </h2>
                                                    <div class="preview-image">
                                
                                                            <img  id="preview-image-src" src="">
                                                            <span class="overlay-icon" data-fancybox  href="" >
                                                            </span>
                                                    </div>
                                            </div>
                                    </div>
                                    <div class="col-md-8">
                                            <div class="project-info-popup">
                                                    <h2 class="main-info-heading">
                                                            project details
                                                    </h2>
                                                    <div class="project-info-list">
                                                            <div class="info-items">
                                                                    <h3>project title</h3>
                                                                    <div class="p_ryt_info pt-info">
                                                                    <span> </span>
                                                                    </div>
                                                            </div>
                                                            <div class="info-items">
                                                                    <h3>Color prefrence</h3>
                                                                    <div class="p_ryt_info cp-info">
                                                                    <span> </span>
                                                                    </div>
                                                            </div>
                                                            <div class="info-items">
                                                                    <h3>category</h3>
                                                                    <div class="p_ryt_info ct-info">
                                                                     <p></p>
                                                                    </div>
                                                            </div>
                                                            <div class="info-items">
                                                                    <h3>brand profile</h3>
                                                                    <div class="p_ryt_info bp-info">
                                                                     
                                                                    </div>
                                                            </div>
                                                            <div class="cstm-list-info">
                                                            <div class="p_ryt_info Extra-questions"></div>  </div>
                                                            <div class="info-items">
                                                                    <h3>Attachments</h3>
                                                                    <div class="p_ryt_info atm-info">
                                                                     
                                                                    </div>
                                                            </div>
                                                    </div>
                                            </div>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-12">
                  
                                        <?php
                                         $qustArr  = array(
                                                            'q1' =>"Did you follow all the instructions given by the customer in the product descriptions or the latest revision comments?",
                                                            'q2' =>"Is the design created according to the brand guidelines and information provided in the brand profile?",
                                                            'q3' =>"Are you satisfied with the quality of your design you are submitting? (Does it look good?)",
                                                            'q4' =>"Did you review all the attachments sent by the customer and include them, as needed?",
                                                            'q5' =>"Is the design created according to the dimensions given by the customer?",
                                                            'q6' =>"Is the design matching the color preferences given by the customer? (if provided)",
                                                            'q7' =>"Did you include all the required text?",
                                                            'q8' =>"Did you attach the correct source files as requested by the customer? ",
                                                            'q9' =>"Did you include the font files in the source folder if required?"

                                                        ); 
                                        ?>
                                            <div class="review-option-main">
                                                <div class="backToprevQuestion" style="display: none; ">
                                                <a href="javascript:void(0); "  >
                                                    <i class="fas fa-angle-left"></i>
                                                </a>
                                            </div>

                                                <?php  $lastvar=""; $i=1; foreach ($qustArr as $k => $Ques) {  
                                                    if($i ==1){
                                                        $active ="active";
                                                     }else{ 
                                                        $active ="hide";
                                                     }
                                                       
                                                     if($i == count($qustArr)){
                                                         
                                                        $lastvar = $i; 
                                                     }
                                                      
                                           
                                            ?>
                                            <div class="review-option-outer <?php echo $active; ?>" data-id="<?php echo $i; ?>">
                                                <div class="review-option">  
                                                    
                                                        <p class="">  <?php echo $Ques; ?></p>
                                                        <div class="like-dislike">
                                                           <a href="#" data-like="1" data-count="<?php echo $i; ?>" ><i class="far fa-thumbs-up"></i> Yes</a>
                                                           <a href="#" data-like="2" data-count="<?php echo $i; ?>" ><i class="far fa-thumbs-down"></i> No</a>
                                                </div>
                                            </div>


                                                
                                                    
                                         </div>

                                         <?php $i++; } ?>
                                         </div>
                                           
                                            <span id="totalQuestion" data-last="<?php echo $lastvar; ?>"></span>
                                            <div style="display:none;" class="warningMsg">
                                                 <span id="warningMsg"></span>
                                       
                                            <div class="submit-draft">
                                                 <input id="sub-button" type="submit" value="Submit" class="load_more button" />
                                                  <input id="cancel-button" type="button" value="cancel" class="button default-btn" />
                                                
                                            </div>
                                           </div>
                                        </form>
                                        
                                        
                                        <div class="back_to_draft" id="back_to_draft">
                                            <a href="javascript:void(0); "  >
                                                <i class="fas fa-angle-left"></i> back
                                            </a>
                                        </div>
                                    </div>
                                 </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <!-- User Notes Popup -->
    <div class="modal fade slide-3 model-close-button in" id="show_notepad_area" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="popup_h2 del-txt notetitle"> Notes of <span></span></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="add_notes_sec">
                        <div class="usernotes_list">

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <!-- User Notes Popup -->
    <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/customer/jquery.min.js');?>"></script>
    <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/customer/bootstrap.min.js');?>"></script>
    <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/dropify/dist/js/dropify.min.js');?>"></script>
    <script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
    <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/jquery.fancybox.min.js');?>"></script> 
    <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/jquery.mousewheel.min.js');?>"></script>
     
    <script type="text/javascript">
        $('[data-fancybox="images"]').fancybox({
            buttons: [
            'download',
            'thumbs',
            'close'
            ]
        });
        $(document).ready(function () {

            $(".read_more_btn").click(function () {
                $(".read_more").toggle();
            });
            $('#message_container .msgk-chatrow.clearfix').each(function (e) {
                if ($('figure', this).length > 0) {
                    $(this).addClass('margin_top');
                }
            });

        });
        $("#customerwithornot").on('change', function (e) {
            var optionSelected = $("option:selected", this);
            var valueSelected = this.value;
            $(".send_request_chat").attr('data-customerwithornot', valueSelected);
            if (valueSelected == 0)
            {
                $('#message_container_without_cus').show();
                $('#message_container').hide();
//                $('#message_container_without_cus').stop().animate({
//                    scrollTop: $('#message_container_without_cus')[0].scrollHeight
//                }, 1);
} else if (valueSelected == 1) {
    $('#message_container_without_cus').hide();
    $('#message_container').show();
//                $('#message_container').stop().animate({
//                    scrollTop: $('#message_container')[0].scrollHeight
//                }, 1);
}
});
        $(document).on('click', ".remove_selected", function (e) {
            e.preventDefault();
            $(this).closest('.dropify-preview').css('display', 'none');
            $(this).closest('.file-drop-area').find('input').val('');
            $(this).closest('.file-drop-area').find('span').show();
            $(this).closest('.file-drop-area').find('.file-msg').html('Drag and drop file here or <span class="nocolsl">Click Here</span>');
        });
        function validateAndUploadS(input) {
            var URL = window.URL || window.webkitURL;
            var file = input.files[0];
            var exten = $(input).val().split('.').pop();
            var imgext = ['jpg', 'jpeg', 'png', 'gif','PNG','JPG','JPEG'];

            if($("#sour").parents(".file-drop-area").hasClass("red-alert")){
                $("#sour").parents(".file-drop-area").removeClass("red-alert");
            }

            if (exten == "zip" || exten == "rar") {
                $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-archive-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                $('.goup_1 .file-drop-area .dropify-preview').show();
                $('.goup_1 .file-drop-area span').hide();
            } else if (exten == "docx" || exten == "doc") {
                $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-word-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                $('.goup_1 .file-drop-area .dropify-preview').show();
                $('.goup_1 .file-drop-area span').hide();
            } else if (exten == "pdf") {
                $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-pdf-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                $('.goup_1 .file-drop-area .dropify-preview').show();
                $('.goup_1 .file-drop-area span').hide();
            } else if (exten == "txt") {
                $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-text-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                $('.goup_1 .file-drop-area .dropify-preview').show();
                $('.goup_1 .file-drop-area span').hide();
            }
            else if (exten == "psd") {
                $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                $('.goup_1 .file-drop-area .dropify-preview').show();
                $('.goup_1 .file-drop-area span').hide();
            }
            else if (exten == "ai") {
                $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                $('.goup_1 .file-drop-area .dropify-preview').show();
                $('.goup_1 .file-drop-area span').hide();
            } else if (jQuery.inArray(exten, imgext) != '-1') {
                //console.log(URL.createObjectURL(file));
                $(".goup_1 .file-drop-area .dropify-preview").html('<img src="' + URL.createObjectURL(file) + '" class="img_dropify" style="height: 150px; width: 100%;"><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                $('.goup_1 .file-drop-area .dropify-preview').show();
                $('.goup_1 .file-drop-area span').hide();
            } else {
                $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg">You have selected <strong>' + exten + '</strong> file</h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                $('.goup_1 .file-drop-area .dropify-preview').show();
                $('.goup_1 .file-drop-area span').hide();
            }
        }
        function validateAndUploadP(input) {
            var URL = window.URL || window.webkitURL;
            var file = input.files[0];
              if($("#preview_image").parents(".file-drop-area").hasClass("red-alert")){
               $("#preview_image").parents(".file-drop-area").removeClass("red-alert"); 
            }
            if (file.type.indexOf('image/') !== 0) {
                this.value = null;
                console.log("invalid");
            }
            else {
                if (file) {
                    console.log(URL.createObjectURL(file));
                    $(".goup_2 .file-drop-area span").hide();
                    $(".goup_2 .file-drop-area .dropify-preview").css('display', 'block');
                    $(".goup_2 .img_dropify").attr('src', URL.createObjectURL(file));
                }
            }

        }
//        window.onload = function () {
//            $('#message_container').stop().animate({
//                scrollTop: $('#message_container')[0].scrollHeight
//            }, 1);
//        }
$(document).ready(function () {
    $('.sendtext').keypress(function (e) {
        if (e.which == 13) {
            $('.send').click();
            return false;
        }
    });
});
function submitOnEnter(inputElement, event) {
    if (event.keyCode == 13) {
        inputElement.form.submit();
    }
}
</script>

<script type="text/javascript">
    function message(obj) {
        var request_id = obj.getAttribute('data-requestid');
        var sender_type = obj.getAttribute('data-senderrole');
        var sender_id = obj.getAttribute('data-senderid');
        var reciever_id = obj.getAttribute('data-receiverid');
        var reciever_type = obj.getAttribute('data-receiverrole');
        var text_id = 'text_' + request_id;
        var message = $('.' + text_id).val();
        if (message.indexOf("http://") >= 0) {
            var link_http_string = (message.substr(message.indexOf("http://") + 0));
            var link = link_http_string.split(' ')[0];
        }
        if (message.indexOf("https://") >= 0) {
            var link_https_string = (message.substr(message.indexOf("https://") + 0));
            var link = link_https_string.split(' ')[0];
        }
        if (message.includes(link)) {
            message = message.replace(link, '<a target="_blank" href="' + link + '">' + link + '</a>');
        } else {
            message = message;
        }
        var verified_by_admin = "1";
        var profile_image = obj.getAttribute('data-profile_pic');
        var designer_name = obj.getAttribute('data-sendername');
        var customer_withornot = obj.getAttribute('data-customerwithornot');
        if (message != "") {
            $('.text_' + request_id).val("");
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>designer/Request/send_message_request",
                data: {"request_id": request_id,
                "sender_type": sender_type,
                "sender_id": sender_id,
                "reciever_id": reciever_id,
                "reciever_type": reciever_type,
                "message": message,
                "with_or_not_customer": customer_withornot},
                success: function (data) {
                    if (customer_withornot == 0) {
                        if ($('#message_container_without_cus > div.msgk-chatrow:last-child > div').hasClass("msgk-right") === true) {
                            $("#message_container > div.msgk-chatrow:nth-last-child(1)").find('.just_now').css('display', 'none');

                            $('#message_container_without_cus').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + 'del_' + data + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + data + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + data + '"></i><div class="open-edit open-edit_' + data + '" style="display: none;"><a href="javascript:void(0)" class="editmsg" data-editid="' + data + '">edit</a><a href="javascript:void(0)" class="deletemsg" data-delid="' + data + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + data + '" style="display:none"><form method="post" id="editMainmsg"><textarea class="pstcmm sendtext" id="edit_main_msg_' + data + '">' + message + '</textarea><a href="javascript:void(0)" class="edit_save"  data-id="' + data + '">Save</a><a href="javascript:void(0)" class="cancel_main" data-msgid="' + data + '">Cancel</a></form></div> <div class="msgk-mn msg_desc_' + data + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + data + ' edit_icon_msg" title="Edited"></span><span class="msgk-umsxxt took_' + data + '">' + message + '</span></pre></div></div></div></div>');
                        } else {
                            $('#message_container_without_cus').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + 'del_' + data + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + data + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + data + '"></i><div class="open-edit open-edit_' + data + '" style="display: none;"><a href="javascript:void(0)" class="editmsg" data-editid="' + data + '">edit</a><a href="javascript:void(0)" class="deletemsg" data-delid="' + data + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + data + '" style="display:none"><form method="post" id="editMainmsg"><textarea class="pstcmm sendtext" id="edit_main_msg_' + data + '">' + message + '</textarea><a href="javascript:void(0)" class="edit_save"  data-id="' + data + '">Save</a><a href="javascript:void(0)" class="cancel_main" data-msgid="' + data + '">Cancel</a></form></div> <div class="msgk-mn msg_desc_' + data + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + data + ' edit_icon_msg" title="Edited"></span><span class="msgk-umsxxt took_' + data + '">' + message + '</span></pre></div></div></div></div>');
                        }

//                    $('#message_container_without_cus').stop().animate({
//                        scrollTop: $('#message_container_without_cus')[0].scrollHeight
//                    }, 1);
} else if (customer_withornot == 1) {
    if ($('#message_container > div.msgk-chatrow:last-child > div').hasClass("msgk-right") === true) {
        $("#message_container > div.msgk-chatrow:nth-last-child(1)").find('.just_now').css('display', 'none');

        $('#message_container').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + 'del_' + data + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + data + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + data + '"></i><div class="open-edit open-edit_' + data + '" style="display: none;"><a href="javascript:void(0)" class="editmsg" data-editid="' + data + '">edit</a><a href="javascript:void(0)" class="deletemsg" data-delid="' + data + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + data + '" style="display:none"><form method="post" id="editMainmsg"><textarea class="pstcmm sendtext" id="edit_main_msg_' + data + '">' + message + '</textarea><a href="javascript:void(0)" class="edit_save"  data-id="' + data + '">Save</a><a href="javascript:void(0)" class="cancel_main" data-msgid="' + data + '">Cancel</a></form></div> <div class="msgk-mn msg_desc_' + data + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + data + ' edit_icon_msg" title="Edited"></span><span class="msgk-umsxxt took_' + data + '">' + message + '</span></pre></div></div></div></div>');
    } else {
        $('#message_container').prepend('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + 'del_' + data + '"><div class="time-edit"><p class="msgk-udate just_now">Just Now</p><div class="editDelete editdeletetoggle_' + data + '"><i class="fas fa-ellipsis-v openchange" data-chatid="' + data + '"></i><div class="open-edit open-edit_' + data + '" style="display: none;"><a href="javascript:void(0)" class="editmsg" data-editid="' + data + '">edit</a><a href="javascript:void(0)" class="deletemsg" data-delid="' + data + '">delete</a></div></div></div><div class="msgk-mn-edit edit_main_' + data + '" style="display:none"><form method="post" id="editMainmsg"><textarea class="pstcmm sendtext" id="edit_main_msg_' + data + '">' + message + '</textarea><a href="javascript:void(0)" class="edit_save"  data-id="' + data + '">Save</a><a href="javascript:void(0)" class="cancel_main" data-msgid="' + data + '">Cancel</a></form></div> <div class="msgk-mn msg_desc_' + data + '"><div class="msgk-umsgbox"><pre><span class="edit_icon_' + data + ' edit_icon_msg" title="Edited"></span><span class="msgk-umsxxt took_' + data + '">' + message + '</span></pre></div></div></div></div>');
    }

//                    $('#message_container').stop().animate({
//                        scrollTop: $('#message_container')[0].scrollHeight
//                    }, 1);
}

}
});
}
//            $('#message_container').stop().animate({
//                scrollTop: $('#message_container')[0].scrollHeight
//            }, 1);
}
</script>
<script>
    $('#right_nav > li').click(function () {
        $(this).toggleClass('open');
    });
 // feed back script
  $(document).ready(function(){
       $("#next_project").click(function(){

        if (jQuery('#sour').val() == '') {
             $("#sour").parents(".file-drop-area").addClass("red-alert"); 
             $('#warningError').fadeIn().css("color","red").html("Please Attach Source File");
             setTimeout(function(){ $('#warningError').fadeOut().text('');  }, 3000);
             
         }else{
             if (jQuery('#preview_image').val() == '') {
                $("#preview_image").parents(".file-drop-area").addClass("red-alert"); 
                $('#warningError').fadeIn().css("color","red").html("Please Attach Preview File");
                setTimeout(function(){ $('#warningError').fadeOut().text('');  }, 3000);
             }else{
                var project_title,color_pref,category=[],brand_profile,Attachments;
                project_title =  $(".project-title").text();
                color_pref    =  $(".color-pref").text();
                $(".category span").each(function(k,v){
                      //category.push(v);   
                        //$(".ct-info p").html(category);
                        var com = '';
                    if(k == 0){
                        com = '<p class="seprater"> > </p>'; 
                    }    
                    $(".ct-info").append("<span class="+k+">"+$.trim($(this).text())+"</span>"+com);
                });  
                brand_profile =  $(".brand-profile").html();
                if(brand_profile == " "){
                    brand_profile = "No brand profile selected"; 
                }
                var main_divForQuestion = $(".main_divForQuestion").html();
                $(".attch-icon .img-link").each(function(k,v){

                             var imageArr = ['jpeg', 'gif', 'bmp', 'png',"jpg"];
                             var url = $(this).attr("href");
                            // console.log("vs",url); 
                             var baseName = getPageName(url);
                             ext = baseName.substring(baseName.lastIndexOf('.') + 1);
                              
                             if (jQuery.inArray(ext, imageArr)!='-1') {
                                $(".atm-info").append("<div class='images-gallary' data-fancybox  href='"+$(this).attr("href")+"' ><img src='"+$(this).attr("href")+"'></div>");
                                }else {
                                    $(".atm-info").append(v); 
                                }

         
                });  

                

                $(".pt-info span").text(project_title);
                $(".cp-info span").text(color_pref);
                $(".bp-info").html(brand_profile);
                $(".Extra-questions").html(main_divForQuestion);

                $("#selectimagespopup").hide();
                $("#project-prew-detail").show(); 
                var preview_image = $(".preview_file").attr("src"); 
                $(".preview-image").find("img").attr("src",preview_image); 
                $(".overlay-icon").attr("href",preview_image); 
             }
         }
 
       });

       $("#back_to_draft a").click(function(){
        $("#project-prew-detail").hide();
        $("#selectimagespopup").show();
       }); 
       $(".like-dislike a").click(function(){
       var StroeArrA= {}; 
       var Counter = $("#Counter").attr("data-slide");
            
            var totalQuestion;
             $(this).siblings().removeClass("a-active").find("i").removeClass("fas").addClass("far");    
             $(this).addClass("a-active").find("i").removeClass("far").addClass("fas");
             var like_Check = $(this).attr("data-like"); 
             var lastCheck =  $(".review-option-outer").last().hasClass("active");

             $(".review-option-main").find(".active").removeClass("active").addClass("hide").fadeIn("slow").next(".review-option-outer").addClass("active").fadeIn("slow").removeClass("hide");
                 var quesNumber =  $(this).data("count");
                totalQuestion = $("#totalQuestion").data("last");
              StroeArrA[quesNumber]  = like_Check; 
              $("#collection-q"+quesNumber).remove(); 
              $("#collectionFeedback").append("<input type='hidden' class='updating' id='collection-q"+quesNumber+"' name='feedbackColl["+quesNumber+"]' value='"+like_Check+"'>");
              $("#Counter").attr("data-slide",quesNumber); 
              $(".backToprevQuestion").show();
               if(quesNumber == totalQuestion){   
                  $(".review-option-main").hide();  

                   var NumberArr = [];
                      $( ".updating" ).each(function( index,val ) {
                        NumberArr.push($( this ).val()); 
                    });
                        
                  
                   if(jQuery.inArray("2", NumberArr) !== -1){
                   
                    $("#warningMsg").css('color',"#fff").text("As per your answers, it seems that you have missed some instructions from customer"); 
                    $("#sub-button").removeClass("load_more").val("Submit Anyway");
                    $("#sub-button").addClass("default-btn");
                    $("#cancel-button").addClass("load_more").val("Cancel");
                    $("#cancel-button").removeClass("default-btn");
                     $(".warningMsg").fadeIn(); 
                   }else{
                    
                     $("#warningMsg").css('color',"#fff").text("As per answers, you have followed all the required instructions carefully, Approve the design now."); 
                    $("#sub-button").addClass("load_more").val("Submit");
                    $("#sub-button").removeClass("default-btn");
                    $("#cancel-button").removeClass("load_more").val("Cancel");
                    $("#cancel-button").addClass("default-btn");
                         $(".warningMsg").fadeIn(); 
                 
                   }
                    
               }
      
        });
      
    });
  
    $(".backToprevQuestion a").click(function(){
        var Counter = $("#Counter").attr("data-slide");
        var newCounter = Counter - 1; 
        $("#Counter").attr("data-slide",newCounter);
        if(Counter==1){
            $(".backToprevQuestion").hide();
        }else{
            $(".backToprevQuestion").show();
        }
        $(".review-option-main").find(".active").removeClass("active").addClass("hide").fadeIn("slow").prev(".review-option-outer").addClass("active").fadeIn("slow").removeClass("hide");
    });
    
 $("#cancel-button").click(function(){
    $("#Addfiles").modal("hide");
     location.reload(); 
 }); 


 

function getPageName(url) {
    var index = url.lastIndexOf("/") + 1;
    var filenameWithExtension = url.substr(index);
    var filename = filenameWithExtension.split(".")[0]; // <-- added this line
    return filename;                                    // <-- added this line
}
</script>

<?php
if (!empty($chat_request)) {
    $prjtid = $chat_request[0]['request_id'];
    $customerid = $chat_request[0]['reciever_id'];
    $designerid = $chat_request[0]['sender_id'];
}
?>
