<style type="text/css" media="screen">
    header.nav-section {
        display: none;
    }
    .mydivposition {
   cursor: crosshair;
   padding: 20px;
   display: inline-block;
   width: auto;
   margin: auto;
   float: none;
   transform-origin: top;
   }
   body, .project-info-page .side_bar {
   padding-top: 0px; 
   }
   
   .posone1,.posone2{
   cursor: move;
   }
   svg.leader-line {
   z-index: 999;
   /*position: absolute;*/
   }
   svg.leader-line use {
   stroke: #dc223c;
   }
   .custom-color use{
   fill:#dc223c !important;
   }
   .hidenotch:after{
      opacity: 0 !important;
   }
   .MarkasFav {
   display: inline-flex;
   padding: 0px;
   margin: 0px;
   }
   .MarkasFav .click{
   position: relative;
   }
   .MarkasFav .CoPyOrMove img {
   width: 45%;
   cursor: pointer;
   }
   div#CopYfileToFolder,div#DirectorySave {
   margin-top: 40px;
   }
   .fa-heart-o:before {
   content: "\f004";
   }
   span.cstm_span.fa-heart.fas {
   color: #f90500;
   }
   
    
   /* line Drawer Css start from herer  */
   /* line Drawer Css End here  */
   
</style>
<?php $_SESSION['designerpic'] = isset($user[0]['profile_picture']) ? $user[0]['profile_picture'] : 'user-admin.png'; ?>
<div class="dot-header">
    <span id="OpenpopUp" data-open="0"></span>
   <span id="project-pageinfo-id"></span>
    <div class="custom-container">
        <div class="flex-headers">
            <p class="savecols-col left">
                <a href="<?php echo base_url(); ?>designer/request/project_info/<?php echo $designer_file[0]['request_id']; ?>" class="back-link-xx0"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive"></a>
            </p>
            <div class="middle-tital">
                <div class="zoom-btns">
                    <a id="zoomOut" href="javascript:void(0)" data-zoomoutcount='0'><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/icon-zoom-out.svg" class="img-responsive"></a>
                    <a id="zoomIn" href="javascript:void(0)" data-count='0'><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/icon-zoom-in.svg" class="img-responsive"></a>
                </div>
                <div class="despeginate-box">
                    <a class="despage prev" href="#myCarousel" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
                    <span class="file_title"><?php echo $designer_file[0]['file_name']; ?></span>
                    <a class="despage next" href="#myCarousel" data-slide="next"><i class="fas fa-chevron-right"></i></a>
                </div>
                <div class="sidebar-toggle">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/toggle.svg" class="img-responsive">
                </div>
            </div>
            <div class="down-approv">
                <?php
                for ($i = 0; $i < sizeof($designer_file); $i++) {
                    ?>
                    <div class="hdr-buttons butt_<?php echo $designer_file[$i]['id']; ?>"
                    <?php
                    if ($main_id == $designer_file[$i]['id']) {
                        echo "style='display: block;'";
                    } else {
                        echo "style='display:none;'";
                    }
                    ?>>
                        <div class="buttons_quality">
                            <div class="status-bttn">  
                                <?php if ($designer_file[$i]['status'] == "pending") { ?>
                                    <h3 class="btn-d bluetext">Pending</h3>
                                <?php } elseif ($designer_file[$i]['status'] == "Reject") { ?>
                                    <h3 class="btn-d holdcolor rejected">Rejected</h3>
                                <?php } elseif ($designer_file[$i]['status'] == "Approve") { ?>
                                    <h3 class="btn-d green notappchanges">Approved</h3>
                                <?php } ?>
                            </div>
                            <div id="date_approved" class="approvedate  admin-del"> 
                                <div class="downld_del_sec">
                                    <form action="<?php echo base_url(); ?>/designer/request/downloadzip" method="post">
                                        <input type="hidden" name="srcfile" value="<?php echo "public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>">
                                        <input type="hidden" name="prefile" value="<?php echo "public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>">
                                        <input type="hidden" name="project_name" value="<?php echo $data[0]['title']; ?>">
                                        <button type="submit" name="submit" style="border:none"><i class="fa fa-download" aria-hidden="true"></i></button>
                                    </form>
                                    <form action="<?php echo base_url(); ?>/designer/request/gz_delete_files" method="post">
                                        <input type="hidden" name="srcfile" value="<?php echo "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>">
                                        <input type="hidden" name="draft_count" value="<?php echo $designer_file[$i]['draft_count']; ?>">
                                        <input type="hidden" name="prefile" value="<?php echo "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>">
                                        <input type="hidden" name="thumb_prefile" value="<?php echo "uploads/requests/" . $_GET['id'] . "/_thumb/" . $designer_file[$i]['file_name']; ?>">
                                        <input type="hidden" name="project_name" value="<?php echo $data[0]['title']; ?>">
                                        <input type="hidden" name="requests_files" value="<?php echo $designer_file[$i]['id']; ?>">
                                        <input type="hidden" name="request_id" value="<?php echo $_GET['id']; ?>">
                                        <button type="submit" name="gz_delete_files" onclick="return cnfrm_delete()" style="border:none"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </form>
                                </div>
                            </div>
                        </div> 
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="main-three-outer">
    <div class="left-slide-sidebar">
        <div class="pro-ject-leftbar two-can">
            <div class="orb-xbin one-can slick_thumbs">
                <ul class="list-unstyled orbxbin-list" id="thub_nails">
                    <?php for ($i = 0; $i < sizeof($designer_file); $i++) { ?>
                        <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="active1 <?php echo $designer_file[$i]['id']; ?>" id="<?php echo $designer_file[$i]['id']; ?>">
                            <figure class="imgobxbins" style="background-color:#fff;">
                                <?php
                                $type = substr($designer_file[$i]['file_name'], strrpos($designer_file[$i]['file_name'], '.') + 1);
                                ?>
                                <img src="<?php echo imageUrl($type, $_GET['id'], $designer_file[$i]['file_name']); ?>" class="img-responsive">
                            </figure>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="pro-ject-wrapper dot-comment">
        <div class="view-commentBox">
            <div class="row">
                <div class="col-md-12">
                    <div id="myCarousel" class="carousel fade-carousel carousel-fade slide" data-ride="carousel" data-interval="false">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" id="cro" >
                            <?php for ($i = 0; $i < sizeof($designer_file); $i++) { ?>
                                <div class="item slides two-can outer-post <?php
                                if ($designer_file[$i]['id'] == $main_id) {
                                    echo 'active';
                                }
                                ?>" id="<?php echo $designer_file[$i]['id']; ?>" data-name="<?php echo $designer_file[$i]['file_name']; ?>">
                                      <div class="customer_loader loading_<?php echo $designer_file[$i]['id']; ?>" style="display:none;">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/gz_customer_loader.gif">
                                     </div>
                                    <div class="col-md-12 mydivposition mydivposition_<?php echo $designer_file[$i]['id']; ?>"  style=" z-index: 0; ">
                                        <div class="gz ajax_loader loader loading_<?php echo $designer_file[$i]['id']; ?>" style="display:none;">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif">
                                        </div>
                                        <div class="slid" data-id="<?php echo $designer_file[$i]['id']; ?>">
                                            <?php
                                            $type = substr($designer_file[$i]['file_name'], strrpos($designer_file[$i]['file_name'], '.') + 1);
                                            if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                ?>
                                                <img class="img-responsive" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" style="/*max-height:calc(100vh - 80px);*/margin: auto;display: block;"/>
                                                <p class="extension_name"><?php echo $type; ?></p>
                                            <?php } else { ?>
                                                <img class="img-responsive" src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" style="/*max-height:calc(100vh - 80px);*/margin: auto;display: block;/*webkit-filter:blur(8px);filter:blur(8px);*/"/>
                                            <?php }
                                            ?>
                                        </div>
                                        <!-- Chat toolkit start-->
                                        <div class="appenddot_<?php echo $designer_file[$i]['id']; ?> remove_dot">
                                            <?php
                                            $total = array_diff(array_column($designer_file[$i]['chat'], 'xco'), ['']);
                                            $counttotaldotleft = count(array_values($total));
                                            for ($j = 0; $j < sizeof($designer_file[$i]['chat']); $j++) {
                                                if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                                    $background = "pinkbackground";
                                                } else {
                                                    $background = "bluebackground";
                                                }
                                                ?>
                                                <?php
                                                if ($designer_file[$i]['chat'][$j]['xco'] != "") {
                                                    if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                                        $image1 = "dot_image1.png";
                                                        $image2 = "dot_image2.png";
                                                    } else {
                                                        $image1 = "designer_image1.png";
                                                        $image2 = "designer_image2.png";
                                                    }
                                                    ?>
                                                    <div id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" class="abcabcabc" style="display:none;position:absolute;opacity:0;width:30px;z-index:0;height:30px;" data-dot="<?php echo $counttotaldotleft; ?>" data-left="<?php echo $designer_file[$i]['chat'][$j]['xco']; ?>" data-top="<?php echo $designer_file[$i]['chat'][$j]['yco']; ?>" onclick='show_customer_chat(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, event,$(this));'>
                                                        <div class="customer_chatimage1 beatHeart customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><span class="numberdot" id="dotnum_<?php echo $designer_file[$i]['id']; ?>"><?php echo $counttotaldotleft; ?></span></div>
                                                        <div style="display:none;" class="customer_chatimage2 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><span class="numberdot" id="dotnum_<?php echo $designer_file[$i]['id']; ?>"><?php echo $counttotaldotleft; ?></span></div> 
                                                    </div>
                                                    <?php if (is_numeric($designer_file[$i]['chat'][$j]['xco'])) { ?>
                                                        <div class="customer_chat customer_chat<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none;position:absolute;left:<?php echo $designer_file[$i]['chat'][$j]['xco'] - 3; ?>px;top:<?php echo $designer_file[$i]['chat'][$j]['yco'] + 25; ?>px;padding: 5px;border-radius: 5px;">
                                                            <div class="posone2" >
                                                                <div class="posonclick-add">
                                                                    <div class="posone2abs arrowdowns" id="dotnum_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>_Next" onclick="return avoidClick(event);">
                                                                        <a href="javascript:void(0)" onclick="return closeChatBox(event);" class="crossblink"><i class="fa fa-times"></i></a>
                                                                        <div class="comment-box-es new-patrn">
                                                                            <div class="cmmbx-col left">
                                                                                <div class="cmmbxcir">
                                                                                    <figure class="pro-circle-img-xxs">
                                                                                        <img src="<?php echo (!empty($designer_file[$i]['chat'][$j]['profile_picture'])) ? FS_PATH_PUBLIC_UPLOADS_PROFILE . $designer_file[$i]['chat'][$j]['profile_picture'] : FS_PATH_PUBLIC_UPLOADS_PROFILE . 'user-admin.png'; ?>">
                                                                                    </figure>
                                                                                </div>
                                                                                <div class="new_msg_des">
                                                                                    <div class="ryt_header">
                                                                                        <span class="cmmbxtxt">
                                                                                            <?php echo isset($designer_file[$i]['chat'][$j]['first_name']) ? $designer_file[$i]['chat'][$j]['first_name'] : $designer_file[$i]['chat'][$j]['customer_name']; ?>
                                                                                        </span>
                                                                                        <span class="kevtxt" title='<?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?>'><?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?></span>
                                                                                    </div>
                                                                                    <pre><p class="distxt-cmm"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></p></pre>
                                                                                                                                                                    </div>
                                                                                                                                                                    
                                                                                                                                                                </div>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                        <?php
                                                    }
                                                }if (is_numeric($designer_file[$i]['chat'][$j]['xco'])) {
                                                    $counttotaldotleft--;
                                                }
                                                ?>
                                            <?php } ?>
                                                                                </div>
                                                                                <div class="openchat" style="width:35%;position:absolute;display:none;padding:10px 20px; z-index: 999;">
                                                                                    <div class="posone1" id="dot-drag-<?php echo $designer_file[$i]['id']; ?>" style="z-index: 999999999999;">
                                                                                        <div class="posonclick-add">
                                                                                            <div class="posone3abs arrowdowns">
                                                                                                <div class="cmmbxpost">
                                                                                                    <p class="leave-commt">Leave a Comment</p>
                                                                                                    <input type="text" class="tff-con text_write text_<?php echo $designer_file[$i]['id']; ?>" placeholder="" onkeydown="javascript: if (event.keyCode
                                                                                                                    == 13) {
                                                                                                                $('.send_text<?php echo $designer_file[$i]['id']; ?>').click();
                                                                                                            }">
                                                                                                </div>
                                                                                                <div class="postrow12">
                                                                                                    <span class="pstcancel"><a class="mycancellable" href="javascript:void(0);">Cancel</a></span>
                                                                                                    <span class="pstbtns">
                                                                                                        <button class="pstbtns-a send_request_img_chat send_text send_text<?php echo $designer_file[$i]['id']; ?>" style="border: none;border-radius: 5px;padding: 3px 10px 4px;" data-fileid="<?php echo $designer_file[$i]['id']; ?>"
                                                                                                                data-senderrole="designer" 
                                                                                                                data-senderid="<?php echo $_SESSION['user_id']; ?>" data-designerpic="<?php echo isset($user[0]['profile_picture']) ? $user[0]['profile_picture'] : 'user-admin.png'; ?>"
                                                                                                                data-receiverid="<?php echo $request[0]['customer_id']; ?>" 
                                                                                                                data-receiverrole="customer"
                                                                                                                data-customername="<?php echo $user[0]['first_name'] . " " . $user[0]['last_name']; ?>"
                                                                                                                data-xco="" data-yco="">Post</button>
                                                                                                    </span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                            <?php } ?>
                </div>	

            </div>
        </div>		
    </div>
        </div>
</div>
    <div class="pro-ject-rightbar" id="comment_list_all">
        <?php
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['src_file'] = str_replace(' ', '+', $designer_file[$i]['src_file']);
            $designer_file[$i]['file_name'] = str_replace(' ', '+', $designer_file[$i]['file_name']);
            ?>
                                <div class="comment_sec comment-sec-<?php echo $designer_file[$i]['id']; ?>" id="<?php echo $designer_file[$i]['id']; ?>" 
            <?php
            if ($main_id == $designer_file[$i]['id']) {
                echo "style='display: block;'";
            } else {
                echo "style='display:none;'";
            }
            ?>>
        <div class=""> 
        <div class="cmmtype-box">
                <form action="" method="post">
                    <div class="cmmtype-row">
                        <textarea class="pstcmm sendtext abc_<?php echo $designer_file[$i]['id']; ?>"  Placeholder="Post Comment" id="sendcom"  autocomplete="off" data-reqid="<?php echo $designer_file[$i]['id']; ?>"></textarea>
                        <div class="send-attech">
                            <label for="image">
                                <input type="file" name="shre_main_draft_file[]" id="shre_file_<?php echo $designer_file[$i]['id']; ?>" class="shre_file" data-withornot="1" data-draftid="<?php echo $designer_file[$i]['id']; ?>" style="display:none;" multiple onchange="uploadDraftchatfile(event, 'designer', '<?php echo $request[0]['id']; ?>', '<?php echo $_SESSION['user_id']; ?>', 'customer', '<?php echo $request[0]['customer_id']; ?>', '<?php echo $_SESSION['first_name']; ?>', '<?php echo $request[0]['profile_picture']; ?>', '0', 'designer_seen')"/>
                                <span class="darftattchmnt" data-draft-id="<?php echo $designer_file[$i]['id']; ?>" title="Add Files"><i class="fa fa-paperclip" aria-hidden="true"></i></span>
                                <input type="hidden" value="" name="saved_draft_file[]" class="delete_file"/>
                            </label> 
                        <span class="cmmsend">
                            <button type="button" class="cmmsendbtn send_comment_without_img send_<?php echo $designer_file[$i]['id']; ?>" id="send_comment" onclick="send_comment_without_img('<?php echo $designer_file[$i]['id'] ?>', 'designer', '0', '', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>')">
                               <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-chat-send.png" class="img-responsive">
                           </button>
                        </span>
                       </div>
                    </div>
                </form> 
            </div>
            <div class="ajax_searchload chat-align" style="display:none; text-align:center;">
                <img src="<?php echo base_url();?>public/assets/img/customer/gz_customer_loader.gif" />
            </div>
            <ul class="list-unstyled list-comment-es comment_list_view one-can messagediv_<?php echo $designer_file[$i]['id']; ?>" id="comment_list">
                <?php
                $total = array_diff(array_column($designer_file[$i]['chat'], 'xco'), ['']);
                $counttotaldots = count(array_values($total));
                for ($j = 0; $j < sizeof($designer_file[$i]['chat']); $j++) { ?>
                <?php if ($designer_file[$i]['chat'][$j]['xco'] != "NaN" && $designer_file[$i]['chat'][$j]['yco'] != "NaN") { ?>
                <div class="comment-box-es new-patrn">
                        <div class="comments_link" id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>">
                            <div class="cmmbx-row">
                            <div class="all-sub-outer"> 
                            <div class="cmmbx-col left ">
                                <div class="cmmbxcir">
                                    <figure class="pro-circle-img-xxs">
                                         <img src="<?php echo (!empty($designer_file[$i]['chat'][$j]['profile_picture'])) ? FS_PATH_PUBLIC_UPLOADS_PROFILE . $designer_file[$i]['chat'][$j]['profile_picture'] : FS_PATH_PUBLIC_UPLOADS_PROFILE . 'user-admin.png'; ?>">
                                    </figure>
                <?php
                // echo "<pre/>";print_R($designer_file);
                if ($designer_file[$i]['chat'][$j]['xco'] != "" && $designer_file[$i]['chat'][$j]['yco'] != "") {
                if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                $background_color = "#f93c55";
                } elseif ($designer_file[$i]['chat'][$j]['sender_role'] == "designer") {
                $background_color = "#1a3147";
                }
                ?>
                <span class="cmmbxonline" onclick="show_customer_chat(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, event,$(this))">
                  <span class="numberdot" id="dotnum_<?php echo $designer_file[$i]['id']; ?>"><?php echo $counttotaldots; ?></span>
                </span>
                <?php } ?>
                </div>
                <div class="cmmbx-col">
                    <div class="ryt_header">
                <span class="cmmbxtxt"><?php echo isset($designer_file[$i]['chat'][$j]['first_name']) ? $designer_file[$i]['chat'][$j]['first_name'] : $designer_file[$i]['chat'][$j]['customer_name']; ?></span>
                <span class="kevtxt" title='<?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?>'><?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?>
                </span>
            </div>
            <?php if($designer_file[$i]['chat'][$j]['is_filetype'] != 1){ ?>
                <pre><p class="distxt-cmm distxt-cmm_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></p></pre>
            <?php } else{
            $type = strtolower(substr($designer_file[$i]['chat'][$j]['message'], strrpos($designer_file[$i]['chat'][$j]['message'], '.') + 1));
            $updateurl = '';
            if (in_array($type, DOCARROFFICE)) {
                $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                $class = "doc-type-file";
                $exthtml = '<span class="file-ext">' . $type . '</span>';
            } elseif (in_array($type, FONT_FILE)) {
                $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                $class = "doc-type-file";
                $exthtml = '<span class="file-ext">' . $type . '</span>';
            } elseif (in_array($type, ZIPARR)) {
                $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-zip.svg';
                $class = "doc-type-zip";
                $exthtml = '<span class="file-ext">' . $type . '</span>';
            } elseif (in_array($type, IMAGEARR)) {
                $imgVar = imageRequestDraftchatUrl($type, $designer_file[0]['request_id'] . '/' .$designer_file[$i]['chat'][$j]['request_file_id'], $designer_file[$i]['chat'][$j]['message']);
                $updateurl = '/_thumb';
                $class = "doc-type-image";
                $exthtml = '';
            }
            $basename = $designer_file[$i]['chat'][$j]['message'];
            $basename = strlen($basename) > 20 ? substr($basename, 0, 20) . "..." . $type : $basename;
            $data_src = FS_PATH_PUBLIC_UPLOADS . 'requestdraftmainchat/' . $designer_file[0]['request_id'] . '/' .$designer_file[$i]['chat'][$j]['request_file_id'].'/'. $designer_file[$i]['chat'][$j]['message'];?>
           <span class="msgk-umsxxt took_<?php echo $chatdata['id']; ?>">
                <div class="contain-info <?php echo (!in_array($type, IMAGEARR)) ? 'only_file_class' : ''; ?>" >
                    <a class="open-file-chat <?php echo $class; ?>" data-ext="<?php echo $type; ?>" data-src="<?php echo $data_src; ?>">
                        <img src="<?php echo $imgVar; ?>"> <?php echo $exthtml; ?>
                    </a>
                    <div class="download-file">
                        <div class="file-name">
                            <h4><b><?php echo $basename; ?></b></h4>
                        </div>
                        <a href="<?php echo base_url() ?>customer/request/download_stuffFromChat?ext=<?php echo $type; ?>&file-upload=<?php echo $data_src; ?>" target="_blank"> 
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/dwnlod-img.svg" />
                        </a>
                    </div>
                </div>
            </span>
            <?php } ?>   
            <p class="space-b"></p>
            <div class="edit_del">
                <?php if ($designer_file[$i]['chat'][$j]['xco'] == '') { ?>
                    <a href="javascript:void(0)" onclick="replyButton(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, false)" class="reply_to_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><i class="fas fa-reply-all"></i></a>
                <?php } ?>
                <?php if ($_SESSION['user_id'] == $designer_file[$i]['chat'][$j]['sender_id']) { ?>    
                <?php if($designer_file[$i]['chat'][$j]['is_filetype'] != 1){ ?>
                <a href="javascript:void(0)" onclick='editMessages(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, false)'  class="edit_to_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><i class="fas fa-pen"></i></a> 
                <?php } ?>
                <a href="javascript:void(0)" onclick="deleteMsg(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>);" class="del_msg"><i class="fas fa-trash"></i></a>                                                                               
                <?php } ?> 
            </div> 
            <div class="msgk-mn-edit comment-submit_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none">
                <form action="" method="post">
                    <textarea class="pstcmm sendtext" id="editdot_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, 'left')" data-id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>">Save</a> <a href="javascript:void(0)" onclick="cancelButton(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>)" class="cancel">Cancel</a>
                </form>
            </div>
                </div>
            </div>
            <div class="closeoncross reply_msg_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none">
                <form action="" method="post">
                    <div class="cmmtype-row wide_text" onclick="wideText(this)">
                        <textarea class="pstcmm sendtext text1_<?php echo $designer_file[$i]['id']; ?>" id="text1_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" Placeholder="Post Comment" autocomplete="off" data-reqid="<?php echo $designer_file[$i]['id']; ?>"></textarea>
                        <span class="cmmsend">
                            <a class="cmmsendbtn send_comment_without_img send_<?php echo $designer_file[$i]['id']; ?>"
                                onclick="send_comment_without_img('<?php echo $designer_file[$i]['id'] ?>', 'customer', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>', 'leftreply', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>');">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive">
                            </a>
                        </span>
                    </div>
                </form> 
            </div>
            <div class="allreplyy two-can">
                <?php foreach ($designer_file[$i]['chat'][$j]['replies'] as $key => $val) { ?>
                    <div class="cmmbx-col left reply clearfix reply_<?php echo $val['id']; ?>">
                        <div class="cmmbxcir">
                            <figure class="pro-circle-img-xxs t5">
                                <img src="<?php echo (!empty($val['profile_picture'])) ? FS_PATH_PUBLIC_UPLOADS_PROFILE . $val['profile_picture'] : FS_PATH_PUBLIC_UPLOADS_PROFILE . 'user-admin.png'; ?>">
                            </figure>
                        </div>
                        <div class="msg_sect">
                            <div class="name_edit_del">
                            <span class="cmmbxtxt">
                                <?php echo isset($val['first_name']) ? $val['first_name'] : $val['customer_name']; ?>
                            </span>
                            <?php $currentTime = new DateTime(date("Y-m-d h:i:s")); ?>
                            <div class="msgk-mn-edit comment-submit_ryt_<?php echo $val['id']; ?>" style="display:none">
                                <form action="" method="post">
                                    <textarea class="pstcmm sendtext" id="editryt_<?php echo $val['id']; ?>"><?php echo $val['message']; ?></textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(<?php echo $val['id']; ?>, 'right')" data-id="<?php echo $val['id']; ?>">Save</a> <a href="javascript:void(0)" onclick="cancelButton(<?php echo $val['id']; ?>)" class="cancel">Cancel</a>
                                </form>
                            </div>
                        </div>
                    <pre><p class="distxt-cmm distxt-cmm_ryt_<?php echo $val['id']; ?>"><?php echo $val['message']; ?></p></pre>
                    <p class="space-b"></p>
                <?php if ($_SESSION['user_id'] == $val['sender_id']) { ?>
                    <div class="edit_del"> 
                        <a href="javascript:void(0)" onclick='editMessages(<?php echo $val['id']; ?>, true)' class="edit_to_ryt_<?php echo $val['id']; ?>"><i class="fas fa-pencil-alt"></i></a>
                        <a href="javascript:void(0)" onclick="deleteMsg(<?php echo $val['id']; ?>);" class="del_msg"><i class="far fa-trash-alt"></i></a>                                                                               
                    </div>
                <?php } ?>
                    </div>
                </div>
                <?php } ?>
                </div>
                </div>
                </div>
                </div>
             </div>
                </li>
                <?php
                } if (is_numeric($designer_file[$i]['chat'][$j]['xco'])) {
                $counttotaldots--;
                }
                ?>
                <?php } ?>
                </ul>
                </div>
                </div>
        <?php } ?>
    </div>
</div>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/designer/jquery.min.js');?>"></script>
<script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'js/designer/bootstrap.min.js');?>"></script>
<script type="text/javascript">
var $rowperpage = '';
var $assets_path = '<?php echo FS_PATH_PUBLIC_ASSETS; ?>';
var requestmainchatimg = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTMAINCHATFILES; ?>";
var requestdraftchatimg = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES; ?>";
var $customer_name = '<?php echo $_SESSION['first_name'] ?>';
var $designer_img = '<?php echo $_SESSION['designerpic'] ?>';
var profile_path = "<?php  echo FS_PATH_PUBLIC_UPLOADS_PROFILE ?>";
$('#myCarousel').carousel({
    interval: false,
    cycle: true
});

var total_design1 = <?php echo sizeof($designer_file); ?>;
var ind_id = $('.<?php echo $main_id; ?>').index();
$(".pagin-one").html(ind_id + 1 + " of " + total_design1);

$(document).ready(function () {
    $('#thub_nails li figure').removeClass('active');
    $('#thub_nails .<?php echo $main_id; ?> figure').addClass('active')
    // Slick Slider script Start here 
    $('#changepass').click(function (e) {
        e.preventDefault();
    });
    $(".next").click(function () {
        setTimeout(function () {
            var chat_id = $('#cro .active').attr('id');
            $('.comment_sec').css('display', 'none');
            $('#comment_list_all #' + chat_id).css('display', 'block');
        }, 1000);
    });

    $(".prev").click(function () {
        setTimeout(function () {
            var chat_id = $('#cro .active').attr('id');
            $('.comment_sec').css('display', 'none');
            $('#comment_list_all #' + chat_id).css('display', 'block');
        }, 1000);
    });
});

$(document).on('click', '.slick_thumbs ul li', function (e) {
    var dataId = $(this).attr('id');
    var Outerdivwidth = $('.outer-post').width();
    var Outerdivheight = $('.outer-post').height();
    var outer_main = $('.mydivposition_' + dataId);
    var main_img = $(this).find('img');
    var actualimgWidth = main_img.get(0).naturalWidth;
    var actualimgHeight = main_img.get(0).naturalHeight;
    var outer_main_height = outer_main.height();
    var calcval = (Outerdivheight - outer_main_height) / 2;
    if (Outerdivheight > outer_main_height) {
        $(outer_main).css('margin-top', calcval + "px");
    }
    if (actualimgWidth > Outerdivwidth || actualimgHeight > Outerdivheight) {
        $('#zoomIn').css('cursor', 'pointer');
        $('#zoomIn').css('opacity', '1');
        $('#zoomOut').prop('disabled', false);
        $('#zoomIn').prop('disabled', false);
    } else {
        $('#zoomOut').css('cursor', 'no-drop');
        $('#zoomOut').css('opacity', '0.5');
        $('#zoomIn').css('cursor', 'no-drop');
        $('#zoomIn').css('opacity', '0.5');
        $('#zoomOut').prop('disabled', true);
        $('#zoomIn').prop('disabled', true);
    }
    $('#zoomOut').attr('data-zoomoutcount', '0');
    $('#zoomIn').attr('data-count', '0');
    $('.slick_thumbs ul li').css('pointer-events', 'none');
    if (dataId) {
        $('.hdr-buttons').css('display', 'none')
        $('.butt_' + dataId).css('display', 'block');
    }
    var url = $(this).find('img').attr('src');
    var parts = url.split("/");
    var last_part = parts[parts.length - 1];
    $('.file_title').html(last_part);
    $('.abcabcabc').css("display", "none");
    $('.customer_loader').css('display','block');
$('.slid').css('display','none');
    setTimeout(function () {
        $('.loading_' + dataId).css("display", "none");
        $('.customer_loader').css('display','none');
$('.slid').css('display','block');
    $('.slick_thumbs ul li').css('pointer-events', 'auto');
        $('.abcabcabc').css("display", "block");
        refreshDotsActiveTab();
    }, 2000);
    $(".slick_thumbs ul li").find('figure').removeClass('active');
    $(this).find('figure').addClass('active');
    var thumb_index = $(this).index();
    $('ul.slick-dots').find('li:eq(' + thumb_index + ')').trigger('click');
    var dot_length = $('ul.slick-dots').children().length;
    var count_slid = thumb_index;
    var current_design = count_slid;
    var total_design = <?php echo sizeof($designer_file); ?>;
    current_design += 1;
    if (current_design > total_design) {
        current_design = 1;
    }
    $(".pagin-one").html(current_design + " of " + total_design);
});

$(document).on('click', "#comment_list li", function (event) {
    var clicked_id = $(this).find('.comments_link').attr('id');
    var topPosition = $("#" + clicked_id + "").css('top');
    var newtopPosition = topPosition.substring(0, topPosition.length - 2); //string 800
    newtopPosition = parseFloat(newtopPosition) || 0;
    var position = $("#" + clicked_id + "").offset();
    var posX = position.left;
    var posR = ($(window).width() + $(window).scrollLeft()) - (position.left + $('#whatever').outerWidth(true));
    if (newtopPosition < 200) {
        $(".customer_chat").hide();
        $('.customer_chat' + clicked_id + ' .posone2abs').addClass('topposition');
    } else {
        $(".customer_chat").hide();
        $('.customer_chat' + clicked_id + ' .posone2abs').removeClass('topposition');
    }
    if (event.pageX - posR > 450) {
        $('.customer_chat' + clicked_id + ' .posone2abs').addClass('leftposition');
    } else {
        $('.customer_chat' + clicked_id + ' .posone2abs').removeClass('leftposition');
    }
    if (newtopPosition > 500) {
        $(".customer_chat").hide();
        $(".customer_chatimage2").hide();
        $(".customer_chatimage1").show();
        $(".customer_chatimage2.customer_chatimage" + clicked_id).show();
        $(".customer_chatimage1.customer_chatimage" + clicked_id).hide();
        $(".customer_chat" + clicked_id).toggle();
        $('html, body').animate({
            scrollTop: $(".customer_chat" + clicked_id + " .arrowdowns").offset().top
        }, 100);
    } else {
        $(".customer_chat").hide();
        $(".customer_chatimage2").hide();
        $(".customer_chatimage1").show();
        $(".customer_chatimage2.customer_chatimage" + clicked_id).show();
        $(".customer_chatimage1.customer_chatimage" + clicked_id).hide();
        $(".customer_chat" + clicked_id).toggle();
    }

});
function showclicktocomment(event, id) {
    if ($(window).width() <= '767') {
        return false;
    }
    var position = $("#mydivposition_" + id).offset();
    var posX = position.left;
    var posY = position.top;
    var left = event.pageX - posX;
    var top = event.pageY - posY + +10;
    var window_height = $(window).height();
    if (top > window_height - 100) {
        top = top - 40;
    }
    if ($(event.target).is(".posone2abs") || $(event.target).parents('div.posone2abs').length
            || $(event.target).is(".customer_chatimage1") || $(event.target).is(".customer_chatimage2")) {
        return false;
    }
    $('.showclicktocomment').remove();
    if ($(".openchat").css("display") != "block") {
        $('.openchat').after('<div class="showclicktocomment" style="z-index: 2147483647; color: rgb(255, 255, 255); background: rgb(0, 194, 121); padding: 5px; font-weight: 600; position: absolute; left: ' + left + 'px; top: ' + top + 'px; display: none; font-family:GothamPro;">\Click To Comment\</div>');
        $("#mydivposition_" + id + ' .showclicktocomment').show();
        return false;
    }

}
function closeChatBox(e) {
    $(".leader-line").remove(); 
     $("#OpenpopUp").attr("data-open",0);
    $('.customer_chat').hide();
    e.preventDefault();
    e.stopPropagation();
    return false;
}
function avoidClick(e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
}
function show_customer_chat(id, event,click) {
//    debugger;
    $this = click; 
    var datadot=  $this.attr("data-dot"); 
    $(".arrowdowns").removeClass("hidenotch");
    if(datadot > 0 || $this.find("span").hasClass("numberdot")){
        $("#OpenpopUp").attr("data-open", "2");
    }
    $(".posone1").removeAttr("style");
    $(".posone2abs").removeAttr("style");
    $(".leader-line").remove();

    var left_cord = $('#' + id).css('left');
    var top_cord = $('#' + id).css('top');
//                                            console.log("left_cord", left_cord);
//                                            console.log("top_cord", top_cord);
    $('.customer_chat' + id).css('left', left_cord);
    $('.customer_chat' + id).css('top', top_cord);
    var topPosition = $("#" + id + "").css('top');
    var newtopPosition = parseFloat(topPosition.substring(0, topPosition.length - 2)) || 0; //string 800
    
    if (newtopPosition < 200) {
        $('.customer_chat' + id + ' .posone2abs').addClass('topposition');
    } else {
        //$('.customer_chat' + id + ' .posone2abs').removeClass('topposition');
    }
    setTimeout(function () {
        $('.myallremove').remove();
        $(".openchat").hide();
        $(".customer_chat").each(function () {
            if ($(this).html() == "<label></label>") {
                $(this).remove();
            }
            if ($(this).text() == "") {
                $(this).remove();
            }
        });
        $('.showclicktocomment').remove();
        $('.mycancel').remove();
        event.preventDefault();
    }, 1);

$(".customer_chat" + id).show().siblings('.customer_chat').hide();
var Attrid = $("#project-pageinfo-id").attr("data-id"); 
    DrawALine(id,"dotnum_"+id+"_Next");
return false;
}

$(document).ready(function () {
     $('.customer_loader').css('display','block');
$('.slid').css('display','none');
    if ($(window).width() <= '767') {
        $('.remove_dot').css('display', 'none');
    }
    setTimeout(function () {
        $('#thub_nails .<?php echo $main_id; ?>').click();
    }, 2);
    $('#thub_nails li').click(function () {
        var id = $(this).attr('id');
        $(".comment_sec").hide();
        $('.openchat').hide();
        $(".pro-ject-rightbar").find("#" + id).show();
    });
    var total_design = <?php echo sizeof($designer_file); ?>;
    $(".next").click(function () {
        var index = $('#thub_nails li figure.active').closest('.active1').index();
        if (index + 1 == total_design) {
            $('#thub_nails li:first-child').click();
        } else {
            $('#thub_nails li figure.active').closest('.active1').next().click();
        }
    });
    $(".prev").click(function () {
        var index = $('#thub_nails li figure.active').closest('.active1').index();
        if (index == 0) {
            $('#thub_nails li:last-child').click();
        } else {
            $('#thub_nails li figure.active').closest('.active1').prev().click();
        }
    });
});


function send_comment_without_img(request_id, reciever_type, parent_id, replyfrom, requstfileid) {
    //console.log(parent_id);
    var sender_type = '<?php echo $_SESSION['role'] ?>';
    var sender_id = '<?php echo $_SESSION['user_id'] ?>';
    var reciever_id = '<?php echo $request[0]['customer_id']; ?>';
    if (parent_id == 0) {
        var text_id = '.abc_' + request_id;
    } else if (replyfrom == 'rightreply') {
        //console.log(requstfileid);
        var text_id = '#text2_' + requstfileid;
    } else if (replyfrom == 'appendleft') {
        //console.log('appendleft');
        var text_id = '#text3_' + requstfileid;
    } else {
        var text_id = '#text1_' + requstfileid;
    }
    var message = $(text_id).val();
    var verified_by_admin = "1";
    var customer_name = '<?php echo $_SESSION['first_name'] ?>';
    var designer_img = '<?php echo $_SESSION['designerpic'] ?>';
    if (message != "") {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>designer/request/send_message",
            data: {"request_file_id": request_id,
                "sender_role": sender_type,
                "sender_id": sender_id,
                "receiver_role": reciever_type,
                "receiver_id": reciever_id,
                "message": message,
                "customer_name": customer_name,
                "parent_id": parent_id,
                "designer_seen": 1,
            },
            success: function (data) {
                //console.log(data);
                var left = 'left';
                var right = 'right';
                var appendright = 'rightreply';
                $('.sendtext').val("");
                if (parent_id == 0) {
                    $(".messagediv_" + request_id + "").prepend('<li><div class="comment-box-es new-patrn"><div class="comments_link"><div class="cmmbx-row"><div class="all-sub-outer"><div class="cmmbx-col left "><div class="clickable" id=' + data + '><div class="cmmbxcir"><figure class="pro-circle-img-xxs"><img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>' + designer_img + '" class="mCS_img_loaded"></figure></div><div class="cmmbx-col"><div class="ryt_header"><span class="cmmbxtxt">' + customer_name + ' </span><span class="kevtxt">Just Now</span></div><pre><p class="distxt-cmm distxt-cmm_ryt_' + data + '">' + message + ' </p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="replyButton(' + data + ', true)" class="reply_to_' + data + '"><i class="fas fa-reply-all"></i></a><a href="javascript:void(0)" onclick="editMessages(' + data + ', true)" class="edit_to_' + data + '"><i class="fas fa-pencil-alt"></i></a><a href="javascript:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="fas fa-trash"></i></a></div></div></div><div class="msgk-mn-edit comment-submit_ryt_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editryt_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + right + '\')" data-id="' + data + '">Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div><div class="closeoncross reply_msg_ryt_' + data + '" style="display:none;"><form action="" method="post"><div class="cmmtype-row" onclick="wideText(this)"><textarea class="pstcmm sendtext text1_' + request_id + '" id="text2_' + data + '" placeholder="Post Comment" autocomplete="off" data-reqid="' + request_id + '"></textarea><span class="cmmsend"></span><a class="cmmsendbtn send_comment_without_img send_' + request_id + '" onclick="send_comment_without_img(' + request_id + ',\'' + sender_type + '\',\'' + data + '\',\'' + appendright + '\',\'' + data + '\');"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive"></a></div></form></div></div></div></div></div></li>');
                } else {
                    $('.reply_msg_' + parent_id).before('<div class="cmmbx-col left reply reply_' + data + '"><div class="cmmbxcir"><figure class="pro-circle-img-xxs t5"><img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>' + designer_img + '" class="mCS_img_loaded"></figure></div><div class="msg_sect"><div class="name_edit_del"><span class="cmmbxtxt">' + customer_name + '</span><pre><p class="distxt-cmm distxt-cmm_ryt_' + data + '">' + message + '</p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="editMessages(' + data + ', false)" class="edit_to_' + data + '"><i class="fas fa-pencil-alt"></i></a><a href="javascript:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="fas fa-trash"></i></a></div><div class="msgk-mn-edit comment-submit_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editdot_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + left + '\')" data-id=' + data + '>Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div></div></div>');
                    $('.reply_msg_ryt_' + parent_id).before('<div class="cmmbx-col left reply reply_' + data + '"><div class="cmmbxcir"><figure class="pro-circle-img-xxs t5"><img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>' + designer_img + '" class="mCS_img_loaded"></figure></div><div class="msg_sect"><div class="name_edit_del"><span class="cmmbxtxt">' + customer_name + '</span><pre><p class="distxt-cmm distxt-cmm_ryt_' + data + '">' + message + '</p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="editMessages(' + data + ', false)" class="edit_to_' + data + '"><i class="fas fa-pencil-alt"></i></a><a href="javascript:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="fas fa-trash"></i></a></div><div class="msgk-mn-edit comment-submit_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editdot_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + left + '\')" data-id=' + data + '>Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div></div></div>');
                    //$('.dottt_' + parent_id).append('<div class="cmmbx-col left"><div class="cmmbxcir"><figure class="pro-circle-img-xxs t5"><img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>' + designer_img + '" class="mCS_img_loaded"></figure></div><div class="msg_sect"><div class="name_edit_del"><span class="cmmbxtxt">' + customer_name + '</span><pre><p class="distxt-cmm_ryt_' + data + '">' + message + '</p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="editMessages(' + data + ', false)" class="edit_to_' + data + '"><i class="fas fa-pencil-alt"></i></a><a href="javascrip:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="fas fa-trash"></i></a></div><div class="comment-submit_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editdot_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + left + '\')" data-id=' + data + '>Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div></div></div>');
                    $('.reply_msg_ryt_' + parent_id).css('display', 'none');
                    $('.reply_to_ryt_' + parent_id).css('display', 'inline-block');
                }
            },
        });
    }
}

$('.send_request_img_chat').click(function (event) {
    event.stopPropagation();
    var request_file_id = $(this).attr("data-fileid");
    var sender_role = $(this).attr("data-senderrole");
    var sender_id = $(this).attr("data-senderid");
    var receiver_role = $(this).attr("data-receiverrole");
    var receiver_id = $(this).attr("data-receiverid");
    var text_id = 'text_' + request_file_id;
    var message = $('.' + text_id).val();
    var customer_name = $(this).attr("data-customername");
    var xco = $(this).attr("data-xco");
    var yco = $(this).attr("data-yco");
    var designer_img = $(this).data("designerpic");
    if (message != "") {
        $(this).prop('disabled', true);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>designer/request/send_message",
            data: {"request_file_id": request_file_id,
                "sender_role": sender_role,
                "sender_id": sender_id,
                "receiver_role": receiver_role,
                "receiver_id": receiver_id,
                "message": message,
                "customer_name": customer_name,
                "xco": xco,
                "yco": yco,
                "designer_seen": 1,
            },
            success: function (data) {
                $('.send_comment_without_img').prop('disabled', false);
                // alert("---"+data);
                //alert("Settings has been updated successfully.");
                $('.text_' + request_file_id).val("");
                $(".messagediv_" + request_file_id).append('<li><div class="comment-box-es new-patrn"><div class="cmmbx-row comments_link" id="' + data + '"><div class="cmmbx-col left"><div class="cmmbxcir"><figure class="pro-circle-img-xxs"><img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>' + designer_img + '" class="mCS_img_loaded"></figure><span class="cmmbxonline" style="background-color: #1a3147;"></span></div><div class="cmmbx-col"><span class="cmmbxtxt">' + customer_name + ' </span><span class="kevtxt">Just Now</span></div><pre><p class="distxt-cmm">' + message + '</p></pre></div></div><p class="space-b"></p></div></li>');
                $('.appenddot_' + request_file_id).append('<div id="' + data + '" class="" style="position:absolute;left:' + (xco - 10) + 'px;top:' + (yco - 10) + 'px;width:30px;z-index:99999999;height:30px;" onclick="show_customer_chat(' + data + ', event,$(this));"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer_image1.png" class="customer_chatimage1 customer_chatimage101" style=" display: none;"><img style="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer_image2.png" class="customer_chatimage2 customer_chatimage101"></div>');
                $('.appenddot_' + request_file_id).append('<div class="customer_chat customer_chat' + data + '" style="position: absolute; left: ' + (xco - 10) + 'px; top: ' + (yco - 10) + 'px; padding: 5px; border-radius: 5px; display: none;"><div class="posone2" style="z-index: 99999999999999999999999;top:0px; "><div class="posonclick-add"><div class="posone2abs arrowdowns"><div class="comment-box-es new-patrn"><div class="cmmbx-col left"><div class="cmmbxcir"><figure class="pro-circle-img-xxs"><img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>' + designer_img + '"></figure></div><span class="cmmbxtxt">' + customer_name + '</span></div><p class="space-a"></p><pre><p class="distxt-cmm">' + message + '</p></pre></div></div></div></div></div>');
                $(".mycancel").remove();
                //$(".messagediv_<?php echo $main_id; ?>").stop().animate({
                //scrollTop: $(".messagediv_<?php echo $main_id; ?>")[0].scrollHeight
                //}, 1000);
            }
        });

        $('.openchat').hide();
    }
});

$('.mycancellable').click(function (event) {
    event.stopPropagation();
    $('.openchat').hide();
    var mycancel = $(this).attr('data-cancel');
    $('.mydiv' + mycancel).hide();
});

function mycancellable(id)
{
    $('.openchat').hide();
    var mycancel = $(".mycancellable" + id).attr('data-cancel');
    $('.mydiv' + mycancel).hide();
}
$('.text_write').click(function (event) {
    event.stopPropagation();
});

function EditClick(data_id, flag) {
    if (flag == 'left') {
        var updated_msg = $('#editdot_' + data_id).val();
    } else {
        var updated_msg = $('#editryt_' + data_id).val();
    }
    $.ajax({
        type: "POST",
        dataType: "JSON",
        url: "<?php echo base_url(); ?>admin/dashboard/editComment",
        data: {"request_file_id": data_id,
            "updated_msg": updated_msg,
        },
        success: function (data) {
            if (flag == 'left') {
                $('.distxt-cmm_' + data_id).css('display', 'block');
                $('.distxt-cmm_' + data_id).text(data.message);
                $('.distxt-cmm_ryt_' + data_id).text(data.message);
                $('.comment-submit_' + data_id).css('display', 'none');
                $('.edit_to_' + data_id).css('display', 'inline-block');
            } else {
                $('.distxt-cmm_' + data_id).text(data.message);
                $('.distxt-cmm_ryt_' + data_id).css('display', 'block');
                $('.distxt-cmm_ryt_' + data_id).text(data.message);
                $('.edit_to_ryt_' + data_id).css('display', 'inline-block');
                $('.comment-submit_ryt_' + data_id).css('display', 'none');
            }
        },
    });
}

function cancelButton(cancelid) {
    $('.comment-submit_' + cancelid).css('display', 'none');
    $('.distxt-cmm_' + cancelid).css('display', 'block');
    $('.edit_to_' + cancelid).css('display', 'inline-block');
    $('.comment-submit_ryt_' + cancelid).css('display', 'none');
    $('.distxt-cmm_ryt_' + cancelid).css('display', 'block');
    $('.edit_to_ryt_' + cancelid).css('display', 'inline-block');
}

function deleteMsg(id) {
    var cnfrm = cnfrm_delete();
    var mainid = '<?php echo $main_id; ?>';
    var numItems = $('.dotlist_' + mainid + '').length;
    if (cnfrm == true) {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: "<?php echo base_url(); ?>admin/dashboard/deleteRequestmsg",
            data: {"request_file_id": id, },
            success: function (data) {
                if (data.status == 'success') {
                    var outer_main = $('#cro .active .mydivposition');
                    outer_main.find('.abcabcabc').each(function () {
                        var deldot = $('#' + id).data('dot');
                        var currentdots = $(this).data('dot');
                        if (currentdots > deldot) {
                            $(this).data('dot', currentdots - 1);
                            $(this).find('.numberdot').text(currentdots - 1);
                        }
                    });

                    $('.dotlist_' + mainid).each(function () {
                        var deldot = $('#' + id).data('dot');
                        var currentdots = $(this).find('.clickable').data('dot');
//                                                                console.log("currentdots", currentdots);
//                                                                console.log("deldot", deldot);
                        if (currentdots > deldot) {
                            $(this).data('dot', currentdots - 1);
                            $(this).find('.numberdot').text(currentdots - 1);
                        }
                    });
                    $('#' + id).remove();
                    $('.customer_chat' + id).remove();
                    $('#' + id).closest('li').remove();
                    $('.reply_' + id).remove();
                }
            },
        });
    }
}
</script>
