<style>
.pad{
	padding:5px;
}
.colorblack{
	color:#000;
	padding-bottom:0px;
	font-size:15px;
}
.nav-tab-pills-image ul li .nav-link:hover{
	border-bottom:unset !important;
}
.nav-tab-pills-image ul .nav-item+.nav-item{
	margin-left:9px;
}
.nav-tab-pills-image ul li .nav-link:focus
{
	border-bottom:unset !important;
}
.my_profile_sec .changeprofilepage{padding: 25px 15px 20px;}
.profile_image_sec .name{font-size: 21px;}
</style>

	<?php /* ?>
	<div class="nav-tab-pills-image">
		<ul class="nav nav-tabs"  style="border:none;">                      
			<li class="nav-item" style="background:#2f4458;margin-left:6px;color:#fff;border-radius: 12px 0px 0px 12px;">
				<a class="nav-link" style="background:#2f4458;color:#fff;border-radius: 12px 0px 0px 12px" href="<?php echo base_url(); ?>designer/profile/edit_profile"  style="">
					Edit Profile
				</a>
			</li>
			<li class="nav-item active" style="color:#fff;background:#ccc;">
				<a class="nav-link" style="color:#2f4458;background: #ccc;font-weight: 600;" href="<?php echo base_url(); ?>designer/profile/change_password">
					Change Password
				</a>
			</li>
			 <li class="nav-item" style="color:#fff;background:#ccc;margin-left:6px;border-radius:0px 12px 12px 0px;">
				<a class="nav-link" style="color:#2f4458;background: #ccc;font-weight: 600;border-radius: 0px 12px 12px 0px" href="<?php echo base_url(); ?>designer/profile/public_profile">
					Public Profile
				</a>
			</li>
			
		</ul>		
	</div>
	<?php */ ?>

	<div class="setting_tabs_link">
        <a href="<?php echo base_url(); ?>designer/profile/edit_profile" class="float-xs-left content-title-main active" style="display:inline-block; padding-left:33px; color: #1a3147;">
            My Profile
        </a>
        <a href="<?php echo base_url(); ?>designer/request/active_clients" class="float-xs-left content-title-main" style="color: #9da0a3; padding-left: 5%;">
            Customers
        </a>
    </div><!-- setting_tabs_link -->
</div><!-- col-10 -->


<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="background-color:#fff;">
	<div class="col-md-10 offset-md-1">
		<div class="my_profile_sec">
		<?php if($this->session->flashdata('message_error') != '') {?>				
			   <div class="alert alert-danger alert-dismissable">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
				</div>
			   <?php }?>
			   <?php if($this->session->flashdata('message_success') != '') {?>				
			   <div class="alert alert-success alert-dismissable">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
					<strong><?php echo $this->session->flashdata('message_success');?></strong>
				</div>
			   <?php }?>
			<form action="" method="post" enctype="multipart/form-data">
				<div class="row" style="margin-top: 30px; margin-bottom: 30px;">
					<div class="col-lg-5 text-center profile_icon_sec">
						<div class="changeprofilepage" style="margin-bottom:30px;">
							    <div class="imagemain2" style="padding-bottom: 20px; display: none;">
							        <input type="file" onChange="validateAndUpload(this);" class="form-control dropify waves-effect waves-button" name="profile" style="display:none;border: 2px solid #ccccce !important;border-radius: 5px !important;" data-plugin="dropify" data-height="100" id="inFile"/>
							    </div>

						    <div class="clearfix profile_image_sec">
							    <div class="imagemain">
							        <?php if($data[0]['profile_picture']){ ?>
							            <img src="<?php echo base_url()."uploads/profile_picture/".$data[0]['profile_picture']; ?>" />
							        <?php }else{ ?>
							        <div class="myprofilebackground" style="width:50px;height:50px;border-radius:50%;background: #0190ff;text-align: center;padding-top: 15px;font-size: 15px;color: #fff;">
							            <?php
							                $first_name = ""; $last_name="";
							                if(isset($data[0]['first_name'])){
							                    $first_name = substr($data[0]['first_name'],0,1);
							                }
							                if(isset($data[0]['last_name'])){
							                    $last_name = substr($data[0]['last_name'],0,1);
							                }
							            echo $first_name.$last_name; ?>
							        </div>
							        <?php } ?>
							        <span class="edit_icon"><a href="#" onclick="$('.dropify').click();" class="link1 font18 bold"><i class="fa fa-pencil" aria-hidden="true"></i></a></span>
							    </div>


						            <div class="text_bx design_text_bx">
						                <div class="name"><?php if(isset($data[0]['first_name'])){ echo $data[0]['first_name']; } ?> <?php if(isset($data[0]['last_name'])){ echo $data[0]['last_name']; } ?></div>
						                <div class="text_span">
						                <?php
						                $date  = strtotime($data[0]['created']);
						                $year  = date('Y',$date);
						                $monthName = date('F',$date);
						                ?>
						                    <span class="two">Member Since <?php echo $monthName . " " . $year ; ?></span>
						                </div>
						            </div><!-- text bx -->
						        <div class="sep1"></div>
						        <style>
						        .dropify-wrapper{ display:none; height: 200px !important; }
						        </style>  					        
						    </div><!-- prifile image sec -->
						</div><!-- changeprofilepage -->
						
						<div class="changeprofilepage skill_section text-left">
							<h4 class="heading">Skills</h4>

							<div class="range-container">
							  	<p class="text">graphic design</p>
							  	<label class="range">
							   		<input class="range-input"
							        	type="range"
							          	min="0"
							          	max="100"
							          	step="1"
							          	value="60">
							   		<span class="range-box">
							     		<span class="range-counter">50</span>
							   		</span>
								</label>
							</div><!-- range-container -->
							<div class="range-container">
							  	<p class="text">web design</p>
							  	<label class="range">
							   		<input class="range-input"
							        	type="range"
							          	min="0"
							          	max="100"
							          	step="1"
							          	value="60">
							   		<span class="range-box">
							     		<span class="range-counter">50</span>
							   		</span>
								</label>
							</div><!-- range-container -->
							<div class="range-container">
							  	<p class="text">Illustration</p>
							  	<label class="range">
							   		<input class="range-input"
							        	type="range"
							          	min="0"
							          	max="100"
							          	step="1"
							          	value="60">
							   		<span class="range-box">
							     		<span class="range-counter">50</span>
							   		</span>
								</label>
							</div><!-- range-container -->
							<div class="range-container">
							  	<p class="text">logo design</p>
							  	<label class="range">
							   		<input class="range-input"
							        	type="range"
							          	min="0"
							          	max="100"
							          	step="1"
							          	value="60">
							   		<span class="range-box">
							     		<span class="range-counter">50</span>
							   		</span>
								</label>
							</div><!-- range-container -->

							<div class="add_skill"><a href="#">+ Add Skill</a></div>
						</div><!-- changeprofilepage -->
					</div><!-- col -->

					<div class="col-lg-7">
						<div class="pmformmain">
    						<h4 class="heading">Edit Profile</h4>

                           	<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="inplabel">First Name</label>                                        
                                        <input type="text" name="first_name" placeholder="First Name" class="form-control" value="<?php if(isset($data[0]['first_name'])){ echo $data[0]['first_name']; } ?>"/>
                                    </div>                                    
                                </div><!-- col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="inplabel">Last Name</label>                                        
                                        <input type="text" name="last_name" placeholder="Last Name" class="form-control" value="<?php if(isset($data[0]['last_name'])){ echo $data[0]['last_name']; } ?>"/>
                                    </div>
                                </div><!-- col -->
                            </div><!-- row -->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="inplabel">Company</label>
                                        <input type="text" name="company_name" class="form-control" value="<?php if(isset($data[0]['company_name'])){ echo $data[0]['company_name']; } ?>">
                                    </div>
                                </div><!-- col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="inplabel" style="width:100%">&nbsp;</label>
                                        <input type="button" name="savebtn" class="form-control change_pass" value="Change Password" data-toggle="modal" data-target="#myModal">
                                    </div>
                                </div><!-- col -->
                            </div><!-- row -->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="inplabel">Notification Email</label>                                        
                                        <input type="email" placeholder="Notification Email" name="notification_email" class="form-control"  value="<?php if(isset($data[0]['notification_email'])){ echo $data[0]['notification_email']; } ?>"/>
                                    </div>
                                </div><!-- col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="inplabel">Phone</label>                                        
                                        <input type="text" placeholder="Phone" name="phone" class="form-control" value="<?php if(isset($data[0]['phone'])){ echo $data[0]['phone']; } ?>"/>
                                    </div>
                                </div><!-- col -->
                            </div><!-- row -->

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="inplabel">Address Line 1</label>
                                        <input type="text" name="address_line_1" class="form-control" value="<?php if(isset($data[0]['address_line_1'])){ echo $data[0]['address_line_1']; } ?>">
                                    </div>
                                </div><!-- col -->
                            </div><!-- row -->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="inplabel">City</label>
                                        <input type="text" name="city" class="form-control" value="<?php if(isset($data[0]['city'])){ echo $data[0]['city']; } ?>">
                                    </div>
                                </div><!-- col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="inplabel">State</label>
                                        <input type="text" name="state" class="form-control" value="<?php if(isset($data[0]['state'])){ echo $data[0]['state']; } ?>">
                                    </div>
                                </div><!-- col -->
                            </div><!-- row -->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="inplabel">Zip</label>
                                        <input type="text" name="zip" class="form-control" value="<?php if(isset($data[0]['zip'])){ echo $data[0]['zip']; } ?>">
                                    </div>
                                </div><!-- col -->
                            </div><!-- row -->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">                                        
                                    	<input type="submit" name="savebtn" value="Save Info" class="form-control save_info"/>
                                    </div>
                                </div><!-- col -->
                            </div><!-- row -->
						</div><!-- pmformmain -->
					</div><!-- col -->
				</div><!-- row -->

				<?php /* ?>

				<h3 style="text-align:center;color:#1a3147;font-size:25px;font-weight:600;">Edit Profile</h3>
				<div class="pad">
					<p class="colorblack">First Name</p>
						<input type="text" name="first_name" placeholder="First Name" class="form-control" style="border: 2px solid #ccccce !important;border-radius: 5px !important;" value="<?php if(isset($data[0]['first_name'])){ echo $data[0]['first_name']; } ?>"/>
				</div  class="pad">
				<div class="pad">	 
					<p class="colorblack">Last Name</p>
						<input type="text" style="border: 2px solid #ccccce !important;border-radius: 5px !important;" name="last_name" placeholder="Last Name" class="form-control" value="<?php if(isset($data[0]['last_name'])){ echo $data[0]['last_name']; } ?>"/>
				</div>
				<div class="pad">	
					<p class="colorblack">Phone </p>
						<input type="text" style="border: 2px solid #ccccce !important;border-radius: 5px !important;" placeholder="Phone" name="phone" class="form-control" value="<?php if(isset($data[0]['phone'])){ echo $data[0]['phone']; } ?>"/>
				</div>
				<div class="pad">				
					<p class="colorblack">Notification Email</p>
						<input type="email" style="border: 2px solid #ccccce !important;border-radius: 5px !important;" placeholder="Notification Email" name="notification_email" class="form-control"  value="<?php if(isset($data[0]['notification_email'])){ echo $data[0]['notification_email']; } ?>"/>
				</div >
				<div class="pad">	
					<p class="colorblack">Skype ID </p>
						<input type="email" style="border: 2px solid #ccccce !important;border-radius: 5px !important;" placeholder="Skype ID" name="skype_id" class="form-control" value="<?php if(isset($data[0]['skype_id'])){ echo $data[0]['skype_id']; } ?>"/>
				</div>
				<div class="pad">	
					<p class="colorblack">Paypal ID</p>
						<input type="email" style="border: 2px solid #ccccce !important;border-radius: 5px !important;" placeholder="Paypal ID" name="paypal_email_address" class="form-control" value="<?php if(isset($data[0]['paypal_email_address'])){ echo $data[0]['paypal_email_address']; } ?>"/>
				</div>


				<div class="pad">
					<p class="colorblack">Profile Image</p>
					<div class="col-md-6">
						<?php if($data[0]['profile_picture']){ ?>
						<div class="col-md-6">
							<img src="<?php echo base_url()."uploads/profile_picture/".$data[0]['profile_picture']; ?>" />
						</div>
						<?php }else{ ?>
						<div class="myprofilebackground" style="width:50px;height:50px;border-radius:50%;background: #0190ff;text-align: center;padding-top: 15px;font-size: 15px;color: #fff;">
							<?php
								$first_name = ""; $last_name="";
								if(isset($data[0]['first_name'])){
									$first_name = substr($data[0]['first_name'],0,1);
								}
								if(isset($data[0]['last_name'])){
									$last_name = substr($data[0]['last_name'],0,1);
								}
							echo $first_name.$last_name; ?>
						</div>
						<?php } ?>
					</div>
					<div class="col-md-6">
						<input type="file" class="form-control dropify waves-effect waves-button" name="profile" style="border: 2px solid #ccccce !important;border-radius: 5px !important;" data-plugin="dropify" data-height="100" id="file"/>
					</div>
				</div>
				

				<div class="pad" >	
					<input  style="background-color:#ec1c41;color:#fff" type="submit" name="savebtn" value="Save Changes" class="form-control"/>
				</div>	
				</table>
				<?php */ ?>
			</form>		
		</div><!-- my_profile_sec -->
	</div>	
</div>



<script>
window.addEventListener('resize', triggerAll);
document.addEventListener('input', (evt) => RangeInput(
  evt.target
));

triggerAll();

function triggerAll() {
  document
    .querySelectorAll('.range-input')
    .forEach(RangeInput);
}

function RangeInput(input) {
  if (input.type !== 'range') {
    return;
  }

  const range = (input.max - input.min) / 100;
  const thumbSize = 20;
  const inputHalfWidth = input.clientWidth - thumbSize;
  const balloonPosition = (parseFloat(input.value) * (inputHalfWidth / 100)) - (thumbSize/2);
  const balloon = input.nextElementSibling;

  balloon.style.left = `${balloonPosition}px`;
  balloon.firstElementChild.textContent = input.value;

  input.style.setProperty('--value', (input.value - input.min) / range);
}
</script>