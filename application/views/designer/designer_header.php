<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google" content="notranslate">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Designer Portal | Graphics Zoo</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>favicon.ico">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" rel = 'stylesheet'">
    <!-- Bootstrap -->
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/designer/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/designer/reset.css');?>" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/designer/adjust.css');?>" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/designer/style.css');?>" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/designer/responsive.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'plugins/ui-slider/jquery.ui-slider.css');?>" />
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/custom_style_for_all.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'css/jquery.fancybox.min.css');?>">
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/dot-comment/leader-line.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/dot-comment/anim-event.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/dot-comment/in-view.min.js"></script>

    <script>
    var baseUrl = "<?php echo base_url(); ?>";
    var $assets_path = "<?php echo FS_PATH_PUBLIC_ASSETS ?>";
    var requestmainchatimg = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTMAINCHATFILES; ?>";
    var requestdraftchatimg = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES; ?>";
</script>
</head>
<?php
$CI = & get_instance();
$CI->load->library('myfunctions');
$CI->load->helper('notifications');
$helper_data = helpr();
$edit_profile = $helper_data['profile_data'];
$messagenotifications = $helper_data['messagenotifications'];
$messagenotification_number = $helper_data['messagenotification_number'];
$notifications = $helper_data['notifications'];
$notification_number = $helper_data['notification_number'];
$current_url = current_url();
$pageslug = explode('/',$current_url);
$classondotcomment = (in_array('project_image_view',$pageslug)) ? 'view_file_body' : '';
?>
<body class="deshboard-bgnone <?php echo $classondotcomment;?>" style="background: #f2f2f2;">
    <?php 
    if(!in_array('project_image_view',$pageslug)){
        ?>
        <header class="nav-section">
            <div class="container-fluid">
                <nav class="navbar navbar-default flex-show centered"> <!-- centered -->
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <ul class="gz-breadcrumb">
                            <?php
                            //echo "<pre/>";print_R($breadcrumbs);
                            foreach ($breadcrumbs as $name => $url) {
                                ?>
                                <li><a href="<?php echo $url; ?>"><?php echo $name; ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="box-setting">
                        <ul id="right_nav" class="list-unstyled list-setting top_noti">

                            <!-- Message Section -->
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <span class="mess-box">
                                     <i class="icon-gz_message_icon"></i>
                                     <?php
                                     if (isset($messagenotifications)) {
                                        if (sizeof($messagenotifications) > 0) {
                                            ?>
                                            <span class="numcircle-box">

                                            </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                </span>
                            </a>
                            <div class="dropdown-menu">
                                <h3 class="head-notifications">Messages</h3>
<!--                                <a href='javascript:void(0)' class='clearall_msges' data-allmsges='<?php //echo json_encode($messagenotifications); ?>'>clear All</a>-->
                                <ul class="list-unstyled list-notificate">
                                    <?php if (sizeof($messagenotifications) > 0) { 
                                        //echo "<pre/>";print_R($messagenotifications);
                                        ?>
                                        <?php for ($i = 0; $i < sizeof($messagenotifications); $i++) { ?>
                                            <li><a href="<?php echo base_url() . $messagenotifications[$i]['url']; ?>">
                                                <div class="setnoti-fication">
                                                    <figure class="pro-circle-k1">
                                                        <img src="<?php echo $messagenotifications[$i]['profile_picture']; ?>" class="img-responsive msg">
                                                    </figure>

                                                    <div class="notifitext">
                                                        <p class="ntifittext-z1">
                                                            <h5 style="font-size: 13px;"><?php echo $messagenotifications[$i]['heading']; ?></h5>
<!--                                                            <small style="font-size: 12px;">-->
                                                                <?php
                                                                if (strlen($messagenotifications[$i]['title']) >= 50) {
                                                                    echo substr($messagenotifications[$i]['title'], 0, 40) . "..";
                                                                } else {
                                                                    echo $messagenotifications[$i]['title'];
                                                                }
                                                                ?>
<!--                                                    </small>-->
                                                            </p>
                                                            <!-- <p class="ntifittext-z1"><strong>kevin Jenkins</strong> posted in <strong>Other Detail</strong></p> -->
                                                            <p class="ntifittext-z2">
                                                                <?php
                                                                $approve_date = $messagenotifications[$i]['created'];
                                                                $msgdate = $CI->myfunctions->onlytimezoneforall($approve_date);
                                                                echo $msgdate;

                                                                ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </a></li>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <li style="padding:10px;">No new message</li>
                                        <?php } ?>

                                    </ul>
                                </div>
                            </li>
                            <!-- Message Section End -->
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <span class="bell-box">
                                       <i class="icon-gz_bell_icon"></i>
                                       <?php
                                       if (isset($notifications)) {
                                        if (sizeof($notifications) > 0) {
                                            ?>
                                            <span class="numcircle-box">

                                            </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                </span>
                            </a>
                            <div class="dropdown-menu">
                                <h3 class="head-notifications">Notifications</h3>
                                <ul class="list-unstyled list-notificate">
                                    <?php if (sizeof($notifications) > 0) { ?>
                                        <?php for ($i = 0; $i < sizeof($notifications); $i++) { ?>
                                            <li><a href="<?php echo base_url() . $notifications[$i]['url']; ?>">
                                                <div class="setnoti-fication">
                                                    <figure class="pro-circle-k1">
                                                        <img src="<?php echo $notifications[$i]['profile_picture']; ?>" class="img-responsive">
                                                    </figure>

                                                    <div class="notifitext"  title="<?php echo $notifications[$i]['title']; ?>">
                                                        <p class="ntifittext-z1"><strong><?php echo $notifications[$i]['first_name'] . " " . $notifications[$i]['last_name']; ?></strong>
                                                            <?php echo $notifications[$i]['title']; ?>
                                                        </p>
                                                        <p class="ntifittext-z2">
                                                            <?php
                                                            $approve_date = $notifications[$i]['created'];
                                                            $notidate = $CI->myfunctions->onlytimezoneforall($approve_date);
                                                            echo $notidate;

                                                            ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </a></li>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <li style="padding:10px;">No new notification</li>
                                    <?php } ?>

                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expended="false">
                                <figure class="pro-circle-img-xxs">
                                    <img src="<?php echo $edit_profile[0]['profile_picture']; ?>" class="img-responsive">
                                </figure>
                            </a>
                            <div class="dropdown-menu pull-right profile-dropdown" aria-labelledby="dropdownMenuButton">
                                <h3 class="head-notifications"><?php echo $edit_profile[0]['first_name'] . " " . $edit_profile[0]['last_name']; ?></h3>
                                <h2 class="head-notifications"><?php echo $edit_profile[0]['email']; ?></h2>
                                <div class="profile_wrapper">
                                    <ul class="list-unstyled list-notificate">
                                        <li>
                                            <a href="<?php echo base_url(); ?>designer/request/logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>

        </nav>
    </div>
</header>

<div class="admin-wraper">
    <div class="admin-sidebar">
        <?php $this->load->view('designer/designer_sidebar'); ?>            
    </div>
<?php } ?>
