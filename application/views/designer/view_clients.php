<h2 class="float-xs-left content-title-main" style="display:inline-block;">Clients</h2>
</div>
<div class="col-md-12" style="background:white;">
<div class="col-md-10 offset-md-1">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147; } 
.pinktext { color: #ec4159; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
	font-size: 18px !important;
    padding: 8px 0px !important;
    font-weight: 600 !important;
    line-height: 31px;
    text-align: left !important;
    padding-left: 10px !important;
	width: 10px;
    padding: 4px !important;
}
.projecttitle{
	font-size: 25px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: 1px solid #000;
    background: unset;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
table {     border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}
.nav-tab-pills-image ul li .nav-link {
    color:#1a3147;
	font-weight:600;
	padding: 7px 25px;
}
</style>
    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">    

				<div class="col-sm-12" style="margin-top:15px;">
					<div class="col-sm-1" style="padding:0px;">
						<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;" />
					</div>
					<div class="col-sm-11">
						<h3 class="darkblacktext weight600" style="letter-spacing:0px;">Kevin Jenkins</h3>
						<p class="greytext">Member since November 21, 2017</p>
					</div>
				</div>
                <div class="nav-tab-pills-image">
                    <ul class="nav nav-tabs" role="tablist">                      
                        <li class="nav-item active" style="background: #ededed;border-radius: 7px;margin-left: 1px;">
                            <a class="nav-link" data-toggle="tab" href="#designs_request_tab" role="tab">
                                Active Request (3)
                            </a>
                        </li>
                        <li class="nav-item" style="background: #ededed;border-radius: 7px;margin-left: 1px;">
                            <a class="nav-link" data-toggle="tab" href="#inprogressrequest" role="tab">
                                Request approved (0)
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                     <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Project Name</th>
												<th>Designer Name</th>
												<th>Requested</th>
												<th>In-progress</th>
												<th>Due Date</th>
												<th>Messages</th>
												<th>Icon</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										   <tr class="trborder" onclick="window.location.href='<?php echo base_url(); ?>designer/request/view_request/5'">
												<td class="greenbackground whitetext numbercss">1st</td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5">Project 1</p>
													<p class="greentext weight600 textleft pl20 pb5">Waiting for approval</p>
													<p class="darkblacktext textleft pl20">This is a test Description</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Designer</p>
													<p class="darkblacktext weight600">Rey</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Requested</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">In-progress</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Due Date</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Messages</p>
													<p class="darkblacktext weight600"><i class="fa fa-envelope orangetext"></i><span class="orangetext pl5">3</span></p>
												</td>
												<td>
													<i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star"></i>
												</td>
											</tr> 
											
											<tr class="trborder" onclick="window.location.href='<?php echo base_url(); ?>designer/request/view_request/5'">
												<td class=" orangebackground whitetext numbercss">2nd</td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5">Project 2</p>
													<p class=" orangetext weight600 textleft pl20 pb5">Waiting for Review</p>
													<p class="darkblacktext textleft pl20">This is a test Description</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Designer</p>
													<p class="darkblacktext weight600">Rey</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Requested</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">In-progress</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Due Date</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Messages</p>
													<p class="darkblacktext weight600"><i class="fa fa-envelope orangetext"></i><span class="orangetext pl5">3</span></p>
												</td>
												<td>
													<i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star "></i><i class="fa fa-star"></i>
												</td>
											</tr>

											<tr class="trborder" onclick="window.location.href='<?php echo base_url(); ?>designer/request/view_request/5'">
												<td class=" pinkbackground whitetext numbercss">3rd</td>
												<td>
													<p class="darkblacktext weight600 projecttitle pb0 pt5">Project 3</p>
													<p class=" bluetext weight600 textleft pl20 pb5">In-Queue</p>
													<p class="darkblacktext textleft pl20">This is a test Description</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Designer</p>
													<p class="darkblacktext weight600">Rey</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Requested</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">In-progress</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Due Date</p>
													<p class="darkblacktext weight600">00/00/0000</p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Messages</p>
													<p class="darkblacktext weight600"><i class="fa fa-envelope orangetext"></i><span class="orangetext pl5">3</span></p>
												</td>
												<td>
													<i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
												</td>
											</tr>
											
											
										</tbody>
									</table>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
												<th>Design Status</th>
												<th>Customer Name</th> 
												<th>Designer Name</th>
												<th>Title</th>
                                                <th>Date Requested</th>
												<th>Messages()</th>
												<th>Action</th>
                                                                           
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
</div>