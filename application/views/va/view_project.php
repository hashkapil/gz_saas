<h2 class="float-xs-left content-title-main" style="display:inline-block;">Projects</h2>
<a href="<?php echo base_url(); ?>va/dashboard/view_clients" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Clients</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_designer" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Designers</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_qa" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">QA</a>
</div>
<div class="col-md-12" style="background:white;">
<div class="col-md-10 offset-md-1">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147; } 
.darkblackbackground { background-color:#1a3147; } 
.lightdarkblacktext { color:#b4b9be; }
.lightdarkblackbackground { background-color:#f4f4f4; }
.pinktext { color: #ec4159 !important; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
	font-size: 31px !important;
    padding: 8px 0px !important;
    font-weight: 600 !important;
    letter-spacing: -3px !important;
}
.projecttitle{
	font-size: 25px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: 1px solid #000;
    background: unset;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
table {     border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}
.nav-tab-pills-image ul li .nav-link{
	background:#ededed;
	font-weight: 600;
	color: #2f4458;
}
.ls0{ letter-spacing: 0px; }
</style>
    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;">                
                <div class="nav-tab-pills-image">
                    <ul class="nav nav-tabs" role="tablist" style="border-bottom:unset !important;">                      
                        <li class="nav-item active">
                            <a class="nav-link" data-toggle="tab" href="#designs_request_tab" role="tab">
                                Project Info
                            </a>
                        </li>
                        <li class="nav-item" style="margin-left:0px;">
                            <a class="nav-link" data-toggle="tab" href="#inprogressrequest" role="tab">
                                Files
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                            <div class="row" style="padding-bottom: 50px;border-bottom: 1px solid #1a3147;">
							
								<div class="col-md-9" style="border-right:1px solid;">
								<div class="col-sm-12" style="padding:0px;border-bottom:1px solid;">
                                <div class="col-md-5">
                                    <p>
										<h4 class="darkblacktext weight600 ls0" style="display:inline-block;">Priority:</h4>
										<span class="greenbackground" style="padding: 10px;color: white;margin-left: 15px;font-size: 25px;">1st</span>
									</p>
									<p>
										<h4 class="darkblacktext weight600 ls0" style="display:inline-block;">Category:</h4>
										<span class="darkblacktext" style="font-size:18px;"><?php echo $data[0]['category']; ?></span>
									</p>
									<p>
										<h4 class="darkblacktext weight600 ls0">Project Title:</h4>
										<span class="lightdarkblacktext" style="font-size:18px;"><?php echo $data[0]['title']; ?></span>
									</p>
									<p>
										<h4 class="darkblacktext weight600 ls0">Design Dimension:</h4>
										<span class="lightdarkblacktext" style="font-size:18px;"><?php echo $data[0]['design_dimension']; ?></span>
									</p>
									<p>
										<h4 class="darkblacktext weight600 ls0">What industry is this design for?</h4>
										<span class="lightdarkblacktext" style="font-size:18px;"><?php echo $data[0]['business_industry']; ?></span>
									</p>
									<p>
										<h4 class="darkblacktext weight600 ls0">Description</h4>
										<span class="lightdarkblacktext" style="font-size:18px;"><?php echo $data[0]['description']; ?></span>
									</p>
                                </div>
								<div class="col-md-7">
									 <p>
										<h4 class="darkblacktext weight600 ls0" style="display:inline-block;">Designer:</h4>
										<span class="lightdarkblacktext" style="padding: 10px;margin-left: 15px;font-size: 25px;"><?php echo $data[0]['designer_first_name']; ?></span>
										<h4 class="darkblacktext weight600 ls0" style="display:inline-block;margin-left: 30px;">Status:</h4>
										<span class="bluetext" style="padding: 10px;margin-left: 15px;font-size: 25px;"><?php echo $data[0]['status']; ?></span>
									</p>
									<p>
										<h4 class="darkblacktext weight600 ls0">Additional info to designer?</h4>
										<span class="lightdarkblacktext" style="font-size:18px;"><?php echo $data[0]['other_info']; ?></span>
									</p>
									
									<p style="margin-top:20px;">
										<h4 class="darkblacktext weight600 ls0">Attachments</h4>
										<?php for($i=0;$i<sizeof($data[0]['customer_attachment']);$i++){ ?>
											<div class="lightdarkblackbackground" style="padding:5px;">
												<img src="<?php echo base_url()."uploads/requests/".$data[0]['id']."/".$data[0]['customer_attachment'][$i]['file_name']; ?>" />
											</div>
										<?php } ?>
									</p>
								</div>
								</div>
								
								<div class="col-sm-12">
									<p style="margin-top:20px;">
										<h4 class="darkblacktext weight600 ls0">Style Suggestions</h4>
										<?php if($data[0]['image'] != ""){
											$images = explode(",",$data[0]['image']);
											for($i=0;$i<sizeof($images);$i++){ ?>
												<div class="col-sm-3">
													<image src="<?php echo $images[$i]; ?>" style="max-width:100%;" />
												</div>
										<?php	}
										} ?>
									</p>
									<p>
										<h4 class="darkblacktext weight600 ls0" style="clear:both;">Additional description for the style?</h4>
										<span class="lightdarkblacktext" style="font-size:18px;"><?php echo $data[0]['additional_description']; ?></span>
									</p>
									<p>
										<h4 class="darkblacktext weight600 ls0">Color Preferences</h4>
										<?php if($data[0]['color'] != ""){
											$color = explode(",",$data[0]['color']);
											for($i=0;$i<sizeof($color);$i++){ ?>
										<div class="col-sm-3" style="" >
											<div class="col-sm-5" style="" >
												<div style="background:<?php echo $color[$i]; ?>;width:50px;height:50px;"></div>
											</div>
											<div class="col-sm-7" style="margin-top: 10px;" >
												<p class="greytext" style="font-size: 16px;display:inline-block;"><?php echo $color[$i]; ?></p>
											</div>
										</div>
										<?php	}
										} ?>
									</p>
								</div>
								</div>
								<div class="col-md-3">
									<div class="col-md-12">
											<p style="margin-top:20px;">
											<h4 class="darkblacktext weight600 ls0">Message</h4>
											<div class="col-md-12 messagediv_<?php echo $data[0]['id']; ?>" style="padding:0px;overflow-y:scroll;height:300px;">
										<?php for($j=0;$j<sizeof($chat_request);$j++){ ?>
											<?php if($chat_request[$j]['sender_type'] == "va"){ ?>
											<div class="col-md-12 greytext" style="border:1px solid;border-radius:8px;">
												<p class="darkblacktext weight600" style="text-align:right;font-size: 18px;padding-bottom: 0px;padding-top: 10px;"><?php echo "VA"; ?></p>
												
												<p class="greytext" style="line-height: 16px;text-align:right;"><?php echo $chat_request[$j]['message']; ?></p>
												
												<p class="darkblacktext weight600" style="text-align:right;">
												<?php 											
												$date=strtotime($chat_request[$j]['created']);//Converted to a PHP date (a second count)
												//Calculate difference
												$diff=strtotime(date("Y:m:d H:i:s"))-$date;//time returns current time in seconds
												$minutes = floor($diff/60);
												$hours = floor($diff/3600);
												if($hours == 0){
													echo $minutes. " Minutes Ago";
												}else{
													$days = floor($diff / (60 * 60 * 24));
													if($days == 0){
														echo $hours. " Hours Ago";
													}else{
														echo $days. " Days Ago";
													}
												}
												?>
												</p>
											</div>
											<?php }else{ ?>
											<div class="col-md-12 greytext" style="">
												<p class="pinktext weight600" style="font-size: 18px;padding-bottom: 0px;padding-top: 10px;"><?php if($chat_request[$j]['sender_type'] == "designer"){ echo $data[0]['designer_name']; }elseif($chat_request[$j]['sender_type'] == "admin"){ echo "Admin"; }elseif($chat_request[$j]['sender_type'] == "customer"){ echo $data[0]['customer_name']; }elseif($chat_request[$j]['sender_type'] == "qa"){ echo "QA"; } ?></p>
												<p class="greytext" style="line-height: 16px;"><?php echo $chat_request[$j]['message']; ?></p>
												<p class="pinktext weight600" style="">
												<?php 											
												$date=strtotime($chat_request[$j]['created']);//Converted to a PHP date (a second count)
												//Calculate difference
												$diff=strtotime(date("Y:m:d H:i:s"))-$date;//time returns current time in seconds
												$minutes = floor($diff/60);
												$hours = floor($diff/3600);
												if($hours == 0){
													echo $minutes. " Minutes Ago";
												}else{
													$days = floor($diff / (60 * 60 * 24));
													if($days == 0){
														echo $hours. " Hours Ago";
													}else{
														echo $days. " Days Ago";
													}
												}
												?>
												</p>
											</div>
											<?php } ?>
										<?php } ?>
										
										</div>
											<input type='text' class='form-control text_<?php echo $data[0]['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px;" placeholder="Reply To Client" />
											<span style="float: right;margin-right: 6px;margin-top: -20px;position: relative;z-index: 2;color: red;" class="pinktext fa fa-send send_request_chat" 
											data-requestid="<?php echo $data[0]['id']; ?>" 
											data-senderrole="va" 
											data-senderid="<?php echo $_SESSION['user_id']; ?>" 
											data-receiverid="<?php echo $data[0]['customer_id']; ?>" 
											data-receiverrole="customer"
											data-sendername="<?php echo "VA"; ?>"></span>
										</p>
										</div>
								</div>
                            </div>
							
							<!--<div class="row" style="margin:50px 0px;">
								<input type="button" class="darkblackbackground whitetext weight600" style="padding:15px 30px;font-size:18px;min-width: 160px;border: unset;border-radius: 6px;" value="EDIT REQUEST" />
								<input type="button" class="whitetext weight600" style="padding:15px 30px;font-size:18px;min-width: 160px;background:#e52344;border: unset;border-radius: 6px;margin-left: 20px;" value="SAVE" />
							</div>-->
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">
                            <div class="row" style="padding-bottom: 50px;border-bottom: 1px solid #1a3147;">
                                <div class="col-md-12">
									<div class="col-md-8">
										<div class="col-md-12" style="border-right:1px solid #1a3147;">
											<p style="margin-top:20px;">
												<h4 class="darkblacktext weight600 ls0" style="display:inline-block;">Upload Files</h4>
												<button class="btn pinktext weight600" style="background:#fff;border-radius:5px;border: 2px solid;float: right;">+ Upload File</button>
											</p>

											<?php
											for($i=0;$i<sizeof($designer_preview_file);$i++){ 
												$image_url = base_url()."uploads/requests/".$request_id."/".$designer_preview_file[$i]['file_name'];
												$created_date_time = strtotime($designer_preview_file[$i]['created']);
												
												$now_date_time = strtotime(date("Y-m-d H:i:s"));
												$span = "";
												if(($now_date_time - $created_date_time) < 2000){
													$span = '<span style="background:#ec445c;padding:5px;color:#fff;font-weight:600;position: absolute;top: 0px;">NEW</span>';
												}
											?>
												<div class="col-md-3" style="margin: 5px 0px;<?php if($i==0){ echo "clear:both;"; }?>">
													<a href="<?php echo base_url(); ?>va/dashboard/view_files/<?php echo $designer_preview_file[$i]['id']."?id=".$request_id; ?>">
														<div class=" col-md-12 " style="display: inline-block;width:100%;height:100px;padding: 0px;">
															<img src="<?php echo $image_url; ?>" width=100%/>
															<?php echo $span; ?>
														</div>
													</a>
												</div>
											<?php }	?>
										</div>
									
									</div>
									<div class="col-md-4">
										<div class="col-md-12">
											<p style="margin-top:20px;">
											<h4 class="darkblacktext weight600 ls0">Message</h4>
											<div class="col-md-12 messagediv_<?php echo $data[0]['id']; ?>" style="padding:0px;overflow-y:scroll;height:300px;">
										<?php for($j=0;$j<sizeof($chat_request);$j++){ ?>
											<?php if($chat_request[$j]['sender_type'] == "va"){ ?>
											<div class="col-md-12 greytext" style="border:1px solid;border-radius:8px;">
												<p class="darkblacktext weight600" style="text-align:right;font-size: 18px;padding-bottom: 0px;padding-top: 10px;"><?php echo "VA"; ?></p>
												
												<p class="greytext" style="line-height: 16px;text-align:right;"><?php echo $chat_request[$j]['message']; ?></p>
												
												<p class="darkblacktext weight600" style="text-align:right;">
												<?php 											
												$date=strtotime($chat_request[$j]['created']);//Converted to a PHP date (a second count)
												//Calculate difference
												$diff=strtotime(date("Y:m:d H:i:s"))-$date;//time returns current time in seconds
												$minutes = floor($diff/60);
												$hours = floor($diff/3600);
												if($hours == 0){
													echo $minutes. " Minutes Ago";
												}else{
													$days = floor($diff / (60 * 60 * 24));
													if($days == 0){
														echo $hours. " Hours Ago";
													}else{
														echo $days. " Days Ago";
													}
												}
												?>
												</p>
											</div>
											<?php }else{ ?>
											<div class="col-md-12 greytext" style="">
												<p class="pinktext weight600" style="font-size: 18px;padding-bottom: 0px;padding-top: 10px;"><?php if($chat_request[$j]['sender_type'] == "designer"){ echo $data[0]['designer_name']; }elseif($chat_request[$j]['sender_type'] == "admin"){ echo "Admin"; }elseif($chat_request[$j]['sender_type'] == "customer"){ echo $data[0]['customer_name']; }elseif($chat_request[$j]['sender_type'] == "qa"){ echo "QA"; } ?></p>
												<p class="greytext" style="line-height: 16px;"><?php echo $chat_request[$j]['message']; ?></p>
												<p class="pinktext weight600" style="">
												<?php 											
												$date=strtotime($chat_request[$j]['created']);//Converted to a PHP date (a second count)
												//Calculate difference
												$diff=strtotime(date("Y:m:d H:i:s"))-$date;//time returns current time in seconds
												$minutes = floor($diff/60);
												$hours = floor($diff/3600);
												if($hours == 0){
													echo $minutes. " Minutes Ago";
												}else{
													$days = floor($diff / (60 * 60 * 24));
													if($days == 0){
														echo $hours. " Hours Ago";
													}else{
														echo $days. " Days Ago";
													}
												}
												?>
												</p>
											</div>
											<?php } ?>
										<?php } ?>
										
										</div>
											<input type='text' class='form-control text_<?php echo $data[0]['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px;" placeholder="Reply To Client" />
											<span style="float: right;margin-right: 6px;margin-top: -20px;position: relative;z-index: 2;color: red;" class="pinktext fa fa-send send_request_chat" 
											data-requestid="<?php echo $data[0]['id']; ?>" 
											data-senderrole="va" 
											data-senderid="<?php echo $_SESSION['user_id']; ?>" 
											data-receiverid="<?php echo $data[0]['customer_id']; ?>" 
											data-receiverrole="customer"
											data-sendername="<?php echo "VA"; ?>"></span>
										</p>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
</div>