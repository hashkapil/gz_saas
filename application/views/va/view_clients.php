<h2 class="float-xs-left content-title-main" style="display:inline-block;">Projects</h2>
<a href="<?php echo base_url(); ?>va/dashboard/view_clients" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Clients</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_designer" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Designers</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_qa" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">QA</a>
</div>
<div class="col-md-12" style="background:white;">
<div class="col-md-10 offset-md-1">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147; } 
.darkblackbackground { background-color:#1a3147; }
.pinktext { color: #ec4159; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }
.ls0{ letter-spacing:0px; }
.numbercss{
	font-size: 18px !important;
    padding: 8px 0px !important;
    font-weight: 600 !important;
    line-height: 31px;
    text-align: left !important;
    padding-left: 10px !important;
}
.projecttitle{
	font-size: 25px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: 1px solid #000;
    background: unset;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
table {     border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}
.nav-tab-pills-image ul li .nav-link {
    color:#1a3147;
	font-weight:600;
	padding: 7px 25px;
}
</style>
    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;">                
                <div class="nav-tab-pills-image">
					<div class="col-sm-4" style="padding:0px;">
						<ul class="nav nav-tabs" role="tablist" style="border-bottom: unset;padding:0px;">                      
							<li class="nav-item active" style="background: #ededed;border-radius: 7px;margin-left: 1px;padding:0px;">
								<a class="nav-link" data-toggle="tab" href="#active_clients" role="tab">
									Active Clients (<?php echo sizeof($data); ?>)
								</a>
							</li>
							<li class="nav-item" style="background: #ededed;border-radius: 7px;margin-left: 1px;padding:0px;">
								<a class="nav-link" data-toggle="tab" href="#previous_clients" role="tab">
									Previous Clients (0)
								</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-4 darkblacktext">
						<button class="btn btn-default weight600" style="border-radius:5px;background:#cecece">Filter</button>
					</div>
					<div class="col-sm-1">&nbsp;</div>
					<div class="col-sm-3">
						<div class="col-sm-12 darkblacktext weight600" style="margin-top: 10px;padding: 0px;text-align:right;">
							<i class="darkblacktext fa fa-search"></i> Search
						</div>
						
					</div>
                    <div class="tab-content">
                        <div class="tab-pane active content-datatable datatable-width" id="active_clients" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                     <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Project Name</th>
												<th>No. of Requests</th>
												<th>Active Requests</th>
												<th>Designer</th>
												<th>Rating</th>
												<th>Messages</th>
                                            </tr>
                                        </thead>
                                        <tbody>
											<?php for($i=0;$i<sizeof($data);$i++){ ?>
										   <tr class="trborder" onclick="window.location.href='<?php echo base_url(); ?>va/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
												<?php if($data[$i]['plan_turn_around_days'] == "1"){ ?>
												<td class="darkblackbackground whitetext numbercss" style="width: 100px;">Premium Member </br><p class="whitetext" style="font-size: 14px;">1-Day Turnaround</p></td>
											<?php }elseif($data[$i]['plan_turn_around_days'] == "3"){ ?>
												<td class=" pinkbackground whitetext numbercss">Standard Member </br><p class="whitetext" style="font-size: 14px;">3-Day Turnaround</p></td>
											<?php }else{ ?>
												<td class=" greybackground whitetext numbercss">No Member </br><p class="whitetext" style="font-size: 14px;">No Plan</p></td>
											<?php } ?>
												<td>
													<div class="col-sm-3">
														<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;" />
													</div>
													<div class="col-sm-6" style="text-align:left;">
														<a href="<?php echo base_url()."admin/request/client_projects/".$data[$i]['id'];?>"><h5 class="darkblacktext weight600"><?php echo $data[$i]['first_name']." ".$data[$i]['last_name']?></h5></a>
														<p class="greytext">Member since  <?php echo date("M d, Y",strtotime($data[$i]['created']))?></p>
													</div>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">No. of</br> Requests</p>
													<p class="darkblacktext weight600"><?php echo $data[$i]['no_of_request']; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Active </br>Requests</p>
													<p class="darkblacktext weight600"><?php echo $data[$i]['active_request']; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Designer</p>
													<p class="darkblacktext weight600"><?php if($data[$i]['designer_name']){ echo $data[$i]['designer_name']; }else{ echo "No Designer"; } ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Rating</p>
													<i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star"></i>
												</td>
												<td>
													<i class="fa fa-envelope"></i><p class="greytext pb0 weight600 font18 directmessage" style="display: inline-block;margin-left: 5px;" data-clientname="<?php echo $data[$i]['first_name']." ".$data[$i]['last_name']?>" data-clientid="<?php echo $data[$i]['id']; ?>" data-imageurl="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;">Direct Message</p>
												</td>
											</tr> 
											<?php } ?>
											
										</tbody>
									</table>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="previous_clients" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <table data-plugin="datatable" data-responsive="true" class="custom-table table table-striped table-hover dt-responsive">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
												<th>Design Status</th>
												<th>Customer Name</th> 
												<th>Designer Name</th>
												<th>Title</th>
                                                <th>Date Requested</th>
												<th>Messages()</th>
												<th>Action</th>
                                                                           
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
						
                    </div>
					
                </div>
            </div>
        </div>
    </div>
	<!--------model start--------->
	
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header" style="display:none;">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modal Header</h4>
		</div>
		<div class="modal-body" style="padding:0px;">
			<div class="col-sm-12" style="padding:30px;">
				<div class="col-sm-12">
					<h3 class="darkblacktext weight600 ls0" style="margin-bottom: 20px;">Add Client</h3>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="fname" class="darkblacktext weight600"><strong>First Name:</strong></label>
						<input type="text" class="form-control" id="example1" name="First Name" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>	
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Last Name:</label>
						<input type="text" class="form-control" id="example1" name="Last Name" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="Email" class="darkblacktext weight600">Email:</label>
						<input type="text" class="form-control" id="Email" name="Email" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="Phone" class="darkblacktext weight600">Phone:</label>
						<input type="number" class="form-control" id="Phone" name="Phone" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="Password" class="darkblacktext weight600">Password:</label>
						<input type="Password" class="form-control" id="example1" name="Password" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-12"  style="border-bottom:  1px solid;padding-bottom: 18px;margin-bottom: 22px; ">
						<label for="Confirm Password" class="darkblacktext weight600">Confirm Password:</label>
						<input type="Password" class="form-control" id="Confirm Password" name="Confirm Password" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
				</div>
				<div class="col-sm-12">		  
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="Confirm Password" class="darkblacktext weight600">Select Payment Method:</label>
						<select class="form-control " id="sel1" style="background-color: #ededed  !important;border-radius: 6px !important;border:unset !important;">
							<option>Credit/Debit Card</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
						</select>
					</div>
				  
					<div class="col-sm-8" style="margin-bottom:10px;">
						<label for="Card Number" class="darkblacktext weight600">Card Number:</label>
						<input type="number" class="form-control" id="Card Number" name="Card Number" required style=" border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
				  
					<div class="col-sm-3" style="margin-bottom:10px;">
						<label for="Cvs" class="darkblacktext weight600">CVC:</label>
						<input type="text" class="form-control" id="CVC" name="CVC" required style="border: 2px solid #c7c9cc !important;border-radius: 6px !important;">
					</div>
				  
					<div class="col-sm-7" style="margin-bottom:10px;">
						<label for="edate" class="darkblacktext weight600">Expiration Date:</label>
						<input type="date" class="form-control" id="edate" name="Expiration Date" required style="border: 2px solid #c7c9cc !important;border-radius: 6px !important;">
					</div>
					<div class="col-sm-10" style="margin-bottom:10px;">
						<button type="button" class="btn pinkbackground weight600" style="margin:  auto;width: 80%;height: 60px;font-size:20px;margin-top: 20px;">Add Customer</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" style="display:none;">Close</button>
		</div>
	</div>
			
   </div>
	
</div>

				
					<!--------model end----------->
<div id="directmessage" class="modal fade" role="dialog" style="margin-top:10%;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body " style="font-size: 170px;height: 270px;">
			
			<h3 style="letter-spacing: -1px;font-size: 21px;display:inline-block;" class="darkblacktext weight600">Direct Message to </h3>
			<h3 style="letter-spacing: -1px;font-size: 21px;display:inline-block;width:50%;" class="darkblacktext weight600 image_name">Direct Message to </h3>
			
			<div class="col-sm-10">
				<textarea class="form-control" style="height: 125px;margin-top: 10px;border: 1px solid !important;"></textarea>
			</div>
			<div class="col-sm-2">
				<i class="fa fa-send pinktext" style="font-size: 35px;"></i>
			</div>
      </div>
    </div>

  </div>
</div>
</section>
</div>