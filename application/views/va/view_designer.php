<a href="<?php echo base_url(); ?>va/dashboard" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Projects</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_clients" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Clients</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_designer" class="float-xs-left content-title-main" style="padding-left: 10%;">Designers</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_qa" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">QA</a>
</div>
<div class="col-md-12" style="background:white;">
<div class="col-md-10 offset-md-1">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147 !important; } 
.darkblackbackground { background-color:#1a3147; }
.pinktext { color: #ec4159; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.font16 { font-size:16px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
	font-size: 31px !important;
    padding: 8px 0px !important;
    font-weight: 600 !important;
    letter-spacing: -3px !important;
}
.projecttitle{
	font-size: 25px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: 1px solid #000;
    background: unset;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
table {     border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}
.nav-tab-pills-image ul li .nav-link {
    color:#1a3147;
	font-weight:600;
	padding: 7px 50px;
}
</style>
    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 30px;">

                <div class="nav-tab-pills-image">
					<div class="col-sm-9">
						<ul class="nav nav-tabs" role="tablist" style="border-bottom: unset;padding:0px;">                      
							<li class="nav-item active" style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" data-toggle="tab" href="#current_designer" role="tab">
									Current Designer
								</a>
							</li>
							<li class="nav-item" style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" data-toggle="tab" href="#designer_application" role="tab">
									Designer Application
								</a>
							</li>
						</ul>
					</div>
					<div class="col-sm-3">
						<input type="button" class="btn weight600 darkblacktext" style="border-radius:5px;background:#fff;float: right;padding:10px 30px;margin-left: 20px;border:2px solid" value="+ Add Designer"  data-toggle="modal" data-target="#add_designer"/>
						<p class="weight600 darkblacktext font18" style="display:inline-block;float: right;">
							<i class="darkblacktext fa fa-search"></i> Search
						</p>
					</div>
					
                    <div class="tab-content">
                        <div class="tab-pane active content-datatable datatable-width" id="current_designer" role="tabpanel">
                            <div class="row">
								
                                <div class="col-md-12" style="margin-top: 40px;">
								 <?php if($this->session->flashdata('message_error') != '') {?>				
							   <div class="alert alert-danger alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
									<strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
								</div>
							   <?php }?>
							   <?php if($this->session->flashdata('message_success') != '') {?>				
							   <div class="alert alert-success alert-dismissable">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
									<strong><?php echo $this->session->flashdata('message_success');?></strong>
								</div>
							   <?php }?>
                                     <table data-plugin="datatable" data-responsive="true" class="custom-table table table-hover dt-responsive">
                                        <thead>
                                            <tr>
												<th>Designer Name</th>
												<th>Handle Request</th>
												<th>Active Request</th>
												<th>Rating</th>
												<th>Messages</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										<?php for($i=0;$i<sizeof($designers);$i++){ ?>
										   <tr class="trborder">
												<td style="width: 500px;">
													<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;width: 19%;display: inline-block;" />
													<div style="display:inline-block;width:70%;text-align: left;padding-left: 20px;">
														<h4 class="darkblacktext" style="display: inline-block;font-size: 20px;font-weight: 600;"><?php echo $designers[$i]['first_name']." ".$designers[$i]['last_name']?> | </h4>
														<p class="greytext" style="display: inline-block;font-size: 13px;font-weight: 100;padding: 0px;"><?php echo $designers[$i]['email']; ?></p></br>
														<p class="greytext" style="display: inline-block;font-size: 13px;font-weight: 100;">Member since <?php echo date("M d, Y",strtotime($designers[$i]['created']))?></p>
													</div>													
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Requests being handled</p>
													<p class="darkblacktext weight600"><?php echo $designers[$i]['handled']; ?></p>
												</td>
												<td>
													<p class="greytext pb0 weight600 font18">Active Requests</p>
													<p class="darkblacktext weight600"><?php echo $designers[$i]['active_request']; ?></p>
												</td>
												<td style="padding:0px;">
													<p class="greytext pb0 weight600 font18">Rating</p>
													<i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star orangetext"></i><i class="fa fa-star"></i>
												</td>
												<td>
													<i class="fa fa-envelope"></i><p class="greytext pb0 weight600 font18 directmessage" style="display: inline-block;margin-left: 5px;" data-clientname="James Franco" data-clientid="3" data-imageurl="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;">Direct Message</p>
													<a href="<?php echo base_url()."admin/accounts/client_info/".$designers[$i]['id']; ?>"><i class="fa fa-eye"></i></a>
												</td>
											</tr> 
											<?php } ?>
											
											
											
										</tbody>
									</table>
									 
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="designer_application" role="tabpanel">
                            <div class="row">
								

                                <div class="col-md-12" style="margin-top: 40px;">
                                    <div class="col-sm-3" style="padding: 8px;">
										<div class="col-sm-12" style="padding:15px;border: 1px solid;box-shadow: 0px 0px 3px 3px #aaaaaa;">
											<div style="width:100%;height:150px;" class="greybackground"></div>
											<h5 class="darkblacktext weight600">PORTFOLIO IMAGE 1</h5>
											<p class="darkblacktext">Web</p>
											<p class="greytext">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac felis rhoncus, posuere est eget, pretium felis. Ut lacinia condimentum consequat. Phasellus non tempor</p>
											<div class="col-sm-6">
												<p class="darkblacktext weight600 font16">02/23/17</p>
											</div>
											<div class="col-sm-6" style="float:right;">
												<i class="fa fa-trash pinktext font16" style="float:right;"></i>
												<p class="greytext font16" style="display:inline-block;float: right;margin-right: 10px;">EDIT</p>
											</div>
										</div>
									 </div>
									 <div class="col-sm-3" style="padding: 8px;">
										<div class="col-sm-12" style="padding:15px;border: 1px solid;box-shadow: 0px 0px 3px 3px #aaaaaa;">
											<div style="width:100%;height:150px;" class="greybackground"></div>
											<h5 class="darkblacktext weight600">PORTFOLIO IMAGE 1</h5>
											<p class="darkblacktext">Web</p>
											<p class="greytext">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac felis rhoncus, posuere est eget, pretium felis. Ut lacinia condimentum consequat. Phasellus non tempor</p>
											<div class="col-sm-6">
												<p class="darkblacktext weight600 font16">02/23/17</p>
											</div>
											<div class="col-sm-6" style="float:right;">
												<i class="fa fa-trash pinktext font16" style="float:right;"></i>
												<p class="greytext font16" style="display:inline-block;float: right;margin-right: 10px;">EDIT</p>
											</div>
										</div>
									 </div>
									 <div class="col-sm-3" style="padding: 8px;">
										<div class="col-sm-12" style="padding:15px;border: 1px solid;box-shadow: 0px 0px 3px 3px #aaaaaa;">
											<div style="width:100%;height:150px;" class="greybackground"></div>
											<h5 class="darkblacktext weight600">PORTFOLIO IMAGE 1</h5>
											<p class="darkblacktext">Web</p>
											<p class="greytext">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac felis rhoncus, posuere est eget, pretium felis. Ut lacinia condimentum consequat. Phasellus non tempor</p>
											<div class="col-sm-6">
												<p class="darkblacktext weight600 font16">02/23/17</p>
											</div>
											<div class="col-sm-6" style="float:right;">
												<i class="fa fa-trash pinktext font16" style="float:right;"></i>
												<p class="greytext font16" style="display:inline-block;float: right;margin-right: 10px;">EDIT</p>
											</div>
										</div>
									 </div>
									 <div class="col-sm-3" style="padding: 8px;">
										<div class="col-sm-12" style="padding:15px;border: 1px solid;box-shadow: 0px 0px 3px 3px #aaaaaa;">
											<div style="width:100%;height:150px;" class="greybackground"></div>
											<h5 class="darkblacktext weight600">PORTFOLIO IMAGE 1</h5>
											<p class="darkblacktext">Web</p>
											<p class="greytext">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac felis rhoncus, posuere est eget, pretium felis. Ut lacinia condimentum consequat. Phasellus non tempor</p>
											<div class="col-sm-6">
												<p class="darkblacktext weight600 font16">02/23/17</p>
											</div>
											<div class="col-sm-6" style="float:right;">
												<i class="fa fa-trash pinktext font16" style="float:right;"></i>
												<p class="greytext font16" style="display:inline-block;float: right;margin-right: 10px;">EDIT</p>
											</div>
										</div>
									 </div>
									 
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="pendingforapproverequest" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
</div>


<div id="add_designer" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top: 10%;">

    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header" style="display:none;">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modal Header</h4>
		</div>
		<div class="modal-body" style="padding:0px;">
			<div class="col-sm-12" style="padding:30px;">
				<div class="col-sm-12" style="border-bottom: 1px solid;padding-bottom: 20px;">
					<h3 class="darkblacktext weight600 ls0" style="margin-bottom: 20px;">Add Designer</h3>
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="fname" class="darkblacktext weight600"><strong>First Name</strong></label>
						<input type="text" class="form-control" name="first_name" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>	
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Last Name</label>
						<input type="text" class="form-control" name="last_name" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Email</label>
						<input type="text" class="form-control" name="email" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
					<div class="col-sm-6" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Phone</label>
						<input type="text" class="form-control" name="phone" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
				</div>
			
				<div class="col-sm-12" style="margin-top:30px;">		  
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="fname" class="darkblacktext weight600"><strong>Password</strong></label>
						<input type="text" class="form-control" name="password" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>	
					<div class="col-sm-12" style="margin-bottom:10px;">
						<label for="lname" class="darkblacktext weight600">Confirm Password</label>
						<input type="text" class="form-control" name="confirm_password" required style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;">
					</div>
				</div>
				
				<div class="col-sm-12">
					<button type="button" class="btn pinkbackground weight600" style="margin:  auto;height: 60px;font-size:20px;margin-top: 20px;width: 100%;">Add Designer</button>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" style="display:none;">Close</button>
		</div>
	</div>
			
   </div>
	
</div>

<div id="add_portflio" class="modal fade" role="dialog">
  <div class="modal-dialog" style="margin-top: 10%;">

    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header" style="display:none;">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Modal Header</h4>
		</div>
		<div class="modal-body" style="padding:0px;">
			<div class="col-sm-12" style="padding:30px;">
				<div class="col-sm-12">
					<h3 class="darkblacktext weight600 ls0" style="margin-bottom: 20px;">Add Portfolio</h3>
					<div class="col-sm-4 greybackground">
						<a href="#" style="border: 3px solid;border-radius: 73px;padding: 29px 28px 32px 36px;;margin: auto;display: block;width: 100px;margin-top: 14px;" class="greytext">
							<span class="fa fa-plus" style="font-size: 30px;"></span>
						</a>
						<p style="text-align:center;margin-top:10px;font-size: 18px;margin-bottom: 30px;">Add Image</p>
					</div>
					<div class="col-sm-8" style="margin-bottom:10px;">
						<div class="col-sm-12" style="margin-bottom:10px;">
							<label for="fname" class="darkblacktext weight600"><strong>Portfolio Category</strong></label>
							<select class="form-control" name="portfolio_category" style="background-color: #ededed  !important;border-radius: 6px !important;border:unset !important;">
								<option>Web</option>
							</select>
						</div>
						<div class="col-sm-12" style="margin-bottom:10px;">
							<label for="fname" class="darkblacktext weight600"><strong>Title</strong></label>
							<input type="text" class="form-control" name="portfolio_title" style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;" />
						</div>
						<div class="col-sm-12" style="margin-bottom:10px;">
							<label for="fname" class="darkblacktext weight600"><strong>Caption</strong></label>
							<textarea  style="border: 2px solid #c7c9cc !important; border-radius: 6px !important;" name="portfolio_caption" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<div class="col-sm-12">		  
					<div class="col-sm-10" style="margin-bottom:10px;">
						<button type="button" class="btn pinkbackground weight600" style="margin:  auto;width: 50%;height: 50px;font-size:20px;margin-top: 20px;">Publish</button>
						<button type="button" class="btn darkblacktext weight600" style="margin:  auto;width: 40%;height: 50px;font-size:20px;margin-top: 20px;background:#fff;">Preview</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal" style="display:none;">Close</button>
		</div>
	</div>
			
   </div>
	
</div>

<div id="directmessage" class="modal fade" role="dialog" style="margin-top:10%;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body " style="font-size: 170px;height: 270px;">
			
			<h3 style="letter-spacing: -1px;font-size: 21px;display:inline-block;" class="darkblacktext weight600">Direct Message to </h3>
			<h3 style="letter-spacing: -1px;font-size: 21px;display:inline-block;width:50%;" class="darkblacktext weight600 image_name">Direct Message to </h3>
			
			<div class="col-sm-10">
				<textarea class="form-control" style="height: 125px;margin-top: 10px;border: 1px solid !important;"></textarea>
			</div>
			<div class="col-sm-2">
				<i class="fa fa-send pinktext" style="font-size: 35px;"></i>
			</div>
      </div>
    </div>

  </div>
</div>