<a href="<?php echo base_url(); ?>va/dashboard" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Projects</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_clients" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">Clients</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_designer" class="float-xs-left content-title-main" style="padding-left: 10%;">Designers</a>
<a href="<?php echo base_url(); ?>va/dashboard/view_qa" class="float-xs-left content-title-main" style="color: inherit;padding-left: 10%;">QA</a>
</div>
<div class="col-md-12" style="background:white;">
<div class="col-md-10 offset-md-1">
<section id="content-wrapper">
<style>
/* padding css start */
.pb0{ padding-bottom:0px; }
.pb5{ padding-bottom:5px; }
.pb10{ padding-bottom:10px; }
.pt0{ padding-top:0px; }
.pt5{ padding-top:5px; }
.pt10{ padding-top:10px; }
.pl0{ padding-left:0px; }
.pl5{ padding-left:5px; }
.pl10{ padding-left:10px; }
/* padding css end */
.greenbackground { background-color:#98d575; }
.greentext { color:#98d575; }
.orangebackground { background-color:#f7941f; }
.pinkbackground { background-color: #ec4159; }
.orangetext { color:#f7941f; }
.bluebackground { background-color:#409ae8; }
.bluetext{ color:#409ae8; }
.whitetext { color:#fff !important; }
.blacktext { color:#000; }
.greytext { color:#cccccc; }
.greybackground { background-color:#ededed; }
.darkblacktext { color:#1a3147 !important; } 
.pinktext { color: #ff0024;; }
.weight600 { font-weight:600; }
.font18 { font-size:18px; }
.font16 { font-size:16px; }
.textleft { text-align:left; }
.textright { text-align:right; }
.textcenter { text-align:center; }
.pl20 { padding-left:20px; }

.numbercss{
	font-size: 31px !important;
    padding: 8px 0px !important;
    font-weight: 600 !important;
    letter-spacing: -3px !important;
}
.projecttitle{
	font-size: 25px;
    padding-bottom: 0px;
    text-align: left;
    padding-left: 20px;
}
.trborder{
	border: 1px solid #000;
    background: unset;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
table {     border-spacing: 0 1em; }
.trash{     
	color: #ec4159;
    font-size: 25px; 
}
.nav-tab-pills-image ul li .nav-link {
    color:#1a3147;
	font-weight:600;
	padding: 7px 25px;
}
</style>
    <div class="content">
		
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:30px;">         

                <div class="nav-tab-pills-image">
					<div class="col-sm-9">
						<ul class="nav nav-tabs" role="tablist" style="border-bottom: unset;padding:0px;">                      
							<li class="nav-item <?php if($active_tab=="customer"){ echo "active"; } ?>" style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" href="<?php echo base_url(); ?>va/dashboard/chat/customer" role="tab">
									Chat with Client
								</a>
							</li>
							<li class="nav-item  <?php if($active_tab=="designer"){ echo "active"; } ?>" style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" href="<?php echo base_url(); ?>va/dashboard/chat/designer" >
									Chat with Designer
								</a>
							</li>
							 <li class="nav-item <?php if($active_tab=="qa"){ echo "active"; } ?>"style="background: #ededed;border-radius: 7px;margin-left: 1px;">
								<a class="nav-link" href="<?php echo base_url(); ?>va/dashboard/chat/qa">
									Chat with QA/VA
								</a>
							</li>
							<button class="weight600 btn darkblacktext" style="border:1px solid; border-radius:8px;padding: 11px 15px;background: white;margin-left: 2%;">+ Create group Chat</button>
						</ul>
						
					</div>
					<div class="col-sm-3">
						<p class="weight600 darkblacktext font18" style="display:inline-block;float: right;">
							<i class="darkblacktext fa fa-search"></i> Search
						</p>
					</div>
					
                    <div class="tab-content">
                        <div class="tab-pane active content-datatable datatable-width" id="chat_with_client" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                     <div class="col-md-11 offset-md-1" style="margin-top: 40px;">
										<div class="col-md-3" style="max-height:500px;overflow-y:scroll;">
										<?php for($i=0;$i<sizeof($customer);$i++){ ?>
											<a href="<?php echo base_url().'va/dashboard/room/'.$customer[$i]['id']; ?>?user_type=<?php echo $active_tab; ?>">
											<div class="col-md-12" style="background: aliceblue;border-radius: 5px;padding: 10px;">
												<div class="col-md-3" style="padding: 0px;">
													<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
												</div>
												<div class="col-md-9">
													<p class="darkblacktext weight600 font18" style="padding: 0px;"><?php echo $customer[$i]['first_name']." ".$customer[$i]['last_name']; ?></p>
													<p class="greentext weight600 font16" style="padding: 0px;">Online</p>
												</div>
											</div>
											</a>
										<?php } ?>
											<div class="col-md-12" style="border-radius: 5px;padding: 10px;">
												<div class="col-md-3" style="padding: 0px;">
													<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
												</div>
												<div class="col-md-9">
													<p class="darkblacktext weight600 font18" style="padding: 0px;">Clayton Call</p>
													<p class="orangetext weight600 font16" style="padding: 0px;">Standby</p>
												</div>
											</div>
											<div class="col-md-12" style="border-radius: 5px;padding: 10px;">
												<div class="col-md-3" style="padding: 0px;">
													<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
												</div>
												<div class="col-md-9">
													<p class="darkblacktext weight600 font18" style="padding: 0px;">James Franco</p>
													<p class="greytext weight600 font16" style="padding: 0px;">Offline</p>
												</div>
											</div>
										</div>
										
										<div class="col-md-9" style="border-left: 1px solid;">
										<?php if(isset($chat)){ ?>
										<div class="col-md-12 messagediv_<?php echo $room_no; ?>"" style="border-bottom:1px solid;">
										<?php for($i=0;$i<sizeof($chat);$i++){ ?>
											
											<?php if($_SESSION['user_id'] == $chat[$i]['sender_id']){ ?>
												<div class="col-md-2">
													&nbsp;
												</div>
												<div class="col-md-10">
													<div class="col-md-12 greytext" style="margin-top: 20px;background:aliceblue;border-radius:8px;margin-bottom: 10px;width: 60%;float: right;">
														<p class="darkblacktext" style="line-height: 16px;text-align:right;padding-top: 10px;"><?php echo $chat[$i]['message']; ?></p>
														<p class="darkblacktext weight600" style="text-align:right;"><?php 											
														$date=strtotime($chat[$i]['created']);
														$diff=strtotime(date("Y:m:d H:i:s"))-$date;
														$minutes = floor($diff/60);
														$hours = floor($diff/3600);
														if($hours == 0){
															echo $minutes. " Minutes Ago";
														}else{
															$days = floor($diff / (60 * 60 * 24));
															if($days == 0){
																echo $hours. " Hours Ago";
															}else{
																echo $days. " Days Ago";
															}
														}
														?></p>
													</div>
												</div>
											<?php }else{ ?>
												<div class="col-md-2">
													<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
												</div>
												<div class="col-md-10">
													<div class="col-md-12 greytext" style="border:1px solid;border-radius:8px;">
														<p class="darkblacktext weight600" style="font-size: 18px;padding-bottom: 0px;padding-top: 10px;"><?php echo $userdata[$chat[$i]['sender_id']]; ?></p>
														<p class="darkblacktext" style="line-height: 16px;"><?php echo $chat[$i]['message']; ?></p>
														<p class="darkblacktext weight600">
														<?php 											
														$date=strtotime($chat[$i]['created']);
														$diff=strtotime(date("Y:m:d H:i:s"))-$date;
														$minutes = floor($diff/60);
														$hours = floor($diff/3600);
														if($hours == 0){
															echo $minutes. " Minutes Ago";
														}else{
															$days = floor($diff / (60 * 60 * 24));
															if($days == 0){
																echo $hours. " Hours Ago";
															}else{
																echo $days. " Days Ago";
															}
														}
														?></p>
													</div>
												</div>
											<?php } ?>
											
										<?php } ?>
										</div>
											<div class="col-md-12" style="margin-top: 20px;">
												<input type='text' class='form-control room_chat_text' style="border: 1px solid !important;border-radius: 6px !important;" placeholder="Type a message here" />
												<span style="position: absolute;top: 10px;right: 30px;font-size: 18px;" class="pinktext fa fa-send send_room_chat" data-room="<?php echo $room_no; ?>"></span>
											</div>
										<?php } ?>
										</div>
									 </div>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="chat_with_designer" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                     <div class="col-md-11 offset-md-1" style="margin-top: 40px;">
										<div class="col-md-3">
											<div class="col-md-12" style="background: aliceblue;border-radius: 5px;padding: 10px;">
												<div class="col-md-3" style="padding: 0px;">
													<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
												</div>
												<div class="col-md-9">
													<p class="darkblacktext weight600 font18" style="padding: 0px;">Rey</p>
													<p class="greentext weight600 font16" style="padding: 0px;">Online</p>
												</div>
											</div>
											<div class="col-md-12" style="border-radius: 5px;padding: 10px;">
												<div class="col-md-3" style="padding: 0px;">
													<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
												</div>
												<div class="col-md-9">
													<p class="darkblacktext weight600 font18" style="padding: 0px;">Justin</p>
													<p class="orangetext weight600 font16" style="padding: 0px;">Standby</p>
												</div>
											</div>
											<div class="col-md-12" style="border-radius: 5px;padding: 10px;">
												<div class="col-md-3" style="padding: 0px;">
													<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
												</div>
												<div class="col-md-9">
													<p class="darkblacktext weight600 font18" style="padding: 0px;">Ken</p>
													<p class="greytext weight600 font16" style="padding: 0px;">Offline</p>
												</div>
											</div>
										</div>
										<div class="col-md-9" style="border-left: 1px solid;">
											<div class="col-md-12" style="border-bottom:1px solid;">
												<div class="col-md-2">
													<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
												</div>
												<div class="col-md-10">
													<div class="col-md-12 greytext" style="border:1px solid;border-radius:8px;">
														<p class="darkblacktext weight600" style="font-size: 18px;padding-bottom: 0px;padding-top: 10px;">Rey</p>
														<p class="darkblacktext" style="line-height: 16px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
														<p class="darkblacktext weight600">4 Hours Ago</p>
													</div>
													<div class="col-md-12 greytext" style="margin-top: 20px;background:aliceblue;border-radius:8px;margin-bottom: 10px;width: 60%;float: right;">
														<p class="darkblacktext" style="line-height: 16px;text-align:right;padding-top: 10px;">Lorem ipsum dolor sit amet, consectetur  adipiscing elit...</p>
														<p class="darkblacktext weight600" style="text-align:right;">4 Hours Ago</p>
													</div>
												</div>
											</div>
											<div class="col-md-12" style="margin-top: 20px;">
												<input type='text' class='form-control' style="border: 1px solid !important;border-radius: 6px !important;" placeholder="Type a message here" />
												<span style="position: absolute;top: 10px;right: 30px;font-size: 18px;" class="pinktext fa fa-send"></span>
											</div>
										</div>
									 </div>
                                </div>
                            </div>
                        </div>
						
						<div class="tab-pane content-datatable datatable-width" id="chat_with_qa_va" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-11 offset-md-1" style="margin-top: 40px;">
										<div class="col-md-3">
											<div class="col-md-12" style="background: aliceblue;border-radius: 5px;padding: 10px;">
												<div class="col-md-3" style="padding: 0px;">
													<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
												</div>
												<div class="col-md-9">
													<p class="darkblacktext weight600 font18" style="padding: 0px;">Melanie</p>
													<p class="greentext weight600 font16" style="padding: 0px;">Online</p>
												</div>
											</div>
											<div class="col-md-12" style="border-radius: 5px;padding: 10px;">
												<div class="col-md-3" style="padding: 0px;">
													<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
												</div>
												<div class="col-md-9">
													<p class="darkblacktext weight600 font18" style="padding: 0px;">Sherri</p>
													<p class="orangetext weight600 font16" style="padding: 0px;">Standby</p>
												</div>
											</div>
										</div>
										<div class="col-md-9" style="border-left: 1px solid;">
											<div class="col-md-12" style="border-bottom:1px solid;">
												<div class="col-md-2">
													<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
												</div>
												<div class="col-md-10">
													<div class="col-md-12 greytext" style="border:1px solid;border-radius:8px;">
														<p class="darkblacktext weight600" style="font-size: 18px;padding-bottom: 0px;padding-top: 10px;">Melanie</p>
														<p class="darkblacktext" style="line-height: 16px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
														<p class="darkblacktext weight600">4 Hours Ago</p>
													</div>
													<div class="col-md-12 greytext" style="margin-top: 20px;background:aliceblue;border-radius:8px;margin-bottom: 10px;width: 60%;float: right;">
														<p class="darkblacktext" style="line-height: 16px;text-align:right;padding-top: 10px;">Lorem ipsum dolor sit amet, consectetur  adipiscing elit...</p>
														<p class="darkblacktext weight600" style="text-align:right;">4 Hours Ago</p>
													</div>
												</div>
											</div>
											<div class="col-md-12" style="margin-top: 20px;">
												<input type='text' class='form-control' style="border: 1px solid !important;border-radius: 6px !important;" placeholder="Type a message here" />
												<span style="position: absolute;top: 10px;right: 30px;font-size: 18px;" class="pinktext fa fa-send"></span>
											</div>
										</div>
									 </div>
                                </div>
                            </div>
                        </div>
						
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
</div>