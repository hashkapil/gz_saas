<!DOCTYPE html>
<html>
    <head>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?php
            if (isset($page_title)):
                echo $page_title;
            else:
                //echo $this->fetch('title');
            endif;
            ?>            
        </title>

<!-- START GLOBAL CSS -->
<link rel="stylesheet" href="/theme/assets/global/plugins/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="/theme/assets/icons_fonts/elegant_font/elegant.min.css">
<link rel="stylesheet" href="/theme/assets/layouts/layout-left-top-menu/css/color/light/color-default.css">
<link rel="stylesheet" href="/theme/assets/global/plugins/switchery/dist/switchery.min.css">
<link rel="stylesheet" href="/theme/assets/global/plugins/perfect-scrollbar/css/perfect-scrollbar.min.css">
<link rel="stylesheet" href="/theme/assets/global/plugins/dropify/dist/css/dropify.min.css">
<link rel="stylesheet" href="/theme/assets/global/plugins/dropify/dist/css/dropify.min.css">
<link rel="stylesheet" href="/theme/assets/global/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/theme/assets/global/plugins/bootstrap-wysiwyg/css/code_view.css">
<link rel="stylesheet" href="/theme/assets/global/plugins/bootstrap-wysiwyg/css/image_manager.css">
<link rel="stylesheet" href="/theme/assets/global/plugins/bootstrap-wysiwyg/css/image.css">
<link rel="stylesheet" href="/theme/assets/global/plugins/bootstrap-wysiwyg/css/table.css">
<link rel="stylesheet" href="/theme/assets/global/plugins/bootstrap-wysiwyg/css/video.css">
<!-- END GLOBAL CSS -->

<link rel="stylesheet" href="/theme/assets/global/plugins/datatables/media/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="/theme/assets/global/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="/theme/assets/global/plugins/datatables-scroller/css/scroller.bootstrap4.min.css">
<!-- Include Editor style. -->

<link rel="stylesheet" href="/theme/assets/layouts/layout-left-menu/css/color/light/color-spice.min.css">
<link rel="stylesheet" href="/theme/assets/layouts/layout-left-menu/css/color/light/color-dodger-blue.min.css">

<!-- START TEMPLATE GLOBAL CSS -->
<link rel="stylesheet" href="/theme/assets/global/css/components.min.css">
<link rel="stylesheet" href="/theme/assets/layouts/layout-left-menu/css/layout.min.css">

<!-- START CUSTOME CSS -->
<link rel="stylesheet" href="/theme/assets/global/css/style-datatable.css">
<link rel="stylesheet" href="/theme/assets/global/css/style-datatable.css">
<link rel="stylesheet" href="/public/css/admin_custom.css">

<link rel="stylesheet" href="/public/css/slick.css">
<link rel="stylesheet" href="/public/css/slick-theme.css">
</head>
<body>
<style>
.dropbtn {
    background-color: inherit;
    font-size: 16px;
    border: none;
    cursor: pointer;
	margin-top: 30px;
}
*:focus {
    outline: none;
}
.dropbtn:hover, .dropbtn:focus {
    
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    overflow: auto;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}


.show {display:block;}
.dropdown button i {
    border: 1px solid;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: 3px;
	margin-left: 5px;
}
.down {
    transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
}
/*Header signup button*/
li.signlink a:hover {
    color:white !important;
    background:#f23f5b !important;
    border: 1px solid #f23f5b !important;
}
li.hoverred a:hover{
	border-bottom:1px solid #f23f5b;
	color:#1A3147 !important;
	opacity:1;
}
</style>
	<div class="wrapper">
		<section id="main" class="container-fluid">
			<div class="content-cont">
				<div class="row">
						<!-- filter sidebar
					<div id="filter-sidebar" class="col-xs-6 col-sm-3 visible-sm visible-md visible-lg collapse sliding-sidebar" style="padding:0px;">-->
					<style>
					.btn-secondary{
						color: #1a3147 !important;
						border: unset;
						background: unset;
						font-weight:600;
						font-size: 17px;
						margin-top: 30px;
						
					}
					a.dropdown-item {
						font-weight: 600;
						font-size: 18px !important;
						background: #fff;
						padding-bottom: 10px;
					}
					.btn-secondary:hover {
						color: #373a3c;
						background-color: #f6f7fb !important;
						border-color: #ffffff !important;
					}
					.notdropdown:after{
						border-right: unset !important;
						border-left: unset !important;
					}
					</style>
					<div class="col-md-10 offset-md-1" style="margin-top: 20px;">
						<div class="col-sm-9">
							<div class="col-sm-2">
								<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="max-width:100%;" />
							</div>
							<div class="dropdown">
								<a href="<?php echo base_url(); ?>va/dashboard">
							  <button class="btn btn-secondary dropdown-toggle notdropdown" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false">
								Dashboard
							  </button>
							  </a>
							</div>
							<div class="dropdown">
								<a href="<?php echo base_url(); ?>va/dashboard/rating">
							  <button class="btn btn-secondary dropdown-toggle notdropdown" id="dropdownMenuButton" aria-haspopup="true" aria-expanded="false">
								Rating
							  </button>
							  </a>
							</div>
						</div>
						<div class="col-sm-3" style="float:right;">
							<div class="dropdown">
							  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Chat
							  </button>
							  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="<?php echo base_url(); ?>va/dashboard/chat/designer">Chat With Designer</a>
								<a class="dropdown-item" href="<?php echo base_url(); ?>va/dashboard/chat/customer">Chat With Customer</a>
								<a class="dropdown-item" href="<?php echo base_url(); ?>va/dashboard/chat/qa">Chat With QA</a>
								<a class="dropdown-item" href="<?php echo base_url(); ?>customer/request/log_out">Logout</a>
							  </div>
							</div>
							
							<i class="fa fa-user" style="color: black;font-size: 20px;padding: 30px 10px;float:right;"></i>
							<i class="fa fa-bell" style="color: black;font-size: 20px;padding: 30px 10px;float:right;"></i>
							<i class="fa fa-envelope" style="color: black;font-size: 20px;padding: 30px 10px;float:right;"></i>
						</div>
					</div>
					
					<div class="col-md-10 offset-md-1" style="margin-top: 50px;">
					