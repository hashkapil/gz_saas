<html>
    <head>
        <style>
            .error_wrapper {
                /* width: 100vw; */
                /* height: 100vh; */
                display: flex;
                align-items: center;
            }
            .error_message_dueto_prmsn {
                margin: 50px auto;
                display: inline-block;
                font-family: 'Poppins', sans-serif;
                width: 90%;
            }
            .error_message_dueto_prmsn h3 {
                font-size: 40px;
                font-weight: normal;
                color: #444;
                margin-bottom: 15px;
            }
            .error_message_dueto_prmsn p {
                font-size: 20px;
                color: #666;
            }
        </style>
        <title>Access Denied | Graphics Zoo</title>
    </head>
    <body>
        <div class="row">
            <div class="error_wrapper">
                <div class="error_message_dueto_prmsn">
                    <h3>Sorry, the link has expired or broken.</h3>

                    <p>  Please contact the person who shared this link with you.</p>
                </div>
            </div>
            
        </div>
    </body>
</html>
