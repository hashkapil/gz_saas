<style type="text/css">
	body{
		background: #fafafa;
	}
</style>

<p class="space-b"></p>
	
	<section class="con-b">
		<div class="container">
		
			<div class="row">
				<div class="col-md-4">
					<div class="project-row-qq1 settingedit-box">
						<form method="post" enctype="multipart/form-data" action="">
							<div class="settrow">
								
								<div class="settcol right">
									<p class="btn-x"><input type="submit" class="btn-e" value="Save" name="submit"></p> 
								</div>
							</div>
							
							<div class="setimg-row33">
								<div class="imagemain2" style="padding-bottom: 20px; display: none;">
								        <input type="file" onChange="validateAndUpload(this);" class="form-control dropify waves-effect waves-button" name="qaprofilepic" style="display:none;border: 2px" id="inFile"/>
								    </div>
								<figure class="setimg-box33 imagemain">
									<img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $edit_profile[0]['profile_picture']; ?>" class="img-responsive telset33">
									<a class="setimg-blog33" href="javascript:void(0);" onclick="$('.dropify').click();">
										<span class="setimgblogcaps">
											<img src="<?php echo base_url(); ?>theme/assets/qa/images/icon-camera.png" class="img-responsive"><br>
											<span class="setavatar33">Change <br>Avatar</span>
										</span>
									</a>
								</figure>
								<figure class="setimg-box33 imagemain3" style="display: none;">
									<img src="" class="img-responsive telset33" id="image3">
									<a class="setimg-blog33" href="javascript:void(0);" onclick="$('.dropify').click();">
										<span class="setimgblogcaps">
											<img src="<?php echo base_url(); ?>theme/assets/qa/images/icon-camera.png" class="img-responsive"><br>
											<span class="setavatar33">Change <br>Avatar</span>
										</span>
									</a>
								</figure>
							</div>
							<p class="space-a"></p>
							<div class="form-group setinput">
								<input type="text" class="form-control input-b" placeholder="Shawn Alley" value="<?php echo $edit_profile[0]['first_name']." ".$edit_profile[0]['last_name']?>" name="fndlname">
							</div>
							
							<div class="form-group setinput">
								<input type="text" class="form-control input-b" placeholder="<?php echo $edit_profile[0]['email']; ?>" name="email" value="<?php echo $edit_profile[0]['email']; ?>">
								<p class="seter"><span>Administrative Email</span></p>
							</div>
							<p class="space-b"></p>
							<p class="chngpasstext"><a href="javascript:void(0)" class="upl-oadfi-le noborder" data-toggle="modal" data-target="#ChangePasswordModel"><img src="<?php echo base_url(); ?>theme/assets/qa/images/change-password.png" class="img-responsive"> <span>Change Password</span></a></p>
						</form>
					</div>	
				</div>
				
				<div class="col-md-4">
					<div class="project-row-qq1 settingedit-box">
						<form method="post" action="<?php echo base_url(); ?>qa/dashboard/editqadetaiL">
							<div class="settrow">
								<div class="settcol left">
									<h3 class="sub-head-b">About Info</h3>
								</div>
								<div class="settcol right">
									<p class="btn-x"><input type="submit" class="btn-e" value="Save"></p> 
								</div>
							</div>
							<p class="space-d"></p>
							
								
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<input type="text" class="form-control input-c" placeholder="Paypal Id" value="<?php echo $edit_profile[0]['paypal_email_address']; ?>" name="paypalid">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control input-c" placeholder="Title" value="Title" name="title" >
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control input-c" placeholder="Phone" value="<?php echo $edit_profile[0]['phone']; ?>" name="phone">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<textarea class="form-control textarea-b" placeholder="Address" value="<?php echo $edit_profile[0]['address_line_1']; ?>" name="address"></textarea>
									</div>
								</div>
								
								<div class="col-sm-12">
									<div class="form-group">
										<input type="text" class="form-control input-c" placeholder="City" value="<?php echo $edit_profile[0]['city']; ?>" name="city">
									</div>
								</div>
								
								<div class="col-sm-12">
									<div class="form-group">
										<input type="text" class="form-control input-c" placeholder="State" value="<?php echo $edit_profile[0]['state']; ?>" name="state">
									</div>
								</div>
								
								<div class="col-sm-12">
									<div class="form-group">
										<input type="text" class="form-control input-c" placeholder="Zip" value="<?php echo $edit_profile[0]['zip']; ?>" name="zip">
									</div>
								</div>
								
							</div>
						
						</form>
					</div>	
				</div>
				
				<!--<div class="col-md-4">
					<div class="project-row-qq1 settingedit-box">
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-b">Billing and Subscription</h3>
							</div>
						</div>
						<p class="space-b"></p>
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-g">Current Plan</h3>
							</div>
							<div class="settcol right">
								<p class="setrightstst">Cycle: May 01,2018-June 01, 2018</p> 
							</div>
						</div>
						<p class="space-a"></p>
						
						<div class="cyclebox25">
							<div class="settrow">
								<div class="settcol left">
									<h3 class="sub-head-g">Monthly Golden Plan</h3>
									<p class="setpricext25">$259</p>
								</div>
								<div class="settcol right">
									<p class="btn-x"><button class="btn-f chossbpanss">Change Plan</button></p>
								</div>
							</div>
						</div>
						<p class="space-d"></p>
						
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-b">Billing Method</h3>
							</div>
							<div class="settcol right">
								<p class="btn-x"><a href="setting-view.html" class="btn-e">Save</a></p>
							</div>
						</div>
						
						<p class="space-c"></p>
						
						<div class="form-group">
							<label class="label-x2">Paypal Id</label>
							<p class="space-a"></p>
							<div class="row">
								<div class="col-md-10">
									<input type="text" class="form-control input-c" placeholder="xxxx-xxxx-xxxx">
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Expiry Date">
								</div>
							</div>
							
							<div class="col-sm-4 col-sm-offset-1">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="CVC">
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Name">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Zip Code">
								</div>
							</div>
						</div>
						
					</div>	
				</div> -->
				
			</div>
			
		</div>
	</section>
	
	<!-- Modal -->
	<div class="modal fade" id="ChangePasswordModel" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				
				<div class="cli-ent-model-box">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div class="cli-ent-model">
						<header class="fo-rm-header">
							<h3 class="head-c">Change Password</h3>
						</header>
						<div class="fo-rm-body">
							<form action="<?php echo base_url(); ?>qa/dashboard/change_password_old" method="post">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group goup-x1">
										<label class="label-x3">Current Password</label>
										<p class="space-a"></p>
										<input type="password" required class="form-control input-c" name="old_password">
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group goup-x1">
										<label class="label-x3">New Password</label>
										<p class="space-a"></p>
										<input type="password" required class="form-control input-c" name="new_password">
									</div>
								</div>
								<div class="col-sm-12">
									<div class="form-group goup-x1">
										<label class="label-x3">Confirm Password</label>
										<p class="space-a"></p>
										<input type="password" required class="form-control input-c" name="confirm_password">
									</div>
								</div>
							</div>
							
							<p class="btn-x"><button type="submit" class="btn-g">Save</button></p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<?php echo base_url(); ?>theme/assets/global/plugins/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript">
		function validateAndUpload(input){
			var URL = window.URL || window.webkitURL;
			var file = input.files[0];
			if(file){
				console.log(URL.createObjectURL(file));
				$(".imagemain").hide();
				$(".imagemain3").show();
				$("#image3").attr('src', URL.createObjectURL(file));
				$(".dropify-wrapper").show();
			}
		}
	</script>