<style type="text/css">
    ul.list-header-blog li.active a{
        color: #e52344;
        border-bottom: 2px solid #e52344;
    }
</style>
<section class="con-b">
    <div class="container">
        <?php if($this->session->flashdata('message_error') != '') {?>              
           <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>              
            </div>
        <?php }?>
        <?php if($this->session->flashdata('message_success') != '') {?>                
           <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong><?php echo $this->session->flashdata('message_success');?></strong>
            </div>
       <?php }?>
        <div class="header-blog">
            <div class="row flex-show">
                <div class="col-md-8">
                    <ul class="list-unstyled list-header-blog">
                        <li class="active"><a href="javascript:void(0)">Active (<?php echo $countcustomer; ?>)</a></li>        
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="search-box">
                        <form method="post" class="search-group clearfix">
                            <input type="text" placeholder="Search here..." class="form-control searchdata" id="search_text">
                            <button type="submit" class="search-btn search search_data_ajax">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                               <!--  <svg class="icon">
                                    <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-magnifying-glass"></use>
                                </svg> -->
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <p class="space-e"></p>
        
        <div class="cli-ent table" id="clients">
            <?php for ($i = 0; $i < count($data); $i++) {
                ?>
                <!-- Start Row -->
                <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                    <div class="cli-ent-col td" style="width: 35%;"  onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'" >
                        <div class="cli-ent-xbox text-left">
                            <div class="cell-row">
                                <div class="cell-col" style="width: 80px; padding-right: 15px;">
                                    <?php if ( ($data[$i]['profile_picture'] != "") && (file_exists(base_url()."uploads/profile_picture/".$data[$i]['profile_picture']))){ ?>
                                        <figure class="cli-ent-img circle one">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$data[$i]['profile_picture'];?>" class="img-responsive one">
                                        </figure>
                                    <?php }else{ ?>
                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 20px;">
                                    <?php echo ucwords(substr($data[$i]['first_name'],0,1)) .  ucwords(substr($data[$i]['last_name'],0,1)); ?>
                                        </figure>
                                    <?php } ?>
                                </div>
                                
                                <div class="cell-col" style="width: 185px;">
                                    <h3 class="pro-head-q"><a href="<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>"><?php echo $data[$i]['first_name'] . " " . $data[$i]['last_name'] ?></a></h3>
                                    <p class="pro-a"> <?php echo $data[$i]['email'];?></p>
                                </div>
                                
                                <div class="cell-col">
                                    <p class="neft">
                                        <span class="blue text-uppercase">
                                            <?php if ($data[$i]['plan_turn_around_days'] == "1") { 
                                                echo "Premium Member"; 
                                            }elseif ($data[$i]['plan_turn_around_days'] == "3") {
                                                echo "Standard Member";
                                            }else{
                                                echo "No Member";
                                            } ?>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 8%;"  onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'" >
                        <div class="cli-ent-xbox text-center">
                            <p class="word-wrap-one">Sign Up Date</p>
                            <p class="space-a"></p>
                            <p class="pro-b"><?php echo date("M/d/Y", strtotime($data[$i]['created'])) ?></p>
                        </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 12%;"  onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'" >
                        <div class="cli-ent-xbox text-center">
                            <p class="word-wrap-one">Next Billing Date</p>
                            <p class="space-a"></p>
                            <p class="pro-b">
                                <?php
                                    $date = "";
                                    if (checkdate(date("m", strtotime($data[$i]['modified'])), date("d", strtotime($data[$i]['modified'])), date("Y", strtotime($data[$i]['modified'])))) {
                                        $over_date = "";
                                        if ($data[$i]['plan_turn_around_days']) {
                                            $date = date("Y-m-d", strtotime($data[$i]['modified']));
                                            $time = date("h:i:s", strtotime($data[$i]['modified']));
                                            $data[$i]['plan_turn_around_days'] = "2";
                                            $date = date("m/d/Y g:i a", strtotime($date . " " . $data[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                            $diff = date_diff(date_create(date("Y-m-d", strtotime($data[$i]['modified']))), date_create($date));
                                        }
                                    }
                                    $current_date = strtotime(date("Y-m-d"));
                                    $now = time(); // or your date as well

                                    $expiration_date = strtotime($date);
                                    $datediff = $now - $expiration_date;
                                    $date_due_day = round($datediff / (60 * 60 * 24));
                                    $color = "";
                                    $text_color = "white";
                                    if ($date_due_day == 0) {
                                        $date_due_day = "Due  </br>Today";
                                        $color = "#f7941f";
                                    } else
                                    if ($date_due_day == (-1)) {
                                        $date_due_day = "Due  </br>Tomorrow";
                                        $color = "#98d575";
                                    } else
                                    if ($date_due_day > 0) {
                                        $date_due_day = "Over Due";
                                        $color = "red";
                                    } else if ($date_due_day < 0) {
                                        $date_due_day = "Due from </br>" . number_format($date_due_day) . " days";
                                        $text_color = "black";
                                    }
                                    ?>
                            <?php echo date_format(date_create($date), "M /d /Y"); ?></p>
                        </div>
                    </div>
                    
                    <div class="cli-ent-col td" style="width: 5%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                            <div class="cli-ent-xbox text-center">
                                    <p class="word-wrap-one">Active</p>
                                    <p class="space-a"></p>
                                    <p class="pro-a"><?php echo $data[$i]['active']; ?></p>
                            </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                            <div class="cli-ent-xbox text-center">
                                    <p class="word-wrap-one">In Queue</p>
                                    <p class="space-a"></p>
                                    <p class="pro-a"><?php echo $data[$i]['inque_request']; ?></p>
                            </div>
                    </div>
                   <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                            <div class="cli-ent-xbox text-center">
                                    <p class="word-wrap-one">Pending Approval</p>
                                    <p class="space-a"></p>
                                    <p class="pro-a"><?php echo $data[$i]['review_request']; ?></p>
                            </div>
                    </div>
                    <div class="cli-ent-col td" style="width: 15%;">
					
						<?php if(!empty($data[$i]['designer'])){ ?>
							<div class="cli-ent-xbox">
								<div class="cell-row">
									<div class="cell-col" style="width: 36px; padding-right: 10px;">
										<a href="">
											<?php if($data[$i]['designer'][0]['profile_picture'] != ""){ ?>
												<figure class="pro-circle-img">
													<img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$data[$i]['designer'][0]['profile_picture'] ;?>" class="img-responsive">
												</figure>
											<?php }else{ ?>
												<figure class="pro-circle-img">
													<img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
												</figure>
											<?php } ?>
										</a>
									</div>
									<div class="cell-col">
										<p class="text-h text-wrap" title="<?php echo $data[$i]['designer'][0]['first_name'] . " " . $data[$i]['designer'][0]['last_name']; ?>
												">
											<?php echo $data[$i]['designer'][0]['first_name'];?>
											<?php 
											if(strlen($data[$i]['designer'][0]['last_name']) > 5){ 
												echo ucwords(substr($data[$i]['designer'][0]['last_name'],0,1)); 
											}else{ 
												echo $data[$i]['designer'][0]['last_name']; 
											} ?>
												
										</p>
										<p class="pro-b">Designer</p>
										<p class="space-a"></p>
										<a href="#" class="addde-signersk1 permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $data[$i]['id']; ?>">
											<span class="sma-red">+</span> Add Designer
										 </a>
									</div>
								</div> 
							</div>
						<?php }else{ ?>
						<div class="cli-ent-xbox">
							<a href="#" class="upl-oadfi-le noborder permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign" data-customerid="<?php echo $data[$i]['id']; ?>">
								<span class="icon-crss-3">
									<span class="icon-circlxx55 margin5">+</span>
									<p class="attachfile">Assign designer<br> permanently</p>
								</span>
							</a>
						</div>
						<?php }?>
                    </div>
                </div>
                <!-- End Row -->
            <?php } ?>            
        </div>
        
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="AddPermaDesign" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            
            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c text-center">Add Designer</h3>
                    </header>
                    <div class="noti-listpopup">
                        <div class="newsetionlist">
                            <div class="cli-ent-row tr notificate">
                                <div class="cli-ent-col td" style="width: 30%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Designer</h3>
                                    </div>
                                </div>
                                
                                <div class="cli-ent-col td" style="width: 45%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Skill</h3>
                                    </div>
                                </div>
                                
                                <div class="cli-ent-col td" style="width: 25%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b text-center">Active Requests</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="<?php echo base_url();?>qa/dashboard/assign_designer_for_customer" method="post">
                            <ul class="list-unstyled list-notificate">
                                <?php foreach ($all_designers as $value): ?>
                                <li>
                                    <a href="#">
                                        <div class="cli-ent-row tr notificate">
                                            <div class="cli-ent-col td" >
                                                <div class="radio-boxxes">
                                                    <label class="containerones">
                                                      <input type="radio" value="<?php echo $value['id'];?>" name="assign_designer" id="<?php echo $value['id'];?>" data-image-pic="<?php echo $value['profile_picture']?>" data-name="<?php echo $value['first_name'] ." ". $value['last_name'];?>">
                                                      <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            
                                            <div class="cli-ent-col td" style="width: 30%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <div class="setnoti-fication">
                                                        <figure class="pro-circle-k1">
                                                        <?php if($value['profile_picture'] != ""){?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$value['profile_picture']?>" class="img-responsive">
                                                        <?php }else{ ?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                        <?php } ?>
                                                        </figure>
                                                        
                                                        <div class="notifitext">
                                                            <p class="ntifittext-z1"><strong><?php echo $value['first_name'] ." ". $value['last_name'];?></strong></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="cli-ent-col td" style="width: 45%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <p class="pro-a">UX Designer, Landing page, Mobile App  UX Designer, Landing page, Mobile App<span class="sho-wred">+2</span></p>
                                                </div>
                                            </div>
                                            
                                            <div class="cli-ent-col td" style="width: 25%;">
                                                <div class="cli-ent-xbox text-left">
                                                    <div class="cli-ent-xbox text-center">
                                                        
                                                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $value['active_request'];?></span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <?php endforeach; ?>
                                
                            </ul>
                            <input type="hidden" name="customer_id" id="assign_customer_id">
                            <p class="space-c"></p>
                            <p class="btn-x text-center"><button name="submit" type="submit" class="btn-g minxx1" id="assign_designer_per" >Assign Designer</button></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
    $(document).on('click', ".permanent_assign_design", function(){
            var request_id = $(this).attr('data-requestid');
            var customer_id = $(this).attr('data-customerid');
            $('#AddPermaDesign #assign_request_id').val(request_id);
            $('#AddPermaDesign #assign_customer_id').val(customer_id);
        });
     $('.searchdata').keyup(function(e){
            var search = $('#search_text').val();
            $.get( "<?php echo base_url();?>qa/dashboard/search_ajax_client?name="+search, function( data ){
               $( "#clients" ).html( data );  
            });
           
        } );
        $('.search_data_ajax').click(function(e){
            e.preventDefault();
             $('.searchdata').keyup();
        });

        $(document).ready(function() {
            var total_record = 0;
            var total_groups = <?php echo $countcustomer ;?>;  
            $('#clients').load("<?php echo base_url() ?>qa/dashboard/load_more_customer",
             {'group_no':total_record}, function(){
                total_record++;
            });
            $(window).scroll(function() {  
                if($(window).scrollTop()+$(window).height() >= $('#footer_div').offset().top)  
                {           
                    if(total_record < total_groups)
                    { 
                      $.post('<?php echo site_url() ?>qa/dashboard/load_more_customer',{'group_no': total_record},
                        function(data){ 
                            if (data != "") {                               
                                $('#clients').append(data);               
                                total_record++;
                            }
                        });     
                    }
                }
            });
        });
</script>