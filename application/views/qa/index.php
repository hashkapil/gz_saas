<style type="text/css">
.product-list-show .cli-ent-col.td .cell-col.priority-sec {
    background: #99a7b0;
    width: 30px;
    text-align: center;
    border-radius: 15px;
    color: #fff;
    font-size: 13px;
    padding-top: 1px;margin: 0 !important;
    margin-left: 4px !important;
    font: normal 11px/18px 'GothamPro', sans-serif;
}
.product-list-show .cli-ent-col.td.col-w1 {
    display: inline-flex;
    align-items: center;
    flex-direction: row !important;
}
.similar-prop .cli-ent-model form input.edit_expected {
    padding: 8px;
    border-radius: 6px;
    border: 1px solid #ccc;
}
.similar-prop .cli-ent-model form {
    margin-top: 30px;
}
.expectd_date {
    font: normal 14px/20px 'GothamPro', sans-serif;
}
.product-list-show .cli-ent-col.td .cli-ent-xbox {
    font: normal 11px/20px 'GothamPro', sans-serif;
}
.product-list-show .cli-ent-col.td .cli-ent-xbox a.editexpected {
    font-size: 18px;
    color: #1a3147;
}
.cell-col p.pro-b b {
font-weight: normal;
margin-bottom: 2px;
display: inline-block;
}
    .setnoti-fication{
        display: inline-flex;
    }
/**end newdesign css**/
    ul.list-header-blog li.active a{
        border-bottom: 2px solid #e52344;
    }
    .search-box {
        position: absolute;
        top: -143px;
        right: -91px;
    }
    .ajax_loader{
    margin-top:20px;
}
    .ajax_searchload {
     position: absolute;
       top: 19px;
    RIGHT: 96PX;
    }
    a.load_more.button {
        background-color: #e8304d;
        border: none;
        border-radius: 50px;
        color: white!important;
        padding: 8px 45px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 17px;
        margin: -5px 0;
        cursor: pointer;
        text-decoration: none !important;
        margin-top: 20px;
        font-family: 'Montserrat', sans-serif;
    }
    .nodata {
    text-align: center;
    padding: 25px;
}
.fail {
    margin-top: 6px;
    color: #f00;
    font-weight: bold;
}
.margin5{
    margin-bottom:5px; 
}
h4.head-c.draft_no {
    font: normal 13px/24px 'GothamPro', sans-serif;
}
.alert strong{
    font-family:'GothamPro', sans-serif;
}
</style>
<?php //print_r($data);exit;?>
<section class="con-b">
        <div class="container">
                <div class="header-blog">
            <?php if ($this->session->flashdata('message_error') != '') { ?>
                <div id="message" class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_error'); ?>
                    </p>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('message_success') != '') { ?>
                <div id="message" class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_success'); ?>
                    </p>
                </div>
            <?php } ?>
            <input type="hidden" value="" class="bucket_type"/>
            <div class="row flex-show">
                <div class="col-md-10">
                    <ul class="list-unstyled list-header-blog top-tabs"  style="border:none;">
                        <li class="<?php echo ($bucket_type == '') ? 'active' : ''; ?>" id="all_pro">
                            <a href="<?php echo base_url(); ?>qa/dashboard">All Projects</a>
                        </li>
                        <li class="<?php echo ($bucket_type == '2') ? 'active' : ''; ?>" id="creative_pro">
                            <a href="<?php echo base_url(); ?>qa/dashboard/index/2">Creative Project </a>
                        </li>  
                        <li class="<?php echo ($bucket_type == '1') ? 'active' : ''; ?>" id="small_pro">
                            <a href="<?php echo base_url(); ?>qa/dashboard/index/1">Artwork</a>
                        </li>         
                    </ul>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
            <div class="header-blog">
                <div class="row flex-show">
                    <div class="col-md-10">
                        <ul id="status_check" class="list-unstyled list-header-blog small" role="tablist">
                            <li class="active" id="1">
                                <a data-toggle="tab" href="#Qa_active" data-status="active,disapprove" data-pagin ="<?php echo isset($incoming_request_count)?$incoming_request_count:'0'; ?>" role="tab">Active (<?php echo isset($incoming_request_count)?$incoming_request_count:'0'; ?>)</a>
                            </li>         
                            <li id="2">
                                <a data-toggle="tab" href="#Qa_in_queue" data-status="assign" data-pagin ="<?php echo isset($ongoing_request_count)?$ongoing_request_count:'0'; ?>" role="tab">In-Queue (<?php echo isset($ongoing_request_count)?$ongoing_request_count:'0'; ?>)</a>
                            </li>       
                            <li id="3">
                                <a data-toggle="tab" href="#Qa_pending_review" data-status="pendingrevision" data-pagin ="<?php echo isset($pending_review_request_count)?$pending_review_request_count:'0'; ?>" role="tab">Pending Review (<?php echo isset($pending_review_request_count)?$pending_review_request_count:'0'; ?>) </a>
                            </li>      
                            <li id="4">
                                <a data-toggle="tab" href="#Qa_pending_approval" data-status="checkforapprove" data-pagin ="<?php echo isset($pending_request_count)?$pending_request_count:'0'; ?>" role="tab">Pending Approval (<?php echo isset($pending_request_count)?$pending_request_count:'0'; ?>) </a>
                            </li>       
                            <li id="5">
                                <a data-toggle="tab" href="#Qa_completed" data-status="approved" data-pagin ="<?php echo isset($approved_request_count)?$approved_request_count:'0'; ?>" role="tab">Completed (<?php echo isset($approved_request_count)?$approved_request_count:'0'; ?>)</a>
                            </li>   
                            <li id="6">
                                <a data-toggle="tab" href="#Qa_hold" data-status="hold" data-pagin ="<?php echo isset($hold_request_count)?$hold_request_count:'0'; ?>" role="tab">On Hold (<?php echo isset($hold_request_count)?$hold_request_count:'0'; ?>)</a>
                            </li> 
                            <li id="7">
                                <a data-toggle="tab" href="#Qa_cancel" data-status="cancel" data-pagin ="<?php echo isset($cancel_request_count)?$cancel_request_count:'0'; ?>" role="tab">Cancelled (<?php echo isset($cancel_request_count)?$cancel_request_count:'0'; ?>)</a>
                            </li> 
                        </ul>
                    </div>
                    <div class="col-md-2">
                        <a href="#" class="adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $incoming_request[$i]['id']; ?>" data-designerid= "<?php echo $incoming_request[$i]['designer_id']; ?>">
                            <span class="">Add Designer<i class="fa fa-angle-down" aria-hidden="true"></i></span>
                        </a>
                    </div>
                    
                </div>
            </div>
            
            <p class="space-e"></p>
            
            <div class="cli-ent table">
              <div class="tab-content">
                <!--QA Active Section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $incoming_request_count; ?>" class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">
                    <!--<div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">-->
                    <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                <div class="ajax_searchload" style="display:none;">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                </div>
<!--                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">-->
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                    <!-- <svg class="icon">
                                        <use xlink:href="<?php //echo base_url();     ?>theme/assets/qa/images/symbol-defs.svg#icon-magnifying-glass"></use>
                                    </svg> -->
                                </button>
                            </form>
                        </div>
                    </div>  
                    <div class="product-list-show">
                        <?php if ($incoming_request_count >= 1) { ?>
                            <div class="">
                                <div class="cli-ent-row tr tableHead">
                                    <div class="cli-ent-col td">
                                        <input type="checkbox" class="checkAll" data-attr="in_active" name="project"> 
                                    </div>
                                    <div class="cli-ent-col td" style="width: 28%;">
                                        PROJECT NAME
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        CLIENT
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        PROJECT STATUS
                                    </div>
                                    <div class="cli-ent-col td" style="width: 12%;">
                                        DESIGNER
                                    </div>
                                    <div class="cli-ent-col td"style="width: 9%;">
                                        MESSAGES
                                    </div>
                                    <div class="cli-ent-col td"style="width: 9%;">
                                        VERIFIED
                                    </div>
                                    <div class="cli-ent-col td"style="width: 13%;">
                                        TASK TIME
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <?php if (sizeof($incoming_request) == 0) { ?>
                                <div class="nodata">
                                    <h3 class="head-b draft_no">They are no projects at this time</h3>
                                </div>
                            <?php } else { ?>
                                <?php for ($i = 0; $i < sizeof($incoming_request); $i++) { ?>
                                    <!-- Start Row -->
                                    <div class="cli-ent-row tr brdr">
                                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                                        <div class="cli-ent-col td">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" >
                                                        <input type="checkbox" name="project" class="selected_pro in_active"  value="<?php echo $incoming_request[$i]['id']; ?>"/> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
<!--                                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $incoming_request[$i]['id']; ?>/1'">
                                            <?php //if($incoming_request[$i]['is_trail'] == 1){ ?>
                                            <div class="promo-req"><?php //echo PROMO_TEXT; ?></div>
                                             <?php //} ?>
                                            <div class="cli-ent-xbox">
                                                <p class="pro-a">Expected on</p>
                                                <p class="space-a"></p>
                                                <p class="pro-b">
                                                    <?php //echo date('M d, Y h:i A', strtotime($incoming_request[$i]['expected']));
                                                    ?>
                                                </p>
                                            </div>
                                        </div>-->
                                        <div class="cli-ent-col td" style="width: 28%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $incoming_request[$i]['id']; ?>/1'">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" >
                                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $incoming_request[$i]['id']; ?>/1"><?php echo $incoming_request[$i]['title']; ?></a></h3>
                                                        <?php if($incoming_request[$i]['category_name']!= '' || $incoming_request[$i]['subcategory_name']!=''){ ?>
                                                        <p class="pro-b"><b><?php echo $incoming_request[$i]['category_name']; ?></b> <b><?php echo isset($incoming_request[$i]['subcategory_name'])?"> ".$incoming_request[$i]['subcategory_name']:''; ?></b></p>
                                                        <?php } ?>
                                                        <p class="pro-b"><?php echo substr($incoming_request[$i]['description'], 0, 30); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $incoming_request[$i]['id']; ?>/1'">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href="#">
                                                            <figure class="pro-circle-img">
                                                                <?php if ($incoming_request[$i]['customer_profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$incoming_request[$i]['customer_profile_picture'] ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure>
                                                        </a>
                                                    </div>
                                                    <div class="cell-col">
                                                        <p class="text-h text-wrap" title="<?php echo $incoming_request[$i]['customer_first_name'] . " " . $incoming_request[$i]['customer_last_name']; ?>
                                                           ">
                                                               <?php echo $incoming_request[$i]['customer_first_name']; ?>
                                                               <?php
                                                               if (strlen($incoming_request[$i]['customer_last_name']) > 5) {
                                                                   echo ucwords(substr($incoming_request[$i]['customer_last_name'], 0, 1));
                                                               } else {
                                                                   echo $incoming_request[$i]['customer_last_name'];
                                                               }
                                                               ?>

                                                        </p>
                                                        <p class="pro-b">Client</p>
                                                        <div class="sub_user_req_info qa-sub-user">
                                                                    <p class="text-h text-wrap">
                                                                        <?php echo $incoming_request[$i]['sub_first_name']; ?>
                                                                        <?php
                                                                        if (strlen($incoming_request[$i]['sub_last_name']) > 5) {
                                                                            echo ucwords(substr($incoming_request[$i]['sub_last_name'], 0, 1));
                                                                        } else {
                                                                            echo $incoming_request[$i]['sub_last_name'];
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                    <?php
                                                                    if ($incoming_request[$i]['sub_first_name'] != '') {
                                                                        ?>
                                                                        <p class="pro-b">Sub User</p>
                                                                    <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        
                                        
                                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $incoming_request[$i]['id']; ?>/1'">
                                            <div class="cell-col col-w1">
                                                <p class="neft text-center">
                                                    <?php if ($incoming_request[$i]['status_qa'] == "active") { ?>
                                                        <span class="green text-uppercase text-wrap">In-Progress</span>
                                                    <?php } elseif ($incoming_request[$i]['status_qa'] == "disapprove" && $incoming_request[$i]['who_reject'] == 1) { ?>
                                                        <span class="red orangetext text-uppercase text-wrap">REVISION</span>
                                                    <?php } else { ?>
                                                        <span class="red text-uppercase text-wrap">Quality Revision</span>
                                                    <?php } ?>
                                                </p>
                                            </div>
                                        </div>
                                        
                                        <div class="cli-ent-col td" style="width: 12%;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <?php  if($incoming_request[$i]['designer_id'] != "" && $incoming_request[$i]['designer_id'] != 0){ ?>
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href="#" class="<?php echo $incoming_request[$i]['id']; ?>">
                                                            <figure class="pro-circle-img">
                                                                <?php if ($incoming_request[$i]['profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$incoming_request[$i]['profile_picture'] ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure>
                                                        </a>
                                                    </div>
                                                    <div class="cell-col <?php echo $incoming_request[$i]['id']; ?>">
                                                        <p class="text-h text-wrap"title="<?php echo $incoming_request[$i]['designer_first_name'] . " " . $incoming_request[$i]['designer_last_name']; ?>
                                                           ">
                                                               <?php echo $incoming_request[$i]['designer_first_name']; ?>
                                                               <?php
                                                               if (strlen($incoming_request[$i]['designer_first_name']) > 5) {
                                                                   echo ucwords(substr($incoming_request[$i]['designer_last_name'], 0, 1));
                                                               } else {
                                                                   echo $incoming_request[$i]['designer_last_name'];
                                                               }
                                                               ?>
                                                        </p>
                                                        <p class="pro-b">Designer</p>
                                                        <p class="space-a"></p>
<!--                                                        <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $incoming_request[$i]['id']; ?>" data-designerid= "<?php echo $incoming_request[$i]['designer_id']; ?>">
                                                            <span class="sma-red">+</span> Add Designer
                                                        </a>-->
                                                    </div>
                                                    <?php } else { ?>
                                                        <div class="cell-col <?php echo $incoming_request[$i]['id']; ?>">No Designer Assigned</div>
                                                    <?php } ?>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $incoming_request[$i]['id']; ?>/1'">
                                            <div class="cli-ent-xbox text-center">
                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $incoming_request[$i]['id']; ?>/1">
                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                                            <?php if ($incoming_request[$i]['total_chat'] + $incoming_request[$i]['comment_count'] != 0) { ?>
                                                                <span class="numcircle-box">
                                                                    <?php echo $incoming_request[$i]['total_chat'] + $incoming_request[$i]['comment_count']; ?>
                                                                </span>
                                                            <?php } ?>  
                                                        </span>
                                                        <?php //echo $incoming_request[$i]['total_chat_all'];  ?></a></p>
                                            </div>
                                        </div>
<!--                                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $incoming_request[$i]['id']; ?>'">
                                            <div class="cli-ent-xbox text-center">
                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $incoming_request[$i]['id']; ?>/1">
                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13">
                                                            <?php if (count($incoming_request[$i]['total_files']) != 0) { ?>
                                                                <span class="numcircle-box"><?php echo count($incoming_request[$i]['total_files']); ?></span><?php } ?></span>
                                                        <?php echo count($incoming_request[$i]['total_files_count']); ?>
                                                    </a></p>
                                            </div>
                                        </div>-->
                                          <?php 
                                    //echo $active_project[$i]['verified'];
                                    if($incoming_request[$i]['verified'] == '1') {
                                        $class = 'verified_by_qa';
                                        $disable = '';
                                    }elseif($incoming_request[$i]['verified'] == '2'){
                                        $class = 'verified_by_designer';
                                        $disable = 'disabled';
                                    }else{
                                        $class = '';
                                        $disable = '';
                                    }
                                    ?>
                                    <div class="cli-ent-col td" style="width: 9%;">
                                        <div class="cli-ent-xbox">
                                        <div class="cell-row">
                                            <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $incoming_request[$i]['id'];?>">
                                                <label>
                                                <input type="checkbox" name="project" class="verified" data-pid="<?php echo $incoming_request[$i]['id'];?>" <?php echo ($incoming_request[$i]['verified'] == 1 || $incoming_request[$i]['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable;?>/>
                                                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                                                </label>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                        <div class="cli-ent-col td" style="width: 13%;">
                                            <?php if($incoming_request[$i]['is_trail'] == 1){ ?>
                                            <div class="promo-req"><?php //echo PROMO_TEXT; ?></div>
                                             <?php } ?>
                                            <div class="cli-ent-xbox">
                                                <a href="javascript:void(0)" class="editexpected" date-expected="<?php echo date('M d, Y h:i A', strtotime($incoming_request[$i]['expected'])); ?>" date-reqid="<?php echo $incoming_request[$i]['id']; ?>" data-timezonedate="<?php echo $incoming_request[$i]['usertimezone_date']; ?>">
                                                    <i class='fa fa-pencil-square-o'></i> 
                                                </a>
<!--                                                <p class="pro-a">Expected on</p>-->
                                                 <p class="space-a"></p>
                                                <p class="pro-b">
                                                <p id="demo_<?php echo $incoming_request[$i]['id']; ?>" class="expectd_date" data-id="<?php echo $incoming_request[$i]['id']; ?>" data-date="<?php echo date('M d, Y h:i:A', strtotime($incoming_request[$i]['expected'])); ?>"></p>
                                                <?php echo date('M d, Y h:i:A', strtotime($incoming_request[$i]['expected'])); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Row -->
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php if ($incoming_request_count > LIMIT_QA_LIST_COUNT) { ?>
                        <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_active_project; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--QA Active Section End Here -->
                <!--QA IN Queue Section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $ongoing_request_count; ?>" class="tab-pane content-datatable datatable-width" id="Qa_in_queue" role="tabpanel">
                    <!--<div class="tab-pane content-datatable datatable-width" id="Qa_in_queue" role="tabpanel">-->
                    <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                <div class="ajax_searchload" style="display:none;">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                </div>
                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="product-list-show">
                        <?php if ($ongoing_request_count >= 1) { ?>
                            <div class="">
                                <div class="cli-ent-row tr tableHead">
                                    <div class="cli-ent-col td">
                                        <input type="checkbox" class="checkAll" data-attr="in_queue" name="project">
                                    </div>
                                    <!-- <div class="cli-ent-col td" style="width: 10%;">
                                        PRIORITY
                                    </div> -->
                                    <div class="cli-ent-col td" style="width: 34%;">
                                        PROJECT  NAME
                                    </div>
                                    <div class="cli-ent-col td" style="width: 14%;">
                                        PROJECT STATUS
                                    </div>
                                    <div class="cli-ent-col td" style="width: 12%;">
                                        CLIENT
                                    </div>
                                    <div class="cli-ent-col td" style="width: 13%;">
                                        DESIGNER
                                    </div>
                                    <div class="cli-ent-col td"style="width: 9%;">
                                        VERIFIED
                                    </div>
                                    <div class="cli-ent-col td" style="width: 9%;">
                                        MESSAGES
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <?php if (sizeof($ongoing_request) == 0) { ?>
                                <div class="nodata">
                                    <h3 class="head-b draft_no">They are no projects at this time</h3>
                                </div>
                            <?php } else { ?>
                                <?php for ($i = 0; $i < sizeof($ongoing_request); $i++) { ?>
                                    <!-- Start Row -->
                                    <div class="cli-ent-row tr brdr">
                                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                                        <div class="cli-ent-col td">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" >
                                                        <input type="checkbox" name="project" class="selected_pro in_queue"  value="<?php echo $ongoing_request[$i]['id']; ?>"/> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $ongoing_request[$i]['id']; ?>/2'">
                                            <?php if($ongoing_request[$i]['is_trail'] == 1){ ?>
                                            <div class="promo-req inqueue"><?php //echo PROMO_TEXT; ?></div>
                                             <?php } ?>
                                            <div class="cli-ent-xbox">
                                                <p class="pro-a">In Queue</p>
                                                <div class="cell-col">
                                                    <?php //echo isset($ongoing_request[$i]['priority'])? $ongoing_request[$i]['priority']:''; ?>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="cli-ent-col td" style="width: 34%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $ongoing_request[$i]['id']; ?>/2'">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" >
                                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $ongoing_request[$i]['id']; ?>/2"><?php echo $ongoing_request[$i]['title']; ?></a></h3>
                                                        <?php if($ongoing_request[$i]['category_name']!= '' || $ongoing_request[$i]['subcategory_name']!=''){ ?>
                                                        <p class="pro-b"><b><?php echo $ongoing_request[$i]['category_name']; ?></b> <b><?php echo isset($ongoing_request[$i]['subcategory_name'])?"> ".$ongoing_request[$i]['subcategory_name']:''; ?></b></p>
                                                        <?php } ?>
                                                        <p class="pro-b"><?php echo substr($ongoing_request[$i]['description'], 0, 30); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cli-ent-col td col-w1" style="width: 14%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $inprogressrequest[$i]['id']; ?>/2'">
                                            <p class="neft text-center"><span class="gray text-uppercase">IN-queue</span></p>
                                            <div class="cell-col priority-sec">
                                                    <?php echo isset($ongoing_request[$i]['priority'])? $ongoing_request[$i]['priority']:''; ?>
                                                </div>

                                        </div>
                                        <div class="cli-ent-col td" style="width: 14%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $ongoing_request[$i]['id']; ?>/2'">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href="#" >
                                                            <figure class="pro-circle-img">
                                                                <?php if ($ongoing_request[$i]['customer_profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$ongoing_request[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure>
                                                        </a>
                                                    </div>
                                                    <div class="cell-col">
                                                        <p class="text-h text-wrap" title="<?php echo $ongoing_request[$i]['customer_first_name'] . " " . $ongoing_request[$i]['customer_last_name']; ?>
                                                           ">
                                                               <?php echo $ongoing_request[$i]['customer_first_name']; ?>
                                                               <?php
                                                               if (strlen($ongoing_request[$i]['customer_last_name']) > 5) {
                                                                   echo ucwords(substr($ongoing_request[$i]['customer_last_name'], 0, 1));
                                                               } else {
                                                                   echo $ongoing_request[$i]['customer_last_name'];
                                                               }
                                                               ?>

                                                        </p>
                                                        <p class="pro-b">Client</p>
                                                        <div class="sub_user_req_info qa-sub-user">
                                                                    <p class="text-h text-wrap">
                                                                        <?php echo $ongoing_request[$i]['sub_first_name']; ?>
                                                                        <?php
                                                                        if (strlen($ongoing_request[$i]['sub_last_name']) > 5) {
                                                                            echo ucwords(substr($ongoing_request[$i]['sub_last_name'], 0, 1));
                                                                        } else {
                                                                            echo $ongoing_request[$i]['sub_last_name'];
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                    <?php
                                                                    if ($ongoing_request[$i]['sub_first_name'] != '') {
                                                                        ?>
                                                                        <p class="pro-b">Sub User</p>
                                                                    <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cli-ent-col td" style="width: 12%;">
                                            <div class="cli-ent-xbox text-left p-left1">
                                                <div class="cell-row">
                                                    <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                                    <!-- <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                                        <a href="" class="<?php echo $ongoing_request[$i]['id']; ?>"><figure class="pro-circle-img">
                                                    <?php if ($ongoing_request[$i]['profile_picture'] != "") { ?>
                                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$ongoing_request[$i]['profile_picture'] ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                        </figure></a>
                                                    </div>
                                                    <div class="cell-col <?php echo $ongoing_request[$i]['id']; ?>">
                                                        <p class="text-h" title="<?php echo $ongoing_request[$i]['designer_first_name'] . " " . $ongoing_request[$i]['designer_last_name']; ?>">
                                                    <?php echo $ongoing_request[$i]['designer_first_name']; ?>
                                                    <?php
                                                    if (strlen($ongoing_request[$i]['designer_last_name']) > 5) {
                                                        echo ucwords(substr($ongoing_request[$i]['designer_last_name'], 0, 1));
                                                    } else {
                                                        echo $ongoing_request[$i]['designer_last_name'];
                                                    }
                                                    ?>

                                                            </p>
                                                        <p class="pro-b">Designer</p>
                                                        <p class="space-a"></p>
                                                        <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $ongoing_request[$i]['id']; ?>" data-designerid= "<?php echo $ongoing_request[$i]['designer_id']; ?>">
                                                            <span class="sma-red">+</span> Add Designer
                                                        </a> 
                                                    </div> -->
                                                </div>

                                            </div>
                                        </div>
                                        <?php
                                        if($ongoing_request[$i]['verified'] == '1') {
                                        $class = 'verified_by_qa';
                                        $disable = '';
                                        }elseif($ongoing_request[$i]['verified'] == '2'){
                                            $class = 'verified_by_designer';
                                            $disable = 'disabled';
                                        }else{
                                            $class = '';
                                            $disable = '';
                                        }
                                        ?>
                                    <div class="cli-ent-col td" style="width: 9%;">
                                        <div class="cli-ent-xbox">
                                        <div class="cell-row">
                                            <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $ongoing_request[$i]['id'];?>">
                                                <label>
                                                <input type="checkbox" name="project" class="verified" data-pid="<?php echo $ongoing_request[$i]['id'];?>" <?php echo ($ongoing_request[$i]['verified'] == 1 || $ongoing_request[$i]['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable;?>/>
                                                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                                                </label>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $ongoing_request[$i]['id']; ?>/2'">
                                            <div class="cli-ent-xbox text-center">
                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $ongoing_request[$i]['id']; ?>/2">
                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($ongoing_request[$i]['total_chat'] + $ongoing_request[$i]['comment_count'] != 0) { ?>
                                                                <span class="numcircle-box">
                                                                    <?php echo $ongoing_request[$i]['total_chat'] + $ongoing_request[$i]['comment_count']; ?>
                                                                </span>
                                                            <?php } ?></span>
                                                        <?php //echo $ongoing_request[$i]['total_chat_all'];    ?></a></p>
                                            </div>
                                        </div>
<!--                                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $ongoing_request[$i]['id']; ?>/2'">
                                            <div class="cli-ent-xbox text-center">
                                                <p class="pro-a inline-per"><a href="<?php //echo base_url(); ?>qa/dashboard/view_project/<?php echo $ongoing_request[$i]['id']; ?>">
                                                        <span class="inline-imgsssx"><img src="<?php //echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13">  <?php if (count($ongoing_request[$i]['total_files']) != 0) { ?>
                                                                <span class="numcircle-box"><?php //echo count($ongoing_request[$i]['total_files']); ?></span><?php } ?></span>
                                                        <?php //echo count($ongoing_request[$i]['total_files_count']); ?></a></p>
                                            </div>
                                        </div>-->
                                    </div> <!-- End Row -->
                                    <?php
                                }
                            }
                            ?>    

                            <!-- <p class="space-e"></p>
                            
                            <div class="loader">
                                <a class="loader-link text-uppercase" href=""><svg class="icon" width="35">
                                    <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-loading"></use>
                                </svg> Loading</a>
                            </div> -->
                        </div>
                        <?php if ($ongoing_request_count > LIMIT_QA_LIST_COUNT) { ?>
                            <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_active_project; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--QA IN Queue Section End Here -->
                <!--QA Pending Review Section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $pending_review_request_count; ?>" class="tab-pane content-datatable datatable-width" id="Qa_pending_review" role="tabpanel">
                    <!--<div class="tab-pane content-datatable datatable-width" id="Qa_pending_review" role="tabpanel">-->
                    <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                <div class="ajax_searchload" style="display:none;">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                </div>
                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                    <!-- <svg class="icon">
                                        <use xlink:href="<?php //echo base_url();          ?>theme/assets/qa/images/symbol-defs.svg#icon-magnifying-glass"></use>
                                    </svg> -->
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="product-list-show">
                        <?php if ($pending_review_request_count >= 1) { ?>
                            <div class="">
                                <div class="cli-ent-row tr tableHead">
                                    <div class="cli-ent-col td">
                                        <input type="checkbox" class="checkAll" name="project" data-attr="pending_rev">
                                    </div>
                                    <div class="cli-ent-col td" style="width: 28%;">
                                        PROJECT NAME
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        CLIENT
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        PROJECT STATUS
                                    </div>
                                    <div class="cli-ent-col td" style="width: 12%;">
                                        DESIGNER
                                    </div>
                                    <div class="cli-ent-col td"style="width: 9%;">
                                        MESSAGES
                                    </div>
                                    <div class="cli-ent-col td"style="width: 10%;">
                                        VERIFIED
                                    </div>
                                    <div class="cli-ent-col td"style="width: 13%;">
                                        TASK TIME
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        <div class="row">
                            <?php if (sizeof($pending_review_request) == 0) { ?>
                                <div class="nodata">
                                    <h3 class="head-b draft_no">They are no projects at this time</h3>
                                </div>
                            <?php } else { ?>
                                <?php for ($i = 0; $i < sizeof($pending_review_request); $i++) {
                                    ?>
                                    <!-- Start Row -->
                                    <div class="cli-ent-row tr brdr">
                                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                                    <div class="cli-ent-col td">
                                        <div class="cli-ent-xbox text-left">
                                            <div class="cell-row">
                                                <div class="cell-col">
                                                    <input type="checkbox" name="project" class="selected_pro pending_rev" value="<?php echo $pending_review_request[$i]['id']; ?>"/> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cli-ent-col td" style="width: 28%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_review_request[$i]['id']; ?>/3'">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" >
                                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_review_request[$i]['id']; ?>/3"><?php echo $pending_review_request[$i]['title']; ?></a></h3>
                                                        <?php if($pending_review_request[$i]['category_name']!= '' || $pending_review_request[$i]['subcategory_name']!=''){ ?>
                                                        <p class="pro-b"><b><?php echo $pending_review_request[$i]['category_name']; ?></b> <b><?php echo isset($pending_review_request[$i]['subcategory_name'])?"> ".$pending_review_request[$i]['subcategory_name']:''; ?></b></p>
                                                        <?php } ?>
                                                        <p class="pro-b"><?php echo substr($pending_review_request[$i]['description'], 0, 30); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_review_request[$i]['id']; ?>'">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href=""><figure class="pro-circle-img">
                                                                <?php if ($pending_review_request[$i]['customer_profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$pending_review_request[$i]['customer_profile_picture'] ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure></a>
                                                    </div>
                                                    <div class="cell-col">
                                                        <p class="text-h text-wrap" title="<?php echo $pending_review_request[$i]['customer_first_name'] . " " . $pending_review_request[$i]['customer_last_name']; ?>
                                                           ">
                                                               <?php echo $pending_review_request[$i]['customer_first_name']; ?>
                                                               <?php
                                                               if (strlen($pending_review_request[$i]['customer_last_name']) > 5) {
                                                                   echo ucwords(substr($pending_review_request[$i]['customer_last_name'], 0, 1));
                                                               } else {
                                                                   echo $pending_review_request[$i]['customer_last_name'];
                                                               }
                                                               ?>

                                                        </p>
                                                        <p class="pro-b">Client</p>
                                                        <div class="sub_user_req_info qa-sub-user">
                                                                    <p class="text-h text-wrap">
                                                                        <?php echo $pending_review_request[$i]['sub_first_name']; ?>
                                                                        <?php
                                                                        if (strlen($pending_review_request[$i]['sub_last_name']) > 5) {
                                                                            echo ucwords(substr($pending_review_request[$i]['sub_last_name'], 0, 1));
                                                                        } else {
                                                                            echo $pending_review_request[$i]['sub_last_name'];
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                    <?php
                                                                    if ($pending_review_request[$i]['sub_first_name'] != '') {
                                                                        ?>
                                                                        <p class="pro-b">Sub User</p>
                                                                    <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="cli-ent-col td client-project" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $pendingreview[$i]['id']; ?>/3'">
                                            <p class="neft text-center"><span class="lightbluetext text-uppercase text-wrap">Pending Review</span></p>
                                        </div>

                                        <div class="cli-ent-col td" style="width: 16%;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <?php  if($pending_review_request[$i]['designer_id'] != "" && $pending_review_request[$i]['designer_id'] != 0){ ?>
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href="" class="<?php echo $pending_review_request[$i]['id']; ?>""><figure class="pro-circle-img">
                                                                <?php if ($pending_review_request[$i]['profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$pending_review_request[$i]['profile_picture'] ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure></a>
                                                    </div>
                                                    <div class="cell-col <?php echo $pending_review_request[$i]['id']; ?>">
                                                        <p class="text-h text-wrap" title="<?php echo $pending_review_request[$i]['designer_first_name'] . " " . $pending_review_request[$i]['designer_last_name']; ?>">
                                                            <?php echo $pending_review_request[$i]['designer_first_name']; ?>
                                                            <?php
                                                            if (strlen($pending_review_request[$i]['designer_last_name']) > 5) {
                                                                echo ucwords(substr($pending_review_request[$i]['designer_last_name'], 0, 1));
                                                            } else {
                                                                echo $pending_review_request[$i]['designer_last_name'];
                                                            }
                                                            ?>
                                                        </p>
                                                        <p class="pro-b">Designer</p>
                                                        <p class="space-a"></p>
                                                        <?php //if ($pending_review_request[$i]['designer_assign_or_not'] == 0) { ?>
<!--                                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $pending_review_request[$i]['id']; ?>" data-designerid= "<?php echo $pending_review_request[$i]['designer_id']; ?>">
                                                                <span class="sma-red">+</span> Add Designer
                                                            </a>-->
                                                        <?php //} ?>
                                                    </div>
                                                    <?php }else{ ?>
                                                        <div class="cell-col <?php echo  $pending_review_request[$i]['id'];?>">No Designer Assigned</div>
                                                    <?php } ?>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_review_request[$i]['id']; ?>/3'">
                                            <div class="cli-ent-xbox text-center">
                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_review_request[$i]['id']; ?>/3">
                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($pending_review_request[$i]['total_chat'] + $pending_review_request[$i]['comment_count'] != 0) { ?>
                                                                <span class="numcircle-box">
                                                                    <?php echo $pending_review_request[$i]['total_chat'] + $pending_review_request[$i]['comment_count']; ?>
                                                                </span>
                                                            <?php } ?></span>
                                                        <?php //echo $pending_review_request[$i]['total_chat_all'];   ?></a></p>
                                            </div>
                                        </div>
<!--                                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_review_request[$i]['id']; ?>/3'">
                                            <div class="cli-ent-xbox text-center">
                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_review_request[$i]['id']; ?>/3">
                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13">  <?php if (count($pending_review_request[$i]['total_files']) != 0) { ?>
                                                                <span class="numcircle-box"><?php echo count($pending_review_request[$i]['total_files']); ?></span><?php } ?></span>
                                                        <?php echo count($pending_review_request[$i]['total_files_count']); ?></a></p>
                                            </div>
                                        </div>-->
                                                        <?php
                                        if($pending_review_request[$i]['verified'] == '1') {
                                        $class = 'verified_by_qa';
                                        $disable = '';
                                        }elseif($pending_review_request[$i]['verified'] == '2'){
                                            $class = 'verified_by_designer';
                                            $disable = 'disabled';
                                        }else{
                                            $class = '';
                                            $disable = '';
                                        }
                                        ?>
                                    <div class="cli-ent-col td" style="width: 10%;">
                                        <div class="cli-ent-xbox">
                                        <div class="cell-row">
                                            <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $pending_review_request[$i]['id'];?>">
                                                <label>
                                                <input type="checkbox" name="project" class="verified" data-pid="<?php echo $pending_review_request[$i]['id'];?>" <?php echo ($pending_review_request[$i]['verified'] == 1 || $pending_review_request[$i]['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable;?>/>
                                                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                                                </label>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                        <div class="cli-ent-col td" style="width: 13%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_review_request[$i]['id']; ?>/3'">
                                            <?php if($pending_review_request[$i]['is_trail'] == 1){ ?>
                                            <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                                             <?php } ?>
                                            <div class="cli-ent-xbox">
                                                <p class="pro-a">Expected on</p>
                                                <p class="space-a"></p>
                                                <p class="pro-b">
                                                    <?php echo date('M d, Y h:i A', strtotime($pending_review_request[$i]['revisiondate']));
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div> 
                                    <!-- End Row -->
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php if ($pending_review_request_count > LIMIT_QA_LIST_COUNT) { ?>
                            <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_active_project; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div></div>
                <!--QA Pending Review Section End Here -->
                <!--QA Pending Approval Section Start Here -->
               <div data-group="1" data-loaded=""  data-total-count="<?php echo $pending_request_count; ?>" class="tab-pane content-datatable datatable-width" id="Qa_pending_approval" role="tabpanel">
                    <!--<div class="tab-pane content-datatable datatable-width" id="Qa_pending_approval" role="tabpanel">-->
                    <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                <div class="ajax_searchload" style="display:none;">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                </div>
                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                    <!-- <svg class="icon">
                                        <use xlink:href="<?php //echo base_url();          ?>theme/assets/qa/images/symbol-defs.svg#icon-magnifying-glass"></use>
                                    </svg> -->
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="product-list-show">
                        <?php if ($pending_request_count >= 1) { ?>
                            <div class="">
                                <div class="cli-ent-row tr tableHead">
                                    <div class="cli-ent-col td">
                                        <input type="checkbox" class="checkAll" name="project" data-attr="pending_app">
                                    </div>
                                    <div class="cli-ent-col td" style="width: 28%;">
                                        PROJECT NAME
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        CLIENT
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        PROJECT STATUS
                                    </div>
                                    <div class="cli-ent-col td" style="width: 12%;">
                                        DESIGNER
                                    </div>
                                    <div class="cli-ent-col td"style="width: 9%;">
                                        MESSAGES
                                    </div>
                                    <div class="cli-ent-col td"style="width: 10%;">
                                        VERIFIED
                                    </div>
                                    <div class="cli-ent-col td"style="width: 13%;">
                                        TASK TIME
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <?php if (sizeof($pending_request) == 0) { ?>
                                <div class="nodata">
                                    <h3 class="head-b draft_no">They are no projects at this time</h3>
                                </div>
                            <?php } else { ?>
                                <?php for ($i = 0; $i < sizeof($pending_request); $i++) { ?>
                                    <!-- Start Row -->
                                    <div class="cli-ent-row tr brdr">
                                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                                            <div class="cli-ent-col td">
                                                <div class="cell-row">
                                                    <div class="cell-col" >
                                                        <input type="checkbox" name="project" class="selected_pro pending_app" value="<?php echo $pending_request[$i]['id']; ?>"/> 
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="cli-ent-col td" style="width: 28%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_request[$i]['id']; ?>/4'">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" >
                                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_request[$i]['id']; ?>/4"><?php echo $pending_request[$i]['title']; ?></a></h3>
                                                        <?php if($pending_request[$i]['category_name']!= '' || $pending_request[$i]['subcategory_name']!=''){ ?>
                                                        <p class="pro-b"><b><?php echo $pending_request[$i]['category_name']; ?></b> <b><?php echo isset($pending_request[$i]['subcategory_name'])?"> ".$pending_request[$i]['subcategory_name']:''; ?></b></p>
                                                        <?php } ?>
                                                        <p class="pro-b"><?php echo substr($pending_request[$i]['description'], 0, 30); ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_request[$i]['id']; ?>/4'">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href="javascript:void(0);"><figure class="pro-circle-img">
                                                                <?php if ($pending_request[$i]['customer_profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$pending_request[$i]['customer_profile_picture'] ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure></a>
                                                    </div>
                                                    <div class="cell-col">
                                                        <p class="text-h text-wrap" title="<?php echo $pending_request[$i]['customer_first_name'] . " " . $pending_request[$i]['customer_last_name']; ?>
                                                           ">
                                                               <?php echo $pending_request[$i]['customer_first_name']; ?>
                                                               <?php
                                                               if (strlen($pending_request[$i]['customer_last_name']) > 5) {
                                                                   echo ucwords(substr($pending_request[$i]['customer_last_name'], 0, 1));
                                                               } else {
                                                                   echo $pending_request[$i]['customer_last_name'];
                                                               }
                                                               ?>

                                                        </p>
                                                        <p class="pro-b">Client</p>
                                                        <div class="sub_user_req_info qa-sub-user">
                                                                    <p class="text-h text-wrap">
                                                                        <?php echo $pending_request[$i]['sub_first_name']; ?>
                                                                        <?php
                                                                        if (strlen($pending_request[$i]['sub_last_name']) > 5) {
                                                                            echo ucwords(substr($pending_request[$i]['sub_last_name'], 0, 1));
                                                                        } else {
                                                                            echo $pending_request[$i]['sub_last_name'];
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                    <?php
                                                                    if ($pending_request[$i]['sub_first_name'] != '') {
                                                                        ?>
                                                                        <p class="pro-b">Sub User</p>
                                                                    <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        
                                        <div class="cli-ent-col td" style="width: 16%;">
                                            <div class="cell-col col-w1">
                                                <p class="neft text-center"><span class="red text-uppercase bluetext text-wrap">Pending-approval</span></p>
                                            </div>
                                        </div>

                                        <div class="cli-ent-col td" style="width: 12%;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <?php  if($pending_request[$i]['designer_id'] != "" && $pending_request[$i]['designer_id'] != 0){ ?>
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href="javascript:void(0);" class="<?php echo $pending_request[$i]['id']; ?>"><figure class="pro-circle-img">
                                                                <?php if ($pending_request[$i]['profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$pending_request[$i]['profile_picture'] ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure></a>
                                                    </div>
                                                    <div class="cell-col <?php echo $pending_request[$i]['id']; ?>">
                                                        <p class="text-h text-wrap" title="<?php echo $pending_request[$i]['designer_first_name'] . " " . $pending_request[$i]['designer_last_name']; ?>">
                                                            <?php echo $pending_request[$i]['designer_first_name']; ?>
                                                            <?php
                                                            if (strlen($pending_request[$i]['designer_last_name']) > 5) {
                                                                echo ucwords(substr($pending_request[$i]['designer_last_name'], 0, 1));
                                                            } else {
                                                                echo $pending_request[$i]['designer_last_name'];
                                                            }
                                                            ?>
                                                        </p>
                                                        <p class="pro-b">Designer</p>
                                                        <p class="space-a"></p>
                                                        <?php //if ($pending_request[$i]['designer_assign_or_not'] == 0) { ?>
<!--                                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $pending_request[$i]['id']; ?>" data-designerid= "<?php echo $pending_request[$i]['designer_id']; ?>">
                                                                <span class="sma-red">+</span> Add Designer
                                                            </a>-->
                                                        <?php //} ?>
                                                    </div>
                                                    <?php }else{ ?>
                                                     <div class="cell-col <?php echo  $pending_request[$i]['id'];?>">No Designer Assigned</div>
                                                    <?php } ?>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_request[$i]['id']; ?>/3'">
                                            <div class="cli-ent-xbox text-center">
                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_request[$i]['id']; ?>/4">
                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($pending_request[$i]['total_chat'] + $pending_request[$i]['comment_count'] != 0) { ?>
                                                                <span class="numcircle-box">
                                                                    <?php echo $pending_request[$i]['total_chat'] + $pending_request[$i]['comment_count']; ?>
                                                                </span>
                                                            <?php } ?></span>
                                                        <?php //echo $pending_request[$i]['total_chat_all'];    ?></a></p>
                                            </div>
                                        </div>
<!--                                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_request[$i]['id']; ?>/4'">
                                            <div class="cli-ent-xbox text-center">
                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_request[$i]['id']; ?>/4">
                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13">  <?php if (count($pending_request[$i]['total_files']) != 0) { ?>
                                                                <span class="numcircle-box"><?php echo count($pending_request[$i]['total_files']); ?></span><?php } ?></span>
                                                        <?php echo count($pending_request[$i]['total_files_count']); ?></a></p>
                                            </div>
                                        </div>-->
                                                        <?php
                                        if($pending_request[$i]['verified'] == '1') {
                                        $class = 'verified_by_qa';
                                        $disable = '';
                                        }elseif($pending_request[$i]['verified'] == '2'){
                                            $class = 'verified_by_designer';
                                            $disable = 'disabled';
                                        }else{
                                            $class = '';
                                            $disable = '';
                                        }
                                        ?>
                                    <div class="cli-ent-col td" style="width: 10%;">
                                        <div class="cli-ent-xbox">
                                        <div class="cell-row">
                                            <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $pending_request[$i]['id'];?>">
                                                <label>
                                                <input type="checkbox" name="project" class="verified" data-pid="<?php echo $pending_request[$i]['id'];?>" <?php echo ($pending_request[$i]['verified'] == 1 || $pending_request[$i]['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable;?>/>
                                                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                                                </label>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="cli-ent-col td" style="width: 13%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $pending_request[$i]['id']; ?>/4'">
                                           <?php if($pending_request[$i]['is_trail'] == 1){ ?>
                                            <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                                             <?php } ?>
                                            <div class="cli-ent-xbox">
                                                <p class="pro-a">Delivered on</p>
                                                <p class="space-a"></p>
                                                <p class="pro-b">
                                                    <?php echo date('M d, Y h:i A', strtotime($pending_request[$i]['reviewdate']));
                                                    ?>
                                                </p>
                                                 <p class="pro-b">May 30, 2018</p> 
                                            </div>
                                        </div>
                                    </div> 
                                    <!-- End Row -->
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php if ($pending_request_count > LIMIT_QA_LIST_COUNT) { ?>
                            <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_active_project; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--QA Pending Approval Section End Here -->
                <!--QA Completed Section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $approved_request_count; ?>" class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel">
                    <!--<div class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel">-->
                    <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                <div class="ajax_searchload" style="display:none;">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                </div>
                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                    <!-- <svg class="icon">
                                        <use xlink:href="<?php //echo base_url();         ?>theme/assets/qa/images/symbol-defs.svg#icon-magnifying-glass"></use>
                                    </svg> -->
                                </button>
                            </form>
                        </div>
                    </div>
                        <div class="product-list-show">
                           <?php if ($approved_request_count >= 1) { ?>
                            <div class="">
                                <div class="cli-ent-row tr tableHead">
                                    <div class="cli-ent-col td">
                                        <input type="checkbox" class="checkAll" data-attr="completed" name="project">
                                    </div>
                                    <div class="cli-ent-col td" style="width: 28%;">
                                        PROJECT NAME
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        CLIENT
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        PROJECT STATUS
                                    </div>
                                    <div class="cli-ent-col td" style="width: 12%;">
                                        DESIGNER
                                    </div>
                                    <div class="cli-ent-col td"style="width: 9%;">
                                        MESSAGES
                                    </div>
                                    <div class="cli-ent-col td"style="width: 10%;">
                                        VERIFIED
                                    </div>
                                    <div class="cli-ent-col td"style="width: 13%;">
                                        APPROVED
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <?php if (sizeof($approved_request) == 0) { ?>
                                <div class="nodata">
                                    <h3 class="head-b draft_no">They are no projects at this time</h3>
                                </div>
                            <?php } else { ?>
                                <?php for ($i = 0; $i < sizeof($approved_request); $i++) { ?>
                                    <!-- Start Row -->
                                    <div class="cli-ent-row tr brdr">
                                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                                        <div class="cli-ent-col td" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $approved_request[$i]['id']; ?>/5'"style="cursor: pointer;">
                                            <div class="cell-row">
                                                <div class="cell-col" >
                                                    <input type="checkbox" name="project" class="selected_pro completed" value="<?php echo $approved_request[$i]['id']; ?>"/> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cli-ent-col td" style="width: 28%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $approved_request[$i]['id']; ?>/5'"style="cursor: pointer;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">

                                                    <div class="cell-col" >
                                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $approved_request[$i]['id']; ?>/5"><?php echo $approved_request[$i]['title']; ?></a></h3>
                                                        <?php if($approved_request[$i]['category_name']!= '' || $approved_request[$i]['subcategory_name']!=''){ ?>
                                                        <p class="pro-b"><b><?php echo $approved_request[$i]['category_name']; ?></b> <b><?php echo isset($approved_request[$i]['subcategory_name'])?"> ".$approved_request[$i]['subcategory_name']:''; ?></b></p>
                                                        <?php } ?>
                                                        <p class="pro-b">
                                                            <?php echo substr($approved_request[$i]['description'], 0, 30); ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $approved_request[$i]['id']; ?>/5'"style="cursor: pointer;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href="javascript:void(0);"><figure class="pro-circle-img">
                                                                <?php if ($approved_request[$i]['customer_profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$approved_request[$i]['customer_profile_picture'] ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure></a>
                                                    </div>
                                                    <div class="cell-col">
                                                        <p class="text-h text-wrap" title="<?php echo $approved_request[$i]['customer_first_name'] . " " . $approved_request[$i]['customer_last_name']; ?>">
                                                            <?php echo $approved_request[$i]['customer_first_name']; ?>
                                                            <?php
                                                            if (strlen($approved_request[$i]['customer_last_name']) > 5) {
                                                                echo ucwords(substr($approved_request[$i]['customer_last_name'], 0, 1));
                                                            } else {
                                                                echo $approved_request[$i]['customer_last_name'];
                                                            }
                                                            ?>

                                                        </p>
                                                        <p class="pro-b">Client</p>
                                                        <div class="sub_user_req_info qa-sub-user">
                                                                    <p class="text-h text-wrap">
                                                                        <?php echo $approved_request[$i]['sub_first_name']; ?>
                                                                        <?php
                                                                        if (strlen($approved_request[$i]['sub_last_name']) > 5) {
                                                                            echo ucwords(substr($approved_request[$i]['sub_last_name'], 0, 1));
                                                                        } else {
                                                                            echo $approved_request[$i]['sub_last_name'];
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                    <?php
                                                                    if ($approved_request[$i]['sub_first_name'] != '') {
                                                                        ?>
                                                                        <p class="pro-b">Sub User</p>
                                                                    <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $approved_request[$i]['id']; ?>/5'"style="cursor: pointer;">
                                            <div class="cell-col col-w1">
                                                <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                            </div>
                                        </div>

                                        <div class="cli-ent-col td" style="width: 12%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $approved_request[$i]['id']; ?>/5'"style="cursor: pointer;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <?php  if($approved_request[$i]['designer_id'] != "" && $approved_request[$i]['designer_id'] != 0){ ?>
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href="javascript:void(0);"><figure class="pro-circle-img">
                                                                <?php if ($approved_request[$i]['profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$approved_request[$i]['profile_picture'] ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure></a>
                                                    </div>
                                                    <div class="cell-col">
                                                        <p class="text-h text-wrap" title="<?php echo $approved_request[$i]['designer_first_name'] . " " . $approved_request[$i]['designer_last_name']; ?> ">
                                                            <?php echo $approved_request[$i]['designer_first_name']; ?>
                                                            <?php
                                                            if (strlen($approved_request[$i]['designer_last_name']) > 5) {
                                                                echo ucwords(substr($approved_request[$i]['designer_last_name'], 0, 1));
                                                            } else {
                                                                echo $approved_request[$i]['designer_last_name'];
                                                            }
                                                            ?>

                                                        </p>
                                                        <p class="pro-b">Designer</p>

                                                    </div>
                                                    <?php } else { ?>
                                                        <div class="cell-col <?php echo $approved_request[$i]['id']; ?>">No Designer Assigned</div>
                                                    <?php } ?>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $approved_request[$i]['id']; ?>/5'"style="cursor: pointer;">
                                            <div class="cli-ent-xbox text-center">
                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $approved_request[$i]['id']; ?>/5">
                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($approved_request[$i]['total_chat'] + $approved_request[$i]['comment_count'] != 0) { ?>
                                                                <span class="numcircle-box">
                                                                    <?php echo $approved_request[$i]['total_chat'] + $approved_request[$i]['comment_count']; ?>
                                                                </span>
                                                            <?php } ?></span></a></p>
                                            </div>
                                        </div>
                                          <?php
                                        if($approved_request[$i]['verified'] == '1') {
                                        $class = 'verified_by_qa';
                                        $disable = '';
                                        }elseif($approved_request[$i]['verified'] == '2'){
                                            $class = 'verified_by_designer';
                                            $disable = 'disabled';
                                        }else{
                                            $class = '';
                                            $disable = '';
                                        }
                                        ?>
                                    <div class="cli-ent-col td" style="width: 10%;">
                                        <div class="cli-ent-xbox">
                                        <div class="cell-row">
                                            <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $approved_request[$i]['id'];?>">
                                                <label>
                                                <input type="checkbox" name="project" class="verified" data-pid="<?php echo $approved_request[$i]['id'];?>" <?php echo ($approved_request[$i]['verified'] == 1 || $approved_request[$i]['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable;?>/>
                                                <i class="fa fa-circle-thin" aria-hidden="true"></i>
                                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                <i class="fa fa-check-circle" aria-hidden="true"></i>
                                                </label>
                                            </div>
                                        </div>
                                        </div>
                                    </div>               
                                <div class="cli-ent-col td" style="width: 13%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $approved_request[$i]['id']; ?>/5'"style="cursor: pointer;">
                                            <?php if($approved_request[$i]['is_trail'] == 1){ ?>
                                            <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                                             <?php } ?>
                                            <div class="cli-ent-xbox">
                                                <h3 class="app-roved green">Approved on</h3>
                                                <p class="space-a"></p>
                                                <p class="pro-b">
                                                    <?php echo date('M d, Y h:i A', strtotime($approved_request[$i]['approvedate']));
                                                    ?>
                                                </p>
                                                 <p class="pro-b">May 30, 2018</p> 
                                            </div>
                                        </div>
                                    </div> <!-- End Row -->
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php if ($approved_request_count > LIMIT_QA_LIST_COUNT) { ?>
                            <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_active_project; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--QA Completed Section End Here -->
                
                
                 <!--QA Hold Section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $hold_request_count; ?>" class="tab-pane content-datatable datatable-width" id="Qa_hold" role="tabpanel">
                    <!--<div class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel">-->
                    <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                <div class="ajax_searchload" style="display:none;">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                </div>
                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                    <!-- <svg class="icon">
                                        <use xlink:href="<?php //echo base_url();         ?>theme/assets/qa/images/symbol-defs.svg#icon-magnifying-glass"></use>
                                    </svg> -->
                                </button>
                            </form>
                        </div>
                    </div>
                        <div class="product-list-show">
                           <?php if ($hold_request_count >= 1) { ?>
                            <div class="">
                                <div class="cli-ent-row tr tableHead">
                                    <div class="cli-ent-col td">
                                        <input type="checkbox" class="checkAll" data-attr="hold" name="project">
                                    </div>
                                    <div class="cli-ent-col td" style="width: 28%;">
                                        PROJECT NAME
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        CLIENT
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        PROJECT STATUS
                                    </div>
                                    <div class="cli-ent-col td" style="width: 12%;">
                                        DESIGNER
                                    </div>
                                    <div class="cli-ent-col td"style="width: 9%;">
                                        MESSAGES
                                    </div>
                                    <div class="cli-ent-col td"style="width: 13%;">
                                        Hold
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <?php 
                            //echo "<pre/>";print_r($hold_request);exit;
                            if (sizeof($hold_request) == 0) { ?>
                                <div class="nodata">
                                    <h3 class="head-b draft_no">They are no projects at this time</h3>
                                </div>
                            <?php } else { ?>
                                <?php for ($i = 0; $i < sizeof($hold_request); $i++) { ?>
                                    <!-- Start Row -->
                                    <div class="cli-ent-row tr brdr">
                                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                                        <div class="cli-ent-col td" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $hold_request[$i]['id']; ?>/6'"style="cursor: pointer;">
                                            <div class="cell-row">
                                                <div class="cell-col" >
                                                    <input type="checkbox" name="project" class="selected_pro hold" value="<?php echo $hold_request[$i]['id']; ?>"/> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cli-ent-col td" style="width: 28%;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">

                                                    <div class="cell-col" >
                                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $hold_request[$i]['id']; ?>/6"><?php echo $hold_request[$i]['title']; ?></a></h3>
                                                        <p class="pro-b">
                                                            <?php echo substr($hold_request[$i]['description'], 0, 30); ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $hold_request[$i]['id']; ?>/6'"style="cursor: pointer;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href="javascript:void(0);"><figure class="pro-circle-img">
                                                                <?php if ($hold_request[$i]['customer_profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$hold_request[$i]['customer_profile_picture'] ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure></a>
                                                    </div>
                                                    <div class="cell-col">
                                                        <p class="text-h text-wrap" title="<?php echo $hold_request[$i]['customer_first_name'] . " " . $hold_request[$i]['customer_last_name']; ?>">
                                                            <?php echo $hold_request[$i]['customer_first_name']; ?>
                                                            <?php
                                                            if (strlen($hold_request[$i]['customer_last_name']) > 5) {
                                                                echo ucwords(substr($hold_request[$i]['customer_last_name'], 0, 1));
                                                            } else {
                                                                echo $hold_request[$i]['customer_last_name'];
                                                            }
                                                            ?>

                                                        </p>
                                                        <p class="pro-b">Client</p>
                                                        <div class="sub_user_req_info qa-sub-user">
                                                                    <p class="text-h text-wrap">
                                                                        <?php echo $hold_request[$i]['sub_first_name']; ?>
                                                                        <?php
                                                                        if (strlen($hold_request[$i]['sub_last_name']) > 5) {
                                                                            echo ucwords(substr($hold_request[$i]['sub_last_name'], 0, 1));
                                                                        } else {
                                                                            echo $hold_request[$i]['sub_last_name'];
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                    <?php
                                                                    if ($hold_request[$i]['sub_first_name'] != '') {
                                                                        ?>
                                                                        <p class="pro-b">Sub User</p>
                                                                    <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $hold_request[$i]['id']; ?>/6'"style="cursor: pointer;">
                                            <div class="cell-col col-w1">
                                                <p class="neft text-center"><span class="holdcolor text-uppercase text-wrap">On Hold</span></p>
                                            </div>
                                        </div>

                                        <div class="cli-ent-col td" style="width: 12%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $hold_request[$i]['id']; ?>/6'"style="cursor: pointer;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <?php  if($hold_request[$i]['designer_id'] != "" && $hold_request[$i]['designer_id'] != 0){ ?>
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href="javascript:void(0);"><figure class="pro-circle-img">
                                                                <?php if ($hold_request[$i]['profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$hold_request[$i]['profile_picture'] ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure></a>
                                                    </div>
                                                    <div class="cell-col">
                                                        <p class="text-h text-wrap" title="<?php echo $hold_request[$i]['designer_first_name'] . " " . $hold_request[$i]['designer_last_name']; ?> ">
                                                            <?php echo $hold_request[$i]['designer_first_name']; ?>
                                                            <?php
                                                            if (strlen($hold_request[$i]['designer_last_name']) > 5) {
                                                                echo ucwords(substr($hold_request[$i]['designer_last_name'], 0, 1));
                                                            } else {
                                                                echo $hold_request[$i]['designer_last_name'];
                                                            }
                                                            ?>

                                                        </p>
                                                        <p class="pro-b">Designer</p>

                                                    </div>
                                                    <?php }else{ ?>
                                                        <div class="cell-col <?php echo  $hold_request[$i]['id'];?>">No Designer Assigned</div>
                                                    <?php } ?>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $hold_request[$i]['id']; ?>/5'"style="cursor: pointer;">
                                            <div class="cli-ent-xbox text-center">
                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $hold_request[$i]['id']; ?>/5">
                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($hold_request[$i]['total_chat'] + $hold_request[$i]['comment_count'] != 0) { ?>
                                                                <span class="numcircle-box">
                                                                    <?php echo $hold_request[$i]['total_chat'] + $hold_request[$i]['comment_count']; ?>
                                                                </span>
                                                            <?php } ?></span>
                                                        <?php //echo $approved_request[$i]['total_chat_all'];    ?></a></p>
                                            </div>
                                        </div>
                                     
                                <div class="cli-ent-col td" style="width: 13%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $hold_request[$i]['id']; ?>/5'"style="cursor: pointer;">
                                            <?php if($hold_request[$i]['is_trail'] == 1){ ?>
                                            <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                                             <?php } ?>
                                            <div class="cli-ent-xbox">
                                                <h3 class="app-roved holdcolors">Hold on</h3>
                                                <p class="space-a"></p>
                                                <p class="pro-b">
                                                    <?php echo date('M d, Y h:i A', strtotime($hold_request[$i]['modified']));
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div> <!-- End Row -->
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php if ($hold_request_count > LIMIT_QA_LIST_COUNT) { ?>
                            <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $hold_request_count; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--QA hold Section End Here -->
                
                <!--QA cancel section Start Here -->
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $cancel_request_count; ?>" class="tab-pane content-datatable datatable-width" id="Qa_cancel" role="tabpanel">
                    <!--<div class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel">-->
                    <div class="col-md-7"></div>
                    <div class="col-md-4">
                        <div class="search-box">
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                                <div class="ajax_searchload" style="display:none;">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" />
                                </div>
                                <input type="hidden" name="status" id="status" value="cancel">
                                <button type="submit" class="search-btn search search_data_ajax">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                                </button>
                            </form>
                        </div>
                    </div>
                        <div class="product-list-show">
                           <?php if ($cancel_request_count >= 1) { ?>
                            <div class="">
                                <div class="cli-ent-row tr tableHead">
                                    <div class="cli-ent-col td">
                                        <input type="checkbox" class="checkAll" data-attr="cancel" name="project">
                                    </div>
                                    <div class="cli-ent-col td" style="width: 28%;">
                                        PROJECT NAME
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        CLIENT
                                    </div>
                                    <div class="cli-ent-col td" style="width: 16%;">
                                        PROJECT STATUS
                                    </div>
                                    <div class="cli-ent-col td" style="width: 12%;">
                                        DESIGNER
                                    </div>
                                    <div class="cli-ent-col td"style="width: 9%;">
                                        MESSAGES
                                    </div>
                                    <div class="cli-ent-col td"style="width: 13%;">
                                        Hold
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <?php 
                            //echo "<pre/>";print_r($hold_request);exit;
                            if (sizeof($cancel_request) == 0) { ?>
                                <div class="nodata">
                                    <h3 class="head-b draft_no">They are no projects at this time</h3>
                                </div>
                            <?php } else { ?>
                                <?php for ($i = 0; $i < sizeof($cancel_request); $i++) { ?>
                                    <!-- Start Row -->
                                    <div class="cli-ent-row tr brdr">
                                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                                        <div class="cli-ent-col td" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $cancel_request[$i]['id']; ?>/6'"style="cursor: pointer;">
                                            <div class="cell-row">
                                                <div class="cell-col" >
                                                    <input type="checkbox" name="project" class="selected_pro cancel" value="<?php echo $cancel_request[$i]['id']; ?>"/> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cli-ent-col td" style="width: 28%;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">

                                                    <div class="cell-col" >
                                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $cancel_request[$i]['id']; ?>/6"><?php echo $cancel_request[$i]['title']; ?></a></h3>
                                                        <p class="pro-b">
                                                            <?php echo substr($cancel_request[$i]['description'], 0, 30); ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $cancel_request[$i]['id']; ?>/6'"style="cursor: pointer;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href="javascript:void(0);"><figure class="pro-circle-img">
                                                                <?php if ($cancel_request[$i]['customer_profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$cancel_request[$i]['customer_profile_picture'] ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure></a>
                                                    </div>
                                                    <div class="cell-col">
                                                        <p class="text-h text-wrap" title="<?php echo $cancel_request[$i]['customer_first_name'] . " " . $cancel_request[$i]['customer_last_name']; ?>">
                                                            <?php echo $cancel_request[$i]['customer_first_name']; ?>
                                                            <?php
                                                            if (strlen($cancel_request[$i]['customer_last_name']) > 5) {
                                                                echo ucwords(substr($cancel_request[$i]['customer_last_name'], 0, 1));
                                                            } else {
                                                                echo $cancel_request[$i]['customer_last_name'];
                                                            }
                                                            ?>

                                                        </p>
                                                        <p class="pro-b">Client</p>
                                                        <div class="sub_user_req_info qa-sub-user">
                                                                    <p class="text-h text-wrap">
                                                                        <?php echo $cancel_request[$i]['sub_first_name']; ?>
                                                                        <?php
                                                                        if (strlen($cancel_request[$i]['sub_last_name']) > 5) {
                                                                            echo ucwords(substr($cancel_request[$i]['sub_last_name'], 0, 1));
                                                                        } else {
                                                                            echo $cancel_request[$i]['sub_last_name'];
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                    <?php
                                                                    if ($cancel_request[$i]['sub_first_name'] != '') {
                                                                        ?>
                                                                        <p class="pro-b">Sub User</p>
                                                                    <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $cancel_request[$i]['id']; ?>/6'"style="cursor: pointer;">
                                            <div class="cell-col col-w1">
                                                <p class="neft text-center"><span class="holdcolor text-uppercase text-wrap">Cancelled</span></p>
                                            </div>
                                        </div>

                                        <div class="cli-ent-col td" style="width: 12%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $cancel_request[$i]['id']; ?>/6'"style="cursor: pointer;">
                                            <div class="cli-ent-xbox text-left">
                                                <div class="cell-row">
                                                    <?php  if($cancel_request[$i]['designer_id'] != "" && $cancel_request[$i]['designer_id'] != 0){ ?>
                                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                        <a href="javascript:void(0);"><figure class="pro-circle-img">
                                                                <?php if ($cancel_request[$i]['profile_picture'] != "") { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$cancel_request[$i]['profile_picture'] ?>" class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                                <?php } ?>
                                                            </figure></a>
                                                    </div>
                                                    <div class="cell-col">
                                                        <p class="text-h text-wrap" title="<?php echo $cancel_request[$i]['designer_first_name'] . " " . $cancel_request[$i]['designer_last_name']; ?> ">
                                                            <?php echo $cancel_request[$i]['designer_first_name']; ?>
                                                            <?php
                                                            if (strlen($cancel_request[$i]['designer_last_name']) > 5) {
                                                                echo ucwords(substr($cancel_request[$i]['designer_last_name'], 0, 1));
                                                            } else {
                                                                echo $cancel_request[$i]['designer_last_name'];
                                                            }
                                                            ?>

                                                        </p>
                                                        <p class="pro-b">Designer</p>

                                                    </div>
                                                    <?php }else{ ?>
                                                        <div class="cell-col <?php echo  $cancel_request[$i]['id'];?>">No Designer Assigned</div>
                                                    <?php } ?>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $cancel_request[$i]['id']; ?>/5'"style="cursor: pointer;">
                                            <div class="cli-ent-xbox text-center">
                                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $cancel_request[$i]['id']; ?>/5">
                                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($cancel_request[$i]['total_chat'] + $cancel_request[$i]['comment_count'] != 0) { ?>
                                                                <span class="numcircle-box">
                                                                    <?php echo $cancel_request[$i]['total_chat'] + $cancel_request[$i]['comment_count']; ?>
                                                                </span>
                                                            <?php } ?></span>
                                                        <?php //echo $approved_request[$i]['total_chat_all'];    ?></a></p>
                                            </div>
                                        </div>
                                     
                                <div class="cli-ent-col td" style="width: 13%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $cancel_request[$i]['id']; ?>/5'"style="cursor: pointer;">
                                            <?php if($cancel_request[$i]['is_trail'] == 1){ ?>
                                            <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                                             <?php } ?>
                                            <div class="cli-ent-xbox">
                                                <h3 class="app-roved holdcolors">Cancelled on</h3>
                                                <p class="space-a"></p>
                                                <p class="pro-b">
                                                    <?php echo date('M d, Y h:i A', strtotime($cancel_request[$i]['modified']));
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div> <!-- End Row -->
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php if ($cancel_request_count > LIMIT_QA_LIST_COUNT) { ?>
                            <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                            <div class="" style="text-align:center">
                                <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $cancel_request_count; ?>" class="load_more button">Load more</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!--QA cancel section End Here -->
            </div>
            </div>
          
            
        </div>
    </section>
    
    <!-- Modal -->
    <div class="modal fade" id="AddDesign" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-nose" role="document">
            <div class="modal-content">
                
                <div class="cli-ent-model-box">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="cli-ent-model">
                        <header class="fo-rm-header">
                            <h3 class="head-c text-center">Add Designer</h3>
                        </header>
                        <div class="noti-listpopup">
                            <div class="newsetionlist">
                                <div class="cli-ent-row tr notificate">
                                    <div class="cli-ent-col td" style="width: 30%;">
                                        <div class="cli-ent-xbox text-left">
                                            <h3 class="pro-head space-b">Designer</h3>
                                        </div>
                                    </div>
                                    
                                    <div class="cli-ent-col td" style="width: 45%;">
                                        <div class="cli-ent-xbox text-left">
                                            <h3 class="pro-head space-b">Skill</h3>
                                        </div>
                                    </div>
                                    
                                    <div class="cli-ent-col td" style="width: 25%;">
                                        <div class="cli-ent-xbox text-left">
                                            <h3 class="pro-head space-b text-center">Active Requests</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <form action="" method="post">
                                <ul class="list-unstyled list-notificate">
                                    <?php 
                                    //echo "<pre/>";print_r($all_designers);
                                    foreach ($all_designers as $value): ?>
                                    <li>
                                        <a href="#">
                                            <div class="cli-ent-row tr notificate">
                                                <div class="cli-ent-col td" >
                                                    <div class="radio-boxxes">
                                                        <label class="containerones">
                                                          <input type="radio" value="<?php echo $value['id'];?>" name="assign_designer" id="<?php echo $value['id'];?>" data-image-pic="<?php echo $value['profile_picture']?>" data-name="<?php echo $value['first_name'] ." ". $value['last_name'];?>">
                                                          <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                
                                                <div class="cli-ent-col td" style="width: 30%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="setnoti-fication">
                                                            <figure class="pro-circle-k1">
                                                            <?php if($value['profile_picture'] != ""){?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$value['profile_picture']?>" class="img-responsive">
                                                            <?php }else{ ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                            <?php } ?>
                                                            </figure>
                                                            <div class="notifitext">
                                                                <p class="ntifittext-z1"><strong><?php echo $value['first_name'] ." ". $value['last_name'];?></strong></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="cli-ent-col td" style="width: 45%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <p class="pro-a">UX Designer, Landing page, Mobile App  UX Designer, Landing page, Mobile App<span class="sho-wred">+2</span></p>
                                                    </div>
                                                </div>
                                                
                                                <div class="cli-ent-col td" style="width: 25%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="cli-ent-xbox text-center">
                                                            
                                                            <p class="neft text-center"><span class="red text-uppercase"><?php echo $value['active_request'];?></span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <?php endforeach; ?>
                                    
                                </ul>
                                <input type="hidden" name="request_id" id="request_id">
                                <p class="space-c"></p>
                                <p class="btn-x text-center"><button name="submit" type="submit" class="btn-g minxx1" id="assign_desig" data-dismiss="modal" aria-label="Close">Select Designer</button></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal similar-prop fade" id="editExpected" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <button id="close-d" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
                <div class="cli-ent-model">
                <header class="fo-rm-header">
                        <h3 class="head-c text-center">Edit Expected date</h3>
                    </header>
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/edit-date.png" class="img-responsive center-block">
                    <form action="<?php echo base_url(); ?>qa/dashboard/change_expectedDate" method="post">
                        <label>Expected Date</label>
                        <input type="text" name="edit_expected" class="edit_expected"  value="" readonly/>
                        <input type="hidden" name="edit_reqid" class="edit_reqid"  value=""/>
                        <p class="space-c"></p>
                        <p class="btn-x text-center">
                            <button name="submit" type="submit" class="btn-g minxx1">Save</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
  
    <input type="hidden" id="urlforback" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/jquery/dist/jquery.min.js"></script>
     <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/qa/bootstrap.min.js"></script>
    <script>       
    // For Ajax Search
    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results==null){
           return null;
        }
        else{
           return decodeURI(results[1]) || 0;
        }
    }
    var status = $.urlParam('status');
    $('#status_check #'+status+" a").click();
        $('#right_nav > li').click(function(){
            $(this).toggleClass('open');
        });
//        $('#status_check li a').click(function(){
//            var status = $(this).data('status');
//            $('#status').val(status);
//            $('#search_text').val("");
//            ajax_call();
//        });
//        
//        $('.searchdata').keyup(function(e){
//            ajax_call(); 
//        });
//        $('.search_data_ajax').click(function(e){
//            e.preventDefault();
//             ajax_call();
//        });
//
//        function ajax_call(){
//            var search = $('#search_text').val();
//            var status = $('#status').val();
//            $.get( "<?php echo base_url();?>qa/dashboard/search_ajax?title="+search+"&status="+status, function( data ){
//                var content_id = $('a[data-status="'+status+'"]').attr('href');
//               $( content_id ).html( data );  
//            }); 
//        }

        // End Ajax Search

        $(document).on('click', ".adddesinger", function(){
            var request_id = $(this).attr('data-requestid');
            var designer_id = $(this).attr('data-designerid');
            $('#AddDesign #request_id').val(request_id);
            $('#AddDesign input#'+designer_id).click();
        });
        $(document).on('click', ".permanent_assign_design", function(){
            var request_id = $(this).attr('data-requestid');
            var customer_id = $(this).attr('data-customerid');
            $('#AddPermaDesign #assign_request_id').val(request_id);
            $('#AddPermaDesign #assign_customer_id').val(customer_id);
        });

        $('#assign_desig').click(function(e){
            e.preventDefault();
            var designer_id = $('input[name=assign_designer]:checked').val();
            var request_id = $('#request_id').val();
            var designer_name = $('input[name=assign_designer]:checked').attr('data-name');
            var designer_pic = $('input[name=assign_designer]:checked').attr('data-image-pic');
            $.ajax({
                url:"<?php echo base_url();?>qa/Dashboard/assign_designer_ajax",
                type:'POST',
                data:{
                    'assign_designer':designer_id,
                    'request_id':request_id
                },
                success: function(data){
                    //$('.close').click();
                    $("."+request_id+" p.text-h").html(designer_name);
                    if(designer_pic == ""){
                      $("."+request_id+" img").attr('src','<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png');  
                    }else{
                        $("."+request_id+" img").attr('src','<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>'+designer_pic);
                    }
                }
            });

        });
        
        
        
//         $(document).ready(function() {
//            var status = $('#status_check li.active a').attr('data-status');
//            var id = $('#status_check li.active a').attr('href');
//            var total_pagin = $('#status_check li.active a').attr('data-pagin');
//            var total_record = 0;
//            var total_groups = total_pagin; 
//            // total_record++;
//            $(id).load("<?php echo base_url() ?>qa/dashboard/load_more",
//             {'group_no':total_record,'status':status}, function(){
//                 
//                total_record++;
//            });
//            $(window).scroll(function() { 
//
//            var scroll_total_pagin = $('#status_check li.active a').attr('data-pagin');
//            var status_scroll = $('#status_check li.active a').attr('data-status');
//            var tabid = $('#status_check li.active a').attr('href');  
//            var total_groups_sc = scroll_total_pagin;
//                if($(window).scrollTop()+$(window).height() >= $('#footer_div').offset().top)  
//                {          
//                    if(total_record < total_groups_sc)
//                    { 
//                      $.post('<?php echo site_url() ?>qa/dashboard/load_more',{'group_no': total_record,'status':status_scroll},
//                        function(data){ 
//                            if (data != "") { 
//                                //console.log(data);
//                                $(tabid).append(data);               
//                                total_record++;
//                            }
//                        });     
//                    }
//                }
//            });
//        });
            
function load_more_qa() {
    var bucket_type = '<?php echo $bucket_type; ?>';
    var tabid = $('#status_check li.active a').attr('href');
    var activeTabPane = $('.tab-pane.active');
    var status_scroll = $('#status_check li.active a').attr('data-status');
    var searchval = activeTabPane.find('.search_text').val();
    //activeTabPane.attr('data-search-text', searchval);
    $.post('<?php echo site_url() ?>qa/dashboard/load_more_qa', {'group_no': 0, 'status': status_scroll, 'search': searchval,'bucket_type':bucket_type},
    function (data) {
        var newTotalCount = 0;
        if (data != '') {
            //  console.log(tabid+ ".product-list-show .row");
            $(".ajax_searchload").fadeOut(500);
            $(tabid + " .product-list-show .row").html(data);
            newTotalCount = $(tabid + " .product-list-show .row").find("#loadingAjaxCount").attr('data-value');
            $(tabid + " .product-list-show .row").find("#loadingAjaxCount").remove();
            activeTabPane.attr("data-total-count", newTotalCount);
            activeTabPane.attr("data-loaded", $rowperpage);
            activeTabPane.attr("data-group", 1);
        } else {
            activeTabPane.attr("data-total-count", 0);
            activeTabPane.attr("data-loaded", 0);
            activeTabPane.attr("data-group", 1);
            $(tabid + ".product-list-show .row").html("");
        }
        if ($rowperpage >= newTotalCount) {
            activeTabPane.find('.load_more').css("display", "none");
        } else {
            activeTabPane.find('.load_more').css("display", "inline-block");
        }
    });
}


/*********Search *******************/
$('.search_data_ajax').click(function (e) {
    //console.log($rowperpage);
    e.preventDefault();
    load_more_qa();
});
$('.searchdata').keyup(function (e) {
    e.preventDefault();
    delay(function () {
    $(".ajax_searchload").fadeIn(500);
    load_more_qa();
    }, 500);
});

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

/**************start on load & tab change get all records with load more button*****************/
$(document).ready(function () {
    countTimer();
    $rowperpage = <?php echo LIMIT_QA_LIST_COUNT; ?>;
    var bucket_type = '<?php echo $bucket_type; ?>';
    $('.tab-pane').attr("data-loaded", $rowperpage);
    $(document).on("click", "#load_more", function () {
        countTimer();
        $(".ajax_loader").css("display","block");
        $(".load_more").css("display","none");
        var row = parseInt($(this).attr('data-row'));
        row = row + $rowperpage;
        var activeTabPane = $('.tab-pane.active');
        var searchval = activeTabPane.find('.search_text').val();
        var allcount = parseInt(activeTabPane.attr("data-total-count"));
        var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
        var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
        var status_scroll = $('#status_check li.active a').attr('data-status');
        var tabid = $('#status_check li.active a').attr('href');
        if (allLoaded < allcount) {
            $.post('<?php echo site_url() ?>qa/dashboard/load_more_qa', {'group_no': activeTabPaneGroupNumber, 'status': status_scroll, 'search': searchval,'bucket_type':bucket_type},
            function (data) {
                if (data != "") {
                    $(tabid + " .product-list-show .row").append(data);
                    row = row + $rowperpage;
                    $(".ajax_loader").css("display","none");
                    $(".load_more").css("display","inline-block");
                    activeTabPane.find('.load_more').attr('data-row', row);
                    activeTabPaneGroupNumber++;
                    activeTabPane.attr('data-group', activeTabPaneGroupNumber);
                    allLoaded = allLoaded + $rowperpage;
                    activeTabPane.attr('data-loaded', allLoaded);
                    if (allLoaded >= allcount) {
                        activeTabPane.find('.load_more').css("display", "none");
                    } else {
                        activeTabPane.find('.load_more').css("display", "inline-block");
                    }
                }
            });
        }
    });
});
/**************End on load & tab change get all records with load more button*****************/
$(".checkAll").change(function () {
var attribute = $(this).attr('data-attr');
//  console.log(attribute);
if ($(this).is(':checked')) {
    $('.' + attribute).prop("checked", true);
} else {
    $('.' + attribute).prop("checked", false);
}
var requestIDs = [];
$.each($("input[name='project']:checked"), function () {
    if ($(this).val() != 'on') {
        requestIDs.push($(this).val());
    }
});
$('.adddesinger').attr('data-requestid', requestIDs);
//console.log(requestIDs);
});

 $(document).on('change', '.selected_pro', function () {
var requestIDs = [];
$.each($("input[name='project']:checked"), function () {
    if ($(this).val() != 'on') {
        requestIDs.push($(this).val());
    }
});
$('.adddesinger').attr('data-requestid', requestIDs);
});

/***************Edit Expected****************/
$(document).on('click', '.editexpected', function () {
    var data_expected = $(this).attr('date-expected');
    var data_reqid = $(this).attr('date-reqid');
    $('#editExpected').modal();
    $('input[name=edit_expected]').val(data_expected);
    $('input[name=edit_reqid]').val(data_reqid);
});

$(document).on('click', '.edit_expected', function () {
    var date = jQuery(this).datetimepicker();
});


$(document).on('change', '.selected_pro', function () {
    var checked = $("input[type='checkbox']:checked").length;
    if (checked >= 1 && $(window).scrollTop() > '370') {
        $('.adddesinger').addClass('relate_check');
    } else {
        $('.adddesinger').removeClass('relate_check');
    }
});



function countTimer() {
    $('.expectd_date').each(function () {
        var expected_date = $(this).attr('data-date');
        //var user_datetimezone = ($(this).attr('data-timezonedate'))?$(this).attr('data-timezonedate'):'utc';
        var user_datetimezone = '<?php echo $_SESSION['timezone'] ?>';
        console.log(user_datetimezone);
        var data_id = $(this).attr('data-id');
//        console.log('expected_date',expected_date);
//        console.log('user_datetimezone',user_datetimezone);
// Set the date we're counting down to
        var countDownDate = new Date(expected_date).getTime();
        
// Update the count down every 1 second
        var x = setInterval(function () {
            // Get todays date and time
           var nowdate = new Date().toLocaleString('en-US', { timeZone: user_datetimezone });
           
           if(user_datetimezone != '' || user_datetimezone != null){
               var now = new Date(nowdate).getTime();
               //console.log("if"+user_datetimezone);
           }else{
               //console.log("else"+user_datetimezone);
               var now = new Date().getTime();
           }
//            console.log('countDownDate',countDownDate);
            
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            
            // Output the result in an element with id="demo"
            if(minutes > 0){
                var timer = ("0" + days).slice(-2) + " : " +("0" + hours).slice(-2) + " : "
                    + ("0" + minutes).slice(-2) + " : " + ("0" + seconds).slice(-2) + " ";
                $('#demo_'+data_id).text(timer);
//            document.getElementById("demo_" + data_id).text =  ("0" + days).slice(-2) + " : " +("0" + hours).slice(-2) + " : "
//                    + ("0" + minutes).slice(-2) + " : " + ("0" + seconds).slice(-2) + " ";
           // console.log("minutes greater",minutes+    "expectedd"+  expected_date);
           }else{
               $('#demo_'+data_id).text("EXPIRED");
               //document.getElementById("demo_" + data_id).text = "EXPIRED" ;
              // console.log("class","demo_" + data_id+    "expected"+  expected_date);
           }
            // If the count down is over, write some text 
//            if (distance < 0) {
//                clearInterval(x);
//                document.getElementById("demo" + data_id).innerHTML = "EXPIRED";
//            }
        }, 1000);
    });
    }
    
$(document).on('change','.is_verified input[type="checkbox"]',function(e){
//alert("cxvxcv");
var data_id = $(this).attr('data-pid');
var ischecked = ($(this).is(':checked')) ? 1 : 0;
if(ischecked == 1){
    $('#verified_'+data_id).addClass('verified_by_admin'); 
}else{
     $('#verified_'+data_id).removeClass('verified_by_admin'); 
}
  $.ajax({
        type: 'POST',
        url: "<?php echo base_url(); ?>qa/dashboard/request_verified_by_qa",
        data: {data_id: data_id,ischecked: ischecked},
        success: function (data) {
        }
  });
});
    </script>
