<style type="text/css">
    ul.list-header-blog li.active a{
        color: #e52344;
        border-bottom: 2px solid #e52344;
    }
    .list-header-blog.small li {
        margin-right: 0px;
    }
</style>
<?php //echo "<pre>"; print_r($active_project); exit;?>
<section class="con-b">
    <div class="container">
        <div class="header-blog">
            <div class="row flex-show">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="client-leftshows">
                                <?php if ($userdata[0]['profile_picture']){ ?>
                                    <figure class="cli-ent-img circle one">
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$userdata[0]['profile_picture'];?>" class="img-responsive one">
                                    </figure>
                                <?php }else{ ?>
                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 20px; font-family: GothamPro-Medium; color: #fff;padding-top: 20px;">
                                <?php echo ucwords(substr($userdata[0]['first_name'],0,1)) .  ucwords(substr($userdata[0]['last_name'],0,1)); ?>
                                    </figure>
                                <?php } ?>
                                <h3 class="pro-head-q space-b"><a href="javascript:void(0)"><?php echo ucwords($userdata[0]['first_name']) . " " . ucwords($userdata[0]['last_name']); ?></a></h3>
                                <p class="neft text-left">
                                    <span class="blue text-uppercase">
                                        <?php if ($userdata[0]['plan_turn_around_days'] == "1") { 
                                                echo "Premium Member"; 
                                            }elseif ($userdata[0]['plan_turn_around_days'] == "3") {
                                                echo "Standard Member";
                                            }else{
                                                echo "No Member";
                                            } ?>
                                    </span>
                                </p>
                            </div>
                        </div>
                        
                        <div class="col-md-8">
                            <ul id="status_check" class="list-unstyled list-header-blog small" role="tablist">
                                <li class="active" id="1"><a data-status="active,disapprove,assign" data-toggle="tab" href="#Qa_client_active" role="tab">Active (<?php echo sizeof($active_project) ?>)</a></li>          
                                <li  id="2"><a data-status="checkforapprove" data-toggle="tab" href="#Qa_client_pending" role="tab">Pending Approval (<?php echo sizeof($check_approve_project);?>)</a></li>
                                <li  id="3"><a data-status="approved" data-toggle="tab" href="#Qa_client_completed" role="tab">Completed (<?php echo sizeof($complete_project); ?>)</a></li>
                            </ul>
                        </div>
                    </div>
                
                </div>
                <div class="col-md-4">
                    <div class="search-box">
                        <form method="post" class="search-group clearfix">
                            <input type="text" placeholder="Search here..." class="form-control searchdata" id="search_text">
                            <input type="hidden" name="userid" value="<?php echo $userdata[0]['id'];?>" id="userid">
                             <input type="hidden" name="status" id="status" value="active,disapprove,assign">
                            <button type="submit" class="search-btn search search_data_ajax">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                               <!--  <svg class="icon">
                                    <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-magnifying-glass"></use>
                                </svg> -->
                            </button>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
        
        <p class="space-e"></p>
        
        <div class="cli-ent table">
            <div class="tab-content">
            <!--QA Active Section Start Here -->
                <div class="tab-pane active content-datatable datatable-width" id="Qa_client_active" role="tabpanel">
                     <?php //echo "<pre>"; print_r($active_project); exit;?>
                    <?php for ($i = 0; $i < sizeof($active_project); $i++) { 
                        ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $active_project[$i]['id']; ?>/1'" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 12%;">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">
                                    <?php if($active_project[$i]['status'] == "active") { 
                                        echo "Expected on";
                                    }elseif ($active_project[$i]['status'] == "disapprove") {
                                        echo "Expected on";
                                    }elseif ($active_project[$i]['status'] == "assign") {
                                        echo "In-Queue";
                                    } ?>
                                </p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php if($active_project[$i]['status'] == "active") { 
                                        echo date('M d, Y h:i A', strtotime($active_project[$i]['expected']));
                                    }elseif ($active_project[$i]['status'] == "disapprove") {
                                        echo date('M d, Y h:i A', strtotime($active_project[$i]['expected']));
                                    } ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 48%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    
                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url()."qa/dashboard/view_project/".$active_project[$i]['id']; ?>/1"><?php echo $active_project[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($active_project[$i]['description'], 0, 50); ?></p>
                                    </div>
<!--                                    <div class="cell-col"><?php //echo isset($active_project[$i]['priority']) ? $active_project[$i]['priority'] : ''; ?></div>-->
                                    <div class="cell-col col-w1">
                                        <p class="neft text-center">

                                            <?php if($active_project[$i]['status'] == "active" || $active_project[$i]['status'] ==  "checkforapprove" ){ ?>
                                            <span class="green text-uppercase">In-Progress</span>
                                        <?php }elseif($active_project[$i]['status'] == "disapprove"){ ?>
                                            <span class="red orangetext text-uppercase">REVISION</span>
                                        <?php } elseif ($active_project[$i]['status'] == "assign") { ?>
                                                <span class="gray text-uppercase">In-Queue</span>
                                           <?php } ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 20%;">
                            <div class="cli-ent-xbox text-left p-left1">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                        <a href="<?php echo base_url()."qa/dashboard/view_project/".$active_project[$i]['id']; ?>/1">
                                        <?php if($active_project[$i]['profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$active_project[$i]['profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($active_project[$i]['designer_first_name'],0,1)) .  ucwords(substr($active_project[$i]['designer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h">
                                            <?php
                                            if ($active_project[$i]['designer_first_name']) {
                                                echo $active_project[$i]['designer_first_name'];
                                                } else {
                                                    echo "No Designer";
                                                }
                                                ?>  
                                            </p>
                                        <p class="pro-b">Designer</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 10%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url()."qa/dashboard/view_project/".$active_project[$i]['id']; ?>/1">
                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if($active_project[$i]['total_chat'] + $active_project[$i]['comment_count'] != 0){ ?>
                                                <span class="numcircle-box">
                                                   <?php echo $active_project[$i]['total_chat'] + $active_project[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                <?php //echo $active_project[$i]['total_chat_all']; ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 10%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url()."qa/dashboard/view_project/".$active_project[$i]['id']; ?>/1">
                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                    <?php if(count($active_project[$i]['total_files']) != 0){?>
                                        <span class="numcircle-box">2</span></span>
                                    <?php  } ?>
                                <?php echo count($active_project[$i]['total_files_count']);?></a></p>
                            </div>
                        </div>
                    </div> <!-- End Row -->
                    <?php } ?>
                    
                  <!--   <p class="space-e"></p>
                    <div class="loader">
                        <a class="loader-link text-uppercase" href=""><svg class="icon" width="35">
                            <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-loading"></use>
                        </svg> Loading</a>
                    </div> -->
                </div>
            <!--QA Active Section End Here -->
            <!--QA Pending Approval Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_client_pending" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($check_approve_project); $i++) { 
                        ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $check_approve_project[$i]['id']; ?>/2'"  style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">
                                    Delivered on
                                </p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($check_approve_project[$i]['deliverydate']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 40%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    
                                    <div class="cell-col">
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url()."qa/dashboard/view_project/".$check_approve_project[$i]['id']; ?>/2"><?php echo $check_approve_project[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($check_approve_project[$i]['description'], 0, 50); ?></p>
                                    </div>
                                    
                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="red bluetext text-uppercase text-wrap">Pending-approval</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="<?php echo base_url()."qa/dashboard/view_project/".$check_approve_project[$i]['id']; ?>/2">
                                        <?php if($check_approve_project[$i]['customer_profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$check_approve_project[$i]['customer_profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($check_approve_project[$i]['customer_first_name'],0,1)) .  ucwords(substr($check_approve_project[$i]['customer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?>
                                    </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $check_approve_project[$i]['customer_first_name']." ".$check_approve_project[$i]['customer_last_name'];?>">
                                            <?php echo $check_approve_project[$i]['customer_first_name'];?>
                                            <?php 
                                                if(strlen($check_approve_project[$i]['customer_last_name']) > 5){ 
                                                    echo ucwords(substr($check_approve_project[$i]['customer_last_name'],0,1)); 
                                                }else{ 
                                                    echo $check_approve_project[$i]['customer_last_name']; 
                                                } ?>
                                            </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="<?php echo base_url()."qa/dashboard/view_project/".$check_approve_project[$i]['id']; ?>/2">
                                        <?php if($check_approve_project[$i]['profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$check_approve_project[$i]['profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($check_approve_project[$i]['designer_first_name'],0,1)) .  ucwords(substr($check_approve_project[$i]['designer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $check_approve_project[$i]['designer_first_name']." ".$check_approve_project[$i]['designer_last_name'];?>">
                                             <?php echo $check_approve_project[$i]['designer_first_name'];?>
                                            <?php 
                                                if(strlen($check_approve_project[$i]['designer_last_name']) > 5){ 
                                                    echo ucwords(substr($check_approve_project[$i]['designer_last_name'],0,1)); 
                                                }else{ 
                                                    echo $check_approve_project[$i]['designer_last_name']; 
                                                } ?>
                                            </p>
                                        <p class="pro-b">Designer</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url()."qa/dashboard/view_project/".$check_approve_project[$i]['id']; ?>/2">
                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if($check_approve_project[$i]['total_chat'] + $check_approve_project[$i]['comment_count'] != 0){ ?>
                                                <span class="numcircle-box">
                                                   <?php echo $check_approve_project[$i]['total_chat'] + $check_approve_project[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                <?php //echo $check_approve_project[$i]['total_chat_all'];?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url()."qa/dashboard/view_project/".$check_approve_project[$i]['id']; ?>/2">
                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                    <?php if(count($check_approve_project[$i]['total_files']) != 0){?>
                                        <span class="numcircle-box">2</span></span>
                                    <?php  } ?>
                                <?php echo count($check_approve_project[$i]['total_files_count']);?></a></p>
                            </div>
                        </div>
                    </div> <!-- End Row -->
                    <?php } ?>
                    
                    <!-- <p class="space-e"></p>
                    <div class="loader">
                        <a class="loader-link text-uppercase" href=""><svg class="icon" width="35">
                            <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-loading"></use>
                        </svg> Loading</a>
                    </div> -->
                </div>
            <!--QA Pending Approval Section End Here -->
            <!--QA Completed Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_client_completed" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($complete_project); $i++) { ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $complete_project[$i]['id']; ?>/3'" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 13%;">
                            <div class="cli-ent-xbox">
                                <h3 class="app-roved green">Approved on</h3>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($complete_project[$i]['approvddate']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 37%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    
                                    <div class="cell-col">
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $complete_project[$i]['id']; ?>/3"><?php echo $complete_project[$i]['title'];?></a></h3>
                                        <p class="pro-b"><?php echo substr($complete_project[$i]['description'], 0, 50); ?></p>
                                    </div>
                                    
                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $complete_project[$i]['id']; ?>/3">
                                        <?php if($complete_project[$i]['customer_profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$complete_project[$i]['customer_profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($complete_project[$i]['customer_first_name'],0,1)) .  ucwords(substr($complete_project[$i]['customer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title=" <?php echo $complete_project[$i]['customer_first_name']." ".$complete_project[$i]['customer_last_name'];?>">
                                            <?php echo $complete_project[$i]['customer_first_name'];?>
                                            <?php 
                                                if(strlen($complete_project[$i]['customer_last_name']) > 5){ 
                                                    echo ucwords(substr($complete_project[$i]['customer_last_name'],0,1)); 
                                                }else{ 
                                                    echo $complete_project[$i]['customer_last_name']; 
                                                } ?>
                                           </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $complete_project[$i]['id']; ?>/3">
                                        <?php if($complete_project[$i]['profile_picture'] != "" ){ ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$complete_project[$i]['profile_picture'];?>" class="img-responsive">
                                            </figure>
                                        <?php }else{ ?>
                                            <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                <?php echo ucwords(substr($complete_project[$i]['designer_first_name'],0,1)) .  ucwords(substr($complete_project[$i]['designer_last_name'],0,1)); ?>
                                            </figure>
                                        <?php } ?></a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $complete_project[$i]['designer_first_name']." ".$complete_project[$i]['designer_last_name'];?>">
                                            <?php echo $complete_project[$i]['designer_first_name'];?>
                                            <?php 
                                                if(strlen($complete_project[$i]['designer_last_name']) > 5){ 
                                                    echo ucwords(substr($complete_project[$i]['designer_last_name'],0,1)); 
                                                }else{ 
                                                    echo $complete_project[$i]['designer_last_name']; 
                                                } ?>
                                            </p>
                                        <p class="pro-b">Designer</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $complete_project[$i]['id']; ?>/3">
                                    <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                        <?php if($complete_project[$i]['total_chat'] + $complete_project[$i]['comment_count'] != 0){ ?>
                                                <span class="numcircle-box">
                                                   <?php echo $complete_project[$i]['total_chat'] + $complete_project[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                <?php //echo $complete_project[$i]['total_chat_all'];?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $complete_project[$i]['id']; ?>/3">
                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if(count($complete_project[$i]['total_files']) != 0){?>
                                        <span class="numcircle-box">2</span></span>
                                    <?php  } ?>
                                <?php echo count($complete_project[$i]['total_files_count']);?></a></p>
                            </div>
                        </div>
                    </div><!-- End Row -->
                    <?php } ?>
                    
                    <!-- <p class="space-e"></p>
                    <div class="loader">
                        <a class="loader-link text-uppercase" href=""><svg class="icon" width="35">
                            <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-loading"></use>
                        </svg> Loading</a>
                    </div> -->
                </div>
            <!--QA Completed Section End Here -->
            </div>
            
            
            
            
        </div>
        
        
        
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="AddClient" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c">Add Client</h3>
                    </header>
                    <div class="fo-rm-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">First Name</label>
                                    <p class="space-a"></p>
                                    <input type="text" class="form-control input-c">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">Last Name</label>
                                    <p class="space-a"></p>
                                    <input type="text" class="form-control input-c">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">Email</label>
                                    <p class="space-a"></p>
                                    <input type="email" class="form-control input-c">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">Phone</label>
                                    <p class="space-a"></p>
                                    <input type="tel" class="form-control input-c">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group goup-x1">
                            <label class="label-x3">Password</label>
                            <p class="space-a"></p>
                            <input type="text" class="form-control input-c">
                        </div>
                        
                        <div class="form-group goup-x1">
                            <label class="label-x3">Confirm Password</label>
                            <p class="space-a"></p>
                            <input type="text" class="form-control input-c">
                        </div>
                        
                        <hr class="hr-b">
                        <p class="space-b"></p>
                        
                        <div class="form-group goup-x1">
                            <label class="label-x3">Select payment Method</label>
                            <p class="space-a"></p>
                            <select class="form-control select-c">
                                <option>Credit/Debit Card.</option>
                                <option>Credit/Debit Card.</option>
                                <option>Credit/Debit Card.</option>
                            </select>
                        </div>
                        
                        <div class="form-group goup-x1">
                            <label class="label-x3">Card Number</label>
                            <p class="space-a"></p>
                            <input type="text" class="form-control input-c" placeholder="xxxx-xxxx-xxxx">
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">Expiry Date</label>
                                    <p class="space-a"></p>
                                    <input type="text" class="form-control input-c" placeholder="22.05.2022">
                                </div>
                            </div>
                            
                            <div class="col-sm-3 col-md-offset-4">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">CVC</label>
                                    <p class="space-a"></p>
                                    <input type="text" class="form-control input-c" placeholder="xxx" maxlength="3">
                                </div>
                            </div>
                        </div>
                        
                        <p class="btn-x"><button class="btn-g">Add Customer</button></p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/jquery/dist/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/qa/bootstrap.min.js"></script>
<script type="text/javascript">     
        // For Ajax Search
    $.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results==null){
           return null;
        }
        else{
           return decodeURI(results[1]) || 0;
        }
    }
    var status = $.urlParam('status');
    $('#status_check #'+status+" a").click();
        $('#right_nav > li').click(function(){
            $(this).toggleClass('open');
        });
     $('#status_check li a').click(function(){
            var status = $(this).data('status');
            $('#status').val(status);
            $('#search_text').val("");
            ajax_call();
        });

        $('.searchdata').keyup(function(e){
            ajax_call();  
        });

        $('.search_data_ajax').click(function(e){
            e.preventDefault();
             ajax_call();
        });
        function ajax_call(){
            var search = $('#search_text').val();
            var status = $('#status').val();
            var userid = $('#userid').val();
            $.get( "<?php echo base_url();?>qa/dashboard/search_ajax_customer_project?title="+search+"&status="+status+"&id="+userid, function( data ){
                var content_id = $('a[data-status="'+status+'"]').attr('href');
               $( content_id ).html( data );  
            });
        }
</script>