<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google" content="notranslate">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>QA Portal | Graphics Zoo</title>
	   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
	<!-- Bootstrap -->
	<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/qa/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/qa/reset.css" rel="stylesheet">
	<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/qa/adjust.css" rel="stylesheet">
	<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/qa/style.css" rel="stylesheet">
	<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/qa/responsive.css" rel="stylesheet">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>favicon.ico">
	<link href="<?php echo FS_PATH_PUBLIC_ASSETS;?>css/custom_style_for_all.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css"/>
        <script>
            var baseUrl = "<?php echo base_url(); ?>";
        </script> 
	<style type="text/css">
	a.custom-logo-link {max-width: 200px;
	}
	a.custom-logo-link img {width: 100%;}
	.list-setting .active a{
		color: #e73250;
	}
	select#customerwithornot {
		background-image: url(<?php echo FS_PATH_PUBLIC_ASSETS;?>img/qa/select-vector.png) !important; 
		background-repeat:no-repeat !important;
		-webkit-appearance: none !important;
		-moz-appearance: none !important;
		appearance: none !important;
		background-position: calc(100% - 15px) center !important;
		padding: 6px 15px;
		border-radius: 10px;
		background: #fff;
		font: normal 16px/25px 'GothamPro', sans-serif;
		box-shadow: 0 0 2px #d7d8d8;
		border: 1px solid #d7d8d8;
	}
	span.mess-box img {
		max-width: 45px;
	}
	ul.list-unstyled.list-notificate {
		max-height: 300px;
		overflow-y: auto;    overflow-x: hidden;
	}
    .setnoti-fication .notifitext {
    white-space: normal;
    width: 200px;
}
</style>
</head>
<?php
$CI = & get_instance();
$CI->load->library('myfunctions');
?>
<body class="deshboard-bgnone" style="background: #fafafa;">
	<header class="nav-section">
		<div class="container-fluid">
			<nav class="navbar navbar-default flex-show centered"> <!-- centered -->
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".small-menu" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="custom-logo-link" href="<?php echo base_url();?>qa/dashboard">
						<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/logo.svg" class="img-responsive"></a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse small-menu" >
						<ul class="nav navbar-nav text-uppercase">
							<li class="<?php if($titlestatus == "dashboard"){ echo "active";}?>">
								<a href="<?php echo base_url();?>qa/dashboard">
									<svg class="icon">
										<use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-briefing"></use>
									</svg> Project Dashboard
								</a>
							</li>
							<li class="<?php if($titlestatus == "view_clients"){ echo "active";}?>">
								<a href="<?php echo base_url();?>qa/dashboard/view_clients">
									<svg class="icon">
										<use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-users"></use>
									</svg> Clients
								</a>
							</li>
							<li class="<?php if($titlestatus == "view_designer"){ echo "active";}?>">
								<a href="<?php echo base_url();?>qa/dashboard/view_designer">
									<svg class="icon">
										<use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-paint-palette"></use>
									</svg> Designers
								</a>
							</li>
						</ul> 
					</div><!-- /.navbar-collapse -->

					<div class="box-setting">
						<ul class="list-unstyled list-setting" id="right_nav">
							<li class="<?php if($titlestatus == "qaprofile"){ echo "active";}?>"><a href="<?php echo base_url();?>qa/dashboard/profile">
								<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-setting.png" class="img-responsive">
                           <!--  <svg class="icon" width="26">
                                <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-settings"></use>
                            </svg> -->
                        </a></li>
                        <!-- Message Section -->
                        <li class="dropdown">
                        	<a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        		<span class="mess-box">
                        			<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-message.png" class="img-responsive"> 
                                   <!--  <svg class="icon">
                                        <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-notification-bell"></use>
                                    </svg> -->
                                    <?php
                                    if (isset($messagenotifications)) {
                                    	if (sizeof($messagenotifications) > 0) { ?>
                                    		<span class="numcircle-box">
                                    			<?php echo $messagenotification_number; ?>
                                    		</span>
                                    	<?php  }  }  ?>
                                    </span>
                                </a>
                                <div class="dropdown-menu">
                                	<h3 class="head-notifications">Messages</h3>
                                	<ul class="list-unstyled list-notificate">
                                		<?php if (sizeof($messagenotifications) > 0) { ?>
                                			<?php for ($i = 0; $i < sizeof($messagenotifications); $i++) { ?>
                                				<li><a href="<?php echo base_url().$messagenotifications[$i]['url'];?>">
                                					<div class="setnoti-fication">
                                						<figure class="pro-circle-k1">
                                							<img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive msg">
                                						</figure>

                                						<div class="notifitext">
                                							<p class="ntifittext-z1">
                                								<h5 style="font-size: 13px;"><?php echo $messagenotifications[$i]['heading']; ?></h5>
                                								<!--small style="font-size: 12px;"--><?php if(strlen($messagenotifications[$i]['title']) >= 50){
                                									echo substr($messagenotifications[$i]['title'], 0, 40).".."; 
                                								}else{
                                									echo $messagenotifications[$i]['title'];
                                								} ?><!--/small-->
                                							</p>
                                							<!-- <p class="ntifittext-z1"><strong>kevin Jenkins</strong> posted in <strong>Other Detail</strong></p> -->
                                							<p class="ntifittext-z2">
                                								<?php $approve_date = $messagenotifications[$i]['created']; 
                                								$msgdate = $CI->myfunctions->onlytimezoneforall($approve_date);
                                								echo $msgdate;
//                                                        echo date("M d, Y h:i A", strtotime($approve_date));
                                								?>
                                							</p>
                                						</div>
                                					</div>
                                				</a></li>
                                			<?php } }else{ ?>
                                				<li style="padding:10px;">No new message</li>
                                			<?php }?>

                                		</ul>
                                		<!-- <div class="seeAll"><a href="#">See All</a></div> -->
                                	</div>
                                </li>
                                <!-- Message Section End -->
                                <li class="dropdown">
                                	<a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                		<span class="bell-box">
                                			<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-bell.png" class="img-responsive"> 
                                   <!--  <svg class="icon">
                                        <use xlink:href="<?php echo base_url(); ?>theme/assets/qa/images/symbol-defs.svg#icon-notification-bell"></use>
                                    </svg> -->
                                    <?php
                                    if (isset($notifications)) {
                                    	if (sizeof($notifications) > 0) { ?>
                                    		<span class="numcircle-box">
                                    			<?php echo $notification_number;?>
                                    		</span>
                                    	<?php  }  }  ?>
                                    </span>
                                </a>
                                <div class="dropdown-menu">
                                	<h3 class="head-notifications">Notifications</h3>
                                	<ul class="list-unstyled list-notificate">
                                		<?php if (sizeof($notifications) > 0) { ?>
                                			<?php for ($i = 0; $i < sizeof($notifications); $i++) { 
                                				if ($notifications[$i]['profile_picture'] != '') {
                                					$notification_profile = $notifications[$i]['profile_picture'];
                                				} else {
                                					$notification_profile = "user-admin.png";
                                				}
                                				?>
                                				<li><a href="<?php echo base_url() . $notifications[$i]['url']; ?>">
                                					<div class="setnoti-fication">
                                						<figure class="pro-circle-k1">
                                							<img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$notification_profile;?>" class="img-responsive">
                                						</figure>

                                						<div class="notifitext" title="<?php echo $notifications[$i]['title']; ?>">
                                							<p class="ntifittext-z1"><strong><?php echo $notifications[$i]['first_name']." ".$notifications[$i]['last_name']; ?></strong>
                                								<?php echo $notifications[$i]['title']; ?>
                                							</p>
                                							<p class="ntifittext-z2">
                                								<?php $approve_date = $notifications[$i]['created']; 
                                								$notidate = $CI->myfunctions->onlytimezoneforall($approve_date);
                                								echo $notidate;
                                                         //echo date("M d, Y h:i A", strtotime($approve_date));
                                								?>
                                							</p>
                                						</div>
                                					</div>
                                				</a></li>
                                			<?php } }else{ ?>
                                				<li style="padding:10px; font: normal 12px/25px 'GothamPro', sans-serif;">No New notification</li>
                                			<?php }?>

                                		</ul>
                                		<!-- <div class="seeAll"><a href="#">See All</a></div> -->
                                	</div>
                                </li>
                                <li>
                                	<a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expended="false">
                                		<figure class="pro-circle-img-xxs">
                                                    <?php if ($edit_profile[0]['profile_picture']): ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $edit_profile[0]['profile_picture']; ?>" class="img-responsive">
                                                    <?php else: ?><img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive telset33">
                                                    <?php endif ?>
                                		</figure>
                                	</a>
                                	<div class="dropdown-menu pull-right" aria-labelledby="dropdownMenuButton">
                                		<h3 class="head-notifications"><?php echo $edit_profile[0]['first_name']." ".$edit_profile[0]['last_name'] ;?></h3>
                                		<h2 class="head-notifications"><?php echo $edit_profile[0]['email']; ?></h2>
                                		<ul class="list-unstyled list-notificate">
                                			<li>
                                				<a href="<?php echo base_url(); ?>qa/dashboard/logout">Logout</a>
                                			</li>
                                		</ul>
                                		<!-- <a href="<?php echo base_url(); ?>qa/dashboard/logout" class="dropdown-item btn-default btn-block" style="padding: 10px;font: normal 12px/25px 'GothamPro', sans-serif;">Logout</a> -->
                                	</div>
                                </li>
                            </ul>
                        </div>

                    </div><!-- /.container-fluid -->
                </nav>
            </div>
        </header>
