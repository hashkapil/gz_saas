<div id="footer_div"></div> 
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/jquery/dist/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/qa/bootstrap.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/qa/lightgallery-all.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/qa/jquery.mousewheel.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/js-card.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/all.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script>

<script type="text/javascript">
    setTimeout(function(){
        $('.alert').fadeOut();
    },2000);
    var $fileInput = $('.file-input');
    var $droparea = $('.file-drop-area');
		// highlight drag area
		$fileInput.on('dragenter focus click', function() {
            $droparea.addClass('is-active');
        });

		// back to normal state
		$fileInput.on('dragleave blur drop', function() {
            $droparea.removeClass('is-active');
        });

		// change inner text
		$fileInput.on('change', function() {
            var filesCount = $(this)[0].files.length;
            var $textContainer = $(this).prev();

            if (filesCount === 1) {
			// if single file is selected, show file name
			var fileName = $(this).val().split('\\').pop();
			$textContainer.text(fileName);
        } else {
			// otherwise show number of files
			$textContainer.text(filesCount + ' files selected');
        }
    });
		
     var total_design = <?php echo sizeof($designer_file); ?>;
     var current_design = 1;
     $(".next").click(function(){
        var index = $('#thub_nails li figure.active').closest('.active1').index();
        if(index+1 == total_design){
            $('#thub_nails li:first-child').click();
        }else{
            $('#thub_nails li figure.active').closest('.active1').next().click();
        }
        $('.slick-current .mydivposition .slid img').css('transform','scale(1)');
        setTimeout(function(){
         var chat_id = $('#cro .active').attr('id');
         $('.comment_sec').css('display','none');
         $('#comment_list_all #'+chat_id).css('display','block');
         $(".messagediv_"+chat_id).stop().animate({
             scrollTop: $(".messagediv_"+chat_id)[0].scrollHeight
         }, 1);
     },1000);
        
    });
     $(".prev").click(function(){
        var index = $('#thub_nails li figure.active').closest('.active1').index();
        if(index == 0){
            $('#thub_nails li:last-child').click();
        }else{
            $('#thub_nails li figure.active').closest('.active1').prev().click();
        }
        $('.slick-current .mydivposition .slid img').css('transform','scale(1)');
        setTimeout(function(){
            var chat_id = $('#cro .active').attr('id');
            $('.comment_sec').css('display','none');
            $('#comment_list_all #'+chat_id).css('display','block');
            $(".messagediv_"+chat_id).stop().animate({
                scrollTop: $(".messagediv_"+chat_id)[0].scrollHeight
            }, 1);
        },1000);

    });
     $(document).on('click', "#comment_list li", function(e){
                 //   alert('dummy');
                //  console.log( e.pageX + ", " + e.pageY );
                var clicked_id = $(this).find('.comments_link').attr('id');
                var topPosition = $("#"+clicked_id+"").css('top');
            var newtopPosition = topPosition.substring(0,topPosition.length-2); //string 800
            newtopPosition = parseFloat(newtopPosition) || 0;
            var position = $("#" + clicked_id + "").offset();
            var posX = position.left;
            var posR = ($(window).width() + $(window).scrollLeft()) - (position.left + $('#whatever').outerWidth(true));
            $(".mycancellable").click();
            $(".openchat").hide();
            if(newtopPosition < 100){
              $(".customer_chat").hide();
              $('.customer_chat'+clicked_id+' .posone2abs').addClass('topposition');
          }else{
              $(".customer_chat").hide();
              $('.customer_chat'+clicked_id+' .posone2abs').removeClass('topposition');
          }
          if(e.pageX - posR > 450){
            $('.customer_chat' + clicked_id + ' .posone2abs').addClass('leftposition');
        }else{
            $('.customer_chat' + clicked_id + ' .posone2abs').removeClass('leftposition');
        }
        if(newtopPosition > 500){
          $(".customer_chat").hide();
          $(".customer_chatimage2").hide();
          $(".customer_chatimage1").show();
          $(".customer_chatimage2.customer_chatimage" + clicked_id).show();
          $(".customer_chatimage1.customer_chatimage" + clicked_id).hide();
          $(".customer_chat" + clicked_id).toggle();
          $('html, body').animate({
             scrollTop: $(".customer_chat"+clicked_id+" .arrowdowns").offset().top
         }, 100);
      }else{
          $(".customer_chat").hide();
          $(".customer_chatimage2").hide();
          $(".customer_chatimage1").show();
          $(".customer_chatimage2.customer_chatimage" + clicked_id).show();
          $(".customer_chatimage1.customer_chatimage" + clicked_id).hide();
          $(".customer_chat" + clicked_id).toggle();
      }
  });
     $(document).on('click','.product-list-show .cli-ent-row.tr',function(){
        $(this).toggleClass('fullheight',1000);
        $(this).find('.mobile-visibles i').toggleClass('fa-plus-circle fa-minus-circle')
    });
</script>
</body>
</html>