<style type="text/css">
    ul.list-header-blog li.active a{
        color: #e52344;
        border-bottom: 2px solid #e52344;
    }
    .search-box {
        position: absolute;
        top: -86px;
        right: -91px;
    }
     .ajax_loader{
    margin-top:20px;
}
    .ajax_searchload {
     position: absolute;
       top: 19px;
    RIGHT: 96PX;
    }
    a.load_more.button {
        background-color: #e8304d;
        border: none;
        border-radius: 50px;
        color: white!important;
        padding: 8px 45px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 17px;
        margin: -5px 0;
        cursor: pointer;
        text-decoration: none !important;
        margin-top: 20px;
        font-family: 'Montserrat', sans-serif;
    }
</style>
<section class="con-b">
    <div class="container">
        <div class="header-blog">
            <div class="row flex-show">
                <div class="col-md-8">
                   <ul id="status_check" class="list-unstyled list-header-blog small" role="tablist">
                        <li class="active">
                            <a data-toggle="tab" href="#Admin_active" data-status="active" role="tab" data-pagin ="<?php echo $designerscount; ?>">Current Designers(<?php echo $designerscount; ?>)</a>
                        </li>       
                    </ul>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
        <p class="space-e"></p>
        <div class="cli-ent table" id="designerslist">
            <div class="tab-content">
                <div data-group="1" data-loaded=""  data-total-count="<?php echo $designerscount; ?>" class="tab-pane active content-datatable datatable-width" id="Admin_active" role="tabpanel">
                     <div class="col-md-7"></div>
                     <div class="col-md-4">
                        <div class="search-box">
                        <form method="post" class="search-group clearfix">
                            <input type="text" placeholder="Search here..." class="form-control searchdata search_text">
                            <div class="ajax_searchload" style="display:none;">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" />
                            </div>
                            <input type="hidden" name="status"  value="active,disapprove,assign,pending,checkforapprove">
                            <button type="submit" class="search-btn search search_data_ajax">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-search.png" class="img-responsive">
                            </button>
                        </form>
                    </div>
                        </div>
                     <div class="product-list-show">
                        <div class="row add-rows">
                            <?php for ($i = 0; $i < sizeof($designers); $i++) { ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'" style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 10%;">
                    <?php
                        if ($designers[$i]['profile_picture']) {
                            ?>
                        <figure class="cli-ent-img circle one">
                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$designers[$i]['profile_picture'];?>" class="img-responsive one">
                        </figure>
                    <?php }else{ ?>
                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 20px;font-family: GothamPro-Medium;">
                            <?php echo ucwords(substr($designers[$i]['first_name'],0,1)) .  ucwords(substr($designers[$i]['last_name'],0,1)); ?>
                        </figure>
                    <?php } ?>
                </div>
                <div class="cli-ent-col td" style="width: 35%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col">
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>"><?php echo $designers[$i]['first_name'] . " " . $designers[$i]['last_name'] ?></a></h3>
                                <p class="pro-b"><?php echo $designers[$i]['email']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="cli-ent-col td" style="width: 15%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Sign Up Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b"><?php echo date("M /d /Y", strtotime($designers[$i]['created'])) ?></p>
                    </div>
                </div>
                
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Requests Being Handled</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="green text-uppercase"><?php echo $designers[$i]['handled']; ?></span></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active Requests</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $designers[$i]['active_request']; ?></span></p>
                    </div>
                </div>
            </div> 
            <!-- End Row -->
            <?php } ?>
                        </div>
                         <?php if ($designerscount > LIMIT_QA_LIST_COUNT) { ?>
                <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" /></div>
                    <div class="" style="text-align:center">
                        <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_active_project; ?>" class="load_more button">Load more</a>
                    </div>
                <?php } ?>
                         
                    </div>
            </div>
        </div>
        </div>
    </div>
        </section>

<!-- Modal -->
<div class="modal fade" id="AddDesigners" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c">Add Designer</h3>
                    </header>
                    <div class="fo-rm-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">First Name</label>
                                    <p class="space-a"></p>
                                    <input type="text" class="form-control input-c">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">Last Name</label>
                                    <p class="space-a"></p>
                                    <input type="text" class="form-control input-c">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">Email</label>
                                    <p class="space-a"></p>
                                    <input type="email" class="form-control input-c">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group goup-x1">
                                    <label class="label-x3">Phone</label>
                                    <p class="space-a"></p>
                                    <input type="tel" class="form-control input-c">
                                </div>
                            </div>
                        </div>
                        
                        <hr class="hr-b">
                        
                        <p class="space-c"></p>
                        
                        <div class="form-group goup-x1">
                            <label class="label-x3">Password</label>
                            <p class="space-a"></p>
                            <input type="text" class="form-control input-c">
                        </div>
                        
                        <div class="form-group goup-x1">
                            <label class="label-x3">Confirm Password</label>
                            <p class="space-a"></p>
                            <input type="text" class="form-control input-c">
                        </div>
                        
                        <p class="space-b"></p>
                        
                        <p class="btn-x"><button class="btn-g">Add Designer</button></p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
/**************start on load & tab change get all records with load more button*****************/

function load_more_designerlist() {
var tabid = $('#status_check li.active a').attr('href');
var activeTabPane = $('.tab-pane.active');
var status_scroll = $('#status_check li.active a').attr('data-status');
var searchval = activeTabPane.find('.search_text').val();
$.post('<?php echo site_url() ?>qa/dashboard/load_more_designer', {'group_no': 0, 'status': status_scroll, 'search': searchval},
function (data) {
    var newTotalCount = 0;
    if (data != '') {
        //console.log(tabid);
        $(".ajax_searchload").fadeOut(500);
        $(tabid + " .product-list-show .add-rows").html(data);
        newTotalCount = $(tabid + " .product-list-show .add-rows ").find("#loadingAjaxCount").attr('data-value');
        $(tabid + " .product-list-show .add-rows").find("#loadingAjaxCount").remove();
        activeTabPane.attr("data-total-count", newTotalCount);
        activeTabPane.attr("data-loaded", $rowperpage);
        activeTabPane.attr("data-group", 1);
    } else {
        activeTabPane.attr("data-total-count", 0);
        activeTabPane.attr("data-loaded", 0);
        activeTabPane.attr("data-group", 1);
        $(tabid + ".product-list-show .add-rows").html("");
    }
    if ($rowperpage >= newTotalCount) {
        activeTabPane.find('.load_more').css("display", "none");
    } else {
        activeTabPane.find('.load_more').css("display", "inline-block");
    }
});
}


$('.searchdata').keyup(function (e) {
e.preventDefault();
delay(function () {
    $(".ajax_searchload").fadeIn(500);
    load_more_designerlist();
}, 1000);
});
var delay = (function () {
var timer = 0;
return function (callback, ms) {
    clearTimeout(timer);
    timer = setTimeout(callback, ms);
};
})();

$('.search_data_ajax').click(function (e) {
e.preventDefault();
load_more_designerlist();
});
$(document).ready(function () {
    $rowperpage = <?php echo LIMIT_QA_LIST_COUNT; ?>;
    $('.tab-pane').attr("data-loaded", $rowperpage);
    $(document).on("click", "#load_more", function () {
        $(".ajax_loader").css("display","block");
        $(".load_more").css("display","none");
        var row = parseInt($(this).attr('data-row'));
        row = row + $rowperpage;
        var activeTabPane = $('.tab-pane.active');
        var searchval = activeTabPane.find('.search_text').val();
        var allcount = parseInt(activeTabPane.attr("data-total-count"));
        var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
        var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
        var status_scroll = $('#status_check li.active a').attr('data-status');
        var tabid = $('#status_check li.active a').attr('href');
        if (allLoaded < allcount) {
            $.post('<?php echo site_url() ?>qa/dashboard/load_more_designer', {'group_no': activeTabPaneGroupNumber, 'status': status_scroll, 'search': searchval},
            function (data) {
                if (data != "") {
                    $(tabid + " .product-list-show .row").append(data);
                    row = row + $rowperpage;
                    $(".ajax_loader").css("display","none");
                    $(".load_more").css("display","inline-block");
                    activeTabPane.find('.load_more').attr('data-row', row);
                    activeTabPaneGroupNumber++;
                    activeTabPane.attr('data-group', activeTabPaneGroupNumber);
                    allLoaded = allLoaded + $rowperpage;
                    activeTabPane.attr('data-loaded', allLoaded);
                    if (allLoaded >= allcount) {
                        activeTabPane.find('.load_more').css("display", "none");
                    } else {
                        activeTabPane.find('.load_more').css("display", "inline-block");
                    }
                }
            });
        }
    });
});
/**************End on load & tab change get all records with load more button*****************/
</script>