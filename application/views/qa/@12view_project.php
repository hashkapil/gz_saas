<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>public/css/jquery.fancybox.min.css">
<style type="text/css">
    .overlay{
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        height: 100%;
        width: 100%;
        opacity: 0;
        transition: .5s ease;
        background-color:rgba(2, 2, 2, 0.5019607843137255);
    }
    .dropify-preview:hover .overlay{
        opacity: 1;
    }
    .rem_btn{
        color: white;
        font-size: 20px;
        position: absolute;
        top: 15%;
        right: 0%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
    }
    .rem_btn button{
        z-index: 7;
        top: 10px;
        right: 10px;
        background: 0 0;
        border: 2px solid #FFF;
        text-transform: uppercase;
        font-size: 11px;
        padding: 10px 10px;
        font-weight: 700;
        color: #FFF;
        font-family: GothamPro-Medium;
    }
    .draft_no {
        position: absolute;
        top: 90%;
        left: 30%;
        color: #5963759c;
    }
    figure.msgk-uimg span {
        font-size: 11px;
        color: #55747d;
        text-align: center;
    }
    figure.msgk-uimg {
        display: grid;
    }
    .msgk-user-chat{
        margin-bottom: 0px;
    }
    .msgk-chatrow{
        padding-top: 0px;
    }
    .msgk-mn.center_message .msgk-umsxxt{
        border-radius: 22px !important;
    }
    .msgk-mn.last_message .msgk-umsxxt{
        border-radius: 22px 20px 0 22px !important;
    }
    .msgk-left .last_message .msgk-umsxxt{
        border-radius: 22px 20px 22px 0 !important;
    }
    .msgk-user-chat.msgk-right {
        padding-right: 40px;
    }
    .msgk-right .right-img{
        left: auto;
        right: 0;
    }
    .msgk-umsgbox span{
        width: 100%;
        word-wrap: break-word;
    }

    #message_container .msgk-chatrow.clearfix:last-child {
        margin-bottom: 10px;
    }
    #message_container_without_cus .msgk-chatrow.clearfix:last-child {
        margin-bottom: 10px;
    }
    a.vwwv img {
        height: 100%;
        width: 100%;
    }
    a.msgk-uleft img {
        height: 100%;
    }
    .vewviewer-cicls img {
        height: 100%;
    }
    .hoversetss {
        left: -30%;
    }
    .srdselect {
        padding:6px 5px !important;
        padding-right:30px !important;
        max-width: 100% !important;
        width: 196px;
    }
    .select-b::-ms-expand {
        display: none;
    }
    .orangetext{
        background: #FF8C00 !important;
        color: #fff !important;
        border-color: #FF8C00 !important; 
    }
    .bluetext{
        background: #000080 !important;
        color: #fff !important;
        border-color: #000080 !important; 
    }
    .lightbluetext{
        color: #fff !important;
        border-color: #0000FF !important; 
        background: #0000FF !important  ;
    }
    .green{
        color: #fff !important;
        border-color: #37c473 !important; 
        background: #37c473  !important  ;
        font: bold 14px/25px 'GothamPro', sans-serif !important;
    }
    .gray {
        background-color: #a3a8ae !important;
        border-color: #a3a8ae !important;
        color: #ffffff !important;
    }
    /*#message_container .msgk-chatrow:nth-child(1){
        display: none;
    }*/
    .margin_top {
        margin-top: 20px;
    }
    .red{
        background-color: #e52344 !important;
        color: #ffffff !important;
        border: none !important;
    }
    .notestxx1 {
        position: absolute;
        z-index: 5;
        width: 100%;
        height: 100%;
        display: block;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        padding: 15px;
    }
    .dropify-preview i {
        font-size: 100px;
    }
    .dropify-preview {
        text-align: center;
    }
    h4.file-msg {
        padding: 34px 0px;
    }
    .project-row-qq2.chat_box {
        clear: both;
    }
    .srd-col.status_opt {
        width: 50%;
        float: left;
        margin-top: -10px;
    }
    .status_inf {
        width: 50%;
        float: left;
    }
    .status_opt select#customerwithornot{
        padding: 6px 32px 6px 7px;
        font: normal 12px/25px 'GothamPro', sans-serif;
    }
    button.btn.btn-y {
        background: #37c473;
        color: #fff;
        width: 30%;
    }
    button.btn.btn-n {
        background: #e73250;
        color: #fff;
        width: 30%;
    }
    #myModal .modal-dialog {
        width: 480px;
        margin: 50px auto;
    }
    #myModal .modal-content{
        height: 200px;
    }
    #myModal .cli-ent-model {
        padding: 60px;
    }
    a.icon-crss-2 {
        width: 100%;
        height: 100%;
    }
    .accimgbx33{
        height:125px;
    }
    .fan_cy a img{
        height: 100%;
    }
    .fan_cy {
        display: inline-flex;
        max-height: 120px;
        max-width: 150px;
    }
</style>
<section class="con-b">
    <div class="container"> 
        <p class="space-d"></p>
        <div style="display: none;" id="statusfail" class="alert alert-danger alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <p class="head-c">
                Something went wrong.
            </p>
        </div>
        <div style="display: none;" id="statuspass" class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <p class="head-c">
                Status successfully changed.
            </p>
        </div>
        <?php if ($this->session->flashdata('message_error') != '') { ?>
            <div id="message" class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_error'); ?>
                </p>
            </div>
        <?php } ?>

        <?php if ($this->session->flashdata('message_success') != '') { ?>
            <div id="message" class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_success'); ?>
                </p>
            </div>
        <?php }
        ?>

        <div class="row ">
            <?php preg_match("/[^\/]+$/", $_SERVER['REQUEST_URI'], $matches); ?>
            <?php
            if (isset($_SERVER['HTTP_REFERER'])) {
                if (strpos($_SERVER['HTTP_REFERER'], 'qa/dashboard/view_project') == false) {
                    $last_url = explode('?', $_SERVER['HTTP_REFERER'])[0];
                    $_SESSION['lastUrl'] = $last_url;
                } else {
                    if (isset($_SESSION['lastUrl'])) {
                        $last_url = $_SESSION['lastUrl'];
                    } else {
                        $last_url = base_url() . '/qa/dashboard';
                    }
                }
            } else {
                if (isset($_SESSION['lastUrl'])) {
                    $last_url = $_SESSION['lastUrl'];
                } else {
                    $last_url = base_url() . '/qa/dashboard';
                }
            }
            ?>
            <div class="col-md-8">
                <div class="rheight-xt">
                    <h3 class="head-b"><p class="savecols-col left"><a href="<?php echo $last_url; ?>?status=<?php echo $last_word = $matches[0]; ?>" class="backbtns"></a></p> <?php echo $data[0]['title']; ?></h3>
                    <p class="space-b"></p>
                </div>
                <div class="project-row-qq1">
                    <div class="design-draft-header ddnos mobile">
                        <div class="row flex-show">
                            <div class="col-sm-4">
                                <div class="descol">
                                    <h3 class="head-b">Design Drafts</h3>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="descol right-instbox">
                                    <?php if ($data[0]['status'] != "approved") { ?>
                                        <a href="#" class="upl-oadfi-le" data-toggle="modal" data-target="#Addfiles" >
                                            <span class="icon-crss-3">
                                                <span class="icon-circlxx55">+</span>
                                                <p class="attachfile">Add files</p>
                                            </span>
                                        </a>
                                        <?php if ($data[0]['designer_assign_or_not'] == 0) { ?>
                                            <a href="#" class="upl-oadfi-le noborder adddesinger" data-toggle="modal" data-target="#AddDesigner" data-requestid="<?php echo $data[0]['id']; ?>" data-designerid= "<?php echo $data[0]['designer_id']; ?>">
                                                <span class="icon-crss-3">
                                                    <span class="icon-circlxx55">+</span>
                                                    <p class="attachfile">Add Designer</p>
                                                </span>
                                                <input class="file-input" multiple="" type="file">
                                            </a>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if ($data[0]['status'] != "assign") { ?>
                                        <div class="right-inst"> 
                                            <span class="vewiew-boxxt11">
                                                <span class="vewviewer-cicls" >
                                                    <?php if (($edit_profile[0]['profile_picture'] != "") || (file_exists(base_url() . "uploads/profile_picture/" . $edit_profile[0]['profile_picture']))) { ?>
                                                        <img src="<?php echo base_url() ?>uploads/profile_picture/<?php echo $edit_profile[0]['profile_picture']; ?>" class="img-responsive"/>
                                                    <?php } else { ?>
                                                        <img src="<?php echo base_url() ?>uploads/profile_picture/user-admin.png" class="img-responsive"/>
                                                    <?php } ?>
                                                </span>
                                                <span class="vewviewer-cicls" >
                                                    <?php if (($data[0]['customer_profile_picture'] != "") || (file_exists(base_url() . "uploads/profile_picture/" . $data[0]['customer_profile_picture']))) { ?>
                                                        <img src="<?php echo base_url() ?>uploads/profile_picture/<?php echo $data[0]['customer_profile_picture']; ?>" class="img-responsive"/>
                                                    <?php } else { ?>
                                                        <img src="<?php echo base_url() ?>uploads/profile_picture/user-admin.png" class="img-responsive"/>
                                                    <?php } ?>
                                                </span>
                                                <span class="vewviewer-cicls" >
                                                    <?php if (($data[0]['profile_picture'] != "") || (file_exists(base_url() . "uploads/profile_picture/" . $data[0]['profile_picture']))) { ?>
                                                        <img src="<?php echo base_url() ?>uploads/profile_picture/<?php echo $data[0]['profile_picture']; ?>" class="img-responsive"/>
                                                    <?php } else { ?>
                                                        <img src="<?php echo base_url() ?>uploads/profile_picture/user-admin.png" class="img-responsive"/>
                                                    <?php } ?>
                                                </span>
                                            </span>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="orb-xb">
                        <div class="orb-xb-col right">
                            <div class="orbxb-img-xx3s one-can">
                                <div class="row-designdraftnew">
                                    <?php if ($data[0]['designer_attachment']) { ?>
                                        <ul class="list-unstyled clearfix list-designdraftnew">
                                            <?php for ($i = 0; $i < count($data[0]['designer_attachment']); $i++) { ?>
                                                <li>
                                                    <div class="figimg-one">
                                                        <a href="<?php echo base_url() . "qa/dashboard/view_files/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $request_id; ?>">
                                                            <?php
                                                            $type = substr($data[0]['designer_attachment'][$i]['file_name'], strrpos($data[0]['designer_attachment'][$i]['file_name'], '.') + 1);
                                                            ?>
                                                            <img src="<?php echo imageUrl($type,$data[0]['id'],$data[0]['designer_attachment'][$i]['file_name']); ?>" class="img-responsive">
                                                            <?php if ($data[0]['designer_attachment'][$i]['qa_seen'] == 0) { ?>
                                                                <span class="adnewpp">New</span>
                                                            <?php } ?>
                                                            <?php if ($data[0]['designer_attachment'][$i]['status'] == "pending") { ?>
                                                                <span class="notestxx1">
                                                                    <p class="text-right">
                                                                        <span class="notapp-roved">Pending review</span>
                                                                    </p>
                                                                </span>
                                                            <?php } elseif ($data[0]['designer_attachment'][$i]['status'] == "Reject") { ?>
                                                                <span class="notestxx">
                                                                    <p class="text-right">
                                                                        <span class="notapp-roved">Rejected</span>
                                                                    </p>
                                                                </span>
                                                            <?php } elseif ($data[0]['designer_attachment'][$i]['status'] == "Approve" && $data[0]['status'] == 'approved') { ?>
                                                                <span class="notestxx">
                                                                    <p class="text-right">
                                                                        <span class="notapp-roved">Approved</span>
                                                                    </p>
                                                                </span> 
                                                            <?php } ?>

                                                        </a>
                                                        <div class="chatbox-add">
                                                            <a href="<?php echo base_url() . "qa/dashboard/view_files/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $request_id; ?>">
                                                                <div class="chaticoncrl01">
                                                                    <img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat-1.png" class="img-responsive">
                                                                    <?php for ($j = 0; $j < count($file_chat_array); $j++) { ?>
                                                                        <?php if ($file_chat_array[$j]['id'] == $data[0]['designer_attachment'][$i]['id']) { ?>
                                                                            <span class="numcircle-box-01"><?php echo $file_chat_array[$j]['count']; ?></span>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php } ?>

                                        </ul>
                                    <?php } else { ?>
                                        <div class="figimg-one no_img">
                                            <?php if ($data[0]['status'] == 'draft') { ?>
                                                <h3 class="head-b draft_no">
                                                    Design Project is in Draft
                                                </h3>
                                                <a class="btn btn-x draft_btn1" href="<?php echo base_url(); ?>customer/request/new_request_brief/<?php echo $data[0]['id']; ?>?rep=1">Edit</a>
                                                <a class="btn btn-x draft_btn2" href="<?php echo base_url(); ?>customer/request/new_request_review/<?php echo $data[0]['id']; ?>?rep=1">Publish</a>
                                            <?php } elseif ($data[0]['status'] == 'assign') { ?>
                                                <h3 class="head-b draft_no">
                                                    Design Project is in queue
                                                </h3>
                                            <?php } elseif ($data[0]['status'] == 'active') { ?>
                                                <h3 class="head-b draft_no">
                                                    Designer is working on the designs
                                                </h3>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="srd-col status_opt">
                    <input type="hidden" value="<?php echo $data[0]['id']; ?>" id="request_id_status">
                    <select class="statusselect select-b form-control" onchange="statuschange(<?php echo $data[0]['id']; ?>, this.value)">
                        <option disabled value="" selected>Change Status</option>
                        <option <?php
                        if ($data[0]['status'] == "checkforapprove") {
                            echo 'disabled';
                        }
                        ?> value="checkforapprove">Review your design</option>
                        <option <?php
                        if ($data[0]['status'] == "active") {
                            echo 'disabled';
                        }
                        ?> value="active">Design In Progress</option>
                        <option <?php
                        if ($data[0]['status'] == "approved") {
                            echo 'disabled';
                        }
                        ?> value="approved">Completed</option>
                        <option <?php
                        if ($data[0]['status'] == "disapprove") {
                            echo 'disabled';
                        }
                        ?> value="disapprove">Revision In Progress</option>
                        <option <?php
                        if ($data[0]['status'] == "assign") {
                            echo 'disabled';
                        }
                        ?> value="assign">In Queue</option>
                    </select>
                </div>
                <div class="rheight-xt toprrsrr status_inf">
                    <p class="btn-x text-right">
                        <?php
                        if ($data[0]['status_designer'] == "checkforapprove") {
                            $status = "Pending Approval";
                            $color = "bluetext";
                        } elseif ($data[0]['status_designer'] == "active") {

                            $status = "Design In Progress";
                            $color = "green";
                        } elseif ($data[0]['status_designer'] == "disapprove" && $data[0]['who_reject'] == 1) {
                            $status = "Revision";
                            $color = "orangetext";
                        } elseif ($data[0]['status_designer'] == "disapprove" && $data[0]['who_reject'] == 0) {
                            $status = "Quality Revision";
                            $color = "red";
                        } elseif ($data[0]['status_designer'] == "pending" || $data[0]['status_designer'] == "assign") {
                            $status = "In Queue";
                            $color = "gray";
                        } elseif ($data[0]['status_designer'] == "draft") {
                            $status = "Draft";
                            $color = "gray";
                        } elseif ($data[0]['status_designer'] == "approved") {
                            $status = "Completed";
                            $color = "green";
                        } elseif ($data[0]['status_designer'] == "pendingrevision") {
                            $status = "Pending Review";
                            $color = "lightbluetext";
                        } else {
                            $status = "";
                            $color = "greentext";
                        }
                        ?>
                        <a class="btn-d <?php echo $color; ?>" href="javascript:void(0)" style="border-radius: 15px;">
                            <?php echo $status; ?>
                        </a>




                        <!-- <a class="btn-d" href="javascript:void(0)" style="border-radius: 15px;"><?php
                        if ($data[0]['status_qa'] == "active") {
                            echo "IN-PROGRESS";
                        } elseif ($data[0]['status_qa'] == "assign") {
                            echo "IN-QUEUE";
                        } elseif ($data[0]['status_qa'] == "checkforapprove") {
                            echo "PENDING-APPROVAL";
                        } elseif ($data[0]['status_qa'] == "approved") {
                            echo "COMPLETED";
                        } elseif ($data[0]['status_qa'] == "disapprove") {
                            echo "REVISION";
                        } elseif ($data[0]['status_qa'] == "pendingrevision") {
                            echo "PENDING REVIEW";
                        }
                        ?></a> -->
                    </p>
                    <p class="space-c"></p>
                </div>
                <div class="project-row-qq2 chat_box" style="overflow: unset;">
                    <div class="design-draft-header ddnos">
                        <div class="srd-row mobile">
                            <div class="srd-col">
                                <select class="srdselect select-b" id="customerwithornot">
                                    <option value="1">With Customer</option>
                                    <option value="0">Without Customer</option>
                                </select>
                            </div>
                            <div class="srd-col right">
                                <div class="right-inst">
                                    <?php if ($data[0]['status'] != "assign") { ?>
                                        <span class="vewiew-boxxt22">
                                            <a class="vwwv" href="javascript:void(0)">
                                                <span class="vewviewer-cicls">
                                                    <?php if (($edit_profile[0]['profile_picture'] != "") || (file_exists(base_url() . "uploads/profile_picture/" . $edit_profile[0]['profile_picture']))) { ?>
                                                        <img src="<?php echo base_url() ?>uploads/profile_picture/<?php echo $edit_profile[0]['profile_picture']; ?>" class="img-responsive"/>
                                                    <?php } else { ?>
                                                        <img src="<?php echo base_url() ?>uploads/profile_picture/user-admin.png" class="img-responsive"/>
                                                    <?php } ?>
                                                </span>
                                                <span class="hoversetss"><?php echo $edit_profile[0]['first_name']; ?></span>
                                            </a>
                                            <a class="vwwv" href="javascript:void(0)">
                                                <span class="vewviewer-cicls">
                                                    <?php if (($data[0]['customer_profile_picture'] != "") || (file_exists(base_url() . "uploads/profile_picture/" . $data[0]['customer_profile_picture']))) { ?>
                                                        <img src="<?php echo base_url() ?>uploads/profile_picture/<?php echo $data[0]['customer_profile_picture']; ?>" class="img-responsive"/>
                                                    <?php } else { ?>
                                                        <img src="<?php echo base_url() ?>uploads/profile_picture/user-admin.png" class="img-responsive"/>
                                                    <?php } ?>
                                                </span>
                                                <span class="hoversetss"><?php echo $data[0]['customer_name']; ?></span>
                                            </a>
                                            <a class="vwwv" href="javascript:void(0)">
                                                <span class="vewviewer-cicls">
                                                    <?php if (($data[0]['profile_picture'] != "") || (file_exists(base_url() . "uploads/profile_picture/" . $data[0]['profile_picture']))) { ?>
                                                        <img src="<?php echo base_url() ?>uploads/profile_picture/<?php echo $data[0]['profile_picture']; ?>" class="img-responsive"/>
                                                    <?php } else { ?>
                                                        <img src="<?php echo base_url() ?>uploads/profile_picture/user-admin.png" class="img-responsive"/>
                                                    <?php } ?>
                                                </span> 
                                                <span class="hoversetss"><?php echo $data[0]['designer_name']; ?></span>
                                            </a>
                                        </span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="chat-box-row">
                        <div class="read-msg-box two-can message_container_without_cus" id="message_container_without_cus" style="padding:0px; height:320px; overflow-y: scroll;width: 99%;overflow-x: hidden; display: none;">
                            <?php for ($j = 0; $j < count($chat_request_without_customer); $j++) { ?>

                                <div class="msgk-chatrow clearfix">
                                    <!-- Left Start -->

                                    <?php if ($chat_request_without_customer[$j]['sender_type'] != "qa") { ?>  
                                        <div class="msgk-user-chat msgk-left <?php echo $chat_request_without_customer[$j]['sender_type']; ?>">

                                            <!-- If Current Message and Previous Message are not Same Sender Type-->
                                            <?php if ($j == 0) { ?>
                                                <?php if ($chat_request_without_customer[$j]['sender_type'] == "customer") { ?>

                                                    <figure class="msgk-uimg"><a class="msgk-uleft" href="javascript:void(0)" title="Customer"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request_without_customer[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>

                                                <?php } elseif ($chat_request_without_customer[$j]['sender_type'] == "designer") { ?>

                                                    <figure class="msgk-uimg"><a class="msgk-uleft" href="javascript:void(0)" title="Designer"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request_without_customer[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>

                                                <?php } elseif ($chat_request_without_customer[$j]['sender_type'] == "admin") { ?>

                                                    <figure class="msgk-uimg"><a class="msgk-uleft" href="javascript:void(0)" title="Admin"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request_without_customer[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>

                                                <?php } ?>
                                            <?php } ?>
                                            <?php if ($j > 0) { ?>
                                                <?php if ($chat_request_without_customer[$j]['sender_type'] != $chat_request_without_customer[$j - 1]['sender_type']) { ?>
                                                    <?php if ($chat_request_without_customer[$j]['sender_type'] == "customer") { ?>

                                                        <figure class="msgk-uimg"><a class="msgk-uleft" href="javascript:void(0)" title="Customer"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request_without_customer[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>

                                                    <?php } elseif ($chat_request_without_customer[$j]['sender_type'] == "designer") { ?>

                                                        <figure class="msgk-uimg"><a class="msgk-uleft" href="javascript:void(0)" title="Designer"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request_without_customer[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>

                                                    <?php } elseif ($chat_request_without_customer[$j]['sender_type'] == "admin") { ?>

                                                        <figure class="msgk-uimg"><a class="msgk-uleft" href="javascript:void(0)" title="Admin"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request_without_customer[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>

                                                    <?php } ?>
                                                    <div class="msgk-mn">
                                                        <div class="msgk-umsgbox">
                                                            <span class="msgk-umsxxt"><?php echo $chat_request_without_customer[$j]['message']; ?></span>
                                                        </div>
                                                    </div>
                                                    <!-- If Current Message and Next Message are not Same Sender Type-->
                                                <?php } elseif (array_key_exists($j + 1, $chat_request_without_customer) && $chat_request_without_customer[$j]['sender_type'] != $chat_request_without_customer[$j + 1]['sender_type']) { ?>
                                                    <div class="msgk-mn last_message">
                                                        <div class="msgk-umsgbox">
                                                            <span class="msgk-umsxxt"><?php echo $chat_request_without_customer[$j]['message']; ?></span>
                                                        </div>
                                                    </div>
                                                    <p class="msgk-udate right">
                                                        <?php
                                                        $date = strtotime($chat_request_without_customer[$j]['created']); //Converted to a PHP date (a second count)
                                                        //Calculate difference
                                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
                                                        $minutes = floor($diff / 60);
                                                        $hours = floor($diff / 3600);
                                                        if ($hours == 0) {
                                                            echo $minutes . " Minutes Ago";
                                                        } else {
                                                            $days = floor($diff / ( 60 * 60 * 24 ));
                                                            if ($days == 0) {
                                                                echo $hours . " Hours Ago";
                                                            } else {
                                                                echo $days . " Days Ago";
                                                            }
                                                        }
                                                        ?>
                                                    </p>
                                                    <!-- If Current Message and Previous Message are Same Sender Type-->
                                                <?php } else { ?>
                                                    <div class="msgk-mn center_message">
                                                        <div class="msgk-umsgbox">
                                                            <span class="msgk-umsxxt"><?php echo $chat_request_without_customer[$j]['message']; ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <div class="msgk-mn">
                                                    <div class="msgk-umsgbox">
                                                        <span class="msgk-umsxxt"><?php echo $chat_request_without_customer[$j]['message']; ?></span>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>

                                    <?php } ?>
                                    <!-- Left End -->
                                    <!-- Right End -->
                                    <?php //echo "<pre>"; print_r($chat_request_without_customer);exit;  ?>
                                    <?php if ($chat_request_without_customer[$j]['sender_type'] == "qa") { ?>

                                        <div class="msgk-user-chat msgk-right <?php echo $chat_request_without_customer[$j]['sender_type']; ?>">
                                            <?php if ($j == 0) { ?>
                                                <figure class="msgk-uimg right-img"><a class="msgk-uleft" href="javascript:void(0)" title="Customer"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request_without_customer[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>
                                            <?php } ?>
                                            <?php
                                            if ($j > 0) {


                                                if (($chat_request_without_customer[$j]['sender_type'] != $chat_request_without_customer[$j - 1]['sender_type'])) {
                                                    ?>
                                                    <figure class="msgk-uimg right-img"><a class="msgk-uleft" href="javascript:void(0)" title="Customer"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request_without_customer[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>
                                                    <div class="msgk-mn">
                                                        <div class="msgk-umsgbox">
                                                            <span class="msgk-umsxxt"><?php echo $chat_request_without_customer[$j]['message']; ?></span>
                                                        </div>                                          
                                                    </div>
                                                <?php } elseif (array_key_exists($j + 1, $chat_request_without_customer) && $chat_request_without_customer[$j]['sender_type'] != $chat_request_without_customer[$j + 1]['sender_type']) { ?>
                                                    <div class="msgk-mn last_message">
                                                        <div class="msgk-umsgbox">
                                                            <span class="msgk-umsxxt"><?php echo $chat_request_without_customer[$j]['message']; ?></span>
                                                        </div>                                          
                                                    </div>
                                                    <p class="msgk-udate">
                                                        <?php
                                                        $date = strtotime($chat_request_without_customer[$j]['created']); //Converted to a PHP date (a second count)
                                                        //Calculate difference
                                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
                                                        $minutes = floor($diff / 60);
                                                        $hours = floor($diff / 3600);
                                                        if ($hours == 0) {
                                                            echo $minutes . " Minutes Ago";
                                                        } else {
                                                            $days = floor($diff / ( 60 * 60 * 24 ));
                                                            if ($days == 0) {
                                                                echo $hours . " Hours Ago";
                                                            } else {
                                                                echo $days . " Days Ago";
                                                            }
                                                        }
                                                        ?>
                                                    </p>
                                                <?php } else { ?>
                                                    <div class="msgk-mn center_message">
                                                        <div class="msgk-umsgbox">
                                                            <span class="msgk-umsxxt"><?php echo $chat_request_without_customer[$j]['message']; ?></span>
                                                        </div>                                          
                                                    </div>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <div class="msgk-mn">
                                                    <div class="msgk-umsgbox">
                                                        <span class="msgk-umsxxt"><?php echo $chat_request_without_customer[$j]['message']; ?></span>
                                                    </div>                                          
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <?php //if($j != 0){ ?>
                                        <?php //}  ?>
                                    <?php } ?>   
                                    <!-- Right End -->
                                </div>
                            <?php } ?>
                        </div>
                        <div class="read-msg-box two-can" id="message_container" style="padding:0px; height:320px; overflow-y: scroll;width: 99%;overflow-x: hidden;">
                            <?php for ($j = 0; $j < count($chat_request); $j++) { ?>

                                <div class="msgk-chatrow clearfix">
                                    <!-- Left Start -->

                                    <?php if ($chat_request[$j]['sender_type'] != "qa") { ?>  
                                        <div class="msgk-user-chat msgk-left <?php echo $chat_request[$j]['sender_type']; ?>">

                                            <!-- If Current Message and Previous Message are not Same Sender Type-->
                                            <?php if ($j == 0) { ?>
                                                <?php if ($chat_request[$j]['sender_type'] == "customer") { ?>

                                                    <figure class="msgk-uimg"><a class="msgk-uleft" href="javascript:void(0)" title="Customer"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>

                                                <?php } elseif ($chat_request[$j]['sender_type'] == "designer") { ?>

                                                    <figure class="msgk-uimg"><a class="msgk-uleft" href="javascript:void(0)" title="Designer"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>

                                                <?php } elseif ($chat_request[$j]['sender_type'] == "admin") { ?>

                                                    <figure class="msgk-uimg"><a class="msgk-uleft" href="javascript:void(0)" title="Admin"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>

                                                <?php } ?>
                                            <?php } ?>
                                            <?php if ($j > 0) { ?>
                                                <?php if ($chat_request[$j]['sender_type'] != $chat_request[$j - 1]['sender_type']) { ?>
                                                    <?php if ($chat_request[$j]['sender_type'] == "customer") { ?>

                                                        <figure class="msgk-uimg"><a class="msgk-uleft" href="javascript:void(0)" title="Customer"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>

                                                    <?php } elseif ($chat_request[$j]['sender_type'] == "designer") { ?>

                                                        <figure class="msgk-uimg"><a class="msgk-uleft" href="javascript:void(0)" title="Designer"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>

                                                    <?php } elseif ($chat_request[$j]['sender_type'] == "admin") { ?>

                                                        <figure class="msgk-uimg"><a class="msgk-uleft" href="javascript:void(0)" title="Admin"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>

                                                    <?php } ?>
                                                    <div class="msgk-mn">
                                                        <div class="msgk-umsgbox">
                                                            <span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span>
                                                        </div>
                                                    </div>
                                                    <!-- If Current Message and Next Message are not Same Sender Type-->
                                                <?php } elseif (array_key_exists($j + 1, $chat_request) && $chat_request[$j]['sender_type'] != $chat_request[$j + 1]['sender_type']) { ?>
                                                    <div class="msgk-mn last_message">
                                                        <div class="msgk-umsgbox">
                                                            <span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span>
                                                        </div>
                                                    </div>
                                                    <p class="msgk-udate right">
                                                        <?php
                                                        $date = strtotime($chat_request[$j]['created']); //Converted to a PHP date (a second count)
                                                        //Calculate difference
                                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
                                                        $minutes = floor($diff / 60);
                                                        $hours = floor($diff / 3600);
                                                        if ($hours == 0) {
                                                            echo $minutes . " Minutes Ago";
                                                        } else {
                                                            $days = floor($diff / ( 60 * 60 * 24 ));
                                                            if ($days == 0) {
                                                                echo $hours . " Hours Ago";
                                                            } else {
                                                                echo $days . " Days Ago";
                                                            }
                                                        }
                                                        ?>
                                                    </p>
                                                    <!-- If Current Message and Previous Message are Same Sender Type-->
                                                <?php } else { ?>
                                                    <div class="msgk-mn center_message">
                                                        <div class="msgk-umsgbox">
                                                            <span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <div class="msgk-mn">
                                                    <div class="msgk-umsgbox">
                                                        <span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>

                                    <?php } ?>
                                    <!-- Left End -->
                                    <!-- Right End -->
                                    <?php //echo "<pre>"; print_r($chat_request);exit;  ?>
                                    <?php if ($chat_request[$j]['sender_type'] == "qa") { ?>

                                        <div class="msgk-user-chat msgk-right <?php echo $chat_request[$j]['sender_type']; ?>">
                                            <?php if ($j == 0) { ?>
                                                <figure class="msgk-uimg right-img"><a class="msgk-uleft" href="javascript:void(0)" title="Customer"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>
                                            <?php } ?>
                                            <?php
                                            if ($j > 0) {


                                                if (($chat_request[$j]['sender_type'] != $chat_request[$j - 1]['sender_type'])) {
                                                    ?>
                                                    <figure class="msgk-uimg right-img"><a class="msgk-uleft" href="javascript:void(0)" title="Customer"><img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_request[$j]['profile_picture']; ?>" class="img-responsive"></a></figure>
                                                    <div class="msgk-mn">
                                                        <div class="msgk-umsgbox">
                                                            <span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span>
                                                        </div>                                          
                                                    </div>
                                                <?php } elseif (array_key_exists($j + 1, $chat_request) && $chat_request[$j]['sender_type'] != $chat_request[$j + 1]['sender_type']) { ?>
                                                    <div class="msgk-mn last_message">
                                                        <div class="msgk-umsgbox">
                                                            <span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span>
                                                        </div>                                          
                                                    </div>
                                                    <p class="msgk-udate">
                                                        <?php
                                                        $date = strtotime($chat_request[$j]['created']); //Converted to a PHP date (a second count)
                                                        //Calculate difference
                                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date; //time returns current time in seconds
                                                        $minutes = floor($diff / 60);
                                                        $hours = floor($diff / 3600);
                                                        if ($hours == 0) {
                                                            echo $minutes . " Minutes Ago";
                                                        } else {
                                                            $days = floor($diff / ( 60 * 60 * 24 ));
                                                            if ($days == 0) {
                                                                echo $hours . " Hours Ago";
                                                            } else {
                                                                echo $days . " Days Ago";
                                                            }
                                                        }
                                                        ?>
                                                    </p>
                                                <?php } else { ?>
                                                    <div class="msgk-mn center_message">
                                                        <div class="msgk-umsgbox">
                                                            <span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span>
                                                        </div>                                          
                                                    </div>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                                <div class="msgk-mn">
                                                    <div class="msgk-umsgbox">
                                                        <span class="msgk-umsxxt"><?php echo $chat_request[$j]['message']; ?></span>
                                                    </div>                                          
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <?php //if($j != 0){   ?>
                                        <?php //}  ?>
                                    <?php } ?>   
                                    <!-- Right End -->
                                </div>
                            <?php } ?>
                        </div>

                        <div class="cmmtype-box">
                            <div class="cmmtype-row">
                                <span class="cmmfiles"><img src="<?php echo base_url(); ?>theme/assets/qa/images/icon-attech.png" class="img-responsive"></span>
                                <input <?php
                                if ($data[0]['status'] == "assign") {
                                    echo "disabled";
                                }
                                ?> type="text" class="pstcmm t_w text_<?php echo $data[0]['id']; ?>" Placeholder="Type a message...">
                                <span class="cmmsend">
                                    <button <?php
                                    if ($data[0]['status'] == "assign") {
                                        echo "disabled";
                                    }
                                    ?> class="cmmsendbtn send_request_chat"
                                        data-designerid="<?php echo $data[0]['designer_id']; ?>"
                                        data-requestid="<?php echo $data[0]['id']; ?>"  
                                        data-senderrole="qa" 
                                        data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                                        data-receiverid="<?php echo $data[0]['customer_id']; ?>" 
                                        data-receiverrole="customer"
                                        data-sendername="<?php echo $data[0]['designer_name']; ?>"
                                        data-customerwithornot="1" 
                                        data-profilepic="<?php echo base_url() ?>uploads/profile_picture/<?php echo $edit_profile[0]['profile_picture']; ?>">
                                        <img src="<?php echo base_url(); ?>theme/assets/qa/images/icon-chat-send.png" class="img-responsive">
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p class="space-d"></p>

        <div class="project-row-qq1">
            <div class="design-draft-header-02 ddnos">
                <div class="row flex-show flex-show-em">
                    <div class="col-xs-12">
                        <div class="descol">
                            <h3 class="head-b">Project Information</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="designdraft-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="designdraft-views">
                            <div class="form-group goup-x1">
                                <label class="label-x2"><span class="prowest">1</span> Project Title</label>
                                <p class="space-a"></p>
                                <div class="review-textcolww"><?php echo $data[0]['title']; ?></div>
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x2"><span class="prowest">2</span> Target Market</label>
                                <p class="space-a"></p>
                                <div class="review-textcolww"><?php echo $data[0]['business_industry']; ?></div>
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x2"><span class="prowest">3</span> Color plates</label>
                                <div class="form-group">
                                    <?php
                                    if ($data[0]['design_colors']) {
                                        $color = explode(",", $data[0]['design_colors']);
                                    }
                                    ?>
                                    <p class="space-a"></p>
                                    <?php if ($data[0]['design_colors'] && in_array("Let Designer Choose", $color)) { ?>
                                        <div class="radio-check-kk1">
                                            <label> 
                                                <div class="review-rowww">
                                                    <span class="review-colww left">
                                                        <span class="review-textcolww">
                                                            Let Designer Choose for me.</span>
                                                    </span>

                                                    <span class="review-colww">
                                                        <?php if ($data[0]['status'] != "approved"): ?>
                                                            <a class="review-circleww" href="<?php echo base_url(); ?>customer/request/new_request_brief/<?php echo $data[0]['id']; ?>?rep=1"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-edit.png" class="img-responsive">
                                                            </a>
                                                        <?php endif ?>

                                                    </span>
                                                </div>
                                            </label>
                                        </div>
                                        <?php
                                    }
                                    else {
                                        $flag = 0;
                                        ?>

                                        <p class="space-a"></p>

                                        <div class="lessrows">
                                            <div class="right-lesscol noflexes">
                                                <div class="plan-boxex-xx6 clearfix">
                                                    <?php
                                                    if ($data[0]['design_colors'] && in_array("blue", $color)) {
                                                        $flag = 1;
                                                        ?>
                                                        <label for="id1" class="radio-box-xx2"> 
                                                            <div class="check-main-xx3">
                                                                <figure class="chkimg">
                                                                    <img src="<?php echo base_url() ?>theme/customer-assets/images/col-1.png" class="img-responsive">
                                                                </figure>                                       
                                                                <h3 class="sub-head-c text-center">Blue
                                                                </h3>
                                                            </div>
                                                        </label>
                                                    <?php } ?>
                                                    <?php
                                                    if ($data[0]['design_colors'] && in_array("aqua", $color)) {
                                                        $flag = 1;
                                                        ?>
                                                        <label for="id2" class="radio-box-xx2"> 
                                                            <span class="checkmark"></span>
                                                            <div class="check-main-xx3">
                                                                <figure class="chkimg">
                                                                    <img src="<?php echo base_url() ?>theme/customer-assets/images/col-2.png" class="img-responsive">
                                                                </figure>                                       
                                                                <h3 class="sub-head-c text-center">Auqa</h3>
                                                            </div>
                                                        </label>
                                                    <?php } ?>
                                                    <?php
                                                    if ($data[0]['design_colors'] && in_array("green", $color)) {
                                                        $flag = 1;
                                                        ?>
                                                        <label for="id3" class="radio-box-xx2"> 
                                                            <div class="check-main-xx3">
                                                                <figure class="chkimg">
                                                                    <img src="<?php echo base_url() ?>theme/customer-assets/images/col-3.png" class="img-responsive">
                                                                </figure>                                       
                                                                <h3 class="sub-head-c text-center">Greens</h3>
                                                            </div>
                                                        </label>
                                                    <?php } ?>
                                                    <?php
                                                    if ($data[0]['design_colors'] && in_array("purple", $color)) {
                                                        $flag = 1;
                                                        ?>
                                                        <label for="id4" class="radio-box-xx2"> 
                                                            <div class="check-main-xx3">
                                                                <figure class="chkimg">
                                                                    <img src="<?php echo base_url() ?>theme/customer-assets/images/col-4.png" class="img-responsive">
                                                                </figure>                                       
                                                                <h3 class="sub-head-c text-center">Purple</h3>
                                                            </div>
                                                        </label>
                                                    <?php } ?>
                                                    <?php
                                                    if ($data[0]['design_colors'] && in_array("pink", $color)) {
                                                        $flag = 1;
                                                        ?>
                                                        <label for="id5" class="radio-box-xx2"> 
                                                            <div class="check-main-xx3" onclick="funcheck()">
                                                                <figure class="chkimg">
                                                                    <img src="<?php echo base_url() ?>theme/customer-assets/images/col-5.png" class="img-responsive">
                                                                </figure>                                       
                                                                <h3 class="sub-head-c text-center">Pink</h3>
                                                            </div>
                                                        </label>
                                                    <?php } ?>

                                                </div>  
                                            </div>
                                        </div>

                                        <p class="space-a"></p>

                                    <?php }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x2"><span class="prowest">4</span> Design Software Requirement</label>
                                <p class="space-a"></p>
                                <div class="review-textcolww"><?php echo $data[0]['designer']; ?></div>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="designdraft-views">
                            <div class="form-group goup-x1">
                                <label class="label-x2"><span class="prowest">5</span> Deliverables</label>
                                <p class="space-a"></p>
                                <div class="review-textcolww"><?php echo $data[0]['deliverables']; ?></div>
                            </div>

                            <div class="form-group">
                                <label class="label-x2"><span class="prowest">6</span> Description</label>
                                <p class="space-a"></p>
                                <p class="text-d"><?php
                                    if (strlen($data[0]['description']) > 200) {
                                        echo '<span class="read_more">' . substr($data[0]['description'], 0, 220) . '...<a class="read_more read_more_btn" id="readmore" style="color:#e73250;">[See More]</a></span>';
                                        echo "<span class='more_text read_more' style='display:none;'>" . $data[0]['description'] . "</span><a class='read_more_btn read_more' style='color:#000;'></a>";
                                    } else {
                                        echo "<span class='more_text'>" . $data[0]['description'] . "</span>";
                                    }
                                    ?>  </p>
                            </div>
                            <div class="light-box" id="imgrmv">
                                <ul class="list-unstyled list-accessimg">
                                    <?php for ($i = 0; $i < count($data[0]['customer_attachment']); $i++) { ?>

                                        <?php
                                        $type = substr($data[0]['customer_attachment'][$i]['file_name'], strrpos($data[0]['customer_attachment'][$i]['file_name'], '.') + 1);
                                        if ($type == "pdf") {
                                            ?>
                                            <li>
                                                <div class="accimgbx33">
                                                    <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                        <img src="<?php echo base_url(); ?>public/default-img/pdf.png" height="150"/>
                                                    </a>
                                                </div>
                                            </li>
                                        <?php } elseif ($type == "doc" || $type == "docx") { ?>
                                            <li><div class="accimgbx33">
                                                    <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                        <img src="<?php echo base_url(); ?>public/default-img/docx.png" height="150"/>
                                                    </a>
                                                </div>
                                            </li>
                                        <?php } else { ?>
                                            <li><div class="accimgbx33">
                                                    <a href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" data-fancybox="images">
                                                        <img src="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" />
                                                    </a>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>

                    </div>

                </div>
            </div>  
        </div>
    </div>

</section>

<!-- Modal -->
<div class="modal fade" id="Addfiles" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c">Add File</h3>
                    </header>
                    <div class="selectimagespopup">
                        <form method="post" action="" enctype="multipart/form-data">
                            <div class="form-group goup-x1 goup_1">
                                <label class="label-x3">Source File:</label>
                                <p class="space-a"></p>
                                <div class="file-drop-area">
                                    <span class="fake-img"><img src="<?php echo base_url(); ?>theme/assets/img/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                    <input type="hidden" name="tab_status" value="<?php echo $matches[0]; ?>"/>
                                    <input class="file-input" type="file" name="src_file" onchange="validateAndUploadS(this);">
                                    <div class="dropify-preview" style="display: none; width: 100%;">
                                        <img src="" class="img_dropify" style="height: 150px; width: 100%;">
                                        <div class="overlay">
                                            <div class="rem_btn">
                                                <button class="remove_selected">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group goup-x1 goup_2">
                                <label class="label-x3">Preview File:</label>
                                <p class="space-a"></p>
                                <div class="file-drop-area">
                                    <span class="fake-img"><img src="<?php echo base_url(); ?>theme/assets/img/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                    <input class="file-input" type="file" name="preview_file"  onchange="validateAndUploadP(this);" accept="image/*">
                                    <div class="dropify-preview" style="display: none; width: 100%;">
                                        <img src="" class="img_dropify" style="height: 150px; width: 100%;">
                                        <div class="overlay">
                                            <div class="rem_btn">
                                                <button class="remove_selected">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p class="btn-x"><input type="submit" value="Submit" class="btn-g" style="border-radius: 7px;margin: auto;display: block;margin-top: 10px;"/></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="AddDesigner" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="cli-ent-model-box">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c">Add Designer</h3>
                    </header>
                    <div class="noti-listpopup">
                        <form action="" method="post"> 
                            <ul class="list-unstyled list-notificate">
                                <?php foreach ($alldesigners as $value): ?>
                                    <li>
                                        <a href="#">
                                            <div class="radio-boxxes">
                                                <label class="containerones">
                                                    <input type="radio" value="<?php echo $value['id']; ?>" name="assign_designer" id="<?php echo $value['id']; ?>">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <input type="hidden" name="tab_status" value="<?php echo $matches[0]; ?>"/>
                                            <div class="setnoti-fication">
                                                <figure class="pro-circle-k1">
                                                    <?php if ($value['profile_picture'] != "") { ?>
                                                        <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $value['profile_picture'] ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo base_url(); ?>uploads/profile_picture/user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure>

                                                <div class="notifitext">
                                                    <p class="ntifittext-z1"><strong><?php echo $value['first_name'] . " " . $value['last_name']; ?></strong></p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                <?php endforeach; ?>  
                            </ul>
                            <input type="hidden" name="request_id" id="request_id">
                            <p class="space-c"></p>
                            <p class="btn-x text-center"><button name="adddesingerbtn" type="submit" class="btn-g minxx1">Select Designer</button></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<button style="display: none;" id="confirmation" data-toggle="modal" data-target="#myModal">click here</button>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" name="request_id_status" id="request_id_status" value=""/>
            <input type="hidden" name="valuestatus" id="valuestatus" value=""/>
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h3 class="head-c text-center msgal">Are you sure you want to change the status to </h3>
                    </header>
                    <div class="confirmation_btn text-center">
                        <button class="btn btn-y" data-dismiss="modal" aria-label="Close">Yes</button>
                        <button class="btn btn-n" data-dismiss="modal" aria-label="Close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<script src="<?php echo base_url() ?>theme/customer-assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>theme/assets/global/plugins/dropify/dist/js/dropify.min.js"></script>
<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="<?php echo base_url() ?>public/js/jquery.fancybox.min.js"></script>

<script type="text/javascript">
                                        $('[data-fancybox="images"]').fancybox({
                                            buttons: [
                                                'download',
                                                'thumbs',
                                                'close'
                                            ]
                                        });
                                        function statuschange(request_id, value) {
                                            $('#confirmation').click();
                                            $('#request_id_status').val(request_id);
                                            $('#valuestatus').val(value);
                                            $('.msgal').append("<strong>" + value + "</strong>");
                                        }
                                        $('.btn-y').click(function () {
                                            var request_id = $('#request_id_status').val();
                                            var valueSelected = $('#valuestatus').val();
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo base_url(); ?>qa/dashboard/change_project_status",
                                                dataType: "json",
                                                data: {
                                                    'request_id': request_id,
                                                    'value': valueSelected,
                                                },
                                                success: function (response) {
                                                    if (response == '1') {
                                                        $('#statuspass').css('display', 'block');
                                                        setTimeout(function () {
                                                            $('#statuspass').fadeOut();
                                                            location.reload();
                                                        }, 2000);
                                                    } else {
                                                        $('#statusfail').css('display', 'block');
                                                        setTimeout(function () {
                                                            $('#statusfail').fadeOut();
                                                            location.reload();
                                                        }, 2000);
                                                    }
                                                }
                                            });

                                        });
                                        $(document).ready(function () {
                                            $('#lightgallery').lightGallery({
                                                thumbnail: false,
                                                zoom: false,
                                                mousewheel: false,
                                                fullScreen: false,
                                                width: "70%"
                                            });
                                        });
                                        $('.t_w').keypress(function (e) {
                                            if (e.which == 13) {
                                                $('.send_request_chat').click();
                                                return false;
                                            }
                                        });
                                        $('#message_container .msgk-chatrow.clearfix').each(function (e) {
                                            if ($('figure', this).length > 0) {
                                                $(this).addClass('margin_top');
                                            }
                                        });
                                        $('.send_request_chat').click(function () {
                                            var designer_id = $(this).attr("data-designerid");
                                            var request_id = $(this).attr("data-requestid");
                                            var sender_type = $(this).attr("data-senderrole");
                                            var sender_id = $(this).attr("data-senderid");
                                            var reciever_id = $(this).attr("data-receiverid");
                                            var reciever_type = $(this).attr("data-receiverrole");
                                            var text_id = 'text_' + request_id;
                                            var message = $('.' + text_id).val();
                                            var verified_by_admin = "1";
                                            var customer_name = $(this).attr("data-sendername");
                                            var customer_withornot = $(this).attr("data-customerwithornot");
                                            var profilepic = $(this).attr("data-profilepic");
                                            if (message != "") {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>qa/dashboard/send_message_request",
                                                    data: {"request_id": request_id,
                                                        "designer_id": designer_id,
                                                        "sender_type": sender_type,
                                                        "sender_id": sender_id,
                                                        "reciever_id": reciever_id,
                                                        "reciever_type": reciever_type,
                                                        "message": message,
                                                        "with_or_not_customer": customer_withornot,
                                                        "qa_seen": "1",
                                                    },
                                                    success: function (data) {
                                                        //alert("---"+data);
                                                        //alert("Settings has been updated successfully.");
                                                        $('.text_' + request_id).val("");
                                                        if (customer_withornot == 0) {
                                                            if ($('#message_container_without_cus > div.msgk-chatrow:last-child > div').hasClass("msgk-right") === true) {
                                                                $('#message_container_without_cus').append('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + '"> <div class="msgk-mn"><div class="msgk-umsgbox"><span class="msgk-umsxxt">' + message + '</span></div></div><p class="msgk-udate">Just Now</p></div></div>');
                                                            } else {
                                                                $('#message_container_without_cus').append('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + '"> <figure class="msgk-uimg right-img"><a class="msgk-uleft" href="javascript:void(0)" title="Customer"><img src="' + profilepic + '" class="img-responsive"></a></figure><div class="msgk-mn"><div class="msgk-umsgbox"><span class="msgk-umsxxt">' + message + '</span></div></div><p class="msgk-udate">Just Now</p></div></div>');
                                                            }
                                                            $('#message_container_without_cus').stop().animate({
                                                                scrollTop: $('#message_container_without_cus')[0].scrollHeight
                                                            }, 1000);
                                                        } else if (customer_withornot == 1) {
                                                            if ($('#message_container > div.msgk-chatrow:last-child > div').hasClass("msgk-right") === true) {
                                                                $('#message_container').append('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + '"> <div class="msgk-mn"><div class="msgk-umsgbox"><span class="msgk-umsxxt">' + message + '</span></div></div><p class="msgk-udate">Just Now</p></div></div>');
                                                            } else {
                                                                $('#message_container').append('<div class="msgk-chatrow clearfix"><div class="msgk-user-chat msgk-right ' + sender_type + '"> <figure class="msgk-uimg right-img"><a class="msgk-uleft" href="javascript:void(0)" title="Customer"><img src="' + profilepic + '" class="img-responsive"></a></figure><div class="msgk-mn"><div class="msgk-umsgbox"><span class="msgk-umsxxt">' + message + '</span></div></div><p class="msgk-udate">Just Now</p></div></div>');
                                                            }
                                                            $('#message_container').stop().animate({
                                                                scrollTop: $('#message_container')[0].scrollHeight
                                                            }, 1000);
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                        // Add designer section through modal

                                        $('.adddesinger').click(function () {
                                            var request_id = $(this).attr('data-requestid');
                                            var designer_id = $(this).attr('data-designerid');
                                            $('#AddDesigner #request_id').val(request_id);
                                            $('#AddDesigner input#' + designer_id).click();
                                        });

                                        // Add designer section through modal End



                                        $(document).on('click', ".remove_selected", function (e) {
                                            e.preventDefault();
                                            $(this).closest('.dropify-preview').css('display', 'none');
                                            $(this).closest('.file-drop-area').find('span').show();
                                        });
                                        function validateAndUploadS(input) {
                                            var URL = window.URL || window.webkitURL;
                                            var file = input.files[0];
                                            var exten = $(input).val().split('.').pop();
                                            var imgext = ['jpg', 'jpeg', 'png', 'gif'];
                                            //alert(exten); 
                                            if (exten == "zip" || exten == "rar") {
                                                $(".goup_1 .file-drop-area .dropify-preview").html('<i class="fa fa-file-archive-o" aria-hidden="true"></i><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                                                $('.goup_1 .file-drop-area .dropify-preview').show();
                                                $('.goup_1 .file-drop-area span').hide();
                                            } else if (exten == "docx" || exten == "doc") {
                                                $(".goup_1 .file-drop-area .dropify-preview").html('<i class="fa fa-file-word-o" aria-hidden="true"></i><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                                                $('.goup_1 .file-drop-area .dropify-preview').show();
                                                $('.goup_1 .file-drop-area span').hide();
                                            } else if (exten == "pdf") {
                                                $(".goup_1 .file-drop-area .dropify-preview").html('<i class="fa fa-file-pdf-o" aria-hidden="true"></i><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                                                $('.goup_1 .file-drop-area .dropify-preview').show();
                                                $('.goup_1 .file-drop-area span').hide();
                                            } else if (exten == "txt") {
                                                $(".goup_1 .file-drop-area .dropify-preview").html('<i class="fa fa-file-text-o" aria-hidden="true"></i><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                                                $('.goup_1 .file-drop-area .dropify-preview').show();
                                                $('.goup_1 .file-drop-area span').hide();
                                            }
                                            else if (exten == "psd") {
                                                $(".goup_1 .file-drop-area .dropify-preview").html('<i class="fa fa-file-powerpoint-o" aria-hidden="true"></i><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                                                $('.goup_1 .file-drop-area .dropify-preview').show();
                                                $('.goup_1 .file-drop-area span').hide();
                                            }
                                            else if (exten == "ai") {
                                                $(".goup_1 .file-drop-area .dropify-preview").html('<i class="fa fa-file-o" aria-hidden="true"></i><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                                                $('.goup_1 .file-drop-area .dropify-preview').show();
                                                $('.goup_1 .file-drop-area span').hide();
                                            } else if (jQuery.inArray(exten, imgext) != '-1') {
                                                console.log(URL.createObjectURL(file));
                                                $(".goup_1 .file-drop-area .dropify-preview").html('<img src="' + URL.createObjectURL(file) + '" class="img_dropify" style="height: 150px; width: 100%;"><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                                                $('.goup_1 .file-drop-area .dropify-preview').show();
                                                $('.goup_1 .file-drop-area span').hide();
                                            } else {
                                                $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg">You have selected <strong>' + exten + '</strong> file</h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
                                                $('.goup_1 .file-drop-area .dropify-preview').show();
                                                $('.goup_1 .file-drop-area span').hide();
                                            }
                                        }
                                        function validateAndUploadP(input) {
                                            var URL = window.URL || window.webkitURL;
                                            var file = input.files[0];
                                            if (file) {
                                                console.log(URL.createObjectURL(file));
                                                $(".goup_2 .file-drop-area span").hide();
                                                $(".goup_2 .file-drop-area .dropify-preview").css('display', 'block');
                                                $(".goup_2 .img_dropify").attr('src', URL.createObjectURL(file));
                                            }
                                        }
                                        $(document).ready(function () {
                                            $(".more_less_Click").click(function () {
                                                $(".more_additional").toggle();
                                            });
                                            $(".read_more_btn").click(function () {
                                                $(".read_more").toggle();
                                            });



                                            $(".open_attachment_open").click(function () {
                                                var url = $(this).attr("data-url");
                                                $(".modal_img").attr("src", url);
                                                $(".modal_download").attr("href", url);
                                            });
                                        });
</script>
<?php
if (!empty($chat_request)) {
    $prjtid = $chat_request[0]['request_id'];
    $customerid = $chat_request[0]['reciever_id'];
    $designerid = $chat_request[0]['sender_id'];
}
?>
<script>
    jQuery(document).ready(function ($) {
        setInterval(function () {
            var project_id = "<?php echo $prjtid; ?>";
            var cst_id = "<?php echo $customerid; ?>";
            var desg_id = "<?php echo $designerid; ?>";
            var qaseen = "0";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>qa/dashboard/load_messages_from_all_user",
                dataType: "json",
                data: {
                    'project_id': project_id,
                    'cst_id': cst_id,
                    'desg_id': desg_id,
                    'qa_seen': "0"
                },
                success: function (response) {
                    if (response) {
                        console.log(response);
                        console.log(response.profile_picture);
                        if (response.with_or_not == 0) {
                            if ($('#message_container_without_cus > div.msgk-chatrow:last-child > div').hasClass(response.typee) === true) {
                                $('#message_container_without_cus').append("<div class='msgk-chatrow clearfix'><div class='msgk-user-chat msgk-left " + response.typee + "'><div class='msgk-mn'><div class='msgk-umsgbox'><span class='msgk-umsxxt'>" + response.message + "</span></div></div><p class='msgk-udate right'>New Message Arrived</p></div></div>");
                            } else {
                                $('#message_container_without_cus').append("<div class='msgk-chatrow clearfix'><div class='msgk-user-chat msgk-left " + response.typee + "'><figure class='msgk-uimg'><a class='msgk-uleft' href='javascript:void(0)'><img src='<?php echo base_url(); ?>uploads/profile_picture/" + response.profile_picture + "' class='img-responsive'></a></figure><div class='msgk-mn'><div class='msgk-umsgbox'><span class='msgk-umsxxt'>" + response.message + "</span></div></div><p class='msgk-udate right'>New Message Arrived</p></div></div>");
                            }
                            $('#message_container_without_cus').stop().animate({
                                scrollTop: $('#message_container_without_cus')[0].scrollHeight
                            }, 1000);
                        } else if (response.with_or_not == 1) {
                            if ($('#message_container > div.msgk-chatrow:last-child > div').hasClass(response.typee) === true) {
                                $('#message_container').append("<div class='msgk-chatrow clearfix'><div class='msgk-user-chat msgk-left " + response.typee + "'><div class='msgk-mn'><div class='msgk-umsgbox'><span class='msgk-umsxxt'>" + response.message + "</span></div></div><p class='msgk-udate right'>New Message Arrived</p></div></div>");
                            } else {
                                $('#message_container').append("<div class='msgk-chatrow clearfix'><div class='msgk-user-chat msgk-left " + response.typee + "'><figure class='msgk-uimg'><a class='msgk-uleft' href='javascript:void(0)'><img src='<?php echo base_url(); ?>uploads/profile_picture/" + response.profile_picture + "' class='img-responsive'></a></figure><div class='msgk-mn'><div class='msgk-umsgbox'><span class='msgk-umsxxt'>" + response.message + "</span></div></div><p class='msgk-udate right'>New Message Arrived</p></div></div>");
                            }
                            $('#message_container').stop().animate({
                                scrollTop: $('#message_container')[0].scrollHeight
                            }, 1000);
                        }
                    }
                    else {
                        //console.log('I am not in');
                    }
                    //console.log(data);
                },
                error: function (error) {
                    console.log("error");
                }
            });

        }, 2000);
        $('#message_container').stop().animate({
            scrollTop: $('#message_container')[0].scrollHeight
        }, 1);
    });
    $("#customerwithornot").on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        $(".send_request_chat").attr('data-customerwithornot', valueSelected);
        if (valueSelected == 0)
        {
            $('#message_container_without_cus').show();
            $('#message_container').hide();
            $('#message_container_without_cus').stop().animate({
                scrollTop: $('#message_container_without_cus')[0].scrollHeight
            }, 1);
        } else if (valueSelected == 1) {
            $('#message_container_without_cus').hide();
            $('#message_container').show();
            $('#message_container').stop().animate({
                scrollTop: $('#message_container')[0].scrollHeight
            }, 1);
        }
    });

</script>