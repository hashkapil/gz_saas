<?php
if (isset($_GET['rep']) && $_GET['rep'] == 1) {
    $stat = "Project Update";
} else {
    $stat = 'Create Project';
}
?>	
<section class="con-b">
    <div class="container">
        <p class="space-d"></p>
        <?php if ($this->session->flashdata('message_error') != '') { ?>
            <div id="message" class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_error'); ?>
                </p>
            </div>
        <?php } ?>

        <?php if ($this->session->flashdata('message_success') != '') { ?>
            <div id="message" class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_success'); ?>
                </p>
            </div>
        <?php }
        ?>

        <form method="post" action="<?php echo base_url(); ?>customer/request/new_request_submit/<?php echo $data[0]['id']; ?>">
            <div class="flex-show uplink-row clearfix">
                <span class="uplink-col left"><h3 class="sub-head-d"><?php echo $stat; ?></h3></span>
                <span class="uplink-col right">
                    <ul class="list-unstyled list-bread-crumb">
                        <li><a href="">Dashboard</a></li>
                        <li><?php echo $stat; ?></li>
                    </ul>
                </span>
            </div>

            <div class="header-bxx2">
                <ul class="list-unstyled list-tab-xx5">
                    <li><a href="">Category</a></li>			
                    <li><a href="<?php echo base_url(); ?>customer/request/new_request/brief/<?php echo $data[0]['id']; ?>">Brief</a></li> 			
                    <li class="acitve"><a href="<?php echo base_url(); ?>customer/request/new_request/review">Review</a></li>        
                </ul>
            </div>

            <p class="space-d"></p>

            <div class="cate-main-boxx3">
                <div class="row">
                    <!-- <div class="col-md-4">
                            <div class="cat-box-xx122">
                                    <h3 class="sub-head-a">Design Brief</h3>
                                    <p class="space-c"></p>
                                    <div class="text-b para">
                                            <p>vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                                            <p>vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas </p>
                                            <p>molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                                    </div>
                            </div>
                    </div> -->

                    <div class="col-md-10 col-md-offset-1">
                        <div class="cat-form-xx123">
                            <div class="form-group goup-x1">
                                <label class="label-x2">Project Title<span class="maditory">*</span></label>
                                <div class="review-rowww">
                                    <span class="review-colww left">
                                        <span class="review-textcolww"><?php echo $data[0]['title']; ?></span>
                                    </span>
                                    <span class="review-colww">
                                        <a class="review-circleww" href="<?php echo base_url(); ?>customer/request/new_request/brief/<?php echo $data[0]['id']; ?>"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-edit.png" class="img-responsive"></a>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group goup-x1">
                                <label class="label-x2">Target Market<span class="maditory">*</span></label>
                                <div class="review-rowww">
                                    <span class="review-colww left">
                                        <span class="review-textcolww"><?php echo $data[0]['business_industry']; ?></span>
                                    </span>
                                    <span class="review-colww">
                                        <a class="review-circleww" href="<?php echo base_url(); ?>customer/request/new_request/brief/<?php echo $data[0]['id']; ?>"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-edit.png" class="img-responsive"></a>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x2">Color Pallete<span class="maditory">*</span></label>
                                <p class="space-b"></p>

                                <!-- Color Plates -->
                                <div class="form-group">
                                    <?php
                                    if ($data[0]['design_colors']) {
                                        $color = explode(",", $data[0]['design_colors']);
                                    }
                                    ?>
                                    <p class="space-a"></p>
                                    <?php if ($data[0]['design_colors'] && in_array("Let Designer Choose", $color)) { ?>
                                        <div class="radio-check-kk1">
                                            <label> 
                                                <div class="review-rowww">
                                                    <span class="review-colww left">
                                                        <span class="review-textcolww">
                                                            Let Designer Choose for me.</span>
                                                    </span>

                                                    <span class="review-colww">
                                                        <a class="review-circleww" href="<?php echo base_url(); ?>customer/request/new_request/brief/<?php echo $data[0]['id']; ?>"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-edit.png" class="img-responsive"/></a>
                                                    </span>
                                                </div>
                                            </label>
                                        </div>
                                        <?php
                                    } else {
                                        $flag = 0;
                                        ?>

                                        <p class="space-a"></p>

                                        <div class="lessrows">
                                            <div class="right-lesscol noflexes">
                                                <div class="plan-boxex-xx6 clearfix">
                                                    <?php
                                                    if ($data[0]['design_colors'] && in_array("blue", $color)) {
                                                        $flag = 1;
                                                        ?>
                                                        <label for="id1" class="radio-box-xx2"> 
                                                            <div class="check-main-xx3">
                                                                <figure class="chkimg">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-1.png" class="img-responsive">
                                                                </figure>										
                                                                <h3 class="sub-head-c text-center">Blue
                                                                    <!-- <label for="colorbest">	Blues </label>								 -->
                                                                </h3>
                                                            </div>
                                                        </label>
                                                    <?php } ?>
                                                    <?php
                                                    if ($data[0]['design_colors'] && in_array("aqua", $color)) {
                                                        $flag = 1;
                                                        ?>
                                                        <label for="id2" class="radio-box-xx2"> 
                                                            <span class="checkmark"></span>
                                                            <div class="check-main-xx3">
                                                                <figure class="chkimg">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-2.png" class="img-responsive">
                                                                </figure>										
                                                                <h3 class="sub-head-c text-center">Auqa</h3>
                                                            </div>
                                                        </label>
                                                    <?php } ?>
                                                    <?php
                                                    if ($data[0]['design_colors'] && in_array("green", $color)) {
                                                        $flag = 1;
                                                        ?>
                                                        <label for="id3" class="radio-box-xx2"> 
                                                            <div class="check-main-xx3">
                                                                <figure class="chkimg">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-3.png" class="img-responsive">
                                                                </figure>										
                                                                <h3 class="sub-head-c text-center">Greens</h3>
                                                            </div>
                                                        </label>
                                                    <?php } ?>
                                                    <?php
                                                    if ($data[0]['design_colors'] && in_array("purple", $color)) {
                                                        $flag = 1;
                                                        ?>
                                                        <label for="id4" class="radio-box-xx2"> 
                                                            <div class="check-main-xx3">
                                                                <figure class="chkimg">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-4.png" class="img-responsive">
                                                                </figure>										
                                                                <h3 class="sub-head-c text-center">Purple</h3>
                                                            </div>
                                                        </label>
                                                    <?php } ?>
                                                    <?php
                                                    if ($data[0]['design_colors'] && in_array("pink", $color)) {
                                                        $flag = 1;
                                                        ?>
                                                        <label for="id5" class="radio-box-xx2"> 
                                                            <div class="check-main-xx3" onclick="funcheck()">
                                                                <figure class="chkimg">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-5.png" class="img-responsive">
                                                                </figure>										
                                                                <h3 class="sub-head-c text-center">Pink</h3>
                                                            </div>
                                                        </label>
                                                    <?php } ?>
                                                    <?php
                                                    if ($data[0]['design_colors'] && in_array("black", $color)) {
                                                        $flag = 1;
                                                        ?>
                                                        <label for="id6" class="radio-box-xx2"> 
                                                            <div class="check-main-xx3" onclick="funcheck()">
                                                                <figure class="chkimg">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-6.png" class="img-responsive">
                                                                </figure>										
                                                                <h3 class="sub-head-c text-center">Black</h3>
                                                            </div>
                                                        </label>
                                                    <?php } ?>
                                                    <?php
                                                    if ($data[0]['design_colors'] && in_array("orange", $color)) {
                                                        $flag = 1;
                                                        ?>
                                                        <label for="id7" class="radio-box-xx2"> 
                                                            <div class="check-main-xx3" onclick="funcheck()">
                                                                <figure class="chkimg">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-7.png" class="img-responsive">
                                                                </figure>										
                                                                <h3 class="sub-head-c text-center">Orange</h3>
                                                            </div>
                                                        </label>
                                                    <?php } ?>

                                                </div>	
                                            </div>
                                        </div>

                                        <p class="space-a"></p>
                                        <?php if ($flag != 1): ?>
                                            <div class="radio-check-kk1">
                                                <label> 
                                                    <div class="review-rowww">
                                                        <span class="review-colww left">
                                                            <span class="review-textcolww">
                                                                <?php
                                                                echo $data[0]['design_colors'];
                                                                ?></span>
                                                        </span>

                                                        <span class="review-colww">
                                                            <a class="review-circleww" href="<?php echo base_url(); ?>customer/request/new_request/brief/<?php echo $data[0]['id']; ?>"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-edit.png" class="img-responsive"/></a>
                                                        </span>
                                                    </div>
                                                </label>
                                            </div>
                                        <?php endif ?>

                                    <?php }
                                    ?>
                                    <p class="space-a"></p>
                                </div>
                                <!-- Color Plates End -->
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x2">Design Software Requirement<span class="maditory">*</span></label>
                                <div class="review-rowww">
                                    <span class="review-colww left">
                                        <span class="review-textcolww"><?php echo $data[0]['designer']; ?></span>
                                    </span>
                                    <span class="review-colww">
                                        <a class="review-circleww" href="<?php echo base_url(); ?>customer/request/new_request/brief/<?php echo $data[0]['id']; ?>"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-edit.png" class="img-responsive"></a>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x2">Deliverables<span class="maditory">*</span></label>
                                <div class="review-rowww">
                                    <span class="review-colww left">
                                        <span class="review-textcolww">
                                            <?php echo $data[0]['deliverables']; ?></span>
                                    </span>
                                    <span class="review-colww">
                                        <a class="review-circleww" href="<?php echo base_url(); ?>customer/request/new_request/brief/<?php echo $data[0]['id']; ?>"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-edit.png" class="img-responsive"></a>
                                    </span>
                                </div>
                            </div>

                            <div class="form-group goup-x1">
                                <label class="label-x2">Description<span class="maditory">*</span></label>
                                <pre class="txt-disp"><?php echo $data[0]['description']; ?></pre>
                            </div>

                            <div class="form-group goup-x1">
                                <div class="row">
                                    <div class="col-md-12">
                                            <!-- <p class="text-mb">Design.sample.jpg <strong>(1.5 MB)</strong></p> -->
                                        <p class="text-mb">
                                        <ul class="uploaded-img">
                                            <?php //echo "<pre>";print_r($data);
                                            for ($i = 0; $i < sizeof($data[0]['customer_attachment']); $i++) {
                                                
                                            
                                            ?>
                                             <?php 
                                                $type = substr($data[0]['customer_attachment'][$i]['file_name'], strrpos($data[0]['customer_attachment'][$i]['file_name'], '.') + 1);
                                                if ($type == "pdf") {
                                                    ?>
                                                    <li>
                                                        <div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/pdf.png" height="150"/>
                                                        </a>
                                                        </div>
                                                    </li>
                                                <?php } 
                                                   elseif ($type == "zip") {
                                                    ?>
                                                    <li>
                                                        <div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/zip.png" height="150"/>
                                                        </a>
                                                        </div>
                                                    </li>
                                                <?php }elseif ($type == "odt" || $type == "ods") {
                                                    ?>
                                                    <li>
                                                        <div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/odt.png" height="150"/>
                                                        </a>
                                                        </div>
                                                    </li>
                                                <?php }elseif ($type == "doc" || $type == "docx") { ?>
                                                    <li><div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/docx.png" height="150"/>
                                                        </a>
                                                        </div>
                                                    </li>
                                                <?php }elseif ($type == "psd") { ?>
                                                    <li><div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/psd.jpg" height="150"/>
                                                        </a>
                                                        </div>
                                                    </li>
                                                <?php }elseif ($type == "ai") { ?>
                                                    <li><div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/ai.png" height="150"/>
                                                        </a>
                                                        </div>
                                                    </li>
                                                <?php }elseif ($type == "rar") { ?>
                                                    <li><div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/rar.jpg" height="150"/>
                                                        </a>
                                                        </div>
                                                    </li>
                                                <?php }elseif ($type == "mov" || $type == "mp4") { ?>
                                                    <li><div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/mov.png" height="150"/>
                                                        </a>
                                                        </div>
                                                    </li>
                                                <?php } else { ?>
                                                    <li><div class="accimgbx33">
                                                        <a target="_blank" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" data-fancybox="images">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS.$data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" />
                                                        </a>
                                                        </div>
                                                    </li>
                                                    <?php
                                                } 
                                            }?>
                                        </ul>
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                            <!-- <p class="cross-btnlink"><a href=""><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cross-2.png"></a></p> -->
                                    </div>
                                </div>
                                <p class="space-e"></p>

                                <div class="uplinkflexs clearfix">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p class="savecols-col left">
                                                <?php if (isset($_GET['rep']) && $_GET['rep'] == 1): ?>
                                                    <a href="<?php echo base_url(); ?>customer/request/new_request/brief/<?php echo $data[0]['id']; ?>?rep=1" class="back-link-xx0 text-uppercase">Back</a>
                                                <?php else: ?>
                                                    <a href="<?php echo base_url(); ?>customer/request/new_request/brief/<?php echo $data[0]['id']; ?>" class="back-link-xx0 text-uppercase">Back</a>
                                                <?php endif ?>

                                            </p>
                                        </div>

                                        <div class="col-xs-4">
                                            <p class="savecols-col right btn-x">
                                                <button type="submit" name="final_submit" class="btn btn-a text-uppercase min-250 text-center" data-toggle="modal" data-target="#final" >Submit</button>
                                            </p>
                                        </div>

                                        <div class="col-xs-4">
                                            <p class="savecols-col right btn-x"><a href="<?php echo base_url(); ?>customer/request" class="back-link-xx11 text-uppercase">save &amp; exit</a></p> 
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</section>

<!-- jQuery (necessary for JavaScript plugins) -->
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/bootstrap.min.js"></script>
<script>
                                                        var $fileInput = $('.file-input');
                                                        var $droparea = $('.file-drop-area');

                                                        // highlight drag area
                                                        $fileInput.on('dragenter focus click', function () {
                                                            $droparea.addClass('is-active');
                                                        });

                                                        // back to normal state
                                                        $fileInput.on('dragleave blur drop', function () {
                                                            $droparea.removeClass('is-active');
                                                        });

                                                        // change inner text
                                                        $fileInput.on('change', function () {
                                                            var filesCount = $(this)[0].files.length;
                                                            var $textContainer = $(this).prev();

                                                            if (filesCount === 1) {
                                                                // if single file is selected, show file name
                                                                var fileName = $(this).val().split('\\').pop();
                                                                $textContainer.text(fileName);
                                                            } else {
                                                                // otherwise show number of files
                                                                $textContainer.text(filesCount + ' files selected');
                                                            }
                                                        });
</script>
</body>
</html>
