<?php 
//echo "<pre/>";print_R($chatdata);
$docArrOffice = array('doc', 'docx', 'odt', 'ods', 'xlsx', 'xls', 'txt', 'ai', 'pdf', 'ppt', 'pptx', 'pps', 'ppsx', 'tiff', 'xxx', 'eps');
$font_file = array('ttf', 'otf', 'woff', 'eot', 'svg');
$vedioArr = array('mp4', 'Mov');
$audioArr = array('mp3');
$ImageArr = array('jpg', 'png', 'gif', 'jpeg', 'bmp');
$zipArr = array('zip', 'rar');
?>
<div class="read-msg-box clearfix">
   <div class="msgk-chatrow">
    <div class="msgk-user-chat msgk-right <?php echo $chatdata['sender_type']; ?> del_<?php echo $chatdata['id']; ?>">
                 <div class="time-edit">
                    <p class="time_msg"><?php echo $chatdata['chat_created_time']; ?></p>
                <?php if($chatdata['is_deleted'] != 1) { ?>
                    <div class="editDelete editdeletetoggle_<?php echo $chatdata['id']; ?>">
                    <i class="fas fa-ellipsis-v openchange" data-chatid="<?php echo $chatdata['id']; ?>"></i>
                    <div class="open-edit open-edit_<?php echo $chatdata['id']; ?>" style="display:none">
                        <?php if($chatdata['is_filetype'] != 1){ ?>
                        <a href="javascript:void(0)" class="editmsg" data-editid="<?php echo $chatdata['id']; ?>">edit</a>
                        <?php } ?>
                        <a href="javascript:void(0)" class="deletemsg" data-delid="<?php echo $chatdata['id']; ?>">delete</a>
                    </div>
                    </div>
                <?php } ?>
                </div>
                <div class="msgk-mn-edit edit_main_<?php echo $chatdata['id']; ?>" style="display:none">
                    <form method="post" id="editMainmsg">
                        <textarea class="pstcmm sendtext" id="edit_main_msg_<?php echo $chatdata['id']; ?>"><?php echo $chatdata['msg']; ?></textarea> 
                        <a href="javascript:void(0)" class="edit_save"  data-id="<?php echo $chatdata['id']; ?>">Save</a> 
                        <a href="javascript:void(0)" class="cancel_main" data-msgid="<?php echo $chatdata['id']; ?>">Cancel</a>
                    </form>
                </div>
                <div class="msgk-mn msg_desc_<?php echo $chatdata['id']; ?>">
                    <div class="msgk-umsgbox">
                        <?php
                    $varmsg = '';
                    if ($chatdata['is_deleted'] == 1 && $chatdata['is_edited'] != 1) {
                        
                    }
                    if ($chatdata['is_deleted'] == 1 && $chatdata['is_edited'] != 1 || ($chatdata['is_edited'] == 1 && $chatdata['is_deleted'] == 1)) {
                        $varmsg = '<div class="edited_msg deleted_msg"><i class="fa fa-ban" aria-hidden="true"></i> You have deleted this message.</div>';
                    } elseif ($chatdata['is_edited'] == 1 && $chatdata['is_deleted'] != 1) {
                        $varmsg = '<div class="edited_msg">' . $chatdata['msg'] . '</div>';
                    } else {
                        $varmsg = $chatdata['msg'];
                    }
                    if ($chatdata['is_filetype'] == 1 && $chatdata['is_deleted'] != 1) { //echo "test".$chatdata['msg'];exit; 
                        $type = substr($chatdata['msg'], strrpos($chatdata['msg'], '.') + 1);
                        $updateurl = '';
                        if (in_array(strtolower($type), $docArrOffice)) {
                            $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                            $class = "doc-type-file";
                            $exthtml = '<span class="file-ext">' . $type . '</span>';
                        } elseif (in_array(strtolower($type), $font_file)) {
                            $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                            $class = "doc-type-file";
                            $exthtml = '<span class="file-ext">' . $type . '</span>';
                        } elseif (in_array(strtolower($type), $zipArr)) {
                            $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-zip.svg';
                            $class = "doc-type-zip";
                            $exthtml = '<span class="file-ext">' . $type . '</span>';
                        } elseif (in_array(strtolower($type), $ImageArr)) {
                            $imgVar = imageRequestchatUrl(strtolower($type),$chatdata['req_id'], $chatdata['msg']);
                            $updateurl = '/_thumb';
                            $class = "doc-type-image";
                            $exthtml = '';
                        }
                        $basename = $chatdata['msg'];
                        $basename = strlen($basename) > 20 ? substr($basename, 0, 20) . "..." . $type : $basename;
                        $data_src = FS_PATH_PUBLIC_UPLOADS.'requestmainchat/'.$chatdata['req_id'].'/'.$chatdata['msg'];
                        ?>
                        <span class="msgk-umsxxt took_<?php echo $chatdata['id']; ?>">
                            <div class="contain-info <?php echo (!in_array($type, $ImageArr)) ? 'only_file_class' : ''; ?>" >
                                <a class="open-file-chat <?php echo $class; ?>" data-ext="<?php echo $type; ?>" data-src="<?php echo $data_src; ?>">
                                    <img src="<?php echo $imgVar; ?>"> <?php echo $exthtml; ?>
                                </a>
                                <div class="download-file">
                                    <div class="file-name">
                                        <h4><b><?php echo $basename; ?></b></h4>
                                    </div>
                                    <a href="<?php echo base_url() ?>customer/request/download_stuffFromChat?ext=<?php echo $type; ?>&file-upload=<?php echo $data_src; ?>" target="_blank"> 
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/dwnlod-img.svg" />
                                    </a>
                                </div>
                            </div>
                        </span>
                    <?php } else { ?>
                        <pre>
                                    <span class="edit_icon_<?php echo $chatdata['id']; ?> edit_icon_msg">
                                <?php if ($chatdata['is_edited'] == 1 && $chatdata['is_deleted'] != 1) { ?>
                                                        <i class="fas fa-pen"></i>
                                <?php } ?>
                                    </span>
                                    <span class="msgk-umsxxt took_<?php echo $chatdata['id']; ?> message-text"><?php echo $varmsg; ?></span>
                        </pre>
                    <?php } ?>
                    </div>  
                </div>
    </div>
</div>
</div>
