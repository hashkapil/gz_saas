<?php $property = "";
if($clientdata[0]['usertype'] == "Shared"){
    $property = "disabled";
}
?>
<div class="row client-edition">
    <div class="col-md-4">
        <div class="client-profile">
            <div class="profile-pic">
                <figure>
                    <img src="<?php echo $clientdata[0]['profile_picture']; ?>" class="img-responsive telset33">
                </figure>
            </div>
            <div class="main-discp">
                <h3>Client</h3>
                <h2>
                    <?php echo $first_name . ' ' . $last_name; ?>
                </h2>
                <p>
                    <?php if (isset($clientdata[0]['email'])): ?>
                        <?php echo $clientdata[0]['email']; ?>
                    <?php endif ?>
                </p>
                <a href="javascript:void(0)" class="f_clnt_pswrd" data-target="#f_clntreset_password" data-toggle="modal">Reset Password</a>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="detailed-item">
            <ul>
                <li><label for="">Current Plan</label><span><?php echo ($clientdata[0]['plan_name'] != "") ? $clientdata[0]['plan_name'] : "No Member" ?></span></li>
                <li><label for=""><?php echo 'Billing Start'; ?></label><span><?php echo ($clientdata[0]['billing_start_date'] != '') ? $clientdata[0]['billing_start_date'] : 'N/A'; ?></span></li>
                <li><label for=""><?php echo 'Billing End'; ?></label><span><?php echo ($clientdata[0]['billing_end_date'] != '') ? $clientdata[0]['billing_end_date'] : 'N/A'; ?></span></li>
                <li><label for=""><?php echo 'Company Name'; ?></label><span><?php echo ($clientdata[0]['company_name'] != '') ? $clientdata[0]['company_name'] : 'N/A'; ?></span></li>
                <li><label for=""><?php echo 'Address'; ?></label><span><?php echo ($clientdata[0]['address_line_1'] != '') ? $clientdata[0]['address_line_1'] : 'N/A'; ?></span></li>
                <li><label for=""><?php echo 'User Type'; ?></label><span><?php echo ($clientdata[0]['usertype'] != '') ? $clientdata[0]['usertype'] : 'N/A'; ?></span></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="list-header-blog client-inform">
                <li class="active"><a href="#general" class="stayhere" data-toggle="tab">Profile Info</a></li>
                <li><a href="#billingPlan" class="stayhere" data-toggle="tab">Billing</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="tab-content">
                <div class="tab-pane active" id="general">   
                    <div class="tabs-card">
                        <form method="post" id="about_info_client_form" action="<?php echo base_url() . 'customer/AgencyClients/customer_edit_profile/' . $clientdata[0]['id']; ?>">
                            <div class=""><!-- settingedit-box -->
                                <div class="settrow">
                                    <h2 class="main-info-heading">Profile Information</h2>
                                    <p class="fill-sub">Please fill your Profile Information</p>
                                    <input type="hidden" id="user_id" name="userid" value="<?php echo $clientdata[0]['id']; ?>">
                                    <div class="settcol right">
                                        <p class="btn-x">
                                            <input type="submit" id="about_info_btn" name="savebtn" class="btn-e save_info" value="Save"/>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['first_name']) && $clientdata[0]['first_name'] != "") ? "label-active" : ""; ?>"> First Name </p>
                                            <input type="text" name="first_name" class="input" value="<?php
                                            if (isset($clientdata[0]['first_name'])) {
                                                echo $clientdata[0]['first_name'];
                                            }
                                            ?>" id="user_name" required/>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['last_name']) && $clientdata[0]['last_name'] != "") ? "label-active" : ""; ?>"> Last Name </p>
                                            <input type="text" name="last_name" class="input" value="<?php
                                            if (isset($clientdata[0]['last_name'])) {
                                                echo $clientdata[0]['last_name'];
                                            }
                                            ?>" id="last_name" required/>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['email']) && $clientdata[0]['email'] != "") ? "label-active" : ""; ?>"> Email </p>
                                            <input type="email" class="input" value="<?php echo $clientdata[0]['email']; ?>" name="notification_email" required/>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['company_name']) && $clientdata[0]['company_name'] != "") ? "label-active" : ""; ?>"> Company Name </p>
                                            <input type="text" class="input" value="<?php
                                            if (isset($clientdata[0]['company_name'])) {
                                                echo $clientdata[0]['company_name'];
                                            }
                                            ?>" name="company_name">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['title']) && $clientdata[0]['title'] != "") ? "label-active" : ""; ?>"> Title </p>
                                            <input type="text" class="input" value="<?php
                                            if (isset($clientdata[0]['title'])) {
                                                echo $clientdata[0]['title'];
                                            }
                                            ?>" name="title">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['phone']) && $clientdata[0]['phone'] != "") ? "label-active" : ""; ?>"> phone </p>
                                            <input type="text" class="input" value="<?php
                                            if (isset($clientdata[0]['phone'])) {
                                                echo $clientdata[0]['phone'];
                                            }
                                            ?>" name="phone">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['address_line_1']) && $clientdata[0]['address_line_1'] != "") ? "label-active" : ""; ?>"> Address </p>
                                            <textarea class="input" name="address_line_1" placeholder="<?php echo $clientdata[0]['address_line_1']; ?>"><?php
                                            if (isset($clientdata[0]['address_line_1'])) {
                                                echo $clientdata[0]['address_line_1'];
                                            }
                                            ?></textarea>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['city']) && $clientdata[0]['city'] != "") ? "label-active" : ""; ?>"> City </p>
                                            <input type="text" class="input" value="<?php
                                            if (isset($clientdata[0]['city'])) {
                                                echo $clientdata[0]['city'];
                                            }
                                            ?>" name="city">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['state']) && $clientdata[0]['state'] != "") ? "label-active" : ""; ?>"> state </p>
                                            <input type="text" class="input" value="<?php
                                            if (isset($clientdata[0]['state'])) {
                                                echo $clientdata[0]['state'];
                                            }
                                            ?>" name="state">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>

                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['zip']) && $clientdata[0]['zip'] != "") ? "label-active" : ""; ?>"> zip Code </p>
                                            <input type="text" class="input" value="<?php
                                            if (isset($clientdata[0]['zip'])) {
                                                echo $clientdata[0]['zip'];
                                            }
                                            ?>" name="zip">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div> 
                        </form>
                    </div>
                    <!--survey info-->

                    <div class="tabs-card">
                        <form class="user_level_set" action="<?php echo base_url() . 'customer/AgencyClients/customer_active_requests/' . $clientdata[0]['id']; ?>" id="custom_active_req" method="post">
                            <div class="settrow">
                                <h2 class="main-info-heading">User Setting</h2>
                                <p class="fill-sub"><?php echo ($clientdata[0]['overwrite'] == 0) ? "These are default plan settings,You can override these from here." : "Following data is the override plan settings,You can change these from here."; ?></p>
                                <div class="settcol right">
                                    <p class="btn-x">
                                        <input type="submit" id="req_info_btn" name="savebtn" class="btn-e save_info" value="Save"/>
                                    </p>
                                </div>
                            </div>
                            <input type="hidden" id="curuser_id" name="curuser_id" value="<?php echo $clientdata[0]['id']; ?>">
                            <div class="row">
                                <?php if ($clientdata[0]['plan_data']['plan_type_name'] == "one_time") { ?>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['total_requests']) && $clientdata[0]['total_requests'] != "") ? "label-active" : ""; ?>"> How many requests</p>
                                            <input type="number" min="-1" class="total_requests input" id="usertotalreq" name="total_requests" value="<?php
                                            if (isset($clientdata[0]['total_requests'])) {
                                                echo $clientdata[0]['total_requests'];
                                            }
                                            ?>" required="">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                <?php } else { ?>
                                <div class="f_display_none_sec">
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['total_active_req']) && $clientdata[0]['total_active_req'] != "") ? "label-active" : ""; ?>">Total Active Requests</p>
                                            <input type="number" min="1" class="total_req_count input" id="useractivereq" name="total_req_count" value="<?php
                                            if (isset($clientdata[0]['total_active_req'])) {
                                                echo $clientdata[0]['total_active_req'];
                                            }
                                            ?>" <?php echo $property; ?>>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($clientdata[0]['total_inprogress_req']) && $clientdata[0]['total_inprogress_req'] != "") ? "label-active" : ""; ?>">Inprogress Requests</p>
                                            <input type="number" min="1" class="input" id="userinprogreq" name="inprogess_req_count" value="<?php
                                            if (isset($clientdata[0]['total_inprogress_req'])) {
                                                echo $clientdata[0]['total_inprogress_req'];
                                            }
                                            ?>" <?php echo $property; ?>>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="f_display_none_sec">
                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <div class="label-txt <?php echo (isset($clientdata[0]['brand_profile_count']) && $clientdata[0]['brand_profile_count'] != "") ? "label-active" : ""; ?>"> How many brand profile
                                                <div class="tool-hover">
                                                    <i class="fas fa-info-circle"></i>
                                                    <div class="tool-text"><strong>Note:</strong> <span>Keep -1</span>, To allow user to add unlimited brand profile.
                                                        <span>Keep 1 to n number</span>, To allow user to add specific number of brand profile.
                                                        <span>Keep 0</span>, To not allow to users to add brand profile.
                                                    </div>
                                                </div></div>
                                                <input type="number" name="brand_size" min="-1" class="brand_size input" id="brand_size" value="<?php echo isset($clientdata[0]['brand_profile_count']) ? $clientdata[0]['brand_profile_count'] : ""; ?>">
                                                <div class="line-box">
                                                    <div class="line"></div>
                                                </div>
                                            </label>
                                        </div>
                                </div>
<!--                                    <div class="col-sm-4">
                                        <label class="form-group">
                                            <div class="label-txt label-active">User Type</div>
                                            <select class="shareduser input" name="shared_user">
                                                <option value="1" <?php
//                                                if ($clientdata[0]['is_shared_user'] == 1) {
//                                                    echo "selected";
//                                                }
                                                ?>>Shared</option>
                                                <option value="0" <?php
//                                                if ($clientdata[0]['is_shared_user'] == 0) {
//                                                    echo "selected";
//                                                }
                                                ?>>Dedicated</option>
                                            </select>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>-->
                            </div>
                            <div class="row">
                                    <div class="col-sm-4">
                                        <label class="notify-lines">
                                            <div class="switch-custom-usersetting-check switch-custom_toggle">
                                                <input type="checkbox" name="file_management" data-cid="<?php echo $clientdata[0]['id']; ?>" id="switch_toggle_<?php echo $clientdata[0]['id']; ?>" <?php echo ($clientdata[0]['file_management'] == 1) ? 'checked' : ''; ?> />
                                                <label for="switch_toggle_<?php echo $clientdata[0]['id']; ?>"></label> 
                                            </div>
                                            <p class="label-txt">File Management</p>
                                        </label>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="notify-lines">
                                            <div class="switch-custom-usersetting-check f_filesharing">
                                                <input type="checkbox" name="file_sharing" data-cid="<?php echo $clientdata[0]['id']; ?>" id="switch_file_<?php echo $clientdata[0]['id']; ?>" <?php echo ($clientdata[0]['file_sharing'] == 1) ? 'checked' : ''; ?> />
                                                <label for="switch_file_<?php echo $clientdata[0]['id']; ?>"></label> 
                                            </div>
                                            <p class="label-txt">File Sharing Capabilities</p>
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="tab-pane" id="billingPlan">
                        <div class="white-boundries">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="settrow">
                                        <h2 class="main-info-heading">Billing and Subscription</h2>
                                        <p class="fill-sub">Update Billing or Change user's Subscription Plan below.</p>
                                    </div>
                                </div>
                        </div>
                <div class="card-js">
                    <?php  if ($clientdata[0]['card']) { ?>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="form-group">
                                    <p class="label-txt <?php echo ($clientdata[0]['card']['0']->last4 != '') ? 'label-active' : ''; ?>">Card Number <span>*</span></p>
                                    <input type="text" name="card-number" class="card-number input" value='************<?php echo $clientdata[0]['card']['0']->last4; ?>' readonly maxlength="16">
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label class="form-group">
                                    <p class="label-txt label-active fixedlabelactive">Expiry Date <span>*</span></p>
                                    <input type="text" class="input expiry-month" value="<?php echo $clientdata[0]['card']['0']->exp_month; ?>/<?php echo $clientdata[0]['card']['0']->exp_year; ?>" name="expiry-month-date" id="card_expir_date" readonly placeholder="MM  /  YY" onkeyup="dateFormat(this.value,this);">
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label class="form-group">
                                    <p class="label-txt label-active">CVV <span>*</span></p>
                                    <input type="text" name="cvc" value="***" id="cvc_detail" class="input" maxlength="3" readonly>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="btn-here">
                                        <input type="hidden" name="current_plan" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }else{
                        echo "Card details not found";
                    } ?>
                </div>
            </div>
            <div class="white-boundries">
                <div class="client_billin_secton">
                    <div class="settrow">
                        <h2 class="main-info-heading">Billing Information</h2>
                        <p class="fill-sub">Please change your billing plan here</p>
                        <input type="hidden" id="user_id" name="userid" value="1883">
                        <div class="settcol right">
                                <p class="btn-x slctpln_subs">
                                    <a href="javascript:void(0)" class="btn-f Change_Plan chossbpanss"><?php echo ($clientdata[0]['plan_name'] != '') ? "Change plan" : "Select plan"; ?></a>
                                </p>
                                <p class="btn-x bckbtntoback" style="display:none">
                                    <a href="javascript:void(0)" class="btn-f Change_Plan backfromnewplan">Back</a>
                                </p>
                        </div>
                    </div>

                    <div class="row billing_plan_view">
                        <div class="col-md-6 col-sm-6 current_actv_pln billing_plan">
                            <div class="flex-grid">
                                <div class="price-card best-offer">
                                    <div class ="chooseplan active">
                                        <?php
                                        if ($clientdata[0]['plan_name'] != '') {
                                            $agency_priceclass = "<span class='" . $clientdata[0]['plan_data']['plan_type'] . " agency_price'> " . $clientdata[0]['plan_data']['plan_price'] . "</span>";
                                            if ($clientdata[0]['plan_data']['plan_type'] == "unlimited") {
                                                $clientdata[0]['plan_data']['plan_type'] = $clientdata[0]['total_requests'] . " Request";
                                            }
                                            ?>
                                            <h2><?php echo $clientdata[0]['plan_data']['plan_name']; ?></h2>
                                            <h3><font>$</font><?php echo $agency_priceclass; ?><span>/<?php echo $clientdata[0]['plan_data']['plan_type']; ?></span></h3>
                                            <div class="p-benifite">
                                                <?php if ($clientdata[0]['plan_data']['plan_type_name'] == 'one_time') { ?>
                                                    <ul>
                                                        <li><p>Total Request: <?php echo $clientdata[0]['total_requests']; ?></p></li>
                                                        <li><p>Added Request: <?php echo $clientdata[0]['request']['added_request']; ?></p></li>
                                                        <li><p>Pending Request: <?php echo $clientdata[0]['request']['pending_req']; ?></p></li>
                                                    </ul>
                                                    <?php
                                                } else {
                                                    $pln_feature = explode("_", $clientdata[0]['plan_data']['features']);
                                                    $plnfeature = "<ul>";
                                                    if (!empty($pln_feature)) {
                                                        foreach ($pln_feature as $text) {
                                                            $plnfeature .= "<li>" . $text . "</li>";
                                                        }
                                                    }
                                                    $plnfeature .= "<ul>";
                                                    echo $plnfeature;
                                                }
                                                ?>
                                            </div>
                                            <div class="price-sign-up">
                                                <a type="button" class="ud-dat-p currentPlan" disabled>
                                                    Current Plan
                                                </a>
                                            </div>
                                            <div class="active-check" style="display: none"><i class="fas fa-check"></i></div>
                                            <?php
                                        } else {
                                            echo "no selected Plan";
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row billing_plan new_plan_billingfr_chng two-can owl-carousel" style="display:none">

                        <?php
                        //echo "<pre>";print_r($active_subscriptions);
                        foreach ($active_subscriptions as $userplan) {
                            if($userplan["payment_mode"] == $clientdata[0]['plan_data']["payment_mode"]){
                            $agencyclass = $userplan['plan_type'] . " upgrd_agency_pln";
                            $pln_feature = explode("_", $userplan['features']);
                            $plnfeature = "<ul>";
                            if (!empty($pln_feature)) {
                                foreach ($pln_feature as $text) {
                                    $plnfeature .= "<li>" . $text . "</li>";
                                }
                            }
                            if (!isset($agency_data[0]['reserve_count']) || $agency_data[0]['reserve_count'] == -1) { 
                                $reserveslot = -1;
                            }
                            $plnfeature .= "<ul>";
                            $agency_priceclass = "<span class='" . $userplan['plan_type'] . " agency_price'> " . $userplan['plan_price'] . "</span>";
                            ?>
                            <div class="flex-grid">
                                <div class="price-card">
                                    <div class="chooseplan">
                                        <h2><?php echo $userplan['plan_name']; ?></h2>
                                        <h3><font>$</font><?php echo $agency_priceclass; ?><span>/<?php echo $userplan['plan_type']; ?></span></h3>
                                        <div class="p-benifite">
                                            <div class="price-details">
                                                <?php
                                                if ($userplan['plan_type'] == 'yearly') {
                                                    $amount = 'annual_price';
                                                } else {
                                                    $amount = 'amount';
                                                }
                                                echo $plnfeature;
                                                ?>
                                            </div>
                                        </div>
                                        <div class="price-sign-up">
                                            <?php if ($clientdata[0]['plan_data']['plan_type'] == 'yearly' && $userplan['plan_type'] == 'monthly') { ?>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#planpermissionspopup" type="button" class="nfChngPln ud-dat-p <?php echo $agencyclass; ?>">
                                                    Change Plan
                                                </a>
                                                <?php
                                            }else if($clientdata[0]['plan_data']['plan_type_name'] == 'all' && $userplan['plan_type_name'] == 'one_time'){ ?>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#sbusrplnprmsnspopup" type="button" class="nfChngPln ud-dat-p <?php echo $agencyclass; ?>">
                                                    Change Plan
                                                </a>
                                            <?php }else if($userplan['client_slot']['slot'] == 0){?>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#cantaddsubs" data-reserveslot="<?php echo $reserveslot; ?>" data-assignreq="<?php echo $clientdata[0]['req_count']['main_inprogress_req']; ?>" data-alrdyassignreq="<?php echo $clientdata[0]['req_count']['addedsbrqcount']; ?>" data-pndngrq="<?php echo $clientdata[0]['req_count']['pending_req']; ?>" type="button" class="cantaddclnt ud-dat-p <?php echo $agencyclass; ?>">
                                                    Change Plan
                                                </a>
                                            <?php }else { 
                                                    if ($clientdata[0]['plan_name'] == "") { ?>
                                                    <a href="javascript:void(0)" data-value="<?php echo $userplan['plan_id']; ?>" data-clientid="<?php echo $clientdata[0]['id']; ?>" data-price="<?php echo $userplan['plan_price']; ?>" data-applycoupn="<?php echo $userplan['apply_coupon']; ?>" data-toggle="modal" data-target="#Upgrdsubuseracount" type="button" class="upgrdclntacnt ud-dat-p <?php echo $agencyclass; ?>">
                                                        Choose Plan
                                                    </a>
                                                <?php }elseif($clientdata[0]['plan_name'] == $userplan['plan_id']){ ?>
                                                    <a type="button" class="ud-dat-p currentPlan" disabled>
                                                        Current Plan
                                                     </a>
                                                <?php } else { ?> 
                                                    <a href="javascript:void(0)" data-value="<?php echo $userplan['plan_id']; ?>" data-clientid="<?php echo $clientdata[0]['id']; ?>" data-price="<?php echo $userplan['plan_price']; ?>" data-inprogress="<?php echo $userplan['global_inprogress_request']; ?>" data-totlactive="<?php echo $userplan['global_active_request']; ?>" data-display_name="<?php echo $userplan['plan_name']; ?>" data-toggle="modal" data-target="#chnagesubuserpln" type="button" class="ChngclntPln ud-dat-p <?php echo $agencyclass; ?>">
                                                        Change Plan
                                                    </a>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                        <div class="active-check" style="display: none"><i class="fas fa-check"></i></div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade similar-prop" id="f_clntreset_password" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Reset Password</h2>
                    <div class="cross_popup" data-dismiss="modal"> x</div>
                </header>
                <div class="cli-ent-model">
                    <h3 class="text-center">An email will be sent to the customer with the password.</h3>
                    <form action="<?php echo base_url(); ?>customer/AgencyClients/reset_client_password" method="post" accept-charset="utf-8">
                        <input type="hidden" name="reset_client" class="input" id="reset_client" value="<?php echo $clientdata[0]['id']; ?>"/>
                        <input type="hidden" name="reset_email" class="input" id="reset_email" value="<?php echo $clientdata[0]['email']; ?>"/>
                        <div class="col-md-12 password_toggle">
                            <div class="notify-lines">
                                <p>Use Random Password</p>
                                <label class="form-group switch-custom-usersetting-check">
                                    <input type="checkbox" name="genrate_password" id="password_ques" checked/>
                                    <label for="password_ques"></label> 
                                </label>
                            </div>
                        </div>
                        <div class="create_password" style="display:none">
                            <label class="form-group">
                                <p class="label-txt">Enter Password<span>*</span></p>
                                <input type="password" name="password" class="input" id="password" value=""/>
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                        </div>
                        <div class="form-group">
                            <div class="btn-here text-center">
                                <input type="submit" name="save" class="btn-red" value="Reset">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    <?php if($clientdata[0]["requests_type"] != "one_time"){ ?>
        var client_inprogress_req = '<?php echo $clientdata[0]['total_inprogress_req'];?>';
    <?php }else{ ?>
        var client_inprogress_req = '<?php echo intval($clientdata[0]['total_requests']/$clientdata[0]['plan_data']['global_inprogress_request']);?>';
    <?php }?>
</script>