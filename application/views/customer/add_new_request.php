<?php
$editID = isset($_GET['reqid']) ? $_GET['reqid']:'';
if($canuseradd == 0 || $useraddrequest != 2 && $editID == ''){
    redirect(base_url().'customer/request/design_request');
}
$countedval = count($allqust);
$halfcount = (int)(($countedval+2)/2);
$questionssection = ($countedval >= 6) ? 'twosection' : 'onesection';
?>
<section id="add-categeory">
    <div class="container">
        <div class="header-blog">
            <?php if ($this->session->flashdata('message_error') != '') { ?>
                <div id="message" class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_error'); ?>
                    </p>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('message_success') != '') { ?>
                <div id="message" class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_success'); ?>
                    </p>
                </div>
            <?php }
            ?>
        </div>
        <form method="post" id="submit_requests_form">
            <div class="row">
                <?php
                $is_trial = $profile_data[0]['is_trail'];
                $editvar = '';
                $editID = isset($_GET['reqid']) ? $_GET['reqid'] : '';
                $dupID = isset($_GET['did']) ? $_GET['did'] : '';
                if ($editID && $editID != '') {
                    $editvar = $existeddata[0];
                }if ($dupID && $dupID != '') {
                    $editvar = $duplicatedata[0];
                }if ($editID == '' && $dupID == '') {
                    $editvar = '';
                }
                $category = array('Popular Design' => 'Popular Design', 'Branding & Logos' => 'Branding & Logos', 'Social Media' => 'Social Media', 'Marketing' => 'Marketing', 'UI/UX' => 'UI/UX', 'Artwork' => 'Artwork', 'Custom' => 'Custom');
                //echo "<pre>";print_r($editvar);
                ?>
                <input type="hidden" id="for_trial" value="<?php echo $is_trial; ?>">
                <div class="col-lg-12 catagry-heading" data-step="8" data-intro="Use this form to add details about the design you need created." data-position='right' data-scrollTo='tooltip'>
                    <h3>Add New Project<span>Provide details of your new design project.</span></h3>
                </div>
                <div class="col-lg-12 col-md-12">
                    <div class="catagory">
                        <div class="step-row">
                            <h3>Step 1.</h3>
                            <span class="desc_steps">Select the category and sub-category that best suits your project.</span>
                        </div>
                        <div class="form-group">
                            <select id="cat_page" class="form-control select-a" required name="category_id" onchange="getval(this);">
                                <?php foreach ($allcategories as $key => $value) { ?>
                                    <option value="<?php echo $value['id']; ?>" 
                                            <?php echo ($value['id'] == $editvar['category_id'] || $value['name'] == $editvar['category']) ? 'selected' : ''; ?>>
                                        <?php echo $value['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="error_msg"></div>
                        <div class="plan-boxex-xx5 clearfix two-can" id="boxes">
                            <?php foreach ($allcategories as $kp => $vp) { ?>
                                <div class="form-group" class="hide-prev" id="<?php echo "child_" . $vp['id']; ?>" style="display:none">
                                    <div class="row">
                                        <?php foreach ($vp['child'] as $kc => $vc) { 
                                            //echo "<pre/>";print_R($vc['agency_only']);
                                            //if(in_array($vc['id'],AGENCY_CATEGORIES)){
                                            ?>
                                            <div class="col-xs-6 col-md-2">
                                                <label class="radio-box-xx2 <?php echo ($vc['agency_only'] == '1') ? 'allowed_agency_file' : ''; ?>"> 
                                                    <input name="subcategory_id" class="subcategory_class"  value="<?php echo $vc['id']; ?>" type="radio" <?php echo ($editvar['subcategory_id'] == $vc['id'] || $editvar['logo_brand'] == $vc['name']) ? 'checked' : ''; ?> required="">
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3">
                                                        <?php if ($is_trial == 1) { ?><span class="upgrade-label"></span><?php } ?>
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/customer-images/<?php echo $vc['image_url']; ?>" class="img-responsive">
                                                        </figure>										
                                                        <h3><?php echo $vc['name']; ?></h3>
                                                    </div>
                                                </label>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>

                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="add_request_loader" style="display: block; justify-content: center;width: 100%;padding-bottom: 15px;">
                        <img class="imgloader_questt" src="<?php echo FS_PATH_PUBLIC_ASSETS.'img/customer/gz_customer_loader.gif';?>" style="display: none;">
                </div>
                <div class="for_loading_while">
               <div class="col-lg-12 col-md-12 sampleexist" style="<?php echo (!empty($samples)) ? 'display:block' : 'display:none'; ?>">
                    <div class="catg-sample-list">
                        <div class="step-row">
                            <h3 class="step_samp">Step 2.</h3>
                        </div>
                        <div class="color-inside catagry-heading">
                                <h3> Design choice preferences<span class='sample_label' style="<?php echo (!empty($samples)) ? 'display:block' : 'display:none'; ?>">Select between <span class="min_val"> <?php echo $minval; ?></span> - <span class="max_val"> <?php echo $maxval; ?></span> design styles you prefer for this project.</span></h3>
                            </div>
                        <div class="color-combination">
                            
                            <div class='maximumselection' data-maxsel="<?php echo (isset($maxval)) ? $maxval + 1 : ''; ?>">
                                <div class="plan-boxex-xx5 clearfix two-can sample" id="boxes">
                                        <div class="form-group owl-carousel owl-theme" id="samples_categories" data-minchk="<?php echo (isset($minval)) ? $minval : ''; ?>" data-maxchk="<?php echo (isset($maxval)) ? $maxval : ''; ?>">
                                            <?php foreach ($samples as $ka => $va) { ?>
                                                <div class="sample_div">
                                                    <label class="radio-box-xx2"> 
                                                        <input name="sample_subcat[]" class="sample_subcat" id="<?php echo $va['material_id']; ?>" value="<?php echo $va['material_id']; ?>" type="checkbox" <?php echo (in_array($va['material_id'], $selectdmaterials)) ? 'checked' : ''; ?>>
                                                        <span class="checkmark"></span>
                                                        <div class="check-main-xx3">
                                                            <figure class="chkimg">
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS; ?>samples/<?php echo $va['sample_id'] . '/' . $va['sample_material']; ?>" class="img-responsive">
                                                            </figure>										
                                                        </div>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <div class="error_msg"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>	
                <div class="col-lg-12 col-md-12">
                    <div class="step-row">
                        <h3 class="step_qus">Step <?php echo (!empty($samples)) ? '3.' : '2.'; ?></h3>
                         <span class="desc_steps not_sample">Provide some details about your project.</span>
                    </div>
                    <div class="row">
                        <div class="col-md-6 frstappnd">         
                            <div class="catagory-right">
                                <div class="title_class">
                                    <label class="form-group">
                                        <p class="label-txt <?php echo isset($editvar['title']) ? "label-active" : ""; ?>"> Project Title * </p>
                                        <input type="text" class="input title" name="title"  value="<?php echo isset($editvar['title']) ? $editvar['title'] : ""; ?>" required=""/>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                        <div class="error_msg"></div>
                                    </label>
                                </div>
                                <label class="form-group">
                                    <p class="label-txt <?php echo isset($editvar['brand_id']) ? "label-active" : ""; ?>">Select Brand Profile</p>
                                    <select name="brand_name" class="input  select-a">
                                        <option value=""></option>
                                        <?php
                                        if ($user_profile_brands != '') {
                                            for ($i = 0; $i < sizeof($user_profile_brands); $i++) {
                                                ?>
                                                <option value="<?php echo $user_profile_brands[$i]['id']; ?>" <?php
                                                if ($user_profile_brands[$i]['id'] == $editvar['brand_id']) {
                                                    echo "selected";
                                                }
                                                ?>><?php echo $user_profile_brands[$i]['brand_name']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                    </select>
                                    <div class="line-box">
                                        <div class="line"></div>
                                    </div>
                                </label>
                                <div class="categoryqust">
                                    <?php
                                    $countedval = count($allqust);
//                                    echo $countedval;
                                    $halfcount = (int)(($countedval + 2)/2);
//                                    echo $halfcount;
                                    if($questionssection == 'twosection'){
                                      $brkgpnt = (int)($halfcount-2);
                                    }else{
                                       $brkgpnt = count($allqust);
                                    }
                                    for ($i = 0; $i < $brkgpnt; $i++) {
                                        $data['allqust'] = $allqust[$i];
                                        $this->load->view('customer/questions_template', $data);
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 secondappnd" style="<?php echo ($questionssection == 'twosection') ? 'display:block' : 'display:none';?>">         
                            <div class="catagory-right">
                                <div class="categoryqust">
                                <?php
                                for ($j = $brkgpnt; $j < count($allqust); $j++) {
                                    $data['allqust'] = $allqust[$j];
                                    $this->load->view('customer/questions_template', $data);
                                }
                                ?>
                                </div>
                            </div>
                        </div>

                        <div class="colors-row <?php echo ($questionssection == 'onesection') ? 'rightcolclass' : '';?>">
                        <div class="col-md-6 colrpref <?php echo ($questionssection == 'onesection') ? 'ryttop' : '';?>">                        
                            <div class="color-combination color-colmn">
                                <div class="color-inside catagry-heading">
                                    <h3>Color preference <span>Choose color or let designer choose.</span></h3>
                                    <div class="sound-signal">
                                        <div class="form-radion">
                                            <input type="radio" name="color_pref[]" class="preference color_codrr" id="soundsignal1" <?php echo ($editvar['color_pref'] == "Let Designer Choose" || $editvar['color_pref'] == '') ? 'checked' : ''; ?> value="Let Designer Choose">
                                            <label for="soundsignal1">Let Designer Choose for me.</label>
                                        </div>
                                        <?php $color_pref =  (isset($editvar['color_pref']) && $editvar['color_pref'] !== "Let Designer Choose") ? "block" : "none";  ?>
                                        <div class="form-radion select-hax">
                                            <input type="radio" name="color_pref[]" id="soundsignal2" class="preference color_codrr" value="color_code" <?php echo ($editvar['color_pref'] !== "Let Designer Choose" && $editvar['color_pref'] != '') ? 'checked' : '' ?>>
                                            <label for="soundsignal2">Color Code</label>
                                            <div class="form-radion choose-color-option choose_select_pref " style="display:<?php echo $color_pref;?>">
                                            <label class="form-group">
                                                    <select name="color_pref[]" class="input select-a choose_design_pref">
                                                        <option value="">Choose color type</option>
                                                        <option value="hex" <?php echo ($editvar['color_pref'] == 'hex') ? 'selected' : '';?>>Hex Code</option>
                                                        <option value="pantone" <?php echo ($editvar['color_pref'] == 'pantone') ? 'selected' : '';?>>Pantone Color</option>
                                                    </select>
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                            </label>
                                        </div>
                                             </div>
                                        <div class="form-radion choose-color-option choose_select_pref" style="display:<?php echo $color_pref;?>">
                                            <label class="form-group color_choice" style="display:<?php echo $color_pref;?>">
                                                <p class="label-txt <?php echo (isset($editvar['color_pref']) && $editvar['color_pref'] !== "Let Designer Choose" && $editvar['color_pref'] != '') ? 'label-active' : '';?>">color code </p>
                                                 <?php if(!empty($editvar['color_values'])){
                                                $links_arr = explode(',',$editvar['color_values']);
                                                foreach($links_arr as $lk => $lv){ ?>
                                                <input type="text" name="color_values[]" class="input f_color_nm" id="hexa-color2" placeholder="" value="<?php echo (isset($lv) && $lv !== "Let Designer Choose") ? $lv : '' ?>">
                                                    <div class="line-box">
                                                      <div class="line"></div>
                                                  </div>
                                              <?php }}else{ ?>
                                                <input type="text" name="color_values[]" class="input f_color_nm" id="hexa-color2" placeholder="" value="">
                                                <div class="line-box">
                                                  <div class="line"></div>
                                              </div>
                                          <?php } ?>
                                                <a class="add_more_colors">
                                                    <i class="icon-gz_plus_icon"></i>
                                                </a>
                                                 <div class="input_color_container"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 delvrb <?php echo ($questionssection == 'onesection') ? 'rytbtm' : '';?>">
                            <div class="color-combination color-colmn">

                                <div class="color-inside catagry-heading">
                                    <h3>Deliverables <span>What would you like your designer to deliver?</span></h3>
                                    <div class="sound-signal2">
                                        <?php $deliverable = explode(',', $editvar['deliverables']); ?>
                                        <div class="form-radion2">
                                            <label class="containerr">Source Files
                                                <input type="checkbox" name="filetype[]" value="SourceFiles" checked="checked" <?php echo in_array('SourceFiles', $deliverable) ? 'checked' : ''; ?>>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="form-radion2">
                                            <label class="containerr">JPEG
                                                <input type="checkbox" name="filetype[]" value="jpeg" <?php echo in_array('jpeg', $deliverable) ? 'checked' : ''; ?>>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>

                                        <div class="form-radion2">
                                            <label class="containerr">PNG
                                                <input type="checkbox" name="filetype[]" value="png" <?php echo in_array('png', $deliverable) ? 'checked' : ''; ?>>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>

                                        <div class="form-radion2">
                                            <label class="containerr">PDF
                                                <input type="checkbox" name="filetype[]" value="pdf" <?php echo in_array('pdf', $deliverable) ? 'checked' : ''; ?>>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>  
                </div>
                </div>
            <div class="row for_loading_while">
                <section class="color-sec">
                     <div class="drop-file-wrap">

                        <div class="step-row">
                            <h3 class="step_upl">Step <?php echo (!empty($samples)) ? '4.' : '3.'; ?></h3>
                            <span class="desc_steps">Attach your own images or choose from the stock library.</span>
                        </div>         

                        <div class="col-md-6">
                            <div class="drop-file">
                                <div class="form-group goup-x1"> 
                                    <div class="file-drop-area file-upload">
                                        <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/cloud-computing.svg" class="img-responsive"></span>
                                        <span class="file-msg">Drag and drop file here or 
                                            <span class="nocolsl">Click Here</span>
                                        </span>
                                        <input type="file" class="file-input project-file-input" multiple="" name="file-upload[]" id="file_input">
                                    </div>
                                    <p class="text-g">Example: Doc, Docx, Odt, Pdf, Jpg, Png, Jpeg, Psd, Ai, Zip, Mp4, Mov, tiff, ttf, eps </p>
                                </div>
                                <?php if (isset($editvar['customer_attachment'])) { ?>
                                    <hr/>
                                    <div class="uploadFileListContainer row">   
                                        <?php
                                        for ($i = 0; $i < count($editvar['customer_attachment']); $i++) {
                                            $type = substr($editvar['customer_attachment'][$i]['file_name'], strrpos($editvar['customer_attachment'][$i]['file_name'], '.') + 1);
                                            ?>
                                            <div  class="uploadFileRow" id="file<?php echo $editvar['customer_attachment'][$i]['id']; ?>">
                                                <div class="col-md-6">
                                                    <div class="extnsn-lst">
                                                        <p class="text-mb">
                                                            <?php echo $editvar['customer_attachment'][$i]['file_name']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <!--  <div class="col-md-12">
                           <div class="uploadFileListContainer  uploadFileListContain">   
                           </div>
                         </div>  -->
                        <!-- <div class="col-lg-12 ad_new_reqz text-center">
                            <input type="submit" class=" theme-save-btn" id="request_submit_op" data-count="1" name="request_submit">
                            <input type="submit" class="theme-cancel-btn" id="request_exit_op" value="Save in Draft" name="request_exit">
                        </div> -->
                        <div class="col-md-6">
                            <div class="card_design">
                                <div class="card_wrapper catagry-heading">
                                    <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/search-stock.svg" class="img-responsive"></span>
                                    <span class="file-msg">Search Stock Photos 
                                            <span class="nocolsl">Click here</span>
                                        </span>
                                    <button type="button" class="btn upload_imgeaseBtn" data-toggle="modal" data-target="#uploadModel">
                                        <!--<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cloud.png" class="img-responsive">
                                        <span class="nocolsl">Click Here</span>-->
                                    </button>
                                </div>
                                <p class="text-g">Add stock images to your project request.</p>
                            </div>
                        </div>                

                    </div>
                </section>
                <div class="col-md-12">
                    <div class="uploadFileListContainer  uploadFileListContain">   

                    </div>
                </div>
                <div style="display: flex; justify-content: center;width: 100%;padding-bottom: 15px;">
                    <img class="imgloader_loader" src="">
                </div>
                <div class="col-lg-12 ad_new_reqz text-center">
                    <input type="submit" class=" theme-save-btn" id="request_submit" data-count="1" value="Submit" name="request_submit"/>
                    <input type="submit" class="theme-cancel-btn" id="request_exit" value="Save in Draft" name="request_exit"/>
                </div>

            </div>
        </form>
    </div>
</section>
<button style="display: none;" id="trial_expire" data-toggle="modal" data-target="#TrialExpire">click here</button>
<button style="display: none;" id="Select_PackForUP" data-toggle="modal" data-target="#SelectPackForUP">click here</button>
<!--<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/bootstrap.min.js"></script>-->
<script type="text/javascript">
    $session = "<?php echo isset($_SESSION['click_count']) ? $_SESSION['click_count'] : ''; ?>";
    $without_pay_user = "<?php echo $profile_data[0]['without_pay_user'] ?>";
    $uploadPath = "<?php echo FS_PATH_PUBLIC_UPLOADS ?>";
    $minsample = "<?php echo $minval + 1 ?>";
    $maxsample = "<?php echo $maxval + 1 ?>";
    $countSamples = "<?php echo count($samples);?>";
</script>