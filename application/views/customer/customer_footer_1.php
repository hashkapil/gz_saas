<style>
div#drift-widget-container iframe#drift-widget {
    left: 10px !important;
    bottom: 10px !important;
}
</style>
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#site_tour" id="website_tour" style="display: none;">Open Modal</button>
  <!-- Modal -->
  <div id="site_tour" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" data-interval="false">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <div class="button_next">
            <button type="button" class="close skip_btn" data-dismiss="modal">Skip tour</button>
        </div>
          <div class="sec_header_logo text-center">
            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/front_end/logo.png">
        </div>
        </div>
        <div class="modal-body">
          <div class="thumbnail">
            <div data-interval="false" id="tourCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="screen_shot">
                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/add_project.png">
              </div>
              <div class="title-heading">
                <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                    <h3 class="text-left">Create New Project</h3>
                    <p class="text-left">Click on create project to add details about your design project so a designer can work on designs for your project. you can also save the project to the "Drafts" before submitting so you can come back to it at a later time.</p>
                  </div>
                </div>
              </div>
                    </div>

                    <div class="item">
                        <div class="screen_shot">
                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/priority.png">
              </div>
              <div class="title-heading">
                <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                    <h3 class="text-left">Change Project Priority</h3>
                    <p class="text-left">When your project are In Queue, you can move the priority of them based on your need. If you don't need a project designed any more, you can simply delete it.</p>
                  </div>
                </div>
              </div>
                    </div>
                    <div class="item">
                        <div class="screen_shot">
                <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/send_msg_update.png">
              </div>
              <div class="title-heading">
                <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                    <h3 class="text-left">Send messages for updates</h3>
                    <p class="text-left">Use the message box on the project overview page to update your entire design team on new changes and ask for any project related information. </p>
                  </div>
                </div>
              </div>
                    </div>
                    <div class="item">
                        <div class="screen_shot">
                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/Screenshot.png">
              </div>
              <div class="title-heading">
                <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                    <h3 class="text-left">View designs and make revisions</h3>
                    <p class="text-left">Once you receive a design draft, click on it to view and review the design. if you would like any revision made, simply click on the designer will start working on those revisions.</p>
                  </div>
                </div>
              </div>
                    </div>
                    <div class="item">
                        <div class="screen_shot">
                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/Approved.png">
              </div>
              <div class="title-heading">
                <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                    <h3 class="text-left">Approve design to download files and complete project</h3>
                    <p class="text-left">When you like the design file you have received, all you need to do is click on "Approve" and your files will be available fo you to download and the project will be marked as "Complete". </p>
                  </div>
                </div>
              </div>
                    </div>
                    <div class="next_but text-center">
                      <a class="btn btn-default continue" href="#tourCarousel" data-slide="next"><b>Continue</b></a>
                  </div>
                </div>
            </div>
        </div>
        </div>
      </div>

    </div>
  </div>


     <!--  Copy file to another folder popup modal  -->
      <div class="modal fade slide-3 similar-prop model-close-button in" id="CopYfileToFolder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <header class="fo-rm-header">
                                <h2 class="popup_h2 del-txt">Copy File to Custom folder</h2>
                                <div class="cross_popup" data-dismiss="modal"> x</div>
                              </header>
                              <div class="testemail_section">
                                <div class="email_header_temp_cont"><?php //echo $email_header_footer_sec['header_part'];   ?></div>
                                <form method="POST" action="#" >
                                  <div class="fo-rm-body">
                                    <div class="row">
                                      <input type="hidden" class="form-control input-d from" id="from" name="from" value="">
                                      <input type="hidden" class="form-control input-d folder_id" name="folder_id" value="0">
                                      <input type="hidden"  id="original_file" >
                                      <input type="hidden"  id="copy_folder_path" >
                                      <input type="hidden"  id="req_id">
                                      <input type="hidden"  id="file_id">
                                      <div class="col-sm-12"> 
                                        <div class="text-left head-copy">
                                              <label class="form-group">
                                        <p class="label-txt label-active">Copy your file Under</p>
  
                        </label>
                                         <a class="create_folder btn-g" ><span aria-hidden="true">Create folder</span></a>
                                       </div>
                                     
                  <div class="filetree" id="copy_file_filetree"> 
                    <div class="breadCrumCopYFolder">
                     <a class="folder_nest">/My folder</a> 
                   </div>
                   <span id="eRroSpan"></span>
                   <ul class="main-tree" id="copy_file_mainTree">

                   </ul>
                 </div>
               </div>
             </div>
           </div>
           <div class="form-group goup-x1">
            <div class="cell-col">
              <p class="btn-x copy-create">
                <button type="button" disabled id="copy_file" class="btn btn-g disable text-uppercase">Copy</button>

              </p>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
  <!--link sharing permissions popup-->
<!--  <div class="modal change-parmision" id="change_permissions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content full_height_modal">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body formbody tow-can">
            <form action="javascript:void(0)">
                <input type="hidden" class="shareID" value=""/>
                <div class="">
                        <div class="notify-lines top_name">
                            <p class="user_name"></p>
                            <p class="user_email"></p>
                        </div>
                        <div class="notify-lines">
                            <p>Allow for downloading</p>
                            <div class="switch-custom-usersetting-check">
                                <span class="checkstatus"></span>
                                <input type="checkbox" class="edit_permission" value="download_source_file" name="link_permissions" id="switch_downloading"/>
                                <label for="switch_downloading"></label> 
                            </div>
                        </div>
                        <div class="notify-lines">
                            <p>Mark As Completed</p>
                            <div class="switch-custom-usersetting-check">
                                <span class="checkstatus"></span>
                                <input type="checkbox" class="edit_permission" value="mark_as_completed" name="link_permissions" id="switch_Completed"/>
                                <label for="switch_Completed"></label> 
                            </div>
                        </div>
                        <div class="notify-lines finish-line">
                            <p>Allow Add Comments</p>
                            <div class="switch-custom-usersetting-check">
                                <span class="checkstatus"></span>
                                <input type="checkbox" class="edit_permission" value="comment_revision" name="link_permissions"  id="switch_Comments"/>
                                <label for="switch_Comments"></label> 
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>-->
   <!--end link sharing permissions popup-->
 <!-- jQuery (necessary for JavaScript plugins) -->
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS;?>js/customer/jquery.min.js"></script>
     <script src="<?php echo FS_PATH_PUBLIC_ASSETS;?>js/customer/bootstrap.min.js"></script>
<!--    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/js-card.js"></script>-->
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS;?>plugins/ui-slider/jquery.ui-slider.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/lightgallery-all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script> 
     <script src="https://www.graphicszoo.com/public/assets/js/jquery.fancybox.min.js"></script>
    <script type="text/javascript">
    var total_inprogress_req = '';
    var subusers_reqdata = '';
   <?php //if($profile[0]['tour'] == 0){ ?>
//    $(function(){
//      $('#website_tour').trigger('click');
//    });
  <?php //} ?>
  function sitetour(){
    $.ajax({
        type: "POST",
        url:"<?php echo base_url(); ?>customer/request/user_update_tour",
        data:{
            "tour":1,
        },
      });
      }
      $('#tourCarousel').carousel({
        wrap: false,
        interval: false,
      });
      $('.skip_btn').click(function(){
        $('#tourCarousel').carousel(0);
        sitetour();
      });
      var totalslide = $('#tourCarousel .item').length - 1;
      $('.next_but a').click(function(){
        var currentslide =  $('#tourCarousel .item.active').index();
        if(currentslide == totalslide){
          $('.carousel').carousel(0);
          sitetour();
          $(this).attr('data-dismiss','modal');
          $(this).trigger('click');
        }
      });
   setTimeout(function(){
      $('.alert').fadeOut();
    },2000);
        
    $(document).ready(function () {
            $('#comment_ul li').click(function(){
                $(".pro-ject-rightbar").find('.shared_project_class').hide();
                var id = $(this).attr('id');
                $(".comment_sec").hide();
                $(".public_link").hide();
                $(".shared_draft_class").hide();
                $('.openchat').hide();  
                $('#people_share').attr('data-draftid',id)
                $(".pro-ject-rightbar").find("#"+id).show();
                $(".pro-ject-rightbar").find('#public_'+id).show();
                $(".pro-ject-rightbar").find('.shared_draft_user_'+id).show();
            });
            setTimeout(function(){
                $('#comment_ul .<?php echo $main_id;?>').click();
            },2);
            var total_design1 = <?php echo sizeof($designer_file_count); ?>;
            var ind_id = $('.<?php echo $main_id; ?>').index();
            $(".pagin-one").html(ind_id+1 + " of " + total_design1);
            

            $('.slick_thumbs ul li').click(function(){
            $(".slick_thumbs ul li").find('figure').removeClass('active');
            $(this).find('figure').addClass('active');
            var thumb_index = $(this).index();
            $('ul.slick-dots').find('li:eq(' + thumb_index + ')').trigger('click');
            var dot_length = $('ul.slick-dots').children().length;

            var count_slid = thumb_index;
            var current_design = count_slid;
            var total_design = <?php echo sizeof($designer_file_count); ?>;
            current_design += 1;
            if (current_design > total_design) {
                current_design = 1;
            }

            $(".pagin-one").html(current_design + " of " + total_design);
        });
        // Slick Slider script Start here 
        $('#changepass').click(function(e){
            e.preventDefault();
        });
        var total_design = <?php echo sizeof($designer_file_count); ?>;
        var current_design = 1;
        
        $(".next").click(function () {
            var index = $('#comment_ul li figure.active').closest('.active1').index();
           if(index+1 == total_design){
            $('#comment_ul li:first-child').click();
           }else{
            $('#comment_ul li figure.active').closest('.active1').next().click();
           }
            setTimeout(function(){
                var chat_id = $('#cro .active').attr('id');
                $('.comment_sec').css('display','none');
                $(".pro-ject-rightbar").find("#"+chat_id).show();
                $(".messagediv_"+chat_id).stop().animate({
                  scrollTop: $(".messagediv_"+chat_id)[0].scrollHeight
                }, 1);
            },1000);
        });

        $(".prev").click(function () {
            var index = $('#comment_ul li figure.active').closest('.active1').index();
               if(index == 0){
                $('#comment_ul li:last-child').click();
               }else{
                $('#comment_ul li figure.active').closest('.active1').prev().click();
               }
            setTimeout(function(){
                var chat_id = $('#cro .active').attr('id');
                $('.comment_sec').css('display','none');
                $(".pro-ject-rightbar").find("#"+chat_id).show();
                $(".messagediv_"+chat_id).stop().animate({
                  scrollTop: $(".messagediv_"+chat_id)[0].scrollHeight
                }, 1);
            },1000);
        });
     
   
        var position = 0;

        $('.ui-slider-control')
            .UISlider({
                min: 1,
                max: 101,
                smooth: false,
                value: position
            })
            .on('change thumbmove', function (event, value) {
                var targetPath = $(event.target).data('target');
                $(targetPath).text(value);
                $('#dataaaaa').val(targetPath);
            })
            .on('start', function () {
                $('.value').addClass('editing');
            })
            .on('end', function () {
                $('.value').removeClass('editing');
            });

        $('.value').text(position);


        $('input[type=checkbox]')
        .on('change', function () {

            var name = $(this).prop('name'),
                value = $(this).prop('checked');

            if (name === 'popup') {

                $('.popup-buttons').toggle( value );

            } else {

                $('.ui-slider-control').UISlider( name, value );
            }
        });
    });
    var $fileInput = $('.file-input');
    var $droparea = $('.file-drop-area');

    // highlight drag area
    $fileInput.on('dragenter focus click', function() {
      $droparea.addClass('is-active');
    });

    // back to normal state
    $fileInput.on('dragleave blur drop', function() {
      $droparea.removeClass('is-active');
    });

    // change inner text
    $fileInput.on('change', function() {
      var filesCount = $(this)[0].files.length;
      var $textContainer = $(this).prev();

      if (filesCount === 1) {
        // if single file is selected, show file name
        var fileName = $(this).val().split('\\').pop();
        $textContainer.text(fileName);
      } else {
        // otherwise show number of files
        $textContainer.text(filesCount + ' files selected');
      }
    });

    function validateAndUpload(input){
        var URL = window.URL || window.webkitURL;
        var file = input.files[0];
        if(file){
            console.log(URL.createObjectURL(file));
            $(".imagemain").hide();
            $(".imagemain3").show();
            $("#image3").attr('src', URL.createObjectURL(file));
            $(".dropify-wrapper").show();
        }
    }

    $(document).ready(function(){
        $('.progress-value > span').each(function(){
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            },{
                duration: 1500,
                easing: 'swing',
                step: function (now){
                    $(this).text(Math.ceil(now));
                }
            });
        });
    });
        
//   Hotjar Tracking Code for www.graphicszoo.com 
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:676752,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <!--***************Live Chat Script*************************-->
<?php
if ($login_user_data[0]['user_flag'] != 'client') { ?>
  <script>
    "use strict";
    !function() {
      var t = window.driftt = window.drift = window.driftt || [];
      if (!t.init) {
        if (t.invoked) return void (window.console && console.error && console.error("Drift snippet included twice."));
        t.invoked = !0, t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
        t.factory = function(e) {
          return function() {
            var n = Array.prototype.slice.call(arguments);
            return n.unshift(e), t.push(n), t;
          };
        }, t.methods.forEach(function(e) {
          t[e] = t.factory(e);
        }), t.load = function(t) {
          var e = 3e5, n = Math.ceil(new Date() / e) * e, o = document.createElement("script");
          o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + n + "/" + t + ".js";
          var i = document.getElementsByTagName("script")[0];
          i.parentNode.insertBefore(o, i);
        };
      }
    }();
    drift.SNIPPET_VERSION = '0.3.1';
    drift.load('d6yi38hkhwsd');
  </script>
<?php } else { 
  if (!empty($client_script)) {
        /***Start client script before close head tag***/
        foreach ($client_script as $k => $s_vl) {
            if (($s_vl['show_only_specific_page'] == "cus_portl" || $s_vl['show_only_specific_page'] == "all_pages") && $s_vl["script_position"] == "before_body") {
                if (strpos(base64_decode($s_vl["tracking_script"]), '</script>') !== false) {
                    echo base64_decode($s_vl["tracking_script"]);
                } else {
                    ?>
                    <script>
                    <?php echo base64_decode($s_vl["tracking_script"]); ?>
                    </script>
                    <?php
                }
            }
        }
        /***End client script before close body tag***/
    }
} ?> 
<!--***************End Live Chat Script*************************-->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/js/toastr.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/owl/owl.carousel.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/customer_portal.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/filemanagement.js"></script>
  </body>
</html>
