<!DOCTYPE html>
<?php
$CI = & get_instance();
$CI->load->library('myfunctions');
$reQuestUrl = $this->router->fetch_method();
// echo $show_agency_setting ; die('asd');  
$get_method =$this->router->fetch_method();
//echo $urlopen;  
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=7,9,10">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Language" content="en">
    <meta name="google" content="notranslate">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Customer Portal | <?php echo $page_title; ?></title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700&display=swap&subset=devanagari,latin-ext" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $custom_favicon; ?>">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/reset.css" rel="stylesheet">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/adjust.css" rel="stylesheet">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/editor/editor.css" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'css/customer/style.css'); ?>" rel="stylesheet">
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'css/customer/responsive.css'); ?>" rel="stylesheet">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/jquery.fancybox.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/1.3.1/css/toastr.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!--<link rel="stylesheet" href="<?php //echo FS_PATH_PUBLIC_ASSETS;             ?>js/js-card.css">-->
    <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'css/custom_style_for_all.css'); ?>" rel="stylesheet">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/customer/introjs.css" rel="stylesheet">
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/intro.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/jquery.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/bootstrap.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/jquery.fancybox.min.js"></script>
    <?php
    $url = current_url();
    $u_rl = explode("/", $url);
    $messageurl = substr($url, strrpos($url, '/') + 1);
//    print_R($u_rl);
    $bodyclassondotcomnt = (in_array('project_image_view',$u_rl)) ? 'view_file_body' : '';
    if ($messageurl == 'setting-view' || $messageurl == 'user_setting') {
        ?>
        <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/chosen.css" rel="stylesheet">
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/chosen.jquery.js"></script>
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/init.js"></script>
    <?php } ?>

    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/dot-comment/leader-line.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/dot-comment/anim-event.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/dot-comment/in-view.min.js"></script>
    
    <script>
        var baseUrl = "<?php echo site_url(); ?>";
        $rowperpage = <?php echo LIMIT_CUSTOMER_LIST_COUNT; ?>;
        $assets_path = "<?php echo FS_PATH_PUBLIC_ASSETS ?>";
        $uploadAssestPath = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS ?>";
        $uploadbrandPath = "<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' ?>";
        $uploadcustom_files = "<?php echo FS_PATH_PUBLIC_UPLOADS . 'custom_files/' ?>";
        is_trail = "<?php echo $login_user_data[0]['is_trail']; ?>";
        is_logged_in = "<?php echo $login_user_data[0]['is_logged_in']; ?>";
        is_without_pay_user = "<?php echo $parent_user_data[0]['without_pay_user']; ?>";
        total_active_requests = "<?php echo TOTAL_ACTIVE_REQUEST; ?>";
        jqueryarray = <?php echo json_encode(STATE_TEXAS); ?>;
        subscription_plan = '<?php echo json_encode($all_tier_prices); ?>';
        new_plans = <?php echo NEW_PLANS; ?>;
        $plan_price = '';
        $discount_amount = 0;
        $tax_amount = 0;
        $tax_value = 0;
        $discount_notes = '';
        new_plans = <?php echo NEW_PLANS; ?>;
        var profilepath = "<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>";
        if (subscription_plan) {
            subscription_plan = jQuery.parseJSON(subscription_plan);
        }
        var requestmainchatimg = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTMAINCHATFILES; ?>";
        var requestdraftchatimg = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES; ?>";
    </script>
    <?php
    if ($login_user_data[0]['user_flag'] == 'client' && !empty($client_script)) {
            /***Start client script before close head tag***/
            foreach ($client_script as $k => $s_vl) {
                if (($s_vl['show_only_specific_page'] == "cus_portl" || $s_vl['show_only_specific_page'] == "all_pages") && $s_vl["script_position"] == "before_head") {
                    if (strpos(base64_decode($s_vl["tracking_script"]), '</script>') !== false) {
                        echo base64_decode($s_vl["tracking_script"]);
                    } else {
                        ?>
                        <script>
                    <?php echo base64_decode($s_vl["tracking_script"]); ?>
                        </script>
                        <?php
                    }
                }
            }
            /***End client script before close body tag***/
    }
    $CI->load->library('customfunctions');
    $this->load->view('front_end/variable_css');
    $CI->load->helper('notifications');
    $helper_data = helpr();
    $profile = $helper_data['profile_data'];
    $messagenotifications = $helper_data['messagenotifications'];
    $messagenotification_number = $helper_data['messagenotification_number'];
    $notifications = $helper_data['notifications'];
    $notification_number = $helper_data['notification_number'];
    if (in_array($parent_user_data[0]['plan_name'], AGENCY_USERS) && $_SERVER['HTTP_HOST'] != DOMAIN_NAME) {
        $baseurl = base_url() . "customer/request/design_request";
    } else {
        $baseurl = base_url();
    }
    ?>
</head>

<body class="deshboard-bgnone fixedloading  <?php
if (strpos($url, 'project_image_view') !== false || $popup == '0') {
    echo "no_header_pg ";
}if ($_SESSION['switchtouseracc'] == 1 && $_SESSION['switch_userid'] == $_SESSION['user_id']) {
    echo " loggedinasuser ";
}
 echo $get_method. ' ' .$bodyclassondotcomnt; 
?>" style="background:#f2f2f2;<?php
      if ($_SESSION['switchtouseracc'] == 1 && $_SESSION['switch_userid'] == $_SESSION['user_id'] && strpos($url, 'project_image_view') !== false) {
          echo "padding-top:40px";
      }
      ?>">
          <?php if ($_SESSION['switchtouseracc'] == 1 && $_SESSION['switch_userid'] == $_SESSION['user_id']) { ?>
        <div class="logout-notify">
            <h2>You logged in as : <?php echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name']; ?> </h2>
            <div class="logout-account">
                <a href="<?php echo base_url(); ?>admin/accounts/backtoownaccount/<?php echo $_SESSION['switchfrom_id']; ?>/customer" class="backtoadmin">Exit Access</a>
            </div>
        </div>
    <?php } ?>
    <header class="nav-section <?php
    if ($_SESSION['switchtouseracc'] == 1 && $_SESSION['switch_userid'] == $_SESSION['user_id']) {
        echo "go-down";
    }
    ?>">
        <div class="container-fluid">
            <nav class="navbar navbar-default flex-show lefted">
                <!-- centered -->
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="small-menu" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="custom-logo-link" href="<?php echo $baseurl ?>">
                        <?php
                        $filename = substr(strrchr($custom_logo, "."), 1);
                        if ($filename == 'svg') {
                            $style = 'style="width:100%"';
                        }
                        ?>
                        <img src="<?php echo $custom_logo; ?>" class="img-responsive" >
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse small-menu">
                    <ul class="nav navbar-nav">
                        <li class="">
                            <a href="<?php echo base_url(); ?>customer/request/design_request">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                            <g>
                                <g>
                                    <rect x="5" y="8.2" width="2" height="0.5"/>
                                </g>
                                <g>
                                    <rect x="5" y="11.7" width="2" height="0.5"/>
                                </g>
                                <g>
                                    <rect x="5" y="15.2" width="2" height="0.5"/>
                                </g>
                                <g>
                                    <rect x="9.5" y="9.7" width="6" height="0.5"/>
                                </g>
                                <g>
                                    <rect x="9.5" y="13.7" width="6" height="0.5"/>
                                </g>
                                <g>
                                    <rect x="5.8" y="13.2" width="0.5" height="2.2"/>
                                </g>
                                <g>
                                    <path d="M17,20.3H8c-1.2,0-2.3-1-2.3-2.3v-1.3h0.5V18c0,1,0.8,1.8,1.8,1.8h9c1,0,1.8-0.8,1.8-1.8V6
                                    c0-1-0.8-1.8-1.8-1.8H8C7,4.3,6.3,5,6.3,6v2.5H5.8V6c0-1.2,1-2.3,2.3-2.3h9c1.2,0,2.3,1,2.3,2.3v12C19.2,19.2,18.2,20.3,17,20.3z"
                                    />
                                </g>
                                <g>
                                    <rect x="5.8" y="9.7" width="0.5" height="2.2"/>
                                </g>
                            </g>
                        </svg>
                        <span>Projects</span>
                        <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <?php if ($canuseradd_brand_pro != 0) { ?>
                            <li>
                                <a href="<?php echo base_url(); ?>customer/brand_profiles">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                        <g>
                                        <g>
                                        <g>
                                        <path d="M12,3.7C14.5,6,17.4,7,18.5,7.3v5.8c0,2.3-1.2,4.5-3.2,5.6L12,20.4l-3.3-1.9c-2-1.1-3.2-3.2-3.2-5.6V7.4
                                              C6.5,7.1,9.5,6,12,3.7 M12,3C9,6,5,7,5,7v6c0,2.5,1.3,4.8,3.5,6l3.5,2l3.5-1.9c2.2-1.2,3.5-3.5,3.5-6V6.9C19,6.9,15,6,12,3L12,3z
                                              "/>
                                        </g>
                                        </g>
                                        <g>
                                        <path d="M12,12.3c-1.2,0-2.3-1-2.3-2.3s1-2.3,2.3-2.3s2.3,1,2.3,2.3S13.2,12.3,12,12.3z M12,8.3
                                              c-1,0-1.8,0.8-1.8,1.8s0.8,1.8,1.8,1.8s1.8-0.8,1.8-1.8S13,8.3,12,8.3z"/>
                                        </g>
                                        <g>
                                        <path d="M14,16.3h-4c-0.7,0-1.3-0.5-1.3-1.3v-1c0-0.8,0.2-1.4,0.7-2l0.4,0.3c-0.4,0.5-0.5,1-0.5,1.6v1
                                              c0,0.5,0.3,0.8,0.8,0.8h4c0.5,0,0.8-0.3,0.8-0.8v-1c0-0.6-0.2-1.2-0.5-1.6l0.4-0.3c0.4,0.5,0.7,1.2,0.7,2v1
                                              C15.3,15.7,14.7,16.3,14,16.3z"/>
                                        </g>
                                        </g>
                                        </svg>
                                        <span>Brand Profiles</span> 
                                        <span class="sr-only">(current)</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if ($login_user_data[0]['parent_id'] == 0) { ?>
                            <li>
                                <a href="<?php echo base_url(); ?>customer/setting-view#management">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                        <g>
                                        <g>
                                        <path d="M11.9,10.8c-1.9,0-3.5-1.6-3.5-3.5s1.6-3.5,3.5-3.5s3.5,1.6,3.5,3.5S13.8,10.8,11.9,10.8z M11.9,4.3
                                              c-1.7,0-3,1.4-3,3s1.4,3,3,3s3-1.4,3-3S13.6,4.3,11.9,4.3z"/>
                                        </g>
                                        <g>
                                        <path d="M14.4,20.3H9.5c-1.2,0-2.2-1-2.2-2.2v-4.5c0-1.8,1.5-3.3,3.3-3.3h2.7c1.8,0,3.3,1.5,3.3,3.3V18
                                              C16.6,19.2,15.6,20.3,14.4,20.3z M10.6,10.7c-1.6,0-2.8,1.3-2.8,2.8V18c0,1,0.8,1.7,1.7,1.7h4.9c1,0,1.7-0.8,1.7-1.7v-4.5
                                              c0-1.6-1.3-2.8-2.8-2.8H10.6z"/>
                                        </g>
                                        <g>
                                        <path d="M6.5,18.1H6.1c-1.2,0-2.2-1-2.2-2.2V13c0-1.3,1-2.3,2.3-2.3c0,0,0,0,0,0c-0.8-0.6-1.3-1.6-1.3-2.6
                                              c0-1.8,1.4-3.2,3.2-3.2v0.5c-1.5,0-2.7,1.2-2.7,2.7c0,0.9,0.4,1.7,1.1,2.2c0.3,0.2,0.3,0.4,0.3,0.6c-0.1,0.2-0.3,0.3-0.5,0.3
                                              c-1,0-1.8,0.8-1.8,1.8v2.8c0,1,0.8,1.7,1.7,1.7h0.4V18.1z"/>
                                        </g>
                                        <g>
                                        <path d="M17.9,18.1h-0.5v-0.5h0.5c1,0,1.7-0.8,1.7-1.7V13c0-1-0.8-1.8-1.8-1.8c-0.2,0-0.4-0.1-0.5-0.3
                                              c-0.1-0.2,0-0.4,0.1-0.6c0.7-0.6,1.1-1.4,1.1-2.2c0-1.5-1.2-2.7-2.7-2.7V4.8c1.8,0,3.2,1.4,3.2,3.2c0,1-0.4,1.9-1.3,2.6
                                              c0,0,0,0,0,0c1.3,0,2.4,1.1,2.4,2.3v2.8C20.2,17.1,19.2,18.1,17.9,18.1z"/>
                                        </g>
                                        </g>
                                        </svg>
                                        <span>Teams</span>
                                        <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <?php if($login_user_data[0]['is_affiliated'] != 0){ ?>
                                <li class="<?php
                                if ($messageurl == 'affiliate') {
                                    echo "active";
                                    } else {
                                        echo "";
                                    }
                            ?>">
                                 <a href="<?php echo base_url(); ?>customer/affiliate">
                                 <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" enable-background="new 0 0 512 512" xml:space="preserve">
                                <path d="M378.5,150.9h1.4c15,0,27.1-12.2,27.1-27.1V111c0-15-12.2-27.1-27.1-27.1h-1.4c-15,0-27.1,12.2-27.1,27.1v12.8
                                    C351.4,138.8,363.5,150.9,378.5,150.9z M361.4,111c0-9.4,7.7-17.1,17.1-17.1h1.4c9.4,0,17.1,7.7,17.1,17.1v12.8
                                    c0,9.4-7.7,17.1-17.1,17.1h-1.4c-9.4,0-17.1-7.7-17.1-17.1V111z"></path>
                                <path d="M461.8,138.7c0-45.6-37-82.6-82.6-82.7c-44.4,0-80.9,35.1-82.6,79.6l-110.6,57.3c-34.9-29.5-87-25.1-116.4,9.8
                                    s-25.1,87,9.8,116.4c32.4,27.4,80.2,25.8,110.7-3.7L296.7,377c2,45.6,40.7,80.9,86.2,78.9c45.6-2,80.9-40.7,78.9-86.2
                                    c-1.9-42.2-35.3-76.2-77.5-78.8v-69.6C427.8,218.5,461.8,182.4,461.8,138.7z M296.9,365.5l-99.9-57.7c25.7-31.8,24.3-77.5-3.3-107.6
                                    L297,146.8c4,40.4,36.7,71.9,77.2,74.4v69.6C333.6,293.3,300.8,325,296.9,365.5z M399.5,159.8c9.8,1.8,17,10.3,17,20.3V201
                                    c-23,13.8-51.7,13.8-74.6,0v-20.9c0.1-10.3,7.6-19,17.8-20.4c10.1,11,27.3,11.7,38.3,1.6C398.5,160.8,399,160.3,399.5,159.8
                                    L399.5,159.8z M379.2,66.1c40.1,0,72.6,32.5,72.6,72.5c0,21.1-9.2,41.2-25.2,55v-13.6c0-16.2-12.6-29.6-28.7-30.6
                                    c-2.7-0.2-4,1.5-5,2.6c-6,7.3-16.7,8.4-24,2.4c-1.1-0.9-2.1-1.9-2.9-3.1c-1-1.3-2.5-2.1-4.2-2c-16.6,0.5-29.8,14.1-29.9,30.7v13.6
                                    c-30.4-26.1-33.8-72-7.7-102.3C338,75.3,358.1,66.1,379.2,66.1z M95.5,318.2v-20.9c0.1-10.3,7.6-19,17.8-20.4
                                    c10.1,11,27.3,11.7,38.3,1.6c0.5-0.5,1-1,1.5-1.5c9.8,1.8,17,10.3,17,20.3v20.9C147.1,332,118.4,332,95.5,318.2L95.5,318.2z
                                    M180.1,311v-13.6c0-16.2-12.6-29.6-28.7-30.6c-2.6-0.2-4,1.5-5,2.6c-6,7.3-16.7,8.4-24,2.4c-1.1-0.9-2.1-1.9-2.9-3.1
                                    c-1-1.3-2.5-2.1-4.2-2c-16.6,0.5-29.8,14.1-29.9,30.7V311c-30.4-26.2-33.8-72-7.7-102.4c26.2-30.4,72-33.8,102.4-7.7
                                    s33.8,72,7.7,102.4C185.4,306.1,182.9,308.6,180.1,311z M341.9,435.5v-20.9c0.1-10.3,7.6-19,17.8-20.4c10.1,11,27.3,11.7,38.3,1.6
                                    c0.5-0.5,1-1,1.5-1.5c9.8,1.8,17,10.3,17,20.3v20.9C393.6,449.3,364.9,449.3,341.9,435.5L341.9,435.5z M451.8,373.3
                                    c0,21.1-9.2,41.2-25.2,55v-13.6c0-16.2-12.6-29.6-28.7-30.6c-2.7-0.2-4,1.5-5,2.6c-6,7.3-16.7,8.4-24,2.4c-1.1-0.9-2.1-1.9-2.9-3.1
                                    c-1-1.3-2.5-2.1-4.2-2c-16.6,0.5-29.8,14.1-29.9,30.7v13.6c-30.4-26.2-33.8-72-7.6-102.4c26.2-30.4,72-33.8,102.4-7.6
                                    C442.6,332.1,451.8,352.1,451.8,373.3z"></path>
                                <path d="M351.4,345.6v12.8c0,15,12.2,27.1,27.1,27.1h1.4c15,0,27.1-12.2,27.1-27.1v-12.8c0-15-12.2-27.1-27.1-27.1h-1.4
                                    C363.5,318.4,351.4,330.6,351.4,345.6z M397,345.6v12.8c0,9.4-7.7,17.1-17.1,17.1h-1.4c-9.4,0-17.1-7.7-17.1-17.1v-12.8
                                    c0-9.4,7.7-17.1,17.1-17.1h1.4C389.3,328.5,397,336.1,397,345.6z"></path>
                                <path d="M133.5,201.1h-1.4c-15,0-27.1,12.2-27.1,27.1V241c0,15,12.2,27.1,27.1,27.1h1.4c15,0,27.1-12.2,27.1-27.1v-12.8
                                    C160.6,213.3,148.4,201.2,133.5,201.1z M150.6,241c0,9.4-7.7,17.1-17.1,17.1h-1.4c-9.4,0-17.1-7.7-17.1-17.1v-12.8
                                    c0-9.4,7.7-17.1,17.1-17.1h1.4c9.4,0,17.1,7.7,17.1,17.1V241z"></path>
                                </svg>
                                <span>Affiliate</span>
                                <span class="sr-only">(current)</span>
                            </a>
                                <?php } ?>
                            </li>
                        <?php } ?>
                        <li class="<?php
                        if ($messageurl == 'filemanagement') {
                            echo "active";
                        } else {
                            echo "";
                        }
                        ?>" id="filemanagement">
                            <a href="<?php echo base_url(); ?>customer/filemanagement">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                <g>
                                <path d="M17.3,18.3H6.1c-1.2,0-2.3-1-2.3-2.3V7c0-0.7,0.5-1.3,1.3-1.3h3.5c0.5,0,0.8,0.3,1,0.5l0.4,0.5h8
                                      c0.6,0,1.2,0.3,1.7,0.7c0.4,0.5,0.6,1.1,0.6,1.8l-0.6,7C19.4,17.3,18.4,18.3,17.3,18.3z M5.1,6.3C4.6,6.3,4.3,6.5,4.3,7v9
                                      c0,1,0.8,1.8,1.8,1.8h11.2c0.9,0,1.7-0.7,1.8-1.6l0.6-7c0-0.5-0.1-1-0.5-1.4c-0.3-0.4-0.8-0.6-1.3-0.6h-7.6L11,8.4
                                      c0.2,0.2,0.4,0.4,0.6,0.4h4.5c0.7,0,1.3,0.5,1.3,1.3v5h-0.5v-5c0-0.5-0.3-0.8-0.8-0.8h-4.5c-0.4,0-0.8-0.2-1-0.6L9.2,6.5
                                      C9.1,6.4,8.8,6.3,8.6,6.3H5.1z"/>
                                </g>
                                </svg>
                                <span>Files</span>

                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <?php 
                        
                        //"test".$show_agency_setting;
                        if ($show_agency_setting == 1) { ?>
                            <li class="<?php
                            if ($messageurl == 'client_management' || $messageurl == 'user_setting') {
                                echo "active";
                            } else {
                                echo "";
                            }
                            ?>">
                                <a href="<?php echo base_url(); ?>customer/user_setting">
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                                    <g>
                                    <g>
                                    <path d="M6.4,19.2H4.1c-0.1,0-0.3-0.1-0.3-0.3v-2.3c0-0.1,0.1-0.3,0.3-0.3h0.6l2.6-2.6c0.1-0.1,0.3-0.1,0.4,0
                                          l1.6,1.6c0,0,0.1,0.1,0.1,0.2s0,0.1-0.1,0.2l-2.6,2.6V19C6.7,19.1,6.6,19.2,6.4,19.2z M4.3,18.7h1.8v-0.5c0-0.1,0-0.1,0.1-0.2
                                          l2.5-2.5l-1.3-1.3L5,16.8c0,0-0.1,0.1-0.2,0.1H4.3V18.7z"/>
                                    </g>
                                    <g>
                                    <path d="M11.7,17.4c-0.7,0-1.3-0.3-1.8-0.8l-3.4-3.4c-1-1-1-2.6,0-3.6l1.8-1.8l7,7l-1.8,1.8
                                          C13,17.1,12.4,17.4,11.7,17.4z M8.3,8.4L6.8,9.9C6,10.7,6,12,6.8,12.8l3.4,3.4c0.8,0.8,2.1,0.8,2.9,0l1.5-1.5L8.3,8.4z"/>
                                    </g>
                                    <g>
                                    <path d="M15,15.1l-7-7l2.9-2.9c0.5-0.5,1.2-0.5,1.7-0.1l7.8,4.7L15,15.1z M8.6,8.1l6.3,6.3l4.5-4.5l-7.2-4.4
                                          c-0.4-0.3-0.8-0.2-1.1,0.1L8.6,8.1z"/>
                                    </g>
                                    <g>

                                    <rect x="16.2" y="7.9" transform="matrix(0.6923 0.7216 -0.7216 0.6923 12.1653 -8.8799)" width="0.5" height="3.9"/>
                                    </g>
                                    <g>

                                    <rect x="14.5" y="6.8" transform="matrix(0.7071 0.7071 -0.7071 0.7071 10.2716 -7.9335)" width="0.5" height="3.3"/>
                                    </g>
                                    <g>

                                    <rect x="12.7" y="5.7" transform="matrix(0.7264 0.6873 -0.6873 0.7264 8.3759 -6.9602)" width="0.5" height="2.8"/>
                                    </g>
                                    </g>
                                    </svg>
                                    <span>White Label</span>
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php //} ?>
                    </ul>
                </div><!-- /.navbar-collapse -->
                <div class="box-setting">
                    <ul id="right_nav" class="list-unstyled list-setting">
                        <!-- Message Section -->
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <span class="mess-box">
                                    <i class="icon-gz_message_icon"></i>
                                    <?php
                                    if (isset($messagenotifications)) {
                                        if (sizeof($messagenotifications) > 0) {
                                            ?>
                                            <span class="numcircle-box dot-notify">
                                            </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                </span>
                            </a>
                            <div class="dropdown-menu">
                                <h3 class="head-notifications">Messages</h3>
                                <ul class="list-unstyled list-notificate">
                                    <?php if (sizeof($messagenotifications) > 0) { ?>
                                        <?php
                                        for ($i = 0; $i < sizeof($messagenotifications); $i++) {
                                            $messagetitle = strip_tags($messagenotifications[$i]['title']);
                                            ?>
                                            <li>
                                                <a href="<?php echo base_url() . $messagenotifications[$i]['url']; ?>">
                                                    <div class="setnoti-fication">
                                                        <figure class="pro-circle-k1">
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive msg">
                                                        </figure>
                                                        <div class="notifitext">
                                                            <p class="ntifittext-z1">
                                                                <strong title="<?php echo $messagenotifications[$i]['heading']; ?>">
                                                                    <?php
                                                                    if (strlen($messagenotifications[$i]['heading']) > 20) {
                                                                        echo substr($messagenotifications[$i]['heading'], 0, 15) . "";
                                                                    } else {
                                                                        echo $messagenotifications[$i]['heading'];
                                                                    }
                                                                    ?>
                                                                </strong>
                                                                <span class="d-time">
                                                                    <?php
                                                                    $approve_date = $messagenotifications[$i]['created'];
                                                                    $msgdate = $CI->myfunctions->onlytimezoneforall($approve_date);
                                                                    echo $msgdate;
                                                                    ?>

                                                                </span>
                                                            </p>
                                                            <p class="ntifittext-z2">
                                                                <span>
                                                                    <?php
                                                                    if (strlen($messagetitle) >= 50) {
                                                                        echo substr($messagetitle, 0, 40) . "..";
                                                                    } else {
                                                                        echo $messagetitle;
                                                                    }
                                                                    ?>
                                                                </span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        <?php }
                                        ?>
                                    <?php } else { ?>
                                        <li style="padding:10px;">No new message</li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </li>
                        <!-- Message Section End -->
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <span class="bell-box">
                                    <i class="icon-gz_bell_icon"></i>
                                    <?php
                                    if (isset($notifications)) {

                                        if (sizeof($notifications) > 0) {
                                            ?>
                                            <span class="numcircle-box dot-notify">
                                            </span>
                                            <?php
                                        }
                                    }
                                    ?>
                                </span>
                            </a>
                            <div class="dropdown-menu ">
                                <h3 class="head-notifications">Notifications</h3>
                                <ul class="list-unstyled list-notificate">
                                    <?php
                                    if (sizeof($notifications) > 0) {
                                        ?>
                                        <?php for ($i = 0; $i < sizeof($notifications); $i++) { ?>
                                            <li>
                                                <a href="<?php echo base_url() . $notifications[$i]['url']; ?>">
                                                    <div class="setnoti-fication">
                                                        <figure class="pro-circle-k1">
                                                            <img src="<?php echo $notifications[$i]['profile_picture']; ?>" class="img-responsive">
                                                        </figure>
                                                        <div>
                                                            <div class="notifitext" title="<?php echo $notifications[$i]['title']; ?>">
                                                                <p class="ntifittext-z1">
                                                                    <strong>
                                                                        <?php echo $notifications[$i]['first_name'] . " " . $notifications[$i]['last_name']; ?>
                                                                    </strong>
                                                                    <span class="d-time">
                                                                        <?php
                                                                        $approve_date = $notifications[$i]['created'];
                                                                        $notidate = $CI->myfunctions->onlytimezoneforall($approve_date);
                                                                        echo $notidate;
                                                                        ?>

                                                                    </span>
                                                                </p>
                                                            </div>
                                                            <p class="ntifittext-z2">
               <!--                                                <strong>
                                                                <?php //echo $notifications[$i]['req_title'];  ?>
                                                               </strong>-->
                                                                <span>
                                                                    <?php echo $notifications[$i]['title']; ?></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <li style="padding:10px;">No New notification</li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="#" class="login-droup" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="client-ndtl">
                                    <p class="client_name">
                                        <?php echo $profile[0]['first_name'] . " " . $profile[0]['last_name']; ?>
                                    </p>
                                    <p class="client_role">
                                        <?php echo $profile[0]['role']; ?>
                                    </p>
                                </div>
                                <div class="client-prfl">
                                    <figure class="pro-circle-img-xxs">
                                        <img src="<?php echo $profile[0]['profile_picture']; ?>" class="img-responsive">
                                    </figure>
                                    <span>
                                        <i class="fas fa-caret-down"></i>
                                    </span>
                                </div>
                            </a>
                            <div class="dropdown-menu pull-right profile-dropdown" aria-labelledby="dropdownMenuButton">
                                <h3 class="head-notifications">
                                    <?php echo $profile[0]['first_name'] . " " . $profile[0]['last_name']; ?>
                                    <span class="mail_text"> <?php echo $profile[0]['email']; ?></span>
                                </h3>
                                <div class="profile_wrapper">
                                    <ul class="list-unstyled list-notificate">
                                        <li>
                                            <a href="<?php echo base_url(); ?>customer/setting-view"><i class="fas fa-cog"></i> Settings</a>
                                        </li>
                                        <?php if($profile[0]['is_affiliated'] == 0 && $profile[0]['parent_id'] == 0) { ?>
<!--                                        <li>
                                            <a href="<?php //echo base_url(); ?>customer/request/affiliate_instruction"><i class="fas fa-sign-out-alt"></i> Become Affiliate</a>
                                        </li>-->
                                        <?php } ?>
                                        <li>
                                            <a href="<?php echo base_url(); ?>customer/request/log_out"><i class="fas fa-sign-out-alt"></i> Logout</a>
                                        </li>
                                         <li onclick="check_activeClass();">
                                            <a><i class="fas fa-anchor"></i> Site Tour</a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <?php if ($popup != '0') { ?>
        <div class="upgrdpopup_ontop <?php
        if (strpos($url, 'project-info') !== false) {
            echo "project_info_popup_up";
        }
        ?>"> 
                 <?php if ($parent_user_data[0]['is_trail'] == 1) { ?>
                <div class="container-fluid upgrade-hdr-p header_popup">
                    <div class="custom_container">
                        <div class="row">
                            <div class="col-sm-12 upgrade-inside-p">
                                <h2>
                                    <i class="fa fa-clock"></i>
                                    Your Free Account is limited to 1 free design request. To submit more requests, upgrade your plan now!
                                </h2>
                                <div class="upgrade-list">
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item">
                                            <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>customer/request/upgrADE_account" class="Select_PackageForUP upgrade f_upgrade-link">UPGRADE</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } elseif ($parent_user_data[0]['without_pay_user'] == 1) {
                ?>
                <div class="container-fluid upgrade-hdr-p header_popup">
                    <div class="custom_container">
                        <div class="row">
                            <div class="col-lg-12 upgrade-inside-p">
                                <h2>
                                    <i class="fa fa-clock"></i>
                                    Your Free Account is showing dummy design request. To submit orignal requests, upgrade your plan now!
                                </h2>
                                <div class="upgrade-list">
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item">
                                            <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>customer/request/upgrADE_account" class="Select_PackageForUP upgrade f_upgrade-link">UPGRADE</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else if ($usermessage != "") { ?>
                <div class="container-fluid upgrade-hdr-p header_popup">
                    <div class="custom_container">
                        <div class="row">
                            <div class="col-lg-12 upgrade-inside-p">
                                <h2>
                                    <i class="fa fa-clock"></i>
                                    <?php echo $usermessage; ?>
                                </h2>
                                <?php if ($parent_cancel_subscription == 1 && ($login_user_data[0]['parent_id'] == 0 || ($login_user_data[0]['user_flag'] == "client" && $login_user_plan[0]["payment_mode"] != 0) || ($login_user_data[0]['user_flag'] == 'client' && $login_user_plan[0]["payment_mode"] == 0 && $agency_info[0]['show_billing'] == 1))) { ?>
                                    <div class="upgrade-list">
                                        <ul class="list-unstyled list-inline">
                                            <li class="list-inline-item">
                                                <a href="javascript:void(0);" class="reactivate_plan f_upgrade-link">Click Here to Reactivate</a>
                                            </li>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }  ?>
        </div>
    <?php } 
