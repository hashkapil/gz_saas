<div class="flex-grid">
    <div class="price-card">
        <div class="chooseplan f_main_clnt">
            <h2><?php echo $userplan['plan_name']; ?></h2>
            <h3><font>$</font><?php echo $agency_priceclass; ?></h3>
            <div class="p-benifite">
                <div class="price-details">
                    <?php
                        $amount = 'amount';
                    $options = "";
                    foreach ($userplan['tier_prices'] as $tier_prices) {
                        $options .= '<option value ="' . $tier_prices['quantity'] . '" data-amount="' . $tier_prices[$amount] . '" data-annual_price="' . $tier_prices["annual_price"] . '">' . $tier_prices['quantity'] . ' Dedicated Designer</option>';
                    }
                    $features = str_replace('{{QUANTTY}}', $options, $userplan['features']);
                    echo $features;
                    ?>
                </div>
            </div>
            <div class="price-sign-up">
                <?php if ($userplan['info']['target'] != '' && $userplan['info']['target'] == 'cantaddsubs') { ?>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#<?php echo $userplan['info']['target']; ?>" data-value="<?php echo $userplan['plan_id']; ?>" data-assignreq="<?php echo $userplan['requests']['main_inprogress_req']; ?>" data-alrdyassignreq="<?php echo $userplan['requests']['addedsbrqcount']; ?>" data-pndngrq="<?php echo $userplan['requests']['pending_req']; ?>" type="button" class="<?php echo $userplan['info']['class']; ?> ud-dat-p <?php echo $agencyclass; ?>">
                        <?php echo $userplan['info']['text']; ?>
                    </a>
                <?php } else if ($userplan['info']['target'] != '') { ?>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#<?php echo $userplan['info']['target']; ?>" data-value="<?php echo $userplan['plan_id']; ?>" data-price="<?php echo $userplan['plan_price']; ?>" data-inprogress="<?php echo $userplan['global_inprogress_request']; ?>" data-display_name="<?php echo $userplan['plan_name']; ?>" data-clientid="<?php echo $data[0]['id']; ?>" data-applycoupn="<?php echo $userplan['apply_coupon']; ?>"  type="button" class="<?php echo $userplan['info']['class']; ?> ud-dat-p <?php echo $agencyclass; ?>">
                        <?php echo $userplan['info']['text']; ?>
                    </a>
                <?php } else { ?>
                    <a href="javascript:void(0)" data-value="<?php echo $userplan['plan_id']; ?>" data-price="<?php echo $userplan['plan_price']; ?>" data-inprogress="<?php echo $userplan['global_inprogress_request']; ?>" data-display_name="<?php echo $userplan['plan_name']; ?>" data-clientid="<?php echo $data[0]['id']; ?>" data-applycoupn="<?php echo $userplan['apply_coupon']; ?>"  type="button" class="<?php echo $userplan['info']['class']; ?> ud-dat-p <?php echo $agencyclass; ?>">
                        <?php echo $userplan['info']['text']; ?>
                    </a> 
                <?php } ?>
            </div>
            <div class="active-check" style="display: none"><i class="fas fa-check"></i></div>
        </div>
    </div>
</div>