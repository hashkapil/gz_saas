<div class="white-boundries">
    <div class="headerWithBtn">
        <h2 class="main-info-heading">My Payment History</h2>
    </div>
    <div class="avail-amount" style="text-align:right;">
        <h2><span>Available:</span><?php echo '$'.$totalavailablecomm; ?></h2>
    </div>
    <div id="no-more-tables">
        <div class="managment-list">
            <?php if($payment_history){?>
            <table>
                <tbody>
                    <?php 
//                    echo "<pre/>";print_R($payment_history);exit;
                    foreach($payment_history as $pkey => $pval){ ?>
                    <tr>
                        <td class="name"><span><?php echo (isset($pval['name']) && trim($pval['name']) != '') ? $pval['name'] : 'N/A';?></span></td>
                        <td><span><?php echo (isset($pval['email']) && $pval['email'] != '') ? $pval['email'] : 'N/A';?></span></td>
                        <td><span><?php echo '$'.$pval['com_amnt'];?></span></td>
                        <td><span><?php echo ucfirst($pval['type']);?></span></td>
                        <td><span><?php echo ($pval['type'] == 'credit') ? $pval['signup_date'] : $pval['debited_date'];?></span></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php }else{
                echo "<div class='no_record_found'>No Data Found</div>";
            } ?>
        </div>
    </div>
</div>