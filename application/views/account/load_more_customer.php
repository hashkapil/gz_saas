<!-- Pro Box Start -->
<?php
//echo "grid"."<br/>";
//echo "<pre/>";print_r($projects);
?>
<!--<a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $projects['id']; ?>/1">-->
<div class="col-xs-6 col-md-3 col-sm-6">
    <div class="pro-productss pro-box-r1">
        <div class="img-status">

            <div class="drafted_img">
                <div class="simple-option1">
                    <?php
                    //echo "<pre/>";print_R($canuserdel);
                    if ($trial[0]['is_trail'] != 1) {
                        ?>
                        <a href="javascript:void(0)" class="show_trans" data-id="<?php echo $projects['id']; ?>" data-url="<?php echo base_url(); ?>customer/request/deleteproject/<?php echo $projects['id']; ?>" class="reddelete">
                            <i class="fas fa-ellipsis-v"></i></a>
                    <?php } ?>
                    <div class="transcopy" id="trans_<?php echo $projects['id']; ?>" style="display:none">
                        <?php if ($projects['status'] != "approved" && $parent_user_data[0]['is_cancel_subscription'] != 1) { ?>
                            <a href="<?php echo base_url(); ?>customer/request/add_new_request?reqid=<?php echo $projects['id']; ?>">
                                <i class="icon-gz_edit_icon" aria-hidden="true"></i> Edit
                             </a>
                        <?php } if ($canuserdel != 0) { ?>
                            <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>customer/request/deleteproject/<?php echo $projects['id']; ?>" class="reddelete">
                                <i class="icon-gz_delete_icon" aria-hidden="true"></i> Delete</a>
                            <?php
                        }
                        if ($canuseradd != 0) {
                            ?>
                            <a href="<?php echo base_url(); ?>customer/request/add_new_request?did=<?php echo $projects['id']; ?>">
                                <i class="fa fa-copy"></i>Duplicate</a>
                            <?php
                        }
                        if ($projects['status'] != 'cancel' && $projects['dummy_request'] == 0 && $change_project_status != 0) {
                            ?>
                            <a href="<?php echo base_url(); ?>customer/request/cancelproject/<?php echo $projects['id']; ?>">
                                <i class="far fa-window-close"></i>Cancel</a>
                            <?php
                        }
                        if ($parent_user_data[0]['is_cancel_subscription'] != 1 && $projects['status'] == "cancel" && $change_project_status != 0) {
                            ?> 
                            <a class="cancel_subs" href="<?php echo base_url() ?>customer/request/markAsInProgress/<?php echo $projects['id']; ?>/yes"><i class="fas fa-arrow-left"></i>Move to progress</a>
                        <?php } ?>
                    </div>
                </div>
                <a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $projects['id']; ?>/1">
                    <?php
                    if (!empty($projects['latest_draft'])) {
                        $path = FS_PATH . FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $projects['id'] . "/_thumb/" . $projects['latest_draft'];
                        if (file_get_contents($path)) {
                            ?>
                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $projects['id'] . "/_thumb/" . $projects['latest_draft']; ?>" class="img-responsive draft_img"/>
                        <?php } else { ?>
                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $projects['id'] . "/" . $projects['latest_draft']; ?>" class="img-responsive draft_img"/>
                            <?php
                        }
                    } else {
                        ?>


                        <div class="thumbnail_perview">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_project_thumbnail.svg" class="img-responsive thumbnail_icon"/>
                        </div>

                    <?php } ?>
                </a>
            </div>

            <a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $projects['id']; ?>/1">

                <div class="pro-inrightbox">
                    <?php if ($projects['status'] == "active" || $projects['status'] == "checkforapprove" || $projects['status'] == "disapprove" || $projects['status'] == "hold") { ?>
                        <p class="neft">
                            <span class="status-btn <?php echo $projects['customer_variables']['color']; ?> ">
                                <?php echo $projects['customer_variables']['status']; ?>
                            </span>
                        </p>
                    <?php } elseif ($projects['status'] == "assign") { ?>
                        <div class="">
                            <p class="neft">
                                <span class="status-btn  <?php echo $projects['customer_variables']['color']; ?>">
                                    <?php echo $projects['customer_variables']['status']; ?>
                                </span>
                            </p>
                        </div>
                    <?php } elseif ($projects['status'] == "draft") { ?>
                        <p class="neft">
                            <span class="status-btn <?php echo $projects['customer_variables']['color']; ?> ">
                                <?php echo $projects['status']; ?>
                            </span>
                        </p>
                    <?php } elseif ($projects['status'] == "hold") { ?>
                        <p class="neft">
                            <span class="status-btn <?php echo $projects['customer_variables']['color']; ?> ">
                                <?php echo $projects['status']; ?>
                            </span>
                        </p>
                    <?php } elseif ($projects['status'] == "cancel") { ?>
                        <p class="neft">
                            <span class="status-btn <?php echo $projects['customer_variables']['color']; ?> ">
                                <?php echo $projects['status']; ?>
                            </span>
                        </p>
                    <?php } else { ?>
                        <p class="neft">
                            <span class="status-btn <?php echo $projects['customer_variables']['color']; ?> ">
                                <?php echo "Completed"; ?>
                            </span>
                        </p>
                    <?php } ?>
                </div>
            </a>
            <?php if(!empty($projects['client_name'])) { ?>
                     <h2>Added By : <span><?php echo $projects['client_name'][0]['first_name'].' '.$projects['client_name'][0]['last_name']; ?></span></h2>
            <?php } ?>
        </div>
        <div class="pro-box-r2">
            <a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $projects['id']; ?>/1">
                <?php
                if (strlen($projects['title']) > 45) {
                    $name = substr($projects['title'], 0, 45) . " ...";
                } else {
                    $name = $projects['title'];
                }
                ?>
                <h3 title="<?php echo $projects['title']; ?>"><?php echo $name; ?></h3>
                
            </a>
            <div class="pro-inbox-r1">
                <div class="pro-inleftbox">
                    <p class="pro-a">
                        <?php
                        if ($projects['status'] == "active") {
                            echo "Expected on";
                        } elseif ($projects['status'] == "checkforapprove") {
                            echo "Delivered on";
                        } elseif ($projects['status'] == "disapprove") {
                            echo "Expected on";
                        } elseif ($projects['status'] == "assign") {
                            echo "Created on";
                        } elseif ($projects['status'] == "draft") {
                            echo "Created on";
                        } elseif ($projects['status'] == "approved") {
                            echo "Approved on";
                        } elseif ($projects['status'] == "cancel") {
                            echo "Cancelled on";
                        } elseif ($projects['status'] == "hold") {
                            echo "Hold on";
                        }
                        ?>
                        <span  class="date_bold">
                            <?php
                            if ($projects['status'] == "active") {
                                echo date('m/d/Y', strtotime($projects['expected']));
                            } elseif ($projects['status'] == "checkforapprove") {
                                echo date('m/d/Y', strtotime($projects['deliverydate']));
                            } elseif ($projects['status'] == "disapprove") {
                                echo date('m/d/Y', strtotime($projects['expected']));
                            } elseif ($projects['status'] == "assign") {
                                echo date('m/d/Y', strtotime($projects['created']));
                            } elseif ($projects['status'] == "draft") {
                                echo date('m/d/Y', strtotime($projects['created']));
                            } elseif ($projects['status'] == "approved") {
                                echo date('m/d/Y', strtotime($projects['approvddate']));
                            } elseif ($projects['status'] == "cancel" || $projects['status'] == "hold") {
                                echo date('m/d/Y', strtotime($projects['modified']));
                            }
                            ?>
                        </span>
                    </p>
                    <?php if ($projects['status'] != 'assign') { ?>
                        <div class="pro-inleftbox">

                            <p class="pro-a inline-per">
                                <a href="<?php echo base_url() ?>customer/request/project-info/<?php echo $projects['id']; ?>/1" style="position: relative;">
                                    <span class="inline-imgsssx">
                                        <i class="icon-gz_message_icon"></i>
                                        <?php if ($projects['total_chat'] + $projects['comment_count'] != 0) {
                                            ?>
                                            <!-- class="numcircle-box count_chatmsg" -->
                                            <span>
                                                <?php echo $projects['total_chat'] + $projects['comment_count']; ?>
                                            </span>
                                        <?php } ?>

                                    </span>
                                </a>
                            </p>
                        </div>
                    <?php } ?>
                    <?php if ($projects['status'] == 'active' || $projects['status'] == 'disapprove' || $projects['status'] == 'checkforapprove') { ?>
                            <!--                <p class="green">Active </p>-->
                    <?php } elseif ($projects['status'] == 'assign') { ?>
                        <p class="select-pro-a pro-a" data-toggle="tooltip" title="Priority" >
                            <!--Priority -->
                            <?php
//                            if ($projects['max_priority'][0]['priority'] || $projects['subuser_max_priority'][0]['sub_user_priority']) {
                                $priorities_array = $projects['max_priority'];
                                $subpriorities_array = $projects['subuser_max_priority'];
//                            } else {
//                                //echo "activeTab".$activeTab; 
//                                $priorities_array = $activeTab;
//                                $subpriorities_array = $activeTab;
//                            }
//                                echo "priorities_array".$priorities_array;
//                                echo "activeTab".$activeTab;
                            if ($projects['customer_id'] !== $login_user_id && $canaccessallbrands == 0 && $can_manage_priorities == 1 && $projects['created_by'] == $login_user_id && $login_user_data[0]['user_flag'] == 'client') {
                                ?>
                                <select class="prior" onchange="prioritize(this.value, <?php echo $projects['sub_user_priority'] ?>,<?php echo $projects['id'] ?>, 'client')">
                                    <?php for ($j = 1; $j <= $subpriorities_array; $j++) { ?>
                                        <option value="<?php echo $j; ?>" <?php echo ($j == $projects['sub_user_priority']) ? "selected" : ""; ?>><?php echo $j; ?></option>                                             
                                    <?php } ?>
                                </select><?php
                            } else if ($projects['customer_id'] !== $login_user_id && ($canaccessallbrands == 0 || $can_manage_priorities == 0)) {
                                echo 'Priority : ' . $projects['priority'];
                            } else { //echo "elsepri";
                                ?>
                                <select class="prior" onchange="prioritize(this.value, <?php echo $projects['priority'] ?>,<?php echo $projects['id'] ?>)">
                                    <?php for ($j = 1; $j <= $priorities_array; $j++) { ?>
                                        <option value="<?php echo $j; ?>" <?php echo ($j == $projects['priority']) ? "selected" : ""; ?>><?php echo $j; ?></option>                                              
                                    <?php } ?>
                                </select><?php }
                                ?>
                        </p>
                    <?php } ?>
                </div>

            </div>
        </div>

    </div>
</div> 
<!--        </a>-->






