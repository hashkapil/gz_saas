<style type="text/css" media="screen">
   header.nav-section {
   display: none;
   }
   .mydivposition {
   cursor: crosshair;
   padding: 20px;
   display: inline-block;
   width: auto;
   margin: auto;
   float: none;
   transform-origin: top;
   }
   body, .project-info-page .side_bar {
   padding-top: 0px; 
   }
   
   .posone1,.posone2{
   cursor: move;
   }
   svg.leader-line {
   z-index: 999;
   /*position: absolute;*/
   }
   svg.leader-line use {
   stroke: #dc223c;
   }
   .custom-color use{
   fill:#dc223c !important;
   }
   .hidenotch:after{
      opacity: 0 !important;
   }
   .MarkasFav {
   display: inline-flex;
   padding: 0px;
   margin: 0px;
   }
   .MarkasFav .click{
   position: relative;
   }
   .MarkasFav .CoPyOrMove img {
   width: 45%;
   cursor: pointer;
   }
   div#CopYfileToFolder,div#DirectorySave {
   margin-top: 40px;
   }
   .fa-heart-o:before {
   content: "\f004";
   }
   span.cstm_span.fa-heart.fas {
   color: #f90500;
   }
   
    
   /* line Drawer Css start from herer  */
   /* line Drawer Css End here  */
   
</style>
<?php 
$_SESSION['designerpic'] = $user[0]['profile_picture']; 
$url = current_url();
$latestdraftid = substr($url, strrpos($url, '/') + 1);
//echo "<pre/>";print_R($_SESSION);exit; 
?>
<div class="dot-header">
    <span id="OpenpopUp" data-open="0"></span>
   <span id="project-pageinfo-id"></span>
    <div class="">
        <div class="flex-headers">
            <p class="savecols-col left">
                <a  href="<?php echo base_url();?>account/request/project-info/<?php echo $designer_file[0]['request_id']; ?>" class="back-link-xx0"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive"> Back</a>
            </p>
            <div class="middle-tital">
                <div class="zoom-btns">
                    <a id="zoomOut" href="javascript:void(0)" data-zoomoutcount='0'><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/icon-zoom-out.svg" class="img-responsive"></a>
                    <a id="zoomIn" href="javascript:void(0)" data-count='0'><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/icon-zoom-in.svg" class="img-responsive"></a>
                </div>
                <div class="despeginate-box">
                    <a class="despage prev" href="#myCarousel" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
                    <span class="file_title"><?php echo $designer_file[0]['file_name']; ?></span>
                    <a class="despage next" href="#myCarousel" data-slide="next"><i class="fas fa-chevron-right"></i></a>
                </div>
                <?php 
                //print_R($allrole);
                if($allroles['user'] != 'designer') { ?>
                <div class="fev_copy_msg">
                    <div class="click MarkasFavClick"  id="" data-id="" data-requested-id="">
                        <span class="cstm_span far fa-heart"  title="<?php echo $markAs; ?>">
                            <svg data-toggle="tooltip" data-placement="bottom" title="Add to Favorites" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve">
                                <g>
                                    <path d="M475.7,164.6C469.2,92,417.7,39.3,353.3,39.3c-42.8,0-82.1,23.1-104.2,60.1c-21.9-37.5-59.5-60.1-101.7-60.1
                                          c-64.3,0-115.7,52.7-122.4,125.4c-0.5,3.2-2.7,20.1,3.9,47.6C38.3,252,60,288.1,91.5,316.8l157.5,142.9l160.2-142.9
                                          c31.5-28.6,53.2-64.7,62.6-104.5C478.4,184.7,476.3,167.8,475.7,164.6z"></path>
                                    <path fill="none" stroke="#000000" stroke-width="24" stroke-miterlimit="10" d="M485,162.2c-6.8-75.6-60.5-130.6-127.4-130.6
                                          c-44.6,0-85.5,24-108.5,62.6c-22.8-39.1-62-62.6-105.8-62.6c-67,0-120.5,54.8-127.4,130.6c-0.5,3.3-2.8,20.9,4,49.6
                                          c9.7,41.4,32.3,78.9,65.2,108.8l164,148.8l166.8-148.8c32.8-29.8,55.4-67.4,65.2-108.8C487.8,183.1,485.6,165.5,485,162.2z"></path>
                                </g>
                            </svg>
                        </span>
                    </div>
                    <div class="CoPyOrMove" data-id=""  title="Copy File" data-file="" data-projid="">
                        <svg data-toggle="tooltip" data-placement="bottom" title="Copy" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve"> <g id="Text-files"><path d="M421.7,71.4h-31c-0.6,0-1.2,0.2-1.8,0.4V39.2c0-21.6-18-39.2-40.1-39.2H79.8C57.7,0,39.7,17.6,39.7,39.2v366
                            c0,21.6,18,39.2,40.1,39.2H127v17.7c0,20.9,17.3,37.8,38.6,37.8h256.1c21.3,0,38.6-17,38.6-37.8V109.3
                            C460.3,88.4,443,71.4,421.7,71.4z M55.6,405.2v-366c0-12.9,10.9-23.4,24.3-23.4h268.9c13.4,0,24.3,10.5,24.3,23.4v366
                            c0,12.9-10.9,23.4-24.3,23.4H79.8C66.4,428.6,55.6,418.1,55.6,405.2z M444.4,462.2c0,12.1-10.2,22-22.7,22H165.6
                            c-12.5,0-22.7-9.9-22.7-22v-17.7h205.9c22.1,0,40.1-17.6,40.1-39.2V86.9c0.6,0.1,1.2,0.4,1.8,0.4h31c12.5,0,22.7,9.9,22.7,22V462.2
                            z"></path></g></svg>
                    </div>
                </div>
                <?php } ?>
                <div class="sidebar-toggle">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/toggle.svg" class="img-responsive">
                </div>
            </div> 
            <div class="down-approv" data-step="11" data-intro="Click approve to complete the project and download the files at any time." data-position='right' data-scrollTo='tooltip'> 
            <?php 
                for ($i = 0; $i < sizeof($designer_file); $i++) { ?>
                <div class="hdr-buttons butt_<?php echo $designer_file[$i]['id'];?>"
                        <?php
                        if ($main_id == $designer_file[$i]['id']) {
                            echo "style='display: block;'";
                        } else {
                            echo "style='display:none;'";
                        }
                        ?>>
                   <?php 
                    if($allroles['user'] == 'customer'){
                   if (($designer_file[$i]['status'] == "customerreview" || $designer_file[$i]['status'] == "askforrevision") && $main_user_data[0]['is_cancel_subscription'] != 1) {
                        ?>
                        <div class="" id="approved_id">
                            <div class="buttons_quality">
                                <div id="status">
                                    <p class="not-approve-project">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-check-ap.svg" class="img-responsive"><?php
                                        if ($designer_file[$i]['status'] == "Approve") {
                                            echo "Approved";
                                        }
                                        ?>
                                        <?php if ($canapprove_file !== 0) { ?>
                                            <input type="button" class="darkblackbackground whitetext weight600 response_request" style="border: 0px;padding: 0px;" value="Approve" data-fileid="<?php echo $designer_file[$i]['id']; ?>" data-file="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" data-requestid="<?php echo $designer_file[$i]['request_id']; ?>" data-status="approved" data-approve-date="<?php echo date("M d, Y h:i A"); ?>" />
                                        <?php } ?>

                                    </p>
                                </div>
                                <form action="<?php echo base_url(); ?>account/request/downloadzip" method="post">
                                    <input type="hidden" name="request_id" value="<?php echo $_GET['id']; ?>">
                                    <input type="hidden" name="draft_id" value="<?php echo $designer_file[$i]['id']; ?>">
                                    <input type="hidden" name="srcfile" value="<?php echo "design_requests/public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>">
                                    <input type="hidden" name="prefile" value="<?php echo "design_requests/public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>">
                                    <input type="hidden" name="project_name" value="<?php echo $data[0]['title']; ?>">
                                    <?php //if ($user[0]['is_trail'] == '0' || ($user[0]['is_trail'] !== '1' && $data[0]['status'] != 'approved' && $candownload_file !== 0)) {
                                    if($user[0]['is_trail'] == '0' && $candownload_file !== 0){ ?>
                                        <button type="submit" name="submit"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/download-form.svg" class="img-responsive"><span>Download</span></button>    
                                    <?php } ?>
                                </form>
                            </div>
                        </div>
                    <?php } 
                    else { ?>
                        <div class="" data-step="11" data-intro="Click approve to complete the project and download the files at any time." data-position='right' data-scrollTo='tooltip'>
                            <div class="buttons_quality">
                                <div id="status">
                                    <p><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-check-ap.svg" class="img-responsive"><?php
                                    if ($designer_file[$i]['status'] == "Approve") {
                                        echo "Approved";
                                    }
                                    ?></p>
                                </div>
                                <?php if ($main_user_data[0]['is_cancel_subscription'] != 1 || $request[0]['status'] == "approved") { ?>
                                    <form action="<?php echo base_url(); ?>account/request/downloadzip" method="post">
                                        <input type="hidden" name="request_id" value="<?php echo $_GET['id']; ?>">
                                        <input type="hidden" name="draft_id" value="<?php echo $designer_file[$i]['id']; ?>">
                                        <input type="hidden" name="srcfile" value="<?php echo "public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>">
                                        <input type="hidden" name="prefile" value="<?php echo "public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>">
                                        <input type="hidden" name="project_name" value="<?php echo $data[0]['title']; ?>">
                                        <?php
                                        if ($user[0]['is_trail'] != 1) {
                                            if ($candownload_file !== 0) {
                                                ?>
                                                <button type="submit" name="submit"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/download-form.svg" class="img-responsive"> <span>Download</span></button>
                                                <?php
                                            }
                                        } elseif ($user[0]['is_trail'] == 1 && $user[0]['can_trialdownload'] == 1) {
                                            if ($candownload_file !== 0) {
                                                ?>
                                                <button type="submit" name="submit"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/download-form.svg" class="img-responsive"><span> Download</span></button>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </form>
                                <?php } ?>

                            </div>
                        </div>
                    <?php } ?>
                    <?php }
                    elseif($allroles['user'] == 's_user'){ ?> 
                    <div class="">
                    <div class="buttons_quality">
                        <?php 
                        //echo $designer_file[$i]['status'];
                        if ($designer_file[$i]['status'] == "pending") { ?>
<!--                        <div id="status">-->
                        <?php if($approve_reject != 0){ ?>
                        <p class="btn-x desi-gned notapproved">
                            <input type="button" class="btn-c greenbtn approve response_request_by_admin" value="Quality Pass" data-fileid="<?php echo $designer_file[$i]['id'];?>" data-file="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" data-status="customerreview"  data-requestid="<?php echo $_GET['id'];?>" data-approve-date="<?php echo date("M d, Y h:i A");?>">
                            <input type="button" class="btn-c redbtn approve response_request_by_admin" value="Quality Fail" data-fileid="<?php echo $designer_file[$i]['id'];?>" data-file="<?php echo base_url() . "uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" data-status="Reject"  data-requestid="<?php echo $_GET['id'];?>"> 
                        </p>
                        <?php } ?>
                        <p class="qua-litypas-sed approved app_quanlity_psd" style="display: none;"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-check-ap.svg" class="img-responsive"> Quality Passed </p>
                            <h3 class="rejected notapprove rej_desgn_prjct" style="display: none;">NOT APPROVED</h3>
<!--                        </div>-->
                    <?php } elseif ($designer_file[$i]['status'] == "Reject") { ?>
                        <div id="status">
                        <h3 class="rejected">Not Approved</h3>
                        </div>
                    <?php } elseif ($designer_file[$i]['status'] == "customerreview") { ?>
                        <div id="status">
                        <p class="qua-litypas-sed approved"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-check-ap.svg" class="img-responsive"> Quality Passed</p>
                       </div>
                    <?php } elseif ($designer_file[$i]['status'] == "Approve") { ?>
                        <div id="status">
                        <p class="qua-litypas-sed approved"> <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-check-ap.svg" class="img-responsive">Approved</p>
                        </div>
                    <?php } ?>
                    <div class="approvedate admin-del">  
                    <div class="downld_del_sec">
                        <?php if ($candownload_file !== 0) { ?>
                        <form action="<?php echo base_url(); ?>account/request/downloadzip" method="post">
                            <input type="hidden" name="srcfile" value="<?php echo "design_requests/public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>">
                            <input type="hidden" name="prefile" value="<?php echo "design_requests/public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>">
                            <input type="hidden" name="project_name" value="<?php echo isset($data[0]['title']) ? $data[0]['title'] : 'Graphicszoo'; ?>">
                            <button type="submit" name="submit" style="border:none"><i class="fa fa-download" aria-hidden="true"></i></button>
                        </form>
                        <?php } ?>
                        <form action="<?php echo base_url(); ?>account/request/gz_delete_files" method="post">
                            <input type="hidden" name="draft_count" value="<?php echo $designer_file[$i]['draft_count']; ?>">
                            <input type="hidden" name="srcfile" value="<?php echo "design_requests/public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>">
                            <input type="hidden" name="prefile" value="<?php echo "design_requests/public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>">
                            <input type="hidden" name="thumb_prefile" value="<?php echo "design_requests/public/uploads/requests/" . $_GET['id'] . "/_thumb/" . $designer_file[$i]['file_name']; ?>">
                            <input type="hidden" name="project_name" value="<?php echo $data[0]['title']; ?>">
                            <input type="hidden" name="requests_files" value="<?php echo $designer_file[$i]['id']; ?>">
                            <input type="hidden" name="request_id" value="<?php echo $_GET['id']; ?>">
                            <button type="submit" name="gz_delete_files" onclick="return cnfrm_delete()" style="border:none"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </form>
<!--                        <button class="nav-menu-itm" type="button" name="bars" style="border:none"><i class="fa fa-bars" aria-hidden="true"></i></button>-->
                    </div>
                </div>
            </div>
            </div> 
                    <?php }
                    else{ ?> 
                        <div class="buttons_quality">
                             <div class="status-bttn">  
                                 <?php if ($designer_file[$i]['status'] == "pending") { ?>
                                     <h3 class="btn-d bluetext">Pending</h3>
                                 <?php } elseif ($designer_file[$i]['status'] == "Reject") { ?>
                                     <h3 class="btn-d holdcolor rejected">Rejected</h3>
                                 <?php } elseif ($designer_file[$i]['status'] == "Approve") { ?>
                                     <h3 class="btn-d green notappchanges">Approved</h3>
                                 <?php } ?>
                             </div>
                             <div id="date_approved" class="approvedate  admin-del"> 
                                 <div class="downld_del_sec">
                                     <?php if ($candownload_file !== 0) { ?>
                                     <form action="<?php echo base_url(); ?>/account/request/downloadzip" method="post">
                                         <input type="hidden" name="srcfile" value="<?php echo "design_requests/public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>">
                                         <input type="hidden" name="prefile" value="<?php echo "design_requests/public/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>">
                                         <input type="hidden" name="project_name" value="<?php echo $data[0]['title']; ?>">
                                         <button type="submit" name="submit" style="border:none"><i class="fa fa-download" aria-hidden="true"></i></button>
                                     </form>
                                     <?php } ?>
                                     <form action="<?php echo base_url(); ?>/designer/request/gz_delete_files" method="post">
                                         <input type="hidden" name="srcfile" value="<?php echo "design_requests/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['src_file']; ?>">
                                         <input type="hidden" name="draft_count" value="<?php echo $designer_file[$i]['draft_count']; ?>">
                                         <input type="hidden" name="prefile" value="<?php echo "design_requests/uploads/requests/" . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>">
                                         <input type="hidden" name="thumb_prefile" value="<?php echo "design_requests/uploads/requests/" . $_GET['id'] . "/_thumb/" . $designer_file[$i]['file_name']; ?>">
                                         <input type="hidden" name="project_name" value="<?php echo $data[0]['title']; ?>">
                                         <input type="hidden" name="requests_files" value="<?php echo $designer_file[$i]['id']; ?>">
                                         <input type="hidden" name="request_id" value="<?php echo $_GET['id']; ?>">
                                         <button type="submit" name="gz_delete_files" onclick="return cnfrm_delete()" style="border:none"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                     </form>
                                 </div>
                             </div>
                         </div>
                    <?php } ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
</div>
<div class="main-three-outer">
    <?php //echo "<pre/>";print_r($designer_file);?>
    <div class="left-slide-sidebar">
        <div class="pro-ject-leftbar two-can">
            <div class="orb-xbin slick_thumbs">
                <ul class="list-unstyled orbxbin-list" id="comment_ul">
                    <?php
                    $j = 0;
                    for ($i = 0; $i < sizeof($designer_file); $i++) {
                        if($allroles['user'] == 'customer') {
                        $ap = $designer_file[$i]['status'];
                        if (($ap != "pending") && ($ap != "Reject")) {
                            ?>
                            <li data-target="#myCarousel" data-slide-to="<?php echo $j; ?>" class="active1 <?php echo $designer_file[$i]['id']; ?>" id="<?php echo $designer_file[$i]['id']; ?>" request_id="<?php echo $_GET['id']; ?>">
                                <figure class="imgobxbins <?php
                                if ($designer_file[$i]['id'] == $main_id) {
                                    echo 'active';
                                }
                                ?>" style="background: #fff;">
                                <?php
                                $type = substr($designer_file[$i]['file_name'], strrpos($designer_file[$i]['file_name'], '.') + 1);
                                ?>
                                <img src="<?php echo imageUrl_SAAS($type, $_GET['id'], $designer_file[$i]['file_name']); ?>" class="img-responsive" />
                            </figure>
                        </li>
                        <?php
                        $j++;
                    }
                }else { ?>
                    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="active1 <?php echo $designer_file[$i]['id']; ?>" id="<?php echo $designer_file[$i]['id']; ?>">
                        <figure class="imgobxbins leftnav_<?php echo $designer_file[$i]['id']; ?>" style="background-color:#fff;">
                        <?php
                            $type = substr($designer_file[$i]['file_name'], strrpos($designer_file[$i]['file_name'], '.') + 1);
                        ?>
                        <img src="<?php echo imageUrl_SAAS($type, $_GET['id'], $designer_file[$i]['file_name']); ?>" class="img-responsive">
                        </figure>
                    </li>
                   <?php } } ?>
            </ul>
        </div>
    </div>
</div>
<div class="pro-ject-wrapper dot-comment">
    <div class="view-commentBox">
            <div class="row">
            <div class="col-md-12">
                <div id="myCarousel" class="carousel fade-carousel carousel-fade slide" data-ride="carousel" data-interval="false">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" id="cro" style="z-index: 99;overflow: unset;">
                        <?php
                        for ($i = 0; $i < sizeof($designer_file); $i++) {
                            $ap = $designer_file[$i]['status'];
                            if($allroles['user'] == 'customer'){
                            if (($ap != "pending") && ($ap != "Reject")) {
                                ?>
                                <div class="item slides two-can outer-post <?php if ($designer_file[$i]['id'] == $main_id) { echo ' active';}?>" id="<?php echo $designer_file[$i]['id']; ?>" data-name="<?php echo $designer_file[$i]['file_name']; ?>">
                                    <div class="customer_loader loading_<?php echo $designer_file[$i]['id']; ?>" style="display:none;">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/gz_customer_loader.gif">
                                     </div>
                                    <div class="col-md-12 mydivposition mydivposition_<?php echo $designer_file[$i]['id']; ?>" <?php if ($cancomment_on_req != 0) { ?> onmouseout="hideclicktocomment();" onmouseover="showclicktocomment(event,<?php echo $designer_file[$i]['id']; ?>);" onmousemove="showclicktocomment(event,<?php echo $designer_file[$i]['id']; ?>);" onclick="<?php if($main_user_data[0]['is_cancel_subscription'] != 1){ ?>printMousePos(event,<?php echo $designer_file[$i]['id']; ?>); <?php } ?>" id="mydivposition_<?php echo $designer_file[$i]['id']; ?>"<?php } ?>>
                                        <div class="loader loading_<?php echo $designer_file[$i]['id']; ?>" style="display:none;">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif">
                                        </div>
                                        <div class="slid" data-id="<?php echo $designer_file[$i]['id']; ?>">
                                            <?php
                                            $type = substr($designer_file[$i]['file_name'], strrpos($designer_file[$i]['file_name'], '.') + 1);
                                            if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                ?>
                                                <img class="img-responsive" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" style="/*max-height:calc(100vh - 80px);*/margin: auto;display: block;webkit-filter:blur(8px);filter:blur(8px)"/>
                                                <p class="extension_name"><?php echo $type; ?></p>
                                            <?php } else { ?>
                                                <img class="img-responsive" src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS_SAAS . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" style="/*max-height:calc(100vh - 80px);*/margin: auto;display: block;/*-webkit-filter:blur(8px);filter:blur(8px)*/"/>
                                            <?php }
                                            ?>
                                        </div>
                                        <div class="appenddot_<?php echo $designer_file[$i]['id']; ?> remove_dot">
                                            <?php
                                            $total = array_diff(array_column($designer_file[$i]['chat'], 'xco'), ['']);
                                            $counttotaldotleft = count(array_values($total));
                                            for ($j = 0; $j < sizeof($designer_file[$i]['chat']); $j++) {
                                                if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                                    $background = "pinkbackground";
                                                } else {
                                                    $background = "bluebackground";
                                                }
                                                ?>
                                                <?php
                                                if ($designer_file[$i]['chat'][$j]['xco'] != "") { ?>
                                                    <div id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" class="abcabcabc" style="display:none;position:absolute;opacity:0;width:30px;z-index:0;height:30px;" data-dot="<?php echo $counttotaldotleft; ?>" data-left="<?php echo $designer_file[$i]['chat'][$j]['xco']; ?>" data-top="<?php echo $designer_file[$i]['chat'][$j]['yco']; ?>" onclick='show_customer_chat(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, event,$(this));'>
                                                        <div class="customer_chatimage1 beatHeart customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"  data-step="12" data-intro="Use these dots to add revision comments for your designer with pinpoint accuracy." data-position='right' data-scrollTo='tooltip'><span class="numberdot" id="dotnum_<?php echo $designer_file[$i]['id']; ?>"><?php echo $counttotaldotleft; ?></span></div>
                                                        <div style="display:none;" class="customer_chatimage2 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"></div> 
                                                    </div>
                                                    <?php 
                                                    //echo "<pre/>";print_R($designer_file);
                                                if (is_numeric($designer_file[$i]['chat'][$j]['xco'])) { ?>
                                                    <div class="customer_chat customer_chat<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none;position:absolute;left:<?php echo $designer_file[$i]['chat'][$j]['xco'] - 3; ?>px;top:<?php echo $designer_file[$i]['chat'][$j]['yco'] + 25; ?>px;padding: 5px;border-radius: 5px;">
                <div class="posone2" style="z-index: 99999999999999999999999;">
                    <div class="posonclick-add">
                        <div class="posone2abs arrowdowns"  id="dotnum_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>_Next"  onclick="return avoidClick(event);" >
                            <a href="javascript:void(0)" onclick="return closeChatBox(event);" class="crossblink"><i class="fa fa-times"></i></a>
                            <div class="comment-box-es new-patrn">
                                <div class="cmmbx-col left">
                                    <div class="cmmbxcir">
                                        <figure class="pro-circle-img-xxs t1">
                                            <img src="<?php echo $designer_file[$i]['chat'][$j]['profile_picture']; ?>">
                                        </figure>
                                    </div>
                                    <div class='new_msg_des'>
                                        <div class="ryt_header">
                                            <span class="cmmbxtxt">
                                                <?php echo isset($designer_file[$i]['chat'][$j]['first_name'])?$designer_file[$i]['chat'][$j]['first_name']:$designer_file[$i]['chat'][$j]['customer_name']; ?>
                                            </span>
                                            <span class="kevtxt" title='<?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?>'><?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?></span>
                                        </div>
                                        <pre><p class="distxt-cmm distxt-cmm_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></p></pre>
                                            <div class="edit_del">
                                                <a href="javascript:void(0)" onclick="replyButton(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, false)" class="reply_to_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><i class="fas fa-reply-all"></i></a>
                                            <?php if ($_SESSION['user_id'] == $designer_file[$i]['chat'][$j]['sender_id']) { ?>    
                                                <a href="javascript:void(0)" onclick='editMessages(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, false)'  class="edit_to_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><i class="icon-gz_edit_icon"></i></a> 
                                                <a href="javascrip:void(0)" onclick="deleteMsg(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>);" class="del_msg"><i class="icon-gz_delete_icon"></i></a>                                                                               
                                            <?php } ?>
                                            </div>
                                            <div class="msgk-mn-edit comment-submit_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none">
                                                <form action="" method="post">
                                                    <textarea class="pstcmm sendtext" id="editdot_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, 'left')" data-id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>">Save</a> <a href="javascript:void(0)" class="cancel" onclick="cancelButton(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>)">Cancel</a>
                                                </form>
                                            </div>

                                    </div>
                                </div>
                                <div class="allreplyy two-can">
                                <?php
                                foreach ($designer_file[$i]['chat'][$j]['replies'] as $key => $val) { ?>
                                    <div class="cmmbx-col left reply reply_<?php echo $val['id']; ?>">
                                        <div class="cmmbxcir">
                                            <figure class="pro-circle-img-xxs t5">
                                                <img src="<?php echo $val['profile_picture']; ?>">
                                            </figure>
                                        </div>
                                        <div class="msg_sect">
                                            <div class="name_edit_del">
                                               <span class="cmmbxtxt"><?php echo isset($val['first_name'])?$val['first_name']:$val['customer_name']; ?></span>
                                               <pre><p class="distxt-cmm distxt-cmm_<?php echo $val['id']; ?>"><?php echo $val['message']; ?></p></pre>
                                               <?php if ($_SESSION['user_id'] == $val['sender_id']) { ?> 
                                                <div class="edit_del">
                                                    <a href="javascript:void(0)" onclick='editMessages(<?php echo $val['id']; ?>, false)' class="edit_to_<?php echo $val['id']; ?>"><i class="icon-gz_edit_icon"></i></a>
                                                    <a href="javascrip:void(0)" onclick="deleteMsg(<?php echo $val['id']; ?>);" class="del_msg"><i class="icon-gz_delete_icon"></i></a>                                                                               
                                                </div>
                                            <?php } ?>
                                            <div class="msgk-mn-edit comment-submit_<?php echo $val['id']; ?>" style="display:none">
                                                <form action="" method="post">
                                                    <textarea class="pstcmm sendtext" id="editdot_<?php echo $val['id']; ?>"><?php echo $val['message']; ?></textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(<?php echo $val['id']; ?>, 'left')" data-id="<?php echo $val['id']; ?>">Save</a> <a href="javascript:void(0)" onclick="cancelButton(<?php echo $val['id']; ?>)" class="cancel">Cancel</a>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                                </div>
                            <div class="closeoncross reply_msg_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none">
                                <form action="" method="post">
                                    <div class="cmmtype-row wide_text">
                                        <textarea class="pstcmm sendtext text1_<?php echo $designer_file[$i]['id']; ?>" id="text1_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" Placeholder="Post Comment" autocomplete="off" data-reqid="<?php echo $designer_file[$i]['id']; ?>"></textarea>
                                        <span class="cmmsend">
                                            <a class="cmmsendbtn send_comment_without_img send_<?php echo $designer_file[$i]['id']; ?>"
                                                onclick="send_comment_without_img('<?php echo $designer_file[$i]['id'] ?>', 'customer', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>', 'leftreply', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>');">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive">
                                            </a>
                                        </span>
                                    </div>
                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <?php
            }
            }
            ?>
            <?php
            if (is_numeric($designer_file[$i]['chat'][$j]['xco'])) {
                $counttotaldotleft--;
            }
        }
        ?>
        </div>
        <div class="openchat" style="width:35%;position:absolute;display:none;padding:10px 20px;">
            
        <div class="posone1">
            <div class="posonclick-add">
                <div class="posone3abs arrowdowns" id="dot-drag-<?php echo $designer_file[$i]['id']; ?>">
                    <div class="cmmbxpost">
                        <p class="leave-commt">Post a comment</p>
                        <textarea type="text" placeholder="Write Comment here" class="tff-con text_write text_<?php echo $designer_file[$i]['id']; ?>" onclick="wideText(this)" placeholder="Write Comment here"></textarea>
                    </div>
                    <div class="postrow12">
                        <span class="pstcancel"><a class="mycancellable" href="javascript:void(0);">Cancel</a></span>
                        <span class="pstbtns">
                          <button class="pstbtns-a send_request_img_chat send_text send_text<?php echo $designer_file[$i]['id']; ?>" data-fileid="<?php echo $designer_file[$i]['id']; ?>"
                          data-senderrole="customer"
                          data-senderid="<?php echo $_SESSION['user_id']; ?>"
                          data-receiverid="<?php echo $request[0]['designer_id']; ?>"
                          data-receiverrole="designer"
                          data-designerpic="<?php echo $user[0]['profile_picture']; ?>"
                          data-customername="<?php echo $_SESSION['first_name']; ?>"
                          data-xco="" data-yco="" data-request_id="<?php echo $data[0]['id']; ?>">Send</button></span>
                    </div>
                  </div>
              </div>
          </div> 
        </div>   
        <div class="showclicktocomment" style="display:none;">
          Click To Comment
      </div>
    </div>
            <div class="overlay_comment1"></div>
            </div>
        <?php
    }
    }
                        elseif($allroles['user'] == 's_user'){ ?> 
                        <div class="item slides two-can outer-post <?php if ($designer_file[$i]['id'] == $main_id) { echo 'active';}?>" id="<?php echo $designer_file[$i]['id']; ?>" data-name="<?php echo $designer_file[$i]['file_name']; ?>">
                                <div class="customer_loader loading_<?php echo $designer_file[$i]['id']; ?>" style="display:none;">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/gz_customer_loader.gif">
                                </div>
                                <div class="col-md-12 mydivposition mydivposition_<?php echo $designer_file[$i]['id']; ?>"  onclick="printMousePos(event,<?php echo $designer_file[$i]['id']; ?>);" id="mydivposition_<?php echo $designer_file[$i]['id']; ?>" onmouseout="hideclicktocomment();" onmouseover="showclicktocomment(event,<?php echo $designer_file[$i]['id']; ?>);" onmousemove="showclicktocomment(event,<?php echo $designer_file[$i]['id']; ?>);" >
                                    <div class="gz ajax_loader loader loading_<?php echo $designer_file[$i]['id']; ?>" style="display:none;">
                                       <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif">
                                    </div>
                                    <div class="slid" data-id="<?php echo $designer_file[$i]['id']; ?>">
                                        <?php
                                        $type = substr($designer_file[$i]['file_name'], strrpos($designer_file[$i]['file_name'], '.') + 1);
                                        if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                            ?>
                                            <img class="img-responsive" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" style="/*max-height:calc(100vh - 80px);*/margin: auto;display: block;"/>
                                            <p class="extension_name"><?php echo $type; ?></p>
                                        <?php } else { ?>
                                            <img class="img-responsive" src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS_SAAS . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" style="/*webkit-filter:blur(8px);filter:blur(8px);*/" />
                                        <?php }
                                        ?>
                                    </div>
                                    <!-- Chat toolkit start-->
                                    <div class="appenddot_<?php echo $designer_file[$i]['id']; ?> remove_dot DotAppenD">
                                        <?php
                                        $total = array_diff(array_column($designer_file[$i]['chat'],'xco'),['']);
                                        $counttotaldotleft = count(array_values($total));
                                        for ($j = 0; $j < sizeof($designer_file[$i]['chat']); $j++) {
                                            if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                                $background = "pinkbackground";
                                            } else {
                                                $background = "bluebackground";
                                            }
                                            ?>
                                            <?php
                                            if ($designer_file[$i]['chat'][$j]['xco'] != "") {
                                                if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                                    $image1 = "dot_image1.png";
                                                    $image2 = "dot_image2.png";
                                                } else {
                                                    $image1 = "designer_image1.png";
                                                    $image2 = "designer_image2.png";
                                                }
                                                ?>
                                                <?php if (is_numeric($designer_file[$i]['chat'][$j]['xco'])) {
                                                    
                                                     //echo $counttotaldotleft;
                                                    ?>
                                                    <div id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" class="abcabcabc" style="display:none;position:absolute;opacity:0;width:30px;z-index:0;height:30px;" data-dot="<?php echo $counttotaldotleft; ?>" data-left="<?php echo $designer_file[$i]['chat'][$j]['xco']; ?>" data-top="<?php echo $designer_file[$i]['chat'][$j]['yco']; ?>" onclick='show_customer_chat(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, event,$(this));'>
                                                        <div  id="dot-comment-<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" class="customer_chatimage1 beatHeart customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><span class="numberdot" id="dotnum_<?php echo $designer_file[$i]['id']; ?>"><?php echo $counttotaldotleft; ?></span></div>
                                                        <div style="display:none;" class="customer_chatimage2 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"></div>
                                                    </div>
                                                    <div class="customer_chat customer_chat<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" data-left="<?php echo $designer_file[$i]['chat'][$j]['xco']; ?>" data-top="<?php echo $designer_file[$i]['chat'][$j]['yco']; ?>" style="display:none;position:absolute;left:<?php echo $designer_file[$i]['chat'][$j]['xco']; ?>;top:<?php echo $designer_file[$i]['chat'][$j]['yco']; ?>;padding: 5px;border-radius: 5px;">
                                                         <div class="posone2" style="z-index: 99999999999999999999999;">
                                                                <div class="posonclick-add" id="posonclick-add<?php echo $designer_file[$i]['chat'][$j]['id']; ?>">
                                                                    <div class="posone2abs arrowdowns"  id="dotnum_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>_Next" onclick="return avoidClick(event);" >
                                                                        <a href="javascrip:void(0)" onclick="return closeChatBox(event);" class="crossblink"><i class="fa fa-times"></i></a>
                                                                        <div class="comment-box-es new-patrn">
                                                                            <div class="cmmbx-col left">
                                                                                <div class="cmmbxcir">
                                                                                    <figure class="pro-circle-img-xxs t1">
                                                                                        <img src="<?php echo $designer_file[$i]['chat'][$j]['profile_picture']; ?>">
                                                                                    </figure>
                                                                                </div>
                                                                                <div class='new_msg_des'>
                                                                                    <div class="ryt_header">
                                                                                        <span class="cmmbxtxt">
                                                                                            <?php echo isset($designer_file[$i]['chat'][$j]['first_name'])?$designer_file[$i]['chat'][$j]['first_name']:$designer_file[$i]['chat'][$j]['customer_name']; ?>
                                                                                        </span>
                                                                                        <span class="kevtxt" title='<?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?>'><?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?></span>
                                                                                    </div>
                                                                                    <pre><p class="distxt-cmm distxt-cmm_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></p></pre>
                                                                                        <div class="edit_del">
                                                                                            <a href="javascript:void(0)" onclick="replyButton(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, false)" class="reply_to_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><i class="fas fa-reply-all"></i></a>
                                                                                            <?php if ($_SESSION['user_id'] == $designer_file[$i]['chat'][$j]['sender_id']) { ?>    
                                                                                            <a href="javascript:void(0)" onclick='editMessages(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, false)'  class="edit_to_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><i class="fas fa-pen"></i></a> 
                                                                                            <a href="javascrip:void(0)" onclick="deleteMsg(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>);" class="del_msg"><i class="fas fa-trash"></i></a>                                                                               
                                                                                        <?php } ?>
                                                                                            </div>
                                                                                        <div class="msgk-mn-edit comment-submit_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none">
                                                                                            <form action="" method="post">
                                                                                                <textarea class="pstcmm sendtext" id="editdot_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, 'left')" data-id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>">Save</a> <a href="javascript:void(0)" class="cancel" onclick="cancelButton(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>)">Cancel</a>
                                                                                            </form>
                                                                                        </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="allreplyy two-can">
                                                                            <?php
                                                                            foreach ($designer_file[$i]['chat'][$j]['replies'] as $key => $val) {
                                                                                //echo "<pre/>";print_r($val);
                                                                                ?>
                                                                                <div class="cmmbx-col left reply reply_<?php echo $val['id']; ?>">
                                                                                    <div class="cmmbxcir">
                                                                                        <figure class="pro-circle-img-xxs t5">
                                                                                            <img src="<?php echo $val['profile_picture']; ?>">
                                                                                        </figure>
                                                                                    </div>
                                                                                    <div class="msg_sect">
                                                                                        <div class="name_edit_del">
                                                                                           <span class="cmmbxtxt"><?php echo isset($val['first_name'])?$val['first_name']:$val['customer_name']; ?></span>
                                                                                           <pre><p class="distxt-cmm distxt-cmm_<?php echo $val['id']; ?>"><?php echo $val['message']; ?></p></pre>

                                                                                           <?php 
                                                                                           //echo $_SESSION['user_id'] ."==". $val['sender_id'];
                                                                                           if ($_SESSION['user_id'] == $val['sender_id']) { ?> 
                                                                                            <div class="edit_del">
                                                                                                <a href="javascript:void(0)" onclick='editMessages(<?php echo $val['id']; ?>, false)' class="edit_to_<?php echo $val['id']; ?>"><i class="fas fa-pen"></i></a>
                                                                                                <a href="javascrip:void(0)" onclick="deleteMsg(<?php echo $val['id']; ?>);" class="del_msg"><i class="fas fa-trash"></i></a>                                                                               
                                                                                            </div>
                                                                                        <?php } ?>
                                                                                        <div class="msgk-mn-edit comment-submit_<?php echo $val['id']; ?>" style="display:none">
                                                                                            <form action="" method="post">
                                                                                                <textarea class="pstcmm sendtext" id="editdot_<?php echo $val['id']; ?>"><?php echo $val['message']; ?></textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(<?php echo $val['id']; ?>, 'left')" data-id="<?php echo $val['id']; ?>">Save</a> <a href="javascript:void(0)" onclick="cancelButton(<?php echo $val['id']; ?>)" class="cancel">Cancel</a>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <?php } ?>
                                                                        </div>
                                                                        <div class="closeoncross reply_msg_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none">
                                                                            <form action="" method="post">
                                                                                <div class="cmmtype-row wide_text">
                                                                                    <textarea class="pstcmm sendtext text1_<?php echo $designer_file[$i]['id']; ?>" id="text1_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" Placeholder="Post Comment" autocomplete="off" data-reqid="<?php echo $designer_file[$i]['id']; ?>"></textarea>
                                                                                    <span class="cmmsend">
                                                                                        <a class="cmmsendbtn send_comment_without_img send_<?php echo $designer_file[$i]['id']; ?>"
                                                                                            onclick="send_comment_without_img('<?php echo $designer_file[$i]['id'] ?>', 'customer', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>', 'leftreply', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>');">
                                                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive">
                                                                                        </a>
                                                                                    </span>
                                                                                </div>
                                                                            </form> 
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                                }
                                            }
                                            ?>
                                    <?php 
                                     if(is_numeric($designer_file[$i]['chat'][$j]['xco'])){
                                        $counttotaldotleft--;
                                        }} ?>
                                    </div>
                                    <div class="openchat" style="width:35%;position:absolute;display:none;padding:10px 20px;">
                                        <div class="posone1">
                                            <div class="posonclick-add" id="posonclick-add-inner<?php echo $designer_file[$i]['id']; ?>">
                                                <div class="posone3abs arrowdowns">
                                                    <div class="cmmbxpost">
                                                        <p class="leave-commt">Post a comment</p>
                                                        <textarea type="text" placeholder="Write Comment here" class="tff-con text_write text_<?php echo $designer_file[$i]['id']; ?>" onclick="wideText(this)" placeholder="Write Comment here"></textarea>
                                                    </div>
                                                    <div class="postrow12">
                                                        <span class="pstcancel"><a class="mycancellable" href="javascript:void(0);">Cancel</a></span>
                                                        <span class="pstbtns">
                                                            <button class="pstbtns-a send_request_img_chat send_text send_text<?php echo $designer_file[$i]['id']; ?>" style="border: none;border-radius: 5px;padding: 3px 10px 4px;" 
                                                                    data-fileid="<?php echo $designer_file[$i]['id']; ?>"
                                                                    data-senderrole="customer" 
                                                                    data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                                                                    data-designerpic="<?php echo $user[0]['profile_picture']; ?>"
                                                                    data-receiverid="<?php echo $request[0]['customer_id']; ?>" 
                                                                    data-receiverrole="customer"
                                                                    data-parentid = '0'
                                                                    data-customername="<?php echo $user[0]['first_name']; ?>"
                                                                    data-xco="" data-yco="">Send</button>
                                                        </span> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="showclicktocomment" style="display:none;">
                                        Click To Comment
                                    </div>
                                    <!-- Chat Toolkit end -->
                                </div>
                                <?php if ($designer_file[$i]['status'] != "Reject") { ?>
                                    <!--<div class="overlay_comment"></div>-->
                            <?php } ?>
                            </div>
                        <?php $counttotaldots--; }  else{ ?>
                             <div class="item slides two-can outer-post <?php
                                if ($designer_file[$i]['id'] == $main_id) {
                                    echo 'active';
                                }
                                ?>" id="<?php echo $designer_file[$i]['id']; ?>" data-name="<?php echo $designer_file[$i]['file_name']; ?>">
                                      <div class="customer_loader loading_<?php echo $designer_file[$i]['id']; ?>" style="display:none;">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/gz_customer_loader.gif">
                                     </div>
                                    <div class="col-md-12 mydivposition mydivposition_<?php echo $designer_file[$i]['id']; ?>"  style=" z-index: 0; ">
                                        <div class="gz ajax_loader loader loading_<?php echo $designer_file[$i]['id']; ?>" style="display:none;">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz-ajax-loader.gif">
                                        </div>
                                        <div class="slid" data-id="<?php echo $designer_file[$i]['id']; ?>">
                                            <?php
                                            $type = substr($designer_file[$i]['file_name'], strrpos($designer_file[$i]['file_name'], '.') + 1);
                                            if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                ?>
                                                <img class="img-responsive" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" style="/*max-height:calc(100vh - 80px);*/margin: auto;display: block;"/>
                                                <p class="extension_name"><?php echo $type; ?></p>
                                            <?php } else { ?>
                                                <img class="img-responsive" src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS_SAAS . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" style="/*max-height:calc(100vh - 80px);*/margin: auto;display: block;/*webkit-filter:blur(8px);filter:blur(8px);*/"/>
                                            <?php }
                                            ?>
                                        </div>
                                        <!-- Chat toolkit start-->
                                        <div class="appenddot_<?php echo $designer_file[$i]['id']; ?> remove_dot">
                                            <?php
                                            $total = array_diff(array_column($designer_file[$i]['chat'], 'xco'), ['']);
                                            $counttotaldotleft = count(array_values($total));
                                            for ($j = 0; $j < sizeof($designer_file[$i]['chat']); $j++) {
                                                if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                                    $background = "pinkbackground";
                                                } else {
                                                    $background = "bluebackground";
                                                }
                                                ?>
                                                <?php
                                                if ($designer_file[$i]['chat'][$j]['xco'] != "") {
                                                    if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                                                        $image1 = "dot_image1.png";
                                                        $image2 = "dot_image2.png";
                                                    } else {
                                                        $image1 = "designer_image1.png";
                                                        $image2 = "designer_image2.png";
                                                    }
                                                    ?>
                                                    <div id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" class="abcabcabc" style="display:none;position:absolute;opacity:0;width:30px;z-index:0;height:30px;" data-dot="<?php echo $counttotaldotleft; ?>" data-left="<?php echo $designer_file[$i]['chat'][$j]['xco']; ?>" data-top="<?php echo $designer_file[$i]['chat'][$j]['yco']; ?>" onclick='show_customer_chat(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, event,$(this));'>
                                                        <div class="customer_chatimage1 beatHeart customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><span class="numberdot" id="dotnum_<?php echo $designer_file[$i]['id']; ?>"><?php echo $counttotaldotleft; ?></span></div>
                                                        <div style="display:none;" class="customer_chatimage2 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><span class="numberdot" id="dotnum_<?php echo $designer_file[$i]['id']; ?>"><?php echo $counttotaldotleft; ?></span></div> 
                                                    </div>
                                                    <?php if (is_numeric($designer_file[$i]['chat'][$j]['xco'])) { ?>
                                                        <div class="customer_chat customer_chat<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none;position:absolute;left:<?php echo $designer_file[$i]['chat'][$j]['xco'] - 3; ?>px;top:<?php echo $designer_file[$i]['chat'][$j]['yco'] + 25; ?>px;padding: 5px;border-radius: 5px;">
                                                            <div class="posone2" >
                                                                <div class="posonclick-add">
                                                                    <div class="posone2abs arrowdowns" id="dotnum_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>_Next" onclick="return avoidClick(event);">
                                                                        <a href="javascript:void(0)" onclick="return closeChatBox(event);" class="crossblink"><i class="fa fa-times"></i></a>
                                                                        <div class="comment-box-es new-patrn">
                                                                            <div class="cmmbx-col left">
                                                                                <div class="cmmbxcir">
                                                                                    <figure class="pro-circle-img-xxs">
                                                                                        <img src="<?php echo (!empty($designer_file[$i]['chat'][$j]['profile_picture'])) ? FS_PATH_PUBLIC_UPLOADS_PROFILE . $designer_file[$i]['chat'][$j]['profile_picture'] : FS_PATH_PUBLIC_UPLOADS_PROFILE . 'user-admin.png'; ?>">
                                                                                    </figure>
                                                                                </div>
                                                                                <div class="new_msg_des">
                                                                                    <div class="ryt_header">
                                                                                        <span class="cmmbxtxt">
                                                                                            <?php echo isset($designer_file[$i]['chat'][$j]['first_name']) ? $designer_file[$i]['chat'][$j]['first_name'] : $designer_file[$i]['chat'][$j]['customer_name']; ?>
                                                                                        </span>
                                                                                        <span class="kevtxt" title='<?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?>'><?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?></span>
                                                                                    </div>
                                                                                    <pre><p class="distxt-cmm"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></p></pre>
                                                                                                                                                                    </div>
                                                                                                                                                                    
                                                                                                                                                                </div>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                        <?php
                                                    }
                                                }if (is_numeric($designer_file[$i]['chat'][$j]['xco'])) {
                                                    $counttotaldotleft--;
                                                }
                                                ?>
                                            <?php } ?>
                                            </div>
                                            <div class="openchat" style="width:35%;position:absolute;display:none;padding:10px 20px; z-index: 999;">
                                                <div class="posone1" id="dot-drag-<?php echo $designer_file[$i]['id']; ?>" style="z-index: 999999999999;">
                                                    <div class="posonclick-add">
                                                        <div class="posone3abs arrowdowns">
                                                            <div class="cmmbxpost">
                                                                <p class="leave-commt">Leave a Comment</p>
                                                                <input type="text" class="tff-con text_write text_<?php echo $designer_file[$i]['id']; ?>" placeholder="" onkeydown="javascript: if (event.keyCode
                                                                                == 13) {
                                                                            $('.send_text<?php echo $designer_file[$i]['id']; ?>').click();
                                                                        }">
                                                            </div>
                                                            <div class="postrow12">
                                                                <span class="pstcancel"><a class="mycancellable" href="javascript:void(0);">Cancel</a></span>
                                                                <span class="pstbtns">
                                                                    <button class="pstbtns-a send_request_img_chat send_text send_text<?php echo $designer_file[$i]['id']; ?>" style="border: none;border-radius: 5px;padding: 3px 10px 4px;" data-fileid="<?php echo $designer_file[$i]['id']; ?>"
                                                                            data-senderrole="designer" 
                                                                            data-senderid="<?php echo $_SESSION['user_id']; ?>" data-designerpic="<?php echo isset($user[0]['profile_picture']) ? $user[0]['profile_picture'] : 'user-admin.png'; ?>"
                                                                            data-receiverid="<?php echo $request[0]['customer_id']; ?>" 
                                                                            data-receiverrole="customer"
                                                                            data-customername="<?php echo $user[0]['first_name'] . " " . $user[0]['last_name']; ?>"
                                                                            data-xco="" data-yco="">Post</button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        <?php }  
                        } ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="pro-ject-rightbar"  id="comment_list_all"> 
 <div class="sidebar-header">
    <ul id="comment-people" class="list-header-blog" role="tablist" style="border:none;">
        <li class="active" id="1"><a data-toggle="tab" data-isloaded="0" data-view="" data-status="active,disapprove,assign,pending,checkforapprove" href="#comments_tab" role="tab"><i class="icon-gz_message_icon"></i> Comments </a></li>          
        <?php //echo "<pre/>";print_R($login_user_data);
        if($login_user_data[0]['role'] == 'customer'){
        ?>
        <li class="" id="2" ><a class="nav-link tabmenu" data-isloaded="0" data-view="" data-proid="<?php echo $draftrequest_id; ?>" data-draftid="<?php echo $latestdraftid; ?>" data-status="draft" data-toggle="tab" href="#peoples_tab" id="people_share" role="tab">
            <i class="icon-gz_share"></i> Share</a>
        </li>
        <?php } ?>
        </ul>
    </div>
    <div class="tab-content">
        <div data-group="1" data-loaded=""  class="tab-pane active content-datatable datatable-width" id="comments_tab" role="tabpanel">
            <?php
            for ($i = 0; $i < sizeof($designer_file); $i++) {
                $designer_file[$i]['src_file'] = str_replace(' ', '+', $designer_file[$i]['src_file']);
                $designer_file[$i]['file_name'] = str_replace(' ', '+', $designer_file[$i]['file_name']);
                ?>
                <div class="comment_sec comment-sec-<?php echo $designer_file[$i]['id']; ?>" id="<?php echo $designer_file[$i]['id']; ?>" 
                    <?php
                    if ($main_id == $designer_file[$i]['id']) {
                        echo "style='display: block;'";
                    } else {
                        echo "style='display:none;'";
                    }
                    ?>>
                    <div class=""> 
                        <?php //if ($main_user_data[0]['is_cancel_subscription'] != 1) { ?>
                            <div class="cmmtype-box">
                                <form action="" method="post">
                                    <div class="cmmtype-row">
                                        <span class="cmmfiles"></span>
                                        <textarea class="pstcmm sendtext abc_<?php echo $designer_file[$i]['id']; ?>"  Placeholder="Post Comment" id="sendcom"  autocomplete="off" data-reqid="<?php echo $designer_file[$i]['id']; ?>" <?php
                                        if ($cancomment_on_req === 0) {
                                            echo "disabled";
                                        }
                                        ?>></textarea>
                                        <div class="send-attech">
<!--                                        <label for="image">
                                         <input type="file" name="shre_main_draft_file[]" id="shre_file_<?php echo $designer_file[$i]['id']; ?>" class="shre_file" data-withornot="1" data-draftid="<?php echo $designer_file[$i]['id']; ?>" style="display:none;" multiple onchange="uploadDraftchatfile(event,'customer','<?php echo $data[0]['id']; ?>','<?php echo $_SESSION['user_id']; ?>','designer','<?php echo $request[0]['designer_id']; ?>','<?php echo $request[0]['customer_first_name']; ?>','<?php echo $request[0]['profile_picture']; ?>','0','<?php echo $allroles['seen_by'] ?>')"/>
                                         <span class="darftattchmnt" data-draft-id="<?php echo $designer_file[$i]['id']; ?>" title="Add Files"><i class="fa fa-paperclip" aria-hidden="true"></i></span>
                                         <input type="hidden" value="" name="saved_draft_file[]" class="delete_file"/>
                                        </label>-->
                                        <span class="cmmsend">
                                            <button type="button" class="cmmsendbtn send_comment_without_img send_<?php echo $designer_file[$i]['id']; ?>" id="send_comment" onclick="send_comment_without_img('<?php echo $designer_file[$i]['id'] ?>', '<?php echo $allroles['role'];?>', '0', '', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>')">
                                             <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-chat-send.png" class="img-responsive">
                                         </button>
                                         <!-- id="send_comment" -->
                                     </span>
                                 </div>
                                </div>
                             </form>
                         </div>
                     <?php //} ?>
                         <div class="ajax_searchload chat-align" style="display:none; text-align:center;">
                             <img src="<?php echo base_url();?>public/assets/img/customer/gz_customer_loader.gif" />
                        </div>
                     <ul class="list-unstyled list-comment-es two-can messagediv_<?php echo $designer_file[$i]['id']; ?>" id="comment_list">
                        <?php
//                        echo "<pre/>";print_R($designer_file);exit;
                        $total = array_diff(array_column($designer_file[$i]['chat'], 'xco'), ['']);
                        $counttotaldots = count(array_values($total));
                        for ($j = 0; $j < sizeof($designer_file[$i]['chat']); $j++) {
                             if ($designer_file[$i]['chat'][$j]['xco'] != 'NaN' || $designer_file[$i]['chat'][$j]['yco'] != 'NaN'): ?>
                                <li class="<?php echo (isset($designer_file[$i]['chat'][$j]['xco']) && $designer_file[$i]['chat'][$j]['xco'] != '') ? "dotlist_" . $designer_file[$i]['id'] . "" : ''; ?>">
                                    <div class="comment-box-es new-patrn">
                                        <div class="comments_link">
                                           <div class="cmmbx-row">
                                            <div class="all-sub-outer">
                                                <div class="cmmbx-col left">
                                                    <div class="clickable" id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" data-dot="<?php echo $counttotaldots; ?>">
                                                        <div class="cmmbxcir">
                                                            <figure class="pro-circle-img-xxs t2">
                                                                <img src="<?php echo $designer_file[$i]['chat'][$j]['profile_picture']; ?>">
                                                            </figure>
                                                            <?php if ($designer_file[$i]['chat'][$j]['xco'] != "" && $designer_file[$i]['chat'][$j]['yco'] != "") { ?>
                                                                <span class="cmmbxonline" onclick="show_customer_chat(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>,event,$(this))">
                                                                 <span class="numberdot" id="dotnum_<?php echo $designer_file[$i]['id']; ?>"><?php echo $counttotaldots; ?></span>
                                                             </span>
                                                         <?php } ?>
                                                     </div>
                                                     <div class="cmmbx-col">
                                                       <div class="ryt_header">
                                                        <span class="cmmbxtxt">
                                                            <?php
                                                            if ($designer_file[$i]['chat'][$j]['sender_role'] == 'designer') {
                                                                echo $designer_file[$i]['chat'][$j]['first_name'];
                                                            } else if ($designer_file[$i]['chat'][$j]['sender_role'] == 'customer' && $designer_file[$i]['chat'][$j]['sender_id'] != 0) {
                                                                if(isset($designer_file[$i]['chat'][$j]['first_name']) && $designer_file[$i]['chat'][$j]['first_name'] != ''){
                                                                    echo $designer_file[$i]['chat'][$j]['first_name'];
                                                                }else{
                                                                    echo $designer_file[$i]['chat'][$j]['customer_name'];
                                                                }
                                                            } else if ($designer_file[$i]['chat'][$j]['sender_role'] == 'customer' && $designer_file[$i]['chat'][$j]['sender_id'] == 0) {
                                                                echo $designer_file[$i]['chat'][$j]['customer_name'];
                                                            } else if ($designer_file[$i]['chat'][$j]['sender_role'] == 'manager') {
                                                                if(isset($designer_file[$i]['chat'][$j]['first_name']) && $designer_file[$i]['chat'][$j]['first_name'] != ''){
                                                                    echo $designer_file[$i]['chat'][$j]['first_name'];
                                                                }else{
                                                                    echo $designer_file[$i]['chat'][$j]['customer_name'];
                                                                }
                                                            } 
                                                            ?>
                                                        </span>
                                                        <span class="kevtxt" title='<?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?>'><?php echo date('M d,Y h:i A', strtotime($designer_file[$i]['chat'][$j]['created'])); ?></span>
                                                    </div>
                                                    <?php if($designer_file[$i]['chat'][$j]['is_filetype'] != 1){ ?>
                                                    <pre><p class="distxt-cmm distxt-cmm_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></p></pre>
                                                    <?php } else{
                                                            $type = strtolower(substr($designer_file[$i]['chat'][$j]['message'], strrpos($designer_file[$i]['chat'][$j]['message'], '.') + 1));
                                                            $updateurl = '';
                                                            if (in_array($type, DOCARROFFICE)) {
                                                                $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                                                                $class = "doc-type-file";
                                                                $exthtml = '<span class="file-ext">' . $type . '</span>';
                                                            } elseif (in_array($type, FONT_FILE)) {
                                                                $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                                                                $class = "doc-type-file";
                                                                $exthtml = '<span class="file-ext">' . $type . '</span>';
                                                            } elseif (in_array($type, ZIPARR)) {
                                                                $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-zip.svg';
                                                                $class = "doc-type-zip";
                                                                $exthtml = '<span class="file-ext">' . $type . '</span>';
                                                            } elseif (in_array($type, IMAGEARR)) {
                                                                $imgVar = imageRequestDraftchatUrl($type, $designer_file[0]['request_id'] . '/' .$designer_file[$i]['chat'][$j]['request_file_id'], $designer_file[$i]['chat'][$j]['message']);
                                                                $updateurl = '/_thumb';
                                                                $class = "doc-type-image";
                                                                $exthtml = '';
                                                            }
                                                            $basename = $designer_file[$i]['chat'][$j]['message'];
                                                            $basename = strlen($basename) > 20 ? substr($basename, 0, 20) . "..." . $type : $basename;
                                                            $data_src = FS_PATH_PUBLIC_UPLOADS . 'requestdraftmainchat/' . $designer_file[0]['request_id'] . '/' .$designer_file[$i]['chat'][$j]['request_file_id'].'/'. $designer_file[$i]['chat'][$j]['message'];?>
                                                           <span class="msgk-umsxxt took_<?php echo $chatdata['id']; ?>">
                                                                <div class="contain-info <?php echo (!in_array($type, IMAGEARR)) ? 'only_file_class' : ''; ?>" >
                                                                    <a class="open-file-chat <?php echo $class; ?>" data-ext="<?php echo $type; ?>" data-src="<?php echo $data_src; ?>">
                                                                        <img src="<?php echo $imgVar; ?>"> <?php echo $exthtml; ?>
                                                                    </a>
                                                                    <div class="download-file">
                                                                        <div class="file-name">
                                                                            <h4><b><?php echo $basename; ?></b></h4>
                                                                        </div>
                                                                        <a href="<?php echo base_url() ?>customer/request/download_stuffFromChat?ext=<?php echo $type; ?>&file-upload=<?php echo $data_src; ?>" target="_blank"> 
                                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/dwnlod-img.svg" />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </span>
                                                            <?php } ?> 
                                                    <div class="edit_del"> 
                                                     <a href="javascript:void(0)" onclick='replyButton(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, true)' class="reply_to_ryt_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><i class="fas fa-reply-all"></i></a>
                                                     <?php if ($_SESSION['user_id'] == $designer_file[$i]['chat'][$j]['sender_id']) { 
                                                     if($designer_file[$i]['chat'][$j]['is_filetype'] != 1){    
                                                     ?>
                                                     <a href="javascript:void(0)" onclick='editMessages(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, true)' class="edit_to_ryt_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><i class="icon-gz_edit_icon"></i></a>
                                                     <?php } ?>
                                                     <a href="javascript:void(0)" onclick="deleteMsg('<?php echo $designer_file[$i]['chat'][$j]['id']; ?>')" class="del_msg"><i class="far fa-trash-alt"></i></a>
                                                    <?php } ?>
                                                    </div>
                                             </div>
                                                    </div>
                                         <div class="msgk-mn-edit comment-submit_ryt_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none">
                                             <form action="" method="post">
                                                 <textarea class="pstcmm sendtext" id="editryt_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"><?php echo $designer_file[$i]['chat'][$j]['message']; ?></textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>, 'right')" data-id="<?php echo $designer_file[$i]['chat'][$j]['id']; ?>">Save</a> <a href="javascript:void(0)" class="cancel" onclick="cancelButton(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>)">Cancel</a>
                                             </form>
                                         </div>
                                     </div>
                                     <?php foreach ($designer_file[$i]['chat'][$j]['replies'] as $key => $val) { ?>
                                        <div class="cmmbx-col left reply clearfix reply_<?php echo $val['id']; ?>">
                                            <div class="cmmbxcir">
                                                <figure class="pro-circle-img-xxs t5">
                                                    <img src="<?php echo $val['profile_picture']; ?>">
                                                </figure>
                                            </div>
                                            <div class="msg_sect">
                                                <div class="name_edit_del">
                                                    <span class="cmmbxtxt">
                                                        <?php echo isset($val['first_name']) ? $val['first_name'] : $val['customer_name']; ?>
                                                    </span>
                                                    <?php $currentTime = new DateTime(date("Y-m-d h:i:s")); ?>
                                                    <div class="msgk-mn-edit comment-submit_ryt_<?php echo $val['id']; ?>" style="display:none">
                                                        <form action="" method="post">
                                                            <textarea class="pstcmm sendtext" id="editryt_<?php echo $val['id']; ?>"><?php echo $val['message']; ?></textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(<?php echo $val['id']; ?>, 'right')" data-id="<?php echo $val['id']; ?>">Save</a> <a href="javascript:void(0)" onclick="cancelButton(<?php echo $val['id']; ?>)" class="cancel">Cancel</a>
                                                        </form>
                                                    </div>
                                                </div>
                                                <pre><p class="distxt-cmm distxt-cmm_ryt_<?php echo $val['id']; ?>"><?php echo $val['message']; ?></p></pre>
                                                <?php if ($_SESSION['user_id'] == $val['sender_id']) { ?>
                                                <div class="edit_del"> 
                                                    <a href="javascript:void(0)" onclick='editMessages(<?php echo $val['id']; ?>, true)' class="edit_to_ryt_<?php echo $val['id']; ?>"><i class="icon-gz_edit_icon"></i></a>
                                                    <a href="javascrip:void(0)" onclick="deleteMsg(<?php echo $val['id']; ?>);" class="del_msg"><i class="far fa-trash-alt"></i></a>                                                                               
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <div class="closeoncross reply_msg_ryt_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none">
                                        <form action="" method="post">
                                            <div class="cmmtype-row wide_text">
                                                <textarea class="pstcmm sendtext text1_<?php echo $designer_file[$i]['id']; ?>" id="text2_<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" Placeholder="Post Comment" autocomplete="off" data-reqid="<?php echo $designer_file[$i]['id']; ?>"></textarea>
                                                <span class="cmmsend">
                                                    <a class="cmmsendbtn send_comment_without_img send_<?php echo $designer_file[$i]['id']; ?>"
                                                        onclick="send_comment_without_img('<?php echo $designer_file[$i]['id'] ?>', 'designer', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>', 'rightreply', '<?php echo $designer_file[$i]['chat'][$j]['id']; ?>');">
                                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive">
                                                    </a>
                                                </span>
                                            </div>
                                        </form> 
                                    </div>
                                </div>
                            </div>
                        </div>                     
                    </div>
                </li>
                <?php
            endif;
            if (is_numeric($designer_file[$i]['chat'][$j]['xco'])) {
                $counttotaldots--;
            }
        }
        ?>                      
    </ul>

</div>
</div>
<?php } ?>
</div>
<div data-group="1" data-loaded=""  class="tab-pane content-datatable datatable-width" id="peoples_tab" role="tabpanel">
<div class="sharedBy two-can">
        <?php if ($show_filesharing != 1) { ?>
            <div class="sharing-access-denied">You don't have access to file sharing. Please <a href="<?php echo base_url(); ?>customer/setting-view#billing" target="_blank">upgrade</a> your plan for File Sharing.</div>
        <?php } else { ?>
        <div class="team_member_sec">
            <h3>Team
                    <div class="right-inst">
                        <a class="vwwv" href="javascript:void(0);">

                            <span class="vewviewer-cicls">
                                <img src="<?php echo $data[0]['customer_image']; ?>" class="img-responsive"/>
                            </span>
                            <span class="hoversetss"><?php echo $data[0]['customer_name']; ?>
                            </span>
                        </a>
                        <a class="vwwv" href="javascript:void(0);">
                            <span class="vewviewer-cicls">
                                <img src="<?php echo $data[0]['designer_image']; ?>" class="img-responsive"/>
                            </span>
                            <span class="hoversetss"><?php echo $data[0]['designer_name']; ?> (Designer)
                            </span>
                        </a>
                        <a class="vwwv" href="javascript:void(0);">
                            <span class="vewviewer-cicls">
                                <img src="<?php echo $data[0]['qa_image']; ?>" class="img-responsive"/>
                            </span>
                            <span class="hoversetss"><?php echo $data[0]['qa_name']; ?> (QA)
                            </span>
                        </a>
                    </div>
            </h3>

        </div>
        <div class="error_messages_people">
        <?php if ($this->session->flashdata('message_error') != '') { ?>
            <div id="message" class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_error'); ?>
                </p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>
            <div id="message" class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_success'); ?>
                </p>
            </div>
        <?php }
        ?>
        </div>
        <div class="loader" id="loader_peoples_tab" style="display: none; margin-top: 35px;">
            <img src="http://localhost/graphicszoolive/public/assets/img/ajax-loader.gif">
        </div>
        <div class="ShareLinkHtml">
                
        </div>
    <?php } ?>
</div>
</div>
</div>
</div>
</div>
<!-- Approved Model -->
<div id="approvemodal" class="modal fade" role="dialog" style="">
    <div class="modal-dialog" style="margin-bottom: 0px;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body " style="background-color:#6ad154;text-align: center;font-size: 170px;">
                <i class="fa fa-check whitetext"></i>
                <h3 style="letter-spacing: -1px;font-size: 21px;" class="whitetext weight600">DESIGN APPROVED!</h3>
            </div>
            <div class="modal-footer" style="text-align:center;">
                <h4 class="pinktext weight600" style="font-size:16px;letter-spacing: -1px;">RATE YOUR DESIGN</h4>
                <p style="font-size:35px;">
                    <i class="fa fa-star pl5 greytext myapprovestar myapprovestar1"  data-id="1"></i>
                    <i class="fa fa-star pl5 greytext myapprovestar myapprovestar2" data-id="2"></i>
                    <i class="fa fa-star pl5 greytext myapprovestar myapprovestar3" data-id="3"></i>
                    <i class="fa fa-star pl5 greytext myapprovestar myapprovestar4" data-id="4"></i>
                    <i class="fa fa-star pl5 greytext myapprovestar myapprovestar5" data-id="5"></i>
                </p>
                <input type="hidden" id="myapprovedid" />
                <p style="padding-left:10%;padding-right:10%;margin-top:35px;">
                    Click "Download' button to access source files and hi-resolution version of your design.
                </p>
                <a type="button" class="btn whitetext weight600 design_download" style="background:#6ad154;padding:15px;border-radius: 5px;" download>DOWNLOAD</a>
                <p style="color:#6ad154;cursor:pointer;"  data-dismiss="modal">Thanks, I'll download this later.</p>
                <p style="margin-top:25px;">Please be notified that all source files will be removed after 14 days upon approval</p>

            </div>
        </div>

    </div>
</div>
<!-- End Approved Model -->


<button style="display: none;" id="verify_PhoneNumber" data-toggle="modal" data-target="#verifyPhoneNumber">click here</button>  
<button style="display: none;" id="responseverify" data-toggle="modal" data-target="#responseverifymodal">click here</button>
<div class="modal fade similar-prop" id="verifyPhoneNumber" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <img class="cross_popup" data-dismiss="modal" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cross.png">
                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/verify.png">
                    <header class="fo-rm-header">
                        <h2 class="popup_h2 del-txt">Please verify your phone number </h2>
                        <h3 class="head-c text-center">An agent will call you to provide you with the approved design files. </h3>

                    </header>
                    <form action="<?php echo base_url(); ?>customer/Request/verifyPhoneNumber" method="post" role="form" class="ChangeCurrent_Plan" id="ChangeCurrent_Plan">
                       <div class="confirmation_btn text-center">
                           <br/>
                           <input type="tel" name="verfy_number" id="verfy_number" value="<?php echo $user[0]['phone']; ?>">
                           <input type="hidden" name="user_id" id="user_id" value="<?php echo $_SESSION['user_id']; ?>">
                           <input type="hidden" name="req_id" id="req_id" value="<?php echo $_GET['id']; ?>">
                           <?php for ($i = 0; $i < sizeof($designer_file); $i++) { ?>
                            <input type="hidden" name="request_url" id="request_url" value="<?php echo $designer_file[$i]['id']; ?>?id=<?php echo $_GET['id']; ?>">
                        <?php } ?>
                        <input type="submit" class="submit btn btn-ndelete" id="verfyphn_number" value="Verify" name="verfyphn_number"/>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade similar-prop" id="responseverifymodal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <img class="cross_popup" data-dismiss="modal" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cross.png">
                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/waiting.png">
                    <header class="fo-rm-header">
                        <h2 class="popup_h2 del-txt">Your request is in process..</h2>
                        <h3 class="head-c text-center">Our agent will call you to provide you with the approved design files. </h3>
                    </header>
                </div>
            </div>
        </div>
    </div>
</div>

<!--*********people share*************-->

<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/account/s_all.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/designer/bootstrap.min.js"></script>
<script type="text/javascript">
var sizeoffiles;
var $rowperpage = '';
var $assets_path = '<?php echo FS_PATH_PUBLIC_ASSETS; ?>';
var requestmainchatimg = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTMAINCHATFILES; ?>";
var requestdraftchatimg = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES; ?>";
var $customer_name = '<?php echo $_SESSION['first_name'] ?>';
var $designer_img = '<?php echo $_SESSION['designerpic'] ?>';
var profile_path = "";
    $(document).on("click", ".f_revw_feedback", function () {
            var dataId = $('.outer-post.active').attr('id');
            var id = dataId;
           $.ajax({
            method: 'POST',
            url: "<?php echo base_url(); ?>customer/request/get_designfeedback",
            data: {id:id},
            dataType: 'json',
            success: function (response) {
                if(response != ""){
                    var value = response[0].satisfied_with_design;
                    $(".review_addtional_note").val(response[0].additional_notes);
                    $(".review_addtional_note").prev(".label-txt").addClass("label-active");
                    $("input[name=satisfied_with_design][value='"+value+"']").prop("checked",true);
                    $("#f_reviewpopup").modal("show");
                    $("#f_reviewpopup .dgn_revw").html("FEEDBACK");
                    $(".review_frm").css("display","none");
                    $("#f_reviewpopup").removeAttr('data-backdrop');
                    $("#f_reviewpopup").removeAttr('data-keyboard');
                    $('.cross_popup').css('display','block');
                }
            }
         });
    });
    function check_feedbackexist(id){
        $.ajax({
        method: 'POST',
        url: "<?php echo base_url(); ?>customer/request/get_designfeedback",
        data: {id:id},
        dataType: 'json',
        success: function (response) {
            if(response == ""){
                $(".f_revw_feedback").css("display","none");
            }else{
                $(".f_revw_feedback").css("display","block");
            }
        }
     });
   }
    $('.btn_reset').click(function () {
        $('#cro .active .mydivposition .slid img').css('transform', 'scale(1)');
        $(this).css('display', 'none');
        $('#cro .active .overlay_comment1').css('display', 'none');
    });

$(document).on('click', "#comment_list li", function (event) {
//$("#comment_list li").click(function(){
    var clicked_id = $(this).find('.comments_link').attr('id');
    var topPosition = $("#" + clicked_id + "").css('top');
    var leftPosition = $("#" + clicked_id + "").css('left');
var newtopPosition = topPosition.substring(0, topPosition.length - 2); //string 800
newtopPosition = parseFloat(newtopPosition) || 0;
var newleftPosition = leftPosition.substring(0, leftPosition.length - 2); //string 800
newleftPosition = parseFloat(newleftPosition) || 0;
$(".mycancel").hide();
$(".openchat").hide();
if (newtopPosition < 200) {
    $(".customer_chat").hide();
    $('.customer_chat' + clicked_id + ' .posone2abs').addClass('topposition');
} else {
    $(".customer_chat").hide();
    $('.customer_chat' + clicked_id + ' .posone2abs').removeClass('topposition');
}
if (newleftPosition > 450) {
    $('.customer_chat' + clicked_id + ' .posone2abs').addClass('leftposition');
} else {
    $('.customer_chat' + clicked_id + ' .posone2abs').removeClass('leftposition');
}
if (newtopPosition > 500) {
    $(".customer_chat").hide();
    $(".customer_chatimage2").hide();
    $(".customer_chatimage1").show();
    $(".customer_chatimage2.customer_chatimage" + clicked_id).show();
    $(".customer_chatimage1.customer_chatimage" + clicked_id).hide();
    $(".customer_chat" + clicked_id).toggle();
    $('html, body').animate({
        scrollTop: $(".customer_chat" + clicked_id + " .arrowdowns").offset().top
    }, 100);
} else {
    $(".customer_chat").hide();
    $(".customer_chatimage2").hide();
    $(".customer_chatimage1").show();
    $(".customer_chatimage2.customer_chatimage" + clicked_id).show();
    $(".customer_chatimage1.customer_chatimage" + clicked_id).hide();
    $(".customer_chat" + clicked_id).toggle();
}
});

<?php if (($user[0]['is_trail'] == '1' && $user[0]['is_trailverified'] !== '1' && $user[0]['can_trialdownload'] == '0')) { ?>
    $('.response_request').click(function () {
        $('#verify_PhoneNumber').trigger('click');
        $('#verfyphn_number').click(function () {
            var btn = $('.response_request');
            var fileid = $('.response_request').attr('data-fileid');
            var value = $('.response_request').val();
            var statusValue = $('.response_request').attr('data-status');
            var file = $('.response_request').attr('data-file');
            var request_id = $('.response_request').attr('data-requestid');
            var approve_date = $('.response_request').attr('data-approve-date');
            var parent = $('.response_request').parent('.comment_sec').attr('id');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>account/request/request_response",
                data: {"fileid": fileid,
                "value": statusValue,
                "request_id": request_id,
                "filestatus": value
            },
            success: function (data) {
        //alert("---"+data);
        //alert("Settings has been updated successfully.");
        if (data == 1) {
            if (value == "Approve") {
                $(btn).closest('p').hide();
                $(btn).closest('p').next('form').find('#download_btn').css('display', 'block');
                $(btn).closest('#approved_id').prev('#date_approved').html("<p class='dateApp'>" + approve_date + "</p>");
                $(btn).closest('#status').html("<p><img src='<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-check-ap.svg' class='img-responsive'>Approved</p>");
                $('.overlay_comment').remove();
                $('.text1_' + fileid).removeAttr("disabled");
                $('.send_' + fileid).removeAttr("disabled");
               
//                $(btn).closest('p').hide();
//                $(btn).closest('p').next('form').find('#download_btn').css('display', 'block');
//                $(btn).closest('#approved_id').prev('#date_approved').html("<p class='dateApp'>" + approve_date + "</p>");
//                $(btn).closest('#approved_id').prev('#date_approved').prev('#status').html("<p>Approved</p>");
//                $('.overlay_comment').remove();
//                $('.text1_' + fileid).removeAttr("disabled");
//                $('.send_' + fileid).removeAttr("disabled");
               // window.location.reload();
                // $('.comment_sec_'+parent+'').find('.approvedate').html("<p class='dateApp'>"+approve_date+"</p>");
                //$('.main_file').attr('href', file);
            } else if (value == "askforrevision") {
                $(btn).hide();
                $('.overlay_comment').remove();
                $('.text1_' + fileid).removeAttr("disabled");
                $('.send_' + fileid).removeAttr("disabled");
            }
            if(data.is_reveiw){
                $("#f_reviewpopup").modal("hide");
               }else{
                $("#f_reviewpopup").modal("show");
            }
           $("#review_draftid").val(fileid);
           $("#review_reqid").val(request_id);
           $(".review_frm").css("display","block");
           $("#f_reviewpopup .dgn_revw").html("SEND US YOUR VALUABLE FEEDBACK!");
        }
        }
        });
        });
});

<?php } ?>

<?php if ($user[0]['is_trail'] == '1' && $user[0]['is_trailverified'] == '1' && $user[0]['can_trialdownload'] == '0') { ?>
    $('.response_request').click(function () {
        $('#responseverify').trigger('click');
    });
<?php } ?>

<?php if (($user[0]['is_trail'] != '1') || ($user[0]['is_trail'] == '1' && $user[0]['is_trailverified'] == '1' && $user[0]['can_trialdownload'] == '1')) { ?>
    $('.response_request').click(function () {
        var btn = $(this);
        var fileid = $(this).attr('data-fileid');
        var value = $(this).val();
        var statusValue = $(this).attr('data-status');
        var file = $(this).attr('data-file');
        var request_id = $(this).attr('data-requestid');
        var approve_date = $(this).attr('data-approve-date');
        var parent = $(this).parent('.comment_sec').attr('id');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>account/request/request_response",
            dataType: "json",
            data: {"fileid": fileid,
            "value": statusValue,
            "request_id": request_id,
            "filestatus": value
        },
        success: function (data) {
            if (data.success == true) {
                if (value == "Approve") {
                    $(btn).closest('p').hide();
                    $(btn).closest('p').next('form').find('#download_btn').css('display', 'block');
                    $(btn).closest('#approved_id').prev('#date_approved').html("<p class='dateApp'>" + approve_date + "</p>");
                    $(btn).closest('#status').html("<p><img src='<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-check-ap.svg' class='img-responsive'>Approved</p>");
                    $('.overlay_comment').remove();
                    $('.text1_' + fileid).removeAttr("disabled");
                    $('.send_' + fileid).removeAttr("disabled");
//                    $(btn).closest('p').hide();
//                    $(btn).closest('p').next('form').find('#download_btn').css('display', 'block');
//                    $(btn).closest('#approved_id').prev('#date_approved').html("<p class='dateApp'>" + data.modified + "</p>");
//                    $(btn).closest('#approved_id').prev('#date_approved').prev('#status').html("<p>Approved</p>");
//                    $('.overlay_comment').remove();
//                    $('.text1_' + fileid).removeAttr("disabled");
//                    $('.send_' + fileid).removeAttr("disabled");
//                    window.location.reload();

                } else if (value == "askforrevision") {
                    $(btn).hide();
                    $('.overlay_comment').remove();
                    $('.text1_' + fileid).removeAttr("disabled");
                    $('.send_' + fileid).removeAttr("disabled");
                }
               $("#review_draftid").val(fileid);
               $("#review_reqid").val(request_id);
               $(".review_frm").css("display","block");
               $("#f_reviewpopup .dgn_revw").html("SEND US YOUR VALUABLE FEEDBACK!");
               if(data.is_reveiw){
                $("#f_reviewpopup").modal("hide");
               }else{
                   $("#f_reviewpopup").modal("show");
               }
            }
        }
    });
    });
<?php } ?>

$('.send_request_img_chat').click(function (e) {
//    console.log("test dot");
    e.preventDefault();
    var request_file_id = $(this).attr("data-fileid");
    var sender_role = $(this).attr("data-senderrole");
    var sender_id = $(this).attr("data-senderid");
    var receiver_role = $(this).attr("data-receiverrole");
    var receiver_id = $(this).attr("data-receiverid");
    var text_id = 'text_' + request_file_id;
    var message = $.trim($('.' + text_id).val());
    var customer_name = $(this).attr("data-customername");
    var xco = $(this).attr("data-xco");
    var yco = $(this).attr("data-yco");
    var request_id = $(this).attr("data-request_id");
    var countli = ($('.messagediv_' + request_file_id + ' li').length);
    var countval = parseInt(countli) + 1;
    var designer_img = $(this).data("designerpic");
    if (message != "") {
        $(this).prop('disabled', true);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>account/request/send_message",
            data: {"request_file_id": request_file_id,
            "sender_role": sender_role,
            "sender_id": sender_id,
            "receiver_role": receiver_role,
            "receiver_id": receiver_id,
            "message": message,
            "customer_name": customer_name,
            "xco": xco,
            "yco": yco,
            "parent_id": 0,
            "request_id": request_id,
            "customer_seen": 1,
        },
        success: function (data) {
            var res = data.split("_");
            var update = $.trim(res[0]);
            data = $.trim(res[1]);
            $('.send_request_img_chat').prop('disabled', false);
            if (xco != '') {
                var li_class = 'dotlist_' + request_file_id + ' ';
                li_class += 'reply_' + data;
            } else {
                li_class = '';
            }
            var numItems = $('.dotlist_' + request_file_id + '').length;
            var numDots = numItems + 1;
            var left = 'left';
            var appendleft = 'appendleft';
            var appendright = 'rightreply';
            var right = 'right';
            var left_cord = $('.openchat').css('left');
            var top_cord = $('.openchat').css('top');
            $('.text_' + request_file_id).val("");
            $(".messagediv_" + request_file_id + "").prepend('<li class="' + li_class + '"><div class="comment-box-es new-patrn"><div class="cmmbx-row"><div class="all-sub-outer"><div class="cmmbx-col left"><div class="cmmbxcir"><figure class="pro-circle-img-xxs"><img src="' + designer_img + '" class="mCS_img_loaded"></figure><span class="cmmbxonline" onclick="show_customer_chat('+data+',event,$(this))"><span class="numberdot" id="dotnum_' + request_file_id + '">' + numDots + '</span></span></div><div class="cmmbx-col"><div class="ryt_header"><span class="cmmbxtxt test2">' + customer_name + ' </span><span class="kevtxt">Just Now</span></div><pre><p class="distxt-cmm distxt-cmm_ryt_' + data + '">' + message + ' </p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="replyButton(' + data + ',true)" class="reply_to_ryt_' + data + '"><i class="fas fa-reply-all"></i></a><a href="javascript:void(0)" onclick="editMessages(' + data + ',true)" class="edit_to_ryt_' + data + '"><i class="icon-gz_edit_icon"></i></a><a href="javascrip:void(0)" onclick="deleteMsg(' + data + ')" class="del_msg"><i class="icon-gz_delete_icon"></i></a></div></div><div class="msgk-mn-edit comment-submit_ryt_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editryt_' + data + '">' + message + '</textarea><a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + right + '\')" data-id=' + data + '>Save</a><a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div></div></div><div class="closeoncross reply_msg_ryt_' + data + '" style="display:none"><form action="" method="post"><div class="cmmtype-row wide_text" onclick="wideText(this)"><textarea class="pstcmm sendtext text1_' + request_file_id + '" id="text2_' + data + '" placeholder="Post Comment" autocomplete="off" data-reqid="' + request_file_id + '"></textarea><a class="cmmsendbtn send_comment_without_img send_' + request_file_id + '" onclick="send_comment_without_img(' + request_file_id + ',\'' + sender_role + '\',\'' + data + '\',\'' + appendright + '\',\'' + data + '\');"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive"></a></div></form></div></div></div></div></div></li>');
            $('.appenddot_' + request_file_id).append('<div id="' + data + '" class="abcabcabc" style="position:absolute;left:' + left_cord + ';top:' + top_cord + ';width:30px;z-index:99999999;height:30px;" onclick="show_customer_chat(' + data + ', event,$(this));"><div class="customer_chatimage1 beatHeart customer_chatimage' + data + '" ><span class="numberdot" id="dotnum_' + request_file_id + '">' + numDots + '</span></div><div style="display:none;" class="customer_chatimage2 customer_chatimage' + data + '"></div></div>');
            $('.appenddot_' + request_file_id).append('<div class="customer_chat customer_chat' + data + '" style="position: absolute; left: ' + xco + '; top: ' + yco + '; padding: 5px; border-radius: 5px; display: none;"><div class="posone2" style="z-index: 99999999999999999999999;top:0px; "><div class="posonclick-add"><div class="posone2abs arrowdowns" onclick="return avoidClick(event);"><a href="javascrip:void(0)" onclick="return closeChatBox(event);" class="crossblink">×</a><div class="comment-box-es new-patrn two-can dotreply dottt_' + data + '"><div class="cmmbx-col left reply_' + data + '"><div class="cmmbxcir"><figure class="pro-circle-img-xxs t4"><img src="' + designer_img + '"></figure></div><div class="new_msg_des"><div class="ryt_header"><span class="cmmbxtxt">' + customer_name + '</span><span class="kevtxt">just now</span></div><pre><p class="distxt-cmm distxt-cmm_' + data + '">' + message + '</pre><div class="edit_del"><a href="javascript:void(0)" onclick="replyButton(' + data + ',false)" class="edit_to_' + data + '"><i class="fas fa-reply-all"></i></a><a href="javascript:void(0)" onclick="editMessages(' + data + ',false)" class="edit_to_' + data + '"><i class="icon-gz_edit_icon"></i></a><a href="javascrip:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="icon-gz_delete_icon"></i></a></div><div class="msgk-mn-edit comment-submit_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editdot_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + left + '\')" data-id="' + data + '" >Save</a><a href="javascript:void(0)" class="cancel" onclick="cancelButton(' + data + ')">Cancel</a></p></form></div></div></div><div class="closeoncross reply_msg_' + data + '" style="display:none"><form action="" method="post"><div class="cmmtype-row wide_text" onclick="wideText(this)"><textarea class="pstcmm sendtext text1_" id="text3_' + data + '" Placeholder="Post Comment" autocomplete="off"></textarea><span class="cmmsend"><a class="cmmsendbtn send_comment_without_img" onclick="send_comment_without_img(' + request_file_id + ',\'' + receiver_role + '\',\'' + data + '\',\'' + appendleft + '\',\'' + data + '\');"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive"></a></span></div></form></div></div></div></div></div></div>');
            $(".mycancel").remove();
            if (update == 'yes') {
                $("#review_draftid").val(request_file_id);
                $("#review_reqid").val(request_id);
                $(".review_frm").css("display","block");
                $("#f_reviewpopup .dgn_revw").html("SEND US YOUR VALUABLE FEEDBACK!");
                $("#f_reviewpopup").modal("show");
           }
        }
    });
$('.openchat').hide();
}
});

function showclicktocomment(event, id) {
    if ($(window).width() <= '767') {
        return false;
    }
    var position = $("#mydivposition_" + id).offset();
    var posX = position.left;
    var posY = position.top;
    var left = event.pageX - posX;
    var top = event.pageY - posY + +10;
    var window_height = $(window).height();
    var window_width = $("#mydivposition_" + id).width();
    if (top > window_height - 100) {
        top = top - 40;
    }
    if (left > window_width - 200) {
        left = left - 130;
    }
    if ($(event.target).is(".posone2abs") || $(event.target).parents('div.posone2abs').length
        || $(event.target).is(".customer_chatimage1") || $(event.target).is(".customer_chatimage2") || $(event.target).is(".numberdot")) {
        return false;
}
$('.showclicktocomment').remove();
if ($(".openchat").css("display") != "block") {
    $('.openchat').after('<div class="showclicktocomment" style="left: ' + left + 'px; top: ' + top + 'px; display: none;">\Click To Comment\</div>');
    $("#mydivposition_" + id + ' .showclicktocomment').show();
return false;
}

}

function hideclicktocomment()
{
    $('.showclicktocomment').remove();
}

function closeChatBox(e) {
    $(".leader-line").remove(); 
    $("#OpenpopUp").attr("data-open",0);
    $('.customer_chat').hide();
    e.preventDefault();
    e.stopPropagation();
    return false;
}

function avoidClick(e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
}

function show_customer_chat(id, event,click) {
//    debugger;
    $this = click; 
    var datadot=  $this.attr("data-dot"); 
     $(".arrowdowns").removeClass("hidenotch");
      
    if(datadot > 0 || $this.find("span").hasClass("numberdot")){
       $("#OpenpopUp").attr("data-open", "2");
    }
    $(".posone1").removeAttr("style");
    $(".posone2abs").removeAttr("style");
    $(".leader-line").remove();

    var left_cord = $('#' + id).css('left');
    var top_cord = $('#' + id).css('top');
   // console.log("id",id);
    $('.customer_chat' + id).css('left', left_cord);
    $('.customer_chat' + id).css('top', top_cord);
    var topPosition = $("#" + id + "").css('top');
    var leftPosition = $("#" + id + "").css('left');
    var newtopPosition = topPosition.substring(0, topPosition.length - 2); //string 800
    newtopPosition = parseFloat(newtopPosition) || 0;
//    console.log("newtopPosition",newtopPosition); 
    // console.log("newtopPosition",newtopPosition);
    if (newtopPosition < 200) {
        $('.customer_chat' + id + ' .posone2abs').addClass('topposition');
    } else {
        $('.customer_chat' + id + ' .posone2abs').removeClass('topposition');
    }

             
    setTimeout(function () {
        $('.myallremove').remove();
        $(".openchat").hide();
        $(".customer_chat").each(function () {
            if ($(this).html() == "<label></label>") {
                $(this).remove();
            }
            if ($(this).text() == "") {
                $(this).remove();
            }
        })
//        $(".customer_chat" + id).show();
//        $(".customer_chatimage" + id).toggle();
        
        $('.showclicktocomment').remove();
        $('.mycancel').remove();
        event.preventDefault();
    }, 1);
    
    $(".customer_chat" + id).show().siblings('.customer_chat').hide();

    var Attrid = $("#project-pageinfo-id").attr("data-id"); 
     DrawALine(id,"dotnum_"+id+"_Next");
    return false;
}

$(".text_write").click(function (event) {
    $('.showclicktocomment').remove();
    return false;
    event.preventDefault();
});

$(".openchat").click(function (event) {
    $('.showclicktocomment').remove();
    return false;
    event.preventDefault();
});

$(document).ready(function () {
    if ($(window).width() <= '767') {
        $('.remove_dot').css('display', 'none');
    }
    $('#comment_ul li figure').removeClass('active');
    $('#comment_ul .<?php echo $main_id; ?> figure').addClass('active');

    $('#sendtext').keypress(function (e) {
        var postbtn = $(this).attr('data-reqid')
        if (e.which == 13) {
            $('.send_' + postbtn).click();
            return false;
        }
    });
});

 $('.mycancellable').click(function () {
      $(".leader-line").remove();
   $('.openchat').hide();
   var mycancel = $(this).attr('data-cancel');
   $('.mydiv' + mycancel).hide();
   });
   

function mycancellable(id)
{
    $('.openchat').hide();
    var mycancel = $(".mycancellable" + id).attr('data-cancel');
    $('.mydiv' + mycancel).hide();
}

/****************dot comment**************/
 

    function EditClick(data_id, flag) {
        if (flag == 'left') {
            var updated_msg = $('#editdot_' + data_id).val();
        } else {
            var updated_msg = $('#editryt_' + data_id).val();
        }
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: "<?php echo base_url(); ?>account/request/editComment",
            data: {"request_file_id": data_id,
            "updated_msg": updated_msg,
            "reqID": <?php echo $request[0]['id']; ?>,
        },
        success: function (data) {
            if (flag == 'left') {
                $('.distxt-cmm_' + data_id).css('display', 'block');
                $('.distxt-cmm_' + data_id).text(data.message);
                $('.distxt-cmm_ryt_' + data_id).text(data.message);
                $('.comment-submit_' + data_id).css('display', 'none');
                $('.edit_to_' + data_id).css('display', 'inline-block');
            } else {
                $('.distxt-cmm_' + data_id).text(data.message);
                $('.distxt-cmm_ryt_' + data_id).css('display', 'block');
                $('.distxt-cmm_ryt_' + data_id).text(data.message);
                $('.edit_to_ryt_' + data_id).css('display', 'inline-block');
                $('.comment-submit_ryt_' + data_id).css('display', 'none');
            }
        },
    });
    }

function deleteMsg(id) {
    var cnfrm = cnfrm_delete();
    var mainid = '<?php echo $main_id; ?>';
    var numItems = $('.dotlist_' + mainid + '').length;
    var reqID = '<?php echo $request[0]['id']; ?>';
    if (cnfrm == true) {
    $.ajax({
        type: "POST",
        dataType: "JSON",
        url: "<?php echo base_url(); ?>account/request/deleteRequestmsg",
        data: {"request_file_id": id, "reqID":reqID},
        success: function (data) {
            $(".leader-line").remove(); 
            if (data.status == 'success') {
                var outer_main = $('#cro .active .mydivposition');
                outer_main.find('.abcabcabc').each(function () {
                    var deldot = $('#' + id).data('dot');
                    var currentdots = $(this).data('dot');
                    if (currentdots > deldot) {
                        $(this).data('dot', currentdots - 1);
                        $(this).find('.numberdot').text(currentdots - 1);
                    }
                });
                $('.dotlist_' + mainid).each(function () {
                    var deldot = $('#' + id).data('dot');
                    var currentdots = $(this).find('.clickable').data('dot');
                    if (currentdots > deldot) {
                        $(this).data('dot', currentdots - 1);
                        $(this).find('.numberdot').text(currentdots - 1);
                    }
                });
                $('#' + id).remove();
                $('.customer_chat' + id).remove();
                $('#' + id).closest('li').remove();
                $('.reply_' + id).remove();
            }
        },
    });
}
}

function send_comment_without_img(request_id, reciever_type, parent_id, replyfrom, requstfileid) {
//  console.log("test ryt");
        var sender_type = '<?php echo $_SESSION['role'] ?>';
        var sender_id = '<?php echo $_SESSION['user_id'] ?>';
        var reciever_id = '<?php echo $request[0]['designer_id']; ?>';
        var request_ids = '<?php echo $request[0]['id']; ?>';
        var  is_saas = '<?php echo $_SESSION['is_saas']; ?>'
        var rcvr_role = '';
        if((is_saas == 1 && sender_type == 'customer') || sender_type == 'manager'){
         rcvr_role =  'customer';  
        }else if(sender_type == 'designer'){
          rcvr_role =  'designer';
        }else{
          rcvr_role =  'designer';
        }
        if (parent_id == 0) {
            var text_id = '.abc_' + request_id;
        } else if (replyfrom == 'rightreply') {
            //console.log(requstfileid);
            var text_id = '#text2_' + requstfileid;
        } else if (replyfrom == 'appendleft') {
            //console.log('appendleft');
            var text_id = '#text3_' + requstfileid;
        } else {
            var text_id = '#text1_' + requstfileid;
        }
        var message = $(text_id).val();
        var verified_by_admin = "1";
        var customer_name = '<?php echo $_SESSION['first_name'] ?>';
        var designer_img = '<?php echo $_SESSION['designerpic'] ?>';
        if (message != "") {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>account/request/send_message",
                data: {"request_file_id": request_id,
                "sender_role": sender_type,
		"request_id": request_ids,
                "sender_id": sender_id,
                "receiver_role": rcvr_role,
                "receiver_id": reciever_id,
                "message": message,
                "customer_name": customer_name,
                "parent_id": parent_id,
                "<?php echo $allroles['seen_by']; ?>": 1,
            },
            success: function (data) {
                   var res = data.split("_");
                   var update = $.trim(res[0]);
                   data = $.trim(res[1]);
                    var left = 'left';
                    var right = 'right';
                    var appendright = 'rightreply';
                    $('.sendtext').val("");
                    if (parent_id == 0) {
                        $(".messagediv_" + request_id + "").prepend('<li><div class="comment-box-es new-patrn"><div class="comments_link"><div class="cmmbx-row"><div class="all-sub-outer"><div class="cmmbx-col left "><div class="clickable" id=' + data + '><div class="cmmbxcir"><figure class="pro-circle-img-xxs"><img src="' + designer_img + '" class="mCS_img_loaded"></figure></div><div class="cmmbx-col"><div class="ryt_header"><span class="cmmbxtxt">' + customer_name + ' </span><span class="kevtxt">Just Now</span></div><pre><p class="distxt-cmm distxt-cmm_ryt_' + data + '">' + message + ' </p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="replyButton(' + data + ', true)" class="reply_to_' + data + '"><i class="fas fa-reply-all"></i></a><a href="javascript:void(0)" onclick="editMessages(' + data + ', true)" class="edit_to_' + data + '"><i class="icon-gz_edit_icon"></i></a><a href="javascrip:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="icon-gz_delete_icon"></i></a></div></div></div><div class="msgk-mn-edit comment-submit_ryt_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editryt_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + right + '\')" data-id="' + data + '">Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div><div class="closeoncross reply_msg_ryt_' + data + '" style="display:none;"><form action="" method="post"><div class="cmmtype-row" onclick="wideText(this)"><textarea class="pstcmm replying sendtext text1_' + request_id + '" id="text2_' + data + '" placeholder="Post Comment" autocomplete="off" data-reqid="' + request_id + '"></textarea><a class="cmmsendbtn send_comment_without_img send_' + request_id + '" onclick="send_comment_without_img(' + request_id + ',\'' + sender_type + '\',\'' + data + '\',\'' + appendright + '\',\'' + data + '\');"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/designer/icon-chat-send.png" class="img-responsive"></a></div></form></div></div></div></div></div></li>');
                    } else {
                        $('.reply_msg_' + parent_id).before('<div class="cmmbx-col left reply reply_' + data + '"><div class="cmmbxcir"><figure class="pro-circle-img-xxs t5"><img src="' + designer_img + '" class="mCS_img_loaded"></figure></div><div class="msg_sect"><div class="name_edit_del"><span class="cmmbxtxt">' + customer_name + '</span><pre><p class="distxt-cmm distxt-cmm_ryt_' + data + '">' + message + '</p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="editMessages(' + data + ', false)" class="edit_to_' + data + '"><i class="icon-gz_edit_icon"></i></a><a href="javascrip:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="icon-gz_delete_icon"></i></a></div><div class="msgk-mn-edit comment-submit_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editdot_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + left + '\')" data-id=' + data + '>Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div></div></div>');
                        $('.reply_msg_ryt_' + parent_id).before('<div class="cmmbx-col left reply reply_' + data + '"><div class="cmmbxcir"><figure class="pro-circle-img-xxs t5"><img src="' + designer_img + '" class="mCS_img_loaded"></figure></div><div class="msg_sect"><div class="name_edit_del"><span class="cmmbxtxt">' + customer_name + '</span><pre><p class="distxt-cmm distxt-cmm_ryt_' + data + '">' + message + '</p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="editMessages(' + data + ', false)" class="edit_to_' + data + '"><i class="icon-gz_edit_icon"></i></a><a href="javascrip:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="icon-gz_delete_icon"></i></a></div><div class="msgk-mn-edit comment-submit_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editdot_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + left + '\')" data-id=' + data + '>Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div></div></div>');
                        //$('.dottt_' + parent_id).append('<div class="cmmbx-col left"><div class="cmmbxcir"><figure class="pro-circle-img-xxs t5"><img src="' + designer_img + '" class="mCS_img_loaded"></figure></div><div class="msg_sect"><div class="name_edit_del"><span class="cmmbxtxt">' + customer_name + '</span><pre><p class="distxt-cmm_ryt_' + data + '">' + message + '</p></pre><div class="edit_del"><a href="javascript:void(0)" onclick="editMessages(' + data + ', false)" class="edit_to_' + data + '"><i class="icon-gz_edit_icon"></i></a><a href="javascrip:void(0)" onclick="deleteMsg(' + data + ');" class="del_msg"><i class="icon-gz_delete_icon"></i></a></div><div class="comment-submit_' + data + '" style="display:none"><form action="" method="post"><textarea class="pstcmm sendtext" id="editdot_' + data + '">' + message + '</textarea> <a href="javascript:void(0)" class="edit_msg" onclick="EditClick(' + data + ',\'' + left + '\')" data-id=' + data + '>Save</a> <a href="javascript:void(0)" onclick="cancelButton(' + data + ')" class="cancel">Cancel</a></form></div></div></div></div>');
                        $('.reply_msg_ryt_' + parent_id).css('display', 'none');
                        $('.reply_to_ryt_' + parent_id).css('display', 'inline-block');
                    }
                    if (update == 'yes') {
                        $("#review_draftid").val(request_id);
                        $("#review_reqid").val(request_ids);
                        $(".review_frm").css("display","block");
                        $("#f_reviewpopup .dgn_revw").html("SEND US YOUR VALUABLE FEEDBACK!");
                        $("#f_reviewpopup").modal("show");
                   }
                },
            });
}
}




$(document).on('click','.slick_thumbs ul li',function(e){
    var dataId = $(this).attr('id');
    var Outerdivwidth = $('.outer-post').width();
    var Outerdivheight = $('.outer-post').height();
    var outer_main = $('.mydivposition_'+dataId);
    var main_img = $(this).find('img');
    var actualimgWidth = main_img.get(0).naturalWidth;
    var actualimgHeight = main_img.get(0).naturalHeight;
    var outer_main_height = outer_main.height();
    var calcval = (Outerdivheight - outer_main_height)/2;
    if(Outerdivheight > outer_main_height){
        $(outer_main).css('margin-top',calcval+"px");
    }
    if(actualimgWidth > Outerdivwidth || actualimgHeight > Outerdivheight) {
        $('#zoomIn').css('cursor', 'pointer');
        $('#zoomIn').css('opacity', '1');
        $('#zoomOut').prop('disabled', false);
        $('#zoomIn').prop('disabled', false);
    }else{
        $('#zoomOut').css('cursor', 'no-drop');
        $('#zoomOut').css('opacity', '0.5');
        $('#zoomIn').css('cursor', 'no-drop');
        $('#zoomIn').css('opacity', '0.5');
        $('#zoomOut').prop('disabled', true);
        $('#zoomIn').prop('disabled', true);
    }
    $('.slick_thumbs ul li').css('pointer-events','none');
    $('#zoomOut').attr('data-zoomoutcount','0');
    $('#zoomIn').attr('data-count','0');
    if(dataId){
        $('.hdr-buttons').css('display','none')
        $('.butt_'+dataId).css('display','block');
    }
    var url = $(this).find('img').attr('src');
    var parts = url.split("/");
    var last_part = parts[parts.length-1];
    $('.file_title').html(last_part);
//    $('.loading_' + dataId).css("display", "flex");
//    $('.slid').css("display", "none");
//    $('.abcabcabc').css("display", "none");
    $('.abcabcabc').css("display", "none");
    $('.customer_loader').css('display','block');
   $('.slid').css('display','none');
//    $('.slid img').css({
//            "-webkit-filter": "blur(8px)",
//            "filter": "blur(8px)"
//    });
    setTimeout(function () {
        $('.loading_' + dataId).css("display", "none");
        $('.customer_loader').css('display','none');
        $('.slid').css('display','block');
//        $('.slid img').css({
//            "-webkit-filter": "blur(0px)",
//            "filter": "blur(0px)"
//        });
         $('.slick_thumbs ul li').css('pointer-events','auto');
         
        $('.abcabcabc').css("display", "block");
        refreshDotsActiveTab();
    }, 2000);
    $(".slick_thumbs ul li").find('figure').removeClass('active');
    $(this).find('figure').addClass('active');
    var thumb_index = $(this).index();
    $('ul.slick-dots').find('li:eq(' + thumb_index + ')').trigger('click');
    var dot_length = $('ul.slick-dots').children().length;
    var count_slid = thumb_index;
    var current_design = count_slid;
    var total_design = <?php echo sizeof($designer_file); ?>;
    current_design += 1;
    if (current_design > total_design) {
        current_design = 1;
    }
    $(".pagin-one").html(current_design + " of " + total_design);
});

/* 28*9*2019  */
   $(window).on("load",function(){
        $('.customer_loader').css('display','block');
        $('.slid').css('display','none');
       $dataid=  $(".slick_thumbs li").find("figure.active").parents("li").attr('id');
       $request_id=  $(".slick_thumbs li").find("figure.active").parents("li").attr('request_id');
       $("#project-pageinfo-id").attr("data-id",$dataid);
        checkForFavOrUnFav($dataid,$request_id);
        $file_title=  $(".file_title").text();
        //$(".MarkasFavClick").html('<div class="click MarkasFavClick active"  id="MarkasFavClick_"'+$dataid+'" data-id="'+$dataid+'" data-requested-id="'+$request_id"><div class="tooltiptext">Already marked</div><span class="cstm_span fa fa-heart"></span><p class="info">Added to favourites!</p></div>');
       $(".MarkasFavClick").attr("id","MarkasFavClick_"+$dataid);
       $(".MarkasFavClick").attr("data-id",$dataid);
       $(".MarkasFavClick").attr("data-requested-id",$request_id);
       $(".CoPyOrMove").attr("data-id",$request_id);
       $(".CoPyOrMove").attr("data-projid",$dataid);
       $(".CoPyOrMove").attr("data-file",$file_title);
       
       /** review feedback js**/
       $(".f_revw_feedback").attr("id","f_revw_feedback_"+$dataid);
       $(".f_revw_feedback").attr("data-id",$dataid);
       check_feedbackexist($dataid);
   }); 
   
   $(".next,.prev ").on("click",function(){
       var dataId,datatitle,reqid;
      if($(this).hasClass('prev')){
           dataId = $('.outer-post.active').prev().attr('id');
           datatitle = $('.outer-post.active').prev().data('name');
           reqid = $('.outer-post.active').prev().data('req-id');
           if(typeof dataId == 'undefined'){
               dataId = $('.outer-post:last').attr('id');
               reqid = $('.outer-post:last').data('req-id');
               datatitle = $('.outer-post:last').data('name');
           }
       }else{
           dataId = $('.outer-post.active').next().attr('id');
           datatitle = $('.outer-post.active').next().data('name');
           reqid = $('.outer-post.active').next().data('req-id');
           if(typeof dataId == 'undefined'){
               dataId = $('.outer-post:first').attr('id');
               reqid = $('.outer-post:first').data('req-id');
               datatitle = $('.outer-post:first').data('name');
           }
       }
       
       $(".CoPyOrMove").attr("data-id",reqid);
       $(".CoPyOrMove").attr("data-projid",dataId);
       $(".CoPyOrMove").attr("data-file",datatitle);
       $(".leader-line").hide(); 
       $("#project-pageinfo-id").attr("data-id",dataId);
       checkForFavOrUnFav(dataId,reqid);
   
       $(".MarkasFavClick").attr("data-id",dataId);
       $(".MarkasFavClick").attr("id","MarkasFavClick_"+dataId);
       $(".MarkasFavClick").attr("data-requested-id",reqid);
       check_feedbackexist(dataId);
   
   });
   $(".active1").click(function(){
     $(".leader-line").remove();
       var dataid=  $(this).attr('id');
       var req_id=  $(this).attr('request_id');
       var file_title=  $(".file_title").text();
       $(".MarkasFavClick").attr("data-id",dataid);
       $(".MarkasFavClick").attr("id","MarkasFavClick_"+dataid);
       $(".MarkasFavClick").attr("data-requested-id",req_id);
   
       $(".CoPyOrMove").attr("data-id",req_id);
       $(".CoPyOrMove").attr("data-projid",dataid);
       $(".CoPyOrMove").attr("data-file",file_title);
        $("#OpenpopUp").attr("data-open","1");
         $("#project-pageinfo-id").attr("data-id",dataid);
       checkForFavOrUnFav(dataid,req_id);
       
       /** review feedback js**/
       $(".f_revw_feedback").attr("id","f_revw_feedback_"+dataid);
       $(".f_revw_feedback").attr("data-id",dataid);
       check_feedbackexist(dataid);
   });


   $(".outer-post").on("scroll",function() {
     var dataid = $(".dotComment").attr("data-id");
     var Attrid = $("#project-pageinfo-id").attr("data-id");
       
      $(".leader-line").hide(); 
   });


$('#f_feedback_frm').submit(function(){
    $(this).find(':button[type=submit]').prop('disabled', true);
});

  $('.response_request_by_admin').click(function () {
            var btn = this;
            var parent = $(this).closest('.comment_sec').attr('id');
            var fileid = $(this).attr('data-fileid');
            var value = $(this).attr("data-status");
            var file = $(this).attr('data-file');
            var request_id = $(this).attr('data-requestid');
            var approve_date = $(this).attr('data-approve-date');
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>account/request/request_response_by_admin",
                data: {"fileid": fileid,
                    "value": value,
                    "request_id": request_id},
                success: function (data) {
                    if (data == 1) {
                        if (value == "customerreview") {
                            $(btn).parent().css('display','none');
                            $(btn).parent().next('.app_quanlity_psd').css('display','block');
                            $(btn).parent().prev('.approvedate').html("<p class='dateApp'>"+approve_date+"</p>");

                        } else if(value == "Reject"){
                             $(btn).parent().css('display','none');
                            $(btn).parent().siblings().eq(2).css('display','block');
                            $("#"+fileid+" .overlay_comment").remove();
                        }
                    }
                }
            });
});
   /*29.9.19*/
 </script>

<style type="text/css">
   .MarkasFav {
   display: inline-flex;
   padding: 0px;
   margin: 0px;
   }
   .MarkasFav .click{
   position: relative;
   }
   .MarkasFav .CoPyOrMove img {
   width: 45%;
   cursor: pointer;
   }
   div#CopYfileToFolder,div#DirectorySave {
   margin-top: 40px;
   }
   .fa-heart-o:before {
   content: "\f004";
   }
   span.cstm_span.fa-heart.fas {
    color: #f90500;
}
</style>

