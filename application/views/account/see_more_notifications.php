<style>
h2.brnd-info-section {
    font: normal 21px/25px 'GothamPro-Medium', sans-serif;
    font-weight: 600;
    margin-bottom: 10px;
}

a.view-materials:hover{
	    color: #e73250;
}
.product-list-show h3 {
    color: #e52344;
    text-transform: uppercase;
}
.brand_profile_row .cell-row {
    font: normal 15px/25px 'GothamPro', sans-serif;
}
.brand_profile_row .cell-row a.view-materials {
    background: #e52344;
    color: #fff;
    width: 100px;
    display: block;
    text-align: center;
    line-height: 29px;
    border-radius: 30px;font-size: 13px;
    text-decoration: none;
}
.brand_info_not_found {
    background: #f5f5f5;
    padding: 80px;
    text-align: center;
    border: 1px solid #ccc;
    border-radius: 12px;
    font-family: GothamPro, sans-serif;
    color: #e73250;
    margin-top: 30px;
}
</style>
<section class="con-b">
<div class="container">
 <h2 class="brnd-info-section">Notifications</h2>	
<div class="product-list-show">
	<div class="row">  
   <?php if(sizeof($notifications) > 0){
       //echo "<pre/>";print_r($notifications);
       ?>
		<!-- Start Row -->
		<div class="cli-ent-row tr brdr">
			<div class="cli-ent-col td" style="width: 20%;">
				<div class="cli-ent-xbox text-left">
				<div class="cell-row">
				<div class="cell-col">
				<h3 class="pro-head space-b">Title</h3>
				</div>
				</div>
				</div>
			</div>
		<div class="cli-ent-col td" style="width: 20%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			<div class="cell-col">
			<h3 class="pro-head space-b">Notification Date</h3>
			
			</div>
			</div>
			</div>
		</div>
                <div class="cli-ent-col td" style="width: 20%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">

			<div class="cell-col">
			<h3 class="pro-head space-b">Action</h3>
			
			</div>
			</div>
			</div>
		</div>
		</div> <!-- End Row -->
		<div class="brand_profile_row">
		<?php for($i=0; $i<sizeof($notifications);$i++){ ?>
		<!-- Start Row -->
		<div class="cli-ent-row tr brdr">
			<div class="cli-ent-col td" style="width: 20%;">
				<div class="cli-ent-xbox text-left">
				<div class="cell-row">
                                <figure class="pro-circle-k1">
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$notifications[$i]['profile_picture']; ?>" class="img-responsive">
                                </figure>
                                <strong><?php echo $notifications[$i]['first_name'] . " " . $notifications[$i]['last_name']; ?></strong>
				<?php echo $notifications[$i]['title']; ?>
				</div>
				</div>
			</div>
		<div class="cli-ent-col td" style="width: 20%;">
			<div class="cli-ent-xbox text-left">
			<div class="cell-row">
			<?php $approve_date = $notifications[$i]['created'];
                            echo date("M d, Y h:i A", strtotime($approve_date));
                            ?>
			</div>
			</div>
		</div>
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-left">
			<div class="cell-row">

                        <div class="cell-col">
                            <h3 class="pro-head space-b" style="cursor: pointer;">
                            <a href="<?php echo base_url() . $notifications[$i]['url']; ?>" class="view-materials">Read </a> </br>
			</div>
                            
                        </div>
                        </div>
                </div>
		</div>
		
		<?php } ?>
       </div> <!-- End Row -->
	</div>
        <?php } ?>
	</div>
</div>
</div>
</section>