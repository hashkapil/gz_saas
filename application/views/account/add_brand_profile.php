<?php 
if($canuseradd_brand_pro == 0){ 
  redirect(base_url().'customer/request/design_request');
}else if($is_add_brandprofile == 0 && $_GET['editid'] == ""){
   redirect(base_url().'customer/brand_profiles'); 
} ?>
<section id="brand_profile">
    <div class="container">
        <div class="header-blog">
            <?php if ($this->session->flashdata('message_error') != '') { ?>
                <div id="message" class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_error'); ?>
                    </p>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('message_success') != '') { ?>
                <div id="message" class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_success'); ?>
                    </p>
                </div>
            <?php }
            ?>
        </div>
        <div class="brand-outline">
          <form method="post">
              <?php
              $editvar = '';
              $editID = isset($_GET['editid']) ? $_GET['editid']:'';
              if($editID && $editID != ''){
                $editvar = $exist_prodile_data[0];
                $edit_materail = $exist_prodile_material_data;
            }if($editID == '' && $dupID == ''){
                $editvar = '';
                $edit_materail = '';
            }?>
            <div class="row">
                <div class="col-lg-12 catagry-heading">
                    <?php if($editID == ''){ ?>
                      <h3>Add New Brand Profile<span>Please fill out the information below to create a brand profile</span></h3>
                  <?php } else { ?>
                   <h3>Edit Brand Profile<span>Please fill out the information below to update a brand profile</span></h3>
               <?php  } ?>
           </div>
       </div>
       <div class="row">
        <div class="col-md-12">
            <div class="white-boundries mr-t0">
                <div class="row">
                    <div class="col-md-6">
                        <label class="form-group">
                            <p class="label-txt <?php echo (isset($editvar['brand_name']) && $editvar['brand_name']!= '') ? "label-active" : ""; ?>">Brand Name <span>*</span></p>
                            <input type="text" class="input" name="brand_name" id="exampleInputName" aria-describedby="NameHelp" required value="<?php echo isset($editvar['brand_name']) ? $editvar['brand_name'] : ""; ?>">
                            <div class="line-box">
                              <div class="line"></div>
                          </div>
                      </label>
                  </div>
                  <div class="col-md-6">
                    <label class="form-group">
                        <p class="label-txt <?php echo (isset($editvar['website_url']) && $editvar['website_url']!= '') ? "label-active" : ""; ?>">Website URL</p>
                        <input type="text" class="input" name="website_url" id="exampleInputwebsite_url" value="<?php echo isset($editvar['website_url']) ? $editvar['website_url'] : ""; ?>">
                        <div class="line-box">
                          <div class="line"></div>
                      </div>
                  </label>
              </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-6">
               <label class="form-group">
                <p class="label-txt <?php echo (isset($editvar['fonts']) && $editvar['fonts']!= '') ? "label-active" : ""; ?>">Fonts</p>
                <input type="text" class="input" name="fonts" id="exampleInputwebsite_url" value="<?php echo isset($editvar['fonts']) ? $editvar['fonts'] : ""; ?>">
                <div class="line-box">
                  <div class="line"></div>
              </div>
          </label>
      </div>
      <div class="col-lg-6 col-md-6">
        <label class="form-group">
            <p class="label-txt label-active">Color Preferences</p>
            <select name="color_preference" class="input">
                <option value="No Preference" <?php echo ($editvar['color_preference'] == 'No Preference') ? "selected" : ""; ?> >No Preference</option>
                <option value="CMYK Files" <?php echo ($editvar['color_preference'] == 'CMYK Files') ? "selected" : ""; ?> >CMYK Files</option>
                <option value="RGB Files" <?php echo ($editvar['color_preference'] == 'RGB Files') ? "selected" : ""; ?> >RGB Files</option>
            </select>
            <div class="line-box">
              <div class="line"></div>
          </div>
      </label>
  </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <label class="form-group">
            <p class="label-txt <?php echo (isset($editvar['description']) && $editvar['description']!= '') ? "label-active" : ""; ?>">Description <span>*</span></p>
            <textarea class="input" rows="5" name="description" id="comment" required><?php echo isset($editvar['description']) ? $editvar['description'] : ""; ?></textarea>
            <div class="line-box">
              <div class="line"></div>
          </div>
          <p class="fill-sub">Please provide us as much detail as possible. Include things such as color preferences, styles you like, text you want written on the graphic, and anything else that may help the designer.</p>
      </label>
  </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <label class="form-group">
            <p class="label-txt label-active">addition reference link</p>
            <?php if(!empty($editvar['google_link'])){
                $links_arr = explode(',',$editvar['google_link']);
                                //echo "<pre/>";print_r($links_arr);
                foreach($links_arr as $lk => $lv){?>
                    <input type="text" name="google_link[]" class="input google_link f_cls_nm" value="<?php echo isset($lv)?$lv:'';?>"/>
                    <div class="line-box">
                      <div class="line"></div>
                  </div>
              <?php }}else{ ?>
                <input type="text" name="google_link[]" class="input google_link f_cls_nm"/>
                <div class="line-box">
                  <div class="line"></div>
              </div>
          <?php } ?>
          <a class="add_more_button">
              <i class="icon-gz_plus_icon"></i>
          </a>
          <div class="input_fields_container"></div>
      </label>
  </div>
</div>
</div>
</div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="upload-card">
            <label for="exampleInputlogo">Brand Logo</label>
            <div class="file-drop-area file-upload">
                <span class="fake-img">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_Draft_img_icon.svg" class="img-responsive">
                </span>
                <span class="file-msg">Select files to upload or Drag and drop file</span>
                <input type="file" class="file-input project-file-input brand_file_upload" multiple="" name="logo_upload[]" id="logo_file_input">
            </div>
        </div>
        <div class="uploadFileListContainer row logo_uploadFileListContain">                      
        </div>
        <?php if(sizeof($edit_materail) > 0){ ?>
            <div class="uploadFileListContainer row">   
                <?php
                for ($i = 0; $i < count($edit_materail); $i++) { 
                 if($edit_materail[$i]['file_type'] == 'logo_upload') { ?>
                    <div  class="uploadFileRow" id="file<?php echo $edit_materail[$i]['id']; ?>">
                        <div class="col-md-12">
                            <div class="extnsn-lst">
                                <p class="text-mb">
                                    <?php echo $edit_materail[$i]['filename']; ?>
                                </p>
                                <p class="cross-btnlink">
                                    <a href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$editID.'/'.$edit_materail[$i]['filename']; ?>" download>
                                        <span><i class="fa fa-download" aria-hidden="true"></i></span>
                                    </a>
                                </p>
                                <p class="cross-btnlink">
                                <a href="javascript:void(0)" data-fieldname="logo" data-delfile="<?php echo $edit_materail[$i]['filename']; ?>" data-fileid="<?php echo $edit_materail[$i]['id']; ?>" data-flpath="<?php echo FS_UPLOAD_PUBLIC_UPLOADS.'brand_profile/'.$editID.'/'.$edit_materail[$i]['filename']; ?>" class="edt_brnd_prfl">
                                    <span>x</span>
                                </a>
                                    </p>
                            </div>
                        </div>
                    </div>
                <?php } 
            } ?>
        </div>
    <?php } ?>
</div>
<div class="col-md-4">
    <div class="upload-card">
        <label for="exampleInputlogo">Marketing Materials</label>
        <div class="file-drop-area file-upload">
            <span class="fake-img">
                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_Draft_img_icon.svg" class="img-responsive">
            </span>
            <span class="file-msg">Select files to upload or Drag and drop file</span>
            <input type="file" class="file-input project-file-input brand_file_upload" multiple="" name="materials_upload[]" id="materials_file_input">
        </div>
    </div>
    <div class="uploadFileListContainer row materials_uploadFileListContain">                      
    </div>
    <?php if(sizeof($edit_materail) > 0){ ?>
        <div class="uploadFileListContainer row">   
            <?php
            for ($i = 0; $i < count($edit_materail); $i++) { 
                                   // $type = substr($edit_materail['customer_attachment'][$i]['file_name'], strrpos($editvar['customer_attachment'][$i]['file_name'], '.') + 1);
             if($edit_materail[$i]['file_type'] == 'materials_upload') { ?>
                <div  class="uploadFileRow" id="file<?php echo $edit_materail[$i]['id']; ?>">
                    <div class="col-md-12">
                        <div class="extnsn-lst">
                            <p class="text-mb">
                                <?php echo $edit_materail[$i]['filename']; ?>
                            </p>
                            <p class="cross-btnlink">
                                    <a href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$editID.'/'.$edit_materail[$i]['filename']; ?>" download>
                                        <span><i class="fa fa-download" aria-hidden="true"></i></span>
                                    </a>
                                </p>
                            <p class="cross-btnlink">
                            <a href="javascript:void(0)" data-fieldname="marketing_materials" data-delfile="<?php echo $edit_materail[$i]['filename']; ?>" data-fileid="<?php echo $edit_materail[$i]['id']; ?>" data-flpath="<?php echo  FS_UPLOAD_PUBLIC_UPLOADS.'brand_profile/'.$editID.'/'.$edit_materail[$i]['filename']; ?>" class="edt_brnd_prfl">
                                <span>x</span>
                            </a>
                            </p>
                        </div>
                    </div>
                </div>
            <?php } 
        } ?>
    </div>
<?php } ?>

</div>
<div class="col-md-4">
    <div class="upload-card">
        <label for="exampleInputlogo">Additional Images </label>
        <div class="file-drop-area file-upload">
         <span class="fake-img">
            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_Draft_img_icon.svg" class="img-responsive">
        </span>
         <span class="file-msg">Select files to upload or Drag and drop file</span>
         <input type="file" class="file-input project-file-input brand_file_upload" multiple="" name="additional_upload[]" id="additional_file_input">
     </div>
 </div>
 <div class="uploadFileListContainer row additional_uploadFileListContain">                      
 </div>
                            <?php //echo "<pre>";print_r($edit_materail); 
                            if(sizeof($edit_materail) > 0){ ?>
                                <div class="uploadFileListContainer row">   
                                    <?php
                                    for ($i = 0; $i < count($edit_materail); $i++) { 
                                   // $type = substr($edit_materail['customer_attachment'][$i]['file_name'], strrpos($editvar['customer_attachment'][$i]['file_name'], '.') + 1);
                                       if($edit_materail[$i]['file_type'] == 'additional_upload') { ?>
                                        <div  class="uploadFileRow" id="file<?php echo $edit_materail[$i]['id']; ?>">
                                            <div class="col-md-12">
                                                <div class="extnsn-lst">
                                                    <p class="text-mb">
                                                        <?php echo $edit_materail[$i]['filename']; ?>
                                                    </p>
                                                    <p class="cross-btnlink">
                                                        <a href="<?php echo FS_PATH_PUBLIC_UPLOADS.'brand_profile/'.$editID.'/'.$edit_materail[$i]['filename']; ?>" download>
                                                            <span><i class="fa fa-download" aria-hidden="true"></i></span>
                                                        </a>
                                                    </p>
                                                    <p class="cross-btnlink">
                                                    <a href="javascript:void(0)" data-fieldname="additional_images" data-delfile="<?php echo $edit_materail[$i]['filename']; ?>" data-fileid="<?php echo $edit_materail[$i]['id']; ?>" data-flpath="<?php echo FS_UPLOAD_PUBLIC_UPLOADS.'brand_profile/'.$editID.'/'.$edit_materail[$i]['filename']; ?>" class="edt_brnd_prfl">
                                                        <span>x</span>
                                                    </a>
                                                        </p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-lg-12 ad_new_reqz">
                        <div class="submit-brnd-btn">
                            <input type="submit" class="submit cnsl-sbt" value="save" id="brand_profile_submit" name="brand_profile"/>
                            <a href="<?php echo base_url() ?>customer/brand_profiles" class="cnsl-sbt theme-cancel-btn">Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!--<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/bootstrap.min.js"></script>-->