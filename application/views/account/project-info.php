<link rel="stylesheet" type="text/css" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/jquery.fancybox.min.css">
<?php
$url = current_url();
$checkpage = explode('/', $url);
if($profile[0]['role'] == 'customer' && $profile[0]['is_saas'] == 1){
    $seen_by = 'admin_seen';
}elseif($profile[0]['role'] == 'customer' && $profile[0]['parent_id'] != 0){
    $seen_by = 'customer_seen';
}elseif($profile[0]['role'] == 'manager'){
     $seen_by = 'qa_seen';
}else{
    $seen_by = 'designer_seen';
}
?>
<style>
.flex-this{
    display: flex;
    align-items: center;
    justify-content: space-between;
}
</style>
<section class="project-info-page">
    <div class="content_section">  
        <?php if ($this->session->flashdata('message_error') != '') { ?>
            <div id="message" class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_error'); ?>
                </p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>
            <div id="message" class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_success'); ?>
                </p>
            </div>
        <?php }
        ?>
        <?php
        preg_match("/[^\/]+$/", $_SERVER['REQUEST_URI'], $matches);
        if (isset($_SERVER['HTTP_REFERER'])) {
            if (strpos($_SERVER['HTTP_REFERER'], 'account/request/project-info') == false) {
                $last_url = explode('?', $_SERVER['HTTP_REFERER'])[0];
                $_SESSION['lastUrl'] = $last_url;
            } else {
                if (isset($_SESSION['lastUrl'])) {
                    $last_url = $_SESSION['lastUrl'];
                } else {
                    $last_url = base_url() . '/qa/dashboard';
                }
            }
        } else {
            $last_url = isset($_SESSION['lastUrl']) ? $_SESSION['lastUrl'] : '';
        }
//        echo $profile[0]['role'].$s_user_permsn['assigndes_to_project'];
        ?>
        <div class="content_wrapper">
            <div class="rheight-xt d-progress">
                <h3 class="head-b">
                    <p class="savecols-col left">
                        <a href="<?php echo $last_url; ?>?status=<?php echo $last_word = $matches[0]; ?>" class="backbtns">
                            <i class="icon-gz_back_icon"></i>
                        </a>
                    </p>
                    <?php echo $data[0]['title']; ?>
                </h3>
                <div class="flex-this">
                        <?php if (($data[0]['status'] != "approved" && $data[0]['status'] != "hold") && ($profile[0]['role'] == 'manager' && $s_user_permsn['upload_draft'] != 0) || ($profile[0]['role'] == 'customer' && $profile[0]['is_saas'] == 1) || ($profile[0]['role'] == 'designer')) { ?>
                            <a href="javascript:void(0)" class="adddesinger upload" data-toggle="modal" data-target="#Addfiles">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/trail.svg"><span> Upload Draft</span>
                            </a>
                        <?php }
                        if (($profile[0]['role'] == 'manager' && $s_user_permsn['assigndes_to_project'] != 0) || ($profile[0]['role'] == 'customer' && $profile[0]['parent_id'] == 0 )) { ?>
                                    <a href="javascript:void(0)" class="adddesinger mrl_10" data-toggle="modal" data-target="#AddDesigner" data-requestid="<?php echo $data[0]['id']; ?>" data-designerid= "<?php echo $data[0]['designer_id']; ?>">
                                       <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/assign_designer.svg" class="img-responsive"> <span>Assign Designer</span>
                                   </a>
                        <?php } 
                        if(($profile[0]['role'] == 'manager' && $s_user_permsn["change_project_status"] != 0) || ($profile[0]['role'] == 'customer' && $profile[0]['parent_id'] == 0)){ ?>
                        <div class="srd-col status_opt">
                            <input type="hidden" value="<?php echo $data[0]['id']; ?>" id="request_id_status">
                            <div class="tool-hover">
                                <select class="form-control statusselect select-b" onchange="statuschange(<?php echo $data[0]['id']; ?>, this)" <?php //echo ($cancel_subscription == 1) ? "disabled" : ""; ?>>
                                    <option disabled value="" selected>Change Status</option>
                                    <option value="assign"<?php
                                    if ($data[0]['status_admin'] == "assign") {
                                        echo 'disabled ' . ' selected';
                                    }
                                    ?>>In Queue</option>
                                    <option value="active" <?php
                                    if ($data[0]['status_admin'] == "active") {
                                        echo 'disabled ' . ' selected';
                                    }
                                    ?>>Design In Progress</option>
                                    <?php  if($s_user_permsn["canapprove_projct"] != 0){ ?>
                                    <option value="disapprove" <?php
                                    if ($data[0]['status_admin'] == "disapprove") {
                                        echo 'disabled ' . ' selected';
                                    }
                                    ?>>Revision In Progress</option>
                                    <?php } ?>
                                            <?php if ($data[0]['status_admin'] == "hold") { ?>
                                        <option value="unhold" data-status="false">Unhold</option>   
                                    <?php } ?>
                                    <option value="hold" <?php
                                    if ($data[0]['status_admin'] == "hold") {
                                        echo 'disabled ' . ' selected';
                                    }
                                    ?> data-status="true">Hold</option> 
                                    <option value="pendingrevision" <?php
                                    if ($data[0]['status_admin'] == "pendingrevision") {
                                        echo 'disabled ' . ' selected';
                                    }
                                    ?> >Pending Review</option>
                                    <option value="checkforapprove" <?php
                                    if ($data[0]['status_admin'] == "checkforapprove") {
                                        echo 'disabled ' . ' selected';
                                    }
                                    ?> >Review your design</option>
                                <?php  if($s_user_permsn["canapprove_projct"] != 0){ ?>
                                    <option value="approved" <?php
                                    if ($data[0]['status_admin'] == "approved") {
                                        echo 'disabled ' . ' selected';
                                    }
                                    ?>>Completed</option>
                                <?php } ?>
                                    <option value="cancel" <?php
                                    if ($data[0]['status_admin'] == "cancel") {
                                        echo 'disabled ' . ' selected';
                                    }
                                    ?>>Cancelled</option>
                                </select>
                                <?php if ($cancel_subscription == 1) { ?>
                                    <div class="tool-text">This customer subscription is cancelled, You need to reactivate it to change project status.</div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                <?php  
                if($profile[0]['role'] == 'customer' && $profile[0]['parent_id'] != 0 ){ ?>
                <div class="focusuyt">
                    <ul class="h-c-r">
                        <li class="dropdown btn-d-status curnt_req_status <?php echo $data['customer_variables']['color']; ?>">
                            <a href="javascript:void(0)">
                                <?php echo $data['customer_variables']['status']; ?>
                                <?php if ($data[0]['status'] != "assign" && $data[0]['status'] != "draft" && $data[0]['status'] != "pending") { ?>
                                    <span> <i class="fas fa-caret-down"></i></span>
                                <?php } ?>
                            </a>
                            <?php
                            if ($data[0]['status'] != "assign" && $data[0]['status'] != "draft" && $data[0]['status'] != "pending") {
                                if ($data[0]['status'] != "cancel" && $data[0]['status'] != "approved") {
                                    ?>
                                    <ul class="dropdown-menu">
                                        <?php if ($data[0]['status'] != "hold") { ?>
                                            <li>
                                                <a class="btn-d project_on_hold" href="javascript:void(0)" data-request_id="<?php echo $data[0]['id']; ?>" data-status="true">Put on hold<span></span></a>
                                            </li>
                                        <?php } else { ?>
                                            <li>
                                                <a class="btn-d project_on_hold" href="javascript:void(0)" data-request_id="<?php echo $data[0]['id']; ?>" data-status="false">Put on unhold<span></span></a>
                                            </li>
                                        <?php }
                                        ?>
                                        <?php if (($data[0]['status'] == "checkforapprove" || $data[0]['status'] == "active" || $data[0]['status'] == "disapprove" || $data[0]['status'] == "hold") && $s_user_permsn["canapprove_projct"] != 0) { ?>
                                            <li>
                                                <a class="btn-d checkfeedbacksubmit" data-current_status="<?php echo $data[0]['status']; ?>" data-reqid="<?php echo $data[0]['id'] ?>" href="<?php echo base_url() ?>account/request/markAsCompleted/<?php echo $request_id; ?>">Mark as completed<span></span></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } else { ?>
                                    <?php if ($data[0]['status'] == "approved" && $main_user_data[0]['is_cancel_subscription'] != 1) { ?>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="btn-d" href="<?php echo base_url() ?>account/request/markAsInProgress/<?php echo $request_id; ?>">Move to progress<span></span></a>
                                            </li>
                                        </ul>
                                    <?php } ?>
                                    <?php
                                }
                            }
                            ?>
                        </li>
                    </ul>
                </div>
                <?php } ?>
            </div>
            <!--design-draft-header ddnos mobile-->
            <div class="">
                <div class="row flex-show">
                    <div class="col-sm-12">
                        <ul id="project_status_check" class="list-unstyled list-header-blog" role="tablist" style="border:none;">
                            <li class="active" id="1"><a data-toggle="tab" href="#designs_request_tab" role="tab">Project Drafts </a></li>
                            <li class="" id="2"><a class="nav-link tabmenu" data-toggle="tab" href="#inprogressrequest" role="tab">Project Information </a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="project-row-qq1">
                <!-- class="orb-xb" -->
                <div>
                    <div class="orb-xb-col right">
                        <div class="tab-content">
                            <div data-group="1" data-loaded="" class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                                <div class="orbxb-img-xx3s two-can">
                                    <div class="row-designdraftnew" data-step="3" data-intro="This is where you will see the designs for this project. Approve, ask for a revision, or share the designs with others" data-position='right' data-scrollTo='tooltip'>
                                        <?php 
                                        if ($data[0]['designer_attachment']): ?>
                                            <ul class="list-unstyled clearfix list-designdraftnew">
                                                <?php
                                                $divide = (count($data[0]['designer_attachment'])) % 3;
                                                //echo "divide".$divide;
                                                for ($i = 0; $i < count($data[0]['designer_attachment']); $i++) :
                                                    //echo $i."check"."<br/>";
                                                    if ($divide == 2) {
                                                        if ($i <= 1) {
                                                            $class = "col-md-6 big_box";
                                                        } else {
                                                            $class = "col-lg-4 col-md-6";
                                                        }
                                                    }
                                                    else {
                                                        $class = "col-lg-4 col-md-6";
                                                    }
                                                    $ap = $data[0]['designer_attachment'][$i]['status'];
                                                    if($allroles['user'] == 'customer'){
                                                    if (($ap != "pending") && ($ap != "Reject")) {
                                                        ?>
                                                        <li class="<?php echo $class; ?>">
                                                            <div class="figimg-one">
                                                                <div class="draft-go">
                                                                    <?php if ($data[0]['designer_attachment'][$i]['status'] == "Approve" && $data[0]['status'] == "approved") { ?>
                                                                        <span class="notestxx">
                                                                            <p class="text-right">
                                                                                <span class="notapp-roved">Approved</span>
                                                                            </p>
                                                                        </span>
                                                                    <?php } ?>
                                                                    <?php if ($data[0]['designer_attachment'][$i]['customer_seen'] == 0) { ?>
                                                                        <div class="status_label">
                                                                            <svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve">
                                                                            <g>
                                                                            <path fill-rule="evenodd" fill="#A02302" d="M1.9,71.1l4.5,6.7V60.6L1.9,71.1z" />
                                                                            <path fill-rule="evenodd" fill="#A02302" d="M71.4,2.2l6.7,4.5H60.9L71.4,2.2z" />
                                                                            <path fill-rule="evenodd" fill="#F15A29" d="M2,46.5L47,2.2l24.5,0L1.9,70.9L2,46.5z" />
                                                                            </g>
                                                                            </svg>
                                                                            <span class="adnewpp">New Upload</span>
                                                                        </div>
                                                                    <?php } ?>


                                                                    <?php
                                                                    $type = substr($data[0]['designer_attachment'][$i]['file_name'], strrpos($data[0]['designer_attachment'][$i]['file_name'], '.') + 1);
                                                                    $filename = $data[0]['designer_attachment'][$i]['file_name'];
                                                                    $imagePreFix = substr($filename, 0, strpos($filename, "."));
                                                                    ?>
                                                                    <div class="draft_img_wrapper">
                                                                        <img src="<?php echo imageUrl_SAAS($type, $data[0]['id'], $data[0]['designer_attachment'][$i]['file_name']); ?>" class="img-responsive" />
                                                                    </div>
                                                                    <?php if($s_user_permsn["review_design"] != 0){ ?>
                                                                        <div class="view-share">
                                                                            <a class="view-draft" href="<?php echo base_url() . "account/request/project_image_view/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $request_id; ?>">
                                                                                <i class="fas fa-eye"></i> View
                                                                            </a>
                                                                            <?php if ($show_filesharing == 1) { ?>
                                                                                <a href="javascript:void(0)" class="share_draft" id="public_share" data-toggle="modal" data-target="#Sharerequest" data-proid="<?php echo isset($data[0]['id']) ? $data[0]['id'] : ''; ?>" data-draftid="<?php echo $data[0]['designer_attachment'][$i]['id']; ?>" data-draftname="<?php echo $imagePreFix; ?>">
                                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/share.svg"> Share
                                                                                </a>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>


                                                                <div class="tm-heading">
                                                                    <div class="iconwithtitle">
                                                                        <?php if($s_user_permsn["review_design"] != 0){ ?>
                                                                            <a href="<?php echo base_url() . "account/request/project_image_view/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $request_id; ?>">
                                                                                <h3>
                                                                                    <?php echo $imagePreFix; ?>
                                                                                </h3>
                                                                            </a>
                                                                        <?php }else{ ?>
                                                                            <h3>
                                                                                <?php echo $imagePreFix; ?>
                                                                            </h3>
                                                                        <?php } ?>
                                                                        
                                                                    </div>
                                                                    <div class="chatbox-add">
                                                                        <p>
                                                                            <?php echo $data[0]['designer_attachment'][$i]['created']; ?>
                                                                        </p>
                                                                        <div class="fev_copy_msg">
                                                                            <!-- mark as favorite code start from here  -->
                                                                            <?php
                                                                            $projectId = $data[0]['designer_attachment'][$i]['id'];
                                                                            $requestedID = $data[0]['designer_attachment'][$i]['request_id'];
                                                                            $customer_id = $data[0]['customer_id'];
                                                                            $this->db->select('mark_as_fav');
                                                                            $this->db->from('request_files');
                                                                            $condArr = array('id' => $projectId, 'request_id' => $requestedID);
                                                                            $this->db->where($condArr);
                                                                            $sql = $this->db->get();
                                                                            $reslt = $sql->result();

                                                                            if ($reslt[0]->mark_as_fav == 1) {
                                                                                $class_add = 'active active-2 active-3';
                                                                                $faClass = 'fas fa-heart';
                                                                                $markAs = 'Already marked';
                                                                            } else {
                                                                                $class_add = '';
                                                                                $faClass = 'far fa-heart';
                                                                                $markAs = 'Mark as favorite';
                                                                            }
                                                                            ?>  
                                                                            <div data-toggle="tooltip" data-placement="bottom" title="Add to Favorites" class="click <?php echo $class_add; ?>" id="cstm_list_<?php echo $i; ?>" data-id="<?php echo $projectId; ?>" data-requested-id="<?php echo $requestedID; ?>">
                                                                                <span  class="cstm_span <?php echo $faClass; ?>" title="<?php echo $markAs; ?>">
                                                                                    <svg  version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                          viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve">
                                                                                    <g>
                                                                                    <path d="M475.7,164.6C469.2,92,417.7,39.3,353.3,39.3c-42.8,0-82.1,23.1-104.2,60.1c-21.9-37.5-59.5-60.1-101.7-60.1
                                                                                          c-64.3,0-115.7,52.7-122.4,125.4c-0.5,3.2-2.7,20.1,3.9,47.6C38.3,252,60,288.1,91.5,316.8l157.5,142.9l160.2-142.9
                                                                                          c31.5-28.6,53.2-64.7,62.6-104.5C478.4,184.7,476.3,167.8,475.7,164.6z"/>
                                                                                    <path fill="none" stroke="#000000" stroke-width="24" stroke-miterlimit="10" d="M485,162.2c-6.8-75.6-60.5-130.6-127.4-130.6
                                                                                          c-44.6,0-85.5,24-108.5,62.6c-22.8-39.1-62-62.6-105.8-62.6c-67,0-120.5,54.8-127.4,130.6c-0.5,3.3-2.8,20.9,4,49.6
                                                                                          c9.7,41.4,32.3,78.9,65.2,108.8l164,148.8l166.8-148.8c32.8-29.8,55.4-67.4,65.2-108.8C487.8,183.1,485.6,165.5,485,162.2z"/>
                                                                                    </g>
                                                                                    </svg> 
                                                                                </span>

                                                                            </div>
                                                                            <!-- <?php echo $imagePreFix; ?> -->
                                                                            <div data-toggle="tooltip" data-placement="bottom" title="Copy" class="CoPyOrMove" data-id="<?php echo $requestedID; ?>"  title="Copy File" data-file ="<?php echo $imagePreFix; ?>" data-projid ="<?php echo $projectId; ?>" >
                                                                                <!-- <i class="far fa-copy"></i> -->
                                                                                <svg  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                      viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve"> <g id="Text-files"><path d="M421.7,71.4h-31c-0.6,0-1.2,0.2-1.8,0.4V39.2c0-21.6-18-39.2-40.1-39.2H79.8C57.7,0,39.7,17.6,39.7,39.2v366
                                                                                      c0,21.6,18,39.2,40.1,39.2H127v17.7c0,20.9,17.3,37.8,38.6,37.8h256.1c21.3,0,38.6-17,38.6-37.8V109.3
                                                                                      C460.3,88.4,443,71.4,421.7,71.4z M55.6,405.2v-366c0-12.9,10.9-23.4,24.3-23.4h268.9c13.4,0,24.3,10.5,24.3,23.4v366
                                                                                      c0,12.9-10.9,23.4-24.3,23.4H79.8C66.4,428.6,55.6,418.1,55.6,405.2z M444.4,462.2c0,12.1-10.2,22-22.7,22H165.6
                                                                                      c-12.5,0-22.7-9.9-22.7-22v-17.7h205.9c22.1,0,40.1-17.6,40.1-39.2V86.9c0.6,0.1,1.2,0.4,1.8,0.4h31c12.5,0,22.7,9.9,22.7,22V462.2
                                                                                      z"/></g></svg>

                                                                            </div>

                                                                            <!-- mark as favorite code Ends here -->
                                                                            <a data-toggle="tooltip" data-placement="bottom" title="View Image" href="<?php echo base_url() . "account/request/project_image_view/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $request_id; ?>">

                                                                                <i class="icon-gz_message_icon"></i>
                                                                                <?php for ($j = 0; $j < count($file_chat_array); $j++): ?>
                                                                                    <?php if ($file_chat_array[$j]['id'] == $data[0]['designer_attachment'][$i]['id']): ?>
                                                                                        <span class="numcircle-box-01">
                                                                                            <?php echo $file_chat_array[$j]['count']; ?></span>
                                                                                    <?php endif; ?>
                                                                                <?php endfor; ?>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php }}else{ ?>
                                                        <li class="<?php echo $class; ?>">
                                                            <div class="figimg-one">
                                                                <div class="draft-go">
                                                                    <?php if ($data[0]['designer_attachment'][$i]['status'] == "Approve" && $data[0]['status'] == "approved") { ?>
                                                                        <span class="notestxx">
                                                                            <p class="text-right">
                                                                                <span class="notapp-roved">Approved</span>
                                                                            </p>
                                                                        </span>
                                                                    <?php } ?>
                                                                    <?php if ($data[0]['designer_attachment'][$i]['customer_seen'] == 0) { ?>
                                                                        <div class="status_label">
                                                                            <svg version="1.1" baseProfile="tiny" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve">
                                                                            <g>
                                                                            <path fill-rule="evenodd" fill="#A02302" d="M1.9,71.1l4.5,6.7V60.6L1.9,71.1z" />
                                                                            <path fill-rule="evenodd" fill="#A02302" d="M71.4,2.2l6.7,4.5H60.9L71.4,2.2z" />
                                                                            <path fill-rule="evenodd" fill="#F15A29" d="M2,46.5L47,2.2l24.5,0L1.9,70.9L2,46.5z" />
                                                                            </g>
                                                                            </svg>
                                                                            <span class="adnewpp">New Upload</span>
                                                                        </div>
                                                                    <?php } ?>


                                                                    <?php
                                                                    $type = substr($data[0]['designer_attachment'][$i]['file_name'], strrpos($data[0]['designer_attachment'][$i]['file_name'], '.') + 1);
                                                                    $filename = $data[0]['designer_attachment'][$i]['file_name'];
                                                                    $imagePreFix = substr($filename, 0, strpos($filename, "."));
                                                                    ?>
                                                                    <div class="draft_img_wrapper">
                                                                        <img src="<?php echo imageUrl_SAAS($type, $data[0]['id'], $data[0]['designer_attachment'][$i]['file_name']); ?>" class="img-responsive" />
                                                                    </div>
                                                                    <?php if($s_user_permsn["review_design"] != 0){ ?>
                                                                    <div class="view-share">
                                                                        <a class="view-draft" href="<?php echo base_url() . "account/request/project_image_view/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $request_id; ?>">
                                                                            <i class="fas fa-eye"></i> View
                                                                        </a>
                                                                        <?php //if ($show_filesharing == 1) { ?>
<!--                                                                            <a href="javascript:void(0)" class="share_draft" id="public_share" data-toggle="modal" data-target="#Sharerequest" data-proid="<?php echo isset($data[0]['id']) ? $data[0]['id'] : ''; ?>" data-draftid="<?php echo $data[0]['designer_attachment'][$i]['id']; ?>" data-draftname="<?php echo $imagePreFix; ?>">
                                                                                <img src="<?php //echo FS_PATH_PUBLIC_ASSETS; ?>img/share.svg"> Share
                                                                            </a>-->
                                                                        <?php //} ?>
                                                                    </div>
                                                                    <?php } ?>
                                                                </div>


                                                                <div class="tm-heading">
                                                                    <div class="iconwithtitle">
                                                                        <?php if($s_user_permsn["review_design"] != 0){ ?>
                                                                        <a href="<?php echo base_url() . "account/request/project_image_view/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $request_id; ?>">
                                                                            <h3>
                                                                                <?php echo $imagePreFix; ?>
                                                                            </h3>
                                                                        </a>
                                                                        <?php }else{ ?>
                                                                            <h3>
                                                                                <?php echo $imagePreFix; ?>
                                                                            </h3>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="chatbox-add">
                                                                        <p>
                                                                            <?php echo $data[0]['designer_attachment'][$i]['created']; ?>
                                                                        </p>
                                                                        <div class="fev_copy_msg">
                                                                            <!-- mark as favorite code start from here  -->
                                                                            <?php
                                                                            $projectId = $data[0]['designer_attachment'][$i]['id'];
                                                                            $requestedID = $data[0]['designer_attachment'][$i]['request_id'];
                                                                            $customer_id = $data[0]['customer_id'];
                                                                            $this->db->select('mark_as_fav');
                                                                            $this->db->from('request_files');
                                                                            $condArr = array('id' => $projectId, 'request_id' => $requestedID);
                                                                            $this->db->where($condArr);
                                                                            $sql = $this->db->get();
                                                                            $reslt = $sql->result();

                                                                            if ($reslt[0]->mark_as_fav == 1) {
                                                                                $class_add = 'active active-2 active-3';
                                                                                $faClass = 'fas fa-heart';
                                                                                $markAs = 'Already marked';
                                                                            } else {
                                                                                $class_add = '';
                                                                                $faClass = 'far fa-heart';
                                                                                $markAs = 'Mark as favorite';
                                                                            }
                                                                            ?>  
                                                                            <div data-toggle="tooltip" data-placement="bottom" title="Add to Favorites" class="click <?php echo $class_add; ?>" id="cstm_list_<?php echo $i; ?>" data-id="<?php echo $projectId; ?>" data-requested-id="<?php echo $requestedID; ?>">
                                                                                <span  class="cstm_span <?php echo $faClass; ?>" title="<?php echo $markAs; ?>">
                                                                                    <svg  version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                          viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve">
                                                                                    <g>
                                                                                    <path d="M475.7,164.6C469.2,92,417.7,39.3,353.3,39.3c-42.8,0-82.1,23.1-104.2,60.1c-21.9-37.5-59.5-60.1-101.7-60.1
                                                                                          c-64.3,0-115.7,52.7-122.4,125.4c-0.5,3.2-2.7,20.1,3.9,47.6C38.3,252,60,288.1,91.5,316.8l157.5,142.9l160.2-142.9
                                                                                          c31.5-28.6,53.2-64.7,62.6-104.5C478.4,184.7,476.3,167.8,475.7,164.6z"/>
                                                                                    <path fill="none" stroke="#000000" stroke-width="24" stroke-miterlimit="10" d="M485,162.2c-6.8-75.6-60.5-130.6-127.4-130.6
                                                                                          c-44.6,0-85.5,24-108.5,62.6c-22.8-39.1-62-62.6-105.8-62.6c-67,0-120.5,54.8-127.4,130.6c-0.5,3.3-2.8,20.9,4,49.6
                                                                                          c9.7,41.4,32.3,78.9,65.2,108.8l164,148.8l166.8-148.8c32.8-29.8,55.4-67.4,65.2-108.8C487.8,183.1,485.6,165.5,485,162.2z"/>
                                                                                    </g>
                                                                                    </svg> 
                                                                                </span>

                                                                            </div>
                                                                            <!-- <?php echo $imagePreFix; ?> -->
                                                                            <div data-toggle="tooltip" data-placement="bottom" title="Copy" class="CoPyOrMove" data-id="<?php echo $requestedID; ?>"  title="Copy File" data-file ="<?php echo $imagePreFix; ?>" data-projid ="<?php echo $projectId; ?>" >
                                                                                <!-- <i class="far fa-copy"></i> -->
                                                                                <svg  version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                                                      viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve"> <g id="Text-files"><path d="M421.7,71.4h-31c-0.6,0-1.2,0.2-1.8,0.4V39.2c0-21.6-18-39.2-40.1-39.2H79.8C57.7,0,39.7,17.6,39.7,39.2v366
                                                                                      c0,21.6,18,39.2,40.1,39.2H127v17.7c0,20.9,17.3,37.8,38.6,37.8h256.1c21.3,0,38.6-17,38.6-37.8V109.3
                                                                                      C460.3,88.4,443,71.4,421.7,71.4z M55.6,405.2v-366c0-12.9,10.9-23.4,24.3-23.4h268.9c13.4,0,24.3,10.5,24.3,23.4v366
                                                                                      c0,12.9-10.9,23.4-24.3,23.4H79.8C66.4,428.6,55.6,418.1,55.6,405.2z M444.4,462.2c0,12.1-10.2,22-22.7,22H165.6
                                                                                      c-12.5,0-22.7-9.9-22.7-22v-17.7h205.9c22.1,0,40.1-17.6,40.1-39.2V86.9c0.6,0.1,1.2,0.4,1.8,0.4h31c12.5,0,22.7,9.9,22.7,22V462.2
                                                                                      z"/></g></svg>

                                                                            </div>

                                                                            <!-- mark as favorite code Ends here -->
                                                                            <a data-toggle="tooltip" data-placement="bottom" title="View Image" href="<?php echo base_url() . "account/request/project_image_view/" . $data[0]['designer_attachment'][$i]['id'] . "?id=" . $request_id; ?>">

                                                                                <i class="icon-gz_message_icon"></i>
                                                                                <?php for ($j = 0; $j < count($file_chat_array); $j++): ?>
                                                                                    <?php if ($file_chat_array[$j]['id'] == $data[0]['designer_attachment'][$i]['id']): ?>
                                                                                        <span class="numcircle-box-01">
                                                                                            <?php echo $file_chat_array[$j]['count']; ?></span>
                                                                                    <?php endif; ?>
                                                                                <?php endfor; ?>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li> 
                                                   <?php } 
                                                    endfor; ?>
                                            </ul>
                                        <?php else: ?>
                                            <div class="figimg-one no_img" style="border:none;">
                                                <?php if ($data[0]['status'] == 'draft') { ?>
                                                    <h3 class="head-b draft_no">
                                                        Design Project is in Draft
                                                    </h3>
                                                    <a class="btn btn-x btn-red draft_btn2" href="<?php echo base_url(); ?>account/request/add_new_request?reqid=<?php echo $data[0]['id']; ?>">Publish</a>
                                                <?php } elseif ($data[0]['status'] == 'assign') { ?>
                                                    <h3 class="head-b draft_no">
                                                        Design Project is in queue
                                                    </h3>
                                                <?php } elseif ($data[0]['status'] == 'active') { ?>
                                                    <h3 class="head-b draft_no">
                                                        Designer is working on the designs
                                                    </h3>
                                                <?php } ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div data-group="1" data-loaded="" data-search-text="" class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">
                                <div class="right-ins-edit">
                                    <?php if ($data[0]['status'] != "approved" && $main_user_data[0]['is_cancel_subscription'] != 1) { ?>
                                        <a class="" href="<?php echo base_url(); ?>account/request/add_new_request?reqid=<?php echo $data[0]['id']; ?>">
                                            <i class="icon-gz_edit_icon"></i> Edit
                                        </a>
                                    <?php } ?>
                                </div>
                                <div class="project-row-qq1">
                                    <div class="designdraft-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="designdraft-views">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x2"><span class="prowest"></span> Project Title</label>
                                                        <div class="review-textcolww">
                                                            <?php echo $data[0]['title']; ?>
                                                        </div>
                                                    </div>
                                                    <?php if (isset($data[0]['business_industry']) && $data[0]['business_industry'] != '') { ?>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"><span class="prowest"></span> Target Market</label>
                                                            <div class="review-textcolww">
                                                                <?php echo $data[0]['business_industry']; ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (!empty($data[0]['design_dimension'])) { ?>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"><span class="prowest"></span> Design Dimension</label>
                                                            <div class="review-textcolww">
                                                                <?php echo $data[0]['design_dimension']; ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (!empty($data[0]['design_colors'])) { ?>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"><span class="prowest"></span> Color Pallete</label>
                                                            <!-- Color Plates -->
                                                            <div class="form-group">
                                                                <?php
                                                                if ($data[0]['design_colors']) {
                                                                    $color = explode(",", $data[0]['design_colors']);
                                                                }
                                                                ?>
                                                                <p class="space-a"></p>
                                                                <?php if ($data[0]['design_colors'] && in_array("Let Designer Choose", $color)) { ?>
                                                                    <div class="radio-check-kk1">
                                                                        <label>
                                                                            <div class="">
                                                                                <span class="review-colww left">
                                                                                    <span class="review-textcolww">
                                                                                        Let Designer Choose for me.</span>
                                                                                </span>
                                                                            </div>
                                                                        </label>
                                                                    </div>
                                                                    <?php
                                                                } else {
                                                                    $flag = 0;
                                                                    ?>
                                                                    <div class="lessrows">
                                                                        <div class="right-lesscol noflexes">
                                                                            <div class="plan-boxex-xx6 clearfix">
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("blue", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id1" class="radio-box-xx2">
                                                                                        <div class="check-main-xx3">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-1.png" class="img-responsive">
                                                                                            </figure>
                                                                                            <h3 class="sub-head-c text-center">Blue
                                                                                            </h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("aqua", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id2" class="radio-box-xx2">
                                                                                        <span class="checkmark"></span>
                                                                                        <div class="check-main-xx3">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-2.png" class="img-responsive">
                                                                                            </figure>
                                                                                            <h3 class="sub-head-c text-center">Auqa</h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("green", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id3" class="radio-box-xx2">
                                                                                        <div class="check-main-xx3">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-3.png" class="img-responsive">
                                                                                            </figure>
                                                                                            <h3 class="sub-head-c text-center">Greens</h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("purple", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id4" class="radio-box-xx2">
                                                                                        <div class="check-main-xx3">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-4.png" class="img-responsive">
                                                                                            </figure>
                                                                                            <h3 class="sub-head-c text-center">Purple</h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("pink", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id5" class="radio-box-xx2">
                                                                                        <div class="check-main-xx3" onclick="funcheck()">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-5.png" class="img-responsive">
                                                                                            </figure>
                                                                                            <h3 class="sub-head-c text-center">Pink</h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("black", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id6" class="radio-box-xx2">
                                                                                        <div class="check-main-xx3" onclick="funcheck()">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-6.png" class="img-responsive">
                                                                                            </figure>
                                                                                            <h3 class="sub-head-c text-center">Pink</h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                                <?php
                                                                                if ($data[0]['design_colors'] && in_array("orange", $color)) {
                                                                                    $flag = 1;
                                                                                    ?>
                                                                                    <label for="id7" class="radio-box-xx2">
                                                                                        <div class="check-main-xx3" onclick="funcheck()">
                                                                                            <figure class="chkimg">
                                                                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-7.png" class="img-responsive">
                                                                                            </figure>
                                                                                            <h3 class="sub-head-c text-center">Pink</h3>
                                                                                        </div>
                                                                                    </label>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <p class="space-a"></p>
                                                                    <?php if ($flag != 1): ?>
                                                                        <div class="radio-check-kk1">
                                                                            <label>
                                                                                <div class="">
                                                                                    <span class="review-colww left">
                                                                                        <span class="review-textcolww">
                                                                                            <?php
                                                                                            echo $data[0]['design_colors'];
                                                                                            ?></span>
                                                                                    </span>
                                                                                </div>
                                                                            </label>
                                                                        </div>
                                                                    <?php endif ?>
                                                                <?php }
                                                                ?>
                                                                <p class="space-a"></p>
                                                            </div>
                                                            <!-- Color Plates End -->
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (!empty($data[0]['designer'])) { ?>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"><span class="prowest"></span> Design Software Requirement</label>
                                                            <div class="review-textcolww">
                                                                <?php echo $data[0]['designer']; ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if (!empty($data[0]['color_pref'])) { ?>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"><span class="prowest"></span> Color pallete</label>
                                                            <div class="review-textcolww">
                                                                <?php
                                                                echo ucfirst($data[0]['color_pref']) . "<br/>";
                                                                if ($data[0]['color_values']) {
                                                                    ?>
                                                                    <span class="color_theme">
                                                                        <?php
                                                                        echo $data[0]['color_values'];
                                                                    }
                                                                    ?>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    <?php }if ($data[0]['deliverables'] != '') { ?>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"><span class="prowest"></span> Deliverables</label>
                                                            <div class="review-textcolww">
                                                                <?php echo $data[0]['deliverables']; ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x2"><span class="prowest"></span> Category</label>
                                                        <div class="review-textcolww">
                                                            <span>
                                                                <?php
                                                                echo isset($data['cat_name']) ? $data['cat_name'] : $data[0]['category'];
                                                                if ($data[0]['logo_brand'] != '') {
                                                                    ?></span><span>
                                                                    <?php
                                                                    echo $data[0]['logo_brand'];
                                                                } elseif ($data['subcat_name'] != '') {
                                                                    ?></span><span>
                                                                        <?php
                                                                        echo $data['subcat_name'];
                                                                    }
                                                                    ?></span></div>
                                                    </div>
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x2"><span class="prowest"></span> Brand Profile</label>
                                                        <div class="review-textcolww">
                                                            <?php if ($branddata[0]['brand_name'] != '') { ?>
                                                                <a class="showbrand_profile" data-toggle="modal" data-target="#brand_profile"><i class="fa fa-user"></i>
                                                                    <?php echo $branddata[0]['brand_name']; ?></a>
                                                            <?php } else { ?>
                                                                No brand profile selected
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <!--******questions & answer*******-->

                                                    <?php
                                                    if (!empty($request_meta)) {
                                                        foreach ($request_meta as $qkey => $qval) {
                                                            if ($qval['question_label'] == 'DESIGN DIMENSIONS' || $qval['question_label'] == 'DESCRIPTION') {
                                                                if ($qval['question_label'] == 'DESIGN DIMENSIONS') {
                                                                    if ($qval['answer'] != '') {
                                                                        $qval['answer'] = $qval['answer'];
                                                                    } else {
                                                                        $qval['answer'] = $data[0]['design_dimension'];
                                                                    }
                                                                }
                                                                if ($qval['question_label'] == 'DESCRIPTION') {
                                                                    if ($qval['answer'] != '') {
                                                                        $qval['answer'] = $qval['answer'];
                                                                    } else {
                                                                        $qval['answer'] = $data[0]['description'];
                                                                    }
                                                                }
                                                            } else {
                                                                $qval['answer'] = $qval['answer'];
                                                            }
                                                            // $asdj = substr($qval['answer'],97); 

                                                             //echo "<prE>"; print_r($qval['answer']); exit;  
                                                            if(isset($qval['answer']) && $qval['answer'] != ''){
                                                                if(strlen(htmlspecialchars($qval['answer'])) > 100){
                                                                    $answer = substr(htmlspecialchars($qval['answer']),0,97).'<a class="expand_txt read_more" href="javascript:void(0);">Read More</a><div class="read_more_txt hide">'.substr(htmlspecialchars($qval['answer']),97).'</div><a class="expand_txt read_less hide" href="javascript:void(0);">Read Less</a>';
                                                                }else{
                                                                    $answer = htmlspecialchars($qval['answer']);
                                                                }
                                                            }else{
                                                                $answer = "N/A";
                                                            }
                                                            /*                                                             * ****************Description excluded************************** */
                                                            if ($qval['question_label'] != 'DESCRIPTION') {
                                                                ?>
                                                                <div class="form-group goup-x1">
                                                                    <label class="label-x2"> <?php echo $qval['question_label']; ?></label>
                                                                    <div class="review-textcolww">
                                                                         <?php echo $answer; ?>
                                                                    </div>
                                                                </div>      
                                                            <?php } else { ?>
                                                                <div class="form-group goup-x1" style="width:100%;">
                                                                    <label class="label-x2"> <?php echo $qval['question_label']; ?></label>
                                                                    <div class="review-textcolww">
                                                                        <pre> <?php echo $answer; ?></pre>
                                                                    </div>
                                                                </div> 
                                                                <?php
                                                            }
                                                        }
                                                    } else {
                                                        ?>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"> Design Dimension</label>
                                                            <div class="review-textcolww">
                                                                <?php echo isset($data[0]['design_dimension']) ? $data[0]['design_dimension'] : 'N/A'; ?>
                                                            </div>
                                                        </div>
                                                        <div class="form-group goup-x1">
                                                            <label class="label-x2"> Description</label>
                                                            <div class="review-textcolww">
                                                                <pre><?php echo isset($data[0]['description']) ? (strlen(htmlspecialchars($data[0]['description'])) > 100)?substr(htmlspecialchars($data[0]['description']),0,97).'project-info<a class="expand_txt read_more" href="javascript:void(0);">Read More</a><div class="read_more_txt hide">'.substr(htmlspecialchars($data[0]['description']),97).'</div> <a class="expand_txt read_less hide" href="javascript:void(0);">Read Less</a> ':htmlspecialchars($data[0]['description']) : 'N/A'; ?></pre>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                    ?>
                                                    <!--*********samples************-->
                                                    <?php if (!empty($sampledata)) { ?>
                                                        <div class="clearfix"></div>
                                                        <div class="design-ch-pre">
                                                            <h3 class="head-b base-heading request_heading">Design choice preferences</h3>
                                                            <ul class="list-unstyled list-accessimg">
                                                                <?php
                                                                //echo "<pre/>";print_R($sampledata);  
                                                                foreach ($sampledata as $skey => $sval) {
                                                                    ?>
                                                                    <li>
                                                                        <div class="accimgbx33">
                                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'samples/' . $sval['sample_id'] . '/' . $sval['sample_material']; ?>" />
                                                                            <!--                                                            <div class="viewDelete">
                                                                                                                                            <a href="<?php //echo FS_PATH_PUBLIC_UPLOADS_REQUESTS_SAAS . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name'];       ?>" data-fancybox="images">
                                                                                                                                                View
                                                                                                                                            </a>
                                                                                                                                            <a href="<?php //echo base_url() . "customer/Request/download_projectfile/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']       ?>"> Download
                                                                                                                                            </a>
                                                                                                                                            <p class="cross-btnlink">
                                                                                                                                                <a href="javascript:void(0)" class="delete_file_req" data-id="<?php //echo $data[0]['id'];       ?>" data-name="<?php //echo $data[0]['customer_attachment'][$i]['file_name'];       ?>">
                                                                                                                                                    <span>Delete</span>
                                                                                                                                                </a>
                                                                                                                                            </p>
                                                                                                                                        </div>-->
                                                                        </div>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                    <?php } ?>
                                                    <!--                                                                            <div class="form-group discrptions">
                                                                                                                                    <label class="label-x2"><span class="prowest"></span> Description</label>
                                                                                                                                   <p class="text-d">
                                                    <?php
//                                                                                    if (strlen($data[0]['description']) > 200) {
//                                                                                        echo '<pre class="read_more txt-disp">' . $data[0]['description'] . '</pre>';
//                                                                                        echo "<pre class='more_text read_more txt-disp' style='display:none;'>" . $data[0]['description'] . "</pre><a class='read_more_btn read_more' style='color:#000;'></a>";
//                                                                                    } else {
//                                                                                        echo "<pre class='txt-disp'>" . $data[0]['description'] . "</pre>";
//                                                                                    }
                                                    ?>
                                                                                                                                    </p>
                                                                                                                                </div>-->
                                                    <div class="light-box sss" id="imgrmv">
                                                        <h3 class="base-heading">Attachments</h3>
                                                        <ul class="list-unstyled list-accessimg">
                                                            <li>
                                                                <a class="accimgbx33 add attach_file_dv" data-toggle="modal" data-target="#uploadModel">
                                                                    <span class="icon-crss-3">
                                                                        <i class="fas fa-paperclip"></i>
                                                                        <span class="icon-circlxx55">+</span>
                                                                        <p class="attachfile">Add attachments</p>
                                                                    </span>
                                                                    <input class="file-input" multiple="" type="file">
                                                                </a>
                                                            </li>
                                                            <?php for ($i = 0; $i < count($data[0]['customer_attachment']); $i++) { ?>
                                                                <?php
                                                                $type = substr($data[0]['customer_attachment'][$i]['file_name'], strrpos($data[0]['customer_attachment'][$i]['file_name'], '.') + 1);
                                                                if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                                    ?>
                                                                    <li>
                                                                        <div class="accimgbx33 ">
                                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150" />
                                                                            <p class="extension_name">
                                                                                <?php echo $type; ?>
                                                                            </p>
                                                                            <div class="viewDelete">
                                                                                <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS_SAAS . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>"> View
                                                                                </a>
                                                                                <a href="<?php echo base_url() . "account/Request/download_projectfile/" . $data[0]['id'] . "?imgpath=" . $data[0]['customer_attachment'][$i]['file_name'] ?>"> Download
                                                                                </a>
                                                                                <p class="cross-btnlink">
                                                                                    <a href="javascript:void(0)" class="delete_file_req" data-id="<?php echo $data[0]['id']; ?>" data-name="<?php echo $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                                        <span>Delete</span>
                                                                                    </a>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                <?php } else { ?>
                                                                    <li>
                                                                        <div class="accimgbx33 ">
                                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS_SAAS . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" />
                                                                            <div class="viewDelete">
                                                                                <a href="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS_SAAS . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" data-fancybox="images">
                                                                                    View
                                                                                </a>
                                                                                <a href="<?php echo base_url() . "account/Request/download_projectfile/" . $data[0]['id'] . "?imgpath=" . $data[0]['customer_attachment'][$i]['file_name'] ?>"> Download
                                                                                </a>
                                                                                <p class="cross-btnlink">
                                                                                    <a href="javascript:void(0)" class="delete_file_req" data-id="<?php echo $data[0]['id']; ?>" data-name="<?php echo $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                                        <span>Delete</span>
                                                                                    </a>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </li>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="side_bar" data-step="4" data-intro="Use this message box to send comments about the projects and receive updates from your design team." data-position='right' data-scrollTo='tooltip'>
        <div class="gz-sidebar">
            <div class="sidebar-header">
                <ul id="comment-people" class="list-header-blog sidebar_style" role="tablist" style="border:none;">
                    <li class="active" id="1">
                        <a data-toggle="tab" data-isloaded="0" data-view="" data-status="active,disapprove,assign,pending,checkforapprove" href="#comments_tab" role="tab">
                            <i class="icon-gz_message_icon"></i> Comments 
                        </a>
                    </li>
                    <?php if($profile[0]['role'] == 'customer'){ ?>
                    <li class="" id="2">
                        <a class="nav-link tabmenu" id="people_shares" data-proid="<?php echo isset($data[0]['id']) ? $data[0]['id'] : ''; ?>" data-isloaded="0" data-view="" data-status="draft" data-toggle="tab" href="#peoples_tab" role="tab" data-step="5" data-intro="See who you have shared this project with add more people as needed." data-position='right' data-scrollTo='tooltip'>
                            <i class="icon-gz_share"></i>
                            Share
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="tab-content">
                <div data-group="1" data-loaded="" class="tab-pane active content-datatable datatable-width" id="comments_tab" role="tabpanel">
                    <div class="project-row-qq2" style="overflow: unset;">
                        <div class="cmmtype-box">
                            <div class="cmmtype-row <?php
                            if ($data[0]['status'] == "assign" ) {
                                echo "disable_msgbox";
                            }
                            ?>">
                                <textarea class="pstcmm customer_pro text_<?php echo $data[0]['id']; ?>" 
                                          <?php if ($data[0]['status'] == "assign") { ?>Placeholder="Messaging is not enabled for designs in queue"  disabled="" <?php
                                          }else if ($cancomment_on_req == 0) {
                                              echo 'disabled';
                                          } 
                                          else {
                                              echo 'Placeholder="Type a message..."';
                                          }
                                          ?>></textarea>
                                <div class="send-attech">
                                    <?php if ($cancomment_on_req != 0) { ?>
                                        <label for="image">
                                            <input type="file" name="shre_file[]" id="shre_file" data-withornot="1" data-reqID="<?php echo $data[0]['id']; ?>" style="display:none;" multiple onchange="uploadChatfile(event, '<?php echo $data[0]['id']; ?>', '<?php echo $_SESSION['role']; ?>', '<?php echo $_SESSION['user_id']; ?>', '<?php echo $data[0]['designer_id']; ?>', 'designer', '<?php echo $data[0]['first_name']; ?>', '<?php echo $data[0]['profile_picture']; ?>', '<?php echo $seen_by; ?>')"/>
                                            <span class="attchmnt" title="Add Files"><i class="fa fa-paperclip" aria-hidden="true"></i></span>
                                            <input type="hidden" value="" name="saved_file[]" class="delete_file"/>
                                        </label>
                                    <?php } ?>
                                    <span <?php
                                    if ($data[0]['status'] == "assign") {
                                        echo "disabled";
                                    }
                                    ?> class="cmmsend chatsendbtn send_request_chat send" 
                                        data-profile_pic="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" 
                                        data-requestid="<?php echo $data[0]['id']; ?>" data-senderrole="<?php echo $_SESSION['role']; ?>" 
                                        data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                                        data-receiverid="<?php echo $data[0]['designer_id']; ?>"
                                        data-receiverrole="designer" 
                                        data-sendername="<?php echo $data[0]['customer_name']; ?>" 
                                        onclick="message(this);
                                            return false;"
                                        <?php
                                        if ($data[0]['status'] == "assign" || $cancomment_on_req == 0) {
                                            echo "disabled";
                                        }
                                        ?>><button class="cmmsendbtn">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-chat-send.png" class="img-responsive" /></button>
                                    </span>
                                    <?php //}    ?>
                                </div>
                            </div>
                        </div>
                        <div class="chat-box-row" id="allChats">
                            <div class="ajax_searchload chat-align" style="display:none; text-align:center;">
                                <img src="<?php echo base_url(); ?>public/assets/img/customer/gz_customer_loader.gif" />
                            </div>
                            <div class="two-can" id="message_container">
                                <?php
                                for ($j = 0; $j < count($mainchat_request); $j++) {
                                    $chatdata = $mainchat_request[$j];
                                    if ($chatdata['chat_created_datee'] != "") {
                                        ?>
                                        <div class="date_print msgdate_cre"><span class="msgk-udate t4">
                                                <?php echo $chatdata['chat_created_datee']; ?>
                                            </span></div>
                                        <?php
                                    }
                                    if ($mainchat_request[$j]['with_or_not_customer'] == 1) {
                                        if ($mainchat_request[$j]['sender_id'] != $login_user_id) {
                                            $this->load->view('account/main_chat_left', array('chatdata' => $chatdata,"CheckForMainORsub" =>$CheckForMainORsub));
                                        } else {
                                            $this->load->view('account/main_chat_right', array('chatdata' => $chatdata,"CheckForMainORsub" =>$CheckForMainORsub));
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-group="1" data-loaded="" class="tab-pane content-datatable datatable-width" id="peoples_tab" role="tabpanel">
                    <div class="sharedBy two-can">
                        <?php if ($show_filesharing != 1) { ?>
                            <div class="sharing-access-denied">You don't have access to file sharing. Please <a href="<?php echo base_url(); ?>account/setting-view#billing" target="_blank">upgrade</a> your plan for File Sharing.</div>
                        <?php } else { ?>
                            <div class="team_member_sec">
                                <h3>Team
                                    <div class="right-inst">
                                        <a class="vwwv" href="javascript:void(0);">

                                            <span class="vewviewer-cicls">
                                                <img src="<?php echo $team_member[0]['customer_image']; ?>" class="img-responsive"/>
                                            </span>
                                            <span class="hoversetss"><?php echo $team_member[0]['customer_name']; ?>
                                            </span>
                                        </a>
                                        <a class="vwwv" href="javascript:void(0);">
                                            <span class="vewviewer-cicls">
                                                <img src="<?php echo $team_member[0]['designer_image']; ?>" class="img-responsive"/>
                                            </span>
                                            <span class="hoversetss"><?php echo $team_member[0]['designer_name']; ?> (Designer)
                                            </span>
                                        </a>
                                        <a class="vwwv" href="javascript:void(0);">
                                            <span class="vewviewer-cicls">
                                                <img src="<?php echo $team_member[0]['qa_image']; ?>" class="img-responsive"/> 
                                            </span>
                                            <span class="hoversetss"><?php echo $team_member[0]['qa_name']; ?> (QA)
                                            </span>
                                        </a>
                                    </div>
                                </h3>

                            </div>
                            <h3>Share via Link
                                <label class="switch-custom-usersetting-check uncheckview">
                                    <input type="checkbox" name="disabeled_link" class="form-control" id="disabeled_link" data-reqid="<?php echo isset($data[0]['id']) ? $data[0]['id'] : ''; ?>" <?php echo (isset($publiclink['is_disabled']) && $publiclink['is_disabled'] == 0 ) ? 'checked' : ''; ?>>
                                    <label for="disabeled_link"></label>
                                </label>
                            </h3>
                            <div class="share-people" id="mainpubliclinkshr" <?php echo (isset($publiclink['is_disabled']) && $publiclink['is_disabled'] == 1 ) ? 'style="display:none"' : ''; ?>>

                                <span class="copiedone"></span>
                                <input type="text" readonly class="show_publink" name="linkshared" value="<?php echo (isset($publiclink['public_link']) && $publiclink['public_link'] != '') ? $publiclink['public_link'] : ''; ?>" />
                                <span class="copy_public_link" title="Copy link"><i class="far fa-clone"></i></span>
                                <button class="change_pubpermision" data-toggle="modal" data-target="#change_pubpermision" data-req_id="<?php echo $data[0]['id']; ?>" type="button"><span><i class="fas fa-ellipsis-v"></i></span></button>
                                <div class="permision-here">
                                    <form action="javascript:void(0)">
                                        <div class="">
                                            <?php //echo "<pre/>";print_r($publicpermissions);      ?>
                                            <div class="notify-lines">
                                                <p>Allow user for Downloading</p>
                                                <div class="switch-custom-usersetting-check people_public_permsn">
                                                    <span class="checkstatus"></span>
                                                    <input type="checkbox" value="download_source_file" name="peoplepubliclink_permissions[]" id="pplpublic_dwnld" <?php echo (isset($publicpermissions[0]['download_source_file']) && $publicpermissions[0]['download_source_file'] == 1) ? 'checked' : ''; ?>/>
                                                    <label for="pplpublic_dwnld"></label>
                                                </div>
                                            </div>
                                            <div class="notify-lines">
                                                <p>Allow user to Mark As Completed</p>
                                                <div class="switch-custom-usersetting-check people_public_permsn">
                                                    <span class="checkstatus"></span>
                                                    <input type="checkbox" value="mark_as_completed" name="peoplepubliclink_permissions[]" id="pplpublic_cmpld" <?php echo (isset($publicpermissions[0]['mark_as_completed']) && $publicpermissions[0]['mark_as_completed'] == 1) ? 'checked' : ''; ?>/>
                                                    <label for="pplpublic_cmpld"></label>
                                                </div>
                                            </div>
                                            <div class="notify-lines">
                                                <p>Allow user to Add Commenting</p>
                                                <div class="switch-custom-usersetting-check people_public_permsn">
                                                    <span class="checkstatus"></span>
                                                    <input type="checkbox" value="comment_revision" name="peoplepubliclink_permissions[]" id="pplpublic_cmnt" <?php echo (isset($publicpermissions[0]['commenting']) && $publicpermissions[0]['commenting'] == 1) ? 'checked' : ''; ?>/>
                                                    <label for="pplpublic_cmnt"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <h3>Share Via Email
                                <!-- <label class="switch-custom-usersetting-check uncheckview">
                                    <input type="checkbox" name="sharebyemail_link" class="form-control" id="sharebyemail_link">
                                    <label for="sharebyemail_link"></label>
                                </label>-->
                                <a href="javascript:void(0)" class="sharebyemail_link" id="sharebyemail_link" data-id="<?php echo $data[0]['id']; ?>"> <i class="icon-gz_plus_icon"></i></a>
                            </h3>
                            <div id="share_emailby_link_frm" class="share_emailby_link_frm_<?php echo $data[0]['id']; ?>" style="display:none">
                                <form action="<?php echo base_url(); ?>account/request/sharerequest" class="clearfix" method="post">
                                    <input type="hidden" name="request_id" value="<?php echo isset($data[0]['id']) ? $data[0]['id'] : ''; ?>" />
                                    <input type="hidden" name="shared_by" value="<?php echo isset($login_user_id) ? $login_user_id : ''; ?>" />
                                    <div class="">
                                        <div class="input-submitt">
                                            <input type="text" name="user_name" value="" class="sharemail" placeholder="Enter Name *" required="" />
                                            <input id="email_val" class="sharemail" type="email" name="email" placeholder="Enter Email Address *" required="">

                                        </div>
                                        <div class="show_error"></div>
                                        <div class="show_permisionbox">
                                            <div class="notify-lines">
                                                <p>Allow for downloading</p>
                                                <div class="switch-custom-usersetting-check">
                                                    <span class="checkstatus"></span>
                                                    <input type="checkbox" value="download_source_file" name="link_permissions[]" id="switch_2" checked />
                                                    <label for="switch_2"></label>
                                                </div>
                                            </div>
                                            <div class="notify-lines">
                                                <p>Mark As Completed</p>
                                                <div class="switch-custom-usersetting-check">
                                                    <span class="checkstatus"></span>
                                                    <input type="checkbox" value="mark_as_completed" name="link_permissions[]" id="switch_3" checked />
                                                    <label for="switch_3"></label>
                                                </div>
                                            </div>
                                            <div class="notify-lines">
                                                <p>Allow Add Comments</p>
                                                <div class="switch-custom-usersetting-check">
                                                    <span class="checkstatus"></span>
                                                    <input type="checkbox" value="comment_revision" name="link_permissions[]" id="switch_4" checked />
                                                    <label for="switch_4"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-submitt">
                                            <input type="submit" name="shareit" class="btn-red" value="Submit">
                                            <input type="submit" name="cancelshareit" class="theme-cancel-btn cancelshareitbymail" data-id="<?php echo $data[0]['id']; ?>" id="cancelshareitbymail" value="Cancel">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?php if (!empty($sharedUser)) { ?>
                                <!--                    <h3>Currently Sharing</h3>-->
                                <div id="sharedpeople" class="shareduser">
                                    <?php
                                    //echo "<pre/>";print_r($publiclink);
                                    foreach ($sharedUser as $sk => $sv) {
                                        ?>
                                        <div class="sharedbymail_block_<?php echo $sv['id']; ?>">
                                            <ul class="shared_project_class shared_user_<?php echo $sv['id']; ?>">
                                                <li><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/share-bg.svg" class="0"></li>
                                                <li><span>
                                                        <?php echo $sv['user_name']; ?></span>
                                                    <?php echo $sv['email']; ?>
                                                </li>
                                                <li style="text-align:right"><a href="javascript:void(0)" class="del_shareduser" data-req_id="<?php echo $data[0]['id'];?>" data-id="<?php echo $sv['id']; ?>"><i class="icon-gz_delete_icon"></i></a>
                                                    <button class="change_permisn" type="button" data-shareid="<?php echo $sv['id']; ?>"><span><i class="fas fa-ellipsis-v"></i></span></button>
                                                </li>
                                            </ul>
                                            <div class="private_permisn" id="private_permisn_<?php echo $sv['id']; ?>" style="display:none">
                                                <form action="javascript:void(0)">
                                                    <input type="hidden" class="shareID" value="" />
                                                    <div class="">
                                                        <div class="notify-lines top_name">
                                                            <p class="user_name"></p>
                                                            <p class="user_email"></p>
                                                        </div>
                                                        <div class="notify-lines">
                                                            <p>Allow for downloading</p>
                                                            <div class="switch-custom-usersetting-check">
                                                                <span class="checkstatus"></span>
                                                                <input type="checkbox" class="chang_prmsn_publink" data-id="<?php echo $sv['id']; ?>" value="download_source_file" name="link_permissions" id="switch_downloading_<?php echo $sv['id']; ?>" />
                                                                <label for="switch_downloading_<?php echo $sv['id']; ?>"></label>
                                                            </div>
                                                        </div>
                                                        <div class="notify-lines">
                                                            <p>Mark As Completed</p>
                                                            <div class="switch-custom-usersetting-check">
                                                                <span class="checkstatus"></span>
                                                                <input type="checkbox" class="chang_prmsn_publink" data-id="<?php echo $sv['id']; ?>" value="mark_as_completed" name="link_permissions" id="switch_Completed_<?php echo $sv['id']; ?>" />
                                                                <label for="switch_Completed_<?php echo $sv['id']; ?>"></label>
                                                            </div>
                                                        </div>
                                                        <div class="notify-lines">
                                                            <p>Allow Add Comments</p>
                                                            <div class="switch-custom-usersetting-check">
                                                                <span class="checkstatus"></span>
                                                                <input type="checkbox" class="chang_prmsn_publink" data-id="<?php echo $sv['id']; ?>" value="commenting" name="link_permissions" id="switch_Comments_<?php echo $sv['id']; ?>" />
                                                                <label for="switch_Comments_<?php echo $sv['id']; ?>"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <div class="show_error"></div>
                    <!--                    <div class="show_permisionbox">
                                            <div class="notify-lines">
                                                <p>Allow for downloading</p>
                                                <div class="switch-custom-usersetting-check">
                                                    <span class="checkstatus"></span>
                                                    <input type="checkbox" value="download_source_file" name="link_permissions[]" id="switch_2" checked />
                                                    <label for="switch_2"></label>
                                                </div>
                                            </div>
                                            <div class="notify-lines">
                                                <p>Mark As Completed</p>
                                                <div class="switch-custom-usersetting-check">
                                                    <span class="checkstatus"></span>
                                                    <input type="checkbox" value="mark_as_completed" name="link_permissions[]" id="switch_3" checked />
                                                    <label for="switch_3"></label>
                                                </div>
                                            </div>
                                            <div class="notify-lines">
                                                <p>Allow Add Comments</p>
                                                <div class="switch-custom-usersetting-check">
                                                    <span class="checkstatus"></span>
                                                    <input type="checkbox" value="comment_revision" name="link_permissions[]" id="switch_4" checked />
                                                    <label for="switch_4"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-submitt">
                                        <input type="submit" name="shareit" class="btn-blue" value="Submit">
                                        </div>-->
                </div>
            </div>


        </div>
    </div>
</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="brand_profile" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <header class="fo-rm-header">
                        <h2 class="popup_h2 del-txt">
                            <?php echo $branddata[0]['brand_name']; ?>
                        </h2>
                        <div class="cross_popup" data-dismiss="modal" aria-label="Close">
                            x
                        </div>
                    </header>
                    <?php if ($branddata[0]['id'] != '') { ?>
                        <div class="displayed-pic">
                            <div class="dp-here">
                                <?php
                                $type = substr($brand_materials_files['latest_logo'], strrpos($brand_materials_files['latest_logo'], '.') + 1);
                                $allow_type = array("jpg", "jpeg", "png", "gif");
                                if (isset($brand_materials_files['latest_logo']) && in_array(strtolower($type), $allow_type)) {
                                    ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files['latest_logo']; ?>" height="150">
                                <?php } elseif (isset($brand_materials_files['latest_logo']) && !in_array(strtolower($type), $allow_type)) { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/all-file.jpg" height="150">
                                    <!-- <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files['latest_logo']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i><span>Download</span></a></div> -->
                                <?php } else { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/logo-brand-defult.jpg" height="150" />
                                <?php } ?>
                            </div>
                            <div class="profile_info">
                                <div class="profile colm">
                                    <div class="form-group">
                                        <div class="brand_labal">Fonts Size</div>
                                        <div class="brand_info">
                                            <?php echo $branddata[0]['fonts']; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile colm">
                                    <div class="form-group">
                                        <div class="brand_labal">Color Preferences</div>
                                        <div class="brand_info">
                                            <?php echo $branddata[0]['color_preference']; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile colm">
                                    <div class="form-group">
                                        <div class="brand_labal">Website URL </div>
                                        <div class="brand_info">
                                            <?php echo $branddata[0]['website_url']; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php if (!empty($branddata[0]['google_link'])) { ?>
                                    <div class="profile colm" style="width:100%">
                                        <div class="form-group">
                                            <div class="brand_labal">Addition Reference Link</div>
                                            <div class="brand_info">
                                                <?php
                                                $brandlinks = explode(',', $branddata[0]['google_link']);
                                                foreach ($brandlinks as $bk => $bv) {
                                                    echo $bv . "<br/>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="profile colm">
                                    <div class="form-group">
                                        <div class="brand_labal">Brand Description </div>
                                        <div class="brand_info discrptions two-can">
                                            <?php echo $branddata[0]['description']; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="all-brand-outer">
                            <div class="profile colm materials-sp">
                                <div class="brand_labal">Brand Logo </div>
                                <div class="brand_info row">
                                    <?php
                                    for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                                        if ($brand_materials_files[$i]['file_type'] == 'logo_upload') {
                                            $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                            if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150" />
                                                            <p class="extension_name">
                                                                <?php echo $type; ?>
                                                            </p>
                                                        </a>
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="profile colm materials-sp">
                                <div class="brand_labal">Market Materials </div>
                                <div class="brand_info row">
                                    <?php
                                    for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                                        if ($brand_materials_files[$i]['file_type'] == 'materials_upload') {
                                            $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                            if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150" />
                                                            <p class="extension_name">
                                                                <?php echo $type; ?>
                                                            </p>
                                                        </a>
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                                        <div class="bottom-icon">
                                                            <a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="profile colm materials-sp">
                                <div class="brand_labal">Additional Images </div>
                                <div class="brand_info row">
                                    <?php
                                    for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
                                        if ($brand_materials_files[$i]['file_type'] == 'additional_upload') {
                                            $type = substr($brand_materials_files[$i]['filename'], strrpos($brand_materials_files[$i]['filename'], '.') + 1);
                                            if (in_array(strtolower($type), ALLOWED_DOCFILE_TYPES)) {
                                                ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <a target="_blank" download="" href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/defult-icon.png" height="150" />
                                                            <p class="extension_name">
                                                                <?php echo $type; ?>
                                                            </p>
                                                        </a>
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></div>
                                                    </div>

                                                </div>
                                            <?php } else { ?>
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="accimgbx33">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" alt="materials-img">
                                                        <div class="bottom-icon"><a href="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $branddata[0]['id'] . '/' . $brand_materials_files[$i]['filename']; ?>" download="" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a></div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<!----------start share modals-------------->
<div class="modal fade sharing-popup in" id="Sharerequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content full_height_modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <ul class="nav nav-tabs tabtop  tabsetting">
                <li class="active"> <a href="#share_public" data-toggle="tab"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/link-share.png" class="img-responsive">Share by link</a> </li>
                <li> <a href="#share_private" data-toggle="tab">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/email-share.png" class="img-responsive"> Share by email</a> </li>
            </ul>
            <div class="modal-body formbody two-can">
                <div class="tab-content">
                    <div class="tab-pane active fade in" id="share_public">
                        <h3 class="endisablepublnk">Enable the link for sharing: <label class="switch-custom-usersetting-check uncheckview">
                                <p class="label_case"></p>
                                <input type="checkbox" name="disabeled_link" class="form-control" id="disabeled_link_pop" data-reqid="" data-draftid="">
                                <label for="disabeled_link_pop"></label>
                            </label></h3>
                        <div class="overhelm">
                            <label>Copy this url to share: <div class='copied'></div></label>
                            <!--                      <p class="public_link"></p>-->
                            <div class="copy-to-clipboard">
                                <input readonly name="public_link" class="public_link" type="text" value="">
                            </div>
                            <!--                        <input type="text" name="public_link" class="public_link" value=""/>  -->
                            <div class="notify-lines">
                                <p>Allow user for Downloading</p>
                                <div class="switch-custom-usersetting-check public_permsn">
                                    <p class="pub_dwn"></p>
                                    <span class="checkstatus"></span>
                                    <input type="checkbox" value="download_source_file" name="publiclink_permissions[]" id="public_dwnld" />
                                    <label for="public_dwnld"></label>
                                </div>
                            </div>
                            <div class="notify-lines">
                                <p>Allow user to Mark As Completed</p>
                                <div class="switch-custom-usersetting-check public_permsn">
                                    <p class="pub_cmpl"></p>
                                    <span class="checkstatus"></span>
                                    <input type="checkbox" value="mark_as_completed" name="publiclink_permissions[]" id="public_cmpld" />
                                    <label for="public_cmpld"></label>
                                </div>
                            </div>
                            <div class="notify-lines finish-line">
                                <p>Allow user to Add Commenting</p>
                                <div class="switch-custom-usersetting-check public_permsn">
                                    <p class="pub_cmnt"></p>
                                    <span class="checkstatus"></span>
                                    <input type="checkbox" value="comment_revision" name="publiclink_permissions[]" id="public_cmnt" />
                                    <label for="public_cmnt"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade in" id="share_private">
                        <form action="<?php echo base_url(); ?>account/request/sharerequest" class="clearfix" method="post">
                            <input type="hidden" name="request_id" value="<?php echo isset($data[0]['id']) ? $data[0]['id'] : ''; ?>" />
                            <input type="hidden" name="draft_id" value="<?php //echo isset($data[0]['id'])?$data[0]['id']:'';         ?>" />
                            <input type="hidden" name="shared_by" value="<?php echo isset($login_user_id) ? $login_user_id : ''; ?>" />
                            <input type="hidden" name="shared_from" value="popup" />
                            <div class="col-md-12">
                                <label id="project_name"></label>
                                <input type="text" name="pro_name" class="pro_name" value="" disabled="" />
                                <p class="space-c"></p>
                                <label>Share this project to</label>
                                <div class="input-submitt">
                                    <input type="text" name="user_name" value="" class="sharemail" placeholder="Enter Name" required="" />
                                    <input id="email_val" class="sharemail" type="email" name="email" placeholder="Enter Email Address" required="">
                                    <input type="submit" name="shareit" class="btn-blue" value="Submit">
                                </div>
                                <div class="show_error"></div>
                                <div class="show_permisionbox" style="display:none">
                                    <div class="notify-lines">
                                        <p>Allow for downloading</p>
                                        <div class="switch-custom-usersetting-check">
                                            <p class="privt_dwnld">On </p>
                                            <span class="checkstatus"></span>
                                            <input type="checkbox" value="download_source_file" name="link_permissions[]" id="switch_dwn" class="prvt_links" checked />
                                            <label for="switch_dwn"></label>
                                        </div>
                                    </div>
                                    <div class="notify-lines">
                                        <p>Mark As Completed</p>
                                        <div class="switch-custom-usersetting-check">
                                            <p class="privt_cmplt">On </p>
                                            <span class="checkstatus"></span>
                                            <input type="checkbox" value="mark_as_completed" name="link_permissions[]" id="switch_com" class="prvt_links"  checked />
                                            <label for="switch_com"></label>
                                        </div>
                                    </div>
                                    <div class="notify-lines">
                                        <p>Allow Add Comments</p>
                                        <div class="switch-custom-usersetting-check">
                                            <p class="privt_cmnt">On </p>
                                            <span class="checkstatus"></span>
                                            <input type="checkbox" value="comment_revision" name="link_permissions[]" id="switch_rev" class="prvt_links" checked />
                                            <label for="switch_rev"></label>
                                        </div>
                                    </div>
                                </div>
                                <p class="space-c"></p>
                                <div class="">
                                    <table id="shareduser" class="shareduser"></table>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!----------Start share modals---------------->
<div class="modal change-parmision" id="change_permissions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content full_height_modal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body formbody tow-can">
                <form action="javascript:void(0)">
                    <input type="hidden" class="shareID" value=""/>
                    <div class="">
                        <div class="notify-lines top_name">
                            <p class="user_name"></p>
                            <p class="user_email"></p>
                        </div>
                        <div class="notify-lines">
                            <p>Allow for downloading</p>
                            <div class="switch-custom-usersetting-check">
                                <p class="pri_dwn"></p>
                                <span class="checkstatus"></span>
                                <input type="checkbox" class="edit_permission" value="download_source_file" name="link_permissions" id="switch_downloading"/>
                                <label for="switch_downloading"></label> 
                            </div>
                        </div>
                        <div class="notify-lines">
                            <p>Mark As Completed</p>
                            <div class="switch-custom-usersetting-check">
                                <p class="pri_compl"></p>
                                <span class="checkstatus"></span>
                                <input type="checkbox" class="edit_permission" value="mark_as_completed" name="link_permissions" id="switch_Completed"/>
                                <label for="switch_Completed"></label> 
                            </div>
                        </div>
                        <div class="notify-lines finish-line">
                            <p>Allow Add Comments</p>
                            <div class="switch-custom-usersetting-check">
                                <p class="pri_com"></p>
                                <span class="checkstatus"></span>
                                <input type="checkbox" class="edit_permission" value="comment_revision" name="link_permissions"  id="switch_Comments"/>
                                <label for="switch_Comments"></label> 
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!----------End share modals---------------->
<div class="modal fade slide-3 in" id="f_reviewpopup" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="popup_h2 del-txt dgn_revw">SEND US YOUR VALUABLE FEEDBACK!</h2>
                <!--                   <div class="cross_popup" data-dismiss="modal"> x</div>-->
            </div>
            <div class="modal-body">
                <div class="design_review_form">
                    <div class="text-aprt">
                        <form action="<?php echo base_url(); ?>admin/Dashboard/design_review_frm/1" class="f_feedback_frm" method="post" id="f_feedback_frm">
                            <input type="hidden" name="review_draftid" id="review_draftid" value="<?php echo $data[0]['approved_attachment']; ?>">
                            <input type="hidden" name="review_reqid" id="review_reqid" value="<?php echo $data[0]['id']; ?>">
                            <input type="hidden" name="from_markascomplete" id="from_markascomplete" value="">
                            <label class="form-group">
                                <p class="choose-feedbk"> How satisfied are you with the designers updates?</p>
                                <div class="sound-signal">
                                    <div class="form-radion happy_custm">
                                        <input type="radio" name="satisfied_with_design" class="preference" id="soundsignal1" value="5" required="">
                                        <label for="soundsignal1">
                                            <div class="emoji-check"><i class="fas fa-check-circle"></i></div>
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/happy.svg" class="img-responsive emoji-img"> 
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/happy-green.svg" class="img-responsive emoji-img-check">   <strong> Love it!  </strong></label>
                                    </div>
                                    <div class="form-radion mid_custm">
                                        <input type="radio" name="satisfied_with_design" id="soundsignal2" class="preference" value="3" required="">
                                        <label for="soundsignal2">
                                            <div class="emoji-check"><i class="fas fa-check-circle"></i></div>
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/happiness.svg" class="img-responsive emoji-img">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/happiness-yellow.svg" class="img-responsive emoji-img-check">     <strong> Neutral  </strong></label>
                                    </div>
                                    <div class="form-radion sad_custm">
                                        <input type="radio" name="satisfied_with_design" id="soundsignal3" class="preference" value="1" required="">
                                        <label for="soundsignal3">
                                            <div class="emoji-check"><i class="fas fa-check-circle"></i></div>
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/sad.svg" class="img-responsive emoji-img">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/sad-red.svg" class="img-responsive emoji-img-check">     <strong> Dissatisfied </strong></label>
                                    </div>
                                </div>
                            </label>
                            <label class="form-group">
                                <p class="label-txt">Additional Note</p>
                                <textarea name="additional_notes" class="review_addtional_note input"></textarea>
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                            <button type="submit" name="review_frm" class="btn-red button review_frm">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--***********modal for SAAS admin for uploading draft*********-->
<!-- Modal -->
<div class="modal similar-prop nonflex fade" id="Addfiles" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Add File</h2>
                <div id="close-d" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
            </header>
            <?php //echo "<pre/>";print_R($data);?>
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <div class="selectimagespopup">
                        <h2>Upload Draft</h2>
                        <form method="post" action="<?php echo base_url(); ?>account/request/uploadDraft/<?php echo $data[0]['id'];?>" enctype="multipart/form-data">
                        <div class="upload-draft-colm">
                            <div class="form-group goup-x1 goup_1 text-left">
                                <label class="label-x3">Source File:</label>
                                <p class="space-a"></p>
                                <div class="file-drop-area">
                                    <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                    <input type="hidden" name="tab_status" value="<?php echo $matches[0]; ?>"/>
                                    <input class="file-input" type="file" name="src_file" onchange="validateAndUploadS(this);">
                                    <div class="dropify-preview" style="display: none; width: 100%;">
                                        <img src="" class="img_dropify" style="height: 150px; width: 100%;">
                                        <div class="overlay">
                                            <div class="rem_btn">
                                                <button class="remove_selected">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group goup-x1 goup_2 text-left">
                                <label class="label-x3 ">Preview File:</label>
                                <p class="space-a"></p>
                                <div class="file-drop-area">
                                    <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-cloud.png" class="img-responsive"></span>
                                    <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>
                                    <input class="file-input" type="file" name="preview_file"  onchange="validateAndUploadP(this);" accept="image/*">
                                    <div class="dropify-preview" style="display: none; width: 100%;">
                                        <img src="" class="img_dropify" style="height: 150px; width: 100%;">
                                        <div class="overlay">
                                            <div class="rem_btn">
                                                <button class="remove_selected">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p class="allowed-type  text-left"> <i>Allowed file types : jpg, jpeg, png, gif</i></p>
                            </div>
                        </div>
                            <p class="btn-x"><input type="submit" value="Submit" class="button all-button" /></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Assign designer modal -->
<div class="modal similar-prop nonflex fade" id="AddDesigner" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Assign Designer</h2>
                <div id="close-d" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <div class="noti-listpopup">
                        <div class="newsetionlist">
                            <div class="cli-ent-row tr notificate">
                                <div class="cli-ent-col td" style="width: 33%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Designer</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 45%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Skill</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 22%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b text-center">Active Requests</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="<?php echo base_url();?>account/request/assignDesignerToRequest" method="post">
                            <ul class="list-unstyled list-notificate two-can">
                                <?php foreach ($designer_list as $designers) { //echo "<pre/>";print_R($designers); ?>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <div class="cli-ent-row tr notificate">
                                                <div class="cli-ent-col td" >
                                                    <div class="sound-signal">
                                                        <div class="form-radion">
                                                            <input class="selected_btn" type="radio" value="<?php echo $designers['id']; ?>" name="assign_designer" id="<?php echo $designers['id']; ?>" data-image-pic="<?php echo $designers['profile_picture'] ?>" data-name="<?php echo $designers['first_name'] . " " . $designers['last_name']; ?>">
                                                            <label for="<?php echo $designers['id']; ?>" data-image-pic="<?php echo $designers['profile_picture'] ?>"></label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 30%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="setnoti-fication">
                                                            <figure class="pro-circle-k1">
                                                                <img src="<?php echo $designers['profile_picture'] ?>" class="img-responsive">
                                                            </figure>

                                                            <div class="notifitext">
                                                                <p class="ntifittext-z1">
                                                                    <strong>
                                                                        <?php echo $designers['first_name'] . " " . $designers['last_name']; ?>

                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 45%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <p class="pro-a">UX Designer, Landing page, Mobile App  UX Designer, Landing page, Mobile App<span class="sho-wred">+2</span></p>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 25%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="cli-ent-xbox text-center">

                                                            <p class="neft text-center"><span class="red text-uppercase"><?php echo $designers['active_request']; ?></span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>  
                                <?php } ?>                          
                            </ul>
                            <input type="hidden" name="request_id" id="request_id" value="<?php echo isset($data[0]['id']) ? $data[0]['id'] : ''; ?>">
                            <p class="space-c"></p>
                            <p class="btn-x text-center">
                                <button name="adddesingerbtn" type="submit" id="assign_desigertoreq" class="button all-button">Assign Designer</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<button style="display: none;" id="confirmation" data-toggle="modal" data-target="#myModal">click here</button>

<!-- Modal -->
<div class="modal fade similar-prop" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" name="request_id_status" id="request_id_status" value=""/>
            <input type="hidden" name="valuestatus" id="valuestatus" value=""/>
            <input type="hidden" name="status_flag" id="status_flag" value=""/>
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Change  Status</h2>
                <div class="cross_popup" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/change_plan.svg">
                    <h3 class="head-c text-center msgal">Are you sure you want to change the status to <strong></strong></h3>
                    <div class="confirmation_btn text-center">
                        <button class="btn btn-y btn-ndelete" data-dismiss="modal" aria-label="Close">Yes Change It</button>
                        <button class="btn btn-n btn-ydelete" data-dismiss="modal" aria-label="Close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/jquery.fancybox.min.js"></script>
<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/jquery.mousewheel.min.js"></script>
<?php
if (!empty($chat_request)) {
    $prjtid = $chat_request[0]['request_id'];
    $customerid = $chat_request[0]['reciever_id'];
    $designerid = $chat_request[0]['sender_id'];
}
?>
<script>
 function validateAndUploadS(input) {
        var URL = window.URL || window.webkitURL;
        var file = input.files[0];
        var exten = $(input).val().split('.').pop();
        var imgext = ['jpg', 'jpeg', 'png', 'gif','PNG','JPG','JPEG'];

        if($("#sour").parents(".file-drop-area").hasClass("red-alert")){
            $("#sour").parents(".file-drop-area").removeClass("red-alert");
        }

        if (exten == "zip" || exten == "rar") {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-archive-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        } else if (exten == "docx" || exten == "doc") {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-word-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        } else if (exten == "pdf") {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-pdf-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        } else if (exten == "txt") {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-text-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        }
        else if (exten == "psd") {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-powerpoint-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        }
        else if (exten == "ai") {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg"><strong><i class="fa fa-file-o" aria-hidden="true"></i>' + file.name + '</strong></h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        } else if (jQuery.inArray(exten, imgext) != '-1') {
            //console.log(URL.createObjectURL(file));
            $(".goup_1 .file-drop-area .dropify-preview").html('<img src="' + URL.createObjectURL(file) + '" class="img_dropify" style="height: 150px; width: 100%;"><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        } else {
            $(".goup_1 .file-drop-area .dropify-preview").html('<h4 class="file-msg">You have selected <strong>' + exten + '</strong> file</h4><div class="overlay"><div class="rem_btn"><button class="remove_selected">Remove</button></div></div>');
            $('.goup_1 .file-drop-area .dropify-preview').show();
            $('.goup_1 .file-drop-area span').hide();
        }
    }
    
function validateAndUploadP(input) {
    var URL = window.URL || window.webkitURL;
    var file = input.files[0];
      if($("#preview_image").parents(".file-drop-area").hasClass("red-alert")){
       $("#preview_image").parents(".file-drop-area").removeClass("red-alert"); 
    }
    if (file.type.indexOf('image/') !== 0) {
        this.value = null;
        //console.log("invalid");
    }
    else {
        if (file) {
           // console.log(URL.createObjectURL(file));
            $(".goup_2 .file-drop-area span").hide();
            $(".goup_2 .file-drop-area .dropify-preview").css('display', 'block');
            $(".goup_2 .img_dropify").attr('src', URL.createObjectURL(file));
        }
    }
}

 $(document).on('click', ".remove_selected", function (e) {
            e.preventDefault();
            $(this).closest('.dropify-preview').css('display', 'none');
            $(this).closest('.file-drop-area').find('input').val('');
            $(this).closest('.file-drop-area').find('span').show();
            $(this).closest('.file-drop-area').find('.file-msg').html('Drag and drop file here or <span class="nocolsl">Click Here</span>');
        });
</script>
