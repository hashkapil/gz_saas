<?php     

   if (!empty($alldraftpubliclinks)) {
       foreach ($alldraftpubliclinks as $pubkey => $pubval) {
           ?>
<div class="public_link" id="public_<?php echo $pubval['draft_id']; ?>" data-draftid ="<?php echo $pubval['draft_id']; ?>">
   <h3>Share via Link
      <label class="switch-custom-usersetting-check uncheckview">
      <input type="checkbox" name="disabeled_link" class="form-control disabled_draft_pub" id="disabeled_link_<?php echo $pubval['draft_id']; ?>" data-reqid="<?php echo $pubval['request_id']; ?>" data-draftid="<?php echo $pubval['draft_id']; ?>" <?php echo (isset($pubval['is_disabled']) && $pubval['is_disabled'] == 0) ? 'checked' : ''; ?>>
      <label for="disabeled_link_<?php echo $pubval['draft_id']; ?>"></label> 
      </label>
   </h3>
   <div class="share-people" id="mainpubliclinkshr_<?php echo $pubval['draft_id']; ?>" <?php echo (isset($pubval['is_disabled']) && $pubval['is_disabled'] == 1 )? 'style="display:none"' :'test'; ?>>
      <span class="copiedone"></span>   
      <input type="text" readonly class="show_publink" name="linkshared" value="<?php echo (isset($pubval['public_link']) && $pubval['public_link'] != '') ? $pubval['public_link'] : ''; ?>"/>
      <span class="copy_public_link" ><i class="far fa-clone"></i></span>
      <button class="change_pubpermision" data-shareID="<?php echo $pubval['id']; ?>" data-draft_id="<?php echo $pubval['draft_id']; ?>" data-request_id="<?php echo $pubval['request_id']; ?>" data-toggle="modal" data-target="#change_pubpermision" type="button"><span><i class="fas fa-ellipsis-v"></i></span></button>
      <div class="permision-here">
         <form action="javascript:void(0)">
            <div class="">
               <div class="notify-lines">
                  <p>Allow user for Downloading</p>
                  <div class="switch-custom-usersetting-check">
                     <span class="checkstatus"></span>
                     <input type="checkbox" class="chang_prmsn_publink" data-id="<?php echo $pubval['id']; ?>" value="download_source_file" name="peoplepubliclink_permissions[]" id="switch_downloading_<?php echo $pubval['id']; ?>" />
                     <label for="switch_downloading_<?php echo $pubval['id']; ?>"></label> 
                  </div>
               </div>
               <div class="notify-lines">
                  <p>Allow user to Mark As Completed</p>
                  <div class="switch-custom-usersetting-check">
                     <span class="checkstatus"></span>
                     <input type="checkbox" class="chang_prmsn_publink" data-id="<?php echo $pubval['id']; ?>" value="mark_as_completed" name="peoplepubliclink_permissions[]" id="switch_Completed_<?php echo $pubval['id']; ?>" />
                     <label for="switch_Completed_<?php echo $pubval['id']; ?>"></label> 
                  </div>
               </div>
               <div class="notify-lines">
                  <p>Allow user to Add Commenting</p>
                  <div class="switch-custom-usersetting-check">
                     <span class="checkstatus"></span>
                     <input type="checkbox" class="chang_prmsn_publink" data-id="<?php echo $pubval['id']; ?>" value="commenting" name="peoplepubliclink_permissions[]"  id="switch_Comments_<?php echo $pubval['id']; ?>" />
                     <label for="switch_Comments_<?php echo $pubval['id']; ?>"></label> 
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
   <h3>
      Shared Via Email
      <label class="switch-custom-usersetting-check uncheckview">
         <!--input type="checkbox" name="sharebyemail_link" class="form-control" id="sharebyemail_link"-->
         <!--label for="sharebyemail_link"></label-->
      </label>
      <a href="javascript:void(0)" class="sharebyemail_link" data-id="<?php echo $pubval['draft_id'];?>" id="sharebyemail_link"> <i class="icon-gz_plus_icon"></i></a>
   </h3>
   <div id="share_emailby_link_frm" class="share_emailby_link_frm_<?php echo $pubval['draft_id'];?>" style="display:none">
      <form action="<?php echo base_url(); ?>account/request/sharerequest" class="clearfix" method="post">
         <input type="hidden" name="request_id" value="<?php echo $reqid; ?>"/>
         <input type="hidden" name="draft_id" value="<?php echo $pubval['draft_id'];?>"/>
         <input type="hidden" name="shared_by" value="<?php echo isset($login_user_id) ? $login_user_id : ''; ?>"/>
         <div class="">
            <div class="input-submitt">
               <input type="text" name="user_name" value="" class="sharemail"  placeholder="Enter Name *" required=""/>
               <input id="email_val" class="sharemail" type="email" name="email"  placeholder="Enter Email Address *" required="">
            </div>
            <div class="show_error"></div>
            <div class="show_permisionbox">
               <div class="notify-lines">
                  <p>Allow for downloading</p>
                  <div class="switch-custom-usersetting-check">
                     <span class="checkstatus"></span>
                     <input type="checkbox" value="download_source_file" name="link_permissions[]" id="switch1_<?php echo $pubval['draft_id'];?>" checked/>
                     <label for="switch1_<?php echo $pubval['draft_id'];?>"></label> 
                  </div>
               </div>
               <div class="notify-lines">
                  <p>Mark As Completed</p>
                  <div class="switch-custom-usersetting-check">
                     <span class="checkstatus"></span>
                     <input type="checkbox" value="mark_as_completed" name="link_permissions[]" id="switch2_<?php echo $pubval['draft_id'];?>" checked/>
                     <label for="switch2_<?php echo $pubval['draft_id'];?>"></label> 
                  </div>
               </div>
               <div class="notify-lines">
                  <p>Allow Add Comments</p>
                  <div class="switch-custom-usersetting-check">
                     <span class="checkstatus"></span>
                     <input type="checkbox" value="comment_revision" name="link_permissions[]"  id="switch3_<?php echo $pubval['draft_id'];?>" checked/>
                     <label for="switch3_<?php echo $pubval['draft_id'];?>"></label> 
                  </div>
               </div>
            </div>
            <div class="input-submitt">
               <input type="submit" name="shareit" class="btn-red" value="Submit">
               <input type="submit" name="cancelshareit" class="theme-cancel-btn cancelshareitbymail" id="cancelshareitbymail" data-id="<?php echo $pubval['draft_id'];?>" value="Cancel">
            </div>
         </div>
      </form>
   </div>
</div>
<?php
   }
   } else {
   echo "<h3 style='text-align=center'>No Data found</h3>";
   }
//   echo "<pre> sharedreqUser"; print_r($sharedreqUser);
//   echo "<pre> shareddraftUser"; print_r($shareddraftUser);
   
   ?>
<?php if ((!empty($sharedreqUser)) || (!empty($shareddraftUser))) { ?>
<div id="sharedpeople" class="shareduser">
   <!--                                    <h5>Shared Request by Email</h5>-->
   <!--requests shared people-->
   <?php foreach ($sharedreqUser as $sk => $sv) { ?>
   <div class="sharedbymail_block_<?php  echo $sv['id'];?>">
      <ul class="shared_project_class shared_user_<?php echo $sv['id']; ?>">
         <li><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/share-bg.svg" class="img-responsive"></li>
         <li><span><?php echo $sv['user_name']; ?></span><?php echo $sv['email']; ?></li>
         <li style="text-align:right"><a href="javascript:void(0)" class="del_shareduser" data-id="<?php echo $sv['id']; ?>"><i class="far fa-trash-alt"></i></a>
            <button class="change_permisn" data-toggle="modal" data-target="#change_permissions" type="button" data-shareid="<?php echo $sv['id']; ?>"><span><i class="fas fa-ellipsis-v"></i></span></button>
         </li>
      </ul>
      <div class="private_permisn" id="private_permisn_<?php echo $sv['id']; ?>" style="display:none">
         <form action="javascript:void(0)">
            <input type="hidden" class="shareID" value=""/>
            <div class="">
               <div class="notify-lines top_name">
                  <p class="user_name"></p>
                  <p class="user_email"></p>
               </div>
               <div class="notify-lines">
                  <p>Allow for downloading</p>
                  <div class="switch-custom-usersetting-check">
                     <span class="checkstatus"></span>
                     <input type="checkbox" class="chang_prmsn_publink" data-id="<?php echo $sv['id']; ?>" value="download_source_file" name="link_permissions" id="switch_downloading_<?php echo $sv['id']; ?>"/>
                     <label for="switch_downloading_<?php echo $sv['id']; ?>"></label> 
                  </div>
               </div>
               <div class="notify-lines">
                  <p>Mark As Completed</p>
                  <div class="switch-custom-usersetting-check">
                     <span class="checkstatus"></span>
                     <input type="checkbox" class="chang_prmsn_publink" data-id="<?php echo $sv['id']; ?>" value="mark_as_completed" name="link_permissions" id="switch_Completed_<?php echo $sv['id']; ?>"/>
                     <label for="switch_Completed_<?php echo $sv['id']; ?>"></label> 
                  </div>
               </div>
               <div class="notify-lines">
                  <p>Allow Add Comments</p>
                  <div class="switch-custom-usersetting-check">
                     <span class="checkstatus"></span>
                     <input type="checkbox" class="chang_prmsn_publink" data-id="<?php echo $sv['id']; ?>" value="commenting" name="link_permissions"  id="switch_Comments_<?php echo $sv['id']; ?>"/>
                     <label for="switch_Comments_<?php echo $sv['id']; ?>"></label> 
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
   <?php } ?>
   <!--<h5>Shared Draft by Email</h5>-->
   <!--draft shared people-->
   <?php foreach ($shareddraftUser as $sdk => $sdv) { ?>
   <div class="sharedbymail_block_<?php echo $sdv['id']; ?>">
      <ul class="shared_project_class shared_draft_user_<?php echo $sdv['draft_id']; ?>">
         <li><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/share-bg.svg" class="img-responsive"></li>
         <li><span><?php echo $sdv['user_name']; ?></span><?php echo $sdv['email']; ?></li>
         <li style="text-align:right"><a href="javascript:void(0)" class="del_shareduser" data-id="<?php echo $sdv['id']; ?>"><i class="far fa-trash-alt"></i></a>
            <button class="change_permisn" data-toggle="modal" data-target="#change_permissions" type="button" data-shareid="<?php echo $sdv['id']; ?>"><span><i class="fas fa-ellipsis-v"></i></span></button>
         </li>
      </ul>
      <div class="private_permisn" id="private_permisn_<?php echo $sdv['id']; ?>" style="display:none">
         <form action="javascript:void(0)">
            <input type="hidden" class="shareID" value=""/>
            <div class="">
               <div class="notify-lines top_name">
                  <p class="user_name"></p>
                  <p class="user_email"></p>
               </div>
               <div class="notify-lines">
                  <p>Allow for downloading</p>
                  <div class="switch-custom-usersetting-check">
                     <span class="checkstatus"></span>
                     <input type="checkbox" class="chang_prmsn_publink" data-id="<?php echo $sdv['id']; ?>" value="download_source_file" name="link_permissions" id="switch_downloading_<?php echo $sdv['id']; ?>"/>
                     <label for="switch_downloading_<?php echo $sdv['id']; ?>"></label> 
                  </div>
               </div>
               <div class="notify-lines">
                  <p>Mark As Completed</p>
                  <div class="switch-custom-usersetting-check">
                     <span class="checkstatus"></span>
                     <input type="checkbox" class="chang_prmsn_publink" data-id="<?php echo $sdv['id']; ?>" value="mark_as_completed" name="link_permissions" id="switch_Completed_<?php echo $sdv['id']; ?>"/>
                     <label for="switch_Completed_<?php echo $sdv['id']; ?>"></label> 
                  </div>
               </div>
               <div class="notify-lines">
                  <p>Allow Add Comments</p>
                  <div class="switch-custom-usersetting-check">
                     <span class="checkstatus"></span>
                     <input type="checkbox" class="chang_prmsn_publink" data-id="<?php echo $sdv['id']; ?>" value="commenting" name="link_permissions"  id="switch_Comments_<?php echo $sdv['id']; ?>"/>
                     <label for="switch_Comments_<?php echo $sdv['id']; ?>"></label> 
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
   <?php } ?>
   </tbody>
   </table>
   <?php } ?>
</div>