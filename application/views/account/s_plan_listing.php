<div class="tab-content">
    <div class="tab-pane active" id="f_monthly_pln"> 
        <div class="billing_plan two-can <?php echo $type_ofuser; ?>">
            <?php
            foreach ($userplans as $userplan) {
                    $agencyclass = $userplan['plan_type'] . " upgrd_agency_pln";
                    $prc = $userplan['plan_price'];
                    $agency_priceclass = "<span class='" . $userplan['plan_type'] . " agency_price'> " . $userplan['plan_price'] . "</span><span>/" . $userplan['plan_type'] . "</span>";
                    $this->load->view('account/plan_details', array("userplan" => $userplan, "agencyclass" => $agencyclass, "agency_priceclass" => $agency_priceclass, "data" => $data));
            }
            ?>
        </div>
    </div>
</div>