<style>
    #file{
        outline: none;
        font-size: 0px;
    }
</style>
<section class="con-b">
    <div class="container">
        <p class="space-d"></p>
        <?php if ($this->session->flashdata('message_error') != '') { ?>
            <div id="message" class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_error'); ?>
                </p>
            </div>
        <?php } ?>

        <?php if ($this->session->flashdata('message_success') != '') { ?>
            <div id="message" class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php echo $this->session->flashdata('message_success'); ?>
                </p>
            </div>
        <?php }
        ?>

        <div class="flex-show uplink-row clearfix">
            <span class="uplink-col left"><h3 class="sub-head-d">
                    <?php
                    if (isset($_GET['rep']) && $_GET['rep'] == 1) {
                        $stat = "Project Update";
                    } else {
                        $stat = 'Create Project';
                    }
                    echo $stat;
                    ?>						
                </h3></span>
            <span class="uplink-col right">
                <ul class="list-unstyled list-bread-crumb">
                    <li><a href="">Dashboard</a></li>
                    <li><?php echo $stat; ?></li>
                </ul>
            </span>
        </div>
        <?php if (empty($data)): ?>
            <form method="post" action="" enctype="multipart/form-data">
                <div class="header-bxx2">
                    <ul class="list-unstyled list-tab-xx5">
                        <li><a href="<?php echo base_url(); ?>customer/request/new_request/category">Category</a></li>			
                        <li class="acitve"><a href="<?php echo base_url(); ?>customer/request/new_request/brief">Brief</a></li>
                        <li><a href="#">Review</a></li>        
                    </ul>
                </div>

                <p class="space-d"></p>

                <div class="cate-main-boxx3">
                    <div class="row"><!-- 
                            <div class="col-md-4">
                                    <div class="cat-box-xx122">
                                            <h3 class="sub-head-a">Design Brief</h3>
                                            <p class="space-c"></p>
                                            <div class="text-b para">
                                                    <p>vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                                                    <p>vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas </p>
                                                    <p>molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                                            </div>
                                    </div>
                            </div> -->

                        <div class="col-md-10 col-md-offset-1">
                            <div class="cat-form-xx123">
                                <div class="form-group goup-x1">
                                    <label class="label-x2"><span class="prowest">1</span> Project Title<span class="maditory">*</span></label>
                                    <p class="space-b"></p>
                                    <input type="text" name="project_title" id="project_title" class="form-control input-b" placeholder="Design a Logo" required>
                                </div>

                                <p class="space-c"></p>

                                <div class="form-group goup-x1">
                                    <label class="label-x2"><span class="prowest">2</span> Target Market<span class="maditory">*</span></label>
                                    <p class="space-a"></p>
                                    <p class="text-g">Who are you trying to talk/sell to?</p>
                                    <p class="space-b"></p>
                                    <input type="text" class="form-control input-b" placeholder="Tarket Market 18-40 Yrs Generation" name="industry_design" id="industry_design" required>
                                </div>

                                <p class="space-d"></p>

                                <!-- Color Plates -->
                                <div class="form-group">

                                    <label class="label-x2"><span class="prowest">3</span> Color Pallete<span class="maditory">*</span></label>
                                    <p class="space-a"></p>
                                    <p class="text-g">What color best represent your company and why?</p>
                                    <p class="space-b"></p>

                                    <div class="radio-check-kk1">
                                        <label for="DesignerChoose1" class="radio-box-kk2"> 
                                            <input id="DesignerChoose1" name="designColors[]" type="radio" checked onclick="uncheck(this.id)" value="Let Designer Choose" required>
                                            <span class="checkmark circles"></span>
                                            Let Designer Choose for me.
                                        </label>
                                    </div>

                                    <p class="space-a"></p>

                                    <div class="lessrows">
                                        <div class="left-lesscol">
                                            <div class="radio-check-kk1">
                                                <label for="colorbest" class="radio-box-kk2"> 
                                                    <input id="colorbest" name="designColors[]" value="" onclick="check()" type="radio" required>
                                                    <span class="checkmark circles"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="right-lesscol noflexes">
                                            <div class="plan-boxex-xx6 clearfix">

                                                <label for="id1" class="radio-box-xx2"> 
                                                    <input id="id1" name="designColors[]" type="checkbox" value="blue">
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-1.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">Blue</h3>
                                                    </div>
                                                </label>

                                                <label for="id2" class="radio-box-xx2"> 
                                                    <input id="id2" name="designColors[]" type="checkbox" value="aqua">
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-2.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">Aqua</h3>
                                                    </div>
                                                </label>

                                                <label for="id3" class="radio-box-xx2"> 
                                                    <input id="id3" name="designColors[]" type="checkbox" value="green">
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-3.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">Greens</h3>
                                                    </div>
                                                </label>

                                                <label for="id4" class="radio-box-xx2"> 
                                                    <input id="id4" name="designColors[]" type="checkbox" value="purple">
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-4.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">Purple</h3>
                                                    </div>
                                                </label>

                                                <label for="id5" class="radio-box-xx2"> 
                                                    <input id="id5" name="designColors[]" type="checkbox" value="pink">
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-5.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">Pink</h3>
                                                    </div>
                                                </label>
                                                <label for="id6" class="radio-box-xx2"> 
                                                    <input id="id6" name="designColors[]" type="checkbox" value="black">
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-6.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">black</h3>
                                                    </div>
                                                </label>
                                                <label for="id7" class="radio-box-xx2"> 
                                                    <input id="id7" name="designColors[]" type="checkbox" value="orange">
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-7.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">orange</h3>
                                                    </div>
                                                </label>

                                            </div>	
                                        </div>
                                    </div>

                                    <p class="space-a"></p>

                                    <div class="form-group goup-x1">
                                        <div class="radio-check-kk1">
                                            <label for="HexCode" class="radio-box-kk2"> 
                                                <input id="HexCode" name="designColors[]" type="radio"   onclick="uncheck(this.id)">
                                                <span class="checkmark circles"></span>
                                                Enter Hex Code
                                            </label>
                                            <input type="text" id="HexCodeVal" class="form-control input-b mx1" name="HexCodeVal" placeholder="# 000000">
                                        </div>
                                    </div>

                                </div>
                                <!-- Color Plates End -->


                                <p class="space-d"></p>

                                <!-- Design Software Requirement -->
                                <div class="">
                                    <label class="label-x2"><span class="prowest">4</span> Design Software Requirement<span class="maditory">*</span></label>
                                    <p class="space-b"></p>
                                    <div class="radio-check-kk1">
                                        <label for="DesignerChoose" class="radio-box-kk2"> 
                                            <input id="DesignerChoose" name="designer" type="radio" checked value="Let Designer Choose for me">
                                            <span class="checkmark circles"></span>
                                            Let Designer Choose for me.
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group goup-x1 radio-check-kk1">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6">
                                            <label for="AdobeIllustrator" class="radio-box-kk2"> 
                                                <input id="AdobeIllustrator" name="designer" type="radio" value="Adobe Illustrator">
                                                <span class="checkmark circles"></span>
                                                Adobe Illustrator
                                            </label>
                                        </div>

                                        <div class="col-md-3 col-sm-6">
                                            <label for="AdobePhotoshop" class="radio-box-kk2"> 
                                                <input id="AdobePhotoshop" name="designer" type="radio" value="Adobe Photoshop">
                                                <span class="checkmark circles"></span>
                                                Adobe Photoshop
                                            </label>
                                        </div>

                                        <div class="col-md-3 col-sm-6">
                                            <label for="AdobeinDesign" class="radio-box-kk2"> 
                                                <input id="AdobeinDesign" name="designer" type="radio" value="Adobe  in Design">
                                                <span class="checkmark circles"></span>
                                                Adobe  in Design
                                            </label>
                                        </div>

                                        <div class="col-md-3 col-sm-6">
                                            <label for="Sketch" class="radio-box-kk2"> 
                                                <input id="Sketch" name="designer" type="radio" value="Sketch">
                                                <span class="checkmark circles"></span>
                                                Sketch
                                            </label>
                                        </div>

                                        <div class="col-md-4 col-sm-6">
                                            <div class="diff-typeimg">
                                                <span class="left-exits"><label for="design" class="radio-box-kk2"> 
                                                        <input id="design" name="designer" type="radio">
                                                        <span class="checkmark circles"></span>

                                                    </label></span>

                                                <span class="right-exits"><input id="designVal" class="form-control input-b" placeholder="Type others here...." type="text"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Design Software Requirement End -->

                                <p class="space-d"></p>

                                <!-- Deliverables -->
                                <div class="form-group goup-x1">
                                    <label class="label-x2"><span class="prowest">5</span> Deliverables<span class="maditory">*</span></label>
                                    <p class="space-b"></p>
                                    <div class="checkbox-check-kk1">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-6">
                                                <label for="SourceFiles" class="radio-box-kk2"> 
                                                    <input id="SourceFiles" value="SourceFiles" name="filetype[]" type="checkbox" checked="checked">
                                                    <span class="checkmark checkboxes"></span>
                                                    Source Files
                                                </label>
                                            </div>

                                            <div class="col-md-3 col-sm-6">
                                                <label for="PNG" class="radio-box-kk2"> 
                                                    <input id="PNG" name="filetype[]" value="png" type="checkbox">
                                                    <span class="checkmark checkboxes"></span>
                                                    PNG.
                                                </label>
                                            </div>

                                            <div class="col-md-3 col-sm-6">
                                                <label for="JPEG" class="radio-box-kk2"> 
                                                    <input id="JPEG" name="filetype[]" value="jpeg" type="checkbox">
                                                    <span class="checkmark checkboxes"></span>
                                                    JPEG
                                                </label>
                                            </div>

                                            <div class="col-md-3 col-sm-6">
                                                <label for="PDF" class="radio-box-kk2"> 
                                                    <input id="PDF" name="filetype[]" value="pdf" type="checkbox">
                                                    <span class="checkmark checkboxes"></span>
                                                    PDF
                                                </label>
                                            </div>

                                            <div class="col-md-4 col-sm-6">
                                                <div class="diff-typeimg">
                                                    <span class="left-exits">
                                                        <label for="otherDel" class="radio-box-kk2"> 
                                                            <input id="otherDel" name="filetype[]" type="checkbox">
                                                            <span class="checkmark checkboxes"></span>
                                                        </label>
                                                    </span>

                                                    <span class="right-exits"><input type="text" id="otherDelVal" class="form-control input-b" placeholder="Type others here..."></span>
                                                </div>
                                            </div>

                                        </div> <!-- .row -->
                                    </div>
                                </div>
                                <!-- Deliverables End -->

                                <p class="space-d"></p>

                                <div class="form-group goup-x1">
                                    <label class="label-x2"><span class="prowest">6</span> Description<span class="maditory">*</span></label>
                                    <p class="space-a"></p>
                                    <p class="text-c">Please provide us the final, revised copy / text to include each page, as a word document.</p>
                                    <p class="space-b"></p>
                                    <textarea class="form-control textarea-a" placeholder="Tarket Market 18-40 Yrs Generation" name="description" id="description" required></textarea>
                                </div>

                                <div class="form-group goup-x1"> 
                                    <div class="file-drop-area file-upload">
                                        <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cloud.png" class="img-responsive"></span>
                                        <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>

                                        <input type="file" class="file-input project-file-input" multiple="" name="file-upload[]" id="file_input"/>

                                    </div>

                                    <p class="text-g">Example: Doc, Docx, Odt, Pdf, Jpg, Png, Jpeg, Psd, Ai, Zip, Mp4, Mov </p>
                                    <p class="space-e"></p>
                                    <div class="uploadFileListContainer row">                      
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                                <!-- <p class="text-mb">Design.sample.jpg <strong>(1.5 MB)</strong></p> -->
                                                <!-- <p class="pro-a">Category <strong><?php echo $_POST['category']; ?></strong></p>
                                                <p class="pro-a">Logo Brand <strong><?php echo $_POST['logo-brand']; ?></strong></p> -->
                                            <input type="hidden" name="category" value="<?php echo $_POST['category']; ?>" />
                                            <?php if (isset($_POST['logo-brand'])): ?>
                                                <input type="hidden" name="logo-brand" value="<?php echo $_POST['logo-brand']; ?>" />
                                            <?php else: ?>
                                                <input type="hidden" name="logo-brand" value="no logo brand selected" />
                                            <?php endif ?>

                                        </div>
                                        <!--											<div class="col-md-6">
                                                                                                                                        <p class="cross-btnlink"><a href="">
                                                                                                                                                 <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cross-2.png"> 
                                                                                                                                        </a></p>
                                                                                                                                </div>-->
                                    </div>
                                    <p class="space-e"></p>
                                    <div class="uplinkflexs clearfix">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <p class="savecols-col left"><a href="<?php echo base_url(); ?>customer/request/new_request/category" class="back-link-xx0 text-uppercase">Back</a></p>
                                            </div>

                                            <div class="col-xs-4">
                                                <p class="savecols-col center btn-x"><input class="btn btn-a text-uppercase min-250 text-center" type="submit" name="proceed" value="Next" onclick="focusOn()"></p>
                                            </div>

                                            <div class="col-xs-4">
                                                    <!-- <p class="savecols-col right btn-x"><a class="back-link-xx11 text-uppercase">save &amp; exit</a></p> -->
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </form>	

        <?php else: ?>
            <?php
            if (isset($_GET['rep']) && $_GET['rep'] == 1) {
                $action = base_url() . "customer/request/new_request_brief_update/" . $data[0]['id'] . "?rep=1";
            } else {
                $action = base_url() . "customer/request/new_request_brief_update/" . $data[0]['id'];
            }
            ?>
            <form method="post" action="<?php echo $action; ?>" enctype="multipart/form-data">
                <div class="header-bxx2">
                    <ul class="list-unstyled list-tab-xx5">
                        <li><a href="<?php echo base_url(); ?>customer/request/new_request/category">Category</a></li>			
                        <li class="acitve"><a href="<?php echo base_url(); ?>customer/request/new_request/brief">Brief</a></li>
                        <li><a href="#">Review</a></li>        
                    </ul>
                </div>

                <p class="space-d"></p>

                <div class="cate-main-boxx3">
                    <div class="row">
                        <!-- <div class="col-md-4">
                                <div class="cat-box-xx122">
                                        <h3 class="sub-head-a">Design Brief</h3>
                                        <p class="space-c"></p>
                                        <div class="text-b para">
                                                <p>vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                                                <p>vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas </p>
                                                <p>molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
                                        </div>
                                </div>
                        </div> -->

                        <div class="col-md-10 col-md-offset-1">
                            <div class="cat-form-xx123">
                                <div class="form-group goup-x1">
                                    <label class="label-x2"><span class="prowest">1</span> Project Title<span class="maditory">*</span></label>
                                    <p class="space-b"></p>
                                    <input type="text" name="project_title" id="project_title" class="form-control input-b" value="<?php echo $data[0]['title']; ?>" placeholder="Design a Logo" required>
                                </div>

                                <p class="space-c"></p>

                                <div class="form-group goup-x1">
                                    <label class="label-x2"><span class="prowest">2</span> Target Market<span class="maditory">*</span></label>
                                    <p class="space-a"></p>
                                    <p class="text-g">Who are you trying to talk/sell to?</p>
                                    <p class="space-b"></p>
                                    <input type="text" class="form-control input-b" placeholder="Tarket Market 18-40 Yrs Generation" value="<?php echo $data[0]['business_industry']; ?>" name="industry_design" id="industry_design" required>
                                </div>

                                <p class="space-d"></p>

                                <!-- Color Plates -->
                                <?php
                                $color[] = "";
                                if (isset($data[0]['design_colors'])) {
                                    $color = explode(",", $data[0]['design_colors']);
                                }
                                ?>
                                <div class="form-group">

                                    <label class="label-x2"><span class="prowest">3</span> Color Pallete<span class="maditory">*</span></label>
                                    <p class="space-a"></p>
                                    <p class="text-g">What color best represent your company and why?</p>
                                    <p class="space-b"></p>

                                    <div class="radio-check-kk1">
                                        <label for="DesignerChoose1" class="radio-box-kk2"> 
                                            <input id="DesignerChoose1" name="designColors[]" type="radio" onclick="uncheck(this.id)" value="Let Designer Choose" <?php if ($data[0]['design_colors'] && in_array("Let Designer Choose", $color)): ?>
                                                       checked
                                                   <?php endif ?>  required>
                                            <span class="checkmark circles"></span>
                                            Let Designer Choose for me.
                                        </label>
                                    </div>

                                    <p class="space-a"></p>
                                    <div class="lessrows">
                                        <div class="left-lesscol">
                                            <div class="radio-check-kk1">
                                                <label for="colorbest" class="radio-box-kk2"> 
                                                    <input id="colorbest" name="designColors[]" value="" onclick="check()" type="radio" 
                                                    <?php if ($data[0]['design_colors'] && (in_array("blue", $color)) || (in_array("aqua", $color)) || (in_array("green", $color)) || (in_array("purple", $color)) || (in_array("pink", $color)) && (!(in_array("Let Designer Choose", $color)))): ?>


                                                               checked
                                                           <?php endif ?> >

                                                    <span class="checkmark circles"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="right-lesscol noflexes">
                                            <div class="plan-boxex-xx6 clearfix">

                                                <label for="id1" class="radio-box-xx2"> 
                                                    <input id="id1" name="designColors[]" type="checkbox" value="blue" <?php if ($data[0]['design_colors'] && in_array("blue", $color)): ?>
                                                               checked
                                                           <?php endif ?> >
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-1.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">Blue
                                                            <!-- <label for="colorbest">	Blues </label>								 -->
                                                        </h3>
                                                    </div>
                                                </label>

                                                <label for="id2" class="radio-box-xx2"> 
                                                    <input id="id2" name="designColors[]" type="checkbox" value="aqua" <?php if ($data[0]['design_colors'] && in_array("aqua", $color)): ?>
                                                               checked
                                                           <?php endif ?> >
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-2.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">Auqa</h3>
                                                    </div>
                                                </label>

                                                <label for="id3" class="radio-box-xx2"> 
                                                    <input id="id3" name="designColors[]" type="checkbox" value="green" <?php if ($data[0]['design_colors'] && in_array("green", $color)): ?>
                                                               checked
                                                           <?php endif ?> >
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-3.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">Greens</h3>
                                                    </div>
                                                </label>

                                                <label for="id4" class="radio-box-xx2"> 
                                                    <input id="id4" name="designColors[]" type="checkbox" value="purple" <?php if ($data[0]['design_colors'] && in_array("purple", $color)): ?>
                                                               checked
                                                           <?php endif ?> >
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-4.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">Purple</h3>
                                                    </div>
                                                </label>

                                                <label for="id5" class="radio-box-xx2"> 
                                                    <input id="id5" name="designColors[]" type="checkbox" value="pink" <?php if ($data[0]['design_colors'] && in_array("pink", $color)): ?>
                                                               checked
                                                           <?php endif ?>  >
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-5.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">Pink</h3>
                                                    </div>
                                                </label>
                                                <label for="id6" class="radio-box-xx2"> 
                                                    <input id="id6" name="designColors[]" type="checkbox" value="black" <?php if ($data[0]['design_colors'] && in_array("black", $color)): ?>
                                                               checked
                                                           <?php endif ?>  >
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-6.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">Black</h3>
                                                    </div>
                                                </label>
                                                <label for="id7" class="radio-box-xx2"> 
                                                    <input id="id7" name="designColors[]" type="checkbox" value="orange" <?php if ($data[0]['design_colors'] && in_array("orange", $color)): ?>
                                                               checked
                                                           <?php endif ?>  >
                                                    <span class="checkmark"></span>
                                                    <div class="check-main-xx3" onclick="checkradio()">
                                                        <figure class="chkimg">
                                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/col-7.png" class="img-responsive">
                                                        </figure>										
                                                        <h3 class="sub-head-c text-center">Orange</h3>
                                                    </div>
                                                </label>

                                            </div>	
                                        </div>
                                    </div>

                                    <p class="space-a"></p>

                                    <div class="form-group goup-x1">
                                        <div class="radio-check-kk1">
                                            <label for="HexCode" class="radio-box-kk2"> 
                                                <input id="HexCode" name="designColors[]" type="radio"   onclick="uncheck(this.id)"
                                                       <?php if ($data[0]['design_colors'] && !(in_array("blue", $color)) && !(in_array("aqua", $color)) && !(in_array("green", $color)) && !(in_array("purple", $color)) && !(in_array("pink", $color)) && (!(in_array("Let Designer Choose", $color)))): ?> checked value="">
                                                    <span class="checkmark circles"></span>
                                                    Enter Hex Code
                                                </label>
                                                <input type="text" id="HexCodeVal" class="form-control input-b mx1" name="HexCodeVal" value="<?php echo $data[0]['design_colors'] ?>" placeholder="# 000000">
                                            <?php else: ?>
                                                <input id="HexCode" name="designColors[]" type="radio"   onclick="uncheck(this.id)"value="">
                                                <span class="checkmark circles"></span>
                                                Enter Hex Code
                                                </label>
                                                <input type="text" id="HexCodeVal" class="form-control input-b mx1" name="HexCodeVal" value="" placeholder="# 000000">
                                            <?php endif ?>
                                        </div>
                                    </div>


                                </div>
                                <!-- Color Plates End -->


                                <p class="space-d"></p>

                                <!-- Design Software Requirement -->
                                <div class="">
                                    <label class="label-x2"><span class="prowest">4</span> Design Software Requirement<span class="maditory">*</span></label>
                                    <p class="space-b"></p>
                                    <div class="radio-check-kk1">
                                        <label for="DesignerChoose" class="radio-box-kk2"> 
                                            <input id="DesignerChoose" name="designer" type="radio" value="Let Designer Choose for me" <?php if ($data[0]['designer'] == "Let Designer Choose for me"): ?>
                                                       checked
                                                   <?php endif ?>  required>
                                            <span class="checkmark circles"></span>
                                            Let Designer Choose for me.
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group goup-x1 radio-check-kk1">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6">
                                            <label for="AdobeIllustrator" class="radio-box-kk2"> 
                                                <input id="AdobeIllustrator" name="designer" type="radio" value="Adobe Illustrator" <?php if ($data[0]['designer'] == "Adobe Illustrator"): ?>
                                                           checked
                                                       <?php endif ?> >
                                                <span class="checkmark circles"></span>
                                                Adobe Illustrator
                                            </label>
                                        </div>

                                        <div class="col-md-3 col-sm-6">
                                            <label for="AdobePhotoshop" class="radio-box-kk2"> 
                                                <input id="AdobePhotoshop" name="designer" type="radio" value="Adobe Photoshop" <?php if ($data[0]['designer'] == "Adobe Photoshop"): ?>
                                                           checked
                                                       <?php endif ?> >
                                                <span class="checkmark circles"></span>
                                                Adobe Photoshop
                                            </label>
                                        </div>

                                        <div class="col-md-3 col-sm-6">
                                            <label for="AdobeinDesign" class="radio-box-kk2"> 
                                                <input id="AdobeinDesign" name="designer" type="radio" value="Adobe  in Design" <?php if ($data[0]['designer'] == "Adobe  in Design"): ?>
                                                           checked
                                                       <?php endif ?> >
                                                <span class="checkmark circles"></span>
                                                Adobe  in Design
                                            </label>
                                        </div>

                                        <div class="col-md-3 col-sm-6">
                                            <label for="Sketch" class="radio-box-kk2"> 
                                                <input id="Sketch" name="designer" type="radio" value="Sketch" <?php if ($data[0]['designer'] == "Sketch"): ?>
                                                           checked
                                                       <?php endif ?> >
                                                <span class="checkmark circles"></span>
                                                Sketch
                                            </label>
                                        </div>

                                        <div class="col-md-4 col-sm-6">
                                            <div class="diff-typeimg">
                                                <span class="left-exits"><label for="design" class="radio-box-kk2">
                                                        <?php if (($data[0]['designer'] != "Let Designer Choose for me") && ($data[0]['designer'] != "Adobe Illustrator") && ($data[0]['designer'] != "Adobe Photoshop") && ($data[0]['designer'] != "Adobe  in Design") && ($data[0]['designer'] != "Sketch")): ?> 
                                                            <input id="design" name="designer" type="radio"	checked>
                                                            <span class="checkmark circles"></span>

                                                        </label></span>

                                                    <span class="right-exits"><input id="designVal" class="form-control input-b" value="<?php echo $data[0]['designer']; ?>" placeholder="Type others here...." type="text"></span>
                                                <?php else: ?>
                                                    <input id="design" name="designer" type="radio"	>
                                                    <span class="checkmark circles"></span>

                                                    </label></span>

                                                    <span class="right-exits"><input id="designVal" class="form-control input-b" value="" placeholder="Type others here...." type="text"></span>
                                                <?php endif ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Design Software Requirement End -->

                                <p class="space-d"></p>

                                <!-- Deliverables -->
                                <?php
                                $deliverables[] = "";
                                $d_count = 0;
                                if ($data[0]['deliverables']) {
                                    $deliverables = explode(",", $data[0]['deliverables']);
                                    $d_count = count($deliverables);
                                }
                                ?>

                                <div class="form-group goup-x1">
                                    <label class="label-x2"><span class="prowest">5</span> Deliverables<span class="maditory">*</span></label>
                                    <p class="space-b"></p>
                                    <div class="checkbox-check-kk1">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-6">
                                                <label for="SourceFiles" class="radio-box-kk2"> 
                                                    <input id="SourceFiles" value="SourceFiles" name="filetype[]" type="checkbox" <?php
                                                    if ($data[0]['deliverables'] && in_array("SourceFiles", $deliverables)) {
                                                        $d_count--;
                                                        ?>checked <?php } ?>  required>
                                                    <span class="checkmark checkboxes"></span>
                                                    Source Files
                                                </label>
                                            </div>

                                            <div class="col-md-3 col-sm-6">

                                                <label for="PNG" class="radio-box-kk2"> 
                                                    <input id="PNG" name="filetype[]" value="png" type="checkbox" <?php
                                                    if ($data[0]['deliverables'] && in_array("png", $deliverables)) {
                                                        $d_count--;
                                                        ?>checked <?php } ?> >
                                                    <span class="checkmark checkboxes"></span>
                                                    PNG.
                                                </label>


                                            </div>

                                            <div class="col-md-3 col-sm-6">

                                                <label for="JPEG" class="radio-box-kk2"> 
                                                    <input id="JPEG" name="filetype[]" value="jpeg" type="checkbox" <?php
                                                    if ($data[0]['deliverables'] && in_array("jpeg", $deliverables)) {
                                                        $d_count--;
                                                        ?>checked <?php } ?> >
                                                    <span class="checkmark checkboxes"></span>
                                                    JPEG
                                                </label>

                                            </div>

                                            <div class="col-md-3 col-sm-6">

                                                <label for="PDF" class="radio-box-kk2"> 
                                                    <input id="PDF" name="filetype[]" value="pdf" type="checkbox" <?php
                                                    if ($data[0]['deliverables'] && in_array("pdf", $deliverables)) {
                                                        $d_count--;
                                                        ?>checked <?php } ?> >
                                                    <span class="checkmark checkboxes"></span>
                                                    PDF
                                                </label>

                                            </div>

                                            <div class="col-md-4 col-sm-6">
                                                <div class="diff-typeimg">
                                                    <span class="left-exits">
                                                        <label for="otherDel" class="radio-box-kk2"> 
                                                            <input id="otherDel" name="filetype[]" type="checkbox" <?php if ($d_count > 0) { ?> checked <?php } ?>>
                                                            <span class="checkmark checkboxes"></span>
                                                        </label>
                                                    </span>

                                                    <span class="right-exits"><input type="text" id="otherDelVal" class="form-control input-b" placeholder="Type others here..." value="<?php echo $deliverables[(count($deliverables) - 1)]; ?>"></span>

                                                </div>
                                            </div>

                                        </div> <!-- .row -->
                                    </div>
                                </div>
                                <!-- Deliverables End -->

                                <p class="space-d"></p>

                                <div class="form-group goup-x1">
                                    <label class="label-x2"><span class="prowest">6</span> Description<span class="maditory">*</span></label>
                                    <p class="space-a"></p>
                                    <p class="text-c">Please provide us the final, revised copy / text to include each page, as a word document.</p>
                                    <p class="space-b"></p>
                                    <textarea class="form-control textarea-a" placeholder="Tarket Market 18-40 Yrs Generation" name="description" id="description" required><?php echo $data[0]['description']; ?></textarea>
                                </div>

                                <div class="form-group goup-x1">
                                    <div class="file-drop-area file-upload">
                                        <span class="fake-img"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cloud.png" class="img-responsive"></span>
                                        <span class="file-msg">Drag and drop file here or <span class="nocolsl">Click Here</span></span>

                                        <input type="file" class="file-input project-file-input" multiple="" name="file-upload[]" id="file_input"/>

                                    </div>

                                    <p class="text-g">Example: Doc, Docx, Odt, Pdf, Jpg, Png, Jpeg, Psd, Ai, Zip, Mp4, Mov </p>
                                    <p class="space-e"></p>
                                    <div class="uploadFileListContainer row">                      
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                                        <!-- <p class="text-mb">Design.sample.jpg <strong>(1.5 MB)</strong></p> -->
                                            <p style="margin-top: 2px;width: 333px !important;padding: 0px;">
                                            <ul class="uploaded-img">
                                                <?php for ($i = 0; $i < count($data[0]['customer_attachment']); $i++) { ?>

                                                    <?php
                                                    $type = substr($data[0]['customer_attachment'][$i]['file_name'], strrpos($data[0]['customer_attachment'][$i]['file_name'], '.') + 1);
                                                    if ($type == "pdf") {
                                                        ?>
                                                        <li>
                                                            <div class="accimgbx33">
                                                                <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/pdf.png" height="150"/>
                                                                </a>
                                                            </div>
                                                        </li>
                                                        <?php
                                                    } elseif ($type == "zip") {
                                                        ?>
                                                        <li>
                                                            <div class="accimgbx33">
                                                                <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/zip.png" height="150"/>
                                                                </a>
                                                            </div>
                                                        </li>
                                                    <?php } elseif ($type == "odt" || $type == "ods") {
                                                        ?>
                                                        <li>
                                                            <div class="accimgbx33">
                                                                <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/odt.png" height="150"/>
                                                                </a>
                                                            </div>
                                                        </li>
                                                    <?php } elseif ($type == "doc" || $type == "docx") { ?>
                                                        <li><div class="accimgbx33">
                                                                <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                    <img src="<?php echo base_url(); ?>public/default-img/docx.png" height="150"/>
                                                                </a>
                                                            </div>
                                                        </li>
                                                    <?php } elseif ($type == "psd") { ?>
                                                        <li><div class="accimgbx33">
                                                                <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/psd.jpg" height="150"/>
                                                                </a>
                                                            </div>
                                                        </li>
                                                    <?php } elseif ($type == "ai") { ?>
                                                        <li><div class="accimgbx33">
                                                                <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                    <img src="<?php echo base_url(); ?>public/default-img/ai.png" height="150"/>
                                                                </a>
                                                            </div>
                                                        </li>
                                                    <?php } elseif ($type == "mov") { ?>
                                                        <li><div class="accimgbx33">
                                                                <a target="_blank" download="" href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/mov.png" height="150"/>
                                                                </a>
                                                            </div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li><div class="accimgbx33">
                                                                <a href="<?php echo base_url() . "uploads/requests/" . $data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" data-fancybox="images">
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS.$data[0]['id'] . "/" . $data[0]['customer_attachment'][$i]['file_name']; ?>" />
                                                                </a>
                                                            </div>
                                                        </li>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </ul>
                                            </p>
                                                          <!-- <input type="hidden" name="category" value="<?php echo $_POST['category']; ?>" />
                                                          <input type="hidden" name="logo-brand" value="<?php echo $_POST['logo-brand']; ?>" /> -->
                                        </div>
                                        <!--<div class="col-md-6">
                                                <p class="cross-btnlink"><a href=""><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/icon-cross-2.png"></a></p>
                                        </div>-->
                                    </div>
                                    <p class="space-e"></p>
                                    <div class="uplinkflexs clearfix">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <p class="savecols-col left"><a href="<?php echo base_url(); ?>customer/request/new_request/category" class="back-link-xx0 text-uppercase">Back</a></p>
                                            </div>

                                            <div class="col-xs-4">
                                                <p class="savecols-col center btn-x"><input class="btn btn-a text-uppercase min-250 text-center" type="submit" name="proceed" value="Next" onclick="focusOn()"></p>
                                            </div>

                                            <div class="col-xs-4">
                                                    <!-- <p class="savecols-col right btn-x"><a class="back-link-xx11 text-uppercase">save &amp; exit</a></p> -->
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </form>	

        <?php endif ?>
        <!-- <button class="btn btn-info" onclick="focusOn();">Hello</a> -->
    </div>
</section>

<!-- jQuery (necessary for JavaScript plugins) -->
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/bootstrap.min.js"></script>
<script type="text/javascript">

                                                    function checkradio() {
                                                        document.getElementById('colorbest').checked = true
                                                    }

                                                    function check() {
                                                        document.getElementById('id1').checked = true
                                                        document.getElementById('id2').checked = true
                                                        document.getElementById('id3').checked = true
                                                        document.getElementById('id4').checked = true
                                                        document.getElementById('id5').checked = true
                                                        document.getElementById('id6').checked = true
                                                        document.getElementById('id7').checked = true
                                                    }

                                                    function uncheck(str) {
                                                        document.getElementById('id1').checked = false
                                                        document.getElementById('id2').checked = false
                                                        document.getElementById('id3').checked = false
                                                        document.getElementById('id4').checked = false
                                                        document.getElementById('id5').checked = false
                                                        document.getElementById('id6').checked = false
                                                        document.getElementById('id7').checked = false
                                                        if (str == "HexCode") {
                                                            document.getElementById('HexCodeVal').focus()
                                                        }
                                                    }

                                                    function focusOn() {

                                                        if (document.getElementById('HexCode').checked == true) {
                                                            document.getElementById('HexCodeVal').required = true
                                                            var val1 = document.getElementById("HexCodeVal").value
                                                            document.getElementById("HexCode").value = ""
                                                            document.getElementById("HexCode").value = val1
                                                        } else {
                                                            document.getElementById('HexCodeVal').required = false
                                                        }

                                                        if (document.getElementById("design").checked == true) {
                                                            document.getElementById("designVal").required = true
                                                            var val2 = document.getElementById("designVal").value
                                                            document.getElementById("design").value = ""
                                                            document.getElementById("design").value = val2
                                                        }
                                                        else {
                                                            document.getElementById("designVal").required = false
                                                        }

                                                        if (document.getElementById("otherDel").checked == true) {
                                                            document.getElementById("otherDelVal").required = true
                                                            var val3 = document.getElementById("otherDelVal").value
                                                            document.getElementById("otherDel").value = ""
                                                            document.getElementById("otherDel").value = val3
                                                        }
                                                        else {
                                                            document.getElementById("otherDelVal").required = false
                                                        }
                                                    }
                                                    if (navigator.userAgent.indexOf("MSIE") > 0) {
                                                        $("#file").mousedown(function () {
                                                            $(this).trigger('click');
                                                        });
                                                        $('.carousel-inner > .item > a > img, .carousel-inner > .item > img, .img-responsive, .thumbnail a > img, .thumbnail > img').css('display', 'inline-block');
                                                    }


                                                    function formatFileSize(bytes, decimalPoint) {
                                                        if (bytes == 0)
                                                            return '0 Bytes';
                                                        var k = 1000,
                                                                dm = decimalPoint || 2,
                                                                sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
                                                                i = Math.floor(Math.log(bytes) / Math.log(k));
                                                        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
                                                    }
                                                    var $droparea = $('.project-file-drop-area');
                                                    var storedFiles = [];
                                                    var deletedFiles = [];
                                                    // highlight drag area
                                                    $('#file_input').on('dragenter focus click', function () {
                                                        $droparea.addClass('is-active');
                                                    });

                                                    // back to normal state
                                                    $('#file_input').on('dragleave blur drop', function () {
                                                        $droparea.removeClass('is-active');
                                                    });
                                                    $(document).ready(function () {
                                                        $('#file_input').change(function () {
                                                            var filesCount = $(this)[0].files.length;
                                                            var $textContainer = $(this).prev();
                                                            var $fileListContainer = $(".uploadFileListContainer");
                                                            filesCount = storedFiles.length;
                                                            if (filesCount === 1) {
                                                                // if single file is selected, show file name
                                                                var fileName = $(this).val().split('\\').pop();
                                                            } else if (filesCount >= 1) {
                                                            }
                                                            $fileListContainer.append('<div class="ajax-img-loader"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/Loading_icon.gif" height="150"/></div>');

                                                            var fileInput = $('#file_input')[0];
                                                            if (fileInput.files.length > 0) {

                                                                var formData = new FormData();
                                                                $.each(fileInput.files, function (k, file) {
                                                                    formData.append('file-upload[]', file);
                                                                });

                                                                $.ajax({
                                                                    method: 'post',
                                                                    url: "<?php echo base_url(); ?>customer/request/process",
                                                                    data: formData,
                                                                    dataType: 'json',
                                                                    contentType: false,
                                                                    processData: false,
                                                                    success: function (response) {
                                                                        console.log(response);
                                                                        if (response.status == '1') {
                                                                            if (response.files.length > 0) {
                                                                                $.each(response.files, function (k, v) {
                                                                                    storedFiles.push(v[0]);
                                                                                    $fileListContainer.html("");
                                                                                    $.each(storedFiles, function (key, value) {
                                                                                        if (value.error == false) {
                                                                                            $('.ajax-img-loader').css('display', 'none');
                                                                                            $fileListContainer.append('<div  class="uploadFileRow " id="file' + key + '"><div class="col-md-6"><div class="extnsn-lst">\n\
                                              <p class="text-mb">' + value.file_name + '<strong> (' + formatFileSize(value.file_size, 2) + ') </strong></p>\n\
                                                  <p class="cross-btnlink">\n\
                                                      <a href="javascript:void(0)" class="delete_file" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
                                                         <span>x</span>\n\
                                                      </a>\n\
                                                     <input type="hidden" value="' + value.file_name + '" name="delete_file[]" class="delete_file"/><input type="hidden" value="" name="upload_path" class="upload_path" />\n\
                          \n\
                          \n\
                          \n\
                                                  </p>\n\
                                              </div></div></div>');
                                                                                        } else {
                                                                                            $fileListContainer.append('<div  class="uploadFileRow" id="file' + key + '"><div class="col-md-6" ><div class="extnsn-lst error-list">\n\
                                              <p class="text-mb">' + value.file_name + '<br/><strong>' + value.error_msg + '</strong></p><p class="cross-btnlink">\n\
                                                      <a href="javascript:void(0)" class="delete_file" data-file-index="' + key + '" data-file-name="' + value.file_name + '">\n\
                                                         <span>x</span>\n\
                                                      </a>\n\
                                                     \n\
                          \n\
                          \n\
                          \n\
                                                  </p>\n\
                                              </div></div></div>');
                                                                                        }
                                                                                    });
                                                                                });
                                                                            }
                                                                        } else {
                                                                            //
                                                                        }
                                                                        // console.log(response);
                                                                        //$(".upload_path").val(response);
                                                                    }
                                                                });
                                                            } else {
                                                                console.log('No Files Selected');
                                                            }
                                                        });

                                                    });
                                                    $(document).on("click", ".delete_file", function () {
                                                        var file_index = $(this).data("file-index");
                                                        deletedFiles.push($(this).data("id"));
                                                        storedFiles.splice(file_index, 1);
                                                        var file_name = $(this).data("file-name");
                                                        var folderPath = $(".upload_path").val();
                                                        //           alert(file_name);
                                                        //           alert(folderPath);
                                                        var dataString = 'file_name=' + file_name + '&folderPath=' + folderPath;
                                                        $("#file" + file_index).remove();
                                                        var filesCount = storedFiles.length;
                                                        if (filesCount === 1) {
                                                            // $(".file-msg").text($(this).data("file-name"));
                                                        } else {
                                                            // otherwise show number of files
                                                            //$(".file-msg").text(filesCount + ' files selected');
                                                        }
                                                        $.ajax({
                                                            method: 'POST',
                                                            url: "<?php echo base_url(); ?>customer/request/delete_file_from_folder",
                                                            data: dataString,
                                                            success: function (response) {

                                                            }
                                                        });
                                                    });
</script>
</body>
</html>