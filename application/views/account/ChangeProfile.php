<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ChangeProfile extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct(){
	  parent::__construct();
		$this->load->library('javascript');
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->helper('url');
		$this->load->model('Request_model');
		$this->load->model('Welcome_model');
		$this->load->model('Admin_model');
		$this->load->model('Account_model');
		$this->load->model('Stripe');
		
	}
	
	public function checkloginuser()

	{

		if(!$this->session->userdata('user_id')){

			redirect(base_url());

		}

	}
	
	public function index() 
	{		
		$this->checkloginuser();
		$user_id = $_SESSION['user_id'];
		$data = $this->Admin_model->getuser_data($user_id);
		  
		if(!empty($_POST))
		{
			unset($_POST['savebtn']);
			if (!empty($_FILES)) {
		    if ($_FILES['profile']['name'] != "") {
          if (!empty($_FILES)) {
            $config2 = array(
                'upload_path' => './uploads/profile_picture',
                'allowed_types' => "*",
                'encrypt_name' => TRUE
            );
            $config2['profile'] = $_FILES['profile']['name'];

            $this->load->library('upload', $config2);

            if ($this->upload->do_upload('profile')) {
                $data = array($this->upload->data());
								$_POST['profile_picture'] = $data[0]['file_name'];
            } else {
            $data = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('message_error', $data, 5);
                    }
                }
            }
        }
        //print_r($_POST);
        //die("js");
			$update_data_result = $this->Admin_model->update_data('users',$_POST,array("id"=>$user_id));
			if($update_data_result)
			{
				$this->session->set_flashdata('message_success', 'User Profile Updated Successfully.!', 5);
			}else{
				$this->session->set_flashdata('message_error', 'User Profile Not Updated.!', 5);
			}
			redirect(base_url()."customer/ChangeProfile/");
		}
	$messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
    $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
    $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
    $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
    $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

    $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile"=>$profile_data));
    
		$this->load->view('customer/setting-edit',array("data"=>$data));
		$this->load->view('customer/footer_customer');
  //   $this->load->view('customer/changeprofile',array("data"=>$data));
		// $this->load->view('customer/customer_footer');
	}
	
	public function change_password_old()
	{
		$this->checkloginuser();
			if(!empty($_POST)){
				$user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
				
				if($user_data[0]['new_password'] != $_POST['old_password']){
					$this->session->set_flashdata("message_error","Old Password is not correct..!",5);
					redirect(base_url()."customer/ChangeProfile");
				}
				if($_POST['new_password'] != $_POST['confirm_password']){
					$this->session->set_flashdata("message_error","New And Confirm Password is not match..!",5);
					redirect(base_url()."customer/ChangeProfile");
				}
				if($this->Admin_model->update_data("users",array("new_password"=>md5($_POST['new_password'])),array("id"=>$_SESSION['user_id']))){
					$this->session->set_flashdata("message_success","Password is changed successfully..!",5);
				}else{
					$this->session->set_flashdata("message_error","Password is not changed successfully..!",5);
				}
				redirect(base_url()."customer/ChangeProfile");
			}
		
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
		$this->load->view('customer/change_password');
		$this->load->view('customer/footer_customer');
	}
	
	public function billing_subscription()
	{
		$user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
               // echo "<pre>";print_r($user_data);exit;
		$planlist = $this->Stripe->getallsubscriptionlist();
		$current_plan = $this->Stripe->getcurrentplan($user_data[0]['current_plan']);
		$customerdetails = $this->Stripe->getcustomerdetail($user_data[0]['current_plan']);
		//echo '<pre>'; print_r($customerdetails);
		// $carddetails['last4'] = $customerdetails->sources->data[0]->last4;
		// $carddetails['exp_month'] = $customerdetails->sources->data[0]->exp_month;
		// $carddetails['exp_year'] = $customerdetails->sources->data[0]->exp_year;
		// $carddetails['brand'] = $customerdetails->sources->data[0]->brand;
		if($current_plan!=""){
			$current_plan = $current_plan->items->data[0]->plan->id;
		}
		
		if(!empty($_POST)){
                    
			if($_POST['plan_name'] == ""){
				$this->session->set_flashdata("message_error","Please Choose Another Plan..!");
				redirect(base_url()."customer/ChangeProfile/billing_subscription");
			}
			if($_POST['plan_name'] == $current_plan){
				$this->session->set_flashdata("message_error","Please Choose Another Plan..!");
				redirect(base_url()."customer/ChangeProfile/billing_subscription");
			}
			$data = $this->Stripe->updateuserplan($user_data[0]['current_plan'],$_POST['plan_name']);
			if($data[0]=="error"){
				$this->session->set_flashdata("message_error",$data[1]);
				redirect(base_url()."customer/ChangeProfile/billing_subscription");
			}
			if($data[0]=="success"){
				$this->session->set_flashdata("message_success","Plan Updated Successfully..!");
				redirect(base_url()."customer/ChangeProfile/billing_subscription");
			}
		}
		$invoices= $this->Stripe->get_customer_invoices($user_data[0]['customer_id']);
		if(isset($invoices['data'])){
			$invoices = $invoices['data'];
		}else{
			$invoices = array();
		}
		$profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
    $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
    $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
    $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
     $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

    $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile"=>$profile_data));
   
    //$this->load->view('customer/billing_subscription',array("planlist"=>$planlist,"current_plan"=>$current_plan,"carddetails"=>$carddetails,"invoices"=>$invoices));
    $this->load->view('customer/billing_subscription',array("planlist"=>$planlist,"current_plan"=>$current_plan,"invoices"=>$invoices));
		$this->load->view('customer/customer_footer_1');
	}
	
	public function change_plan()
	{
		$user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
                //echo "<pre>";print_r($user_data);exit;
		$customerdetails = $this->Stripe->getcustomerdetail($user_data[0]['current_plan']);
                //echo "<pre>";print_r($customerdetails);exit;
		$updatecard = $this->Stripe->change_user_card($customerdetails->id,$_POST);
		if($updatecard[0]=="success"){
			$this->session->set_flashdata("message_success",$updatecard[1]);
		}else{
			$this->session->set_flashdata("message_error",$updatecard[1]);
		}
		redirect(base_url()."customer/setting-view");
	}
        
        public function change_current_plan()
	{
            //print_R($_POST);exit();
            $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
           
            $customer_id = $_SESSION['user_id'];
            $subscription_plan_id = $_POST['plan_name'];
            $updateuserplan = $this->Stripe->updateuserplan($user_data[0]['customer_id'],$_POST['plan_name']);
            $subscription = $updateuserplan['data']['subscriptions']; 
            $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);
           //echo "<pre>"; print_R($plandetails);
            $customer['id'] = $customer_id;
            $customer['current_plan'] = $subscription['data']['0']->id;
            $customer['plan_name'] = $subscription_plan_id;
            $customer['plan_amount'] = $plandetails['amount'] / 100;
            $customer['plan_interval'] = $plandetails['interval'];
            $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
            $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
            $customer['display_plan_name'] = $plandetails['name'];
            //echo $updateuserplan['data']['subscriptions']['id'];
          // echo "<pre>"; print_r($updateuserplan['1']);exit();
            if($updateuserplan[0] != 'success'){
                $this->session->set_flashdata('message_error', $updateuserplan['1'], 5);
                 redirect(base_url() . "customer/setting-view");
            }else{
            $id = $this->Welcome_model->update_data("users", $customer, array("id" => $customer_id));
            if($id){
                 $this->session->set_flashdata('message_success', $updateuserplan['1'], 5);
                 redirect(base_url() . "customer/setting-view");
            }
            }
           //echo "<pre>"; print_R($plandetails);exit();
        }
	
	public function setting_edit()
	{	
		$this->checkloginuser();
		$user_id = $_SESSION['user_id'];
		$data = $this->Admin_model->getuser_data($user_id);
		
		if(!empty($_POST))
		{
			unset($_POST['savebtn']);
			if (!empty($_FILES)) {
            if ($_FILES['profile']['name'] != "") {
                if (!empty($_FILES)) {
                    $config2 = array(
                        'upload_path' => './uploads/profile_picture',
                        'allowed_types' => "*",
                        'encrypt_name' => TRUE
                    );
                    $config2['profile'] = $_FILES['profile']['name'];

                    $this->load->library('upload', $config2);

                    if ($this->upload->do_upload('profile')) {
                        $data = array($this->upload->data());
						$_POST['profile_picture'] = $data[0]['file_name'];
                    } else {
                        $data = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('message_error', $data, 5);
                    }
                }
            }
        }
			$update_data_result = $this->Admin_model->update_data('users',$_POST,array("id"=>$user_id));
			if($update_data_result)
			{
				$this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
			}else{
				$this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
			}
			redirect(base_url()."customer/setting-view/");
		}
		
    $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
    $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
    $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
    $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

    $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
    $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile"=>$profile_data));
		
    $this->load->view('customer/setting-edit',array("data"=>$data));
    $this->load->view('customer/footer_customer');
		//$this->load->view('customer/customer_footer');
	}
	
	public function change_password()
	{
		$this->checkloginuser();
			if(!empty($_POST)){
				$user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
				
				if($user_data[0]['new_password'] != $_POST['old_password']){
					$this->session->set_flashdata("message_error","Old Password is not correct..!",5);
					redirect(base_url()."customer/setting-edit");
				}
				if($_POST['new_password'] != $_POST['confirm_password']){
					$this->session->set_flashdata("message_error","New And Confirm Password is not match..!",5);
					redirect(base_url()."customer/setting-edit");
				}
				if($this->Admin_model->update_data("users",array("new_password"=>md5($_POST['new_password'])),array("id"=>$_SESSION['user_id']))){
					$this->session->set_flashdata("message_success","Password is changed successfully..!",5);
				}else{
					$this->session->set_flashdata("message_error","Password is not changed successfully..!",5);
				}
				redirect(base_url()."customer/setting-edit");
			}
		
    $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
    $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
    $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
		$this->load->view('customer/change_password');
		$this->load->view('customer/footer_customer');
		//$this->load->view('customer/customer_footer');
	}
	public function change_password_front()
	{
		$this->checkloginuser();
			if(!empty($_POST)){
				$user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
				
				if($user_data[0]['new_password'] != md5($_POST['old_password'])){
					$this->session->set_flashdata("message_error","Old Password is not correct..!",5);
					redirect(base_url()."customer/setting-view");
				}
				if($_POST['new_password'] != $_POST['confirm_password']){
					$this->session->set_flashdata("message_error","New And Confirm Password is not match..!",5);
					redirect(base_url()."customer/setting-view");
				}
				if($this->Admin_model->update_data("users",array("new_password"=>md5($_POST['new_password'])),array("id"=>$_SESSION['user_id']))){
					$this->session->set_flashdata("message_success","Password is changed successfully..!",5);
				}else{
					$this->session->set_flashdata("message_error","Password is not changed successfully..!",5);
				}
				redirect(base_url()."customer/setting-view");
			}
		
    $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
    $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
    $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
		$this->load->view('customer/change_password');
		$this->load->view('customer/footer_customer');
		//$this->load->view('customer/customer_footer');
	}

	public function setting_view()
	{

		$this->checkloginuser();
		$user_id = $_SESSION['user_id'];
		$data = $this->Admin_model->getuser_data($user_id);
		
	    $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
	    $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
	    $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
	    $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
	    $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
	    if($data[0]['current_plan'] != "" || $data[0]['current_plan'] != null ){
		    $card_details = $this->get_customer_detail_from_strip($data[0]['current_plan']);
		    $plan_details = $this->getdetailplan($data[0]['current_plan']);
		}else{
			$card_details = [];
			$plan_details = [];
		}
	    $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile"=>$profile_data));
			
	    $this->load->view('customer/setting-view',array("data"=>$data,'card' => $card_details,'plan' => $plan_details));
	    $this->load->view('customer/footer_customer');
		//$this->load->view('customer/customer_footer');
	}

	public function edit_profile_image_form(){
		$user_id = $_SESSION['user_id'];
        if ($_FILES['profile']['name'] != "" || isset($_POST)) {  
        	$fname = explode(' ',$_POST['first_name']);      
            if (!empty($_FILES) || isset($_POST)) {
                $config = array(
                    'upload_path' => './uploads/profile_picture',
                    'allowed_types' => "gif|jpg|png|jpge",
                    'encrypt_name' => TRUE
                );
                //$config2['profile'] = $_FILES['profile']['name'];
                $this->load->library('upload', $config);
                // $this->upload->initialize($config2);
                if ($this->upload->do_upload('profile')) {
                    $data = array($this->upload->data());
                    $profile_pic = array(
                        'profile_picture' => $data[0]['file_name'],
                        'first_name' =>  $fname[0],
                        'last_name' => $fname[1],
                        'email' => $_POST['notification_email'],
                    );

                    $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
                    if ($update_data_result) {
                        $this->session->set_flashdata('message_success', 'User Profile Picture Update Successfully.!', 5);
                    } else {
                        $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!', 5);
                    }
                } else {
                    $profile_pic = array(
	                    'first_name' =>  $fname[0],
	                    'last_name' => $fname[1],
	                    'email' => $_POST['notification_email'],
	                );

	                $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
	                if ($update_data_result) {
	                    $this->session->set_flashdata('message_success', 'User Profile Picture Update Successfully.!', 5);
	                } else {
	                    $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!', 5);
	                }
                }
            }
        }
	}
	public function customer_edit_profile(){
        $user_id = $_SESSION['user_id'];
        	if($_POST['company_name'] != ""){
            $profileUpdate = array(  
               'company_name' => $_POST['company_name'],
               'phone' => $_POST['phone'],
               'address_line_1' => $_POST['address_line_1'],
               'title' => $_POST['title'],
               'city' => $_POST['city'],
               'state' => $_POST['state'],
               'zip' => $_POST['zip'],
            );
        $update_data_result = $this->Admin_model->designer_update_profile($profileUpdate, $user_id);
        if ($update_data_result) {
        	//echo "hello";
            $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
        } else {
        	//echo "hiii";
            $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
        }
    	}
     }
 	public function get_customer_detail_from_strip($plan_id)
	{
		$card_detail = $this->Stripe->getcustomerdetail($plan_id);
		return $card = $card_detail->sources->data;
	}
 	public function getdetailplan($plan_id)
	{
		$card_detail = $this->Stripe->getcurrentplan($plan_id);
		return $card_detail->items->data[0]->plan;
	}
}