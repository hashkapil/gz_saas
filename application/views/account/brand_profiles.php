<?php
if ($canuseradd_brand_pro == 0) {
    redirect(base_url() . 'account/request/design_request');
}
?>
<section class="edit-profile-sec">
    <div class="custom_container">
        <div class="header-blog">
            <?php if ($this->session->flashdata('message_error') != '') { ?>
                <div id="message" class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_error'); ?>
                    </p>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('message_success') != '') { ?>
                <div id="message" class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_success'); ?>
                    </p>
                </div>
            <?php }
            ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 catagry-heading">
                        <div class="row" data-step="6" data-intro="Create brand profiles to help organize your projects and add all the important files within the profile." data-position='right' data-scrollTo='tooltip'>
                            <div class="col-md-7">
                                <h3>Brand Profile List <span>Please find below Brand Profiles information.</span></h3>
                            </div>
                            <div class="col-md-5 brand_sub_list">
                                <?php
                                //echo "<pre/>";print_R($profile_data);
                                if ($profile_data[0]['parent_id'] == '0' || $profile_data[0]['role'] == 'manager') {
                                    ?>
                                    <div class="subusers_req_search brand_search">
                                        <?php if (sizeof($subusersdata) > 0) { ?> 
                                            <select data-placeholder="All Clients" class="chosen-select" tabindex="2" onchange="location = this.value;">
                                                <?php if ($_GET['client_id'] == '') { ?>
                                                    <option value="">All Clients</option>
                                                <?php } ?>
                                                <?php for ($i = 0; $i < sizeof($subusersdata); $i++) { ?>
                                                    <option value="<?php echo base_url(); ?>account/brand_profiles?client_id=<?php echo $subusersdata[$i]['id']; ?>" <?php
                                                    if ($_GET['client_id'] == $subusersdata[$i]['id']) {
                                                        echo "selected";
                                                    }
                                                    ?>><?php echo $subusersdata[$i]['first_name'] . ' ' . $subusersdata[$i]['last_name']; ?></option>
                                                        <?php } if ($_GET['client_id'] != '') { ?>
                                                    <option value="<?php echo base_url(); ?>account/brand_profiles">All Clients</option>
                                                <?php } ?>
                                            </select>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                                <?php if ($is_add_brandprofile != 0) { ?>
                                    <a href="<?php echo base_url(); ?>account/add_brand_profile" class="new-brand">+ Add New Brand Profile</a>
                                <?php } else { ?>
                                    <a data-toggle="modal" data-target="#accessdeniedbrand" class="new-brand">+ Add New Brand Profile</a>    
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row">
                            <?php
                            if (sizeof($user_profile_brands) > 0) {
                                for ($i = 0; $i < sizeof($user_profile_brands); $i++) {
                                    ?>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="brand-card">
                                            <div class="bCard-header">
                                                <?php
                                                if (!empty($user_profile_brands[$i]['client_name'])) {
                                                    if ($profile_data[0]['parent_id'] == '0' || $profile_data[0]['user_flag'] == 'manager') {
                                                        ?>  
                                                        <h2>Added By : <span><?php echo $user_profile_brands[$i]['client_name'][0]['first_name'] . ' ' . $user_profile_brands[$i]['client_name'][0]['last_name']; ?></span></h2>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                                <a href="<?php echo base_url(); ?>account/add_brand_profile?editid=<?php echo $user_profile_brands[$i]['id']; ?>">
                                                    <i class="icon-gz_edit_icon"></i>
                                                </a>
                                                <a data-id="<?php echo $user_profile_brands[$i]['id']; ?>" class="delete_brands">
                                                    <i class="icon-gz_delete_icon"></i>
                                                </a>
                                            </div>
                                            <div class="user-image">
                                                <?php
                                                $type = substr($user_profile_brands[$i]['latest_logo'], strrpos($user_profile_brands[$i]['latest_logo'], '.') + 1);
                                                $allow_type = array("jpg", "jpeg", "png", "gif");
                                                if (isset($user_profile_brands[$i]['latest_logo']) && in_array(strtolower($type), $allow_type)) {
                                                    ?>
                                                    <img class="cross_popup" data-dismiss="modal" src="<?php echo FS_PATH_PUBLIC_UPLOADS . 'brand_profile/' . $user_profile_brands[$i]['id'] . '/' . $user_profile_brands[$i]['latest_logo']; ?>">
                                                <?php } else { ?>
                                                    <!-- <img class="cross_popup" data-dismiss="modal" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/user-bg.svg"> -->
                                                <?php } ?>
                                            </div>
                                            <div class="name-info"><a href="<?php echo base_url(); ?>account/add_brand_profile?editid=<?php echo $user_profile_brands[$i]['id']; ?>">
                                                    <h3> <?php echo $user_profile_brands[$i]['brand_name']; ?></h3></a>
                                                <?php if (strpos($user_profile_brands[$i]['website_url'], 'http') !== false || strpos($user_profile_brands[$i]['website_url'], 'https') !== false) { ?>
                                                    <a href="<?php echo $user_profile_brands[$i]['website_url']; ?>" target="_blank"><p><?php echo $user_profile_brands[$i]['website_url']; ?></p></a>
                                                <?php } else { ?>
                                                    <a href="http://<?php echo $user_profile_brands[$i]['website_url']; ?>" target="_blank"><p><?php echo $user_profile_brands[$i]['website_url']; ?></p></a>
                                            <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade similar-prop" id="projectBrand" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Delete Brand</h2>
                    <div class="cross_popup" data-dismiss="modal"> x</div>
                </header>
                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/delete-bg.svg">


                    <h3 class="head-c text-center">Are you sure you want to delete this brand? </h3>

                    <div class="confirmation_btn three-lbl text-center"> 
                        <form action="<?php echo base_url(); ?>account/BrandProfile/delete_brand_profile" method="post" >
                            <div class="del-brfrof">
                                <div class="sound-signal">
                                    <div class="form-radion">
                                        <input type="hidden" name="brand_ids" id="brand_ids" class="brand_ids" value=""> 
                                        <input type="radio" name="delete_brand" id="del_all_req" class="brand_radio" value="1">
                                        <label for="del_all_req">Delete all requests of this brand profile</label>
                                    </div>
                                    <div class="form-radion">
                                        <input type="radio" name="delete_brand" id="reassign_req" class="brand_radio" value="2">
                                        <label for="reassign_req">Reassign requests to other brand profile</label>
                                        <div class="assign_brand_to_req"></div> 
                                    </div>
                                    <div class="form-radion">
                                        <input type="radio" name="delete_brand" id="del_ass_brnd" class="brand_radio" value="3" checked> 
                                        <label for="del_ass_brnd">Remove brand assigning from these requests</label>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" name="delete_brand_profile" class="btn btn-ydelete" >delete</button>
                            <button class="btn btn-ndelete" data-dismiss="modal" aria-label="Close">Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade similar-prop" id="accessdeniedbrand" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">


            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Access Denied</h2>
                <div class="cross_popup" data-dismiss="modal"> x </div>
            </header>
            <div class="cli-ent-model">
                <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
                <h3 class="head-c text-center">You don't have access to add brand profile. Please contact us for more info. </h3>
            </div>
        </div>
    </div>
</div>
<button style="display: none;" id="delete" data-toggle="modal" data-target="#projectBrand">click here</button>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/chosen.jquery.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/init.js"></script>
<script>
    var brand_profiles_data = <?php echo json_encode($user_profile_brands); ?>;
</script>