<style>
    .by_brand .chosen-container .chosen-drop ul.chosen-results li:first-child {
    display: none;
}
</style>
<?php
if ($login_user_data[0]['user_flag'] == "client") {
    $target = "#dontaccesstosubuser";
} else {
    $target = "#alredyassignedreq";
}

if($login_user_data[0]["role"] == "manager" && $assign_designer_toproject != 1){
    $c_role = "m_user";
}else{
    $c_role = $u_role;
}
?>
<section class="con-b">
    <div class="custom_container">
        <div class="header-blog">
                <div class="alert alert-danger alert-dismissable error" style="display:none">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c" id="show_error"></p>
                        <?php echo $this->session->flashdata('message_error'); ?>
                    </p>
                </div>
                <div class="alert alert-success alert-dismissable success" style="display:none">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <p class="head-c">
                        <?php echo $this->session->flashdata('message_success'); ?>
                    </p>
                </div>
            <div class="row flex-show">
                <div class="col-md-9">
                    <ul id="status_check" class="list-unstyled list-header-blog" role="tablist" data-u_role="<?php echo $c_role; ?>" style="border:none;">
                        <?php if ($login_user_data[0]["is_saas"] == 1 || $login_user_data[0]["role"] == "manager") { ?>
                            <li class="active" id="1">
                                <a data-toggle="tab" href="#designs_request_tab" data-isloaded="0" data-view="grid" data-status="active,disapprove" data-s_class ="in_active" data-tabid="1" role="tab" data-pagin ="<?php echo $count_active_project; ?>">Active <span>(<?php echo isset($count_active_project)?$count_active_project:0; ?>)</span> </a>
                            </li>
                            <li id="2">
                                <a data-toggle="tab" href="#in_queue_tab" data-isloaded="0" data-view="grid" data-status="assign" role="tab" data-s_class ="in_queue" data-tabid="2" data-pagin ="<?php echo $count_project_i; ?>">In Queue <span>(<?php echo isset($count_project_i)?$count_project_i:0; ?>)</span>  </a>
                            </li>  
                            <li id="3">
                                <a data-toggle="tab" href="#pending_review_tab" data-isloaded="0" data-view="grid" data-status="pendingrevision" data-s_class ="pending_rev" data-tabid="3" data-pagin ="<?php echo $count_project_p; ?>" role="tab">Pending Review <span>(<?php echo isset($count_project_p)?$count_project_p:0; ?>)</span></a>
                            </li>         
                            <li id="4">
                                <a data-toggle="tab" href="#pendingapproval_designs_tab" data-isloaded="0" data-view="grid" data-status="checkforapprove" data-s_class ="pending_app" data-tabid="4" data-pagin ="<?php echo $count_project_checkforap; ?>" role="tab">Pending Approval <span>(<?php echo isset($count_project_checkforap)?$count_project_checkforap:0; ?>) </span></a>
                            </li>       
                            <li id="5">
                                <a data-toggle="tab" href="#approved_designs_tab" data-isloaded="0" data-view="grid" data-status="approved" data-tabid="5" data-s_class ="completed" data-pagin ="<?php echo $count_project_c; ?>" role="tab">Completed <span>(<?php echo isset($complete_project_count)?$complete_project_count:0; ?>) </span></a>
                            </li> 
                            <li id="6">
                                <a data-toggle="tab" href="#hold_designs_tab" data-isloaded="0" data-view="grid" data-status="hold" data-tabid="6" data-s_class ="hold" data-pagin ="<?php echo $count_project_h; ?>" role="tab">On Hold <span>(<?php echo isset($hold_project_count)?$hold_project_count:0; ?>)</span></a>
                            </li> 
                            <li id="7">
                                <a data-toggle="tab" href="#cancel_designs_tab" data-isloaded="0" data-view="grid" data-status="cancel" data-tabid="7" data-s_class ="cancel" data-pagin ="<?php echo $count_project_cancel; ?>" role="tab">Cancelled <span>(<?php echo isset($cancel_project_count)?$cancel_project_count:0; ?>)</span></a>
                            </li> 
                        <?php } else { ?>
                            <li class="active" id="1"><a data-toggle="tab" data-isloaded="0" data-view="grid" data-status="active,disapprove,assign,pending,checkforapprove" href="#designs_request_tab" role="tab">Active (<?php echo isset($count_active_project) ? $count_active_project : 0; ?>)</a></li>			
                            <li class="" id="2"><a class="nav-link tabmenu" data-isloaded="0" data-view="grid" data-status="draft" data-toggle="tab" href="#inprogressrequest" role="tab">Draft (<?php echo isset($draft_count) ? $draft_count : 0; ?>)</a></li> 			
                            <li class="" id="3"><a class="nav-link tabmenu" data-isloaded="0" data-view="grid" data-status="approved" data-toggle="tab" href="#approved_designs_tab" role="tab">Completed (<?php echo isset($complete_project_count) ? $complete_project_count : 0; ?>) </a></li>
                            <li class="" id="4"><a class="nav-link tabmenu" data-isloaded="0" data-view="grid" data-status="cancel" data-toggle="tab" href="#cancel_designs_tab" role="tab">Cancelled (<?php echo isset($cancel_project_count) ? $cancel_project_count : 0; ?>) </a></li> 
                            <li class="" id="5"><a class="nav-link tabmenu" data-isloaded="0" data-view="grid" data-status="hold" data-toggle="tab" href="#hold_designs_tab" role="tab">Hold (<?php echo isset($hold_project_count) ? $hold_project_count : 0;?>) </a></li> 
                        <?php } ?>
                    </ul>
                </div>
                <div class="col-md-3 text-right">
                    <?php if($login_user_data[0]["is_saas"] == 1 || ($login_user_data[0]["role"] == "manager" && $assign_designer_toproject == 1)){ ?>
                        <a href="javascript:void(0)" class="adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $active_project[$i]['id']; ?>" data-designerid= "<?php echo $active_project[$i]['designer_id']; ?>">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/assign_designer.svg" class="img-responsive"><span>Assign Designer</span>
                        </a>
                    <?php } ?>
                    <div class="subusers_req_search brand_search"><?php if (sizeof($subusersdata) > 0) { ?> 
                            <select data-placeholder="All Clients" class="chosen-select" tabindex="2" onchange="location = this.value;">
                                <?php if ($_GET['client_id'] == '') { ?>
                                    <option value="">All Clients</option>
                                <?php } ?>
                                <?php for ($i = 0; $i < sizeof($subusersdata); $i++) { ?>
                                    <option value="<?php echo base_url(); ?>account/request/design_request?client_id=<?php echo $subusersdata[$i]['id']; ?>" <?php
                                    if ($_GET['client_id'] == $subusersdata[$i]['id']) {
                                        echo "selected";
                                    }
                                    ?>><?php echo $subusersdata[$i]['first_name'].' '.$subusersdata[$i]['last_name']; ?></option>
                                    <?php } if ($_GET['client_id'] != '') { ?>
                                    <option value="<?php echo base_url(); ?>account/request/design_request">All Clients</option>
                                <?php }
                                ?>
                            </select>
                        <?php } ?></div>
<!--                    <div class="brand_search by_brand"><?php if (sizeof($user_profile_brands) > 0) { ?> 
                            <select data-placeholder="All Brand Profiles" class="chosen-select" tabindex="2" onchange="location = this.value;">
                               <?php if ($_GET['brand_id'] == '') { ?>
                                <option value="">All Brand Profiles</option>
                               <?php } ?>
                                <option value="<?php echo base_url(); ?>account/add_brand_profile"><div>+</div> Add Profile</option>
                                <?php for ($i = 0; $i < sizeof($user_profile_brands); $i++) { ?>
                                    <option value="<?php echo base_url(); ?>account/request/design_request?brand_id=<?php echo $user_profile_brands[$i]['id']; ?>" <?php
                                    if ($_GET['brand_id'] == $user_profile_brands[$i]['id']) {
                                        echo "selected";
                                    }
                                    ?>> <p class="logo_icn"></p><?php echo $user_profile_brands[$i]['brand_name']; ?></option>
                                    <?php 
                                } 
                                if ($_GET['brand_id'] != '') { ?>
                                    <option value="<?php echo base_url(); ?>account/request/design_request">All Brand Profiles</option>
                                <?php } 
                                ?>
                            </select>
                        <?php } ?></div> -->
<!--                    <div class="list-grid">
                        <a href="javascript:void(0)" data-view='list' data-satus="" class="view_class active" id="list_view">
                            <i class="icon-gz_list_icon"></i>
                        </a>
                        <a href="javascript:void(0)" data-view='grid' data-satus="" class="view_class" id="grid_view">
                            <i class="icon-gz_grid_icon"></i>
                        </a>
                    </div>-->
                    <div class="search-first">
                     <div class="focusout-search-box">
                        <div class="search-box" <?php if ($brand_id != '') { ?> style="right: 351px;" <?php } ?>>
                            <form method="post" class="search-group clearfix">
                                <input type="text" placeholder="Search here..." class="form-control searchdata search_text" data-view="">
                                <div class="ajax_searchload" style="display:block;">
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" />
                                </div>
                                <input type="hidden" name="status" id="status" value="active,disapprove,assign,pending,checkforapprove">
                                <div class="close-search"><i class="far fa-eye-slash"></i></div>
                                    <button type="submit" class="search-btn search search_data_ajax">
                                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_search_icon.svg" class="img-responsive">
                                    </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p class="space-c"></p>
    <div class="tab-content s_projct_lst" data-step="2" data-intro="View and manage all of your projects from the project dashboard. You can delete, duplicate or reprioritize projects" data-position='right' data-scrollTo='tooltip'>
        <div class="loader_tab" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
        <div data-group="1" data-loaded=""  data-total-count="<?php echo $count_active_project; ?>" class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
            <div class="product-list-show">
                <div class="row">
                    <div class="grid_add" style="display:none;">
                        <?php 
//                        echo $canuseradd."<br>".$canuseraddrequest;exit;
                        if($canuseradd != 0){ ?>
                            <!-- Pro Box Start -->
                            <div class="col-md-3 col-sm-6">
                                <div class="add_profordesign">
                                <?php
                            //echo $canuseraddrequest;
                            // for trail status = 0
                                if ($canuseraddrequest == 0) {
                                // echo "123";
                                    ?>
                                    <a href="javascript:void(0)" data-url="" class="pro-productss pro-box-s1 trail-expire">
                                        <div class="pro-box-caps1">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_add_project.svg" class="img-responsive">
                                            
                                            <span class="pro-add-texts1" href="<?php echo base_url(); ?>account/request/add_new_request">Create Project</span>
                                            <p class="small-click">Click here to add new Project</p>
                                        </div>
                                    </a>
                                    <?php
                                } 
                            // for $99 user status = 1
                                if ($canuseraddrequest == 1) {
                                    ?>
                                    <a href="javascript:void(0)" data-url="" data-toggle="modal" data-target="#ChangePlan" class="pro-productss pro-box-s1">
                                        <div class="pro-box-caps1">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_add_project.svg" class="img-responsive">
                                            <span class="pro-add-texts1" href="<?php echo base_url(); ?>account/request/add_new_request">Create Project</span>
                                            <p class="small-click">Click here to add new Project</p>
                                        </div>
                                    </a>
                                    <?php
                                }
                            // for add new request user status = 1
                                if ($canuseraddrequest == 2) { 
                                    ?>
                                    <a href="<?php echo base_url(); ?>account/request/add_new_request" class="pro-productss pro-box-s1 tst">
                                        <div class="pro-box-caps1">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_add_project.svg" class="img-responsive">
                                            <span class="pro-add-texts1 " href="<?php echo base_url(); ?>account/request/add_new_request">Create Project</span>
                                            <p class="small-click">Click here to add new Project</p>
                                        </div>
                                    </a>
                                    <?php
                                } 
                                if ($canuseraddrequest == 4) {
                                    ?>
                                    <a href="javascript:void(0)" data-url="" data-toggle="modal" data-target="#upgradefortynineplan" class="pro-productss pro-box-s1">
                                        <div class="pro-box-caps1">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_add_project.svg" class="img-responsive">
                                            <span class="pro-add-texts1" href="<?php echo base_url(); ?>account/request/add_new_request">Create Project</span>
                                            <p class="small-click">Click here to add new Project</p>
                                        </div>
                                    </a>
                                    <?php
                                } 
                                if ($canuseraddrequest == 5) {
                                    ?>
                                    <a href="javascript:void(0)" data-url="" data-toggle="modal" data-target="#dontaccesstosubuser" class="pro-productss pro-box-s1">
                                        <div class="pro-box-caps1">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_add_project.svg" class="img-responsive">
                                            <span class="pro-add-texts1" href="<?php echo base_url(); ?>account/request/add_new_request">Create Project</span>
                                            <p class="small-click">Click here to add new Project</p>
                                        </div>
                                    </a>
                                    <?php
                                }
                                if ($canuseraddrequest == 3 && $profile_data[0]['parent_id'] == 0) {
                                ?>
<!--                                    <a href="javascript:void(0)" data-url="" data-toggle="modal" data-target="#reActivate" class="pro-productss pro-box-s1 disabled_due_cancel">-->
                                        <a class="pro-productss pro-box-s1 disabled_due_cancel">
                                        <div class="pro-box-caps1">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_add_project.svg" class="img-responsive">
                                            <span class="pro-add-texts1" href="<?php echo base_url(); ?>account/request/add_new_request">Create Project</span>
                                            <p class="small-click">Click here to add new Project</p>
                                        </div>
                                    </a>
                                <?php
                            }if ($canuseraddrequest == 3 && $profile_data[0]['parent_id'] != 0) {
                                ?>
                                    <a href="javascript:void(0)" data-url="" data-toggle="modal" data-target="#cancelSubscriptionsubuser" class="pro-productss pro-box-s1">
                                        <div class="pro-box-caps1">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_add_project.svg" class="img-responsive">
                                            <span class="pro-add-texts1" href="<?php echo base_url(); ?>account/request/add_new_request">Create Project</span>
                                            <p class="small-click">Click here to add new Project</p>
                                        </div>
                                    </a>
                                <?php
                                }if ($canuseraddrequest == 6) { ?>
                                    <a href="javascript:void(0)" data-url="" data-toggle="modal" data-target="<?php echo $target; ?>" class="pro-productss pro-box-s1">
                                        <div class="pro-box-caps1">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_add_project.svg" class="img-responsive">
                                            <span class="pro-add-texts1" href="<?php echo base_url(); ?>account/request/add_new_request">Create Project</span>
                                            <p class="small-click">Click here to add new Project</p>
                                        </div>
                                    </a>
                            <?php } ?>
                            </div>
						</div>
                            <!-- Pro Box End -->
                        <?php } ?>
                    </div>
                    <div class="list-add-project">
                            <?php // echo $canuseradd." - ".$canuseraddrequest;
                            if($canuseradd != 0){ ?>
                                <!-- Pro Box Start -->
                                <div class="col-md-12">
                                    <div>
                                    <?php
                            // for add new request user status = 1
                                    if ($canuseraddrequest == 2) { 
                                        ?>
                                        <a href="<?php echo base_url(); ?>account/request/add_new_request" class="pro-productss pro-box-s1 tst">
                                            <div class="pro-box-caps1">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_add_project.svg" class="img-responsive">
                                                <div class="create-prjct">
                                                    <span class="pro-add-texts1 " href="<?php echo base_url(); ?>account/request/add_new_request">Create Project</span>
                                                    <p class="small-click">Click here to add new Project</p>
                                                </div>
                                            </div>
                                        </a>
                                        <?php
                                    } 
                                    if ($canuseraddrequest == 5) {
                                        ?>
                                        <a href="javascript:void(0)" data-url="" data-toggle="modal" data-target="#dontaccesstosubuser" class="pro-productss pro-box-s1">
                                            <div class="pro-box-caps1">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_add_project.svg" class="img-responsive">
                                                <div class="create-prjct">
                                                    <span class="pro-add-texts1" href="<?php echo base_url(); ?>account/request/add_new_request">Create Project</span>
                                                    <p class="small-click">Click here to add new Project</p>
                                                </div>
                                            </div>
                                        </a>
                                        <?php
                                    } 
                                    if ($canuseraddrequest == 3 && $profile_data[0]['parent_id'] == 0) {
                                    ?>
                                        <a class="pro-productss pro-box-s1 disabled_due_cancel">
                                            <div class="pro-box-caps1">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_add_project.svg" class="img-responsive">
                                                <div class="create-prjct">
                                                <span class="pro-add-texts1" href="<?php echo base_url(); ?>account/request/add_new_request">Create Project</span>
                                                <p class="small-click">Click here to add new Project</p>
                                                </div>
                                            </div>
                                        </a>
                                    <?php } if ($canuseraddrequest == 3 && $profile_data[0]['parent_id'] != 0) {
                                            ?>
                                            <a href="javascript:void(0)" data-url="" data-toggle="modal" data-target="#cancelSubscriptionsubuser" class="pro-productss pro-box-s1">
                                                <div class="pro-box-caps1">
                                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/gz_icons/gz_add_project.svg" class="img-responsive">
                                                    <div class="create-prjct">
                                                        <span class="pro-add-texts1" href="<?php echo base_url(); ?>account/request/add_new_request">Create Project</span>
                                                        <p class="small-click">Click here to add new Project</p>
                                                    </div>
                                                </div>
                                            </a>
                                    <?php
                                    }?>
                                </div> 
                                </div>
                                <!-- Pro Box End -->
                            <?php }

                            ?>
                        </div>
                        <?php
                        $activeTab = 0;
                        foreach ($active_project as $project) {
                            if ($project['status'] == "assign") {
                                $activeTab++;
                                $data['activeTab'] = $activeTab;
                            }
                        }
                   //     if($count_active_project != 0){
                        ?>	
                        <div id="prior_data">
                            <div class="col-md-12">
                                <div class="list-view-table">
                                    <span id="loadingAjaxCount" data-value="<?php echo $count_active_project; ?>"></span>
                                    <table>
                                        <thead>
                                            <tr>
                                                <?php if($login_user_data[0]["is_saas"] == 1 || ($login_user_data[0]["role"] == "manager" && $assign_designer_toproject == 1)){ ?>
                                                    <th>
                                                        <div class="sound-signal">
                                                            <div class="form-radion2">
                                                                <label class="containerr">
                                                                    <input type="checkbox" class="checkAll" data-attr="in_active" name="project">  
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </th>
                                                <?php } ?>
                                                <th>project name</th>
                                                <th>Client</th>
                                                <th>Designer</th>
                                                <th>project status</th>
                                                <th>project time</th>
                                                <?php if($u_role != "customer"){ ?>
                                                <th>verified</th>
                                                <?php } ?>
                                                <th>action</th>
                                            </tr>
                                        </thead>
                                        <tbody> 
                            <?php
                            for ($i = 0; $i < count($active_project); $i++) {
                                if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "checkforapprove" || $active_project[$i]['status'] == "disapprove") {
                                    $data['projects'] = $active_project[$i];  
                                    $data['user_data'] = $active_project[$i]['user_data'];
                                    // echo $active_project[$i]['view_type'] ;exit;
                                    $this->load->view('account/projects_list_view',$data); }
                                    if ($active_project[$i]['status'] != "active" && $active_project[$i]['status'] != "checkforapprove" && $active_project[$i]['status'] != "disapprove")
                                    { 
                                      $data['projects'] = $active_project[$i]; 
                                      $data['user_data'] = $active_project[$i]['user_data'];
                                      $this->load->view('account/projects_list_view',$data);   
                                    }
                              }
                              ?>
                            </tbody>   
                            </table>
                          </div>	
                            </div>
                            <?php if ($count_active_project > LIMIT_ALL_LIST_COUNT) { ?>
                              <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                              <div class="" style="text-align:center">
                                  <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_active_project; ?>" class="load_more  theme-save-btn button">Load more</a>
                              </div>
                            <?php } ?>
                        </div>
                     <?php //} ?>
                </div>
                   </div>
               </div>

        <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $count_project_i; ?>" class="tab-pane content-datatable datatable-width" id="in_queue_tab" role="tabpanel">
            <!-- Drafts -->
            <div class="product-list-show">
                <div class="row">
                    <!-- Pro Box Start -->	

                </div>

                <?php if ($count_project_i > LIMIT_ALL_LIST_COUNT) { ?>
                    <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                    <div class="" style="text-align:center">
                        <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_i; ?>" class="load_more theme-save-btn">Load more</a>
                    </div> 
                <?php } ?>
            </div>

            <p class="space-e"></p>
        </div>
        
        <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $draft_count; ?>" class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">
            <!-- Drafts -->
            <div class="product-list-show">
                <div class="row">
                    <!-- Pro Box Start -->	

                </div>

                <?php if ($draft_count > LIMIT_ALL_LIST_COUNT) { ?>
                    <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                    <div class="" style="text-align:center">
                        <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $draft_count; ?>" class="load_more theme-save-btn">Load more</a>
                    </div> 
                <?php } ?>
            </div>

            <p class="space-e"></p>
        </div>
        <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $count_project_p; ?>" class="tab-pane content-datatable datatable-width" id="pending_review_tab" role="tabpanel">
            <!-- Drafts -->
            <div class="product-list-show">
                <div class="row">
                    <!-- Pro Box Start -->	

                </div>

                <?php if ($count_project_p > LIMIT_ALL_LIST_COUNT) { ?>
                    <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                    <div class="" style="text-align:center">
                        <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_p; ?>" class="load_more theme-save-btn">Load more</a>
                    </div> 
                <?php } ?>
            </div>

            <p class="space-e"></p>
        </div>
        <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $count_project_checkforap; ?>" class="tab-pane content-datatable datatable-width" id="pendingapproval_designs_tab" role="tabpanel">
            <!-- Drafts -->
            <div class="product-list-show">
                <div class="row">
                    <!-- Pro Box Start -->	

                </div>

                <?php if ($count_project_checkforap > LIMIT_ALL_LIST_COUNT) { ?>
                    <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                    <div class="" style="text-align:center">
                        <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_project_checkforap; ?>" class="load_more theme-save-btn">Load more</a>
                    </div> 
                <?php } ?>
            </div>

            <p class="space-e"></p>
        </div>
    <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $complete_project_count; ?>" class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">

        <!-- Complete Projects -->
        <div class="product-list-show">
            <div class="row">
                <!-- Pro Box Start -->

            </div>
            <?php if ($complete_project_count > LIMIT_ALL_LIST_COUNT) { ?>
                <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                <div class="" style="text-align:center">
                    <a id="load_more" href="javascript:void(0)" data-row = "0" data-count="<?php echo $complete_project_count; ?>" class="load_more theme-save-btn">Load more</a>
                </div> 
            <?php } ?>
        </div>
        <p class="space-e"></p>
    </div>
    <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $cancel_project_count; ?>" class="tab-pane content-datatable datatable-width" id="cancel_designs_tab" role="tabpanel">

        <!-- Complete Projects -->
        <div class="product-list-show">
            <div class="row">
                <!-- Pro Box Start -->

            </div>
            <?php if ($cancel_project_count > LIMIT_ALL_LIST_COUNT) { ?>
                <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif" /></div>
                <div class="" style="text-align:center">
                    <a id="load_more" href="javascript:void(0)" data-row = "0" data-count="<?php echo $cancel_project_count; ?>" class="load_more theme-save-btn">Load more</a>
                </div> 
            <?php } ?>
        </div>
        <p class="space-e"></p>
    </div>
        <div data-group="1" data-loaded="" data-search-text="" data-total-count="<?php echo $hold_project_count; ?>" class="tab-pane content-datatable datatable-width" id="hold_designs_tab" role="tabpanel">
                <!-- Complete Projects -->
                <div class="product-list-show">
                    <div class="row">
                        <!-- Pro Box Start -->

                    </div>
                    <?php if ($hold_project_count > LIMIT_ALL_LIST_COUNT) { ?>
                        <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/gz_customer_loader.gif" /></div>
                        <div class="" style="text-align:center">
                            <a id="load_more" href="javascript:void(0)" data-row = "0" data-count="<?php echo $hold_project_count; ?>" class="load_more theme-save-btn">Load more</a>
                        </div> 
                    <?php } ?>
                </div>
                <p class="space-e"></p>
            </div>
    </div>

</section>
<!-- Modal -->
<div class="modal fade similar-prop" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <input type="hidden" name="priorityFrom" id="priorityFrom" value=""/>
            <input type="hidden" name="priorityto" id="priorityto" value=""/>
            <input type="hidden" name="user_type" id="priority_user_type" value=""/>
            <input type="hidden" name="id" id="id" value=""/>
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Change the priority</h2>
                <div class="cross_popup" data-dismiss="modal"> x </div>
            </header>
                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS.'img/gz_icons/change-status.svg'?>">
                        <h3 class="head-c text-center">Are you sure you want to change the priority of this project?</h3>
                   
                        <div class="confirmation_btn text-center">
                    <button class="btn btn-ydelete btn-y-priority" data-dismiss="modal" aria-label="Close">Yes</button>
                    <button class="btn btn-ndelete" data-dismiss="modal" aria-label="Close">No</button>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Model for delete project -->
<div class="modal fade similar-prop" id="projectDelete" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
                
               <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Delete project</h2>
                    <div class="cross_popup" data-dismiss="modal"> x </div>
                    </header>
            <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/delete-bg.svg">
                        <h3 class="head-c text-center">Are you sure you want to delete this project? </h3>
                    
                    <div class="confirmation_btn text-center">
                        <a href="" class="btn btn-ydelete">Delete</a>
                        <button class="btn btn-ndelete" data-dismiss="modal" aria-label="Close">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade similar-prop" id="projectcopy" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <img class="cross_popup" data-dismiss="modal" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cross.png">
                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/duplicate.png">
                    <h2 class="popup_h2">Duplicate project</h2>
                    <header class="fo-rm-header">
                        <h3 class="head-c text-center">Are you sure you want to duplicate this project? </h3>
                    </header>
                    <div class="confirmation_btn text-center">
                        <a href="" class="btn btn-ndelete">Yes, duplicate it</a>
                        <button class="btn btn-ydelete" data-dismiss="modal" aria-label="Close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="TrialExpire" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <div class="cli-ent-model">
                    <header class="fo-rm-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>                   
                    </header>
                    <div class="confirmation_body">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/cli-ent-icon.png" alt="Graphics Zoo" title="Graphics Zoo">
                        <p>You have run out of free designs.<br/> you must <a data-value="M299G" class="UpgrdSubs" href="javascript:void(0);">upgrade to continue</a>
                        getting unlimited graphic design services.</p>
                        <a href="javascript:void(0)" data-value="M299G" type="button" class="UpgrdSubs ud-dat-p">
                            UPGRADE NOW
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade similar-prop" id="dontaccesstosubuser" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                
                    <header class="fo-rm-header">
                        <h2 class="popup_h2 del-txt">Access Denied</h2>
                        <div class="cross_popup" data-dismiss="modal"> x </div>
                    </header>
                <div class="cli-ent-model">
                    <div class="confirmation_body">
                        <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
                        <h3 class="head-c text-center">You don't have permission to add request. please <a id="contactwithagency"> contact</a> to support</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Model for Reactivate subscription -->
<div class="modal fade similar-prop" id="cancelSubscriptionsubuser" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Cancel Subscription</h2>
                <div class="cross_popup" data-dismiss="modal"> x </div>
                </header>
                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
                        <h3 class="head-c text-center">Sorry, you can't add request,your subscription is cancelled.</h3>
                </div>status_check
            </div>
        </div>
    </div>
</div>
<!-- access denied -->
<div class="modal fade similar-prop" id="alredyassignedreq" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Access Denied</h2>
                <div class="cross_popup" data-dismiss="modal"> x </div>
                </header>
                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/default-img/cancelsubs_bg.svg">
                        <h3 class="head-c text-center">You have already assigned all of your dedicated designers to your clients. So you can't add more requests.</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal similar-prop nonflex fade" id="AddDesign" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-nose" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                <h2 class="popup_h2 del-txt">Add Designer</h2>
                <div id="close-d" class="cross_popup close edit_close" data-dismiss="modal"> x</div>
            </header>
            <div class="cli-ent-model-box">
                <div class="assigndesigner_err"></div>
                <div class="cli-ent-model">
                    <div class="noti-listpopup">
                        <div class="newsetionlist">
                            <div class="cli-ent-row tr notificate">
                                <div class="cli-ent-col td" style="width: 33%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Designer</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 45%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b">Skill</h3>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 22%;">
                                    <div class="cli-ent-xbox text-left">
                                        <h3 class="pro-head space-b text-center">Active Requests</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="" method="post">
                            <ul class="list-unstyled list-notificate two-can">
                                <?php foreach ($designer_list as $designers) { ?>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <div class="cli-ent-row tr notificate">
                                                <div class="cli-ent-col td" >
                                                    <div class="sound-signal">
                                                        <div class="form-radion">
                                                            <input class="selected_btn" type="radio" value="<?php echo $designers['id']; ?>" name="assign_designer" id="de_<?php echo $designers['id']; ?>" data-image-pic="<?php echo $designers['profile_picture'] ?>" data-name="<?php echo $designers['first_name'] . " " . $designers['last_name']; ?>">
                                                            <label for="de_<?php echo $designers['id']; ?>" data-image-pic="<?php echo $designers['profile_picture'] ?>"></label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 30%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="setnoti-fication">
                                                            <figure class="pro-circle-k1">
                                                                <img src="<?php echo $designers['profile_picture'] ?>" class="img-responsive">
                                                            </figure>

                                                            <div class="notifitext">
                                                                <p class="ntifittext-z1">
                                                                    <strong>
                                                                        <?php echo $designers['first_name'] . " " . $designers['last_name']; ?>

                                                                    </strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 45%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <p class="pro-a">UX Designer, Landing page, Mobile App  UX Designer, Landing page, Mobile App<span class="sho-wred">+2</span></p>
                                                    </div>
                                                </div>

                                                <div class="cli-ent-col td" style="width: 25%;">
                                                    <div class="cli-ent-xbox text-left">
                                                        <div class="cli-ent-xbox text-center">

                                                            <p class="neft text-center"><span class="red text-uppercase"><?php echo $designers['active_request']; ?></span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </li>  
                                <?php } ?>                          
                            </ul>
                            <input type="hidden" name="request_id" id="request_id">
                            <p class="space-c"></p>
                            <p class="btn-x text-center">
                                <button name="submit" type="submit" id="assign_desig" class="button all-button" data-dismiss="modal" aria-label="Close">Assign Designer</button>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<button style="display: none;" id="contactwithagencyusers" data-toggle="modal" data-target="#contact_detail">click here</button>
<button style="display: none;" id="confirmation" data-toggle="modal" data-target="#myModal">click here</button>
<button style="display: none;" id="delete" data-toggle="modal" data-target="#projectDelete">click here</button>
<button style="display: none;" id="procopy" data-toggle="modal" data-target="#projectcopy">click here</button>
<button style="display: none;" id="trial_expire" data-toggle="modal" data-target="#TrialExpire">click here</button>

<div id="footer_div"></div>
<!-- jQuery (necessary for JavaScript plugins) -->
<!--<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/customer_portal.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/customer/bootstrap.min.js"></script>-->
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/chosen.jquery.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/init.js"></script>

<script>
var $brand_id = "<?php echo isset($_GET['brand_id']) ? $_GET['brand_id'] : ''?>";
var $client_id = "<?php echo isset($_GET['client_id']) ? $_GET['client_id'] : '' ?>";
</script>
