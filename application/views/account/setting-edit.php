<p class="space-b"></p>
	
	<section class="con-b">
		<div class="container">
			<?php if($this->session->flashdata('message_error') != '') {?>
				<div id="message" class="alert alert-danger alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
				  
				  	<p class="head-c"><?php echo $this->session->flashdata('message_error'); ?></p>
				</div>
				<?php }?>

				<?php if($this->session->flashdata('message_success') != '') {?>
				<div id="message" class="alert alert-success alert-dismissable">
				  <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
				  <p class="head-c"><?php echo $this->session->flashdata('message_success');?></p>
				</div>
			<?php }?>

		<form action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-4">
					<div class="project-row-qq1 settingedit-box">
						<div class="settrow">
							<div class="settcol right">
								<p class="btn-x"><input type="submit" id="savebtn1" name="savebtn" class="btn-e save_info" value="Save"/></p> 
							</div>
						</div>
						
						<div class="setimg-row33 changeprofilepage">
							<div class="imagemain2" style="padding-bottom: 20px; display: none;">
							  <input type="file" onChange="validateAndUpload(this);" class="form-control dropify waves-effect waves-button" name="profile" data-plugin="dropify" id="inFile" style="display: none"/>
							</div>
							<div class="clearfix profile_image_sec">
								<div class="imagemain">
									<figure class="setimg-box33">
										<?php if ($data[0]['profile_picture']): ?>
											<img src="<?php echo base_url()."uploads/profile_picture/".$data[0]['profile_picture']; ?>" class="img-responsive telset33">
											<?php else: ?>
												<div class="myprofilebackground" style="width:50px;height:50px;border-radius:50%;background: #0190ff;text-align: center;padding-top: 15px;font-size: 15px;color: #fff;">
												<?php 
													$first_name = ""; $last_name="";
													  if(isset($data[0]['first_name'])){
													      $first_name = substr($data[0]['first_name'],0,1);
													  }
													  if(isset($data[0]['last_name'])){
													      $last_name = substr($data[0]['last_name'],0,1);
													  }
													  echo $first_name.$last_name; ?>
												 ?>
												 </div>
										<?php endif; ?>
										
										<a class="setimg-blog33 link1 font18 bold" href="#"  onclick="$('.dropify').click();" class="link1 font18 bold" for="profile">
											<span class="setimgblogcaps">
												<img src="<?php echo base_url() ?>theme/customer-assets/images/icon-camera.png" class="img-responsive"><br>
												<span class="setavatar33">Change <br>Avatar</span>
											</span>
										</a>
									</figure>
									<style>
					        	.dropify-wrapper{ display:none; height: 200px !important; }
					        </style> 
								</div>

								<div class="imagemain3" style="display: none;">
										<figure class="setimg-box33">
										
											<img id="image3" src="" class="img-responsive telset33">
											
										
										<a class="setimg-blog33 link1 font18 bold" href="#"  onclick="$('.dropify').click();" class="link1 font18 bold" for="profile">
											<span class="setimgblogcaps">
												<img src="<?php echo base_url() ?>theme/customer-assets/images/icon-camera.png" class="img-responsive"><br>
												<span class="setavatar33">Change <br>Avatar</span>
											</span>
										</a>
									</figure>
									<style>
					        	.dropify-wrapper{ display:none; height: 200px !important; }
					        </style> 
								</div>
							</div>
						</div>
						<p class="space-a"></p>
						<div class="form-group setinput">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" name="first_name" class="form-control input-c" value="<?php if(isset($data[0]['first_name'])){ echo $data[0]['first_name']; } ?>" placeholder="First Name"/>
									</div>
								</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" name="last_name" class="form-control input-c" value="<?php if(isset($data[0]['last_name'])){ echo $data[0]['last_name']; } ?>" placeholder="Last Name"/>
								</div>
							</div>
						</div>
						</div>
						
						<div class="form-group setinput">
							<input type="text" class="form-control input-b" value="<?php if(isset($data[0]['notification_email'])){ echo $data[0]['notification_email']; } ?>" name="notification_email" placeholder="Notification Email"/>
							<p class="seter"><span>Administrative Email</span></p>
						</div>
						<p class="space-b"></p>
						<p class="chngpasstext">
							<a href="" class="change_pass" name="savebtn" value="Change Password" data-toggle="modal" data-target="#myModal"  ><img src="<?php echo base_url() ?>theme/customer-assets/images/change-password.png" class="img-responsive"> <span>Change Password</span></a></p>
							<!-- name="savebtn"  -->
					</div>	
				</div>
				
				<div class="col-md-4">
					<div class="project-row-qq1 settingedit-box">
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-b">About Info</h3>
							</div>
							<div class="settcol right">
								<p class="btn-x">
									<!-- <a href="<?php echo base_url() ?>customer/request/setting-view" class="btn-e save_info" name="savebtn" value="Save Info">Save</a> -->
									<input type="submit" id="savebtn" name="savebtn" class="btn-e save_info" value="Save"/>
								</p>
							</div>
						</div>
						<p class="space-d"></p>
						
							
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control input-c" value="<?php if(isset($data[0]['company_name'])){ echo $data[0]['company_name']; } ?>" placeholder="Company Name" name="company_name">
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" value="<?php if(isset($data[0]['title'])){ echo $data[0]['title']; } ?>"placeholder="Title" name="title">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" value="<?php if(isset($data[0]['phone'])){ echo $data[0]['phone']; } ?>" name="phone" placeholder="Phone">
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<textarea class="form-control textarea-b" name="address_line_1" placeholder="<?php echo $data[0]['address_line_1']; ?>"><?php if(isset($data[0]['address_line_1'])){ echo $data[0]['address_line_1']; } ?></textarea>
								</div>
							</div>
							
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control input-c" value="<?php if(isset($data[0]['address_line_1'])){ echo $data[0]['city']; } ?>" name="city" placeholder="City">
								</div>
							</div>
							
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control input-c" value="<?php if(isset($data[0]['address_line_1'])){ echo $data[0]['state']; } ?>" name="state" placeholder="State">
								</div>
							</div>
							
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control input-c" value="<?php if(isset($data[0]['address_line_1'])){ echo $data[0]['zip']; } ?>" name="zip" placeholder="Zip">
								</div>
							</div>
							
						</div>
						
						
					</div>	
				</div>
				
				<div class="col-md-4">
					<div class="project-row-qq1 settingedit-box">
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-b">Billing and Subscription</h3>
							</div>
						</div>
						<p class="space-b"></p>
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-g">Current Plan</h3>
							</div>
							<div class="settcol right">
								<p class="setrightstst">Cycle: May 01,2018-June 01, 2018</p> 
							</div>
						</div>
						<p class="space-a"></p>
						
						<div class="cyclebox25">
							<div class="settrow">
								<div class="settcol left">
									<h3 class="sub-head-g">Monthly Golden Plan</h3>
									<p class="setpricext25">$259</p>
								</div>
								<div class="settcol right">
									<p class="btn-x"><a href="<?php echo base_url(); ?>customer/ChangeProfile/billing_subscription" class="btn-f chossbpanss">Change Plan</a></p>
								</div>
							</div>
						</div>
						<p class="space-d"></p>
						
						<div class="settrow">
							<div class="settcol left">
								<h3 class="sub-head-b">Billing Method</h3>
							</div>
							<div class="settcol right">
								<p class="btn-x"><a href="<?php echo base_url() ?>customer/setting-view" class="btn-e">Save</a></p>
							</div>
						</div>
						
						<p class="space-c"></p>
						
						<div class="form-group">
							<label class="label-x2">Card Number</label>
							<p class="space-a"></p>
							<div class="row">
								<div class="col-md-10">
									<input type="text" class="form-control input-c" placeholder="xxxx-xxxx-xxxx" > <!-- name="card_number" -->
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Expiry Date" > <!-- name="expiry_date" -->
								</div>
							</div>
							
							<div class="col-sm-4 col-sm-offset-1">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="CVC" > <!-- name="cvc" -->
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Name" > <!-- name="name_on_Card" -->
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control input-c" placeholder="Zip Code" > <!-- name="zip_code" -->
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</form>
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					
					<div class="cli-ent-model-box">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<div class="cli-ent-model">
							<header class="fo-rm-header">
								<h3 class="head-c">Change Password</h3>
							</header>
							<div class="fo-rm-body">
								<form method="post" action="<?php echo base_url(); ?>customer/ChangeProfile/change_password">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group goup-x1">
											<label class="label-x3">Current Password</label>
											<p class="space-a"></p>
											<input type="password" class="form-control input-c" name="old_password">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group goup-x1">
											<label class="label-x3">New Password</label>
											<p class="space-a"></p>
											<input type="password" class="form-control input-c"  name="new_password">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group goup-x1">
											<label class="label-x3">Confirm Password</label>
											<p class="space-a"></p>
											<input type="password" class="form-control input-c" name="confirm_password">
										</div>
									</div>
								</div>
								<p class="btn-x"><button class="btn-g" name="savebtn">Save</button></p>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</section>
    <!-- jQuery (necessary for JavaScript plugins) -->
    <script src="<?php echo base_url() ?>theme/customer-assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>theme/customer-assets/js/bootstrap.min.js"></script>
	<script>
		setTimeout(function(){
			$('#message').fadeOut();
		},2000);
		var $fileInput = $('.file-input');
		var $droparea = $('.file-drop-area');

		// highlight drag area
		$fileInput.on('dragenter focus click', function() {
		  $droparea.addClass('is-active');
		});

		// back to normal state
		$fileInput.on('dragleave blur drop', function() {
		  $droparea.removeClass('is-active');
		});

		$(document).ready(function() {
			$('#togal_form').click(function (){
				$("#password_form").toggle();
			});  
		});

		function validateAndUpload(input){
			//alert("In Function"); 
			var URL = window.URL || window.webkitURL;
			var file = input.files[0];
			
			if(file){
				console.log(URL.createObjectURL(file));
				// console.log(file);
				$(".imagemain").hide();

				$(".imagemain2").show();
				$(".imagemain3").show();
				$("#image3").attr('src',URL.createObjectURL(file));

				$(".dropify-wrapper").show();
			}
		}
	
		// change inner text
		$fileInput.on('change', function() {
		  var filesCount = $(this)[0].files.length;
		  var $textContainer = $(this).prev();

		  if (filesCount === 1) {
			// if single file is selected, show file name
			var fileName = $(this).val().split('\\').pop();
			$textContainer.text(fileName);
		  } else {
			// otherwise show number of files
			$textContainer.text(filesCount + ' files selected');
		  }
		});
	</script>
  </body>
</html>

 <!-- <?php echo "<pre>";print_r($data);echo "</pre>"; ?> -->