<?php //echo "<pre>";print_r($login_user_data ); ?>
<tr>
<?php if($login_user_data[0]["is_saas"] == 1 || ($login_user_data[0]["role"] == "manager" && $assign_designer_toproject == 1)){ ?>
    <td>
       <?php if($projects["is_transfer"] != 1){ ?>
        <div class="cli-ent-col td">
            <div class="cli-ent-xbox text-left">
                <div class="cell-row">
                    <div class="cell-col">
                        <div class="sound-signal">
                            <div class="form-radion2">
                                <label class="containerr">
                                    <input type="checkbox" name="project[]" class="selected_pro <?php echo $projects['addClass'];?> add_des_scrl"  value="<?php echo $projects['id']; ?>" data-id="<?php echo $projects['id']; ?>" id="<?php echo $projects['id']; ?>"/> 
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <?php } ?>
    </td>
<?php } ?>
    <td>
        <div class="img-status">
            <a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $projects['id']; ?>/1">
                <?php
                if (!empty($projects['latest_draft'])) {
                    $path = FS_PATH . FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS_SAAS . $projects['id'] . "/_thumb/" . $projects['latest_draft'];
                    if (file_get_contents($path)) {
                        ?>
                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS_SAAS . $projects['id'] . "/_thumb/" . $projects['latest_draft']; ?>" class="img-responsive" />
                    <?php } else { ?>
                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS_SAAS . $projects['id'] . "/" . $projects['latest_draft']; ?>" class="img-responsive" />
                        <?php
                    }
                } else {
                    ?>
                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS; ?>images/default.svg" class="img-responsive" />
                <?php } ?>

            </a>

        </div>
        <a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $projects['id']; ?>/1">
            <?php
            if (strlen($projects['title']) > 45) {
                $name = substr($projects['title'], 0, 45) . " ...";
            } else {
                $name = $projects['title'];
            }
            ?>
            <h3 title="<?php echo $projects['title']; ?>"><?php echo $name; ?></h3>
        </a>
        <div class="pro-inleftbox">

            <p class="pro-a inline-per">
                <a href="<?php echo base_url() ?>account/request/project-info/<?php echo $projects['id']; ?>/1" style="position: relative;">
                    <span class="inline-imgsssx">
                        <i class="icon-gz_message_icon"></i>
                        <?php if ($projects['total_chat'] + $projects['comment_count'] != 0) {
                            ?>
                            <!-- class="numcircle-box count_chatmsg" -->
                            <span>
                                <?php echo $projects['total_chat'] + $projects['comment_count']; ?>
                            </span>
                        <?php } ?>

                    </span>
                </a>
            </p>
        </div>
    </td>
    <td>

        <?php if (!empty($projects['client_name'])) { ?>
            <h2><span><?php echo $projects['client_name'][0]['first_name'] . ' ' . $projects['client_name'][0]['last_name']; ?></span></h2>
        <?php } else { 
            $s_name = $projects['c_name'][0]['first_name'] . ' ' . $projects['c_name'][0]['last_name']; ?>
            <span class="blank-td"><?php echo $s_name; ?></span>
        <?php } ?>
    </td>
    <?php if($login_user_data[0]["role"] != "designer"){ ?>
        <td>
                <!---Start Designer name---->
                <div class="cli-ent-col td">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <?php if ($projects['designer_id'] != "" && $projects['designer_id'] != 0) { ?>
                                <div class="cell-col <?php echo $projects['id']; ?>">
                                    <p class="text-h text-wrap">
                                        <?php echo ((strlen($projects['designer_first_name']) < 6) ? $projects['designer_first_name'] : substr($projects['designer_first_name'], 0, 6)); ?>
                                        <?php
                                        if (strlen($projects['designer_last_name']) > 5) {
                                            echo ucwords(substr($projects['designer_last_name'], 0, 1));
                                        } else {
                                            echo $projects['designer_last_name'];
                                        }
                                        ?>
                                    </p>
                                    <?php ?>
                                </div>
                            <?php } else { ?>
                                <div class="cell-col <?php echo $projects['id']; ?>"><p class="text-h text-wrap"></p></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!---End Designer name---->
        </td>
    <?php } ?>
    <td>
        <a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $projects['id']; ?>/1">

            <div class="pro-inrightbox">
                <p class="neft">
                <?php if ($projects['status'] == "active" || $projects['status'] == "checkforapprove" || $projects['status'] == "disapprove" || $projects['status'] == "hold") { ?>
                                           <!--<p class="green">Active </p>-->
                        <span class="status-btn <?php echo $projects['customer_variables']['color']; ?> ">
                            <?php echo $projects['customer_variables']['status']; ?>
                        </span>
                <?php } elseif ($projects['status'] == "assign") { ?>
                        <span class="status-btn <?php echo $projects['customer_variables']['color']; ?>">
                            <?php echo $projects['customer_variables']['status']; ?>
                        </span>
                <?php } elseif ($projects['status'] == "cancel") { ?>
                        <span class="status-btn <?php echo $projects['customer_variables']['color']; ?>">
                            <?php echo $projects['status']; ?>
                        </span>
                <?php } elseif ($projects['status'] == "draft") { ?>
                        <span class="status-btn <?php echo $projects['customer_variables']['color']; ?>">
                            <?php echo $projects['status']; ?>
                        </span>
                <?php } else { ?>
                        <span class="status-btn <?php echo $projects['customer_variables']['color']; ?>">
                            <?php echo "Completed"; ?>
                        </span>
                <?php } ?>
                    <?php if ($projects['status'] == 'assign') { ?>
                    <p class="select-pro-a pro-a" data-toggle="tooltip" title="Priority" >
                        <!--Priority -->
                        <?php
                        $priorities_array = $projects['max_priority'];
                        $subpriorities_array = $projects['subuser_max_priority'];
                        if ($projects['customer_id'] !== $login_user_id && $canaccessallbrands == 0 && $can_manage_priorities == 1 && $projects['created_by'] == $login_user_id && $login_user_data[0]['user_flag'] == 'client') {
                            ?>
                            <select class="prior" onchange="prioritize(this.value, <?php echo $projects['sub_user_priority'] ?>,<?php echo $projects['id'] ?>, 'client')">
                                <?php for ($j = 1; $j <= $subpriorities_array; $j++) { ?>
                                    <option value="<?php echo $j; ?>" <?php echo ($j == $projects['sub_user_priority']) ? "selected" : ""; ?>><?php echo $j; ?></option>                                             
                                <?php } ?>
                            </select><?php
                        } else if ($projects['customer_id'] !== $login_user_id && ($canaccessallbrands == 0 || $can_manage_priorities == 0)) {
                            echo "<span>".$projects['priority']."</span>";
                        } else {
                            if ($login_user_data[0]["is_saas"] != 1 && $login_user_data[0]["role"] == "customer") {
                                ?>
                                <select class="prior" onchange="prioritize(this.value, <?php echo $projects['priority'] ?>,<?php echo $projects['id'] ?>)">
                                    <?php for ($j = 1; $j <= $priorities_array; $j++) { ?>
                                        <option value="<?php echo $j; ?>" <?php echo ($j == $projects['priority']) ? "selected" : ""; ?>><?php echo $j; ?></option>                                              
                                    <?php } ?>
                                </select>
                            <?php
                            } else {
                                echo "<span>".$projects['priority']."</span>";
                            }
                        }
                        ?>
                    </p>
                <?php } ?>
                </p>
            </div>
        </a>
    </td>
    <td>
        <p class="pro-a">
            <?php
           // echo "<pre>";print_r($projects["al_status"]["by"]);
            if ($projects['status'] == "active") {
                echo "Expected on";
            } elseif ($projects[$projects["al_status"]["by"]] == "checkforapprove") {
                echo "Delivered on";
            } elseif ($projects[$projects["al_status"]["by"]] == "disapprove") {
                echo "Delivered on";
            } elseif ($projects[$projects["al_status"]["by"]] == "pendingrevision") {
                echo "Delivered on";
            } elseif ($projects[$projects["al_status"]["by"]] == "assign") {
                echo "Created on";
            } elseif ($projects[$projects["al_status"]["by"]] == "draft") {
                echo "Created on";
            } elseif ($projects[$projects["al_status"]["by"]] == "approved") {
                echo "Approved on";
            } elseif ($projects[$projects["al_status"]["by"]] == "cancel") {
                echo "Cancelled on";
            } elseif ($projects[$projects["al_status"]["by"]] == "hold") {
                echo "Hold on";
            }
            ?>
            <span  class="date_bold">
                <?php
                if($login_user_data[0]["role"] == "customer" && $login_user_data[0]["is_saas"] != 1){
                    $format = 'm/d/Y';
                }else{
                    $format = 'M d, Y h:i:A';
                }
                if ($projects[$projects["al_status"]["by"]] == "active") {
                    echo date($format, strtotime($projects['expected']));
                } elseif ($projects[$projects["al_status"]["by"]] == "checkforapprove") {
                    echo date($format, strtotime($projects['deliverydate']));
                } elseif ($projects[$projects["al_status"]["by"]] == "disapprove") {
                    echo date($format, strtotime($projects['expected']));
                }elseif ($projects[$projects["al_status"]["by"]] == "pendingrevision") {
                    echo date($format, strtotime($projects['revisiondate']));
                } elseif ($projects[$projects["al_status"]["by"]] == "assign") {
                    echo date($format, strtotime($projects['created']));
                } elseif ($projects[$projects["al_status"]["by"]] == "draft") {
                    echo date($format, strtotime($projects['created']));
                } elseif ($projects[$projects["al_status"]["by"]] == "approved") {
                    echo date($format, strtotime($projects['approvddate']));
                } elseif ($projects[$projects["al_status"]["by"]] == "cancel" || $projects['status'] == "hold") {
                    echo date($format, strtotime($projects['modified']));
                }
                ?>
            </span>
        </p>
    </td>
<?php if($login_user_data[0]["role"] != "customer" || $login_user_data[0]["is_saas"] == 1){ ?>
    <!----verified tab start-->
<td>
    <?php
//    echo $projects['verified'];
    if($projects["al_status"]["user"] == "designer"){
        $value = 2;
    }elseif ($projects["al_status"]["user"] == "s_user") {
            $value = 1;
    }
    if ($projects['verified'] == 1) {
        $class = 'verified_by_admin';
        $icon = '<i class="far fa-check-circle"></i> <span class="vrfy_txt"></span>Verified';
        $disable = '';
    } elseif ($projects['verified'] == 2) {
        $class = 'verified_by_designer';
        $icon = '<i class="fas fa-check-circle"></i> <span class="vrfy_txt"></span>Verified';
        $disable = 'disabled';
    } else {
        $class = '';
        $icon = '<i class="far fa-circle"></i> <span class="vrfy_txt">Verify</span>';
        $disable = '';
    }
    
    ?>
    <div class="cli-ent-col td" style="width: 9%;">
        <div class="cli-ent-xbox">
            <div class="cell-row">
                <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $projects['id']; ?>">
                    <label>
                        <input type="checkbox" name="project" class="verified" data-value="<?php echo $value; ?>" data-pid="<?php echo $projects['id']; ?>" <?php echo ($projects['verified'] == 1 || $projects['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable; ?>/>
                        <?php echo $icon; ?>
                    </label>
                </div>
            </div>
        </div>
    </div>
</td>
<!----verified tab end-->
<?php } ?>
<?php if($login_user_data[0]["role"] != "designer"){ ?>
    <td>
        <div class="action-list">
            <?php if ($projects['status'] != "approved" && $parent_user_data[0]['is_cancel_subscription'] != 1) { ?>
                <a data-toggle="tooltip" data-placement="bottom" title="Edit" href="<?php echo base_url(); ?>account/request/add_new_request?reqid=<?php echo $projects['id']; ?>">
                    <i class="icon-gz_edit_icon" aria-hidden="true"></i>
                </a>
            <?php } if ($canuserdel != 0 && $trial[0]['total_requests'] == 0) { ?>
                <a data-toggle="tooltip" data-placement="bottom" title="Delete" href="javascript:void(0)" data-url="<?php echo base_url(); ?>account/request/deleteproject/<?php echo $projects['id']; ?>" class="reddelete">
                    <i class="icon-gz_delete_icon " aria-hidden="true"></i></a>
                <?php
            }
            if ($canuseradd != 0) {
                ?>
                <a data-toggle="tooltip" data-placement="bottom" title="Copy" href="<?php echo base_url(); ?>account/request/add_new_request?did=<?php echo $projects['id']; ?>">
                <!-- <i class="far fa-copy"></i>-->
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 500" enable-background="new 0 0 500 500" xml:space="preserve">
                    <g id="Text-files">
                    <path d="M421.7,71.4h-31c-0.6,0-1.2,0.2-1.8,0.4V39.2c0-21.6-18-39.2-40.1-39.2H79.8C57.7,0,39.7,17.6,39.7,39.2v366
                          c0,21.6,18,39.2,40.1,39.2H127v17.7c0,20.9,17.3,37.8,38.6,37.8h256.1c21.3,0,38.6-17,38.6-37.8V109.3
                          C460.3,88.4,443,71.4,421.7,71.4z M55.6,405.2v-366c0-12.9,10.9-23.4,24.3-23.4h268.9c13.4,0,24.3,10.5,24.3,23.4v366
                          c0,12.9-10.9,23.4-24.3,23.4H79.8C66.4,428.6,55.6,418.1,55.6,405.2z M444.4,462.2c0,12.1-10.2,22-22.7,22H165.6
                          c-12.5,0-22.7-9.9-22.7-22v-17.7h205.9c22.1,0,40.1-17.6,40.1-39.2V86.9c0.6,0.1,1.2,0.4,1.8,0.4h31c12.5,0,22.7,9.9,22.7,22V462.2
                          z"></path>
                    </g>
                    </svg>
                </a> 
                <?php
            }
            if ($projects['status'] != 'cancel' && $projects['dummy_request'] == 0 && $change_project_status != 0) {
                ?>
                <a data-toggle="tooltip" data-placement="bottom" title="Cancel" href="<?php echo base_url(); ?>account/request/cancelproject/<?php echo $projects['id']; ?>">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         width="612px" height="612px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                    <g>
                    <g id="cross">
                    <g>
                    <polygon points="612,36.004 576.521,0.603 306,270.608 35.478,0.603 0,36.004 270.522,306.011 0,575.997 35.478,611.397 
                             306,341.411 576.521,611.397 612,575.997 341.459,306.011"/>
                    </g>
                    </g>
                    </g>
                    </svg>
                </a>
                <?php
            }
            if ($parent_user_data[0]['is_cancel_subscription'] != 1 && $projects['status'] == "cancel" && $change_project_status != 0) {
                ?> 
                <a class="cancel_subs" href="<?php echo base_url() ?>account/request/markAsInProgress/<?php echo $projects['id']; ?>/yes"><i class="fas fa-arrow-left"></i>Move to progress</a>
            <?php } ?>
        </div>
    </td>
<?php }?>
</tr>



