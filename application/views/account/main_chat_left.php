<?php 
if (isset($chatdata['fname']) && $chatdata['fname'] != '') {
    $username = $chatdata['fname'];
}elseif(isset($chatdata['sender_data'][0]['first_name']) && $chatdata['sender_data'][0]['first_name'] != ''){
     $username = $chatdata['sender_data'][0]['first_name'];
} else {
    $username = $chatdata['shared_name'];
}
if(strpos($chatdata['profile_picture'], '/public/uploads/profile_picture/') !== false){
    $chatdata['dp'] = $chatdata['profile_picture'];
} else{
    $chatdata['dp'] = '';
}
$docArrOffice = array('doc', 'docx', 'odt', 'ods', 'xlsx', 'xls', 'txt', 'ai', 'pdf', 'ppt', 'pptx', 'pps', 'ppsx', 'tiff', 'xxx', 'eps');
$font_file = array('ttf', 'otf', 'woff', 'eot', 'svg');
$vedioArr = array('mp4', 'Mov');
$audioArr = array('mp3');
$ImageArr = array('jpg', 'png', 'gif', 'jpeg', 'bmp');
$zipArr = array('zip', 'rar');
?>
<div class="read-msg-box clearfix">
    <div class="msgk-chatrow clearfix">
        <div class="msgk-user-chat msgk-left <?php echo $chatdata['sender_type']; ?>">
            <!-- If Current Message and Previous Message are not Same Sender Type-->
                <?php 
                if($chatdata['dp']){ ?>
                    <figure class="msgk-uimg">
                        <a class="msgk-uleft" href="javascript:void(0)" title="<?php echo isset($chatdata['fname']) ? $chatdata['fname'] : $chatdata['shared_name']; ?>">
                            <img src="<?php echo $chatdata['dp']; ?>" class="img-responsive">
                        </a>
                    </figure>
                    <?php } ?>
                    <p class="msgk-udate right">
                        <?php echo $username . ' ' . $chatdata['chat_created_time']; ?>
                    </p>
                    <div class="msgk-mn">
                        <div class="msgk-umsgbox">
                            <?php if ($chatdata['is_filetype'] != '1') { ?>
                            <pre>
                                <span class="msgk-umsxxt"><?php echo $chatdata['msg']; ?></span>
                            </pre>
                            <?php } 
                        else {
                        $type = substr($chatdata['msg'], strrpos($chatdata['msg'], '.') + 1);
                        $updateurl = '';
                        if (in_array(strtolower($type), $docArrOffice)) {
                            $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                            $class = "doc-type-file";
                            $exthtml = '<span class="file-ext">' . $type . '</span>';
                        } elseif (in_array(strtolower($type), $font_file)) {
                            $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-file.svg';
                            $class = "doc-type-file";
                            $exthtml = '<span class="file-ext">' . $type . '</span>';
                        } elseif (in_array(strtolower($type), $zipArr)) {
                            
                            $imgVar = FS_PATH_PUBLIC_ASSETS . 'img/default-img/chat-zip.svg';
                            $class = "doc-type-zip";
                            $exthtml = '<span class="file-ext">' . $type . '</span>';
                        } elseif (in_array(strtolower($type), $ImageArr)) {
                            $imgVar = imageRequestchatUrl_SAAS(strtolower($type), $chatdata['req_id'], $chatdata['msg']);   
                          
                            
                            $updateurl = '/_thumb';
                            $class = "doc-type-image";
                            $exthtml = '';
                        }
                        $basename = $chatdata['msg'];
                        $basename = strlen($basename) > 20 ? substr($basename, 0, 20) . "..." . $type : $basename;
                        $data_src = FS_PATH_PUBLIC_UPLOADS_SAAS.'requestmainchat/'.$chatdata['req_id'].'/'.$chatdata['msg'];
                        ?>
                    <span class="msgk-umsxxt">
                    
                    <div class="contain-info <?php echo (!in_array($type,$ImageArr)) ? 'only_file_class' :''; ?>">
                        <a class="open-file-chat <?php echo $class; ?>" data-ext="<?php echo $type; ?>" data-src="<?php echo $data_src; ?>">
                            <img src="<?php echo $imgVar; ?>"> <?php echo $exthtml; ?>
                        </a>
                        <div class="download-file">
                            <div class="file-name"><h4><b><?php echo $basename; ?></b></h4>
                            </div>
                        <a href="<?php echo base_url() ?>customer/request/download_stuffFromChat?ext=<?php echo $type; ?>&file-upload=<?php echo $data_src; ?>" target="_blank"> 
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/dwnlod-img.svg" />
                        </a>
                    </div>
                    </div>
                    
                    </span>
<?php } ?>
                        </div>
                    </div>
        </div>
    </div>
</div>
