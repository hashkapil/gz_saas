<?php // die("hello"); ?>
<section id="brand_profile" class="setting_agency">
    <div class="container">
        <div class="header-blog">
         <a href="<?php echo base_url(); ?>account/setting-view" class="back-link-xx0 text-uppercase">
             <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/back-link-xx0.png" class="img-responsive">
         </a>
         <?php
         if ($this->session->flashdata('message_error') != '') {
             ?>
             <div id="message" class="alert alert-danger alert-dismissable">
                 <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                 <p class="">
                    <?php echo $this->session->flashdata('message_error'); ?>
                </p>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('message_success') != '') { ?>
            <div id="message" class="alert alert-success alert-dismissable success_msg">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
             <p class="">
                <?php echo $this->session->flashdata('message_success'); ?>
            </p>
        </div>
    <?php }
    ?>
</div> 
<div class="row mobile_menu_mngmnt">
    <div class="col-md-12">
        <div class="account-tab">
            <div class="tab-toggle togglemenuforres"><p><span></span><span></span><span></span></p></div>
            <ul class="list-header-blog">
                <li class="active"><a href="#general" class="stayhere" data-toggle="tab">Branding</a></li>
                <li><a href="#button_theme" class="stayhere" data-toggle="tab">Color Preferences</a></li>
                <li><a href="#email_settings" class="stayhere" data-toggle="tab">Email Settings</a></li>
                <li><a href="#Admin_Email" class="stayhere" data-toggle="tab">Email Templates</a></li>
                <li><a href="#personal" class="stayhere" data-toggle="tab">Contact info</a></li>
                <li><a href="#live_chat" class="f_live_chat stayhere" data-toggle="tab">Additional Setting</a></li>
                <li class="for_agency_user stayhere dsf"><a href="<?php echo base_url(); ?>account/client_management" class="stayhere"><i class="fas fa-cog"></i> Client Management</a></li>
                <li class="for_agency_user stayhere"><a href="<?php echo base_url(); ?>account/designer_management" class="stayhere"><i class="fas fa-cog"></i> Designer Management</a></li>&nbsp
            </ul>
        </div>
    </div>
</div>

<div class="tab-content mb-t30">
    <div class="brand-outline white-boundries tab-pane active" id="general">
        <form enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>account/AgencyUserSetting/change_agency_user_general_setting" id="personal_form_data">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="main-info-heading">Update Branding Setting</h2>
                    <p class="fill-sub">Please fill below information to change your setting.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 display-info">
                    <div class="change_logo dp">
                        <?php if ($changeinfo[0]['logo']): ?>
                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_USER_LOGO . $changeinfo[0]['user_id'] . '/' . $changeinfo[0]['logo']; ?>" class="img-responsive telset33">
                            <?php else: ?>
                                <div class="" >
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/logo.png" class="img-responsive telset33">
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="imagemain2">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Change Logo</p>
                                        <input type="file" onChange="validateAndUpload(this);" data-file="logo" class="input" name="profile" data-plugin="dropify" id="inFile"/>
                                        <span id="changelogovalue">Choose File</span>
                                        <label for="inFile" class="infile-btn"><i class="fas fa-upload"></i></label>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                    </div> 
                                </div>
                                <div class="col-md-6">
                                    <div class="imagemain2">
                                        <label class="form-group">
                                            <p class="label-txt label-active">Change Favicon (16*16 Or 32*32)</p>
                                            <input type="file" onChange="validateAndUpload(this);" class="input" data-file="favicon" name="favicon" data-plugin="dropify" id="inFilefav"/>
                                            <span id="changefaviconvalue">Choose File</span>
                                            <label for="inFilefav" class="infile-btn">
                                                <?php if ($changeinfo[0]['favicon']): ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_USER_LOGO . $changeinfo[0]['user_id'] . '/favicon/' . $changeinfo[0]['favicon']; ?>" class="img-responsive telset33">
                                                <?php else: ?>
                                                    <div class="" >
                                                        <img src="<?php echo base_url(); ?>favicon.ico" class="img-responsive telset33">
                                                    </div>
                                                <?php endif; ?>
                                            </label>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="domain_subdomain">
                                        <div class="form-group-b">
                                            <p class="toggle-text">Use own domain: <span class="domain_info" data-toggle="modal" data-target="#setmaindomain" id="domain_info_pop"><i class="fas fa-info-circle"></i></span>
                                            </p>
                                            <label class="form-group switch-custom-usersetting-check">
                                                <input type="checkbox" name="slct_domain_subdomain" id="slct_domain_subdomain" class="slct_domain_subdomain" <?php echo (isset($changeinfo[0]['is_main_domain']) && $changeinfo[0]['is_main_domain'] == "1") ? "checked" : "" ?>/>
                                                <label for="slct_domain_subdomain"></label> 
                                            </label>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group"> 
                                        <div class="alternate-domain">
                                            <?php 
                                            if($changeinfo[0]['domain_name'] != ""){
                                            if($changeinfo[0]['ssl_or_not'] == "0"){
                                                $url = "http://".$changeinfo[0]['domain_name'];
                                            }else{
                                                $url = "https://".$changeinfo[0]['domain_name'];
                                            }
                                                $info = '<a href="'.$url.'" target="_blank" class="redrct_lnk" data-toggle="tooltip" title="'.$url.'"><i class="fas fa-external-link-alt"></i></a>';
                                            }else{
                                               $info = "";
                                            }
                                            ?>
                                            
                                            <div class="subdomain_nm" style="display:none"> 
                                                <p class="domain_https">https://</p>
                                                <label class="form-group">
                                                    <div class="label-txt label-active clint_cll">Sub Domain Name: <?php echo $info; ?></div>
                                                    <input type="text" class="input" name="sub_domain_name" id="sub_domain_name" value="<?php echo (isset($changeinfo[0]['domain_name']) && $changeinfo[0]['is_main_domain'] == "0") ? strstr($changeinfo[0]['domain_name'], '.', true) : '' ?>">
                                                    <p class="domain_ex">.graphicszoo.com</p> 
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                                <div class="validation"></div>
                                            </div>
                                            <div class="domain_nm" style="display:none"> 
                                                <p class="domain_https"><?php echo (isset($changeinfo[0]['ssl_or_not']) && $changeinfo[0]['ssl_or_not'] == 1) ? "https://" : "http://" ?></p>
                                                <label class="form-group">
                                                    <div class="label-txt label-active clint_cll">Domain Name: <?php echo $info; ?></div>
                                                    <input type="text" class="input" name="domain_name" id="domain_name" value="<?php echo (isset($changeinfo[0]['domain_name']) && $changeinfo[0]['is_main_domain'] == "1") ? $changeinfo[0]['domain_name'] : '' ?>">
                                                    <div class="line-box">
                                                        <div class="line"></div>
                                                    </div>
                                                </label>
                                                <div class="validation"></div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12"> <h3 class="title">Body Color</h3></div>
                                <div class="col-lg-6 col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Primary Color</p>
                                        <input type="text" id="primary_color" class="input sel_colorpicker" name="primary_color" value="<?php echo isset($changeinfo[0]['primary_color']) ? $changeinfo[0]['primary_color'] : '#e42547'; ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                    <label class="errspan" for="primary_color"><span class="bgcolor_primary" style="background-color:<?php echo $themePrimaryColor; ?>;"></span></label>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Secondary Color</p>
                                        <input type="text" id="secondary_color" class="input sel_colorpicker" name="secondary_color"  value="<?php echo isset($changeinfo[0]['secondary_color']) ? $changeinfo[0]['secondary_color'] : '#1a3148' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                    <label class="errspan" for="secondary_color"><span class="bgcolor_secondary" style="background-color:<?php echo $secondary_color; ?>;"></span></label>
                                </div>
                            </div>
                            <div class="row>">
                                <div class="ad_new_reqz text-left">
                                    <input type="submit" id="savebtn1" name="savebtn" class="btn-e save_info btn-red submit cnsl-sbt" id="save_option" value="Save">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="brand-outline white-boundries tab-pane" id="button_theme"> 
                <form  method="post" action="<?php echo base_url(); ?>account/AgencyUserSetting/change_agency_user_button_setting" id="button_theme_form_data">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="main-info-heading">Status Button Color</h2>
                            <p class="fill-sub">Please fill below information to change your setting.</p>
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="neft">
                                        <span class="agency_inprogress">In Progress</span>
                                    </p>
                                </div>
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Inprogress Status Button Font Color</p>
                                        <input type="text" id="inprog_font_color" class="form-control sel_colorpicker input" name="inprog_font_color" value="<?php echo (isset($changeinfo[0]['inprog_font_color']) && $changeinfo[0]['inprog_font_color'] != '') ? $changeinfo[0]['inprog_font_color'] : '#ffffff' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Inprogress Status Button Background Color</p>
                                        <input type="text" id="inprog_bkg_color" class="input sel_colorpicker" name="inprog_bkg_color" value="<?php echo (isset($changeinfo[0]['inprog_bkg_color']) && $changeinfo[0]['inprog_bkg_color'] != "") ? $changeinfo[0]['inprog_bkg_color'] : '#37c473' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="neft">
                                        <span class="agency_revision">Revision</span>
                                    </p>
                                </div>
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Revision Status Button Font Color</p>
                                        <input type="text" id="revsion_font_color" class="input sel_colorpicker" name="revsion_font_color" value="<?php echo (isset($changeinfo[0]['revsion_font_color']) && $changeinfo[0]['revsion_font_color'] != '') ? $changeinfo[0]['revsion_font_color'] : '#ffffff' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Revision Status Button Background Color</p>
                                        <input type="text" id="revsion_bkg_color" class="input sel_colorpicker" name="revsion_bkg_color" value="<?php echo (isset($changeinfo[0]['revsion_bkg_color']) && $changeinfo[0]['revsion_bkg_color'] != '') ? $changeinfo[0]['revsion_bkg_color'] : '#FF8C00' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="neft">
                                        <span class="agency_review">Review</span>
                                    </p>
                                </div>
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Review Design Status Button Font Color</p>
                                        <input type="text" id="review_font_color" class="input sel_colorpicker input" name="review_font_color" value="<?php echo (isset($changeinfo[0]['review_font_color']) && $changeinfo[0]['review_font_color'] != '') ? $changeinfo[0]['review_font_color'] : '#ffffff' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Review Design Status Button Background Color</p>
                                        <input type="text" id="review_bkg_color" class="input sel_colorpicker" name="review_bkg_color" value="<?php echo (isset($changeinfo[0]['review_bkg_color']) && $changeinfo[0]['review_bkg_color'] != '') ? $changeinfo[0]['review_bkg_color'] : '#000080' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="neft">
                                        <span class="inqueue_status_button">In Queue</span>
                                    </p>
                                </div>
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">In Queue Status Button Font Color</p>
                                        <input type="text" class="input sel_colorpicker" id="inqueue_font_color" name="inqueue_font_color" value="<?php echo (isset($changeinfo[0]['inqueue_font_color']) && $changeinfo[0]['inqueue_font_color'] != '') ? $changeinfo[0]['inqueue_font_color'] : '#ffffff' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div> 
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">In Queue Status Button Background Color</p>
                                        <input type="text" id="inqueue_bkg_color" class="input sel_colorpicker" name="inqueue_bkg_color" value="<?php echo (isset($changeinfo[0]['inqueue_bkg_color']) && $changeinfo[0]['inqueue_bkg_color'] != '') ? $changeinfo[0]['inqueue_bkg_color'] : '#f3c500' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="neft">
                                        <span class="hold_status_button">Hold</span>
                                    </p>
                                </div>
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Hold Status Button Font Color</p>
                                        <input type="text" class="input sel_colorpicker" id="hold_font_color" name="hold_font_color" value="<?php echo (isset($changeinfo[0]['hold_font_color']) && $changeinfo[0]['hold_font_color'] != '') ? $changeinfo[0]['hold_font_color'] : '#ffffff' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div> 
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Hold Status Button Background Color</p>
                                        <input type="text" id="hold_bkg_color" class="input sel_colorpicker" name="hold_bkg_color" value="<?php echo (isset($changeinfo[0]['hold_bkg_color']) && $changeinfo[0]['hold_bkg_color'] != '') ? $changeinfo[0]['hold_bkg_color'] : '#ec2929' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="neft">
                                        <span class="draft_status_button">Draft</span>
                                    </p>
                                </div>
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Draft Status Button Font Color</p>
                                        <input type="text" class="input sel_colorpicker" id="draft_font_color" name="draft_font_color" value="<?php echo (isset($changeinfo[0]['draft_font_color']) && $changeinfo[0]['draft_font_color'] != '') ? $changeinfo[0]['draft_font_color'] : '#ffffff' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div> 
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Draft Status Button Background Color</p>
                                        <input type="text" id="draft_bkg_color" class="input sel_colorpicker" name="draft_bkg_color" value="<?php echo (isset($changeinfo[0]['draft_bkg_color']) && $changeinfo[0]['draft_bkg_color'] != '') ? $changeinfo[0]['draft_bkg_color'] : '#333332' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="neft">
                                        <span class="cancel_status_button">Cancel</span>
                                    </p>
                                </div>
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Cancel Status Button Font Color</p>
                                        <input type="text" class="input sel_colorpicker" id="cancel_font_color" name="cancel_font_color" value="<?php echo (isset($changeinfo[0]['cancel_font_color']) && $changeinfo[0]['cancel_font_color'] != '') ? $changeinfo[0]['cancel_font_color'] : '#ffffff' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div> 
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Cancel Status Button Background Color</p>
                                        <input type="text" id="cancel_bkg_color" class="input sel_colorpicker" name="cancel_bkg_color" value="<?php echo (isset($changeinfo[0]['cancel_bkg_color']) && $changeinfo[0]['cancel_bkg_color'] != '') ? $changeinfo[0]['cancel_bkg_color'] : '#c0304a' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <p class="neft">
                                        <span class="completed_status_button agency_completed">Completed </span>
                                    </p>
                                </div>
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Completed Status Button Font Color</p>
                                        <input type="text" class="input sel_colorpicker" id="complete_font_color" name="complete_font_color" value="<?php echo (isset($changeinfo[0]['complete_font_color']) && $changeinfo[0]['complete_font_color'] != '') ? $changeinfo[0]['complete_font_color'] : '#ffffff' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div> 
                                <div class="col-md-5">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Completed Status Button Background Color</p>
                                        <input type="text" id="complete_bkg_color" class="input sel_colorpicker" name="complete_bkg_color" value="<?php echo (isset($changeinfo[0]['complete_bkg_color']) && $changeinfo[0]['complete_bkg_color']!= '') ? $changeinfo[0]['complete_bkg_color'] : '#37c473' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div> 
                            </div>
                        </div>
                        <div class="col-lg-12 ad_new_reqz text-center">
                            <input type="submit" id="savebtn2" name="savebtn" class="btn-e save_info btn-red submit cnsl-sbt" id="save_option" value="Save">
                        </div>
                    </div>
                </form>
				<hr/>
                <form  method="post" action="<?php echo base_url(); ?>account/AgencyUserSetting/change_agency_user_notification_setting" id="notification_theme_form_data">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="main-info-heading">Notification Message Color</h2>
                            <p class="fill-sub">Please fill below information to change your setting.</p>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="message_sucs" class="msg_scss alert-success">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                        <p class="">This is success message.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Success Message Color</p>
                                        <input type="text" id="success_msg_color" class="input sel_colorpicker" name="success_msg_color" value="<?php echo (isset($changeinfo[0]['success_msg_color']) && $changeinfo[0]['success_msg_color'] != '') ? $changeinfo[0]['success_msg_color'] : '#3c763d' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>                            
                                    <label class="errspan" for="success_msg_color"><span class="success_msg" style="background-color:<?php echo $success_msg_color; ?>;"></span></label>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Success Message Background Color</p>
                                        <input type="text" id="success_msg_bg_color" class="input sel_colorpicker" name="success_msg_bg_color" value="<?php echo (isset($changeinfo[0]['success_msg_bg_color']) && $changeinfo[0]['success_msg_bg_color'] != '') ? $changeinfo[0]['success_msg_bg_color'] : '#dff0d8' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>                            
                                    <label class="errspan" for="success_msg_bg_color"><span class="success_bg_msg" style="background-color:<?php echo $success_msg_bg_color; ?>;"></span>
                                    </label>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="message_err" class="msg_err alert-danger">
                                        <a href="#" class="close" data-dismiss="" aria-label="close">×</a>
                                        <p class="">This is error message.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Error Message Color</p>
                                        <input type="text" id="error_msg_color" class="input sel_colorpicker" name="error_msg_color" value="<?php echo (isset($changeinfo[0]['error_msg_color']) && $changeinfo[0]['error_msg_color'] != '') ? $changeinfo[0]['error_msg_color'] : '#a94442' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>                           
                                    <label class="errspan" for="error_msg_color"><span class="error_msges" style="background-color:<?php echo $error_msg_color; ?>;"></span></label>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <label class="form-group">
                                        <p class="label-txt label-active">Error Message Background Color</p>
                                        <input type="text" id="error_msg_bg_color" class="input sel_colorpicker" name="error_msg_bg_color" value="<?php echo (isset($changeinfo[0]['error_msg_bg_color']) && $changeinfo[0]['error_msg_bg_color']!='') ? $changeinfo[0]['error_msg_bg_color'] : '#f2dede' ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>                           
                                    <label class="errspan" for="error_msg_bg_color"><span class="error_bg_msg" style="background-color:<?php echo $error_msg_bg_color; ?>;"></span></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 ad_new_reqz text-center">
                            <input type="submit" id="savebtn3" name="savebtn" class="btn-e save_info btn-red submit cnsl-sbt" id="save_option" value="Save">
                        </div>
                    </div>
                </form>

            </div>

            <div class="brand-outline white-boundries tab-pane" id="email_settings"> 
                <form  method="post" action="<?php echo base_url(); ?>account/AgencyUserSetting/change_agency_user_email_setting" id="agency_user_email_setting">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="main-info-heading">Email Settings</h2>
                            <p class="fill-sub">Please fill the complete form before sending test email,email settings you might need from your email provider</p><br/>
                            <button type="button" data-toggle="modal" data-target="#SendtestEmail" class="send_test_email_agency" aria-label="Close"><span aria-hidden="true">Test Email</span></button>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <label class="form-group">
                                        <div class="label-txt label-active">Reply Name<div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Your name as you would like in reply email.</div></div></div>
                                        <input type="text" id="reply_name" class="input" name="reply_name" value="<?php echo (isset($changeinfo[0]['reply_to_name']) && $changeinfo[0]['reply_to_name'] != '') ? $changeinfo[0]['reply_to_name'] : 'Graphicszoo'; ?>">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label> 
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <label class="form-group">
                                        <div class="label-txt label-active <?php //echo (isset($changeinfo[0]['reply_to_email']) && $changeinfo[0]['reply_to_email'] != "") ? "label-active" : ""; ?>">Reply Email <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Your email where user want to reply your message.</div></div></div>
                                        <input type="email" id="reply_email" class="input" name="reply_email" value="<?php echo (isset($changeinfo[0]['reply_to_email']) && $changeinfo[0]['reply_to_email'] != '') ? $changeinfo[0]['reply_to_email'] : $config_email['sender']; ?>" required>
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="smt-enable show-hidelabel">
                                <div class="switch-custom-usersetting-check is_smtp_enabele">
                                    <input type="checkbox" name="is_smtp_enable" id="is_smtp_enable">
                                    <label for="is_smtp_enable"></label> 
                                    <h3>Enter your SMTP details</h3>
                                    <div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Use your own details for email.</div></div>
                                </div>
                            </div>
                        </div>

                        <div class='port_settings' style='display:none'>
                            <div class="col-md-12">
                                <p  class="fill-sub">You can send your users email through your own SMTP service. For more details about custom SMTP <a class="theme-color" href="https://www.youtube.com/watch?v=5SbC2fCf5qo" target="_blank">check cpanel instruction's</a> OR <a class="theme-color" href="https://in.godaddy.com/help/configuring-mail-clients-with-cpanel-email-8861" target="_blank">godaddy instruction's</a>
								according your hosting. If you're using Gmail for your email services, enable the switch for less secure apps in <a href="https://myaccount.google.com/lesssecureapps" target="_blank">your account</a>.</p><!--<div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">For Gmail - host (smtp.gmail.com), port (465), email secure (SSL)</div></div>--><br>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <label class="form-group ">
                                            <div class="label-txt <?php echo (isset($changeinfo[0]['from_name']) && $changeinfo[0]['from_name']!='') ? "label-active" : ""; ?>">From Name<div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Choose your sender name as you would like it to appear in messages that you send. Example: John</div></div></div>
                                            <input type="text" id="from_name" class="input" name="from_name" value="<?php echo isset($changeinfo[0]['from_name']) ? $changeinfo[0]['from_name'] : '' ?>">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        <div class="error_msg"></div>
                                        </label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 from_email_text">
                                        <label class="form-group">
                                            <div class="label-txt <?php echo (isset($changeinfo[0]['from_email']) && $changeinfo[0]['from_email']!='') ? "label-active" : ""; ?>">From Email <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Your email address for this account, such as doe@example.com.</div></div></div>
                                            <input type="email" id="from_email" class="input" name="from_email" value="<?php echo isset($changeinfo[0]['from_email']) ? $changeinfo[0]['from_email'] : '' ?>">
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        <div class="error_msg"></div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 port_text">
                                        <label class="form-group">
                                            <div class="label-txt <?php echo (isset($changeinfo[0]['port']) && $changeinfo[0]['port']!='') ? "label-active" : ""; ?>">Port <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">The port number used by the incoming mail server,common port numbers 143,465.</div></div></div>
                                            <input type="text" id="port" class="input" name="port" value="<?php echo isset($changeinfo[0]['port']) ? $changeinfo[0]['port'] : '' ?>" >
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                            <div class="error_msg"></div>
                                        </label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 host_name">
                                        <label class="form-group">
                                            <div class="label-txt <?php echo (isset($changeinfo[0]['host']) && $changeinfo[0]['host']!='') ? "label-active" : ""; ?>">Host Name <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">The host name of the incoming mail server, such as mail.example.com.</div></div></div>
                                            <input type="text" id="host_name" class="input" name="host" value="<?php echo isset($changeinfo[0]['host']) ? $changeinfo[0]['host'] : '' ?>" >
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        <div class="error_msg"></div>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 host_username">
                                        <label class="form-group">
                                            <div class="label-txt <?php echo (isset($changeinfo[0]['host_username']) && $changeinfo[0]['host_username']!= '') ? "label-active" : ""; ?>">Email Address <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Please Enter your email address.</div></div></div>
                                            <input type="text" id="host_username" class="input" name="host_username"  autocomplete="off" value="<?php echo isset($changeinfo[0]['host_username']) ? $changeinfo[0]['host_username'] : '' ?>" >
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                            <div class="error_msg"></div>
                                        </label>
                                    </div>
                                    <div class="col-lg-6 col-md-6 host_password">
                                        <label class="form-group">
                                            <div class="label-txt">Email Password <span>*</span><div class="tool-hover"><i class="fas fa-info-circle"></i><div class="tool-text">Please Enter your email password.</div></div></div>
                                            <input type="password" id="host_password" class="input f_host_pswrd" name="host_password" autocomplete="off" value="" <?php echo (isset($changeinfo[0]['host_password']) && $changeinfo[0]['host_password'] != "")?"disabled":""; ?> data-is_password="<?php echo (isset($changeinfo[0]['host_password']) && $changeinfo[0]['host_password'] != "")?"yes":""; ?>">
                                            <?php if(isset($changeinfo[0]['host_password']) && $changeinfo[0]['host_password'] != ""){ ?>
                                            <div class="update-pwd"><input type="checkbox" name="update_pss" id="f_update_psswrd"><span></span><label for="f_update_psswrd">Update</label></div>
                                            <?php }?>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                            <?php //echo (isset($changeinfo[0]['host_password']) && $changeinfo[0]['host_password']!='') ? "<p class='input-information'>Enter only if you want to change password. </p>":""?>
                                        <div class="error_msg"></div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 email_secure">
                                        <label class="form-group">
                                            <div class="label-txt label-active">Email Secure <span>*</span>
                                                <div class="tool-hover"><i class="fas fa-info-circle"></i>
                                                    <div class="tool-text">Does the incoming mail server support SSL (Secure Sockets Layer) or TLS (Transport Layer Security) encryption? </div>
                                                </div>
                                            </div>
                                            <select class="input" name="email_secure" id="email_secure">
                                                <option value="ssl" <?php echo ($changeinfo[0]['mail_secure'] && $changeinfo[0]['mail_secure'] == "ssl") ? "selected" : '' ?>>SSL</option>
                                                <option value="tls" <?php echo ($changeinfo[0]['mail_secure'] && $changeinfo[0]['mail_secure'] == "tls") ? "selected" : '' ?>>TLS</option>
                                            </select>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                            <div class="error_msg"></div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 ad_new_reqz">
                            <input type="submit" id="email_setting" name="savebtn" class="btn-e save_info btn-red submit cnsl-sbt" id="save_option" value="Save">
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane content-datatable white-boundries datatable-width" id="Admin_Email" role="tabpanel">
                <div class="" id="email_setting_list">
                    <div class="headerWithBtn">
                        <h2 class="main-info-heading">Email Setting</h2>
                        <p class="fill-sub">Please update below information to update your Request Category</p>
                        <div class="addPlusbtn">
                            <a href="javascript:void(0)" class="edit_email_temp header_footer_btn txtbtn" data-id="<?php echo $email_header_footer_sec['header_part_id']; ?>"> Header</a><a href="javascript:void(0)" class="edit_email_temp header_footer_btn" data-id="<?php echo $email_header_footer_sec['footer_part_id']; ?>"> Footer</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="managment-list">
                                <?php for ($i = 0; $i < sizeof($emailslist); $i++) {
                                    ?>
                                    <ul>
                                        <li class="usericon">
                                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/msg-icon-bg.svg" class="img-responsive">
                                        </li>
                                        <li class="username">
                                            <div class="email_title">
                                                <label class="title"><?php echo $emailslist[$i]['email_name']; ?></label>
                                                <p class="brief_intro"> <?php echo $emailslist[$i]['email_description']; ?></p>
                                            </div>
                                        </li>
                                        <li class="three-icon">
                                            <div class="switch-custom-usersetting-check activate-emails">
                                                <input type="checkbox" name="enable_disable_setting_for_email" data-id="<?php echo $emailslist[$i]['id']; ?>" id="enable_disable_set_<?php echo $emailslist[$i]['id']; ?>" <?php if ($emailslist[$i]['disable_email_to_user'] == 0) { echo 'checked'; } ?>/>
                                                <label for="enable_disable_set_<?php echo $emailslist[$i]['id']; ?>"></label> 
                                            </div>
                                            <div class="c">
                                                <h3 class="pro-head space-b" style="cursor: pointer; text-align: right">
                                                    <a href="javascript:void(0)" class="edit_email_temp" data-id="<?php echo $emailslist[$i]['id']; ?>"> 
                                                        <span><i class="icon-gz_edit_icon"></i></span>
                                                    </a>
                                                </h3>
                                            </div>
                                            <div class="email_preview">
                                                <div data-toggle="modal" data-target="#emailPreview" class="emailPreview" data-id="<?php echo $emailslist[$i]['id']; ?>">
                                                    <div class="email_header_temp_cont" style="display:none"><?php echo $email_header_footer_sec['header_part']; ?></div>
                                                    <div class="email_body_cont" style="display:none"><?php echo $emailslist[$i]['email_text']; ?></div>
                                                    <div class="email_footer_temp_cont" style="display:none"><?php echo $email_header_footer_sec['footer_part']; ?></div>
                                                    <span><i class="fas fa-eye"></i></span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade new-test-email  edit_single_email_temp in" id="email_edit_temp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
                        <!-- <div class="edit_single_email_temp" style="display:none"> -->
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3>Edit Email Template</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="email_preview single_edit_action_btns">
                                        <div data-toggle="modal" data-target="#Preview" class="emailPreview" data-id="">
                                            <div class="inneremail_header_temp_cont" style="display:none"><?php echo $email_header_footer_sec['header_part']; ?></div>
                                            <div class="email_body_cont" style="display:none"></div>
                                            <div class="inneremail_footer_temp_cont" style="display:none"><?php echo $email_header_footer_sec['footer_part']; ?></div>
                                            <span><i class="fa fa-eye" aria-hidden="true"></i></span>  Preview
                                        </div>
                                        <button type="button" data-toggle="modal" data-target="#SendEmail" data-id="" class="send_test_email btn-red" aria-label="Close"><span aria-hidden="true">Test Email</span></button>
                                    </div>

                                    <form action="<?php echo base_url(); ?>account/Emailcontent/editemail" method="post" enctype="multipart/form-data">
                                        <div class="fo-rm-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <label class="form-group">
                                                        <p class="label-txt label-active">Title</p>
                                                        <input type="hidden" class="input input-d" name="id" id="email_id" value="">
                                                        <input type="hidden" class="input input-d" name="user_id" id="user_id" value="">
                                                        <input type="text" class="input input-d" name="email_name" id="email_name" value="">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>
                                                    <label class="form-group hide_subjecttoheaderfooter">
                                                        <p class="label-txt label-active">Subject</p>
                                                        <input type="text" class="input input-d" id="subject" name="subject" value="">
                                                        <div class="line-box">
                                                            <div class="line"></div>
                                                        </div>
                                                    </label>

                                                    <div class="form-group goup-x1">
                                                        <textarea id="txtEditor" name="description"></textarea>
                                                    </div>

                                                    <div class="form-group goup-x1">
                                                        <div class="email-footer-btn">
                                                            <button type="submit" name="submit" id="save"  class="save-publish btn-red">Publish</button>
                                                            <h3 class="space-b reset_email_template " style="cursor: pointer; text-align: center"></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="brand-outline white-boundries tab-pane" id="personal">
                    <form enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>account/AgencyUserSetting/change_agency_user_personal_setting" id="general_form_data">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="main-info-heading">Contact Info</h2>
                                <p class="fill-sub">Please update below information to update your contact info.</p>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($changeinfo[0]['physical_address']) && $changeinfo[0]['physical_address'] != "") ? "label-active" : ""; ?>">Physical Address <span>*</span></p>
                                            <input type="text" class="input" name="physical_address" value="<?php echo isset($changeinfo[0]['physical_address']) ? $changeinfo[0]['physical_address'] : '' ?>" required>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($changeinfo[0]['contact_number']) && $changeinfo[0]['contact_number'] != "") ? "label-active" : ""; ?>">Contact Number <span>*</span></p>
                                            <input type="text" class="input" name="contact_number" value="<?php echo isset($changeinfo[0]['contact_number']) ? $changeinfo[0]['contact_number'] : '' ?>" required>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($changeinfo[0]['email_address']) && $changeinfo[0]['email_address'] != "") ? "label-active" : ""; ?>">Email Address <span>*</span></p>
                                            <input type="email" class="input" name="email_address" value="<?php echo isset($changeinfo[0]['email_address']) ? $changeinfo[0]['email_address'] : '' ?>" required>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <label class="form-group">
                                            <p class="label-txt <?php echo (isset($changeinfo[0]['company_name']) && $changeinfo[0]['company_name'] != "") ? "label-active" : ""; ?>">Company Name <span>*</span></p>
                                            <input type="text" class="input" name="company_name" value="<?php echo isset($changeinfo[0]['company_name']) ? $changeinfo[0]['company_name'] : '' ?>" required>
                                            <div class="line-box">
                                                <div class="line"></div>
                                            </div>
                                        </label>
                                </div>
                                </div>
                            </div>
                            <div class="col-lg-12 ad_new_reqz">
                                <input type="submit" id="savebtn4" name="savebtn" class="btn-e save_info btn-red submit cnsl-sbt" id="save_option" value="Save">
                                <input type="button" id="savebtn5" name="savebtn" class="btn-red contact_preview submit" value="Preview">
                                <!--                    <a href="javascript:void(0)" class="new-subuser contact_preview">Preview</a>-->
                            </div>
                        </div>
                    </form>
                </div>

                <div class="brand-outline white-boundries tab-pane" id="live_chat"> 
                    <div class="inner-sub-section">
                        <?php $this->load->view('account/agency_user/agency_settings', array("changeinfo" => $changeinfo)); ?>
                    </div>
                </div>
        </div>
    </div>
</section>

<!-- Model for preview contact info  -->
<div class="modal fade similar-prop sharing-popup in" id="contact_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">Contact Us</h2>
                    <div class="cross_popup" data-dismiss="modal"> x</div>
                </header>
            <div class="modal-body cli-ent-model">
                <div class="congrts">
                    <h4><i class="fa fa-question-circle"></i></h4>
                </div>
                <h3 class="head-c text-center">If you have any query, Contact Us</h3>
                <div class="text-center">
                <?php if ($physical_address != '' || $physical_address != NULL) { ?>
                    <p><strong>Address : </strong> <?php echo $physical_address; ?></p>
                <?php }if ($contact_number != '' || $contact_number != NULL) { ?>
                    <p><strong>Contact : </strong> <?php echo $contact_number; ?></p>
                <?php } if ($email_address != '' || $email_address != NULL) { ?>
                    <p><strong>Email : </strong> <?php echo $email_address; ?></p>
                <?php } ?>
                </div>  
            </div>
        </div>
    </div>
</div>
    <!-- Model for email preview -->
    <div class="modal fade slide-3 similar-prop model-close-button in" id="emailPreview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
               <button type="button" class="close email-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
               <div class="email_preview_section">

               </div>
           </div>
       </div>
   </div>
   <!-- Model for edit preview -->
   <div class="modal fade slide-3 similar-prop model-close-button in" id="Preview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
           <button type="button" class="close email-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
           <div class="preview_section">
           </div>
           <p class="space-b"></p>
       </div>
   </div>
</div>
<!-- Model for identifiers -->
<div class="modal fade slide-3 similar-prop model-close-button in" id="identifierDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="identifier_detail">
             <button type="button" class="close identifier_close email-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
             <div class="email_header_temp_cont"><?php echo $email_header_footer_sec['header_part']; ?></div><br/><br/>
             <div class="email_body_cont identifier_description"></div>
         </div>
         <p class="space-b"></p>
     </div>
 </div>
</div>
<!-- Model for test email -->
<div class="modal fade similar-prop in" id="SendEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Test Email</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="testemail_section">
                <form method="POST" action="<?php echo base_url(); ?>account/Emailcontent/send_agency_testemail">
                    <div class="fo-rm-body">
                        <div class="row">
                            <div class="col-sm-12">
                                 <label class="form-group">
                                    <p class="label-txt label-active">Email Address</p>
                                   <input type="hidden" class="input input-d" id="template_id" name="template_id" value="">
                                    <input type="email" class="input input-d" id="user_email_adrs" name="user_email" value="" required>
                                    <div class="line-box">
                                      <div class="line"></div>
                                  </div>
                              </label>
                            </div>
                        </div>
                        <div class="form-group goup-x1">
                            <div class="cell-col">
                                <p class="btn-x bl-ogbtn text-right">
                                    <button type="submit" name="submit" id="send" class="btn-g text-uppercase">Send</button></p>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Model for Agency test email -->
    <div class="modal similar-prop fade new-test-email in" id="SendtestEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3>Test Email</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="testemail_section">

                    <form method="POST" action="<?php echo base_url(); ?>account/AgencyUserSetting/Send_testemail_toAgencyUser">
                        <div class="fo-rm-body">
                            <div class="row">
                                <div class="col-sm-12">
                                   <label class="form-group">
                                    <p class="label-txt <?php echo (isset($editvar['brand_name']) && $editvar['brand_name']!= '') ? "label-active" : ""; ?>">Email Address</p>
                                    <input type="hidden" class="input input-d" id="emailtemplate_id" name="template_id" value="">
                                    <input type="email" class="input input-d" id="testuser_email" name="user_email" value="" required>
                                    <div class="line-box">
                                      <div class="line"></div>
                                  </div>
                              </label>
                          </div>
                      </div>
                      <div class="form-group goup-x1">
                        <div class="cell-col">
                            <p class="btn-x bl-ogbtn text-right">
                                <button type="submit" name="submit" id="send2" class="btn-g text-uppercase">Send</button></p>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <p class="space-b"></p>
        </div>
    </div>
</div>

<!-- Model for show domain setting info to user -->
<div class="modal fade slide-3 model-close-button in similar-prop" id="setmaindomain" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
 <iframe id="myiFrame" data-src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/Instructions_for_domain_subdomain.pdf" src="about:blank"></iframe>
<div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="cli-ent-model-box">
            <header class="fo-rm-header">
               <h2 class="popup_h2 del-txt">Point your domain</h2>
              
               <div class="cross_popup" data-dismiss="modal">x</div>
            </header>
            <div class="cli-ent-model">
               <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/dns.png">
               <p><br/></p>
               <p>You can use your own domain or subdomain and the complete application with run there. For this, you will have to point your domain to <b>A records</b> to our server which is <b>3.17.33.76</b>.
                  For further instructions <a href="javascript:void(0)" target="_blank" class="domain_inst site_btn" id="domain_inst">click here</a> or you can contact us and we will be here to support you.</p>
                
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Model for show ssl msg -->
<div class="modal fade slide-3 model-close-button in similar-prop" id="sslmsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="cli-ent-model-box">
                <header class="fo-rm-header">
                    <h2 class="popup_h2 del-txt">SSL Certificate</h2>
                    <div class="cross_popup" data-dismiss="modal">x</div>
                </header>
                <div class="cli-ent-model">
                    <img class="delete_img1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/https.png">
                    
                    
                    <p>For activating HTTPS,
                    you must have a valid SSL certificate for the domain/subdomain. Contact us for any query related to this.</p>
                    

                </div>
            </div>

        </div>
    </div>
</div>
<!-- Model for live chat  -->
<div class="modal fade similar-prop sharing-popup in" id="livechatpopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="padding-left: 15px;">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <header class="fo-rm-header">
            <h2 class="popup_h2 del-txt">Live Chat</h2>
            <div class="cross_popup" data-dismiss="modal"> x</div>
         </header>
         <div class="modal-body cli-ent-model">
            <img src="<?php echo FS_PATH_PUBLIC_ASSETS.'img/livechat.png' ?>"/>
            
         </div>
      </div>
   </div>
</div>
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#setmaindomain" id="setmaindomain_url" style="display: none;">Open Modal</button>
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#sslmsg" id="sslmsg_sec" style="display: none;">Open Modal</button>
<link href="<?php echo FS_PATH_PUBLIC_ASSETS ?>css/bootstrap-colorpicker.min.css" rel="stylesheet">
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>plugins/editor/editor.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/bootstrap-colorpicker.js"></script>
<script type="text/javascript">
    var loggedinemail = '<?php echo isset($useroptions_data[0]['email_address']) ? $useroptions_data[0]['email_address'] : ''; ?>';
    var is_smtp_saved = "<?php echo (isset($changeinfo[0]['host_username']) && $changeinfo[0]['host_username'] != '') ? 1 : 0 ?>";
</script>

