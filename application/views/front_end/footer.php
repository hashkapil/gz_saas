<style>
.hova:hover{
	border-radius:3px !important;
}
h2, .alternating h3 {
    font-size: 35px;
    line-height: 38px;
    margin-top: unset;
    color: #fff;
    font-family: unset;
    font-weight: bold;
}	
</style>
<?php //echo "<pre/>";print_r($login_user_data);exit;?>
<section class="get-sale-section">
	<div class="container">
		<div class="col-md-12" style="text-align:center;">
			<h2>Try Graphics Zoo Risk-Free</h2>
			<div class="started-btn-section" >
				<a class="" href="<?php echo base_url(); ?>pricing"><button class="hova form-control btn custom-get-btn">Get Started</button></a>
			</div>	
		</div>
	</div>
    <div class="container" style="display:none;">
        <div class="row">
            <div class="col-sm-5 col-md-4">
                <div class="get-sale-content">
                    <h2>Get 10% off</h2>
                    <p>Sign up for our newsletter here</p>
                </div>
            </div>
            <div class="col-sm-7 col-md-8">
                <form class="form-inline row">
                    <div class="form-group col-xs-12 col-sm-8 col-md-8">
                        <input type="text" class="form-control" placeholder="Your Email Address">
                    </div>
                    <div class="form-group col-xs-12 col-sm-4 col-md-4">
                        <a href="#" class="btn btn2 btnfull">submit</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<footer class="footer-main">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-3">
                <div class="footer-logo">
                    <a href="<?php base_url(); ?>" rel="nofollow"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_images/logo.png" class="img-responsive img" alt="Footer Logo" style="margin-left: 30px;"></a>
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <div class="footer-links">
                           <ul>
                               <li><a rel="canonical" href="<?php echo base_url(); ?>portfolio">Portfolio</a></li>
                               <li><a rel="canonical" href="<?php echo base_url(); ?>pricing">Pricing</a></li>
                               <li><a rel="canonical" href="<?php echo base_url(); ?>faq">FAQs</a></li>
                               <li><a rel="canonical" href="<?php echo base_url(); ?>blog">Blog</a></li>
                           </ul>
                       </div>
                    </div>
                    <div class="col-sm-4 col-md-4"> 
                        <div class="footer-links">
                           <ul>
                               <li><a rel="canonical" href="<?php echo base_url(); ?>contactus">Contact us</a></li>
                               <li><a rel="canonical" href="<?php echo base_url(); ?>aboutus">About us</a></li>
                               <li><a rel="canonical" href="<?php echo base_url(); ?>login">Login</a></li>
                               <li><a rel="canonical" href="<?php echo base_url(); ?>signup">Sign up</a></li>
                           </ul>
                       </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="footer-links">
                           <ul>
                               <li><a rel="canonical" href="<?php echo base_url(); ?>termsandconditions">Terms and Conditions</a></li>
                               <li><a rel="canonical" href="<?php echo base_url(); ?>privacypolicy">Privacy Policy</a></li>
                           </ul>
                       </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-3">
                <div class="footer-social">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-google-plus"></i></a>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <p>all rights reserved</p>
        </div>
    </div>

</footer>

<!-----Start Script----->

<!--script src="js/jquery-3.2.1.min.js"></script-->
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_js/bootstrap.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_js/owl.carousel.js"></script>
<script>
      $(document).ready(function() {
        $('#testimonialmob').owlCarousel({
          //items: 6,

          loop: true,
          navText: [ '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>' ],
          dots: true,
          margin: 10,
          autoplay:true,
          autoplayTimeout: 2000,
          autoplayHoverPause:false,
          responsiveClass: true,
          responsive: {
            0: {
              items: 1,
              nav: true
            },
            768: {
              items: 2,
              nav: false
            },
            991: {
              items: 2,
              nav: true

            }
          }
        });


        $('mousewheel', '.owl-stage', function (e) {
            if (e.deltaY>0) {
                owl.trigger('next.owl');
            } else {
                owl.trigger('prev.owl');
            }
            e.preventDefault();
        });
      });
</script>


<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_js/slick.js"></script>
<script type="text/javascript">



    /*$(document).on('ready', function() {
        $(".center-slider").slick({
            dots: true,
            infinite: true,
            centerMode: true,
            slidesToShow: 5,
            slidesToScroll: 1
        });
    });*/

   $(document).on('ready', function() {
    $('.center-slider').slick({
      dots: true,
      infinite: true,
      //speed: 300,
      slidesToShow: 5,
      slidesToScroll: 1,

      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 575,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });

});



    setTimeout(function(){
      $('.slick-slide').on('mouseenter',function() {

          if (!$(this).hasClass('slick-center')) {
           $('.slick-center').addClass('small-center');
           $('.slick-center').removeClass('large-center');
          } else {
           $('.slick-center').addClass('large-center');
           $('.slick-center').removeClass('small-center');
          }

        });
         $('.slick-slide').on('mouseover',function() {

          if (!$(this).hasClass('slick-center')) {
           $('.slick-center').addClass('small-center');
           $('.slick-center').removeClass('large-center');
          } else {
           $('.slick-center').addClass('large-center');
           $('.slick-center').removeClass('small-center');
          }

        });

      $('.slick-slide').on('mouseout',function(){
       if (!$(this).hasClass('slick-center')) {
        $('.slick-center').addClass('large-center');
           $('.slick-center').removeClass('small-center');
          } else {

           $('.slick-center').addClass('large-center');
           $('.slick-center').removeClass('small-center');
          }
      });

     }, 1000);



</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-103722162-1', 'auto');
    ga('send', 'pageview');

</script>
<script>
    !function () {
        var t;
        if (t = window.driftt = window.drift = window.driftt || [], !t.init)
            return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0,
                    t.methods = ["identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on"],
                    t.factory = function (e) {
                        return function () {
                            var n;
                            return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                        };
                    }, t.methods.forEach(function (e) {
                t[e] = t.factory(e);
            }), t.load = function (t) {
                var e, n, o, i;
                e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"),
                        o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js",
                        n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
            });
    }();
    drift.SNIPPET_VERSION = '0.3.1';
    drift.load('d6yi38hkhwsd'); 
</script>

</body>
</html>