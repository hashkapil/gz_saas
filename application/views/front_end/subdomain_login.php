<?php $CI =& get_instance();
$CI->load->library('myfunctions'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Sign In | <?php echo $page_title; ?></title>
<link rel="shortcut icon canonical" type="image/x-icon" href="<?php echo $custom_favicon; ?>">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" rel="stylesheet canonical">
<link rel="stylesheet canonical" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
<link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/font-awesome.min.css">
<link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/custom.css">
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/bootstrap.min.js" ></script>
<link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet canonical">
<link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/style.css">
<!-- Google Tag Manager -->
<!--<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-57XFPLD');</script>-->
<!-- End Google Tag Manager -->
<style>
@media only screen and (max-width: 767px) {
  .subscription_pad {
    padding-top: 30px;
  }
}
div#logo img {
    max-width: 200px;
    max-height: 100px;
}
.back-to-main a{
    margin-left: 10px;
    color: #e8304d;
}
footer{
  display: none;
}
#SignupModal h2 {
    font-size: 20px;
    margin-bottom: 20px;
    color: #153669;
    font-weight: 400;
}
.border-bottom{
  padding-bottom: 17px;
    padding-right: 10px;
    border-color: #DCDCDC;
}
select.subs_plan {
    padding: 6px;
}
.select_wrap {
    border-bottom: 1px solid #e8304d;
}
.plans_table {
    border: 1px solid #e8304d;
    padding: 20px 15px;
    border-radius: 10px;
}
.plans_table h3 {
    text-transform: uppercase;
    color: #e8304d;
}
.plan_price span {
    font-size: 30px;
    font-weight: 800;
}
button.pricing_btn {
    border: 1px solid #e8304d;
    background: transparent;
    width: 100%;
    border-radius: 30px;
    color: #e8304d;
    text-transform: uppercase;
    font-size: 14px;
    font-weight: 800;
    padding: 5px;
    margin-bottom: 15px;
}
.plan_list{list-style:none; padding:15px}
h6{padding-left:15px;}
section.footer_section {
    padding: 50px 50px 80px;
}
button.btn.btn-y {
    background: #37c473;
    color: #fff;
    width: 30%;
  }
  button.btn.btn-n {
      background: #e73250;
      color: #fff;
      width: 30%;
  }
  #forgot_password .modal-dialog {
  max-width: 600px;
  margin: 50px auto;
}
#forgot_password .modal-dialog p.sub-title{margin-top: 50px;}
#forgot_password .cli-ent-model {
 padding: 10% 20%;
}
  div#logo {
    text-align: center;
    margin-bottom: 24px;
}
#signin {
    padding: 30px 0px 40px;
}
h1 {
    font-size: 40px;
    font-weight: 600;
    margin-bottom: 15px;
    margin-bottom: 50px;
}
.subdomain {
    max-width: 600px;
    margin: auto;
    width: 100%;
    background: #fff;
    box-shadow: 0 0 30px #ccc;
    padding: 40px;
    border-radius: 12px;
}
.subdomain .inner-addon button[type="submit"] {
    width: 100%;
}
</style>
</head>
<?php 
$this->load->view('front_end/variable_css'); 
$domainlogo = FS_PATH_PUBLIC_ASSETS . 'front_end/Updated_Design/img/Dashboard_logo.svg';
if($custom_logo == $domainlogo){
    $custom_logo = FS_PATH_PUBLIC_ASSETS . 'front_end/Updated_Design/img/logo.svg';
}
if (!empty($client_script)) {
    /***Start client script before close head tag***/
    foreach ($client_script as $k => $s_vl) {
        if (($s_vl['show_only_specific_page'] == "login" || $s_vl['show_only_specific_page'] == "all_pages") && $s_vl["script_position"] == "before_head") {
            if (strpos(base64_decode($s_vl["tracking_script"]), '</script>') !== false) {
                echo base64_decode($s_vl["tracking_script"]);
            } else{ ?>
                <script>
              <?php  echo base64_decode($s_vl["tracking_script"]); ?>
                </script>
                <?php
            }
        }
    }
    /***End client script before close body tag***/
}
  ?>
<body>
    <!-- Google Tag Manager (noscript) -->
<!--    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57XFPLD"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>-->
    <!-- End Google Tag Manager (noscript) -->
<section id="signin">
    <div class="container">
      <div class="subdomain">
    <div id="logo" class="pull-center">
                <!-- Uncomment below if you prefer to use an image logo -->
                <a href="<?php echo base_url(); ?>" rel="nofollow"><img src="<?php echo $custom_logo; ?>" alt="<?php  echo $page_title;?>" title="<?php echo $page_title; ?>" /></a>
    </div>
      <div class="section-title text-center">
        <h1>Sign In</h1>
      </div>
      <div class="row">
        <div class="col-md-12">
          <form method="post" class="signin-form" action="<?php echo base_url()."welcome/loginforsubdomain"?>">
            <?php if($this->session->flashdata('message_error') != '') {?>       
               <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong><?php echo $this->session->flashdata('message_error'); ?></strong>              
                </div>
               <?php }?>
               <?php if($this->session->flashdata('message_success') != '') {?>             
               <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong><?php echo $this->session->flashdata('message_success');?></strong>
                </div>
            <?php }?>
            <div class="form-signin-group col-md-12 inner-addon left-addon">
            <input type="email" name="email" class="form-control" id="uname" placeholder="Username" data-rule="minlen:4" data-msg="Please enter at correct username">
            <div class="validation"></div>
            </div>
            <input type="hidden" name="url" value="<?php echo isset($_SERVER['QUERY_STRING'])? $_SERVER['QUERY_STRING']:'' ?>">
            <div class="form-signin-group col-md-12 inner-addon left-addon">
            <input type="password" name="password" class="form-control" id="password" placeholder="Password" data-rule="minlen:4" data-msg="Please enter at correct password">
            <div class="validation"></div>
            </div>
            <div class="form-signin-group col-md-12 checkbox">
            <div class="form-radion2">
              <a data-toggle="modal" data-target="#forgot_password" href="javascript:void(0)">Forgot your password?</a>
            </div>
          </div>
            <div class="form-signin-group col-md-12 inner-addon">
            <button type="submit" class="red-theme-btn">Sign In</button>
            </div>
            <?php if($show_signup == 1){ ?>
            <div class="form-signin-group col-md-12 text-center">
                <a href="<?php echo base_url(); ?>user_signup" rel="canonical" class="create_account_link">Create Account</a>
           
            </div>
            <?php } ?>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- Modal -->
    <div class="modal fade" id="forgot_password" tabindex="-1" role="dialog" aria-labelledby="forgot_password" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="cli-ent-model-box">
            <div class="cli-ent-model">
              <header class="fo-rm-header">
                <h2 class="text-center">Forgot your Password?</h2>
                <p class="sub-title">Enter your email address and we'll send you a link to reset your password.</p>
              </header>
              <div class="confirmation_btn text-center">
                <form action="<?php echo base_url();?>welcome/forgot_password" method="post" >
                  <div class="">
                    <input class="form-control input-c" type="email" placeholder="Email Address" name="email" id="email" value=""/>
                    <input class="form-control input-c" type="hidden" name="fromdomain" id="fromdomain" value="1"/>
                  </div>
                  <div class="">
                    <button class="red-theme-btn" type="submit">Send Link</button>
                    <p>Not a member? <strong><a href="<?php echo base_url(); ?>user_signup" rel="canonical" class="create_account_link">Sign up!</a></strong></p>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php
  if (!empty($client_script)) {
      /***Start client script before close head tag***/
      foreach ($client_script as $k => $s_vl) {
          if (($s_vl['show_only_specific_page'] == "login" || $s_vl['show_only_specific_page'] == "all_pages") && $s_vl["script_position"] == "before_body") {
              if (strpos(base64_decode($s_vl["tracking_script"]), '</script>') !== false) {
                  echo base64_decode($s_vl["tracking_script"]);
              } else{ ?>
                  <script>
                <?php  echo base64_decode($s_vl["tracking_script"]); ?>
                  </script>
                  <?php
              }
          }
      }
      /***End client script before close head tag***/
  }
?>
</body>