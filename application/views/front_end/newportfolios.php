<style type="text/css">
section.publection-sec {
    display: none;
}
#myBtnContainer .active{
    border-bottom: 2px solid #e83833;
    border-radius: 0px;
}
.grid {
    display: grid;
    grid-gap: 10px;
    grid-template-columns: repeat(auto-fill, minmax(287px,1fr));
    grid-auto-rows: 20px;
}
.item {
    background-color: #ffffff;
}
ul.all-project li{
    overflow: hidden;
}
.box-overflow a{
    position: absolute;
    z-index: 1;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}
ul.all-project li:hover img{
    -webkit-transform: rotate(0) scale(1);
    -moz-transform: rotate(0) scale(1);
    -ms-transform: rotate(0) scale(1);
    -o-transform: rotate(0) scale(1);
    transform: rotate(0) scale(1);
}
.tab-content>.tab-pane{
    display: block !important;
    visibility: hidden;
    height: 0px;
    overflow: hidden;
}
.tab-content>.active{
    display: block !important;
    visibility: visible;
    height: 100%;
}
.websites_page{
    height: 100%;
}
div#Websites li.thumb{
    max-height: inherit;
}
#portfolioloader {
    position: fixed;
    left: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    opacity: .8;
}
</style>
<section class="page-heading-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Our Creative Work</h2>
                <p>
                    We create all types of graphic designs. From logos, brochures, flyers, web pages, 
                    t-shirts, illustrations to much more. Take a look at some of our work!
                </p>
            </div>
        </div>
    </div>
</section>
<!--------------Portfolie tabs---------------------->
<section class="poerfolioTabs">
    <div class="container">
        <div class="row">
            <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <ul id="tabsJustified" class="nav nav-tabel">
                    <li>
                        <a href="" data-target="#All" data-toggle="tab" class="active show_loading">
                            <button class="btn btn-default filter-button">All</button>
                        </a>

                    </li>
                    <li>
                        <a href="" data-target="#Logo" data-toggle="tab" class="show_loading">
                            <button class="btn btn-default filter-button">Logo</button>
                        </a>
                    </li>
                    <li>
                        <a href="" data-target="#T-shirt" data-toggle="tab" class="show_loading">
                            <button class="btn btn-default filter-button" >T-shirt</button>
                        </a>
                    </li>
                    <li>
                        <a href="" data-target="#Print" data-toggle="tab" class="show_loading">
                            <button class="btn btn-default filter-button" data-filter="spray">Print</button>
                        </a>
                    </li>
                    <li>
                        <a href="" data-target="#Ads" data-toggle="tab" class="show_loading">
                            <button class="btn btn-default filter-button" data-filter="irrigation">Ads</button>
                        </a>
                    </li>
                    <li>
                        <a href="" data-target="#Websites" data-toggle="tab" class="show_loading">
                            <button class="btn btn-default filter-button" data-filter="irrigation">Websites</button>
                        </a>
                    </li>
                    <!--li><a href="" data-target="#App" data-toggle="tab">
                        <button class="btn btn-default filter-button" data-filter="irrigation"> App</button>
                    </a></li-->
                    <li>
                        <a href="" data-target="#Other" data-toggle="tab" class="show_loading">
                            <button class="btn btn-default filter-button" data-filter="irrigation"> Other</button>
                        </a>
                    </li>
                </ul>
                <div id="tabsJustifiedContent" class="tab-content">

                    <!---------All portfolio---------------->
                    <div id="All" class="tab-pane fade active show">
                        <!-- ==============Logo Designs start ============== -->
                        <div id="portfolioloader">
                            <img src="http://dev.graphicszoo.com/public/assets/img/ajax-loader.gif"/>
                        </div>
                        <ul class="all-project grid" id="showLoadmore">
                            <!-- other design card -->
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add7.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add7.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>
                                    </div>
                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt1.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt1.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>
                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads51.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads51.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print55.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print55.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo9.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo9.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>
                            
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt53.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt53.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>
                                </div>
                            </li>
                            <li class="item thumb">
                                <a class="thumbnail" href="#" da websites_pageta-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website2.jpg" data-target="#image-gallery"></a>
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website2.jpg" class="img-responsive">
                                <div class="hover-txt">
                                    <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                    <p>Websites</p>
                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt52.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt52.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>
                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads58.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads58.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>
                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo53.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/logo53.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>
                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo60.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/logo60.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print6.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print6.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Branding</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print7.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print7.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website57.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website57.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>
                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add4.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add4.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print51.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print51.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>
                                </div>
                            </li>
                            
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>
                                </div>
                            </li>
                            
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt63.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt63.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>
                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt61.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt61.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>
                                </div>
                            </li>
                            
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo57.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo57.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>
                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads53.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads53.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print13.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print13.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/print56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads57.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/ads57.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>
                                </div>

                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt64.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt64.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add8.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add8.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print57.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print57.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo61.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/logo61.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other6.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other6.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print8.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print8.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo63.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/logo63.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other4.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other4.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other3.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other3.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo55.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo55.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Branding</p>
                                    </div>
                                </div>

                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other51.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other51.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>
                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo15.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo15.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other52.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/other52.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt51.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt51.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print5.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print5.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other53.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/other53.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo51.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo51.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print14.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print14.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo64.jpg" data-target="#image-gallery"></a>

                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/logo64.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other1.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other1.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add1.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add1.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo52.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo52.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>


                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add2.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add2.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add3.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add3.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other55.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other55.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt7.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt7.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo10.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo10.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other57.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other57.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo11.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo11.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other58.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other58.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other59.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other59.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo12.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo12.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website51.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website51.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo66.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo66.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website53.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website53.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt11.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt11.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo58.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo58.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website55.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website55.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print9.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print9.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo59.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo59.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo65.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo65.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website58.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website58.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website59.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website59.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print11.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print11.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo62.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo62.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print52.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print52.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo14.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo14.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add5.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add5.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add6.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add6.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add9.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add9.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add10.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add10.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add11.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add11.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add12.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add12.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>   
                            
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads59.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads59.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt60.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt60.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads52.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads52.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print2.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print2.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt3.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt3.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt59.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt59.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo5.jpg" data-target="#image-gallery"></a> 
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo5.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other2.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other2.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>print</p>
                                    </div>

                                </div>
                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt58.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt58.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo4.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo4.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website3.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website3.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Websites</p>
                                    </div>
                                </div>

                            </li>

                            

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt57.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt57.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website4.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website4.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Websites</p>
                                    </div>
                                </div>

                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print4.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print4.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>



                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo7.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo7.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo6.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo6.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website6.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website6.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Websites</p>
                                    </div>
                                </div>

                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt5.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt5.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt55.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt55.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>



                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads55.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads55.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Branding</p>
                                    </div>

                                </div>
                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print10.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print10.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>
                                </div>
                            </li>
                            
                            
                            

                            <!-- <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li> -->

                            <!-- other design card end -->


                            <!-- website design card -->
                            
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website52.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website52.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>

                            <!-- website design card end -->

                            <!-- print design card -->

                            
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print53.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print53.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            

                            <!-- print design card end -->

                            <!-- tshirt design card -->

                            
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt62.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt62.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            

                            <!-- tshirt design card end -->


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            


                            <!-- ==============Logo Designs end ============== -->
                            <!-- ==============ads Designs start ============== -->

                            <!-- <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads60.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads60.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>
                                </div>

                            </li> -->



                            

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website1.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website1.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Websites</p>
                                    </div>

                                </div>
                            </li>
                            




                            


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print12.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print12.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            


                            <!-- <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt8.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt8.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>

                                </div>
                            </li> -->

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo13.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo13.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>






                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website5.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website5.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Websites</p>
                                    </div>
                                </div>

                            </li>





                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other5.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other5.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>

                        </ul>
                        
                    </div>

                    <!---------Logo portfolio---------------->

                    <div id="Logo" class="tab-pane fade">
                        <ul class="all-project grid">
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo51.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo51.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo64.jpg" data-target="#image-gallery"></a>

                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/logo64.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo52.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo52.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>


                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo60.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/logo60.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>

                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo53.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/logo53.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>

                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo57.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo57.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo55.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo55.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>
                                </div>

                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo63.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/logo63.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>




                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo66.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo66.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>



                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo58.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo58.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo61.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/logo61.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>



                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo59.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo59.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo65.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo65.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo62.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo/logo62.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads51.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads51.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>logo</p>
                                    </div>

                                </div>
                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo1.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo1.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo2.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo2.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo3.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo3.jpg" class="img-responsive"><
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo4.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo4.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo5.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo5.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo6.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo6.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo7.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo7.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo9.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo9.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo10.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo10.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo11.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo11.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo12.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo12.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo13.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo13.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo14.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo14.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo15.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/logo15.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Logo</p>
                                    </div>

                                </div>
                            </li>


                        </ul>
                        
                    </div>

                    <!---------T-Shirt portfolio---------------->
                    <div id="T-shirt" class="tab-pane fade">
                        <ul class="all-project grid">
                            <!-- tshirt design card -->
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt64.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt64.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt63.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt63.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt62.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt62.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt61.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt61.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt60.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt60.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt59.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt59.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt58.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt58.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt57.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt57.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt55.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt55.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt53.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt53.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt52.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt52.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt51.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt/t-shirt51.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-shirt</p>
                                    </div>

                                </div>
                            </li>
                            <!-- tshirt design card end -->

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt1.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt1.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt2.jpg"data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt2.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt3.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt3.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt5.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt5.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt7.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt7.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt8.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt8.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt11.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt11.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>T-Shirt</p>
                                    </div>

                                </div>
                            </li>

                        </ul>
                        
                    </div>
                    <!---------Print portfolio---------------->
                    <div id="Print" class="tab-pane fade">
                        <ul class="all-project grid">
                            <!-- print design card -->
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print51.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print51.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print52.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print52.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print53.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print53.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print55.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print55.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/print56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print57.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print/print57.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <!-- print design card end -->

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print1.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print1.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print2.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print2.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print3.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print3.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print4.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print4.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print5.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print5.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print7.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print7.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print6.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print6.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print8.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print8.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print9.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print9.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print10.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print10.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print11.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print11.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print12.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print12.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print13.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print13.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print14.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/print14.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Print</p>
                                    </div>

                                </div>
                            </li>

                        </ul>
                        
                    </div>
                    <!---------Ads portfolio---------------->

                    <div id="Ads" class="tab-pane fade">

                        <ul class="all-project grid">
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads53.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads53.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads55.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads55.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads58.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads58.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>




                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads57.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/ads57.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>
                                </div>

                            </li>




                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>
                           <!--  <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads60.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads60.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>
                                </div>

                            </li> -->



                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/ads/ads56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add1.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add1.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>


                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add2.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add2.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add3.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add3.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add4.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add4.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add5.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add5.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add7.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add7.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add8.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add8.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add6.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add6.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add9.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add9.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add10.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add10.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add11.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add11.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add12.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/add12.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Ad</p>

                                    </div>

                                </div>
                            </li>

                        </ul>
                        
                    </div>
                    <!---------Websites portfolio---------------->

                    <div id="Websites" class="tab-pane fade">

                        <ul class="all-project grid">
                            <!-- website design card -->
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website51.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website51.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website52.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website52.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website53.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website53.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website55.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website55.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website57.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website57.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website58.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website58.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website59.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website/website59.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>website</p>
                                    </div>

                                </div>
                            </li>



                            <!-- website design card end -->

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website1.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website1.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Websites</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website2.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website2.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Websites</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website3.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website3.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Websites</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website4.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website4.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Websites</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website5.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website5.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Websites</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website6.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/website6.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Websites</p>
                                    </div>

                                </div>
                            </li>

                        </ul>

                    </div>
                    <!--------- App portfolio---------------->

                    <!---------  Other portfolio---------------->

                    <div id="Other" class="tab-pane fade">

                        <ul class="all-project grid">
                            <!-- other design card -->

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other52.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/other52.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other53.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/thumb/other53.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other54.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other54.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other55.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other55.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow websites_page">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other56.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other56.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other57.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other57.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other58.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other58.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other59.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other/other59.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>

                            <!-- other design card end -->
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other1.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other1.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other2.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other2.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other3.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other3.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other4.jpg" ata-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other4.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>

                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other5.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other5.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>


                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other6.jpg" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/other6.jpg" class="img-responsive">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                                        <p>Other</p>
                                    </div>

                                </div>
                            </li>


                        </ul>

                        
                    </div>
                    <div class="col-md-12 text-center loading" id="load_more_other">
                        <a href="javascript:void(0)" class="red-theme-btn load_more">
                            View More
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-fluid">
                        </a>
                    </div>
                </div>  

                
                <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="image-gallery-title"></h4>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <img id="image-gallery-image" class="img-responsive" src="">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fas fa-chevron-left"></i>
                                </button>

                                <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fas fa-chevron-right"></i>
                                </button>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>

</script>
<section class="riskFree-sec pricing-risk">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Join more than 500+ customers</h2>
                <h3>Try Graphics Zoo Risk-Free For 14 Days</h3>
                <a href="http://dev.graphicszoo.com/signup" class="red-theme-btn">Get Started Now
                    <img src="http://dev.graphicszoo.com/public/assets/front_end/Updated_Design/img/red-long-arrow.png" class="img-fluid"></a>

                </div>
            </div>
        </div>
    </section>

    <script type='text/javascript'>

        function make_grid(){
            var grid = $(".grid");
            var getItem = grid.children('li');
            getItem.each(function(){
                var getItem_height = 0;
                getItem_height = $(this).find('img').height();
                var rowHeight =  parseInt($(grid).css( "grid-auto-rows" ));
                var rowGap = parseInt($(grid).css('grid-row-gap'));
                var rowSpan = Math.ceil((getItem_height + rowGap) / (rowHeight + rowGap));
                if(rowSpan > 15){
                    $(this).css( "grid-row-end", "span 15");
                }
                else{
                    $(this).css( "grid-row-end", "span " + rowSpan );
                }
            });
        }

        var tablistID = $(".nav-tabel").children('li').find('a.active').attr('data-target');
        var numToShow = 20;
        function loadData(id){
            var list = $(id).find('.grid > li');
            var numInList = list.length;
            list.hide();
            list.slice(0, numToShow).show();
        }

        $(window).load(function(){
            make_grid();
            loadData(tablistID);

        });

        $('.show_loading').click(function () {
            $('.load_more').show();
            var getID = $(this).attr('data-target');
            var list = $(getID).find('.grid > li');
            if(list.length <= numToShow){
                $('.load_more').hide();
            }
            loadData(getID);
        });


        $('.load_more').click(function(){   
           var list = $(tablistID).find('.grid > li');
           var getListLength = list.length;

           var showing = list.filter(':visible').length;
           list.slice(showing , showing + numToShow).show();
           var showing = list.filter(':visible').length;
           if(showing >= getListLength){
            $(this).hide();
        }

    });


        /***********portfolio active tab from home page***************/
        var url = window.location.href;
        var activeTab = url.substring(url.indexOf("#") + 1);
        var hash = window.location.href.indexOf('#');
        if(activeTab != '' && hash > 0){
         $(".tab-pane").removeClass("active");
         $('#' + activeTab).addClass("active show");
         make_grid();
         var id = $('.show_loading').data("target");
         var list = $("" + id + " li");
         var button = $(".load_more");
         loadMore(list, button);
            // $('.loading').css('display','none');
            $('#tabsJustified').find('li a').each(function(e) {
             var dataattr = $(this).data('target');
             var fromurl = ("#" + activeTab);
             if(dataattr == fromurl){
                 $(this).addClass('active');
                 $(this).closest('li').siblings().find('a').removeClass('active');
                 return false;
             }
         });
        }

        $(window).load(function() {
         $("#portfolioloader").fadeOut("slow");
     });




 </script>
