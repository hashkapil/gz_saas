<<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&display=swap" rel="stylesheet canonical">
<?php $CI =& get_instance();
$CI->load->library('customfunctions');
//echo "<pre>";print_r($plan_price['agency_price']);exit; ?>
<section class="page-heading-sec pricing-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Even Pricing is Easier with Graphics Zoo</h1>
                <p>Perfectly Scalable. Pay Only for What You Need.</p>
            </div>
            <div class="col-md-12">
                <div class="plan_selct_bt">
                  <div class="switch_btn_plan">
                      <a href="javascript:void(0)" class="monthly_plan active">Monthly Plan</a>
                      <a href="javascript:void(0)" class="yearly_plan">Annual Plan&nbsp;<span>(Save 15%)</span></a>
                  </div>
                 <!--  <i class="fas fa-check tick_icon"></i> -->
                  <!--<span class="save_txt"><span>→ </span>  Save 15%</span>-->
                 <!--  <img src="<?php //echo FS_PATH_PUBLIC_ASSETS.'front_end/Updated_Design/img/dashed_border.svg';?>" class="img-fluid dashed_border_img"> -->
              </div>
          </div>
      </div>
  </div>    
</section>

<div class="pricing-tabel-section section-gap">
    <div class="container">
        <div class="top-price">
            <div class="row">
                <div class="col-md-4 bunss_pln">
                    <div class="price-card best-offer">
                        <div class="price-card-header">
                            <div class="price-name">
                                <p class="text-center">BUSINESS</p>
                            </div>
                            <div class="price-amount text-center">
                                <h2><span class="currency-sign">$</span><span class="chngg_price busns_price"><?php echo SUBSCRIPTION_MONTHLY_PRICE; ?></span><span class="time-period">per month <font class="time-period yearly_pln-prc"></font></span></h2>
                            </div>
                            <div class="price-details">
                                <ul>
                                    <li>
                                        <p><span>14 Day</span>&nbsp;Satisfaction Guarantee</p>
                                    </li>
                                     <li>
                                        <p>Unlimited Designs & Revisions</p>
                                    </li>
                                    <li>
                                        <div class="quantiy-details">
                                            <select class="pricebasedquantity">
                                                <?php foreach ($plan_price['business_price'] as $value) { ?>
                                                    <option value="<?php echo $value['quantity']; ?>" data-amount="<?php echo $value['amount']; ?>" data-annual_price="<?php echo $value['annual_price']; ?>"><?php echo $value['quantity']; ?> <span> Dedicated <?php echo ($value['quantity'] == 1)?'Designer':"Designers";?></span></option>
                                                <?php } ?>
                                            </select>

                                        </div>
                                    </li>
                                   
                                </ul>
                            </div>
                            <div class="price-sign-up">
                                <a href="javascript:void(0)" rel="canonical" data-value="0" data-id="<?php echo SUBSCRIPTION_MONTHLY; ?>" class="btn-plan_pricing busns_plan" >SignUp Now</a>
                            </div>
                        </div>
                        <div class="price-details">
                            <ul>
                                <li>
                                    <p><i class="fas fa-check"></i>Project-Flow Technology</p>
                                    <p><i class="fas fa-check"></i>5 Brand Profiles</p>
                                    <p><i class="fas fa-check"></i>3 Users</p>
                                    <p><i class="fas fa-check"></i>File Sharing</p>
                                    <p><i class="fas fa-check"></i>Email Requests</p>
                                    <p><i class="fas fa-check"></i>Stock Photos Integration</p>
                                    <p><i class="fas fa-check"></i>Custom brand guideline, Slide decks, Infographics & more</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 cpmnny_pln">
                 <h3 class="popular_pln">Most Popular</h3>
                 <div class="price-card best-offer">
                    <div class="price-card-header">
                        <div class="for_plan_grey">
                            <div class="price-name navy">
                                <p class="text-center">BUSINESS UNLIMITED</p>
                            </div>
                        </div>
                        <div class="price-amount text-center">
                            <h2><span class="currency-sign">$</span><span class="chngg_price cmny_price"><?php echo COMPANY_PLAN_PRICE; ?></span><span class="time-period">per month <font class="time-period yearly_pln-prc"></font></span></h2>
                        </div>
                        <div class="price-details">
                            <ul>
                                <li>
                                    <p><span>14 Day </span>&nbsp;Satisfaction Guarantee</p>
                                </li>
                                <li>
                                    <p>Unlimited Designs & Revisions</p>
                                </li>
                                <li>
                                    <div class="quantiy-details">
                                        <select  class="pricebasedquantity">
                                            <?php foreach ($plan_price['company_price'] as $value) { ?>
                                                <option value="<?php echo $value['quantity']; ?>" data-amount="<?php echo $value['amount']; ?>" data-annual_price="<?php echo $value['annual_price']; ?>"><?php echo $value['quantity']; ?><span> Dedicated <?php echo ($value['quantity'] == 1)?'Designer':"Designers";?></span></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </li>
                                
                            </ul>
                        </div>
                        <div class="price-sign-up">
                            <a href="javascript:void(0)" rel="canonical" data-value="0" data-id="<?php echo COMPANY_PLAN; ?>" class="btn-plan_pricing cmny_plan" >SignUp Now</a>
                        </div>
                    </div>
                    <div class="price-details">
                        <ul>
                         <li>
                            <div  class="everything">Everything in Business Plan <span>+</span></div>
                        </li>
                        <li>
                            <p><i class="fas fa-check"></i>Unlimited Brand Profiles</p>
                            <p><i class="fas fa-check"></i>Unlimited Users & Teams</p>
                            <p><i class="fas fa-check"></i>White-Label Platform</p>
                            <p><i class="fas fa-check"></i>Folders</p>
                            <p><i class="fas fa-check"></i>Custom Website Designs</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-4 agncy_plnn">
            <div class="price-card best-offer">
                <div class="price-card-header">
                    <div class="price-name">
                        <p class="text-center">AGENCY</p>
                    </div>
                    <div class="price-amount text-center">
                        <h2><span class="currency-sign">$</span><span class="chngg_price agenc_price"><?php echo AGENCY_PLAN_PRICE; ?></span><span class="time-period">per month <font class="time-period yearly_pln-prc"></font></span>

                        </h2>
                    </div>
                    <div class="price-details">
                        <ul>
                            <li>
                                <p><span>30 Day</span>&nbsp;Satisfaction Guarantee</p>
                            </li>
                             <li>
                                <p>Unlimited Designs & Revisions</p>
                            </li>
                            <li>
                                <div class="quantiy-details">
                                    <select  class="pricebasedquantity">
                                        <?php foreach ($plan_price['agency_price'] as $value) { ?>
                                            <option value="<?php echo $value['quantity']; ?>" data-amount="<?php echo $value['amount']; ?>" data-annual_price="<?php echo $value['annual_price']; ?>"><?php echo $value['quantity']; ?><span> Dedicated  <?php echo ($value['quantity'] == 1)?'Designer':"Designers";?></span></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </li>
                           
                        </ul>
                    </div>
                    <div class="price-sign-up">
                        <a href="javascript:void(0)" rel="canonical" data-value="0" data-id="<?php echo AGENCY_PLAN; ?>" class="btn-plan_pricing agenc_plan" >SignUp Now</a>
                    </div>
                </div>
                <div class="price-details">
                    <ul>
                     <li>
                        <div class="everything">Everything in Business Unlimited <span>+</span></div>
                    </li>
                    <li>
                        <p><i class="fas fa-check"></i>Day-time Designers</p>
                        <p><i class="fas fa-check"></i>Real-time Collaboration</p>
                        <p><i class="fas fa-check"></i>Animated GIFs</p>
                        <p><i class="fas fa-check"></i>Custom API Integrations (Coming Soon)</p>
                        <p><i class="fas fa-check"></i>Same Day Delivery</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center">
        <a href="#plan-comparison" class="red-theme-btn comparison" rel="nofollow">See Full Comparison
            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/red-long-arrow.png" class="img-fluid" alt="Long Arrow"></a>

        </div>
    </div>
</div>
</div>
</div>

<!------ testomonial-sec-------->
<section class="testomonial-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>What others have to say about us</h2>
                <p>Our customers love us and they have shared a few reviews to say just how much.
                    <br/>Take a look at some of the awesome feedback we have received.
                </p>
            </div>
            <div class="testimonial_slider owl-carousel owl-theme owl-loaded owl-drag">
                <div class="col-md-12">
                    <div class="comment_sec">
                        <div class="comment-here">
                            <div class="img_div">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post4.png" class="img-fluid no_bg" alt="Patrick Black">
                            </div>
                            <div class="header-part">
                                <h4>Patrick Black </h4>
                                <p class="place_name">Florida, USA</p>
                                <p class="comment5">
                                    <?php
                                    echo substr("Graphics Zoo has been amazing to work with for my graphic design needs. They are fast and the designs are always on point. They do a great job following directions I provide, and the design queue makes it easy for me to prioritize all my design jobs needed. Their service has become an invaluable resource for my company!", 0, 160) . "...";
                                    ?>
                                </p>
                                <div class="star-list"><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="comment_sec">
                        <div class="comment-here">
                            <div class="img_div">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post2.png" class="img-fluid dark" alt="Shea Bramer">
                            </div>
                            <div class="header-part">
                                <h4>Shea Bramer</h4>
                                <p>Marketing Coordinator, Western Rockies</p>
                                <p class="comment5">
                                    <?php
                                    echo substr("We are a small marketing team for a large financial company and new growth in the company meant that the graphic design workload was becoming overwhelming for our team. Graphicszoo created an easy way for us to keep up with the workload as our company continues to grow. They pay close attention to our branding, and are very detail oriented (as I can be quite demanding). The work they provide for us is always very high quality and I always get what I'm asking for. Their prices are also very competitive. We highly suggest this company!", 0, 140) . "...";
                                    ?>
                                </p>
                                <div class="star-list"><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="comment_sec">
                        <div class="comment-here">
                            <div class="img_div">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post3.png" class="img-fluid light" alt="Skylar Domine">
                            </div>
                            <div class="header-part">
                                <h4>Skylar Domine </h4>
                                <p>Goat Grass Co</p>
                                <p class="comment5">
                                    <?php
                                    echo substr("I've been through dozens of design teams and none of which ever checked off all the boxes- price, turnaround time, consistency, and accuracy.  I'm actually 'wowed' by this company and will have my loyalty for years to come from my circle. Highly recommend to anyone looking for design work. If you can visualize it then they can create it!", 0, 150) . "...";
                                    ?>
                                </p>
                                <div class="star-list"><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="comment_sec">
                        <div class="comment-here">
                            <div class="img_div">
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/green-gurd1.jpg" class="img-fluid no_bg" alt="Nathan Leathers">
                            </div>
                            <div class="header-part">
                                <h4>Nathan Leathers - Owner</h4>
                        <p>Green Gaurd, Printing and Apparel</p>
                                <p class="comment5">
                                    <?php
                                    echo substr("Graphics Zoo has been amazing to work with for my graphic design needs. They are fast and the designs are always on point. They do a great job following directions I provide, and the design queue makes it easy for me to prioritize all my design jobs needed. Their service has become an invaluable resource for my company!", 0, 150) . "...";
                                    ?>
                                </p>
                                <div class="star-list"><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="comment_sec">
                        <div class="comment-here">
                            <div class="img_div">
                            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/focal-shift.png" class="img-fluid no_bg" alt="Thomas Elliott Fite">
                            </div>
                            <div class="header-part">
                                <h4>Thomas Elliott Fite</h4>
                                <p>Focal Shift </p>
                                <p class="comment5">
                                    <?php
                                    echo substr("Graphics Zoo has been amazing to work with for my graphic design needs. They are fast and the designs are always on point. They do a great job following directions I provide, and the design queue makes it easy for me to prioritize all my design jobs needed. Their service has become an invaluable resource for my company!", 0, 150) . "...";
                                    ?>
                                </p>
                                <div class="star-list"><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i><i class="fas fa-star full"></i></div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>

        </div>
    </div>
    <div class="testomonial-out">
        <div class="testomonial owl-carousel owl-theme">
            <div class="item">
                <div class="comment-here">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post4.png" class="img-fluid no_bg" alt="Patrick Black">
                    <div class="header-part">
                        <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star full"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                        <h4>Patrick Black </h4>
                        <p>Florida, USA</p>
                    </div>
                    <p class="comment5">
                        <?php
                        echo substr("Graphics Zoo has been amazing to work with for my graphic design needs. They are fast and the designs are always on point. They do a great job following directions I provide, and the design queue makes it easy for me to prioritize all my design jobs needed. Their service has become an invaluable resource for my company!", 0, 150) . "...";
                        ?>
                    </p>
                    <a href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" alt="Long Arrow"></a>

                </div>
            </div>
            <div class="item">
                <div class="comment-here">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post2.png" class="img-fluid dark" alt="Shea Bramer">
                    <div class="header-part">
                        <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                        <h4>Shea Bramer</h4>
                        <p>Marketing Coordinator, Western Rockies
                        </p>
                    </div>
                    <p class="comment5">
                        <?php
                        echo substr("We are a small marketing team for a large financial company and new growth in the company meant that the graphic design workload was becoming overwhelming for our team. Graphicszoo created an easy way for us to keep up with the workload as our company continues to grow. They pay close attention to our branding, and are very detail oriented (as I can be quite demanding). The work they provide for us is always very high quality and I always get what I'm asking for. Their prices are also very competitive. We highly suggest this company!", 0, 150) . "...";
                        ?>

                    </p>
                    <a href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img alt="Long Arrow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png"></a>

                </div>
            </div>
            <div class="item">
                <div class="comment-here">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post3.png" class="img-fluid light" alt="Skylar Domine">
                    <div class="header-part">
                        <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                        <h4>Skylar Domine </h4>
                        <p>Goat Grass Co</p>
                    </div>
                    <p class="comment5">
                        <?php
                        echo substr("I've been through dozens of design teams and none of which ever checked off all the boxes- price, turnaround time, consistency, and accuracy.  I'm actually 'wowed' by this company and will have my loyalty for years to come from my circle. Highly recommend to anyone looking for design work. If you can visualize it then they can create it!", 0, 150) . "...";
                        ?>

                    </p>
                    <a href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" alt="Long Arrow"></a>
                </div>
            </div>
            <div class="item">
                <div class="comment-here">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/green-gurd1.jpg" class="img-fluid no_bg" alt="Nathan Leathers">
                    <div class="header-part">
                        <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                        </div>
                        <h4>Nathan Leathers - Owner</h4>
                        <p>Green Gaurd, Printing and Apparel</p>
                    </div>
                    <p class="comment5">
                        <?php
                        echo substr("Had another competitor prior to this, and within 3 days Graphics Zoo has far exceeded any level of service I had with the previous competitor in 2 years.  They are very responsive and so far fast and solid production of content!", 0, 150) . "...";
                        ?>

                    </p>
                    <a href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" alt="Long Arrow"></a>
                </div>
            </div>
            <div class="item">
                <div class="comment-here">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/focal-shift.png" class="img-fluid no_bg" alt="Thomas Elliott Fite">
                    <div class="header-part">
                        <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                        </div>
                        <h4>Thomas Elliott Fite</h4>
                        <p>Focal Shift </p>
                    </div>
                    <p class="comment5">
                        <?php
                        echo substr("Pretty impressed! Been with them for several months now and we've finally found our work flow with them! Great job GZ and keep up the great work!", 0, 150) . "...";
                        ?>

                    </p>
                    <a href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" alt="Long Arrow"></a>
                </div>
            </div>
            <div class="item">
                <div class="comment-here">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/scott.png" class="img-fluid no_bg" alt="Scott Levesque">
                    <div class="header-part">
                        <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                        </div>
                        <h4>Scott Levesque</h4>
                        <p>Be Free Clothing Company</p>
                    </div>
                    <p class="comment5">
                        <?php
                        echo substr("Great designs, quick turnaround and awesome to work with. Thank you.", 0, 150) . "...";
                        ?>

                    </p>
                    <a href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" alt="Long Arrow"></a>

                </div>
            </div>

        </div>
    </div>
</section>

<!------ testomonial-sec end-------->


<!------ Risk free-sec -------->
<section class="riskFree-sec pricing-risk">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Join more than 500+ customers</h2>
                <h3>Try Graphics Zoo Risk-Free For 14 Days</h3>
                <a rel="canonical" href="<?php echo base_url(); ?>signup" class="red-theme-btn" rel="nofollow">Get Started Now
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/red-long-arrow.png" class="img-fluid" alt="Long Arrow"></a>

                </div>
            </div>
        </div>
    </section>

    <!------/ Risk free-sec -------->


    <!------ Plan comparison-sec -------->

    <section class="plan-comperesion" id="plan-comparison">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Full Plan Comparison</h2>
                </div>
                <div class="col-md-12">
                    <div id="no-more-tables">
                        <table class="col-md-12 table-condensed cf">
                            <thead class="cf">
                                <tr>
                                    <th><br/></th>
                                    <th>BUSINESS PLAN</th>
                                    <th>BUSINESS UNLIMITED PLAN</th>
                                    <th class="numeric">AGENCY PLAN</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td data-title="">Plan fee</td>
                                    <td data-title="BUSINESS PLAN"><strong>$349</strong></td>
                                    <td data-title="BUSINESS UNLIMITED PLAN"><strong>$449</strong></td>
                                    <td data-title="AGENCY PLAN" ><strong>$849+</strong></td>
                                </tr>
                                <tr>
                                    <td data-title="">Requests/Month</td>
                                    <td data-title="BUSINESS PLAN">Unlimited Requests</td>
                                    <td data-title="BUSINESS UNLIMITED PLAN">Unlimited Requests</td>
                                    <td data-title="AGENCY PLAN" >Unlimited Requests</td>
                                </tr>
                                <tr>
                                    <td data-title="">1-Day Turnaround</td>
                                    <td data-title="BUSINESS PLAN">Daily</td>
                                    <td data-title="BUSINESS UNLIMITED PLAN" >Daily</td>
                                    <td data-title="AGENCY PLAN" >Daily</td>
                                </tr>
								<tr>
                                    <td data-title="">Dedicated Designers</td>
                                    <td data-title="BUSINESS PLAN">1</td>
                                    <td data-title="BUSINESS UNLIMITED PLAN">1</td>
                                    <td data-title="AGENCY PLAN" >2+</td>
                                </tr>
                                <tr>
                                    <td data-title="">Active Requests</td>
                                    <td data-title="BUSINESS PLAN">2</td>
                                    <td data-title="BUSINESS UNLIMITED PLAN">2</td>
                                    <td data-title="AGENCY PLAN">4+</td>
                                </tr>
                                <tr>
                                    <td data-title="">Share and Collaborate</td>
                                    <td data-title="BUSINESS PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="BUSINESS UNLIMITED PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="AGENCY PLAN" ><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td data-title="">Unlimited Revisions</td>
                                    <td data-title="BUSINESS PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="BUSINESS UNLIMITED PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="AGENCY PLAN" ><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td data-title="">Unlimited Brands</td>
                                    <td data-title="BUSINESS PLAN">5 Brand Profiles</td>
                                    <td data-title="BUSINESS UNLIMITED PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="AGENCY PLAN" ><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td data-title="">User Management</td>
                                    <td data-title="BUSINESS PLAN">3 Users</td>
                                    <td data-title="BUSINESS UNLIMITED PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="AGENCY PLAN" ><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td data-title="">Quality Designers</td>
                                    <td data-title="BUSINESS PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="BUSINESS UNLIMITED PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="AGENCY PLAN" ><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td data-title="">Dedicated Support</td>
                                    <td data-title="BUSINESS PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="BUSINESS UNLIMITED PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="AGENCY PLAN" ><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td data-title="">Unlimted Free Stock Images</td>
                                    <td data-title="BUSINESS PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="BUSINESS UNLIMITED PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="AGENCY PLAN" ><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td data-title="">No Contracts</td>
                                    <td data-title="BUSINESS PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="BUSINESS UNLIMITED PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="AGENCY PLAN" ><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td data-title="">White label</td>
                                    <td data-title="BUSINESS PLAN"><i class="fas fa-check grey-check"></i></td>
                                    <td data-title="BUSINESS PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="AGENCY PLAN" ><i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td data-title="">Email Requests</td>
                                    <td data-title="BUSINESS PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="BUSINESS UNLIMITED PLAN"><i class="fas fa-check"></i></td>
                                    <td data-title="AGENCY PLAN" ><i class="fas fa-check"></i></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="text-center">
                        <a rel="canonical" href="<?php echo base_url(); ?>signup" class="red-theme-btn">Sign Up Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="faq-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Frequently Asked Question</h2>
                </div>
            </div>
            <div id="accordion" class="faq-accordion">
                <div class="row">
                    <div class="col-md-6">
                        <div class="accordin-outer">
                            <div class=" collapsed" data-toggle="collapse" href="#collapseOne">
                                <a class="card-title">
                                    <h4>Do I really get unlimited designs?</h4>
                                </a>
                            </div>
                            <div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
                                <p>Yes! You really get unlimited designs and revisions for a flat monthly rate. You can request as many revisions as you like until you are completely satisfied. We work on 1 new request a day.</p>
                            </div>
                        </div>
                        <div class="accordin-outer">
                            <div class=" collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                <a class="card-title">
                                    <h4>What does Risk-Free guarantee really mean?</h4>
                                </a>
                            </div>
                            <div id="collapse2" class="card-body collapse" data-parent="#accordion" >
                                <p>If you do not approve any of your designs or graphics within the first 14 days of
                                    your trial, we will give you a full refund, no questions asked.
                                </p>
                            </div>
                        </div>
                        <div class="accordin-outer">
                            <div class=" collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                <a class="card-title">
                                    <h4>What if I need 220 designs per day?</h4>
                                </a>
                            </div>
                            <div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
                                <p>If you know your design volume will be higher than the active requests per day is,
                                we recommend you get additional accounts so your design volume can be handled properly.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="accordin-outer">
                            <div class=" collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                <a class="card-title">
                                    <h4>How long does it take to receive designs?</h4>
                                </a>
                            </div>
                            <div id="collapseThree" class="card-body collapse" data-parent="#accordion" >
                                <p>Every design request is serviced within 1 business day. Please keep in mind though,
                                    that we value quality over everything else. Your designer will be in constant communication
                                with you and will keep you updated on the status of your designs. </p>
                            </div>
                        </div>
                        <div class="accordin-outer">
                            <div class=" collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                <a class="card-title">
                                    <h4>What can I get designed?</h4>
                                </a>
                            </div>
                            <div id="collapse3" class="card-body collapse" data-parent="#accordion" >
                                <p>You can get anything designed from your designer. Everything from banners,
                                    flyers, t-shirts, book covers and much more. Just keep in mind larger and more
                                complicated designs may take a little longer to finish.</p>
                            </div>
                        </div>
                        <div class="accordin-outer">
                            <div class=" collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                <a class="card-title">
                                    <h4>Is there really no contract?</h4>
                                </a>
                            </div>
                            <div id="collapse4" class="card-body collapse" data-parent="#accordion" >
                                <p>Yes, that's right! No contract, so you can cancel at anytime.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>
var monthly_plans_price = '<?php echo json_encode($plan_price); ?>';
if (monthly_plans_price) {
    monthly_plans_price = jQuery.parseJSON(monthly_plans_price);
} 

var yearly_plans_price = '<?php echo json_encode($agency_price); ?>';
if (yearly_plans_price) {
    yearly_plans_price = jQuery.parseJSON(yearly_plans_price);
} 

jQuery(document).ready(function(){
    jQuery(".testimonial_slider").owlCarousel({
        autoplayHoverPause: true,
        loop: false,
        lazyLoad: true,
        autoplay: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            992: {
                items: 2
            }
        }

    });
});

$(document).on('click','.yearly_plan',function(){

    $(".monthly_plan").removeClass("active");
    $(this).addClass("active");

           var selected_bussquantity = $('.bunss_pln .pricebasedquantity').val();
           var selected_cmnyquantity = $('.cpmnny_pln .pricebasedquantity').val();
           var selected_agncyquantity = $('.agncy_plnn .pricebasedquantity').val();
           

           // $('.busns_price').html(yearly_plans_price.business_price[1].amount);
            $('.busns_plan').data('id',yearly_plans_price.business_price[1].plan_id);

          //  $('.cmny_price').html(yearly_plans_price.company_price[1].amount);
            $('.cmny_plan').data('id',yearly_plans_price.company_price[1].plan_id);

         //   $('.agenc_price').html(yearly_plans_price.agency_price[2].amount);
            $('.agenc_plan').data('id',yearly_plans_price.agency_price[2].plan_id);

            $('.yearly_pln-prc').html("(Billed Annually)");

            /** business plan dropdown **/
            var bunss_pln = '';
            $.each(yearly_plans_price.business_price, function (k, v) {
				bunss_pln += '<option value="' + v.quantity + '" data-amount="' + v.amount + '">' + v.quantity + '<span>&nbsp;Dedicated ';
				bunss_pln += (v.quantity == 1) ? 'Designer' : 'Designers';
				bunss_pln += '</span></option>';
                //bunss_pln += '<option value="' + v.quantity + '" data-amount="' + v.amount + '">' + v.quantity + '<span>&nbsp;Dedicated Designers</span></option>';
            });
            $('.bunss_pln .pricebasedquantity').html(bunss_pln);

            /** company plan dropdown **/
            var cpmnny_pln = '';
            $.each(yearly_plans_price.company_price, function (k, v) {
                cpmnny_pln += '<option value="' + v.quantity + '" data-amount="' + v.amount + '">' + v.quantity + '<span>&nbsp;Dedicated ';
				cpmnny_pln += (v.quantity == 1) ? 'Designer' : 'Designers';
				cpmnny_pln += '</span></option>';
				//cpmnny_pln += '<option value="' + v.quantity + '" data-amount="' + v.amount + '">' + v.quantity + '<span>&nbsp;Dedicated Designers</span></option>';
            });
            $('.cpmnny_pln .pricebasedquantity').html(cpmnny_pln);

            /** agency plan dropdown **/
            var agncy_plnn = '';
            $.each(yearly_plans_price.agency_price, function (k, v) {
                agncy_plnn += '<option value="' + v.quantity + '" data-amount="' + v.amount + '">' + v.quantity + '<span>&nbsp;Dedicated Designers</span></option>';
            });
            $('.agncy_plnn .pricebasedquantity').html(agncy_plnn);
            
            /**set price and quantity **/
            $(".bunss_pln .pricebasedquantity").val(selected_bussquantity).find("option[value=" + selected_bussquantity +"]").attr('selected', true);
            var busns_price = $(".bunss_pln .pricebasedquantity option:selected").attr('data-amount');
            $(".busns_price").html(busns_price);
            
            $(".cpmnny_pln .pricebasedquantity").val(selected_cmnyquantity).find("option[value=" + selected_cmnyquantity +"]").attr('selected', true);
            var cmny_price = $(".cpmnny_pln .pricebasedquantity option:selected").attr('data-amount');
            $(".cmny_price").html(cmny_price);
            
            $(".agncy_plnn .pricebasedquantity").val(selected_agncyquantity).find("option[value=" + selected_agncyquantity +"]").attr('selected', true);
            var agenc_price = $(".agncy_plnn .pricebasedquantity option:selected").attr('data-amount');
            $(".agenc_price").html(agenc_price);
});

$(document).on('click','.monthly_plan',function(){

    $(".yearly_plan").removeClass("active");
    $('.yearly_pln-prc').html("");
    $(this).addClass("active");
    
            var selected_bussquantity = $('.bunss_pln .pricebasedquantity').val();
            var selected_cmnyquantity = $('.cpmnny_pln .pricebasedquantity').val();
            var selected_agncyquantity = $('.agncy_plnn .pricebasedquantity').val();
            
           // $('.busns_price').html(monthly_plans_price.business_price[1].amount);
            $('.busns_plan').data('id',monthly_plans_price.business_price[1].plan_id);

          //  $('.cmny_price').html(monthly_plans_price.company_price[1].amount);
            $('.cmny_plan').data('id',monthly_plans_price.company_price[1].plan_id);

           // $('.agenc_price').html(monthly_plans_price.agency_price[2].amount);
            $('.agenc_plan').data('id',monthly_plans_price.agency_price[2].plan_id);

            /** business plan dropdown **/
            var bunss_pln = '';
            $.each(monthly_plans_price.business_price, function (k, v) {
				bunss_pln += '<option value="' + v.quantity + '" data-amount="' + v.amount + '">' + v.quantity + '<span>&nbsp;Dedicated ';
				bunss_pln += (v.quantity == 1) ? 'Designer' : 'Designers';
				bunss_pln += '</span></option>';
                //bunss_pln += '<option value="' + v.quantity + '" data-amount="' + v.amount + '">' + v.quantity + '<span>&nbsp;Dedicated Designers</span></option>';
            });
            $('.bunss_pln .pricebasedquantity').html(bunss_pln);

            /** company plan dropdown **/
            var cpmnny_pln = '';
            $.each(monthly_plans_price.company_price, function (k, v) {
				cpmnny_pln += '<option value="' + v.quantity + '" data-amount="' + v.amount + '">' + v.quantity + '<span>&nbsp;Dedicated ';
				cpmnny_pln += (v.quantity == 1) ? 'Designer' : 'Designers';
				cpmnny_pln += '</span></option>';
                //cpmnny_pln += '<option value="' + v.quantity + '" data-amount="' + v.amount + '">' + v.quantity + '<span>&nbsp;Dedicated Designers</span></option>';
            });
            $('.cpmnny_pln .pricebasedquantity').html(cpmnny_pln);

            /** agency plan dropdown **/
            var agncy_plnn = '';
            $.each(monthly_plans_price.agency_price, function (k, v) {
                agncy_plnn += '<option value="' + v.quantity + '" data-amount="' + v.amount + '">' + v.quantity + '<span>&nbsp;Dedicated Designers</span></option>';
            });
            $('.agncy_plnn .pricebasedquantity').html(agncy_plnn);
            
            /**set price and quantity **/
            $(".bunss_pln .pricebasedquantity").val(selected_bussquantity).find("option[value=" + selected_bussquantity +"]").attr('selected', true);
            var busns_price = $(".bunss_pln .pricebasedquantity option:selected").attr('data-amount');
            $(".busns_price").html(busns_price);
            
            $(".cpmnny_pln .pricebasedquantity").val(selected_cmnyquantity).find("option[value=" + selected_cmnyquantity +"]").attr('selected', true);
            var cmny_price = $(".cpmnny_pln .pricebasedquantity option:selected").attr('data-amount');
            $(".cmny_price").html(cmny_price);
            
            $(".agncy_plnn .pricebasedquantity").val(selected_agncyquantity).find("option[value=" + selected_agncyquantity +"]").attr('selected', true);
            var agenc_price = $(".agncy_plnn .pricebasedquantity option:selected").attr('data-amount');
            $(".agenc_price").html(agenc_price);

});

jQuery('.pricebasedquantity').on('change', function () {
    var final_SubPrice = $('option:selected', this).attr('data-amount');
    $(this).closest('.price-card-header').find('.chngg_price').html(final_SubPrice);
    jQuery('.btn-plan_pricing').data('value', this.value);
});

/** slide toggle **/
$("button.menu-toggle").click(function(){
    $(".mobile-class").slideToggle();
});

$(document).on('click', '.btn-plan_pricing', function () {
    var planname = $(this).data("id");
    var value = $(this).closest('.price-card-header').find('.pricebasedquantity option:selected').val();
    if (value == 0) {
        var url = '<?php echo base_url(); ?>signup?pln=' + planname;
    } else {
        var url = '<?php echo base_url(); ?>signup?pln=' + planname + '&val=' + value;
    }
    window.location.href = url;
});
</script>