<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="index,follow"/>
    <?php
    $metatitle = isset($data[0]['meta_title'])?$data[0]['meta_title']:$data[0]['title'];
    $metadesc = isset($data[0]['meta_description'])?$data[0]['meta_description']:'';
    $metakeywords= isset($data[0]['meta_keywords'])?$data[0]['meta_keywords']:'';
    $pageUrl = $page ? $page : '';
    $myuri = $_SERVER['REQUEST_URI'];
    ?>
    <title><?php echo $metatitle ?></title>
    <meta name="description" content="<?php echo $metadesc ?>">
    <meta name="keywords" content="<?php echo $metakeywords ?>">
    <?php
    $myuri = $_SERVER['REQUEST_URI'];
    if ($myuri == "/aboutus") {
        ?>
        <meta property="og:title" content="Graphics Zoo: On Demand Graphic Design Services"/>
        <meta property="og:description" content="How do you find a graphic designer or outsource graphic design company to make your business impressive? Visit Graphics Zoo for on-demand design and marketing graphic design services at affordable prices."/>
        <meta property="og:site_name" content="Graphicszoo.com" />

        <meta property="twitter:card" content="summary"/>
        <meta property="twitter:title" content="Graphics Zoo: On Demand Graphic Design Services"/>
        <meta property="twitter:description" content="How do you find a graphic designer or outsource graphic design company to make your business impressive? Visit Graphics Zoo for on-demand design and marketing graphic design services at affordable prices."/>
        <meta property="twitter:URL" content="@add twitter handle">

    <?php }if ($myuri == "/portfolio") { ?>
        <meta property="og:title" content="Graphics Zoo: Logo, Artwork, Print, Website & T-shirt Graphic Design Company"/>
        <meta property="og:description" content="Hire freelance logo designers and t-shirt graphic designers to create an impressive business logo and designs for t-shirts. Our logo and t-shirt artwork designs match your style and build a strong impact on the industry. Take a look at our designs that we have delivered to our clients."/>
        <meta property="og:site_name" content="Graphicszoo.com" />

        <meta property="twitter:card" content="summary"/>
        <meta property="twitter:title" content="Graphics Zoo: Logo, Artwork, Print, Website & T-shirt Graphic Design Company"/>
        <meta property="twitter:description" content="Hire freelance logo designers and t-shirt graphic designers to create an impressive business logo and designs for t-shirts. Our logo and t-shirt artwork designs match your style and build a strong impact on the industry. Take a look at our designs that we have delivered to our clients."/>
        <meta property="twitter:URL" content="@add twitter handle">
    <?php }if ($myuri == "/pricing") { ?>
        <meta property="og:title" content="Graphics Zoo Offers Website Design Subscription & Digital Graphic Design Tools" />
        <meta property="og:description" content="Are you on a hunt for digital graphic design tools or website design subscription services? Get in touch with Graphics Zoo. We offer cost-effective monthly packages for digital graphic design."/>
        <meta property="og:site_name" content="Graphicszoo.com" />

        <meta property="twitter:card" content="summary"/>
        <meta property="twitter:title" content="Graphics Zoo Offers Website Design Subscription & Digital Graphic Design Tools" />
        <meta property="twitter:description" content="Are you on a hunt for digital graphic design tools or website design subscription services? Get in touch with Graphics Zoo. We offer cost-effective monthly packages for digital graphic design." />
        <meta property="twitter:URL" content="@add twitter handle">
    <?php }if ($myuri == "/") { ?>
        <meta property="og:title" content="Graphics Zoo Offers Unlimited Graphic Design Services & Packages"/>
        <meta property="og:description" content="Are you on a lookout for a graphic design company ? Graphics Zoo gives unlimited graphic design services packages for your business. Our expert team provides professional graphic design services and helps you connect with your target audience."/>
        <meta property="og:site_name" content="Graphicszoo.com" />

        <meta property="twitter:card" content="summary"/>
        <meta property="twitter:title" content="Graphics Zoo Offers Unlimited Graphic Design Services & Packages"/>
        <meta property="twitter:description" content="Are you on a lookout for a graphic design company ? Graphics Zoo gives unlimited graphic design services packages for your business. Our expert team provides professional graphic design services and helps you connect with your target audience."/>
        <meta property="twitter:URL" content="@add twitter handle">
    <?php } ?>


    <?php
    $myuri = $_SERVER['REQUEST_URI'];
    if ($myuri == "/") {
        ?>
        <link rel="canonical" href="https://www.graphicszoo.com/" />
    <?php }if ($myuri == "/aboutus") { ?>
        <link rel="canonical" href="https://www.graphicszoo.com/aboutus" />
    <?php }if ($myuri == "/portfolio") { ?>
        <link rel="canonical" href="https://www.graphicszoo.com/portfolio" />
    <?php }if ($myuri == "/pricing") { ?>
        <link rel="canonical" href="https://www.graphicszoo.com/pricing" />
    <?php }if ($myuri == "/blog") { ?>
        <link rel="canonical" href="https://www.graphicszoo.com/blog" />
    <?php }if ($myuri == "/faq") { ?>
        <link rel="canonical" href="https://www.graphicszoo.com/faq" />
    <?php }if ($myuri == "/support") { ?>
        <link rel="canonical" href="https://www.graphicszoo.com/support" />
    <?php }if ($myuri == "/terms-and-conditions") { ?>
        <link rel="canonical" href="https://www.graphicszoo.com/terms-and-conditions" />
    <?php }if ($myuri == "/privacy-policy") { ?>
        <link rel="canonical" href="https://www.graphicszoo.com/privacy-policy" />
    <?php } ?>

    <!-- Bootstrap CSS -->
    <link rel="shortcut icon canonical" type="image/x-icon" href="<?php echo FAV_ICON_PATH; ?>">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" rel="stylesheet nofollow canonical">
    <link rel="stylesheet nofollow canonical" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.css">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.theme.default.min.css">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/jquery.fancybox.css"/>
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/bootstrap.min.css">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/font-awesome.min.css">
    <link rel="stylesheet canonical" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'front_end/Updated_Design/css/style.css');?>">
    <link rel="stylesheet canonical" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'front_end/Updated_Design/css/custom.css');?>">
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/jquery.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.js"></script>
    <script rel="nofollow" src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.5/waypoints.min.js"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-57XFPLD');</script>
    <!-- End Google Tag Manager -->
<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s)
    {
        if (f.fbq)
            return;
        n = f.fbq = function () {
            n.callMethod ?
            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq)
            f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '157875968111046');
    fbq('track', 'PageView');
</script>
<noscript>
<a href="javascript:void(0)" rel="nofollow"><img alt="facebook" height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=157875968111046&ev=PageView&noscript=1"></a>
</noscript>
   <!-- End Facebook Pixel Code -->

   <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "LocalBusiness",

        "address": {
        "@type": "PostalAddress",
        "streetAddress": "245 Commerce Green Blvd.",
        "addressLocality": "Sugar Land",
        "addressRegion": "TX",
        "postalCode": "77479"

    },

    "name": "Graphics Zoo",
    "telephone": "888-976-2747",
    "email": "<?php echo SUPPORT_EMAIL; ?>",
    "url": "https://www.graphicszoo.com",
    "image": "https://www.graphicszoo.com/public/new_fe_images/logo.png",
    "priceRange": "$$",
    "sameAs" :
    [
    "https://www.facebook.com/graphicszoo1/",
    "https://www.instagram.com/graphics_zoo/"
    ]
}
</script>



</head>



<body 
<?php
$role_id = isset($_SESSION['role']) ? $_SESSION['role'] : "";
if (isset($id)) {
    echo "id=" . $id;
    if ($id == 'single-blog-page') {
        echo " class='blog-list'";
    }
}
?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57XFPLD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header id="header">
  <!--   <div class="top-header-bar text-center">Get Unlimited Graphic Designs with 14 day Money Back Guarantee!
        <a href="<?php //echo base_url(); ?>pricing"> Get Started Now</a></div> -->
        <div class="container">
         <div class="flex-header">
            <div id="logo" class="pull-left">
                <!-- Uncomment below if you prefer to use an image logo -->
                <a href="<?php echo base_url(); ?>" rel="nofollow"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/logo.svg" alt="GraphicsZoo" title="GraphicsZoo" /></a>
            </div>
            <button type="button" class="menu-toggle">
                <span></span>
                <span></span>
                <span></span>                                
            </button>
            <div class="mobile-class">
                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        <li class='<?php
                        if ($titlestatus == "portfolio") {
                            echo "active";
                        }
                        ?>'><a href="<?php echo base_url(); ?>portfolio" rel="canonical">Portfolio</a></li>
                        <li class='<?php
                        if ($titlestatus == "pricing") {
                            echo "active";
                        }
                        ?>'><a href="<?php echo base_url(); ?>pricing" rel="canonical">Pricing</a></li>
                        <li class='<?php
                        if ($titlestatus == "testimonial") {
                            echo "active";
                        }
                        ?>'><a href="<?php echo base_url(); ?>testimonials" rel="canonical">Testimonials</a></li>
                        <li class='<?php
                        if ($titlestatus == "features") {
                            echo "active";
                        }
                        ?>'><a href="<?php echo base_url(); ?>features" rel="canonical">Features</a></li>
                    
                        <li class="resources"><a href="#/">Resources</a>
                           <ul>
                               <li><a href="<?php echo base_url(); ?>blog" rel="canonical">Blog</a></li>
                        
                               <li>
                                   <a href="<?php echo base_url(); ?>aboutus" rel="canonical">About Us</a>
                               </li>
                            
                               <li>
                                   <a href="<?php echo base_url(); ?>support" rel="canonical">Contact Us</a>
                               </li>
                           </ul>
                        </li>
                    </ul>
                </nav>
                <div class="navbar-right clearfix">
                    <ul>
                        <!-- <li><span><i class="fas fa-phone"></i></span><a href="tel:(800) 225-6674">(800) 225-6674</a> </li> -->
                        <?php //if($titlestatus != '' || $role_id == 'customer'){  ?>
                            <?php if ($role_id == 'customer') { ?>
                                <a href="<?php echo base_url(); ?>customer/request/design_request" class="button" >DASHBOARD</a>
                            <?php } elseif ($role_id == 'designer') { ?>
                                <a href="<?php echo base_url(); ?>designer/request" class="button">DASHBOARD</a>
                            <?php } elseif ($role_id == 'admin') { ?>
                                <a href="<?php echo base_url(); ?>admin/dashboard" class="button">DASHBOARD</a>
                            <?php } elseif ($role_id == 'qa') { ?>
                                <a href="<?php echo base_url(); ?>qa/dashboard" class="button">DASHBOARD</a>
                            <?php } elseif ($role_id == 'va') { ?>
                                <a href="<?php echo base_url(); ?>va/dashboard" class="button">DASHBOARD</a>
                            <?php } else { ?>
                                <a rel="canonical" href="<?php echo base_url(); ?>login"  class="log-in">Login</a>
                                <a rel="canonical" href="<?php echo base_url(); ?>signup" class="button mar-space">SIGN UP</a>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
                <!-- <div class="clearfix"></div> -->
            </div>

        </header>