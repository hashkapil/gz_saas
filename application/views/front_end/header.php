<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Home | Graphics Zoo</title>
  <?php echo meta();?>
  <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_css/bootstrap.min.css" rel="stylesheet canonical">
  <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_css/style.css" rel="stylesheet canonical">
  <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_css/slider-vertical.css" rel="stylesheet canonical">
  <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_css/owl.carousel.min.css">

  <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_css/slick.css">
  <link rel="stylesheet canonical" type="text/css" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_css/slick-theme.css">

  <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_css/font-awesome.css">
  <link rel="shortcut icon canonical" type="image/x-icon" href="<?php echo FAV_ICON_PATH;?>">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet canonical">
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_js/jquery-2.2.0.min.js"></script>

<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/js-card.js"></script>
<link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>css/js-card.css">
</head>


<body>

<!--<div class="loadingmain">
    <div class="loading">
        <img src="images/loading.svg" class="img-responsive img" alt="" />
    </div>
</div>-->

<!-- header start -->
<header class="headermain">

<div class="topsubscribemain">
    <div class="container-fluid">
        <div class="row row-no-padding">
            <div class="col-md-12">
                <div class="collapse" id="collapseExample">
                    <div class="subscribeboxmain">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-4">
                                <h3>Sign up and Save 25%</h3>
                                <p>Off your 1st month</p>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="email@example.com" aria-describedby="basic-addon2">
                                    <span class="input-group-addon" id="basic-addon2">
                                        <a href="#">Submit</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="topsubscribeleft">Chat with us. We're available 24/7</div>

                <!-- <div class="subscribebtn">
                    <a href="#collapseExample" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample">subscribe + save</a>
                </div> -->
				
            </div>
        </div>
    </div>
</div>

		<nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="<?php echo base_url(); ?>" rel="nofollow"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/new_fe_images/logo.png" class="img-responsive img" alt="" /></a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav menuleft">
                    <li class="<?php if($current_action == "home"){ echo "active"; } ?>"><a href="<?php echo base_url(); ?>" style="font-size:15px;">Home</a></li>
                    <li class="<?php if($current_action == "portfolio"){ echo "active"; } ?>"><a href="<?php echo base_url(); ?>portfolio" style="font-size:15px;">Portfolio</a></li>
                    <li class="<?php if($current_action == "pricing"){ echo "active"; } ?>"><a href="<?php echo base_url(); ?>pricing" style="font-size:15px;">Pricing</a></li>
                    <li class="<?php if($current_action == "blog"){ echo "active"; } ?>"><a href="<?php echo base_url(); ?>blog" style="font-size:15px;">Blog</a></li>
                    <li class="<?php if($current_action == "aboutus"){ echo "active"; } ?>"><a href="<?php echo base_url(); ?>aboutus" style="font-size:15px;">About Us</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right menuright">
                    <li><a href="<?php echo base_url(); ?>login">Log In</a></li>
                    <li class="button">
                        <!--<a href="javascript:(void:0;)" data-toggle="modal" data-target=".popupsignup">Sign Up</a>-->
                        <a rel="canonical" href="<?php echo base_url(); ?>signup">Sign Up</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

</header>
