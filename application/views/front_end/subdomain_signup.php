﻿<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Sign Up | <?php echo $page_title; ?></title>
    <link rel="shortcut icon canonical" type="image/x-icon" href="<?php echo $custom_favicon; ?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,600i,700,700i,800,800i,900,900i" rel="stylesheet canonical">
    <link rel="stylesheet canonical" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/font-awesome.min.css">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/style.css">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/custom.css">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet canonical">
	<?php $CI =& get_instance();
	$CI->load->library('myfunctions');
	$this->load->view('front_end/variable_css');  ?>    
    <style>
    .epassError.disc-code p.alert.alert-danger {
        background: transparent;
        border: none;
        padding: 0;
        margin: 10px 0;
        color: #e8304d;
    }
    .sing-up-heading {
        padding-bottom: 40px;
        padding-top: 30px;
    }
    div#logo img {
        max-width: 200px;
        max-height: 100px;
    }
    span.CouponSucc_code {
        color: #214a80;
        margin-top: 9px;
        display: block;
    }
    span.CouponErr_code {
        color: #e8304d;
        margin-top: 9px;
        display: block;
    }
    .grey-price{color:#828d99;}
    .subHeading {
        font-size: 18px !important;
        text-transform: capitalize;
        font-weight:600 !important;
    }
    select#pricebasedquantity {
        width: 41px;
        -webkit-appearance: none;
        margin-top: 20px;
    }
    .first-Ul {
        color: #253858;
        font-size: 12px;
        font-weight: 500;
        line-height: 22px;
    }
    div#logo {
        text-align: center;
        margin-bottom: 24px;
    }
    section.sing-up-heading {
        background: none!important; 
    }
    .bill-infoo {

        width: auto; 
        box-shadow: none; 
        margin-left: 0px; 
    }
    .sign-here .both-outer {
        background: #fff;
        padding: 20px;
        border-radius: 12px;
        max-width: 400px;
        box-shadow: 0 0 30px #ccc;
        margin: auto;
    }
    h1 {
        font-size: 40px;
        font-weight: 600;
        margin-bottom: 15px;
        margin-bottom: 50px;
    }
    .subdomain {
        max-width: 100%;
        margin: auto;
        width: 100%;
        background: #fff;
        box-shadow: 0 0 30px #ccc;
        padding: 40px;
        border-radius: 12px;
    }
.similar-prop .modal-dialog {
    max-width: 760px;
    width: 100% !important;
    margin:20px auto !important;
    height: 100%;
}
    .similar-prop .modal-dialog .modal-content {
    width: 100%;
    border-radius: 0;
}
h2.popup_h2 {
    font-size: 16px;
    color: #0d0d0d;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 1px;
}
header.fo-rm-header .cross_popup {
    position: absolute;
    right: 20px;
    top: 13px;
    font-size: 20px;
    opacity: 1;
    cursor: pointer;
    font-weight: 400;
}
header.fo-rm-header {
    padding: 20px;
    box-shadow: 0 2px 5px rgba(152, 167, 176, 0.5);
}
.conditions_text, .privacy_text{
    max-height: 400px;
    overflow-y: auto;
    white-space: normal;
        word-break: break-all;
}
.subdomain_signup {
    display: flex;
}
.subdomain_signup > div {
   padding: 0 15px;
}
.subdomain_signup .parsonial-plan {
    width: 30%;
}
.subdomain_signup .pb-info {
    width: 70%;
}
p.no_subs {
    text-align: center;
    font-weight: bold;
    padding: 10px 0;
}
.parsonial-plan .par-out .monthly-activate {
    margin: 12px 0;
}
.par-out .select-bx-p {
    margin-bottom: 20px;
}
.parsonial-plan .par-out select {
    background-position: 96%;
    padding: 0px 12px;
}
.total-pPrice p:empty, .monthly-activate:empty {
    margin: 0 !important;
    display: none;
}
.parsonial-plan .par-out select:focus {
    outline: none;
}
.par-out .monthly-activate ul {
    margin-top: 0;
}
.monthly-activate:empty + .total-pPrice {
    margin-top: 35px;
}
.monthly-activate ul li {
    padding-left: 0;
}
.subdomain_signup .parsonial-plan {
    padding-top: 10px;
}
.par-out .monthly-activate {
    font-size: 14px;
    color: #727c83;
    text-align: center;
    list-style-type: none;
    line-height: 32px;
}
.monthly-activate ul li {
    padding: 0;
    line-height: 32px;
 }
.card-sec a.create_account_link {
    font-weight: 600;
}
.c-signup button {
    width: 100%;
    margin-top: 10px;
}


.maintain-wrap {
    max-width: 500px;
    margin: 45px auto 0;
    border: 1px solid #ccc;
    border-radius: 8px;
    padding: 40px;
}

.maintain-wrap img {
    /* width: 80px; */
    opacity: 0.8;
}

.maintain-wrap p {
    margin: 0;
    font-size: 18px;
    font-weight: 600;
}

.maint-img {
    /* margin-bottom: 30px; */
    width: 100px;
    height: 100px;
    margin: -91px auto 25px;
    border: 1px solid #ccc;
    padding: 20px;
    border-radius: 50px;
    /* margin-top: -88px; */
    background: #fff;
}

</style>
<?php
    if (!empty($client_script)) {
        /***Start client script before close head tag***/
        foreach ($client_script as $k => $s_vl) {
            if (($s_vl['show_only_specific_page'] == "signup" || $s_vl['show_only_specific_page'] == "all_pages") && $s_vl["script_position"] == "before_head") {
                if (strpos(base64_decode($s_vl["tracking_script"]), '</script>') !== false) {
                    echo base64_decode($s_vl["tracking_script"]);
                } else {
                    ?>
                    <script>
                    <?php echo base64_decode($s_vl["tracking_script"]); ?>
                    </script>
                    <?php
                }
            }
        }
        /***End client script before close body tag***/
    }
?>
</head>
<body>

    <section class="sing-up-heading ">
        <div class="container">
            <div class="subdomain">
            <div id="logo" class="sign-logo pull-center">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo $custom_logo; ?>" alt="<?php echo $page_title; ?>" title="<?php echo $page_title; ?>" /></a>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h1 class="text-center">Sign up</h1>

                </div>
            </div>
            <?php if(empty($subscription_list)){ ?>
                <div class='col-md-12'><div class='top_bar_signup maintain-wrap text-center'><div class="maint-img">



                    <img src='<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/customer/web-maintenance.svg' alt='web-maintenance'></div> <p>This page is under maintenance. Please contact admin/support for help.</p></div></div>
            <?php }else{ ?>
            <div class="row sign-here">
                <div class="">
                    <div class="">
                        <?php if ($this->session->flashdata('message_error') != '') { ?>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                                <strong><?php echo $this->session->flashdata('message_error'); ?></strong>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('message_success') != '') { ?>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                                <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                            </div>
                        <?php } ?>
                        <div class="subdomain_signup">
                           
                            <div class="parsonial-plan">
                                <div class="par-out">
                                     <?php if(!empty($subscription_list)){ ?>
                                    <div class="select-bx-p">
                                        <div class="sub_custom-select" >
                                            <select  id="priceselector" >
                                                <?php
                                                
                                                    foreach ($subscription_list as $subscription_k => $subscription_v) {
                                                        echo '<option value="' . $subscription_v['plan_id'] . '" data-quantity="' . $subscription_v['global_inprogress_request'] . '" data-amount="' . $subscription_v['plan_price'] . '">' . $subscription_v['plan_name'] . '</option>';
                                                    }
                                                
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="monthly-activate"></div>

                                    <div class="total-pPrice">
                                        <p class="plan-price">
                                            Plan Price:
                                            <span id="change_prc" class="box item1">$
                                                <span></span>
                                            </span>
                                        </p>
                                        <p class="plan-price tax_prc_for_texas"></p>

                                        <hr>
                                        <div class="g-total">
                                            <h3>Grand Total <span id="grand_total">$<span></span></span></h3>
                                        </div>
                                        <span class="coupon-des-newsignup"></span>
                                    </div>
                                    <?php }else{
                                        echo "<p class='no_subs'>No Subscription</p>";
                                    } ?>
                                </div>
                            </div>
                             
                            <div class="pb-info">
                                <form action="" method="post" role="form" class="contactForm" id="subdomainsignup_frm">
                                    <input type="hidden" value=""  name="plan_name" id="plan_name">
                                    <input type="hidden" value="1"  name="in_progress_request" id="in_progress_request">
                                    <input type="hidden" id="selected_price" name="selected_price" value=""/>
                                    <input type="hidden" id="final_plan_price" name="final_plan_price" value=""/>
                                    <input type="hidden" id="plan_type_name" name="plan_type_name" value=""/>
                                    <input type="hidden" id="couponinserted_ornot" class="couponinserted_ornot" name="couponinserted_ornot" value=""/>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text"  name="first_name"  id="fname" class="form-control" data-rule="minlen:4" data-msg="Please enter First Name" required="" placeholder="First Name *">
                                                </div>
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" name="last_name" class="form-control" id="lname" placeholder="Last Name *"  data-msg="Please enter Last Name" required="">
                                                </div>
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" pattern="\d*" name="phone_number" class="form-control" id="phone_number" placeholder="Phone Number *"  maxlength="10" data-msg="Please enter Phone Number" required="">
                                                </div>
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group"> 
                                                    <input type="email"   name="email" class="form-control" id="email" placeholder="Email *" data-rule="email" data-msg="Please enter a valid email" required="">
                                                </div>
                                                <div class="validation"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="password" name="password" class="form-control" placeholder="Password *" data-rule="minlen:4" data-msg="Please enter the password" required="" >
                                                </div>
                                            </div>
                                        </div>
                                    <?php if($online_payment != 0){ ?>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" pattern="\d*" class="form-control" name="card_number" placeholder="Card Number *" required="" maxlength="16">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" id="expir_date" name="expir_date" class="form-control" placeholder="MM  /  YY *" onkeyup="dateFormat(this.value);" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 cvv">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" pattern="\d*" name="cvc" class="form-control" placeholder="CVV *" maxlength="4" required="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control disc-code" name="Discount" placeholder="Discount Code"  data-msg="Please enter a valid email">
                                            </div>
                                            <div class="ErrorMsg epassErrorSuccess disc-code">
                                            </div>
                                        </div>

                                    </div>
                                    <?php } ?>
                                    </div>
                                    <?php 
                                    if($policy_array['show_term_privacy'] == 1 && ($policy_array['term_conditions'] != "" || $policy_array['privacy_policy'] != "")){ 
                                        if(strip_tags($policy_array['term_conditions']) != ""){
                                                $termtext = '<a data-toggle="modal" data-target="#termconditions">Terms</a>';
                                        }else{
                                            $termtext = 'Terms';
                                        }
                                        if(strip_tags($policy_array['privacy_policy']) != ""){
                                            $policytext = '<a data-toggle="modal" data-target="#privacy_policy">Data Use Policy</a>';
                                        }else{
                                            $policytext = "Data Use Policy";
                                        }
                                    ?>
                                    <div class="" id="chk-box-pr">
                                        <div class="form-group">
                                            <div class="form-radion2">
                                                <label class=""> 
                                                    <p class="terms-txt">
                                                        By clicking the below button, you agree to our <?php echo $termtext; ?> and that you have read our <?php echo $policytext; ?>, including our Cookie Use.
                                                    </p>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                  <?php }
                                  if(!empty($subscription_list)){?>
                                    <div class="text-center c-signup">
                                        <button type="submit" name="submit" id="submit" class="red-theme-btn">Register</button> 
                                    </div>
                                  <?php } ?>
                                    <div class="card-sec row">
                                        <?php if($online_payment != 0){ ?>
                                         <div class="col-lg-6  col-12 text-left">
                                             <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/stripe-lock.png" class="img-fluid stripe-lock" alt="stripe-lock">
                                            </div>
                                         <?php } ?>
                                        <div class="col-lg-6  col-12 text-center">
                                            Already have an account? <a href="<?php echo base_url(); ?>" class="create_account_link">Login</a>
                                        </div>
                                       
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            <?php } ?>
          </div>
      </div>
      </div>
  </section>
  <div class="modal fade similar-prop" id="termconditions" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
  <div class="modal-dialog custom-modal" role="document">
    <div class="modal-content">
      <header class="fo-rm-header">
        <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
        <h2 class="popup_h2 del-txt" id="myModalLabel">Terms and Conditions</h2>
      </header>
      <div class="modal-body model-tour term_content">
          <div class="conditions_text">
              <?php echo $policy_array['term_conditions']; ?>
          </div>
    </div>
  </div>
</div>
</div>
<div class="modal fade similar-prop" id="privacy_policy" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
  <div class="modal-dialog custom-modal" role="document">
    <div class="modal-content">
      <header class="fo-rm-header">
        <div class="cross_popup" data-dismiss="modal" aria-label="Close">×</div>
        <h2 class="popup_h2 del-txt" id="myModalLabel">Privacy Policy</h2>
      </header>
      <div class="modal-body model-tour privacy_content">
          <div class="privacy_text">
              <?php echo $policy_array['privacy_policy']; ?>
          </div>
    </div>
  </div>
</div>
</div>
<?php
if (!empty($client_script)) {
    /***Start client script before close head tag** */
    foreach ($client_script as $k => $s_vl) {
        if (($s_vl['show_only_specific_page'] == "signup" || $s_vl['show_only_specific_page'] == "all_pages") && $s_vl["script_position"] == "before_body") {
            if (strpos(base64_decode($s_vl["tracking_script"]), '</script>') !== false) {
                echo base64_decode($s_vl["tracking_script"]);
            } else {
                ?>
                <script>
            <?php echo base64_decode($s_vl["tracking_script"]); ?>
                </script>
                <?php
            }
        }
    }
    /***End client script before close body tag***/
}
?>
</body>

<script src="<?php echo FS_PATH_PUBLIC_ASSETS ?>js/customer/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!--<script async="" src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'js/front_end.js'); ?>"></script>-->
<script> 
var BASE_URL = "<?php echo base_url(); ?>";
var assetspath = "<?php echo FS_PATH_PUBLIC_ASSETS ?>";
var sales_tax = "";
$plan_price = '';
$from = 'sub';
$discount_amount = 0;
$tax_amount = 0;
$tax_value = 0;
$discount_notes = '';

if($('.monthly-activate').length > 0){ 
    console.log("just testing");
    var setSelected = jQuery("#priceselector").val();
    var setAmount = jQuery("#priceselector").find(':selected').data('amount');
    Setselectedvalue(setSelected,setAmount); 
}

function Setselectedvalue(selectedval,setAmount) {
    console.log(selectedval,setAmount);
        $plan_price = setAmount;
        var user_id = "<?php echo $user_id; ?>";
        jQuery('#plan_name').val(selectedval);
        jQuery('#selected_price').val(setAmount);
        jQuery('#final_plan_price').val(setAmount);
        jQuery('#change_prc span').html(setAmount);
        jQuery('#grand_total span').html(setAmount);
        jQuery('.discount-amont-sec').css('display','none');
        jQuery('.disc-code').val('');  
        jQuery('.ErrorMsg').css('display','none');
        jQuery('.coupon-des-newsignup').css('display','none');
        $.ajax({
            type: 'POST',
            url: BASE_URL+'welcome/getsubsbaseduser_byplanid',
            data: {planid: selectedval,user_id:user_id},
            dataType: 'json',
            success: function (data) {
                
                if(data[0]){
                    var features = data[0].features;
                    var feature_array = features.split("_");
                    features_html = ""
                    if(feature_array != ""){
                        console.log("feature_array",feature_array);
                    features_html = "<ul>";
                    $.each(feature_array,function(k,val){
                        features_html += "<li>"+val+"</li>";
                    });
                    features_html += "</ul>";
                    }
                    jQuery('.monthly-activate').html(features_html);
                    jQuery('#plan_type_name').val(data[0].plan_type_name);
                    jQuery('#couponinserted_ornot').val(data[0].apply_coupon);
                    jQuery('#in_progress_request').val(data[0].global_inprogress_request);
                    
                }
            }
        });
    var discount_empty =  jQuery('.disc-code').val();
        if(discount_empty == ''){
           $('.ErrorMsg').css('display','none');
            $discount_amount = 0;
            $discount_notes = '';
           return; 
        }
}
jQuery('#priceselector').on('change', function (e) {
    var getSelectedPrice = $(this).val();
    var getAmount = $(this).find('option:selected').attr("data-amount");
  Setselectedvalue(getSelectedPrice,getAmount);
});
function dateFormat(val) {
    if (val.length == 2) {
        $('#expir_date').val($('#expir_date').val() + '/');
    }
}
$(document).on('blur','.disc-code',function(e){
            e.preventDefault();
            var dicountCode = $('.disc-code').val();
            var plan_type_name = $("#plan_type_name").val();
            var apply_couponor_not = $("#couponinserted_ornot").val();
            var setSelected = $("#priceselector").find(':selected').data('amount');;
        $("span.CouponSucc_code, span.CouponErr_code").remove();
        if(dicountCode == ''){
            $('.ErrorMsg').css('display','none');
            $discount_amount = 0;
            $discount_notes = '';
            showPriceSectionHTML();
           return;
       }
       else{
        $('.ErrorMsg').css('display','block');
    }
    $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponSucc_code"><img src="'+assetspath+'img/ajax-loader.gif" alt="loading"/></span>');
    delay(function(){
        $.ajax({
            type: 'POST',
            url: BASE_URL+'welcome/CheckCouponValidation',
            data: {dicountCode: dicountCode,apply_couponor_not:apply_couponor_not},
            dataType: 'json',
            success: function (data) {
                 // console.log(data);
                  $('.ErrorMsg').css('display','block');
                  $('.couponinserted_ornot').val(data.inertcoupon);
                  if (data.status == 1) {
                      if(data.return_data.valid == '1'){
                        if(data.return_data.amount_off !== null){
                            var str = data.return_data.amount_off;
                            var resStr = str/100;
                            $discount_amount = resStr;
                            //var finalAmount = setSelected - resStr;                              
                        } else{
                            var TotalPercent = data.return_data.percent_off;
                            var calcPrice  =  setSelected * (TotalPercent / 100 );
                            $discount_amount = calcPrice;
                       }
                       $('.CouponErr_code').css({"display": "none"});  
                       $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponSucc_code"><i class="fas fa-check-circle"></i> Coupon Applied</span>');
                    var discount_notes = '';                  
                    if(data.return_data.duration == 'forever'){
                        discount_notes = '<p>Forever</p>';
                    }else if(data.return_data.duration == 'once'){
                        discount_notes = '<p>One Time Only</p>';
                   } else{
                        discount_notes = '<p>First ' + data.return_data.duration_in_months + ' Months Only</p>';
                    }
                   $discount_notes = discount_notes;
               }else{
                    $('.CouponSucc_code').css({"display": "none"});
                    $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponErr_code"><i class="fas fa-times-circle"></i>This coupon is not valid</span>');
                    $discount_amount = 0;
                    $discount_notes = '';
               }
               showPriceSectionHTML();
           }
           else{
                showPriceSectionHTML();
                $('.CouponSucc_code').css({"display": "none"});
                $('.ErrorMsg.epassErrorSuccess.disc-code').html('<span class="CouponErr_code"><i class="fas fa-times-circle"></i> ' + data.message + '</span>');
                $discount_amount = 0;
                $discount_notes = '';
           }
       }
   });  
    }, 1000 );
    

});
 var delay = (function(){
          var timer = 0;
          return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
})();
function showPriceSectionHTML(){
    console.log("testing...");
    $plan_price = parseFloat($("#priceselector").find(':selected').data('amount'));
    $discount_amount = parseFloat($discount_amount);
    console.log('plan_price',$plan_price);
//    console.log('tax_amount',$tax_amount);
    console.log('discount_amount',$discount_amount);
    var total_price = ($plan_price - $discount_amount);
    if(total_price % 1 !== 0){
        total_price = total_price.toFixed(2);
    }
    var htmll = '<p class="plan-price">Plan Price:<span id="change_prc" class="box item1">$<span>'+$plan_price+'</span></span></p>';
    if(parseFloat($discount_amount) > 0){
        htmll += '<p class="plan-price discount-amont-sec">Discount: <span class="box item12"><span>- $'+$discount_amount.toFixed(2)+'</span></span></p>';	
    }
    htmll += '<hr>';
    htmll += '<div class="g-total"><h3>Grand Total <span id="grand_total"><span>';
    if(parseFloat($discount_amount) > 0){
        var strikeamount = parseFloat($plan_price);
            htmll += '<strike>$';
            htmll += Math.round(strikeamount * 100) / 100;
            htmll += '</strike> $'+total_price;
    } else{
            htmll += '$'+total_price;
    }
    htmll += '</span></span></h3></div>';
    if(parseFloat($discount_amount).toFixed(2) > 0){
            htmll += '<span class="coupon-des-newsignup">'+$discount_notes+'</span>';
    }
    $('.total-pPrice').html(htmll);
    $('#selected_price').val(total_price);
}
</script>
</html>
