<!--==========================
Hero Section
============================-->
<section id="hero_section" class="wow fadeIn">
	<div class="hero-container">
		<div class="container">
			<div class="row">
				<div class="col-md-12 mt-5 pt-5">
					<div class="section-title text-center">
						<h2>News</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container blog_posts">
		<div class="row">
			<div class="col-md-4">
				<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/news1.jpg" >
			</div>
			<div class="col-md-8">
				<h2>Can Camdem startup hire 100 city students by 2019?</h2>
				<div class="blog_attr mb-4">
					<span><i class="fa fa-user"></i> Admin</span>
					<span><i class="fa fa-list"></i> News</span>
					<span><i class="fa fa-comment"></i> Leave a comment</span>
				</div>
				<p>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"</p>
			</div>
		</div>
	</div>
</section><!-- #hero -->
<section class="blog_posts">
	<div class="container">
		<div class="section_separator"></div>
		<div class="row">
			<div class="col-md-8 order2">
				<h2>10 Philly startups to Watch in 2018</h2>
				<div class="blog_attr mb-4">
					<span><i class="fa fa-user"></i> Admin</span>
					<span><i class="fa fa-list"></i> News</span>
					<span><i class="fa fa-comment"></i> Leave a comment</span>
				</div>
				<p>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"</p>
			</div>
			<div class="col-md-4">
				<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/news2.jpg" >
			</div>
		</div>
		<div class="section_separator"></div>
		<div class="row">
			<div class="col-md-4">
				<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/news3.jpg" >
			</div>
			<div class="col-md-8">
				<h2>Camdem start-up looks to hire 100 local students by end to year</h2>
				<div class="blog_attr mb-4">
					<span><i class="fa fa-user"></i> Admin</span>
					<span><i class="fa fa-list"></i> News</span>
					<span><i class="fa fa-comment"></i> Leave a comment</span>
				</div>
				<p>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"</p>
			</div>
		</div>
		<div class="section_separator"></div>
		<div class="row">
			<div class="col-md-8 order2">
				<h2>First Tech Startup to join Camden NJ</h2>
				<div class="blog_attr mb-4">
					<span><i class="fa fa-user"></i> Admin</span>
					<span><i class="fa fa-list"></i> News</span>
					<span><i class="fa fa-comment"></i> Leave a comment</span>
				</div>
				<p>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"</p>
			</div>
			<div class="col-md-4">
				<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/news4.jpg" >
			</div>
		</div>
	</div>
</section>
<section id="pagination_section" class="mt-5">
	<ul class="pagination pagination-lg">
		<li class="first"><a href="#">Prev</a></li>
		<li><a href="#">1</a></li>
		<li class="active"><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li><a href="#">4</a></li>
		<li class="last"><a href="#">Next</a></li>
	</ul>
</section>