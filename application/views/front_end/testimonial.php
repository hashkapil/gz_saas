<style>
    section.publection-sec{display: none}
</style>
<section class="test-monial">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1>What Our Clients Are Saying</h1>
                <div class="rating">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/star-yellow.png" alt="star-yellow">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/star-yellow.png" alt="star-yellow">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/star-yellow.png" alt="star-yellow">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/star-yellow.png" alt="star-yellow">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/star-half.png" alt="star-half">
                </div>
                <p class="avrg">4.9/5 based on customer reviews</p>
            </div>

        </div>

        <div class="row">
            <div class="col-md-6">
                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/main-comment1.png" alt="main-comment1" class="img-fluid center-block">
            </div>
            <div class="col-md-6">
                <div class="main-comment">
                    <p>Graphics Zoo has been amazing to work with for my graphic design needs. They are fast and the designs are always on point. They do a great job following directions I provide, and the design queue makes it easy for me to prioritize all my design jobs needed. Their service has become an invaluable resource for my company!</p>
                    <h3>Patrick Black </h3>
                    <h6>President/CEO, Perfect Imprints</h6>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="client-review">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>SB</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <p class="cl-name">Shea Bramer</p>
                            <p class="time-ago">Marketing Coordinator, Western Rockies</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>We are a small marketing team for a large financial company and new growth in the company meant that the graphic design workload was becoming overwhelming for our team. Graphicszoo created an easy way for us to keep up with the workload as our company continues to grow. They pay close attention to our branding, and are very detail oriented (as I can be quite demanding). The work they provide for us is always very high quality and I always get what I'm asking for. Their prices are also very competitive. We highly suggest this company!</p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>SD</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <p class="cl-name">Skylar Domine </p>
                            <p class="time-ago">Goat Grass Co</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>I've been through dozens of design teams and none of which ever checked off all the boxes- price, turnaround time, consistency, and accuracy.  I'm actually 'wowed' by this company and will have my loyalty for years to come from my circle. Highly recommend to anyone looking for design work. If you can visualize it then they can create it!</p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>NL</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <p class="cl-name">Nathan Leathers</p>
                            <p class="time-ago"> Owner - Green Gaurd, Printing and Apparel</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>Had another competitor prior to this, and within 3 days Graphics Zoo has far exceeded any level of service I had with the previous competitor in 2 years.  They are very responsive and so far fast and solid production of content!</p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>TF</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <p class="cl-name">Thomas Elliott Fite</p>
                            <p class="time-ago"> Focal Shift</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>Pretty impressed! Been with them for several months now and we've finally found our work flow with them! Great job GZ and keep up the great work!</p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>SL</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <p class="cl-name">Scott Levesque</p>
                            <p class="time-ago"> Be Free Clothing Company</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>Great designs, quick turnaround and awesome to work with. Thank you.</p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>TG</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <p class="cl-name">Timothy Galon</p>
                            <p class="time-ago">The LunchBox Digital Solutions</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>I am so glad I found Graphics Zoo. Prior to signing up with you guys, I was struggling to find a decent
                            designer and had to re-teach my business model each time I found someone. With you guys, 
                            I feel like I have my own personal in-house 
                            design team and no longer have to re-invent the wheel every single time I need a new design done. </p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>SS</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star grey-star"></i></div>
                            <p class="cl-name">Steve Smith</p>
                            <p class="time-ago">T-Print Services </p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>My team at Graphics Zoo is amazing! Running a t-shirt business is not so easy,
                            but my designer is so good at giving me new creative designs every day.
                            I have seen a huge jump in numbers, 
                            because now I am able to produce new t-shirt designs daily! </p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>MG</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star grey-star"></i></div>
                            <p class="cl-name">May Goodwin</p>
                            <p class="time-ago">Goodwin Agency</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>Graphics Zoo is the best! I highly recommend this company to all marketing agencies. 
                            My team, which includes my account manager and designer, is so good at turning around all my 
                            various design requests in time and with super high quality options.  I love this team!</p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>GC</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <p class="cl-name">Greyson Chance</p>
                            <p class="time-ago">Honestbee</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>Graphics Zoo is the way to go.. enough said!</p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>SH</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star grey-star"></i></div>
                            <p class="cl-name">Susan Haley</p>
                            <p class="time-ago">JetPay</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>From start to finish, Graphics Zoo has helped me tremendously with my start-up.
                            I say start to finish, because I have designed my website template with them, all my sub-pages, all my product designs, and now every day we update and create new ads! 
                            I can't believe just 1 company has so much to offer. Best deal ever. </p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>JW</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <p class="cl-name">Joseph W</p>
                            <p class="time-ago">3 Hours Ago</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>Graphics Zoo is an incredible service, they have a dedicated team that will work tirelessly
                            to make sure you are please with the designs! Their diverse team of designers allow for 
                            multiple styles, and unique artwork that is limited with a traditional graphic designer.
                            With Graphic Zoo, we are able to have all of our designs completed at our leisure,
                            without carrying the salary of a full time or freelance designer. </p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>JM</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
                            <p class="cl-name">Jody Maberry </p>
                            <p class="time-ago">3 Hours Ago</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>Graphics Zoo has been helpful, responsive, and easy to work with.</p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>DL</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star grey-star"></i></div>
                            <p class="cl-name">Dawn McManamy Lemons </p>
                            <p class="time-ago">Impact Branding Unlimited</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>We got off to a rocky start - but they worked with my to get the right designer for what I needed. 
                            Had a great experience after that. For the price, a great service.</p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>AB</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star grey-star"></i></div>
                            <p class="cl-name">April Barnard</p>
                            <p class="time-ago">Marketing With April </p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p>I have always received good service from this company. For the price, you receive one graphic a day. 
                            They do offer you multi-reversion which makes it nice, when you have a difficult client.</p>

                    </div>
                </div>
                <div class="lines">
                    <div class="left-sec">
                        <div class="left-part">
                            <span>EL</span>
                        </div>
                        <div class="other-part">
                            <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star grey-star"></i></div>
                            <p class="cl-name">Erric Liddic</p>
                            <p class="time-ago">On The Go Prints</p>
                        </div>
                    </div>
                    <div class="right-sec">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/before-comment.png" alt="before-comment">
                        <p> We use graphics zoo for some of our small yet often time consuming work like cropping images,
                            basic print layout, and vector converting. It's been a great value for our us! </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="riskFree-sec pricing-risk">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Join more than 500+ customers</h2>
                <h3>Try Graphics Zoo Risk-Free For 14 Days</h3>
                <a rel="canonical" href="<?php echo base_url(); ?>pricing" class="red-theme-btn" rel="nofollow">Get Started Now
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/red-long-arrow.png" class="img-fluid" alt="red-long-arrow"></a>

            </div>
        </div>
    </div>
</section>