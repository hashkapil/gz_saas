<section class="affiliate-section affiliate-banner saas_banner_section">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="left-earn-sec">
					<span class="saas-subhead">Get Started</span>
					<h1>Lorem ipsum dolor sit amet, <span> consectetur </span> adipiscing elit.</h1>
					<p>Sed efficitur odio eget tellus faucibus, vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt lorem vel viverra.
					</p>
					<a class="join_today landing_button" href="/affiliate_signup">Get Started</a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="saas-banner-image">
					<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/landing-banner.jpg" class="">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="affiliate-section landing-info">
<div class="container">
		<div class="row">
			<div class="offset-md-2 col-md-8 text-center">
			<h2>Sed efficitur odio eget tellus</h2>
			<p>vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt</p>
					<div class="abt-landing">
					<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/aboutus_Insight.jpg" class="">
					</div>
			</div>
		</div>
	</div>	
</section>

<section class="affiliate-section why-affiliate landing-feature">
	<div class="container">
		<h2 class="affiliate-sub-heading">Our Features </h2>
		<div class="row">
			<div class="col-md-4">
				<div class="affil-item">
					<div class="ftr-icons">
						<i class="far fa-chart-bar"></i>
					</div>
					<h3>Sed efficitur odio eget</h3>
					<p>vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt</p>
				</div>
			</div>
				<div class="col-md-4">
				<div class="affil-item">
				<div class="ftr-icons">
				<i class="fas fa-draw-polygon"></i>
					</div>
					<h3>Sed efficitur odio eget</h3>
					<p>vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt</p>
				</div>
			</div>
				<div class="col-md-4">
				<div class="affil-item">
				<div class="ftr-icons">
				<i class="fas fa-cogs"></i>
					</div>
					<h3>Sed efficitur odio eget</h3>
					<p>vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt</p>
				</div>
			</div>

		</div>
		<div class="row">		
				<div class="col-md-4">
				<div class="affil-item">
				<div class="ftr-icons">
						<i class="far fa-chart-bar"></i>
					</div>
					<h3>Sed efficitur odio eget</h3>
					<p>vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt</p>
				</div>
			</div>
				<div class="col-md-4">
				<div class="affil-item">
				<div class="ftr-icons">
				<i class="fas fa-cogs"></i>
					</div>
					<h3>Sed efficitur odio eget</h3>
					<p>vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt</p>
				</div>
			</div>
				<div class="col-md-4">
				<div class="affil-item">
				<div class="ftr-icons">
						<i class="far fa-chart-bar"></i>
					</div>
					<h3>Sed efficitur odio eget</h3>
					<p>vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="affiliate-section affiliate-banner saas_banner_section">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="left-earn-sec">
					<span class="saas-subhead">lorem ipsum text</span>
					<h1>Lorem ipsum dolor sit amet, adipiscing elit.</h1>
					<p>Sed efficitur odio eget tellus faucibus, vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt lorem vel viverra.
					</p>
					
				</div>
			</div>
			<div class="col-md-6">
				<div class="saas-banner-image">
					<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/landing-banner.jpg" class="">
				</div>
			</div>
		</div>
	</div>
</section>
<section class="affiliate-section affiliate-banner saas_banner_section flex-dir">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="left-earn-sec">
					<span class="saas-subhead">lorem ipsum text</span>
					<h1>Lorem ipsum dolor sit amet, adipiscing elit.</h1>
					<p>Sed efficitur odio eget tellus faucibus, vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt lorem vel viverra.
					</p>
					
				</div>
			</div>
			<div class="col-md-6">
				<div class="saas-banner-image">
					<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/landing-banner.jpg" class="">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="affiliate-section affiliate-banner saas_banner_section">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="left-earn-sec">
					<span class="saas-subhead">lorem ipsum text</span>
					<h1>Lorem ipsum dolor sit amet, adipiscing elit.</h1>
					<p>Sed efficitur odio eget tellus faucibus, vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt lorem vel viverra.
					</p>
					
				</div>
			</div>
			<div class="col-md-6">
				<div class="saas-banner-image">
					<img alt="earn"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/saas_landing/landing-banner.jpg" class="">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="affiliate-section why-affiliate landing-options">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="affil-item">
					
					<h3>Sed efficitur odio eget</h3>
					<p>vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt</p>
				</div>
			</div>
				<div class="col-md-3">
				<div class="affil-item">
			
					<h3>Sed efficitur odio eget</h3>
					<p>vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="affil-item">
				
					<h3>Sed efficitur odio eget</h3>
					<p>vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt</p>
				</div>
			</div>
				<div class="col-md-3">
				<div class="affil-item">
				
					<h3>Sed efficitur odio eget</h3>
					<p>vel ultrices tortor aliquam. Integer viverra hendrerit commodo.
					Aenean id finibus mauris, in ultrices nisl. Curabitur cursus tincidunt</p>
				</div>
			</div>

		</div>
	
	</div>
</section>

<section class="affiliate-section affiliate-talk">
	<div class="container">
		<h2 class="affiliate-sub-heading">What Our Client Say</h2>
		<div class="row">
			<div class="col-md-4">
				<div class="talk-item">
					<div class="talker-header">				
						<div class="talker-image"><img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/talker-a.png" class=""></div>
						<div class="talker-info">
							<h4>Patrick Mills</h4>
							<span>New York, USA</span>
						</div>
						
					</div>
					<div class="talker-say">
							<p>Graphiczoo hands-down provides the best affiliate program in the industry with a mixture of unlimited offers for the affiliates. I have referred it to a lot of friends and acquaintances and they have been more than just pleased with the graphic designs and services. Earning has never been more fun for me.</p>
						</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="talk-item">
					<div class="talker-header">				
						<div class="talker-image"><img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/talker-b.png" class=""></div>
						<div class="talker-info">
							<h4>Patrick Mills</h4>
							<span>New York, USA</span>
						</div>
						
					</div>
					<div class="talker-say">
							<p>I have been an affiliate for years now and have collaborated with all types of brands. The experience with Graphiczoo was rather different and better in a lot of aspects. I love their team. They’re so friendly, dedicated and hard-working. This is an awesome option for some passive income. Thank you Graphiczoo!</p>
						</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="talk-item">
					<div class="talker-header">				
						<div class="talker-image"><img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/talker-a.png" class=""></div>
						<div class="talker-info">
							<h4>Patrick Mills</h4>
							<span>New York, USA</span>
						</div>
						
					</div>
					<div class="talker-say">
							<p>Responsive staff, cool designs, simple payout options, and great other services. Graphiczoo has it all! My references have been extremely satisfied and hence I would definitely urge all of you to join the impeccable affiliate program of the Graphiczoo!</p>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>