
<!--==========================
Hero Section
============================-->
<section id="hero_section" class="wow fadeIn pb-5">
	<div class="hero-container">
		<div class="container">
			<div class="row career">
			<div class="col-md-12" style="margin-top:7rem;">
				<div class="section-title text-center">
					<h2>Career</h2>
				</div>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
			</div>
			</div>
		</div>
	</div>
</section><!-- #hero -->

<div class="container text-center">
	<div class="row">
		<div class="col-md-12">
			<ul class="openings">
				<li>
					<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/internship.png" height="80" alt="internship">
					<p>Internship Available</p>
				</li>
				<li>
					<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/graphic-icon.png" height="80" alt="graphic-icon">
					<p>Graphic Designer</p>
				</li>
				<li>
					<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/ux-designer.png" height="80" alt="ux-designer">
					<p>UI/UX Designer</p>
				</li>
				<li>
					<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/front-dev.png" height="80" alt="front-dev">
					<p>Front-end Developer</p>
				</li>
				<li>
					<img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/internship.png" height="80" alt="internship">
					<p>Social Media Manager</p>
				</li>
			</ul>
		</div>
	</div>
</div>

<div class="container text-center">
	<div class="row">
		<div class="col-md-12">
			<button class="button big-btn mt-5 mb-5"><b>JOIN NOW</b></button>
			<p style="color: #969696;font-weight: 600; font-size: 16px;">Please send internship inquiries to <a style="color:#e8304d; text-decoration: none;" href="mailto:hello@graphicszoo.com">hello@graphicszoo.com</a></p>
		</div>
		
	</div>
</div>