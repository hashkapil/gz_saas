<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        if ($this->agent->is_browser()) {
            $agent = $this->agent->browser();
        } elseif ($this->agent->is_robot()) {
            $agent = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $agent = $this->agent->mobile();
        } else {
            $agent = 'Unidentified User Agent';
        }
       // echo "hello".$agent;
        if ($agent != "Firefox") {
            ?>
            <link  rel="shortcut icon canonical" type="image/x-icon" href="<?php echo FAV_ICON_PATH; ?>">
            <link rel="preload" href="//use.fontawesome.com/releases/v5.4.1/css/all.css" as="style" onload="this.onload = null;
                        this.rel = 'stylesheet nofollow'">
            <link rel="preload" href="//fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" as="style" onload="this.onload = null;
                        this.rel = 'stylesheet nofollow'">
            <link rel="preload" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.css" as="style" onload="this.onload = null;
                        this.rel = 'stylesheet'">
            <link rel="preload" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.theme.default.min.css" as="style" onload="this.onload = null;
                        this.rel = 'stylesheet'">
            <link rel="preload" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/jquery.fancybox.css" as="style" onload="this.onload = null;
                        this.rel = 'stylesheet'"> 
            <link rel="preload" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/bootstrap.min.css" as="style" onload="this.onload = null;
                        this.rel = 'stylesheet'">
            <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'front_end/Updated_Design/css/style.css'); ?>" rel="stylesheet" type="text/css" media = "all">
            <link rel="preload" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/custom.css" as="style" onload="this.onload = null;
                        this.rel = 'stylesheet'">
        <?php } else { ?> 
            <link  rel="shortcut icon canonical" type="image/x-icon" href="<?php echo FAV_ICON_PATH; ?>">
            <link href="//fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900&font-display=swap"  rel="stylesheet nofollow canonical" type="text/css" >
            <link href="//use.fontawesome.com/releases/v5.4.1/css/all.css"  rel="stylesheet nofollow" type="text/css" >
            <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.css"  rel="stylesheet" type="text/css" >
            <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.theme.default.min.css" rel="stylesheet" type="text/css" >
            <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
            <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/bootstrap.min.css" rel="stylesheet" type="text/css">
            <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/font-awesome.min.css" rel="stylesheet" type="text/css" >
            <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'front_end/Updated_Design/css/style.css'); ?>" rel="stylesheet" type="text/css">
            <link href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'front_end/Updated_Design/css/custom.css'); ?>" rel="stylesheet" type="text/css">
            <?php
        }

        $currenturl = current_url();
        $pagename = explode('/', $currenturl);
        ?>
        <?php if ($currenturl != base_url()) { ?> 
            <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/jquery.min.js"></script>
        <?php } ?> 

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="robots" content="index,follow"/>
        <?php
        $metatitle = "";
        $metadesc = "";
        $metakeywords = "";
        $pageUrl = $page ? $page : '';
//        echo "testttt".$pageUrl;exit;
        $myuri = $_SERVER['REQUEST_URI'];
//        echo "<pre/>";print_R($myuri);
        if ($myuri == "/aboutus") {
            $metatitle = "About Graphics Zoo";
            $metadesc = "Graphics Zoo is the best graphic designing company in the United States. Our primary objective is to make your business impressive. We believe all it takes is a great design to get you to the next level.";
            $metakeywords = "logo and graphic design services, best graphics company, unlimited logo design, graphic design online";
        } else if ($myuri == "/aboutus/") {
            $metatitle = "Graphics Zoo About Us";
            $metadesc = "Graphics Zoo is the best graphic designing company in the US. Our primary objective is to make your business impressive. We believe all it takes is a great design to get you to the next level of acheivements.";
            $metakeywords = "unlimited logo and graphic design services, best graphics company, unlimited logo design, graphic design online services";
        }else if ($myuri == "/portfolio") {
            $metatitle = "Graphics Design Portfolio - Outstanding Design Portfolio";
            $metadesc = "Are you Looking for inspiration for your graphic design portfolio? You're at the right place, check out our favorite portfolios from our expert designers.";
            $metakeywords = "unlimited logo design, logo and graphic design services, creative graphic designing work, online design services";
        } else if ($myuri == "/features") {
            $metatitle = "Features | Graphics Zoo Features";
            $metadesc = "Our main features that make your life easier. Check out some of the amazing functions we have added like Interactive Revisions, Project priority, & Simple Communication.";
            $metakeywords = "online graphic design services, graphic design service provider, graphic design online, monthly graphic design package";
        } else if ($myuri == "/testimonials") {
            $metatitle = "Client Testimonials";
            $metadesc = "Testimonials. What People Are Saying About Our Work. Read testimonials from our fabulous clients about a wide variety of graphic design projects!";
        } else if ($myuri == "/blog") {
            $metatitle = "Best Graphic Design Blogs for Inspiration You've Ever Seen";
            $metadesc = "Are you looking for the best graphic design blogs? We have compiled a list on the website. These blogs will inspire your creativity & teach you new skills.";
        } else if ($myuri == "/faq") {
            $metatitle = "Graphic Design Frequently Asked Questions - Graphics Zoo FAQs";
            $metadesc = "Know how the Graphic Design process works? Read throughout Graphic Design Frequently Asked Questions in our FAQ section.";
        } else if ($myuri == "/privacypolicy") {
            $metatitle = "Graphics Zoo - Privacy Policy | Graphic Design Advertising Company";
            $metadesc = "Our privacy policy has been compiled to better serve those who are concerned with how their Personally Identifiable Information (PII) is being used online.";
        } else if ($myuri == "/support") {
            $metatitle = "On Site Graphic Design Support - Graphic Design Expert Help";
            $metadesc = "Need to get in touch? Our Graphic Design Expert pretty easy to reach to provide On-Site Graphic Design Support. Feel free to email us, reach out on social media.";
        } else if ($myuri == "/login") {
            $metatitle = "Log In Graphics Zoo - Easy Access to your Account";
            $metadesc = "Graphics Zoo uses cookies to create the most effective website experience for our visitors and users. Full details can be found here.";
        } else if ($myuri == "/signup") {
            $metatitle = "Sign up Graphics Zoo - Register a New Account";
            $metadesc = "Sign Up or Register a new account to meet the community of awesome designers and clients.";
        } else if ($myuri == "/pricing") {
            $metatitle = "Graphic Design Pricing - Graphic Design packages - Graphic Design Monthly Packages";
            $metadesc = "Are you looking for graphic design pricing & packages? Graphics Zoo offers you high-quality graphic designs & best deals on Graphic Design Monthly Packages.";
            $metakeywords = "graphic design packages, rates for graphic design services, monthly graphic design package";
        } else if ($myuri == "/termsandconditions") {
            $metatitle = "Our Terms and Conditions | T & C's of Graphics Zoo";
            $metadesc = "Terms of Service Agreement is a legally binding agreement that shall govern the relationship with our users and others which may interact or interface with Graphicszoo.com";
            $metakeywords = "online design services,best graphics company,graphic design packages";
        } else if ($myuri == "/") {
            $metatitle = "Unlimited Graphic Design - Online Graphic Design";
            $metadesc = "Graphics Zoo offers unlimited graphic design starting at $297/per month. Here we provide custom graphic designs, online graphic designs & unlimited revisions.";
            $metakeywords = "monthly graphic design service, unlimited graphic design packages, creative graphic design company, brand design agency, logo graphic design services, social media graphic design, advertising graphic design, marketing graphic design";
        } else if ($pageUrl == "404") {
            $metatitle = "Page Not Found | Graphics Zoo";
            $metadesc = "The page which you are looking for is not available on the server";
        } else {
            $metatitle = "Unlimited Graphic Design - Online Graphic Design";
            $metadesc = "Graphics Zoo offers unlimited graphic design starting at $297/per month. Here we provide custom graphic designs, online graphic designs & unlimited revisions.";
        }
//        echo $metatitle;
        ?>
        <title><?php echo $metatitle ?></title>
        <meta name="description" content="<?php echo $metadesc ?>">
        <meta name="keywords" content="<?php echo $metakeywords ?>">



        <?php
        $myuri = $_SERVER['REQUEST_URI'];
        if ($myuri == "/aboutus") {
            ?>
            <meta property="og:title" content="Graphics Zoo: On Demand Graphic Design Services"/>
            <meta property="og:description" content="How do you find a graphic designer or outsource graphic design company to make your business impressive? Visit Graphics Zoo for on-demand design and marketing graphic design services at affordable prices."/>
            <meta property="og:site_name" content="Graphicszoo.com" />

            <meta property="twitter:card" content="summary"/>
            <meta property="twitter:title" content="Graphics Zoo: On Demand Graphic Design Services"/>
            <meta property="twitter:description" content="How do you find a graphic designer or outsource graphic design company to make your business impressive? Visit Graphics Zoo for on-demand design and marketing graphic design services at affordable prices."/>
            <meta property="twitter:URL" content="@add twitter handle">

        <?php }if ($myuri == "/portfolio") { ?>
            <meta property="og:title" content="Graphics Zoo: Logo, Artwork, Print, Website & T-shirt Graphic Design Company"/>
            <meta property="og:description" content="Hire logo designers and t-shirt graphic designers to create an impressive business logo and designs for t-shirts. Our logo and t-shirt artwork designs match your style and build a strong impact on the industry. Take a look at our designs that we have delivered to our clients."/>
            <meta property="og:site_name" content="Graphicszoo.com" />

            <meta property="twitter:card" content="summary"/>
            <meta property="twitter:title" content="Graphics Zoo: Logo, Artwork, Print, Website & T-shirt Graphic Design Company"/>
            <meta property="twitter:description" content="Hire logo designers and t-shirt graphic designers to create an impressive business logo and designs for t-shirts. Our logo and t-shirt artwork designs match your style and build a strong impact on the industry. Take a look at our designs that we have delivered to our clients."/>
            <meta property="twitter:URL" content="@add twitter handle">
        <?php }if ($myuri == "/pricing") { ?>
            <meta property="og:title" content="Graphics Zoo Offers Website Design Subscription & Digital Graphic Design Tools" />
            <meta property="og:description" content="Are you on a hunt for digital graphic design tools or website design subscription services? Get in touch with Graphics Zoo. We offer cost-effective monthly packages for digital graphic design."/>
            <meta property="og:site_name" content="Graphicszoo.com" />

            <meta property="twitter:card" content="summary"/>
            <meta property="twitter:title" content="Graphics Zoo Offers Website Design Subscription & Digital Graphic Design Tools" />
            <meta property="twitter:description" content="Are you on a hunt for digital graphic design tools or website design subscription services? Get in touch with Graphics Zoo. We offer cost-effective monthly packages for digital graphic design." />
            <meta property="twitter:URL" content="@add twitter handle">
        <?php }if ($myuri == "/") { ?>
            <meta property="og:title" content="Graphics Zoo Offers Unlimited Graphic Design Services & Packages"/>
            <meta property="og:description" content="Are you on a lookout for a graphic design company ? Graphics Zoo gives unlimited graphic design services packages for your business. Our expert team provides professional graphic design services and helps you connect with your target audience."/>
            <meta property="og:site_name" content="Graphicszoo.com" />

            <meta property="twitter:card" content="summary"/>
            <meta property="twitter:title" content="Graphics Zoo Offers Unlimited Graphic Design Services & Packages"/>
            <meta property="twitter:description" content="Are you on a lookout for a graphic design company ? Graphics Zoo gives unlimited graphic design services packages for your business. Our expert team provides professional graphic design services and helps you connect with your target audience."/>
            <meta property="twitter:URL" content="@add twitter handle">
        <?php } ?>


        <?php
        $myuri = $_SERVER['REQUEST_URI'];
        if ($myuri == "/") {
            ?>
            <link rel="canonical" href="<?php echo base_url(); ?>" />
        <?php }if ($myuri == "/aboutus") { ?>
            <link rel="canonical" href="<?php echo base_url(); ?>aboutus" />
        <?php }if ($myuri == "/portfolio") { ?>
            <link rel="canonical" href="<?php echo base_url(); ?>portfolio" />
        <?php }if ($myuri == "/pricing") { ?>
            <link rel="canonical" href="<?php echo base_url(); ?>pricing" />
        <?php }if ($myuri == "/blog") { ?>
            <link rel="canonical" href="<?php echo base_url(); ?>blog" />
        <?php }if ($myuri == "/faq") { ?>
            <link rel="canonical" href="<?php echo base_url(); ?>faq" />
        <?php }if ($myuri == "/support") { ?>
            <link rel="canonical" href="<?php echo base_url(); ?>support" />
        <?php }if ($myuri == "/terms-and-conditions") { ?>
            <link rel="canonical" href="<?php echo base_url(); ?>terms-and-conditions" />
        <?php }if ($myuri == "/privacy-policy") { ?>
            <link rel="canonical" href="<?php echo base_url(); ?>privacy-policy" />
        <?php } ?>

        <!---Start global variables -->
        <script>
                var BASE_URL = "<?php echo base_url(); ?>";
                var assets_path = "<?php echo FS_PATH_PUBLIC_ASSETS; ?>";
                $rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
                var subscription_plan = '<?php echo json_encode(SUBSCRIPTION_DATA); ?>';
                var total_active_requests = "<?php echo TOTAL_ACTIVE_REQUEST; ?>";
        </script>
        <!--- End global variables -->

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-57XFPLD');</script>
        <!-- End Google Tag Manager -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <!--script defer src="https://www.googletagmanager.com/gtag/js?id=UA-103722162-1"></script-->
        <script>
            setTimeout(function () {
                $.getScript("https://www.googletagmanager.com/gtag/js?id=UA-103722162-1", function (data, textStatus, jqxhr) {

                });
            }, 7000);
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-103722162-1');
        </script>
        <meta name="google-site-verification" content="jNJk_m4f6Y2kYTcA9kqXNX1A4evH5jlfDds6Rl9xHL0" />
        <!-- End Google Tag Manager -->
    </head>
    <body 
    <?php
    $role_id = isset($_SESSION['role']) ? $_SESSION['role'] : "";
    $is_saas = isset($_SESSION['is_saas']) ? $_SESSION['is_saas'] : "";
    if (isset($id)) {
        echo "id=" . $id;
        if ($id == 'single-blog-page') {
            echo " class='blog-list'";
        }
    }
    ?>>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57XFPLD"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <header id="header">
            <div class="container">
                <div class="flex-header">
                    <div id="logo" class="pull-left">
                        <a href="<?php echo base_url(); ?>" rel="nofollow"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/logo.svg" alt="GraphicsZoo" title="GraphicsZoo" /></a>
                    </div>
                    <button type="button" class="menu-toggle">
                        <span></span>
                        <span></span>
                        <span></span>                                
                    </button>
                    <div class="mobile-class">
                        <nav id="nav-menu-container">
                            <ul class="nav-menu">
                                <li class='<?php
                                if ($titlestatus == "portfolio") {
                                    echo "active";
                                }
                                ?>'><a rel="canonical" href="<?php echo base_url(); ?>portfolio">Portfolio</a></li>
                                <li class='<?php
                                if ($titlestatus == "pricing") {
                                    echo "active";
                                }
                                ?>'><a rel="canonical" href="<?php echo base_url(); ?>pricing">Pricing</a></li>
                                <li class='<?php
                                if ($titlestatus == "testimonial") {
                                    echo "active";
                                }
                                ?>'><a rel="canonical" href="<?php echo base_url(); ?>testimonials">Testimonials</a></li>
                                <li class='<?php
                                if ($titlestatus == "features") {
                                    echo "active";
                                }
                                ?>'><a rel="canonical" href="<?php echo base_url(); ?>features">Features</a></li>

                                <li class="resources"><a href="#/">Resources</a>
                                    <ul>
                                        <li><a rel="canonical" href="<?php echo base_url(); ?>blog">Blog</a></li>

                                        <li>
                                            <a rel="canonical" href="<?php echo base_url(); ?>aboutus">About Us</a>
                                        </li>

                                        <li>
                                            <a rel="canonical" href="<?php echo base_url(); ?>support">Contact Us</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <div class="navbar-right clearfix">
                            <ul>
                                <?php if ($role_id == 'customer' && $is_saas == 0) { ?>
                                    <a href="<?php echo base_url(); ?>customer/request/design_request" class="button" >DASHBOARD</a>
                                <?php } elseif (($role_id == 'customer' && $is_saas == 1) || ($role_id == 'customer' && $is_saas == 2 )) { ?>
                                    <a href="<?php echo base_url(); ?>account/dashboard" class="button">DASHBOARD</a>
                                <?php }elseif ($role_id == 'designer') { ?>
                                    <a href="<?php echo base_url(); ?>designer/request" class="button">DASHBOARD</a>
                                <?php } elseif ($role_id == 'admin') { ?>
                                    <a href="<?php echo base_url(); ?>admin/dashboard" class="button">DASHBOARD</a>
                                <?php } elseif ($role_id == 'qa') { ?>
                                    <a href="<?php echo base_url(); ?>qa/dashboard" class="button">DASHBOARD</a>
                                <?php } elseif ($role_id == 'va') { ?>
                                    <a href="<?php echo base_url(); ?>va/dashboard" class="button">DASHBOARD</a>
                                <?php } else { ?>
                                    <a rel="canonical" href="<?php echo base_url(); ?>login"  class="log-in">Login</a>
                                    <a rel="canonical" href="<?php echo base_url(); ?>signup" class="button mar-space">SIGN UP</a>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>