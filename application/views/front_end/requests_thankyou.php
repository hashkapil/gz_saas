<!DOCTYPE html>
<?php //echo "<pre>";print_r($user_data); ?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Thank You | Graphics Zoo</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- Favicons -->
    <link rel="shortcut icon canonical" type="image/x-icon" href="<?php echo FAV_ICON_PATH; ?>">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,600i,700,700i,800,800i,900,900i" rel="stylesheet nofollow canonical">
    <!-- Bootstrap css -->
    <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/bootstrap/css/bootstrap.css" rel="stylesheet canonical">
    <!-- Libraries CSS Files -->
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet canonical">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/animate/animate.min.css" rel="stylesheet canonical">

    <!-- Font Awesomw library -->
    <link rel="stylesheet nofollow canonical" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">

    <!-- Main Stylesheet File -->
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/style.css" rel="stylesheet canonical">
    <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/custom.css" rel="stylesheet canonical">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-57XFPLD');</script>
    <!-- End Google Tag Manager -->
</head>
<style>
    .subscription_pad {
    padding-bottom: 50px;
}
.modal-content {
    box-shadow: 0 0 35px rgba(0, 0, 0, 0.1);
    border: 1px solid #E5E5E5;
}
.modal-body{padding: 0}
.thank-header {
    background: #e82f4d;
    color: #fff;font-size: 90px;
}
.thank-header span {
    transition: 0.56s; display: block;
}
.sign-up-pagep:hover .thank-header span {
    transform: rotate(15deg);
}
.current_plan_details h2 {
    font-weight: 600;
    font-size: 26px !important;
    margin-top: 30px;
    color: #000 !IMPORTANT;
    white-space: normal !important;
}
.current_plan_details h3 {
    color: #e82f4d;
    text-transform: capitalize;
    margin-bottom: 20px;
    margin-top: 20px;
}
.current_plan_details p {
    margin-bottom: 7px;
    font-size: 16px;
}
a.sign-up-p {
    cursor: pointer;
    text-decoration: none !important;
    border-radius: 50px !important;
    font-size: 17px;
    background: #e82f4d;
    font-weight: 600;
    margin: 16px 0 0;
    padding: 12px 18px;
    color: #fff !important;
    border: #e82f4d;
    display: inline-block;
}
</style>
<body id="signup-page">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57XFPLD"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
      <!-- Modal -->
      <div id="SignupModal" class="modal_ fade_" role="dialog_">
        <div class="col-md-8 sign-up-pagep">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container">
                        <?php //echo $user_data['display_plan_name']."<pre>";print_r($current_plan_details); ?> 
                        <div class="row">

                          <div class="subscription_pad" style="text-align:center;    width: 100%;">
                            <div class="thank-header">
                                <span><i class="fas fa-thumbs-up"></i></span>
                               
                            </div>
                            <div class="current_plan_details">
                                 <h2>You have Successfully Signed up!</h2>
                                <h3><?php echo $plan_details[0]['plan_name']; ?></h3>
                                <?php if(in_array($user_data['plan_name'],NEW_PLANS)){ ?>
                                    <p>
                                        Upto <?php echo $user_data['total_requests']; ?> active request(s)</p>
                                    <?php }elseif($user_data['plan_name'] == SUBSCRIPTION_99_PLAN){ ?>
                                        <p> 3 Requests per month</p>
                                    <?php }else{ ?>
                                        <p>Upto <?php echo $user_data['total_active_req']; ?> active requests</p>
                                        <p> Unlimited Requests per month</p>
                                    <?php } ?>
                                    <?php if($plan_details[0]['is_agency'] == 1){ ?>
                                     <p>Plan Amount: $<?php echo $plan_details[0]['plan_price']; ?></p>
                                 <?php } ?>
                                 <?php if($current_plan_details['percent_off'] != ''){ ?>
                                     <p>Discount: <?php echo $current_plan_details['percent_off']; ?>%</p>
                                 <?php } ?>
                                 <?php if($current_plan_details['tax_percent'] != '' || $current_plan_details['tax_percent'] != 0){ ?>
                                     <p>Tax Amount: $<?php echo $current_plan_details['tax_amount']; ?></p>
                                 <?php } ?>
                                 <p>Total Amount: $<?php echo $current_plan_details['final_amount']; ?></p>
                                 <p style="margin:30px auto 10px;    max-width: 560px; color: #666 ">Go to the dashboard to submit a new design request and see what your new design team can do for you.</p>
                                 <div><a class="sign-up-p" href="<?php echo base_url(); ?>customer/request/design_request"> Go to Dashboard</a></div>
                             </div>

                         </div>
                     </div>
                 </div>
             </div>

         </div>
     </div>

 </div>
</div>
    <!--==========================
    Hero Section
    ============================-->

    <!--==========================
    Footer
    ============================-->
    <section class="footer_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">

                </div>
            </div>
        </div>
    </section>
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/jquery/jquery.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/jquery/jquery-migrate.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/superfish/hoverIntent.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/superfish/superfish.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/easing/easing.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/wow/wow.min.js"></script>
    <!-- Contact Form JavaScript File -->
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/contactform/contactform.js"></script>

    <!-- Template Main Javascript File -->
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/main.js"></script>
    <script type="text/javascript">
            // function ValidDate(d) {
            //      d = new String(d)
            //      var re1 = new RegExp("[0|1][0-9]\/[1-2][0-9]{3}");
            //      var re2 = new RegExp("^[1[3-9]]");
            //      var re3 = new RegExp("^00");
            //      var expir_date = re1.test(d)&&(!re2.test(d))&&(!re3.test(d)); 
            //      if(expir_date === true){
            //         $('#expir_date').val(d);
            //      }else{
            //         $('#expir_date').val("");
            //      }               
            // }
            function dateFormat(val) {
                if (val.length == 2) {
                    $('#expir_date').val($('#expir_date').val() + '/');
                }
                /*if(val.length == 5){
                 document.getElementById('cvv').focus();
             }*/
         }
         $('.subs_plan').on('change', function () {
            var sub_val = $(this).val();
            if (sub_val == 'A2870G') {
                $('.plans_table_sec').show();
                $('.plans_table').hide();
                $('.plans_table_logo').hide();

            } else if (sub_val == 'M299G') {
                $('.plans_table_sec').hide();
                $('.plans_table_logo').hide();
                $('.plans_table').show();
            }
        });
    </script>
    <!-- REFERSION TRACKING: BEGIN -->
    <script rel="nofollow" src="//graphicszoo.refersion.com/tracker/v3/pub_01537b7d8dcde95d5a0d.js"></script>
    <script>
       _refersion(function(){

          _rfsn._addTrans({
             'order_id': '<?php echo $user_data['customer_id']; ?>',
             'shipping': '0',
             'tax': '<?php echo $user_data['taxpaid']; ?>',
             'discount': '0',
             'discount_code': 'NULL',
             'currency_code': 'USD'
         });

          _rfsn._addCustomer({
             'first_name': '<?php echo $user_data['first_name']; ?>',
             'last_name': '<?php echo $user_data['last_name']; ?>',
             'email': '<?php echo $user_data['email']; ?>',
             'ip_address': '<?php  echo $_SERVER['REMOTE_ADDR'];?>'
         });

          _rfsn._addItem({
             'sku': '<?php echo $user_data['current_plan']; ?>',
             'quantity': '1',
             'price': '<?php echo $user_data['plan_amount']; ?>'
         });

          _rfsn._sendConversion();

      });
  </script>
  <!-- REFERSION TRACKING: END -->
</body>
</html>