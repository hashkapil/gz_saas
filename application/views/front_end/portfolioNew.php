<section class="page-heading-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Our Creative Work</h1>
                <p>
                    We create all types of graphic designs. From logos, brochures, flyers, web pages, 
                    t-shirts, illustrations to much more. Take a look at some of our work!
                </p>
            </div>
        </div>
    </div>
</section>
<!--------------Portfolie tabs---------------------->
<section class="poerfolioTabs">
    <div class="container">
        <div class="row">
            <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <ul id="tabsJustified" class="nav nav-tabel">
                    <li>
                        <a href="#All" data-target="#All" data-id="0" data-toggle="tab" class="active show_loading">
                            <button class="btn btn-default filter-button">All</button>
                        </a>

                    </li>
                    <li>
                        <a href="#Logo" data-target="#Logo" data-id="1" data-toggle="tab" class="show_loading">
                            <button class="btn btn-default filter-button">Logo</button>
                        </a>
                    </li>
                    <li>
                        <a href="#T-shirt" data-target="#T-shirt" data-id="3" data-toggle="tab" class="show_loading">
                            <button class="btn btn-default filter-button" >T-shirt</button>
                        </a>
                    </li>
                    <li>
                        <a href="#Print" data-target="#Print" data-id="9" data-toggle="tab" class="show_loading">
                            <button class="btn btn-default filter-button" data-filter="spray">Print</button>
                        </a>
                    </li>
                    <li>
                        <a href="#Ads" data-target="#Ads" data-id="10" data-toggle="tab" class="show_loading">
                            <button class="btn btn-default filter-button" data-filter="irrigation">Ads</button>
                        </a>
                    </li>
                    <li>
                        <a href="#Websites" data-target="#Websites" data-id="11" data-toggle="tab" class="show_loading">
                            <button class="btn btn-default filter-button" data-filter="irrigation">Websites</button>
                        </a>
                    </li>
                    <li>
                        <a href="#Other" data-target="#Other" data-id="5" data-toggle="tab" class="show_loading">
                            <button class="btn btn-default filter-button" data-filter="irrigation"> Other</button>
                        </a>
                    </li>
                </ul>
                <div id="tabsJustifiedContent" class="tab-content">
<!--                    <div id="portfolioloader">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/ajax-loader.gif"/>
                    </div>-->
                    <!---------All portfolio---------------->
                    <div id="" class="tab-pane fade active show" data-group="1" data-total-count="<?php echo $datacount; ?>">
<!--                         ==============Logo Designs start ============== -->
                        <ul class="all-project grid" id="All">
                            <?php 
                            foreach($data as $key => $val){ ?>
                            <li class="item thumb">
                                <div class="content box-overflow">
                                    <a class="thumbnail" href="#" data-image-id="<?php echo $val['id']; ?>" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_UPLOADS_PORTFOLIOS.$val['image']; ?>" data-target="#image-gallery"></a>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PORTFOLIOS.$val['image']; ?>" class="img-responsive" alt="<?php echo pathinfo($val['image'], PATHINFO_FILENAME);?>">
                                    <div class="hover-txt">
                                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png" alt="search-icon"></span>
                                        <p><?php echo $val['title'];?></p>
                                    </div>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                   
                    

                    <!---------Logo portfolio---------------->

                     <div id="" class="tab-pane fade" data-group="1" data-total-count="<?php echo $datacount; ?>">
                        <ul class="all-project grid" id="Logo">
                            
                        </ul>
                    </div>

                    <!---------T-Shirt portfolio---------------->
                        <div id="" class="tab-pane fade" data-group="1" data-total-count="<?php echo $datacount; ?>">
                            <ul class="all-project grid" id="T-shirt">
                            </ul>
                        </div>
                    <!---------Print portfolio---------------->
                    <div id="" class="tab-pane fade" data-group="1" data-total-count="<?php echo $datacount; ?>">
                            <ul class="all-project grid" id="Print">
                            </ul>
                    </div>
                
                    <!---------Ads portfolio---------------->
                    <div id="" class="tab-pane fade" data-group="1" data-total-count="<?php echo $datacount; ?>">
                            <ul class="all-project grid" id="Ads">
                            </ul>
                    </div>

             
                    <!---------Websites portfolio---------------->

                    <div id="" class="tab-pane fade" data-group="1" data-total-count="<?php echo $datacount; ?>">
                            <ul class="all-project grid" id="Websites">
                            </ul>
                    </div>
                    <!--------- App portfolio---------------->

                    <!---------  Other portfolio---------------->
                    
                    <div id="" class="tab-pane fade" data-group="1" data-total-count="<?php echo $datacount; ?>">
                            <ul class="all-project grid" id="Other">
                            </ul>
                    </div>
                    
                     <?php if ($datacount > LIMIT_ADMIN_LIST_COUNT) { ?>
                        <div class="col-md-12 text-center loading" id="load_more_other">
                            <div class="ajax_loader" style="display:none;text-align:center"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/ajax-loader.gif" alt="ajax-loader" /></div>
                            <a id="load_more" href="javascript:void(0)" class="red-theme-btn load_more" rel="nofollow">
                                View More
                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-fluid" alt="long-arrow">
                            </a>
                            </div>
                      <?php } ?>
      
                    </div>
                </div> 
            <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="image-gallery-title"></h4>
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <img id="image-gallery-image" class="img-responsive" src="">
                            </div>
                            <div class="modal-footer">
                            </div> 
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>
<section class="riskFree-sec pricing-risk">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Join more than 500+ customers</h2>
                <h3>Try Graphics Zoo Risk-Free For 14 Days</h3>
                <a rel="canonical nofollow" href="<?php echo base_url(); ?>signup" class="red-theme-btn">Get Started Now
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/red-long-arrow.png" class="img-fluid" alt="red-long-arrow"></a>

                </div>
            </div>
        </div>
    </section>

 
    <script type='text/javascript'>
        function make_grid(){
//            console.log("griding");
            var grid = $(".grid");
            var getItem = grid.children('li');
            getItem.each(function(){
                var getItem_height = 0;
                getItem_height = $(this).find('img').height();
                var rowHeight =  parseInt($(grid).css( "grid-auto-rows" ));
                var rowGap = parseInt($(grid).css('grid-row-gap'));
                if(getItem_height == 0){
                    getItem_height = 363; //static height if height is 0
                }
//                console.log("getItem_height",getItem_height);
//                console.log("rowHeight",rowHeight);
//                console.log("rowGap",rowGap);
                var rowSpan = Math.ceil((getItem_height + rowGap) / (rowHeight + rowGap));
                //console.log("rowSpan",rowSpan);
                if(rowSpan > 15){
                    $(this).css( "grid-row-end", "span 15");
                }
                else{
                    $(this).css( "grid-row-end", "span " + rowSpan );
                }
            });
        }
       $(window).load(function(){
            make_grid();
        });


  /**************start on load & tab change get all records with load more button*****************/
    $(document).ready(function () {
        
        /** slide toggle **/
        $("button.menu-toggle").click(function(){
            $(".mobile-class").slideToggle();
        });
        
        
        $rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
        $('.tab-pane').attr("data-loaded", $rowperpage);
         $(".ajax_loader").css("display","none");
        $(document).on("click", "#load_more", function (e) {
            e.preventDefault();
            $(".ajax_loader").css("display","block");
            $(".load_more").css("display","none");
            var row = parseInt($(this).attr('data-row'));
            ///var allcount = parseInt($(this).attr('data-count'));
            row = row + $rowperpage;
            var activeTabPane = $('.tab-pane.active');
//            console.log(activeTabPane);
            var allcount = parseInt(activeTabPane.attr("data-total-count"));
            var allLoaded = parseInt(activeTabPane.attr("data-loaded"));
            var activeTabPaneGroupNumber = parseInt(activeTabPane.attr('data-group'));
            var status_scroll = $('#tabsJustified li a.active').attr('data-id');
            var tabid = $('#tabsJustified li a.active').attr('href');
            if (allLoaded < allcount) {
                    $.post('<?php echo site_url() ?>welcome/load_more_portfolio', {'group_no': activeTabPaneGroupNumber, 'id': status_scroll},
                    function (data) {
                        if (data != "") {
                            $(tabid).append(data);
                            //$(tabid+" .product-list-show .row").append(data);
                            row = row + $rowperpage;
                            $(".ajax_loader").css("display","none");
                            $(".load_more").css("display","inline-block");
                            activeTabPane.find('.load_more').attr('data-row', row);
                            activeTabPaneGroupNumber++;
                            activeTabPane.attr('data-group', activeTabPaneGroupNumber);
                            allLoaded = allLoaded + $rowperpage;
                            activeTabPane.attr('data-loaded', allLoaded);
                            make_grid();
                            ///console.log('allLoaded',allLoaded);
                            //console.log('allcount',allcount);
                            if (allLoaded >= allcount) {
                                console.log('test');
                                $('#load_more').css("display", "none");
                                $(".ajax_loader").css("display","none");
                            }else{
                                activeTabPane.find('.load_more').css("display", "inline-block");
                                $(".ajax_loader").css("display","none");
                            }
                        }
                    });
                }
        });
    });
/**************End on load & tab change get all records with load more button*****************/


        /***********portfolio active tab from home page***************/
        var url = window.location.href;
        var activeTab = url.substring(url.indexOf("#") + 1);
        var hash = window.location.href.indexOf('#');
        if(activeTab != '' && hash > 0){
           $(".tab-pane").removeClass("active");
           $('#' + activeTab).addClass("active show");
           make_grid();
           var id = $('.show_loading').data("target");
           var list = $("" + id + " li");
           var button = $(".load_more");
           loadMore(list, button);
            // $('.loading').css('display','none');
            $('#tabsJustified').find('li a').each(function(e) {
               var dataattr = $(this).data('target');
               var fromurl = ("#" + activeTab);
               if(dataattr == fromurl){
                   $(this).addClass('active');
                   
                   $(this).closest('li').siblings().find('a').removeClass('active');
                   return false;
               }
           });
        }
        
$('#tabsJustified li a').click(function (e) {
 $(".ajax_loader").css("display","inline-block");
 $(".load_more").css("display","none");
//$('#load_more').css("display", "inline-block");
var hrefdata = $(this).attr('href');
//console.log(hrefdata);
$('.tab-pane').removeClass('active show');
$(hrefdata).parent('.tab-pane').addClass('active show');
var currentstatus = $(this).data('id');
var tab = $(this).attr('href');
//console.log(currentstatus);
//console.log(hrefdata);
e.preventDefault();
    load_more_portfolio(currentstatus,tab);
});

function load_more_portfolio(currentstatus = '',tabActive = ''){
    var activetab = $('#tabsJustified li a.active').attr('href');
    var tabid = (tabActive != '') ? tabActive : activetab ;
    var activeTabPane = $('.tab-pane.active');
    var status_Active = $('#tabsJustified li a.active').attr('data-id');
//    console.log(currentstatus);
//    console.log(tabid);
    var id   = currentstatus;
    if(currentstatus !== ''){
       id =  currentstatus;
    }else{
       id =  status_Active;
    }
//    console.log('id',id);
    //console.log('tab',tabid);
    //var searchval = $('.tab-pane.active .search_text').val();
    $.post('<?php echo site_url() ?>welcome/load_more_portfolio', {'group_no': 0, 'id': id},
        function (data){
            var newTotalCount = 0;
            if(data != ''){
                 $(".ajax_loader").css("display","none");
//                console.log(tabid,data);
                $(".ajax_searchload").fadeOut(500);
                $(tabid).html(data);
                 make_grid();
//                $(tabid+ " .product-list-show .row").html(data);
                newTotalCount = $(tabid).closest('.tab-pane').find("#loadingAjaxCount").attr('data-value');
                $(tabid+" .product-list-show .row").find("#loadingAjaxCount").remove();
                activeTabPane.attr("data-total-count",newTotalCount);
                activeTabPane.attr("data-loaded",$rowperpage);
                activeTabPane.attr("data-group",1);
            } else {
                activeTabPane.attr("data-total-count",0);
                activeTabPane.attr("data-loaded",0);
                activeTabPane.attr("data-group",1);
                $(tabid).html("");
            }
            if ($rowperpage >= newTotalCount) {
                $('#load_more').css("display", "none");
                //activeTabPane.find('.load_more').css("display", "none");
            } else{
                $('#load_more').css("display", "inline-block");
                //activeTabPane.find('.load_more').css("display", "inline-block");
            }
        });
}
</script>
