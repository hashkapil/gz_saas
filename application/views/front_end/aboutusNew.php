<style>
section.publection-sec{display: none}
</style>
    <section class="page-heading-sec">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <h1>One Team & One Dream</h1>
           <p>We are a fun group that believes all it takes is a great <br/>
           design to get you to the next level</p>
           
           <a rel="canonical" href="<?php echo base_url(); ?>pricing" class="red-theme-btn">Get Started Now</a>
         </div>
       </div>
     </div>
   </section>


   <section class="nopadding padd-section">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/who-we-are.jpg" class="whowe-img" alt="who-we-are">
        </div>

        <div class="col-md-6">
          <div class="mt-5 pt-3">
            <h2 class="heading-btn">Who We Are</h2>
            <p style="color:#828d99; font-size: 15px;">We are a team of entrepreneurs that struggled to get quality graphic designs on a tight budget. Our competitors seemed to be constantly updating their site, logo, color scheme, ads, marketing material, etc, while we wasted way to much time going back and forth with designers. Through our frustration of always playing catch up, we created Graphics Zoo!</p>
            <div class="three-smily">
              <div class="boxOne">
                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/sweet-smily.png" alt="sweet-smily">
                <div>
                  <h2>500+</h2>
                  <p>Happy Clients</p>
                </div>
              </div>
              <div class="boxOne">
                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/thumbs-up.png" alt="thumbs-up">
                <div>
                  <h2>10K+</h2>
                  <p>Approved Projects </p>
                </div>
              </div>
              <div class="boxOne">
                <i class="far fa-star"></i>
                <div>
                  <h2>4.9/5</h2>
                  <p>By Customers</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section><!-- #who we are-->

  <!--==========================
    Team Section
    ============================-->
    <section id="team" class="padd-section text-center">

     <div class="container">
      <div lass="section-title text-center">
        <h2>Our Creative Team</h2>
        <p>We value open source, learning, remote work and open communication.</p>
      </div>
      <div class="logos-slider"> 
       <div class="logs-slider owl-carousel owl-theme">
        <div class="item">
         <div class="team-member">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/team/Cathy_Creative.jpg" alt="Cathy

          _Creative" class="img-responsive">
          <h3>Cathy</h3>
          <p>Creative Designer</p>
        </div>
      </div>
      <div class="item">
        <div class="team-member">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/team/Ardel_Creative.jpg" class="img-responsive" alt="Ardel_Creative">
          <h3>Ardel</h3>
          <p>Creative Designer</p>
        </div>
      </div>
      <div class="item">
        <div class="team-member">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/team/Jaykarl_Creative.jpg" class="img-responsive" alt="Jaykarl_Creative">
          <h3>Jaykarl</h3>
          <p>Creative Designer</p>
        </div>
      </div>
      <div class="item">
        <div class="team-member">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/team/Joy_Operations.jpg" class="img-responsive" alt="Joy_Operations">
          <h3>Joy</h3>
          <p>Operations Lead</p>
        </div>
      </div>
      <div class="item">
        <div class="team-member">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/team/Karen_Account.jpg" class="img-responsive" alt="Karen_Account">
          <h3>Karen</h3>
          <p>Account Manager</p>
        </div>
      </div>
      <div class="item">
        <div class="team-member">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/team/Luigi_Quality.jpg" class="img-responsive" alt="Luigi_Quality">
          <h3>Luigi</h3>
          <p>Quality Reviewer</p>
        </div>
      </div>
      <div class="item">
        <div class="team-member">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/team/Mazahir.jpg" class="img-responsive" alt="Mazahir">
          <h3>Mazahir</h3>
          <p>Creative Designer</p>
        </div>
      </div>
      <div class="item">
        <div class="team-member">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/team/Melanie_Creative.jpg" class="img-responsive" alt="Melanie_Creative">
          <h3>Melanie</h3>
          <p>Creative Designer</p>
        </div>
      </div>
      <div class="item">
        <div class="team-member">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/team/Raiah.jpg" class="img-responsive" alt="Raiah">
          <h3>Raiah</h3>
          <p>Creative Designer</p>
        </div>
      </div>
      <div class="item">
        <div class="team-member">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/team/Reign_Creative.jpg" class="img-responsive" alt="Reign_Creative">
          <h3>Reign</h3>
          <p>Creative Designer</p>
        </div>
      </div>
      <div class="item">
        <div class="team-member">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/team/Yohan_Creative.jpg" class="img-responsive" alt="Yohan_Creative">
          <h3>Yohan</h3>
          <p>Creative Designer</p>
        </div>
      </div>

    </div>
    
  </div>

</div>
</section>

<!--==========================
    Problems we solve
    ============================-->
    <section id="problems-solve" class="padd-section text-center">
      <div class="container">
        <div class="section-title text-center">
          <h2>Problems We Solve</h2>
        </div>
      </div>
      <div class="container">
        <div class="row">
         <div class="col-md-6">
          <div class="shadow-wrapper">
           <p><strong>We need a fresh brand identity</strong> to help us stand out from the noise.</p>
         </div>
       </div>
       <div class="col-md-6">
        <div class="shadow-wrapper">
         <p><strong>Can you help us build marketing </strong>materials for this upcoming event?</p>
       </div>
     </div>
     <div class="col-md-6">
      <div class="shadow-wrapper">
       <p><strong>We want ad designs to launch a digital marketing</strong>  campaign for our business.</p>
     </div>
   </div>
   <div class="col-md-6">
    <div class="shadow-wrapper">
     <p><strong>I run a print shop and need your team to </strong> create designs files for my customer’s orders</p>
   </div>
 </div>
</div>
<div class="row mt-1 mb-1">
 <div class="col-md-6 col-6" style="text-align: right; padding: 0 5px 0 0;">
  <a rel="canonical" href="<?php echo base_url(); ?>portfolio" class="blue-theme-btn" rel="nofollow">OUR WORK  <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-responsive" alt="long-arrow"></a>
</div>
<div class="col-md-6 col-6" style="text-align: left;padding: 20px 0px 0px 5px;">
  
  <a rel="canonical" href="<?php echo base_url(); ?>support" class="red-theme-btn" rel="nofollow">LET'S TALK ? <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-responsive" alt="long-arrow">
  </a>
</div>
</div>
</div>
</section>	


<section class="riskFree-sec pricing-risk">
 <div class="container">
  <div class="row">
   <div class="col-md-12 text-center">
    <h2>Join more than 500+ customers</h2>
    <h3>Try Graphics Zoo Risk-Free For 14 Days</h3>
    <a rel="canonical" href="<?php echo base_url(); ?>pricing" class="red-theme-btn" rel="nofollow">Get Started Now
     <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>front_end/Updated_Design/img/red-long-arrow.png" class="img-fluid" alt="red-long-arrow"></a>

   </div>
 </div>
</div>
</section>