<style>
@media only screen and (max-width: 768px){
	.view_blogp{
		font-size:25px !important;
		padding-top:85px !important;
	}
	.view_blogdate
	{
		font-size: 12px !important;
	}
}	
</style>

<div class="container-fluid blog_row"  style="padding:0px;">
	<div class="row" style="margin:0px;background-image: url('<?php echo FS_PATH_PUBLIC_UPLOADS_BLOGS.$data[0]['image']; ?>');height:300px;">
		<div class="col-md-7 col-md-offset-2 col-lg-7 col-lg-offset-2 col-sm-12 col-xs-12">	
			<p class="view_blogp" style="color: #fff;text-align: center;font-size: 50px;font-weight: 600;padding-top: 67px;"><?php echo $data[0]['title']; ?></p>
			<p style="color:#fff;text-align:center;" class="view_blogdate">by Author on <?php echo $data[0]['created']; ?></p>
		</div>
	</div>
	<div class="row" style="margin:0px;">
		<div class="col-md-offset-1 col-md-10 col-lg-10 col-lg-offset-1 col-sm-12 col-xs-12" style="padding-top:25px;">
			
			<p style="text-align:center;font-size:16px;color:#000;"><?php echo $data[0]['body']; ?></p>
			
		</div>
	</div>    
</div>	