<section class="page-heading-sec pricing-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 >Unlimited Graphic Design Services <br> Low Monthly Rate</h1>
                <p>Simple flat-rate pricing to meet your business needs</p>
            </div>
        </div>
    </div>
</section>
<!--------------Portfolie tabs---------------------->
<section class="pricing-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="main-heading"><span>14 </span>Days Risk-Free Guarantee!</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12  text-center">
                <a rel="canonical" href="<?php echo base_url(); ?>signup" class="nothing">
                    <div class="price-plan">
                        <h3>Golden Plan</h3>
                        <h4>MONTHLY</h4>
                        <h2>$299<span>/Mon</span></h2>
                        <input type="submit" class="sing-up" value="Sign up">
                        <ul>
                            <li>This Includes</li>
                            <li>1-Day Turnaround</li>
                            <li>14 Day Risk Free Guarantee</li>
                            <li>No Contract</li>
                            <li>Unlimited Design Requests</li>
                            <li> Unlimited Revisions</li>
                            <li> Quality Designers</li>
                            <li> Dedicated Support</li>
                            <li> Unlimited free stock images from</li>
                            <li> Pixabay and Unsplash</li>
                        </ul>
                    </div>
                </a>
                <a rel="canonical" href="<?php echo base_url(); ?>signup" class="nothing">
                    <div class="price-plan par-anm">
                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ribbon.png" class="img-fluid ribbon-img" alt="Golden plan">
                        <h3>Golden Plan</h3>
                        <h4 class="annual">Annual</h4>
                        <h2>$239<span>/Mon</span></h2>
                        <input type="submit"class="sing-up" value="Sign up">
                        <ul>
                            <li>This Includes</li>
                            <li>1-Day Turnaround</li>
                            <li>14 Day Risk Free Guarantee</li>
                            <li>No Contract</li>
                            <li>Unlimited Design Requests</li>
                            <li> Unlimited Revisions</li>
                            <li> Quality Designers</li>
                            <li> Dedicated Support</li>
                            <li> Unlimited free stock images from</li>
                            <li> Pixabay and Unsplash</li>
                        </ul>

                    </div>
                </a>

            </div>
        </div>
    </div>
</section>

<!------ risk-free-sec -------->
<section class="faq-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Frequently Asked Question</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h4>Do I really get unlimited designs?</h4>
                <p>Yes! You really get unlimited designs and revisions for a flat monthly rate. You can request as many revisions as you like until you are completely satisfied. We work on 1 new request a day.</p>
                <h4>What does Risk-Free guarantee really mean?</h4>
                <p>If you do not approve any of your designs or graphics within the first 14 days of
                    your trial, we will give you a full refund, no questions asked.
                </p>
                <h4>What if I need 220 designs per day?</h4>
                <p>If you know your design volume will be higher than the active requests per day is,
                    we recommend you get additional accounts so your design volume can be handled properly.</p>
            </div>
            <div class="col-md-6">
                <h4>How long does it take to receive designs?</h4>
                <p>Every design request is serviced within 1 business day. Please keep in mind though,
                    that we value quality over everything else. Your designer will be in constant communication
                    with you and will keep you updated on the status of your designs. </p>
                <h4>What can I get designed?</h4>
                <p>You can get anything designed from your designer. Everything from banners,
                    flyers, t-shirts, book covers and much more. Just keep in mind larger and more 
                    complicated designs may take a little longer to finish.</p>
                <h4>Is there really no contract?</h4>
                <p>Yes, that's right! No contract, so you can cancel at anytime.</p>
            </div>
        </div>
    </div>
</section>