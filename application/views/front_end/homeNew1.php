<style>
#hero{background: #fff;    padding: 40px 0 0;}
#hero h1 {
    margin: 70px 0 70px 0;font-size: 37px;
    }
    #hero img {
    max-width: 100%;
    margin-bottom: 0;
}
div#hero-banner-slide {
    margin-top: -46px;min-height: 540px;
}
.logos-slider .owl-nav.disabled {
  display: block;
}
.logos-slider {
  float: none;
  overflow: hidden;
    padding: 0 80px;
    width: 350px;
margin:-40px auto 0;
    padding-top: 20px;
}
.logos-slider .owl-stage-outer {
    overflow: hidden;    width: 130%;
    padding: 20px;
}
.logos-slider .owl-nav.disabled {
    display: block;
    position: absolute;
    left: 0;
    right: 0;
    width: 100%;
    top: 28%;
}
.firstActiveItem .logos-name{box-shadow: 0 0 30px rgba(0, 0, 0, 0.15);}
.firstActiveItem .logos-name::after, .logos-name:hover::after{display: none}
.logos-slider .owl-nav .owl-next{
    background: #fff  url(<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/arrow-publish.png) no-repeat !important;
    font-size: 0;
    width: 70px;
    right: -110px;    position: absolute;
    background-position: 10px 11px !important;
    height: 50px;    top: -9px;
    border-radius: 0;
  }
  .logos-slider .owl-nav .owl-prev{
    background: #fff url(<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/arrow-publish.png) no-repeat !important;
    font-size: 0;
    width: 70px;
    transform: rotate(180deg);   position: absolute;    top: -9px;
    left: -80px;height: 50px;
    background-position: 10px 22px !important;
  }
</style>
<!--==========================Hero Section============================-->
<section id="hero">
  <div class="container">
      <?php //if ($this->session->flashdata('message_error') != '') { ?>
<!--            <div id="message" class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php //echo $this->session->flashdata('message_error'); ?>
                </p>
            </div>-->
        <?php //} ?>
        <?php //if ($this->session->flashdata('message_success') != '') { ?>
<!--            <div id="message" class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p class="head-c">
                    <?php //echo $this->session->flashdata('message_success'); ?>
                </p>
            </div>-->
        <?php //} ?>
    <div class="row">
      <div class="col-md-6 col-lg-6">
        <!-- <h1>Unlimited Custom Graphic Designs Simple Flat Monthly Rate</h1>
          <h2>An online graphic design service that donesn't limit you. No hourly billing, No contracts, Cancel anytime</h2> -->
          <h1>Unlimited Graphic Designs <br/>Flat Monthly Rate</h1>
          <p>The best solution for all your creative design needs</p>
          <p>Dedicated Design Team, Unlimited Brands, No Contracts</p>
          <a href="<?php echo base_url(); ?>pricing" class="btn-get-started scrollto">Get Started Now</a>
          <div class="btns">
            <a class="fancybox fancybox.iframe" href="https://www.youtube.com/embed/VjHrU_jBmDY" rel="gallery nofollow"><span><i class="fas fa-play-circle"></i></span></a>
          </div>
        </div>
        <div class="col-md-6">
         <div id="hero-banner-slide"></div>
         <div class="logos-slider">
          <div class="logs-slider owl-carousel owl-theme">
            <div class="item" data-name="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Logo-&-Branding.jpg">
              <div class="logos-name" >
                <h3>Logo & Branding</h3>  
              </div>
            </div>
            <div class="item" data-name="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Tshirt-Design.jpg">

              <div class="logos-name" >
                <h3>T-Shirt Designs</h3>  
              </div>
            </div>
            <div class="item" data-name="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Social-Media.jpg">
              <div class="logos-name" >
                <h3>Social Media Posts</h3>  
              </div>
            </div>
            <div class="item" data-name="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Website.jpg">
              <div class="logos-name" >
                <h3>Web & Mobile UI/UX</h3>  
              </div>
            </div>
            <div class="item" data-name="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Poster-&-Flyers.jpg">
              <div class="logos-name" >
                <h3>Flyers & Brochures</h3>  
              </div>
            </div>
            <div class="item" data-name="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Product.jpg">
              <div class="logos-name" >
                <h3>Product Packaging</h3>  
              </div>
            </div>
            <div class="item" data-name="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/banner/Print-Ready-files.jpg">
              <div class="logos-name" >
                <h3>Print Ready Files</h3>  
              </div>
            </div>
          </div>
          <script>
            jQuery(document).ready(function($) {
              jQuery('.logs-slider').owlCarousel({
                margin: 20,
                loop:true,
                autoplay:true,
                smartSpeed: 200,
                slideTransition: 'linear',
                autoplaySpeed: 1000,
                navigation : true,
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 1
                  },
                  992: {
                    items: 1
                  },
                  1100: {
                    items: 1
                  }
                }
              });

              checkClasses();
              var names = $('.logs-slider').find('.firstActiveItem > .item').attr('data-name');
              $("#hero-banner-slide").html("<img src='"+names+"'>");
              //console.log('nnn',names);

              jQuery('.logs-slider').on('translated.owl.carousel', function(event) {
                checkClasses();
                var names = $(this).find('.firstActiveItem > .item').attr('data-name');
                $("#hero-banner-slide").html("<img src='"+names+"'>");


              });
              jQuery('.logs-slider').on('click', '.item', function () {
                $('.owl-item').removeClass('firstActiveItem');
                $(this).parent().addClass('firstActiveItem');
                var names = $(this).attr('data-name');
                $("#hero-banner-slide").html("<img src='"+names+"'>");
              });

              function checkClasses(){
                var total = $('.logs-slider .owl-stage .owl-item.active').length;
                $('.logs-slider .owl-stage .owl-item').removeClass('firstActiveItem');


                $('.logs-slider .owl-stage .owl-item.active').each(function(index){
                  if (index === 0) {
                // this is the first one
                $(this).addClass('firstActiveItem');
              }
            });
              }


            });

          </script>
        </div>
      </div>
    </div>
  </div>
</section>

  <!--==========================
    Logo Slider Section
    ============================-->
    <div class="satelment">
      <div class="container">

      </div>
    </div>
  <!--==========================
    Get Started Section
    ============================-->

    <section id="get-started" class="text-center">
      <div class="container">
        <div class="trusted-by text-center">
          <h4>Trusted by some of the most exciting companies around the globe!</h4>
          <p>Rated 4.9/5 for quality, turnaround, and communication</p>
          <div class="trusted">
            <div class="inner-trusted">
              <ul class="all-logo">
                <li><img alt="goat grass" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/goat-grass.png">
                 <!--  <ul>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star grey-star"></i></li>
                    <li><i class="fas fa-star grey-star"></i></li>
                  </ul> -->
                </li>
                <li><img alt="Bread_Logo_Retina" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/Bread_Logo_Retina.png">
                  <!-- <ul>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star grey-star"></i></li>
                  </ul> -->
                </li>
                <li><img alt="enicode" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/enicode.png">
                  <!-- <ul>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star grey-star"></i></li>
                    <li><i class="fas fa-star grey-star"></i></li>
                  </ul>
                </li> -->
                <li><img alt="western-rockies" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/western-rockies.png" >
                  <!-- <ul>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star grey-star"></i></li>
                  </ul> -->
                </li>
                <li><img alt="green-gurd" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/green-gurd.png" >
                 <!--  <ul>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                  </ul> -->
                </li>
                <li><img alt="perfect-imprents" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/perfect-imprents.png" >
                 <!--  <ul>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                  </ul> -->
                </li>
                <li><img alt="ourbookkiper" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ourbookkiper.png" >
                 <!--  <ul>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                    <li><i class="fas fa-star"></i></li>
                  </ul> -->
                </li>
              </ul>
            </div>
          </div> 
        </div>
      </div>
    </section>

    <section class="old-new">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2 style="text-transform: none; margin-bottom: 40px;">How we have changed Graphic Designing</h2>
            <br/>
          </div>
        </div>
        <div class="row">
         <div class="col-md-6">
           <h2>Old way</h2>
           <p>Other Designs Firms</p>
           <img alt="old-way" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/old-way.png" class="center-block">
           <div class="emoji-sec">
             <img alt="sad-emoji" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/sad-emoji.png" class="center-block">
             <p>Time taking process & needs<span>
             different plateform</span></p>
           </div>
         </div>
         <div class="col-md-6">
           <h2>New way</h2>
           <p>Designs with Graphicszoo</p>
           <img alt="new-way" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/new-way.png" class="center-block">
           <div class="emoji-sec">
             <img alt="laugh-emoji" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/laugh-emoji.png" class="center-block">
             <p>Easy and under one <span>
             plateform</span></p>
           </div>
         </div>
       </div>
     </div>
   </section>




   <div class="exclusive-feature text-center">
    <h2>Features Built with You in Mind</h2>
    <p>Project management taken to the next level. See what makes us unique and why you will love working with us for your design needs. </p>

    <div class="owl-carousel owl-theme features_slider">
      <div class="item">
        <div class="feature-here">
          <img alt="no-contract" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/no-contract.png" class="center-block">
          <h3>No Contracts</h3>  
          <p>Pay month to month, upgrade/downgrade as you please and cancel anytime. 
            Flat Monthly Rate
          </p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="monthly-rate" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/monthly-rate.png" class="center-block">
          <h3>Flat Monthly Rate</h3>  
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="graphic-icon" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/graphic-icon.png" class="center-block">
          <h3>Dedicated Designer</h3>  
          <p>Work one-on-one with a dedicated designer who will take out the time to understand your business needs.</p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="simple-comunication" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/simple-comunication.svg" class="center-block">
          <h3>Simple Communication</h3>  
          <p>Chat directly with your design team via the design portal.</p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="Source-files" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/Source-files.svg" class="center-block">
          <h3>Source Files </h3>  
          <p>All designs include the source files you request.</p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="Unlimited-projects" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/Unlimited-projects.svg" class="center-block">
          <h3>Unlimited projects and revisions </h3>  
          <p>Submit as many projects as you have and request as many revisions as you need. We will work hard to make sure we get it done for you.</p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="brand-profile" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/brand-profile.svg" class="center-block">
          <h3>Brand Profiles</h3>  
          <p>Create brand profiles for each of your brands by uploading asset files such as logos, brand guidelines, and reference designs. </p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="link-share-icon" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/link-share-icon.svg" class="center-block">
          <h3>Link Sharing</h3>  
          <p>Share your designs with other team members and customers by sending a link of the design to them. </p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="white-label" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/white-label.svg" class="center-block">
          <h3>White Label</h3>  
          <p>Take our platform to the next level, by making it all yours.</p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="user-managment" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/user-managment.svg" class="center-block">
          <h3>User Management</h3>  
          <p>Add team members to your account as sub users and limit their permissions as you desire. </p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="email-share-icon" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/email-share-icon.svg" class="center-block">
          <h3>Email Requests</h3>  
          <p>Submit your requests via email and they will automatically get added to your dashboard.</p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="Interactive-revisions" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/Interactive-revisions.svg" class="center-block">
          <h3>Interactive Revisions</h3>  
          <p>With pinpoint accuracy, mark up your design drafts by using our click to comment feature to let you designer know exactly where you want changes. </p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="project-flow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/project-flow.svg" class="center-block">
          <h3>Project Flow Technology</h3>  
          <p>Our system is built with efficiency in mind. Your projects will automatically flow from one stage to the next so your designs are done in a timely manner. </p>
        </div>
      </div>
      <div class="item">
        <div class="feature-here">
          <img alt="project-priorty" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/project-priorty.svg" class="center-block">
          <h3>Project Priority </h3>  
          <p>Manage your requests by having the ability to re-prioritize design requests based on your business needs.</p>
        </div>
      </div>
    </div>
    <script>
      jQuery('.features_slider').owlCarousel({
        autoplayHoverPause: true,
        margin: 20,
        loop:true,
        autoplay:true,
        dots:false,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 2
          },
          992: {
            items: 4  
          }
        }
      });

    </script>
    <a rel="canonical" href="<?php echo base_url(); ?>features" class="red-theme-btn">View More
      <img alt="long-arrow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-fluid"></a>

    </div>

    <section class="howWeWork">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2>How We Works</h2>
            <p>Checkout our simple and fastest work flow</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-5">
            <div class="nav nav-tabs nav-fill"  id="nav-tab" role="tablist">
              <a href="#submit-rq" class="active" data-toggle="tab" role="tab" aria-selected="true">
                <h3>1. Submit Project <i class="fas fa-caret-right"></i></h3>
                <p>Create a new project by simply adding your design details such as name, description, file formats you would like to receive, and any examples you may have.</p>
              </a>
              <a href="#feedback-rev" data-toggle="tab" role="tab" aria-selected="false">
                <h3>2. Feedback and Revisions <i class="fas fa-caret-right"></i></h3>
                <p>Once you receive your initial drafts let us know if there are any revisions you need by clicking anywhere on the design to put a feedback comment. Do this as many times as you need. </p>
              </a>
              <a href="#approve-rq" data-toggle="tab" role="tab" aria-selected="false">
                <h3>3. Approve and Download <i class="fas fa-caret-right"></i></h3>
                <p>When you get the design as you had imagined, just click approve and download the source files requested.</p>
              </a>
            </div>
          </div>
          <div class="col-md-7">
           <div class="tab-content" id="nav-tabConten">
             <div class="tab-pane fade show active" role="tabpanel"  id="submit-rq">
              <img alt="submit-project" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/submit-project.png" class="img-fluid">
            </div>
            <div role="tabpanel" class="tab-pane fade" id="feedback-rev">
              <img alt="submit-rq" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/submit-rq.png" class="img-fluid">
            </div>
            <div role="tabpanel" class="tab-pane fade" id="approve-rq">
              <img alt="approve-download" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/approve-download.png" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="col-md-12 text-center">
         <a href="<?php echo base_url(); ?>pricing" class="red-theme-btn">Get Started Now
          <img alt="long-arrow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-fluid"></a>
        </div>
      </div>
    </div>
  </section>
  <!--==========================
    Our Top Features
    ============================-->
    <section class="top-feature">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- <h2>Logos, Websites, Social Media Posts<br/>
            Brochures, T-Shirts & more!</h2> -->
            <h2>Our Works </h2>
            <p class="sub-heading">We can create all types of graphic designs. From logos, brochures, flyers, web pages, t-shirts, illustrations to much more. Take a look at some of our work!</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="port-text">
              <img alt="ptf2" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ptf2.jpg" class="img-fluid">
              <div class="hover-txt"> <a href="<?php echo base_url(); ?>portfolio">View More Logos</a></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="port-text">
              <img alt="ptf3"  src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ptf3.jpg" class="img-fluid">
              <div class="hover-txt"> <a href="<?php echo base_url(); ?>portfolio">View More Branding</a></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="port-text">
              <img alt="ptf4" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ptf4.jpg" class="img-fluid">
              <div class="hover-txt"> <a href="<?php echo base_url(); ?>portfolio">View More Print</a></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="port-text">
              <img alt="ptf5" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ptf5.jpg" class="img-fluid">
              <div class="hover-txt"> <a href="<?php echo base_url(); ?>portfolio">View More Web & Monile Application</a></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="port-text">
              <img alt="shirt1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/a-portfolio/t-shirt1.jpg" class="img-fluid">
              <div class="hover-txt"> <a href="<?php echo base_url(); ?>portfolio">View More T-Shirts</a></div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="port-text">
              <img alt="ptf7" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/ptf7.jpg" class="img-fluid">
              <div class="hover-txt"> <a href="<?php echo base_url(); ?>portfolio">View More Packing Labels</a></div>
            </div>
          </div>
          <div class="col-md-12 text-center m-t-50">
           <a href="<?php echo base_url(); ?>portfolio" class="red-theme-btn">View More
            <img alt="long-arrow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-fluid"></a>
          </div>
        </div>
      </div>
    </section> 



    <!------ testomonial-sec-------->
    <section class="testomonial-sec">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
           <h2>What others have to say about us</h2>
           <p>Our customers love us and they have shared a few reviews to say just how much.
            <br/>Take a look at some of the awesome feedback we have received.
          </p>
        </div>
      </div>
    </div>
    <div class="testomonial-out">
      <div class="testomonial owl-carousel owl-theme">
        <div class="item">
          <div class="comment-here">
            <img alt="post4" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post4.png" class="img-fluid no_bg">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star full"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
              <h4>Patrick Black </h4> 
              <p>President/CEO, <a href="www.perfectimprints.com">Perfect Imprints</a></p>
            </div>
            <p class="comment5">
              <?php
              echo substr("Graphics Zoo has been amazing to work with for my graphic design needs. They are fast and the designs are always on point. They do a great job following directions I provide, and the design queue makes it easy for me to prioritize all my design jobs needed. Their service has become an invaluable resource for my company!",0,150)."...";
              ?>

            </p>
            <a href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png"></a>

          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img alt="post2" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post2.png" class="img-fluid dark">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
              <h4>Shea Bramer</h4> 
              <p>Marketing Coordinator
                <a href="http://www.westernrockiesfcu.org/">westernrockiesfcu.org
                </a>
              </p>
            </div>
            <p class="comment5">
              <?php
              echo substr("We are a small marketing team for a large financial company and new growth in the company meant that the graphic design workload was becoming overwhelming for our team. Graphicszoo created an easy way for us to keep up with the workload as our company continues to grow. They pay close attention to our branding, and are very detail oriented (as I can be quite demanding). The work they provide for us is always very high quality and I always get what I'm asking for. Their prices are also very competitive. We highly suggest this company!",0,150)."...";
              ?>

            </p>
            <a href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png"></a>

          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img alt="post3" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post3.png" class="img-fluid light">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
              <h4>Skylar Domine </h4> 
              <p><a href="https://goatgrasscbd.co/">Goat Grass Co</a></p>
            </div>
            <p class="comment5">
              <?php
              echo substr("I've been through dozens of design teams and none of which ever checked off all the boxes- price, turnaround time, consistency, and accuracy.  I'm actually 'wowed' by this company and will have my loyalty for years to come from my circle. Highly recommend to anyone looking for design work. If you can visualize it then they can create it!",0,150)."...";
              ?>

            </p>
            <a href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img alt="long-arrow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png"></a>
          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img alt="green-gurd" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/green-gurd.png" class="img-fluid no_bg">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
              </div>
              <h4>Nathan Leathers - Owner</h4> 
              <p>Green Gaurd, Printing and Apparel</p>
            </div>
            <p class="comment5">
              <?php
              echo substr("Had another competitor prior to this, and within 3 days Graphics Zoo has far exceeded any level of service I had with the previous competitor in 2 years.  They are very responsive and so far fast and solid production of content!",0,150)."...";
              ?>

            </p>
            <a href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img alt="long-arrow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png"></a>
          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img alt="focal-shift" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/focal-shift.png" class="img-fluid no_bg">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
              </div>
              <h4>Thomas Elliott Fite</h4> 
              <p>Focal Shift </p>
            </div>
            <p class="comment5">
              <?php
              echo substr("Pretty impressed! Been with them for several months now and we've finally found our work flow with them! Great job GZ and keep up the great work!",0,150)."...";
              ?>

            </p>
            <a href="<?php echo base_url(); ?>testimonials" class="red-theme-btn">Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png"></a>
          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img alt="scott" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/scott.png" class="img-fluid no_bg">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
              </div>
              <h4>Scott Levesque</h4>
              <p> Be Free Clothing Company</p> 
            </div>
            <p class="comment5">
              <?php
              echo substr("Great designs, quick turnaround and awesome to work with. Thank you.",0,150)."...";
              ?>

            </p>
            <a href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img alt="long-arrow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png"></a>

          </div>
        </div>

      </div>
      <script>
       jQuery('.testomonial').owlCarousel({
        loop: true,
        margin: 20,
        items:3,
        nav: true,
        dots:false,
        autoplay:true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items:2
          },
          992: {
            items: 3
          }
        }
      });
    </script>
  </div>
</section>

<!------ testomonial-sec end-------->


<!------ Risk free-sec -------->
<section class="riskFree-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
       <h3>Try Graphics Zoo Risk-Free For 14 Days</h3>
       <a href="<?php echo base_url(); ?>pricing" class="red-theme-btn" rel="nofollow">Get Started Now
        <img alt="red-long-arrow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/red-long-arrow.png" class="img-fluid"></a>

      </div>
    </div>
  </div>
</section>

<!------/ Risk free-sec -------->

<section class="resource-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
       <h2>Graphics Zoo Blogs and Resources</h2>
       <p>Stay in the know on the newest graphic design trends by accessing our constantly 
         updated <br/>resources and guides for all business types.</p>
       </div>
     </div>
     <?php //echo "<pre>";print_r($blog_data); ?>
     <div class="row">
      <?php for($i = 0;$i<3; $i++) {
        $created = new DateTime($blog_data[$i]['created']); 
        $today = new DateTime(date('Y-m-d h:i:s')); 
        $diff = $today->diff($created)->format("%a");
        ?>
        <div class="col-md-4 item">
          <div class="resource-here">
            <div class="image_container">
              <img alt="<?php echo $blog_data[$i]['image']; ?>" src="<?php echo FS_UPLOAD_PUBLIC_UPLOADS; ?>blogs/<?php echo $blog_data[$i]['image']; ?>" class="center-block">

            </div> 

            <a href="<?php echo base_url(); ?>view_blog/<?php echo $blog_data[$i]['id']; ?>"><?php echo $blog_data[$i]['title']; ?></a>
            <p><?php echo $diff; ?> days ago</p>
          </div>
        </div>
      <?php } ?>
<!--      <div class="col-md-4 ">
        <div class="resource-here">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/blog2.jpg" class="center-block">
          <a href="#">Advertising Slogans for Electronics Online 
          Store</a>
          <p>6 days ago</p>
        </div>
      </div>
      <div class="col-md-4 ">
        <div class="resource-here">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/blog3.jpg" class="center-block">
          <a href="#">Advertising Slogans for Electronics Online 
          Store</a>
          <p>6 days ago</p>
        </div>
      </div>-->
      <a href="<?php echo base_url()."blog" ?>" rel="nofollow" class="red-theme-btn">View More   <img alt="long-arrow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-fluid"></a> 
    </div>

  </div>
</div>
</section>

