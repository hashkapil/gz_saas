<section class="page-heading-sec pricing-header">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Our main features that make your life easier</h1>
				<p>Check out some of the amazing functions we have added to the #1 project management system.</p>
			</div>
		</div>
	</div>
</section>

<section class="feature-section">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="feature-txt" >
          <div class="feature_img_container">
            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/features/brand-profile.png" class="center-block" alt="brand-profile">
          </div>
          <div class="feature_content">
            <h3>Brand Profiles</h3>  
            <p>Rather than having to upload the logo, brand guidelines, reference files each time with a request, you can build out the brand profiles for each of the separate brands you work with. Then when submitting a request, it’s as simple as selecting the brand you are requesting a design for, so the designer right away has all information needed to stay within the brand guidelines.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="feature-txt" >
          <div class="feature_img_container">
            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/features/email-request.png" class="center-block" alt="email-request">
          </div>
          <div class="feature_content">
            <h3>Email Requests</h3>  
            <p>Submit your requests via email and they will automatically get added to your dashboard. All you have to do is send an email to newrequest@graphicszoo.com with the details of your project and let us handle it from there.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="feature-txt" >
          <div class="feature_img_container">
            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/features/user-managment.png" class="center-block" alt="user-managment">
          </div>
          <div class="feature_content">
            <h3>User Management</h3>
            <p>Add team members to your account as sub users to your main account. You can set permissions for each of your sub users so they have full admin rights or be limited to certain options such as view only, ability to comment, ability to download, or can only see projects of specific brands that you have setup.</p> 
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="feature-txt" >
          <div class="feature_img_container">
            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/features/link-sharing.png" class="center-block" alt="link-sharing">
          </div>
          <div class="feature_content">
           <h3>Link Sharing</h3>  
           <p>Share design drafts with any of your team members or customers by simply copying the link for the design. You can also set the permissions when sharing so the viewers can view only, have the ability to add comments, and/or download the designs.</p>
         </div>
       </div>

     </div>
     <div class="col-md-4">
      <div class="feature-txt" >
        <div class="feature_img_container">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/features/white-label.png" class="center-block" alt="white-label">
        </div>
        <div class="feature_content">
          <h3>White Label</h3>  
          <p>Take your company to the next level, by making our platform yours! You can customize the Graphics Zoo platform by adding in your logo, changing up the colors and adding in your own customers. This will allow you to efficiently manage your customers design requests and let them work with us one on one as your in-house design team.</p>
        </div>

      </div>
    </div>
    <div class="col-md-4">
      <div class="feature-txt" >
        <div class="feature_img_container">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/features/simple-communation.png" class="center-block" alt="simple-communation">
        </div>
        <div class="feature_content">
          <h3>Simple Communication</h3>  
          <p>Chat one on one with your designer and account manager. Put your comments directly on the design drafts page so your Design Team has clarity on your notes. Also, feel free to live chat us, email us or call us.</p>
        </div>
      </div>

    </div>
    <div class="col-md-4">
      <div class="feature-txt" >
        <div class="feature_img_container">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/features/source-fles.png" class="center-block" alt="source-fles">
        </div>
        <div class="feature_content">
          <h3>Source Files</h3>  
          <p>You have the ability to download all the source files you request for each design. Most common types of files include AI, PSD, PNG, DST and JPG. Also, the source files include fonts and asset files as needed based on the request type.</p>
        </div>
      </div>

    </div>
    <div class="col-md-4">
      <div class="feature-txt" >
        <div class="feature_img_container">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/features/interactive-revisions.png" class="center-block" alt="interactive-revisions">
        </div>
        <div class="feature_content">
          <h3>Interactive Revisions</h3>  
          <p>With pinpoint accuracy, mark up your design drafts by using our click to comment feature to let you designer know exactly what and where you want changes.</p>
        </div>
      </div>

    </div>

    <div class="col-md-4">
      <div class="feature-txt" >
        <div class="feature_img_container">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/features/project-priority.png" class="center-block" alt="project-priority">
        </div>
        <div class="feature_content">
          <h3>Project Priority </h3>  
          <p>Manage your requests by having the ability to re-prioritize design requests based on your business needs.</p>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="feature-txt row">
        <div class="feature_img_container lastflow col-md-8">
          <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/features/project-flow-technology.png" class="center-block" alt="project-flow-technology">
        </div>
        <div class="feature_content col-md-4">
          <h3>Project Flow Technology</h3>  
          <p>Our system is built with efficiency in mind. Your projects will automatically flow from one stage to the next so your designs are done in a timely manner.</p>
        </div>
      </div>
    </div>
  </div>
</div>
</section>

<div class="clearfix"></div>

    <!------ testomonial-sec-------->
    <section class="testomonial-sec">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
           <h2>What others have to say about us</h2>
           <p>Our customers love us and they have shared a few reviews to say just how much.
            <br/>Take a look at some of the awesome feedback we have received.
          </p>
        </div>
      </div>
    </div>
    <div class="testomonial-out">
      <div class="testomonial owl-carousel owl-theme">
        <div class="item">
          <div class="comment-here">
            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post4.png" class="img-fluid no_bg" alt="post4">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star full"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
              <h4>Patrick Black </h4> 
              <p>President/CEO, Perfect Imprints</p>
            </div>
            <p class="comment5">
              <?php
              echo substr("Graphics Zoo has been amazing to work with for my graphic design needs. They are fast and the designs are always on point. They do a great job following directions I provide, and the design queue makes it easy for me to prioritize all my design jobs needed. Their service has become an invaluable resource for my company!",0,150)."...";
              ?>

            </p>
            <a rel="canonical" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" alt="long-arrow"></a>

          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post2.png" class="img-fluid dark" alt="post2">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
              <h4>Shea Bramer</h4> 
              <p>Marketing Coordinator, Western Rockies
              </p>
            </div>
            <p class="comment5">
              <?php
              echo substr("We are a small marketing team for a large financial company and new growth in the company meant that the graphic design workload was becoming overwhelming for our team. Graphicszoo created an easy way for us to keep up with the workload as our company continues to grow. They pay close attention to our branding, and are very detail oriented (as I can be quite demanding). The work they provide for us is always very high quality and I always get what I'm asking for. Their prices are also very competitive. We highly suggest this company!",0,150)."...";
              ?>

            </p>
            <a rel="canonical" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" alt="long-arrow"></a>

          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img alt="post3" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/post3.png" class="img-fluid light">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>
              <h4>Skylar Domine </h4> 
              <p>Goat Grass Co</p>
            </div>
            <p class="comment5">
              <?php
              echo substr("I've been through dozens of design teams and none of which ever checked off all the boxes- price, turnaround time, consistency, and accuracy.  I'm actually 'wowed' by this company and will have my loyalty for years to come from my circle. Highly recommend to anyone looking for design work. If you can visualize it then they can create it!",0,150)."...";
              ?>

            </p>
            <a rel="canonical" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" alt="long-arrow"></a>
          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img alt="green-gurd1" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/green-gurd1.jpg" class="img-fluid no_bg" alt="green-gurd1">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
              </div>
              <h4>Nathan Leathers - Owner</h4> 
              <p>Green Gaurd, Printing and Apparel</p>
            </div>
            <p class="comment5">
              <?php
              echo substr("Had another competitor prior to this, and within 3 days Graphics Zoo has far exceeded any level of service I had with the previous competitor in 2 years.  They are very responsive and so far fast and solid production of content!",0,150)."...";
              ?>

            </p>
            <a rel="canonical" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" rel="nofollow">Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" alt="long-arrow"></a>
          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/focal-shift.png" class="img-fluid no_bg" alt="focal-shift">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
              </div>
              <h4>Thomas Elliott Fite</h4> 
              <p>Focal Shift </p>
            </div>
            <p class="comment5">
              <?php
              echo substr("Pretty impressed! Been with them for several months now and we've finally found our work flow with them! Great job GZ and keep up the great work!",0,150)."...";
              ?>

            </p>
            <a rel="canonical nofollow" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn" >Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" alt="long-arrow"></a>
          </div>
        </div>
        <div class="item">
          <div class="comment-here">
            <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/scott.png" class="img-fluid no_bg" alt="scott">
            <div class="header-part">
              <div class="star-list"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
              </div>
              <h4>Scott Levesque</h4>
              <p>Be Free Clothing Company</p> 
            </div>
            <p class="comment5">
              <?php
              echo substr("Great designs, quick turnaround and awesome to work with. Thank you.",0,150)."...";
              ?>

            </p>
            <a rel="canonical nofollow" href="<?php echo base_url(); ?>testimonials" class="red-theme-btn">Read more<img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" alt="long-arrow"></a>

          </div>
        </div>

      </div>
  </div>
</section>

<!------ testomonial-sec end-------->

<div class="clearfix"></div>
<!------ Risk free-sec -------->
<section class="riskFree-sec pricing-risk">
 <div class="container">
  <div class="row">
   <div class="col-md-12 text-center">
    <h2>Join more than 500+ customers</h2>
    <h3>Try Graphics Zoo Risk-Free For 14 Days</h3>
    <a rel="canonical nofollow" href="<?php echo base_url(); ?>pricing" class="red-theme-btn">Get Started Now
     <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/red-long-arrow.png" class="img-fluid" alt="red-long-arrow"></a>
   </div>
 </div>
</div>
</section>

<!------/ Risk free-sec -------->





<section class="faq-sec">
  <div class="container">
   <div class="row">
    <div class="col-md-12 text-center">
     <h2>Frequently Asked Question</h2>
   </div>
 </div>
 <div id="accordion" class="faq-accordion">
  <div class="row">
   <div class="col-md-6">
    <div class="accordin-outer">
     <div class=" collapsed" data-toggle="collapse" href="#collapseOne">
      <a class="card-title">
       <h4>Do I really get unlimited designs?</h4>
     </a>
   </div>
   <div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
    <p>Yes! You really get unlimited designs and revisions for a flat monthly rate. You can request as many revisions as you like until you are completely satisfied. We work on 1 new request a day.</p>
  </div>
</div>
<div class="accordin-outer">
 <div class=" collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2">
  <a class="card-title">
   <h4>What does Risk-Free guarantee really mean?</h4>
 </a>
</div>
<div id="collapse2" class="card-body collapse" data-parent="#accordion" >
  <p>If you do not approve any of your designs or graphics within the first 14 days of
   your trial, we will give you a full refund, no questions asked.
 </p>
</div>
</div>
<div class="accordin-outer">
 <div class=" collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
  <a class="card-title">
   <h4>What if I need 220 designs per day?</h4>
 </a>
</div>
<div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
  <p>If you know your design volume will be higher than the active requests per day is,
  we recommend you get additional accounts so your design volume can be handled properly.</p>
</div>
</div>
</div>
<div class="col-md-6">
  <div class="accordin-outer">
   <div class=" collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
    <a class="card-title">
     <h4>How long does it take to receive designs?</h4>
   </a>
 </div>
 <div id="collapseThree" class="card-body collapse" data-parent="#accordion" >
  <p>Every design request is serviced within 1 business day. Please keep in mind though,
   that we value quality over everything else. Your designer will be in constant communication
 with you and will keep you updated on the status of your designs. </p>
</div>
</div>
<div class="accordin-outer">
 <div class=" collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
  <a class="card-title">
   <h4>What can I get designed?</h4>
 </a>
</div>
<div id="collapse3" class="card-body collapse" data-parent="#accordion" >
  <p>You can get anything designed from your designer. Everything from banners,
   flyers, t-shirts, book covers and much more. Just keep in mind larger and more 
 complicated designs may take a little longer to finish.</p>
</div>
</div>
<div class="accordin-outer">
 <div class=" collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4">
  <a class="card-title">
   <h4>Is there really no contract?</h4>
 </a>
</div>
<div id="collapse4" class="card-body collapse" data-parent="#accordion" >
  <p>Yes, that's right! No contract, so you can cancel at anytime.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
