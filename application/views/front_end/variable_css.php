<?php
$CI =& get_instance();
$CI->load->library('myfunctions');
//echo $approve_bg_color;exit;
?>
<style>
:root {
	--theme-primary-color: <?php echo $themePrimaryColor; ?>; 
	--theme-secondary-color:<?php echo $secondary_color; ?>;


	--theme-primary-font-size:<?php echo $font_size; ?>;
	--theme-heading-font-size:<?php echo $title_font_size; ?>;
	--theme-primary-font-family:<?php echo $font_fmly; ?>;


	--theme-header-bg-color:<?php echo "#ffffff"; ?>;
	--theme-inprogress-bg-color:<?php echo $inprog_bkg_color; ?>;
	--theme-revision-bg-color:<?php echo $revsion_bkg_color; ?>;
	--theme-review-bg-color:<?php echo $review_bkg_color; ?>;
	--theme-completed-bg-color:<?php echo $complete_bkg_color; ?>;
	--theme-approve-bg-color:<?php echo $approve_bg_color; ?>;
	--theme-setting-tab-bg-color:<?php echo $setting_tab_bkg_color; ?>;
	--theme-inqueue-bg-color:<?php echo $inqueue_bkg_color; ?>;
	--theme-hold-bg-color:<?php echo $hold_bkg_color; ?>;
	--theme-cancel-bg-color:<?php echo $cancel_bkg_color; ?>;
	--theme-draft-bg-color:<?php echo $draft_bkg_color; ?>;
	

	
	--theme-inprogress-color:<?php echo $inprog_font_color; ?>;
	--theme-revision-color:<?php echo $revsion_font_color; ?>;
	--theme-review-color:<?php echo $review_font_color; ?>;
	--theme-completed-color:<?php echo $complete_font_color; ?>;
	--theme-inqueue-color:<?php echo $inqueue_font_color; ?>;
	--theme-hold-color:<?php echo $hold_font_color; ?>;
	--theme-cancel-color:<?php echo $cancel_font_color; ?>;
	--theme-draft-color:<?php echo $draft_font_color; ?>;
	--theme-success-message-color:<?php echo $success_msg_color; ?>;
	--theme-error-message-color:<?php echo $error_msg_color; ?>;
	--theme-success-message-bg-color:<?php echo $success_msg_bg_color; ?>;
	--theme-error-message-bg-color:<?php echo $error_msg_bg_color; ?>;
	
/* 	--shadowColor: 0px 10px 50px 0px rgba(var(--theme-primary-color), 0.08); */

}

/* div#agency_user h2.main-info-heading, .catagry-heading h3, .d-progress.rheight-xt .head-b, .design-draft-header .head-b, .descol.bottom-info h3.head-b{font-size: var(--theme-heading-font-size)}
div#no-more-tables tr td, .catagory-right label, .color-inside h4, .review-textcolww
{font-size: var(--theme-primary-font-size)} */

.nav-section{background:var(--theme-header-bg-color);}


.numcircle-box, span.count_chatmsg, input.btn-red, .btn-red, .line-box .line,
.billing_plan .flex-grid .chooseplan.active a, .theme-save-btn, 
.billing_plan_view p.btn-x .btn-f.chossbpanss, .btn-x .btn-f,
.list-setting li .dropdown-menu h3.head-notifications, .ad_new_reqz .submit, .form-radion2 .containerr input:checked ~ .checkmark, 
.sound-signal input[type="radio"]:checked + label:before, .pro-box-s1:hover .pro-add-boxss2, .submit:hover, .btn-x .btn-g, .adnewpp, .red-theme-btn:hover, .review-textcolww a.showbrand_profile, .review-textcolww a.showbrand_profile i, .sharing-popup .tabsetting li a:hover, .sharing-popup .tabsetting li.active a:hover, .sharing-popup .tabsetting li.active a, .similar-prop .btn.btn-ydelete, .add-lining a.add_more_button, a.add_more_colors, a.remove_field, input#brand_profile_submit, .btn-x .btn-g:hover, .submit:hover, .pstbtns .pstbtns-a, .comment-box-es .cmmbxonline, .accimgbx33 p.cross-btnlink a, .red-theme-btn, ul.h-c-r li li a span, .header_footer_button a:hover, .switch-custom-usersetting-check input:checked + label:after, li.for_agency_user.stayhere, a.new-brand, .upload-gallery button.close, .upload-gallery .search-container .select-search button.serach_btn, .btn-x .btn-e, a.active_blling_acont, .nav-section .navbar-default .navbar-toggle .icon-bar, .tab-toggle span, .no_found .conct_btn a, .uploadAnDsave button, i.checkmark_pop.selected.fa.fa-check, .upload-gallery #loadMore, .show_all_folders .creat_btn_css ul li a:hover, .section_title_header a.create_directory.upldflsfrmcstom, .all_request_file .dropdown-menu li:hover, .all_request_files .dropdown-menu li:hover, .upload_btn_cst a.upld_brand, .affiliated-btn, body .four-bg, a.add_more_button, a.add_more_script, .add_more_colors, .inner-sub-section .agncy_stng_sidebar ul.list-header-blog li a::before
{
	background-color: var(--theme-primary-color);
}
.similar-prop .btn.btn-ndelete, .site-log-btn a
{  background-color: var(--theme-secondary-color );}

.four-four .error-info h1, .error-info h2{ color: var(--theme-secondary-color );}



.list-header-blog > li.active a, .list-header-blog > li.active a:hover, .list-header-blog > li:not(.for_agency_user) a:hover,  .pro-box-s1:hover .pro-box-caps1 .pro-add-texts1, .white-boundries .chooseplan.active h2, .white-boundries .chooseplan:hover h2, .email_preview .emailPreview, .sharing-popup input.pro_name, h2.popup_h2.del-txt, .text-mb strong, .despeginate-box a.despage, table#shareduser td a.del_shareduser, .create_account_link:hover, .list-setting > li a:hover, ul.h-c-r li li a, ul.h-c-r li li a:hover, .header_footer_button a, .white-boundries .project-row-qq1.settingedit-box h4, .sidebar-header li.active a, a.new-subuser, button.send_test_email_agency, .addPlusbtn a, p.fill-sub a.livechatpopup, select#cat_page, select#cat_page, .btn-x .btn-e:hover, .theme-color, .fill-sub a, .client-edition .main-discp h3, form#subscrptn_frm .f_toggle_plan_active+p.label-txt, .list-setting li .dropdown-menu.profile-dropdown .profile_wrapper ul li a:hover, .list-grid a:hover, .list-grid a.active, .no_found .conct_btn a, .customer_designer_directory a:hover, .customer_designer_directory a.active, .show_all_folders .dropdown a.uplod_crt_new, .create_account_link:hover, .create_account_link, .site-log-btn a
{
	color: var(--theme-primary-color) !important;
}
input.btn-blue{
	background-color: var(--theme-setting-tab-bg-color);
}
.list-setting .dropdown-menu:before
{border-bottom-color:  var(--theme-primary-color)}

.pro-box-s1:hover, .billing_plan_view p.btn-x .btn-f.chossbpanss, .email_preview .emailPreview, .btn-x .btn-g:hover, .accimgbx33.add:hover, .sharing-popup input.pro_name, .btn-x .btn-g, .sharing-popup .tabsetting li a:hover, .sharing-popup .tabsetting li.active a:hover, .sharing-popup .tabsetting li.active a, .red-theme-btn, .red-theme-btn:hover, .header_footer_button a, .sidebar-header li.active a, .switch-custom-usersetting-check input:checked + label, .btn-x .btn-e, .btn-x .btn-e:hover, .upload-gallery .thumbnil-img li::before, .customer_designer_directory a:hover, .customer_designer_directory a.active
{border-color:var(--theme-primary-color) }

.nav-section .navbar-nav > li > a:hover, .nav-section .navbar-nav > li.active > a, .nav-section .navbar-nav > li.current-menu-parent > a, .nav-section .navbar-nav > li.current-menu-item > a, .nav-section .navbar-default .navbar-nav > li > a:active, .nav-section .navbar-default .navbar-nav > li > a:focus, .nav-section .navbar-default .navbar-nav > li > a:hover{color: var(--theme-primary-color);}
.nav-section .navbar-nav > li.active > a svg path, .nav-section .navbar-nav > li:hover > a svg path{fill: var(--theme-primary-color);}
li.active-result.result-selected::after, .sound-signal input[type="radio"] + label:before, .radio-box-xx2 input:checked ~ .check-main-xx3{border:2px solid var(--theme-primary-color);}

.pro-ject-rightbar .cmmtype-box, .add_profordesign .pro-box-s1:hover
{border:1px solid  var(--theme-primary-color); }
.red-theme-btn:hover{background-image: none;}

.red-theme-btn:hover{   
    box-shadow: 0px 0px 22px -4px var(--theme-primary-color);
}

/**  Status button  **/
.agency_inprogress{ background:var(--theme-inprogress-bg-color); color: var(--theme-inprogress-color); }
.agency_review{ background:var(--theme-review-bg-color); color: var(--theme-review-color);}
.agency_revision{background: var(--theme-revision-bg-color); color: var(--theme-revision-color);}
.completed_status_button{color: var(--theme-completed-color);background: var(--theme-completed-bg-color);}
.inqueue_status_button{color: var(--theme-inqueue-color);background: var(--theme-inqueue-bg-color);}
.hold_status_button{color: var(--theme-hold-color);background: var(--theme-hold-bg-color);}
.cancel_status_button{color: var(--theme-cancel-color);background: var(--theme-cancel-bg-color);}
.draft_status_button{color: var(--theme-draft-color);background: var(--theme-draft-bg-color);}

input.btn-red:hover{box-shadow:1px 7px 10px rgba(0, 0, 0, 0.2);}
.email_preview .emailPreview:hover, .btn-x .btn-g:hover{color: #fff;}
.alert-success{background: var(--theme-success-message-bg-color); color: var(--theme-success-message-color);}
.alert-danger{background:var(--theme-error-message-bg-color) ; color:var(--theme-error-message-color); }
.nav-section .navbar-nav>li>a:hover svg, .nav-section .navbar-nav>li.active>a svg{fill:var(--theme-primary-color)}
</style>
