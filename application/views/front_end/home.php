<style>
.mainright:before, .mainright:after {
    content: '';
    display: none;
}
@media screen and (max-width: 1580px)
.mainright {
    background-size: 60% 100%;
}
.mainright {
    /*background: url(<?php echo base_url(); ?>public/new_fe_images/mainbanner.png) no-repeat right top;*/
    background: url(<?php echo base_url(); ?>public/new_fe_images/new-banner-1.png) no-repeat right bottom;
    position: relative;
}
@media (max-width: 991px) {
    .bannermain{
        background: none;
    }
}
</style>
<!-- Popup Start -->
<div class="modal fade popupall popupsignup" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h3 class="text-center dblue">Sign up for our newsletter<br />and get 10% off!</h3>
                <p>Just enter your email here</p>
                <form class="formmain">
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Your Email Address">
                    </div>
                    <button type="submit" class="btn btn1 btnfull capitalize">Submit</button>
                    <div class="clear sep"></div>
                    <div><a href="#" class="link" data-dismiss="modal">No, Thanks!</a></div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Popup End -->


<section class="bannermain mainright">

    <div class="container-fluid">
        <div class="row row-no-padding">
            <div class="col-sm-12 hidden-md hidden-lg">
                <div class="bannermainmob"><img src="<?php echo base_url(); ?>public/new_fe_images/portfoliomain.jpg" class="img-responsive img" alt="main banner" /></div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-5 col-lg-5">
                <div class="leftpart">
                    <h1 class="dblue">Unlimited custom graphic designs</h1>
                    <h2>Unlimited revisions <br />1-day turnaround</h2>
                    <h2>1 flat monthly rate</h2>
                    <div class="sep"></div>
                    <a href="<?php echo base_url(); ?>pricing" class="btn btn1">Get Started Now </a>
                </div>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7">
                &nbsp;
            </div>

        </div>
    </div>
</section>

<section class="brandlogo-main">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <div class="row">
                    <div class="col-xs-6 col-sm-3 col-md-3">
                        <div class="brandlogo-box"><div><img src="<?php echo base_url(); ?>public/new_fe_images/company-logo1.png" class="img-responsive img" alt="" /></div></div>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3">
                        <div class="brandlogo-box"><div><img src="<?php echo base_url(); ?>public/new_fe_images/company-logo2.png" class="img-responsive img" alt="" /></div></div>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3">
                        <div class="brandlogo-box"><div><img src="<?php echo base_url(); ?>public/new_fe_images/company-logo3.png" class="img-responsive img" alt="" /></div></div>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3">
                        <div class="brandlogo-box"><div><img src="<?php echo base_url(); ?>public/new_fe_images/company-logo4.png" class="img-responsive img" alt="" /></div></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="designscreate-main">
    <div class="container-fluid">
        <div class="row row-no-padding d-flex">

            <div class="col-md-6 col-lg-7">
                <div class="row">

                    <div class="col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-2">
                        <div class="lefttpart">
                            <div class="row">
                                <div class="col-xs-6 col-sm-4 col-md-6">
                                    <div class="boxes">
                                        <div class="media">
                                            <div class="media-left">
                                                <img class="media-object img" src="<?php echo base_url(); ?>public/new_fe_images/no-contract.png" alt="no contract">
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">No Contract</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-xs-6 col-sm-4 col-md-6">
                                    <div class="boxes">
                                        <div class="media">
                                            <div class="media-left">
                                                <img class="media-object img" src="<?php echo base_url(); ?>public/new_fe_images/source-files.png" alt="Source Files">
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Source Files<br />Included</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-6 col-sm-4 col-md-6">
                                    <div class="boxes">
                                        <div class="media">
                                            <div class="media-left">
                                                <img class="media-object img" src="<?php echo base_url(); ?>public/new_fe_images/unlimited-designs.png" alt="Unlimited Designs">
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Unlimited Designs Requests and Revisions</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-6">
                                    <div class="boxes">
                                        <div class="media">
                                            <div class="media-left">
                                                <img class="media-object img" src="<?php echo base_url(); ?>public/new_fe_images/dedicated-designer.png" alt="Dedicated Designer">
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Dedicated Designer</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-xs-6 col-sm-4 col-md-6">
                                    <div class="boxes">
                                        <div class="media">
                                            <div class="media-left">
                                                <img class="media-object img" src="<?php echo base_url(); ?>public/new_fe_images/communication.png" alt="Communication">
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Simple Communication<br />Platform</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-6">
                                    <div class="boxes">
                                        <div class="media">
                                            <div class="media-left">
                                                <img class="media-object img" src="<?php echo base_url(); ?>public/new_fe_images/flat-rate.png" alt="Flat Rate">
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Flat Rate Price</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-6 col-lg-5 rightpartbg">
                <div class="rightpart">
                    <h1>Designs we <br />create</h1>
                    <h4>Types of designs we create</h4>
                    <p>We are an established unlimited graphic design company and we create all types of designs for our customers. Anything you can image for your graphic design needs, we can certainly help create that. The categories are only some of our popular items that we work on, but nothing is too far fetched for our team.</p>

                    <div class="sep"></div>
                    <a href="<?php echo base_url(); ?>portfolio" class="btn btn2">SEE OUR PORTFOLIO</a>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="workflow-main">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <h1 class="blue text-center">Simple Workflow and Communication Tool</h1>
            </div>

            <div class="col-md-12">

                <div class="Workflow-slider">
                    <div class="carousel slide vertical" id="carousel-vertical" data-ride="carousel">

                        <div class="row">

                            <div class="col-md-7 col-md-push-5 col-lg-8 col-lg-push-4">
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <img src="<?php echo base_url(); ?>public/new_fe_images/send-requests.png" class="img-responsive img" alt="...">
                                    </div>
                                    <div class="item">
                                        <img src="<?php echo base_url(); ?>public/new_fe_images/comment-on-revisions.png" class="img-responsive img" alt="...">
                                    </div>
                                    <div class="item">
                                        <img src="<?php echo base_url(); ?>public/new_fe_images/approve-designs.png" class="img-responsive img" alt="...">
                                    </div>
                                    <div class="item">
                                        <img src="<?php echo base_url(); ?>public/new_fe_images/chat-directly.png" class="img-responsive img" alt="...">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5 col-md-pull-7 col-lg-4 col-lg-pull-8">
                                <div class="leftpart">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carousel-vertical" data-slide-to="0" class="active">
                                            <div class="leftpartbox">
                                                <h4>Send design requests in just minutes</h4>
                                                <p>Whenever you are ready and need a graphic design created, answer the quick few questions to help your designer understand your needs.</p>
                                            </div>
                                        </li>

                                        <li data-target="#carousel-vertical" data-slide-to="1">
                                            <div class="leftpartbox">
                                                <h4>Comment for revisions on the fly </h4>
                                                <p>Once you receive your designs, use the point and click tool to leave comments for revisions so your perfect masterpiece can be created.</p>
                                            </div>
                                        </li>

                                        <li data-target="#carousel-vertical" data-slide-to="2">
                                            <div class="leftpartbox">
                                                <h4>Approve designs as they come</h4>
                                                <p>If you are satisfied with the designs, simply approve the design to receive your files and the request will be marked as completed and we start the next one.</p>
                                            </div>
                                        </li>

                                        <li data-target="#carousel-vertical" data-slide-to="3">
                                            <div class="leftpartbox">
                                                <h4>Chat directly with your designer </h4>
                                                <p>Sometimes it just better to always stay connected and we give you that piece of mind. Start a chat with your designer if something is just not clear enough.</p>
                                            </div>
                                        </li>
                                    </ol>

                                    <div class="leftpartbox">
                                        <a href="<?php echo base_url(); ?>pricing" class="btn btn3">Get Started Now</a>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>


            </div>

        </div>
    </div>
</section>

<section class="how-it-works">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="red text-center">How it Works</h1>
            </div>

            <div class="col-sm-4 col-md-4">
                <div class="how-works-outer">
                    <div class="imagebox">
                        <img src="<?php echo base_url(); ?>public/new_fe_images/submit-request.png" class="img-responsive img" alt="Submit Request" />
                    </div>
                    <div class="steps-content">
                        <h3>Submit Request</h3>
                        <p>Fill out your design brief so your <br />designer can start working on creating a <br />graphic to suit your needs.</p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-4">
                <div class="how-works-outer">

                    <div class="imagebox">
                        <img src="<?php echo base_url(); ?>public/new_fe_images/review-designs.png" class="img-responsive img" alt="Review Designs" />
                    </div>

                    <div class="steps-content">
                        <h3>Review Design</h3>
                        <p>Work with your designer through <br />our communication platform <br />to perfect your design.</p>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 col-md-4">
                <div class="how-works-outer">

                    <div class="imagebox">
                        <img src="<?php echo base_url(); ?>public/new_fe_images/approve-desings.png" class="img-responsive img" alt="Appprove Designs" />
                    </div>

                    <div class="steps-content">
                        <h3>Approve Design</h3>
                        <p>Once you have accepted your design, <br />receive your completed files and restart <br />the process for your next project.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-12 text-center">
                <a href="<?php echo base_url(); ?>pricing" class="btn btn1">send request now</a>
            </div>
            <div class="bg-lines-main"></div>
        </div>
    </div>
    <div class="bg-text-section">
        <p>how <br/> it works</p>
    </div>
</section>

<section class="ourworks-main">

    <div class="container">
        <div class="ourworks-titlemain">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="dblue">Our works</h1>
                    <p>We have created all types of graphic designs. From logos, brochures, flyers, web pages, t-shirts,<br /> illustrations to much more. Take a look at some of our work!</p>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <div class="row row-no-padding">
            <div class="col-md-12">
                <div class="center-slider slickslider">
				<?php for($i=0;$i<sizeof($data);$i++){ ?>
                    <div>
                        <div class="ourworks-slidbox">
                          <img src="<?php echo base_url();?>/public/img/<?php echo $data[$i]['image']; ?>" class="img-responsive img" alt="Our Works Slider" />
                            <div class="data">
                                <h4><?php echo $data[$i]['title']; ?></h4>
                                <p><?php echo $data[$i]['body']; ?></p>
                            </div>
                        </div>
                    </div>
				<?php } ?>	
                    <!--<div>
                        <div class="ourworks-slidbox">
                          <img src="<?php echo base_url(); ?>public/new_fe_images/slide2.png" class="img-responsive img" alt="Our Works Slider" />
                            <div class="data">
                                <h4>Web design</h4>
                                <p>Don't let your business suffer from a mediocre site. We can make it stand out!</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="ourworks-slidbox">
                          <img src="<?php echo base_url(); ?>public/new_fe_images/slide3.png" class="img-responsive img" alt="Our Works Slider" />
                            <div class="data">
                                <h4>Logo/Branding</h4>
                                <p>Your brand is your businesses identity, therefore make it awesome :)</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="ourworks-slidbox">
                          <img src="<?php echo base_url(); ?>public/new_fe_images/slide4.png" class="img-responsive img" alt="Our Works Slider" />
                            <div class="data">
                                <h4>Illustration</h4>
                                <p>Everyone loves cool illustrations, let us make one for you.</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="ourworks-slidbox">
                          <img src="<?php echo base_url(); ?>public/new_fe_images/slide5.png" class="img-responsive img" alt="Our Works Slider" />
                            <div class="data">
                                <h4>Poster/Flyer</h4>
                                <p>Have an upcoming event or party? Let us help you with the marketing materials.</p>
                            </div>
                        </div>
                    </div>


                    <div>
                        <div class="ourworks-slidbox">
                          <img src="<?php echo base_url(); ?>public/new_fe_images/slide1.png" class="img-responsive img" alt="Our Works Slider" />
                            <div class="data">
                                <h4>T-shirt design</h4>
                                <p>Get a shirt designed for work, play, or fun. Shirts always bring everyone together.</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="ourworks-slidbox">
                          <img src="<?php echo base_url(); ?>public/new_fe_images/slide2.png" class="img-responsive img" alt="Our Works Slider" />
                            <div class="data">
                                <h4>Web design</h4>
                                <p>Don't let your business suffer from a mediocre site. We can make it stand out!</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="ourworks-slidbox">
                          <img src="<?php echo base_url(); ?>public/new_fe_images/slide3.png" class="img-responsive img" alt="Our Works Slider" />
                            <div class="data">
                                <h4>Logo/Branding</h4>
                                <p>Your brand is your businesses identity, therefore make it awesome :)</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="ourworks-slidbox">
                          <img src="<?php echo base_url(); ?>public/new_fe_images/slide4.png" class="img-responsive img" alt="Our Works Slider" />
                            <div class="data">
                                <h4>Illustration</h4>
                                <p>Everyone loves cool illustrations, let us make one for you.</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="ourworks-slidbox">
                          <img src="<?php echo base_url(); ?>public/new_fe_images/slide5.png" class="img-responsive img" alt="Our Works Slider" />
                            <div class="data">
                                <h4>Poster/Flyer</h4>
                                <p>Have an upcoming event or party? Let us help you with the marketing materials.</p>
                            </div>
                        </div>
                    </div>-->





                  </div>
            </div>

            <div class="col-md-12 text-center">
                <a href="<?php echo base_url(); ?>categories" class="btn btn1 arrowr">See More</a>
            </div>

        </div>
    </div>


</section>

<section class="testimonial-main">
    <div class="container">
        <div class="testimonial-contain">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1>What others have to say about us</h1>
                    <p>We can tell you we are awesome, but it maybe more credible if we let some of customers do the raving!<br /> Take a look at some of the awesome reviews we have received.</p>
                </div>
                <div class="col-md-12">
                    <div class="testimonial-star">
                        <img src="<?php echo base_url(); ?>public/new_fe_images/5-star.png" class="img-responsive img" alt="Client's Rating" />
                        <p>" Excellent and superb graphic design<br />service for start-up companies! "</p>
                    </div>
                </div>
            </div>


            <div class="testimonial-box hidden-xs hidden-sm">
                <div class="row">

                    <div class="col-md-4">
                        <div class="testimonial-imgbox">
                            <img src="<?php echo base_url(); ?>public/new_fe_images/avatar-1.png" class="img-responsive img" alt="avatar" />
                            <div class="overlap">
                                <p>You have been amazing with your ability to always get the right designs to me on time. Every design is delivered in the time frame that we expect. We are glad we found Graphics Zoo</p>
                                <h4>Timothy Galon,</h4>
                                <p>The LunchBox Digital Solutions</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="testimonial-imgbox active">
                            <img src="<?php echo base_url(); ?>public/new_fe_images/avatar-2.png" class="img-responsive img" alt="avatar" />
                            <div class="overlap">
                                <p>They are really good! I recommend Graphics Zoo to anyone who is starting out.</p>
                                <h4>Susan Haley,</h4>
                                <p class="last">CEO, JetPay</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="testimonial-imgbox">
                            <img src="<?php echo base_url(); ?>public/new_fe_images/avatar-3.png" class="img-responsive img" alt="avatar" />
                            <div class="overlap">
                                <p>Dealing with graphic designers has never been easier. The team at Graphics Zoo just finds a way to make the perfect designs. The best part is whenever I have questions or concerns the team is always ready to make it right.</p>
                                <h4>Greyson Chance,</h4>
                                <p>CEO, Honestbee</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div class="testimonialmob-main hidden-md hidden-lg">

                <div id="testimonialmob" class="owl-carousel owl-theme">

                    <div class="item">
                        <div class="testimonial-imgbox">
                            <img src="<?php echo base_url(); ?>public/new_fe_images/avatar-1.png" class="img-responsive img" alt="avatar" />
                            <div class="overlap">
                                <p>You have been amazing with your ability to always get the right designs to me on time. Every design is delivered in the time frame that we expect. We are glad we found Graphics Zoo</p>
                                <h4>Timothy Galon,</h4>
                                <p>The LunchBox Digital Solutions</p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-imgbox">
                            <img src="<?php echo base_url(); ?>public/new_fe_images/avatar-2.png" class="img-responsive img" alt="avatar" />
                            <div class="overlap">
                                <p>They are really good! I recommend Graphics Zoo to anyone who is starting out.</p>
                                <h4>Susan Haley,</h4>
                                <p class="last">CEO, JetPay</p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-imgbox">
                            <img src="<?php echo base_url(); ?>public/new_fe_images/avatar-3.png" class="img-responsive img" alt="avatar" />
                            <div class="overlap">
                                <p>Dealing with graphic designers has never been easier. The team at Graphics Zoo just finds a way to make the perfect designs. The best part is whenever I have questions or concerns the team is always ready to make it right.</p>
                                <h4>Greyson Chance,</h4>
                                <p>CEO, Honestbee</p>
                            </div>
                        </div>
                    </div>


                </div>

            </div>

        </div>
    </div>
</section>

<section class="designer-message-main">
    <div class="container">

        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="designer-contentmain">

                    <h1 class="blue text-center">A message from our designers</h1>

                    <div class="designer-content">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="left-video-part">
                                    <img src="<?php echo base_url(); ?>public/new_fe_images/video-placeholder.png" class="img-responsive" alt="Designer Saying Video">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="designer-right-content">
                                    <p>Hi there! Thanks for checking us out. We are super excited to work with you to help create your next masterpiece. All the designers at Graphics Zoo work hard to make sure you always love what we create. If its not perfect, we will make as many revisions as you would like to  it just right. Now, stop looking and let us show you what we can make for you!</p>
                                    <a href="<?php echo base_url(); ?>pricing" class="btn btn3">Get Started Now </a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<?php
	$cook = 123456;
	if(!isset($_COOKIE["graphicview"]))
	{
		setcookie("graphicview","123456",time() + (86400 * 30),"/"); 
	?>
	

<script type="text/javascript">
    $(window).on('load',function(){
        $('#myModal').modal('show');
    });
</script>
<?php
	}else{
		//print_r($_COOKIE);
	}
	?>
	

	
	
<script>
 jQuery(document).ready(function ($) {
$('#my-image-slider2').slick({
		  infinite: true,
		  speed: 300,
		  slidesToShow: 3,
		  slidesToScroll: 3,
		  autoplay:true,
		autoplaySpeed:40000000000000,
		touchMove:true,
		arrows: true,
		  responsive: [
			{
			  breakpoint: 600,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		  ]
		});	
		
$('#my-home-slider').slick({
		  infinite: true,
		  speed: 300,
		  slidesToShow: 2,
		  slidesToScroll: 2,
		  autoplay:true,
		autoplaySpeed:40000000000000,
		touchMove:true,
		arrows: false,
		  responsive: [
			{
			  breakpoint: 600,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		  ]
		});			
});
</script>