<style>

section.terms-condition {
    padding-bottom: 60px;
}
section.publection-sec {
    display: none;
}
</style>
<section class="page-heading-sec">
 <div class="container">
   <div class="row">
     <div class="col-md-12">
       <h1>Frequently Asked Question</h1>
       <p>If you can't find the answer you're looking for, please contact us directly.</p>
   </div>
</div>
</div>
</section>
<section class="terms-condition">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <b>Do I really get unlimited designs?</b>
        <br>
        <p>Yes! You really get unlimited designs and revisions for a flat monthly rate. You can request as many revisions as you like until you are completely satisfied. We work on 1 new request a day.</p>
        <br>
        <b>What can I get designed?</b>
        <br>
        <p>You can get anything designed from your designer. Everything from banners, flyers, t-shirts, book covers and much more. Just keep in mind larger and more complicated designs may take a little longer to finish.</p>
        <br>
        <b>How long does it take to receive designs?</b>
        <br>
        <p>Every design request is serviced within 1 business day. Please keep in mind though, that we value quality over everything else. Your designer will be in constant communication with you and will keep you updated on the status of your designs.</p>
        <br>
        <b>What if I need 220 designs per day?</b>
        <br>
        <p>If you know your design volume will be higher than what your sole designer is going to be able to handle, we recommend you get additional accounts so your design volume can be handled properly.</p>
        <br>
        <b>What does Risk-Free guarantee really mean?</b>
        <br>
        <p>If you do not approve any of your designs or graphics within the first 14 days of your trial, we will give you a full refund, no questions asked.</p>
        <br>
        <b>Is there really no contract?</b>
        <br>
        <p>Yes, that's right! No contract, so you can cancel at anytime.</p>
        <br>
        <b>Can I give you reference designs/graphics to show you the style I want to use?</b>
        <br>
        <p>Yes, we actually recommend it. Our designers will use your references as a starting point to create something great based on your design style.</p>
        <br>
        <b>How long do revisions take to complete?</b>
        <br>
        <p>We understand the importance of getting work done quickly. That's why we make sure your revisions are usually made within 1 business day.</p>
        <br>
        <b>Can I sell the designs you make for me?</b>
        <br>
        <p>Yes, you own complete rights to all design files that we create. You can request all the editable files as well as any specific format you may need.</p>
        <br>
        <b>How long do I have to wait before I can request designs?</b>
        <br>
        <p>Once you register for our services you can immediately start requesting your designs.</p>
        <br>
        <b>How can I reach someone if I have any questions?</b>
        <br>
        <p>If you have any questions or concerns, feel free to email us at <?php echo SUPPORT_EMAIL; ?>, fill out the form on the Contact Us page, or Chat with us live.</p>
      </div>
    </div>
  </div>
</section><!-- #Try Graphics -->
<section class="riskFree-sec pricing-risk">
       <div class="container">
          <div class="row">
             <div class="col-md-12 text-center">
                <h2>Join more than 500+ customers</h2>
                <h3>Try Graphics Zoo Risk-Free For 14 Days</h3>
                <a href="<?php echo base_url();?>pricing" class="red-theme-btn" rel="nofollow">Get Started Now
                   <img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>front_end/Updated_Design/img/red-long-arrow.png" alt="red-long-arrow" class="img-fluid"></a>

               </div>
           </div>
       </div>
   </section>