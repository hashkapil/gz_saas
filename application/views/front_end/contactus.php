<link rel="canonical" href="<?php echo base_url(); ?>public/front_end/streama.css" rel="stylesheet" type="text/css" />
<style>
.inner-700{max-width:700px;}
.intro-copy.dark.inner-700 {max-width: 800px;}
.center{text-align: center;margin: auto;}
.intro-copy h1{font-family: 'Open Sans', sans-serif;color: white;font-weight: 100;/* border-top: 1px solid #F23F5B; *//* border-bottom: 1px solid #F23F5B; */margin: 0;line-height: 1em;/* max-width: 444px; */}
h1{ color: #1d2835 !important;font-size:35px;}
.sm-form-control {
    height: auto;
}
.sm-form-control{
	    display: block;
    width: 100%;
    
    padding: 8px 14px;
    font-size: 15px;
    line-height: 1.42857143;
    color: #555555;
    background-color: #ffffff;
    background-image: none;
    border: 2px solid #DDD;
    border-radius: 0 !important;
    -webkit-transition: border-color ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s;
    transition: border-color ease-in-out .15s;
}
label {
    display: inline-block;
    font-size: 13px;
    font-weight: 700;
    font-family: 'Open Sans', sans-serif;
    text-transform: uppercase;
    letter-spacing: 1px;
    color: #555;
    margin-bottom: 10px;
    cursor: pointer;
}

#contact_form input[type="submit"]{
    background: #F23F5B;
    color: #FFF;
    width: 302px;
    border: none;
    border-radius: 3px;
    text-transform: uppercase;
    text-decoration: none;
    font-weight: 100;
    font-size: 14px;
    letter-spacing: 1px;
    padding: 10px;
}

form .col_full, form .col_half{
	margin-bottom:25px;
}

</style>
<section class="layer bg-white" id="layer_2">
    <section class="intro-plain bg-grey ">
    <div class="inner pad-top-80 pad-bot-40">
        <div class="intro-copy inner-700 center">
            <h1>Contact Us</h1>
            <p class="number">888-976-2747</p>
        </div>
    </div>
</section> 


            <!--            <section class="layer accordians bg-grey" id="layer_2">
                            <div class="inner pad-top-60 pad-bot-60 ">
                                <div class="section-head ">
                                    <h2>Send Message</h2>
                                </div>
            
            
                            </div>
                        </section>-->


<div class="bg-white pad-bot-60" id="layer_3">
    <div class="inner">
        <div class="content">
            <div class="pad-top-20 pad-bot-20">
                <form action="#" id="contact_form" method="post" accept-charset="utf-8">
                    <div class="login-input-wrap">
                        <form action="#" id="template-jobform" method="post" role="form">

                            <div class="grid flex column-2">
                                <div class="col">
                                    <label for="template-jobform-fname">First Name <small>*</small></label>
                                    <input type="text" id="template-jobform-fname" name="template-jobform-fname" value="" class="sm-form-control required" aria-required="true">
                                </div>

                                <div class="col">
                                    <label for="template-jobform-lname">Last Name <small>*</small></label>
                                    <input type="text" id="template-jobform-lname" name="template-jobform-lname" value="" class="sm-form-control required" aria-required="true">
                                </div>
                            </div>

                            <div class="clear"></div>

                            <div class="col_full">
                                <label for="template-jobform-email">Email <small>*</small></label>
                                <input type="email" id="template-jobform-email" name="template-jobform-email" value="" class="required email sm-form-control" aria-required="true">
                            </div>

                            <div class="col_half">
                                <label for="template-jobform-age">Subject <small>*</small></label>
                                <input type="text" name="template-jobform-age" id="template-jobform-age" value="" size="22" tabindex="4" class="sm-form-control required" aria-required="true">
                            </div>                                     

                            <div class="col_full">
                                <label for="template-jobform-experience">Message</label>
                                <textarea name="template-jobform-experience" id="template-jobform-experience" rows="3" tabindex="10" class="sm-form-control"></textarea>
                            </div>                                    

                            <div class="login-input-wrap">
                                <input type="submit" value="Send Message" class="submit box_round4 transition">
                            </div>
                        </form>

                    </div>

            </div>
        </div>
    </div>
</div>

<!-- Footer -->          
<!--<section class="layer section-header bg-black dark footer">
    <div class="inner pad-top-60">
        <div class="section-head center ">
            <h2>CONTACT</h2>
            <p>We are a unlimited graphic design team based in Houston, TX.</p>

        </div>
    </div>
</section> 


<script>fbq('track', 'Contact Us');</script>
</section>

-->


