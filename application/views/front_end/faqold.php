<section class="pricing-main">
    
	<div class="pricing-questions-main">

        <div class="container">
            <div class="innermain-banner">
                <h1>Frequently Asked Question</h1>
                <p class="white">If you can't find the answer you're looking for, please contact us directly.</p>
            </div>
        </div>

        <div class="pricingquestions-data">
            <div class="container">
                <div class="row">

                    <div class="col-md-6">
                        <div class="pricingquestions-databox">
                            <h4>Do I really get unlimited designs?</h4>
                            <p>Yes! You really get unlimited designs and revisions for a flat monthly rate. You can request as many revisions as you like until you are completely satisfied.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pricingquestions-databox">
                            <h4>What can I get designed?</h4>
                            <p>You can get anything designed from your designer. Everything from banners, flyers, t-shirts, book covers and much more. Just keep in mind larger and more complicated designs may take a little longer to finish.</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="pricingquestions-databox">
                            <h4>How long does it take to receive designs?</h4>
                            <p>Every design request is serviced within 1 business day. Please keep in mind though, that we value quality over everything else. Your designer will be in constant communication with you and will keep you updated on the status of your designs.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pricingquestions-databox">
                            <h4>What if I need 220 designs per day?</h4>
                            <p>If you know your design volume will be higher than what your sole designer is going to be able to handle, we recommend you get additional accounts so your design volume can be handled properly.</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="pricingquestions-databox">
                            <h4>What does Risk-Free guarantee really mean?</h4>
                            <p>If you do not approve any of your designs or graphics within the first 14 days of your trial, we will give you a full refund, no questions asked.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pricingquestions-databox">
                            <h4>Is there really no contract?</h4>
                            <p>Yes, that's right! No contract, so you can cancel at anytime.</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="pricingquestions-databox">
                            <h4>Can I give you reference designs/graphics to show you the style I want to use?</h4>
                            <p>Yes, we actually recommend it. Our designers will use your references as a starting point to create something great based on your design style.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pricingquestions-databox">
                            <h4>How long do revisions take to complete?</h4>
                            <p>We understand the importance of getting work done quickly. That's why we make sure your revisions are usually made within 1 business day.</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="pricingquestions-databox">
                            <h4>Can I sell the designs you make for me?</h4>
                            <p>Yes, you own complete rights to all design files that we create. You can request all the editable files as well as any specific format you may need.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pricingquestions-databox">
                            <h4>How long do I have to wait before I can request designs?</h4>
                            <p>Once you register for our services you can immediately start requesting your designs.</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="pricingquestions-databox">
                            <h4>How can I reach someone if I have any questions?</h4>
                            <p>If you have any questions or concerns, feel free to email us at hello@graphicszoo.com, fill out the form on the Contact Us page, or Chat with us live.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

</section>