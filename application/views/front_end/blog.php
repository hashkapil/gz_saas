<?php for ($k = 0; $k < sizeof($data); $k++) {
    ?>
    <style>


        .blog-datamain .box<?php echo $k + 1; ?>:before {
            background: url(<?php base_url(); ?>public/img/<?php echo $data[$k]['image']; ?>) no-repeat left bottom;
        }
        /* // .blog-datamain .box2:before {
             // background: url(<?php base_url(); ?>public/img/<?php echo $data[$k]['image']; ?>) no-repeat center center;
         // }
         // .blog-datamain .box4:before {
            
                 // background: url(<?php base_url(); ?>public/img/<?php echo $data[$k]['image']; ?>) no-repeat center center;
         // }*/
    </style>
<?php } ?>

<section class="blog-main">
    <div class="container">
        <div class="innermain-banner">
            <h1 class="dblue">Stay up to date with our latest blog posts.</h1>
            <p>We got everything you need for your business to succeed in terms of design.<br />Check out this articles and learn about tips and tricks of the industry.</p>
        </div>
        <div class="blog-datamain">
            <div class="row">
                <?php
                 //echo "<pre>";ptint_R($data);
                $j = 0;
                for ($i = 0; $i < sizeof($data); $i++) {
                    if ($j == 0) {
                        ?>	
                        <div class="col-md-7 col-lg-8">
                            <div class="box box<?php echo $i + 1; ?>">
                                <div class="data">
                                    <h3><?php echo $data[$i]['title']; ?></h3>
                                    <p><?php echo $data[$i]['body']; ?></p>
                                    <div class="date"><?php echo date('d-m-Y', strtotime($data[$i]['created'])); ?></div>
                                    <a href="<?php echo base_url() . "view_blog/" . $data[$i]['id']; ?>" class="btn btn1 arrowr">read More </a>
                                </div>
                            </div>
                        </div>
                        <?php
                        if (!isset($data[$i + 1]['title'])) {
                            continue;
                        }
                        ?>
                        <div class="col-md-5 col-lg-4">
                            <div class="box box<?php echo $i + 2; ?>">
                                <div class="data">
                                    <h3><?php echo $data[$i + 1]['title']; ?></h3>
                                    <div class="date"><?php echo date('d-m-Y', strtotime($data[$i]['created'])); ?></div>
                                    <a href="<?php echo base_url() . "view_blog/" . $data[$i + 1]['id']; ?>" class="btn btn1 arrowr">read More </a>
                                </div>
                            </div>
                        </div>
                        <?php
                        $j = 1;
                        $i++;
                    } else {
                        ?>
                        <div class="col-md-5 col-lg-4">
                            <div class="box box<?php echo $i + 1; ?>">
                                <div class="data">
                                    <h3><?php echo $data[$i]['title']; ?></h3>
                                    <div class="date"><?php echo date('d-m-Y', strtotime($data[$i]['created'])); ?></div>
                                    <a href="<?php echo base_url() . "view_blog/" . $data[$i]['id']; ?>" class="btn btn1 arrowr">read More </a>
                                </div>
                            </div>
                        </div>
                        <?php
                        if (!isset($data[$i + 1]['title'])) {
                            continue;
                        }
                        ?>
                        <div class="col-md-7 col-lg-8">
                            <div class="box box<?php echo $i + 2; ?>">
                                <div class="data">
                                    <h3><?php echo $data[$i + 1]['title']; ?></h3>
                                    <p><?php echo $data[$i + 1]['body']; ?></p>
                                    <div class="date"><?php echo date('d-m-Y', strtotime($data[$i]['created'])); ?></div>
                                    <a href="<?php echo base_url() . "view_blog/" . $data[$i + 1]['id']; ?>" class="btn btn1 arrowr">read More </a>
                                </div>
                            </div>
                        </div>
                        <?php
                        $j = 0;
                        $i++;
                    }
                }
                ?>

                <!--<div class="col-md-5 col-lg-4">
                    <div class="box box3">
                        <div class="data">
                            <h3>Things You Should Definitely Include in a Brochure</h3>
                            <div class="date">October 02, 2017</div>
                            <a href="blog-paragraph-main.html" class="btn btn1 arrowr">read More </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-lg-8">
                    <div class="box box4">
                        <div class="data">
                            <h3>4 Things We Need From You Before We Can Design Your Website</h3>
                            <div class="date">September 29, 2017</div>
                            <a href="blog-paragraph-main.html" class="btn btn1 arrowr">read More </a>
                        </div>
                    </div>
                </div>-->


            </div>
        </div>


    </div>
</section>