<style type="text/css">
  #myBtnContainer .active{
    border-bottom: 2px solid #e83833;
    border-radius: 0px;
  }
</style>
<section class="page-heading-sec">
   <div class="container">
       <div class="row">
           <div class="col-md-12">
               <h1 style="font-weight: 600;font-size: 50px;">Portfolio</h1>
           </div>
       </div>
   </div>
</section>
<!--------------Portfolie tabs---------------------->
<section class="poerfolioTabs">

<div class="container">
    <div class="row">
    <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <p class="gallery-title">We create all types of graphic designs. From logos, brochures, flyers, web pages, t-shirts, illustrations to much more. Take a look at some of our work!
            </p>
                                        
     <ul id="tabsJustified" class="nav nav-tabel">
        <li><a href="" data-target="#All" data-toggle="tab" class="active">
        <button class="btn btn-default filter-button">All</button>
        </a>
        </li>
        <li><a href="" data-target="#Logo" data-toggle="tab">
        <button class="btn btn-default filter-button">Logo</button>
    </a>
    </li>
    <li><a href="" data-target="#T-shirt" data-toggle="tab">
        <button class="btn btn-default filter-button" >T-shirt</button>
    </a>
    <li><a href="" data-target="#Print" data-toggle="tab">
        <button class="btn btn-default filter-button" data-filter="spray">Print</button>
    </a></li>
    <li><a href="" data-target="#Ads" data-toggle="tab">
        <button class="btn btn-default filter-button" data-filter="irrigation">Ads</button>
    </a></li>
    <li><a href="" data-target="#Websites" data-toggle="tab">
        <button class="btn btn-default filter-button" data-filter="irrigation">Websites</button>
    </a></li>
    <li><a href="" data-target="#App" data-toggle="tab">
        <button class="btn btn-default filter-button" data-filter="irrigation"> App</button>
    </a></li>
    <li><a href="" data-target="#Other" data-toggle="tab">
        <button class="btn btn-default filter-button" data-filter="irrigation"> Other</button>
    </a></li>
     </ul>
     <div id="tabsJustifiedContent" class="tab-content">

        <!---------All portfolio---------------->
                    <div id="All" class="tab-pane fade active show">
                    <ul class="all-project row">
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project11.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project11.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>T-Shirt</p>
                    </div>
                    </a>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project12.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project12.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    <li class="thumb">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                           data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project15.jpg"
                           data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project15.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Print</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project13.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project13.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Other</p>
                    </div>
                  </a>
                    </li>
                    <li class="thumb">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                           data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project16.jpg"
                           data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project16.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Print</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class=" col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project14.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project14.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Print</p>
                    </div>
                  </a>
                    </li>
                    <li class="thumb">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                           data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project17.jpg"
                           data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project17.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class=" col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project18.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project18.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class=" col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project19.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project19.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Ads</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project20.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project20.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Websits</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project21.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project21.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    <li class="thumb">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                           data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project25.jpg"
                           data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project25.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project22.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project22.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>App</p>
                    </div>
                  </a>
                    </li>
                    <li class="thumb">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                           data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project26.jpg"
                           data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project26.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Print</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project23.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project23.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    <li class="thumb">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                           data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project27.jpg"
                           data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project27.jpg" class="img-responsive"></a>
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Print</p>
                    </div>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project24.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project24.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project28.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project28.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project29.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project29.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Print</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-6 ">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project30.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project30.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>T-Shirt</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    </ul>
                    </div>

        <!---------Logo portfolio---------------->

                <div id="Logo" class="tab-pane fade">
                    <ul class="all-project row">
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project12.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project12.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project17.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project17.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project18.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project18.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project26.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project26.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project27.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project27.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    <div class="col-md-3 col-6">
                        <li class="thumb">
                            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project28.jpg"
                               data-target="#image-gallery">
                    <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project28.jpg" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                        <p>Logo</p>
                    </div>
                  </a>
                    </li>
                    </div>
                    </ul>
                    </div>

         <!---------T-Shirt portfolio---------------->

         <div id="T-shirt" class="tab-pane fade">
                <ul class="all-project row">
                <div class="col-md-3 col-6">
                    <li class="thumb">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                           data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project11.jpg"
                           data-target="#image-gallery">
                <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project11.jpg" class="img-responsive">
                <div class="hover-txt">
                    <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                    <p>T-Shirt</p>
                </div>
              </a>
                </li>
                </div>
                <div class="col-md-6">
                    <li class="thumb">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                           data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project30.jpg"
                           data-target="#image-gallery">
                <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project30.jpg" class="img-responsive">
                <div class="hover-txt">
                    <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                    <p>T-Shirt</p>
                </div>
              </a>
                </li>
                </div>
                </ul>
                </div>
 <!---------Print portfolio---------------->

 <div id="Print" class="tab-pane fade">

        <ul class="all-project row">
        <div class="col-md-3 col-6">
            <li class="thumb">
                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                   data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project24.jpg"
                   data-target="#image-gallery">
        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project24.jpg" class="img-responsive">
        <div class="hover-txt">
            <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
            <p>Print</p>
        </div>
      </a>
        </li>
        </div>
        <div class="col-md-3 col-6">
            <li class="thumb">
                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                   data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project13.jpg"
                   data-target="#image-gallery">
        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project13.jpg" class="img-responsive">
        <div class="hover-txt">
            <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
            <p>Print</p>
        </div>
      </a>
        </li>
        <li class="thumb">
            <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
               data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project26.jpg"
               data-target="#image-gallery">
        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project26.jpg" class="img-responsive">
        <div class="hover-txt">
            <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
            <p>Print</p>
        </div>
      </a>
        </li>
        </div>
        <div class="col-md-3 col-6">
            <li class="thumb">
                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                   data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project14.jpg"
                   data-target="#image-gallery">
        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project14.jpg" class="img-responsive">
        <div class="hover-txt">
            <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
            <p>Print</p>
        </div>
      </a>
        </li>
        </div>

        <div class=" col-md-3 col-6">
            <li class="thumb">
                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                   data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project15.jpg"
                   data-target="#image-gallery">
        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project15.jpg" class="img-responsive">
        <div class="hover-txt">
            <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
            <p>Print</p>
        </div>
      </a>
        </li>
        </div>

       
        </ul>
        </div>
<!---------Ads portfolio---------------->

<div id="Ads" class="tab-pane fade">

        <ul class="all-project row">
        <div class="col-md-3 col-6">
            <li class="thumb">
                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                   data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project16.jpg"
                   data-target="#image-gallery">
        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project16.jpg" class="img-responsive">
        <div class="hover-txt">
            <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
            <p>Ads</p>
        </div>
        
      </a>
        </li>
        </div>
        <div class="col-md-3 col-6">
            <li class="thumb">
                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                   data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project19.jpg"
                   data-target="#image-gallery">
        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project19.jpg" class="img-responsive">
        <div class="hover-txt">
            <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
            <p>Ads</p>
        </div>
      </a>
        </li>
        </div>
        </ul>
    </div>
<!---------Websites portfolio---------------->

<div id="Websites" class="tab-pane fade">

        <ul class="all-project row">
        <div class="col-md-3 col-6">
            <li class="thumb">
                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                   data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project24.jpg"
                   data-target="#image-gallery">
        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project24.jpg" class="img-responsive">
        <div class="hover-txt">
            <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
            <p>Websites</p>
        </div>
      </a>
        </li>
        </div>
        <div class="col-md-6">
            <li class="thumb">
                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                   data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project20.jpg"
                   data-target="#image-gallery">
        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project20.jpg" class="img-responsive">
        <div class="hover-txt">
            <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
            <p>Websites</p>
        </div>
      </a>
        </li>
        </div>
        </ul>
    </div>
    <!--------- App portfolio---------------->

<div id="App" class="tab-pane fade">
        <ul class="all-project row">
                        <div class="col-md-3 col-6">
                            <li class="thumb">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                                   data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project23.jpg"
                                   data-target="#image-gallery">
                        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project23.jpg" class="img-responsive">
                        <div class="hover-txt">
                            <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                            <p>App</p>
                        </div>
                      </a>
                        </li>
                        </div>
                        <div class="col-md-3 col-6">
                            <li class="thumb">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                                   data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project27.jpg"
                                   data-target="#image-gallery">
                        <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project27.jpg" class="img-responsive">
                        <div class="hover-txt">
                            <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                            <p>App</p>
                        </div>
                      </a>
                        </li>
                        </div>
                        </ul>
    </div>
    <!---------  Other portfolio---------------->

    <div id="Other" class="tab-pane fade">

            <ul class="all-project row">
            <div class="col-md-3 col-6">
                <li class="thumb">
                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                       data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project22.jpg"
                       data-target="#image-gallery">
            <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project22.jpg" class="img-responsive">
            <div class="hover-txt">
                <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                <p>Other</p>
            </div>
          </a>
            </li>
            </div>
            <div class="col-md-3 col-6">
                <li class="thumb">
                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title=""
                       data-image="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project29.jpg"
                       data-target="#image-gallery">
            <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/project29.jpg" class="img-responsive">
            <div class="hover-txt">
                <span><img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/search-icon.png"></span>
                <p>Other</p>
            </div>
          </a>
            </li>
            </div>
        </div>
</div>  

<!--<a href="#" class="load-more">Load More</a>-->
<div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="image-gallery-title"></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <img id="image-gallery-image" class="img-responsive" src="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fas fa-chevron-left"></i>
                </button>

                <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fas fa-chevron-right"></i>
                </button>
            </div> 
        </div>
    </div>
</div>
</div>
</section>
<!------ risk-free-sec -------->
<section class="riskfree-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
             <h2 class="stander-heading">Try Graphics Zoo Risk-Free for 14 Days</h2>
             <p class="sub-heading">We know trying a new service can be scary, that’s why we want
                    to make this 100% risk-free for you and your company.</p>
                    <a href="<?php echo base_url(); ?>pricing" class="button">get started</a>
               </div>
           </div>
        </div>
</section>

