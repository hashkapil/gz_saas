 
<style>
	.portfolio-right {
    background: url(<?php base_url() ;?>public/new_fe_images/portfoliomain.jpg) no-repeat center center;
    background-size: cover;
}
</style>
<section class="portfolio-main">
    <div class="container-fluid">
        <div class="row row-no-padding d-flex">
            <div class="col-sm-6 col-md-8 col-md-push-4 portfolio-right col-sm-push-6"></div>
            <div class="col-sm-6 col-md-4 col-md-pull-8 portfolio-left col-sm-pull-6">
                <div class="portfolio-leftbox">
                    <h1>Portfolio</h1>
                    <p>logo / <b>book cover</b> / web design / advertising / packaging / shirt design / posters & flyers / others </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bookcover-main">
    <div class="container">
        <div class="bookcover-titlemain">
            <div class="row">
                <div class="col-sm-12 col-md-4"><h1 class="red">Sample of Our Work</h1></div>
                <div class="col-sm-12 col-md-8">
                    <p>Take a look at a few of the things we have created for previous customers! They loved these styles and made it according to their taste. Don't worry if these don't match your style, we can make it according to whatever your needs maybe.</p>
                </div>
            </div>
        </div>

        <div class="bookcover-boxmain">

            <div class="zoom-gallery"> 

            <div class="row">
			<?php for($i=0;$i<sizeof($data);$i++){ ?>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php echo base_url();?>/public/img/<?php echo $data[$i]['image']; ?>" rel="nofollow" title="Portfolio">
		                    <img src="<?php echo base_url();?>/public/img/<?php echo $data[$i]['image']; ?>" class="img-responsive" alt="" />
	                    </a> 
                    </div>
                </div>
			<?php } ?>	
                <!--<div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide2.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide2.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide3.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide3.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide4.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide4.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>



                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide3.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide3.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide4.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide4.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide1.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide1.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide2.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide2.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>



                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide1.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide1.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide2.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide2.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide3.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide3.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide4.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide4.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>



                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide3.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide3.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide4.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide4.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide1.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide1.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="bookcover-box">
                        <a href="<?php base_url() ;?>public/new_fe_images/slide2.png" title="Portfolio">
		                    <img src="<?php base_url() ;?>public/new_fe_images/slide2.png" class="img-responsive" alt="" />
	                    </a>
                    </div>
                </div>-->


            </div>

            </div>

            <div class="sep2"></div>
            <div class="text-center">
                <a href="#" class="btn btn1 btnlr">More</a>
            </div>

        </div>


    </div>

</section>