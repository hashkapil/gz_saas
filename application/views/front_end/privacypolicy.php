<style>
    section.terms-condition {
        padding-bottom: 60px;
    }
    section.publection-sec {
        display: none;
    }
</style>
<section class="page-heading-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Privacy Policy</h1>
            </div>
        </div>
    </div>
</section>
<section class="terms-condition">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>
                <b>What personal information do we collect from the people that visit our blog, website or app?</b><br>
                <p>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, credit card information or other details to help you with your experience.</p>
                <b>When do we collect information?</b>
                <p>We collect information from you when you register on our site, place an order, subscribe to a newsletter, fill out a form or enter information on our site.</p>
                <b>How do we use your information? </b>
                <p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>
                <p>
                <ul style="padding-left: 20px">
                    <li>To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.</li>
                    <li> To improve our website in order to better serve you.</li>
                    <li>To allow us to better service you in responding to your customer service requests.</li>
                    <li>To administer a contest, promotion, survey or other site feature.</li>
                    <li>To quickly process your transactions.</li>
                    <li>To send periodic emails regarding your order or other products and services.</li>
                    <li> To follow up with them after correspondence (live chat, email or phone inquiries)</li>
                </ul>
                </p>
                <br>
                <b>How do we protect your information?</b>
                <br>
                <p>
                    Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible.
                    We use regular Malware Scanning.
                    Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.
                    We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.
                    All transactions are processed through a gateway provider and are not stored or processed on our servers.
                </p>
                <br>
                <b>Do we use 'cookies'?</b>
                <p>We do not use cookies for tracking purposes. You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies. If you turn cookies off, Some of the features that make your site experience more efficient may not function properly.that make your site experience more efficient and may not function properly.</p>
                <br>
                <b>Third-party disclosure</b>
                <br>
                <p>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it's release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property or safety. <br>However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses. </p>
                <br>
                <b>Third-party links</b>
                <br>
                <p>We do not include or offer third-party products or services on our website.</p>
                <br>
                <b>Google</b>
                <br>
                <p>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en 
                    <br>
                    We use Google AdSense Advertising on our website. Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.</p>
                <br>
                <b>We have implemented the following:</b>
                <ul style="padding-left: 20px">
                    <li>Remarketing with Google AdSense</li>
                    <li>Google Display Network Impression Reporting</li>
                </ul>
                </br> 
                <p>We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website. </p>
                <br>
                <b>Opting out:</b>
                <br>
                <p>Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt Out Browser add on.</p>
                <br>
                <b>California Online Privacy Protection Act</b>
                <br>
                <p>CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. - See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf</p>
                <br>
                <b>According to CalOPPA, we agree to the following:</b>
                <br>
                <p>
                    Users can visit our site anonymously.
                    <br>
                    Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website.
                    <br>
                    Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.
                    <br><br>
                    You will be notified of any Privacy Policy changes:
                <ul style="padding-left: 20px">
                    <li>On our Privacy Policy Page</li> 
                </ul>
                <br>
                <p>Can change your personal information:</p>
                <ul style="padding-left: 20px">
                    <li>By logging in to your account</li> 
                </ul>
                </p>
                <br>
                <b>How does our site handle Do Not Track signals?</b>
                <br>
                <p>We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place. </p>
                <br>
                <b>Does our site allow third-party behavioral tracking?</b>
                <br>
                <p>It's also important to note that we allow third-party behavioral tracking</p>
                <br>
                <b>COPPA (Children Online Privacy Protection Act)</b>
                <br>
                <p>When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.
                    <br>
                    We do not specifically market to children under the age of 13 years old.
                    <br>
                    Do we let third-parties, including ad networks or plug-ins collect PII from children under 13?</p>
                <br>
                <b>Fair Information Practices</b>
                <br>
                <p>The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.</p>
                <br>
                <b>In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</b>
                <br>
                <p>
                    We will notify the users via in-site notification
                <ul style="padding-left: 20px">
                    <li>Within 7 business days</li> 
                </ul>
                <p>We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.</p>
                </p>
                <br>
                <b>CAN SPAM Act</b>
                <br>
                <p>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.</p>
                <br>
                <b>We collect your email address in order to:</b>
                <br>
                <p>
                <ul style="padding-left: 20px">
                    <li>Send information, respond to inquiries, and/or other requests or questions</li> 
                    <li>Process orders and to send information and updates pertaining to orders.</li> 
                    <li>Send you additional information related to your product and/or service</li> 
                    <li>Market to our mailing list or continue to send emails to our clients after the original transaction has occurred.</li> 
                </ul>
                </p>
                <br>
                <b>To be in accordance with CANSPAM, we agree to the following:</b>
                <br>
                <p>
                <ul style="padding-left: 20px">
                    <li>Not use false or misleading subjects or email addresses.</li> 
                    <li>Identify the message as an advertisement in some reasonable way.</li> 
                    <li>Include the physical address of our business or site headquarters.</li> 
                    <li>Monitor third-party email marketing services for compliance, if one is used.</li> 
                    <li>Honor opt-out/unsubscribe requests quickly.</li> 
                    <li>Allow users to unsubscribe by using the link at the bottom of each email.</li> 
                </ul>
                </p>
                <br>
                <b>If at any time you would like to unsubscribe from receiving future emails, you can email us at</b>
                <br>
                <p>
                <ul style="padding-left: 20px">
                    <li> Follow the instructions at the bottom of each email.</li>  
                </ul>
                <p>and we will promptly remove you from ALL correspondence.</p>
                </p>
                <br>
                <b>Contacting Us</b>
                <br>
                <p>
                    If there are any questions regarding this privacy policy, you may contact us using the information below.
                <ul class="terms-ul">
                    <li>GraphicsZoo.com</li>  
                    <li>245 Commerce Green Blvd. Ste #120</li>  
                    <li>Sugar Land, Texas 77478</li>  
                    <li>United States</li>  
                    <li><a href="mailto:info@graphicszoo.com">info@graphicszoo.com</a></li>  
                </ul>
                </p>
            </div>
        </div>
    </div>
</section><!-- #Try Graphics -->
<section class="riskFree-sec pricing-risk">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Join more than 500+ customers</h2>
                <h3>Try Graphics Zoo Risk-Free For 14 Days</h3>
                <a href="<?php echo base_url(); ?>pricing" class="red-theme-btn" rel="nofollow">Get Started Now
                    <img alt="red-long-arrow" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/red-long-arrow.png" class="img-fluid"></a>

            </div>
        </div>
    </div>
</section>