<style>
.authentication-main.loginmain {
    padding: 70px 0px;
    background: url(<?php base_url() ;?>public/new_fe_images/loginbg.png) no-repeat center top;
}
</style>
<section class="authentication-main loginmain">
    <div class="container">
	
        <div class="row">
            <div class="col-md-6 col-md-offset-3 blocklogin" style="display:block">
				
                <div class="boxmain boxsm">
				
                    <div class="logo"><img src="<?php echo base_url(); ?>public/new_fe_images/logo.png" class="img-responsive img" alt="" /></div>
					<?php if($this->session->flashdata('message_error') != '') {?>				
				   <div class="alert alert-danger alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<strong><?php echo $this->session->flashdata('message_error'); ?></strong>				
					</div>
				   <?php }?>
				   <?php if($this->session->flashdata('message_success') != '') {?>				
				   <div class="alert alert-success alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<strong><?php echo $this->session->flashdata('message_success');?></strong>
					</div>
				   <?php }?>
                    <form method="post" action="<?php echo base_url()."login"?>">
                        <div class="form-group">
                            <label class="alllabel">Username</label>
                            <input type="text" id="email" class="form-control" placeholder="email@website.com" name="email">
                        </div>

                        <div class="form-group">
                            <label class="alllabel">Password</label>
                            <input type="password" id="password" class="form-control" placeholder="password" name="password">
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-5 col-md-5">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Remember me
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-7 col-md-7">
                                    <div class="forgotpassword"><a href="#" class="link1" id="forgotpassword1">Forgot your password?</a></div>
                                </div>
                            </div>
                        </div>

                        <div class="sep2"></div>
                        <div class="text-center">
                            <button type="submit" class="btn btn1 btnhalf radius6">Login</button>
                            <div class="sep"></div>
                            <div><a href="#" class="link1 text-uppercase">Sign Up</a></div>
                        </div>

                    </form>

                </div>
            </div>
			<div class="col-md-6 col-md-offset-3"  id="showforgotpassword" style="display:none">
                <div class="boxmain boxsm" style="height:613px;">

                    <div class="logo" style="padding-bottom:40px"><img src="<?php echo base_url(); ?>public/new_fe_images/logo.png" class="img-responsive img" alt="" /></div>
					<h2 style="color:#21374c;margin:auto;text-align:center;">Forgot Password</h2>
					<p style="text-align:center;padding-top:15px;width:50%;margin:auto;">For Reset Your Password Entre Your Email Address</p>	
                    <form method="post" action="<?php echo base_url()."welcome/forgot_password"?>">
                        <div class="form-group" style="padding-top:20px;">
                            <label class="alllabel">Forgot Password</label>
                            <input type="email" id="email" class="form-control" placeholder="Email" name="email">
                        </div>
						<div style="margin:auto;text-align:center;padding-top:45px;">
							<button type="submit" class="btn btn1 btnhalf radius6" >Submit</button>
							
						</div>
						<div style="margin:auto;text-align:center;padding-top:30px;">
							<a href="<?php echo base_url(); ?>login" class="link1 text-uppercase">Login</a>
						</div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function (){
	
	$('#forgotpassword1').click(function (){
		$('.blocklogin').hide();
		$('#showforgotpassword').toggle();
	});
});
</script>
