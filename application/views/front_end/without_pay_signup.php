<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Sign Up | Graphicszoo</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link rel="shortcut icon canonical" type="image/x-icon" href="<?php echo FAV_ICON_PATH;?>">


  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" rel="stylesheet nofollow canonical">

  <!-- Bootstrap css -->
  <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
  <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/bootstrap/css/bootstrap.css" rel="stylesheet nofollow canonical">

  <!-- Libraries CSS Files -->
  <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet canonical">
  <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/animate/animate.min.css" rel="stylesheet canonical">

  <!-- Font Awesomw library -->
  <link rel="stylesheet nofollow canonical" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">

  <!-- Main Stylesheet File -->
  <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/style.css" rel="stylesheet canonical">
  <link href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/custom.css" rel="stylesheet canonical">
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-57XFPLD');</script>
  <!-- End Google Tag Manager -->
</head>
<style>
#header{
  display: none;
}
.header_inner{
  display: block !important;
}
.button.sign-up-p {
    border-radius: 30px;
    text-transform: uppercase;
}
@media only screen and (max-width: 767px) {

	.signup-btn-p h2 {
    font-size: 34px;
  }
  .subscription_pad {
    padding: 0;
  }
}

#signup-page .header .container {
  max-width: 900px;
}

#signup-page .header .already_signin {
  text-align: right;
}
select.form-control:not([size]):not([multiple]) {
  height: 45px;
}
.sign-up-pagep {
  margin: 0 auto 0;
}
#signup-page #header{background:#F3F3F3; position:relative;}
#signup-page {
  background-color: #F3F3F3;
  background-image: url(<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/signup-bg.png);
  background-repeat: no-repeat;
  background-position: center bottom;
}
#signup-page h2{font-family: 'Montserrat', sans-serif; font-weight:400}
.back-to-main a{
  margin-left: 10px;
  color: #e8304d;
}
.modal-dialog{max-width:900px}
.modal-content{
  box-shadow: 0 0 35px rgba(0, 0, 0, 0.1);
  border: 0;
  padding: 25px 0px;
  border-radius: 12px;
}
.form-group{position:relative;}
.form-group i{
  position: absolute;
  top: 13px;
  left: 10px;
  color: #A0A0A0;
}
.col_2 .form-group i{left:24px;}
.form-group input {
  padding-left: 35px;
}
#SignupModal h2 {
  font-size: 20px;
  margin-bottom: 20px;
  color: #153669;
  font-weight: 400;
  text-align:center;
}
.border-bottom{
  padding-bottom: 17px;
  padding-right: 10px;
  border-color: #DCDCDC;
}
select.subs_plan {
  padding: 6px;
  width:100%;
}
.select_wrap {
  border-bottom: 1px solid #e8304d;
}
.plans_table {
  border: 1px solid #e8304d;
  padding: 20px 15px;
  border-radius: 10px;
}
.plans_table h3 {
  text-transform: uppercase;
  color: #e8304d;
}
.plans_table_logo {
  border: 1px solid #e8304d;
  padding: 20px 15px;
  border-radius: 10px;
  height: 100%;
  display: table;
  /*position: absolute;*/
  width: 100%;
}
.plans_table_logo h3 {
  text-transform: uppercase;
  color: #e8304d;
}
.signup_logo{
  display: table-cell;
  vertical-align: middle;
}
.inner-logo{
  margin-left: auto;
  margin-right: auto;
  text-align: center;
}
.plan_price span {
  font-size: 30px;
  font-weight: 800;
}
.plans_table_sec {
  border: 1px solid #e8304d;
  padding: 20px 15px;
  border-radius: 10px;
}
.plans_table_sec h3 {
  text-transform: uppercase;
  color: #e8304d;
}
.plan_price_sec span {
  font-size: 30px;
  font-weight: 800;
}
button.pricing_btn {
  border: 1px solid #e8304d;
  background: transparent;
  width: 100%;
  border-radius: 30px;
  color: #e8304d;
  text-transform: uppercase;
  font-size: 14px;
  font-weight: 800;
  padding: 5px;
  margin-bottom: 15px;
}
.plan_list{list-style:none; padding:15px}
h6{padding-left:15px;}
section.footer_section {
  padding: 50px 50px 80px;
}
.subHeading {
  font-size: 18px !important;
  text-transform: capitalize;
  font-weight:600 !important;
}
.email-valid-text .validation{color:#e8304d;}
</style>
<body id="signup-page2">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57XFPLD"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
  <header  class="form-header">
    <div class="container">
      <div class="row">
        <div class="col-6 col-sm-4 col-md-3"><div id="logo" class="sign-logo">
          <a href="<?php echo base_url(); ?>" rel="nofollow"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/logo.png" alt="Graphics Zoo" title="Graphics Zoo" /></a>
        </div>
      </div>
      <div class="col-6 col-sm-8 col-md-9">
        <div class="right-number">
          <!-- <div class="number"> <span><i class="fas fa-phone"></i></span><a href="tel:(800) 225-6674">(800) 225-6674</a></div> -->
          <a href="<?php echo base_url(); ?>login" class="button mar-space"> Login</a>
        </div>
      </div></div></div>
    </header>
    <section class="page-heading-sec">
     <div class="container">
       <div class="row">
         <div class="col-md-12">
           <h1>Create a free account</h1>
           <p>Ready to get started? Create an account, it takes less 
           than a minute.</p>
         </div>
       </div>
     </div>
   </section>

   <!-- Modal -->
   <div id="SignupModal" class="modal_ fade_" role="dialog_">
    <div class="col-lg-5 col-md-6 col-sm-8 sign-up-pagep pb-info">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <div class="">
            <?php if($this->session->flashdata('message_error') != '') {?>
             <div class="alert alert-danger alert-dismissable">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
              <strong><?php echo $this->session->flashdata('message_error'); ?></strong>
            </div>
          <?php }?>
          <?php if($this->session->flashdata('message_success') != '') {?>
           <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
            <strong><?php echo $this->session->flashdata('message_success');?></strong>
          </div>
        <?php }?>

        <div class="col-md-12">
          <form action="" method="post" id="without_payform" role="form" class="contactForm">

           <!--  <h2>Account Information</h2>
            <hr class="line-sign-up2"> -->
            <div class="row col_2">
              <div class="form-group col-md-6 inner-addon left-addon">
                <input type="text" name="first_name" class="form-control" id="fname" placeholder="First Name *" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="">
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6 inner-addon left-addon">
                <input type="text" name="last_name" class="form-control" id="lname" placeholder="Last Name *" data-rule="minlen:4" data-msg="Please enter at least 4 chars" required="">
                <div class="validation"></div>
              </div>
            </div>
            <div class="row col_2">
              <div class="form-group col-md-12 inner-addon left-addon email-valid-text">
                <input type="email" class="form-control" name="email" id="email" placeholder="Email *" data-rule="email" data-msg="Please enter a valid email" required="">
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-12 inner-addon left-addon">
                <input type="password" class="form-control" name="password" id="password" placeholder="Password *" data-rule="minlen:4" data-msg="Please enter the password" required="">
                <div class="validation"></div>
              </div>
            </div>


            <div class="row col_2">
              <div class="form-group col-md-12 inner-addon left-addon">
                <input type="tel" class="form-control" id="phone" name= "phone" placeholder="Phone" data-rule="phone">
                <div class="validation"></div>
              </div>
            </div>


            <div class="" id="chk-box-pr">
              <div class="form-group">
                <div class="form-radion2">
                  <label class=""> 
                    <p class="terms-txt">
                     By clicking the below button, you agree to our <a href="<?php echo base_url() ?>termsandconditions" target="_blank" rel="canonical">Terms</a> and that you have read our <a href="<?php echo base_url() ?>privacypolicy" target="_blank" rel="canonical">Data Use Policy</a>, including our Cookie Use.
<!--                     <input type="checkbox" name="term" value="1" class="form-control" required="">
                     <span class="checkmark"></span>-->
                   </p>
                 </label>
               </div>
             </div>

           </div>
           <div class="text-left"><button class="button sign-up-p" type="submit"> Sign Up Now</button></div>
         </form>
       </div>
     </div>
   </div>

 </div>
</div>

</div>
</div>
<!--==========================
Hero Section
============================-->

<!--==========================
Footer
============================-->
<section class="footer_section">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        Secured By <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/comodo-secure.png" width="110" >
      </div>
    </div>
  </div>
</section>

<!-- JavaScript Libraries -->
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/jquery/jquery.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/jquery/jquery-migrate.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/superfish/hoverIntent.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/superfish/superfish.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/easing/easing.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/lib/wow/wow.min.js"></script>
<!-- Contact Form JavaScript File -->
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/contactform/contactform.js"></script>

<!-- Template Main Javascript File -->
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/main.js"></script>

<!-- Drip -->
<script type="text/javascript">
  var legalDomains = {"-yahoo.com": true,"-gmail.com": true,"-hotmail.com": true,"-msn.com": true,
  "-outlook.com": true,"-icloud.com": true,"-aol.com": true,"-hotmail.co.uk": true,"-hotmail.fr":true,
  "-rediffmail.com": true,"-yahoo.fr": true,"-live.com": true,"-free.fr": true};

  function validateEmailDomain(str) {
    var matches = str.match(/@(.*)$/);
    if (matches) {
      if (("-" + matches[1]) in legalDomains) {
        return(true);
      }
    }
    return(false);
  }           
//    $( "#without_payform" ).submit(function( e ) {
//        var emailval = $('#email').val();
//        $(".email-valid-text .validation").html("");
//        var isGenericemailID = validateEmailDomain(emailval);
//        if(isGenericemailID == true){
//            $(".email-valid-text .validation").html("<span>Please enter valid business email address</span>");
//            return false;
//        }else{
//          return true;  
//        }
//    });
</script>
<!-- REFERSION TRACKING: BEGIN -->
<script rel="nofollow" src="//graphicszoo.refersion.com/tracker/v3/pub_01537b7d8dcde95d5a0d.js"></script>
<script>_refersion();</script>
<!-- REFERSION TRACKING: END -->
<!-- UTM Tracking: BEGIN -->
<script rel="nofollow" src="https://d12ue6f2329cfl.cloudfront.net/resources/utm_form-1.0.4.min.js" async></script>
<script type="text/javascript" charset="utf-8">
  var _uf = _uf || {};
  _uf.utm_source_field           = "YOUR_SOURCE_FIELD"; // Default 'USOURCE'
  _uf.utm_medium_field           = "YOUR_MEDIUM_FIELD"; // Default 'UMEDIUM'
  _uf.utm_campaign_field         = "YOUR_CAMPAIGN_FIELD"; // Default 'UCAMPAIGN'
  _uf.utm_content_field          = "YOUR_CONTENT_FIELD"; // Default 'UCONTENT'
  _uf.utm_term_field             = "YOUR_TERM_FIELD"; // Default 'UTERM'
    _uf.sessionLength = 2; // In hours. Default is 1 hour
 _uf.add_to_form = "first"; // 'none', 'all', 'first'. Default is 'all'
 console.log(_uf,'_uf');
</script>
</body>
</html>