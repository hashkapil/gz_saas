<?php $CI =& get_instance();
$CI->load->library('customfunctions');
if(isset($_GET['pln']) && $_GET['pln'] != ""){
    $monthlyprice = $all_tier_prices[$_GET['pln']];
}else{
    $monthlyprice = $all_tier_prices[SUBSCRIPTION_MONTHLY];
}
$ip = $this->input->ip_address();
$pageName = $this->input->get("pln");
if($pageName!=""){
   $pageName = "pricing_page"; 
   $planFrom =  $this->input->get("pln");
}
 ?>
<html lang="en">
    <head>   
        <style>
            section.publection-sec {
                display: none;
            }
            select#state {
                height: auto;
                padding: 17px;
                margin: 10px 0;
                background: url(https://s3.console.aws.amazon.com/public/assets/front_end/Updated_Design/img/sign-nep.png);
                background-repeat: no-repeat;
                background-position: 95% 50%;
                background-size: 19px;
                -webkit-appearance: none;
                border-radius: 8px;
            }
            @media (max-width: 992px){
                section.page-heading-sec {
                    margin-top: 0 !important;    padding-top: 40px !important;
                }
            }
        </style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Graphics Zoo Signup | Register a New Account.</title>
        <meta name="description" content="Register a new account for Graphics Zoo design service can take advantage of our monthly packages and plans.">
        <!-- Bootstrap CSS -->
        <link rel="shortcut icon canonical" type="image/x-icon" href="<?php echo FAV_ICON_PATH; ?>">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" rel="stylesheet nofollow canonical">
        <link rel="stylesheet nofollow canonical" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
        <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.css">
        <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.theme.default.min.css">
        <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/jquery.fancybox.css"/>
        <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/bootstrap.min.css">
        <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/font-awesome.min.css">
        <link rel="stylesheet canonical" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'front_end/Updated_Design/css/style.css'); ?>">
        <link rel="stylesheet canonical" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'front_end/Updated_Design/css/custom.css'); ?>">
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/jquery.min.js"></script>
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.js"></script>
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/front_end.js"></script>
        <script rel="nofollow" src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.5/waypoints.min.js"></script>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-57XFPLD');</script>
        <!-- End Google Tag Manager -->
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57XFPLD" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <input type="hidden" value="<?php echo $ip; ?>" id="current_ip">
        <input type="hidden" value="<?php echo $pageName; ?>" id="from_page">
        <input type="hidden" value="<?php echo $planFrom; ?>" id="from_page_plan">        
        <header  class="form-header">
            <div class="container">
                <div class="row">
                    <div class="col-6 col-sm-4 col-md-3"><div id="logo" class="sign-logo">
                            <a href="<?php echo base_url(); ?>" rel="nofollow"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/logo.png" alt="Graphics Zoo" title="Graphics Zoo" /></a>
                        </div>
                    </div>
                    <div class="col-6 col-sm-8 col-md-9">
                        <div class="right-number">
                            <a rel="canonical" href="<?php echo base_url(); ?>login" class="button mar-space"> Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section class="page-heading-sec">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Create your account</h1>
                        <p>Ready to get started? Create an account, it takes less than a minute.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="sing-up-heading ">
            <div class="container">
                <div class="row sign-here">
                    <div class="col-md-12 text-center">
                        <div class="both-outer">
                            <?php if ($this->session->flashdata('message_error') != '') { ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                                    <strong><?php echo $this->session->flashdata('message_error'); ?></strong>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('message_success') != '') { ?>
                                <div class="alert alert-success alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                                    <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                                </div>
                            <?php } ?>
                            <div class="parsonial-plan">

                                <div class="par-out">
                                    <div class="select-bx-p">
                                        <div class="custom-select" >
                                            <select  id="priceselector">
                                                <?php
                                                $selected_plan_id = (isset($_GET['pln']) && $_GET['pln'] != '') ? $_GET['pln'] : SUBSCRIPTION_MONTHLY;
                                                for ($i = 0; $i < sizeof($subscription_list); $i++) {
                                                    $priceexplode = explode('-', $subscription_list[$i]['plan_name']);
                                                    $Netprice = explode('/', $priceexplode[0]);
                                                    $timeperiod = "";
                                                    if (str_replace(' ', '', $Netprice[0]) == "AnnualGoldenPlan") {
                                                        $timeperiod = "Annual";
                                                    } elseif (str_replace(' ', '', $Netprice[0]) == "MonthlyGoldenPlan") {
                                                        $timeperiod = "Monthly";
                                                    }
                                                    //echo $_GET['pln'];
                                                    if ($selected_plan_id == $subscription_list[$i]['plan_id']) {
                                                        $selected = "selected";
                                                    } else {
                                                        $selected = "";
                                                    }
                                                    echo '<option value="' . $subscription_list[$i]['plan_id'] . '" data-amount="' . $subscription_list[$i]['plan_price'] . '" ' . $selected . '>' . $subscription_list[$i]['plan_name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="monthly-activate">
                                        <div class="first-Ul" style="display:none"> 
                                            <span>Up to </span><select  class="pricebasedquantity">
                                                <option value="2" >2</option>
                                            </select><span>active requests</span></div>
                                        <ul style="width: 100%">
                                        </ul>
                                    </div>
                                    <div class="total-pPrice">
                                        <p class="plan-price">
                                            Plan Price:
                                            <span id="change_prc" class="box item1">$
                                                <span></span>
                                            </span>
                                        </p>
                                        <p class="plan-price tax_prc_for_texas">


                                        </p>

                                        <hr>
                                        <div class="g-total">
                                            <h3>Grand Total <span id="grand_total">$<span></span></span></h3>
                                        </div>
                                        <span class="coupon-des-newsignup"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="bill-infoo">
                                <div class="pb-info"> 
                                    <form action="" method="post" role="form" class="contactForm" id="normal_signup">
                                        <input type="hidden" value="M299G"  name="plan_name" id="annual_price">
                                        <input type="hidden" value="1"  name="in_progress_request" id="in_progress_request">
                                        <input type="hidden" id="selected_price" name="selected_price" value=""/>
                                        <input type="hidden" id="final_plan_price" name="final_plan_price" value=""/>
                                        <input type="hidden" id="state_tax" name="state_tax" value="0"/>
                                        <input type="hidden" id="tax_amount" name="tax_amount" value="0"/>
                                        <input type="hidden" id="couponinserted_ornot" class="couponinserted_ornot" name="couponinserted_ornot" value=""/>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="input-group"> 
                                                        <input type="text"  name="first_name"  id="fname" class="form-control" data-rule="minlen:4" data-msg="Please enter First Name" placeholder="First Name *" onblur="Capture_values($(this))" required="">
                                                    </div>
                                                    <div class="validation"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <div class="input-group"> 
                                                        <input type="text" name="last_name" class="form-control" id="lname" placeholder="Last Name *"  data-msg="Please enter Last Name"   onblur="Capture_values($(this))" required="">
                                                    </div>
                                                    <div class="validation"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="input-group"> 
                                                        <input type="email"   name="email" class="form-control" id="email" placeholder="Email Address *" data-rule="email" data-msg="Please enter a valid email" onblur="Capture_values($(this))" required="">
                                                    </div>
                                                    <div class="validation"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="password" name="password" class="form-control" placeholder="Password *" data-rule="minlen:4" data-msg="Please enter the password" onblur="Capture_values($(this))" required="" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="card_number" placeholder="Card Number *" required="" maxlength="19" onblur="Capture_values($(this))">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" id="expir_date" name="expir_date" class="form-control" placeholder="MM  /  YY *" onkeyup="dateFormat(this,this.value);" onblur="Capture_values($(this))" required="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6 cvv">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <input type="text" name="cvc" class="form-control" placeholder="CVV *" maxlength="4" onblur="Capture_values($(this))" required="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control disc-code" name="Discount" placeholder="Discount Code"  data-msg="Please enter a valid email">
                                                    </div>
                                                    <div class="ErrorMsg epassError disc-code">
                                                        <!--                                                    <p class="alert alert-danger"></p>-->
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <select name="state" id="state" class="form-control" onchange="Capture_values($(this))" required="">
                                                            <option value="" selected="selected">Select a State</option>
                                                            <option value="International Customer">International Customer</option>
                                                            <option value="---">-----------------------</option>
                                                            <option value="Alabama">Alabama</option>
                                                            <option value="Alaska">Alaska</option>
                                                            <option value="Arizona">Arizona</option>
                                                            <option value="Arkansas">Arkansas</option>
                                                            <option value="California">California</option>
                                                            <option value="Colorado">Colorado</option>
                                                            <option value="Connecticut">Connecticut</option>
                                                            <option value="Delaware">Delaware</option>
                                                            <option value="District Of Columbia">District Of Columbia</option>
                                                            <option value="Florida">Florida</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Hawaii">Hawaii</option>
                                                            <option value="Idaho">Idaho</option>
                                                            <option value="Illinois">Illinois</option>
                                                            <option value="Indiana">Indiana</option>
                                                            <option value="Iowa">Iowa</option>
                                                            <option value="Kansas">Kansas</option>
                                                            <option value="Kentucky">Kentucky</option>
                                                            <option value="Louisiana">Louisiana</option>
                                                            <option value="Maine">Maine</option>
                                                            <option value="Maryland">Maryland</option>
                                                            <option value="Massachusetts">Massachusetts</option>
                                                            <option value="Michigan">Michigan</option>
                                                            <option value="Minnesota">Minnesota</option>
                                                            <option value="Mississippi">Mississippi</option>
                                                            <option value="Missouri">Missouri</option>
                                                            <option value="Montana">Montana</option>
                                                            <option value="Nebraska">Nebraska</option>
                                                            <option value="Nevada">Nevada</option>
                                                            <option value="New Hampshire">New Hampshire</option>
                                                            <option value="New Jersey">New Jersey</option>
                                                            <option value="New Mexico">New Mexico</option>
                                                            <option value="New York">New York</option>
                                                            <option value="North Carolina">North Carolina</option>
                                                            <option value="North Dakota">North Dakota</option>
                                                            <option value="Ohio">Ohio</option>
                                                            <option value="Oklahoma">Oklahoma</option>
                                                            <option value="Oregon">Oregon</option>
                                                            <option value="Pennsylvania">Pennsylvania</option>
                                                            <option value="Rhode Island">Rhode Island</option>
                                                            <option value="South Carolina">South Carolina</option>
                                                            <option value="South Dakota">South Dakota</option>
                                                            <option value="Tennessee">Tennessee</option>
                                                            <option value="texas">Texas</option>
                                                            <option value="Utah">Utah</option>
                                                            <option value="Vermont">Vermont</option>
                                                            <option value="Virginia">Virginia</option>
                                                            <option value="Washington">Washington</option>
                                                            <option value="est Virginia">West Virginia</option>
                                                            <option value="Wisconsin">Wisconsin</option>
                                                            <option value="Wyoming">Wyoming</option>
                                                        </select>
                                                    </div>
                                                    <div class="ErrorMsg state_validation"></div>
                                                </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="ErrorMsg epassErrorSuccess disc-code">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="form-radion2">
                                                        <label class=""> 
                                                            <p class="terms-txt">
                                                                By clicking the below button, you agree to our <a href="<?php echo base_url() ?>termsandconditions" rel="canonical" target="_blank">Terms</a> and that you have read our <a href="<?php echo base_url() ?>privacypolicy" rel="canonical" target="_blank">Data Use Policy</a>, including our Cookie Use.
        <!--                                                       <input type="checkbox" name="term" value="1" class="form-control" required="">
                                                               <span class="checkmark"></span>-->
                                                            </p>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="display: flex; justify-content: center;width: 100%;padding-bottom: 15px;">
                                                <img class="loaderimg" src="">
                                            </div>



                                        </div>
                                        <input type="submit" name="submit" id="submit" value="Finish and Pay" class="red-theme-btn">

                                        <div class="card-sec row">
                                            <div class="col-lg-12  col-12 text-center">
                                                <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/stripe-lock.png" class="img-fluid stripe-lock" alt="stripe-lock">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/bootstrap.min.js" ></script>
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/jquery.fancybox.js"></script>
        <script type="text/javascript">
            var subscription_99 = '<?php echo SUBSCRIPTION_99_PLAN; ?>';
            var subscription_month = '<?php echo SUBSCRIPTION_MONTHLY; ?>';
            var allplans_price = <?php echo json_encode($all_tier_prices); ?>;
            var selected_prices = <?php echo json_encode($monthlyprice); ?>;
         //   var agencyplan = '<?php echo AGENCY_PLAN; ?>';
            var subscription_plan = <?php echo json_encode($subscription_data); ?>;
            var annual_subscription_plan = <?php echo json_encode($annual_subscription_data); ?>;
            var total_inprogress = '<?php echo isset($_GET['val']) ? $_GET['val'] : ''; ?>';
            var total_active_requests = '<?php echo TOTAL_ACTIVE_REQUEST; ?>';
            var sales_tax = <?php echo json_encode(STATE_TEXAS); ?>;
            var assetspath = '<?php echo FS_PATH_PUBLIC_ASSETS ?>';
            var BASE_URL = "<?php echo base_url(); ?>";
            $rowperpage = "<?php echo LIMIT_ADMIN_LIST_COUNT; ?>";
            $plan_price = '';
            $discount_amount = 0;
            $tax_amount = 0;
            $tax_value = 0;
            $discount_notes = '';
            var ip = "<?php echo $ip; ?>";

            var x, i, j, selElmnt, a, b, c;
            /*look for any elements with the class "custom-select":*/
            x = document.getElementsByClassName("custom-select");
            for (i = 0; i < x.length; i++) {
                selElmnt = x[i].getElementsByTagName("select")[0];
                /*for each element, create a new DIV that will act as the selected item:*/
                a = document.createElement("DIV");
                a.setAttribute("class", "select-selected");
                a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
                x[i].appendChild(a);
                /*for each element, create a new DIV that will contain the option list:*/
                b = document.createElement("DIV");
                b.setAttribute("class", "select-items select-hide");
                for (j = 0; j < selElmnt.length; j++) {
                    /*for each option in the original select element,
                     create a new DIV that will act as an option item:*/
                    c = document.createElement("DIV");
                    c.innerHTML = selElmnt.options[j].innerHTML;
                    c.setAttribute("value", selElmnt.options[j].value);
                    c.setAttribute("data-amount", selElmnt.options[j].dataset.amount);
                    c.addEventListener("click", function (e) {
                        /*when an item is clicked, update the original select box,
                         and the selected item:*/
                        var y, i, k, s, h;
                        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                        h = this.parentNode.previousSibling;
                        for (i = 0; i < s.length; i++) {
                            if (s.options[i].innerHTML == this.innerHTML) {
                                s.selectedIndex = i;
                                h.innerHTML = this.innerHTML;
                                y = this.parentNode.getElementsByClassName("same-as-selected");
                                for (k = 0; k < y.length; k++) {
                                    y[k].removeAttribute("class");
                                }
                                this.setAttribute("class", "same-as-selected");
                                break;
                            }

                        }
                        h.click();
                    });
                    b.appendChild(c);
                }
                x[i].appendChild(b);
                a.addEventListener("click", function (e) {
                    /*when the select box is clicked, close any other select boxes,
                     and open/close the current select box:*/
                    e.stopPropagation();
                    closeAllSelect(this);
                    this.nextSibling.classList.toggle("select-hide");
                    this.classList.toggle("select-arrow-active");
                });
            }

            !function () {
                var t;
                if (t = window.driftt = window.drift = window.driftt || [], !t.init)
                    return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0,
                            t.methods = ["identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on"],
                            t.factory = function (e) {
                                return function () {
                                    var n;
                                    return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                                };
                            }, t.methods.forEach(function (e) {
                        t[e] = t.factory(e);
                    }), t.load = function (t) {
                        var e, n, o, i;
                        e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"),
                                o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js",
                                n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
                    });
            }();
            drift.SNIPPET_VERSION = '0.3.1';
            drift.load('d6yi38hkhwsd');
        </script>
        <!-- REFERSION TRACKING: BEGIN -->
        <script rel="nofollow" src="//graphicszoo.refersion.com/tracker/v3/pub_01537b7d8dcde95d5a0d.js"></script>
        <script>_refersion();</script>
        <!-- REFERSION TRACKING: END -->
        <!-- UTM Tracking: BEGIN -->
        <script rel="nofollow" src="https://d12ue6f2329cfl.cloudfront.net/resources/utm_form-1.0.4.min.js" async></script>
        <script type="text/javascript" charset="utf-8">
        var _uf = _uf || {};
        _uf.utm_source_field = "YOUR_SOURCE_FIELD"; // Default 'USOURCE'
        _uf.utm_medium_field = "YOUR_MEDIUM_FIELD"; // Default 'UMEDIUM'
        _uf.utm_campaign_field = "YOUR_CAMPAIGN_FIELD"; // Default 'UCAMPAIGN'
        _uf.utm_content_field = "YOUR_CONTENT_FIELD"; // Default 'UCONTENT'
        _uf.utm_term_field = "YOUR_TERM_FIELD"; // Default 'UTERM'
        _uf.sessionLength = 2; // In hours. Default is 1 hour
        _uf.add_to_form = "first"; // 'none', 'all', 'first'. Default is 'all'
        
        //UTM Tracking: END
        
        $("input[name = 'card_number'],input[name = 'expir_date'],input[name = 'cvc']").on('keypress', function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          //display error message
          //$("#errmsg").html("Digits Only").show().fadeOut("slow");
                 return false;
        }

        });
        $("input[name = 'card_number']").on('keypress change', function (e) {
            var maxlenth = $(this).attr("maxlength");

           $(this).val(function (index, value) {  
              return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
           });

        });
        </script>
        <?php
        $checkIdExist = $this->session->userdata('vistedid');
        if (empty($checkIdExist)) {
            ?>
            <script>
                $(document).ready(function(){
                    var from_page = $("#from_page").val(); 
                    var from_page_plan = $("#from_page_plan").val(); 
                    Capture_values("0","unknown",ip,from_page,from_page_plan);
                });
       
            </script>
        <?php } ?> 
    </body>
</html>