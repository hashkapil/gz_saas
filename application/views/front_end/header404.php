<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="shortcut icon canonical" type="image/x-icon" href="<?php echo FAV_ICON_PATH; ?>">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" rel="stylesheet canonical">
    <link rel="stylesheet canonical" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.css">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.theme.default.min.css">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/jquery.fancybox.css"/>
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/bootstrap.min.css">
    <link rel="stylesheet canonical" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/font-awesome.min.css">
    <link rel="stylesheet canonical" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'front_end/Updated_Design/css/style.css');?>">
    <link rel="stylesheet canonical" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS.'front_end/Updated_Design/css/custom.css');?>">
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/jquery.min.js"></script>
    <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.5/waypoints.min.js"></script>
    <?php 
    $currenturl = current_url();
        $pagename = explode('/',$currenturl);
        if((!in_array('portfolio',$pagename)) && (!in_array('pricing',$pagename))){ ?>
            <script src="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'js/front_end.js'); ?>"></script>
        <?php } ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="index,follow"/>
    <?php
    $metatitle = "";
    $metadesc = "";
    $metakeywords="";
    $pageUrl = $page ? $page : '';
    $myuri = $_SERVER['REQUEST_URI'];
//    echo "<pre/>";print_R($pageUrl);
    if ($myuri == "/aboutus") {
//$metatitle="Graphics Zoo: An Ultimate Graphic Design Company & Tools in Houston";
        $metatitle = "Graphics Zoo: Graphic Design Company | Online Design Services";
        $metadesc = "How do you find a graphic designer or best graphics company to make your business impressive? Visit Graphics Zoo for on-demand design, logos & brand design services.";
        $metakeywords = "logo and graphic design services, best graphics company, unlimited logo design, graphic design online";
    } else if ($myuri == "/portfolio") {
        $metatitle = "Graphics Zoo: Creative Graphic Design Work | Unlimited Logo Design";
        $metadesc = "Hire us for creative graphic design services to create an impressive business logo, brochures, flyers, web pages, and designs for t-shirts that match your style.";
        $metakeywords = "unlimited logo design, logo and graphic design services, creative graphic designing work, online design services";
    } else if ($myuri == "/features") {
 
        $metatitle = "Graphics Zoo - Features | Graphic Design Service Provider";
        $metadesc = "Are you on a lookout for a professional graphic design agency? Graphics Zoo excels in expert online graphic design services from logo design to print design within your budget.";
        $metakeywords = "online graphic design services, graphic design service provider, graphic design online, monthly graphic design package";
    } else if ($myuri == "/testimonials") {
        $metatitle = "Graphics Zoo - Testimonials | Creative Graphic Design Studio";
        $metadesc = "Read testimonials of what our client say about our unlimited graphic design service. We are also available to answer your questions. You can contact us anytime.";
         }else if ($myuri == "/blog") {
        $metatitle = "Graphics Zoo - Blog | Online Graphic Design Company";
        $metadesc = "Read our latest blog on the logo, advertising & social media graphics, brochures, flyers, web pages, and designs for t-shirts and trending news of the graphic design industry.";
    } else if ($myuri == "/faq") {
        $metatitle = "Graphics Zoo - Graphic Design Frequently Asked Questions";
        $metadesc = "The answers of most frequently asked questions about our unlimited graphic design service. If you can't find the answer you're looking for, please contact us directly.";
    } else if ($myuri == "/privacypolicy") {
        $metatitle = "Graphics Zoo - Privacy Policy | Graphic Design Advertising Company";
        $metadesc = "Our privacy policy has been compiled to better serve those who are concerned with how their Personally Identifiable Information (PII) is being used online.";
    } else if ($myuri == "/support") {
        $metatitle = "Graphics Zoo - Support | Monthly Graphics Design Services";
        $metadesc = "Feel free to contact us through email, chat and social media channels. We are 24 hours available to help you. You can also fill out the contact form to connect with us.";
    } else if ($myuri == "/login") {
        $metatitle = "Graphics Zoo - Login | Online Unlimited Graphic Design";
        $metadesc = "Login your account with your email id and password and get started with Graphics Zoo to take advantage of our cost-effective graphic design services.";
    } else if ($myuri == "/pricing") {
        $metatitle = "Graphics Zoo : Unlimited Graphic Design Packages & Monthly Plans";
        $metadesc = "Get the best monthly graphic design service for your brand. Our graphic design packages start at $ 99 under startup Plan, the business plan under $349 to $649 under agency plan.";
        $metakeywords="graphic design packages, rates for graphic design services, monthly graphic design package";
    }else if ($myuri == "/termsandconditions") {
        $metatitle = "Graphics Zoo  - Terms and Conditions | Unlimited Design Services";
        $metadesc = "You can find the term and condition that covering the terms of our affordable graphic design service to satisfy your business needs and expectations.";
        $metakeywords="online design services,best graphics company,graphic design packages";
    }else if ($myuri == "/") {
        $metatitle = "Graphics Zoo - Unlimited Graphic Design Services";
        $metadesc = "Graphics Zoo gives unlimited graphic design services with monthly packages for your business. Our expert team provides monthly graphic design service.";
        $metakeywords = "monthly graphic design service, unlimited graphic design packages, creative graphic design company, brand design agency, logo graphic design services, social media graphic design, advertising graphic design, marketing graphic design";
    } else if(strpos($myuri, '/pages/posts/') !== false) {
        $metatitle = "Page Not Found | Graphics Zoo";
        $metadesc = "The page which you are looking for is not available on the server";
    }else if ($pageUrl == "404") {
        $metatitle = "Page Not Found | Graphics Zoo";
        $metadesc = "The page which you are looking for is not available on the server";
    }
    else {
        $metatitle = "Graphics Zoo Offers Unlimited Graphic Design Services & Packages in Houston";
        $metadesc = "Are you on a lookout for an unlimited professional graphic design company in Houston? Graphics Zoo excels in expert graphic design services within your budget.";
    }
    ?>
    <title><?php echo $metatitle ?></title>
    <meta name="description" content="<?php echo $metadesc ?>">
    <meta name="keywords" content="<?php echo $metakeywords ?>">
    <?php
    $myuri = $_SERVER['REQUEST_URI'];
    if ($myuri == "/aboutus") {
        ?>
        <meta property="og:title" content="Graphics Zoo: On Demand Graphic Design Services"/>
        <meta property="og:description" content="How do you find a graphic designer or outsource graphic design company to make your business impressive? Visit Graphics Zoo for on-demand design and marketing graphic design services at affordable prices."/>
        <meta property="og:site_name" content="Graphicszoo.com" />

        <meta property="twitter:card" content="summary"/>
        <meta property="twitter:title" content="Graphics Zoo: On Demand Graphic Design Services"/>
        <meta property="twitter:description" content="How do you find a graphic designer or outsource graphic design company to make your business impressive? Visit Graphics Zoo for on-demand design and marketing graphic design services at affordable prices."/>
        <meta property="twitter:URL" content="@add twitter handle">

    <?php }if ($myuri == "/portfolio") { ?>
        <meta property="og:title" content="Graphics Zoo: Logo, Artwork, Print, Website & T-shirt Graphic Design Company"/>
        <meta property="og:description" content="Hire freelance logo designers and t-shirt graphic designers to create an impressive business logo and designs for t-shirts. Our logo and t-shirt artwork designs match your style and build a strong impact on the industry. Take a look at our designs that we have delivered to our clients."/>
        <meta property="og:site_name" content="Graphicszoo.com" />

        <meta property="twitter:card" content="summary"/>
        <meta property="twitter:title" content="Graphics Zoo: Logo, Artwork, Print, Website & T-shirt Graphic Design Company"/>
        <meta property="twitter:description" content="Hire freelance logo designers and t-shirt graphic designers to create an impressive business logo and designs for t-shirts. Our logo and t-shirt artwork designs match your style and build a strong impact on the industry. Take a look at our designs that we have delivered to our clients."/>
        <meta property="twitter:URL" content="@add twitter handle">
    <?php }if ($myuri == "/pricing") { ?>
        <meta property="og:title" content="Graphics Zoo Offers Website Design Subscription & Digital Graphic Design Tools" />
        <meta property="og:description" content="Are you on a hunt for digital graphic design tools or website design subscription services? Get in touch with Graphics Zoo. We offer cost-effective monthly packages for digital graphic design."/>
        <meta property="og:site_name" content="Graphicszoo.com" />

        <meta property="twitter:card" content="summary"/>
        <meta property="twitter:title" content="Graphics Zoo Offers Website Design Subscription & Digital Graphic Design Tools" />
        <meta property="twitter:description" content="Are you on a hunt for digital graphic design tools or website design subscription services? Get in touch with Graphics Zoo. We offer cost-effective monthly packages for digital graphic design." />
        <meta property="twitter:URL" content="@add twitter handle">
    <?php }if ($myuri == "/") { ?>
        <meta property="og:title" content="Graphics Zoo Offers Unlimited Graphic Design Services & Packages"/>
        <meta property="og:description" content="Are you on a lookout for a graphic design company ? Graphics Zoo gives unlimited graphic design services packages for your business. Our expert team provides professional graphic design services and helps you connect with your target audience."/>
        <meta property="og:site_name" content="Graphicszoo.com" />

        <meta property="twitter:card" content="summary"/>
        <meta property="twitter:title" content="Graphics Zoo Offers Unlimited Graphic Design Services & Packages"/>
        <meta property="twitter:description" content="Are you on a lookout for a graphic design company ? Graphics Zoo gives unlimited graphic design services packages for your business. Our expert team provides professional graphic design services and helps you connect with your target audience."/>
        <meta property="twitter:URL" content="@add twitter handle">
    <?php } ?>


    <?php
    $myuri = $_SERVER['REQUEST_URI'];
    if ($myuri == "/") {
        ?>
        <link rel="canonical" href="<?php echo base_url();?>" />
    <?php }if ($myuri == "/aboutus") { ?>
        <link rel="canonical" href="<?php echo base_url();?>aboutus" />
    <?php }if ($myuri == "/portfolio") { ?>
        <link rel="canonical" href="<?php echo base_url();?>portfolio" />
    <?php }if ($myuri == "/pricing") { ?>
        <link rel="canonical" href="<?php echo base_url();?>pricing" />
    <?php }if ($myuri == "/blog") { ?>
        <link rel="canonical" href="<?php echo base_url();?>blog" />
    <?php }if ($myuri == "/faq") { ?>
        <link rel="canonical" href="<?php echo base_url();?>faq" />
    <?php }if ($myuri == "/support") { ?>
        <link rel="canonical" href="<?php echo base_url();?>support" />
    <?php }if ($myuri == "/terms-and-conditions") { ?>
        <link rel="canonical" href="<?php echo base_url();?>terms-and-conditions" />
    <?php }if ($myuri == "/privacy-policy") { ?>
        <link rel="canonical" href="<?php echo base_url();?>privacy-policy" />
    <?php } ?>
        
<!---Start global variables -->
<script>
var BASE_URL = "<?php echo base_url(); ?>";
$rowperpage = <?php echo LIMIT_ADMIN_LIST_COUNT; ?>;
var subscription_plan = '<?php echo json_encode(SUBSCRIPTION_DATA); ?>';
var total_active_requests = "<?php echo TOTAL_ACTIVE_REQUEST; ?>";

</script>
<!--- End global variables -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-103722162-1"></script>
<meta name="google-site-verification" content="jNJk_m4f6Y2kYTcA9kqXNX1A4evH5jlfDds6Rl9xHL0" />
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-103722162-1');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-57XFPLD');</script>
<!-- End Google Tag Manager -->
<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s)
    {
        if (f.fbq)
            return;
        n = f.fbq = function () {
            n.callMethod ?
            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq)
            f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '157875968111046');
    fbq('track', 'PageView');
</script>
<noscript>
    <img alt="facebook" height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=157875968111046&ev=PageView&noscript=1">
</noscript>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "LocalBusiness",

        "address": {
        "@type": "PostalAddress",
        "streetAddress": "245 Commerce Green Blvd.",
        "addressLocality": "Sugar Land",
        "addressRegion": "TX",
        "postalCode": "77479"

    },

    "name": "Graphics Zoo",
    "telephone": "888-976-2747",
    "email": "<?php echo SUPPORT_EMAIL; ?>",
    "url": "https://www.graphicszoo.com",
    "image": "<?php echo base_url();?>public/new_fe_images/logo.png",
    "priceRange": "$$",
    "sameAs" :
    [
    "https://www.facebook.com/graphicszoo1/",
    "https://www.instagram.com/graphics_zoo/"
    ]
}
</script>
</head>
<body 
<?php
$role_id = isset($_SESSION['role']) ? $_SESSION['role'] : "";
if (isset($id)) {
    echo "id=" . $id;
    if ($id == 'single-blog-page') {
        echo " class='blog-list'";
    }
}
?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57XFPLD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header id="header">
    <div class="container">
     <div class="flex-header">
        <div id="logo" class="pull-left">
            <a href="<?php echo base_url(); ?>" rel="nofollow"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/logo.svg" alt="GraphicsZoo" title="GraphicsZoo" /></a>
        </div>
        <button type="button" class="menu-toggle">
            <span></span>
            <span></span>
            <span></span>                                
        </button>
        <div class="mobile-class">
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class='<?php
                    if ($titlestatus == "portfolio") {
                        echo "active";
                    }
                    ?>'><a href="<?php echo base_url(); ?>portfolio" rel="canonical">Portfolio</a></li>
                    <li class='<?php
                    if ($titlestatus == "pricing") {
                        echo "active";
                    }
                    ?>'><a href="<?php echo base_url(); ?>pricing" rel="canonical">Pricing</a></li>
                    <li class='<?php
                    if ($titlestatus == "testimonial") {
                        echo "active";
                    }
                    ?>'><a href="<?php echo base_url(); ?>testimonials" rel="canonical">Testimonials</a></li>
                    <li class='<?php
                    if ($titlestatus == "features") {
                        echo "active";
                    }
                    ?>'><a href="<?php echo base_url(); ?>features" rel="canonical">Features</a></li>

                    <li class="resources"><a href="#/">Resources</a>
                       <ul>
                           <li><a href="<?php echo base_url(); ?>blog" rel="canonical">Blog</a></li>

                           <li>
                               <a href="<?php echo base_url(); ?>aboutus" rel="canonical">About Us</a>
                           </li>

                           <li>
                               <a href="<?php echo base_url(); ?>support" rel="canonical">Contact Us</a>
                           </li>
                       </ul>
                    </li>
                </ul>
            </nav>
            <div class="navbar-right clearfix">
                <ul>
                        <?php if ($role_id == 'customer') { ?>
                            <a rel="canonical" href="<?php echo base_url(); ?>customer/request/design_request" class="button" >DASHBOARD</a>
                        <?php } elseif ($role_id == 'designer') { ?>
                            <a rel="canonical" href="<?php echo base_url(); ?>designer/request" class="button">DASHBOARD</a>
                        <?php } elseif ($role_id == 'admin') { ?>
                            <a rel="canonical" href="<?php echo base_url(); ?>admin/dashboard" class="button">DASHBOARD</a>
                        <?php } elseif ($role_id == 'qa') { ?>
                            <a rel="canonical" href="<?php echo base_url(); ?>qa/dashboard" class="button">DASHBOARD</a>
                        <?php } elseif ($role_id == 'va') { ?>
                            <a rel="canonical" href="<?php echo base_url(); ?>va/dashboard" class="button">DASHBOARD</a>
                        <?php } else { ?>
                            <a rel="canonical" href="<?php echo base_url(); ?>login"  class="log-in">Login</a>
                            <a rel="canonical" href="<?php echo base_url(); ?>signup" class="button mar-space">SIGN UP</a>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        </div>
</header>