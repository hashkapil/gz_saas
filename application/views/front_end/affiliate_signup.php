<?php
$CI = & get_instance();
?>
<!doctype html>
<html lang="en">
    <head>   
        <style>

.review-items span {
    display: block;
    text-align: right;
    font-size: 16px;
    font-style: italic;
}
.review-items p {
    margin: 10px 2px;
        font-style: italic;
    position: relative;
    padding: 35px 25px 30px;
    box-shadow: 0px 1px 8px -1px #39393999;
    border-radius: 10px;
}
.review-items p::before {
        font-style: normal;
    content: "❝";
    position: absolute;
    font-size: 46px;
    z-index: 9999;
    letter-spacing: -1px;
    color: #4c4c4c;
    left: 12px;
    height: 30px;
    width: 30px;
    line-height: normal;
    top: -5px;
}
.review-items p::after {
    font-style: normal;
    height: 30px;
    width: 30px;
    content: "❞";
    position: absolute;
    font-size: 46px;
    z-index: 9999;
    letter-spacing: -1px;
    color: #4c4c4c;
    right: 12px;
    bottom: 12px;    text-align: right;
    top: auto;  
    line-height: normal;
}
.signup-review-slider {
    margin-top: 25px;
}
.signup-review-slider .owl-stage-outer {
    margin-bottom: 0;
}
        
            select#state {
                height: auto;
                padding: 17px;
                margin: 10px 0;
                background: url(https://s3.console.aws.amazon.com/public/assets/front_end/Updated_Design/img/sign-nep.png);
                background-repeat: no-repeat;
                background-position: 95% 50%;
                background-size: 19px;
                -webkit-appearance: none;
                border-radius: 8px;
            }
            @media (max-width: 992px){
                section.page-heading-sec {
                    margin-top: 0 !important;    padding-top: 40px !important;
                }
            }
        </style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Graphics Zoo | Register a New Account</title>
        <meta name="description" content="Register a new account for Graphics Zoo design service can take advantage of our monthly packages and plans.">
        <!-- Bootstrap CSS -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo FAV_ICON_PATH; ?>">
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,900" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
        <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.theme.default.min.css">
        <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/jquery.fancybox.css"/>
        <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'front_end/Updated_Design/css/style.css'); ?>">
        <link rel="stylesheet" href="<?php echo auto_version_asset(FS_PATH_PUBLIC_ASSETS . 'front_end/Updated_Design/css/custom.css'); ?>">
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/jquery.min.js"></script>
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/owl/owl.carousel.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.5/waypoints.min.js"></script>
        <script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>js/front_end.js"></script>
    </head>
    <body>
        <header  class="form-header">
            <div class="container">
                <div class="row">
                    <div class="col-6 col-sm-4 col-md-3"><div id="logo" class="sign-logo">
                            <a href="<?php echo base_url(); ?>"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/logo.png" alt="Graphics Zoo" title="Graphics Zoo" /></a>
                        </div>
                    </div>
                    <div class="col-6 col-sm-8 col-md-9">
                        <div class="right-number">
                            <!-- <div class="number"> <span><i class="fas fa-phone"></i></span><a href="tel:(800) 225-6674">(800) 225-6674</a></div> -->
                            <a href="<?php echo base_url(); ?>login" class="button mar-space"> Login</a>
                        </div>
                    </div></div></div>
        </header>
        <section class="page-heading-sec">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Create your affiliate account</h1>
                        <p>All the Commissions earned will be 10% of the monthly bill for the customer that you have 
reffered to Graphics Zoo for lifetime.</p>
                    </div>
                </div>
            </div>
        </section>
        <section class="sing-up-heading ">
            <div class="container">
                <div class="row sign-here affiliate_signup">
                    <div class="col-md-12 text-center">
                        <div class="both-outer bill-wrapper">
                            <?php if ($this->session->flashdata('message_error') != '') { ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                                    <strong><?php echo $this->session->flashdata('message_error'); ?></strong>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('message_success') != '') { ?>
                                <div class="alert alert-success alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                                    <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                                </div>
                            <?php } ?>
                            <div class="bill-infoo">
                                <div class="pb-info">
                                    <form action="" method="post" role="form" class="contactForm" id="affiliate_signup">
                                         <input type="hidden" value="<?php echo COMMISION_TYPE; ?>"  name="commision_type">
                                          <input type="hidden" value="<?php echo COMMISION_FEES;?>"  name="commision_fees">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>First Name</label>
                                                    <div class="input-group"> 
                                                        <input type="text"  name="first_name"  id="fname" class="form-control" data-rule="minlen:4" data-msg="Please enter First Name" required=""  onblur="Capture_values($(this))" placeholder="First Name *">
                                                    </div>
                                                    <div class="validation"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                <label>Last Name</label>
                                                    <div class="input-group"> 
                                                        <input type="text" name="last_name" class="form-control" id="lname" placeholder="Last Name *"  data-msg="Please enter Last Name" required="" onblur="Capture_values($(this))">
                                                    </div>
                                                    <div class="validation"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                <label>Email Address</label>
                                                    <div class="input-group"> 
                                                        <input type="email" autocomplete="false"  name="email" class="form-control" id="email" placeholder="Email Address *" data-rule="email" data-msg="Please enter a valid email" required="" onblur="Capture_values($(this))">
                                                    </div>
                                                    <div class="validation"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                <label>Password</label>
                                                    <div class="input-group">
                                                        <input type="password" id="password" name="password" class="form-control" placeholder="Password *" data-rule="minlen:4" data-msg="Please enter the password" required="" onblur="Capture_values($(this))" >
                                                    </div>
                                                </div>
                                            </div>
<!--                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group affiliated_key">
                                                            <div class="input-group">
                                                                <p class="refferal_val"><?php echo base_url(); ?>signup?referral=</p>
                                                                <input type="text" id="affiliated_key" name="affiliated_key" class="form-control" placeholder="Affiliated name *" required="">
                                                            </div>
                                                            <em>Enter referral username which will be used to create your affiliate link.</em>
                                                            <div class="ErrorMsg epassError aff_key">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>-->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="form-radion2">
                                                        <label class=""> 
                                                            <p class="terms-txt">
                                                                By clicking the below button, you agree to our <a href="<?php echo base_url() ?>termsandconditions" target="_blank">Terms</a> and that you have read our <a href="<?php echo base_url() ?>privacypolicy" target="_blank">Data Use Policy</a>, including our Cookie Use.
                                                            </p>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="display: flex; justify-content: center;width: 100%;padding-bottom: 15px;">
                                                <img class="loaderimg" src="">
                                            </div>
                                        </div>
                                        <input type="submit" name="submit" id="submit" value="Sign Up" class="red-theme-btn">
                                    </form>
                                </div>
                            </div>
                            <div class="parsonial-plan bill-cont">
                            <img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliate-signup/signup-img.jpg" class="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="affiliate-section affiliate-talk affil-signup-talk">
	<div class="container">
		<h2 class="affiliate-sub-heading">Our Affiliates Talk!</h2>
		<div class="row">
			<div class="col-md-4">
				<div class="talk-item">
					<div class="talker-header">				
						<div class="talker-image"><img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/talker-a.png" class=""></div>
						<div class="talker-info">
							<h4>Patrick Mills</h4>
							<span>New York, USA</span>
						</div>
						
					</div>
					<div class="talker-say">
							<p>Graphiczoo hands-down provides the best affiliate program in the industry with a mixture of unlimited offers for the affiliates. I have referred it to a lot of friends and acquaintances and they have been more than just pleased with the graphic designs and services. Earning has never been more fun for me.</p>
						</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="talk-item">
					<div class="talker-header">				
						<div class="talker-image"><img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/talker-b.png" class=""></div>
						<div class="talker-info">
							<h4>Patrick Mills</h4>
							<span>New York, USA</span>
						</div>
						
					</div>
					<div class="talker-say">
							<p>I have been an affiliate for years now and have collaborated with all types of brands. The experience with Graphiczoo was rather different and better in a lot of aspects. I love their team. They’re so friendly, dedicated and hard-working. This is an awesome option for some passive income. Thank you Graphiczoo!</p>
						</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="talk-item">
					<div class="talker-header">				
						<div class="talker-image"><img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/affiliated/talker-a.png" class=""></div>
						<div class="talker-info">
							<h4>Patrick Mills</h4>
							<span>New York, USA</span>
						</div>
						
					</div>
					<div class="talker-say">
							<p>Responsive staff, cool designs, simple payout options, and great other services. Graphiczoo has it all! My references have been extremely satisfied and hence I would definitely urge all of you to join the impeccable affiliate program of the Graphiczoo!</p>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="publection-sec get-touch-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
         <h2>Join more than 500+ customers</h2>
         <p>Try Graphics Zoo Risk-Free For 14 Days</p>
        <div class="get-tuch-btn">
        	<a class="red-theme-btn" href="#">Get in Touch Today
        		<img alt="" src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/red-long-arrow.png" class="">
        	</a>
        </div>
    </div>
</div>
</div>
</section>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/bootstrap.min.js"></script>
<script src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/js/jquery.fancybox.js"></script>
<script>
//$("#affiliate_signup").submit(function( e ) {
//        var aff_val =  $('#affiliated_key').val();
//        if(/^[a-zA-Z0-9_]*$/.test(aff_val)){
//            return true;
//        }else{
//            $('.affiliated_key .aff_key').html("<span class='validation' style=color:red;font-size:13px;>please enter valid key without any symbol.</span>");
//            return false;
//        }
//    });
    
//$('.refferal_val').on('click',function(){
// $('#affiliated_key').focus();
//});
$(document).ready(function(){
    jQuery("#cstm-review-slide").owlCarousel({
    autoplay:true,
    loop: true,
    mouseDrag:true,
    nav:true,
    dots:true,
    responsive:{
        1000:{
            items:1
        }
        }
    });
    });
</script>
</body>
</html>
