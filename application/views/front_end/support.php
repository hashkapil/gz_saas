<style>
    section.publection-sec{display: none}
</style>
<script src='https://www.google.com/recaptcha/api.js'></script>
<section class="page-heading-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>How can we help?</h1>
                <p>Need to get in touch? We’re pretty easy to reach. Feel  free to email us, reach out on 
                    social media, or fill out the contact form below and let’s connect.</p>
            </div>
        </div>
    </div>
</section>
<!--==========================
    Contact Section
    ============================-->

<section id="contact">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-lg-4 col-md-4 text-center">
                <div class=" chat-box box-wrap mb-3">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/mail-us.png" alt="mail-us">
                    <p>Email with Us</p>
                    <a href="mailto:<?php echo SUPPORT_EMAIL; ?>" class="btn-plan_pricing">SEND EMAIL</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 text-center">
                <div class="chat-box box-wrap mb-3">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/chat-us.png" alt="chat-us">
                    <p>Chat with our Experts</p>
                    <span>Available Mon to Fri 9 AM - 6 PM CST</span>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 text-center">
                <div class="social-box box-wrap mb-3">
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/share-us.png" alt="share-us">
                    <p>Follow Us</p>
                    <div class="social-links">
                        <!-- <a href="#" class="twiter"><i class="fab fa-twitter"></i></a> -->
                        <a target="_blank" href="https://www.facebook.com/graphicszoo1/" class="facebook" rel="nofollow"><i class="fab fa-facebook"></i></a>
                        <a href="https://www.instagram.com/graphics_zoo/" target="_blank" class="dribble" rel="nofollow"><i class="fab fa-instagram"></i></a>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="form">
                    <?php if ($this->session->flashdata('message_error') != '') { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                            <strong><?php echo $this->session->flashdata('message_error'); ?></strong>
                        </div>
                    <?php }if ($this->session->flashdata('message_success') != '') { ?>
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                            <strong><?php echo $this->session->flashdata('message_success'); ?></strong>
                        </div>
                    <?php } ?>
                    <div id="errormessage"></div>
                    <h2>GET IN TOUCH</h2>
                    <p>Send us a message and we will be happy to assist you!</p>
                    <form action="<?php echo base_url(); ?>welcome/send_email_support" method="post" role="form" class="contactForm">
                        <div class="row">
                            <div class="form-group col-md-6 inner-addon left-addon">
                                <input type="text" name="name" class="form-control" id="name" <?php echo $this->input->post('name'); ?> placeholder="Enter First Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" novalidate/>
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-md-6 inner-addon right-addon">
                                <input type="text" name="last-name" class="form-control" id="name" <?php echo $this->input->post('name'); ?> placeholder="Enter Last Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" novalidate/>
                                <div class="validation"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 inner-addon left-addon">
                                <input type="email" class="form-control" name="email" id="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>" placeholder="Enter Email Address *" data-rule="email" data-msg="Please enter a valid email" novalidate/>
                                <div class="validation"></div>
                            </div>
                            <div class="form-group col-md-6 inner-addon left-addon">
                                <input type="text" class="form-control" name="subject" id="subject" value="<?php echo isset($_POST['subject']) ? $_POST['subject'] : ''; ?>" placeholder="Enter Subject *" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" novalidate/>
                                <div class="validation"></div>
                            </div>
                        </div>
                        <div class="form-group inner-addon left-addon">
                            <textarea class="form-control" name="message"  value="<?php echo isset($_POST['message']) ? $_POST['message'] : ''; ?>" rows="11" data-rule="required" data-msg="Please write something for us" placeholder="Enter Message *"></textarea>
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6LdJZ4sUAAAAAKS_wnAoZq12UIdxA2OASm46FpGw"></div>
                        </div>
                        <div class="text-left"><button class="red-theme-btn" type="submit">Submit Your Message</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- #contact -->
<!--==========================
    Problems we solve
    ============================-->
<section id="problems-solve" class="padd-section text-center">
    <div class="container">
        <div class="section-title text-center">
            <h2>Problems We Solve</h2>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="shadow-wrapper">
                    <p><strong>We need a fresh brand identity</strong> to help us stand out from the noise.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="shadow-wrapper">
                    <p><strong>Can you help us build marketing </strong>materials for this upcoming event?</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="shadow-wrapper">
                    <p><strong>We want ad designs to launch a digital marketing</strong>  campaign for our business.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="shadow-wrapper">
                    <p><strong>I run a print shop and need your team to </strong> create designs files for my customer’s orders</p>
                </div>
            </div>
        </div>
        <div class="row mt-1 mb-1">
            <div class="col-md-6 col-6" style="text-align: right; padding: 0 5px 0 0;">
                <a href="<?php echo base_url(); ?>portfolio" class="blue-theme-btn" rel="nofollow">OUR WORK  <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-responsive" alt="Long Arrow"></a>
            </div>
            <div class="col-md-6 col-6" style="text-align: left;padding: 20px 0px 0px 5px;">

                <a href="<?php echo base_url(); ?>support" class="red-theme-btn" rel="nofollow">LET'S TALK ? <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/long-arrow.png" class="img-responsive" alt="Long Arrow">
                </a>
            </div>
        </div>
    </div>
</section>  
<section class="riskFree-sec pricing-risk">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Join more than 500+ customers</h2>
                <h3>Try Graphics Zoo Risk-Free For 14 Days</h3>
                <a href="<?php echo base_url(); ?>pricing" class="red-theme-btn" rel="nofollow">Get Started Now
                    <img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/red-long-arrow.png" alt="red-long-arrow" class="img-fluid"></a>

            </div>
        </div>
    </div>
</section>