<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('Myfunctions');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Welcome_model');
        $this->load->model('Request_model');
        $this->load->model('Stripe');
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }

    public function checkloginuser() {
        if (!$this->session->userdata('user_id')) {
            redirect(base_url());
        }
        if ($this->session->userdata('role') != "admin") {
            redirect(base_url());
        }
    }

    public function index() {
        $this->checkloginuser();
        $count_active_project = $this->Request_model->admin_load_more(array('active', 'disapprove'), true);
        $active_project = $this->Request_model->admin_load_more(array('active', 'disapprove'));
        
       // echo "<pre>";print_r($active_project);
      // $count_active_project = $this->Admin_model->count_active_project(array("active", "disapprove"), "", "", "", "", "status_admin");
        //$active_project = $this->Admin_model->get_all_requested_designs(array("active", "disapprove"), "", "", "", "", "status_admin");
        //echo "<pre>";print_r($active_project);
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($active_project[$i]['id'], $active_project[$i]['designer_id']);
            $active_project[$i]['designer_project_count'] = $this->Request_model->get_designer_active_request($active_project[$i]['designer_id']);
            $getfileid = $this->Request_model->get_attachment_files($active_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {
                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $active_project[$i]['comment_count'] = $commentcount;
            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "admin");
            $active_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($active_project[$i]['id'], $_SESSION['user_id'], "admin");
            $active_project[$i]['expected'] = $this->myfunctions->check_timezone($active_project[$i]['latest_update']);
            $active_project[$i]['total_file'] = $this->Request_model->get_files_count($active_project[$i]['id'], $active_project[$i]['designer_id'], "admin");
            $active_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($active_project[$i]['id']);
        }
        $data['count_project_a'] = $count_active_project;
        $data['active_project'] = $active_project;
        
        $count_inprogress_project = $this->Request_model->admin_load_more(array('assign', 'pending'), true);
        $inprogressrequest = $this->Request_model->admin_load_more(array('assign', 'pending'));
//        $count_inprogress_project = $this->Admin_model->count_active_project(array("assign", "pending"), "", "", "", "", "status_admin", 'priority');
//        $inprogressrequest = $this->Admin_model->get_all_requested_designs(array("assign", "pending"), "", "", "", "", "status_admin", 'priority');
        //echo "<pre>"; print_r($data);
        for ($i = 0; $i < sizeof($inprogressrequest); $i++) {
            $getfileid = $this->Request_model->get_attachment_files($inprogressrequest[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {
                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            // echo "<pre>"; print_r($inprogressrequest[$i]);
            $inprogressrequest[$i]['comment_count'] = $commentcount;
            $inprogressrequest[$i]['priority'] = $inprogressrequest[$i]['priority'];
            $inprogressrequest[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($inprogressrequest[$i]['id'], $inprogressrequest[$i]['designer_id']);
            $inprogressrequest[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($inprogressrequest[$i]['id'], $_SESSION['user_id'], "admin");
            $inprogressrequest[$i]['total_chat'] = $this->Request_model->get_chat_number($inprogressrequest[$i]['id'], $inprogressrequest[$i]['customer_id'], $inprogressrequest[$i]['designer_id'], "admin");
            $inprogressrequest[$i]['total_file'] = $this->Request_model->get_files_count($inprogressrequest[$i]['id'], $inprogressrequest[$i]['designer_id'], "admin");
            $inprogressrequest[$i]['total_files_count'] = $this->Request_model->get_files_count_all($inprogressrequest[$i]['id']);
        }
        $data['count_project_i'] = $count_inprogress_project;
        $data['inprogressrequest'] = $inprogressrequest;
        
        $count_pendingreview_project = $this->Request_model->admin_load_more(array('pendingrevision'), true);
        $pendingreview = $this->Request_model->admin_load_more(array('pendingrevision'));
//        $count_pendingreview_project = $this->Admin_model->count_active_project(array("pendingrevision"), "", "", "", "", "status_admin");
//        $pendingreview = $this->Admin_model->get_all_requested_designs(array("pendingrevision"), "", "", "", "", "status_admin");
        for ($i = 0; $i < sizeof($pendingreview); $i++) {
            $getfileid = $this->Request_model->get_attachment_files($pendingreview[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $pendingreview[$i]['designer_project_count'] = $this->Request_model->get_designer_active_request($pendingreview[$i]['designer'][0]['id']);
            $pendingreview[$i]['comment_count'] = $commentcount;
            $pendingreview[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($pendingreview[$i]['id'], $pendingreview[$i]['designer_id']);
            $pendingreview[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($pendingreview[$i]['id'], $_SESSION['user_id'], "admin");
            $pendingreview[$i]['total_chat'] = $this->Request_model->get_chat_number($pendingreview[$i]['id'], $pendingreview[$i]['customer_id'], $pendingreview[$i]['designer_id'], "admin");
            $pendingreview[$i]['revisiondate'] = $this->onlytimezone($pendingreview[$i]['modified']);
            $pendingreview[$i]['total_file'] = $this->Request_model->get_files_count($pendingreview[$i]['id'], $pendingreview[$i]['designer_id'], "admin");
            $pendingreview[$i]['total_files_count'] = $this->Request_model->get_files_count_all($pendingreview[$i]['id']);
        }
        $data['count_project_p'] = $count_pendingreview_project;
        $data['pendingreview'] = $pendingreview;
        
        $count_pendin_project = $this->Request_model->admin_load_more(array("pendingforapprove", "checkforapprove"), true);
        $pending = $this->Request_model->admin_load_more(array("pendingforapprove", "checkforapprove"));
//        $count_pendin_project = $this->Admin_model->count_active_project(array("pendingforapprove", "checkforapprove"), "", "", "", "", "status_admin");
//        $pending = $this->Admin_model->get_all_requested_designs(array("checkforapprove"), "", "", "", "", "status_admin");
        //echo "<pre>";print_r($pending);
        for ($i = 0; $i < sizeof($pending); $i++) {
            $getfileid = $this->Request_model->get_attachment_files($pending[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $pending[$i]['designer_project_count'] = $this->Request_model->get_designer_active_request($pending[$i]['designer'][0]['id']);
            $pending[$i]['comment_count'] = $commentcount;
            $pending[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($pending[$i]['id'], $pending[$i]['designer_id']);
            $pending[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($pending[$i]['id'], $_SESSION['user_id'], "admin");
            $pending[$i]['reviewdate'] = $this->onlytimezone($pending[$i]['modified']);
            $pending[$i]['total_chat'] = $this->Request_model->get_chat_number($pending[$i]['id'], $pending[$i]['customer_id'], $pending[$i]['designer_id'], "admin");
            $pending[$i]['total_file'] = $this->Request_model->get_files_count($pending[$i]['id'], $pending[$i]['designer_id'], "admin");
            $pending[$i]['total_files_count'] = $this->Request_model->get_files_count_all($pending[$i]['id']);
        }
        $data['count_project_pe'] = $count_pendin_project;
        $data['pending'] = $pending;

        $count_complete_project = $this->Request_model->admin_load_more(array("approved"), true);
        $completed = $this->Request_model->admin_load_more(array("approved"));
//        $count_complete_project = $this->Admin_model->count_active_project(array("approved"), "", "", "", "", "status_admin");
//        $completed = $this->Admin_model->get_all_requested_designs(array("approved"), "", "", "", "", "status_admin");
        for ($i = 0; $i < sizeof($completed); $i++) {
            $getfileid = $this->Request_model->get_attachment_files($completed[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $completed[$i]['designer_project_count'] = $this->Request_model->get_designer_active_request($completed[$i]['designer'][0]['id']);
            $completed[$i]['comment_count'] = $commentcount;
            $completed[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($completed[$i]['id'], $completed[$i]['designer_id']);
            $completed[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($completed[$i]['id'], $_SESSION['user_id'], "admin");
            $completed[$i]['total_chat'] = $this->Request_model->get_chat_number($completed[$i]['id'], $completed[$i]['customer_id'], $completed[$i]['designer_id'], "admin");
            $completed[$i]['approvedate'] = $this->onlytimezone($completed[$i]['approvaldate']);
            $completed[$i]['total_file'] = $this->Request_model->get_files_count($completed[$i]['id'], $completed[$i]['designer_id'], "admin");
            $completed[$i]['total_files_count'] = $this->Request_model->get_files_count_all($completed[$i]['id']);
        }
        $data['count_project_c'] = $count_complete_project;
        $data['completed'] = $completed;
        $data['designer_list'] = $this->Admin_model->get_total_customer("designer");
        for ($i = 0; $i < sizeof($data['designer_list']); $i++) {

            $user = $this->Account_model->get_all_active_request_by_designer($data['designer_list'][$i]['id']);
            $data['designer_list'][$i]['active_request'] = sizeof($user);
        }
       // echo "<pre>"; print_r($data);
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        // $this->load->view('admin/index',array('today_customer'=>$today_customer,'all_customer'=>$all_customer,'today_requests'=>$today_requests,'total_requests'=>$total_requests));
        $this->load->view('admin/index', $data);
        $this->load->view('admin/admin_footer');
    }
    
    public function delete_file_req() {
        $reqid = isset($_POST['request_id']) ? $_POST['request_id'] : '';
        $filename = isset($_POST['filename']) ? $_POST['filename'] : '';
            $success = $this->Request_model->delete_request_file('request_files', $reqid, $filename);
        if ($success) {
            $dir = FCPATH . '/public/uploads/requests/' . $reqid;
            unlink($dir . "/" . $filename); //gi
        }
        $this->session->set_flashdata('message_success', "File is Deleted Successfully.!", 5);
    }

    public function adminprofile() {
        $this->checkloginuser();
        if (($_POST)) {
            $sucess = $this->Account_model->addSubscriptionPlan('subscription_plan', $_POST);
            if ($sucess) {
                $this->session->set_flashdata('success', 'Successfully add subscription plan', 5);
                redirect(base_url() . "admin/dashboard/adminprofile?status=2");
            } else {
                $this->session->set_flashdata('error', 'Subscription plan not added', 5);
                redirect(base_url() . "admin/dashboard/adminprofile?status=2");
            }
        }
        $user_id = $_SESSION['user_id'];
        $data = $this->Admin_model->getuser_data($user_id);
        $subscriptionplan = $this->Request_model->getAllSubscriptionPlan();
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        $this->load->view('admin/admin_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "edit_profile" => $profile_data));

        $this->load->view('admin/adminprofile', array("data" => $data, 'subscriptionplan' => $subscriptionplan));
        $this->load->view('admin/admin_footer');
    }

    public function edit_profile_image_form() {
        $user_id = $_SESSION['user_id'];
        $fname = explode(' ', $_POST['first_name']);
        if ($_FILES['profile']['name'] != "") {
            $file = pathinfo($_FILES['profile']['name']);
            $s3_file = $file['filename'].'-'.rand(1000,1).'.'.$file['extension'];
            $config = array(
                'upload_path' => FS_PATH_PUBLIC_UPLOADS_PROFILE,
                'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                'file_name' => $s3_file
            );
            $check = $this->myfunctions->CustomFileUpload($config,'profile');
            if($check['status'] == 1){
                    $profile_pic = array(
                        'profile_picture' => $s3_file,
                    );
                    $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
                    if ($update_data_result) {
                        $this->session->set_flashdata('message_success', 'User Profile Picture Update Successfully.!');
                    } else {
                        $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!');
                    }
                } else {
                    $this->session->set_flashdata('message_error', $check['msg'], 5);
                    //$this->session->set_flashdata('message_error', $data, 5);
                }
        }if(!empty($this->input->post())){
            $profile_pic = array(
                'first_name' => $fname[0],
                'last_name' => $fname[1],
                'notification_email' => $_POST['notification_email'],
            );
            $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
            if ($update_data_result) {
                $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
            } else {
                $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
            }
        }
    }
    

    public function customer_edit_profile() {
        $user_id = $_SESSION['user_id'];
        if ($_POST['company_name'] != "") {
            $profileUpdate = array(
                'company_name' => $_POST['company_name'],
                'phone' => $_POST['phone'],
                'address_line_1' => $_POST['address_line_1'],
                'title' => $_POST['title'],
                'city' => $_POST['city'],
                'state' => $_POST['state'],
                'zip' => $_POST['zip'],
                'timezone' => $_POST['timezone'],
            );
            print_r($profileUpdate);
            $update_data_result = $this->Admin_model->designer_update_profile($profileUpdate, $user_id);
            if ($update_data_result) {
                $this->session->set_userdata('timezone', $profileUpdate['timezone']);
                //echo "hello";
                $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
            } else {
                //echo "hiii";
                $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
            }
        }
    }

    public function change_password_front() {
        $this->checkloginuser();
        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

            if ($user_data[0]['new_password'] != $_POST['old_password']) {
                $this->session->set_flashdata("message_error", "Old Password is not correct..!", 5);
                redirect(base_url() . "admin/dashboard/adminprofile");
            }
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "admin/dashboard/adminprofile");
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "admin/dashboard/adminprofile");
        }
    }

    public function all_requests() {
        $this->checkloginuser();
        $requested_designs = $this->Admin_model->get_all_requested_designs(array("active", "disapprove"), "", "", "", "", "status_admin");

        $inprogressrequest = $this->Admin_model->get_all_requested_designs(array("assign", "pending"), "", "", "", "", "status_admin");
        $checkforapproverequests = $this->Admin_model->get_all_requested_designs(array("pendingforapprove", "checkforapprove"), "", "", "", "", "status_admin");
        $approved_designs = $this->Admin_model->get_all_requested_designs(array("approved"), "", "", "", "", "status_admin");

        //$get_message = $this->Admin_model->get_unread_message();

        $admin_unread_message_count_array = array();
        $total_unread_message = 0;
        $total_assign_message = 0;
        $total_checkforapprove_message = 0;
        $total_approved_message = 0;

        for ($i = 0; $i < sizeof($requested_designs); $i++) {
            //echo $requested_designs1[$i]->id;
            $admin_unread_msg_count = $this->Admin_model->get_RequestDiscussions_admin($requested_designs[$i]['id'], "admin_seen", 0);
            $admin_unread_msg_count = sizeof($admin_unread_msg_count);
            $admin_unread_message_count_array[$requested_designs[$i]['id']] = $admin_unread_msg_count;
            if ($requested_designs[$i]['status_admin'] == "active" || $requested_designs[$i]['status_admin'] == "disapprove") {
                $total_unread_message += $admin_unread_msg_count;
            } elseif ($requested_designs[$i]['status_admin'] == "assign") {
                $total_assign_message += $admin_unread_msg_count;
            } elseif ($requested_designs[$i]['status_admin'] == "checkforapprove" || $requested_designs[$i]['status_admin'] == "pendingforapprove") {
                $total_checkforapprove_message += $admin_unread_msg_count;
            } elseif ($requested_designs[$i]['status_admin'] == "approved") {
                $total_approved_message += $admin_unread_msg_count;
            }
        }

        $admin_unread_message_count_array2 = array();
        $total_unread_message2 = 0;
        for ($i = 0; $i < sizeof($approved_designs); $i++) {
            //echo $requested_designs1[$i]->id;
            $admin_unread_msg_count = $this->Admin_model->get_RequestDiscussions_admin($approved_designs[$i]['id'], "admin_seen", 0);
            $admin_unread_msg_count = sizeof($admin_unread_msg_count);
            $admin_unread_message_count_array2[$approved_designs[$i]['id']] = $admin_unread_msg_count;
            $total_unread_message2 += $admin_unread_msg_count;
        }
        for ($i = 0; $i < sizeof($inprogressrequest); $i++) {
            //echo $requested_designs1[$i]->id;
            $admin_unread_msg_count = $this->Admin_model->get_RequestDiscussions_admin($inprogressrequest[$i]['id'], "admin_seen", 0);
            $admin_unread_msg_count = sizeof($admin_unread_msg_count);
            $admin_unread_message_count_array2[$inprogressrequest[$i]['id']] = $admin_unread_msg_count;
            $total_unread_message2 += $admin_unread_msg_count;
        }
        $customers = $this->Admin_model->get_total_customer();

        $customerarray = array();

        for ($i = 0; $i < sizeof($customers); $i++) {
            if ($customers[$i]['current_plan'] != "") {

                if ($customers[$i]['plan_turn_around_days'] == 1) {
                    $customerarray[$customers[$i]['id']]['current_plan_color'] = "red";
                } elseif ($customers[$i]['plan_turn_around_days'] !== 3) {
                    $customerarray[$customers[$i]['id']]['current_plan_color'] = "blue";
                } else {
                    $customerarray[$i]['current_plan_name'] = "";
                    $customerarray[$customers[$i]['id']]['current_plan_color'] = "";
                }
            } else {
                $customerarray[$i]['current_plan_name'] = "";
                $customerarray[$customers[$i]['id']]['current_plan_color'] = "";
            }
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/all_requests', array("customerarray" => $customerarray, "customers" => $customers, "total_unread_message2" => $total_unread_message2, "admin_unread_message_count_array2" => $admin_unread_message_count_array2, "total_approved_message" => $total_approved_message, "total_checkforapprove_message" => $total_checkforapprove_message, "total_assign_message" => $total_assign_message, "total_unread_message" => $total_unread_message, "admin_unread_message_count_array" => $admin_unread_message_count_array, "requested_designs" => $requested_designs, "inprogressrequest" => $inprogressrequest, "checkforapproverequests" => $checkforapproverequests, "approved_designs" => $approved_designs));
        $this->load->view('admin/admin_footer');
    }

    public function all_customer() {
        $this->checkloginuser();
        $all_customer = $this->Admin_model->get_total_customer("customer");
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/all_customer', array("all_customer" => $all_customer));
        $this->load->view('admin/admin_footer');
    }

    public function edit_customer($customer_id = null) {
        $this->checkloginuser();
        if ($customer_id == "") {
            redirect(base_url() . "admin/all_customer");
        }
        $customer = $this->Admin_model->getuser_data($customer_id);
        if (empty($customer)) {
            $this->session->set_flashdata('message_error', 'No Customer Found.!', 5);
            redirect(base_url() . "admin/all_customer");
        }
        $subscription_plan_list = array();
        $subscription = array();
        $planid = "";
        if ($customer[0]['current_plan'] != "") {
            $getcurrentplan = $this->Stripe->getcurrentplan($customer[0]['current_plan']);
            print_r($getcurrentplan);
            if (!empty($getcurrentplan)) {
                $plandetails = $getcurrentplan->items->data[0]->plan;
                $planid = $plandetails->id;
            } else {
                $plandetails = "";
                $planid = "";
            }
        }
        $allplan = $this->Stripe->getallsubscriptionlist();
        $subscription[] = "Select Plan";
        for ($i = 0; $i < sizeof($allplan); $i++) {
            $subscription[$allplan[$i]['id']] = $allplan[$i]['name'] . " - $" . $allplan[$i]['amount'] / 100 . "/" . $allplan[$i]['interval'];
        }
        $carddetails = array();
        $carddetails['last4'] = "";
        $carddetails['exp_month'] = "";
        $carddetails['exp_year'] = "";
        $carddetails['brand'] = "";
        $plan = array();
        if ($customer[0]['current_plan'] != "" && $customer[0]['current_plan'] != "0") {
            $getcurrentplan = $this->Stripe->getcurrentplan($customer[0]['current_plan']);
            if (!empty($getcurrentplan)) {
                $plan['userid'] = $getcurrentplan->id;
                $plan['current_period_end'] = $getcurrentplan->current_period_end;
                $plan['current_period_start'] = $getcurrentplan->current_period_start;
                $plan['billing'] = $getcurrentplan->billing;
                $plan['trial_end'] = $getcurrentplan->trial_end;
                $plan['trial_start'] = $getcurrentplan->trial_start;


                $plandetails = $getcurrentplan->items->data[0]->plan;
                $plan['planname'] = $plandetails->name;
                $plan['planid'] = $plandetails->id;
                $plan['interval'] = $plandetails->interval;
                $plan['amount'] = $plandetails->amount / 100;
                $plan['created'] = $plandetails->created;

                $customerdetails = $this->Stripe->getcustomerdetail($customer[0]['current_plan']);
                if (!empty($customerdetails)) {
                    $carddetails['last4'] = $customerdetails->sources->data[0]->last4;
                    $carddetails['exp_month'] = $customerdetails->sources->data[0]->exp_month;
                    $carddetails['exp_year'] = $customerdetails->sources->data[0]->exp_year;
                    $carddetails['brand'] = $customerdetails->sources->data[0]->brand;
                }
            }
        } else {
            $plan['planid'] = "";
        }
        $designer_list = $this->Admin_model->get_total_customer("designer");
        $designs_requested = $this->Admin_model->get_all_requested_designs(array(), $customer_id, "no");
        $designs_approved = $this->Admin_model->get_all_requested_designs(array("approved"), $customer_id, "", "", "", "status_admin");
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/edit_customer', array("customer" => $customer, "subscription" => $subscription, "planid" => $planid, "carddetails" => $carddetails, "designer_list" => $designer_list, "designs_requested" => $designs_requested, "designs_approved" => $designs_approved));
        $this->load->view('admin/admin_footer');
    }

    public function phpinfo() {
        phpinfo();
    }
    
    public function attach_file_process() {
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output['error'] = 'Error while uploading the files. Please try again.';
        //echo "<pre>";print_R($_FILES);
        $dateFolder = strtotime(date('Y-m-d H:i:s'));
        if (!(isset($_SESSION['temp_attachfolder_name']) && $_SESSION['temp_attachfolder_name'] != '')) {
            if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                $_SESSION['temp_attachfolder_name'] = './public/uploads/temp/' . $dateFolder;
            } else {
                $_SESSION['temp_attachfolder_name'] = '';
            }
        } else {
            if (!is_dir($_SESSION['temp_attachfolder_name'])) {
                if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                    $_SESSION['temp_attachfolder_name'] = './public/uploads/temp/' . $dateFolder;
                } else {
                    $_SESSION['temp_attachfolder_name'] = '';
                }
            }
        }
        if ($_SESSION['temp_attachfolder_name'] != '') {
            $files = $_FILES;
            $cpt = count($_FILES['file_upload']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['file_upload']['name'] = $files['file_upload']['name'][$i];
                $_FILES['file_upload']['type'] = $files['file_upload']['type'][$i];
                $_FILES['file_upload']['tmp_name'] = $files['file_upload']['tmp_name'][$i];
                $_FILES['file_upload']['error'] = $files['file_upload']['error'][$i];
                $_FILES['file_upload']['size'] = $files['file_upload']['size'][$i];
                $config = array(
                    'upload_path' => $_SESSION['temp_attachfolder_name'],
                    'allowed_types' => "doc|docx|psd|odt|ods|ai|zip|mp4|mov|pdf|jpg|png|jpeg",
                    'max_size' => '0',
                    'overwrite' => FALSE
                );
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload("file_upload")) {
                    $data = array($this->upload->data());
                    $data[0]['error'] = false;
                    $output['files'][] = $data;
                    $output['status'] = true;
                    $output['error'] = '';
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $error_data = array();
                    $error_data[0] = array();
                    $error_data[0]['file_name'] = $files['file_upload']['name'][$i];
                    $error_data[0]['error'] = true;
                    $error_data[0]['error_msg'] = strip_tags($data['error']);
                    $output['files'][] = $error_data;
                    $output['status'] = true;
                    $output['error'] = '';
                }
            }
        } else {
            $output['error'] = 'Directory not writable. Please try again.';
        }
        echo json_encode($output);
        exit;
    }
    public function delete_attachfile_from_folder() {
        $folderPath = $_SESSION['temp_attachfolder_name'];
        $folderPaths = $_SESSION['temp_attachfolder_name'] . '/*';
        if (!empty($_POST)) {
            $data = $_POST['file_name'];
            $files = glob($folderPaths); // get all file names
            $dir = FCPATH . $folderPath;
            $dirHandle = opendir($dir);
            while ($file = readdir($dirHandle)) {
                if ($file == $data) {
                    unlink($dir . "/" . $file); //give correct path,
                } else {
                    
                }
            }
        }
    }

    public function view_request($id) {
       // die('fvgbhj');
        $this->checkloginuser();
        if ($id == "") {
            redirect(base_url() . "admin/all_requests");
        }
        $url = "admin/dashboard/view_request/" . $id;
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url));
        $this->Welcome_model->update_data("message_notification", array("shown" => 1), array("user_id" => $_SESSION['user_id'], "url" => $url));
        $this->Admin_model->update_data("request_discussions", array("admin_seen" => 1), array("request_id" => $id));
        
        if ($_POST['attach_file']) {
            if ($_FILES['file_upload']['name'] != "") {
                if (!is_dir('public/uploads/requests/' . $id)) {
                    $data = mkdir('./public/uploads/requests/' . $id, 0777, TRUE);
                }
                //Move files from temp folder
                // Get array of all source files
                $uploadPATH = $_SESSION['temp_attachfolder_name'];
                $uploadPATHs = str_replace("./","",$uploadPATH);
                //echo $uploadPATHs;exit; 
                $files = scandir($uploadPATH);
                // Identify directories
                $source = $uploadPATH.'/';
               //echo "<pre>"; print_r($destination);exit;
                $destination = './public/uploads/requests/' . $id.'/';
              // echo "<pre>"; print_r($uploadPATH);exit;
                if(UPLOAD_FILE_SERVER == 'bucket'){
                foreach ($files as $file) {

                if (in_array($file, array(".",".."))) continue;

                $staticName = base_url().$uploadPATHs.'/'.$file;
               //echo $staticName;exit;
                $config = array(
                    'upload_path' => 'public/uploads/requests/'. $id.'/',
                    'file_name' => $file
                );
                $this->load->library('s3_upload');
                $this->s3_upload->initialize($config);
                $this->s3_upload->upload_multiple_file($staticName);
                $delete[] = $source.$file;
                }
                }else{
                    foreach ($files as $file) {

                  if (in_array($file, array(".",".."))) continue;
                  // If we copied this successfully, mark it for deletion
                  //echo APPPATH.$source.$file;
                  $filepath = APPPATH.$source.$file;

                  if (copy($source.$file, $destination.$file)) {
                    $delete[] = $source.$file;
                  }
                }
                }
                 // exit;
                // Cycle through all source files

               //echo "kfdljgi";

                // Delete all successfully-copied files
                foreach ($delete as $file) {
                  unlink($file);
                }
                rmdir($source);
                
                unset($_SESSION['temp_attachfolder_name']);
                 if (isset($_POST['delete_FL']) && $_POST['delete_FL'] != '') {
                   // echo "<pre>"; print_R($_POST['delete_FL']);exit;
                    foreach ($_POST['delete_FL'] as $SubFIlekey => $GetFIleName) {
                        // $GetFIleName = substr($SubFIleName, strrpos($SubFIleName, '/') + 1);
                        $request_files_data = array("request_id" => $id,
                            "user_id" => $_SESSION['user_id'],
                            "user_type" => "customer",
                            "file_name" => $GetFIleName,
                            "created" => date("Y-m-d H:i:s"));
                        //print_r($request_files_data);exit;
                        $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                    }
                }
                redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);

            } else {
                $this->session->set_flashdata('message_error', "Please Upload Preview  Files.!", 5);
                redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
            }
        }

        $request = $this->Request_model->get_request_by_id_admin($id);
        // echo "<pre>";print_r($request);
        $request_id = $request[0]['id'];
        $files = $this->Request_model->get_attachment_files($id, "designer");
        $customer = $this->Account_model->getuserbyid($request[0]['customer_id']);
        //echo "<pre>";print_r($customer);
        $file_chat_array = array();
        for ($i = 0; $i < sizeof($files); $i++) {
            $chat_of_file = $this->Request_model->get_file_chat($files[$i]['id'], "admin");

            if ($chat_of_file) {
                $file_chat_array[$i]['file_name'] = $files[$i]['file_name'];
                $file_chat_array[$i]['count'] = $chat_of_file;
                $file_chat_array[$i]['id'] = $files[$i]['id'];
                $files[$i]['chat_count'] = $chat_of_file;
            } else {
                $files[$i]['chat_count'] = "";
                $file_chat_array[$i]['id'] = "";
            }
            $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
            $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        }

        if (!empty($_FILES)) {
            if ($_FILES['src_file']['name'] != "" && $_FILES['preview_file']['name'] != "") {
                $config['image_library'] = 'gd2';
                if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id)) {
                    $data = mkdir('./'.FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id, 0777, TRUE);
                }
                if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb')) {
                    $data = mkdir('./'.FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb', 0777, TRUE);
                }
                $this->load->library('image_lib', $config);
                $requestforfile = $this->Request_model->get_request_by_id($id);
                $file_name_title = substr($requestforfile[0]['title'], 0, 15);
                $file_name_title = str_replace(" ", "_", $file_name_title);
                $file_name_title = preg_replace('/[^A-Za-z0-9\_]/', '', $file_name_title);
                $six_digit_random_number = mt_rand(100, 999);
                $ext = strtolower(pathinfo($_FILES['src_file']['name'], PATHINFO_EXTENSION));
                $ext1 = strtolower(pathinfo($_FILES['preview_file']['name'], PATHINFO_EXTENSION));
                $sourcename = $file_name_title . '_' . $six_digit_random_number . '.' . $ext;
                $previewname = $file_name_title . '_' . $six_digit_random_number . '1.' . $ext1;
                if (!empty($_FILES)) {
                    $config = array(
                        'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id.'/',
                        'allowed_types' => array('jpg', 'jpeg', 'gif', 'png', 'zip', 'xlsx', 'cad', 'pdf', 'doc', 'docx', 'ppt', 'pptx', 'pps', 'ppsx', 'odt', 'ods', 'txt', 'text', 'xls', 'xlsx', '.mp3', 'm4a', 'ogg', 'wav', 'mp4', 'm4v', 'mov', 'wmv', 'ai', 'psd', 'eps'),
                        'file_name' => $sourcename,
                    );
                    $config2 = array(
                        'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id.'/',
                        'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                        'max_size' => '5000000',
                        'file_name' => $previewname,
                    );
                    
                    if (($_FILES['preview_file']['size'] >= $config2['max_size']) || ($_FILES['preview_file']['size'] == 0)) {
                        $this->session->set_flashdata('message_error', "File too large. File must be less than 5 MB.", 5);
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
                    }
                    if (in_array($ext, $config['allowed_types']) && in_array($ext1, $config2['allowed_types'])) {
                        
                        $check_src_file = $this->myfunctions->CustomFileUpload($config,'src_file');

                        $src_data = array();
                        $src_data[0]['file_name'] = "";
                       if ($check_src_file['status'] == 1) { 
					  // echo $check_src_file['msg'];
                            
                        } else {
                            //$data = array('error' => $this->upload->display_errors());

                            $this->session->set_flashdata('message_error', $check_src_file['msg'], 5);
                        }


                        //$config2['preview_file'] = $_FILES['preview_file']['name'];

                        $_FILES['src_file']['name'] = $file_name_title . '_' . $six_digit_random_number . '.' . $ext1;
                        $_FILES['src_file']['type'] = $_FILES['preview_file']['type'];
                        $_FILES['src_file']['tmp_name'] = $_FILES['preview_file']['tmp_name'];
                        $_FILES['src_file']['error'] = $_FILES['preview_file']['error'];
                        $_FILES['src_file']['size'] = $_FILES['preview_file']['size'];

                         $check_preview_file = $this->myfunctions->CustomFileUpload($config2,'preview_file');

                        if ($check_preview_file['status'] == 1) {
							//echo $check_src_file['msg'];exit;
                            
                            $thumb_tmp_path = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb/';
                            $tmp = $_FILES['preview_file']['tmp_name'];
                              
                            $thumb = $this->myfunctions->make_thumb($tmp, $thumb_tmp_path . $previewname, 600, $ext1);
                            if(UPLOAD_FILE_SERVER == 'bucket'){
                            $staticName = base_url() . $thumb_tmp_path . '/' . $previewname;
                            //echo $staticName;exit;
                            $config = array(
                                'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb/',
                                'file_name' => $previewname
                            );
                            $this->load->library('s3_upload');
                            $this->s3_upload->initialize($config);
                            $uplod_thumb = $this->s3_upload->upload_multiple_file($staticName);
                            if($uplod_thumb['status'] == 1){
                               $delete = $thumb_tmp_path.$previewname;
                                  unlink($delete);

                               rmdir($thumb_tmp_path);
                               rmdir($thumb_dummy_path);

                                
                            }
                            }

                            $request_files_data = array("request_id" => $id,
                                "user_id" => $_SESSION['user_id'],
                                "user_type" => "designer",
                                //	"file_name" => $file_name_title.'_'.$six_digit_random_number.'.'.$ext1,
                                "file_name" => $previewname,
                                "preview" => 1,
                                "admin_seen" => 1,
                                "status" => "customerreview",
                                "created" => date("Y-m-d H:i:s"),
                                "modified" => date("Y-m-d H:i:s"),
                                "src_file" => $sourcename);
                            $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                           // echo "<pre>";print_r($customer[0]);exit;
                            $this->send_email_after_admin_approval($request[0]['title'], $request[0]['id'], $customer[0]['email']);


                            /* $notification_data = array("url" => "customer/request/project_image_view/" . $data . "?id=" . $id,
                              "title" => "Designer Upload New File. Please Check And Approve It..!",
                              "user_id" => $request[0]['customer_id'],
                              "sender_id" => $_SESSION['user_id'],
                              "created" => date("Y-m-d H:i:s"));
                              $this->Welcome_model->insert_data("notification", $notification_data);
                             */
                            $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status" => "checkforapprove", "status_qa" => "checkforapprove", "status_designer" => "checkforapprove", "status_admin" => "checkforapprove", "modified" => date("Y-m-d H:i:s")), array("id" => $id));
                            //$this->send_email_after_adminapprove($request[0]['title'], $request[0]['id'], $customer[0]['email']);
                            $request2 = $this->Admin_model->get_all_requested_designs(array('active'), $request[0]['customer_id']);
                            $request3 = $this->Admin_model->get_all_requested_designs(array('checkforapprove', 'disapprove'), $request[0]['customer_id']);
                            $updatepriority = $this->Request_model->getall_newrequest($request[0]['customer_id'], array('pending', 'assign'), "", "priority");
                            if ((sizeof($request2) < 1) && sizeof($request3) < 3) {
                                $all_requests = $this->Request_model->getall_newrequest($request[0]['customer_id'], array('pending', 'assign'), "", "priority");
                                if (isset($all_requests[0]['id'])) {
                                    $success = $this->Welcome_model->update_data("requests", array("status" => "active", "status_admin" => "active", "status_designer" => "active", "status_qa" => "active", "dateinprogress" => date("Y-m-d H:i:s"), "priority" => 0, "latest_update" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $all_requests[0]['id']));
                                    $this->Admin_model->update_priority_after_approved('1', $request[0]['customer_id']);
                                    $datamsgauto = array(
                                        "request_id" => $all_requests[0]['id'],
                                        "sender_id" => $customer[0]['qa_id'],
                                        "sender_type" => "qa",
                                        "reciever_id" => $request[0]['customer_id'],
                                        "reciever_type" => "customer",
                                        "message" => "The designer is currently working on your project. Send messages here to share updates about the overall project. If you want to share comments about an individual design you can use the comments section within each design draft.",
                                        "with_or_not_customer" => 1,
                                        "admin_seen" => 1,
                                        "qa_seen" => 1,
                                        "designer_seen" => 1,
                                        "created" => date("Y-m-d H:i:s"),
                                    );
                                    $this->Welcome_model->insert_data("request_discussions", $datamsgauto);
                                    $this->send_email_when_inqueue_to_inprogress($all_requests[0]['title'], $all_requests[0]['id'], $all_requests[0]['customer_email']);
                                }
                            } else {
                                //$this->Admin_model->update_priority_after_approved_qa('1', $updatepriority[0]['priority']);
                            }
                            $this->session->set_flashdata('message_success', "File is Uploaded Successfully.!", 5);
                            redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
                        } else {
                           // $data = array('error' => $this->upload->display_errors());

                           $this->session->set_flashdata('message_error', $check_preview_file['msg'], 5);
                        }
                    } else {
                        $this->session->set_flashdata('message_error', "Please upload valid files", 5);
                        redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
                    }
                }
            } else {
                $this->session->set_flashdata('message_error', "Please Upload Preview or Source Both Files.!", 5);
                redirect(base_url() . "admin/dashboard/view_request/" . $id . '/' . $_POST['tab_status']);
            }
        }

        if (isset($_POST['adddesingerbtn'])) {
            $designer_assigned = $this->Admin_model->assign_designer_by_qa($_POST['assign_designer'], $_POST['request_id']);
            redirect(base_url() . "admin/dashboard/view_request/" . $_POST['request_id'] . '/' . $_POST['tab_status']);
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $request_files = $this->Admin_model->get_requested_files_admin($request_id);

        $admin_request_files = $this->Admin_model->get_requested_files($request_id, "", "designer");

        $customer_additional_files = $this->Admin_model->get_requested_files($request_id, "", "customer");

        $admin_files = $this->Admin_model->get_requested_files($request_id, "", "admin");

        $designer_list = $this->Admin_model->get_total_customer("designer");

        $chat_request = $this->Request_model->get_chat_request_by_id($id);
        $timediff = '';
        for ($j = 0; $j < count($chat_request); $j++) {
            //echo $chat_request[$j]['created']."all"."<br/>";
            $chat_request[$j]['msg_created'] = $this->onlytimezone($chat_request[$j]['created']);
            $chat_request[$j]['chat_created_date'] =   date('M d, Y h:i:s', strtotime($chat_request[$j]['msg_created']));
//            if($timediff == ''){
//                $chat_request[$j]['chat_created_date'] =   date('M d, Y h:i:s', strtotime($chat_request[$j]['created']));
//                $timediff =   date('M d, Y h:i:s', strtotime($chat_request[$j]['created']));
//                
//                // echo $timediff."first"."<br/>";
//            }
//            $sincedate = date('M d, Y h:i:s', strtotime($chat_request[$j]['created']));
//             //echo $sincedate."updated"."<br/>";
//            $datetime1 = new DateTime($timediff);
//            $datetime2 = new DateTime($sincedate);
//            $mindiff = $datetime1->diff($datetime2);
//            if($mindiff->i >= BATCH_MESSAGE_TIMELIMIT ){
//                //echo $sincedate."in";
//               $timediff = $sincedate;
//               $chat_request[$j]['chat_created_date'] = $sincedate;
//            }
             //echo "<pre/>";print_r($chat_request[$j]['chat_created_date']);
        }

        $chat_request_without_customer = $this->Request_model->get_chat_request_by_id_without_customer($id);
         $timediff_nocust = '';
        for ($j = 0; $j < count($chat_request_without_customer); $j++) {
            
            $chat_request_without_customer[$j]['msg_created'] = $this->onlytimezone($chat_request_without_customer[$j]['created']);
            $chat_request_without_customer[$j]['chat_created_date'] =   date('M d, Y h:i:s', strtotime($chat_request_without_customer[$j]['msg_created']));
            //echo $chat_request[$j]['created']."all"."<br/>";
//            if($timediff_nocust == ''){
//                $chat_request_without_customer[$j]['chat_created_date'] =   date('M d, Y h:i:s', strtotime($chat_request_without_customer[$j]['created']));
//
//                $timediff_nocust =   date('M d, Y h:i:s', strtotime($chat_request_without_customer[$j]['created']));
//                // echo $timediff."first"."<br/>";
//            }
//            $sincedate_nocust = date('M d, Y h:i:s', strtotime($chat_request_without_customer[$j]['created']));
//             //echo $sincedate."updated"."<br/>";
//            $datetime1 = new DateTime($timediff_nocust);
//            $datetime2 = new DateTime($sincedate_nocust);
//            $mindiff_nocust = $datetime1->diff($datetime2);
//            if($mindiff_nocust->i >= BATCH_MESSAGE_TIMELIMIT ){
//                //echo $sincedate."in";
//               $timediff_nocust = $sincedate_nocust;
//               $chat_request_without_customer[$j]['chat_created_date'] = $sincedate_nocust;
//            }
             //echo "<pre/>";print_r($chat_request[$j]['chat_created_date']);
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/view_request', array("request" => $request, "request_files" => $request_files, "admin_request_files" => $admin_request_files, "designer_list" => $designer_list, "customer_additional_files" => $customer_additional_files, "admin_files" => $admin_files, "chat_request" => $chat_request, "chat_request_without_customer" => $chat_request_without_customer, "edit_profile" => $profile_data, "file_chat_array" => $file_chat_array));
        $this->load->view('admin/admin_footer');
    }

    public function all_designer() {
        $designers = $this->Admin_model->get_total_customer("designer");

        for ($i = 0; $i < sizeof($designers); $i++) {
            $customer = $this->Admin_model->get_total_customer("customer", $designers[$i]['id']);
            $designer_customer[$designers[$i]['id']] = sizeof($customer);
        }
        //print_r($designers); print_r($designer_customer);
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/all_designer', array("designers" => $designers, "designer_customer" => $designer_customer));
        $this->load->view('admin/admin_footer');
    }

    public function view_designer($designer_id = null) {
       
        if ($designer_id == "") {
            redirect(base_url() . "admin/all_designer");
        }
        if (!empty($_POST)) {
            $checkuser_by_email = $this->Admin_model->check_user_by_email($_POST['email'], $designer_id);
            if (!empty($check_user_by_email)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            }
            $_POST['profile_picture'] = "";
            if (isset($_FILES["profile_picture1"]["name"]) && !empty($_FILES["profile_picture1"]["name"])) {
                $config = array(
                    'upload_path' => "public/profile",
                    'allowed_types' => "jpg|png|jpeg|pdf",
                    'encrypt_name' => TRUE
                );

                $new_name = time() . $_FILES["profile_picture1"]['name'];
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("profile_picture1")) {
                    $data = array($this->upload->data());
                    $_POST['profile_picture'] = $data[0]['file_name'];
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message_error', $data['error'], 5);
                    redirect(base_url() . "admin/view_designer/" . $designer_id);
                }
            }
            $data = array("first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "phone" => $_POST['phone'],
                "email" => $_POST['email'],
                "profile_picture" => $_POST['profile_picture'],
                "modified" => date("Y-m-d H:i:s"));

            $success = $this->Welcome_model->update_data("users", $data, array('id' => $designer_id));
            if ($success) {
                $this->session->set_flashdata('message_success', 'Designer Updated Successfully.!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            } else {
                $this->session->set_flashdata('message_error', 'Designer Updated Not Successfully.!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            }
        }
        $designer = $this->Admin_model->getuser_data($designer_id);

        $designs_queue = $this->Admin_model->get_all_requested_designs("", "", "", "", $designer_id, "status_admin");

        $designs_approved = $this->Admin_model->get_all_requested_designs("approved", "", "", "", $designer_id, "status_admin");

        $customer = $this->Admin_model->get_total_customer("customer", $designer_id);

        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/view_designer', array("designer" => $designer, "designs_queue" => $designs_queue, "customer" => $customer, "designer" => $designer, "designs_approved" => $designs_approved));
        $this->load->view('admin/admin_footer');
    }

    public function add_designer() {
        if (!empty($_POST)) {
            $checkuser_by_email = $this->Admin_model->check_user_by_email($_POST['email']);
            if (!empty($check_user_by_email)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available!', 5);
                redirect(base_url() . "admin/add_designer");
            }
            $_POST['profile_picture'] = "";
            if (isset($_FILES["profile_picture"]["name"]) && !empty($_FILES["profile_picture"]["name"])) {
                $config = array(
                    'upload_path' => "public/profile",
                    'allowed_types' => "jpg|png|jpeg|pdf",
                    'encrypt_name' => TRUE
                );

                $new_name = time() . $_FILES["profile_picture"]['name'];
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("profile_picture")) {
                    $data = array($this->upload->data());
                    $_POST['profile_picture'] = $data[0]['file_name'];
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message_error', $data['error'], 5);
                    redirect(base_url() . "admin/add_designer");
                }
            }
            $data = array("first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "phone" => $_POST['phone'],
                "email" => $_POST['email'],
                "new_password" => md5($_POST['password']),
                "profile_picture" => $_POST['profile_picture'],
                "role" => "designer",
                "created" => date("Y-m-d H:i:s"));

            $success = $this->Welcome_model->insert_data("users", $data);
            if ($success) {
                $this->session->set_flashdata('message_success', 'Designer Added Successfully.!', 5);
                redirect(base_url() . "admin/add_designer");
            } else {
                $this->session->set_flashdata('message_error', 'Designer Added Not Successfully.!', 5);
                redirect(base_url() . "admin/add_designer");
            }
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/add_designer');
        $this->load->view('admin/admin_footer');
    }

    public function edit_designer($customer_id) {
        if (!empty($_POST)) {
            $customer = array();
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['email'] = $_POST['email'];
            $customer['phone'] = $_POST['phone'];
            if ($_POST['password'] != "") {
                if ($_POST['password'] == $_POST['confirm_password']) {
                    $customer['new_password'] = md5($_POST['password']);
                    $success = $this->Admin_model->update_data('users', $customer, array('id' => $customer_id));
                    if ($success) {
                        $this->session->set_flashdata('message_success', 'Designer profile edit successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_designers");
                    } else {
                        $this->session->set_flashdata('message_error', 'Designer profile not edit successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_designers");
                    }
                } else {
                    $this->session->set_flashdata('message_error', 'Password dose not match', 5);
                    redirect(base_url() . "admin/dashboard/view_designers");
                }
            } else {
                $success = $this->Admin_model->update_data('users', $customer, array('id' => $customer_id));
                if ($success) {
                    $this->session->set_flashdata('message_success', 'Designer profile edit successfully.!', 5);
                    redirect(base_url() . "admin/dashboard/view_designers");
                } else {
                    $this->session->set_flashdata('message_error', 'Designer profile not edit successfully.!', 5);
                    redirect(base_url() . "admin/dashboard/view_designers");
                }
            }
        }
    }

    public function edit_client($customer_id) {
        if (!empty($_POST)) {
            $customer = array();
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['email'] = $_POST['email'];
            $customer['phone'] = $_POST['phone'];
            if ($_POST['password'] != "") {
                if ($_POST['password'] == $_POST['confirm_password']) {
                    $customer['new_password'] = md5($_POST['password']);
                    $success = $this->Admin_model->update_data('users', $customer, array('id' => $customer_id));
                    if ($success) {
                        $this->session->set_flashdata('message_success', 'Designer profile edit successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_clients");
                    } else {
                        $this->session->set_flashdata('message_error', 'Designer profile not edit successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_clients");
                    }
                } else {
                    $this->session->set_flashdata('message_error', 'Password dose not match', 5);
                    redirect(base_url() . "admin/dashboard/view_clients");
                }
            } else {
                $success = $this->Admin_model->update_data('users', $customer, array('id' => $customer_id));
                if ($success) {
                    $this->session->set_flashdata('message_success', 'Designer profile edit successfully.!', 5);
                    redirect(base_url() . "admin/dashboard/view_clients");
                } else {
                    $this->session->set_flashdata('message_error', 'Designer profile not edit successfully.!', 5);
                    redirect(base_url() . "admin/dashboard/view_clients");
                }
            }
        }
    }

    public function gz_delete_files() {
        //print_r($_POST);exit;
        if (isset($_POST)) {
            $id = $_POST['requests_files'];
            $request_id = $_POST['request_id'];
            $delete = $this->Admin_model->delete_data('request_files', $id);
            if ($delete) {
                
                if (array_key_exists('prefile', $_POST)) {
                    $src_filename = $_POST['srcfile'];
                    $pre_filename = $_POST['prefile'];
                    $thumb_prefile = $_POST['thumb_prefile'];

                   // if (file_exists($src_filename) || file_exists($pre_filename) || file_exists($thumb_prefile)) {
                   // $this->load->library('s3_upload');
                    //$this->s3_upload->delete_file_from_s3($src_filename,$pre_filename,$thumb_prefile);
                  //  $this->s3_upload->delete_file_from_s3($pre_filename);
//                        unlink($src_filename);
//                        unlink($pre_filename);
//                        unlink($thumb_prefile);
                        redirect(base_url() . 'admin/dashboard/view_request/' . $request_id . '/1');
//                    } else {
//                      //  echo 'Could not delete ' . $filename . ', file does not exist';
//                    }
                }
            }
        }
    }

    public function add_customer() {
        $allplan = $this->Stripe->getallsubscriptionlist();

        $subscription[] = "Select Plan";
        for ($i = 0; $i < sizeof($allplan); $i++) {
            $subscription[$allplan[$i]['id']] = $allplan[$i]['name'] . " - $" . $allplan[$i]['amount'] / 100 . "/" . $allplan[$i]['interval'];
        }

        if (!empty($_POST)) {
            $customer = array();
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['email'] = $_POST['email'];
            $customer['new_password'] = md5($_POST['password']);

            $customer['current_plan'] = $_POST['subscription_plans_id'];

            $customer['role'] = "customer";


            //$stripe_controller = new StripesController();
            $stripe_customer = $stripe_controller->createCustomer($customer->email);
            if (!$stripe_customer['status']) {

                $cusomer_save = $this->Welcome_model->insert_data("users", $customer);
                //$customer->current_plan = $stripe_customer['data']['customer_id'];
                if ($cusomer_save <= 0) {
                    $this->Flash->error('Customer cannot ne created.');
                    return $this->redirect(Router::url(['action' => 'index'], TRUE));
                }


                $this->Users->delete($customer);
                $this->Flash->error($stripe_customer['message']);
                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }

            $customer_id = $stripe_customer['data']['customer_id'];
            $card_number = trim($this->request->getData('last4'));
            $expiry = explode('/', $this->request->getData('exp_month'));
            $expiry_month = $expiry[0];
            $expiry_year = $expiry[1];
            $cvc = $this->request->getData('cvc');



            $stripe_card = $stripe_controller->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);

            if (!$stripe_card['status']) {
                $this->Users->delete($customer);
                $this->Flash->error($stripe_card['message']);
                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }

            $subscription_plan_id = $this->request->getData('subscription_plans_id');

            if ($subscription_plan_id) {
                $stripe_subscription = '';
                $stripe_subscription = $stripe_controller->create_cutomer_subscribePlan($stripe_customer['data']['customer_id'], $subscription_plan_id);
            }

            //Add Plan to a customer
            if (!$stripe_subscription['status']) {
                $this->Users->delete($customer);
                $this->Flash->error($stripe_subscription['message']);
                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }


            $customer->current_plan = $stripe_subscription['data']['subscription_id'];

            $customer->customer_id = $stripe_customer['data']['customer_id'];
            $customer->plan_name = $this->request->getData('subscription_plans_id');
            $plandetails = $stripe_controller->retrieveoneplan($this->request->getData('subscription_plans_id'));
            $customer->plan_amount = $plandetails['amount'] / 100;
            $customer->plan_interval = $plandetails['interval'];
            $customer->plan_trial_period_days = $plandetails['trial_period_days'];
            $customer->plan_turn_around_days = $plandetails['metadata']->turn_around_days;

            $is_customer_save = $this->Users->save($customer);
            // $is_customer_save = "1";

            if ($is_customer_save) {
                $email_data = new \stdClass();
                $email_data->to_email = $customer->email;
                $email_data->subject = 'Welcome to Graphics Zoo!';
                $html = "";

                $html.= '<div style="background: #fff;">
								<div style="width:800px;border: 2px solid #f23f5b;margin: auto;border-radius:20px;">
									<div style="text-align:center;margin-bottom: 20px;">
										<img src="' . SITE_IMAGES_URL . 'img/logo.png" height=80px; style="padding: 25px;"></img>
									</div>
									<div style="margin-top:10px;">
										<p style="padding-left: 25px;">Hi ' . $customer->first_name . ', </p>
									</div>
									<div style="margin-top:10px;text-align:justify;">
										<p style="padding: 0px 25px;">We are very excited that you have decided to join us for your design needs. As you get started, expect to have your designs automatically assigned to the best suited designer. you will continue working with that dedicated designer for all your needs. if you have any questions or concerns please fell free to contact us at <u>'.SUPPORT_EMAIL.'</u> and our team will find a solution for you. We are here to help you with all your needs.</p>
									</div>
									<div style="margin-top:20px;text-align:justify;;padding-bottom:5px;">
										<p style="padding: 0px 25px;">Now, Let\'s go your first design request started. Click below to login and submit a request.</p>
									</div>
									<div style="margin-top:10px;text-align:justify;display: flex;">
										<a href="http://graphicszoo.com/login" style="margin: auto;"><button style="padding:10px;font-size:22px;font-weight:600;color:#fff;background:#f23f5b;border: none;margin: auto;">Request Design</button></a>
									</div>
									
									<div style="margin-top:10px;text-align:justify;display: flex;">
										<p style="padding-left: 25px;">Thank you,</p>
									</div>
									
									<div style="text-align:justify;display: flex;">
										<p style="padding-left: 25px;">Andrew Jones</br>Customer Relations</p>
									</div>
								</div>
							</div>';
                $email_data->template = $html;

                $this->sendMail($email_data);
                // exit;
                $plan_amount = 0;

                switch ($subscription_plan_id):
                    case STRIPE_MONTH_SILVER_PLAN_ID:
                        $plan_amount = STRIPE_MONTH_SILVER_PLAN_AMOUNT / 100;
                        break;
                    case STRIPE_MONTH_GOLDEN_PLAN_ID:
                        $plan_amount = STRIPE_MONTH_GOLDEN_PLAN_AMOUNT / 100;
                        break;
                    case STRIPE_YEAR_SILVER_PLAN_ID:
                        $plan_amount = STRIPE_YEAR_SILVER_PLAN_AMOUNT / 100;
                        break;
                    case STRIPE_YEAR_GOLDEN_PLAN_ID:
                        $plan_amount = STRIPE_YEAR_GOLDEN_PLAN_AMOUNT / 100;
                        break;
                    default :
                        break;
                endswitch;

                $this->request->session()->write('plan_amount', $plan_amount);

                $this->Flash->success('User created successfully and subscribed successfully.');

                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }else {
                $this->Flash->error('User cannot be created.');
            }
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/add_customer', array("subscription" => $subscription));
        $this->load->view('admin/admin_footer');
    }

    public function view_clients() {
        $this->checkloginuser();
        $planlist = $this->Stripe->getallsubscriptionlist();
        $countcustomer = $this->Account_model->clientlist_load_more("paid","",true);
        $activeClients = $this->Account_model->clientlist_load_more("paid");
//        $countcustomer = $this->Account_model->getall_customer_count();
//        $activeClients = $this->Account_model->getall_customer();
        // echo "<pre>";print_r($activeClients);
        // $activeClients = $this->Account_model->getall_customer("assigned");
        for ($i = 0; $i < sizeof($activeClients); $i++) {
            $activeClients[$i]['total_rating'] = $this->Request_model->get_average_rating($activeClients[$i]['id']);
            $active = $this->Account_model->get_all_active_view_request_by_customer(array('active', 'disapprove'), $activeClients[$i]['id']);
            $inque = $this->Account_model->get_all_active_view_request_by_customer(array('assign'), $activeClients[$i]['id']);
            $revision = $this->Account_model->get_all_active_view_request_by_customer(array('disapprove'), $activeClients[$i]['id']);
            $review = $this->Account_model->get_all_active_view_request_by_customer(array('checkforapprove'), $activeClients[$i]['id']);
            $complete = $this->Account_model->get_all_active_view_request_by_customer(array('approved'), $activeClients[$i]['id']);
            $user = $this->Account_model->get_all_active_request_by_customer($activeClients[$i]['id']);
            $activeClients[$i]['active'] = $active;
            $activeClients[$i]['inque_request'] = $inque;
            $activeClients[$i]['revision_request'] = $revision;
            $activeClients[$i]['review_request'] = $review;
            $activeClients[$i]['complete_request'] = $complete;

            if ($activeClients[$i]['designer_id'] != 0) {
                $activeClients[$i]['designer'] = $this->Request_model->get_user_by_id($activeClients[$i]['designer_id']);
            } else {
                $activeClients[$i]['designer'] = "";
            }
        }
        
        $counttrailcustomer = $this->Account_model->clientlist_load_more("free","",true);
        $trailClients = $this->Account_model->clientlist_load_more("free");
        //echo "<pre/>";print_r($trailClients);
        for ($i = 0; $i < sizeof($trailClients); $i++) {
            $trailClients[$i]['total_rating'] = $this->Request_model->get_average_rating($trailClients[$i]['id']);
            $active = $this->Account_model->get_all_active_view_request_by_customer(array('active', 'disapprove'), $trailClients[$i]['id']);
            $inque = $this->Account_model->get_all_active_view_request_by_customer(array('assign'), $trailClients[$i]['id']);
            $revision = $this->Account_model->get_all_active_view_request_by_customer(array('disapprove'), $trailClients[$i]['id']);
            $review = $this->Account_model->get_all_active_view_request_by_customer(array('checkforapprove'), $trailClients[$i]['id']);
            $complete = $this->Account_model->get_all_active_view_request_by_customer(array('approved'), $trailClients[$i]['id']);
            $user = $this->Account_model->get_all_active_request_by_customer($trailClients[$i]['id']);
            $trailClients[$i]['active'] = $active;
            $trailClients[$i]['inque_request'] = $inque;
            $trailClients[$i]['revision_request'] = $revision;
            $trailClients[$i]['review_request'] = $review;
            $trailClients[$i]['complete_request'] = $complete;

            if ($trailClients[$i]['designer_id'] != 0) {
                $trailClients[$i]['designer'] = $this->Request_model->get_user_by_id($trailClients[$i]['designer_id']);
            } else {
                $trailClients[$i]['designer'] = "";
            }
        }
        
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $data["all_designers"] = $this->Request_model->get_all_designers();
        for ($i = 0; $i < sizeof($data['all_designers']); $i++) {

            $user = $this->Account_model->get_all_active_request_by_designer($data['all_designers'][$i]['id']);
            $data['all_designers'][$i]['active_request'] = sizeof($user);
        }
        $subscription_plan_list = array();
        $j = 0;
        for ($i = 0; $i < sizeof($planlist); $i++) {
            if ($planlist[$i]['id'] == '30291' || $planlist[$i]['id'] == 'A21481D' || $planlist[$i]['id'] == 'A1900S' || $planlist[$i]['id'] == 'M99S' || $planlist[$i]['id'] == 'M199G' || $planlist[$i]['id'] == 'M1991D' || $planlist[$i]['id'] == 'M199S' || $planlist[$i]['id'] == 'A10683D' || $planlist[$i]['id'] == 'M993D') {
                continue;
            }
            $subscription_plan_list[$j]['id'] = $planlist[$i]['id'];
            $subscription_plan_list[$j]['name'] = $planlist[$i]['name'] . " - $" . $planlist[$i]['amount'] / 100 . "/" . $planlist[$i]['interval'];
            $subscription_plan_list[$j]['amount'] = $planlist[$i]['amount'] / 100;
            $subscription_plan_list[$j]['interval'] = $planlist[$i]['interval'];
            $subscription_plan_list[$j]['trial_period_days'] = $planlist[$i]['trial_period_days'];
            $j++;
        }

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/view_clients', array("activeClients" => $activeClients, "data" => $data, "planlist" => $subscription_plan_list, 'count_customer' => $countcustomer,'counttrailcustomer' => $counttrailcustomer,'trailClients' => $trailClients));
        $this->load->view('admin/admin_footer');
    }

//    public function view_designers() {
//         
//        $this->checkloginuser();
//        if (!empty($_POST)) {
//            if ($_POST['password'] != $_POST['confirm_password']) {
//                $this->session->set_flashdata('message_error', 'Password and Confirm Password is not Match.!', 5);
//                redirect(base_url() . "admin/dashboard/view_designers");
//            }
//            $email_check = $this->Account_model->getuserbyemail($_POST['email'],'designer');
//            if (!empty($email_check)) {
//                $this->session->set_flashdata('message_error', 'Email Address is already available.!', 5);
//
//                redirect(base_url() . "admin/dashboard/view_designers");
//            }
//            $success = $this->Welcome_model->insert_data("users", array("first_name" => $_POST['first_name'], "last_name" => $_POST['last_name'], "email" => $_POST['email'], "phone" => $_POST['phone'], 'new_password' => md5($_POST['password']), "status" => "1", "is_active" => "1", "role" => "designer", "created" => date("Y-m-d H:i:s")));
//            if ($success) {
//                $this->session->set_flashdata('message_success', 'Designer Added Successfully.!', 5);
//
//                redirect(base_url() . "admin/dashboard/view_designers");
//            } else {
//                $this->session->set_flashdata('message_error', 'Designer Not Added Successfully.!', 5);
//
//                redirect(base_url() . "admin/dashboard/view_designers");
//            }
//        }
//        $designers = $this->Account_model->getall_designer();
//
//        for ($i = 0; $i < sizeof($designers); $i++) {
//            $designers[$i]['total_rating'] = $this->Request_model->get_average_rating($designers[$i]['id']);
//            //echo $designers[$i]['id']."</br>";
//        }
//
//        $user_id = $_SESSION['user_id'];
//        $profile_data = $this->Admin_model->getuser_data($user_id);
//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
//
//        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
//        $this->load->view('admin/view_designers', array("allDesigners" => $designers));
//        $this->load->view('admin/admin_footer');
//    }

    
    public function view_designers() {
        $this->checkloginuser();
        if (!empty($_POST)) {
            if ($_POST['password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata('message_error', 'Password and Confirm Password is not Match.!', 5);
                redirect(base_url() . "admin/dashboard/view_designers");
            }
            $email_check = $this->Account_model->getuserbyemail($_POST['email'],'designer');
            if (!empty($email_check)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available.!', 5);

                redirect(base_url() . "admin/dashboard/view_designers");
            }
            $success = $this->Welcome_model->insert_data("users", array("first_name" => $_POST['first_name'], "last_name" => $_POST['last_name'], "email" => $_POST['email'], "phone" => $_POST['phone'], 'new_password' => md5($_POST['password']), "status" => "1", "is_active" => "1", "role" => "designer", "created" => date("Y-m-d H:i:s")));
            if ($success) {
                $this->session->set_flashdata('message_success', 'Designer Added Successfully.!', 5);

                redirect(base_url() . "admin/dashboard/view_designers");
            } else {
                $this->session->set_flashdata('message_error', 'Designer Not Added Successfully.!', 5);

                redirect(base_url() . "admin/dashboard/view_designers");
            }
        }
       // $designers = $this->Account_model->getall_designer();
        $designerscount = $this->Account_model->designerlist_load_more("",true);
        $designers = $this->Account_model->designerlist_load_more("");
        for ($i = 0; $i < sizeof($designers); $i++) {
            $designers[$i]['total_rating'] = $this->Request_model->get_average_rating($designers[$i]['id']);
            //echo $designers[$i]['id']."</br>";
        }

        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/view_designers', array("allDesigners" => $designers,'designerscount'=>$designerscount));
        $this->load->view('admin/admin_footer');
    }
//    public function view_qa() {
//        $this->checkloginuser();
//        if (!empty($_POST)) {
//            if ($_POST['password'] != $_POST['confirm_password']) {
//                $this->session->set_flashdata('message_error', 'Password and Confirm Password is not Match.!', 5);
//
//                redirect(base_url() . "admin/dashboard/view_qa");
//            }
//            $email_check = $this->Account_model->getuserbyemail($_POST['email']);
//            if (!empty($email_check)) {
//                $this->session->set_flashdata('message_error', 'Email Address is already available.!', 5);
//
//                redirect(base_url() . "admin/dashboard/view_qa");
//            }
//            $success = $this->Welcome_model->insert_data("users", array("first_name" => $_POST['first_name'], "last_name" => $_POST['last_name'], "email" => $_POST['email'], "phone" => $_POST['phone'], 'new_password' => md5($_POST['password']), "is_active" => 1, "status" => "1", "role" => $_POST['role'], "created" => date("Y-m-d H:i:s")));
//            if ($success) {
//                $this->session->set_flashdata('message_success', strtoupper($_POST['role']) . ' Added Successfully.!', 5);
//
//                redirect(base_url() . "admin/dashboard/view_qa");
//            } else {
//                $this->session->set_flashdata('message_error', strtoupper($_POST['role']) . ' Not Added Successfully.!', 5);
//
//                redirect(base_url() . "admin/dashboard/view_qa");
//            }
//        }
//
//        $qa = $this->Account_model->getqa_member();
//
//        /* new */
//        for ($i = 0; $i < sizeof($qa); $i++) {
//            $designers = $this->Request_model->get_designer_list_for_qa("array", $qa[$i]['id']);
//            $qa[$i]['total_designer'] = sizeof($designers);
//
//            $mydesigner = $this->Request_model->get_designer_list_for_qa("", $qa[$i]['id']);
//
//
//            if ($mydesigner) {
//                $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
//                $qa[$i]['total_clients'] = sizeof($data);
//            } else {
//                $qa[$i]['total_clients'] = 0;
//            }
//        }
//
//
//        $va = $this->Account_model->getva_member();
//        /* new */
//        for ($i = 0; $i < sizeof($va); $i++) {
//            $designers = $this->Request_model->get_designer_list_for_qa("array", $va[$i]['id']);
//            $va[$i]['total_designer'] = sizeof($designers);
//
//            $mydesignerr = $this->Request_model->get_designer_list_for_qa("", $va[$i]['id']);
//
//
//            if ($mydesignerr) {
//                $data = $this->Request_model->get_customer_list_for_qa("array", $mydesignerr);
//                $va[$i]['total_clients'] = sizeof($data);
//            } else {
//                $va[$i]['total_clients'] = 0;
//            }
//        }
//
//
//        $user_id = $_SESSION['user_id'];
//        $profile_data = $this->Admin_model->getuser_data($user_id);
//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
//
//        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
//        $this->load->view('admin/view_qa', array("allQa" => $qa, "allVa" => $va));
//        $this->load->view('admin/admin_footer');
//    }

    
    public function view_qa() {
        $this->checkloginuser();
        if (!empty($_POST)) {
            if ($_POST['password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata('message_error', 'Password and Confirm Password is not Match.!', 5);

                redirect(base_url() . "admin/dashboard/view_qa");
            }
            $email_check = $this->Account_model->getuserbyemail($_POST['email']);
            if (!empty($email_check)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available.!', 5);

                redirect(base_url() . "admin/dashboard/view_qa");
            }
            $success = $this->Welcome_model->insert_data("users", array("first_name" => $_POST['first_name'], "last_name" => $_POST['last_name'], "email" => $_POST['email'], "phone" => $_POST['phone'], 'new_password' => md5($_POST['password']), "is_active" => 1, "status" => "1", "role" => $_POST['role'], "created" => date("Y-m-d H:i:s")));
            if ($success) {
                $this->session->set_flashdata('message_success', strtoupper($_POST['role']) . ' Added Successfully.!', 5);

                redirect(base_url() . "admin/dashboard/view_qa");
            } else {
                $this->session->set_flashdata('message_error', strtoupper($_POST['role']) . ' Not Added Successfully.!', 5);

                redirect(base_url() . "admin/dashboard/view_qa");
            }
        }
        $countqa = $this->Account_model->qavalist_load_more('qa',true);
        $qa = $this->Account_model->qavalist_load_more('qa',false);
        //$qa = $this->Account_model->getqa_member();
        /* new */
        for ($i = 0; $i < sizeof($qa); $i++) {
            $designers = $this->Request_model->get_designer_list_for_qa("array", $qa[$i]['id']);
            $qa[$i]['total_designer'] = sizeof($designers);
            $mydesigner = $this->Request_model->get_designer_list_for_qa("", $qa[$i]['id']);
            if ($mydesigner) {
                $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                $qa[$i]['total_clients'] = sizeof($data);
            } else {
                $qa[$i]['total_clients'] = 0;
            }
        }
        $countva = $this->Account_model->qavalist_load_more('va',true);
        $va = $this->Account_model->qavalist_load_more('va',false);
        //$va = $this->Account_model->getva_member();
        /* new */
        for ($i = 0; $i < sizeof($va); $i++) {
            $designers = $this->Request_model->get_designer_list_for_qa("array", $va[$i]['id']);
            $va[$i]['total_designer'] = sizeof($designers);

            $mydesignerr = $this->Request_model->get_designer_list_for_qa("", $va[$i]['id']);


            if ($mydesignerr) {
                $data = $this->Request_model->get_customer_list_for_qa("array", $mydesignerr);
                $va[$i]['total_clients'] = sizeof($data);
            } else {
                $va[$i]['total_clients'] = 0;
            }
        }


        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/view_qa', array("allQa" => $qa, "allVa" => $va,"countqa" => $countqa,"countva" => $countva));
        $this->load->view('admin/admin_footer');
    }
    
    public function send_message_request() {
        // print_r($_POST);
        $sender_type = $_POST['sender_type'];
        if ($sender_type == 'designer') {
            $_POST['designer_seen'] = '1';
        } elseif ($sender_type == 'customer') {
            $_POST['customer_seen'] = '1';
        } elseif ($sender_type == 'admin') {
            $_POST['admin_seen'] = '1';
        }
        //die();
        $designer_id = $_POST['designer_id'];
        unset($_POST['designer_id']);
        $_POST['created'] = date("Y-m-d H:i:s");
        // $_POST['designer_seen'] = "1";
        if ($this->Welcome_model->insert_data("request_discussions", $_POST)) {
            echo '1';

            $rtitle = $this->Request_model->get_request_by_id($_POST['request_id']);
            $qa = $this->Admin_model->getuser_data($designer_id);

            if (!empty($rtitle)) {
                if ($_POST['with_or_not_customer'] == "1") {
                    $data['user_id'] = $_POST['reciever_id'];
                    $data['heading'] = $rtitle[0]['title'];
                    $data['title'] = $_POST['message'];
                    $data['created'] = date("Y:m:d H:i:s");
                    $data['url'] = "customer/request/project_info/" . $_POST['request_id'];
                    $data['sender_users_id'] = $_SESSION['user_id'];
                    $mn = $this->Request_model->get_request_by_data($data['url'], $data['heading'], $data['user_id'], 0);
                    $data['shown'] = 0;
                    $data['count'] = 1;
                    //if (empty($mn)) {
                    $this->Welcome_model->insert_data("message_notification", $data);
                    //} else {
                    //   $this->Welcome_model->update_data("message_notification", array("count" => $mn[0]['count'] + 1, "created" => date("Y:m:d H:i:s")), array("url" => $data['url'], "heading" => $data['heading'], "user_id" => $data['user_id'], "shown" => 0));
                    //}
                }
                $data['user_id'] = $designer_id;
                $data['heading'] = $rtitle[0]['title'];
                $data['title'] = $_POST['message'];
                $data['created'] = date("Y:m:d H:i:s");
                $data['url'] = "designer/request/project_info/" . $_POST['request_id'];
                $data['sender_users_id'] = $_SESSION['user_id'];
                $mn = $this->Request_model->get_request_by_data($data['url'], $data['heading'], $data['user_id'], 0);
                $data['shown'] = 0;
                $data['count'] = 1;
                //if (empty($mn)) {
                $this->Welcome_model->insert_data("message_notification", $data);
                /* } else {
                  $this->Welcome_model->update_data("message_notification", array("count" => $mn[0]['count'] + 1, "created" => date("Y:m:d H:i:s")), array("url" => $data['url'], "heading" => $data['heading'], "user_id" => $data['user_id'], "shown" => 0));
                  } */

                $data['user_id'] = $qa[0]['qa_id'];
                $data['heading'] = $rtitle[0]['title'];
                $data['title'] = $_POST['message'];
                $data['created'] = date("Y:m:d H:i:s");
                $data['url'] = "qa/dashboard/view_project/" . $_POST['request_id'];
                $data['sender_users_id'] = $_SESSION['user_id'];
                $mn = $this->Request_model->get_request_by_data($data['url'], $data['heading'], $data['user_id'], 0);
                $data['shown'] = 0;
                $data['count'] = 1;
                //if (empty($mn)) {
                $this->Welcome_model->insert_data("message_notification", $data);
            } else {
                $data['title'] = "New Message Arrived";
                $data['created'] = date("Y:m:d H:i:s");
                $this->Welcome_model->insert_data("notification", $data);
            }
            $this->Request_model->get_notifications($_SESSION['user_id']);
        } else {
            echo '0';
        }
    }

    public function load_messages_from_all_user() {
        $project_id = $_POST['project_id'];
        $customer_id = $_POST['cst_id'];
        $designer_id = $_POST['desg_id'];
        $msg_seen = $_POST['admin_seen'];
        $chat_request_msg = $this->Request_model->get_admin_unseen_msg($project_id, $customer_id, $designer_id, $msg_seen);
        if (!empty($chat_request_msg)) {
            $message['message'] = $chat_request_msg[0]['message'];
             $message['profile_picture'] = isset($chat_request_msg[0]['profile_picture']) ? $chat_request_msg[0]['profile_picture'] : 'user-admin.png';
            $message['typee'] = $chat_request_msg[0]['sender_type'];
            $message['with_or_not'] = $chat_request_msg[0]['with_or_not_customer'];
            $message['msg_first_name'] = $chat_request_msg[0]['first_name'];
            echo json_encode($message);
            $this->Admin_model->update_data("request_discussions", array("admin_seen" => 1), array("request_id" => $project_id));
        } else {
            // $message = "Hello Everyone";
            // echo $message;
        }
    }

    public function view_files($id) {
        $this->checkloginuser();
        if ($id == "") {
            redirect(base_url() . "admin/dashboard");
        }
        $this->Welcome_model->update_data("request_files", array("admin_seen" => 1), array('id' => $id));
        $this->Admin_model->update_data("request_file_chat", array("admin_seen" => '1'), array("request_file_id" => $id));
        $url = "admin/dashboard/view_files/" . $id . "?id=" . $_GET['id'];
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url));
        if (!empty($_POST)) {
            $success = $this->Welcome_model->update_data("request_files", array("grade" => $_POST['grade']), array('id' => $_POST['id']));
            if ($success) {
                $this->session->set_flashdata('message_success', 'Grade is Submitted Successfully.!', 5);

                redirect(base_url() . "admin/dashboard/view_files/" . $id . "?id=" . $_GET['id']);
            } else {
                $this->session->set_flashdata('message_error', 'Grade is Not Submitted Successfully.!', 5);

                redirect(base_url() . "admin/dashboard/view_files/" . $id . "?id=" . $_GET['id']);
            }
        }

        $designer_file = $this->Admin_model->get_requested_files($_GET['id'], "", "designer");
        $data['request'] = $this->Request_model->get_request_by_id($_GET['id']);
        $data['main_id'] = $id;

        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id']);
            
            $designer_file[$i]['modified_date'] = $this->onlytimezone($designer_file[$i]['modified']);
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified_date'];
            
            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
                
            $designer_file[$i]['chat'][$j]['msg_created'] = $this->onlytimezone($designer_file[$i]['chat'][$j]['created']);
            $designer_file[$i]['chat'][$j]['created'] =   date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
                
            }
        }
        $dataa = $this->Request_model->get_request_by_id($_GET['id']);
        $designer = $this->Account_model->getuserbyid($dataa[0]['designer_id']);
        $customer = $this->Account_model->getuserbyid($dataa[0]['customer_id']);
        $data['user'] = $this->Account_model->getuserbyid($_SESSION['user_id']);
        if (!empty($designer)) {
            $data['data'][0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
            $data['data'][0]['designer_image'] = $designer[0]['profile_picture'];
            $data['data'][0]['title'] = $dataa[0]['title'];
            $data['data'][0]['designer_sortname'] = substr($designer[0]['first_name'], 0, 1) . substr($designer[0]['last_name'], 0, 1);
        }
        if (!empty($customer)) {
            $data['data'][0]['customer_name'] = $customer[0]['first_name'] . " " . $designer[0]['last_name'];
            $data['data'][0]['customer_image'] = $customer[0]['profile_picture'];
            $data['data'][0]['customer_sortname'] = substr($customer[0]['first_name'], 0, 1) . substr($customer[0]['last_name'], 0, 1);
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $data['chat_request'] = $this->Request_model->get_chat_request_by_id($_GET['id']);
        $data['designer_file'] = $designer_file;
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('admin/admin_header', array('edit_profile' => $profile_data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('admin/view_files', $data);
        $this->load->view('admin/admin_footer');
    }

        public function client_projects($id = null) {
        $titlestatus = "view_clients";
        $this->checkloginuser();
        if ($id == "") {
            redirect(base_url() . "admin/dashboard");
        }
        $data['userdata'] = $this->Account_model->getuserbyid($id);

        if (empty($data['userdata'])) {
            redirect(base_url() . "admin/dashboard");
        }
        
        $count_active_project = $this->Account_model->customer_projects_load_more(array('active','disapprove','assign','pendingrevision'),$id,true);
        $active_project = $this->Account_model->customer_projects_load_more(array('active','disapprove','assign','pendingrevision'),$id,false,'','','');
        //$active_project = $this->Account_model->get_all_active_request_by_customer($id);

        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['priority'] = $active_project[$i]['priority'];
            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "admin");
            $active_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($active_project[$i]['id'], $_SESSION['user_id'], "admin");
            $active_project[$i]['total_files'] = $this->Request_model->get_files_count($active_project[$i]['id'], $active_project[$i]['designer_id'], "admin");
            $active_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($active_project[$i]['id']);
            $getfileid = $this->Request_model->get_attachment_files($active_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
                $active_project[$i]['expected'] = $this->myfunctions->check_timezone($active_project[$i]['latest_update']);
            }
            $active_project[$i]['comment_count'] = $commentcount;

            $designer = $this->Request_model->getuserbyid($active_project[$i]['designer_id']);
            $customer = $this->Request_model->getuserbyid($active_project[$i]['customer_id']);
            if (!empty($designer)) {
                $active_project[$i]['designer_first_name'] = $designer[0]['first_name'];
                $active_project[$i]['designer_last_name'] = $designer[0]['last_name'];
                $active_project[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $active_project[$i]['designer_first_name'] = "";
                $active_project[$i]['designer_last_name'] = "";
                $active_project[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $active_project[$i]['customer_first_name'] = $customer[0]['first_name'];
                $active_project[$i]['customer_last_name'] = $customer[0]['last_name'];
                $active_project[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $active_project[$i]['customer_first_name'] = "";
                $active_project[$i]['customer_last_name'] = "";
                $active_project[$i]['plan_turn_around_days'] = 0;
            }
        }
        
        $count_check_approve_project = $this->Account_model->customer_projects_load_more(array('checkforapprove', 'pendingrevision'),$id,true);
        $check_approve_project = $this->Account_model->customer_projects_load_more(array('checkforapprove', 'pendingrevision'),$id);

       // $check_approve_project = $this->Request_model->get_request_list_for_admin($id, array('checkforapprove', 'pendingrevision'));

        for ($i = 0; $i < sizeof($check_approve_project); $i++) {
            $check_approve_project[$i]['total_chat'] = $this->Request_model->get_chat_number($check_approve_project[$i]['id'], $check_approve_project[$i]['customer_id'], $check_approve_project[$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($check_approve_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $check_approve_project[$i]['comment_count'] = $commentcount;
            $check_approve_project[$i]['modified'] = $this->onlytimezone($check_approve_project[$i]['modified']);
            $check_approve_project[$i]['deliverydate'] = $this->onlytimezone($check_approve_project[$i]['latest_update']);
            $check_approve_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($check_approve_project[$i]['id'], $_SESSION['user_id'], "admin");
            $check_approve_project[$i]['total_files'] = $this->Request_model->get_files_count($check_approve_project[$i]['id'], $check_approve_project[$i]['designer_id'], "admin");
            $check_approve_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($check_approve_project[$i]['id']);
        }

//        $disapprove_project = $this->Request_model->get_request_list_for_admin($id, array('disapprove'));
//
//        for ($i = 0; $i < sizeof($disapprove_project); $i++) {
//            if (isset($check_approve_project[$i]['id'])):
//                $disapprove_project[$i]['total_chat'] = $this->Request_model->get_chat_number($check_approve_project[$i]['id'], $disapprove_project[$i]['customer_id'], $disapprove_project[$i]['designer_id'], "admin");
//                $getfileid = $this->Request_model->get_attachment_files($disapprove_project[$i]['id'], "designer");
//                $commentcount = 0;
//                for ($j = 0; $j < sizeof($getfileid); $j++) {
//
//                    $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
//                }
//                $disapprove_project[$i]['comment_count'] = $commentcount;
//                $disapprove_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($check_approve_project[$i]['id'], $_SESSION['user_id'], "admin");
//                $disapprove_project[$i]['total_files'] = $this->Request_model->get_files_count($disapprove_project[$i]['id'], $disapprove_project[$i]['designer_id'], "admin");
//                $disapprove_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($disapprove_project[$i]['id']);
//            endif;
//        }

//        $pending_project = $this->Request_model->get_request_list_for_admin($id, array('pending', 'assign'), "", "index_number");
//
//        for ($i = 0; $i < sizeof($pending_project); $i++) {
//            $pending_project[$i]['total_chat'] = $this->Request_model->get_chat_number($pending_project[$i]['id'], $pending_project[$i]['customer_id'], $pending_project[$i]['designer_id'], "admin");
//            $getfileid = $this->Request_model->get_attachment_files($pending_project[$i]['id'], "designer");
//            $commentcount = 0;
//            for ($j = 0; $j < sizeof($getfileid); $j++) {
//
//                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
//            }
//            $pending_project[$i]['comment_count'] = $commentcount;
//            $pending_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($pending_project[$i]['id'], $_SESSION['user_id'], "admin");
//            $pending_project[$i]['total_files'] = $this->Request_model->get_files_count($pending_project[$i]['id'], $pending_project[$i]['designer_id'], "admin");
//            $pending_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($pending_project[$i]['id']);
//        }

        $count_complete_project = $this->Account_model->customer_projects_load_more(array('approved'),$id,true);
        $complete_project = $this->Account_model->customer_projects_load_more(array('approved'),$id);
        
        //$complete_project = $this->Request_model->get_request_list_for_admin($id, array('approved'), "", "");
        for ($i = 0; $i < sizeof($complete_project); $i++) {
            $complete_project[$i]['total_chat'] = $this->Request_model->get_chat_number($complete_project[$i]['id'], $complete_project[$i]['customer_id'], $complete_project[$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($complete_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $complete_project[$i]['comment_count'] = $commentcount;
            $complete_project[$i]['approvddate'] = $this->onlytimezone($complete_project[$i]['approvaldate']);
            $complete_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($complete_project[$i]['id'], $_SESSION['user_id'], "admin");
            $complete_project[$i]['total_files'] = $this->Request_model->get_files_count($complete_project[$i]['id'], $complete_project[$i]['designer_id'], "admin");
            $complete_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($complete_project[$i]['id']);
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $designer_list = $this->Request_model->get_designer_list_for_qa("array");
        $this->load->view('admin/admin_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, 'titlestatus' => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view('admin/client_projects', array("custid" => $id,"count_active_project" => $count_active_project,"active_project" => $active_project, "count_complete_project" => $count_complete_project,"complete_project" => $complete_project, "pending_project" => $pending_project, "disapprove_project" => $disapprove_project, "count_check_approve_project" => $count_check_approve_project,"check_approve_project" => $check_approve_project, "userdata" => $data['userdata'], "designer_list" => $designer_list));
        $this->load->view('admin/admin_footer');
    }

    public function logout() {
        $this->checkloginuser();
        $this->Welcome_model->update_data("users", array("online" => 0), array("id" => $_SESSION['user_id']));
        $this->Request_model->logout();
        redirect(base_url());
    }

    public function active_view_designer($id) {
        $titlestatus = 'view_designer';
        $sql = "select * from users where designer_id= '" . $id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $designers = $this->Account_model->getuserbyid($id);
        $skills = $this->Request_model->getallskills($id);
        $datadesigner['activeproject'] = array();
        $datadesigner['inqueueproject'] = array();
        $datadesigner['pendingreviewproject'] = array();
        $datadesigner['pendingproject'] = array();
        $datadesigner['completeproject'] = array();
        // Active Designer Project Section Start Here
        $datadesigner['count_active_project'] = $this->Account_model->designer_projects_load_more(array('active','disapprove'),$id,true);
        $datadesigner['activeproject'] = $this->Account_model->designer_projects_load_more(array('active','disapprove'),$id,false,'','','');
        //$datadesigner['activeproject'] = $this->Request_model->get_request_list_for_admin("", array('active', 'disapprove'), $id);
       // $datadesigner['activeproject'] = $this->Request_model->get_request_list_for_qa("", array('active', 'disapprove'), $id);
        for ($i = 0; $i < sizeof($datadesigner['activeproject']); $i++) {

            $datadesigner['activeproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['activeproject'][$i]['id'], $datadesigner['activeproject'][$i]['customer_id'], $datadesigner['activeproject'][$i]['designer_id'], "designer");
            $datadesigner['activeproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['activeproject'][$i]['id'], $_SESSION['user_id'], "admin");
            $datadesigner['activeproject'][$i]['expected'] = $this->myfunctions->check_timezone($datadesigner['activeproject'][$i]['latest_update']);
            $datadesigner['activeproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['activeproject'][$i]['id'], $datadesigner['activeproject'][$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['activeproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $datadesigner['activeproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['activeproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['activeproject'][$i]['id']);
        }

        // Active Designer Project Section End Here
        // In-Queue Designer Project Section Start Here
        $datadesigner['count_inqueueproject'] = $this->Account_model->designer_projects_load_more(array('assign', 'pending'),$id,true);
        $datadesigner['inqueueproject'] = $this->Account_model->designer_projects_load_more(array('assign', 'pending'),$id,false,'','','');
        //$datadesigner['inqueueproject'] = $this->Request_model->get_request_list_for_admin("", array('assign', 'pending'), $id);
        //$datadesigner['inqueueproject'] = $this->Request_model->get_request_list_for_qa("", array('assign', 'pending'), $id);
        for ($i = 0; $i < sizeof($datadesigner['inqueueproject']); $i++) {
            $datadesigner['inqueueproject'][$i]['priority'] = $datadesigner['inqueueproject'][$i]['priority'];
            $datadesigner['inqueueproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['inqueueproject'][$i]['id'], $datadesigner['inqueueproject'][$i]['customer_id'], $datadesigner['inqueueproject'][$i]['designer_id'], "designer");
            $datadesigner['inqueueproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['inqueueproject'][$i]['id'], $_SESSION['user_id'], "admin");
            $datadesigner['inqueueproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['inqueueproject'][$i]['id'], $datadesigner['inqueueproject'][$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['inqueueproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $datadesigner['inqueueproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['inqueueproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['inqueueproject'][$i]['id']);
        }
        // In-Queue Designer Project Section End Here
        // Pending Review Designer Project Section Start Here
        $datadesigner['count_pendingreviewproject'] = $this->Account_model->designer_projects_load_more(array('pendingrevision'),$id,true);
        $datadesigner['pendingreviewproject'] = $this->Account_model->designer_projects_load_more(array('pendingrevision'),$id,false,'','','');
        //$datadesigner['pendingreviewproject'] = $this->Request_model->get_request_list_for_admin("", array('pendingrevision'), $id);
        //$datadesigner['pendingreviewproject'] = $this->Request_model->get_request_list_for_qa("", array('pendingrevision'), $id);
        for ($i = 0; $i < sizeof($datadesigner['pendingreviewproject']); $i++) {
            $datadesigner['pendingreviewproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['pendingreviewproject'][$i]['id'], $datadesigner['pendingreviewproject'][$i]['customer_id'], $datadesigner['pendingreviewproject'][$i]['designer_id'], "designer");
            $datadesigner['pendingreviewproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['pendingreviewproject'][$i]['id'], $_SESSION['user_id'], "admin");
            $datadesigner['pendingreviewproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['pendingreviewproject'][$i]['id'], $datadesigner['pendingreviewproject'][$i]['designer_id'], 'admin');
            $datadesigner['pendingreviewproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['pendingreviewproject'][$i]['id'], $datadesigner['pendingreviewproject'][$i]['designer_id'], "admin");
            $datadesigner['pendingreviewproject'][$i]['revisiondate'] = $this->onlytimezone($datadesigner['pendingreviewproject'][$i]['modified']);
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['pendingreviewproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $datadesigner['pendingreviewproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['pendingreviewproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['pendingreviewproject'][$i]['id']);
        }
        // Pending Review Designer Project Section End Here
        // Pending Approval Designer Project Section Start Here
        $datadesigner['count_pendingproject'] = $this->Account_model->designer_projects_load_more(array('checkforapprove'),$id,true);
        $datadesigner['pendingproject'] = $this->Account_model->designer_projects_load_more(array('checkforapprove'),$id,false,'','','');
//        $datadesigner['pendingproject'] = $this->Request_model->get_request_list_for_admin("", array('checkforapprove'), $id);
        //$datadesigner['pendingproject'] = $this->Request_model->get_request_list_for_qa("", array('checkforapprove'), $id);
        for ($i = 0; $i < sizeof($datadesigner['pendingproject']); $i++) {
            $datadesigner['pendingproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['pendingproject'][$i]['id'], $datadesigner['pendingproject'][$i]['customer_id'], $datadesigner['pendingproject'][$i]['designer_id'], "designer");
            $datadesigner['pendingproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['pendingproject'][$i]['id'], $_SESSION['user_id'], "admin");
            $datadesigner['pendingproject'][$i]['reviewdate'] = $this->onlytimezone($datadesigner['pendingproject'][$i]['modified']);
            $datadesigner['pendingproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['pendingproject'][$i]['id'], $datadesigner['pendingproject'][$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['pendingproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $datadesigner['pendingproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['pendingproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['pendingproject'][$i]['id']);
        }
        // Pending Approval Designer Project Section End Here
        // Complete Designer Project Section Start Here
        $datadesigner['count_completeproject'] = $this->Account_model->designer_projects_load_more(array('approved'),$id,true);
        $datadesigner['completeproject'] = $this->Account_model->designer_projects_load_more(array('approved'),$id,false,'','','');
//        $datadesigner['completeproject'] = $this->Request_model->get_request_list_for_admin("", array('approved'), $id);
        //$datadesigner['completeproject'] = $this->Request_model->get_request_list_for_qa("", array('approved'), $id);
        for ($i = 0; $i < sizeof($datadesigner['completeproject']); $i++) {
            $datadesigner['completeproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['completeproject'][$i]['id'], $datadesigner['completeproject'][$i]['customer_id'], $datadesigner['completeproject'][$i]['designer_id'], "designer");
            $datadesigner['completeproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['completeproject'][$i]['id'], $_SESSION['user_id'], "admin");
            $datadesigner['completeproject'][$i]['approvedate'] = $this->onlytimezone($datadesigner['completeproject'][$i]['approvaldate']);
            $datadesigner['completeproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['completeproject'][$i]['id'], $datadesigner['completeproject'][$i]['designer_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['completeproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $datadesigner['completeproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['completeproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['completeproject'][$i]['id']);
        }
        // Complete Designer Project Section End Here

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $alldesigners = $this->Request_model->get_all_designer_for_qa($_SESSION['user_id']);
        for ($i = 0; $i < sizeof($alldesigners); $i++) {

            $user = $this->Account_model->get_all_active_request_by_designer($alldesigners[$i]['id']);
            $alldesigners[$i]['active_request'] = sizeof($user);
        }
        if ($_POST) {
            $designer_assigned = $this->Admin_model->assign_designer_by_qa($_POST['assign_designer'], $_POST['request_id']);
            redirect(base_url() . "admin/dashboard/active_view_designer/" . $_POST['assign_designer']);
        }
        $this->load->view('admin/admin_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "titlestatus" => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view('admin/active_view_designer', array("data" => $result, "designers" => $designers, 'datadesigner' => $datadesigner, 'alldesigners' => $alldesigners, 'skills' => $skills));
        $this->load->view('admin/admin_footer');
    }

    public function search_ajax_designer_project() {
        $dataArray = $this->input->get('title');
        $dataStatus = $this->input->get('status');
        $datauserid = $this->input->get('id');
        $dataStatus = explode(",", $dataStatus);
        $rows = $this->Request_model->GetAutocomplete_designer(array('keyword' => $dataArray), $dataStatus, $datauserid, "designer");
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rows[$i]->total_chat = $this->Request_model->get_chat_number($rows[$i]->id, $rows[$i]->customer_id, $rows[$i]->designer_id, "designer");
            $rows[$i]->total_chat_all = $this->Request_model->get_total_chat_number($rows[$i]->id, $_SESSION['user_id'], "qa");
            $rows[$i]->total_files = $this->Request_model->get_files_count($rows[$i]->id, $rows[$i]->designer_id, 'admin');
            $getfileid = $this->Request_model->get_attachment_files($rows[$i]->id, "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $rows[$i]->comment_count = $commentcount;
            if ($rows[$i]->status == "active" || $rows[$i]->status == "disapprove") {
                $rows[$i]->expected = $this->myfunctions->check_timezone($rows[$i]->latest_update);
            } elseif ($rows[$i]->status == "checkforapprove") {
                $rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->latest_update);
            } elseif ($rows[$i]->status == "approved") {
                $rows[$i]->approvddate = $this->onlytimezone($rows[$i]->approvaldate);
            }
            if ($rows[$i]->status_admin == "pendingrevision") {
                $rows[$i]->revisiondate = $this->onlytimezone($rows[$i]->modified);
            }
            $rows[$i]->total_files_count = $this->Request_model->get_files_count_all($rows[$i]->id);
        }
        for ($i = 0; $i < sizeof($rows); $i++) {
            if ($rows[$i]->status_designer == "disapprove" || $rows[$i]->status_designer == "active") {
                ?>
                <!--QA Active Section Start Here -->
                <div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 12%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">
                                        <?php
                                        if ($rows[$i]->status == "active") {
                                            echo "Expected on";
                                        } elseif ($rows[$i]->status_admin == "disapprove") {
                                            echo "Expected on";
                                        }
                                        ?>
                                    </p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php
                                        if ($rows[$i]->status == "active") {
                                            echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                        } elseif ($rows[$i]->status_admin == "disapprove") {
                                            echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                        }
                                        ?>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 48%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center">
                                                <?php if ($rows[$i]->status_admin == "active") { ?>
                                                    <span class="green text-uppercase text-wrap">In-Progress</span>
                                                <?php } elseif ($rows[$i]->status_admin == "disapprove" && $rows[$i]->who_reject == 1) { ?>
                                                    <span class="red orangetext text-uppercase text-wrap">REVISION</span>
                                                <?php } elseif ($rows[$i]->status_admin == "disapprove" && $rows[$i]->who_reject == 0) { ?>
                                                    <span class="red text-uppercase text-wrap">Quality Revision</span>
                                                <?php } ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 20%;">
                                <div class="cli-ent-xbox text-left p-left1">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                            <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1">
                                                <figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>

                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                                <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                            <?php } ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Active Section End Here -->
            <?php } elseif ($rows[$i]->status_designer == "assign" || $rows[$i]->status_designer == "pending") { ?>
                <!--QA Active Section Start Here -->
                <div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 12%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">In Queue</p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 48%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                        </div>
                    <!--                                        <div class="cell-col"><?php //echo isset($rows[$i]->priority) ? $rows[$i]->priority : '';   ?></div>-->
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="gray text-uppercase">IN-queue</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 20%;">
                                <div class="cli-ent-xbox text-left p-left1">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                            <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2">
                                                <figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>

                                            </p>
                                            <p class="pro-b">Designer</p>
                                            <p class="space-a"></p>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rows[$i]->id; ?>" data-designerid= "<?php echo $rows[$i]->designer_id; ?>">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                                <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Active Section End Here -->
            <?php } elseif ($rows[$i]->status_designer == "pendingrevision") { ?>
                <!--QA Pending Approval Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_pending_review" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">Expected on</p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rows[$i]->revisiondate)); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class=" lightbluetext text-uppercase text-wrap">Pending Review</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href=""><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                               ">
                                                   <?php echo $rows[$i]->customer_first_name; ?>
                                                   <?php
                                                   if (strlen($rows[$i]->customer_last_name) > 5) {
                                                       echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                   } else {
                                                       echo $rows[$i]->customer_last_name;
                                                   }
                                                   ?>

                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="" class="<?php echo $rows[$i]->id; ?>""><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span>
                                            <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> 
                        <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Pending Approval Section End Here -->
            <?php } elseif ($rows[$i]->status == "checkforapprove") { ?>
                <!--QA Pending Approval Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_pending_approval" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4'">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">Delivered on</p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rows[$i]->reviewdate)); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="red bluetext text-uppercase text-wrap">Pending-approval</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="#"><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                               ">
                                                   <?php echo $rows[$i]->customer_first_name; ?>
                                                   <?php
                                                   if (strlen($rows[$i]->customer_last_name) > 5) {
                                                       echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                   } else {
                                                       echo $rows[$i]->customer_last_name;
                                                   }
                                                   ?>

                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="#" class="<?php echo $rows[$i]->id; ?>""><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span>
                                            <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> 
                        <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Pending Approval Section End Here -->
            <?php } elseif ($rows[$i]->status == "approved") { ?>
                <!--QA Completed Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5'"style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 13%;">
                                <div class="cli-ent-xbox">
                                    <h3 class="app-roved green">Approved on</h3>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rows[$i]->approvddate)); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 37%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b">
                                                <?php echo substr($rows[$i]->description, 0, 30); ?>
                                            </p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href=""><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                <?php echo $rows[$i]->customer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->customer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->customer_last_name;
                                                }
                                                ?>

                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href=""><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?> ">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>

                                            </p>
                                            <p class="pro-b">Designer</p>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span>
                                            <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Completed Section End Here -->
                <?php
            }
        }
    }

    public function search_ajax_client() {
        $dataArray = $this->input->get('name');
        if ($dataArray != "") {
            $data = $this->Request_model->get_all_clients_for_admin(array('keyword' => $dataArray));
            for ($i = 0; $i < sizeof($data); $i++) {
                $user = $this->Account_model->get_all_request_by_customer($data[$i]['id']);
                $data[$i]['no_of_request'] = sizeof($user);
                $active = $this->Account_model->get_all_active_view_request_by_customer(array('active', 'disapprove'), $data[$i]['id']);
                $inque = $this->Account_model->get_all_active_view_request_by_customer(array('assign'), $data[$i]['id']);
                $revision = $this->Account_model->get_all_active_view_request_by_customer(array('disapprove'), $data[$i]['id']);
                $review = $this->Account_model->get_all_active_view_request_by_customer(array('checkforapprove'), $data[$i]['id']);
                $complete = $this->Account_model->get_all_active_view_request_by_customer(array('approved'), $data[$i]['id']);
                $user = $this->Account_model->get_all_active_request_by_customer($data[$i]['id']);
                $data[$i]['active'] = $active;
                $data[$i]['inque_request'] = $inque;
                $data[$i]['revision_request'] = $revision;
                $data[$i]['review_request'] = $review;
                $data[$i]['complete_request'] = $complete;

                $data[$i]['active_request'] = sizeof($user);

                $data[$i]['designer_name'] = "";
                if ($data[$i]['designer_id'] != 0) {
                    $data[$i]['designer'] = $this->Request_model->get_user_by_id($data[$i]['designer_id']);
                } else {
                    $data[$i]['designer'] = "";
                }
                $designer_name = $this->Account_model->getuserbyid($data[$i]['designer_id']);

                if (!empty($designer_name)) {
                    $data[$i]['designer_name'] = $designer_name[0]['first_name'];
                }
            }
        } else {
            $data = $this->Account_model->getall_customer("assigned");
        }
        for ($i = 0; $i < sizeof($data); $i++) {
            ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr not-styler"  style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 60%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" style="width: 80px; padding-right: 15px;">
                                <?php if (($data[$i]['profile_picture'] != "")) { ?>
                                    <figure class="cli-ent-img circle one">
                                        <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $data[$i]['profile_picture']; ?>" class="img-responsive one">
                                    </figure>
                                <?php } else { ?>
                                    <figure class="cli-ent-img circle one">
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive one">
                                    </figure>
                                <?php } ?>
                            </div>

                            <div class="cell-col" style="width: 185px;">
                                <h3 class="pro-head-q">
                                    <a href="<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $data[$i]['id']; ?>"><?php echo ucwords($data[$i]['first_name'] . " " . $data[$i]['last_name']); ?></a>
                                    <br>
                                    <a href="<?php echo base_url(); ?>admin/accounts/view_client_profile/<?php echo $data[$i]['id']; ?>">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </a>
                                    <!-- <a href="#" data-toggle="modal" data-target="#<?php echo $data[$i]['id']; ?>">
                                                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                            </a> -->
                                    <a href="javascript:void(0)" data-customerid="<?php echo $data[$i]['id']; ?>" class="delete_customer">
                                        <i class="fa fa-trash" aria-hidden="true"></i></a>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#viewinfo<?php echo $data[$i]['id']; ?>" class="viewinfo_customer">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                </h3>
                                <p class="pro-a"> <?php echo $data[$i]['email']; ?></p>
                            </div>

                            <div class="cell-col">
                                <p class="neft">
                                    <span class="blue text-uppercase">
                                        <?php
                                        if ($data[$i]['plan_turn_around_days'] == "1") {
                                            echo "Premium Member";
                                        } elseif ($data[$i]['plan_turn_around_days'] == "3") {
                                            echo "Standard Member";
                                        } else {
                                            echo "No Member";
                                        }
                                        ?>
                                    </span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Sign Up Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b"><?php echo date("M/d/Y", strtotime($data[$i]['created'])) ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Next Billing Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b">
                            <?php
                            $date = "";
                            if (checkdate(date("m", strtotime($data[$i]['modified'])), date("d", strtotime($data[$i]['modified'])), date("Y", strtotime($data[$i]['modified'])))) {
                                $over_date = "";
                                if ($data[$i]['plan_turn_around_days']) {
                                    $date = date("Y-m-d", strtotime($data[$i]['modified']));
                                    $time = date("h:i:s", strtotime($data[$i]['modified']));
                                    $data[$i]['plan_turn_around_days'] = "2";
                                    $date = date("m/d/Y g:i a", strtotime($date . " " . $data[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                    $diff = date_diff(date_create(date("Y-m-d", strtotime($data[$i]['modified']))), date_create($date));
                                }
                            }
                            $current_date = strtotime(date("Y-m-d"));
                            $now = time(); // or your date as well

                            $expiration_date = strtotime($date);
                            $datediff = $now - $expiration_date;
                            $date_due_day = round($datediff / (60 * 60 * 24));
                            $color = "";
                            $text_color = "white";
                            if ($date_due_day == 0) {
                                $date_due_day = "Due  </br>Today";
                                $color = "#f7941f";
                            } else
                            if ($date_due_day == (-1)) {
                                $date_due_day = "Due  </br>Tomorrow";
                                $color = "#98d575";
                            } else
                            if ($date_due_day > 0) {
                                $date_due_day = "Over Due";
                                $color = "red";
                            } else if ($date_due_day < 0) {
                                $date_due_day = "Due from </br>" . number_format($date_due_day) . " days";
                                $text_color = "black";
                            }
                            ?>
                            <?php echo date_format(date_create($date), "M /d /Y"); ?></p>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 5%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active Requests</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $data[$i]['active']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 5%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">In Queue Requests</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $data[$i]['inque_request']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 5%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Pending Approval Requests</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $data[$i]['review_request']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="min-width: 171px; max-width: 171px; width: 10%;">
                    <?php if (!empty($data[$i]['designer'])) { ?>
                        <div class="cli-ent-xbox">
                            <div class="cell-row">
                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                    <a href="">
                                        <?php if ($data[$i]['designer'][0]['profile_picture'] != "") { ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $data[$i]['designer'][0]['profile_picture']; ?>" class="img-responsive">
                                            </figure>
                                        <?php } else { ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                            </figure>
                                        <?php } ?>
                                    </a>
                                </div>
                                <div class="cell-col">
                                    <p class="text-h text-wrap" title="<?php echo $data[$i]['designer'][0]['first_name'] . " " . $data[$i]['designer'][0]['last_name']; ?>
                                       ">
                                           <?php echo $data[$i]['designer'][0]['first_name']; ?>
                                           <?php
                                           if (strlen($data[$i]['designer'][0]['last_name']) > 5) {
                                               echo ucwords(substr($data[$i]['designer'][0]['last_name'], 0, 1));
                                           } else {
                                               echo $data[$i]['designer'][0]['last_name'];
                                           }
                                           ?>

                                    </p>
                                    <p class="pro-b">Designer</p>
                                    <p class="space-a"></p>
                                    <a href="#" class="addde-signersk1 permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $data[$i]['id']; ?>">
                                        <span class="sma-red">+</span> Add Designer
                                    </a>
                                </div>
                            </div> 
                        </div>
                    <?php } else { ?>

                        <div class="cli-ent-xbox">
                            <a href="#"  class="upl-oadfi-le noborder permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign" data-customerid="<?php echo $data[$i]['id']; ?>">
                                <span class="icon-crss-3">
                                    <span class="icon-circlxx55 margin5">+</span>
                                    <p class="attachfile">Assign designer<br> permanently</p>
                                </span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="viewinfo<?php echo $data[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="cli-ent-model-box">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="cli-ent-model">
                                    <header class="fo-rm-header">
                                        <h3 class="head-c">View Information</h3>
                                    </header>
                                    <div class="fo-rm-body">
                                        <div class="projects">
                                            <ul class="projects_info_list">
                                                <li>
                                                    <div class="starus"><b>Active Projects</b></div>
                                                    <div class="value"><?php echo $data[$i]['active']; ?>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="starus"><b>In -Queue Projects</b></div>
                                                    <div class="value"><?php echo $data[$i]['inque_request']; ?>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="starus"><b>Revision Projects</b></div>
                                                    <div class="value"><?php echo $data[$i]['revision_request']; ?>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="starus"><b>Review Projects</b></div>
                                                    <div class="value"><?php echo $data[$i]['review_request']; ?>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="starus"><b>Complete Projects</b></div>
                                                    <div class="value"><?php echo $data[$i]['complete_request']; ?>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="<?php echo $data[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="cli-ent-model-box">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="cli-ent-model">
                                    <header class="fo-rm-header">
                                        <h3 class="head-c">Edit Client</h3>
                                    </header>
                                    <div class="fo-rm-body">
                                        <form onSubmit="return EditFormSQa(<?php echo $data[$i]['id']; ?>)" method="post" action="<?php echo base_url(); ?>admin/dashboard/edit_client/<?php echo $data[$i]['id']; ?>">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">First Name</label>
                                                        <p class="space-a"></p>
                                                        <input type="text" name="first_name" required class="efirstname form-control input-c" value="<?php echo ucwords($data[$i]['first_name']); ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Last Name</label>
                                                        <p class="space-a"></p>
                                                        <input type="text" name="last_name" required class="elastname form-control input-c" value="<?php echo ucwords($data[$i]['last_name']); ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Email</label>
                                                        <p class="space-a"></p>
                                                        <input type="email" name="email" required  class="eemailid form-control input-c" value="<?php echo $data[$i]['email']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Phone</label>
                                                        <p class="space-a"></p>
                                                        <input type="tel" name="phone" required class="ephone form-control input-c" value="<?php echo $data[$i]['phone']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="password"   class="form-control input-c epassword" >
                                            </div>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Confirm Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="confirm_password"  class="form-control input-c ecpassword" >
                                            </div>
                                            <div class="epassError" style="display: none;">
                                                <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                            </div>
                                            <!--<hr class="hr-b">
                                            <p class="space-b"></p>
                                            
                                             <div class="form-group goup-x1">
                                                    <label class="label-x3">Select payment Method</label>
                                                    <p class="space-a"></p>
                                                    <select class="form-control select-c">
                                                            <option>Credit/Debit Card.</option>
                                                            <option>Credit/Debit Card.</option>
                                                            <option>Credit/Debit Card.</option>
                                                    </select>
                                            </div>
                                            
                                            <div class="form-group goup-x1">
                                                    <label class="label-x3">Card Number</label>
                                                    <p class="space-a"></p>
                                                    <input type="text" class="form-control input-c" placeholder="xxxx-xxxx-xxxx" name="Card Number" required>
                                            </div>
                                            
                                            <div class="row">
                                                    <div class="col-sm-5">
                                                            <div class="form-group goup-x1">
                                                                    <label class="label-x3">Expiry Date</label>
                                                                    <p class="space-a"></p>
                                                                    <input type="text" name="Expiration Date" required class="form-control input-c" placeholder="22.05.2022">
                                                            </div>
                                                    </div>
                                                    
                                                    <div class="col-sm-3 col-md-offset-4">
                                                            <div class="form-group goup-x1">
                                                                    <label class="label-x3">CVC</label>
                                                                    <p class="space-a"></p>
                                                                    <input type="text" name="CVC" required class="form-control input-c" placeholder="xxx" maxlength="3">
                                                            </div>
                                                    </div>
                                            </div> -->

                                            <p class="space-b"></p>

                                            <p class="btn-x"><button type="submit" class="btn-g">Add Customer</button></p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->
            <?php
        }
    }

    public function search_ajax_designer() {
        $dataArray = $this->input->get('name');
        if ($dataArray != "") {
            $designers = $this->Request_model->get_all_designer_for_admin(array('keyword' => $dataArray), $_SESSION['user_id']);

            for ($i = 0; $i < sizeof($designers); $i++) {
                $designers[$i]['total_rating'] = $this->Request_model->get_average_rating($designers[$i]['id']);

                $user = $this->Account_model->get_all_request_by_designer($designers[$i]['id']);
                $designers[$i]['handled'] = sizeof($user);

                $user = $this->Account_model->get_all_active_request_by_designer($designers[$i]['id']);
                $designers[$i]['active_request'] = sizeof($user);
            }
        } else {
            $designers = $this->Account_model->getall_designer();
        }
        for ($i = 0; $i < sizeof($designers); $i++) {
            ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr"  style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 10%;">
                    <?php
                    if ($designers[$i]['profile_picture']) {
                        ?>
                        <figure class="cli-ent-img circle one">
                            <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $designers[$i]['profile_picture']; ?>" class="img-responsive one">
                        </figure>
                    <?php } else { ?>
                        <figure class="cli-ent-img circle one">
                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive one">
                        </figure>
                    <?php } ?>
                </div>
                <div class="cli-ent-col td" style="width: 35%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">

                            <div class="cell-col">
                                <h3 class="pro-head space-b">
                                    <a href="<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>"><?php echo $designers[$i]['first_name'] . " " . $designers[$i]['last_name'] ?></a>

                                    <a href="<?php echo base_url(); ?>admin/accounts/view_designer_profile/<?php echo $designers[$i]['id']; ?>">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" data-toggle="modal" data-target="#<?php echo $designers[$i]['id']; ?>">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>
                                    <?php if ($designers[$i]['disable_designer'] == 1) { ?>
                                        <a title="Enable" id="enable<?php echo $designers[$i]['id']; ?>" href="#" data-toggle="modal" data-target="#confirmation" data-status="enable" class="confirmation" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                        </a>
                                        <a title="Disable" id="disable<?php echo $designers[$i]['id']; ?>" style="display: none;" href="#" data-toggle="modal" class="confirmation" data-status="disable" data-target="#confirmation" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                            <i class="fa fa-circle-o" aria-hidden="true"></i>
                                        </a>
                                    <?php } else { ?>
                                        <a title="Enable" id="enable<?php echo $designers[$i]['id']; ?>" style="display: none;" href="#" data-toggle="modal" data-target="#confirmation" data-status="enable" class="confirmation" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                        </a>
                                        <a title="Disable" id="disable<?php echo $designers[$i]['id']; ?>" href="#" data-toggle="modal" class="confirmation" data-status="disable" data-target="#confirmation" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                            <i class="fa fa-circle-o" aria-hidden="true"></i>
                                        </a>
                                    <?php } ?>
                                    <a title="delete" id="delete<?php echo $designers[$i]['id']; ?>" href="#" data-toggle="modal" class="confirmationdelete" data-target="#confirmationdelete" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
                                    <p class="pro-b"><?php echo $designers[$i]['email']; ?></p>
                                </h3>
                                <p class="pro-b"><?php echo $designers[$i]['about_me']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 15%;"onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Sign Up Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b"><?php echo date("M /d /Y", strtotime($designers[$i]['created'])) ?></p>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 20%;"onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Requests Being Handled</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="green text-uppercase"><?php echo $designers[$i]['handled']; ?></span></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 20%;"onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active Requests</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $designers[$i]['active_request']; ?></span></p>
                    </div>
                </div>
                <!-- Edit Designer Modal -->
                <div class="modal fade" id="<?php echo $designers[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="cli-ent-model-box">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="cli-ent-model">
                                    <header class="fo-rm-header">
                                        <h3 class="head-c">Edit Designer</h3>
                                    </header>
                                    <div class="fo-rm-body">
                                        <form onSubmit="return EditFormSQa(<?php echo $designers[$i]['id']; ?>)" action="<?php echo base_url(); ?>admin/dashboard/edit_designer/<?php echo $designers[$i]['id']; ?>" method="post">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">First Name</label>
                                                        <p class="space-a"></p>
                                                        <input name="first_name" type="text" required class="form-control input-c efirstname" value="<?php echo ucwords($designers[$i]['first_name']); ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Last Name</label>
                                                        <p class="space-a"></p>
                                                        <input type="text" name="last_name" required  class="form-control input-c elastname" value="<?php echo ucwords($designers[$i]['last_name']); ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Email</label>
                                                        <p class="space-a"></p>
                                                        <input type="email" name="email" required class="form-control input-c eemailid" value="<?php echo $designers[$i]['email']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Phone</label>
                                                        <p class="space-a"></p>
                                                        <input type="tel" name="phone" required class="form-control input-c ephone" value="<?php echo $designers[$i]['phone']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <hr class="hr-b">

                                            <p class="space-c"></p>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="password"   class="form-control input-c epassword">
                                            </div>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Confirm Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="confirm_password"  class="form-control input-c ecpassword">
                                            </div>
                                            <div class="epassError" style="display: none;">
                                                <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                            </div>
                                            <p class="space-b"></p>

                                            <p class="btn-x"><button type="submit" class="btn-g">Add Designer</button></p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Row -->
            <?php
        }
    }

    public function search_ajax_customer_project() {
        $dataArray = $this->input->get('title');
        $dataStatus = $this->input->get('status');
        $datauserid = $this->input->get('id');
        $dataStatus = explode(",", $dataStatus);
        $rows = $this->Request_model->GetAutocomplete_for_admin(array('keyword' => $dataArray), $dataStatus, $datauserid, "admin");
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rows[$i]->total_chat = $this->Request_model->get_chat_number($rows[$i]->id, $rows[$i]->customer_id, $rows[$i]->designer_id, "admin");
            $rows[$i]->total_chat_all = $this->Request_model->get_total_chat_number($rows[$i]->id, $_SESSION['user_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($rows[$i]->id, "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            if ($rows[$i]->status == "active" || $rows[$i]->status == "disapprove") {
                $rows[$i]->expected = $this->myfunctions->check_timezone($rows[$i]->latest_update);
            } elseif ($rows[$i]->status == "checkforapprove") {
                //$rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->latest_update);
                $rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->modified);
            } elseif ($rows[$i]->status == "approved") {
                $rows[$i]->approvddate = $this->onlytimezone($rows[$i]->approvaldate);
            }
            if ($rows[$i]->status_admin == "pendingrevision") {
                $rows[$i]->revisiondate = $this->onlytimezone($rows[$i]->modified);
            }
            $rows[$i]->comment_count = $commentcount;
            $rows[$i]->total_files = $this->Request_model->get_files_count($rows[$i]->id, $rows[$i]->designer_id, "admin");
            $rows[$i]->total_files_count = $this->Request_model->get_files_count_all($rows[$i]->id);
        }
        for ($i = 0; $i < sizeof($rows); $i++) {
            if ($rows[$i]->status == "disapprove" || $rows[$i]->status == "active" || $rows[$i]->status == "assign") {
                ?>
                <!--QA Active Section Start Here -->
                <div class="tab-pane active content-datatable datatable-width" id="Qa_client_active" role="tabpanel">
                    <?php
                    for ($i = 0; $i < sizeof($rows); $i++) {
                        if ($rows[$i]->status == "disapprove" || $rows[$i]->status == "active") {
                            ?>
                            <!-- Start Row -->
                            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'" style="cursor: pointer;">
                                <div class="cli-ent-col td" style="width: 12%;">
                                    <div class="cli-ent-xbox">
                                        <p class="pro-a">
                                            <?php
                                            if ($rows[$i]->status == "active") {
                                                echo "Expected on";
                                            } elseif ($rows[$i]->status == "disapprove") {
                                                echo "Expected on";
                                            } elseif ($rows[$i]->status == "assign") {
                                                echo "In-Queue";
                                            }
                                            ?>
                                        </p>
                                        <p class="space-a"></p>
                                        <p class="pro-b">
                                            <?php
                                            if ($rows[$i]->status == "active") {
                                                echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                            } elseif ($rows[$i]->status == "disapprove") {
                                                echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 48%;">
                                    <div class="cli-ent-xbox text-left">
                                        <div class="cell-row">

                                            <div class="cell-col" >
                                                <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1"><?php echo $rows[$i]->title; ?></a></h3>
                                                <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 50); ?></p>
                                            </div>

                                            <div class="cell-col col-w1">
                                                <p class="neft text-center">
                                                    <?php if ($rows[$i]->status == "active" || $rows[$i]->status == "checkforapprove") { ?>
                                                        <span class="green text-uppercase">In-Progress</span>
                                                    <?php } elseif ($rows[$i]->status == "disapprove") { ?>
                                                        <span class="red orangetext text-uppercase">REVISION</span>
                                                    <?php } elseif ($rows[$i]->status == "assign") { ?>
                                                        <span class="gray text-uppercase">In-Queue</span>
                                                    <?php } ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 20%;">
                                    <div class="cli-ent-xbox text-left p-left1">
                                        <div class="cell-row">
                                            <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                                <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1">
                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                        <figure class="pro-circle-img">
                                                            <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $rows[$i]->profile_picture; ?>" class="img-responsive">
                                                        </figure>
                                                    <?php } else { ?>
                                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                            <?php echo ucwords(substr($rows[$i]->designer_first_name, 0, 1)) . ucwords(substr($rows[$i]->designer_last_name, 0, 1)); ?>
                                                        </figure>
                                                    <?php } ?>
                                                </a>
                                            </div>
                                            <div class="cell-col">
                                                <p class="text-h">
                                                    <?php
                                                    if ($rows[$i]->designer_first_name) {
                                                        echo $rows[$i]->designer_first_name;
                                                    } else {
                                                        echo "No Designer";
                                                    }
                                                    ?>  
                                                </p>
                                                <p class="pro-b">Designer</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1">
                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                                    <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                        <span class="numcircle-box">
                                                            <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                        </span>
                                                    <?php } ?></span>
                                                <?php //echo $rows[$i]->total_chat_all; ?></a></p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1">
                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                        <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                                <?php } ?>
                                                    <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                            <?php
                        }
                        if ($rows[$i]->status == "assign") {
                            ?>
                            <!-- Start Row -->
                            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'" style="cursor: pointer;">
                                <div class="cli-ent-col td" style="width: 12%;">
                                    <div class="cli-ent-xbox">
                                        <p class="pro-a">
                                            <?php
                                            if ($rows[$i]->status == "active") {
                                                echo "Expected on";
                                            } elseif ($rows[$i]->status == "disapprove") {
                                                echo "Expected on";
                                            } elseif ($rows[$i]->status == "assign") {
                                                echo "In-Queue";
                                            }
                                            ?>
                                        </p>
                                        <p class="space-a"></p>
                                        <p class="pro-b">
                                            <?php
                                            if ($rows[$i]->status == "active") {
                                                echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                            } elseif ($rows[$i]->status == "disapprove") {
                                                echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 48%;">
                                    <div class="cli-ent-xbox text-left">
                                        <div class="cell-row">

                                            <div class="cell-col" >
                                                <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1"><?php echo $rows[$i]->title; ?></a></h3>
                                                <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 50); ?></p>
                                            </div>
                        <!--                                            <div class="cell-col"><?php //echo isset($rows[$i]->priority) ? $rows[$i]->priority : '';   ?></div>-->

                                            <div class="cell-col col-w1">
                                                <p class="neft text-center">
                                                    <?php if ($rows[$i]->status == "active" || $rows[$i]->status == "checkforapprove") { ?>
                                                        <span class="green text-uppercase">In-Progress</span>
                                                    <?php } elseif ($rows[$i]->status == "disapprove") { ?>
                                                        <span class="red orangetext text-uppercase">REVISION</span>
                                                    <?php } elseif ($rows[$i]->status == "assign") { ?>
                                                        <span class="gray text-uppercase">In-Queue</span>
                                                    <?php } ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 20%;">
                                    <div class="cli-ent-xbox text-left p-left1">
                                        <div class="cell-row">
                                            <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                                <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1">
                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                        <figure class="pro-circle-img">
                                                            <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $rows[$i]->profile_picture; ?>" class="img-responsive">
                                                        </figure>
                                                    <?php } else { ?>
                                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                            <?php echo ucwords(substr($rows[$i]->designer_first_name, 0, 1)) . ucwords(substr($rows[$i]->designer_last_name, 0, 1)); ?>
                                                        </figure>
                                                    <?php } ?>
                                                </a>
                                            </div>
                                            <div class="cell-col">
                                                <p class="text-h">
                                                    <?php
                                                    if ($rows[$i]->designer_first_name) {
                                                        echo $rows[$i]->designer_first_name;
                                                    } else {
                                                        echo "No Designer";
                                                    }
                                                    ?>  
                                                </p>
                                                <p class="pro-b">Designer</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1">
                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                                    <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                        <span class="numcircle-box">
                                                            <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                        </span>
                                                    <?php } ?></span>
                                                <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1">
                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                        <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                                <?php } ?>
                                                    <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                            <?php
                        }
                    }
                    ?>
                </div>
                <!--QA Active Section End Here -->
            <?php } elseif ($rows[$i]->status == "checkforapprove") { ?>
                <!--QA Pending Approval Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_client_pending" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) {
                        ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'"  style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">
                                        Delivered on
                                    </p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rows[$i]->reviewdate));
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col">
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 50); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center">
                                                <span class="red bluetext text-uppercase text-wrap">Pending-approval</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $rows[$i]->customer_profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rows[$i]->customer_first_name, 0, 1)) . ucwords(substr($rows[$i]->customer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                <?php echo $rows[$i]->customer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->customer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->customer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $rows[$i]->profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rows[$i]->designer_first_name, 0, 1)) . ucwords(substr($rows[$i]->designer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span>
                                            <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                            <?php } ?>
                                                <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Pending Approval Section End Here -->
            <?php } elseif ($rows[$i]->status == "approved") { ?>
                <!--QA Completed Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_client_completed" role="tabpanel">
                    <?php
                    for ($i = 0; $i < sizeof($rows); $i++) {
                        // echo "<pre>";
                        // print_r($rows);
                        // echo "</pre>";
                        ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 13%;">
                                <div class="cli-ent-xbox">
                                    <h3 class="app-roved green">Approved on</h3>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rows[$i]->approvddate));
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 37%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col">
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 50); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $rows[$i]->customer_profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rows[$i]->customer_first_name, 0, 1)) . ucwords(substr($rows[$i]->customer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title=" <?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                <?php echo $rows[$i]->customer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->customer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->customer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $rows[$i]->profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rows[$i]->designer_first_name, 0, 1)) . ucwords(substr($rows[$i]->designer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span>
                                            <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                            <?php } ?>
                                                <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div><!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Completed Section End Here -->
                <?php
            }
        }
    }

    public function search_ajax_request() {
        //print_r($this->input->get());
        $dataArray = $this->input->get('title');
        $dataStatus = $this->input->get('status');
        $dataStatus = explode(",", $dataStatus);
        $rows = $this->Request_model->GetAutocomplete_admin(array('keyword' => $dataArray), $dataStatus, "");
        $count_active_project = $this->Admin_model->count_active_project(array("active", "disapprove"), "", "", "", "", "status_admin");
        // echo "<pre>";print_r($rows);
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rows[$i]->total_chat = $this->Request_model->get_chat_number($rows[$i]->id, $rows[$i]->customer_id, $rows[$i]->designer_id, "admin");
            $rows[$i]->total_chat_all = $this->Request_model->get_total_chat_number($rows[$i]->id, $_SESSION['user_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($rows[$i]->id, "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            if ($rows[$i]->status_admin == "active" || $rows[$i]->status_admin == "disapprove") {
                $rows[$i]->expected = $this->myfunctions->check_timezone($rows[$i]->latest_update);
            } elseif ($rows[$i]->status_admin == "checkforapprove") {
                $rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->modified);
            } elseif ($rows[$i]->status_admin == "approved") {
                $rows[$i]->approvddate = $this->onlytimezone($rows[$i]->approvaldate);
            } elseif ($rows[$i]->status_admin == "pendingrevision") {
                $rows[$i]->revisiondate = $this->onlytimezone($rows[$i]->modified);
            }
            $rows[$i]->designer_project_count = $this->Request_model->get_designer_active_request($rows[$i]->designer_id);
            $rows[$i]->comment_count = $commentcount;
            $rows[$i]->total_files = $this->Request_model->get_files_count($rows[$i]->id, $rows[$i]->designer_id, "admin");
            $rows[$i]->total_files_count = $this->Request_model->get_files_count_all($rows[$i]->id);
        }
        for ($i = 0; $i < sizeof($rows); $i++) {
            // For Active Tab
            if ($rows[$i]->status_admin == "disapprove" || $rows[$i]->status_admin == "active") {
                ?>
                <!--QA Active Section Start Here -->
                <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 12%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">
                                    <?php
                                    if ($rows[$i]->status == "active") {
                                        echo "Expected on";
                                    } elseif ($rows[$i]->status_admin == "disapprove") {
                                        echo "Expected on";
                                    }
                                    ?>
                                </p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php
                                    if ($rows[$i]->status == "active") {
                                        echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                    } elseif ($rows[$i]->status_admin == "disapprove") {
                                        echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 34%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1"><?php echo $rows[$i]->title; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                    </div>

                                    <div class="cell-col col-w1">
                                        <p class="neft text-center">
                                            <?php if ($rows[$i]->status_admin == "active") { ?>
                                                <span class="green text-uppercase text-wrap">In-Progress</span>
                                            <?php } elseif ($rows[$i]->status_admin == "disapprove" && $rows[$i]->who_reject == 1) { ?>
                                                <span class="red orangetext text-uppercase text-wrap">REVISION</span>
                                            <?php } elseif ($rows[$i]->status_admin == "disapprove" && $rows[$i]->who_reject == 0) { ?>
                                                <span class="red text-uppercase text-wrap">Quality Revision</span>
                                            <?php } ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#" >
                                            <figure class="pro-circle-img">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                           ">
                                               <?php echo $rows[$i]->customer_first_name; ?>
                                               <?php
                                               if (strlen($rows[$i]->customer_last_name) > 5) {
                                                   echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                               } else {
                                                   echo $rows[$i]->customer_last_name;
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 20%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <span class="count_project"><?php echo $rows[$i]->designer_project_count; ?></span>
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#" class="<?php echo $rows[$i]->id; ?>">
                                            <figure class="pro-circle-img">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                        <p class="text-h text-wrap"title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>
                                           ">
                                               <?php echo $rows[$i]->designer_first_name; ?>
                                               <?php
                                               if (strlen($rows[$i]->designer_first_name) > 5) {
                                                   echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                               } else {
                                                   echo $rows[$i]->designer_last_name;
                                               }
                                               ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <?php
                                        if ($rows[$i]->status == "active") {
                                            if ($rows[$i]->designer_skills_matched == 0) {
                                                ?>
                                                <div class="checkbox custom-checkbox mob-txt-center  text-left " id="<?php echo $rows[$i]->id; ?>">
                                                    <label>
                                                        <input type="checkbox" class="checkcorrect" value="1" data-request="<?php echo $rows[$i]->id; ?>" onchange="check(this.value, '<?php echo $rows[$i]->id; ?>');"> 
                                                        <span class="cr  right-cr   ">
                                                            <i class="cr-icon fa fa-check"></i>
                                                        </span> <br>
                                                    </label>
                                                </div>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rows[$i]->id; ?>" data-designerid= "<?php echo $rows[$i]->designer_id; ?>">
                                            <span class="sma-red">+</span> Add Designer
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                </span>
                                            <?php } ?>
                                        </span>
                                        <?php //echo $rows[$i]->total_chat_all;   ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                            <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                            </div>
                        </div>
                    </div>
                    <!-- End Row -->
                <?php } ?>
<!--                    <input type="hidden" id="row" value="0">
                    <input type="hidden" id="all" value="<?php //echo $count_active_project; ?>">
                    <div class="" style="display:block;text-align:center">
                        <a href="javascript:void(0)" class="load_more button">Load more</a>
                    </div>-->
                <!--QA Active Section End Here --> 
                <?php
            }
            
            elseif ($rows[$i]->status_admin == "assign") {
                ?>
                <!--QA IN Queue Section Start Here -->
                <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">In Queue</p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 36%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2"><?php echo $rows[$i]->title; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                    </div>
                                    <div class="cell-col"><?php echo isset($rows[$i]->priority) ? $rows[$i]->priority : ''; ?></div>
                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="gray text-uppercase">IN-queue</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#" >
                                            <figure class="pro-circle-img">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                           ">
                                               <?php echo $rows[$i]->customer_first_name; ?>
                                               <?php
                                               if (strlen($rows[$i]->customer_last_name) > 5) {
                                                   echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                               } else {
                                                   echo $rows[$i]->customer_last_name;
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 19%;">
                            <div class="cli-ent-xbox text-left p-left1">
                                <div class="cell-row">
                                    <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $rows[$i]->total_chat_all;   ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                            <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                        <?php echo count($rows[$i]->total_files_count); ?>
                                    </a></p>
                            </div>
                        </div>
                    </div> <!-- End Row -->
                <?php } ?>   
                <!--QA IN Queue Section End Here --> 

            <?php } elseif ($rows[$i]->status_admin == "pendingrevision") {
                ?>
                <!--QA Pending Review Section Start Here -->
                <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">Expected on</p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($rows[$i]->revisiondate));
                                    ?>
                                </p>
                                <!-- <p class="pro-b">May 30, 2018</p> -->
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 30%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3"><?php echo $rows[$i]->title; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                    </div>

                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="lightbluetext text-uppercase text-wrap">Pending Review</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#"><figure class="pro-circle-img">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure></a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                           ">
                                               <?php echo $rows[$i]->customer_first_name; ?>
                                               <?php
                                               if (strlen($rows[$i]->customer_last_name) > 5) {
                                                   echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                               } else {
                                                   echo $rows[$i]->customer_last_name;
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <span class="count_project"><?php echo $rows[$i]->designer_project_count; ?></span>
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#" class="<?php echo $rows[$i]->id; ?>"><figure class="pro-circle-img">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure></a>
                                    </div>
                                    <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                            <?php echo $rows[$i]->designer_first_name; ?>
                                            <?php
                                            if (strlen($rows[$i]->designer_last_name) > 5) {
                                                echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                            } else {
                                                echo $rows[$i]->designer_last_name;
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <?php if ($rows[$i]->designer_assign_or_not == 0) { ?>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rows[$i]->id; ?>" data-designerid= "<?php echo $rows[$i]->designer_id; ?>">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                </span>
                                            <?php } ?></span></span>
                                        <?php //echo $rows[$i]->total_chat_all;    ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                            </div>
                        </div>
                    </div> 
                    <!-- End Row -->
                <?php } ?>
                <!--QA Pending Review Section End Here -->
                <?php
                // End Active Tab
            } elseif ($rows[$i]->status_admin == "checkforapprove") {
                ?>
                <!--QA Pending Approval Section Start Here -->
                <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4'">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">Delivered on</p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($rows[$i]->reviewdate));
                                    ?>
                                </p>
                                <!-- <p class="pro-b">May 30, 2018</p> -->
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 34%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4"><?php echo $rows[$i]->title; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                    </div>

                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="red bluetext text-uppercase text-wrap">Pending Approval</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#"><figure class="pro-circle-img">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure></a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                           ">
                                               <?php echo $rows[$i]->customer_first_name; ?>
                                               <?php
                                               if (strlen($rows[$i]->customer_last_name) > 5) {
                                                   echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                               } else {
                                                   echo $rows[$i]->customer_last_name;
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <span class="count_project"><?php echo $rows[$i]->designer_project_count; ?></span>
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#" class="<?php echo $rows[$i]->id; ?>"><figure class="pro-circle-img">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure></a>
                                    </div>
                                    <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                            <?php echo $rows[$i]->designer_first_name; ?>
                                            <?php
                                            if (strlen($rows[$i]->designer_last_name) > 5) {
                                                echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                            } else {
                                                echo $rows[$i]->designer_last_name;
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <?php if ($rows[$i]->designer_assign_or_not == 0) { ?>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rows[$i]->id; ?>" data-designerid= "<?php echo $rows[$i]->designer_id; ?>">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $rows[$i]->total_chat_all;   ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/3'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rows[$i]->total_files) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                            </div>
                        </div>
                    </div> 
                    <!-- End Row -->
                <?php } ?>
                <!--QA Pending Approval Section End Here -->
            <?php } elseif ($rows[$i]->status_admin == "approved") { ?>
                <!--QA Completed Section Start Here -->
                <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5'"style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 13%;">
                            <div class="cli-ent-xbox">
                                <h3 class="app-roved green">Approved on</h3>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($rows[$i]->approvddate));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 37%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5"><?php echo $rows[$i]->title; ?></a></h3>
                                        <p class="pro-b">
                                            <?php echo substr($rows[$i]->description, 0, 30); ?>
                                        </p>
                                    </div>

                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#"><figure class="pro-circle-img">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure></a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                            <?php echo $rows[$i]->customer_first_name; ?>
                                            <?php
                                            if (strlen($rows[$i]->customer_last_name) > 5) {
                                                echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                            } else {
                                                echo $rows[$i]->customer_last_name;
                                            }
                                            ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <span class="count_project"><?php echo $rows[$i]->designer_project_count; ?></span>
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#"  class="<?php echo $rows[$i]->id; ?>">
                                            <figure class="pro-circle-img">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?> ">
                                            <?php echo $rows[$i]->designer_first_name; ?>
                                            <?php
                                            if (strlen($rows[$i]->designer_last_name) > 5) {
                                                echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                            } else {
                                                echo $rows[$i]->designer_last_name;
                                            }
                                            ?>

                                        </p>
                                        <p class="pro-b">Designer</p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $rows[$i]->total_chat_all;   ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/5">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rows[$i]->total_files) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                            </div>
                        </div>
                    </div> <!-- End Row -->
                <?php } ?>
                <!--QA Completed Section End Here -->
                <?php
            }
        }
    }

    public function search_ajax_qa() {
        $dataArray = $this->input->get('name');
        if ($dataArray != "") {
            $qa = $this->Request_model->get_qa_member(array('keyword' => $dataArray));
            /* new */
            for ($i = 0; $i < sizeof($qa); $i++) {
                $designers = $this->Request_model->get_designer_list_for_qa("array", $qa[$i]['id']);
                $qa[$i]['total_designer'] = sizeof($designers);

                $mydesigner = $this->Request_model->get_designer_list_for_qa("", $qa[$i]['id']);


                if ($mydesigner) {
                    $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                    $qa[$i]['total_clients'] = sizeof($data);
                } else {
                    $qa[$i]['total_clients'] = 0;
                }
            }
        } else {
            $qa = $this->Account_model->getqa_member();
            /* new */
            for ($i = 0; $i < sizeof($qa); $i++) {
                $designers = $this->Request_model->get_designer_list_for_qa("array", $qa[$i]['id']);
                $qa[$i]['total_designer'] = sizeof($designers);

                $mydesigner = $this->Request_model->get_designer_list_for_qa("", $qa[$i]['id']);


                if ($mydesigner) {
                    $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                    $qa[$i]['total_clients'] = sizeof($data);
                } else {
                    $qa[$i]['total_clients'] = 0;
                }
            }
        }
        ?>
        <?php for ($i = 0; $i < sizeof($qa); $i++): ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr">

                <div class="cli-ent-col td" style="width: 65%;">
                    <div class="cell-row">
                        <div class="cell-col" style="padding-right: 15px; width: 100px;">
                            <?php if ($qa[$i]['profile_picture'] != "") { ?>
                                <figure class="cli-ent-img circle one">
                                    <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $qa[$i]['profile_picture']; ?>" class="img-responsive">
                                </figure>
                            <?php } else { ?>
                                <figure class="cli-ent-img circle one">
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                </figure>
                            <?php } ?>
                        </div>

                        <div class="cell-col">
                            <h3 class="pro-head">
                                <a href="javascript:void(0)"><?php echo ucwords($qa[$i]['first_name'] . " " . $qa[$i]['last_name']); ?></a>
                                <a href="<?php echo base_url(); ?>admin/accounts/view_qa_profile/<?php echo $qa[$i]['id']; ?>">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </a>
                                <a href="#" data-toggle="modal" data-target="#<?php echo $qa[$i]['id']; ?>">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                            </h3>
                            <p class="text-a">Member Since <?php echo date("F Y", strtotime($qa[$i]['created'])); ?></p>
                        </div>

                        <!--<div class="cell-col col-w1">
                                <p class="neft"><span class="blue text-uppercase">Premium Member</span></p>
                        </div>-->
                    </div>

                </div>
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">No. of Designers</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="green text-uppercase"><?php echo $qa[$i]['total_clients']; ?></span></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active Requests</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $qa[$i]['total_designer']; ?></span></p>
                    </div>
                </div>
                <!-- Edit QA Modal -->
                <div class="modal fade" id="<?php echo $qa[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="cli-ent-model-box">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <form method="post" onSubmit="return EditFormSQa(<?php echo $qa[$i]['id']; ?>)" action="<?php echo base_url(); ?>admin/accounts/edit_qa/<?php echo $qa[$i]['id']; ?>">
                                    <div class="cli-ent-model">
                                        <header class="fo-rm-header">
                                            <h3 class="head-c">Edit QA</h3>
                                        </header>
                                        <div class="fo-rm-body">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">First Name</label>
                                                        <p class="space-a"></p>
                                                        <input name="first_name" type="text" class="form-control input-c efirstname" value="<?php echo $qa[$i]['first_name']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Last Name</label>
                                                        <p class="space-a"></p>
                                                        <input type="text" name="last_name" class="form-control input-c elastname" value="<?php echo $qa[$i]['last_name']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Email</label>
                                                        <p class="space-a"></p>
                                                        <input type="email" name="email" class="form-control input-c eemailid" value="<?php echo $qa[$i]['email']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Phone</label>
                                                        <p class="space-a"></p>
                                                        <input type="tel" name="phone"  class="form-control input-c ephone" value="<?php echo $qa[$i]['phone']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <hr class="hr-b">

                                            <p class="space-c"></p>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="password" class="form-control input-c epassword">
                                            </div>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Confirm Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="confirm_password"  class="form-control input-c ecpassword">
                                            </div>
                                            <div class="epassError" style="display: none;">
                                                <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                            </div>
                                            <p class="space-b"></p>

                                            <p class="btn-x"><button  type="submit" class="btn-g">Add Person</button></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Row -->
            <?php
        endfor;
    }

    public function search_ajax_va() {
        $dataArray = $this->input->get('name');
        if ($dataArray != "") {
            $va = $this->Request_model->get_va_member(array('keyword' => $dataArray));
            /* new */
            for ($i = 0; $i < sizeof($va); $i++) {
                $designers = $this->Request_model->get_designer_list_for_qa("array", $va[$i]['id']);
                $va[$i]['total_designer'] = sizeof($designers);

                $mydesigner = $this->Request_model->get_designer_list_for_qa("", $va[$i]['id']);


                if ($mydesigner) {
                    $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                    $va[$i]['total_clients'] = sizeof($data);
                } else {
                    $va[$i]['total_clients'] = 0;
                }
            }
        } else {
            $va = $this->Account_model->getva_member();
            /* new */
            for ($i = 0; $i < sizeof($va); $i++) {
                $designers = $this->Request_model->get_designer_list_for_qa("array", $va[$i]['id']);
                $va[$i]['total_designer'] = sizeof($designers);

                $mydesignerr = $this->Request_model->get_designer_list_for_qa("", $va[$i]['id']);


                if ($mydesignerr) {
                    $data = $this->Request_model->get_customer_list_for_qa("array", $mydesignerr);
                    $va[$i]['total_clients'] = sizeof($data);
                } else {
                    $va[$i]['total_clients'] = 0;
                }
            }
        }
        ?>
        <?php for ($i = 0; $i < sizeof($va); $i++): ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr">

                <div class="cli-ent-col td" style="width: 65%;">
                    <div class="cell-row">
                        <div class="cell-col" style="padding-right: 15px; width: 100px;">
                            <?php if ($va[$i]['profile_picture'] != "") { ?>
                                <figure class="cli-ent-img circle one">
                                    <img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $va[$i]['profile_picture']; ?>" class="img-responsive">
                                </figure>
                            <?php } else { ?>
                                <figure class="cli-ent-img circle one">
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                </figure>
                            <?php } ?>
                        </div>

                        <div class="cell-col">
                            <h3 class="pro-head">
                                <a href="javascript:void(0)"><?php echo ucwords($va[$i]['first_name'] . " " . $va[$i]['last_name']); ?></a>
                                <a href="#" data-toggle="modal" data-target="#<?php echo $va[$i]['id']; ?>">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                            </h3>
                            <p class="text-a">Member Since <?php echo date("F Y", strtotime($va[$i]['created'])); ?></p>
                        </div>

                        <!--<div class="cell-col col-w1">
                                <p class="neft"><span class="blue text-uppercase">Premium Member</span></p>
                        </div>-->
                    </div>

                </div>
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">No. of Designers</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="green text-uppercase"><?php echo $va[$i]['total_clients']; ?></span></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active Requests</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $va[$i]['total_designer']; ?></span></p>
                    </div>
                </div>
                <!-- Edit VA Modal -->
                <div class="modal fade" id="<?php echo $va[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="cli-ent-model-box">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <form onSubmit="return EditFormSQa(<?php echo $va[$i]['id']; ?>)" method="post" action="<?php echo base_url(); ?>admin/accounts/edit_va/<?php echo $va[$i]['id']; ?>">
                                    <div class="cli-ent-model">
                                        <header class="fo-rm-header">
                                            <h3 class="head-c">Edit VA</h3>
                                        </header>
                                        <div class="fo-rm-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">First Name</label>
                                                        <p class="space-a"></p>
                                                        <input name="first_name" type="text" class="form-control input-c efirstname" value="<?php echo $va[$i]['first_name']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Last Name</label>
                                                        <p class="space-a"></p>
                                                        <input type="text" name="last_name" class="form-control input-c elastname" value="<?php echo $va[$i]['last_name']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Email</label>
                                                        <p class="space-a"></p>
                                                        <input type="email" name="email" class="form-control input-c eemailid" value="<?php echo $va[$i]['email']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Phone</label>
                                                        <p class="space-a"></p>
                                                        <input type="tel" name="phone"  class="form-control input-c ephone" value="<?php echo $va[$i]['phone']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <hr class="hr-b">

                                            <p class="space-c"></p>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="password" class="form-control input-c epassword">
                                            </div>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Confirm Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="confirm_password"  class="form-control input-c ecpassword">
                                            </div>
                                            <div class="epassError" style="display: none;">
                                                <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                            </div>
                                            <p class="space-b"></p>

                                            <p class="btn-x"><button  type="submit" class="btn-g">Add Person</button></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Row -->
            <?php
        endfor;
    }

    public function request_response() {
        $this->checkloginuser();
        $this->Welcome_model->update_online();
        $request_data = $this->Request_model->get_request_by_id($_POST['request_id']);
        $email = $this->Request_model->getuserbyid($request_data[0]['customer_id']);
        $email_designer = $this->Request_model->getuserbyid($request_data[0]['designer_id']);
        $success = $this->Welcome_model->update_data("request_files", array("modified" => date("Y:m:d H:i:s"), "status" => $_POST['value']), array("id" => $_POST['fileid']));
        if ($success) {
            if ($_POST['value'] == "Reject") {
                $_POST['value'] = "disapprove";
                $this->Welcome_model->update_data("requests", array("status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "who_reject" => 0), array("id" => $_POST['request_id']));

                $notification_data = array("url" => "designer/request/project_info/" . $_POST['request_id'],
                    "title" => "Rejecte your design",
                    "user_id" => $request_data[0]['designer_id'],
                    "sender_id" => $_SESSION['user_id'],
                    "created" => date("Y-m-d H:i:s"));
                $this->Welcome_model->insert_data("notification", $notification_data);
                //$this->send_email_after_qa_approval_for_designer($_POST['fileid'], $_POST['request_id'], 'disapprove', 'admin', $email_designer[0]['email']);
            } 
            elseif ($_POST['value'] == "customerreview") {
                $_POST['value'] = "checkforapprove";
                if ($request_data[0]['status'] != 'approved') {
                    $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status" => $_POST['value'], "status_qa" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                } else {
                    $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status_qa" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                }
                $this->send_email_after_admin_approval($request_data[0]['title'], $request_data[0]['id'], $email[0]['email']);
               // $this->send_email_after_qa_approval_for_designer($_POST['fileid'], $_POST['request_id'], 'approve', 'admin', $email_designer[0]['email']);
                //$this->send_email_after_adminapprove($request_data[0]['title'], $request_data[0]['id'], $email[0]['email']);
                $request2 = $this->Admin_model->get_all_request_count(array('active'), $request_data[0]['customer_id']);
                $request3 = $this->Admin_model->get_all_request_count(array('checkforapprove', 'disapprove'), $request_data[0]['customer_id']);
                $customer_data = $this->Admin_model->getuser_data($request_data[0]['customer_id']);
                $updatepriority = $this->Request_model->getall_newrequest($request_data[0]['customer_id'], array('pending', 'assign'), "", "priority");
                if ((count($request2) < 1) && count($request3) < 3) {
                    $all_requests = $this->Request_model->getall_newrequest($request_data[0]['customer_id'], array('pending', 'assign'), "", "priority");
                    if (isset($all_requests[0]['id'])) {
                        $success = $this->Welcome_model->update_data("requests", array("status" => "active", "status_admin" => "active", "status_designer" => "active", "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $all_requests[0]['id']));
                        $this->Admin_model->update_priority_after_approved('1', $customer_data[0]['id']);
                        $datamsgauto = array(
                            "request_id" => $all_requests[0]['id'],
                            "sender_id" => $customer_data[0]['qa_id'],
                            "sender_type" => "qa",
                            "reciever_id" => $customer_data[0]['id'],
                            "reciever_type" => "customer",
                            "message" => "The designer is currently working on your project. Send messages here to share updates about the overall project. If you want to share comments about an individual design you can use the comments section within each design draft.",
                            "with_or_not_customer" => 1,
                            "admin_seen" => 1,
                            "qa_seen" => 1,
                            "designer_seen" => 1,
                            "created" => date("Y-m-d H:i:s"),
                        );
                        $this->Welcome_model->insert_data("request_discussions", $datamsgauto);
                        $this->send_email_when_inqueue_to_inprogress($all_requests[0]['title'], $all_requests[0]['id'], $all_requests[0]['customer_email']);
                    }
                }

                // $this->Admin_model->update_priority_after_approved_qa('1', $updatepriority[0]['priority']);
                $notification_data = array("url" => "designer/request/project_info/" . $_POST['request_id'],
                    "title" => "Approved your design",
                    "user_id" => $request_data[0]['designer_id'],
                    "sender_id" => $_SESSION['user_id'],
                    "created" => date("Y-m-d H:i:s"));
                $this->Welcome_model->insert_data("notification", $notification_data);
                $notification_data = array("url" => "customer/request/project_info/" . $_POST['request_id'],
                    "title" => "Upload New File. Please Check And Approve..!",
                    "user_id" => $request_data[0]['customer_id'],
                    "sender_id" => $_SESSION['user_id'],
                    "created" => date("Y-m-d H:i:s"));
                $this->Welcome_model->insert_data("notification", $notification_data);
            } else {
                $_POST['value'] = "approved";
                $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                $notification_data = array("url" => "designer/request/project_info/" . $_POST['request_id'],
                    "title" => "Your Design is Approved..!",
                    "user_id" => $request_data[0]['designer_id'],
                    "sender_id" => $_SESSION['user_id'],
                    "created" => date("Y-m-d H:i:s"));
                $this->Welcome_model->insert_data("notification", $notification_data);



                $request2 = $this->Admin_model->get_all_request_count(array("active"), $request_data[0]['customer_id']);

                if (sizeof($request2) <= 1) {
                    $all_requests = $this->Request_model->getall_request($request_data[0]['customer_id'], "'pending','assign'", "", "index_number");
                    if (isset($all_requests[0]['id'])) {
                        $success = $this->Welcome_model->update_data("requests", array("status" => "active", "status_admin" => "active", "status_designer" => "active", "dateinprogress" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $all_requests[0]['id']));
                    }
                }
            }
        }
        echo $success;
        exit;
    }

    public function delete_plan($id) {
        if ($id) {
            if ($this->Admin_model->delete_blog("subscription_plan", $id)) {
                $this->session->set_flashdata("success", "Plan Deleted Successfully..!");
            } else {
                $this->session->set_flashdata("error", "Plan Not Deleted Successfully..!");
            }
        }
        redirect(base_url() . "admin/dashboard/adminprofile?status=2");
    }

    public function edit_plan($id) {
        if (!empty($_POST)) {
            if ($this->Admin_model->update_plan("subscription_plan", $_POST, $id)) {
                $this->session->set_flashdata("success", "Plan Updated Successfully..!");
            } else {
                $this->session->set_flashdata("error", "Plan Not Updated Successfully..!");
            }
        }
        redirect(base_url() . "admin/dashboard/adminprofile?status=2");
    }

    public function onlytimezone($date_zone) {

        /* $tz = date_default_timezone_get();
          $stamp = strtotime($date_zone);
          date_default_timezone_set($_SESSION['timezone']);
          $nextdate = date('Y-m-d H:i:s', $stamp);
          date_default_timezone_set($tz); */
        $nextdate = $this->myfunctions->onlytimezoneforall($date_zone);           
        //$nextdate = $date_zone;
        return $nextdate;
    }

    public function assign_designer_for_customer() {
        $success_add = $this->Admin_model->update_data("users", array('designer_id' => $_POST['assign_designer']), array("id" => $_POST['customer_id']));
        $all_request = $this->Request_model->get_request_by_customer_id($_POST['customer_id']);
        foreach ($all_request as $request) {
            $request_id[] = $request['id'];
        }
        $this->Request_model->update_request_admin($request_id, array('designer_id' => $_POST['assign_designer']));
        if ($success_add) {
            $this->session->set_flashdata('message_success', "Assigned designer Successfully", 5);
            redirect(base_url() . "admin/dashboard/view_clients");
        }
    }

    public function change_project_status() {
        $request = $this->Request_model->get_request_by_id($_POST['request_id']);
        $customer_data = $this->Admin_model->getuser_data($request[0]['customer_id']);
        if ($request[0]['status'] == 'assign') {
            if (isset($_POST['value'])) {
                $deletedpriority = isset($request[0]['priority']) ? $request[0]['priority'] : '';
                $user_id = isset($request[0]['customer_id']) ? $request[0]['customer_id'] : '';
                $priorfrom = $deletedpriority + 1;
                if ($request[0]['latest_update'] == '' || $request[0]['latest_update'] == null) {
                    $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "latest_update" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                } else {
                    $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                }
                $this->Admin_model->update_priority_after_approved_qa(1, $priorfrom, $user_id);
            }
        }
        
        if ($_POST['value'] == "assign") {
            $priority = $this->Welcome_model->priority($request[0]['customer_id']);
            $data2['priority'] = $priority[0]['priority'] + 1;
            $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => $data2['priority'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
            if ($success_add) {
                echo "1";
            } else {
                echo "0";
            }
        } elseif ($_POST['value'] == "active") {
            $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
            $message_count = $this->Request_model->request_discussions_count_for_qa_message($_POST['request_id']);
            if($message_count == 0){
            $datamsgauto = array(
                                "request_id" => $_POST['request_id'],
                                "sender_id" => $customer_data[0]['qa_id'],
                                "sender_type" => "qa",
                                "reciever_id" => $request[0]['customer_id'],
                                "reciever_type" => "customer",
                                "message" => "The designer is currently working on your project. Send messages here to share updates about the overall project. If you want to share comments about an individual design you can use the comments section within each design draft.",
                                "with_or_not_customer" => 1,
                                "admin_seen" => 1,
                                "qa_seen" => 1,
                                "designer_seen" => 1,
                                 "created"=>date("Y-m-d H:i:s")
                            );
            $this->Welcome_model->insert_data("request_discussions", $datamsgauto);
            //$this->send_email_when_inqueue_to_inprogress($request[0]['title'], $request[0]['id'], $customer_data[0]['customer_email']);
            }
            if ($success_add) {
                //NOTE : i think below function will never run 
                // because project status already updated but in below function it is getting only assign status requests to update
                // $this->Admin_model->update_priority_after_approved_qa('1', $request[0]['priority']);
                echo "1";
            } else {
                echo "0";
            }
        } elseif ($_POST['value'] == "disapprove") {
            $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
            if ($success_add) {
                //NOTE : i think below function will never run 
                // because project status already updated but in below function it is getting only assign status requests to update
                // $this->Admin_model->update_priority_after_approved_qa('1', $request[0]['priority']);
                echo "1";
            } else {
                echo "0";
            }
        } elseif ($_POST['value'] == "approved") {
            $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "approvaldate" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
            if ($success_add) {
                //NOTE : i think below function will never run 
                // because project status already updated but in below function it is getting only assign status requests to update
                // $this->Admin_model->update_priority_after_approved_qa('1', $request[0]['priority']);
                echo "1";
            } else {
                echo "0";
            }
        } else {
            $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
            if ($success_add) {
                //NOTE : i think below function will never run 
                // because project status already updated but in below function it is getting only assign status requests to update
                // $this->Admin_model->update_priority_after_approved_qa('1', $request[0]['priority']);
                echo "1";
            } else {
                echo "0";
            }
        }
    }

    public function deletecustomer() {
        $all_request_id = array();
        $file_id = array();
        $all_requests = $this->Account_model->get_all_request_by_customer($_POST['id']);

        foreach ($all_requests as $request) {
            $all_request_id[] = $request['id'];
        }
        if (!empty($all_request_id)) {
            $this->Admin_model->delete_request_data("request_discussions", $all_request_id);

            $all_files = $this->Admin_model->get_all_files_rid($all_request_id);
            foreach ($all_files as $files) {
                $file_id[] = $files['id'];
            }
        }
        if (!empty($file_id)) {
            $this->Admin_model->delete_request_file_data("request_file_chat", $file_id);
            $this->Admin_model->delete_request_data("request_files", $all_request_id);
        }
        if (!empty($all_requests)) {
            foreach ($all_requests as $request_all) {
                $path = FCPATH . "public/uploads/requests/" . $request_all['id'] . "/*";
                $path1 = FCPATH . "public/uploads/requests/" . $request_all['id'];
                if (file_exists($path1)) {
                    $files = glob($path); // get all file names
                    foreach ($files as $file) { // iterate files
                        if (is_file($file))
                            unlink($file); // delete file
                    }
                    rmdir($path1);
                    $all_requests_id[] = $request_all['id'];
                }
            }
        }
        if (!empty($all_request_id)) {
            $this->Admin_model->alldelete_request('requests', $all_request_id);
        }
        $this->Admin_model->customer_delete($_POST['id']);
        echo "1";
    }

     public function send_email_after_admin_approval($project_title, $id, $email) {
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $query1 = $this->db->select('first_name')->from('users')->where('email', $email)->get();
        $fname = $query1->result();
        $data = array(
            'title' => $project_title,
            'project_id' => $id,
            'fname' => isset($fname[0]->first_name) ? $fname[0]->first_name : ''
        );
        $body = $this->load->view('emailtemplate/qa_approved_template', $data, TRUE);
        $receiver = $email;

        $subject = 'Design is Ready for Review.';

        $message = $body;

        $title = 'GraphicsZoo';

        SM_sendMail($receiver, $subject, $message, $title);
    }
    
    public function send_email_after_adminapprove($project_title, $id, $email) {
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $data = array(
            'title' => $project_title,
            'project_id' => $id
        );
        $body = $this->load->view('emailtemplate/qa_approved_template', $data, TRUE);
        $receiver = $email;

        $subject = 'Admin Approved';

        $message = $body;

        $title = 'GraphicsZoo';

        //SM_sendMail($receiver, $subject, $message, $title);


        //    $config = $this->config->item('email_smtp');
        // $from = $this->config->item('from');
        //    $this->load->library('email', $config);
        //    $this->email->set_newline("\r\n");
        //    $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        //    $this->email->set_header('Content-type', 'text/html');
        //    $this->email->from($from, 'GraphicsZoo');
        //    $data = array(
        //         'title'=> $project_title,
        //         'project_id' => $id
        //             );
        //    $this->email->to($email);  // replace it with receiver mail id
        //    $this->email->subject("Admin Approved"); // replace it with relevant subject 
        //    $body = $this->load->view('emailtemplate/qa_approved_template',$data,TRUE);
        //    $this->email->message($body);   
        //    $this->email->send();       
    }

    public function send_email_when_inqueue_to_inprogress($project_title, $id, $custemail) {
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $receiver = $custemail;
        $query1 = $this->db->select('first_name')->from('users')->where('email', $receiver)->get();
        $fname = $query1->result();
        $data = array(
            'title' => $project_title,
            'project_id' => $id,
            'fname' => isset($fname[0]->first_name) ? $fname[0]->first_name : ''
        );
        $body = $this->load->view('emailtemplate/inqueue_template', $data, TRUE);
        $subject = 'Designer has Started Working on your New Project.';

        $message = $body;

        $title = 'GraphicsZoo';

        //SM_sendMail($receiver, $subject, $message, $title);
    }

    public function send_email_after_qa_approval_for_designer($file_id, $project_id, $status, $approveby, $email) {
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $data = array(
            'file_id' => $file_id,
            'project_id' => $project_id,
            'status' => $status,
            'appby' => $approveby
        );
        $body = $this->load->view('emailtemplate/qa_approvel_for_designer_template', $data, TRUE);
        $receiver = $email;

        $subject = 'Admin approved your design';

        $message = $body;

        $title = 'GraphicsZoo';

        //SM_sendMail($receiver, $subject, $message, $title);


        //    $config = $this->config->item('email_smtp');
        // $from = $this->config->item('from');
        //    $this->load->library('email', $config);
        //    $this->email->set_newline("\r\n");
        //    $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        //    $this->email->set_header('Content-type', 'text/html');
        //    $this->email->from($from , 'GraphicsZoo');
        //    $this->email->to($email);  // replace it with receiver mail id
        //    $this->email->subject("Admin approved your design"); // replace it with relevant subject 
        //    $body = $this->load->view('emailtemplate/qa_approvel_for_designer_template',$data,TRUE);
        //    $this->email->message($body);   
        //    $this->email->send();      
    }

  public function load_more() {
        $count_load_more = '';
        $group_no = $this->input->post('group_no');
        $dataStatusText = $this->input->post('status');
        $status = explode(",", $dataStatusText);
        $search = $this->input->post('search');
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $count_load_more = $this->Request_model->admin_load_more($status, true,$start, $content_per_page, "",$search);
        if (is_array($status)) {
            $all_content = $this->Request_model->admin_load_more($status, "",$start, $content_per_page, "",$search);
        } else {
            $all_content = $this->Request_model->admin_load_more($status, "",$start, $content_per_page, "",$search);
        }
        for ($i = 0; $i < sizeof($all_content); $i++) {
            $all_content[$i]['total_chat'] = $this->Request_model->get_chat_number($all_content[$i]['id'], $all_content[$i]['customer_id'], $all_content[$i]['designer_id'], "admin");
            $all_content[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($all_content[$i]['id'], $_SESSION['user_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($all_content[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            if ($all_content[$i]['status_admin'] == "active" || $all_content[$i]['status'] == "disapprove") {
                $all_content[$i]['expected'] = $this->myfunctions->check_timezone($all_content[$i]['latest_update']);
            } elseif ($all_content[$i]['status_admin'] == "checkforapprove") {
                $all_content[$i]['reviewdate'] = $this->onlytimezone($all_content[$i]['modified']);
            } elseif ($all_content[$i]['status_admin'] == "approved") {
                $all_content[$i]['approvddate'] = $this->onlytimezone($all_content[$i]['approvaldate']);
            } elseif ($all_content[$i]['status_admin'] == "pendingrevision") {
                $all_content[$i]['revisiondate'] = $this->onlytimezone($all_content[$i]['modified']);
            }
            $all_content[$i]['designer_project_count'] = $this->Request_model->get_designer_active_request($all_content[$i]['designer_id']);
            $all_content[$i]['comment_count'] = $commentcount;
            $all_content[$i]['total_file'] = $this->Request_model->get_files_count($all_content[$i]['id'], $all_content[$i]['designer_id'], "admin");
            $all_content[$i]['total_files_count'] = $this->Request_model->get_files_count_all($all_content[$i]['id']);
        }
        if (isset($all_content) && is_array($all_content) && count($all_content)) {
            for ($i = 0; $i < count($all_content); $i++) {
                
                if ($all_content[$i]['status_admin'] == "active" || $all_content[$i]['status_admin'] == "disapprove") {
                    ?>
                    <div class="cli-ent-row tr brdr">
                        <div class="cli-ent-col td" style="width: 10%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/1'">
                            <?php if($all_content[$i]['is_trail'] == 1){ ?>
                                            <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                            <?php } ?>
                            <div class="cli-ent-xbox">
                                <p class="pro-a">Expected on</p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['expected']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 30%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/1'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/1"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                    <?php if ($all_content[$i]['status_admin'] == "active") { ?>
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="green text-uppercase text-wrap">In-Progress</span></p>
                                        </div>
                                    <?php } elseif ($all_content[$i]['status_admin'] == "disapprove" && $all_content[$i]['who_reject'] == 1) { ?>
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="red orangetext text-uppercase text-wrap">REVISION</span></p>
                                        </div>
                                    <?php } elseif ($all_content[$i]['status_admin'] == "disapprove" && $all_content[$i]['who_reject'] == 0) { ?>
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="red  text-uppercase text-wrap">Quality Revision</span></p>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/1'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?>
                                           ">
                                               <?php echo $all_content[$i]['customer_first_name']; ?>
                                               <?php
                                               if (strlen($all_content[$i]['customer_last_name']) > 5) {
                                                   echo ucwords(substr($all_content[$i]['customer_last_name'], 0, 1));
                                               } else {
                                                   echo $all_content[$i]['customer_last_name'];
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <span class="count_project"><?php echo $all_content[$i]['designer_project_count']; ?></span>
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)" class="<?php echo $all_content[$i]['id']; ?>"> 
                                            <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col <?php echo $all_content[$i]['id']; ?>">

                                        <p class="text-h text-wrap"title="<?php echo $all_content[$i]['designer_first_name'] . " " . $all_content[$i]['designer_last_name']; ?>
                                           ">
                                               <?php echo $all_content[$i]['designer_first_name']; ?>
                                               <?php
                                               if (strlen($all_content[$i]['designer_first_name']) > 5) {
                                                   echo ucwords(substr($all_content[$i]['designer_last_name'], 0, 1));
                                               } else {
                                                   echo $all_content[$i]['designer_last_name'];
                                               }
                                               ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <?php
//                                        if ($all_content[$i]['status_admin'] == "active") {
//                                            if ($all_content[$i]['designer_skills_matched'] == 0) {
                                                ?>
<!--                                                <div class="checkbox custom-checkbox mob-txt-center  text-left " id="<?php //echo $all_content[$i]['id']; ?>">
                                                    <label>
                                                        <input type="checkbox" class="checkcorrect" value="1"  data-request="<?php //echo $all_content[$i]['id']; ?>" onchange="check(this.value, '<?php echo $all_content[$i]['id']; ?>');"> 
                                                        <span class="cr right-cr">
                                                            <i class="cr-icon fa fa-check"></i>
                                                        </span> <br>
                                                    </label>
                                                </div>-->
                                                <?php
//                                            }
//                                        }
                                        ?>
                                        <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $all_content[$i]['id']; ?>" data-designerid= "<?php echo $all_content[$i]['designer_id']; ?>">
                                            <span class="sma-red">+</span> Add Designer
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/1'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/admin/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?>
                                        </span>
                                        <?php //echo $all_content[$i]['total_chat_all']  ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/1'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/1">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS;?>img/admin/icon-file.png" class="img-responsive" width="13"> 
                                            <?php if (count($all_content[$i]['total_file']) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($all_content[$i]['total_file']); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($all_content[$i]['total_files_count']); ?></a></p>
                            </div>
                        </div>
                    </div>          
                    <?php
                } 
                elseif ($all_content[$i]['status_admin'] == "assign") {
                    ?>
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $all_content[$i]['id']; ?>/2'">
                           <?php if($all_content[$i]['is_trail'] == 1){ ?>
                                <div class="promo-req inqueue"><?php echo PROMO_TEXT; ?></div>
                            <?php } ?>
                            <div class="cli-ent-xbox">
                                <p class="pro-a">In Queue</p>
                                <div class="cell-col priority-sec"><?php echo isset($all_content[$i]['priority']) ? $all_content[$i]['priority'] : ''; ?></div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 36%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $all_content[$i]['id']; ?>/2"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
<!--                                    <div class="cell-col"><?php //echo isset($all_content[$i]['priority']) ? $all_content[$i]['priority'] : ''; ?></div>-->
                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="gray text-uppercase">IN-queue</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?>
                                           ">
                                               <?php echo $all_content[$i]['customer_first_name']; ?>
                                               <?php
                                               if (strlen($all_content[$i]['customer_last_name']) > 5) {
                                                   echo ucwords(substr($all_content[$i]['customer_last_name'], 0, 1));
                                               } else {
                                                   echo $all_content[$i]['customer_last_name'];
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 19%;">
                            <div class="cli-ent-xbox text-left p-left1">
                                <div class="cell-row">
                                            <?php //echo "<pre>";print_r($inprogressrequest); ?>
                                            <?php if($all_content[$i]['designer_first_name']){?>
<!--                                            <span class="count_project"><?php //echo $inprogressrequest[$i]['designer_project_count'];?></span>-->
                                            <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                                <a href="javascript:void(0)" class="<?php echo  $all_content[$i]['id'];?>"> 
                                                    <?php if($all_content[$i]['profile_picture'] != ""){ ?>
                                                        <figure class="pro-circle-img">
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$all_content[$i]['profile_picture'] ;?>" class="img-responsive">
                                                        </figure>
                                                    <?php }else{ ?>
                                                        <figure class="pro-circle-img">
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                        </figure>
                                                    <?php } ?>
                                                </a>
                                            </div>
                                            <div class="cell-col <?php echo  $all_content[$i]['id'];?>">
                                                <p class="text-h text-wrap"title="<?php echo $all_content[$i]['designer_first_name'] . " " . $all_content[$i]['designer_last_name']; ?>">
                                                    <?php echo $all_content[$i]['designer_first_name'];?>
                                                    <?php 
                                                    if(strlen($all_content[$i]['designer_first_name']) > 5){ 
                                                        echo ucwords(substr($all_content[$i]['designer_last_name'],0,1)); 
                                                    }else{ 
                                                        echo $all_content[$i]['designer_last_name']; 
                                                    } ?>
                                                </p>
                                                <p class="pro-b">Designer</p>
                                                <p class="space-a"></p>
                                                <?php if($all_content[$i]['status'] == "active"){
                                                    if($all_content[$i]['designer_skills_matched'] == 0){
                                                 ?>
                                                    <div id="<?php echo $all_content[$i]['id']; ?>" class="checkbox custom-checkbox mob-txt-center  text-left ">
                                                        <label>
                                                            <input type="checkbox" class="checkcorrect" value="1" data-request="<?php echo $all_content[$i]['id']; ?>" onchange="check(this.value, '<?php echo $all_content[$i]['id']; ?>' );"> 
                                                            <span class="cr  right-cr">
                                                                <i class="cr-icon fa fa-check"></i>
                                                            </span> <br>
                                                        </label>
                                                    </div>
                                                <?php } }?>
                                                    <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $all_content[$i]['id']; ?>" data-designerid= "<?php echo $all_content[$i]['designer_id']; ?>">
                                                        <span class="sma-red">+</span> Add Designer
                                                    </a>
                                            </div>
                                        <?php } else{ ?>
                                            <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $all_content[$i]['id']; ?>" data-designerid= "<?php echo $all_content[$i]['designer_id']; ?>">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                             <?php } ?>
                                        </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $all_content[$i]['id']; ?>/2">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $all_content[$i]['total_chat_all'];    ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $all_content[$i]['id']; ?>">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($all_content[$i]['total_file']) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($all_content[$i]['total_file']); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($all_content[$i]['total_files_count']); ?></a></p>
                            </div>
                        </div>
                    </div> <!-- End Row -->
                <?php } 
                elseif ($all_content[$i]['status_admin'] == "pendingrevision") {
                    ?>
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/3'">
                            <?php if($all_content[$i]['is_trail'] == 1){ ?>
                                            <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                            <?php } ?>
                            <div class="cli-ent-xbox">
                                <p class="pro-a">Expected on</p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['modified']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 30%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/3'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/3"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                    <?php if ($all_content[$i]['status_admin'] == "pendingrevision") { ?>
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="red lightbluetext text-uppercase text-wrap">Pending Review</span></p>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/3'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap"><?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?></p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <span class="count_project"><?php echo $all_content[$i]['designer_project_count']; ?></span>
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)" class="<?php echo $all_content[$i]['id']; ?>">
                                            <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col <?php echo $all_content[$i]['id']; ?>">
                                        <p class="text-h text-wrap"><?php echo $all_content[$i]['designer_first_name'] . " " . $all_content[$i]['designer_last_name']; ?></p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <?php 
                                        //if(!isset($all_content[$i]['designer_first_name'])){
                                        //if ($all_content[$i]['designer_assign_or_not'] == 0) { ?>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-requestid="<?php echo $all_content[$i]['id']; ?>" data-designerid= "<?php echo $all_content[$i]['designer_id']; ?>" data-target="#AddDesign">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                        <?php //}} ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/3'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>3">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span></span>
                                        <?php //echo $all_content[$i]['total_chat_all']   ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/3">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/icon-file.png" class="img-responsive" width="13"> <?php if (count($all_content[$i]['total_file']) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($all_content[$i]['total_file']); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($all_content[$i]['total_files_count']); ?></a></p>
                            </div>
                        </div>
                    </div>
                <?php } 
                elseif ($all_content[$i]['status_admin'] == "checkforapprove") {
                    ?>  
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/4'">
                            <?php if($all_content[$i]['is_trail'] == 1){ ?>
                                            <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                            <?php } ?>
                            <div class="cli-ent-xbox">
                                <p class="pro-a">Delivered on</p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['reviewdate']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 40%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/4"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                    <?php if ($all_content[$i]['status_admin'] == "checkforapprove") { ?>
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="red bluetext text-uppercase text-wrap">Pending-Approval</span></p>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap">
                                            <?php echo $all_content[$i]['customer_first_name']; ?>
                                            <?php
                                            if (strlen($all_content[$i]['customer_last_name']) > 5) {
                                                echo ucwords(substr($all_content[$i]['customer_last_name'], 0, 1));
                                            } else {
                                                echo $all_content[$i]['customer_last_name'];
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <span class="count_project"><?php echo $all_content[$i]['designer_project_count']; ?></span>
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)" class="<?php echo $all_content[$i]['id']; ?>">
                                            <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col <?php echo $all_content[$i]['id']; ?>">
                                        <p class="text-h text-wrap">
                                            <?php echo $all_content[$i]['designer_first_name']; ?>
                                            <?php
                                            if (strlen($all_content[$i]['designer_last_name']) > 5) {
                                                echo ucwords(substr($all_content[$i]['designer_last_name'], 0, 1));
                                            } else {
                                                echo $all_content[$i]['designer_last_name'];
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <?php 
                                       // if(!isset($all_content[$i]['designer_first_name'])){
                                       // if ($all_content[$i]['designer_assign_or_not'] == 0) { ?>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-requestid="<?php echo $all_content[$i]['id']; ?>" data-designerid= "<?php echo $all_content[$i]['designer_id']; ?>" data-target="#AddDesign">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                        <?php //}} ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/4">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?>
                                        </span>
                                        <?php //echo $all_content[$i]['total_chat_all']  ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/4">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/icon-file.png" class="img-responsive" width="13"> <?php if (count($all_content[$i]['total_file']) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($all_content[$i]['total_file']); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($all_content[$i]['total_files_count']); ?></a></p>
                            </div>
                        </div>
                    </div>
                <?php }  
                elseif ($all_content[$i]['status_admin'] == "approved") { ?>
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 13%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/5'">
                           <?php if($all_content[$i]['is_trail'] == 1){ ?>
                                            <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                            <?php } ?>
                            <div class="cli-ent-xbox">
                                <h3 class="app-roved green">Approved on</h3>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['approvddate']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 37%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/5"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="green text-uppercase text-wrap">COMPLETED</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap"><?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?></p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <span class="count_project"><?php echo $all_content[$i]['designer_project_count']; ?></span>
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)">
                                            <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap"><?php echo $all_content[$i]['designer_first_name'] . " " . $all_content[$i]['designer_last_name']; ?></p>
                                        <p class="pro-b">Designer</p>
                                         <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $all_content[$i]['id']; ?>" data-designerid= "<?php echo $all_content[$i]['designer_id']; ?>">
                                            <span class="sma-red">+</span> Add Designer
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $all_content[$i]['id']; ?>/5">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/admin/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $completed[$i]['total_chat_all']   ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $all_content[$i]['id']; ?>/5">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($all_content[$i]['total_file']) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($all_content[$i]['total_file']); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($all_content[$i]['total_files_count']); ?></a></p>
                            </div>
                        </div>
                    </div> 
                    <?php
                } 
            } 
          ?>
<!--        <div class="" style="display:block;text-align:center">
            <a id="load_more" href="javascript:void(0)" data-row="0" data-count="<?php echo $count_load_more; ?>" class="load_more button">Load more</a>
        </div>-->
        <?php }?><span id="loadingAjaxCount" data-value="<?php echo $count_load_more;?>"></span>
   <?php }

  public function load_more_customer() {
        $group_no = $this->input->post('group_no');
        $search = $this->input->post('search');
        $status = $this->input->post('status');
        //echo $status;exit;
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $planlist = $this->Stripe->getallsubscriptionlist();
        
        //echo "<pre>";print_r($activeClients);
//        $countcustomer = $this->Account_model->getall_customer_count();
//        $activeClients = $this->Account_model->getall_customer_load_pagin($start, $content_per_page);
        if($status == "paid"){
        $countcustomer = $this->Account_model->clientlist_load_more("paid","",true,$start,$content_per_page,'',$search);
        $activeClients = $this->Account_model->clientlist_load_more("paid","",false,$start,$content_per_page,'',$search);
        ?>
   
        <?php
        for ($i = 0; $i < sizeof($activeClients); $i++) {
            $activeClients[$i]['total_rating'] = $this->Request_model->get_average_rating($activeClients[$i]['id']);
            $active = $this->Account_model->get_all_active_view_request_by_customer(array('active', 'disapprove'), $activeClients[$i]['id']);
            $inque = $this->Account_model->get_all_active_view_request_by_customer(array('assign'), $activeClients[$i]['id']);
            $revision = $this->Account_model->get_all_active_view_request_by_customer(array('disapprove'), $activeClients[$i]['id']);
            $review = $this->Account_model->get_all_active_view_request_by_customer(array('checkforapprove'), $activeClients[$i]['id']);
            $complete = $this->Account_model->get_all_active_view_request_by_customer(array('approved'), $activeClients[$i]['id']);
            $user = $this->Account_model->get_all_active_request_by_customer($activeClients[$i]['id']);
            $activeClients[$i]['active'] = $active;
            $activeClients[$i]['inque_request'] = $inque;
            $activeClients[$i]['revision_request'] = $revision;
            $activeClients[$i]['review_request'] = $review;
            $activeClients[$i]['complete_request'] = $complete;

            if ($activeClients[$i]['designer_id'] != 0) {
                $activeClients[$i]['designer'] = $this->Request_model->get_user_by_id($activeClients[$i]['designer_id']);
            } else {
                $activeClients[$i]['designer'] = "";
            }
        }
        for ($i = 0; $i < sizeof($activeClients); $i++) {
            ?>
            <div class="cli-ent-row tr brdr">
                <div class="cli-ent-col td" style="width: 38%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" style="width: 80px; padding-right: 15px;">
                                <?php if ($activeClients[$i]['profile_picture'] != "") { ?>
                                    <figure class="cli-ent-img circle one">
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$activeClients[$i]['profile_picture']; ?>" class="img-responsive one">
                                    </figure>
                                <?php } else { ?>
                                    <figure class="cli-ent-img circle one">
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive one">
                                    </figure>
                                <?php } ?>
                            </div>

                            <div class="cell-col" style="width: 185px;">
                                <h3 class="pro-head-q">
                                    <a href="<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $activeClients[$i]['id']; ?>"><?php echo ucwords($activeClients[$i]['first_name'] . " " . $activeClients[$i]['last_name']); ?>
                                    </a><br>
                                    <a href="<?php echo base_url(); ?>admin/accounts/view_client_profile/<?php echo $activeClients[$i]['id']; ?>">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </a>
                                    <!-- <a href="#" data-toggle="modal" data-target="#<?php echo $activeClients[$i]['id']; ?>">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a> -->
                                    <a href="javascript:void(0)" data-customerid="<?php echo $activeClients[$i]['id']; ?>" class="delete_customer">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
<!--                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#viewinfo<?php echo $activeClients[$i]['id']; ?>" class="viewinfo_customer">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>-->
                                </h3>
                                <p class="pro-a"> <?php echo $activeClients[$i]['email']; ?></p>
                                <p class="neft">
                                    <span class="blue text-uppercase">
                                        <?php
                                        $member_type = "";
                                        if ($activeClients[$i]['plan_turn_around_days'] == "1") {
                                            $member_type = "Premium Member";
                                        } elseif ($activeClients[$i]['plan_turn_around_days'] == "3") {
                                            $member_type = "Standard Member";
                                        } else {
                                            $member_type = "No Member";
                                        }
                                        echo $member_type;
                                        ?>
                                    </span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $activeClients[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="">Sign Up Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b"><?php echo date("M /d /Y", strtotime($activeClients[$i]['created'])) ?></p>
                    </div>
                </div>
            <!--  <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php //echo base_url();     ?>admin/dashboard/client_projects/<?php //echo $activeClients[$i]['id'];     ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Next Billing Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b"><?php //echo date("M /d /Y",strtotime($activeClients[$i]['created']))    ?></p>
                    </div>
                </div>-->
                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $activeClients[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $activeClients[$i]['active']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 12%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $activeClients[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">In Queue</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $activeClients[$i]['inque_request']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 15%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $activeClients[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Pending Approval</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $activeClients[$i]['review_request']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 15%;">
                     <div class="cli-ent-xbox text-center">
                   <div class="switch-custom">
                        <span class="checkstatus_<?php echo $activeClients[$i]['id']; ?>">
                            <?php echo ($activeClients[$i]['is_active'] == 1)?'Active':'Inactive';?>
                        </span>
                        <input type="checkbox" data-cid="<?php echo $activeClients[$i]['id']; ?>" id="switch_<?php echo $activeClients[$i]['id']; ?>" <?php echo ($activeClients[$i]['is_active'] == 1)?'checked':'';?>/>
                        <label for="switch_<?php echo $activeClients[$i]['id']; ?>"></label> 
                    </div>
                   </div>
                            </div>
                <div class="cli-ent-col td" style="width: 15%;">
                    <?php if (!empty($activeClients[$i]['designer'])) { ?>
                        <div class="cli-ent-xbox">
                            <div class="cell-row">
                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                    <a href="">
                                        <?php if ($activeClients[$i]['designer'][0]['profile_picture'] != "") { ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$activeClients[$i]['designer'][0]['profile_picture']; ?>" class="img-responsive">
                                            </figure>
                                        <?php } else { ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                            </figure>
                                        <?php } ?>
                                    </a>
                                </div>
                                <div class="cell-col">
                                    <p class="text-h text-wrap" title="<?php echo $activeClients[$i]['designer'][0]['first_name'] . " " . $activeClients[$i]['designer'][0]['last_name']; ?>
                                       ">
                                           <?php echo $activeClients[$i]['designer'][0]['first_name']; ?>
                                           <?php
                                           if (strlen($activeClients[$i]['designer'][0]['last_name']) > 5) {
                                               echo ucwords(substr($activeClients[$i]['designer'][0]['last_name'], 0, 1));
                                           } else {
                                               echo $activeClients[$i]['designer'][0]['last_name'];
                                           }
                                           ?>

                                    </p>
                                    <p class="pro-b">Designer</p>
                                    <p class="space-a"></p>
                                    <a href="#" class="addde-signersk1 permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $activeClients[$i]['id']; ?>">
                                        <span class="sma-red">+</span> Add Designer
                                    </a>
                                </div>
                            </div> 
                        </div>
                    <?php } else { ?>
                        <div class="cli-ent-xbox">
                            <a href="#" class="upl-oadfi-le noborder permanent_assign_design assign_de" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $activeClients[$i]['id']; ?>">
                                <span class="icon-crss-3">
                                    <span class="icon-circlxx55 margin5">+</span>
                                    <p class="attachfile">Assign designer<br> permanently</p>
                                </span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <!-- Modal -->
<!--                <div class="modal fade" id="viewinfo<?php echo $activeClients[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="cli-ent-model-box">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="cli-ent-model">
                                    <header class="fo-rm-header">
                                        <h3 class="head-c">View Information</h3>
                                    </header>
                                    <div class="fo-rm-body">
                                        <div class="projects">
                                            <ul class="projects_info_list">
                                                <li>
                                                    <div class="starus"><b>Active Projects</b></div>
                                                    <div class="value"><?php echo $activeClients[$i]['active']; ?>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="starus"><b>In -Queue Projects</b></div>
                                                    <div class="value"><?php echo $activeClients[$i]['inque_request']; ?>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="starus"><b>Revision Projects</b></div>
                                                    <div class="value"><?php echo $activeClients[$i]['revision_request']; ?>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="starus"><b>Review Projects</b></div>
                                                    <div class="value"><?php echo $activeClients[$i]['review_request']; ?>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="starus"><b>Complete Projects</b></div>
                                                    <div class="value"><?php echo $activeClients[$i]['complete_request']; ?>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
                

            </div>
            <?php
        } ?><span id="loadingAjaxCount" data-value="<?php echo $countcustomer;?>"></span>
        <?php
        }
        if($status == "free"){
        $counttrailcustomer = $this->Account_model->clientlist_load_more("free","",true,$start,$content_per_page,'',$search);
        $trailClients = $this->Account_model->clientlist_load_more("free","",false,$start,$content_per_page,'',$search);
        //echo "<pre>";print_r($trailClients);exit;
         for ($i = 0; $i < sizeof($trailClients); $i++) {
            $trailClients[$i]['total_rating'] = $this->Request_model->get_average_rating($trailClients[$i]['id']);
            $active = $this->Account_model->get_all_active_view_request_by_customer(array('active', 'disapprove'), $trailClients[$i]['id']);
            $inque = $this->Account_model->get_all_active_view_request_by_customer(array('assign'), $trailClients[$i]['id']);
            $revision = $this->Account_model->get_all_active_view_request_by_customer(array('disapprove'), $trailClients[$i]['id']);
            $review = $this->Account_model->get_all_active_view_request_by_customer(array('checkforapprove'), $trailClients[$i]['id']);
            $complete = $this->Account_model->get_all_active_view_request_by_customer(array('approved'), $trailClients[$i]['id']);
            $user = $this->Account_model->get_all_active_request_by_customer($trailClients[$i]['id']);
            $trailClients[$i]['active'] = $active;
            $trailClients[$i]['inque_request'] = $inque;
            $trailClients[$i]['revision_request'] = $revision;
            $trailClients[$i]['review_request'] = $review;
            $trailClients[$i]['complete_request'] = $complete;
            if ($trailClients[$i]['designer_id'] != 0) {
                $trailClients[$i]['designer'] = $this->Request_model->get_user_by_id($trailClients[$i]['designer_id']);
            } else {
                $trailClients[$i]['designer'] = "";
            }
        }
        for ($i = 0; $i < sizeof($trailClients); $i++) { ?>
            <div class="cli-ent-row tr brdr">
                <div class="cli-ent-col td" style="width: 38%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" style="width: 80px; padding-right: 15px;">
                                <?php if ($trailClients[$i]['profile_picture'] != "") { ?>
                                    <figure class="cli-ent-img circle one">
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$trailClients[$i]['profile_picture']; ?>" class="img-responsive one">
                                    </figure>
                                <?php } else { ?>
                                    <figure class="cli-ent-img circle one">
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive one">
                                    </figure>
                                <?php } ?>
                            </div>

                            <div class="cell-col" style="width: 185px;">
                                <h3 class="pro-head-q">
                                    <a href="<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $trailClients[$i]['id']; ?>"><?php echo ucwords($trailClients[$i]['first_name'] . " " . $trailClients[$i]['last_name']); ?>
                                    </a><br>
                                    <a href="<?php echo base_url(); ?>admin/accounts/view_client_profile/<?php echo $trailClients[$i]['id']; ?>">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </a>
                                    <!-- <a href="#" data-toggle="modal" data-target="#<?php echo $trailClients[$i]['id']; ?>">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a> -->
                                    <a href="javascript:void(0)" data-customerid="<?php echo $trailClients[$i]['id']; ?>" class="delete_customer">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
<!--                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#viewinfo<?php echo $trailClients[$i]['id']; ?>" class="viewinfo_customer">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>-->
                                </h3>
                                <p class="pro-a"> <?php echo $trailClients[$i]['email']; ?></p>
                                 <p class="neft">
                                    <span class="blue text-uppercase">
                                        <?php
                                        $member_type = "";
                                        if ($trailClients[$i]['plan_turn_around_days'] == "1") {
                                            $member_type = "Premium Member";
                                        } elseif ($trailClients[$i]['plan_turn_around_days'] == "3") {
                                            $member_type = "Standard Member";
                                        } else {
                                            $member_type = "No Member";
                                        }
                                        echo $member_type;
                                        ?>
                                    </span>
                                </p>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $trailClients[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="">Sign Up Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b"><?php echo date("M /d /Y", strtotime($trailClients[$i]['created'])) ?></p>
                    </div>
                </div>
            <!--                    <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php //echo base_url();     ?>admin/dashboard/client_projects/<?php //echo $activeClients[$i]['id'];     ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Next Billing Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b"><?php //echo date("M /d /Y",strtotime($activeClients[$i]['created']))    ?></p>
                    </div>
                </div>-->
                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $trailClients[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $trailClients[$i]['active']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 12%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $trailClients[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">In Queue</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $trailClients[$i]['inque_request']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 15%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/client_projects/<?php echo $trailClients[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Pending Approval</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $trailClients[$i]['review_request']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 15%;">
                     <div class="cli-ent-xbox text-center">
                                <div class="switch-custom">
                                     <span class="checkstatus_<?php echo $trailClients[$i]['id']; ?>">
                                         <?php echo ($trailClients[$i]['is_active'] == 1)?'Active':'Inactive';?>
                                     </span>
                                     <input type="checkbox" data-cid="<?php echo $trailClients[$i]['id']; ?>" id="switch_<?php echo $trailClients[$i]['id']; ?>" <?php echo ($trailClients[$i]['is_active'] == 1)?'checked':'';?>/>
                                     <label for="switch_<?php echo $trailClients[$i]['id']; ?>"></label> 
                                 </div>
                             </div>
                    </div>
                <div class="cli-ent-col td" style="width: 15%;">
                    <?php if (!empty($trailClients[$i]['designer'])) { ?>
                        <div class="cli-ent-xbox">
                            <div class="cell-row">
                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                    <a href="">
                                        <?php if ($trailClients[$i]['designer'][0]['profile_picture'] != "") { ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$trailClients[$i]['designer'][0]['profile_picture']; ?>" class="img-responsive">
                                            </figure>
                                        <?php } else { ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                            </figure>
                                        <?php } ?>
                                    </a>
                                </div>
                                <div class="cell-col">
                                    <p class="text-h text-wrap" title="<?php echo $trailClients[$i]['designer'][0]['first_name'] . " " . $trailClients[$i]['designer'][0]['last_name']; ?>
                                       ">
                                           <?php echo $trailClients[$i]['designer'][0]['first_name']; ?>
                                           <?php
                                           if (strlen($trailClients[$i]['designer'][0]['last_name']) > 5) {
                                               echo ucwords(substr($trailClients[$i]['designer'][0]['last_name'], 0, 1));
                                           } else {
                                               echo $trailClients[$i]['designer'][0]['last_name'];
                                           }
                                           ?>

                                    </p>
                                    <p class="pro-b">Designer</p>
                                    <p class="space-a"></p>
                                    <a href="#" class="addde-signersk1 permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $trailClients[$i]['id']; ?>">
                                        <span class="sma-red">+</span> Add Designer
                                    </a>
                                </div>
                            </div> 
                        </div>
                    <?php } else { ?>
                        <div class="cli-ent-xbox">
                            <a href="#" class="upl-oadfi-le noborder permanent_assign_design assign_de" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $trailClients[$i]['id']; ?>">
                                <span class="icon-crss-3">
                                    <span class="icon-circlxx55 margin5">+</span>
                                    <p class="attachfile">Assign designer<br> permanently</p>
                                </span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <!-- Modal -->
<!--                <div class="modal fade" id="viewinfo<?php echo $trailClients[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="cli-ent-model-box">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="cli-ent-model">
                                    <header class="fo-rm-header">
                                        <h3 class="head-c">View Information</h3>
                                    </header>
                                    <div class="fo-rm-body">
                                        <div class="projects">
                                            <ul class="projects_info_list">
                                                <li>
                                                    <div class="starus"><b>Active Projects</b></div>
                                                    <div class="value"><?php echo $trailClients[$i]['active']; ?>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="starus"><b>In -Queue Projects</b></div>
                                                    <div class="value"><?php echo $trailClients[$i]['inque_request']; ?>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="starus"><b>Revision Projects</b></div>
                                                    <div class="value"><?php echo $trailClients[$i]['revision_request']; ?>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="starus"><b>Review Projects</b></div>
                                                    <div class="value"><?php echo $trailClients[$i]['review_request']; ?>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="starus"><b>Complete Projects</b></div>
                                                    <div class="value"><?php echo $trailClients[$i]['complete_request']; ?>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
                <!-- Modal -->
                <div class="modal fade" id="<?php echo $trailClients[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="cli-ent-model-box">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="cli-ent-model">
                                    <header class="fo-rm-header">
                                        <h3 class="head-c">Add Client</h3>
                                    </header>
                                    <div class="fo-rm-body">
                                        <form onSubmit="return EditFormSQa(<?php echo $trailClients[$i]['id']; ?>)" method="post" action="<?php echo base_url(); ?>admin/dashboard/edit_client/<?php echo $trailClients[$i]['id']; ?>">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">First Name</label>
                                                        <p class="space-a"></p>
                                                        <input type="text" name="first_name" required class="efirstname form-control input-c" value="<?php echo ucwords($trailClients[$i]['first_name']); ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Last Name</label>
                                                        <p class="space-a"></p>
                                                        <input type="text" name="last_name" required class="elastname form-control input-c" value="<?php echo ucwords($trailClients[$i]['last_name']); ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Email</label>
                                                        <p class="space-a"></p>
                                                        <input type="email" name="email" required  class="eemailid form-control input-c" value="<?php echo $trailClients[$i]['email']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Phone</label>
                                                        <p class="space-a"></p>
                                                        <input type="tel" name="phone" required class="ephone form-control input-c" value="<?php echo $trailClients[$i]['phone']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="password"   class="form-control input-c epassword" >
                                            </div>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Confirm Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="confirm_password"  class="form-control input-c ecpassword" >
                                            </div>
                                            <div class="epassError" style="display: none;">
                                                <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                            </div>
                                            <hr class="hr-b">
                                            <p class="space-b"></p>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Select payment Method</label>
                                                <p class="space-a"></p>
                                                <select class="form-control select-c">
                                                    <option>Credit/Debit Card.</option>
                                                    <option>Credit/Debit Card.</option>
                                                    <option>Credit/Debit Card.</option>
                                                </select>
                                            </div>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Card Number</label>
                                                <p class="space-a"></p>
                                                <input type="text" class="form-control input-c" placeholder="xxxx-xxxx-xxxx" name="Card Number" required>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Expiry Date</label>
                                                        <p class="space-a"></p>
                                                        <input type="text" name="Expiration Date" required class="form-control input-c" placeholder="22.05.2022">
                                                    </div>
                                                </div>

                                                <div class="col-sm-3 col-md-offset-4">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">CVC</label>
                                                        <p class="space-a"></p>
                                                        <input type="text" name="CVC" required class="form-control input-c" placeholder="xxx" maxlength="3">
                                                    </div>
                                                </div>
                                            </div> 

                                            <p class="space-b"></p>

                                            <p class="btn-x"><button type="submit" class="btn-g">Add Customer</button></p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        <?php }?><span id="loadingAjaxCount" data-value="<?php echo $counttrailcustomer;?>"></span><?php
        } 
        
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $data["all_designers"] = $this->Request_model->get_all_designers();
        for ($i = 0; $i < sizeof($data['all_designers']); $i++) {
            $user = $this->Account_model->get_all_active_request_by_designer($data['all_designers'][$i]['id']);
            $data['all_designers'][$i]['active_request'] = sizeof($user);
        }
        $subscription_plan_list = array();
        $j = 0;
        for ($i = 0; $i < sizeof($planlist); $i++) {
            if ($planlist[$i]['id'] == '30291' || $planlist[$i]['id'] == 'A21481D' || $planlist[$i]['id'] == 'A1900S' || $planlist[$i]['id'] == 'M99S' || $planlist[$i]['id'] == 'M199G' || $planlist[$i]['id'] == 'M1991D' || $planlist[$i]['id'] == 'M199S' || $planlist[$i]['id'] == 'A10683D' || $planlist[$i]['id'] == 'M993D') {
                continue;
            }
            $subscription_plan_list[$j]['id'] = $planlist[$i]['id'];
            $subscription_plan_list[$j]['name'] = $planlist[$i]['name'] . " - $" . $planlist[$i]['amount'] / 100 . "/" . $planlist[$i]['interval'];
            $subscription_plan_list[$j]['amount'] = $planlist[$i]['amount'] / 100;
            $subscription_plan_list[$j]['interval'] = $planlist[$i]['interval'];
            $subscription_plan_list[$j]['trial_period_days'] = $planlist[$i]['trial_period_days'];
            $j++;
        }     
       }
       
  public function load_more_clientprojects(){
        $status_role = $this->input->post('status');
        $status = explode(',',$status_role);
        $group_no = $this->input->post('group_no');
        $search = $this->input->post('search');
        $datauserid = $this->input->post('id');
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $count_rows = $this->Account_model->customer_projects_load_more($status,$datauserid,true,$start,$content_per_page,'',$search);
        $rows = $this->Account_model->customer_projects_load_more($status,$datauserid,false,$start,$content_per_page,'',$search);
        //echo "<pre>";print_r($rows);
        //$rows = $this->Request_model->GetAutocomplete_for_admin(array('keyword' => $dataArray), $dataStatus, $datauserid, "admin");
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rowobj = (object)$rows[$i];
            //echo "<pre>";print_r($rowobj);
            $rowobj->total_chat = $this->Request_model->get_chat_number($rowobj->id, $rowobj->customer_id, $rowobj->designer_id, "admin");
            $rowobj->total_chat_all = $this->Request_model->get_total_chat_number($rowobj->id, $_SESSION['user_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($rowobj->id, "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {
                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            if ($rowobj->status == "active" || $rowobj->status == "disapprove") {
                $rowobj->expected = $this->myfunctions->check_timezone($rowobj->latest_update);
            } elseif ($rowobj->status == "checkforapprove") {
                //$rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->latest_update);
               $rowobj->reviewdate = $this->onlytimezone($rowobj->modified);
            } elseif ($rowobj->status == "approved") {
               $rowobj->approvddate = $this->onlytimezone($rowobj->approvaldate);
            }
            if ($rowobj->status_admin == "pendingrevision") {
                $rowobj->revisiondate = $this->onlytimezone($rowobj->modified);
            }
            $rowobj->comment_count = $commentcount;
            $rowobj->total_files = $this->Request_model->get_files_count($rowobj->id, $rowobj->designer_id, "admin");
            $rowobj->total_files_count = $this->Request_model->get_files_count_all($rowobj->id);
        }
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rowobj = (object)$rows[$i];
            if ($rowobj->status == "disapprove" || $rowobj->status == "active" || $rowobj->status == "assign") {
                ?>
                <!--QA Active Section Start Here -->
                    <?php
                    for ($i = 0; $i < sizeof($rows); $i++) {
                        if ($rowobj->status == "disapprove" || $rowobj->status == "active") {
                            
                            ?>
                            <!-- Start Row -->
                            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/1'" style="cursor: pointer;">
                                <div class="cli-ent-col td" style="width: 12%;">
                                    <div class="cli-ent-xbox">
                                        <p class="pro-a">
                                            <?php
                                            if ($rowobj->status == "active") {
                                                echo "Expected on";
                                            } elseif ($rowobj->status == "disapprove") {
                                                echo "Expected on";
                                            } elseif ($rowobj->status == "assign") {
                                                echo "In-Queue";
                                            }
                                            ?>
                                        </p>
                                        <p class="space-a"></p>
                                        <p class="pro-b">
                                            <?php
                                            if ($rowobj->status == "active") {
                                                echo date('M d, Y h:i A', strtotime($rowobj->expected));
                                            } elseif ($rows[$i]->status == "disapprove") {
                                                echo date('M d, Y h:i A', strtotime($rowobj->expected));
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 48%;">
                                    <div class="cli-ent-xbox text-left">
                                        <div class="cell-row">

                                            <div class="cell-col" >
                                                <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1"><?php echo $rowobj->title; ?></a></h3>
                                                <p class="pro-b"><?php echo substr($rowobj->description, 0, 50); ?></p>
                                            </div>

                                            <div class="cell-col col-w1">
                                                <p class="neft text-center">
                                                    <?php if ($rowobj->status == "active" || $rowobj->status == "checkforapprove") { ?>
                                                        <span class="green text-uppercase">In-Progress</span>
                                                    <?php } elseif ($rowobj->status == "disapprove") { ?>
                                                        <span class="red orangetext text-uppercase">REVISION</span>
                                                    <?php } elseif ($rowobj->status == "assign") { ?>
                                                        <span class="gray text-uppercase">In-Queue</span>
                                                    <?php } ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 20%;">
                                    <div class="cli-ent-xbox text-left p-left1">
                                        <div class="cell-row">
                                            <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                                <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1">
                                                    <?php if ($rowobj->profile_picture != "") { ?>
                                                        <figure class="pro-circle-img">
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->profile_picture; ?>" class="img-responsive">
                                                        </figure>
                                                    <?php } else { ?>
                                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                            <?php echo ucwords(substr($rowobj->designer_first_name, 0, 1)) . ucwords(substr($rowobj->designer_last_name, 0, 1)); ?>
                                                        </figure>
                                                    <?php } ?>
                                                </a>
                                            </div>
                                            <div class="cell-col">
                                                <p class="text-h">
                                                    <?php
                                                    if ($rowobj->designer_first_name) {
                                                        echo $rowobj->designer_first_name;
                                                    } else {
                                                        echo "No Designer";
                                                    }
                                                    ?>  
                                                </p>
                                                <p class="pro-b">Designer</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1">
                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                                    <?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                                        <span class="numcircle-box">
                                                            <?php echo $rowobj->total_chat + $rows[$i]->comment_count; ?>
                                                        </span>
                                                    <?php } ?></span>
                                                <?php //echo $rows[$i]->total_chat_all; ?></a></p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1">
                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                                                        <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span></span>
                                                <?php } ?>
                                        <?php echo count($rowobj->total_files_count); ?></a></p>
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                            <?php
                        }
                        if ($rowobj->status == "assign") {
                            ?>
                            <!-- Start Row -->
                            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/1'" style="cursor: pointer;">
                                <div class="cli-ent-col td" style="width: 12%;">
                                    <div class="cli-ent-xbox">
                                        <p class="pro-a">
                                            <?php
                                            if ($rowobj->status == "active") {
                                                echo "Expected on";
                                            } elseif ($rowobj->status == "disapprove") {
                                                echo "Expected on";
                                            } elseif ($rowobj->status == "assign") {
                                                echo "In-Queue";
                                            }
                                            ?>
                                        </p>
                                        <p class="space-a"></p>
                                        <p class="pro-b">
                                            <?php
                                            if ($rowobj->status == "active") {
                                                echo date('M d, Y h:i A', strtotime($rowobj->expected));
                                            } elseif ($rows[$i]->status == "disapprove") {
                                                echo date('M d, Y h:i A', strtotime($rowobj->expected));
                                            }
                                            ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 48%;">
                                    <div class="cli-ent-xbox text-left">
                                        <div class="cell-row">

                                            <div class="cell-col" >
                                                <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/1"><?php echo $rowobj->title; ?></a></h3>
                                                <p class="pro-b"><?php echo substr($rowobj->description, 0, 50); ?></p>
                                            </div>
                        <!--                                            <div class="cell-col"><?php //echo isset($rows[$i]->priority) ? $rows[$i]->priority : '';   ?></div>-->

                                            <div class="cell-col col-w1">
                                                <p class="neft text-center">
                                                    <?php if ($rowobj->status == "active" || $rowobj->status == "checkforapprove") { ?>
                                                        <span class="green text-uppercase">In-Progress</span>
                                                    <?php } elseif ($rowobj->status == "disapprove") { ?>
                                                        <span class="red orangetext text-uppercase">REVISION</span>
                                                    <?php } elseif ($rowobj->status == "assign") { ?>
                                                        <span class="gray text-uppercase">In-Queue</span>
                                                    <?php } ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="cli-ent-col td" style="width: 20%;">
                                    <div class="cli-ent-xbox text-left p-left1">
                                        <div class="cell-row">
                                            <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                                <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1">
                                                    <?php if ($rowobj->profile_picture != "") { ?>
                                                        <figure class="pro-circle-img">
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->profile_picture; ?>" class="img-responsive">
                                                        </figure>
                                                    <?php } else { ?>
                                                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                            <?php echo ucwords(substr($rowobj->designer_first_name, 0, 1)) . ucwords(substr($rowobj->designer_last_name, 0, 1)); ?>
                                                        </figure>
                                                    <?php } ?>
                                                </a>
                                            </div>
                                            <div class="cell-col">
                                                <p class="text-h">
                                                    <?php
                                                    if ($rowobj->designer_first_name) {
                                                        echo $rowobj->designer_first_name;
                                                    } else {
                                                        echo "No Designer";
                                                    }
                                                    ?>  
                                                </p>
                                                <p class="pro-b">Designer</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1">
                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                                    <?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                                        <span class="numcircle-box">
                                                            <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                                                        </span>
                                                    <?php } ?></span>
                                                <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                    </div>
                                </div>
                                <div class="cli-ent-col td" style="width: 10%;">
                                    <div class="cli-ent-xbox text-center">
                                        <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/1">
                                                <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                                                        <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span></span>
                                                <?php } ?>
                                                    <?php echo count($rowobj->total_files_count); ?></a></p>
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                            <?php
                        }
                    }
                    ?>
               
                <!--QA Active Section End Here -->
            <?php } 
            elseif ($rowobj->status == "checkforapprove") { ?>
                <!--QA Pending Approval Section Start Here -->
<!--                <div class="tab-pane content-datatable datatable-width" id="Qa_client_pending" role="tabpanel">-->
                    <?php for ($i = 0; $i < sizeof($rows); $i++) {
                        $rowobj = (object)$rows[$i];
                        ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/2'"  style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">
                                        Delivered on
                                    </p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rowobj->reviewdate));
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col">
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/2"><?php echo $rowobj->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rowobj->description, 0, 50); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center">
                                                <span class="red bluetext text-uppercase text-wrap">Pending-approval</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/2">
                                                <?php if ($rowobj->customer_profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->customer_profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rowobj->customer_first_name, 0, 1)) . ucwords(substr($rowobj->customer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rowobj->customer_first_name . " " . $rowobj->customer_last_name; ?>">
                                                <?php echo $rowobj->customer_first_name; ?>
                                                <?php
                                                if (strlen($rowobj->customer_last_name) > 5) {
                                                    echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                                                } else {
                                                    echo $rowobj->customer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rowobj->id; ?>/2">
                                                <?php if ($rowobj->profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rowobj->designer_first_name, 0, 1)) . ucwords(substr($rowobj->designer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                                                <?php echo $rowobj->designer_first_name; ?>
                                                <?php
                                                if (strlen($rowobj->designer_last_name) > 5) {
                                                    echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rowobj->designer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span>
                                            <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url() . "admin/dashboard/view_request/" . $rows[$i]->id; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span></span>
                                            <?php } ?>
                                                <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?>
<!--                </div>-->
                <!--QA Pending Approval Section End Here -->
            <?php } elseif ($rowobj->status == "approved") { ?>
                <!--QA Completed Section Start Here -->
<!--                <div class="tab-pane content-datatable datatable-width" id="Qa_client_completed" role="tabpanel">-->
                    <?php
                    for ($i = 0; $i < sizeof($rows); $i++) {
                        $rowobj = (object)$rows[$i];
                        // echo "<pre>";
                        // print_r($rows);
                        // echo "</pre>";
                        ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 13%;">
                                <div class="cli-ent-xbox">
                                    <h3 class="app-roved green">Approved on</h3>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rowobj->approvaldate));
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 37%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col">
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3"><?php echo $rowobj->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rowobj->description, 0, 50); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                                <?php if ($rowobj->customer_profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->customer_profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rowobj->customer_first_name, 0, 1)) . ucwords(substr($rowobj->customer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title=" <?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                <?php echo $rowobj->customer_first_name; ?>
                                                <?php
                                                if (strlen($rowobj->customer_last_name) > 5) {
                                                    echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                                                } else {
                                                    echo $rowobj->customer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                                <?php if ($rowobj->profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rowobj->designer_first_name, 0, 1)) . ucwords(substr($rowobj->designer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                                                <?php echo $rowobj->designer_first_name; ?>
                                                <?php
                                                if (strlen($rowobj->designer_last_name) > 5) {
                                                    echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rowobj->designer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                                                    </span>
                                                <?php } ?></span>
                                            <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rowobj->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span></span>
                                            <?php } ?>
                                                <?php echo count($rowobj->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div><!-- End Row -->
                    <?php } ?>
<!--                </div>-->
                <!--QA Completed Section End Here -->
                <?php
            }
        }?><span id="loadingAjaxCount" data-value="<?php echo $count_rows;?>"></span><?php
        }

  public function load_more_designer() {
        $group_no = $this->input->post('group_no');
        $search = $this->input->post('search');
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        //$designers = $this->Account_model->getall_designer_pagination($start, $content_per_page);
        $countdesigners = $this->Account_model->designerlist_load_more("",true,$start,$content_per_page,'',$search);
        $designers = $this->Account_model->designerlist_load_more("",false,$start,$content_per_page,'',$search);
        for ($i = 0; $i < sizeof($designers); $i++) {
            $designers[$i]['total_rating'] = $this->Request_model->get_average_rating($designers[$i]['id']);
            //echo $designers[$i]['id']."</br>";
        }
        for ($i = 0; $i < sizeof($designers); $i++) {
            ?>
            <div class="cli-ent-row tr brdr">
                <div class="cli-ent-col td" style="width: 10%;">
                    <?php if ($designers[$i]['profile_picture'] != "") { ?>
                        <figure class="cli-ent-img circle one">
                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$designers[$i]['profile_picture']; ?>" class="img-responsive one">
                        </figure>
                    <?php } else { ?>
                        <figure class="cli-ent-img circle one">
                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive one">
                        </figure>
                    <?php } ?>
                </div>
                <div class="cli-ent-col td" style="width: 35%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">

                            <div class="cell-col">
                                <h3 class="pro-head space-b">
                                    <a href="<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>"><?php echo ucwords($designers[$i]['first_name'] . " " . $designers[$i]['last_name']); ?>
                                    </a>
                                    <a href="<?php echo base_url(); ?>admin/accounts/view_designer_profile/<?php echo $designers[$i]['id']; ?>">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                    </a>
                                    <a href="#" data-toggle="modal" data-target="#<?php echo $designers[$i]['id']; ?>">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </a>
                                    <?php if ($designers[$i]['disable_designer'] == 1) { ?>
                                        <a title="Enable" id="enable<?php echo $designers[$i]['id']; ?>" href="#" data-toggle="modal" data-target="#confirmation" data-status="enable" class="confirmation" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                        </a>
                                        <a title="Disable" id="disable<?php echo $designers[$i]['id']; ?>" style="display: none;" href="#" data-toggle="modal" class="confirmation" data-status="disable" data-target="#confirmation" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                            <i class="fa fa-circle-o" aria-hidden="true"></i>
                                        </a>
                                    <?php } else { ?>
                                        <a title="Enable" id="enable<?php echo $designers[$i]['id']; ?>" style="display: none;" href="#" data-toggle="modal" data-target="#confirmation" data-status="enable" class="confirmation" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                            <i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                        </a>
                                        <a title="Disable" id="disable<?php echo $designers[$i]['id']; ?>" href="#" data-toggle="modal" class="confirmation" data-status="disable" data-target="#confirmation" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                            <i class="fa fa-circle-o" aria-hidden="true"></i>
                                        </a>
                                    <?php } ?>
                                    <a title="delete" id="delete<?php echo $designers[$i]['id']; ?>" href="#" data-toggle="modal" class="confirmationdelete" data-target="#confirmationdelete" data-designerid="<?php echo $designers[$i]['id']; ?>">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
                                    <p class="pro-b"><?php echo $designers[$i]['email']; ?></p>
                                </h3>
                                <p class="pro-b"><?php echo $designers[$i]['about_me']; ?></p>
                            </div>

                            <!--<div class="cell-col col-w1">
                                <p class="neft text-center"><span class="green text-uppercase text-wrap">In-Progress</span></p>
                            </div>-->
                        </div>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 15%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'" >
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Sign Up Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b"><?php echo date("M /d /Y", strtotime($designers[$i]['created'])) ?></p>
                    </div>
                </div>

<!--                <div class="cli-ent-col td" style="width: 20%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'" >
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Requests Being Handled</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="green text-uppercase"><?php echo $designers[$i]['handled']; ?></span></p>
                    </div>
                </div>-->
                <div class="cli-ent-col td" style="width: 20%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'" >
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Pending Approval</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="green text-uppercase"><?php echo $this->Account_model->getcount_requests_fordesigners($designers[$i]['id'], array("checkforapprove")); ?></span></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 20%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'" >
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active Requests</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $this->Account_model->getcount_requests_fordesigners($designers[$i]['id'], array("active","disapprove")); ?></span></p>
                    </div>
                </div> 
                <!-- Edit Designer Modal -->
                <div class="modal fade" id="<?php echo $designers[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="cli-ent-model-box">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="cli-ent-model">
                                    <header class="fo-rm-header">
                                        <h3 class="head-c">Add Designer</h3>
                                    </header>
                                    <div class="fo-rm-body">
                                        <form onSubmit="return EditFormSQa(<?php echo $designers[$i]['id']; ?>)" action="<?php echo base_url(); ?>admin/dashboard/edit_designer/<?php echo $designers[$i]['id']; ?>" method="post">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">First Name</label>
                                                        <p class="space-a"></p>
                                                        <input name="first_name" type="text" required class="form-control input-c efirstname" value="<?php echo ucwords($designers[$i]['first_name']); ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Last Name</label>
                                                        <p class="space-a"></p>
                                                        <input type="text" name="last_name" required  class="form-control input-c elastname" value="<?php echo ucwords($designers[$i]['last_name']); ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Email</label>
                                                        <p class="space-a"></p>
                                                        <input type="email" name="email" required class="form-control input-c eemailid" value="<?php echo $designers[$i]['email']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Phone</label>
                                                        <p class="space-a"></p>
                                                        <input type="tel" name="phone" required class="ephone form-control input-c" value="<?php echo $designers[$i]['phone']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <hr class="hr-b">

                                            <p class="space-c"></p>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="password"   class="form-control input-c epassword">
                                            </div>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Confirm Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="confirm_password" class="form-control input-c ecpassword">
                                            </div>
                                            <div class="epassError" style="display: none;">
                                                <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                            </div>
                                            <p class="space-b"></p>

                                            <p class="btn-x"><button type="submit" class="btn-g">Add Designer</button></p>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }?>
            <span id="loadingAjaxCount" data-value="<?php echo $countdesigners;?>"></span>
   <?php }
   
  public function load_more_designerprojects(){
        $status_role = $this->input->post('status');
        $status = explode(',',$status_role);
        $group_no = $this->input->post('group_no');
        $search = $this->input->post('search');
        $datauserid = $this->input->post('id');
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $count_rows = $this->Account_model->designer_projects_load_more($status,$datauserid,true,$start,$content_per_page,'',$search);
        $rows = $this->Account_model->designer_projects_load_more($status,$datauserid,false,$start,$content_per_page,'',$search);
//        echo "<pre/>";print_r($rows);
//        $rows = $this->Request_model->GetAutocomplete_designer(array('keyword' => $dataArray), $dataStatus, $datauserid, "designer");
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rowobj = (object)$rows[$i];
            $rowobj->total_chat = $this->Request_model->get_chat_number($rowobj->id, $rowobj->customer_id, $rowobj->designer_id, "designer");
            $rowobj->total_chat_all = $this->Request_model->get_total_chat_number($rowobj->id, $_SESSION['user_id'], "qa");
            $rowobj->total_files = $this->Request_model->get_files_count($rowobj->id, $rowobj->designer_id, 'admin');
            $getfileid = $this->Request_model->get_attachment_files($rowobj->id, "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            $rowobj->comment_count = $commentcount;
            if ($rowobj->status == "active" || $rowobj->status == "disapprove") {
                $rowobj->expected = $this->myfunctions->check_timezone($rowobj->latest_update);
            } elseif ($rowobj->status == "checkforapprove") {
                $rowobj->reviewdate = $this->onlytimezone($rowobj->latest_update);
            } elseif ($rowobj->status == "approved") {
                $rowobj->approvddate = $this->onlytimezone($rowobj->approvaldate);
            }
            if ($rowobj->status_admin == "pendingrevision") {
                $rowobj->revisiondate = $this->onlytimezone($rowobj->modified);
            }
            $rowobj->total_files_count = $this->Request_model->get_files_count_all($rowobj->id);
        }
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rowobj = (object)$rows[$i];
            if ($rowobj->status_designer == "disapprove" || $rowobj->status_designer == "active") {
                ?>
                <!--QA Active Section Start Here -->
                <div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) {$rowobj = (object)$rows[$i]; ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/1'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 12%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">
                                        <?php
                                        if ($rowobj->status == "active") {
                                            echo "Expected on";
                                        } elseif ($rowobj->status_admin == "disapprove") {
                                            echo "Expected on";
                                        }
                                        ?>
                                    </p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php
                                        if ($rowobj->status == "active") {
                                            echo date('M d, Y h:i A', strtotime($rowobj->expected));
                                        } elseif ($rowobj->status_admin == "disapprove") {
                                            echo date('M d, Y h:i A', strtotime($rowobj->expected));
                                        }
                                        ?>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 48%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/1"><?php echo $rowobj->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rowobj->description, 0, 30); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center">
                                                <?php if ($rowobj->status_admin == "active") { ?>
                                                    <span class="green text-uppercase text-wrap">In-Progress</span>
                                                <?php } elseif ($rowobj->status_admin == "disapprove" && $rowobj->who_reject == 1) { ?>
                                                    <span class="red orangetext text-uppercase text-wrap">REVISION</span>
                                                <?php } elseif ($rowobj->status_admin == "disapprove" && $rowobj->who_reject == 0) { ?>
                                                    <span class="red text-uppercase text-wrap">Quality Revision</span>
                                                <?php } ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 20%;">
                                <div class="cli-ent-xbox text-left p-left1">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                            <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/1">
                                                <figure class="pro-circle-img">
                                                    <?php if ($rowobj->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                                                <?php echo $rowobj->designer_first_name; ?>
                                                <?php
                                                if (strlen($rowobj->designer_last_name) > 5) {
                                                    echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rowobj->designer_last_name;
                                                }
                                                ?>

                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/1">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                                <?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                                                    </span>
                                            <?php } ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_request/<?php echo $rowobj->id; ?>/1">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rowobj->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?><span id="loadingAjaxCount" data-value="<?php echo $count_rows;?>"></span>
                </div>
                <!--QA Active Section End Here -->
            <?php } 
            elseif ($rowobj->status_designer == "assign" || $rowobj->status_designer == "pending") { ?>
                <!--QA Active Section Start Here -->
                <div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) {$rowobj = (object)$rows[$i]; ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/2'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 12%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">In Queue</p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 48%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/2"><?php echo $rowobj->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rowobj->description, 0, 30); ?></p>
                                        </div>
                    <!--                                        <div class="cell-col"><?php //echo isset($rows[$i]->priority) ? $rows[$i]->priority : '';   ?></div>-->
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="gray text-uppercase">IN-queue</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 20%;">
                                <div class="cli-ent-xbox text-left p-left1">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                            <a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/2">
                                                <figure class="pro-circle-img">
                                                    <?php if ($rowobj->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                                                <?php echo $rowobj->designer_first_name; ?>
                                                <?php
                                                if (strlen($rowobj->designer_last_name) > 5) {
                                                    echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rowobj->designer_last_name;
                                                }
                                                ?>

                                            </p>
                                            <p class="pro-b">Designer</p>
                                            <p class="space-a"></p>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rowobj->id; ?>" data-designerid= "<?php echo $rowobj->designer_id; ?>">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                                <?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                                                    </span>
                                                <?php } ?></span></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rowobj->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?><span id="loadingAjaxCount" data-value="<?php echo $count_rows;?>"></span>  
                </div>
                <!--QA Active Section End Here -->
            <?php } 
            elseif ($rowobj->status_designer == "pendingrevision") { ?>
                <!--QA Pending Approval Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_pending_review" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { $rowobj = (object)$rows[$i];?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">Expected on</p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rowobj->revisiondate)); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3"><?php echo $rowobj->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rowobj->description, 0, 30); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class=" lightbluetext text-uppercase text-wrap">Pending Review</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href=""><figure class="pro-circle-img">
                                                    <?php if ($rowobj->customer_profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rowobj->customer_first_name . " " . $rowobj->customer_last_name; ?>
                                               ">
                                                   <?php echo $rowobj->customer_first_name; ?>
                                                   <?php
                                                   if (strlen($rowobj->customer_last_name) > 5) {
                                                       echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                                                   } else {
                                                       echo $rowobj->customer_last_name;
                                                   }
                                                   ?>

                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="" class="<?php echo $rowobj->id; ?>"><figure class="pro-circle-img">
                                                    <?php if ($rowobj->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col <?php echo $rowobj->id; ?>">
                                            <p class="text-h text-wrap" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                                                <?php echo $rowobj->designer_first_name; ?>
                                                <?php
                                                if (strlen($rowobj->designer_last_name) > 5) {
                                                    echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rowobj->designer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                                                    </span>
                                                <?php } ?></span>
                                            <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rowobj->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> 
                        <!-- End Row -->
                    <?php } ?><span id="loadingAjaxCount" data-value="<?php echo $count_rows;?>"></span>  
                </div>
                <!--QA Pending Approval Section End Here -->
            <?php } 
            elseif ($rowobj->status == "checkforapprove") { ?>
                <!--QA Pending Approval Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_pending_approval" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) {
                        $rowobj = (object)$rows[$i]; ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/4'">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">Delivered on</p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rowobj->reviewdate)); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/4'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/4"><?php echo $rowobj->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rowobj->description, 0, 30); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="red bluetext text-uppercase text-wrap">Pending-approval</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/4'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="#"><figure class="pro-circle-img">
                                                    <?php if ($rowobj->customer_profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rowobj->customer_last_name; ?>
                                               ">
                                                   <?php echo $rowobj->customer_first_name; ?>
                                                   <?php
                                                   if (strlen($rowobj->customer_last_name) > 5) {
                                                       echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                                                   } else {
                                                       echo $rowobj->customer_last_name;
                                                   }
                                                   ?>

                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="#" class="<?php echo $rowobj->id; ?>"><figure class="pro-circle-img">
                                                    <?php if ($rowobj->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col <?php echo $rowobj->id; ?>">
                                            <p class="text-h text-wrap" title="<?php echo $rowobj->designer_first_name . " " . $rowobj->designer_last_name; ?>">
                                                <?php echo $rowobj->designer_first_name; ?>
                                                <?php
                                                if (strlen($rowobj->designer_last_name) > 5) {
                                                    echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rowobj->designer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/3'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/4">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                                                    </span>
                                                <?php } ?></span>
                                            <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/4">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rowobj->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> 
                        <!-- End Row -->
                    <?php } ?><span id="loadingAjaxCount" data-value="<?php echo $count_rows;?>"></span>  
                </div>
                <!--QA Pending Approval Section End Here -->
            <?php } 
            elseif ($rowobj->status == "approved") {?>
                <!--QA Completed Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) {$rowobj = (object)$rows[$i];?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5'"style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 13%;">
                                <div class="cli-ent-xbox">
                                    <h3 class="app-roved green">Approved on</h3>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rowobj->approvaldate)); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 37%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5"><?php echo $rowobj->title; ?></a></h3>
                                            <p class="pro-b">
                                                <?php echo substr($rowobj->description, 0, 30); ?>
                                            </p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href=""><figure class="pro-circle-img">
                                                    <?php if ($rowobj->customer_profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rowobj->customer_first_name . " " . $rowobj->customer_last_name; ?>">
                                                <?php echo $rowobj->customer_first_name; ?>
                                                <?php
                                                if (strlen($rowobj->customer_last_name) > 5) {
                                                    echo ucwords(substr($rowobj->customer_last_name, 0, 1));
                                                } else {
                                                    echo $rowobj->customer_last_name;
                                                }
                                                ?>

                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href=""><figure class="pro-circle-img">
                                                    <?php if ($rowobj->profile_picture != "") {
                                                        //echo "<pre/>";print_r($rowobj);?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$rowobj->profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rowobj->designer_last_name; ?> ">
                                                <?php echo $rowobj->designer_first_name; ?>
                                                <?php
                                                if (strlen($rowobj->designer_last_name) > 5) {
                                                    echo ucwords(substr($rowobj->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rowobj->designer_last_name;
                                                }
                                                ?>

                                            </p>
                                            <p class="pro-b">Designer</p>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rowobj->total_chat + $rowobj->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rowobj->total_chat + $rowobj->comment_count; ?>
                                                    </span>
                                                <?php } ?></span>
                                            <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rowobj->id; ?>/5">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rowobj->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rowobj->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rowobj->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?><span id="loadingAjaxCount" data-value="<?php echo $count_rows;?>"></span>  
                </div>
                <!--QA Completed Section End Here -->
                <?php
            }
        }?> 
  <?php }
    
  public function load_more_qa_va(){
    $status_role = $this->input->post('status');
    if($status_role == 'qa'){
        $group_no = $this->input->post('group_no');
        $search = $this->input->post('search');
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        if ($search != "") {
            $countqa = $this->Account_model->qavalist_load_more('qa',true,$start,$content_per_page,'',$search);
            $qa = $this->Account_model->qavalist_load_more('qa',false,$start,$content_per_page,'',$search);
//            $qa = $this->Request_model->get_qa_member(array('keyword' => $search));
            /* new */
            for ($i = 0; $i < sizeof($qa); $i++) {
                $designers = $this->Request_model->get_designer_list_for_qa("array", $qa[$i]['id']);
                $qa[$i]['total_designer'] = sizeof($designers);
                $mydesigner = $this->Request_model->get_designer_list_for_qa("", $qa[$i]['id']);
                if ($mydesigner) {
                    $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                    $qa[$i]['total_clients'] = sizeof($data);
                } else {
                    $qa[$i]['total_clients'] = 0;
                }
            }
        } else {
            $countqa = $this->Account_model->qavalist_load_more('qa',true,$start,$content_per_page,'',$search);
            $qa = $this->Account_model->qavalist_load_more('qa',false,$start,$content_per_page,'',$search);
            //$qa = $this->Account_model->getqa_member();
            /* new */
            for ($i = 0; $i < sizeof($qa); $i++) {
                $designers = $this->Request_model->get_designer_list_for_qa("array", $qa[$i]['id']);
                $qa[$i]['total_designer'] = sizeof($designers);

                $mydesigner = $this->Request_model->get_designer_list_for_qa("", $qa[$i]['id']);


                if ($mydesigner) {
                    $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                    $qa[$i]['total_clients'] = sizeof($data);
                } else {
                    $qa[$i]['total_clients'] = 0;
                }
            }
        }
        ?>
        <?php for ($i = 0; $i < sizeof($qa); $i++): ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr">

                <div class="cli-ent-col td" style="width: 65%;">
                    <div class="cell-row">
                        <div class="cell-col" style="padding-right: 15px; width: 100px;">
                            <?php if ($qa[$i]['profile_picture'] != "") { ?>
                                <figure class="cli-ent-img circle one">
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$qa[$i]['profile_picture']; ?>" class="img-responsive">
                                </figure>
                            <?php } else { ?>
                                <figure class="cli-ent-img circle one">
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                </figure>
                            <?php } ?>
                        </div>

                        <div class="cell-col">
                            <h3 class="pro-head">
                                <a href="javascript:void(0)"><?php echo ucwords($qa[$i]['first_name'] . " " . $qa[$i]['last_name']); ?></a>
                                <a href="<?php echo base_url(); ?>admin/accounts/view_qa_profile/<?php echo $qa[$i]['id']; ?>">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                                </a>
                                <a href="#" data-toggle="modal" data-target="#<?php echo $qa[$i]['id']; ?>">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                            </h3>
                            <p class="text-a">Member Since <?php echo date("F Y", strtotime($qa[$i]['created'])); ?></p>
                        </div>

                        <!--<div class="cell-col col-w1">
                                <p class="neft"><span class="blue text-uppercase">Premium Member</span></p>
                        </div>-->
                    </div>

                </div>
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">No. of Designers</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="green text-uppercase"><?php echo $qa[$i]['total_clients']; ?></span></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active Requests</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $qa[$i]['total_designer']; ?></span></p>
                    </div>
                </div>
                <!-- Edit QA Modal -->
                <div class="modal fade" id="<?php echo $qa[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">

                            <div class="cli-ent-model-box">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <form method="post" onSubmit="return EditFormSQa(<?php echo $qa[$i]['id']; ?>)" action="<?php echo base_url(); ?>admin/accounts/edit_qa/<?php echo $qa[$i]['id']; ?>">
                                    <div class="cli-ent-model">
                                        <header class="fo-rm-header">
                                            <h3 class="head-c">Edit QA</h3>
                                        </header>
                                        <div class="fo-rm-body">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">First Name</label>
                                                        <p class="space-a"></p>
                                                        <input name="first_name" type="text" class="form-control input-c efirstname" value="<?php echo $qa[$i]['first_name']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Last Name</label>
                                                        <p class="space-a"></p>
                                                        <input type="text" name="last_name" class="form-control input-c elastname" value="<?php echo $qa[$i]['last_name']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Email</label>
                                                        <p class="space-a"></p>
                                                        <input type="email" name="email" class="form-control input-c eemailid" value="<?php echo $qa[$i]['email']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Phone</label>
                                                        <p class="space-a"></p>
                                                        <input type="tel" name="phone"  class="form-control input-c ephone" value="<?php echo $qa[$i]['phone']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <hr class="hr-b">

                                            <p class="space-c"></p>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="password" class="form-control input-c epassword">
                                            </div>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Confirm Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="confirm_password"  class="form-control input-c ecpassword">
                                            </div>
                                            <div class="epassError" style="display: none;">
                                                <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                            </div>
                                            <p class="space-b"></p>

                                            <p class="btn-x"><button  type="submit" class="btn-g">Add Person</button></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Row -->
            <?php
        endfor;?><span id="loadingAjaxCount" data-value="<?php echo $countqa;?>"></span>
    <?php }
    elseif($status_role == 'va'){
        $group_no = $this->input->post('group_no');
        $search = $this->input->post('search');
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        if ($search != "") {
            $countva = $this->Account_model->qavalist_load_more('va',true,$start,$content_per_page,'',$search);
            $va = $this->Account_model->qavalist_load_more('va',false,$start,$content_per_page,'',$search);
            //$va = $this->Request_model->get_va_member(array('keyword' => $search));
            /* new */
            for ($i = 0; $i < sizeof($va); $i++) {
                $designers = $this->Request_model->get_designer_list_for_qa("array", $va[$i]['id']);
                $va[$i]['total_designer'] = sizeof($designers);

                $mydesigner = $this->Request_model->get_designer_list_for_qa("", $va[$i]['id']);


                if ($mydesigner) {
                    $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                    $va[$i]['total_clients'] = sizeof($data);
                } else {
                    $va[$i]['total_clients'] = 0;
                }
            }
        } else {
            $countva = $this->Account_model->qavalist_load_more('va',true,$start,$content_per_page,'',$search);
            $va = $this->Account_model->qavalist_load_more('va',false,$start,$content_per_page,'',$search);
            //$va = $this->Account_model->getva_member();
            /* new */
            for ($i = 0; $i < sizeof($va); $i++) {
                $designers = $this->Request_model->get_designer_list_for_qa("array", $va[$i]['id']);
                $va[$i]['total_designer'] = sizeof($designers);

                $mydesignerr = $this->Request_model->get_designer_list_for_qa("", $va[$i]['id']);


                if ($mydesignerr) {
                    $data = $this->Request_model->get_customer_list_for_qa("array", $mydesignerr);
                    $va[$i]['total_clients'] = sizeof($data);
                } else {
                    $va[$i]['total_clients'] = 0;
                }
            }
        }
        ?>
        <?php for ($i = 0; $i < sizeof($va); $i++): ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr">

                <div class="cli-ent-col td" style="width: 65%;">
                    <div class="cell-row">
                        <div class="cell-col" style="padding-right: 15px; width: 100px;">
                            <?php if ($va[$i]['profile_picture'] != "") { ?>
                                <figure class="cli-ent-img circle one">
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE.$va[$i]['profile_picture']; ?>" class="img-responsive">
                                </figure>
                            <?php } else { ?>
                                <figure class="cli-ent-img circle one">
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE;?>user-admin.png" class="img-responsive">
                                </figure>
                            <?php } ?>
                        </div>

                        <div class="cell-col">
                            <h3 class="pro-head">
                                <a href="javascript:void(0)"><?php echo ucwords($va[$i]['first_name'] . " " . $va[$i]['last_name']); ?></a>
                                <a href="#" data-toggle="modal" data-target="#<?php echo $va[$i]['id']; ?>">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                            </h3>
                            <p class="text-a">Member Since <?php echo date("F Y", strtotime($va[$i]['created'])); ?></p>
                        </div>

                        <!--<div class="cell-col col-w1">
                                <p class="neft"><span class="blue text-uppercase">Premium Member</span></p>
                        </div>-->
                    </div>

                </div>
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">No. of Designers</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="green text-uppercase"><?php echo $va[$i]['total_clients']; ?></span></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active Requests</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $va[$i]['total_designer']; ?></span></p>
                    </div>
                </div>
                <!-- Edit VA Modal -->
                <div class="modal fade" id="<?php echo $va[$i]['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="AddClientLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="cli-ent-model-box">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <form onSubmit="return EditFormSQa(<?php echo $va[$i]['id']; ?>)" method="post" action="<?php echo base_url(); ?>admin/accounts/edit_va/<?php echo $va[$i]['id']; ?>">
                                    <div class="cli-ent-model">
                                        <header class="fo-rm-header">
                                            <h3 class="head-c">Edit VA</h3>
                                        </header>
                                        <div class="fo-rm-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">First Name</label>
                                                        <p class="space-a"></p>
                                                        <input name="first_name" type="text" class="form-control input-c efirstname" value="<?php echo $va[$i]['first_name']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Last Name</label>
                                                        <p class="space-a"></p>
                                                        <input type="text" name="last_name" class="form-control input-c elastname" value="<?php echo $va[$i]['last_name']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Email</label>
                                                        <p class="space-a"></p>
                                                        <input type="email" name="email" class="form-control input-c eemailid" value="<?php echo $va[$i]['email']; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group goup-x1">
                                                        <label class="label-x3">Phone</label>
                                                        <p class="space-a"></p>
                                                        <input type="tel" name="phone"  class="form-control input-c ephone" value="<?php echo $va[$i]['phone']; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <hr class="hr-b">

                                            <p class="space-c"></p>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="password" class="form-control input-c epassword">
                                            </div>

                                            <div class="form-group goup-x1">
                                                <label class="label-x3">Confirm Password</label>
                                                <p class="space-a"></p>
                                                <input type="password" name="confirm_password"  class="form-control input-c ecpassword">
                                            </div>
                                            <div class="epassError" style="display: none;">
                                                <p class="alert alert-danger">Password and confirm pasword does not match!</p>
                                            </div>
                                            <p class="space-b"></p>

                                            <p class="btn-x"><button  type="submit" class="btn-g">Add Person</button></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Row -->
            <?php
        endfor;?><span id="loadingAjaxCount" data-value="<?php echo $countva;?>"></span><?php
    }
    }

    // For Admin verification that designer skills matched or not
    public function designer_skills_matched() {
        $data = array(
            'designer_skills_matched' => $_POST['designer_skills_matched'],
        );
        $success = $this->Admin_model->update_data('requests', $data, array('id' => $_POST['request_id']));
        if ($success) {
            echo "1";
        } else {
            echo "0";
        }
        exit;
    }

    // Disable designer functionality for admin  
    public function disable_designer() {
        if ($_POST['designerid']) {
            $success = $this->Admin_model->update_data('users', array('disable_designer' => 1), array('id' => $_POST['designerid']));
            if ($success) {
                $all_requests = $this->reassign_project_to_enable_designer($_POST['designerid']);
                echo "1";
            } else {
                echo "0";
            }
        }
    }

    // Enable designer functionality for admin  
    public function enable_designer() {
        if ($_POST['designerid']) {
            $success = $this->Admin_model->update_data('users', array('disable_designer' => 0), array('id' => $_POST['designerid']));
            if ($success) {
                echo "1";
            } else {
                echo "0";
            }
        }
    }

    // Delete Designer from Admin 
    public function delete_designer() {
        $success = $this->Request_model->delete_user('users', $_POST['designerid']);
        if ($success) {
            $this->reassign_project_to_enable_designer($_POST['designerid']);
        }
    }

    // Reassign disabled designer Projects
    public function reassign_project_to_enable_designer($id) {
        $designer_request = $this->Request_model->get_request_by_designer_id($id);
        foreach ($designer_request as $value) {
            $get_all_designers_info = $this->Request_model->get_designers_info($value['category']);
            $count_min_request = [];
            foreach ($get_all_designers_info as $designers) {
                $project_count = $this->Request_model->get_request_count_by_designer_id($designers['user_id']);
                $count_min_request[$designers['user_id']] = $project_count;
            }
            $count = array_keys($count_min_request, min($count_min_request));
            $success = $this->Admin_model->update_data('requests', array('designer_id' => $count[0]), array('id' => $value['id']));
            if ($success) {
                echo "1";
            } else {
                echo "0";
            }
        }
    }
    
    public function change_customer_status_active_inactive(){
     $cust_status = $_POST['status'];
     $cust_id = $_POST['data_id'];
    if($_POST['status'] == 'true'){
        echo "1";
        $this->Request_model->update_customer_active_status("users", array("is_active" => 1), array("id" => $cust_id));
    }elseif($_POST['status'] == 'false'){
        echo "0";
        $this->Request_model->update_customer_active_status("users", array("is_active" => 0), array("id" => $cust_id));
    }
}

    public function testemail() {
        $config = $this->config->item('email_smtp');
        $from = $this->config->item('from');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config['smtp_sequre']);
        define('MAIL_HOST', $config['smtp_host']);
        define('MAIL_PORT', $config['smtp_port']);
        define('MAIL_USERNAME', $config['smtp_user']);
        define('MAIL_PASSWORD', $config['smtp_pass']);
        define('MAIL_SENDER', $from);

        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $data = array(
            'title' => 'Hello Test',
            'project_id' => 1
        );
        $body = $this->load->view('emailtemplate/qa_approved_template', $data, TRUE);
        $receiver = 'jatinderkumar0550@gmail.com';

        $subject = 'Test email';

        $message = $body;

        $title = 'GraphicsZoo';

        echo SM_sendMail($receiver, $subject, $message, $title);
    }

}
