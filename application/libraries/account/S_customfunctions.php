<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class S_customfunctions {

    public function __construct() {
        $CI = &get_instance();
        $CI->load->library('session');
        $CI->load->model('Admin_model', '', TRUE);
        $CI->load->model('account/S_admin_model', '', TRUE);
        $CI->load->model('Account_model', '', TRUE);
        $CI->load->model('Request_model', '', TRUE);
        $CI->load->model('account/S_request_model', '', TRUE);
        $CI->load->model('S_admin_model', '', TRUE);
        $CI->load->model('Clients_model', '', TRUE);
        $CI->load->model('Category_model', '', TRUE);
        $CI->load->library('Myfunctions');
        $CI->load->library('s3_upload');
        $agencytier_price = $this->agencytierprice($login_user_id);
        $CI->load->vars($agencytier_price);
        $billing_plans = $this->getplansofcurrentusers();
        $CI->load->vars($billing_plans);

    }

    public function getprofileimageurl($img="") {
         $CI = &get_instance();
         $login_user_data = $CI->load->get_var('login_user_data');
            if($login_user_data[0]['is_saas']==1){
                if(trim($img) == ""){
                 $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE ."user-admin.png";
                }
                else{
                    if(file_get_contents(FS_PATH. FS_UPLOAD_PUBLIC_UPLOADS_PROFILE ."_thumb/". $img)){
                        $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE ."_thumb/".$img;
                    }else{
                        $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE.$img;
                    }
                }
            }else{
                if(trim($img) == ""){
                 $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE_SAAS ."user-admin.png";
             }else{
                 if(file_get_contents(FS_PATH. FS_UPLOAD_PUBLIC_UPLOADS_PROFILE_SAAS ."_thumb/". $img)){
                     $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE_SAAS ."_thumb/".$img;
                 }else{
                     $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE_SAAS .$img;
                 }
             }
         }
       return $path;
    } 
    
    public function getprojectteammembers($customer_id,$designer_id="") {
        $CI = &get_instance();
        $data = array();
        $customer = $CI->S_admin_model->getuser_data($customer_id);
        $data[0]['customer_image'] = $CI->s_customfunctions->getprofileimageurl($customer[0]['profile_picture']);
        $qa = array();
        if (!empty($customer)) {
            $data[0]['customer_name'] = $customer[0]['first_name'] . " " . $customer[0]['last_name'];
            $qa = $CI->S_admin_model->getuser_data($customer[0]['qa_id']);
            $data[0]['qa_image'] = $CI->s_customfunctions->getprofileimageurl($qa[0]['profile_picture']);
            if (!empty($qa)) {
                $data[0]['qa_name'] = $qa[0]['first_name'] . " " . $qa[0]['last_name'];
            }else{
                $data[0]['qa_name'] = '';
            }
        }else{
            $data[0]['customer_name'] = '';
        }
        $designer = $CI->S_admin_model->getuser_data($designer_id);
        $data[0]['designer_image'] = $CI->s_customfunctions->getprofileimageurl($designer[0]['profile_picture']);
        if (!empty($designer)) {
            $data[0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
        } else {
            $data[0]['designer_name'] = "";
        }
//        echo "<pre/>";print_R($data);exit;
        return $data;
    }
    
    public function getfolderfilesofusers() {
        $CI = &get_instance();
        $login_user_id = $CI->load->get_var('login_user_id');
        $main_user = $CI->load->get_var('main_user');
        $userprojects  = $CI->S_file_management->getRequestfilesofUser($main_user);

        $folderstructure = array();
        foreach($userprojects as $uk => $uv){
           $folderstructure['projects'][$uk]['name'] = $uv['name'];
           $folderstructure['projects'][$uk]['request_id'] = $uv['request_id'];
        }    
        $userbrands = $CI->S_request_model->get_brand_profile_by_user_id($login_user_id);
        foreach($userbrands as $k => $v){
           $folderstructure['brands'][$k]['id'] = $v['id'];
           $folderstructure['brands'][$k]['brand_name'] = $v['brand_name'];
        }  
        $allfolders = $CI->S_file_management->getFolderStructure('0');
        foreach($allfolders['folder'] as $k => $v){
           $folderstructure['folder'][$k]['folder_id'] = $v['id'];
           $folderstructure['folder'][$k]['folder_name'] = $v['folder_name'];
           $folderstructure['folder'][$k]['folder_type'] = $v['folder_type'];
        }
        
        foreach($allfolders['folder'] as $k => $v){
            $folderstructure['main_folder'][$k]['folder_id'] = $v['id'];
           $folderstructure['main_folder'][$k]['folder_name'] = $v['folder_name'];
           $folderstructure['main_folder'][$k]['folder_type'] = $v['folder_type'];
           $subfolder = $CI->S_file_management->getSubFolderbyID($v['id']);
           foreach($subfolder as $sk => $sv){
               $folderstructure['subfolder'][$v['id']][$sk]['folder_id'] = $sv['id'];
               $folderstructure['subfolder'][$v['id']][$sk]['folder_name'] = $sv['folder_name'];
               $folderstructure['subfolder'][$v['id']][$sk]['folder_type'] = $sv['folder_type'];
               
           }
        }
       // echo "<pre>";print_r($folderstructure);exit;
        return $folderstructure;
    }
    
    public function agencytierprice() {
        $CI = &get_instance();
        $tier_price = array();
        $tier_price['monthly_plans'] = $CI->S_request_model->getactiveagencyplanid('monthly');
        $tier_price['yearly_plans'] = $CI->S_request_model->getactiveagencyplanid('yearly');
        
        $tier_price['montly_tier_price'] = $CI->S_request_model->getagencytierprice('monthly');
        $key = 1;
        $planid = $tier_price['montly_tier_price'][0]['plan_id'];
        foreach ($tier_price['montly_tier_price'] as $value) {
            if($planid != $value['plan_id']){
                $key = 1;
                $planid = $value['plan_id'];
            }else{
               $key++; 
            }
            $tier_price['subscription_data'][$planid][$key] = $value;
        }
        $tier_price['yearly_tier_price'] = $CI->S_request_model->getagencytierprice('yearly');
        $keys = 1;
        $plan_id = $tier_price['yearly_tier_price'][0]['plan_id'];
        foreach ($tier_price['yearly_tier_price'] as $value) {
            if($plan_id != $value['plan_id']){
                $keys = 1;
                $plan_id = $value['plan_id'];
            }else{
               $keys++; 
            }
            $tier_price['annual_subscription_data'][$plan_id][$keys] = $value;
        }
        
        $tier_price['all_tier_prices'] = array_merge($tier_price['subscription_data'],$tier_price['annual_subscription_data']);
        return $tier_price;
    }
    
//    public function getplansofcurrentusers() {
//    $CI = &get_instance();
//     $main_user = $CI->load->get_var('main_user');
//     $data = $CI->S_admin_model->getuser_data($main_user);
//     $billinginfo = array();
//     if (in_array($data[0]['plan_name'], NEW_PLANS)) {
//            $billinginfo['userplans'] = $CI->S_request_model->getplansforuser('request_based');
//            $billinginfo['type_ofuser'] = 'fortynine_plans';
//        } else {
//            $billinginfo['userplans'] = $CI->S_request_model->getplansforuser();
//            $billinginfo['type_ofuser'] = 'subscription_based';
//        }
//        //echo "<pre>";print_r($billinginfo['userplans']);exit;
//        return $billinginfo;
//    }
    
    public function getplansofcurrentusers() {
    $CI = &get_instance();
     $login_user_data = $CI->load->get_var('login_user_data');
     if($login_user_data[0]['parent_id'] == 0){
         $main_user = $login_user_data[0]['id'];
     }else{
         $main_user = $login_user_data[0]['parent_id'];
     }
     $data = $CI->S_admin_model->getuser_data($main_user);
     if($login_user_data[0]['parent_id'] != 0 && $login_user_data[0]['user_flag'] == 'client'){
         
         $billinginfo['type_ofuser'] = $login_user_data[0]['requests_type'];
         $current_plan = $CI->Clients_model->getsubsbaseduserbyplanid($login_user_data[0]['plan_name'],"plan_id,plan_name,global_inprogress_request,global_active_request,plan_price,plan_type,plan_type_name,features,payment_mode");
         $billinginfo['userplans'] = $CI->Clients_model->getsubscriptionbaseduser($main_user,"",1,"",$current_plan[0]["payment_mode"]);
         
         
         foreach($billinginfo['userplans'] as $key => $userplans){
            $pln_feature = explode("_", $userplans['features']);
            $plnfeature = "<ul>";
            if(!empty($pln_feature)){
                foreach ($pln_feature as $text) {
                    $plnfeature .= "<li>".$text."</li>";
                }
            }
           $plnfeature .= "<ul>";
           if($userplans['plan_type'] == "unlimited"){
               $billinginfo['userplans'][$key]['plan_type'] = $userplans["global_inprogress_request"]." Request"; 
           }
          $canaddclient = $this->checkclientaddornot($data,$userplans);
          $billinginfo['userplans'][$key]['features'] = $plnfeature; 
          $billinginfo['userplans'][$key]['info'] = $this->billingsubsbasedcond($current_plan,$userplans,'client',$login_user_data[0]['plan_name'],$canaddclient,$login_user_data[0]['customer_id']);
          $billinginfo['userplans'][$key]['requests'] = $canaddclient;
         }
         
     }else{
        $current_plan = $CI->S_request_model->getsubscriptionlistbyplanid($data[0]['plan_name']);
        $billinginfo = array();
        if (in_array($data[0]['plan_name'], NEW_PLANS)) {
               $billinginfo['userplans'] = $CI->S_request_model->getplansforuser('request_based');
               $billinginfo['type_ofuser'] = 'fortynine_plans';
        } else {
            $billinginfo['userplans'] = $CI->S_request_model->getplansforuser();
            $billinginfo['type_ofuser'] = 'subscription_based';
        }
        foreach($billinginfo['userplans'] as $key => $userplans){
          $billinginfo['userplans'][$key]['info'] = $this->billingsubsbasedcond($current_plan,$userplans,'main',$data[0]['plan_name']);
          if($userplans['plan_type'] == "unlimited"){
               $billinginfo['userplans'][$key]['plan_type'] = $userplans["global_inprogress_request"]." Request"; 
           }
        }
     }
        return $billinginfo;
    }
    
    public function allStatusafterUnhold($previousstatus){
        if($previousstatus){
            if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $previousstatus)){
                $status_arr = explode('_',$previousstatus);
                $i = 0;
                $keys_array=array("0"=>"status","1"=>"status_admin","2"=>"status_designer","3"=>"status_qa");
                $keys = array_keys($keys_array);
                for($i=0;$i<count($keys);$i++) {
                    $keys_array[$keys_array[$i]]=$status_arr[$i];
                    unset($keys_array[$i]);
                }
            }else{
               $keys_array = array("status"=>$previousstatus,"status_admin"=>$previousstatus,"status_designer"=>$previousstatus,"status_qa"=>$previousstatus);
            }
            return $keys_array;
    }
    }
    
//    public function getdesignerlistforassign() {
//        $CI = &get_instance();
//        $designers = array();
//        $designers['designer_list'] = $CI->S_admin_model->get_total_customer("designer");
//        
//        for ($dl = 0; $dl < count($designers['designer_list']); $dl++) {
//            $user = $CI->Account_model->get_all_active_request_by_designer($designers['designer_list'][$dl]['id']);
//            
//            $designers['designer_list'][$dl]['profile_picture'] = $this->getprofileimageurl($designers['designer_list'][$dl]['profile_picture']);
//           
//            $designers['designer_list'][$dl]['active_request'] = sizeof($user);
//        }
//        return $designers;
//        
//    }
    
    /*********samples upload**********/
    public function movefilestos3($success,$path,$sampleflag = NULL) {
        $CI = &get_instance();
        if (!is_dir($path . $success)) {
            mkdir('./'. $path . $success, 0777, TRUE);
        }
        //Move files from temp folder
        // Get array of all source files
        if($sampleflag != ''){
            $uploadPATH = $_SESSION['temp_attachfolder_name'];
        }else{
            $uploadPATH = $_SESSION['temp_folder_names'];
        }
        $uploadPATHs = str_replace("./", "", $uploadPATH);
        $files = scandir($uploadPATH);
//        echo "uploadpatj".$uploadPATHs;exit;
        // Identify directories
        $source = $uploadPATH . '/';
        $destination = './' .$path. $success . '/';

        if (UPLOAD_FILE_SERVER == 'bucket') {
//            echo "<pre/>";print_R($files);exit;
            foreach ($files as $file) {

                if (in_array($file, array(".", "..")))
                    continue;

                $staticName = base_url() . $uploadPATHs . '/' . $file;
               // echo "staticName".$staticName."<br/>";

                $config = array(
                    'upload_path' => $path . $success . '/',
                    'file_name' => $file
                );
               // echo "<pre/>";print_R($config);

                $CI->s3_upload->initialize($config);
                $is_file_uploaded = $CI->s3_upload->upload_multiple_file($staticName);
                if ($is_file_uploaded['status'] == 1) {
                    $delete[] = $source . $file;
                    unlink('./tmp/tmpfile/' . basename($staticName));
                }
            }
        } else {
            foreach ($files as $file) {
                if (in_array($file, array(".", "..")))
                    continue;
                $is_file_uploaded['filepath'] = APPPATH . $source . $file;
                if (copy($source . $file, $destination . $file)) {
                    $delete[] = $source . $file;
                }
            }
        }
//        echo "<pre>";print_r($files);exit;
        foreach ($delete as $file) {
            unlink($file);
        }
        rmdir($source);
        unset($_SESSION['temp_folder_name']);
        unset($_SESSION['temp_attachfolder_name']);
        if($sampleflag){
            return $files;
        }else{
            return $is_file_uploaded;
        }
        
    }
    
    public function checkclientaddornot($main_user,$userplans="") {
         $CI = &get_instance();
         $useroptions_data = $CI->S_request_model->getAgencyuserinfo($main_user[0]['id']);
         $sharedusers = $CI->S_admin_model->sumallsubuser_requestscount($main_user[0]['id'],1);
         $sharedusers_count = $CI->S_admin_model->sumallsubuser_requestscount($main_user[0]['id'],1,1);
         if(!isset($useroptions_data[0]['reserve_count']) || $useroptions_data[0]['reserve_count'] == -1){
             $allcount = -1;
             $reserve_slot = $main_user[0]['total_inprogress_req'] ;
         }else{
             $reserve_slot = $useroptions_data[0]['reserve_count'];
         }
         
         $dedicatedusers = $CI->S_admin_model->sumallsubuser_requestscount($main_user[0]['id'],0);
         $dedicateddesignercount = $dedicatedusers['total_inprogress_req']*$useroptions_data[0]['no_of_assign_user'];
         $reserve_count = $reserve_slot*$useroptions_data[0]['no_of_assign_user'];
         $subusers = $dedicateddesignercount+$sharedusers['total_inprogress_req'];
         $subusers_reqcount = $subusers+$reserve_count;
         $main_dedicated_designr = $main_user[0]['total_inprogress_req'];
         $main_inprogress_req = $main_user[0]['total_inprogress_req']*$useroptions_data[0]['no_of_assign_user'];
         $pending_req = $main_inprogress_req - $subusers_reqcount;
         
         $sharedratio = (int) ($sharedusers['total_inprogress_req']/$useroptions_data[0]['no_of_assign_user']);
         $shared_dedicated = $dedicatedusers['total_inprogress_req']+$sharedratio;
         $pendreserve_count = $main_dedicated_designr - $shared_dedicated;
         $f_reserve_count = ($pendreserve_count > 0)?$pendreserve_count:0;
         $clientslot =  array('main_dedicated_designr' => $main_dedicated_designr,'main_inprogress_req' => $main_inprogress_req,'addedsbrqcount' => $subusers_reqcount,'pending_req' => $pending_req,'shared_ratio' => $useroptions_data[0]['no_of_assign_user'],"reserve_count" => $reserve_slot,"dedicatedusers" => $dedicatedusers['total_inprogress_req'],"pendreserve_count" => $f_reserve_count,"allslot" => $allcount,"sharedusers" => $sharedusers_count['total_inprogress_req']);
         if(!empty($userplans)){   
           if ($userplans["shared_user_type"] == 0) {
                $inprogress_req = $userplans['global_inprogress_request'] * $useroptions_data[0]["no_of_assign_user"];
                $clientslot['msg'] = "You can't add more dedicated designer";
            } else {
                $inprogress_req = $userplans['global_inprogress_request'];
                $clientslot['msg'] = "You can't add more shared designer";
            }
            $canadd_req = $subusers_reqcount + $inprogress_req;
            if($canadd_req > $main_inprogress_req && $userplans['plan_type_name'] != 'one_time'){
               $clientslot['slot'] = 0; 
            }else{
              $clientslot['slot'] = 1;   
            }
          }
        return $clientslot;
           
    }
    
    public function billingsubsbasedcond($current_plan,$userplans,$logggeduser,$planname="",$canaddclient="",$customerid=""){
        $info = array();
        $CI = &get_instance();
        $parent_user_data = $CI->load->get_var('parent_user_data');
        if($logggeduser == 'client'){
            if ($current_plan[0]['plan_type'] == 'yearly' && $userplans['plan_type'] == 'monthly') { 
                $info['target'] = "planpermissionspopup";
                $info['class'] = "nfChngPln";
                $info['text'] = "Change Plan";
            }else if($current_plan[0]['plan_type_name'] == 'all' && $userplans['plan_type_name'] == 'one_time'){
                $info['target'] = "sbusrplnprmsnspopup";
                $info['class'] = "nfChngPln";
                $info['text'] = "Change Plan";
            }else if($canaddclient['slot'] == 0){
                $info['target'] = "cantaddsubs";
                $info['class'] = "cantaddclnt";
                $info['text'] = "Change Plan";
            }else{   
                if ($userplans['payment_mode'] == 1 && ($planname == "" || $customerid == "")) {
                    $info['target'] = "Upgrdsubuseracount";
                    $info['class'] = "upgrdclntacnt";
                    $info['text'] = "Choose Plan";
                }else if($userplans['plan_type'] != 'unlimited' && $planname == $userplans['plan_id']){
                    $info['target'] = "currentplan";
                    $info['class'] = "currentPlan f_actv_pln";
                    $info['text'] = "Current Plan";
                }else{
                    $info['target'] = "chnagesubuserpln";
                    $info['class'] = "ChngclntPln";
                    $info['text'] = " Change Plan";
                }
            }
        }else{
            if($current_plan[0]['plan_type'] == 'yearly' && $userplans['plan_type'] == 'monthly'){ 
               $info['target'] = "planpermissionspopup";
                $info['class'] = "ChngPln"; 
                $info['text'] = " Change Plan";
            }else if($current_plan[0]['plan_type'] == 'quarterly' && $userplans['plan_type'] == 'monthly'){ 
               $info['target'] = "planpermissionspopup";
                $info['class'] = "ChngPln"; 
                $info['text'] = " Change Plan";
            }else if($userplans['plan_type'] != 'unlimited' && $planname == $userplans['plan_id']){
                    $info['target'] = "currentplan";
                    $info['class'] = "currentPlan f_actv_pln";
                    $info['text'] = "Current Plan";
            }else{
                if ($parent_user_data[0]['is_cancel_subscription'] == 1 && $parent_user_data[0]['parent_id'] == 0 && $planname != "") {
                    $info['target'] = "CnfrmPopup";
                    $info['class'] = "ChngPln";
                    $info['text'] = "Choose Plan";
                }else if($planname == ""){
                    $info['target'] = "";
                    $info['class'] = "upgrd";
                    $info['text'] = "Choose Plan";
                }else{
                    $info['target'] = "CnfrmPopup";
                    $info['class'] = "ChngPln";
                    $info['text'] = "Change Plan";
                }
            }
        }
        
        return $info;
        
    }
    
    public function offlinepayment_process($subscriptions,$planname,$customer,$current_req="") {
        $start = date('Y-m-d H:i:s');
        if($subscriptions[0]['plan_type_name'] == 'one_time'){
            $post_req = isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0;
            $customer['total_requests'] = $current_req+$post_req;
            $customer['total_active_req'] = 0;
            $customer['total_inprogress_req'] = 0; 
            $customer['billing_start_date'] = $start;
        }
        else{
            $customer['total_requests'] = 0;
            $customer['total_active_req'] = isset($subscriptions[0]['global_active_request'])?$subscriptions[0]['global_active_request']:0;
            $customer['total_inprogress_req'] = isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0;
            if ($planname) {
                if($subscriptions[0]['plan_type'] == "monthly"){
                    $enddate = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", strtotime($start)) . " +1 month"));
                }else{
                    $enddate = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", strtotime($start)) . " + 1 year"));
                }
                    $customer['billing_start_date'] = $start;
                    $customer['billing_end_date'] = $enddate;
            }
            
        }
        $customer['plan_interval'] = $subscriptions[0]['plan_type'];
        $customer['payment_status'] = 1;
        $customer['requests_type'] = $subscriptions[0]['plan_type_name'];
        return $customer;
    }
    public function CheckIsSaasUser(){
        $Check = $CI->S_admin_model->CheckForMainORsub($_SESSION['user_id']); 
        if($Check['is_saas']==1){
            $ci = $CI->S_file_management; 
        }else{
            $ci = $CI->S_S_file_management; 
        }
        return $ci; 
      }
}
