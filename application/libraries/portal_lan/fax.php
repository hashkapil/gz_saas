<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fax extends Front_controller {
    public function __construct(){
        parent::__construct();

        $this->userauth->is_login();

        $this->load->model('ct_orders_fax_m');
        $this->load->model('db_portal_info_m');
    }

    public function index()
    {
        //code by heng
        $this->load->model('video_popup_m');
        
        $data['selected_video'] = "Fax";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
        $data['current_page'] = 'FAX';
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

        $this->load->view($this->config->item('portal_folder') . '/fax', $data);
    }

	public function download($order_number){
		if($order_number !== ''){// && (int)$order_number > 0){
			$this->load->helper('download');

			//$order_info = $this->ct_orders_fax_m->get_order_info("Order_Number = '" . $order_number . "'");
			$order_info = $this->ct_orders_fax_m->get_order_info("unique_faxid = '" . $order_number . "'");			

			if(is_array($order_info) && count($order_info)){								
				if($order_info['Service_Type'] == '95'){
					$data = $order_info['Final'];
					//$name = $order_info['Order_Number'] . '.pdf';
					$name = $order_info['unique_faxid'] . '.pdf';
										
					/*/lets directly download the file											
						header('Content-Disposition: attachment; filename="'.$order_info['unique_faxid'].'.pdf"');
						header('Content-Type: text/plain'); # Don't use application/force-download - it's not a real MIME type, and the Content-Disposition header is sufficient
						header('Content-Length: ' . strlen($data));
						header('Connection: close');


						echo $data;
						exit;
					//*///////////////////////////////////
					$download_link = get_download($name, $data);
                    header("Location: " . $download_link);
					exit;
				}else if($order_info['Service_Type'] == '85'){
					header("Location:" . 'http://faxdocs.etollfree.net/completed/' . $order_info['unique_faxid'] . '.pdf');
					exit;
				}
			}			
		}

		redirect(site_url('fax'));
	}

    public function listing(){
        $this->ct_orders_fax_m->get_listing();
    }
}