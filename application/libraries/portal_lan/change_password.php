<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Change_password extends Front_controller {
	public function __construct(){
		parent::__construct();

		$this->userauth->is_login();

		$this->load->model('ct_account_m');
	}

	public function index()
	{
		$data['current_page'] = 'CHANGE_PASSWORD';

		$this->post_process();

		$this->load->view($this->config->item('portal_folder') . '/change_password', $data);
	}

	public function post_process(){

		$sess_user_account_number = $this->session->userdata('sess_user_account_number');

		$action_type = $this->input->post('action_type');

		switch($action_type){
			case 'change_password':
				$old_password = $this->input->post('old_password');
				$confirm_password = $this->input->post('confirm_password');

				$cp_values_array = array(
					'Password' => $confirm_password
				);

				$portal_account_info = $this->ct_account_m->get_account_info("Account_Number = '" . $sess_user_account_number . "'");

				if($portal_account_info['Password'] == $old_password){

					$this->ct_account_m->update_account($sess_user_account_number, $cp_values_array);

					$this->session->set_flashdata('success', 'Your password has been changed successfully.');
				}else{
					$this->session->set_flashdata('error', 'Please check your old password.');
				}

				redirect(site_url('change_password'));

			break;
		}
	}

	public function payment_list(){
		$this->load->library('Datatables');

		$this->datatables->select("Amount, Date, PaymentType, Transaction_ID, ID", FALSE);
		$this->datatables->from('ct_payments');

		$this->datatables->where('Account_Number', $this->session->userdata('sess_user_account_number'));

		$this->datatables->edit_column('Amount', '$1', 'display_price(Amount)');
		$this->datatables->edit_column('Date', '$1', 'efree_date_format(Date, true)');

		echo $this->datatables->generate();
	}
}