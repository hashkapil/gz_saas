<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_account extends Front_controller {
	public function __construct(){
		parent::__construct();

		$this->userauth->is_login();

		$this->load->model('ct_account_m');
		$this->load->model('ct_cc_m');
        $this->load->model('db_portal_info_m');
		$this->config->load('paypal');
	}

	public function index()
	{
		$this->load->model(array('db_country_m', 'db_state_m'));

		$data['current_page'] = 'MY_ACCOUNT';

		$this->post_process();

		$sess_user_account_number = $this->session->userdata('sess_user_account_number');

		$portal_account_info = $this->ct_account_m->get_account_info("Account_Number = '" . $sess_user_account_number . "'");
		$data['portal_account_info'] = $portal_account_info;

		$cc_info = $this->ct_cc_m->get_cc_info("Account_Number = '" . $sess_user_account_number . "'");
		$data['cc_info'] = $cc_info;

		$data['country_array'] = $this->db_country_m->get_countries("", FALSE);
		$data['state_array'] = $this->db_state_m->get_states("", FALSE);
        //code by heng
        $this->load->model('video_popup_m');
        
        $data['selected_video'] = "Account";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);
        
		$this->load->view($this->config->item('portal_folder') . '/my_account', $data);
	}

	public function post_process(){

		$sess_user_account_number = $this->session->userdata('sess_user_account_number');

		$action_type = $this->input->post('action_type');

		if(isset($action_type) && !empty($action_type)){
			switch($action_type){
				case 'update_cc_info':
					$cc_name = $this->input->post('cc_name');
					$cc_type = $this->input->post('cc_type');
					$cc_number = $this->input->post('cc_number');
					$cc_exp_mm = $this->input->post('cc_exp_mm');
					$cc_exp_yy = $this->input->post('cc_exp_yy');
					$cc_csv = $this->input->post('cc_csv');
                    $cc_country = $this->input->post('cc_country');
                    $cc_address = $this->input->post('cc_address');
                    $cc_city = $this->input->post('cc_city');
                    $cc_state = $this->input->post('cc_state');
                    $cc_province = $this->input->post('cc_province');
                    $cc_zip_code = $this->input->post('cc_zip_code');
					
                    
                    if($cc_country == "UNITED STATES"){
                        $cc_state_province = $cc_state;
                    }else{
                        $cc_state_province = $cc_province;
                    }
					/********************Let 's validate the posted data to make sure no one can enter bad info 5/03/2016**************/
					
					//validating name. Alphabets, 3-60 chars 
					if(strlen($cc_name)<3 || strlen($cc_name)>60)
					{
						$this->session->set_flashdata('error', 'Name must be 3-60 characters long.');
						redirect(site_url('my_account'));						
					}
					if (ctype_alpha(str_replace(' ', '', $cc_name)) === false) {
						$this->session->set_flashdata('error', 'Name must contain letters and spaces only.');
						redirect(site_url('my_account'));						  
					}
					//validating card number.Numbers 16 - 18 chars
					if(strlen($cc_number)<15 || strlen($cc_number)>16)
					{
						$this->session->set_flashdata('error', 'Card Number must be 16-18 characters long.');
						redirect(site_url('my_account'));						  
					}
					if (is_numeric($cc_number)=== false) {
						$this->session->set_flashdata('error', 'Card Number must contain numbers only.');
						redirect(site_url('my_account'));						  
					}
					//validating city Alphabets or Numbers or both 3-60 chars
					if(strlen($cc_city)<3 || strlen($cc_city)>60)
					{
						$this->session->set_flashdata('error', 'City must be 3-60 characters long.');
						redirect(site_url('my_account'));			  
					}
					if (ctype_alnum(str_replace(' ', '', $cc_city)) === false) {
						$this->session->set_flashdata('error', 'City can contain letters and numbers only.');
						redirect(site_url('my_account'));							  
					}
					//validating zip Alphabets or Numbers or both 3-12 chars 
					/*if(strlen($cc_zip_code)<3 || strlen($cc_zip_code)>12)
					{
						$this->session->set_flashdata('error', 'Zip code must be 3-12 characters long.');
						redirect(site_url('my_account'));						  
					}
					if (ctype_alnum(str_replace(' ', '', $cc_zip_code)) === false) {
						$this->session->set_flashdata('error', 'Zip code can contain letters and numbers only.');
						redirect(site_url('my_account'));							  
					}
					*/
					/*********************************************************************************************************/
					$cc_values_array = array(
						'Card_Type' => $cc_type,
						'Card_Number' => $cc_number,
						'Expire_Month' => $cc_exp_mm,
						'Expire_Year' => $cc_exp_yy,
						'CSV' => $cc_csv,
						'cardholder_name' => $cc_name,
						'IP' => get_ip(),
						'Status' => 10,
						'Count' => 0,
                        'Country' => $cc_country,
                        'Address' => $cc_address,
                        'City' => $cc_city,
                        'State/Province' => $cc_state_province,
                        'Zip_Code' => $cc_zip_code,
						'profileID' => null,
						'PaymentProfileID' => null,
						'page'=>'PL'
					);
					
					$cc_info = $this->ct_cc_m->get_cc_info("Account_Number = '" . $sess_user_account_number . "'");

					if(is_array($cc_info) && count($cc_info)){
						$this->ct_cc_m->update_cc($sess_user_account_number, $cc_values_array);
					}else{
						$cc_values_array['Account_Number'] = $sess_user_account_number;
						$this->ct_cc_m->add_cc($cc_values_array);
					}

					$this->session->set_flashdata('success', 'Your credit card information has been updated successfully.');
				break;
				case 'update_account_info':
					$account_name = $this->input->post('account_name');
					$account_address = $this->input->post('account_address');
					$city = $this->input->post('city');
					$state = $this->input->post('state');
					$province = $this->input->post('province');
					$country = $this->input->post('country');
					$zip_code = $this->input->post('zip_code');
					$first_name = $this->input->post('first_name');
					$last_name = $this->input->post('last_name');
					$contact_number = $this->input->post('contact_number');
					$login = $this->input->post('login');
					$ebilling = $this->input->post('ebilling');
					$skype_id = $this->input->post('skype_id');

					$account_db_values = array(
						'Account_Name' => $account_name,
						'Address' => $account_address,
						'City' => $city,
						'State' => $state,
						'Province' => $province,
						'ZipCode' => $zip_code,
						'Country' => $country,
						'Contact_FirstName' => $first_name,
						'Contact_LastName' => $last_name,
						'Contact_Number' => $contact_number,
						'eBilling' => $ebilling,
						'Skype_id' => $skype_id
					);

					//'Login' => $login,

					$this->ct_account_m->update_account($sess_user_account_number, $account_db_values);

					$this->session->set_flashdata('success', 'Your account information has been updated successfully.');
					
				break;
				case 'make_payment':
					$total_due_amount = $this->input->post('amount');

					if($total_due_amount >= 1){
						
						$payment_status_array = $this->ct_account_m->make_payment($total_due_amount);
						
						$payment_result = $payment_status_array['result'];

						if(strrchr($payment_result, 'Failed')){
							$this->session->set_flashdata('error', $payment_result);
						}elseif(strrchr($payment_result, 'Success')){
                            $this->session->set_flashdata('action','after_payment');
                            $this->session->set_flashdata('add_amount',$total_due_amount);
                            $this->session->set_flashdata('success', $payment_result);
                        }else{
							$this->session->set_flashdata('success', $payment_result);
						}
					}else{
						$this->session->set_flashdata('error', 'Payment has not succeeded. Please try again.');
					}

				break;
			}

			redirect(site_url('my_account'));
		}
	}

	public function payment_list(){
		$this->load->model('ct_payments_m');
		$this->ct_payments_m->get_payment_listing();
	}
}