<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ring_groups extends Front_controller {
	public function __construct(){
		parent::__construct();

		$this->userauth->is_login();

		$this->load->model(array('ct_ring_groups_m', 'ring_to_type_m'));
        $this->load->model('db_portal_info_m');
	}

	public function index()
	{
        $this->load->model('video_popup_m');
        $data['selected_video'] = "Ring Groups";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");

		$data['current_page'] = 'RING_GROUPS';
		
		//Ring Types
        $data['rttimeout_list'] = $this->ring_to_type_m->get_list_rtt("TimeOutOption = '1'");
		$data['rtt_list'] = $this->ring_to_type_m->get_list_rtt("Portal_Ring_groups = '1'");
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

		$this->load->view($this->config->item('portal_folder') . '/ring_groups', $data);
	}

	public function listing(){
		$this->ct_ring_groups_m->get_listing();
	}
}