<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conference_bridge extends Front_controller {
	public function __construct(){
		parent::__construct();

		$this->userauth->is_login();

		$this->load->model('ct_conference_rooms_m');
        $this->load->model('db_portal_info_m');
	}

	public function index()
	{
        //code by heng
        $this->load->model('video_popup_m');
        
        $data['selected_video'] = "Conference Bridge";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
		$data['current_page'] = 'CONFERENCE_ROOMS';
        
		$sess_user_account_number = $this->session->userdata('sess_user_account_number');

		if(isset($_GET['v_a']) && $_GET['v_a'] == 'edit'){
			$int_id = $_GET['int_id'];
			$data['conf_info'] = $this->ct_conference_rooms_m->get_conf_room_info("`Key` = '" . $int_id ."'");
		}
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

		$this->load->view($this->config->item('portal_folder') . '/conference_rooms', $data);
	}
    
	public function listing(){
		$this->ct_conference_rooms_m->get_listing();
	}
}