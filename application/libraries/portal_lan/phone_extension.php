<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Phone_extension extends Front_controller {
	public function __construct(){
		parent::__construct();

		$this->userauth->is_login();

		$this->load->model('portal_ct_extensions_m');
        $this->load->model('db_portal_info_m');
	}

	public function index()
	{
        //code by heng
        $this->load->model('video_popup_m');
        
        $data['selected_video'] = "Phone Extension";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
		$data['current_page'] = 'PHONE_EXTENSION';
        
		$sess_user_account_number = $this->session->userdata('sess_user_account_number');
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

		$this->load->view($this->config->item('portal_folder') . '/phone_extension', $data);
	}
    
	public function listing(){
		$this->portal_ct_extensions_m->get_listing();
	}
}