<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cdr_download extends Front_controller {
    public function __construct(){
        parent::__construct();

        $this->userauth->is_login();

        $this->load->model('cdr_download_m');
        $this->load->model('db_portal_info_m');
		
    }

    public function index()
    {
        //code by heng
        $this->load->model('video_popup_m');
        
        $data['selected_video'] = "CDR Download";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
        $data['current_page'] = 'REPORTS';
        
        $this->post_process();
        
        $sess_user_account_number = $this->session->userdata('sess_user_account_number');
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

        $this->load->view($this->config->item('portal_folder') . '/cdr_download', $data);
    }
    
    public function post_process(){

        $sess_user_account_number = $this->session->userdata('sess_user_account_number');

        $action_type = $this->input->post('action_type');

        if(isset($action_type) && !empty($action_type)){

            switch($action_type){
                case 'download':
				
					$this->load->model('usage_data_m');
                    //$this->load->library('live_excel');

					$int_id = $this->input->post('int_id');
					
					$str_where = "ID = '". $int_id ."'";
                    
                    $download_info = $this->cdr_download_m->get_cdr_download_info($str_where);
                    
                    $start_date_time = $download_info['Start_Date'];
                    $end_date_time = $download_info['End_Date'];
                    $start_after_date = date("Y-m-d",(strtotime($start_date_time)+24*3600));
                    $end_before_date = date("Y-m-d",(strtotime($end_date_time)-24*3600));
                    $start_date = date("Y-m-d",strtotime($start_date_time));
                    $start_time = date("H:i:s",strtotime($start_date_time));
                    $end_date = date("Y-m-d",strtotime($end_date_time));
                    $end_time = date("H:i:s",strtotime($end_date_time));
                    
                    $str_where = "Account_Number = '".$sess_user_account_number."' AND ((OrigDate BETWEEN '".$start_after_date."' AND '".$end_before_date."') OR (OrigDate = '".$start_date."' AND OrigTime BETWEEN '".$start_time."' AND '23:59:59') OR (OrigDate = '".$end_date."' AND OrigTime BETWEEN '00:00:00' AND '".$end_time."'))";
                    
                    $cdr_list_array = $this->usage_data_m->get_cdr_list_info($str_where);					
					// output headers so that the file is downloaded rather than displayed
					 // Redirect output to a clients web browser (CSV)
                    header('Content-Type: application/csv');
                    header('Content-Disposition: attachment;filename="CDR_Download_' . date('m/d/Y') . '.csv"');
                    header('Cache-Control: max-age=0');
                    // If you're serving to IE 9, then the following may be needed
                    header('Cache-Control: max-age=1');

                    // If you're serving to IE over SSL, then the following may be needed
                    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                    header ('Pragma: public'); // HTTP/1.0

					// create a file pointer connected to the output stream
					$output = fopen('php://output', 'w');

					// output the column headings
					$field_names_array = array("Bill Period", "Orig Date", "Orig Time", "Service Type", "Call duration(seconds)", "Caller ID", "Destination Number","Rate","Charge");
					fputcsv($output, $field_names_array);
					
					foreach($cdr_list_array as $cdr_list_info){
						fputcsv($output, $cdr_list_info);
					}
					exit;
                    /*$this->load->model('usage_data_m');
                    $this->load->library('live_excel');

                    // Create new PHPExcel object
                    $objPHPExcel = new live_excel();

                    // Set document properties
                    $objPHPExcel->getProperties()->setCreator("Live BBBarch")->setLastModifiedBy("Live BBBarch")->setTitle("Live BBBarch")->setSubject("Live BBBarch")->setDescription("Live BBBarch")->setKeywords("Live BBBarch")->setCategory("Live BBBarch");

                    $int_id = $this->input->post('int_id');

                    $str_where = "ID = '". $int_id ."'";
                    
                    $download_info = $this->cdr_download_m->get_cdr_download_info($str_where);
                    
                    $start_date_time = $download_info['Start_Date'];
                    $end_date_time = $download_info['End_Date'];
                    $start_after_date = date("Y-m-d",(strtotime($start_date_time)+24*3600));
                    $end_before_date = date("Y-m-d",(strtotime($end_date_time)-24*3600));
                    $start_date = date("Y-m-d",strtotime($start_date_time));
                    $start_time = date("H:i:s",strtotime($start_date_time));
                    $end_date = date("Y-m-d",strtotime($end_date_time));
                    $end_time = date("H:i:s",strtotime($end_date_time));
                    
                    $str_where = "Account_Number = '".$sess_user_account_number."' AND ((OrigDate BETWEEN '".$start_after_date."' AND '".$end_before_date."') OR (OrigDate = '".$start_date."' AND OrigTime BETWEEN '".$start_time."' AND '23:59:59') OR (OrigDate = '".$end_date."' AND OrigTime BETWEEN '00:00:00' AND '".$end_time."'))";
                    
                    $cdr_list_array = $this->usage_data_m->get_cdr_list_info($str_where);

                    $field_names_array = array("Bill Period", "Orig Date", "Orig Time", "Service Type", "Call duration(seconds)", "Caller ID", "Destination Number");

                    $sheet_col = 'A';
                    $sheet_row = 1;

                    foreach($field_names_array as $field_name){
                        $cell_index = ($sheet_col++) . $sheet_row;
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_index, $field_name);
                        $objPHPExcel->getActiveSheet()->getStyle($cell_index)->getFont()->setBold(true);
                    }

                    $sheet_row++;
                    if(is_array($cdr_list_array) && count($cdr_list_array)){
                        foreach($cdr_list_array as $cdr_list_info){
                            $sheet_col = 'A';

                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $cdr_list_info['Billperiod']);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, efree_date_format($cdr_list_info['OrigDate']));
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $cdr_list_info['OrigTime']);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $cdr_list_info['Disc2']);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $cdr_list_info['Seconds']);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $cdr_list_info['ONumber']);
                            $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $cdr_list_info['DNumber']);

                            $sheet_row++;
                        }
                    }

                    // Rename worksheet
                    $objPHPExcel->getActiveSheet()->setTitle('CDR Download');

                    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                    $objPHPExcel->setActiveSheetIndex(0);

                    // Redirect output to a clients web browser (CSV)
                    header('Content-Type: application/csv');
                    header('Content-Disposition: attachment;filename="CDR_Download_' . date('m/d/Y') . '.csv"');
                    header('Cache-Control: max-age=0');
                    // If you're serving to IE 9, then the following may be needed
                    header('Cache-Control: max-age=1');

                    // If you're serving to IE over SSL, then the following may be needed
                    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                    header ('Pragma: public'); // HTTP/1.0

                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
                    $objWriter->save('php://output');
                    exit;*/
                break;
            }

            redirect(site_url('cdr_download'));
        }
    }
    
    public function listing(){
        $this->cdr_download_m->get_listing();
    }
    
    public function cdrs($download){
        $this->load->model('usage_data_m');
        $this->load->library('live_excel');

        // Create new PHPExcel object
        $objPHPExcel = new live_excel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Live BBBarch")->setLastModifiedBy("Live BBBarch")->setTitle("Live BBBarch")->setSubject("Live BBBarch")->setDescription("Live BBBarch")->setKeywords("Live BBBarch")->setCategory("Live BBBarch");
        
        $str_where = "Download = '". $download ."'";
        
        $download_info = $this->cdr_download_m->get_cdr_download_info($str_where);
        
        $start_date_time = $download_info['Start_Date'];
        $end_date_time = $download_info['End_Date'];
        $start_after_date = date("Y-m-d",(strtotime($start_date_time)+24*3600));
        $end_before_date = date("Y-m-d",(strtotime($end_date_time)-24*3600));
        $start_date = date("Y-m-d",strtotime($start_date_time));
        $start_time = date("H:i:s",strtotime($start_date_time));
        $end_date = date("Y-m-d",strtotime($end_date_time));
        $end_time = date("H:i:s",strtotime($end_date_time));
        
        $str_where = "Account_Number = '".$download_info['Account_Number']."' AND ((OrigDate BETWEEN '".$start_after_date."' AND '".$end_before_date."') OR (OrigDate = '".$start_date."' AND OrigTime BETWEEN '".$start_time."' AND '23:59:59') OR (OrigDate = '".$end_date."' AND OrigTime BETWEEN '00:00:00' AND '".$end_time."'))";
        
        $cdr_list_array = $this->usage_data_m->get_cdr_list_info($str_where);

        $field_names_array = array("Bill Period", "Orig Date", "Orig Time", "Service Type", "Call duration(seconds)", "Caller ID", "Destination Number");

        $sheet_col = 'A';
        $sheet_row = 1;

        foreach($field_names_array as $field_name){
            $cell_index = ($sheet_col++) . $sheet_row;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_index, $field_name);
            $objPHPExcel->getActiveSheet()->getStyle($cell_index)->getFont()->setBold(true);
        }

        $sheet_row++;
        if(is_array($cdr_list_array) && count($cdr_list_array)){
            foreach($cdr_list_array as $cdr_list_info){
                $sheet_col = 'A';

                $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $cdr_list_info['Billperiod']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, efree_date_format($cdr_list_info['OrigDate']));
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $cdr_list_info['OrigTime']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $cdr_list_info['Disc2']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $cdr_list_info['Seconds']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $cdr_list_info['ONumber']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $cdr_list_info['DNumber']);

                $sheet_row++;
            }
        }

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('CDR Download');

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Redirect output to a clients web browser (CSV)
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment;filename="CDR_Download_' . date('m/d/Y') . '.csv"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save('php://output');
        exit;
    }
    
}
