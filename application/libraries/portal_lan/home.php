<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Front_controller {
	public function __construct(){
		parent::__construct();
		
		$this->userauth->is_login();

		$this->load->model('usage_by_period_m');
		$this->load->model('ct_cc_m');
        $this->load->model('db_portal_info_m');		
	
	}

	public function index()
	{		
		//print_r($this->db);
		//die('');return;
		$this->load->model(array('portal_account_info_m', 'ct_account_message_m', 'report_portal_calls_m'));

		$data['current_page'] = 'HOME';

		$sess_user_account_number = $this->session->userdata('sess_user_account_number');

		$portal_account_info = $this->portal_account_info_m->get_account_info("Account_Number = '" . $sess_user_account_number . "'");
		$data['portal_account_info'] = $portal_account_info;

		$cc_info = $this->ct_cc_m->get_cc_info("Account_Number = '" . $sess_user_account_number . "'");
		$data['cc_info'] = $cc_info;

		$account_messages_array = $this->ct_account_message_m->get_account_messages("Account_Number = '" . $sess_user_account_number . "'");
		$data['account_messages_array'] = $account_messages_array;
        
        $account_messages_portal_array = $this->ct_account_message_m->get_account_messages_portal("Account_Number = '" . $sess_user_account_number . "' AND Display = 1");
        $data['account_messages_portal_array'] = $account_messages_portal_array;

		$day_chart_rpt_array = $this->report_portal_calls_m->get_reports_day_data("Account_Number = '" . $sess_user_account_number . "' AND (Date BETWEEN (DATE_SUB( CURRENT_DATE(), INTERVAL 30 DAY )) AND CURRENT_DATE())");

		$total_day_calls = array();
		$per_day_calls = array();

		if(is_array($day_chart_rpt_array) && count($day_chart_rpt_array)){
			foreach($day_chart_rpt_array as $day_chart_rpt_info){

				$call_type = $day_chart_rpt_info['Call_Type'];
				$date = $day_chart_rpt_info['Date'];
				$per_day_calls[$date][$call_type] = array();

				@$per_day_calls[$date][$call_type]['Total_Calls'] += $day_chart_rpt_info['Total_Calls'];
				@$per_day_calls[$date][$call_type]['Total_Minutes'] += $day_chart_rpt_info['Total_Minutes'];

				if(isset($total_day_calls[$call_type]['Total_Calls'])){
					$total_day_calls[$call_type]['Total_Calls'] += $day_chart_rpt_info['Total_Calls'];
				}else{
					$total_day_calls[$call_type]['Total_Calls'] = $day_chart_rpt_info['Total_Calls'];
				}

				if(isset($total_day_calls[$call_type]['Total_Minutes'])){
					$total_day_calls[$call_type]['Total_Minutes'] += $day_chart_rpt_info['Total_Minutes'];
				}else{
					$total_day_calls[$call_type]['Total_Minutes'] = $day_chart_rpt_info['Total_Minutes'];
				}
			}
		}

		$data['total_day_calls'] = $total_day_calls;
		$data['per_day_calls'] = $per_day_calls;

		$week_chart_rpt_array = $this->report_portal_calls_m->get_reports_week_data("Account_Number = '" . $sess_user_account_number . "' AND Week >= (WEEK(DATE_SUB( CURRENT_DATE(), INTERVAL 8 WEEK ))) AND Year = YEAR(CURDATE())");

		$total_week_calls = array();
		$per_week_calls = array();

		if(is_array($week_chart_rpt_array) && count($week_chart_rpt_array)){
			foreach($week_chart_rpt_array as $week_chart_rpt_info){

				$call_type = $week_chart_rpt_info['Call_Type'];
				$week = $week_chart_rpt_info['Week'] .'_'. $week_chart_rpt_info['Date'];
				$per_week_calls[$week][$call_type] = array();

				@$per_week_calls[$week][$call_type]['Total_Calls'] += $week_chart_rpt_info['Total_Calls'];
				@$per_week_calls[$week][$call_type]['Total_Minutes'] += $week_chart_rpt_info['Total_Minutes'];

				if(isset($total_week_calls[$call_type]['Total_Calls'])){
					$total_week_calls[$call_type]['Total_Calls'] += $week_chart_rpt_info['Total_Calls'];
				}else{
					$total_week_calls[$call_type]['Total_Calls'] = $week_chart_rpt_info['Total_Calls'];
				}

				if(isset($total_week_calls[$call_type]['Total_Minutes'])){
					$total_week_calls[$call_type]['Total_Minutes'] += $week_chart_rpt_info['Total_Minutes'];
				}else{
					$total_week_calls[$call_type]['Total_Minutes'] = $week_chart_rpt_info['Total_Minutes'];
				}
			}
		}

		$data['total_week_calls'] = $total_week_calls;
		$data['per_week_calls'] = $per_week_calls;

		$total_month_calls = array();
		$per_month_calls = array();

		$month_chart_rpt_array = $this->report_portal_calls_m->get_reports_month_data("Account_Number = '" . $sess_user_account_number . "' AND Year = YEAR(CURDATE())");

		if(is_array($month_chart_rpt_array) && count($month_chart_rpt_array)){
			foreach($month_chart_rpt_array as $month_chart_rpt_info){

				$call_type = $month_chart_rpt_info['Call_Type'];
				$date = date("M Y", strtotime($month_chart_rpt_info['Year'] . '-' . $month_chart_rpt_info['Month'] . '-01'));
				$per_month_calls[$date][$call_type] = array();

				@$per_month_calls[$date][$call_type]['Total_Calls'] += $month_chart_rpt_info['Total_Calls'];
				@$per_month_calls[$date][$call_type]['Total_Minutes'] += $month_chart_rpt_info['Total_Minutes'];

				if(isset($total_month_calls[$call_type]['Total_Calls'])){
					$total_month_calls[$call_type]['Total_Calls'] += $month_chart_rpt_info['Total_Calls'];
				}else{
					$total_month_calls[$call_type]['Total_Calls'] = $month_chart_rpt_info['Total_Calls'];
				}

				if(isset($total_month_calls[$call_type]['Total_Minutes'])){
					$total_month_calls[$call_type]['Total_Minutes'] += $month_chart_rpt_info['Total_Minutes'];
				}else{
					$total_month_calls[$call_type]['Total_Minutes'] = $month_chart_rpt_info['Total_Minutes'];
				}
			}
		}

		$data['total_month_calls'] = $total_month_calls;
		$data['per_month_calls'] = $per_month_calls;

		//echo '<pre>'; print_r($per_month_calls); exit;

		$data['day_chart_box_array'] = $this->report_portal_calls_m->get_chart_boxes_data("Account_Number = '" . $sess_user_account_number . "' AND Period = 'DAY'", "Call_Type");
		$data['week_chart_box_array'] = $this->report_portal_calls_m->get_chart_boxes_data("Account_Number = '" . $sess_user_account_number . "' AND Period = 'WEEK'", "Call_Type");
		$data['month_chart_box_array'] = $this->report_portal_calls_m->get_chart_boxes_data("Account_Number = '" . $sess_user_account_number . "' AND Period = 'MONTH'", "Call_Type");

		$data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);
        
        $this->load->view($this->config->item('portal_folder') . '/home', $data);
	}

	public function get_chart_data(){
		$chart_type = $_GET['ct'];

		switch($chart_type){
			case 'current_year_mou_chart':
				$current_year_mou_array = $this->usage_by_period_m->get_chart_mou();
				if(is_array($current_year_mou_array) && count($current_year_mou_array)){
					$result_json = json_encode($current_year_mou_array);
					echo $result_json;
				}
			break;
			case 'last_year_mou_chart':	
				$last_year_mou_array = $this->usage_by_period_m->get_chart_mou((date("Y")-1));
				if(is_array($last_year_mou_array) && count($last_year_mou_array)){
					$result_json = json_encode($last_year_mou_array);
					echo $result_json;
				}
			break;
			case 'current_year_calls_chart':
				$current_year_calls_array = $this->usage_by_period_m->get_chart_calls();
				if(is_array($current_year_calls_array) && count($current_year_calls_array)){
					$result_json = json_encode($current_year_calls_array);
					echo $result_json;
				}
			break;
			case 'last_year_calls_chart':
				$last_year_calls_array = $this->usage_by_period_m->get_chart_calls((date("Y")-1));
				if(is_array($last_year_calls_array) && count($last_year_calls_array)){
					$result_json = json_encode($last_year_calls_array);
					echo $result_json;
				}
			break;
			case 'current_year_service_type_chart':
				$current_year_service_array = $this->usage_by_period_m->get_chart_service_type();
				if(is_array($current_year_service_array) && count($current_year_service_array)){
					$result_json = json_encode($current_year_service_array);
					echo $result_json;
				}
			break;
		}
	}

	public function show_404(){
		redirect(site_url('home'), 'location');
	}
}