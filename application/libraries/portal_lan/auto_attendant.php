<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auto_attendant extends Front_controller {
    public function __construct(){
        parent::__construct();

        $this->userauth->is_login();

        $this->load->model(array('auto_attendant_m', 'ring_to_type_m'));
        $this->load->model('db_portal_info_m');
    }

    public function index()
    {
        //code by heng
        $this->load->model('video_popup_m');
        
        $data['selected_video'] = "Auto Attendant";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
        $data['current_page'] = 'AUTO_ATTENDANT';

		//Ring Types
        $data['rttimeout_list'] = $this->ring_to_type_m->get_list_rtt("TimeOutOption = '1'");
		$data['rtt_list'] = $this->ring_to_type_m->get_list_rtt("Portal_AutoATT = '1'");
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

        $this->load->view($this->config->item('portal_folder') . '/auto_attendant', $data);
    }

    public function listing(){
        $this->auto_attendant_m->get_listing();
    }
}