<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Call_screen extends Front_controller {
	public function __construct(){
		parent::__construct();

		$this->userauth->is_login();

		$this->load->model(array('ct_call_screen_m', 'portal_voicemail_m'));
        $this->load->model('db_portal_info_m');
	}

	public function index()
	{
        //code by heng
        $this->load->model('video_popup_m');
        
        $data['selected_video'] = "Call Screening";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
		$data['current_page'] = 'CALL_SCREEN';

		$sess_account_number = $this->session->userdata('sess_user_account_number');
		$data['voicemail_array'] = $this->portal_voicemail_m->get_voice_mail_info("Account_Number = '" . $sess_account_number . "'", FALSE);
        
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);

		$this->load->view($this->config->item('portal_folder') . '/call_screen', $data);
	}

	public function listing(){
		$this->ct_call_screen_m->get_listing();
	}
}