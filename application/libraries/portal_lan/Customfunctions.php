<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customfunctions {

    public function __construct() {
        $CI = &get_instance();
        $CI->load->library('session');
        $CI->load->model('Admin_model', '', TRUE);
        $CI->load->model('Request_model', '', TRUE);
        $CI->load->model('Admin_model', '', TRUE);
        $CI->load->model('Category_model', '', TRUE);
        $CI->load->library('Myfunctions');

    }

    public function getprofileimageurl($img) {
       if($img != NULL){
           if(file_get_contents(FS_PATH. FS_UPLOAD_PUBLIC_UPLOADS_PROFILE ."_thumb/". $img)){
               $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE ."_thumb/".$img;
           }else{
               $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE .$img;
           }
       }else{
           $path =  FS_PATH_PUBLIC_UPLOADS_PROFILE ."user-admin.png";
       }
       return $path;
    }
    
    public function getprojectteammembers($customer_id,$designer_id="") {
        $CI = &get_instance();
        $data = array();
        $customer = $CI->Admin_model->getuser_data($customer_id);
        $data[0]['customer_image'] = $CI->customfunctions->getprofileimageurl($customer[0]['profile_picture']);
        $qa = array();
        if (!empty($customer)) {
            $data[0]['customer_name'] = $customer[0]['first_name'] . " " . $customer[0]['last_name'];
            $qa = $CI->Admin_model->getuser_data($customer[0]['qa_id']);
            $data[0]['qa_image'] = $CI->customfunctions->getprofileimageurl($qa[0]['profile_picture']);
            if (!empty($qa)) {
                $data[0]['qa_name'] = $qa[0]['first_name'] . " " . $qa[0]['last_name'];
            }else{
                $data[0]['qa_name'] = '';
            }
        }else{
            $data[0]['customer_name'] = '';
        }
        $designer = $CI->Admin_model->getuser_data($designer_id);
        $data[0]['designer_image'] = $CI->customfunctions->getprofileimageurl($designer[0]['profile_picture']);
        if (!empty($designer)) {
            $data[0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
        } else {
            $data[0]['designer_name'] = "";
        }
        //echo "<pre>";print_r($data);
        return $data;
    }
}
