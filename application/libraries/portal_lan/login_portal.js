$(document).ready(function() {
	jQuery.validator.addMethod("uniqueEmail", function(value, element) {
		var response;
		$.ajax({
			type: "POST",
			url: _AJAX_PROCESS_URL,
			data:"action_type=check_email_availablity&email="+value,
			dataType:"json",
			async:false,
			success:function(data){
				response = data;
			}
		});

		if(response == 1)
		{
			return false;
		}else{
			return true;
		}
	}, "Email address has already registered with us.");

	jQuery.validator.addMethod("invalid_email", function(value, element) {
		var response;
		$.ajax({
			type: "POST",
			url: _AJAX_PROCESS_URL,
			data:"action_type=check_email_availablity&email="+value,
			dataType:"json",
			async:false,
			success:function(data){
				response = data;
			}
		});

		if(response == 1)
		{
			return true;
		}else{
			return false;
		}
	}, "Email address has not been registered with us.");


	$.validator.messages.required = "";
	$("#frmLogin").validate();
	$("#frmRegister").validate();
	$("#frmForgotPass").validate();
} );

function toggle_province(){
	if($('select[name="country"]').val() == 'UNITED STATES'){
		$('.blk_province').hide();
		$('.blk_state').show();
	}else{
		$('.blk_province').show();
		$('.blk_state').hide();
	}
}