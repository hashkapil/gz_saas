<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Call_log extends Front_controller {
	public function __construct(){
		parent::__construct();

		$this->userauth->is_login();

		$this->load->model('usage_data_m');
        $this->load->model('db_portal_info_m');
	}

	public function index()
	{
        //code by heng
        $this->load->model('video_popup_m');
        
        $data['selected_video'] = "Call Log";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
		$data['current_page'] = 'REPORTS';

		$this->post_process();

		$sess_user_account_number = $this->session->userdata('sess_user_account_number');
       
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);
		$this->load->view($this->config->item('portal_folder') . '/call_log', $data);
	}

	public function post_process(){

		$sess_user_account_number = $this->session->userdata('sess_user_account_number');

		$action_type = $this->input->post('action_type');

		switch($action_type){
			case 'update_cc_info':
				$cc_name = $this->input->post('cc_name');
				$cc_type = $this->input->post('cc_type');
				$cc_number = $this->input->post('cc_number');
				$cc_exp_mm = $this->input->post('cc_exp_mm');
				$cc_exp_yy = $this->input->post('cc_exp_yy');
				$cc_csv = $this->input->post('cc_csv');

				$cc_values_array = array(
					'Card_Type' => $cc_type,
					'Card_Number' => $cc_number,
					'Expire_Month' => $cc_exp_mm,
					'Expire_Year' => $cc_exp_yy,
					'CSV' => $cc_csv,
					'cardholder_name' => $cc_name,
					'IP' => $this->input->ip_address()
				);

				$cc_info = $this->portal_ct_extensions_m->get_cc_info("Account_Number = '" . $sess_user_account_number . "'");

				if(is_array($cc_info) && count($cc_info)){
					$this->portal_ct_extensions_m->update_cc($sess_user_account_number, $cc_values_array);
				}else{
					$cc_values_array['Account_Number'] = $sess_user_account_number;
					$this->portal_ct_extensions_m->add_cc($cc_values_array);
				}

				$this->session->set_flashdata('success', 'Your credit card information has been updated successfully.');

				redirect(site_url('my_account'));

			break;
		}
	}

	public function listing(){
		
			
		$this->usage_data_m->get_listing();
	}
}
