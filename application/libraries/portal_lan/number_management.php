<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Number_management extends Front_controller {
	public function __construct(){
		parent::__construct();

		$this->userauth->is_login();

		$this->load->model('portal_number_list_m');
		$this->load->model('ct_tn_m');
        $this->load->model('db_portal_info_m');
	}

	public function index()
	{
		$this->load->model(array('ring_to_type_m', 'ct_extentions_m', 'ct_call_screen_m', 'ct_conference_m', 'portal_voicemail_m', 'auto_attendant_m', 'ct_ring_groups_m'));

		$data['current_page'] = 'NUMBER_MANAGEMENT';
		$this->post_process();

		$sess_user_account_number = $this->session->userdata('sess_user_account_number');

		$data['rtt_list'] = $this->ring_to_type_m->get_list_rtt("Portal_TN = '1'");
		$data['extension_list'] = $this->ct_extentions_m->get_list_extensions();
		$data['call_screen_list'] = $this->ct_call_screen_m->get_call_screen_info("Account_Number = '" . $this->session->userdata('sess_user_account_number') . "'", FALSE);
		$data['conference_list'] = $this->ct_conference_m->get_list_conference("Account_Number = '" . $this->session->userdata('sess_user_account_number') . "'");
		$data['voice_mail_list'] = $this->portal_voicemail_m->get_ct_voice_mail_info("customer_id = '" . $this->session->userdata('sess_user_account_number') . "'", FALSE);
		$auto_attendant_list = $this->auto_attendant_m->get_auto_attendant_info("Account_Number = '" . $this->session->userdata('sess_user_account_number') . "'", FALSE, FALSE);
		$data['auto_attendant_list'] = $auto_attendant_list['main'];
        $data['ring_group_list'] = $this->ct_ring_groups_m->get_ring_group_info("Account_Number = '" . $this->session->userdata('sess_user_account_number') . "'", FALSE);

        //code by heng
        $this->load->model('video_popup_m');
        
        $data['selected_video'] = "Telephone Numbers";
        $data['title_list'] = $this->video_popup_m->get_title_list();
        $data['video_link'] = $this->video_popup_m->get_video_link("video_title = '" . $data['selected_video'] . "'");
        // end
        $data['portal_info'] = $this->db_portal_info_m->get_version("", TRUE);
        
		$this->load->view($this->config->item('portal_folder') . '/number_management', $data);
	}

	public function post_process(){

		$sess_user_account_number = $this->session->userdata('sess_user_account_number');

		$action_type = $this->input->post('action_type');

		if(isset($action_type) && !empty($action_type)){

			switch($action_type){
				case 'export_selected':
				case 'export_all':
					$this->load->library('live_excel');

					// Create new PHPExcel object
					$objPHPExcel = new live_excel();

					// Set document properties
					$objPHPExcel->getProperties()->setCreator("Live BBBarch")->setLastModifiedBy("Live BBBarch")->setTitle("Live BBBarch")->setSubject("Live BBBarch")->setDescription("Live BBBarch")->setKeywords("Live BBBarch")->setCategory("Live BBBarch");

					$did_array = $this->input->post('chkDID');

					$str_where = "Account_Number = '" . $this->session->userdata('sess_user_account_number') . "'";

					if($action_type == 'export_selected'){
						$str_where .= " AND DID IN ('" . implode("','", $did_array) . "')";
					}

					$number_list_array = $this->portal_number_list_m->get_number_list_info($str_where, FALSE);

					$field_names_array = array("Service Type", "Telephone Number", "Status", "Ring Type", "Destination", "Note", "Last Update");

					$sheet_col = 'A';
					$sheet_row = 1;

					foreach($field_names_array as $field_name){
						$cell_index = ($sheet_col++) . $sheet_row;
						$objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_index, $field_name);
						$objPHPExcel->getActiveSheet()->getStyle($cell_index)->getFont()->setBold(true);
					}

					$sheet_row++;
					if(is_array($number_list_array) && count($number_list_array)){
						foreach($number_list_array as $number_list_info){
							$sheet_col = 'A';

							$objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $number_list_info['Service Type']);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, display_phone_number($number_list_info['DID']));
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $number_list_info['Status']);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $number_list_info['Ring Type']);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $number_list_info['Destination']);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, $number_list_info['Note']);
							$objPHPExcel->setActiveSheetIndex(0)->setCellValue(($sheet_col++) . $sheet_row, efree_date_format($number_list_info['Date']));

							$sheet_row++;
						}
					}

					// Rename worksheet
					$objPHPExcel->getActiveSheet()->setTitle('Number Management');

					// Set active sheet index to the first sheet, so Excel opens this as the first sheet
					$objPHPExcel->setActiveSheetIndex(0);

					// Redirect output to a clients web browser (Excel5)
					header('Content-Type: application/vnd.ms-excel');
					header('Content-Disposition: attachment;filename="Number_Management_' . date('m/d/Y') . '.xls"');
					header('Cache-Control: max-age=0');
					// If you're serving to IE 9, then the following may be needed
					header('Cache-Control: max-age=1');

					// If you're serving to IE over SSL, then the following may be needed
					header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
					header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
					header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
					header ('Pragma: public'); // HTTP/1.0

					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
					$objWriter->save('php://output');
					exit;
				break;
			}

			redirect(site_url('number_management'));
		}
	}

	public function listing(){
		$this->portal_number_list_m->get_listing();
	}
}