<?php

/**
 * Amazon S3 Upload PHP class
 *
 * @version 0.1
 */
class S3_upload {

    function __construct($config = array()) {
        $this->CI = & get_instance();
        $this->CI->load->library('s3');

        $this->CI->config->load('s3', TRUE);
        $s3_config = $this->CI->config->item('s3');
        $this->bucket_name = $s3_config['bucket_name'];
        $this->upload_path = $s3_config['folder_name'];
        $this->s3_url = $s3_config['s3_url'];
        
        empty($config) OR $this->initialize($config, FALSE);
        
        $this->allowed_types = '*';
        $this->max_size =  0;
        $this->file_name = '';
    }

    public function initialize(array $config = array()){
        foreach ($config as $key => &$value){
            $this->$key = $value; 
        }
        return $this;
    }
    /**
     * Verify that the filetype is allowed
     *
     * @param	bool	$ignore_mime
     * @return	bool
     */
    public function is_allowed_filetype($file_path_name) {
        $output = array();
        $output['status'] = false;
        $output['error_msg'] = '';

        if ($this->allowed_types === '*') {
            $output['status'] = true;
            return $output;
        }

        if (empty($this->allowed_types) OR ! is_array($this->allowed_types)) {
            $output['error_msg'] = 'Allowed file types not specified';
            return $output;
        }

        $file = pathinfo($file_path_name);
        $ext = strtolower(ltrim($file['extension'], '.'));

        if (!in_array($ext, $this->allowed_types, TRUE)) {
            $output['error_msg'] = 'File extenstion not allowed (' . $ext . ')';
            return $output;
        }

        $output['status'] = true;
        return $output;
    }

    public function is_allowed_filesize($file_size) {
        return ($this->max_size === 0 OR $this->max_size > $file_size);
    }
    

    function upload_file($file_name) {
        //die('dfgoihj');
        $file_path = $_FILES[$file_name];
      //  print_r($file_path);
        $output = array();
        $output['status'] = false;
        $output['error_msg'] = '';
        // Is the file type allowed to be uploaded?
        $allowed = $this->is_allowed_filetype($file_path['name']);
        if (!$allowed['status']) {
            //echo "wrong";exit;
            $output['status'] = false;
            $output['error_msg'] = $allowed['error_msg'];
            return $output;
        }
        if (! $this->is_allowed_filesize($file_path['size'])) {
            //echo "size";exit;
            $output['status'] = false;
            $output['error_msg'] = 'File size is larger than the allowed file size';
            return $output;
        }

        // generate unique filename
        if($this->file_name == ''){
            $file = pathinfo($file_path['name']);
            $s3_file = $file['filename'] .'.' .strtolower($file['extension']);
        } else{
            $s3_file = $this->file_name;
        }
        //$mime_type = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $file_path);
        $saved = S3::putObject(
                        S3::inputFile($file_path['tmp_name']), $this->bucket_name, $this->upload_path . $s3_file, S3::ACL_PUBLIC_READ, array(), array(// Custom $requestHeaders
                    "Cache-Control" => "max-age=315360000",
                    "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                        )
        );
        if ($saved) {
            //echo $this->s3_url . $this->bucket_name . '/' . $this->upload_path . $s3_file;exit;
            $output['error_msg'] = $this->s3_url . $this->bucket_name . '/' . $this->upload_path . $s3_file;
            $output['status'] = true;
            return $output;
        } else {
            //echo 'not saved';
            $output['error_msg'] = '';
            $output['status'] = false;
           return $output;
        }
    }
    function upload_multiple_file($file_name) {
        //  @mkdir('./tmp',0777);
        $output = array();
        if (!file_exists('./tmp/tmpfile')) {   
            if (@mkdir('./tmp/tmpfile',0777)) {
               
            } else {
               die("Error: Dir not writable.");
            }
        }
        // Create temp file
        $tempFilePath = './tmp/tmpfile/' . basename($file_name);
        $s3_file = basename($file_name);
        $tempFile = fopen($tempFilePath, "w") or die("Error: Unable to open file.");
        $arrContextOptions = array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false));  
        $fileContents = file_get_contents($file_name,false, stream_context_create($arrContextOptions));
        if($fileContents != '' || $fileContents != null){
            //echo $fileContents;exit;
            $tempFile = file_put_contents($tempFilePath, $fileContents);
            // Put on S3
            $saved = S3::putObject(
                            S3::inputFile($tempFilePath), $this->bucket_name, $this->upload_path . $s3_file, S3::ACL_PUBLIC_READ, array(), array(// Custom $requestHeaders
                        "Cache-Control" => "max-age=315360000",
                        "Expires" => gmdate("D, d M Y H:i:s T", strtotime("+5 years"))
                            )
            );
            if ($saved) {
                // echo $this->s3_url . $this->bucket_name . '/' . $this->upload_path . $s3_file .'<br>'. "success";
                $output['status'] = true;
                return $output;
            } else {
                //  echo "error";
                $output['status'] = false;
                return $output;
            }
        } else {
            //  echo "error";
            $output['status'] = false;
            return $output;
        }
    }
    
    function delete_file_from_s3($filename) {
        $delete_file = S3::deleteObject($this->bucket_name,$filename);
                   if($delete_file){
//                       echo $filename. ' file delete';exit;
                       return true;
                   }else{
//                       echo $filename. ' file not delete';exit; 
                     return false;
                   }
    }
    
    public function copyexistfiles($srcpath,$destpath,$filename){
      $output = array();
      
      $response = S3::copyObject($this->bucket_name,$srcpath.$filename,$this->bucket_name, $destpath.$filename);
     if($response){
            $output['status'] = true;
            return $output;
        } else {
            $output['status'] = false;
            return $output;
        }
    }

}
