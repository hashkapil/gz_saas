<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Myfunctions {

    public function __construct() {
        $userdata = array();
        $CI = &get_instance();
        $CI->load->library('session');
        $login_user_id = $_SESSION['user_id'];
        $CI->load->model('Admin_model', '', TRUE);
        $CI->load->model('Request_model', '', TRUE);
        $CI->load->model('Category_model', '', TRUE);
        $CI->load->model('Clients_model', '', TRUE);
        $userData = $this->setUser($login_user_id);
        $userData['login_user_id'] = $_SESSION['user_id'];
        $userData['admin_id'] = "8";
        $canuseradd_brand_pro = $this->isUserPermission('add_brand_pro');
        $userData['canuseradd_brand_pro'] = $canuseradd_brand_pro;
        $CI->load->vars($userData);
        $subdomain_data = $this->getsubdomain_userdata();
        $CI->load->vars($subdomain_data);
        $payment_failusers = $this->faliedpaymentstatususer($login_user_id);
        $CI->load->vars($payment_failusers);
        $this->setaffiliatedcookie();
    }

    public function setUser($login_user_id) {
        $CI = &get_instance();
        $userdata = array();
        $profile_data = $CI->Admin_model->getuser_data($login_user_id);
        $userdata['login_user_data'] = $profile_data;
        if ($profile_data[0]['parent_id'] == 0) {
            $userdata['main_user'] = $_SESSION['user_id'];
        } else {
            $userdata['main_user'] = $profile_data[0]['parent_id'];
        }
        $userdata['parent_user_data'] = $CI->Admin_model->getuser_data($userdata['main_user']);
        if ($profile_data[0]['parent_id'] != 0 && $profile_data[0]['parent_id'] != $login_user_id) {
            $parent_user_id = $profile_data[0]['parent_id'];
            $userdata['parent_user_id'] = $parent_user_id;
        } else {
            $userdata['parent_user_id'] = 0;
        }
        if($profile_data[0]['parent_id'] != 0 && $profile_data[0]['user_flag'] == "client"){
            $plan_name = $userdata['login_user_data'][0]['plan_name'];
        }else{
            $plan_name = $userdata['parent_user_data'][0]['plan_name'];
        }
        $userdata['parent_user_plan'] = $CI->Request_model->getsubscriptionlistbyplanid($userdata['parent_user_data'][0]['plan_name']);
        $userdata['login_user_plan'] = $CI->Request_model->getsubscriptionlistbyplanid($plan_name);
        $userdata['agency_info'] = $CI->Request_model->getAgencyuserinfo($userdata['main_user']);
        $userdata['add_user_limit'] = $userdata['parent_user_plan'][0]['total_sub_user'];
        $userdata['user_plan_name'] = $userdata['parent_user_plan'][0]['plan_name'];
        $sub_users = $CI->Request_model->getAllsubUsers($login_user_id,"manager","","1"); 
        $sub_userspermissions = $CI->Request_model->getuserpermissions($login_user_id);
        $userdata['sub_userspermissions'] = $sub_userspermissions;
        $userdata['sub_usersbrands'] = $CI->Request_model->getuserspermisionBrands($login_user_id);
        $userdata['total_sub_users'] = $sub_users;
        if(($userdata['login_user_data'][0]['parent_id'] == 0 && $userdata['parent_user_plan'][0]['is_agency'] == 1) || ($userdata['login_user_data'][0]['user_flag'] == "manager" && $sub_userspermissions[0]['white_label'] == 1) || $userdata['login_user_data'][0]['is_saas'] == 1){
            $userdata['show_agency_setting'] = 1;
        }
        if(empty($userdata['agency_info']) || $userdata['agency_info'][0]['online_payment'] == 0){
            $userdata['pay_offline'] = 1;
        }
        return $userdata;
    }

    public function onlytimezoneforall($datetime, $timezone = "",$dateformat = "") {
        if ($timezone != '') {
            $gettimezone = $timezone;
        } else {
            $gettimezone = $_SESSION['timezone'];
        }
        $tz = date_default_timezone_get($gettimezone);
        $stamp = strtotime($datetime);
        date_default_timezone_set($gettimezone);
        if($dateformat != ''){
         $nextdate = date($dateformat,$stamp);
        }else{
            $nextdate = date('M d, Y h:i A', $stamp);
        }
        //$nextdate = date('M d, Y h:i A', $stamp);
        date_default_timezone_set($tz);
        return $nextdate;
    }

    public function check_timezone($date_serv, $planname = "", $bucketType = "", $timeline = "", $turnaround = "") {
        $CI = & get_instance();
        $gmttime = $CI->myfunctions->onlytimezoneforall($date_serv, 'America/Chicago');
        $gmt_datetime = new DateTime($gmttime);
        $gmt_date = $gmt_datetime->format('Y-m-d');
        $gmt_datere = $gmt_datetime->format('Y-m-d H:i:s');
        $gmt_time = $gmt_datetime->format('H:i:s');
        $hosttime = new DateTime($gmttime);


        $utctime = $date_serv;
        $getday_datetime = new DateTime($utctime);
        $datetime = new DateTime($utctime);
        $datere = $datetime->format('Y-m-d H:i:s');


        $date = strtotime($gmt_datere);
        $date = date("l", $date);

        /*         * *** check user have 99 plan or not *** */
//         if($planname != '' && $planname == SUBSCRIPTION_99_PLAN){
//           $calculation_time = $timeline*2; 
//         }else{
        $calculation_time = $timeline * $turnaround;
        //}
        if (strlen($calculation_time) <= 1) {
            $caltime = "0" . $calculation_time . ":00:00";
        } else {
            $caltime = "$calculation_time:00:00";
        }

        if (($bucketType == '' || $bucketType == 0) && $timeline == '') {
//            echo "its working<br>";
//         if($planname != '' && $planname == SUBSCRIPTION_99_PLAN){
//           $calcu_time = 48;  
//         }else{
            $calcu_time = 24 * $turnaround;
//         }  
        }
        /*         * *************************** */

        $plusday = "";
        /*         * *** if bucket type and timeline is not blank  **************** */
        if ($bucketType == 1) {

            $start_time = WORKING_TIME_START;
            $end_time = WORKING_TIME_END;
            $todaystrttime = $gmt_date . ' ' . $start_time;
            $todayendtime = $gmt_date . ' ' . $end_time;
            $todaydatetime = $gmt_datere;

            $workingdate = strtotime($todaystrttime);
            $workingday = date("l", $workingdate);
            $day_array = array("Saturday", "Sunday");



            //Check working day according utc time
            if (in_array($workingday, $day_array)) {
                if ($workingday == "Saturday") {
                    $gmt_datetime->modify('+2 day');
                } else {
                    $gmt_datetime->modify('+1 day');
                }
                $afterupdate = $gmt_datetime->format('Y-m-d');
                $nextday_starttime = $afterupdate . ' ' . $start_time;
                $nextday_endtime = $afterupdate . ' ' . $end_time;

                $worktmleftt = $CI->myfunctions->gethours_minutes($todaydatetime, $nextday_starttime);
                $result = $CI->myfunctions->sum_the_datetime($worktmleftt, $caltime);
                $result_arr = explode(":", $result);
                $result_hours = $result_arr[0];
                $result_minutes = $result_arr[1];
                $result_seconds = $result_arr[2];
            } else {

                //difference between strating and gmt time
                $diff = strtotime($todayendtime) - strtotime($todaydatetime);
                $worktmleft = $CI->myfunctions->gethours_minutes($todaydatetime, $todayendtime);

                //time difference between start time and end time 
                $diffstrtandend = $CI->myfunctions->gethours_minutes($gmt_date . ' ' . $start_time, $todayendtime);

                //time difference between end time and next day start time 
                $totaltimediff = $CI->myfunctions->gethours_minutes($gmt_date . ' ' . $diffstrtandend, $gmt_date . ' 24:00:00');

                //get seconds of time
                $worktmleft_sec = $CI->myfunctions->coverttimetosecond($worktmleft);
                $timeline_sec = $CI->myfunctions->coverttimetosecond($caltime);
                $gmt_time_sec = $CI->myfunctions->coverttimetosecond($gmt_time);
                $start_time_sec = $CI->myfunctions->coverttimetosecond($start_time);
                $end_time_sec = $CI->myfunctions->coverttimetosecond($end_time);
                $workinghours_sec = $CI->myfunctions->coverttimetosecond($diffstrtandend);


                $addtimetonextday = "00:00:00";
                if ($timeline_sec > $workinghours_sec) {
                    $addtimetonextday = $totaltimediff;
                }


                if ($diff > 0 && $worktmleft_sec < $timeline_sec) {
                    // echo "worki time left<br>";
                    $result = $CI->myfunctions->sum_the_datetime($totaltimediff, $caltime);
                    $totalhours = $CI->myfunctions->sum_the_datetime($result, $addtimetonextday);
                    $result_arr = explode(":", $totalhours);
                    $result_hours = $result_arr[0];
                    $result_minutes = $result_arr[1];
                    $result_seconds = $result_arr[2];
                } else {

                    //echo "no worki time left<br>";
                    if ($gmt_time_sec <= $start_time_sec) {
                        $diff_strtndend = $CI->myfunctions->gethours_minutes($todaydatetime, $todaystrttime);
                        $addresult = $CI->myfunctions->sum_the_datetime($diff_strtndend, $caltime);
                        $totalhours = $CI->myfunctions->sum_the_datetime($addresult, $addtimetonextday);
                        $result_arr = explode(":", $totalhours);
                        $result_hours = $result_arr[0];
                        $result_minutes = $result_arr[1];
                        $result_seconds = $result_arr[2];
                    } elseif ($gmt_time_sec >= $end_time_sec) {

                        $tommorow = $gmt_datetime->modify("+1 day");
                        $tommorowdate = $tommorow->format('Y-m-d');
                        $todaystrttime = $tommorowdate . ' ' . $start_time;
                        $diff_strtndend = $CI->myfunctions->gethours_minutes($todaydatetime, $todaystrttime);
                        $addresult = $CI->myfunctions->sum_the_datetime($diff_strtndend, $caltime);
                        $totalhours = $CI->myfunctions->sum_the_datetime($addresult, $addtimetonextday);
                        $result_arr = explode(":", $totalhours);
                        $result_hours = $result_arr[0];
                        $result_minutes = $result_arr[1];
                        $result_seconds = $result_arr[2];
                    } else {
                        $result_hours = $calculation_time;
                        $result_minutes = 00;
                        $result_seconds = 00;
                    }
                }
            }
        } elseif ($bucketType == 2) {
            $result_hours = $calculation_time;
            $result_minutes = 00;
            $result_seconds = 00;
        } else {
            $result_hours = $calcu_time;
            $result_minutes = 00;
            $result_seconds = 00;
        }


        $plusday = "+" . $result_hours . " hour +" . $result_minutes . " minutes +" . $result_seconds . " seconds";
//         echo $plusday.' plusdays<br>';
        $hosttime->modify($plusday);
        $cal_day = $hosttime->format('Y-m-d H:i:s');
        // echo $cal_day.' calll day<br>';
        $getdate = strtotime($cal_day);
        $getdate = date("l", $getdate);
        //echo $getdate.' day<br>';
        $day_array = array("Saturday", "Sunday");
        if (in_array($getdate, $day_array)) {
            $date = $getdate;
        } else {
            $date = $getdate;
        }

        if ($date == 'Saturday') {
            $addhours = $result_hours + 48;
            $plusday = "+" . $addhours . " hour +" . $result_minutes . " minutes +" . $result_seconds . " seconds";
        } elseif ($date == 'Sunday') {
            $addhours = $result_hours + 24;
            $plusday = "+" . $addhours . " hour +" . $result_minutes . " minutes +" . $result_seconds . " seconds";
        } else {
            $plusday = "+" . $result_hours . " hour +" . $result_minutes . " minutes +" . $result_seconds . " seconds";
        }
        $datetime->modify($plusday);
        $datere = $datetime->format('Y-m-d H:i:s');
        if (($bucketType == '' || $bucketType == 0) && $timeline == '') {
            $expdatedate = $CI->myfunctions->onlytimezoneforall($datere);
            $expdateformat = new DateTime($expdatedate);
            $expdate = $expdateformat->format('Y-m-d H:i:s');
        } else {
            $expdate = $datere;
        }
        return $expdate;
    }
    
    public function CheckActiveRequest($userId = "", $createdby = "", $reurnuser = "", $usertype = "",$client_slot=array(),$customerid="",$from="",$req_status="") {
        $CI = & get_instance();
        if(empty($client_slot)){
            $customer_data = $CI->Admin_model->getuser_data($userId);
            $totalInprogress = $customer_data[0]['total_inprogress_req'];
            $totalActive = $customer_data[0]['total_active_req'];
        }else{
            $totalInprogress = $client_slot['inprogress'];
            $totalActive = $client_slot['active']; 
        }
        if ($createdby == '' || $createdby == NULL) {
            $get_inqueue_request = $CI->Admin_model->get_all_request_count(array("assign"), $userId);
            $get_active_request = $CI->Admin_model->get_all_request_count(array("active"), $userId);
            $get_revision_request = $CI->Admin_model->get_all_request_count(array("active", "checkforapprove", "disapprove"), $userId);
        } else {
            $get_inqueue_request = $CI->Admin_model->get_all_request_count(array("assign"), $userId, $createdby,$customerid);
            $get_active_request = $CI->Admin_model->get_all_request_count(array("active"), $userId, $createdby,$customerid);
            $get_revision_request = $CI->Admin_model->get_all_request_count(array("active", "checkforapprove", "disapprove"), $userId, $createdby,$customerid);
        }
//       echo $totalInprogress." - totalInprogress req <br>";
//        echo $totalActive." - totalActive req <br>";
//        echo sizeof($get_inqueue_request)." - inqueue req <br>";
//        echo sizeof($get_active_request)." - active req <br>";
//        echo sizeof($get_revision_request)." - get_revision_request <br>";
        $allpreviousstatuses = $CI->customfunctions->allStatusafterUnhold($get_inqueue_request[0]['previous_status']);
        if (in_array($customer_data[0]['plan_name'], NEW_PLANS)) {
            return true;
        }
        if ($usertype != "" && $usertype == 'one_time') {
            return true;
        }
        if($from == "from_hold" && ($req_status == "disapprove" || $req_status == "checkforapprove")){
            if (sizeof($get_active_request) < $totalInprogress || sizeof($get_revision_request) < $totalActive) {
                if ($reurnuser != '' || $reurnuser != NULL) {
                    return $userId;
                } else {
                    return true;
                }
            }
        }else if(in_array($allpreviousstatuses["status"],array("disapprove","checkforapprove"))){
            if (sizeof($get_active_request) < $totalInprogress || sizeof($get_revision_request) < $totalActive) {
                if ($reurnuser != '' || $reurnuser != NULL) {
                    return $userId;
                } else {
                    return true;
                }
            }
        }else{
            if (sizeof($get_active_request) < $totalInprogress && sizeof($get_revision_request) < $totalActive) {
                if ($reurnuser != '' || $reurnuser != NULL) {
                    return $userId;
                } else {
                    return true;
                }
            }
        }
    }

    public function SendQAmessagetoInprogressReq($ReqID = "", $QAID = "", $CusID = "") {
        $CI = & get_instance();
        $datamsgauto = array(
            "request_id" => $ReqID,
            "sender_id" => $QAID,
            "sender_type" => "qa",
            "reciever_id" => $CusID,
            "reciever_type" => "customer",
            "message" => "The designer is currently working on your project. Send messages here to share updates about the overall project. If you want to share comments about an individual design you can use the comments section within each design draft.",
            "with_or_not_customer" => 1,
            "admin_seen" => 1,
            "qa_seen" => 1,
            "designer_seen" => 1,
            "created" => date("Y-m-d H:i:s"),
        );
        //echo "<pre>";print_r($datamsgauto); exit;
        $CI->Welcome_model->insert_data("request_discussions", $datamsgauto);

        return true;
    }

    public function CustomFileUpload($config, $flname) {
        //echo "<pre>";print_r($config);exit;
        $output = array();
        $CI = & get_instance();
//          if(ENVIRONMENT == 'production'){
        if (UPLOAD_FILE_SERVER == 'bucket') {
            $CI->load->library('s3_upload');
            $CI->s3_upload->initialize($config);
            $upl = $CI->s3_upload->upload_file($flname);
            if ($upl['status'] == 1) {
                $output['status'] = true;
                $output['msg'] = $upl['error_msg'];
            } else {
                $output['status'] = false;
                $output['msg'] = $upl['error_msg'];
            }
        } else {
            // print_r($config);exit;
            // $CI->load->library('upload', $config);
            $CI->load->library('upload');
            $CI->upload->initialize($config);
            if ($CI->upload->do_upload($flname)) {

                $output['status'] = true;
                $output['msg'] = 'uploaded file successfully';
            } else {
                $output['status'] = false;
                $output['msg'] = $CI->upload->display_errors();
            }
        }
        // echo "<pre/>";print_R($output);exit;
        return $output;
    }

    public function make_thumb($src, $dest, $desired_width="", $extension,$desired_height="") {
        //echo $src.'check';exit;
        $extension = strtolower($extension);
        if ($extension == 'jpeg' || $extension == 'jpg') {
            $source_image = imagecreatefromjpeg($src);
        }
        if ($extension == 'png') {
            $source_image = imagecreatefrompng($src);
        }
        if ($extension == 'gif') {
            $source_image = imagecreatefromgif($src);
        }
        $width = imagesx($source_image);
        $height = imagesy($source_image);
       // $desired_height = floor($height * ($desired_width / $width));
        if($desired_width != "" && $desired_height == ""){
            $desired_height = floor($height * ($desired_width / $width));
        }else{
            $desired_width = floor($width * ($desired_height / $height));
        }
        $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
        if ($extension == 'gif' || $extension == 'png') {

            // make image transparent
            imagecolortransparent(
                    $thumbnail, imagecolorallocate($virtual_image, 0, 0, 0)
            );

            // additional settings for PNGs
            if ($extension == 'png') {
                imagealphablending($virtual_image, false);
                imagesavealpha($virtual_image, true);
            }
        }
        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
        if ($extension == 'jpeg' || $extension == 'jpg') {
            imagejpeg($virtual_image, $dest);
        }
        if ($extension == 'png') {
            imagepng($virtual_image, $dest);
        }
        if ($extension == 'gif') {
            imagegif($virtual_image, $dest);
        }
        return true;
    }

    public function isUserPermission($permission, $uId = NULL) {
        $uId = isset($uId) ? $uId : $_SESSION['user_id'];
        $global_user_info = $this->setUser($uId);
        if ($global_user_info['login_user_data'][0]['parent_id'] == 0 && $global_user_info['login_user_data'][0]['id'] == $uId) {
            return 2;
        } else {
            if ($global_user_info['sub_userspermissions'][0][$permission] == 1) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function random_password() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $password = array();
        $alpha_length = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alpha_length);
            $password[] = $alphabet[$n];
        }
        return implode($password);
    }

    public function checkIsFileExist($filepath) {
//        echo $filepath;
//        $arr = explode('requests/',$filepath);
//        $filename1 = $arr[1];
//        $filename = FS_PATH.FS_PATH_PUBLIC_UPLOADS_REQUESTS.$filename1;
        //echo $filename;exit;
        $output = array();
        $CI = & get_instance();
        if (UPLOAD_FILE_SERVER == 'bucket') {
            $info = $CI->s3->getObjectInfo('bucket', $filepath);
            if ($info) {
                return true;
            } else {
                return false;
            }
        } else {
            if (file_exists($filepath)) {
                return true;
            } else {
                return false;
            }
        }
    }

    /*     * **************** new function 12.02.2018 *********************** */

    public function changerequeststatusafteradd($created_user, $id, $bucketType, $subcat_data, $login_userdata = null) {
        $CI = & get_instance();
        $table = "requests";
        $customer_data = $CI->Admin_model->getuser_data($created_user);
        $changestatusdate = date("Y-m-d H:i:s", time() + 1);
        $assigndesignerdate = date("Y-m-d H:i:s", time() + 2);
//        $totalInprogress = $customer_data[0]['total_inprogress_req'];
//        $requestdata = $CI->Request_model->get_request_by_id($id);
        $getsubscriptionplan = $CI->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
        if($getsubscriptionplan[0]['is_agency'] == 1){
           $checkslot =  $CI->myfunctions->getslotofallusers($customer_data,$login_userdata);
        }
        if ($customer_data[0]['designer_id'] == 0) {
            $designer_id = 0;
            $assignornot = 0;
        } else {
            $designer_id = $customer_data[0]['designer_id'];
            $assignornot = 1;
        }
        if ($customer_data[0]['designer_id'] != "" || $customer_data[0]['designer_id'] != "0") {
            if (($login_userdata[0]['parent_id'] != 0 && $getsubscriptionplan[0]['is_agency'] == 1 && $login_userdata[0]['user_flag'] == 'client') || ($login_userdata[0]['parent_id'] == 0 && $getsubscriptionplan[0]['is_agency'] == 1)) {
                $data2 = $CI->myfunctions->changerequeststatus_ofsubusertype($created_user, $id, $customer_data, $bucketType, $subcat_data, $designer_id, $assignornot, $login_userdata,$checkslot);
                $key = 'changerequeststatus_ofsubusertype';
            } else {
                $data2 = $CI->myfunctions->changerequeststatus_ofallusertype($created_user, $id, $customer_data, $bucketType, $subcat_data, $designer_id, $assignornot, $login_userdata);
                $key = 'changerequeststatus_ofallusertype';
            }
        } else {
            $data2 = array("status" => "pending", "status_admin" => "pending", "status_designer" => "pending", "status_qa" => "pending");
            $key = 'changerequeststatusafteradd';
        }
        $where = array("id" => $id);
        $priority = $data2['priority'];
        if($priority > 0){
            $CI->Clients_model->updatepriorityforagancyproject($created_user,$priority);
        }
        $update_data = $CI->Welcome_model->update_data($table, $data2, $where);
        $CI->myfunctions->delete_dummy_request($created_user);
        $CI->myfunctions->capture_project_activity($id,'','','draft_to_'.$data2["status"],$key,$login_userdata[0]['id'],1,'','','',$changestatusdate);
        if($designer_id != 0){
         $CI->myfunctions->capture_project_activity($id,'',$designer_id,'assign_designer','changerequeststatusafteradd',$login_userdata[0]['id'],1,'','','',$assigndesignerdate);   
        }
        if ($update_data) {
            return true;
        }
    }

    public function send_email_when_inqueue_to_inprogress($project_title, $id, $email = NULL) {
        $CI = & get_instance();
        // echo $project_title.$id.$email;exit;
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $receiver = $email;
        $query1 = $CI->db->select('first_name')->from('users')->where('email', $receiver)->get();
        $fname = $query1->result();
        $data = array(
            'title' => $project_title,
            'project_id' => $id,
            'fname' => isset($fname[0]->first_name) ? $fname[0]->first_name : ''
        );
        $body = $CI->load->view('emailtemplate/inqueue_template', $data, TRUE);
        $subject = 'Designer has Started Working on your New Project.';

        $message = $body;

        $title = 'GraphicsZoo';

        //SM_sendMail($receiver,$subject,$message,$title);          
    }

    public function gettimezone() {

        $ip = $_SERVER['REMOTE_ADDR'];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://ip-api.com/json/" . $ip,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: 92b0963f-d661-849d-9ca9-734180411906"
            ),
        ));

        $ipInfo = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            
        }
        $ipInfo = json_decode($ipInfo);
        return $ipInfo;
    }

    public function send_email_when_addrequest_by_mail($project_title = NULL, $id = NULL, $email = NULL, $status, $aSubject = NULL) {
        $CI = & get_instance();
        $fname = $CI->Request_model->getuserbyemail($email);
        if($fname[0]['parent_id'] == 0){
            $parent_id = $fname[0]['id'];
        }else{
            $parent_id = $fname[0]['parent_id'];
        }
        $parent_userplan = $CI->Admin_model->getuser_data($parent_id);
        $subdomain_url = $CI->myfunctions->dynamic_urlforsubuser_inemail($parent_id,$parent_userplan[0]['plan_name']);
        $split_email = explode('@', $email);
        if ($status == 'reg_mail') {
            $data['msg'] = '"' . $email . '" is not register on Graphicszoo, please use register email to add request.';
        } elseif ($status == 'blank') {
             $data['msg'] = 'Subject and Message is required part to add request by email.';
        } elseif ($status == 'no_permission') {
             $data['msg'] = 'You have not permission to add request by email.<br><a class="no_permisn">Contact Main User</a>';
        } elseif ($status == 'trial') {
             $data['msg'] = 'Please upgrade your plan to add requests. <br> <p><a href="' . $subdomain_url . 'customer/setting-view#billing" style="margin: 0px 0px 0px 35px;background: #e8304d;color: #fff; text-decoration: none;font-size:15px;padding: 14px 29px;border-radius: 25px;font-weight: bold;">Upgrade Plan</a></p>';
        } elseif ($status == '99user') {
             $data['msg'] = 'Please upgrade your plan to add requests. <br> <p><a href="' . $subdomain_url . 'customer/setting-view#billing" style="margin: 0px 0px 0px 35px;background: #e8304d;color: #fff; text-decoration: none;font-size:15px;padding: 14px 29px;border-radius: 25px;font-weight: bold;">Upgrade Plan</a></p>';
        } elseif ($status == 'cancelsubs') {
             $data['msg'] = 'Please upgrade your plan to add requests. <br> <p><a href="' . $subdomain_url . 'customer/setting-view#billing" style="margin: 0px 0px 0px 35px;background: #e8304d;color: #fff; text-decoration: none;font-size:15px;padding: 14px 29px;border-radius: 25px;font-weight: bold;">Upgrade Plan</a></p>';
        } elseif ($status == 'newplan') {
             $data['msg'] = 'Please upgrade your plan to add requests. <br> <p><a href="' . $subdomain_url . 'customer/setting-view#billing" style="margin: 0px 0px 0px 35px;background: #e8304d;color: #fff; text-decoration: none;font-size:15px;padding: 14px 29px;border-radius: 25px;font-weight: bold;">Upgrade Plan</a></p>';
        }elseif ($status == 'subuserlimit') {
             $data['msg'] = 'Please upgrade your plan to add requests. <br> <p><a style="margin: 0px 0px 0px 35px;background: #e8304d;color: #fff; text-decoration: none;font-size:15px;padding: 14px 29px;border-radius: 25px;font-weight: bold;">Contact Main User</a></p>';
        } else {
             $data['msg'] = 'Successfully Added your request.<br><b>Project: ' . $project_title . '</b> <br>Follow the below provided link to visit.<br> <p><a href="' . $subdomain_url . 'project_info_view/' . $id . '?id'.$fname[0]['id'].'" style="margin: 0px 0px 0px 35px;background: #e8304d;color: #fff; text-decoration: none;font-size:15px;padding: 14px 29px;border-radius: 25px;font-weight: bold;"> New Project Link</a></p> ';
        }
        
        $array = array('CUSTOMER_NAME' => isset($fname[0]['first_name']) ? $fname[0]['first_name'] : $split_email[0],
                            'MESSAGE' => '<p class="content_text" style="font-size: 15px;line-height: 24px;font-family: sans-serif;color: #20364c;margin: 0px 0px 30px 35px;">'.$data['msg'].'</p>',
                       'PROJECT_TITLE' => $project_title,
                        'PROJECT_LINK' => $subdomain_url . 'project_info_view/' . $id . '?id'.$fname[0]['id']
            );
        if($status != 'req_added'){
        $CI->myfunctions->send_email_to_users_by_template($fname[0]['id'], 'error_msg_add_request_by_email', $array, $email);
        }else{
        $CI->myfunctions->send_email_to_users_by_template($fname[0]['id'], 'success_msg_add_request_by_email', $array, $email);    
        }
    }

    public function send_email_after_admin_approval($project_title, $id, $email, $draft_id, $url = NULL) {
        $CI = & get_instance();
        $fname = $CI->Request_model->getuserbyemail($email);
        if ($url != NULL || $url != '') {
            $base_url = $url;
        } else {
            $base_url = base_url();
        }
        $requestdata = $CI->Request_model->get_request_by_id($id);
        $array = array('CUSTOMER_NAME' => isset($fname[0]['first_name']) ? $fname[0]['first_name'] : '',
            'PROJECT_TITLE' => $project_title,
            'PROJECT_LINK' => $base_url . 'project_view/' . $draft_id . '?id=' . $fname[0]['id']);
        $CI->myfunctions->send_email_to_users_by_template($requestdata[0]['customer_id'], 'email_after_approve_design', $array, $email);
    }

    public function send_emails_to_sub_user($customer_id, $permission, $is_approveDraftemailbyadmin = NULL, $requestid = "") {
        $CI = & get_instance();
        $allsubuserbyparentid = $CI->Request_model->getAllsubUsers($customer_id);
        // echo "<pre>";print_r($allsubuserbyparentid);exit;
        if (!empty($allsubuserbyparentid)) {
            foreach ($allsubuserbyparentid as $subuserper) {
                $canapproverequests = $CI->myfunctions->isUserPermission($permission, $subuserper['id']);
                $canallbrandprofileaccess = $CI->myfunctions->isUserPermission('brand_profile_access', $subuserper['id']);
                $canbrandprofileaccess = $CI->Request_model->get_user_brand_profile_by_user_id($subuserper['id']);
                $subuser_email = $CI->Request_model->getuserbyid($subuserper['id']);
                $is_subDraftemailbyuser = $CI->Admin_model->approveDraftemailbyuser($subuserper['id']);
                if ($requestid != "") {
                    $requestofsubuser = $CI->Request_model->get_request_by_id($requestid, $subuserper['id']);
                }
                $issub_cronenable = $CI->Admin_model->emailgoingbyCron($subuserper['id']);
                if (sizeof($canbrandprofileaccess) > 0) {
                    foreach ($canbrandprofileaccess as $user_brands_info) {
                        $brand_id[] = $user_brands_info['brand_id'];
                    }
                    $allreqbybrandid = $CI->Request_model->getAllrequestbybrandId('', $brand_id);
                    if (sizeof($allreqbybrandid) > 0) {
                        foreach ($allreqbybrandid as $reqidbybrand) {
                            if ($reqidbybrand['id'] == $request[0]['id']) {
                                $allowreqid[] = $reqidbybrand['id'];
                            }
                        }
                    }
                }
                if ($canallbrandprofileaccess == 0 && !empty($requestofsubuser)) {
                    $canallbrandprofileaccess = 1;
                }
                // echo "<pre>";print_r($requestofsubuser);
                //  echo $canallbrandprofileaccess.$is_approveDraftemailbyadmin.$canapproverequests.$is_subDraftemailbyuser.$subuser_email[0]['is_active'].'<br>';
                if ($is_approveDraftemailbyadmin != NULL || $is_approveDraftemailbyadmin != '') {

                    if ($is_approveDraftemailbyadmin && $canapproverequests == 1 && $canallbrandprofileaccess == 1 && $is_subDraftemailbyuser && $subuser_email[0]['is_active'] == 1) {
                        $status = 1;
                    } else if ($is_approveDraftemailbyadmin && $canapproverequests == 1 && $canallbrandprofileaccess != 1 && !empty($allowreqid) && $is_subDraftemailbyuser && $subuser_email[0]['is_active'] == 1) {
                        $status = 1;
                    } else {
                        $status = 0;
                    }
                } else {

                    if ($issub_cronenable && $canapproverequests == 1 && $canallbrandprofileaccess == 1 && $subuser_email[0]['is_active'] == 1) {
                        $status = 1;
                    } else if ($issub_cronenable && $canapproverequests == 1 && $canallbrandprofileaccess != 1 && !empty($allowreqid) && $subuser_email[0]['is_active'] == 1) {
                        $status = 1;
                    } else {
                        $status = 0;
                    }
                }


                if ($status == 1) {
                    $return_data = array('cus_id' => $subuser_email[0]['id'], 'email' => $subuser_email[0]['email']);
                    $emails_data[] = $return_data;
                }
            }
        }

        return $emails_data;
    }

    function dynamic_urlforsubuser_inemail($userid, $planname, $type = NULL) {
        $CI = & get_instance();
        $domain_data = $CI->Request_model->getuserinfobydomain('', $userid);
        $getsubscriptionplan = $CI->Request_model->getsubscriptionlistbyplanid($planname);
        // echo "<pre>";print_r($domain_data);
        if ($getsubscriptionplan[0]['is_agency'] == 1 && !empty($domain_data) && ($domain_data[0]['domain_name'] != '' || $domain_data[0]['domain_name'] != NULL)) {
            if ($domain_data[0]['is_main_domain'] == 1 && $domain_data[0]['ssl_or_not'] == 1) {
                $url = 'https://' . $domain_data[0]['domain_name'] . '/';
            } else if ($domain_data[0]['is_main_domain'] == 1 && $domain_data[0]['ssl_or_not'] != 1) {
                $url = 'http://' . $domain_data[0]['domain_name'] . '/';
            } else {
                $url = 'https://' . $domain_data[0]['domain_name'] . '/';
            }
        } else if ($type != NULL && $type == 'login') {
            $url = base_url() . 'login';
        } else {
            $url = base_url();
        }
        // echo $url;exit;
        return $url;
    }

    public function canUserAddNewRequest($trail_user,$billing_cycle_request,$request_for_trial){ 
     $CI = & get_instance();
      $login_user_id = $CI->load->get_var('login_user_id');
      $created_user = $CI->load->get_var('main_user');
      $login_user_data = $CI->load->get_var('login_user_data');
      $getsubscriptionplan = $CI->Request_model->getsubscriptionlistbyplanid($trail_user[0]['plan_name']);
      $useroptions_data = $CI->Request_model->getAgencyuserinfo($created_user);
      $sharedusers = $CI->Admin_model->sumallsubuser_requestscount($created_user,1);
      $dedicatedusers = $CI->Admin_model->sumallsubuser_requestscount($created_user,0);
      $dedicateddesignercount = $dedicatedusers['total_inprogress_req']*$useroptions_data[0]['no_of_assign_user'];
      $main_inprogress_req = $trail_user[0]['total_inprogress_req']*$useroptions_data[0]['no_of_assign_user'];
      $subusers_reqcount = $dedicateddesignercount+$sharedusers['total_inprogress_req'];
      
        if (($trail_user[0]['is_trail'] == 1 && sizeof($request_for_trial) >= 1)) {
            return 0;  //if user trail
        }
        if ($trail_user[0]['is_cancel_subscription'] == 1) {
            return 3;  //if user subscription is canceled
        }
        if($trail_user[0]['billing_cycle_request'] > 0){
            if (sizeof($billing_cycle_request) >= $trail_user[0]['billing_cycle_request']) {
               return 1; //if user have $99 plan
           }else{
               return 2; // user can add request
           }  
        }
       
        if(in_array($trail_user[0]['plan_name'],NEW_PLANS)){
            $all_request = $CI->Request_model->getall_request_for_all_status($trail_user[0]['id']);
            if (!empty($all_request) && sizeof($all_request) >= $trail_user[0]['total_requests']) {
             return 4; //if user have new plan  and user  cann't add request
            }else{
             return 2; // user can add request
            }
        }
         
        $customer_data = $CI->Admin_model->getuser_data($login_user_id);
        
        if ((($customer_data[0]['parent_id'] == 0 && $subusers_reqcount >= $main_inprogress_req) || (($login_user_data[0]['parent_id'] == 0 || $login_user_data[0]['requests_type'] == 'one_time') && $useroptions_data[0]['reserve_count'] == 0)) && $getsubscriptionplan[0]['is_agency'] == 1 && !empty($useroptions_data)){
            return 6; //user  cann't add request because you have already sell your dedicated designer
        }
        
        if ($getsubscriptionplan[0]['is_agency'] == 1 && $customer_data[0]['parent_id'] != 0 && $customer_data[0]['user_flag'] == 'client') {
            if($customer_data[0]['requests_type'] == 'per_month'){
                $all_request = $CI->Request_model->getall_request_for_all_status($login_user_id,$customer_data[0]['billing_start_date'],$customer_data[0]['billing_end_date'],'created');
                $total_requests = $customer_data[0]['billing_cycle_request'];
            }else if($customer_data[0]['requests_type'] == 'one_time'){
                $total_requests = $customer_data[0]['total_requests'];
                $all_request = $CI->Request_model->getall_request_for_all_status($login_user_id,'','','created');  
            }else{
                return 2; // user can add request
            }
            if (sizeof($all_request) >= $total_requests) {
                return 5; //user  cann't add request because cross sub user limit for add request
            } else {
                return 2; // user can add request
            }
        } else {
            return 2; // user can add request
        }
    }

    public function random_sharekey() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $password = array();
        $alpha_length = strlen($alphabet) - 1;
        for ($i = 0; $i < 40; $i++) {
            $n = rand(0, $alpha_length);
            $password[] = $alphabet[$n];
        }
        return implode($password);
    }

    public function gethours_minutes($startdate, $enddate) {
        $diff = strtotime($enddate) - strtotime($startdate);
        $hours = floor($diff / (60 * 60));
        $minutes = $diff - $hours * (60 * 60);
        $worktimeleftfr = $hours . ':' . floor($minutes / 60) . ':' . '00';
        return $worktimeleftfr;
    }

    function sum_the_datetime($time1, $time2) {
        $times = array($time1, $time2);
        $seconds = 0;
        foreach ($times as $time) {
            list($hour, $minute, $second) = explode(':', $time);
            $seconds += $hour * 3600;
            $seconds += $minute * 60;
            $seconds += $second;
        }
        $hours = floor($seconds / 3600);
        $seconds -= $hours * 3600;
        $minutes = floor($seconds / 60);
        $seconds -= $minutes * 60;
        if ($seconds < 9) {
            $seconds = "0" . $seconds;
        }
        if ($minutes < 9) {
            $minutes = "0" . $minutes;
        }
        if ($hours < 9) {
            $hours = "0" . $hours;
        }
        return "{$hours}:{$minutes}:{$seconds}";
    }

    function coverttimetosecond($time) {
        // echo $time.'<br>';;
        $str_time = $time;
        $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);

        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);

        $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
        // echo $time_seconds.'<br>';exit;
        return $time_seconds;
    }

    public function getexpected_datefromtimezone($date, $planname = "", $bucketType = "", $timeline = "", $turnaround = "") {
        //echo $bucketType.' hello '.$timeline;exit;    
        $CI = & get_instance();
        $nextdate = $CI->myfunctions->check_timezone($date, $planname, $bucketType, $timeline, $turnaround);
        return $nextdate;
    }

    public function show_notifications($request_id, $sender_id, $title, $url, $user_id) {
        $CI = & get_instance();
        $notification_data = array(
            "request_id" => $request_id,
            "sender_id" => $sender_id,
            "title" => $title,
            "url" => $url,
            "user_id" => $user_id,
            "created" => date("Y-m-d H:i:s"));
        $CI->Welcome_model->insert_data("notification", $notification_data);
    }

    public function show_messages_notifications($heading, $msg, $request_id, $sender_id, $url, $user_id) {
        $CI = & get_instance();
        $notification_data = array('heading' => $heading,
            'title' => $msg,
            'created' => date("Y:m:d H:i:s"),
            'request_id' => $request_id,
            'sender_users_id' => $sender_id,
            'shown' => 0,
            'count' => 1,
            'user_id' => $user_id,
            'url' => $url);

        $query = $CI->Welcome_model->insert_data("message_notification", $notification_data);
    }

    public function send_email_to_users_by_template($userid, $slug, $array, $email) {
        /*{{POWERED_BY}}*/
        $CI = & get_instance();
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");

        $customer_data = $CI->Admin_model->getuser_data($userid);
        $email_data = $CI->Request_model->getuserbyemail($email);
        if ($customer_data[0]['parent_id'] != 0) {
            $parent_user_id = $customer_data[0]['parent_id'];
        } else {
            $parent_user_id = $customer_data[0]['id'];
        }
		$powerd_text = '';
        //if($email_data[0]['user_flag'] != 'client'){
        //    $powerd_text= '<p style="text-align:center;color: #20364c;font: normal 12px/22px GothamPro-Medium,sans-serif;"><i>Powered by graphicszoo</i><br>For any query contact us at hello@graphicszoo.com</p>';
        //}
        //echo $userid.'parent - '.$parent_user_id;
       //if($email_data[0]['user_flag'] != 'client'){
            //$email_header = $CI->Admin_model->get_template_byslug_and_userid('email_header',0);
            //$email_footer = $CI->Admin_model->get_template_byslug_and_userid('email_footer',0); 
       //}else{
            $email_header = $CI->Admin_model->get_template_byslug_and_userid('email_header',$parent_user_id);
            $email_footer = $CI->Admin_model->get_template_byslug_and_userid('email_footer',$parent_user_id);
       //}
        $email_content = $CI->Admin_model->get_template_byslug_and_userid($slug, $parent_user_id);
        if (!empty($email_content) && $email_content[0]['disable_email_to_user'] == 0) {
            $body = '<div class="container" style="box-shadow: 0px 0px 25px #d6dee4; border:1px solid #eee;max-width:650px;width: 100%;margin:20px auto 15px;">';
            $body .= $email_header[0]['email_text'];
            $body_html = $email_content[0]['email_text'];
            if (count($array)):
                foreach ($array as $k => $v):
                    $literal = '{{' . trim(strtoupper($k)) . '}}';

                    $body_html = str_replace($literal, $v, $body_html);
                endforeach;
            endif;
            $body .= $body_html;
            $body .= $email_footer[0]['email_text'];
            $body .= '</div>';
            //echo $email.$body;exit;
            //$body .= $powerd_text;
            $title = 'GraphicsZoo';
                $result = SM_sendMail($email, $email_content[0]['email_subject'], $body, $title, '', '', $parent_user_id);
                if ($result) {
                    return true;
                }
            
        }
    }

    public function add_dummy_request($id) {
        $CI = & get_instance();
        $customer_data = $CI->Admin_model->getuser_data($id);
        $getsubscriptionplan = $CI->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
        if ($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])) {
            $turn_around_days = $customer_data[0]['turn_around_days'];
        } else {
            $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
        }
        $subcat_data = $CI->Category_model->get_category_byID(23);
        $expected = $CI->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $customer_data[0]['plan_name'], $subcat_data[0]['bucket_type'], $subcat_data[0]['timeline'], $turn_around_days);
        $requestdata = array("customer_id" => $id,
            "title" => "Create A Logo(dummy)",
            "category" => NULL,
            "logo_brand" => NULL,
            "brand_id" => NULL,
            "category_id" => 1,
            "subcategory_id" => 23,
            "dummy_request" => 1,
            "category_bucket" => 1,
            "description" => "Dummy text here dummy text here dummy text here dummy text here...",
            "created" => date("Y-m-d H:i:s"),
            "status" => "checkforapprove",
            "status_admin" => "checkforapprove",
            "status_designer" => "checkforapprove",
            "status_qa" => "checkforapprove",
            "designer_id" => 0,
            "priority" => 0,
            "deliverables" => "SourceFiles",
            "created_by" => $id,
            "approvaldate" => date("Y-m-d H:i:s"),
            "expected_date" => $expected,
            "color_pref" => "Let Designer Choose");
        //  echo "<pre>";print_r($requestdata);
        $success = $CI->Welcome_model->insert_data("requests", $requestdata);

        $CI->myfunctions->upload_draft_with_dummy_request($success, $id, $customer_data);
        $files_data = $CI->Request_model->get_file_all($success);
        //  echo "<pre>";print_r($files_data);
        $CI->myfunctions->send_comment_messages_with_dummy_request($files_data, $success, $id, $customer_data);
        $CI->myfunctions->send_messages_with_dummy_request($success, $id, $customer_data);
        $CI->Welcome_model->update_data("requests", array("expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $success));
    }

    public function upload_draft_with_dummy_request($success, $id, $customer_data) {
        $CI = & get_instance();
        /*         * ********** upload dummy draft ******************* */

        if (!is_dir('public/uploads/requests/' . $success)) {
            $data = mkdir('./public/uploads/requests/' . $success, 0777, TRUE);
        }
        if (!is_dir('public/uploads/requests/' . $success . '/_thumb')) {
            $data = mkdir('./public/uploads/requests/' . $success . '/_thumb', 0777, TRUE);
        }
        $files = scandir('./public/dummy_request/');
        $files_thumb = scandir('./public/dummy_request/_thumb/');
        // Identify directories
        $source = 'public/dummy_request/';
        $source_thumb = 'public/dummy_request/_thumb/';
        $destination = './public/uploads/requests/' . $success . '/';
        $thumb_destination = './public/uploads/requests/' . $success . '/_thumb/';
        $array = array();
        if (UPLOAD_FILE_SERVER == 'bucket') {
            $CI->load->library('s3_upload');
            foreach ($files as $file) {

                if (in_array($file, array(".", "..", "_thumb")))
                    continue;

                //upload main image   
                $staticName = base_url() . $source . $file;
                $config = array(
                    'upload_path' => 'public/uploads/requests/' . $success . '/',
                    'file_name' => $file
                );
                //echo "<pre>";print_r($config);

                $CI->s3_upload->initialize($config);
                $test = $CI->s3_upload->upload_multiple_file($staticName);
                $array[] = $file;
            }
            //upload thumb image
            $thumb_name = base_url() . $source_thumb . $files_thumb[2];
            // echo $thumb_name."<pre>";print_r($files_thumb);exit;
            $config1 = array(
                'upload_path' => 'public/uploads/requests/' . $success . '/__thumb/',
                'file_name' => $files_thumb[2]
            );
            $CI->s3_upload->initialize($config1);
            $test = $CI->s3_upload->upload_multiple_file($thumb_name);

            $thumb_name = base_url() . $source_thumb . $files_thumb[3];


            $config1 = array(
                'upload_path' => 'public/uploads/requests/' . $success . '/__thumb/',
                'file_name' => $files_thumb[3]
            );

            $CI->s3_upload->initialize($config1);
            $test = $CI->s3_upload->upload_multiple_file($thumb_name);
        }else {
            foreach ($files as $file) {

                if (in_array($file, array(".", "..", "_thumb")))
                    continue;
                // echo $filepath;exit;
                copy($source . $file, $destination . $file);
                $array[] = $file;
            }
            copy($source_thumb . $files_thumb[2], $thumb_destination . $files_thumb[2]);
            copy($source_thumb . $files_thumb[3], $thumb_destination . $files_thumb[3]);
        }
        foreach ($array as $filename) {
            $request_files_data = array("request_id" => $success,
                "user_id" => $id,
                "user_type" => "designer",
                "file_name" => $filename,
                "status" => "customerreview",
                "preview" => 1,
                "customer_seen" => 1,
                "designer_seen" => 1,
                "qa_seen" => 1,
                "admin_seen" => 1,
                "created" => date("Y-m-d H:i:s"));
            $file_id = $CI->Welcome_model->insert_data("request_files", $request_files_data);
        }

        /*         * ********** end upload dummy draft ******************* */
    }

    public function send_comment_messages_with_dummy_request($files_data, $success, $id, $customer_data) {
        $CI = & get_instance();
        $send_message = array("request_file_id" => $files_data[0]['id'],
            "sender_role" => "customer",
            "sender_id" => $id,
            "receiver_role" => "designer",
            "receiver_id" => 0,
            "message" => "change color to blue",
            "customer_name" => $customer_data[0]['first_name'],
            "xco" => 688.2352941176471,
            "yco" => 429.3893129770992,
            "qa_seen" => 1,
            "admin_seen" => 1,
            "designer_seen" => 1,
            "customer_seen" => 1,
            "created" => date("Y-m-d H:i:s"));

        $CI->Welcome_model->insert_data("request_file_chat", $send_message);

        $send_message = array("request_file_id" => $files_data[1]['id'],
            "sender_role" => "customer",
            "sender_id" => $id,
            "receiver_role" => "designer",
            "receiver_id" => 0,
            "message" => "Thanks For Update",
            "customer_name" => $customer_data[0]['first_name'],
            "qa_seen" => 1,
            "admin_seen" => 1,
            "designer_seen" => 1,
            "created" => date("Y-m-d H:i:s"),
            "customer_seen" => 1);
        // echo "<pre>";print_r($send_message);exit;
        $CI->Welcome_model->insert_data("request_file_chat", $send_message);
    }

    public function send_messages_with_dummy_request($success, $id, $customer_data) {
        $CI = & get_instance();
        /*         * ***************** send default messages ********************** */

        $message = QA_MESSAGE;
        $send_message = array(
            "request_id" => $success,
            "sender_id" => $customer_data[0]['qa_id'],
            "sender_type" => "qa",
            "reciever_id" => $id,
            "reciever_type" => "customer",
            "message" => $message,
            "with_or_not_customer" => 1,
            "admin_seen" => 1,
            "qa_seen" => 1,
            "designer_seen" => 1,
            "customer_seen" => 1,
            "created" => date("Y-m-d H:i:s"),
        );
        //echo "<pre>";print_r($datamsgauto); exit;
        $CI->Welcome_model->insert_data("request_discussions", $send_message);

        $send_message = array("request_id" => $success,
            "sender_type" => "customer",
            "sender_id" => $id,
            "reciever_id" => 0,
            "reciever_type" => "designer",
            "admin_seen" => 1,
            "qa_seen" => 1,
            "designer_seen" => 1,
            "customer_seen" => 1,
            "created" => date("Y-m-d H:i:s"),
            "message" => "Yes,Please update me asap");

        $CI->Welcome_model->insert_data("request_discussions", $send_message);

        $send_message = array("request_id" => $success,
            "sender_type" => "qa",
            "sender_id" => $customer_data[0]['qa_id'],
            "reciever_id" => $id,
            "reciever_type" => "customer",
            "admin_seen" => 1,
            "qa_seen" => 1,
            "designer_seen" => 1,
            "customer_seen" => 1,
            "created" => date("Y-m-d H:i:s"),
            "message" => "Your design is uploaded,please check and approve it");

        $CI->Welcome_model->insert_data("request_discussions", $send_message);

        $send_message = array("request_id" => $success,
            "sender_type" => "customer",
            "sender_id" => $id,
            "reciever_id" => 0,
            "reciever_type" => "designer",
            "admin_seen" => 1,
            "qa_seen" => 1,
            "designer_seen" => 1,
            "customer_seen" => 1,
            "created" => date("Y-m-d H:i:s"),
            "message" => "Thanks for update me");

        $CI->Welcome_model->insert_data("request_discussions", $send_message);

        /*         * ***************** end send default messages ********************** */
    }

    public function delete_dummy_request($customer_id) {
        $CI = & get_instance();
        $request = $CI->Request_model->get_dummy_request($customer_id, 1, '');
        if (!empty($request)) {
            $CI->Admin_model->delete_request_data("request_discussions", $request[0]['id']);
            $CI->Admin_model->delete_request_data("message_notification", $request[0]['id']);
            $CI->Admin_model->delete_request_data("notification", $request[0]['id']);



            $all_files_nm = $CI->Admin_model->get_all_files_rid($request[0]['id']);

            $folderNmae = 'public/uploads/requests/' . $request[0]['id'];
            foreach ($all_files_nm as $files) {
                $file_id[] = $files['id'];
                $file_name[] = $files['file_name'];
                $file_name[] = $files['src_file'];
            }
            foreach ($file_name as $req_files) {
                if (in_array($req_files, array(".", "..")))
                    continue;
                $CI->load->library('s3_upload');
                $CI->s3_upload->delete_file_from_s3($folderNmae . '/' . $req_files);
                $CI->s3_upload->delete_file_from_s3($folderNmae . '/_thumb/' . $req_files);
                $CI->s3_upload->delete_file_from_s3($folderNmae);
            }

            $all_requests_id[] = $request_all['id'];
            if (!empty($file_id)) {
                $CI->Admin_model->delete_request_file_data("request_file_chat", $file_id);
                $CI->Admin_model->delete_request_data("request_files", $request[0]['id']);
            }
            $CI->Admin_model->alldelete_request('requests', $request[0]['id']);
        }
    }

    public function move_project_inqueqe_to_inprogress($user_id, $request_id = NULL,$actionfrom = NULL,$lastupdtreq = NULL) {
        $CI = & get_instance();
        $customer_data = $CI->Admin_model->getuser_data($user_id);
        $login_user_id = $CI->load->get_var('login_user_id');
        $pervious_status = array("active","disapprove","checkforapprove");
        $getsubscriptionplan = $CI->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
        if ($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])) {
            $turn_around_days = $customer_data[0]['turn_around_days'];
        } else {
            $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
        }
        $subusersdata = $CI->Request_model->getAllsubUsers($user_id, "client", "active");
        if ($request_id == '' || $request_id == NULL) {
            if (($getsubscriptionplan[0]['is_agency'] == 1 && !empty($subusersdata) && $request_id == '') || ($getsubscriptionplan[0]['is_agency'] == 1 && $customer_data[0]['parent_id'] == 0)) {
                $lastupdtreq_d = $CI->Request_model->get_request_by_id($lastupdtreq);
                $project_user = ($lastupdtreq_d[0]['created_by'] == 0) ? $user_id : $lastupdtreq_d[0]['created_by'];
                $pro_userdata = $CI->Admin_model->getuser_data($project_user);
//                echo $project_user;
                $checkslot = $CI->myfunctions->getslotofallusers($customer_data, $pro_userdata);
                $checkslot['project_user'] = $project_user;
                $all_requests[0]['id'] = $CI->myfunctions->moverequests_checksubuserlist($user_id, $checkslot);
                $slotavalible = 1;
            } else {
                $all_requests = $CI->Request_model->getall_newrequest($user_id, array('pending', 'assign'), "", "priority");
                $slotavalible = 0;
            }
            
        } else {
            $all_requests[0]['id'] = $request_id;
            $slotavalible = 0;
        }
        if (isset($all_requests[0]['id'])) {
            $created_userdata = array();
            $request_data = $CI->Request_model->get_request_by_id($all_requests[0]['id']);
            if ($request_data[0]['created_by'] != 0 && $request_data[0]['created_by'] != $user_id) {
                $created_userdata = $CI->Admin_model->getuser_data($request_data[0]['created_by']);
            }
            $allpreviousstatuses = $CI->customfunctions->allStatusafterUnhold($request_data[0]['previous_status']);
            $status_cust = $allpreviousstatuses['status'];
            $prv_status = $request_data[0]['status'];
            if($actionfrom == "markAsInProgress" && $request_data[0]["status"] == "approved"){
                $status = 'disapprove';
                $status_admin = 'disapprove';
                $status_designer = 'disapprove';
                $status_qa = 'disapprove';
            }else{
                if(in_array($status_cust, $pervious_status)){
                    $status = $allpreviousstatuses['status'];
                    $status_admin = $allpreviousstatuses['status_admin'];
                    $status_designer = $allpreviousstatuses['status_designer'];
                    $status_qa = $allpreviousstatuses['status_qa'];
                } else {
                    $status = 'active';
                    $status_admin = 'active';
                    $status_designer = 'active';
                    $status_qa = 'active';
                }
            }
            if (!empty($created_userdata) && $created_userdata[0]['user_flag'] == 'client') {
                $loginuser_d = $created_userdata;
                $clientflag = 1;
            } else {
                $loginuser_d = $customer_data;
                $clientflag = 0;
            }
            if($slotavalible == 1){
                $subActiveReq = $this->checkactiveslotaccuser($request_data[0]['customer_id'],$checkslot,$loginuser_d);
            }else{
                $subActiveReq = 1;
            }
            if($subActiveReq == 1){
                $prev_priority = $request_data[0]['priority'];
                $subprev_priority = $request_data[0]['sub_user_priority'];
                $subcat_data = $CI->Category_model->get_category_byID($request_data[0]['subcategory_id']);
                $expected = $CI->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $customer_data[0]['plan_name'], $request_data[0]['category_bucket'], $subcat_data[0]['timeline'], $turn_around_days);
                $success = $CI->Welcome_model->update_data("requests", array("status" => $status, "status_admin" => $status_admin, "status_designer" => $status_designer, "status_qa" => $status_qa, "dateinprogress" => date("Y-m-d H:i:s"), "priority" => 0, "sub_user_priority" => 0, "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $all_requests[0]['id']));
                if($prv_status == "assign"){
                    $CI->Admin_model->update_priority_after_approved('1', $user_id, $prev_priority);
                        if ($clientflag == 1) {
                            $CI->Admin_model->update_subuserpriority_after_approved('1', $user_id, $request_data[0]['created_by'], $subprev_priority);
                        }
                }
                $CI->myfunctions->SendQAmessagetoInprogressReq($all_requests[0]['id'], $customer_data[0]['qa_id'], $user_id);
                $CI->myfunctions->capture_project_activity($all_requests[0]['id'],'','',$request_data[0]['status'].'_to_'.$status,$actionfrom,$login_user_id);
            }
        }
        if ($success) {
            return true;
        }
    }

    function moverequests_checksubuserlist($user_id,$slot) {
        $CI = & get_instance();
       // echo "<pre>";print_R($slot);exit;
        $subusers_manager = $CI->Request_model->getAllsubUsers($user_id, "manager", "active");
        if (!empty($subusers_manager)) {
            foreach ($subusers_manager as $manager_value) {
                $manager_id[] = $manager_value;
            }
        }
        $users = array();
        if($slot["user_type"] == "main"){
            $onetime_users = $CI->Clients_model->get_sharedusers($user_id,2);
            if(!empty($manager_id) && !empty($onetime_users)){
                $users = array_merge($onetime_users, $manager_id);
            }else if(empty($onetime_users)){
                $users = $manager_id;
            }else{
                $users = $onetime_users;
            }
            $type = "manager";
        }else if($slot["user_type"] == "shared"){
            $users = $CI->Clients_model->get_sharedusers($user_id,1);
            $type = "client";
        }else if($slot["user_type"] == "dedicated"){
            $users = $slot['project_user'];
            $type = "client";
        }else{
            $onetime_users = $CI->Clients_model->get_sharedusers($user_id,2);
            if(!empty($manager_id) && !empty($onetime_users)){
                $users = array_merge($onetime_users, $manager_id);
            }else if(empty($onetime_users)){
                $users = $manager_id;
            }else{
                $users = $onetime_users;
            }
            $type = "manager";
        }
        
        $clients = array();
        if($slot["user_type"] == "dedicated"){
            $clients[] = $users;
        }else{
            if (!empty($users)) {
                foreach ($users as $user_value) {
                    $clients[] = $user_value['id'];
                }
            }
        }
        $all_requests = $CI->Request_model->getall_newrequestforsubuser($user_id, array('pending', 'assign'), $clients, "priority", $type);
        $request_id = $all_requests[0]['id'];
        return $request_id;
    }

    function getDatesFromRange($start, $end, $format = 'Y-m-d') {

        // Declare an empty array 
        $array = array();

        // Variable that store the date interval 
        // of period 1 day 
        $interval = new DateInterval('P1D');

        $realEnd = new DateTime($end);
        $realEnd->add($interval);

        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

        // Use loop to store date into array 
        foreach ($period as $date) {
            $array[] = $date->format($format);
        }

        // Return the array elements 
        return $array;
    }

    public function getsubdomain_userdata() {

        $CI = & get_instance();
        $subdomain_userdata = array();

        $created_user = $CI->load->get_var('main_user');
        $customer_data = $CI->Admin_model->getuser_data($created_user,'plan_name');
        $getsubscriptionplan = $CI->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);

        if ($getsubscriptionplan[0]['is_agency'] == 1) {
            $user_id = $created_user;
        } else {
            $user_id = "";
        }
        $subdomain_name = (explode(".", HOST_NAME));
        if (!empty($subdomain_name)) {
            $getuser = $CI->Request_model->getuserinfobydomain(HOST_NAME);
        }
        if (empty($getuser)) {
            $getuser = $CI->Request_model->getuserinfobydomain("", $user_id);
        }
        if(!empty($getuser) && $getuser[0]['is_main_domain'] == 1){
            $userinfo = $CI->Admin_model->getuser_data($getuser[0]['user_id'],'company_name');
        }else{
            $userinfo[0]['company_name'] = "Graphics Zoo";
        }
        $getuser[0] = isset($getuser[0]) ? $getuser[0] : array();

        $subdomain_userdata['custom_logo'] = ($getuser[0]['logo'] != '') ? FS_PATH_PUBLIC_UPLOADS_USER_LOGO . $getuser[0]['customer_id'] . '/' . $getuser[0]['logo'] : FS_PATH_PUBLIC_ASSETS . 'front_end/Updated_Design/img/Dashboard_logo.svg';
        $subdomain_userdata['custom_favicon'] = ($getuser[0]['favicon'] != '') ? FS_PATH_PUBLIC_UPLOADS_USER_LOGO . $getuser[0]['customer_id'] . '/favicon/' . $getuser[0]['favicon'] :BASEURL.'favicon.ico';
        $subdomain_userdata['themePrimaryColor'] = ($getuser[0]['primary_color'] != '') ? $getuser[0]['primary_color'] : '#e42547';
        $subdomain_userdata['secondary_color'] = ($getuser[0]['secondary_color'] != '') ? $getuser[0]['secondary_color'] : '#1a3148';
        $subdomain_userdata['inprog_bkg_color'] = ($getuser[0]['inprog_bkg_color'] != '') ? $getuser[0]['inprog_bkg_color'] : '#37c473';
        $subdomain_userdata['revsion_bkg_color'] = ($getuser[0]['revsion_bkg_color'] != '') ? $getuser[0]['revsion_bkg_color'] : '#FF8C00';
        $subdomain_userdata['review_bkg_color'] = ($getuser[0]['review_bkg_color'] != '') ? $getuser[0]['review_bkg_color'] : '#000080';
        $subdomain_userdata['draft_bkg_color'] = ($getuser[0]['draft_bkg_color'] != '') ? $getuser[0]['draft_bkg_color'] : '#333332';
        $subdomain_userdata['hold_bkg_color'] = ($getuser[0]['hold_bkg_color'] != '') ? $getuser[0]['hold_bkg_color'] : '#ec2929';
        $subdomain_userdata['cancel_bkg_color'] = ($getuser[0]['cancel_bkg_color'] != '') ? $getuser[0]['cancel_bkg_color'] : '#c0304a';
        $subdomain_userdata['inqueue_bkg_color'] = ($getuser[0]['inqueue_bkg_color'] != '') ? $getuser[0]['inqueue_bkg_color'] : '#f3c500';
        $subdomain_userdata['complete_bkg_color'] = ($getuser[0]['complete_bkg_color'] != '') ? $getuser[0]['complete_bkg_color'] : '#37c473';
        $subdomain_userdata['approve_bg_color'] = ($getuser[0]['approve_button_color'] != '') ? $getuser[0]['approve_button_color'] : '#39b54a';
        $subdomain_userdata['setting_tab_bkg_color'] = ($getuser[0]['setting_tab_bkg_color'] != '') ? $getuser[0]['setting_tab_bkg_color'] : '#1b1d8a';
        $subdomain_userdata['draft_font_color'] = ($getuser[0]['draft_font_color'] != '') ? $getuser[0]['draft_font_color'] : '#ffffff';
        $subdomain_userdata['hold_font_color'] = ($getuser[0]['hold_font_color'] != '') ? $getuser[0]['hold_font_color'] : '#ffffff';
        $subdomain_userdata['cancel_font_color'] = ($getuser[0]['cancel_font_color'] != '') ? $getuser[0]['cancel_font_color'] : '#ffffff';
        $subdomain_userdata['inqueue_font_color'] = ($getuser[0]['inqueue_font_color'] != '') ? $getuser[0]['inqueue_font_color'] : '#ffffff';
        $subdomain_userdata['inprog_font_color'] = ($getuser[0]['inprog_font_color'] != '') ? $getuser[0]['inprog_font_color'] : '#ffffff';
        $subdomain_userdata['revsion_font_color'] = ($getuser[0]['revsion_font_color'] != '') ? $getuser[0]['revsion_font_color'] : '#ffffff';
        $subdomain_userdata['review_font_color'] = ($getuser[0]['review_font_color'] != '') ? $getuser[0]['review_font_color'] : '#ffffff';
        $subdomain_userdata['complete_font_color'] = ($getuser[0]['complete_font_color'] != '') ? $getuser[0]['complete_font_color'] : '#ffffff';
        $subdomain_userdata['agency_livechat_script'] = ($getuser[0]['live_script'] != '') ? $getuser[0]['live_script'] : '';
        $subdomain_userdata['physical_address'] = ($getuser[0]['physical_address'] != '') ? $getuser[0]['physical_address'] : '';
        $subdomain_userdata['contact_number'] = ($getuser[0]['contact_number'] != '') ? $getuser[0]['contact_number'] : '';
        $subdomain_userdata['email_address'] = ($getuser[0]['email_address'] != '') ? $getuser[0]['email_address'] : '';
        $subdomain_userdata['success_msg_color'] = ($getuser[0]['success_msg_color'] != '') ? $getuser[0]['success_msg_color'] : '#3c763d';
        $subdomain_userdata['error_msg_color'] = ($getuser[0]['error_msg_color'] != '') ? $getuser[0]['error_msg_color'] : '#a94442';
        $subdomain_userdata['success_msg_bg_color'] = ($getuser[0]['success_msg_bg_color'] != '') ? $getuser[0]['success_msg_bg_color'] : '#dff0d8';
        $subdomain_userdata['error_msg_bg_color'] = ($getuser[0]['error_msg_bg_color'] != '') ? $getuser[0]['error_msg_bg_color'] : '#f2dede';
        $subdomain_userdata['page_title'] = ($getuser[0]['company_name'] != '') ? $getuser[0]['company_name'] : '';
        $subdomain_userdata['show_signup'] = ($getuser[0]['show_signup'] != '')?$getuser[0]['show_signup']:1;
        $subdomain_userdata['client_script'] = unserialize($getuser[0]['script_data']);
        return $subdomain_userdata;
    }

    function changerequeststatus_ofallusertype($created_user, $id, $customer_data, $bucketType, $subcat_data, $designerid, $assignornot, $login_userdata = null) {
        $CI = & get_instance();
        $totalInprogress = $customer_data[0]['total_inprogress_req'];
        $get_active_design = $CI->Admin_model->get_all_request_count(array("active"), $created_user);
        $getsubscriptionplan = $CI->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
        if ($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])) {
            $turn_around_days = $customer_data[0]['turn_around_days'];
        } else {
            $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
        }
        if (in_array($customer_data[0]['plan_name'], NEW_PLANS)) {
            $expected = $CI->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $customer_data[0]['plan_name'], $bucketType, $subcat_data[0]['timeline'], $turn_around_days);
            $data2 = array("status" => "active", "status_admin" => "active", "status_designer" => "active", "status_qa" => "active", "designer_id" => $designerid, "is_active" => "1", "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s"), "designer_assign_or_not" => $assignornot, "priority" => 0);
            $CI->myfunctions->SendQAmessagetoInprogressReq($id, $customer_data[0]['qa_id'], $created_user);
        } else {
            if (sizeof($get_active_design) >= $totalInprogress) {
                $data2 = array("status" => "assign", "status_admin" => "assign", "status_designer" => "assign", "status_qa" => "assign", "designer_id" => $designerid, "is_active" => "1", "designer_assign_or_not" => $assignornot, "modified" => date("Y-m-d H:i:s"));
                $priority = $CI->Welcome_model->priority($created_user);
                $data2['priority'] = $priority[0]['priority'] + 1;
            } else {
                $ActiveReq = $CI->myfunctions->CheckActiveRequest($created_user);
                if ($ActiveReq == 1) {
                    $expected = $CI->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $customer_data[0]['plan_name'], $bucketType, $subcat_data[0]['timeline'], $turn_around_days);
                    $data2 = array("status" => "active", "status_admin" => "active", "status_designer" => "active", "status_qa" => "active", "designer_id" => $designerid, "is_active" => "1", "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s"), "designer_assign_or_not" => $assignornot, "priority" => 0);
                    $CI->myfunctions->SendQAmessagetoInprogressReq($id, $customer_data[0]['qa_id'], $created_user);
                } else {
                    $data2 = array("status" => "assign", "status_admin" => "assign", "status_designer" => "assign", "status_qa" => "assign", "designer_id" => $designerid, "is_active" => "1", "created" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s"), "designer_assign_or_not" => $assignornot);
                    $priority = $CI->Welcome_model->priority($created_user);
                    $data2['priority'] = $priority[0]['priority'] + 1;
                }
            }
        }
        return $data2;
    }

    function changerequeststatus_ofsubusertype($created_user, $id, $customer_data, $bucketType, $subcat_data, $designerid, $assignornot, $login_userdata = null,$slot = null) {
        $CI = & get_instance();
        $totalInprogress = $customer_data[0]['total_inprogress_req'];
        $get_active_design = $CI->Admin_model->get_all_request_count(array("active"), $created_user);
        $getsubscriptionplan = $CI->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
        if ($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])) {
            $turn_around_days = $customer_data[0]['turn_around_days'];
        } else {
            $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
        }
        if (sizeof($get_active_design) >= $totalInprogress) {
            $data2 = array("status" => "assign", "status_admin" => "assign", "status_designer" => "assign", "status_qa" => "assign", "designer_id" => $designerid, "is_active" => "1", "designer_assign_or_not" => $assignornot, "modified" => date("Y-m-d H:i:s"));
            
            $manage_priority = $CI->myfunctions->managepriority_for_agnecyusers($created_user,$login_userdata,$slot);
            $data2['priority'] = $manage_priority;
            if($slot['user_type'] != "main"){
                $sub_user_priority = $CI->Welcome_model->sub_user_priority($login_userdata[0]['id']);
                $data2['sub_user_priority'] = $sub_user_priority[0]['sub_user_priority'] + 1;
            }
            
        } else {
            //check main user configration
            $ActiveReq = $CI->myfunctions->CheckActiveRequest($created_user);
            if ($ActiveReq == 1) {
                    $subActiveReq = $this->checkactiveslotaccuser($created_user,$slot,$login_userdata);
                if ($subActiveReq == 1) {
                    $expected = $CI->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $customer_data[0]['plan_name'], $bucketType, $subcat_data[0]['timeline'], $turn_around_days);
                    $data2 = array("status" => "active", "status_admin" => "active", "status_designer" => "active", "status_qa" => "active", "designer_id" => $designerid, "is_active" => "1", "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s"), "designer_assign_or_not" => $assignornot, "priority" => 0,"sub_user_priority" => 0);
                    $CI->myfunctions->SendQAmessagetoInprogressReq($id, $customer_data[0]['qa_id'], $created_user);
                } else {
                    $data2 = array("status" => "assign", "status_admin" => "assign", "status_designer" => "assign", "status_qa" => "assign", "designer_id" => $designerid, "is_active" => "1", "created" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s"), "designer_assign_or_not" => $assignornot);
                    $manage_priority = $CI->myfunctions->managepriority_for_agnecyusers($created_user,$login_userdata,$slot);
                    $data2['priority'] = $manage_priority;
                    if($slot['user_type'] != "main"){
                        $sub_user_priority = $CI->Welcome_model->sub_user_priority($login_userdata[0]['id']);
                        $data2['sub_user_priority'] = $sub_user_priority[0]['sub_user_priority'] + 1;
                    }
                }
            } else {
                $data2 = array("status" => "assign", "status_admin" => "assign", "status_designer" => "assign", "status_qa" => "assign", "designer_id" => $designerid, "is_active" => "1", "created" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s"), "designer_assign_or_not" => $assignornot);
                $manage_priority = $CI->myfunctions->managepriority_for_agnecyusers($created_user,$login_userdata,$slot);
                $data2['priority'] = $manage_priority;
                if($slot['user_type'] != "main"){
                    $sub_user_priority = $CI->Welcome_model->sub_user_priority($login_userdata[0]['id']);
                    $data2['sub_user_priority'] = $sub_user_priority[0]['sub_user_priority'] + 1;
                }
            }
        }
        return $data2;
    }
    
        public function checkactiveslotaccuser($created_user,$slot,$login_userdata) {
         $CI = & get_instance();
         
        if ($slot['user_type'] == "shared") {
            $shareduser = $CI->Clients_model->get_sharedusers($created_user, 1);
            foreach ($shareduser as $value) {
                $users[] = $value['id'];
            }
            $slot_moveable = $CI->myfunctions->CheckActiveRequest($users, "created", "", "", $slot);
            if ($slot_moveable == 1) {
                $change_slot = $slot;
                $change_slot['inprogress'] = $login_userdata[0]['total_inprogress_req'];
                $change_slot['active'] = $login_userdata[0]['total_active_req'];
                $subActiveReq = $CI->myfunctions->CheckActiveRequest($login_userdata[0]['id'], "created", "", "", $change_slot);
            } else {
                $subActiveReq = 0;
            }
        } 
        else {
            if ($slot['user_type'] == "dedicated") {
                $subActiveReq = $CI->myfunctions->CheckActiveRequest($login_userdata[0]['id'], "created", "", "", $slot);
            } else {
                $subusers_manager = $CI->Request_model->getAllsubUsers($created_user, "manager", "active");
                if (!empty($subusers_manager)) {
                    foreach ($subusers_manager as $manager_value) {
                        $manager_id[] = $manager_value['id'];
                    }
                }
                $one_time = $CI->Clients_model->get_sharedusers($created_user, 2);
                foreach ($one_time as $value) {
                    $onetime_users[] = $value['id'];
                }
                if (empty($onetime_users)) {
                    $onetime_users = array($login_userdata[0]['id']);
                }
                if (!empty($manager_id)) {
                    $one_timeuser = array_merge($onetime_users, $manager_id);
                } else {
                    $one_timeuser = $onetime_users;
                }
                $subActiveReq = $CI->myfunctions->CheckActiveRequest($one_timeuser, "main", "", "", $slot, $created_user);
            }
        }
        return $subActiveReq;
    }

    public function managepriority_for_agnecyusers($created_user,$login_userdata,$slot) {
        $CI = & get_instance();
        if($slot['user_type'] == "dedicated"){
            $users = $CI->Clients_model->get_sharedusers($created_user,0);
            foreach ($users as $dedicated) {
                $dedicated_users[] = $dedicated['id'];
            }
            $requests = $CI->Clients_model->get_req_not_created_bydedicated(array("assign"),$created_user,$dedicated_users);
            if(empty($requests)){
                $max_priority = $CI->Welcome_model->priority($created_user);
                $priority = $max_priority[0]['priority'] + 1;
            }else{
             $priority = $requests[0]['priority'];
            }
        }else if($slot['user_type'] == "shared"){
            $login_inq = $CI->Admin_model->get_all_request_count(array("assign"), $login_userdata[0]['id'], 'created');
            $loginuser_req = sizeof($login_inq)+1;
            $priority = $CI->myfunctions->shared_reqwith_priorties($created_user,$loginuser_req);
            
        }else{
                $max_priority = $CI->Welcome_model->priority($created_user);
                $priority = $max_priority[0]['priority'] + 1;
        }
            return $priority;
            
    }
    
    public function shared_reqwith_priorties($created_user,$loginuser_req) {
        $CI = & get_instance();
        $shareduser = $CI->Clients_model->get_sharedusers($created_user,1);
        $users = array();
        $ids = array();
        foreach ($shareduser as $value) {
            $users[] = $value['id'];
        }
        $sharedreq = $CI->Clients_model->get_req_not_created_bydedicated(array("assign"), $created_user, $users, 'shared');
        if (!empty($sharedreq)) {
            $temp=0;
            foreach ($sharedreq as $request) {
                if ($request['count'] > $loginuser_req) {
                    $ids = $request['created_by'];
                    
                    $sharedrq = $CI->Clients_model->get_all_shareduserreq(array("assign"), $created_user, $ids);
                    $i = 0;
                    foreach ($sharedrq as $rq) {
                        $i++;
                        if (($temp > $rq['priority'] || $temp == 0) && $i > $loginuser_req) {
                            if ($rq['priority'] > $loginuser_req) {
                                if ($temp != 0 && $temp < $rq['priority']) {
                                    $temp = $temp;
                                } else {
                                    $temp = $rq['priority'];
                                }

                                break;
                            }
                        }
                    }
                }
            }
            if($temp != 0){
                    $priority = $temp;
            }else{
                $max_priority = $CI->Clients_model->get_priority_forshared("max(priority) as priority",array("assign"), $created_user, $users);
                $priority = $max_priority[0]['priority'] + 1;
            }
        } else {
            $one_timeuser = array();
            $one_time = $CI->Clients_model->get_sharedusers($created_user,2);
            foreach ($one_time as $value) {
            $one_timeuser[] = $value['id'];
            }
            $one_timepro = $CI->Clients_model->one_time_users_project(array("assign"), $created_user, $one_timeuser, "min(priority) as priority");
            if($one_timepro[0]['priority'] != ""){
                $priority = $one_timepro[0]['priority'];
            }else{
                $max_priority = $CI->Welcome_model->priority($created_user);
                $priority = $max_priority[0]['priority'] + 1;
            }
        }
        return $priority;
    }


    public function IsSubscriptionCancel($uid, $flag = NULL,$clntid="") {
        $CI = & get_instance();
        $customer_data = $CI->Admin_model->getuser_data($uid);
        if ($flag == 'from_admin_cancel') {
            $allreq = $CI->Request_model->getallrequestaccstatus($uid, array('approved','cancel'),$clntid); 
            foreach ($allreq as $rk => $rv) {
                if ($rv['previous_status'] != '' || $rv['previous_status'] == NULL) {
                    $previous_status = $rv['status'];
                }
                $success = $CI->Welcome_model->update_data("requests", array("status" => 'cancel', "status_admin" => 'cancel', "status_designer" => 'cancel', "status_qa" => 'cancel', "previous_status" => $previous_status, "latest_update" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $rv['id']));
            }
        }
        if ($customer_data[0]['is_cancel_subscription'] == 1) {
            return true;
        }
    }

    public function faliedpaymentstatususer($userid) {
        $CI = &get_instance();
        $userdata = array();
        $profile_data = $CI->Admin_model->getuser_data($userid);
        if ($profile_data[0]['parent_id'] == 0 || $profile_data[0]['user_flag'] == "client") {
            $userdata['parent_user'] = $_SESSION['user_id'];
        } else {
            $userdata['parent_user'] = $profile_data[0]['parent_id'];
        }
        $userdata['parent_user_d'] = $CI->Admin_model->getuser_data($userdata['parent_user']);
        $userdata['parent_payment_status'] = $userdata['parent_user_d'][0]['payment_status'];
        $userdata['parent_cancel_subscription'] = $userdata['parent_user_d'][0]['is_cancel_subscription'];
//         echo "<pre>";print_r($userdata['parent_user_d']);exit;
        if ($userdata['parent_payment_status'] == 0 && $userdata['parent_cancel_subscription'] != 1) {
            $userdata['parent_billing_startdate'] = $userdata['parent_user_d'][0]['billing_start_date'];
            $userdata['parent_billing_enddate'] = $userdata['parent_user_d'][0]['billing_end_date'];
            $currentdate = date("y-m-d h:i:s");
            $datediff = strtotime($currentdate) - strtotime($userdata['parent_billing_startdate']);
            $userdata['days_diff'] = round($datediff / (60 * 60 * 24));
            if ($userdata['days_diff'] >= 1 && $userdata['days_diff'] < 4) {
                $userdata['usermessage'] = "Payment is past due. Please update card details in settings.";
            } elseif ($userdata['days_diff'] >= 4) {
                $userdata['usermessage'] = "Payment is past due. Your account is currently suspended. Please update card details in settings.";
            } elseif ($userdata['days_diff'] == 7) {
                $userdata['usermessage'] = "Payment is past due. Subscription has been cancelled. Please reactivate your subscription for more designs.";
            } else {
                $userdata['usermessage'] = "Payment is past due. Please update card details in settings.";
            }
        } else if ($userdata['parent_cancel_subscription'] == 1) {
            $userdata['parent_plan_name'] = $userdata['parent_user_d'][0]['plan_name'];
            $userdata['parent_customerid'] = $userdata['parent_user_d'][0]['customer_id'];
            $userdata['parent_inprogress_req'] = $userdata['parent_user_d'][0]['total_inprogress_req'];
            $userdata['usermessage'] = "Payment is past due. Subscription has been cancelled. Please reactivate your subscription for more designs.";
        }

        return $userdata;
    }

    public function movecancelprojecttopreviousstatus($uid,$clint_id="") {
        $CI = & get_instance();
        $allreq = $CI->Request_model->getall_newrequest($uid, array('cancel'),"","","",$clint_id);
        if (!empty($allreq)) {
            foreach ($allreq as $rk => $rv) {
                if ($rv['previous_status'] != '' || $rv['previous_status'] != NULL) {
                    $status = $rv['previous_status'];
                    $allpreviousstatuses = $CI->customfunctions->allStatusafterUnhold($status); 
                    $subcat_data = $CI->Category_model->get_category_byID($rv['subcategory_id']);
                    $expected = $CI->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $planname, $subcat_data[0]['bucket_type'], $subcat_data[0]['timeline']);
                    $CI->Welcome_model->update_data("requests", array("status" => $allpreviousstatuses['status'], "status_admin" => $allpreviousstatuses['status_admin'], "status_designer" => $allpreviousstatuses['status_designer'], "status_qa" => $allpreviousstatuses['status_qa'], "previous_status" => '', "latest_update" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s"), "expected_date" => $expected), array("id" => $rv['id']));
                }
            }
        }
        return true;
    }
    
    function customerVariables($pro_status = NULL) {
        $cust_var = array();
        if ($pro_status == "checkforapprove") {
            $cust_var['status'] = "Review design";
            $cust_var['color'] = "agency_review";
        } elseif ($pro_status == "active") {
            $cust_var['status'] = "In Progress";
            $cust_var['color'] = "agency_inprogress";
        } elseif ($pro_status == "disapprove") {
            $cust_var['status'] = "Revision";
            $cust_var['color'] = "agency_revision";
            $cust_var['date'] = "expected";
        } elseif ($pro_status == "pending" || $pro_status == "assign") {
            $cust_var['status'] = "In Queue";
            $cust_var['color'] = "inqueue_status_button";
        } elseif ($pro_status == "hold") {
            $cust_var['status'] = "On Hold";
            $cust_var['color'] = "hold_status_button";
        }elseif ($pro_status == "cancel") {
            $cust_var['status'] = "cancelled";
            $cust_var['color'] = "cancel_status_button";
        }elseif ($pro_status == "approved") {
            $cust_var['status'] = "completed";
            $cust_var['color'] = "completed_status_button";
        } elseif ($pro_status == "draft") {
             $cust_var['status'] = "draft";
            $cust_var['color'] = "draft_status_button";
        } else {
            $cust_var['status'] = "";
            $cust_var['color'] = "greentext";
        }
        return $cust_var;
    }
    
    public function moveinqueuereq_toactive_after_updrage_plan($customer_id,$inprogress_request) {
        $CI = & get_instance();
        $user_data = $CI->Admin_model->getuser_data($customer_id);
        $all_requests = $CI->Request_model->getall_newrequest($customer_id, array('pending', 'assign'), "", "priority");
        if (!empty($all_requests)) {
            $all_activerequests = $CI->Request_model->getall_newrequest($customer_id, array("active"), "", "");
            $active_request_size = sizeof($all_activerequests);
            if ($active_request_size < $inprogress_request) {
                $moverquests = $inprogress_request - $active_request_size;
            }
            
            if (isset($moverquests)) {
                for ($i = 0; $i < $moverquests; $i++) {
                    $request_data = $CI->Request_model->get_request_by_id($all_requests[$i]['id']);
                    $subcat_data = $CI->Category_model->get_category_byID($request_data[0]['subcategory_id']);
                    $expected = $CI->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $customer_data[0]['plan_name'], $request_data[0]['category_bucket'], $subcat_data[0]['timeline']);
                    $success = $CI->Welcome_model->update_data("requests", array("status" => "active", "status_admin" => "active", "status_designer" => "active", "status_qa" => "active", "dateinprogress" => date("Y-m-d H:i:s"), "priority" => 0, "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $all_requests[$i]['id']));
                    $CI->Admin_model->update_priority_after_approved('1', $customer_id);
                    $CI->myfunctions->SendQAmessagetoInprogressReq($all_requests[$i]['id'], $user_data[0]['qa_id'], $customer_id);
                }
            }
        }
    }
    
    public function checkloginuser($role="") {
//        echo "<pre/>";print_R($_SESSION);exit;
        $CI = & get_instance();
        if (!$CI->session->userdata('user_id')) {
            if(isset($_COOKIE['is_remember']) && $_COOKIE['is_remember'] == 1){
                $encid = isset($_COOKIE['rememberID'])? $_COOKIE['rememberID']:'';
                $id = $CI->Request_model->my_decrypt($encid,BASE64KEY);
                $pwd = (isset($user[0]['password']) && $user[0]['password'] != '') ? $user[0]['password']: $user[0]['new_password'];
                $user = $CI->Request_model->getuserbyid($id);
                $CI->session->set_userdata('email', $user[0]['email']);
                $CI->session->set_userdata('password', $pwd);
                $CI->session->set_userdata('role', $user[0]['role']);
                $CI->session->set_userdata('timezone', $user[0]['timezone']);
                $CI->session->set_userdata('first_name', $user[0]['first_name']);
                $CI->session->set_userdata('last_name', $user[0]['last_name']);
                $CI->session->set_userdata('user_id', $user[0]['id']);
                $CI->session->set_userdata('tour', $user[0]['tour']);
                $CI->session->set_userdata('qa_id', $user[0]['qa_id']);
                redirect(current_url());
            }else{
                $protocol = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
                $base_url = $protocol . "://" . $_SERVER['HTTP_HOST'];
                $complete_url = $_SERVER["REQUEST_URI"];
                $requrl = ltrim($complete_url, '/');
                if($_SERVER['HTTP_HOST'] == DOMAIN_NAME){ 
                    $url = base_url() . 'login';
                }else{
                    $url = base_url();
                }
                redirect($url.'?url=' . $requrl);
            }
        }
        if ($role != "") {
            if ($CI->session->userdata('switch_userid') != $_SESSION['user_id'] && $_SESSION['switchtouseracc'] == 1 && $_SESSION['switch_userid'] != "") {
                redirect(base_url());
            }else if ($CI->session->userdata('role') != $role && $_SESSION['switchtouseracc'] == 1) {
                redirect(base_url() . 'exitaccess');
                //  $this->load->view('admin/loginasuser');
            } else if ($CI->session->userdata('role') != $role) {
                redirect(base_url());
            }
        }
    }
    
     public function capture_project_activity($requestid="" ,$draftid="",$designerid="",$slug="",$action_perform_from="",$action_perform_by="",$user_shown="",$admin_shown="",$designer_shown="",$missing_column="",$created="") {
        $CI = & get_instance(); 
        $activity = array(
            "request_id" => $requestid,
             "draft_id" => $draftid,
             "designer_id" => $designerid,
             "slug" => $slug,
             "action_perform_from" => $action_perform_from,
             "action_perform_by" => $action_perform_by,
             "user_shown" => ($user_shown != "")?$user_shown:1,
             "admin_shown" => ($admin_shown != "")?$admin_shown:1,
            "designer_shown" => ($designer_shown != "")?$designer_shown:1,
            "missing_column" => $missing_column,
            "created" => date("Y-m-d H:i:s"));
        $id = $CI->Welcome_model->insert_data("activity_timeline", $activity);
        if($id){
            return true;
        }
         
    }
    
     public function projectactivities($id,$usershow="") {
        $CI = & get_instance(); 
        $created_user = $CI->load->get_var('main_user');
        $loggedinuser = $CI->load->get_var('login_user_data');
        $user_data = $CI->Admin_model->getuser_data($created_user);
        $activities = $CI->Request_model->get_project_activities($id,$usershow);
        $subdomain_url = $CI->myfunctions->dynamic_urlforsubuser_inemail($created_user,$user_data[0]['plan_name']);
        $array = array();
        
        if(!empty($activities)){
            for ($ac = 0; $ac < count($activities); $ac++) {
               $activities[$ac]['profile_picture'] = $CI->customfunctions->getprofileimageurl($activities[$ac]['profile_picture']); 
               $activities[$ac]['created_dt'] = $CI->myfunctions->onlytimezoneforall($activities[$ac]['created']); 
               $activities[$ac]['current_time'] = $CI->myfunctions->onlytimezoneforall(date('Y-m-d H:i:s'));
               $activities[$ac]['created'] = $CI->myfunctions->secondsToTime($activities[$ac]['created_dt'],$activities[$ac]['current_time']); 
                 
               if(($activities[$ac]['draft_id'] != "" || $activities[$ac]['draft_id'] != 0) && $loggedinuser[0]['role'] == 'customer'){
                    $array['PROJECT_LINK'] = "<a href='".$subdomain_url."customer/request/project_image_view/".$activities[$ac]['draft_id'].'?id='.$activities[$ac]['request_id']."' target='_blank'><i class='fas fa-external-link-alt'></i></a>";
               }elseif(($activities[$ac]['draft_id'] != "" || $activities[$ac]['draft_id'] != 0) && $loggedinuser[0]['role'] == 'designer'){
                    $array['PROJECT_LINK'] = "<a href='".$subdomain_url."designer/request/project_image_view/".$activities[$ac]['draft_id'].'?id='.$activities[$ac]['request_id']."' target='_blank'><i class='fas fa-external-link-alt'></i></a>";
               }else{
                    $array['PROJECT_LINK'] = "<a href='".$subdomain_url."admin/dashboard/view_files/".$activities[$ac]['draft_id'].'?id='.$activities[$ac]['request_id']."' target='_blank'><i class='fas fa-external-link-alt'></i></a>";
               }
               
               
               if($activities[$ac]['slug'] == "enable_public_link"){
                   $url = $CI->Request_model->getPublicshareddata($activities[$ac]['request_id'],$activities[$ac]['draft_id']);
                   $array['SHARE_LINK'] = "<a href='".$url[0]['public_link']."' target='_blank'><i class='fas fa-external-link-alt'></i></a>";
               }
               
               $array['USER_NAME'] = $activities[$ac]['user_fisrtname'] .' '.$activities[$ac]['user_lastname'];
               
               /** get role **/
               if($activities[$ac]['user_role'] == "admin" && $activities[$ac]['user_adminaccess'] != 1){
                   $array['ROLE'] = "(AM)";
               }else if($activities[$ac]['user_role'] == "admin" && $activities[$ac]['user_adminaccess'] == 1){
                   $array['ROLE'] = "(GZ)";
               }else{
                   $array['ROLE'] = "(".$activities[$ac]['user_role'].")";
               }
               
               
               if($activities[$ac]['slug'] == "reassign_designer" || $activities[$ac]['slug'] == "assign_designer"){
             
                   if($activities[$ac]['slug'] == "assign_designer"){
                       $designerto = $CI->Admin_model->getuser_data($activities[$ac]['designer_id']);
                   }else{
                      $designerfrom = $CI->Admin_model->getuser_data($activities[$ac]['designer_id']);
                      $designerto = $CI->Admin_model->getuser_data($activities[$ac]['draft_id']); 
                   }
                   $array['DESIGNER_FROM'] = "<strong> ". $designerfrom[0]['first_name']."</strong> ";
                   $array['DESIGNER_TO'] = "<strong>". $designerto[0]['first_name']."</strong> ";
               } 
              if (count($array)):
                foreach ($array as $k => $v):
                     
                    $literal = '{{' . trim(strtoupper($k)) . '}}';
            
                    $activities[$ac]['msg'] = str_replace($literal, $v, $activities[$ac]['msg']);
              
                endforeach;
                
            endif;
            
            }
        }
        return $activities;
        
    }
    
    function secondsToTime($date,$currenttime) {

        $dbDate = strtotime($date);
                $endDate = strtotime($currenttime);
                $diff = $endDate - $dbDate;
                $days = floor($diff/86400);
                $hours = floor(($diff-$days*86400)/(60 * 60));
                $min = floor(($diff-($days*86400+$hours*3600))/60);
                $second = $diff - ($days*86400+$hours*3600+$min*60);

                 if($days > 0) $diff = $days." Days ago";
                 elseif($hours > 0) $diff = $hours." Hours ago";
                 elseif($min > 0) $diff = $min." Minutes ago";
                 else $diff = "Just now";
                 
        return $diff;
    }
    
    function numberwith_ordinal_suffix($number) {
        $ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
        if ((($number % 100) >= 11) && (($number % 100) <= 13))
            return $number . 'th';
        else
            return $number . $ends[$number % 10];
    }
    
    public function update_priority_after_unhold_project($prioritfrom, $prioritto, $request_id, $customer_id) {
        $CI = & get_instance();
        $priorityfrom = $prioritfrom;
        $priorityto = $prioritto;
        $id = $request_id;
        $userid = $customer_id;
        if (isset($userid) && $userid != '') {
            if ($priorityfrom < $priorityto) {
                for ($i = $priorityfrom + 1; $i <= $priorityto; $i++) {
                    $CI->Request_model->update_priority($i, $i - 1, '', $userid);
                }
            } else if ($priorityfrom > $priorityto) {
                for ($i = $priorityfrom - 1; $i >= $priorityto; $i--) {
                    $CI->Request_model->update_priority($i, $i + 1, '', $userid);
                }
            }
        }
        $CI->Request_model->update_priority($priorityfrom, $priorityto, $id);
    }

    public function update_sub_userpriority_after_unhold_project($prioritfrom, $prioritto, $request_id, $customer_id) {
        $CI = & get_instance();
        $priorityfrom = $prioritfrom;
        $priorityto = $prioritto;
        $id = $request_id;
        $userid = $customer_id;
        if (isset($userid) && $userid != '') {
            if ($priorityfrom < $priorityto) {
                for ($i = $priorityfrom + 1; $i <= $priorityto; $i++) {
                    $CI->Request_model->update_sub_userpriority($i, $i - 1, '', $userid);
                }
            } else if ($priorityfrom > $priorityto) {
                for ($i = $priorityfrom - 1; $i >= $priorityto; $i--) {
                    $CI->Request_model->update_sub_userpriority($i, $i + 1, '', $userid);
                }
            }
        }
        $CI->Request_model->update_sub_userpriority($priorityfrom, $priorityto, $id);
    }
    
     function designerVariables($pro_status = NULL,$who_reject = NULL,$date) {
        $desi_var = array();  
        if ($pro_status== "active") {
            $desi_var['status'] = "IN-Progress";
            $desi_var['colorclass'] = "green";
            $desi_var['datelabel']= '';
            $desi_var['date']= $date;
        }elseif($pro_status == "disapprove" && $who_reject == 1){
            $desi_var['status'] = "REVISION";
            $desi_var['colorclass'] = "orangetext";
            $desi_var['datelabel']= '';
            $desi_var['date']= $date;
        }elseif($pro_status == "disapprove" && $who_reject == 0){
            $desi_var['status'] = "Quality Revision";
            $desi_var['colorclass'] = "red";
            $desi_var['datelabel']= '';
            $desi_var['date']= $date;
        }elseif($pro_status == "pendingrevision"){
            $desi_var['status'] = "Pending Review";
            $desi_var['colorclass'] = "lightbluetext";
            $desi_var['datelabel']= 'Delivered on';
            $desi_var['date']= $date;
        } elseif($pro_status == "checkforapprove"){
            $desi_var['status'] = "Pending Approval";
            $desi_var['colorclass'] = "lightbluetext";
            $desi_var['datelabel']= '';
            $desi_var['date']= $date;
        } elseif($pro_status == "approved"){
            $desi_var['status'] = "approved";
            $desi_var['colorclass'] = "green";
            $desi_var['datelabel']= 'Approved on';
            $desi_var['date']= $date;
        } elseif($pro_status == "hold"){
            $desi_var['status'] = "On Hold";
            $desi_var['colorclass'] = "holdcolor";
            $desi_var['datelabel']= 'Hold on';
            $desi_var['date']= $date;
        } else{
          $desi_var['status'] = "";
            $desi_var['colorclass'] = "";
            $desi_var['datelabel']= "";  
            $desi_var['date']= $date;
        }
        return $desi_var;
    }
    
//    function designerVariables($pro_status = NULL,$who_reject = NULL,$DatE=null) {
//         $desi_var = array();     
//        if ($pro_status== "active") {
//            $desi_var['status'] = "IN-Progress";
//            $desi_var['colorclass'] = "green";
//            $desi_var['promText']= 'Expected on';
//        }elseif($pro_status == "disapprove" ){
//            $desi_var['status'] = "IN-Progress";
//            $desi_var['colorclass'] = "red";
//            $desi_var['promText']= 'Expected on';
//        } 
//        elseif ($pro_status == "status_designer" /* || $who_reject == 1*/ ) {
//            $desi_var['status'] = "REVISION";
//            $desi_var['promText']= 'Expected on';
//            $desi_var['colorclass'] = "red orangetext";
//        } elseif ($pro_status == "status_designer" /*|| $who_reject == 0 */ ) {
//            $desi_var['status'] = "Quality Revision";
//            $desi_var['colorclass'] = "red";
//            $desi_var['promText']= '';
//        } elseif ($pro_status == "pendingrevision") {
//            $desi_var['status'] = "Pending Review";
//            $desi_var['colorclass'] = "lightbluetext";
//            $desi_var['promText']= 'Delivered on';
//        } elseif ($pro_status == "checkforapprove") {
//            $desi_var['status'] = "Pending Approval";
//            $desi_var['colorclass'] = "bluetext";
//            $desi_var['promText']= '';
//        }elseif ($pro_status == "approved") 
//        {
//            $desi_var['status'] = "approved";
//            $desi_var['colorclass'] = "green";
//            $desi_var['promText']= 'Approved on';
//            $desi_var['ExDate']= $DatE;
//        }elseif ($pro_status == "hold") {
//            $desi_var['status'] = "On Hold";
//            $desi_var['colorclass'] = "holdcolor";
//            $desi_var['promText']= 'Hold on';
//            $desi_var['ExDate']= $DatE;
//        }else {
//            $desi_var['status'] = "";
//            $desi_var['colorclass'] = "green";
//            $desi_var['promText']= '';
//            $desi_var['ExDate']= '';
//        }
//        // echo $who_reject; 
//    
//        // print_r($desi_var); die;
//        return $desi_var;
//    }
    
    
    
    function adminVariables($pro_status = NULL,$project_msg = NULL,$who_reject = NULL){
        $admin_var = array();
        if ($pro_status == "checkforapprove") {
            $admin_var['deliverdate'] = "Delivered on " . date('M d, Y h:i A', strtotime($project_msg));
            $admin_var['datetime'] = date('M d, Y h:i A', strtotime($project_msg));
            $admin_var['status'] = "Pending Approval";
            $admin_var['color'] = "bluetext";
            $admin_var['label'] = "Task time";
        } elseif ($pro_status == "active") {
            $admin_var['deliverdate'] = date('M d, Y h:i A', strtotime($project_msg));
            $admin_var['status'] = "In Progress";
            $admin_var['datetime'] = date('M d, Y h:i A', strtotime($project_msg));
            $admin_var['color'] = "green";
             $admin_var['label'] = "Task time";
        } elseif ($pro_status == "disapprove" && $who_reject == 1) {
            $admin_var['deliverdate'] = date('M d, Y h:i A', strtotime($project_msg));
            $admin_var['status'] = "Revision";
            $admin_var['datetime'] = date('M d, Y h:i A', strtotime($project_msg));
            $admin_var['color'] = "orangetext";
             $admin_var['label'] = "Task time";
        } elseif ($pro_status == "disapprove" && $who_reject == 0) {
            $admin_var['deliverdate'] = date('M d, Y h:i A', strtotime($project_msg));
            $admin_var['status'] = "Quality Revision";
            $admin_var['datetime'] = date('M d, Y h:i A', strtotime($project_msg));
            $admin_var['color'] = "red";
             $admin_var['label'] = "Task time";
        } elseif ($pro_status == "pending" || $pro_status == "assign") {
            $admin_var['status'] = "In Queue";
            $admin_var['color'] = "gray";
             $admin_var['label'] = "Task time";
        } elseif ($pro_status == "draft") {
            $admin_var['status'] = "Draft";
            $admin_var['color'] = "gray";
             $admin_var['label'] = "Task time";
        } elseif ($pro_status == "approved") {
            $admin_var['deliverdate'] = "Approved on " . $project_msg;
            $admin_var['status'] = "Completed";
            $admin_var['color'] = "green";
             $admin_var['label'] = "Task time";
        } elseif ($pro_status == "pendingrevision") {
            $admin_var['deliverdate'] = $project_msg;
            $admin_var['status'] = "Pending Review";
            $admin_var['color'] = "lightbluetext";
             $admin_var['label'] = "Task time";
        } elseif ($pro_status == "cancel"){
            $admin_var['status'] = "Cancelled";
            $admin_var['color'] = "gray";
             $admin_var['label'] = "Task time";
        } else {
            $admin_var['status'] = "";
            $admin_var['color'] = "greentext";
             $admin_var['label'] = "Task time";
        }
        return $admin_var;
        
    }
    
    public function getslotofallusers($main_userdata,$login_userdata) {
        $CI = & get_instance(); 
        $total_dedicated = $main_userdata[0]['total_inprogress_req'];
        $slot = $CI->customfunctions->checkclientaddornot($main_userdata);
        $shared = $CI->Clients_model->getsubsbaseduserbyplanid($login_userdata[0]['plan_name'],"shared_user_type",$main_userdata[0]['id']);
        if($login_userdata[0]['parent_id'] != 0 && $login_userdata[0]['user_flag'] == 'client'){
            
            if ($shared[0]['shared_user_type'] == 1) {
                
                $total = $slot["dedicatedusers"] + $slot['reserve_count'];
                $shared_dedicated = $total_dedicated - $total;
                $shareduser = ($shared_dedicated >= 0)?$shared_dedicated:0;
                $active_slot = $shareduser * TOTAL_ACTIVE_REQUEST;
                $clientslot = array('inprogress' => $shareduser,
                        'active' => $active_slot,
                        'user_type' => "shared");  //for shared users
                
            } else if ($shared[0]['shared_user_type'] == 0) {
                
                $clientslot = array('inprogress' => $login_userdata[0]['total_inprogress_req'],
                        'active' => $login_userdata[0]['total_inprogress_req']* TOTAL_ACTIVE_REQUEST,
                        'user_type' => "dedicated");  //for dedicated users
            } else {
                
                $clientslot = array('inprogress' => $slot['reserve_count'],
                        'active' => $slot['reserve_count']* TOTAL_ACTIVE_REQUEST,
                        'user_type' => "one_time");  //for reserve slot
            }
        }else{
                $clientslot = array('inprogress' => $slot['reserve_count'],
                       'active' => $slot['reserve_count']* TOTAL_ACTIVE_REQUEST,
                       'user_type' => "main");  //for admin reserve slot
        }
//        echo "<pre>";print_r($clientslot);exit;
        return $clientslot;
        
    }
    
    public function getfacesaccavg($avg){
        if ($avg >= 0 && $avg < 3) {
            $svg = "sad-red.svg";
        } else if ($avg >= 3 && $avg <= 4.24) {
            $svg = "happiness-yellow.svg";
        } else {
            $svg = "happy-green.svg";
        }
        return $svg;
    }
    
    /******affiliate*******/
    public function setaffiliatedcookie() {
        if(isset($_GET['referral']) && $_GET['referral'] != ''){
            $affiliated_key =  $_GET['referral'];
            setcookie("affiliated_key", $affiliated_key, time()+(3600*24*2));
        }
    }

}
