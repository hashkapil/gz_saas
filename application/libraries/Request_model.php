<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Request_model extends CI_Model {
    /* public function get_request_by_id($id)
      {
      $this->db->select("requests.*,
      style_suggestion.additional_description,
      style_suggestion.color,
      style_suggestion.image,
      request_files.file_name,");
      //$this->db->from("requests");
      $this->db->join('style_suggestion','style_suggestion.request_id = requests.id');
      $this->db->join('request_files','request_files.request_id = requests.id');
      $this->db->where("request_files.user_type='customer'");
      $this->db->where("requests.id='$id'");
      $data = $this->db->get('requests');
      $result = $data->result_array();
      for($i=0;$i<sizeof($result);$i++){
      $designer = $this->getuserbyid($result[$i]['designer_id']);
      $customer = $this->getuserbyid($result[$i]['customer_id']);
      if(!empty($designer)){
      $result[$i]['designer_first_name'] = $designer[0]['first_name'];
      $result[$i]['designer_last_name'] = $designer[0]['last_name'];
      }else{
      $result[$i]['designer_first_name'] = "";
      $result[$i]['designer_last_name'] = "";
      }
      if(!empty($customer)){
      $result[$i]['customer_first_name'] = $customer[0]['first_name'];
      $result[$i]['customer_last_name'] = $customer[0]['last_name'];
      }else{
      $result[$i]['customer_first_name'] = "";
      $result[$i]['customer_last_name'] = "";
      }
      }
      return $result;
      } */

    public function getallactive_request() {
        $this->db->select("*");
        $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
        $this->db->where("status!='approved'");
        $this->db->where("status!='draft'");
        $this->db->order_by("index_number", "ASC");
        $data = $this->db->get("requests");
        $result = $data->result_array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
            }
        }
        return $result;
    }

    public function getalldraft_request_count() {
        $this->db->select("*");
        $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
        $this->db->where("status='draft'");
        $this->db->order_by("index_number", "ASC");
        $num_rows = $this->db->count_all_results('requests');
        return $num_rows;
    }

    public function getalldraft_request() {
        $this->db->select("*");
        $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
        $this->db->where("status='draft'");
        $this->db->order_by("index_number", "ASC");
        $this->db->limit(10, 0);
        $data = $this->db->get("requests");
        $result = $data->result_array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
            }
        }
        return $result;
    }

    public function getalldraft_request_pagination($start, $limit) {
        $this->db->select("*");
        $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
        $this->db->where('status', 'draft');
        $this->db->order_by("index_number", "ASC");
        $this->db->limit($limit, $start);
        $data = $this->db->get("requests");
        $result = $data->result_array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
            }
        }
        return $result;
    }

    public function getallcomplete_request_pagination($start, $limit) {
        $this->db->select("*");
        $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
        $this->db->where("status='approved'");
        $this->db->order_by("index_number", "ASC");
        $this->db->limit($limit, $start);
        $data = $this->db->get("requests");
        $result = $data->result_array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
            }
        }
        return $result;
    }

    public function getallcomplete_request_count() {
        $this->db->select("*");
        $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
        $this->db->where("status='approved'");
        $this->db->order_by("index_number", "ASC");
        $num_rows = $this->db->count_all_results('requests');
        return $num_rows;
    }

    public function getallcomplete_request() {
        $this->db->select("*");
        $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
        $this->db->where("status='approved'");
        $this->db->order_by("index_number", "ASC");
        $this->db->limit(7, 0);
        $data = $this->db->get("requests");
        $result = $data->result_array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
            }
        }
        return $result;
    }

    public function getall_request_designer($customer_id = null, $status = array(), $designer_id = "", $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where("requests.customer_id = '" . $customer_id . "'");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id = '" . $designer_id . "'");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }
        if (!empty($status)) {
            $this->db->where("status_designer IN (" . $status . ")");
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(created)", "ASC");
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function getall_request_count($customer_id = null, $status = array(), $designer_id = "", $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where("requests.customer_id = '" . $customer_id . "'");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id = '" . $designer_id . "'");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }

        /* if(count($status)>0){
          }else{ */
        $status = "'draft','approved'";
        //	}

        if (!empty($status)) {
            $this->db->where("status NOT IN (" . $status . ")");
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(created)", "ASC");
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $num_rows = $this->db->count_all_results('requests');
        return $num_rows;

        //echo $this->db->last_query();
    }

    public function getall_request($customer_id = null, $status = array(), $designer_id = "", $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where("requests.customer_id = '" . $customer_id . "'");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id = '" . $designer_id . "'");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }

        /* if(count($status)>0){
          }else{ */
        $status = "'draft','approved'";
        //	}
        if (!empty($status)) {
            $this->db->where("status NOT IN (" . $status . ")");
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(created)", "DESC");
        } else {
            $this->db->order_by($orderby, "DESC");
        }
        $this->db->limit(9, 0);
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function getall_request_pagination($customer_id = null, $status, $designer_id = "", $start, $limit, $orderby = "") {
        $this->db->select("*");
        $this->db->where_in('status', $status);
        if ($customer_id) {
            $this->db->where("requests.customer_id = '" . $customer_id . "'");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id = '" . $designer_id . "'");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(created)", "ASC");
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $this->db->limit($limit, $start);
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function getall_newrequest($customer_id, $status = array(), $designer_id = "", $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where("requests.customer_id = '" . $customer_id . "'");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id = '" . $designer_id . "'");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }

        if (!empty($status)) {
            $this->db->where_in("status", $status);
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(created)", "ASC");
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $this->db->limit(9, 0);
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function getuserbyid($id) {
        $this->db->select("*");
        $this->db->where("id = '" . $id . "'");
        $data = $this->db->get("users");
        $result = $data->result_array();
        return $result;
    }

    public function getbyid($table, $id) {
        $this->db->select("*");
        $this->db->where("id = '" . $id . "'");
        $data = $this->db->get($table);
        $result = $data->result_array();
        return $result;
    }

    public function getbypriority($priority) {
        $this->db->select("*");
        $this->db->where("priority = '" . $priority . "'");
        $data = $this->db->get("requests");
        $result = $data->result_array();
        return $result;
    }

    public function get_priority_by_id($id) {
        $this->db->select("priority");
        $this->db->where("id = '" . $id . "'");
        $data = $this->db->get("requests")->row('priority');
        return $data;
    }

    public function update_priority($priorityfrom, $priorityto, $id = 0) {
        if ($id == 0) {
            $this->db->set('priority', $priorityto)->where('priority', $priorityfrom)->update('requests');
        } else {
            $this->db->set('priority', $priorityto)->where('priority', $priorityfrom)->where('id', $id)->update('requests');
        }
        return true;
    }

    public function getuserbyemail($id) {
        $this->db->select("*");
        $this->db->where("email = '" . $id . "'");
        $data = $this->db->get("users");
        $result = $data->result_array();
        return $result;
    }

    public function get_request_by_data($url, $heading, $user_id, $shown) {
        $this->db->select("*");
        $this->db->where("url = '" . $url . "'");
        $this->db->where("heading = '" . $heading . "'");
        $this->db->where("user_id = '" . $user_id . "'");
        $this->db->where("shown = '" . $shown . "'");
        $data = $this->db->get("message_notification");
        $result = $data->result_array();
        return $result;
    }

    public function get_attachment_files_for_customer($request_id, $status, $role) {
        $this->db->select("*");
        $this->db->where("request_id = '" . $request_id . "'");
        $this->db->where_in('status', $status);
        $data = $this->db->get("request_files");
        $result = $data->result_array();
        if (empty($result)) {
            return array();
        }
        return $result;
    }

    public function get_new_attachment_files_for_customer($request_id, $status) {
        $this->db->select("*");
        $this->db->where("request_id = '" . $request_id . "'");
        $this->db->where_in('status', $status);
        $this->db->where('customer_seen', 0);
        $data = $this->db->get("request_files");
        $result = $data->result_array();
        if (empty($result)) {
            return array();
        }
        return $result;
    }

    public function get_request_by_id_admin($id) {
        $this->db->select("*");
        $this->db->where("id = '" . $id . "'");
        $data = $this->db->get("requests");
        $result = $data->result_array();
        // echo "<pre>";
        // print_r($result);
        // exit;
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            $qa = $this->getuserbyid($designer[$i]['qa_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
            }
            if (!empty($qa)) {
                $result[$i]['qa_first_name'] = $qa[0]['first_name'];
                $result[$i]['qa_profile_picture'] = $qa[0]['profile_picture'];
                $result[$i]['qa_last_name'] = $qa[0]['last_name'];
            } else {
                $result[$i]['qa_first_name'] = "";
                $result[$i]['qa_last_name'] = "";
                $result[$i]['qa_profile_picture'] = "";
            }
            $result[$i]['customer_attachment'] = $this->get_attachment_files($id, "customer");
            $result[$i]['designer_attachment'] = $this->get_attachment_files($id, "designer");
            $result[$i]['admin_attachment'] = $this->get_attachment_files($id, "admin");

            $style_suggestion = $this->get_style_suggestion($id);
            if (empty($style_suggestion)) {
                $result[$i]['additional_description'] = "";
                $result[$i]['color'] = "";
                $result[$i]['image'] = "";
                $result[$i]['selected_color'] = "";
            } else {
                $result[$i]['additional_description'] = $style_suggestion[0]['additional_description'];
                $result[$i]['color'] = $style_suggestion[0]['color'];
                $result[$i]['image'] = $style_suggestion[0]['image'];
                $result[$i]['selected_color'] = $style_suggestion[0]['selected_color'];
            }
        }

        return $result;
    }

    public function get_request_by_id($id) {
        $this->db->select("*");
        $this->db->where("id = '" . $id . "'");
        $data = $this->db->get("requests");
        $this->db->order_by("Date(created)","DESC");
        $result = $data->result_array();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
            }
            $result[$i]['customer_attachment'] = $this->get_attachment_files($id, "customer");
            $result[$i]['designer_attachment'] = $this->get_attachment_files($id, "designer");
            $result[$i]['admin_attachment'] = $this->get_attachment_files($id, "admin");

            $style_suggestion = $this->get_style_suggestion($id);
            if (empty($style_suggestion)) {
                $result[$i]['additional_description'] = "";
                $result[$i]['color'] = "";
                $result[$i]['image'] = "";
                $result[$i]['selected_color'] = "";
            } else {
                $result[$i]['additional_description'] = $style_suggestion[0]['additional_description'];
                $result[$i]['color'] = $style_suggestion[0]['color'];
                $result[$i]['image'] = $style_suggestion[0]['image'];
                $result[$i]['selected_color'] = $style_suggestion[0]['selected_color'];
            }
        }

        return $result;
    }

    public function get_attachment_files($request_id, $role) {
        $this->db->select("*");
        $this->db->where("request_id = '" . $request_id . "'");
        $this->db->where("user_type = '" . $role . "'");
        $this->db->order_by("created","DESC");
        $data = $this->db->get("request_files");
        //echo $this->db->last_query();
        $result = $data->result_array();
        if (empty($result)) {
            return array();
        }
        return $result;
    }

    public function get_average_rating($id) {
        $sql = "SELECT avg(customer_grade) as grade FROM `request_files` WHERE user_id='" . $id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function get_customer_average_rating($customer_id, $designer_id) {
        $sql = " SELECT GROUP_CONCAT(id) as ids FROM `requests` WHERE customer_id = '" . $customer_id . "' and designer_id = '" . $designer_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        if ($result[0]['ids']) {
            $sql = "SELECT avg(customer_grade) as grade FROM `request_files` WHERE user_id='" . $designer_id . "' and request_id IN(" . $result[0]['ids'] . ")";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['grade'];
        }
        return 0;
    }

    public function get_customer_average_rating_qa_dashboard($customer_id) {
        $sql = " SELECT GROUP_CONCAT(id) as ids FROM `requests` WHERE customer_id = '" . $customer_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        if ($result[0]['ids']) {
            $sql = "SELECT avg(customer_grade) as grade FROM `request_files` WHERE request_id IN(" . $result[0]['ids'] . ")";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['grade'];
        }
        return 0;
    }

    // Get Qa Customers
    public function get_customer_list_for_qa_section($id) {
        $this->db->select('id');
        $this->db->where('qa_id', $id);
        $this->db->where('role', 'customer');
        $query = $this->db->get('users');
        $result = $query->result_array();
        return $result; 
    }

    public function get_customer_list_for_qa($type = "", $list) {
        if ($type == "array") {
            $this->db->select('*');
            $this->db->where('qa_id', $_SESSION['user_id']);
            $this->db->where('role', 'customer');
            $this->db->limit(4, 0);
            $query = $this->db->get('users');
            $result = $query->result_array();
            return $result;
        } else {
            $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE designer_id IN(" . $list . ")";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['ids'];
        }
    }

    public function get_customer_list_for_qa_scroll($type = "", $list, $start, $limit) {
        if ($type == "array") {
            $this->db->select('*');
            $this->db->where('qa_id', $_SESSION['user_id']);
            $this->db->where('role', 'customer');
            $this->db->limit($limit, $start);
            $query = $this->db->get('users');
            $result = $query->result_array();
            return $result;
        } else {
            $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE designer_id IN(" . $list . ")";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['ids'];
        }
    }

    public function get_customer_list_for_qa_count($type = "", $list) {
        if ($type == "array") {
            $this->db->select('*');
            $this->db->where('qa_id', $_SESSION['user_id']);
            $this->db->where('role', 'customer');
            $num_rows = $this->db->count_all_results('users');
            return $num_rows;
            // $query = $this->db->get('users');
            // $result = $query->result_array();
            // return $result;
        } else {
            $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE designer_id IN(" . $list . ")";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['ids'];
        }
    }

    // public function get_designer_list_for_qa($type="")
    // {
    // 	if($type == "array"){
    // 		$sql = " SELECT * from `users` WHERE qa_id = '".$_SESSION['user_id']."'";
    // 		$query = $this->db->query($sql);
    // 		$result = $query->result_array();
    // 		return $result;
    // 	}else{
    // 		$sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '".$_SESSION['user_id']."'";
    // 		$query = $this->db->query($sql);
    // 		$result = $query->result_array();
    // 		return $result[0]['ids'];
    // 	}
    // }

    public function get_designer_list_for_qa($type = "", $id = "") {
        if ($type == "array") {
            $this->db->select('*');
            $this->db->where('qa_id', $_SESSION['user_id']);
            $this->db->where('role', 'designer');
            $this->db->limit(5, 0);
            $query = $this->db->get('users');
            $result = $query->result_array();
            return $result;
        } else {
            if ($id) {
                $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '" . $id . "'";
            } else {
                $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '" . $_SESSION['user_id'] . "'";
            }
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['ids'];
        }
    }

    public function get_designer_list_for_qa_scroll($type = "", $start, $limit) {
        if ($type == "array") {
            $this->db->select('*');
            $this->db->where('qa_id', $_SESSION['user_id']);
            $this->db->where('role', 'designer');
            $this->db->limit($limit, $start);
            $query = $this->db->get('users');
            $result = $query->result_array();
            return $result;
        } else {
            if ($id) {
                $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '" . $id . "'";
            } else {
                $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '" . $_SESSION['user_id'] . "'";
            }
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['ids'];
        }
    }

    public function get_designer_list_for_qa_count($type = "", $id = "") {
        if ($type == "array") {
            $this->db->select('*');
            $this->db->where('qa_id', $_SESSION['user_id']);
            $this->db->where('role', 'designer');
            $num_rows = $this->db->count_all_results('users');
            return $num_rows;
        } else {
            if ($id) {
                $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '" . $id . "'";
            } else {
                $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '" . $_SESSION['user_id'] . "'";
            }
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['ids'];
        }
    }

    // Get Request For Qa 
    public function get_request_list_for_qa_section($customer_id = null, $status = array(), $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where_in("customer_id", $customer_id);
        }
        if (!empty($status)) {
            $this->db->where_in('status_qa', $status);
        }
        if ($orderby == "") {
            if ($status[0] == "approved") {
                $this->db->order_by("approvaldate", "DESC");
            } elseif ($status[0] == "pendingrevision") {
                $this->db->order_by("modified", "DESC");
            } elseif ($status[0] == "checkforapprove") {
                $this->db->order_by("modified", "DESC");
            } else {
                $this->db->order_by("latest_update", "ASC");
            }
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $this->db->limit(6, 0);
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                ;
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function get_request_list_for_qa_section_scroll($customer_id = null, $status = array(), $start, $limit, $orderby = "") {
        $result = array();
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where_in("customer_id", $customer_id);
            if (!empty($status)) {
                $this->db->where_in('status_qa', $status);
                if ($orderby == "") {
                    if ($status[0] == "approved") {
                        $this->db->order_by("approvaldate", "DESC");
                    } elseif ($status[0] == "pendingrevision") {
                        $this->db->order_by("modified", "ASC");
                    } elseif ($status[0] == "checkforapprove") {
                        $this->db->order_by("modified", "ASC");
                    }elseif($status[0] == "assign"){
                        $this->db->order_by("priority", "ASC");
                    }else {
                        $this->db->order_by("latest_update", "ASC");
                    }
                    $this->db->limit($limit, $start);
                    $data = $this->db->get("requests");
                    $result = $data->result_array();
                    // echo $this->db->last_query();
                    for ($i = 0; $i < sizeof($result); $i++) {
                        $designer = $this->getuserbyid($result[$i]['designer_id']);
                        $customer = $this->getuserbyid($result[$i]['customer_id']);
                        if (!empty($designer)) {
                            $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                            $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                            $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
                        } else {
                            $result[$i]['designer_first_name'] = "";
                            $result[$i]['designer_last_name'] = "";
                            $result[$i]['profile_picture'] = "";
                        }
                        if (!empty($customer)) {
                            $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                            $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                            $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                            ;
                            $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
                        } else {
                            $result[$i]['customer_first_name'] = "";
                            $result[$i]['customer_last_name'] = "";
                            $result[$i]['customer_profile_picture'] = "";
                            $result[$i]['plan_turn_around_days'] = 0;
                        }
                    }
                }
            }
        } else {
            echo '<div class="nodata">
                                <h3 class="head-b draft_no">They are no projects at this time</h3>
                            </div>';
            //$this->db->order_by($orderby,"ASC");
        }
        return $result;
    }

    public function get_request_list_for_qa_section_count($customer_id = null, $status = array(), $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where_in("customer_id", $customer_id);
        }
        if (!empty($status)) {
            $this->db->where_in('status_qa', $status);
        }
        if ($orderby == "") {
            if ($status[0] == "approved") {
                $this->db->order_by("approvaldate", "DESC");
            } elseif ($status[0] == "pendingrevision") {
                $this->db->order_by("modified", "DESC");
            } elseif ($status[0] == "checkforapprove") {
                $this->db->order_by("modified", "DESC");
            } else {
                $this->db->order_by("latest_update", "ASC");
            }
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $num_rows = $this->db->count_all_results('requests');
        return $num_rows;
    }

    public function get_request_list_for_qa($customer_id = null, $status = array(), $designer_id = "", $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where("requests.customer_id IN (" . $customer_id . ")");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id IN (" . $designer_id . ")");
        } else {
            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }
        if (!empty($status)) {
            $this->db->where_in('status_qa', $status);
        }
        if ($orderby == "") {
            if ($status[0] == "approved") {
                $this->db->order_by("approvaldate", "DESC");
            } elseif ($status[0] == "pendingrevision") {
                $this->db->order_by("modified", "DESC");
            } elseif ($status[0] == "checkforapprove") {
                $this->db->order_by("modified", "DESC");
            } else {
                $this->db->order_by("latest_update", "ASC");
            }
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                ;
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function get_request_list_for_admin($customer_id = null, $status = array(), $designer_id = "", $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where("requests.customer_id IN (" . $customer_id . ")");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id IN (" . $designer_id . ")");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }
        if (!empty($status)) {
            $this->db->where_in('status', $status);
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(modified)", "DESC");
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                ;
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function get_unassign_request_fist_for_qa($customer_id = null, $status = array(), $designer_id = "", $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where("requests.customer_id IN (" . $customer_id . ")");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id IN (" . $designer_id . ")");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }
        if (!empty($status)) {
            $this->db->where("status IN (" . $status . ")");
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(created)", "ASC");
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                ;
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function get_average_rating_for_request($request_id, $designer_id) {
        $sql = "SELECT avg(customer_grade) as grade FROM `request_files` WHERE user_id='" . $designer_id . "' and request_id='" . $request_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function get_files_count_all($request_id) {
        $this->db->select('*');
        $this->db->where('request_id', $request_id);
        $this->db->where('src_file !=', 'null');
        $query = $this->db->get('request_files');
        $result = $query->result_array();
        return $result;
    }

    public function get_files_count($request_id, $designer_id, $seen) {
        //$sql = "SELECT count(*) as fileCount FROM `request_files` WHERE request_id='".$request_id."' and src_file != 'null'";
        $this->db->select('*');
        $this->db->where('request_id', $request_id);
        $this->db->where('src_file !=', 'null');
        if ($seen == 'qa') {
            $this->db->where('qa_seen', 0);
        } elseif ($seen == 'admin') {
            $this->db->where('admin_seen', 0);
        } elseif ($seen == 'customer') {
            $this->db->where('customer_seen', 0);
        } elseif ($seen == 'designer') {
            $this->db->where('designer_seen', 0);
        }
        $query = $this->db->get('request_files');
        $result = $query->result_array();
        return $result;
    }

    public function get_style_suggestion($request_id) {
        $this->db->select("*");
        $this->db->where("request_id = '" . $request_id . "'");
        $data = $this->db->get("style_suggestion");
        $result = $data->result_array();
        if (empty($result)) {
            return array();
        }
        return $result;
    }

    public function new_designer_get_status($designer_id, $customer_id) {
        $this->db->select("*");
        $this->db->where("status='active'");
        $this->db->where("designer_id='" . $designer_id . "'");
        $this->db->where("customer_id='" . $customer_id . "'");
        $data = $this->db->get("requests");
        $result = $data->result_array();
        if (sizeof($result) == 1) {
            return "assign";
        } else {
            return "active";
        }
    }

    public function get_chat_by_id($id, $order = null) {
        $this->db->select("*");
        $this->db->where("request_file_id='" . $id . "'");
        $this->db->order_by("id", $order);
        $data = $this->db->get("request_file_chat");
        $result = $data->result_array();
        return $result;
    }

    public function get_chat_request_by_id($id) {
        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
        $this->db->from('request_discussions as a');
        $this->db->where("request_id='" . $id . "'");
        $this->db->where("with_or_not_customer", 1);
        $this->db->join('users as b', 'b.id = a.sender_id');
        $this->db->order_by("id", "ASC");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }

    public function get_chat_request_by_id_without_customer($id) {
        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
        $this->db->from('request_discussions as a');
        $this->db->where("request_id='" . $id . "'");
        $this->db->where("with_or_not_customer", 0);
        $this->db->join('users as b', 'b.id = a.sender_id');
        $this->db->order_by("id", "ASC");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }

    public function get_notifications($user_id) {
        $this->db->select("n.*,u.first_name,u.last_name,u.profile_picture");
        $this->db->from('notification as n');
        $this->db->where("user_id='" . $user_id . "'");
        $this->db->where("shown='0'");
        $this->db->join('users as u', 'u.id = n.sender_id');
        $this->db->order_by("n.created", "DESC");
        $data = $this->db->get();
        //echo $this->db->last_query();exit; 
        $result = $data->result_array();
        return $result;
    }

    public function get_messagenotifications($user_id) {
        $this->db->select("*");
        $this->db->where("user_id='" . $user_id . "'");
        $this->db->where("shown='0'");
        $this->db->order_by("id", "DESC");
        $data = $this->db->get("message_notification");
        $result = $data->result_array();
        return $result;
    }

    // public function get_notifications($user_id)
    // {
    // 	$this->db->select("*");
    // 	$this->db->where("user_id='".$user_id."'");
    // 	$this->db->where("shown='0'");
    // 	$this->db->order_by("shown","ASC");
    // 	$data = $this->db->get("notification"); 
    // 	$result = $data->result_array();
    // 	return $result;
    // }
    public function get_notifications_number($user_id) {
        $this->db->select("*");
        $this->db->where("user_id='" . $user_id . "'");
        $this->db->where("shown='0'");
        $this->db->order_by("shown", "ASC");
        $data = $this->db->get("notification");
        $result = $data->result_array();
        return $result;
    }

    //  public function get_messagenotifications($user_id)
    // {
    // 	$this->db->select("*");
    // 	$this->db->where("user_id='".$user_id."'");
    // 	$this->db->where("shown='0'");
    // 	$this->db->order_by("id","ASC");
    // 	$data = $this->db->get("message_notification"); 
    // 	$result = $data->result_array();
    // 	return $result;
    // }
    public function get_messagenotifications_number($user_id) {
        $this->db->select("*");
        $this->db->where("user_id='" . $user_id . "'");
        $this->db->where("shown='0'");
        $this->db->order_by("shown", "ASC");
        $data = $this->db->get("message_notification");
        $result = $data->result_array();
        return $result;
    }

    public function get_total_chat_number_customer($request_id, $user_id, $seen, $with) {
        if ($seen == "customer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and with_or_not_customer='" . $with . "'";
        }
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return sizeof($result);
        die();
    }

    public function get_chat_number_customer($request_id, $user_id, $seen, $with) {
        if ($seen == "customer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and customer_seen='0' and with_or_not_customer='" . $with . "'";
        }

        $query = $this->db->query($sql);
        $result = $query->result_array();
        //echo "<pre>"; print_r(sizeof($result)); echo "</pre>";
        return sizeof($result);
        die();
    }

    public function get_total_chat_number($request_id, $user_id, $seen) {
        if ($seen == "designer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "'";
        }
        if ($seen == "qa") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "'";
        }
        if ($seen == "admin") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "'";
        }
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return sizeof($result);
        die();
    }

    public function get_chat_number($request_id, $receiver_id, $sender_id, $seen) {
        if ($seen == "customer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and customer_seen='0'";
        }
        if ($seen == "designer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and designer_seen='0'";
        }
        if ($seen == "admin") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and admin_seen='0'";
        }
        if ($seen == "qa") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and qa_seen='0'";
        }
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //echo "<pre>"; print_r(sizeof($result)); echo "</pre>";
        return sizeof($result);
        die();
    }

    public function get_customer_chat_number($request_id, $receiver_id, $sender_id, $seen) {
        if ($seen == "customer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and customer_seen='0' and with_or_not_customer'1'";
        }
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //echo "<pre>"; print_r(sizeof($result)); echo "</pre>";
        return sizeof($result);
        die();
    }

    public function get_comment_number($request_id, $receiver_id, $sender_id, $seen) {
        if ($seen == "customer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and customer_seen='0'";
        }
        if ($seen == "designer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and designer_seen='0'";
        }
        if ($seen == "admin") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and admin_seen='0'";
        }
        if ($seen == "qa") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and qa_seen='0'";
        }
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //echo "<pre>"; print_r(sizeof($result)); echo "</pre>";
        return sizeof($result);
        die();
    }

    // public function get_chat_number($request_id,$receiver_id,$sender_id,$seen){
    // 	if($seen == "customer"){
    // 		//echo "inif";
    // 		$sql = "select * from request_discussions where request_id='".$request_id."' and reciever_id='".$receiver_id."' and sender_id='".$sender_id."' and customer_seen='0'";
    // 	}
    // 	if($seen == "designer"){
    // 		$sql = "select * from request_discussions where request_id='".$request_id."' and reciever_id='".$receiver_id."' and sender_id='".$sender_id."' and designer_seen='0'";
    // 	}
    // 	if($seen == "admin"){
    // 		$sql = "select * from request_discussions where request_id='".$request_id."' and reciever_id='".$receiver_id."' and sender_id='".$sender_id."' and admin_seen='0'";
    // 	}
    // 	$query = $this->db->query($sql);
    // 	$result = $query->result_array();
    // 	 //echo "<pre>"; print_r(sizeof($result)); echo "</pre>";
    // 	return sizeof($result);
    //  		die();
    // }

    public function get_file_chat($request_file_id, $role) {
        if ($role == "designer") {
            $sql = "select * from request_file_chat where designer_seen='0' and request_file_id='" . $request_file_id . "'";
        } elseif ($role == "customer") {
            $sql = "select * from request_file_chat where customer_seen='0' and request_file_id='" . $request_file_id . "'";
        } elseif ($role == "qa") {
            $sql = "select * from request_file_chat where qa_seen='0' and request_file_id='" . $request_file_id . "'";
        } elseif ($role == "admin") {
            $sql = "select * from request_file_chat where admin_seen='0' and request_file_id='" . $request_file_id . "'";
        }
        $query = $this->db->query($sql);
        return $result = $query->num_rows();
    }

    // public function get_file_chat_number($request_file_id,$role){
    // 	if($role=="designer"){
    // 		$sql="select * from request_file_chat where designer_seen='0' and request_file_id='".$request_file_id."'";
    // 	}elseif($role=="customer"){
    // 		$sql="select * from request_file_chat where customer_seen='0' and request_file_id='".$request_file_id."'";
    // 		echo $sql;
    // 	}
    // 	// $query = $this->db->query($sql);
    // 	// $result = $query->result_array();
    // 	// //if(sizeof($result)){ echo $this->db->last_query(); }
    // 	// //return sizeof($result);
    // 	//   echo "<pre>"; print_r($result); echo "</pre>";
    //            // die();
    // 	die();
    // }

    public function get_customer_unseen_msg($id, $customer_id, $designer_id, $msg_seen) {
        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
        $this->db->from('request_discussions as a');
        $this->db->where("request_id='" . $id . "' and customer_seen='" . $msg_seen . "' and with_or_not_customer='1'");
        $this->db->join('users as b', 'b.id = a.sender_id');
        $this->db->order_by("id", "ASC");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;

        // $this->db->select("*");
        // $this->db->where("request_id='".$id."' and customer_seen='".$msg_seen."' and with_or_not_customer='1'");
        // $data = $this->db->get("request_discussions");
        // $result = $data->result_array();
        // return $result;
    }

    public function get_admin_unseen_msg($id, $customer_id, $designer_id, $msg_seen) {
        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
        $this->db->from('request_discussions as a');
        $this->db->where("request_id='" . $id . "' and admin_seen='" . $msg_seen . "'");
        $this->db->join('users as b', 'b.id = a.sender_id');
        $this->db->order_by("id", "ASC");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;



        // $this->db->select("*");
        // $this->db->where("request_id='".$id."' and qa_seen='".$msg_seen."'");
        // $this->db->join('users','users.id = notification.sender_id');
        // $data = $this->db->get("request_discussions");
        // $result = $data->result_array();
        // return $result;
    }

    public function get_qa_unseen_msg($id, $customer_id, $designer_id, $msg_seen) {
        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
        $this->db->from('request_discussions as a');
        $this->db->where("request_id='" . $id . "' and qa_seen='" . $msg_seen . "'");
        $this->db->join('users as b', 'b.id = a.sender_id');
        $this->db->order_by("id", "ASC");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;



        // $this->db->select("*");
        // $this->db->where("request_id='".$id."' and qa_seen='".$msg_seen."'");
        // $this->db->join('users','users.id = notification.sender_id');
        // $data = $this->db->get("request_discussions");
        // $result = $data->result_array();
        // return $result;
    }

    public function get_designer_unseen_msg($id, $customer_id, $designer_id, $msg_seen) {
        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
        $this->db->from('request_discussions as a');
        $this->db->where("request_id='" . $id . "' and designer_seen='" . $msg_seen . "'");
        $this->db->join('users as b', 'b.id = a.sender_id');
        $this->db->order_by("id", "ASC");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }

    public function get_all_designer_for_qa($id) {
        $this->db->select("*");
        $this->db->where('role', 'designer');
        $this->db->where('qa_id', $id);
        $data = $this->db->get('users');
        $result = $data->result_array();
        return $result;
    }

    public function get_all_clients_for_qa($name = array(), $id = null) {
        $sql = "SELECT * FROM `users` WHERE (`first_name` LIKE '%" . $name['keyword'] . "%' OR `last_name` LIKE '%" . $name['keyword'] . "%' OR `email` LIKE '%" . $name['keyword'] . "%') AND `role` = 'customer' AND `qa_id` = " . $_SESSION['user_id'];
        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function get_all_clients_for_admin($name = array()) {
        $sql = "SELECT * FROM `users` WHERE (`first_name` LIKE '%" . $name['keyword'] . "%' OR `last_name` LIKE '%" . $name['keyword'] . "%' OR `email` LIKE '%" . $name['keyword'] . "%') AND `role` = 'customer'";
        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function get_all_designer_for_admin($name = array()) {
        $sql = "SELECT * FROM `users` WHERE (`first_name` LIKE '%" . $name['keyword'] . "%' OR `last_name` LIKE '%" . $name['keyword'] . "%' OR `email` LIKE '%" . $name['keyword'] . "%') AND `role` = 'designer'";
        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function get_qa_member($name = array()) {
        $sql = "SELECT * FROM `users` WHERE (`first_name` LIKE '%" . $name['keyword'] . "%' OR `last_name` LIKE '%" . $name['keyword'] . "%' OR `email` LIKE '%" . $name['keyword'] . "%') AND `role` = 'qa'";
        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function get_va_member($name = array()) {
        $sql = "SELECT * FROM `users` WHERE (`first_name` LIKE '%" . $name['keyword'] . "%' OR `last_name` LIKE '%" . $name['keyword'] . "%') AND `role` = 'va'";
        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function get_blog_ajax($title = array()) {
        $this->db->select('*');
        $this->db->like('title', $title['keyword'], 'both');
        $query = $this->db->get('blog');
        $result = $query->result_array();
        return $result;
    }

    public function get_portfolio_ajax($title = array()) {
        $this->db->select('*');
        $this->db->like('title', $title['keyword'], 'both');
        $query = $this->db->get('protfolio');
        $result = $query->result_array();
        return $result;
    }

    public function get_all_designer_for_qa_search_ajax($name = array(), $id = null) {
        //$this->db->select("*");
        $sql = "SELECT * FROM `users` WHERE (`first_name` LIKE '%" . $name['keyword'] . "%' OR `last_name` LIKE '%" . $name['keyword'] . "%' OR `email` LIKE '%" . $name['keyword'] . "%') AND `role` = 'designer' AND `qa_id` = " . $_SESSION['user_id'];
        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function logout() {
        $this->session->sess_destroy();
    }

    public function GetAutocomplete($options = array(), $status = array(), $customer_id, $datarole) {
        $result = array();
        // echo "<pre>";print_r($options);exit;
        $this->db->select('a.*');
        $this->db->from('requests as a');
        $this->db->where_in('a.status_qa', $status);
        if ($options['keyword'] != "") {
            $this->db->join('users as b', 'b.id = a.customer_id OR b.id = a.designer_id');
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            $this->db->group_by('a.id'); 
            $this->db->group_end();            
        }
        if ($datarole) {
            $this->db->where_in('a.status', $status);
        }
        if ($customer_id) {
            $this->db->where_in('a.customer_id', $customer_id);
            if ($datarole) {
                $this->db->where_in('a.status', $status);
            }
            if ($status[0] == "approved") {
                $this->db->order_by("a.approvaldate", "DESC");
            } elseif ($status[0] == "pendingrevision") {
                $this->db->order_by("a.modified", "ASC");
            } elseif ($status[0] == "checkforapprove") {
                $this->db->order_by("a.modified", "ASC");
            }elseif($status[0] == "assign"){
                 $this->db->order_by("a.priority", "ASC");
            } else {
                $this->db->order_by("a.latest_update", "ASC");
            }
            $this->db->limit(10, 0);
            $query = $this->db->get(); 
            $result = $query->result();
           // echo $this->db->last_query();
            for ($i = 0; $i < sizeof($result); $i++) {
                $designer = $this->getuserbyid($result[$i]->designer_id);
                $customer = $this->getuserbyid($result[$i]->customer_id);
                if (!empty($designer)) {
                    $result[$i]->designer_first_name = $designer[0]['first_name'];
                    $result[$i]->designer_last_name = $designer[0]['last_name'];
                    $result[$i]->profile_picture = $designer[0]['profile_picture'];
                } else {
                    $result[$i]->designer_first_name = "";
                    $result[$i]->designer_last_name = "";
                    $result[$i]->profile_picture = "";
                }
                if (!empty($customer)) {
                    $result[$i]->customer_first_name = $customer[0]['first_name'];
                    $result[$i]->customer_last_name = $customer[0]['last_name'];
                    $result[$i]->customer_profile_picture = $customer[0]['profile_picture'];
                    $result[$i]->plan_turn_around_days = $customer[0]['plan_turn_around_days'];
                } else {
                    $result[$i]->customer_first_name = "";
                    $result[$i]->customer_last_name = "";
                    $result[$i]->customer_profile_picture = "";
                    $result[$i]->plan_turn_around_days = 0;
                }
            }
        } else {
            echo '<div class="nodata">
                                <h3 class="head-b draft_no">They are no projects at this time</h3>
                            </div>';
        }

        return $result;
    }

    public function GetAutocomplete_qa_customer($options = array(), $status = array(), $customer_id, $datarole) {
        $this->db->select('a.*');
        $this->db->from('requests as a');
        $this->db->join('users as b', 'b.id = a.designer_id');
        $this->db->where_in('a.status', $status);
        if ($options['keyword'] != "") {
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            //$this->db->or_like('b.email', $options['keyword'], 'both');
            $this->db->group_end();
        }
        if ($customer_id) {
            $this->db->where('a.customer_id', $customer_id);
        }
        if ($datarole) {
            $this->db->where_in('a.status', $status);
        }
        $orderDate = '';
        if ($status[0] == "approved") {
            $orderDate = 'approvaldate';
        }
        if ($status[0] == "active") {
            if ($status[1] == "disapprove") {
                $orderDate = 'latest_update';
            }
            $orderDate = 'latest_update';
            if ($status[2] == "assign") {
                $orderDate .= ',modified';
            }
        }
        if ($status[0] == "checkforapprove") {
            $orderDate = 'latest_update';
        }
        //echo $orderDate;
        // $sql = "SELECT * from requests ORDER BY GREATEST(".$orderDate.")";
        // $data = $this->db->query($sql);
        // $result = $data->result_array();


        $query = $this->db->get();
        $result = $query->result();


        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]->designer_id);
            $customer = $this->getuserbyid($result[$i]->customer_id);
            if (!empty($designer)) {
                $result[$i]->designer_first_name = $designer[0]['first_name'];
                $result[$i]->designer_last_name = $designer[0]['last_name'];
                $result[$i]->profile_picture = $designer[0]['profile_picture'];
            } else {
                $result[$i]->designer_first_name = "";
                $result[$i]->designer_last_name = "";
                $result[$i]->profile_picture = "";
            }
            if (!empty($customer)) {
                $result[$i]->customer_first_name = $customer[0]['first_name'];
                $result[$i]->customer_last_name = $customer[0]['last_name'];
                $result[$i]->customer_profile_picture = $customer[0]['profile_picture'];
                ;
                $result[$i]->plan_turn_around_days = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]->customer_first_name = "";
                $result[$i]->customer_last_name = "";
                $result[$i]->customer_profile_picture = "";
                $result[$i]->plan_turn_around_days = 0;
            }
        }
        return $result;
    }

    public function GetAutocomplete_for_admin($options = array(), $status = array(), $customer_id, $datarole) {
        $this->db->select('a.*');
        $this->db->from('requests as a');
        $this->db->join('users as b', 'b.id = a.designer_id');
        if ($datarole) {
            $this->db->where_in('a.status', $status);
        } else {
            $this->db->where_in('a.status_admin', $status);
        }
        if ($options['keyword'] != "") {
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            //$this->db->or_like('b.email', $options['keyword'], 'both');
            $this->db->group_end();
        }
        if ($customer_id) {
            $this->db->where('a.customer_id', $customer_id);
        }
        if ($status[0] == "approved") {
            $this->db->order_by("a.approvaldate", "DESC");
        } elseif ($status[0] == "active") {
            $this->db->order_by("a.modified", "DESC");
        } elseif ($status[0] == "checkforapprove") {
            $this->db->order_by("a.modified", "DESC");
        } else {
            $this->db->order_by("a.latest_update", "ASC");
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]->designer_id);
            $customer = $this->getuserbyid($result[$i]->customer_id);
            if (!empty($designer)) {
                $result[$i]->designer_first_name = $designer[0]['first_name'];
                $result[$i]->designer_last_name = $designer[0]['last_name'];
                $result[$i]->profile_picture = $designer[0]['profile_picture'];
            } else {
                $result[$i]->designer_first_name = "";
                $result[$i]->designer_last_name = "";
                $result[$i]->profile_picture = "";
            }
            if (!empty($customer)) {
                $result[$i]->customer_first_name = $customer[0]['first_name'];
                $result[$i]->customer_last_name = $customer[0]['last_name'];
                $result[$i]->customer_profile_picture = $customer[0]['profile_picture'];
                ;
                $result[$i]->plan_turn_around_days = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]->customer_first_name = "";
                $result[$i]->customer_last_name = "";
                $result[$i]->customer_profile_picture = "";
                $result[$i]->plan_turn_around_days = 0;
            }
        }
        return $result;
    }

    public function GetAutocomplete_admin($options = array(), $status = array(), $customer_id) {
        $this->db->select('a.*');
        $this->db->from('requests as a');
        $this->db->join('users as b', 'b.id = a.customer_id OR b.id = a.designer_id');
        $this->db->where_in('a.status_admin', $status);
        if ($options['keyword'] != "") {
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            //$this->db->or_like('b.email', $options['keyword'], 'both');
            $this->db->group_end();
        }
        if ($customer_id) {
            $this->db->where('a.customer_id', $customer_id);
        }
        $this->db->group_by('a.id');
        if ($status[0] == "approved") {
            $this->db->order_by("DATE(a.approvaldate)", "DESC");
        } elseif ($status[0] == "assign") {
            $this->db->order_by("priority","ASC");
        } elseif ($status[0] == "pendingrevision") {
            $this->db->order_by("DATE(a.modified)", "ASC");
        } elseif($status[0] == 'checkforapprove'){
            $this->db->order_by("DATE(a.modified)", "ASC");
        }else {
            $this->db->order_by("DATE(a.latest_update)", "ASC");
        }
        $this->db->limit(10, 0);
        $query = $this->db->get();
        $result = $query->result();





        /* $this->db->select('a.*');
          $this->db->from('requests as a');
          $this->db->join('users as b','b.id = a.customer_id OR b.id = a.designer_id');
          $this->db->where_in('a.status',$status);
          $this->db->like('a.title', $options['keyword'], 'both');
          $this->db->or_like('b.first_name', $options['keyword'], 'both');
          $this->db->or_like('b.last_name', $options['keyword'], 'both');
          $this->db->or_like('b.email', $options['keyword'], 'both');
          // $this->db->join('users as b','b.id = a.designer_id');
          if($customer_id){
          $this->db->where('a.customer_id',$customer_id);
          }
          if($status[0] == "approved"){
          $this->db->order_by("DATE(a.approvaldate)","DESC");
          }elseif($status[0] == "assign"){
          //$this->db->order_by("priority","ASC");
          }elseif($status[0] == "pendingrevision"){
          $this->db->order_by("DATE(a.modified)","DESC");
          }else{
          $this->db->order_by("DATE(a.latest_update)","DESC");
          }
          $this->db->limit(10, 0);
          $query = $this->db->get();
          $result = $query->result(); */
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]->designer_id);
            $customer = $this->getuserbyid($result[$i]->customer_id);
            if (!empty($designer)) {
                $result[$i]->designer_first_name = $designer[0]['first_name'];
                $result[$i]->designer_last_name = $designer[0]['last_name'];
                $result[$i]->profile_picture = $designer[0]['profile_picture'];
            } else {
                $result[$i]->designer_first_name = "";
                $result[$i]->designer_last_name = "";
                $result[$i]->profile_picture = "";
            }
            if (!empty($customer)) {
                $result[$i]->customer_first_name = $customer[0]['first_name'];
                $result[$i]->customer_last_name = $customer[0]['last_name'];
                $result[$i]->customer_profile_picture = $customer[0]['profile_picture'];
                ;
                $result[$i]->plan_turn_around_days = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]->customer_first_name = "";
                $result[$i]->customer_last_name = "";
                $result[$i]->customer_profile_picture = "";
                $result[$i]->plan_turn_around_days = 0;
            }
        }
        return $result;
    }

    public function GetAutocomplete_designer($options = array(), $status = array(), $customer_id, $datarol) {
        $this->db->select('a.*');
        $this->db->from('requests as a');
        $this->db->join('users as b', 'b.id = a.customer_id');
        if ($datarol) {
            $this->db->where_in('a.status_designer', $status);
        } else {
            $this->db->where_in('a.status', $status);
        }
        if ($options['keyword'] != "") {
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            //$this->db->or_like('b.email', $options['keyword'], 'both');
            $this->db->group_end();
        }

        // $this->db->select('*');
        //    	$this->db->like('title', $options['keyword'], 'both');
        //    	if($datarol){
        //    		$this->db->where_in('status_designer',$status);
        //    	}else{
        //     	$this->db->where_in('status',$status);
        //     }
        if ($customer_id) {
            $this->db->where('a.designer_id', $customer_id);
        }
        if ($status[0] == "approved") {
            $this->db->order_by("DATE(a.approvaldate)", "DESC");
        } elseif ($status[0] == "assign") {
            $this->db->order_by("a.priority", "ASC");
        } elseif ($status[0] == "pendingrevision") {
            $this->db->order_by("a.modified", "DESC");
        } else {
            $this->db->order_by("DATE(a.latest_update)", "ASC");
        }
        //$this->db->order_by("modified","DESC");
        $query = $this->db->get();
        $result = $query->result();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]->designer_id);
            $customer = $this->getuserbyid($result[$i]->customer_id);
            if (!empty($designer)) {
                $result[$i]->designer_first_name = $designer[0]['first_name'];
                $result[$i]->designer_last_name = $designer[0]['last_name'];
                $result[$i]->profile_picture = $designer[0]['profile_picture'];
            } else {
                $result[$i]->designer_first_name = "";
                $result[$i]->designer_last_name = "";
                $result[$i]->profile_picture = "";
            }
            if (!empty($customer)) {
                $result[$i]->customer_first_name = $customer[0]['first_name'];
                $result[$i]->customer_last_name = $customer[0]['last_name'];
                $result[$i]->customer_profile_picture = $customer[0]['profile_picture'];
                ;
                $result[$i]->plan_turn_around_days = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]->customer_first_name = "";
                $result[$i]->customer_last_name = "";
                $result[$i]->customer_profile_picture = "";
                $result[$i]->plan_turn_around_days = 0;
            }
        }
        return $result;
    }

// Ajax Request for active project search

    public function ajax_getall_request($options = array(), $status = array(), $orderby = "") {
        $this->db->select('a.*');
        $this->db->from('requests as a');
        $this->db->join('users as b', 'b.id = a.designer_id');
        $this->db->where_in('a.status', $status);
        if ($options['keyword'] != "") {
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            //$this->db->or_like('b.email', $options['keyword'], 'both');
            $this->db->group_end();
        }
        $this->db->where('a.customer_id', $_SESSION['user_id']);
        if ($orderby == "") {
            if(in_array('active','disapprove','assign','pending','checkforapprove',$status)){
                 $this->db->order_by("DATE(a.created)", "DESC");
            }else{
                $this->db->order_by("DATE(a.created)", "DESC");
            }
        }else {
            $this->db->order_by($orderby, "ASC");
        }
        $this->db->limit(9, 0);
        $data = $this->db->get();
        $result = $data->result_array();
        // echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

// Ajax Request for active project search End
// Ajax Request for Designer search
    public function getall_request_designer_ajax_search($options = array(), $status = array(), $orderby = "") {
        $this->db->select('a.*');
        $this->db->from('requests as a');
        $this->db->join('users as b', 'b.id = a.customer_id');
        $this->db->where_in('a.status_designer', $status);
        if ($options['keyword'] != "") {
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            //$this->db->or_like('b.email', $options['keyword'], 'both');
            $this->db->group_end();
        }
        $this->db->where('a.designer_id', $_SESSION['user_id']);
        if ($orderby == "") {
            if($status[0] == 'active'){
              $this->db->order_by("DATE(a.created)", "DESC");
            }elseif($status[0] =='disapprove'){
            $this->db->order_by("DATE(a.created)", "DESC");
            }elseif($status[0] =='pendingrevision'){
             $this->db->order_by("DATE(a.created)", "ASC");   
            }elseif($status[0] =='checkforapprove'){
             $this->db->order_by("DATE(a.created)", "ASC");   
            }else {
                $this->db->order_by($orderby, "DESC");
            }
        }
        $data = $this->db->get();
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

// Ajax Request for Designer search End

    public function getuserbyrole($role) {
        $this->db->select('id');
        $this->db->where('role', $role);
        $this->db->order_by('id', "ASC");
        $data = $this->db->get('users');
        $result = $data->result_array();
        return $result;
    }

    public function get_blog_by_id($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $data = $this->db->get('blog');
        $result = $data->result_array();
        return $result;
    }

    public function get_portfolio_by_id($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $data = $this->db->get('protfolio');
        $result = $data->result_array();
        return $result;
    }

    public function getAllSubscriptionPlan() {
        $this->db->select("*");
        $data = $this->db->get('subscription_plan');
        $result = $data->result_array();
        return $result;
    }

    public function getallskills($user_id) {
        $this->db->select("*");
        $this->db->where('user_id', $user_id);
        $data = $this->db->get('user_skills');
        $result = $data->result_array();
        return $result;
    }

    public function get_all_designers() {
        $this->db->select('*');
        $this->db->where('role', 'designer');
        $data = $this->db->get('users');
        $result = $data->result_array();
        return $result;
    }

    public function get_file_all($ids) {
        $this->db->select('*');
        $this->db->where_in('request_id', $ids);
        $data = $this->db->get('request_files');
        $result = $data->result_array();
        return $result;
    }

    public function getrequest_by_today_date($todaydate) {
        $this->db->select('*');
        $this->db->where('DATE(dateinprogress)', $todaydate);
        $data = $this->db->get('requests');
        $result = $data->result_array();
        return $result;
    }

    public function get_all_designers_for_today_request() {
        $this->db->select('id,email');
        $this->db->where('role', 'designer');
        $data = $this->db->get('users');
        $result = $data->result_array();
        //return $result;
        for ($i = 0; $i < count($result); $i++) {
            $this->db->select('id,title');
            $this->db->where_in('status', array('active', 'disapprove'));
            $this->db->where('designer_id', $result[$i]['id']);
            $rdata = $this->db->get('requests');
            $rresult = $rdata->result_array();
            $result[$i]['allrequest'] = $rresult;
        }
        return $result;
    }

    public function get_protfolio_category() {
        $this->db->select('*');
        $data = $this->db->get('protfolio_categories');
        $result = $data->result_array();
        return $result;
    }

    public function get_all_tags() {
        $this->db->select('tags');
        $this->db->group_by('tags');
        $data = $this->db->get('blog');
        $result = $data->result_array();
        return $result;
    }

    public function get_all_category() {
        $this->db->select('category');
        $this->db->group_by('category');
        $data = $this->db->get('blog');
        $result = $data->result_array();
        return $result;
    }

    public function get_all_users() {
        $this->db->select('password_reset,email');
        $data = $this->db->get('users');
        $result = $data->result_array();
        return $result;
    }

    public function get_user_by_id($id) {
        $this->db->select('first_name,last_name,profile_picture');
        $this->db->where('id', $id);
        $data = $this->db->get('users');
        $result = $data->result_array();
        return $result;
    }

    public function get_designer_active_request($id) {
        $this->db->where('designer_id', $id);
        $this->db->where_not_in('status', 'approved');
        $num_rows = $this->db->count_all_results('requests');
        return $num_rows;
    }

    // Get Request By Designer id from admin dashboard controller
    public function get_request_by_designer_id($id) {
        $this->db->select('*');
        $this->db->where('designer_id', $id);
        $query = $this->db->get('requests');
        $result = $query->result_array();
        return $result;
    }

    // Get Designer Information
    public function get_designers_info($category) {
        $maping = [
            'Popular Design' => 'graphics',
            'Branding & Logos' => 'brandin_logo',
            'Social Media' => 'ads_banner',
            'Marketing' => 'email_marketing',
            'Email' => 'email_marketing',
            'Ads & Banners' => 'ads_banner',
            'Web Design' => 'webdesign',
            'App Design' => 'appdesign'
        ];
        $this->db->select('a.*');
        $this->db->from('user_skills as a');
        $this->db->where($maping[$category] . "!= 0");
        $this->db->join('users as b', 'b.id = a.user_id');
        $this->db->where('b.disable_designer', 0);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    //Get Designer Projects list Count
    public function get_request_count_by_designer_id($id) {
        $this->db->where('designer_id', $id);
        $count = $this->db->count_all_results('requests');
        return $count;
    }

    // For Delete Designer From Admin
    public function delete_user($table, $id) {
        $this->db->where('id', $id);
        $result = $this->db->delete($table);
        return $result;
    }

    //Get Request by Customer Id 
    public function get_request_by_customer_id($id) {
        $this->db->select('*');
        $this->db->where('customer_id', $id);
        $this->db->where('status !=', 'approved');
        $query = $this->db->get('requests');
        $result = $query->result_array();
        return $result;
    }

    //Update Request Data from Admin or Qa
    public function update_request_admin($request_ids, $designer_id) {
        $this->db->where_in('id', $request_ids);
        $data = $this->db->update('requests', $designer_id);
        return $data;
    }

}
