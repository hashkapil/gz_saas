<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller { 

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('user_agent');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('Myfunctions');
        $this->load->library('Customfunctions');
        $this->load->library('zip');
        $config['image_library'] = 'gd2';
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->model('Welcome_model');
        $this->load->model('Request_model');
        $this->load->model('Category_model');
        $this->load->model('Account_model');
        $this->load->model('Stripe');
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }

    public function checkloginuser() {
        if (!$this->session->userdata('user_id')) {
            redirect(base_url());
        }
        if ($this->session->userdata('role') != "designer") {
            redirect(base_url());
        }
    }

    public function logout() {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_data("users", array("online" => 0), array("id" => $_SESSION['user_id']));
        $this->Request_model->logout();
        redirect(base_url());
    }

   public function index() {
        $this->checkloginuser();
        $breadcrumbs = array("Dashboard " => base_url().'designer/request');
        $this->Welcome_model->update_online();
        $data['active_project'] = $this->Request_model->designer_load_more(array('active','disapprove'), $_SESSION['user_id']);
        $data['count_active_project'] = $this->Request_model->designer_load_more(array('active','disapprove'), $_SESSION['user_id'],true);
        $data['pending_revision_project'] = $this->Request_model->designer_load_more(array('pendingrevision'), $_SESSION['user_id']);
        $data['count_pending_revision_project'] = $this->Request_model->designer_load_more(array('pendingrevision'), $_SESSION['user_id'],true);
        $data['pending_approval_project'] = $this->Request_model->designer_load_more(array('checkforapprove'), $_SESSION['user_id']);
        $data['count_pending_approval_projectt'] = $this->Request_model->designer_load_more(array('checkforapprove'), $_SESSION['user_id'],true);
        $data['approved_project'] = $this->Request_model->designer_load_more(array('approved'), $_SESSION['user_id']);
        $data['count_approved_project'] = $this->Request_model->designer_load_more(array('approved'), $_SESSION['user_id'],true);
        $data['count_hold_project'] = $this->Request_model->designer_load_more(array('hold'), $_SESSION['user_id'],true);
        $this->load->view('designer/designer_header', array("breadcrumbs" => $breadcrumbs));
        $this->load->view('designer/index', $data);
        $this->load->view('designer/designer_footer');
    }
    //  new code 26/7/19 PSk --- >>
    public function load_more_designer() {
        $group_no = $this->input->post('group_no');
        $dataArray = $this->input->post('title');
        $dataStatusText = $this->input->post('status');
        $tab_id = $this->input->post('tabId');
        $tabId = str_replace('#','',$tab_id);
        $dataStatus = explode(",", $dataStatusText);
        $search = $this->input->post('search');
        $content_per_page = LIMIT_DESIGNER_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $dataDesigner = $this->Request_model->designer_load_more($dataStatus,$_SESSION['user_id'],false,$start,$content_per_page,'',$search);
//        echo "<pre/>";print_r($dataDesigner);
        $dataDesigner_count = $this->Request_model->designer_load_more($dataStatus,$_SESSION['user_id'],true,$start,$content_per_page,'',$search);
        for ($i = 0; $i < sizeof($dataDesigner); $i++) {
            $dataDesigner[$i]['category_data'] = $this->Request_model->getRequestCatbyid($dataDesigner[$i]['id'],$dataDesigner[$i]['category_id']);
            if(isset($dataDesigner[$i]['category_data'][0]['category']) && $dataDesigner[$i]['category_data'][0]['category']!= ''){
               $dataDesigner[$i]['category_name']  = $dataDesigner[$i]['category_data'][0]['category'];
            }else{
                 $dataDesigner[$i]['category_name']  = $dataDesigner[$i]['category_data'][0]['cat_name'];
            }
            $dataDesigner[$i]['subcategory_name'] = $this->Request_model->getRequestsubCatbyid($dataDesigner[$i]['id'],$dataDesigner[$i]['category_id'],$dataDesigner[$i]['subcategory_id']);
            $dataDesigner[$i]['total_chat'] = $this->Request_model->get_chat_number($dataDesigner[$i]['id'], $dataDesigner[$i]['customer_id'], $dataDesigner[$i]['designer_id'], "designer");
            $dataDesigner[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($dataDesigner[$i]['id'], $_SESSION['user_id'], "designer");
            $getfileid = $this->Request_model->get_attachment_files($dataDesigner[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "designer");
            }
            if ($dataDesigner[$i]['status_designer'] == "active" || $dataDesigner[$i]['status_designer'] == "disapprove") {
                if ($dataDesigner[$i]['expected_date'] == '' || $dataDesigner[$i]['expected_date'] == NULL) {
                    $dataDesigner[$i]['expected'] = $this->myfunctions->check_timezone($dataDesigner[$i]['latest_update'],$dataDesigner[$i]['current_plan_name']);
                } else {
                    $dataDesigner[$i]['expected'] = $this->onlytimezone($dataDesigner[$i]['expected_date']);
                }
            } elseif ($dataDesigner[$i]['status_designer'] == "checkforapprove") {
               $dataDesigner[$i]['reviewdate'] = $this->onlytimezone($dataDesigner[$i]['modified']);
            } elseif ($dataDesigner[$i]['status_designer'] == "approved") {
                $dataDesigner[$i]['approvddate'] = $this->onlytimezone($dataDesigner[$i]['approvaldate']);
            } elseif ($dataDesigner[$i]['status_designer'] == "pendingrevision") {
                $dataDesigner[$i]['revisiondate'] = $this->onlytimezone($dataDesigner[$i]['modified']);
            }
            if ($dataDesigner[$i]['expected_date'] == '' || $dataDesigner[$i]['expected_date'] == NULL) {
                $dataDesigner[$i]['duedate'] = $this->myfunctions->check_timezone($dataDesigner[$i]['dateinprogress'],$dataDesigner[$i]['current_plan_name']);
            } else {
               $dataDesigner[$i]['duedate'] = $this->onlytimezone($dataDesigner[$i]['expected_date']);
            }
                $dataDesigner[$i]['comment_count'] = $commentcount;
                $dataDesigner[$i]['total_grade'] = $this->Request_model->get_average_rating_for_request($dataDesigner[$i]['id'], $dataDesigner[$i]['designer_id']);
                $dataDesigner[$i]['total_files'] = $this->Request_model->get_files_count($dataDesigner[$i]['id'], $dataDesigner[$i]['designer_id'], "designer");
                $dataDesigner[$i]['total_files_count'] = $this->Request_model->get_files_count_all($dataDesigner[$i]['id']);
                $dataDesigner[$i]['usertimezone_date'] = $dataDesigner[$i]['user_timezone'];
        }
        for ($i = 0; $i < sizeof($dataDesigner); $i++) {             
            for ($i = 0; $i < sizeof($dataDesigner); $i++) {          
                if($dataDesigner[$i]['status_designer'] == 'active'){
                $dateon = $dataDesigner[$i]['expected'];
                $whoreject = '';
                }elseif($dataDesigner[$i]['status_designer'] == 'disapprove' && $dataDesigner[$i]['who_reject'] == 1){
                    $dateon = $dataDesigner[$i]['expected'];
                    $whoreject = $dataDesigner[$i]['who_reject'];
                }elseif($dataDesigner[$i]['status_designer'] == 'disapprove' && $dataDesigner[$i]['who_reject'] == 0){
                    $dateon = $dataDesigner[$i]['expected'];
                    $whoreject = $dataDesigner[$i]['who_reject'];
                }elseif($dataDesigner[$i]['status_designer'] == 'pendingrevision'){
                    $dateon = $dataDesigner[$i]['revisiondate'];
                    $whoreject = '';
                }elseif($dataDesigner[$i]['status_designer'] == 'checkforapprove'){
                    $dateon = $dataDesigner[$i]['reviewdate'];
                    $whoreject = '';
                }elseif($dataDesigner[$i]['status_designer'] == 'approved'){
                    $dateon = $dataDesigner[$i]['approvaldate'];
                    $whoreject = '';
                }elseif($dataDesigner[$i]['status_designer'] == 'hold'){
                    $dateon = $dataDesigner[$i]['modified'];
                    $whoreject = '';
                }else{
                    $dateon = '';
                    $whoreject = '';
                }
                $designerVariables = $this->myfunctions->designerVariables($dataDesigner[$i]['status_designer'],$whoreject,$dateon);
                $result['projects'] = $dataDesigner[$i];
                $result['projects']['designerVariables'] = $designerVariables;
                $this->load->view('designer/tab_code',$result);
            } 
        ?>                       
        <script> countTimer();</script>
       <?php
    }
    }

    public function designer_chat_ajax() {

        $designer_file = $this->Admin_model->get_requested_files_ajax($_POST['fileid'], $_POST['id'], "", "designer", "1");
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id'], "desc");
        }
        $user = $this->Account_model->getuserbyid($_SESSION['user_id']);
        $request = $this->Request_model->get_request_by_id($_POST['id']);
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();
        $_GET['id'] = $_POST['id'];
        $i = 0;
        ?>
        <div class = "col-md-12 " style = "padding:10px;">
            <image src = "<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $_GET['id'] . "/" . $designer_file[$i]['file_name']; ?>" style = "max-height:100%;width:100%;max-height: 600px;margin: auto;display: block;" />
            <!--<div onclick = "$('.open_<?php echo $designer_file[$i]['id']; ?>').toggle();" style = "height:20px;width:20px;border-radius:50%;    background: red;position: absolute;top: 50px;left: 50px;"></div> -->
        </div>
        <div class = "appenddot_<?php echo $designer_file[$i]['id']; ?>">
            <!--Chat toolkit start-->
            <?php
            for ($j = 0; $j < sizeof($designer_file[$i]['chat']); $j++) {
                if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                    $background = "pinkbackground";
                } else {
                    $background = "bluebackground";
                }
                ?>
                <?php
                if ($designer_file[$i]['chat'][$j]['xco'] != "") {
                    if ($designer_file[$i]['chat'][$j]['sender_role'] == "customer") {
                        $image1 = "dot_image1.png";
                        $image2 = "dot_image2.png";
                    } else {
                        $image1 = "designer_image1.png";
                        $image2 = "designer_image2.png";
                    }
                    ?>
                    <div class="<?php //echo $background;         ?>" style="position:absolute;left:<?php echo $designer_file[$i]['chat'][$j]['xco']; ?>px;top:<?php echo $designer_file[$i]['chat'][$j]['yco']; ?>px;width:25px;z-index:99999999999999999;height:25px;" onclick='show_customer_chat(<?php echo $designer_file[$i]['chat'][$j]['id']; ?>);'>

                        <img src="<?php echo FS_PATH_PUBLIC_ASSETS."img/".$image1; ?>" class="customer_chatimage1 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"/>
                        <img style="display:none;" src="<?php echo FS_PATH_PUBLIC_ASSETS."img/".$image2; ?>" class="customer_chatimage2 customer_chatimage<?php echo $designer_file[$i]['chat'][$j]['id']; ?>"/>
                    </div>
                    <div class="customer_chat customer_chat<?php echo $designer_file[$i]['chat'][$j]['id']; ?>" style="display:none;background:#fff;position:absolute;left:<?php echo $designer_file[$i]['chat'][$j]['xco'] + 25; ?>px;top:<?php echo $designer_file[$i]['chat'][$j]['yco'] + 10; ?>px;padding: 5px;border-radius: 5px;">
                        <label><?php echo $designer_file[$i]['chat'][$j]['message']; ?></label>
                    </div>
                <?php }
                ?>
            <?php } ?>
        </div>
        <div class="openchat" style="width:35%;position:absolute;display:none;padding:10px 20px;background:#fff;">
            <textarea onClick="event.stopPropagation();" class='form-control text_write text_<?php echo $designer_file[$i]['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px;margin-bottom:5px;" placeholder="Leave a comment" onkeydown="javascript: if (event.keyCode == 13) {
                        $('.send_text<?php echo $designer_file[$i]['id']; ?>').click();
                    }"></textarea>

            <button onClick="send_request_img_chat('<?php echo $designer_file[0]['id']; ?>');
                    event.stopPropagation();" class="pinkbackground send_request_img_chat send_text send_text<?php echo $designer_file[$i]['id']; ?>" style="border: none;border-radius: 5px;padding: 1px 10px;" data-fileid="<?php echo $designer_file[$i]['id']; ?>" 
                    data-senderrole="designer" 
                    data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                    data-receiverid="<?php echo $request[0]['customer_id']; ?>" 
                    data-receiverrole="customer"
                    data-customername="<?php echo $user[0]['first_name']; ?>"
                    data-xco="" data-yco=""><i class="fa fa-send whitetext"></i></button>

            <span onclick="mycancellable(<?php echo $designer_file[0]['id']; ?>);
                    event.stopPropagation();" class="greytext mycancellable mycancellable<?php echo $designer_file[0]['id']; ?> " style="font-size:10px;cursor:pointer;">Cancel</span>
        </div>
        <?php
    }

    public function send_direct_message() {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();

        if (!empty($_POST)) {
            $date = date("Y-m-d H:i:s");

            $room_id = $this->Account_model->chat_data($_POST['first'], $_POST['first_id'], $_POST['second'], $_POST['second_id']);

            if ($this->Admin_model->insert_data('room_chat', array("room_id" => $room_id, "sender_id" => $_POST['second_id'], "message" => $_POST['text_sms'], "created" => $date))) {
                echo "success";
            } else {
                echo "not_success";
            }
            exit;
        }
    }

    public function load_messages_from_customer() {
        $project_id = $_POST['project_id'];
        $customer_id = $_POST['cst_id'];
        $designer_id = $_POST['desg_id'];
        $msg_seen = $_POST['customerseen'];

        $chat_request_msg = $this->Request_model->get_designer_unseen_msg($project_id, $customer_id, $designer_id, $msg_seen);
        if (!empty($chat_request_msg)) {
            $message['message'] = $chat_request_msg[0]['message'];
             $message['profile_picture'] = isset($chat_request_msg[0]['profile_picture']) ? $chat_request_msg[0]['profile_picture'] : 'user-admin.png';
            $message['typee'] = $chat_request_msg[0]['sender_type'];
            $message['with_or_not'] = $chat_request_msg[0]['with_or_not_customer'];
            $message['msg_first_name'] = $chat_request_msg[0]['first_name'];
            echo json_encode($message);
            $this->Admin_model->update_data("request_discussions", array("designer_seen" => 1), array("request_id" => $project_id));
        } else {
            // $message = "Hello Everyone";
            // echo $message;
        }
    }

    public function designer_profile_edit() {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();

        $user_id = $_SESSION['user_id'];
        $data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('designer/designer_header', array("edit_profile" => $data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('designer/profile_edit', $data);
        $this->load->view('designer/designer_footer');
    }

    public function designer_profile() {
        $this->myfunctions->checkloginuser("designer");
        $breadcrumbs = array("Settings" => base_url().'designer/request');
        $this->Welcome_model->update_online();
        $user_id = $_SESSION['user_id'];
        $skills = $this->Request_model->getallskills($user_id);
        $data = $this->Admin_model->getuser_data($user_id);
        $data[0]['profile_picture'] = $this->customfunctions->getprofileimageurl($data[0]['profile_picture']);
        $timezone = $this->Admin_model->get_timezone();
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('designer/designer_header', array("edit_profile" => $data, "notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number,'breadcrumbs' => $breadcrumbs));
        $this->load->view('designer/designer_profile', array('data' => $data, 'skills' => $skills, "timezone" => $timezone));
        $this->load->view('designer/designer_footer');
    }

    public function project_info($id) {
        $this->myfunctions->checkloginuser("designer");
        $breadcrumbs = array("Dashboard &nbsp >" => base_url().'designer/request',"Projects" => base_url().'designer/request/project_info/'.$id);
        $this->Welcome_model->update_online();
        $login_user_id = $this->load->get_var('login_user_id');
        $admin_id = $this->load->get_var('admin_id');
        $request_meta = $this->Category_model->get_quest_Ans($id);
        $sampledata = $this->Request_model->getSamplematerials($id);
       // echo "<pre/>";print_R($notes);
        if ($id == "") {
            redirect(base_url() . "designer/request");
        }
        $checkUser = $this->Request_model->checkValidUser($id);
        if ($checkUser != 1) {
             $this->session->set_flashdata('message_error', "You don't have permission to access this request!", 50);
            redirect(base_url() . "designer/request");
        }
        $url = "designer/request/project_info/" . $id;
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url));
        $this->Welcome_model->update_data("message_notification", array("shown" => 1), array("user_id" => $_SESSION['user_id'], "url" => $url));
        $this->Admin_model->update_data("request_discussions", array("designer_seen" => 1), array("request_id" => $id));
        $request = $this->Admin_model->get_all_requested_designs(array(), "", "", $id);
        $branddata = $this->Request_model->get_brandprofile_by_id($request[0]['brand_id']);
        $requestforfile = $this->Request_model->get_request_by_id($id);
        $brand_materials_files = $this->Request_model->get_brandprofile_materials_files($branddata[0]['id']);
        for($i=0;$i< sizeof($brand_materials_files);$i++){
          if($brand_materials_files[$i]['file_type'] == 'logo_upload'){
            $brand_materials_files['latest_logo'] =  $brand_materials_files[$i]['filename'];
               break;
          }
        }
        
        for ($i = 0; $i < count($request[0]['designer_attachment']); $i++) {        
        $request[0]['designer_attachment'][$i]['img_created'] = $this->onlytimezone($request[0]['designer_attachment'][$i]['created']);
        $request[0]['designer_attachment'][$i]['created'] =   date('d M, Y', strtotime($request[0]['designer_attachment'][$i]['img_created']));
        }
        $files = $this->Request_model->get_attachment_files($id, "designer");
        $uploadfiles_count = $request[0]['total_draft'];
        
//        echo "<pre>";
//        print_r($files);
//        exit();
        $file_chat_array = array();
        for ($i = 0; $i < sizeof($files); $i++) {
            $chat_of_file = $this->Request_model->get_file_chat($files[$i]['id'], "designer");

            if ($chat_of_file) {
                $file_chat_array[$i]['file_name'] = $files[$i]['file_name'];
                $file_chat_array[$i]['count'] = $chat_of_file;
                $file_chat_array[$i]['id'] = $files[$i]['id'];
                $files[$i]['chat_count'] = $chat_of_file;
            } else {
                $files[$i]['chat_count'] = "";
                $file_chat_array[$i]['id'] = "";
            }
            $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
            $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        }

        if (!empty($_FILES)) {
            $config['image_library'] = 'gd2';
            //print_R($_FILES);

            if ($_FILES['src_file']['name'] != "" && $_FILES['preview_file']['name'] != "") {
                if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id)) {
                    $data = mkdir('./'.FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id, 0777, TRUE);
                }
               
                if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb')) {
                    $data = mkdir('./'.FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb', 0777, TRUE);
                }
                $this->load->library('image_lib', $config);
                $file_name_title = substr($requestforfile[0]['title'], 0, 15);
                $file_name_title = str_replace(" ", "_", $file_name_title);
                $file_name_title = preg_replace('/[^A-Za-z0-9\_]/', '', $file_name_title);
                $six_digit_random_number = mt_rand(100, 999);
                $ext = strtolower(pathinfo($_FILES['src_file']['name'], PATHINFO_EXTENSION));
                $ext1 = strtolower(pathinfo($_FILES['preview_file']['name'], PATHINFO_EXTENSION));
                $sourcename = $file_name_title . '_' . $six_digit_random_number . '.' . $ext;
                $previewname = $file_name_title . '_' . $six_digit_random_number . '1.' . $ext1;
                if (!empty($_FILES)) {
                    $config = array(
                        'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id.'/',
                        'allowed_types' => '*',
                        'file_name' => $sourcename,
                    );
                    $config2 = array(
                        'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id.'/',
                        'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                        'max_size' => '5000000',
                        'file_name' => $previewname,
                    );
                     //   echo $config2['max_size'];
                   
                    
                    if(($_FILES['preview_file']['size'] >= $config2['max_size']) || ($_FILES['preview_file']['size'] == 0)) {
                      $this->session->set_flashdata('message_error', "File too large. File must be less than 5 MB.", 5);
                        redirect(base_url() . "designer/request/project_info/" . $id . '/' . $_POST['tab_status']);
                    }
                    if (in_array($ext1, $config2['allowed_types'])) {
                        // $config['file_name'] = $_FILES['src_file']['name'];

                        $check_src_file = $this->myfunctions->CustomFileUpload($config,'src_file');

                        $src_data = array();
                        $src_data[0]['file_name'] = "";
                        if ($check_src_file['status'] == 1) { 
                            
                        } else {
                            //$data = array('error' => $this->upload->display_errors());

                            $this->session->set_flashdata('message_error', $check_src_file['msg'], 5);
                        }


                        // $config2['preview_file'] = $_FILES['preview_file']['name'];

                        $_FILES['src_file']['name'] = $file_name_title . '_' . $six_digit_random_number . '.' . $ext1;
                        $_FILES['src_file']['type'] = $_FILES['preview_file']['type'];
                        $_FILES['src_file']['tmp_name'] = $_FILES['preview_file']['tmp_name'];
                        $_FILES['src_file']['error'] = $_FILES['preview_file']['error'];
                        $_FILES['src_file']['size'] = $_FILES['preview_file']['size'];

                       $check_preview_file = $this->myfunctions->CustomFileUpload($config2,'preview_file');

                        if ($check_preview_file['status'] == 1) {
                            $thumb_dummy_path = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id;
                            $thumb_tmp_path = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb/';
                            $tmp = $_FILES['preview_file']['tmp_name'];
                            $thumb = $this->myfunctions->make_thumb($tmp, $thumb_tmp_path . $previewname, 600, $ext1);
                            if(UPLOAD_FILE_SERVER == 'bucket'){
                            $staticName = base_url() . $thumb_tmp_path . '/' . $previewname;
                            //echo $staticName;exit;
                            $config = array(
                                'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb/',
                                'file_name' => $previewname
                            );
                            $this->load->library('s3_upload');
                            $this->s3_upload->initialize($config);
                            $uplod_thumb = $this->s3_upload->upload_multiple_file($staticName);
                            if($uplod_thumb['status'] == 1){
                               $delete = $thumb_tmp_path.$previewname;
                                  unlink($delete);
                                  unlink('./tmp/tmpfile/' . basename($staticName));

                               rmdir($thumb_tmp_path);
                               rmdir($thumb_dummy_path);

                                
                            }
                            } 
                            //print_r($request_files_data);
                            $lastcount = $uploadfiles_count+1;
                            $request_files_data = array("request_id" => $id,
                                "user_id" => $_SESSION['user_id'],
                                "user_type" => "designer",
                                "file_name" => $previewname,
                                "preview" => 1,
                                "designer_seen" => 1,
                                "created" => date("Y-m-d H:i:s"),
                                "modified" => date("Y-m-d H:i:s"),
                                "src_file" => $sourcename);

                            $data = $this->Welcome_model->insert_data("request_files", $request_files_data);

                            $qa_id = $this->Request_model->getuserbyid($login_user_id);

                            $email = $this->Request_model->getuserbyid($qa_id[0]['qa_id']);
                            
                            $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status_designer" => 'pendingrevision', "status_admin" => 'pendingrevision', "status_qa" => 'pendingrevision', "modified" => date("Y-m-d H:i:s")), array("id" => $id));
                            
                            $this->send_email_for_qa_approval($data, $id, $email[0]['email']);
                            
                            /**capture upload draft event**/
                            $this->myfunctions->capture_project_activity($id,$data,'','designer_upload_draft','project_info',$login_user_id,'0','1','1',$lastcount);
                            
                            $this->myfunctions->show_notifications($id,$login_user_id,"Designer Upload New File For \"".$request[0]['title']."\". Please Check And Approve It..!","admin/dashboard/view_files/" . $data . "?id=" . $id,$admin_id);
                            $this->myfunctions->show_notifications($id,$login_user_id,"Designer Upload New File For \"".$request[0]['title']."\". Please Check And Approve It..!","qa/dashboard/view_files/" . $data . "?id=" . $id,$qa_id[0]['qa_id']);
                            /** end save notifications when upload draft design **/

                            $changestatusdate = date("Y-m-d H:i:s", time() + 2);
                            $this->myfunctions->capture_project_activity($id,$data,"",$requestforfile[0]['status'].'_to_pendingrevision','view_request',$login_user_id,'0','1','1',"",$changestatusdate);
                            
                            // FEED BACK INSERTION 
                            $feedbackColl = $this->input->post("feedbackColl");
                            $user_id = $this->input->post("user-id");
                            $dataArr = array(
                                "designer_id" => $user_id,
                                "draft_id" => $data,
                                "request_id"=>$id,
                                "user_role" => "designer",
                                "d_feedback" =>1,
                                "d_follow_instructions" =>$feedbackColl[1],
                                "d_brand_guidelines" =>$feedbackColl[2],
                                "d_satisfied" =>$feedbackColl[3],
                                "d_review_attachments" =>$feedbackColl[4],
                                "d_dimensions" =>$feedbackColl[5],
                                "d_color_preferences" =>$feedbackColl[6],
                                "d_required_text" =>$feedbackColl[7],
                                "d_source_files" =>$feedbackColl[8],
                                "d_fonts" =>$feedbackColl[9],
                                "created" => date("Y-m-d H:i:s")
                             );

                            
                            $this->Welcome_model->insert_data("request_files_review",$dataArr);
                            
                            $this->session->set_flashdata('message_success', "File is Uploaded Successfully.!", 5);
                            redirect(base_url() . "designer/request/project_info/" . $id . '/' . $_POST['tab_status']);
                        } else {
                            $this->session->set_flashdata('message_error', $check_preview_file['msg'], 5);
                        }
                    } else {
                        $this->session->set_flashdata('message_error', "Please upload valid files", 5);
                        redirect(base_url() . "designer/request/project_info/" . $id . '/' . $_POST['tab_status']);
                    }
                }
            } else {
                $this->session->set_flashdata('message_error', "Please Upload Preview or Source Both Files.!", 5);
                redirect(base_url() . "designer/request/project_info/" . $id . '/' . $_POST['tab_status']);
            }
        }



        $request_files = $this->Admin_model->get_requested_files($id, $request[0]['customer_id'], "customer");

        $designer_preview_file = $this->Admin_model->get_requested_files($id, "", "designer", "1");
        $designer_source_file = $this->Admin_model->get_requested_files($id, "", "designer");

        $customer_additional_files = $this->Admin_model->get_requested_files($id, "", "customer");

        $admin_files = $this->Admin_model->get_requested_files($id, "", "admin");

        $designer_list = $this->Admin_model->get_total_customer("designer");
        $request = $this->Request_model->get_request_by_id($id);
        $cat_data = $this->Category_model->get_category_byID($request[0]['category_id']);
        $cat_name = $cat_data[0]['name'];
        $subcat_data = $this->Category_model->get_category_byID($request[0]['subcategory_id']);
        $subcat_name = $subcat_data[0]['name'];
        $request['cat_name'] = $cat_name;
        $request['subcat_name'] = $subcat_name;
        $notes = $this->Admin_model->get_users_notes($request[0]['customer_id']);
        $customer = $this->Account_model->getuserbyid($request[0]['customer_id']);
        $qa = array();
        if (!empty($customer)) {
            $request[0]['customer_name'] = $customer[0]['first_name'] . " " . $customer[0]['last_name'];
            $request[0]['customer_image'] = $this->customfunctions->getprofileimageurl($customer[0]['profile_picture']);
            $request[0]['customer_sortname'] = substr($customer[0]['first_name'], 0, 1) . substr($customer[0]['last_name'], 0, 1);
            $qa = $this->Account_model->getuserbyid($customer[0]['qa_id']);
            if (!empty($qa)) {
                $request[0]['qa_name'] = $qa[0]['first_name'] . " " . $qa[0]['last_name'];
                $request[0]['qa_image'] = $this->customfunctions->getprofileimageurl($qa[0]['profile_picture']);
            }
        }
        if (empty($qa)) {
            $request[0]['qa_name'] = '';
            $request[0]['qa_image'] = '';
        }
        $designer = $this->Account_model->getuserbyid($request[0]['designer_id']);
        if (!empty($designer)) {
            $request[0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
            $request[0]['designer_image'] = $this->customfunctions->getprofileimageurl($designer[0]['profile_picture']);
            $request[0]['designer_sortname'] = substr($designer[0]['first_name'], 0, 1) . substr($designer[0]['last_name'], 0, 1);
        } else {
            $request[0]['designer_name'] = "";
            $request[0]['designer_image'] = "";
            $request[0]['designer_sortname'] = "";
        }
        
        $chat_request = $this->Request_model->get_chat_request_by_id($id,'DESC');
        $timediff = '';
        for ($j = 0; $j < count($chat_request); $j++) {
           $chat_request[$j]['msg_created'] = $this->onlytimezone($chat_request[$j]['created']);
            $chat_request[$j]['chat_created_date'] = $chat_request[$j]['msg_created'];
            $chat_request[$j]['chat_created_date1'] =   date('M-d-Y h:i A', strtotime($chat_request[$j]['msg_created']));
            //echo "<pre/>";print_r($chat_request[$j]['chat_created_date1']);
            $currentdate = date('M-d-Y');
            $datetime = new DateTime($chat_request[$j]['chat_created_date1']);
            $date = $datetime->format('M-d-Y');
            $time = $datetime->format('h:i A');
            //$chat_request[$j]['chat_created_time'] = $time;
            
            if($timediff == '' && $datetimediff = ''){
             $chat_request[$j]['chat_created_datee'] =   $date; 
             $timediff =  $chat_request[$j]['chat_created_datee'];
             $datetimediff =   $chat_request[$j]['chat_created_date1'];
            }
            
            $sincedate = $chat_request[$j]['chat_created_date1'];
            $datetime1 = new DateTime($datetimediff);
            $datetime2 = new DateTime($sincedate);
            $mindiff = $datetime1->diff($datetime2);
            if ($mindiff->i >= BATCH_MESSAGE_TIMELIMIT || $chat_request[$j]['sender_id'] != $chat_request[$j - 1]['sender_id'] || $chat_request[$j]['shared_user_name'] != $chat_request[$j - 1]['shared_user_name']) {
//                echo "value of i".$i."<br/>";
                $datetimediff = $sincedate;
                $timeee = date('h:i A', strtotime($sincedate));
                $chat_request[$j]['chat_created_time'] = $timeee;
                $chat_request[$j]['profile_picture'] = $this->customfunctions->getprofileimageurl($chat_request[$j]['profile_picture']);
                $chat_request[$j]['fname'] = $chat_request[$j]['first_name'];
                $chat_request[$j]['shared_name'] = $chat_request[$j]['shared_user_name'];
            }
            if($date != $timediff){
                $timediff = $date;
                $chat_request[$j]['chat_created_datee'] = $date;
                if($date == $currentdate){
                    $chat_request[$j]['chat_created_datee'] = "Today";
                }
            }
        }
        
        $chat_request_without_customer = $this->Request_model->get_chat_request_by_id_without_customer($id);
        $timediff_nocust = '';
        for ($j = 0; $j < count($chat_request_without_customer); $j++) {
            $chat_request_without_customer[$j]['msg_created'] = $this->onlytimezone($chat_request_without_customer[$j]['created']);
            $chat_request_without_customer[$j]['chat_created_date'] = $chat_request_without_customer[$j]['msg_created'];
            $chat_request_without_customer[$j]['chat_created_date1'] =   date('M-d-Y h:i A', strtotime($chat_request_without_customer[$j]['msg_created']));
            //echo "<pre/>";print_r($chat_request[$j]['chat_created_date1']);
            $currentdate = date('M-d-Y');
            $datetime = new DateTime($chat_request_without_customer[$j]['chat_created_date1']);
            $date = $datetime->format('M-d-Y');
            $time = $datetime->format('h:i A');
            //$chat_request_without_customer[$j]['chat_created_time'] = $time;
            if($timediff == '' && $datetimediff = ''){
             $chat_request_without_customer[$j]['chat_created_datee'] =   $date; 
             $timediff =  $chat_request_without_customer[$j]['chat_created_datee'];
             $datetimediff =   $chat_request_without_customer[$j]['chat_created_date1'];
            }
            
            $sincedate = $chat_request_without_customer[$j]['chat_created_date1'];
            $datetime1 = new DateTime($datetimediff);
            $datetime2 = new DateTime($sincedate);
            $mindiff = $datetime1->diff($datetime2);
            
            if ($mindiff->i >= BATCH_MESSAGE_TIMELIMIT || $chat_request_without_customer[$j]['sender_id'] != $chat_request_without_customer[$j - 1]['sender_id'] || $chat_request_without_customer[$j]['shared_user_name'] != $chat_request_without_customer[$j - 1]['shared_user_name']) {
//                echo "value of i".$i."<br/>";
                $datetimediff = $sincedate;
                $timeee = date('h:i A', strtotime($sincedate));
                $chat_request_without_customer[$j]['chat_created_time'] = $timeee;
                $chat_request_without_customer[$j]['profile_picture'] = $this->customfunctions->getprofileimageurl($chat_request_without_customer[$j]['profile_picture']);
                $chat_request_without_customer[$j]['fname'] = $chat_request_without_customer[$j]['first_name'];
                $chat_request_without_customer[$j]['shared_name'] = $chat_request_without_customer[$j]['shared_user_name'];
            }
            if($date != $timediff){
                $timediff = $date;
                $chat_request_without_customer[$j]['chat_created_datee'] = $date;
                if($date == $currentdate){
                    $chat_request_without_customer[$j]['chat_created_datee'] = "Today";
                }
            }
        }
        $activities = $this->myfunctions->projectactivities($id,'designer_shown');
//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $user_id = $_SESSION['user_id'];
        $data = $this->Admin_model->getuser_data($user_id);
        $this->load->view('designer/designer_header', array('breadcrumbs' => $breadcrumbs));
        $this->load->view('designer/designer_project_info', array("data" => $request, "branddata" =>$branddata,"brand_materials_files" => $brand_materials_files, "request_files" => $request_files, "designer_preview_file" => $designer_preview_file, "designer_source_file" => $designer_source_file, "designer_list" => $designer_list, "customer_additional_files" => $customer_additional_files, "admin_files" => $admin_files, 'request_id' => $id, "chat_request" => $chat_request, "chat_request_without_customer" => $chat_request_without_customer, "file_chat_array" => $file_chat_array,'activities' => $activities,'notes'=>$notes,"request_meta" => $request_meta,'sampledata' => $sampledata));
        $this->load->view('designer/designer_footer');
    }

    public function project_image_view($id) {
        $login_user_id = $this->load->get_var('login_user_id');
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();
        if ($id == "") {
            redirect(base_url() . "designer/request");
        }
        $checkUser = $this->Request_model->checkValidUser($_GET['id']);
        if ($checkUser != 1) {
            $this->session->set_flashdata('message_error', "You don't have permission to access this request!", 50);
            redirect(base_url() . "designer/request");
        }
        $url = "designer/request/project_image_view/".$id."?id=".$_GET['id'];
        $this->Welcome_model->update_data("message_notification", array("shown" => 1), array("user_id" => $login_user_id, "url" => $url));
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url));
        $this->Welcome_model->update_data("request_files", array("designer_seen" => 1), array('id' => $id));
        $this->Admin_model->update_data("request_file_chat", array("designer_seen" => '1'), array("request_file_id" => $id));
        $designer_file = $this->Admin_model->get_requested_files($_GET['id'], "", "designer", "1");
        $data['request'] = $this->Request_model->get_request_by_id($_GET['id']);

        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id'],'DESC');
            
            $designer_file[$i]['modified_date'] = $this->onlytimezone($designer_file[$i]['modified']);
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified_date'];
            
            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
            $designer_file[$i]['chat'][$j]['replies'] = $this->Request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
            $designer_file[$i]['chat'][$j]['created'] = $this->onlytimezone($designer_file[$i]['chat'][$j]['created']);     
                
            }
        }
        $dataa = $this->Request_model->get_request_by_id($_GET['id']);
        $customer = $this->Account_model->getuserbyid($dataa[0]['customer_id']);
        if (!empty($customer)) {
            $data['data'][0]['customer_name'] = $customer[0]['first_name'] . " " . $customer[0]['last_name'];
            $data['data'][0]['customer_image'] = $customer[0]['profile_picture'];
            $data['data'][0]['title'] = $dataa[0]['title'];
            $data['data'][0]['customer_sortname'] = substr($customer[0]['first_name'], 0, 1) . substr($customer[0]['last_name'], 0, 1);
        }

        $designer = $this->Account_model->getuserbyid($dataa[0]['designer_id']);
        if (!empty($designer)) {
            $data['data'][0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
        }
        $data['user'] = $this->Account_model->getuserbyid($_SESSION['user_id']);
        $data['chat_request'] = $this->Request_model->get_chat_request_by_id($_GET['id']);
        $data['designer_file'] = $designer_file;
        $data['main_id'] = $id;
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $user_id = $_SESSION['user_id'];
        $infodata = $this->Admin_model->getuser_data($user_id);
        $this->load->view('designer/designer_header', array("notifications" => $notifications, "notification_number" => $notification_number, "edit_profile" => $infodata, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        $this->load->view('designer/project_image_view', $data);
        $this->load->view('designer/designer_footer');
    }

    public function send_message_request() {
        $sender_type = $_POST['sender_type'];
        if ($sender_type == 'designer') {
            $_POST['designer_seen'] = '1';
        } elseif ($sender_type == 'customer') {
            $_POST['customer_seen'] = '1';
        }
        //die();
        $admin_id = $this->load->get_var('admin_id');
        $login_user_id = $this->load->get_var('login_user_id'); 
        $_POST['created'] = date("Y-m-d H:i:s");
        // $_POST['designer_seen'] = "1";
        $success = $this->Welcome_model->insert_data("request_discussions", $_POST);
         if ($success) {
            //echo 'yes';
            $rtitle = $this->Request_model->get_request_by_id($_POST['request_id']);

            if (!empty($rtitle)) {
                /*******insert messages notifications*******/ 
                
                if ($_POST['with_or_not_customer'] == "1") {
                    
                    /* Message notification for customer */
                    $this->myfunctions->show_messages_notifications($rtitle[0]['title'],$_POST['message'],$_POST['request_id'],$login_user_id,$_POST['reciever_type'] . "/request/project_info/" . $_POST['request_id'],$_POST['reciever_id']);
                    }
                    /* Message notification for qa */
                    $this->myfunctions->show_messages_notifications($rtitle[0]['title'],$_POST['message'],$_POST['request_id'],$login_user_id,"qa/dashboard/view_project/" . $_POST['request_id'],$_SESSION['qa_id']);
                    /* Message notification for admin */
                    $this->myfunctions->show_messages_notifications($rtitle[0]['title'],$_POST['message'],$_POST['request_id'],$login_user_id,"admin/dashboard/view_request/" . $_POST['request_id'],$admin_id);
                
                /*******end insert messages notifications*******/
            } else {
                $data['title'] = "New Message Arrived";
                $data['created'] = date("Y:m:d H:i:s");
                $this->Welcome_model->insert_data("notification", $data);
            }
            $this->Request_model->get_notifications($_SESSION['user_id']);
        } else {
            $success = 0;
        }
        echo $success;
    }
    
    public function send_message() {
      // echo $_POST['request_file_id']; exit;
           $admin_id = $this->load->get_var('admin_id');
           $login_user_id = $this->load->get_var('login_user_id'); 
            $request_files = $this->Admin_model->get_request_files_byid($_POST['request_file_id']);
            //echo "<pre>";print_r($request_files);exit;
            $request_data = $this->Request_model->get_request_by_id($request_files[0]['request_id']);
            $userdata = $this->Admin_model->getuser_data($_POST['receiver_id']);
            $_POST['created'] = date("Y-m-d H:i:s");
            if ($this->Welcome_model->insert_data("request_file_chat", $_POST)) {
                echo $this->db->insert_id();
            } else {
                echo '0';
            }
            
            /* Message notification for customer */
                   if ($request_files[0]['status'] != 'pending' && $request_files[0]['status'] != 'Reject') {
                    $this->myfunctions->show_messages_notifications($request_data[0]['title'],$_POST['message'],$request_files[0]['request_id'],$login_user_id,"customer/request/project_image_view/" . $_POST['request_file_id']."?id=".$request_files[0]['request_id'],$_POST['receiver_id']);
                    }
                    /* Message notification for qa */
                    $this->myfunctions->show_messages_notifications($request_data[0]['title'],$_POST['message'],$request_files[0]['request_id'],$login_user_id,"qa/dashboard/view_files/". $_POST['request_file_id']."?id=".$request_files[0]['request_id'],$_SESSION['qa_id']);
                    /* Message notification for admin */
                    $this->myfunctions->show_messages_notifications($request_data[0]['title'],$_POST['message'],$request_files[0]['request_id'],$login_user_id,"admin/dashboard/view_files/" . $_POST['request_file_id']."?id=".$request_files[0]['request_id'],$admin_id);
                    
            /*******end insert messages notifications*******/
        
        exit;
    }

    public function get_ajax_all_request() {
        $dataArray = $this->input->get('title');
        $dataStatus = $this->input->get('status');
        $dataStatus = explode(",", $dataStatus);
        $dataDesigner = $this->Request_model->getall_request_designer_ajax_search(array('keyword' => $dataArray), $dataStatus);
        for ($i = 0; $i < sizeof($dataDesigner); $i++) {
            $dataDesigner[$i]['total_chat'] = $this->Request_model->get_chat_number($dataDesigner[$i]['id'], $dataDesigner[$i]['customer_id'], $dataDesigner[$i]['designer_id'], "designer");
            $dataDesigner[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($dataDesigner[$i]['id'], $_SESSION['user_id'], "designer");
            $getfileid = $this->Request_model->get_attachment_files($dataDesigner[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "designer");
            }
            if ($dataDesigner[$i]['status_designer'] == "active" || $dataDesigner[$i]['status_designer'] == "disapprove") {
                if ($dataDesigner[$i]['expected_date'] == '' || $dataDesigner[$i]['expected_date'] == NULL) {
                    $dataDesigner[$i]['expected'] = $this->myfunctions->check_timezone($dataDesigner[$i]['latest_update'],$dataDesigner[$i]['current_plan_name']);
                } else {
                    $dataDesigner[$i]['expected'] = $this->onlytimezone($dataDesigner[$i]['expected_date']);
                }
            } elseif ($dataDesigner[$i]['status_designer'] == "checkforapprove") {
                $dataDesigner[$i]['reviewdate'] = $this->onlytimezone($dataDesigner[$i]['modified']);
            } elseif ($dataDesigner[$i]['status_designer'] == "approved") {
                $dataDesigner[$i]['approvddate'] = $this->onlytimezone($dataDesigner[$i]['approvaldate']);
            } elseif ($dataDesigner[$i]['status_designer'] == "pendingrevision") {
                $dataDesigner[$i]['revisiondate'] = $this->onlytimezone($dataDesigner[$i]['modified']);
            }
            if ($dataDesigner[$i]['expected_date'] == '' || $dataDesigner[$i]['expected_date'] == NULL) {
                $dataDesigner[$i]['duedate'] = $this->myfunctions->check_timezone($dataDesigner[$i]['dateinprogress'],$dataDesigner[$i]['current_plan_name']);
            } else {
                $dataDesigner[$i]['duedate'] = $this->onlytimezone($dataDesigner[$i]['expected_date']);
            }
            $dataDesigner[$i]['comment_count'] = $commentcount;
            $dataDesigner[$i]['total_grade'] = $this->Request_model->get_average_rating_for_request($dataDesigner[$i]['id'], $dataDesigner[$i]['designer_id']);
            $dataDesigner[$i]['total_files'] = $this->Request_model->get_files_count($dataDesigner[$i]['id'], $dataDesigner[$i]['designer_id'], "designer");
            $dataDesigner[$i]['total_files_count'] = $this->Request_model->get_files_count_all($dataDesigner[$i]['id']);
        }
        for ($i = 0; $i < sizeof($dataDesigner); $i++) {
            if ($dataDesigner[$i]['status_designer'] == 'active' || $dataDesigner[$i]['status_designer'] == 'disapprove') {
                ?>
                <div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">  
                    <!-- List Desh Product -->
                    <?php
                    for ($i = 0; $i < sizeof($dataDesigner); $i++) {
                        $colorclass = "";
                        if ($dataDesigner[$i]['status_designer'] == "active" || $dataDesigner[$i]['status_designer'] == "disapprove") {
                            if ($dataDesigner[$i]['status_designer'] == "active") {
                                $status = "IN-Progress";
                                $colorclass = "green";
                            } elseif ($dataDesigner[$i]['status_designer'] == "disapprove" && $dataDesigner[$i]['who_reject'] == 1) {
                                $status = "REVISION";
                                $colorclass = "red orangetext";
                            } elseif ($dataDesigner[$i]['status_designer'] == "disapprove" && $dataDesigner[$i]['who_reject'] == 0) {
                                $status = "Quality Revision";
                                $colorclass = "red";
                            }
                            ?>       
                            <div class="pro-desh-row" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/project_info/<?php echo $dataDesigner[$i]['id'] ?>/1'">
                                <div class="pro-desh-box delivery-desh" style="width: 12%;">
                                    <p class="pro-a">Due Date</p>
                                    <p class="pro-b">
                                        <?php
                                        echo date('M d, Y h:i A', strtotime($dataDesigner[$i]['duedate']));
                                        ?>
                                    </p>
                                </div>

                                <div class="pro-desh-box delivery-desh" style="width: 12%;">
                                    <p class="pro-a">
                                        <?php
                                        if ($dataDesigner[$i]['status'] == "active") {
                                            echo "Expected on";
                                        } elseif ($dataDesigner[$i]['status_designer'] == "disapprove") {
                                            echo "Expected on";
                                        }
                                        ?>
                                    </p>
                                    <p class="pro-b">
                                        <?php
                                        if ($dataDesigner[$i]['status_designer'] == "active") {
                                            echo date('M d, Y h:i A', strtotime($dataDesigner[$i]['expected']));
                                        } elseif ($dataDesigner[$i]['status_designer'] == "disapprove") {
                                            echo date('M d, Y h:i A', strtotime($dataDesigner[$i]['expected']));
                                        }
                                        ?>       
                                    </p>
                                </div>

                                <div class="pro-desh-box dcol-1">
                                    <div class="desh-head-wwq">
                                        <div class="desh-inblock">
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>designer/request/project_info/<?php echo $dataDesigner[$i]['id'] ?>/1"><?php echo $dataDesigner[$i]['title']; ?></a></h3>
                                            <p class="pro-b"><?php echo $dataDesigner[$i]['category']; ?></p>
                                        </div>
                                        <div class="desh-inblock col-w1 text-right">
                                            <p class="neft text-center"><span class="<?php echo $colorclass; ?> text-uppercase"><?php echo $status; ?></span></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="pro-desh-box" style="width: 13%;">
                                    <div class="pro-circle-list clearfix">
                                        <p class="text-h">Client</p>
                                        <p class="space-a"></p>
                                        <div class="pro-circ-le">
                                            <a href="#">
                                                <?php if ($dataDesigner[$i]['profile_picture']) { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php FS_PATH_PUBLIC_UPLOADS_PROFILE . $dataDesigner[$i]['profile_picture']; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS; ?>user-admin.png" class="img-responsive">
                                                    </figure>
                                                <?php } ?>
                                                <p class="pro-circle-txt text-center">
                                                    <?php echo $dataDesigner[$i]['customer_first_name'] . " " . $dataDesigner[$i]['customer_last_name']; ?>

                                                </p>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="pro-desh-box">
                                    <div class="pro-desh-r1">
                                        <div class="pro-inleftbox">
                                            <p class="pro-a inline-per"><a href="javascript:void(0);">
                                                    <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21">
                                                        <?php if ($dataDesigner[$i]['total_chat'] + $dataDesigner[$i]['comment_count'] != 0) { ?>
                                                            <span class="numcircle-box">
                                                                <?php echo $dataDesigner[$i]['total_chat'] + $dataDesigner[$i]['comment_count']; ?>
                                                            </span>
                                                        <?php } ?></span>
                                                    <?php //echo $dataDesigner[$i]['total_chat'];  ?></a></p>
                                        </div>
                                        <div class="pro-inrightbox">
                                            <p class="pro-a inline-per"><a href="javascript:void(0);">
                                                    <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13">
                                                        <?php if (count($dataDesigner[$i]['total_files']) != 0) { ?>
                                                            <span class="numcircle-box"><?php echo count($dataDesigner[$i]['total_files']); ?></span></span>
                                                    <?php } ?>
                                                        <?php echo count($dataDesigner[$i]['total_files_count']); ?></a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>      
                            <?php
                        }
                    }
                    ?>
                </div>
            <?php } elseif ($dataDesigner[$i]['status_designer'] == 'assign') { ?>
                <div class="tab-pane  content-datatable datatable-width" id="inprogressrequest" role="tabpanel"> 
                    <?php
                    for ($i = 0; $i < sizeof($dataDesigner); $i++) {
                        $colorclass = "";
                        if ($dataDesigner[$i]['status'] == "assign") {
                            $status = "In-Queue";
                            $colorclass = "gray";
                            //echo "greentext";
                        }
                        ?>
                        <div class="pro-desh-row" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/project_info/<?php echo $dataDesigner[$i]['id'] ?>/2'">
                            <div class="pro-desh-box dcol-1">
                                <div class="desh-head-wwq">
                                    <div class="desh-inblock">
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>designer/request/project_info/<?php echo $dataDesigner[$i]['id'] ?>/2"><?php echo $dataDesigner[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo $dataDesigner[$i]['category']; ?></p>
                                    </div>
                                    <div class="desh-inblock col-w1 text-right">
                                        <p class="neft" ><span class="gray text-uppercase"><?php echo $status; ?></span></p>
                                    </div>
                                </div>
                            </div>

                            <div class="pro-desh-box dcol-3">
                                <div class="pro-circle-list clearfix pad-w1">
                                    <p class="pro-a">Delivery</p>
                                    <p class="space-a"></p>
                                    <div class="pro-circle-box">
                                        <a href="javascript:void(0);">
                                            <?php if ($dataDesigner[$i]['customer_profile_picture']) { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE. $dataDesigner[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                            <p class="pro-circle-txt text-center"><?php echo $dataDesigner[$i]['customer_first_name'] . " " . $dataDesigner[$i]['customer_last_name']; ?></p></a>
                                    </div>
                                </div>
                            </div>

                            <div class="pro-desh-box">
                                <div class="pro-desh-r1">
                                    <div class="pro-inleftbox">
                                        <p class="pro-a inline-per"><a href="javascript:void(0);">
                                                <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21">
                                                    <?php if ($dataDesigner[$i]['total_chat'] + $dataDesigner[$i]['comment_count'] != 0) { ?>
                                                        <span class="numcircle-box">
                                                            <?php echo $dataDesigner[$i]['total_chat'] + $dataDesigner[$i]['comment_count']; ?>
                                                        </span>
                                                    <?php } ?></span>
                                                <?php //echo $dataDesigner[$i]['total_chat_all'];   ?></a></p>
                                    </div>
                                    <div class="pro-inrightbox">
                                        <p class="pro-a inline-per">
                                            <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13"> <?php if (count($dataDesigner[$i]['total_files']) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($dataDesigner[$i]['total_files']); ?></span></span>
                                            <?php } ?>
                                            <?php echo count($dataDesigner[$i]['total_files_count']); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?> 
                </div>
            <?php } elseif ($dataDesigner[$i]['status_designer'] == 'pendingrevision') { ?>
                <div class="tab-pane content-datatable datatable-width" id="pending_designs_tab" role="tabpanel">
                    <?php
                    for ($i = 0; $i < sizeof($dataDesigner); $i++) {
                        $colorclass = "";
                        if ($dataDesigner[$i]['status_designer'] == "pendingrevision") {
                            $status = "Pending Review";
                            $colorclass = "lightbluetext";
                            //echo "greentext"; 
                            ?>
                            <div class="pro-desh-row" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/project_info/<?php echo $dataDesigner[$i]['id'] ?>/3'">
                                <div class="pro-desh-box delivery-desh">
                                    <p class="pro-a">Delivered on</p>
                                    <p class="pro-b">
                                        <?php
                                        echo date('M d, Y h:i A', strtotime($dataDesigner[$i]['revisiondate']));
                                        ?>

                                    </p>
                                </div>

                                <div class="pro-desh-box dcol-1">
                                    <div class="desh-head-wwq">
                                        <div class="desh-inblock">
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>designer/request/project_info/<?php echo $dataDesigner[$i]['id'] ?>/3"><?php echo $dataDesigner[$i]['title']; ?></a></h3>
                                            <p class="pro-b"><?php echo $dataDesigner[$i]['category']; ?></p>
                                        </div>
                                        <div class="desh-inblock text-right">
                                            <p class="neft"><span class="<?php echo $colorclass; ?> text-uppercase"><?php echo $status; ?></span></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="pro-desh-box dcol-2">
                                    <div class="pro-circle-list clearfix">
                                        <p class="pro-a">Delivery</p>
                                        <p class="space-a"></p>
                                        <div class="pro-circle-box">
                                            <a href="javascript:void(0);">
                                                <?php if ($dataDesigner[$i]['customer_profile_picture']) { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE. $dataDesigner[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    </figure>
                                                <?php } ?>
                                                <p class="pro-circle-txt text-center"><?php echo $dataDesigner[$i]['customer_first_name'] . " " . $dataDesigner[$i]['customer_last_name']; ?></p></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="pro-desh-box">
                                    <div class="pro-desh-r1">
                                        <div class="pro-inleftbox">
                                            <p class="pro-a inline-per"><a href="javascript:void(0);">
                                                    <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21"><?php if ($dataDesigner[$i]['total_chat'] + $dataDesigner[$i]['comment_count'] != 0) { ?>
                                                            <span class="numcircle-box">
                                                                <?php echo $dataDesigner[$i]['total_chat'] + $dataDesigner[$i]['comment_count']; ?>
                                                            </span>
                                                        <?php } ?></span>
                                                    <?php //echo $dataDesigner[$i]['total_chat_all'];    ?></a></p>
                                        </div>
                                        <div class="pro-inrightbox">
                                            <p class="pro-a inline-per"><a href="javascript:void(0);">
                                                    <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13"> <?php if (count($dataDesigner[$i]['total_files']) != 0) { ?>
                                                            <span class="numcircle-box"><?php echo count($dataDesigner[$i]['total_files']); ?></span></span>
                                                    <?php } ?>
                                                        <?php echo count($dataDesigner[$i]['total_files_count']); ?></a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?> 
                </div>
            <?php } elseif ($dataDesigner[$i]['status_designer'] == 'checkforapprove') { ?>
                <div class="tab-pane content-datatable datatable-width" id="pending_designs_tab" role="tabpanel">
                    <?php
                    for ($i = 0; $i < sizeof($dataDesigner); $i++) {
                        $colorclass = "";
                        if ($dataDesigner[$i]['status'] == "checkforapprove") {
                            $status = "Pending Approval";
                            $colorclass = "bluetext";
                            //echo "greentext"; 
                            ?>
                            <div class="pro-desh-row" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/project_info/<?php echo $dataDesigner[$i]['id'] ?>/4'">
                                <div class="pro-desh-box delivery-desh">
                                    <p class="pro-a">Delivered on</p>
                                    <p class="pro-b">
                                        <?php
                                        echo date('M d, Y h:i A', strtotime($dataDesigner[$i]['reviewdate']));
                                        ?>
                                    </p>
                                </div>

                                <div class="pro-desh-box dcol-1">
                                    <div class="desh-head-wwq">
                                        <div class="desh-inblock">
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>designer/request/project_info/<?php echo $dataDesigner[$i]['id'] ?>/4"><?php echo $dataDesigner[$i]['title']; ?></a></h3>
                                            <p class="pro-b"><?php echo $dataDesigner[$i]['category']; ?></p>
                                        </div>
                                        <div class="desh-inblock text-right">
                                            <p class="neft"><span class="red <?php echo $colorclass; ?> text-uppercase"><?php echo $status; ?></span></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="pro-desh-box dcol-2">
                                    <div class="pro-circle-list clearfix">
                                        <p class="pro-a">Delivery</p>
                                        <p class="space-a"></p>
                                        <div class="pro-circle-box">
                                            <a href="javascript:void(0);">
                                                <?php if ($dataDesigner[$i]['customer_profile_picture']) { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE. $dataDesigner[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    </figure>
                                                <?php } ?>
                                                <p class="pro-circle-txt text-center"><?php echo $dataDesigner[$i]['customer_first_name'] . " " . $dataDesigner[$i]['customer_last_name']; ?></p></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="pro-desh-box">
                                    <div class="pro-desh-r1">
                                        <div class="pro-inleftbox">
                                            <p class="pro-a inline-per"><a href="javascript:void(0);">
                                                    <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21"><?php if ($dataDesigner[$i]['total_chat'] + $dataDesigner[$i]['comment_count'] != 0) { ?>
                                                            <span class="numcircle-box">
                                                                <?php echo $dataDesigner[$i]['total_chat'] + $dataDesigner[$i]['comment_count']; ?>
                                                            </span>
                                                        <?php } ?></span>
                                                    <?php //echo $dataDesigner[$i]['total_chat_all'];    ?></a></p>
                                        </div>
                                        <div class="pro-inrightbox">
                                            <p class="pro-a inline-per"><a href="javascript:void(0);">
                                                    <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13"> <?php if (count($dataDesigner[$i]['total_files']) != 0) { ?>
                                                            <span class="numcircle-box"><?php echo count($dataDesigner[$i]['total_files']); ?></span></span>
                                                    <?php } ?>
                                                        <?php echo count($dataDesigner[$i]['total_files_count']); ?></a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?> 
                </div>
            <?php } elseif ($dataDesigner[$i]['status_designer'] == 'approved') { ?>
                <div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
                    <?php
                    for ($i = 0; $i < sizeof($dataDesigner); $i++) {
                        $colorclass = "";
                        if ($dataDesigner[$i]['status'] == "approved") {
                            $status = "Completed";
                            ?>
                            <div class="pro-desh-row" onclick="window.location.href = '<?php echo base_url(); ?>designer/request/project_info/<?php echo $dataDesigner[$i]['id'] ?>/5'">
                                <div class="pro-desh-box delivery-desh">
                                    <p class="pro-a">Approved on</p>
                                    <p class="pro-b">
                                        <?php
                                        echo date('M d, Y h:i A', strtotime($dataDesigner[$i]['approvddate']));
                                        ?> 
                                    </p>
                                </div>

                                <div class="pro-desh-box dcol-1">
                                    <div class="desh-head-wwq">
                                        <div class="desh-inblock">
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>designer/request/project_info/<?php echo $dataDesigner[$i]['id'] ?>/5"><?php echo $dataDesigner[$i]['title']; ?></a></h3>
                                            <p class="pro-b"><?php echo $dataDesigner[$i]['category']; ?></p>
                                        </div>
                                        <div class="desh-inblock col-w1 text-right">
                                            <p class="neft"><span class="green text-uppercase"><?php echo $status; ?></span></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="pro-desh-box dcol-2">
                                    <div class="pro-circle-list clearfix">
                                        <p class="pro-a">Delivery</p>
                                        <p class="space-a"></p>
                                        <div class="pro-circle-box">
                                            <a href="javascript:void(0);">
                                                <?php if ($dataDesigner[$i]['customer_profile_picture']) { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE. $dataDesigner[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    </figure>
                                                <?php } ?>
                                                <p class="pro-circle-txt text-center"><?php echo $dataDesigner[$i]['customer_first_name'] . " " . $dataDesigner[$i]['customer_last_name']; ?></p></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="pro-desh-box">
                                    <div class="pro-desh-r1">
                                        <div class="pro-inleftbox">
                                            <p class="pro-a inline-per"><a href="javascript:void(0);">
                                                    <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>theme/designer_css/images/icon-chat.png" class="img-responsive" width="21"><?php if ($dataDesigner[$i]['total_chat'] + $dataDesigner[$i]['comment_count'] != 0) { ?>
                                                            <span class="numcircle-box">
                                                                <?php echo $dataDesigner[$i]['total_chat'] + $dataDesigner[$i]['comment_count']; ?>
                                                            </span>
                                                        <?php } ?></span>
                                                    <?php //echo $dataDesigner[$i]['total_chat_all'];   ?></a></p>
                                        </div>
                                        <div class="pro-inrightbox">
                                            <p class="pro-a inline-per"><a href="javascript:void(0);">
                                                    <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>theme/designer_css/images/icon-file.png" class="img-responsive" width="13">
                                                        <?php if (count($dataDesigner[$i]['total_files']) != 0) { ?>
                                                            <span class="numcircle-box"><?php echo count($dataDesigner[$i]['total_files']); ?></span></span>
                                                        <?php } ?>
                                                        <?php echo count($dataDesigner[$i]['total_files_count']); ?></a></p>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <?php
                        }
                    }
                    ?>
                </div>
                </div>
                <?php
            }
        }
    }

    public function downloadzip() {
        if (isset($_POST)) {
            $file_name = FS_PATH;
            $s3Path = str_replace("https://", "http://", $file_name);
            $source_filename = substr($_POST['srcfile'], strrpos($_POST['srcfile'], '/') + 1);
            $this->load->helper('download');
            $path = file_get_contents(FS_PATH . $_POST['srcfile']); // get file name
            $name = $source_filename; // new name for your file
            force_download($name, $path);
        }
    }
    public function gz_delete_files() {
        $login_user_id = $this->load->get_var('login_user_id'); 
        if (isset($_POST)) {
           $id = $_POST['requests_files'];
           $request_id = $_POST['request_id'];

            $delete = $this->Admin_model->delete_data('request_files', $id);
            $this->myfunctions->capture_project_activity($request_id,$id,'','delete_draft_by_designer','gz_delete_files',$login_user_id,'0','1','1',$_POST['draft_count']);
            if ($delete) {
            if (array_key_exists('prefile', $_POST)) {
            $src_filename = $_POST['srcfile'];
            $pre_filename = $_POST['prefile'];
            $thumb_prefile = $_POST['thumb_prefile'];
             redirect(base_url() . 'designer/request/project_info/' . $request_id . '/1');

              }
          }

        }
    }

    

   public function onlytimezone($date_zone) {
         $nextdate = $this->myfunctions->onlytimezoneforall($date_zone); 
        return $nextdate;
    }

   public function send_email_for_qa_approval($file_id, $project_id, $email) {
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $query1 = $this->db->select('first_name')->from('users')->where('email',$email)->get();
        $query2 = $this->db->select('title')->from('requests')->where('id',$project_id)->get();
        $fname = $query1->result();
        $ptitle = $query2->result();
        $data = array(
            'file_id' => $file_id,
            'project_id' => $project_id,
            'title' => isset($ptitle[0]->title) ? $ptitle[0]->title : '',
            'fname' => isset($fname[0]->first_name)?$fname[0]->first_name:''
        );
        $body = $this->load->view('emailtemplate/email_template_for_qa_approval', $data, TRUE);
        $receiver = $email;

        $subject = 'GraphicsZoo: Projects needs a Review';

        $message = $body;

        $title = 'GraphicsZoo';

        SM_sendMail($receiver, $subject, $message, $title);


        // $config = $this->config->item('email_smtp');
        // $from = $this->config->item('from');
        // $this->load->library('email', $config);
        // $this->email->set_newline("\r\n");
        // $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        // $this->email->set_header('Content-type', 'text/html');
        // $this->email->from($from, 'GraphicsZoo');
        // $data = array(
        //      'file_id'=> $file_id,
        //      'project_id' => $project_id
        //          );
        // $this->email->to($email);  // replace it with receiver mail id
        // $this->email->subject("Qa Approved "); // replace it with relevant subject 
        // $body = $this->load->view('emailtemplate/email_template_for_qa_approval',$data,TRUE);
        // $this->email->message($body);   
        // @$this->email->send();     
    }

    public function deletefile($id) {
        
        $delete = $this->Admin_model->delete_data('request_files', $id);
        if ($delete) {
            redirect(base_url() . 'designer/request/project_info/' . $_GET["id"] . '/1');
        }
    }
    
    public function request_verified_by_designer(){
        $data = array();
        $ischecked = $_POST['ischecked'];
        $requestId = $_POST['data_id'];
        $data['is_verified_by_designer'] = date("Y:m:d H:i:s");
        $data['verified'] = $ischecked;
        //if($ischecked){
        $success = $this->Welcome_model->update_data("requests", $data, array("id" => $requestId));
        if($success){
            echo json_encode($data);
        }
    }
    
    public function clearAllmessages(){
       $delids =  (!empty($_POST['delid']) ? $_POST['delid'] : '');
       foreach($delids as $delk => $delv){
            $success = $this->Welcome_model->update_data("message_notification", array('shown'=>'1'), array("id" => $delv));
       }
       echo $success;
    }
    
    public function download_projectfile($id, $filename="") {
    $filename = $_GET['imgpath'];
        $file_name1 = FS_PATH_PUBLIC_UPLOADS_REQUESTS . $id . "/" . $filename;
        //$file_name1 = str_replace(" ", "_", $file_name1);
        $this->load->helper('download');
        $path = file_get_contents($file_name1); // get file name
        $name = $filename; // new name for your file
        force_download($name, $path);

        redirect(base_url() . "designer/request/project_info/" . $id);
    }
}
