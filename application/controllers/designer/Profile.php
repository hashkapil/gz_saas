<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('Myfunctions');
        $this->load->library('Customfunctions');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->model('Welcome_model');
        $this->load->model('Account_model');
        $this->load->model('Request_model');
        $this->load->model('Stripe');
    }

    public function checkloginuser() {
        if (!$this->session->userdata('user_id')) {
            redirect(base_url());
        }
        if ($this->session->userdata('role') != "designer") {
            redirect(base_url());
        }
    }

    public function dashboard() {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();

        redirect(base_url() . "designer/request/all_request");
        $today_customer = $this->Admin_model->get_today_customer();
        $all_customer = $this->Admin_model->get_total_customer();
        $today_requests = $this->Admin_model->get_today_request();
        $total_requests = $this->Admin_model->get_total_request();

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('designer/designer_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));

        $this->load->view('designer/index', array('today_customer' => $today_customer, 'all_customer' => $all_customer, 'today_requests' => $today_requests, 'total_requests' => $total_requests));
        $this->load->view('designer/designer_footer');
    }

    public function view_profile() {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('designer/designer_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));

        $this->load->view('designer/view_profile');
        $this->load->view('designer/designer_footer');
    }

    public function chat() {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();
        //$data['chat_customer'] = $this->Account_model->get_user_for_chat("designer_id","",$_SESSION['user_id']);
        $data['room_no'] = "";
        $data['chat_customer'] = $this->Account_model->get_user_chat_for_designer("designer_id", "customer_id", $_SESSION['user_id']);
         for ($i = 0; $i < sizeof($data['chat_customer']); $i++) {

            $data['chat_customer'][$i]['last_message'] = $this->Account_model->get_last_message_room($data['chat_customer'][$i]['room_id']);
            if ($data['chat_customer'][$i]['customer_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['customer_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['customer_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }
            }
            if ($data['chat_customer'][$i]['qa_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['va_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
        }
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('designer/designer_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));

        $this->load->view('designer/chat', $data);
        $this->load->view('designer/designer_footer');
    }

    public function room_chat($room_no = null) {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();
		$this->Welcome_model->update_data("message_notification",array("shown"=>"1"),array("url"=>"designer/profile/room_chat/".$room_no,"user_id"=>$_SESSION['user_id']));
        if ($room_no == "") {
            redirect(base_url() . "designer/profile/chat");
        }
		$data['room_no'] = $room_no;
        $data['userdata'] = $this->Account_model->get_userdata_from_room_id($room_no);
        $data['chat'] = $this->Account_model->get_chat($room_no);

        //$data['chat_customer'] = $this->Account_model->get_user_for_chat("designer_id",$_SESSION['user_id']);
        $data['chat_customer'] = $this->Account_model->get_user_chat_for_designer("designer_id", "customer_id", $_SESSION['user_id']);
        
         for ($i = 0; $i < sizeof($data['chat_customer']); $i++) {

            $data['chat_customer'][$i]['last_message'] = $this->Account_model->get_last_message_room($data['chat_customer'][$i]['room_id']);
            if ($data['chat_customer'][$i]['customer_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['customer_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['customer_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }
            }
            if ($data['chat_customer'][$i]['qa_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['va_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
        }
        $data['room_no'] = $room_no;
        $user = $this->Account_model->getuserbyid($_SESSION['user_id']);
        $data['myname'] = $user[0]['first_name'] . " " . $user[0]['last_name'];

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('designer/designer_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));

        $this->load->view('designer/chat', $data);
        $this->load->view('designer/designer_footer');
    }

    public function send_room_message() {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();

        if (!empty($_POST)) {
            $_POST['created'] = date("Y-m-d H:i:s");
            $room_data = $this->Account_model->get_userdata_from_room_id($_POST['room_id']);
            $not_data['url'] = "customer/profile/room_chat/" . $_POST['room_id'];
            $not_data['created'] = date("Y:m:d H:i:s");
            $not_data['heading'] = "";
            $not_data['title'] = substr($_POST['message'], 0, 30) . "...";

            $ids = array();
            foreach ($room_data as $id => $name) {
                if ($id == $_SESSION['user_id']) {
                    $not_data['heading'] = $name;
                } else {
                    $ids[] = $id;
                }
            }
            for ($i = 0; $i < sizeof($ids); $i++) {
                $not_data['user_id'] = $ids[$i];
                $mn = $this->Request_model->get_request_by_data($not_data['url'], $not_data['heading'], $not_data['user_id'], 0);
                if (empty($mn)) {
                    $this->Welcome_model->insert_data("message_notification", $not_data);
                } else {
                    $this->Welcome_model->update_data("message_notification", array("count" => $mn[0]['count'] + 1, "created" => date("Y:m:d H:i:s"), "title" => $not_data['title']), array("url" => $not_data['url'], "heading" => $not_data['heading'], "user_id" => $not_data['user_id'], "shown" => 0));
                }
            }
            $success = $this->Welcome_model->insert_data("room_chat", $_POST);

            echo $success;
            exit;
        }
    }

    public function edit_profile() {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();

        $user_id = $_SESSION['user_id'];
        $data = $this->Admin_model->getuser_data($user_id);

        if (!empty($_POST)) {
            unset($_POST['savebtn']);
        if (!empty($_FILES)) {

                    if ($_FILES['profile']['name'] != "") {
                        
                        if (!empty($_FILES)) {
                            $config2 = array(
                                'upload_path' => './uploads/profile_picture',
                                'allowed_types' => "*",
                                'encrypt_name' => TRUE
                            );
                            $config2['profile'] = $_FILES['profile']['name'];

                            $this->load->library('upload', $config2);

                            if ($this->upload->do_upload('profile')) {
                                $data = array($this->upload->data());
        						$_POST['profile_picture'] = $data[0]['file_name'];
                            } else {
                                $data = array('error' => $this->upload->display_errors());
                                $this->session->set_flashdata('message_error', $data, 5);
                            }
                        }
                    }
                }
		
            $update_data_result = $this->Admin_model->update_data('users', $_POST, array("id" => $user_id));
            if ($update_data_result) {
                $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
            } else {
                $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
            }
            redirect(base_url() . "designer/profile/edit_profile");
        }
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('designer/designer_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));

        $this->load->view('designer/edit_profile', array("data" => $data));
        $this->load->view('designer/designer_footer');
    }

    public function change_password() {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();

        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

            if ($user_data[0]['new_password'] != $_POST['old_password']) {
                $this->session->set_flashdata("message_error", "Old Password is not correct..!", 5);
                redirect(base_url() . "customer/ChangeProfile/change_password");
            }
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "customer/ChangeProfile/change_password");
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "designer/profile/change_password");
        }
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('designer/designer_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));

        $this->load->view('designer/change_password');
        $this->load->view('designer/designer_footer');
    }
    public function designer_change_password_profile(){
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();
        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
//            echo "userpwd".$user_data[0]['new_password']."<br/>";
//            echo $_POST['old_password'];
//            exit;
            if ($user_data[0]['new_password'] != md5($_POST['old_password'])) {
                $this->session->set_flashdata("message_error", "Old Password is not correct..!", 5);
                redirect(base_url() . "designer/request/designer_profile");
            }
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "designer/request/designer_profile");
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "designer/request/designer_profile");
        }
    }
    
    public function designer_change_password__edit_profile(){
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();

        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

            if ($user_data[0]['new_password'] != $_POST['old_password']) {
                $this->session->set_flashdata("message_error", "Old Password is not correct..!", 5);
                redirect(base_url() . "designer/request/designer_profile_edit");
            }
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "designer/request/designer_profile_edit");
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "designer/request/designer_profile_edit");
        }
    }
    public function public_profile() {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();

        $full_data = $this->Request_model->getuserbyid($_SESSION['user_id']);
        $dt = $this->Request_model->get_average_rating($full_data[0]['id']);

        $user_id = $_SESSION['user_id'];
        $data = $this->Admin_model->getuser_data($user_id);
        if (!empty($_POST)) {

            unset($_POST['comformbtn']);
            $change_data = $this->Admin_model->update_data('users', $_POST, array("id" => $user_id));
            if ($change_data) {
                $this->session->set_flashdata('message_success', 'About me & Specializations Update Successfully.!', 5);
            } else {
                $this->session->set_flashdata('message_error', 'About me & Specializations Not Update', 5);
            }
            redirect(base_url() . "designer/profile/public_profile");
        }
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('designer/designer_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));

        $this->load->view('designer/public_profile', array("data" => $full_data, "dt" => $dt));
        $this->load->view('designer/designer_footer');
    }

    public function get_chat_ajax() {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();
		$this->Welcome_model->update_data("message_notification", array("shown" => 1), array("url" => "customer/profile/room_chat/".$_POST['roomid']));
        $chat = $this->Account_model->get_chat($_POST['roomid']);
        $data['room_no'] = $_POST['roomid'];
		$room_no = $_POST['roomid'];
        $data['chat_customer'] = $this->Account_model->get_user_for_chat("designer_id", $_SESSION['user_id']);
        $userdata = $this->Account_model->get_userdata_from_room_id($_POST['roomid']);

        for ($i = 0; $i < sizeof($data['chat_customer']); $i++) {
            $data['chat_customer'][$i]['last_message'] = $this->Account_model->get_last_message_room($data['chat_customer'][$i]['room_id']);
            if ($data['chat_customer'][$i]['customer_id'] != "") {
                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['customer_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['qa_id'] != "") {
                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['va_id'] != "") {
                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
        }
        $user = $this->Account_model->getuserbyid($_SESSION['user_id']);
        $data['myname'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
		$myname = $data['myname'];
		$chat_customer = $data['chat_customer']
        ?>

		<div class="chatpage-tit">
			<h4><?php
				if (isset($chat_customer[0]['chat_name'])) {
					echo $chat_customer[0]['chat_name'];
				}
				?></h4>
		</div>

		<div class="chat-main">

			<?php if (isset($chat)) { ?>

				<div class="myscroller2 roomchat messagediv_<?php echo $room_no; ?>" style="height:500px; overflow-y: scroll;">
					<?php for ($i = 0; $i < sizeof($chat); $i++) { ?>


						<!--<div>-->
						<?php if ($_SESSION['user_id'] == $chat[$i]['sender_id']) { ?>


							<div class="chatbox2 right">
								<!--<div class="avatar">
									&nbsp;
								</div>-->

								<div class="message-text-wrapper messageright">
									<p class="description">
										<?php 
										if($chat[$i]['isimage'] == "1"){
											echo '<img src="'.base_url()."uploads/requests/".$chat[$i]['message'].'" />'; 
										}else{ 
											 echo $chat[$i]['message']; 
										} ?>
									</p>
									<p class="posttime">
										<?php
										$date = strtotime($chat[$i]['created']);
										$diff = strtotime(date("Y:m:d H:i:s")) - $date;
										$minutes = floor($diff / 60);
										$hours = floor($diff / 3600);
										if ($hours == 0) {
											echo $minutes . " Minutes Ago";
										} else {
											$days = floor($diff / (60 * 60 * 24));
											if ($days == 0) {
												echo $hours . " Hours Ago";
											} else {
												echo $days . " Days Ago";
											}
										}
										?>
									</p>
								</div>
							</div>


						<?php } else { ?>

							<div class="chatbox left">
								<div class="avatar">
									<img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
								</div>

								<div class="message-text-wrapper messageleft">

									<p class="title">
										<?php echo $userdata[$chat[$i]['sender_id']]; ?>
									</p>
									<p class="description">
										<?php 
										if($chat[$i]['isimage'] == "1"){
											echo '<img src="'.base_url()."uploads/requests/".$chat[$i]['message'].'" />'; 
										}else{ 
											 echo $chat[$i]['message']; 
										} ?>
									</p>
									<p class="posttime">
										<?php
										$date = strtotime($chat[$i]['created']);
										$diff = strtotime(date("Y:m:d H:i:s")) - $date;
										$minutes = floor($diff / 60);
										$hours = floor($diff / 3600);
										if ($hours == 0) {
											echo $minutes . " Minutes Ago";
										} else {
											$days = floor($diff / (60 * 60 * 24));
											if ($days == 0) {
												echo $hours . " Hours Ago";
											} else {
												echo $days . " Days Ago";
											}
										}
										?>
									</p>

								</div>
							</div>
						<?php } ?>
						<!--</div>-->

					<?php } ?>
				</div>

				<!--<div class="col-md-12" style="margin-top: 20px;">
					<input type='text' class='form-control myText room_chat_text text_<?php echo $room_no; ?>' style="border: 1px solid !important;border-radius: 6px !important;" placeholder="Type a message here" />
					<span style="position: absolute;top: 10px;right: 30px;font-size: 18px;" class="pinktext fa fa-send send_room_chat"  data-room="<?php echo $room_no; ?>" data-sendername="<?php echo $myname; ?>"></span>
				</div>-->


				<!--<form class="myform" method="post" action="" enctype="multipart/form-data"  style="clear: both;position: absolute;bottom: -7px;z-index: 9999999999999;">

					<span class="chatcamera"><i class="fa fa-camera" id="openimageupload"></i></span>
					<div class="hiddenfile">
						<input name="upload" type="file" id="fileinput" onchange="validateAndUpload(this);"/>
					</div>
					<input type="hidden" value="<?php echo $room_no; ?>" class="hidden_room_no" name="room_no"/>
					<button type="submit" style="margin-top: -16px; margin-right: 15px;display: none;" class="myfilesubmit">Image Submit</button>

				</form>
				<input type='text' class='form-control myText room_chat_text text_<?php echo $room_no; ?>' style="border: 1px solid; border-radius: 0px !important; height: 60px; padding-right: 30px; padding-left: 60px; box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.2); margin-top: 20px; position: relative; bottom: -20px; left: -20px; right: -20px;" placeholder="Type a message here" />
				<span style="margin-top: -15px; margin-right: 15px;" class="chatsendbtn send_room_chat"  data-room="<?php echo $room_no; ?>" data-sendername="<?php echo $myname; ?>">Send</span>-->



				<!--<input  type='text' class='form-control sendtext text_<?php echo $data[0]['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px; height: 44px;" placeholder="Reply To Client" />
				<span class="chatsendbtn send_request_chat send"
					  data-requestid="<?php echo $data[0]['id']; ?>"
					  data-senderrole="customer"
					  data-senderid="<?php echo $_SESSION['user_id']; ?>"
					  data-receiverid="<?php echo $data[0]['designer_id']; ?>"
					  data-receiverrole="designer"
					  data-sendername="<?php echo $data[0]['customer_name']; ?>">Send
				</span>-->

			<?php } ?>

		</div>

		<div class="chatbtn-boxmain">
			<form onsubmit="myform_notsubmit(event,this);" class="myform" method="post" action="" enctype="multipart/form-data"  style="clear: both;position: absolute;bottom: -7px;z-index: 9999999999999;">
				<span class="chatcamera"><i class="fa fa-paperclip" id="openimageupload" onclick="$('#fileinput').click();"></i></span>
				<div class="hiddenfile">
					<input name="upload" type="file" id="fileinput" onchange="validateAndUpload(this);"/>
				</div>
				<input type="hidden" value="<?php echo $room_no; ?>" class="hidden_room_no" name="room_no"/>
				<button type="submit" style="margin-top: -16px; margin-right: 15px;display: none;" class="myfilesubmit">Image Submit</button>
			</form>
			<input type='text' class='form-control chatlargeinp myText room_chat_text text_<?php echo $room_no; ?>' placeholder="Type a message here" />
			<span style="margin-right: 15px;" class="chatsendbtn send_room_chat"  data-room="<?php echo $room_no; ?>" data-sendername="<?php echo $myname; ?>"  onclick="send_room_chat();">Send</span>
	   </div>

        <?php
    }

    //chat ajex
    public function get_chat_designer_ajex() {
        $this->myfunctions->checkloginuser("designer");
        $this->Welcome_model->update_online();
        
        $data['room_no'] = $_POST['roomid'];
        $room_no = $_POST['roomid'];
        
        $data['chat_customer'] = $this->Account_model->get_user_for_chat("customer_id", $_SESSION['user_id']);
        for ($i = 0; $i < sizeof($data['chat_customer']); $i++) {

            $data['chat_customer'][$i]['last_message'] = $this->Account_model->get_last_message_room($data['chat_customer'][$i]['room_id']);
            if ($data['chat_customer'][$i]['designer_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['designer_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['designer_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }
            }
            if ($data['chat_customer'][$i]['qa_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['va_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
        }
        $chat_customer = $data['chat_customer'];
        $myname = "";
        for ($i = 0; $i < sizeof($chat_customer); $i++) {
            ?>

            
			
			<div data-roomid="<?php echo $chat_customer[$i]['room_id']; ?>" onclick="chatlistbox('<?php echo $chat_customer[$i]['room_id']; ?>')" class="chatlistbox <?php
			if ($chat_customer[$i]['room_id'] == $room_no) {
				echo 'active';
			}
			?>">
				<div class="avatar">
					<?php if ($chat_customer[$i]['image']) { ?>
						<img src="<?php echo base_url(); ?>uploads/profile_picture/<?php echo $chat_customer[$i]['image']; ?>" class="img-responsive" />
					<?php } else { ?>
						<div class="myprofilebackground" style="width:50px; height:50px;border-radius:50%; background: #0190ff;border: 3px solid #0190ff; text-align: center; font-size: 15px; color: #fff;"><p style="font-size: 24px;padding-top: 7px;">
								<?php echo $chat_customer[$i]['sort_name']; ?></p></div>
					<?php } ?>
				</div>
				<div class="message-text-wrapper topspacen">
					<div class="posttime">Dec 11</div>
					<p class="title"><?php echo $chat_customer[$i]['title']; ?></p>
					<p class="message graytext"><?php echo substr($chat_customer[$i]['last_message'],0,20); ?></p>
					<!--<p class="<?php echo $chat_customer[$i]['status_class']; ?> status"><?php echo $chat_customer[$i]['status']; ?></p>-->
					<div class="<?php echo strtolower($chat_customer[$i]['status']); ?>"><i class="fa fa-circle"></i></div>
				</div>
				<div class="clear"></div>
			</div>
            <?php
        }
         }

         public function imgupload() {
            if (!empty($_FILES)) {
            if ($_FILES['upload']['name'] != "") {
                    $config2 = array(
                        'upload_path' => './uploads/requests/',
                        'allowed_types' => "*",
                        'encrypt_name' => TRUE
                    );
                    $config2['preview_file'] = $_FILES['upload']['name'];

                    $_FILES['src_file']['name'] = $_FILES['upload']['name'];
                    $_FILES['src_file']['type'] = $_FILES['upload']['type'];
                    $_FILES['src_file']['tmp_name'] = $_FILES['upload']['tmp_name'];
                    $_FILES['src_file']['error'] = $_FILES['upload']['error'];
                    $_FILES['src_file']['size'] = $_FILES['upload']['size'];
                    $this->load->library('upload', $config2);

                    if ($this->upload->do_upload('upload')) {
                        $data = array($this->upload->data());
                        $data = $this->Welcome_model->insert_data("room_chat", array("room_id"=>$_POST['room_no'],"sender_id"=>$_SESSION['user_id'],"message"=>$data[0]['file_name'],"isimage"=>1,"created"=>date("Y:m:d H:i:s")));
                        echo $data;
                    } else {
                        echo '0';
                    }
                }
            } else {
                echo '0';
            }
         }

        public function designer_edit_profile(){
            $user_id = $_SESSION['user_id'];
            if(isset($_POST['submit'])){
//                echo "<pre/>";print_R($_POST);exit;
                $profileUpdate = array(
                    'first_name' => $this->input->post('first_name'), 
                    'last_name' => $this->input->post('last_name'), 
                    'email' => $this->input->post('email'), 
                    'paypal_email_address' => $this->input->post('paypal_id'), 
                    'phone' => $this->input->post('phone_number'), 
                    'address_line_1' => $this->input->post('address'), 
                    'city' => $this->input->post('city'), 
                    'state' => $this->input->post('state'), 
                    'zip' => $this->input->post('zip_code'), 
                    'timezone' => $this->input->post('timezone'),
                );
                $skillsadd = array(
                'hobbies' => $this->input->post('hobbies'),
                'about' => $this->input->post('about'),
              );
            $update_data_result = $this->Admin_model->designer_update_profile($profileUpdate, $user_id);
            $update_data_result =  $this->Admin_model->designer_update_hobbiesabout($skillsadd, $user_id);
            if ($update_data_result) {
                $this->session->set_userdata('timezone', $profileUpdate['timezone']);
                $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
            } else {
                $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
            }
            redirect(base_url() . "designer/request/designer_profile");
            }
        }
        public function designer_edit_profile_pic(){
            $user_id = $_SESSION['user_id'];
            $name = explode(" ", $_POST['first_last_name']);
           // echo "<pre>";print_r($name);
            if(isset($_POST['submit_pic'])){
                if ($_FILES['profile']['name'] != "") { 
//                    echo "<pre/>";print_R($_FILES);exit;
                    //if (!empty($_FILES)) {
                        $file = pathinfo($_FILES['profile']['name']);
                        $ext = strtolower(pathinfo($_FILES['profile']['name'], PATHINFO_EXTENSION));
                        $s3_file = $file['filename'].'-'.rand(1000,1).'.'.$ext;
                        if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_PROFILE . '_thumb')) {
                            $data = mkdir('./' . FS_UPLOAD_PUBLIC_UPLOADS_PROFILE . '_thumb', 0777, TRUE);
                        }
                        $config = array(
                            'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_PROFILE,
                            'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                            'file_name' => $s3_file
                        );
                        $check = $this->myfunctions->CustomFileUpload($config,'profile');
                        if($check['status'] == 1){
                            
                            /** auto resize profile **/
                            $thumb_tmp_path = FS_UPLOAD_PUBLIC_UPLOADS_PROFILE .'_thumb/';
                            $tmp = $_FILES['profile']['tmp_name'];
                            $this->myfunctions->make_thumb($tmp,$thumb_tmp_path . $s3_file,'', $ext,150);
                            if (UPLOAD_FILE_SERVER == 'bucket') {
                                $staticName = base_url() . $thumb_tmp_path . $s3_file;
                                //echo $staticName;exit;
                                $config = array(
                                    'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_PROFILE .'_thumb/',
                                    'file_name' => $s3_file
                                );
                                $this->load->library('s3_upload');
                                $this->s3_upload->initialize($config);
                                $uplod_thumb = $this->s3_upload->upload_multiple_file($staticName);
                                if ($uplod_thumb['status'] == 1) {
                                    $delete = $thumb_tmp_path . $s3_file;
                                    unlink($delete);
                                    unlink('./tmp/tmpfile/' . basename($staticName));

                                    rmdir($thumb_tmp_path);
                                }
                            }
                            $profile_pic = array(
                                'profile_picture' => $s3_file,
                            );
                            $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
                            if ($update_data_result) {
                                $this->session->set_flashdata('message_success', 'User Profile Picture Update Successfully.!', 5);
                            } else {
                                $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!', 5);
                            }
                        } else {
                            $this->session->set_flashdata('message_error', $check['msg'], 5);
                            //$data = array('error' => $this->upload->display_errors());
                            //$this->session->set_flashdata('message_error', $data, 5);
                        }
                    //}
                }
//                if(!empty($this->input->post())){
//                    $profile_pic = array(
//                        'first_name' => $name[0],  
//                        'last_name' => $name[1],
//                    );
//                    $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
//                    if ($update_data_result) {
//                        $this->session->set_flashdata('message_success', 'User Profile Picture Update Successfully.!', 5);
//                    } else {
//                        $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!', 5);
//                    }
//                }
            }
            redirect(base_url() . "designer/request/designer_profile");
        }
        
        public function addskills()
        {
            $user_id = $_SESSION['user_id'];
//            echo "<pre/>";print_R($_POST);exit;
            if(!empty($_POST)){
                $skills = array(
                    'brandin_logo' => $this->input->post('brandin_logo'), 
                    'ads_banner' => $this->input->post('ads_banner'), 
                    'email_marketing' => $this->input->post('email_marketing'), 
                    'webdesign' => $this->input->post('webdesign'), 
                    'appdesign' => $this->input->post('appdesign'), 
                    'graphics' => $this->input->post('graphics'), 
                    'illustration' => $this->input->post('illustration'), 
                    'user_id' => $user_id,
                    'created' =>  date("Y:m:d H:i:s"),
                );
                 $updateskills = array(
                    'brandin_logo' => $this->input->post('brandin_logo'), 
                    'ads_banner' => $this->input->post('ads_banner'), 
                    'email_marketing' => $this->input->post('email_marketing'), 
                    'webdesign' => $this->input->post('webdesign'), 
                    'appdesign' => $this->input->post('appdesign'), 
                    'graphics' => $this->input->post('graphics'), 
                    'illustration' => $this->input->post('illustration'), 
                    'user_id' => $user_id,
                    'modified' =>  date("Y:m:d H:i:s"),
                );
                $getskills = $this->Request_model->getallskills($user_id);
                if(empty($getskills)){
                    $success = $this->Welcome_model->insert_data("user_skills",  $skills);
                }else{
                    $upsuccess = $this->Admin_model->update_data('user_skills', $updateskills, array("user_id" => $user_id));
                }
                if ($success) {
                    $this->session->set_flashdata('message_success', 'Skills added successfully', 5);
                } else {
                    if ($upsuccess) {
                        $this->session->set_flashdata('message_success', 'Skills Update successfully', 5);
                    } else {
                        $this->session->set_flashdata('message_error', 'Skills not update successfully', 5);
                    }
                }
                
                redirect(base_url() . "designer/request/designer_profile#enableEmail");
            }
        }
        
        public function about_info(){
          $array = array();
          $login_user_id = $this->load->get_var('login_user_id');
          $hobbies = isset($_POST['hobbies'])?$_POST['hobbies']:'';
          $about = isset($_POST['about'])?$_POST['about']:'';
          if(!empty($_POST)){
              $array['hobbies'] = $hobbies;
              $array['about'] = $about;
            $getskills = $this->Request_model->getallskills($login_user_id);
              if(empty($getskills)){
                  $success = $this->Welcome_model->insert_data("user_skills",  $array);
              }else{
                 $success = $this->Admin_model->update_data('user_skills', $array, array("user_id" => $login_user_id));
              }
              if ($success) {
                $this->session->set_flashdata('message_success', 'About info updated successfully', 5);
              }
              redirect(base_url() . "designer/request/designer_profile");
          }
        }
     }
     