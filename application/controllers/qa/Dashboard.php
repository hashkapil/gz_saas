<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('Myfunctions');
        $this->load->library('session');
        $this->load->library('zip');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->model('Welcome_model');
        $this->load->model('Category_model');
        $this->load->model('Stripe');
        $this->load->model('Request_model');
        $this->load->model('Account_model');
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }

    public function checkloginuser() {
        if (!$this->session->userdata('user_id')) {
            redirect(base_url());
        }
        if ($this->session->userdata('role') != "qa") {
            redirect(base_url());
        }
    }

    public function logout() {
        $this->myfunctions->checkloginuser("qa");
        $this->Welcome_model->update_data("users", array("online" => 0), array("id" => $_SESSION['user_id']));
        $this->Request_model->logout();
        redirect(base_url());
    }

    public function index($buckettype = NULL) {
        $titlestatus = "dashboard";
        $this->myfunctions->checkloginuser("qa");
        $user_id = $_SESSION['user_id'];
        $allcustomer = array();
        $mycustomer = $this->Request_model->get_customer_list_for_qa_section($user_id);
        foreach ($mycustomer as $value) {
            array_push($allcustomer, $value['id']);
        }
        $data['incoming_request'] = array();
        $data['ongoing_request'] = array();
        $data['pending_request'] = array();
        $data['pending_review_request'] = array();
        $data['approved_request'] = array();
        $data['incoming_request_count'] = $this->Request_model->qa_load_more(array('active', 'disapprove'), $allcustomer, true, '', '', '', '', $buckettype);
        $data['incoming_request'] = $this->Request_model->qa_load_more(array('active', 'disapprove'), $allcustomer, false, '', '', '', '', $buckettype);
//        echo "sizeof".sizeof($data['incoming_request']);
//        echo "countdata".count($data['incoming_request']);
//        echo "count".$data['incoming_request_count'];
        for ($i = 0; $i < sizeof($data['incoming_request']); $i++) {
            $data['incoming_request'][$i]['category_data'] = $this->Request_model->getRequestCatbyid($data['incoming_request'][$i]['id'], $data['incoming_request'][$i]['category_id']);
            if (isset($data['incoming_request'][$i]['category_data'][0]['category']) && $data['incoming_request'][$i]['category_data'][0]['category'] != '') {
                $data['incoming_request'][$i]['category_name'] = $data['incoming_request'][$i]['category_data'][0]['category'];
            } else {
                $data['incoming_request'][$i]['category_name'] = $data['incoming_request'][$i]['category_data'][0]['cat_name'];
            }
            $data['incoming_request'][$i]['subcategory_name'] = $this->Request_model->getRequestsubCatbyid($data['incoming_request'][$i]['id'], $data['incoming_request'][$i]['category_id'], $data['incoming_request'][$i]['subcategory_id']);
            $data['incoming_request'][$i]['total_chat'] = $this->Request_model->get_chat_number($data['incoming_request'][$i]['id'], $data['incoming_request'][$i]['customer_id'], $data['incoming_request'][$i]['designer_id'], "qa");
            $data['incoming_request'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($data['incoming_request'][$i]['id'], $_SESSION['user_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($data['incoming_request'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {
                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            if ($data['incoming_request'][$i]['expected_date'] == '' || $data['incoming_request'][$i]['expected_date'] == NULL) {
                $data['incoming_request'][$i]['expected'] = $this->myfunctions->check_timezone($data['incoming_request'][$i]['latest_update'], $data['incoming_request'][$i]['current_plan_name']);
            } else {
                $data['incoming_request'][$i]['expected'] = $this->onlytimezone($data['incoming_request'][$i]['expected_date']);
            }
            $data['incoming_request'][$i]['comment_count'] = $commentcount;
            $data['incoming_request'][$i]['total_files'] = $this->Request_model->get_files_count($data['incoming_request'][$i]['id'], $data['incoming_request'][$i]['designer_id'], 'qa');
            $data['incoming_request'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($data['incoming_request'][$i]['id']);
            $data['incoming_request'][$i]['usertimezone_date'] = $data['incoming_request'][$i]['user_timezone'];
        }
        $data['ongoing_request_count'] = $this->Request_model->qa_load_more(array('assign', 'pending'), $allcustomer, true, '', '', '', '', $buckettype);
        $data['ongoing_request'] = $this->Request_model->qa_load_more(array('assign', 'pending'), $allcustomer, false, '', '', '', '', $buckettype);
        for ($i = 0; $i < sizeof($data['ongoing_request']); $i++) {
            $data['ongoing_request'][$i]['category_data'] = $this->Request_model->getRequestCatbyid($data['ongoing_request'][$i]['id'], $data['ongoing_request'][$i]['category_id']);
            if (isset($data['ongoing_request'][$i]['category_data'][0]['category']) && $data['ongoing_request'][$i]['category_data'][0]['category'] != '') {
                $data['ongoing_request'][$i]['category_name'] = $data['ongoing_request'][$i]['category_data'][0]['category'];
            } else {
                $data['ongoing_request'][$i]['category_name'] = $data['ongoing_request'][$i]['category_data'][0]['cat_name'];
            }
            $data['ongoing_request'][$i]['subcategory_name'] = $this->Request_model->getRequestsubCatbyid($data['ongoing_request'][$i]['id'], $data['ongoing_request'][$i]['category_id'], $data['ongoing_request'][$i]['subcategory_id']);
            $data['ongoing_request'][$i]['total_chat'] = $this->Request_model->get_chat_number($data['ongoing_request'][$i]['id'], $data['ongoing_request'][$i]['customer_id'], $data['ongoing_request'][$i]['designer_id'], "qa");
            $data['ongoing_request'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($data['ongoing_request'][$i]['id'], $_SESSION['user_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($data['ongoing_request'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $data['ongoing_request'][$i]['comment_count'] = $commentcount;
            $data['ongoing_request'][$i]['total_files'] = $this->Request_model->get_files_count($data['ongoing_request'][$i]['id'], $data['ongoing_request'][$i]['designer_id'], 'qa');
            $data['ongoing_request'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($data['ongoing_request'][$i]['id']);
        }

        $data['pending_review_request_count'] = $this->Request_model->qa_load_more(array('pendingrevision'), $allcustomer, true, '', '', '', '', $buckettype);
        $data['pending_review_request'] = $this->Request_model->qa_load_more(array('pendingrevision'), $allcustomer, false, '', '', '', '', $buckettype);
        for ($i = 0; $i < sizeof($data['pending_review_request']); $i++) {
            $data['pending_review_request'][$i]['category_data'] = $this->Request_model->getRequestCatbyid($data['pending_review_request'][$i]['id'], $data['pending_review_request'][$i]['category_id']);
            if (isset($data['pending_review_request'][$i]['category_data'][0]['category']) && $data['pending_review_request'][$i]['category_data'][0]['category'] != '') {
                $data['pending_review_request'][$i]['category_name'] = $data['pending_review_request'][$i]['category_data'][0]['category'];
            } else {
                $data['pending_review_request'][$i]['category_name'] = $data['pending_review_request'][$i]['category_data'][0]['cat_name'];
            }
            $data['pending_review_request'][$i]['subcategory_name'] = $this->Request_model->getRequestsubCatbyid($data['pending_review_request'][$i]['id'], $data['pending_review_request'][$i]['category_id'], $data['pending_review_request'][$i]['subcategory_id']);
            $data['pending_review_request'][$i]['total_chat'] = $this->Request_model->get_chat_number($data['pending_review_request'][$i]['id'], $data['pending_review_request'][$i]['customer_id'], $data['pending_review_request'][$i]['designer_id'], "qa");
            $data['pending_review_request'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($data['pending_review_request'][$i]['id'], $_SESSION['user_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($data['pending_review_request'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {
                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $data['pending_review_request'][$i]['revisiondate'] = $this->onlytimezone($data['pending_review_request'][$i]['modified']);
            $data['pending_review_request'][$i]['comment_count'] = $commentcount;
            $data['pending_review_request'][$i]['total_files'] = $this->Request_model->get_files_count($data['pending_review_request'][$i]['id'], $data['pending_review_request'][$i]['designer_id'], 'qa');
            $data['pending_review_request'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($data['pending_review_request'][$i]['id']);
        }

        $data['pending_request_count'] = $this->Request_model->qa_load_more(array('checkforapprove'), $allcustomer, true, '', '', '', '', $buckettype);
        $data['pending_request'] = $this->Request_model->qa_load_more(array('checkforapprove'), $allcustomer, false, '', '', '', '', $buckettype);
        for ($i = 0; $i < sizeof($data['pending_request']); $i++) {
            $data['pending_request'][$i]['category_data'] = $this->Request_model->getRequestCatbyid($data['pending_request'][$i]['id'], $data['pending_request'][$i]['category_id']);
            if (isset($data['pending_request'][$i]['category_data'][0]['category']) && $data['pending_request'][$i]['category_data'][0]['category'] != '') {
                $data['pending_request'][$i]['category_name'] = $data['pending_request'][$i]['category_data'][0]['category'];
            } else {
                $data['pending_request'][$i]['category_name'] = $data['pending_request'][$i]['category_data'][0]['cat_name'];
            }
            $data['pending_request'][$i]['subcategory_name'] = $this->Request_model->getRequestsubCatbyid($data['pending_request'][$i]['id'], $data['pending_request'][$i]['category_id'], $data['pending_request'][$i]['subcategory_id']);
            $data['pending_request'][$i]['total_chat'] = $this->Request_model->get_chat_number($data['pending_request'][$i]['id'], $data['pending_request'][$i]['customer_id'], $data['pending_request'][$i]['designer_id'], "qa");
            $data['pending_request'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($data['pending_request'][$i]['id'], $_SESSION['user_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($data['pending_request'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $data['pending_request'][$i]['reviewdate'] = $this->onlytimezone($data['pending_request'][$i]['modified']);
            $data['pending_request'][$i]['comment_count'] = $commentcount;
            $data['pending_request'][$i]['total_files'] = $this->Request_model->get_files_count($data['pending_request'][$i]['id'], $data['pending_request'][$i]['designer_id'], 'qa');
            $data['pending_request'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($data['pending_request'][$i]['id']);
        }

        $data['approved_request_count'] = $this->Request_model->qa_load_more(array('approved'), $allcustomer, true, '', '', '', '', $buckettype);
        $data['approved_request'] = $this->Request_model->qa_load_more(array('approved'), $allcustomer, false, '', '', '', '', $buckettype);
        for ($i = 0; $i < sizeof($data['approved_request']); $i++) {
            $data['approved_request'][$i]['category_data'] = $this->Request_model->getRequestCatbyid($data['approved_request'][$i]['id'], $data['approved_request'][$i]['category_id']);
            if (isset($data['approved_request'][$i]['category_data'][0]['category']) && $data['approved_request'][$i]['category_data'][0]['category'] != '') {
                $data['approved_request'][$i]['category_name'] = $data['approved_request'][$i]['category_data'][0]['category'];
            } else {
                $data['approved_request'][$i]['category_name'] = $data['approved_request'][$i]['category_data'][0]['cat_name'];
            }
            $data['approved_request'][$i]['subcategory_name'] = $this->Request_model->getRequestsubCatbyid($data['approved_request'][$i]['id'], $data['approved_request'][$i]['category_id'], $data['approved_request'][$i]['subcategory_id']);
            $data['approved_request'][$i]['total_chat'] = $this->Request_model->get_chat_number($data['approved_request'][$i]['id'], $data['approved_request'][$i]['customer_id'], $data['approved_request'][$i]['designer_id'], "qa");
            $data['approved_request'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($data['approved_request'][$i]['id'], $_SESSION['user_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($data['approved_request'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $data['approved_request'][$i]['approvedate'] = $this->onlytimezone($data['approved_request'][$i]['approvaldate']);
            $data['approved_request'][$i]['comment_count'] = $commentcount;
            $data['approved_request'][$i]['total_files'] = $this->Request_model->get_files_count($data['approved_request'][$i]['id'], $data['approved_request'][$i]['designer_id'], 'qa');
            $data['approved_request'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($data['approved_request'][$i]['id']);
        }

        $data['hold_request_count'] = $this->Request_model->qa_load_more(array('hold'), $allcustomer, true, '', '', '', '', $buckettype);
        $data['hold_request'] = $this->Request_model->qa_load_more(array('hold'), $allcustomer, false, '', '', '', '', $buckettype);
//                echo "<pre/>";print_r($data['hold_request']);

        for ($i = 0; $i < sizeof($data['hold_request']); $i++) {
            $data['hold_request'][$i]['total_chat'] = $this->Request_model->get_chat_number($data['hold_request'][$i]['id'], $data['hold_request'][$i]['customer_id'], $data['approved_request'][$i]['designer_id'], "qa");
            $data['hold_request'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($data['hold_request'][$i]['id'], $_SESSION['user_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($data['hold_request'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $data['hold_request'][$i]['modified'] = $this->onlytimezone($data['hold_request'][$i]['modified']);
            $data['hold_request'][$i]['comment_count'] = $commentcount;
            $data['hold_request'][$i]['total_files'] = $this->Request_model->get_files_count($data['hold_request'][$i]['id'], $data['hold_request'][$i]['designer_id'], 'qa');
            $data['hold_request'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($data['hold_request'][$i]['id']);
        }
        
        $data['cancel_request_count'] = $this->Request_model->qa_load_more(array('cancel'), $allcustomer, true, '', '', '', '', $buckettype);
        $data['cancel_request'] = $this->Request_model->qa_load_more(array('cancel'), $allcustomer, false, '', '', '', '', $buckettype);
        for ($i = 0; $i < sizeof($data['cancel_request']); $i++) {
            $data['cancel_request'][$i]['total_chat'] = $this->Request_model->get_chat_number($data['cancel_request'][$i]['id'], $data['cancel_request'][$i]['customer_id'], $data['cancel_request'][$i]['designer_id'], "qa");
            $data['cancel_request'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($data['cancel_request'][$i]['id'], $user_id, "qa");
            $getfileid = $this->Request_model->get_attachment_files($data['hold_request'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $data['cancel_request'][$i]['modified'] = $this->onlytimezone($data['cancel_request'][$i]['modified']);
            $data['cancel_request'][$i]['comment_count'] = $commentcount;
            $data['cancel_request'][$i]['total_files'] = $this->Request_model->get_files_count($data['cancel_request'][$i]['id'], $data['cancel_request'][$i]['designer_id'], 'qa');
            $data['cancel_request'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($data['cancel_request'][$i]['id']);
        }

        $data["all_designers"] = $this->Request_model->get_all_designers();
        for ($i = 0; $i < sizeof($data['all_designers']); $i++) {
            $user = $this->Account_model->get_all_active_request_by_designer($data['all_designers'][$i]['id']);
            $data['all_designers'][$i]['active_request'] = sizeof($user);
        }


        $profile_data = $this->Admin_model->getuser_data($user_id);
        $data['bucket_type'] = $buckettype;
        $data['designer_file'] = [];
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('qa/qa_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "titlestatus" => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view('qa/index', $data);
        $this->load->view('qa/qa_footer');
    }

    public function view_project($id = null) {
        $titlestatus = "view_clients";
        $this->myfunctions->checkloginuser("qa");
        if ($id == "") {
            redirect(base_url() . "qa/dashboard");
        }
        $url = "qa/dashboard/view_project/" . $id;
        $login_user_id = $this->load->get_var('login_user_id');
        $admin_id = $this->load->get_var('admin_id');
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url));
        $this->Welcome_model->update_data("message_notification", array("shown" => 1), array("user_id" => $_SESSION['user_id'], "url" => $url));
        $this->Admin_model->update_data("request_discussions", array("qa_seen" => 1), array("request_id" => $id));
        $request = $this->Request_model->get_request_by_id($id);
        $cat_data = $this->Category_model->get_category_byID($request[0]['category_id']);
        $cat_name = $cat_data[0]['name'];
        $subcat_data = $this->Category_model->get_category_byID($request[0]['subcategory_id']);
        $subcat_name = $subcat_data[0]['name'];
        $request['cat_name'] = $cat_name;
        $request['subcat_name'] = $subcat_name;
        $is_approveDraftemailbyadmin = $this->Admin_model->approveDraftemailbyadmin();
        $is_approveDraftemailbyuser = $this->Admin_model->approveDraftemailbyuser($request[0]['customer_id']);
        $branddata = $this->Request_model->get_brandprofile_by_id($request[0]['brand_id']);
        $brand_materials_files = $this->Request_model->get_brandprofile_materials_files($branddata[0]['id']);
        for ($i = 0; $i < sizeof($brand_materials_files); $i++) {
            if ($brand_materials_files[$i]['file_type'] == 'logo_upload') {
                $brand_materials_files['latest_logo'] = $brand_materials_files[$i]['filename'];
                break;
            }
        }

        $request_id = $request[0]['id'];

        $request_files = $this->Admin_model->get_requested_files($request_id, $request[0]['customer_id'], "customer");
        for ($i = 0; $i < count($request[0]['designer_attachment']); $i++) {
            $request[0]['designer_attachment'][$i]['img_created'] = $this->onlytimezone($request[0]['designer_attachment'][$i]['created']);
            $request[0]['designer_attachment'][$i]['created'] = date('d M, Y', strtotime($request[0]['designer_attachment'][$i]['img_created']));
        }
        $admin_request_files = $this->Admin_model->get_requested_files($request_id, "", "designer");

        $customer_additional_files = $this->Admin_model->get_requested_files($request_id, "", "customer");

        $admin_files = $this->Admin_model->get_requested_files($request_id, "", "admin");

        $designer_list = $this->Admin_model->get_total_customer("designer");
        $designer_preview_file = $this->Admin_model->get_requested_files($id, "", "designer", "1");
        $designer_source_file = $this->Admin_model->get_requested_files($id, "", "designer");

        $customer = $this->Account_model->getuserbyid($request[0]['customer_id']);
        if (!empty($customer)) {
            $request[0]['customer_name'] = $customer[0]['first_name'] . " " . $customer[0]['last_name'];
        }

        $designer = $this->Account_model->getuserbyid($request[0]['designer_id']);
        if (!empty($designer)) {
            $request[0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
            $request[0]['designer_image'] = $designer[0]['profile_picture'];
            $request[0]['designer_sortname'] = substr($designer[0]['first_name'], 0, 1) . substr($designer[0]['last_name'], 0, 1);
        } else {
            $request[0]['designer_name'] = "";
            $request[0]['designer_image'] = "";
            $request[0]['designer_sortname'] = "";
        }

        $files = $this->Request_model->get_attachment_files($id, "designer");
        $file_chat_array = array();
        for ($i = 0; $i < sizeof($files); $i++) {
            $chat_of_file = $this->Request_model->get_file_chat($files[$i]['id'], "qa");
            if ($chat_of_file) {
                $file_chat_array[$i]['file_name'] = $files[$i]['file_name'];
                $file_chat_array[$i]['count'] = $chat_of_file;
                $file_chat_array[$i]['id'] = $files[$i]['id'];
                $files[$i]['chat_count'] = $chat_of_file;
            } else {
                $files[$i]['chat_count'] = "";
                $file_chat_array[$i]['id'] = "";
            }
        }

        $chat_request = $this->Request_model->get_chat_request_by_id($id);
        $timediff = '';
        for ($j = 0; $j < count($chat_request); $j++) {

            $chat_request[$j]['msg_created'] = $this->onlytimezone($chat_request[$j]['created']);
            $chat_request[$j]['chat_created_date'] = $chat_request[$j]['msg_created'];
        }
        $chat_request_without_customer = $this->Request_model->get_chat_request_by_id_without_customer($id);
        $timediff_nocust = '';
        for ($j = 0; $j < count($chat_request_without_customer); $j++) {

            $chat_request_without_customer[$j]['msg_created'] = $this->onlytimezone($chat_request_without_customer[$j]['created']);
            $chat_request_without_customer[$j]['chat_created_date'] = $chat_request_without_customer[$j]['msg_created'];
        }
        if (!empty($_POST)) {

            if (isset($_POST['designer_name']) && $_POST['designer_name'] != "") {

                if ($_POST['designer_name'] != $request[0]['designer_id']) {

                    $status = $this->Request_model->new_designer_get_status($_POST['designer_name'], $request[0]['customer_id']);

                    $this->Welcome_model->update_data("requests", array("status" => $_POST['status'], "status_admin" => $_POST['status'], "status_designer" => $_POST['status']), array("id" => $request[0]['id']));

                    $request2 = $this->Admin_model->get_all_requested_designs(array("pending", "assign", "active"), $request[0]['customer_id']);

                    $success = $this->Welcome_model->update_data("requests", array("designer_id" => $_POST['designer_name']), array("id" => $request2[0]['id']));
                    if ($success) {
                        $status = $this->Request_model->new_designer_get_status($_POST['designer_name'], $request[0]['customer_id']);
                        if ($status == "active") {
                            $this->Welcome_model->update_data("requests", array("status" => "active", "status_admin" => "active", "status_qa" => "active", "status_designer" => "active", "dateinprogress" => date("Y-m-d H:i:s")), array("id" => $request2[0]['id']));
                            $this->Welcome_model->update_data("requests", array("status" => "assign", "status_admin" => "assign", "status_qa" => "assign", "status_designer" => "assign", "designer_id" => $_POST['designer_name']), array("id!=" => $request2[0]['id'], "status" => "pending", "customer_id" => $request[0]['customer_id']));
                            $this->Welcome_model->update_data("requests", array("status" => "assign", "status_admin" => "assign", "status_qa" => "assign", "status_designer" => "assign", "designer_id" => $_POST['designer_name']), array("id!=" => $request2[0]['id'], "status" => "assign", "customer_id" => $request[0]['customer_id']));
                        } else {
                            $this->Welcome_model->update_data("requests", array("status" => "assign", "status_admin" => "assign", "status_qa" => "assign", "status_designer" => "assign", "designer_id" => $_POST['designer_name']), array("status" => "pending", "customer_id" => $request[0]['customer_id']));
                            $this->Welcome_model->update_data("requests", array("status" => "assign", "status_admin" => "assign", "status_designer" => "assign", "status_qa" => "assign", "designer_id" => $_POST['designer_name']), array("status" => "assign", "customer_id" => $request[0]['customer_id']));
                        }
                        $this->session->set_flashdata('message_success', 'Request Updated Successfully.!', 5);

                        redirect(base_url() . "qa/dashboard/view_project/" . $id . '/' . $_POST['tab_status']);
                    } else {
                        $this->session->set_flashdata('message_error', 'Request Updated Successfully.!', 5);

                        redirect(base_url() . "qa/dashboard/view_project/" . $id . '/' . $_POST['tab_status']);
                    }
                } else {
                    if ($this->Welcome_model->update_data("requests", array("status" => $_POST['status'], "status_admin" => $_POST['status'], "status_qa" => $_POST['status'], "status_designer" => $_POST['status']), array("id" => $request[0]['id']))) {

                        $this->session->set_flashdata('message_success', 'Request Updated Successfully.!', 5);
                        redirect(base_url() . "qa/dashboard/view_project/" . $id . '/' . $_POST['tab_status']);
                    } else {

                        $this->session->set_flashdata('message_error', 'Request Not Updated Successfully.!', 5);
                        redirect(base_url() . "qa/dashboard/view_project/" . $id . '/' . $_POST['tab_status']);
                    }
                }
            }
        }

        if (!empty($_FILES)) {
            $config['image_library'] = 'gd2';
            if ($_FILES['src_file']['name'] != "" && $_FILES['preview_file']['name'] != "") {
                if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id)) {
                    $data = mkdir('./' . FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id, 0777, TRUE);
                }if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb')) {
                    $data = mkdir('./' . FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb', 0777, TRUE);
                }
                $this->load->library('image_lib', $config);
                $requestforfile = $this->Request_model->get_request_by_id($id);
                $file_name_title = substr($requestforfile[0]['title'], 0, 15);
                $file_name_title = str_replace(" ", "_", $file_name_title);
                $file_name_title = preg_replace('/[^A-Za-z0-9\_]/', '', $file_name_title);
                $six_digit_random_number = mt_rand(100, 999);
                $ext = strtolower(pathinfo($_FILES['src_file']['name'], PATHINFO_EXTENSION));
                $ext1 = strtolower(pathinfo($_FILES['preview_file']['name'], PATHINFO_EXTENSION));
                $sourcename = $file_name_title . '_' . $six_digit_random_number . '.' . $ext;
                $previewname = $file_name_title . '_' . $six_digit_random_number . '1.' . $ext1;

                if (!empty($_FILES)) {
                    $config = array(
                        'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/',
                        'allowed_types' => '*',
                        'file_name' => $sourcename,
                    );
                    $config2 = array(
                        'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/',
                        'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                        'max_size' => '5000000',
                        'file_name' => $previewname,
                    );

                    if (($_FILES['preview_file']['size'] >= $config2['max_size']) || ($_FILES['preview_file']['size'] == 0)) {
                        $this->session->set_flashdata('message_error', "File too large. File must be less than 5 MB.", 5);
                        redirect(base_url() . "qa/dashboard/view_project/" . $id . '/' . $_POST['tab_status']);
                    }
                    if (in_array($ext1, $config2['allowed_types'])) {

                        $check_src_file = $this->myfunctions->CustomFileUpload($config, 'src_file');

                        $src_data = array();
                        $src_data[0]['file_name'] = "";
                        if ($check_src_file['status'] == 1) {
                            
                        } else {
                            //$data = array('error' => $this->upload->display_errors());

                            $this->session->set_flashdata('message_error', $check_src_file['msg'], 5);
                        }


                        // $config2['preview_file'] = $_FILES['preview_file']['name'];

                        $_FILES['src_file']['name'] = $file_name_title . '_' . $six_digit_random_number . '.' . $ext1;
                        $_FILES['src_file']['type'] = $_FILES['preview_file']['type'];
                        $_FILES['src_file']['tmp_name'] = $_FILES['preview_file']['tmp_name'];
                        $_FILES['src_file']['error'] = $_FILES['preview_file']['error'];
                        $_FILES['src_file']['size'] = $_FILES['preview_file']['size'];

                        $check_preview_file = $this->myfunctions->CustomFileUpload($config2, 'preview_file');

                        if ($check_preview_file['status'] == 1) {
                            //echo $check_src_file['msg'];exit;

                            $thumb_tmp_path = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb/';
                            $tmp = $_FILES['preview_file']['tmp_name'];
                            $thumb = $this->myfunctions->make_thumb($tmp, $thumb_tmp_path . $previewname, 600, $ext1);
                            if (UPLOAD_FILE_SERVER == 'bucket') {
                                $staticName = base_url() . $thumb_tmp_path . '/' . $previewname;
                                //echo $staticName;exit;
                                $config = array(
                                    'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS . $id . '/_thumb/',
                                    'file_name' => $previewname
                                );
                                $this->load->library('s3_upload');
                                $this->s3_upload->initialize($config);
                                //$this->s3_upload->upload_multiple_file($staticName);
                                $uplod_thumb = $this->s3_upload->upload_multiple_file($staticName);
                                if ($uplod_thumb['status'] == 1) {
                                    $delete = $thumb_tmp_path . $previewname;
                                    unlink($delete);

                                    unlink('./tmp/tmpfile/' . basename($staticName));

                                    rmdir($thumb_tmp_path);
                                    rmdir($thumb_dummy_path);
                                }
                            }
                            $request_files_data = array("request_id" => $id,
                                "user_id" => $_SESSION['user_id'],
                                "user_type" => "designer",
                                // "file_name" => $file_name_title . '_' . $six_digit_random_number . '.' . $ext1,
                                "file_name" => $previewname,
                                "preview" => 1,
                                "qa_seen" => 1,
                                "created" => date("Y-m-d H:i:s"),
                                "modified" => date("Y-m-d H:i:s"),
                                "status" => "customerreview",
                                "src_file" => $sourcename);
                            $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                            $this->myfunctions->send_email_after_admin_approval($request[0]['title'], $request[0]['id'], $customer[0]['email'], $data);
                            //send mail to sub user after qa approve
                            $send_emailto_subusers = $this->myfunctions->send_emails_to_sub_user($request[0]['customer_id'], 'approve/revision_requests', $is_approveDraftemailbyadmin, $request[0]['id']);
                            $subdomain_url = $this->myfunctions->dynamic_urlforsubuser_inemail($request[0]['customer_id'], $customer[0]['plan_name']);
                            if (!empty($send_emailto_subusers)) {
                                foreach ($send_emailto_subusers as $send_emailto_subuser) {
                                    $this->myfunctions->send_email_after_admin_approval($request[0]['title'], $request[0]['id'], $send_emailto_subuser['email'], $data, $subdomain_url);
                                }
                            }

                            $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status" => "checkforapprove", "status_qa" => "checkforapprove", "status_designer" => "checkforapprove", "status_admin" => "checkforapprove", "modified" => date("Y-m-d H:i:s")), array("id" => $id));
                          //  $request2 = $this->Admin_model->get_all_requested_designs(array('active'), $request[0]['customer_id']);
                           // $request3 = $this->Admin_model->get_all_requested_designs(array('checkforapprove', 'disapprove'), $request[0]['customer_id']);
                            $ActiveReq = $this->myfunctions->CheckActiveRequest($request[0]['customer_id']);
                            if ($ActiveReq == 1) {
                                $this->myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],"","view_project");

                            }
                             /**capture upload draft event**/
                            $this->myfunctions->capture_project_activity($id,$data,$request[0]['designer_id'],'qa_upload_draft','view_request',$login_user_id,'0','1','1');
                            $this->myfunctions->capture_project_activity($id,$data,$request[0]['designer_id'],$request[0]['status'].'_to_checkforapprove','view_request',$login_user_id,'1','1','0');
                            /* save notifications when upload draft design */
                            $this->myfunctions->show_notifications($id, $login_user_id, "Upload New File For \"" . $request[0]['title'] . "\" . Please Check And Approve..!", "customer/request/project_info/" . $id, $request[0]['customer_id']);
                            $this->myfunctions->show_notifications($id, $login_user_id, "Approved your design For \"" . $request[0]['title'] . "\"", "designer/request/project_info/" . $id, $request[0]['designer_id']);
                            /** end save notifications when upload draft design * */
                            $this->session->set_flashdata('message_success', "File is Uploaded Successfully.!", 5);
                            redirect(base_url() . "qa/dashboard/view_project/" . $id . '/' . $_POST['tab_status']);
                        } else {
                            //$data = array('error' => $this->upload->display_errors());

                            $this->session->set_flashdata('message_error', $check_preview_file['msg'], 5);
                        }
                    } else {
                        $this->session->set_flashdata('message_error', "Please upload valid files", 5);
                        redirect(base_url() . "qa/dashboard/view_project/" . $id . '/' . $_POST['tab_status']);
                    }
                }
            } else {
                $this->session->set_flashdata('message_error', "Please Upload Preview or Source Both Files.!", 5);
                redirect(base_url() . "qa/dashboard/view_project/" . $id . '/' . $_POST['tab_status']);
            }
        }
        if (isset($_POST['adddesingerbtn'])) {
            $designer_assigned = $this->Admin_model->assign_designer_by_qa($_POST['assign_designer'], $_POST['request_id']);
            redirect(base_url() . "qa/dashboard/view_project/" . $_POST['request_id'] . '/' . $_POST['tab_status']);
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $designer_list = $this->Request_model->get_designer_list_for_qa("array");
        $alldesigners = $this->Request_model->get_all_designer_for_qa($_SESSION['user_id']);
        for ($i = 0; $i < sizeof($alldesigners); $i++) {

            $user = $this->Account_model->get_all_active_request_by_designer($alldesigners[$i]['id']);
            $alldesigners[$i]['active_request'] = sizeof($user);
        }
        $this->load->view('qa/qa_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, 'titlestatus' => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view('qa/view_project', array("data" => $request, "branddata" => $branddata, "brand_materials_files" => $brand_materials_files, "request_files" => $request_files, "admin_request_files" => $admin_request_files, "designer_list" => $designer_list, "customer_additional_files" => $customer_additional_files, "admin_files" => $admin_files, "designer_preview_file" => $designer_preview_file, "designer_source_file" => $designer_source_file, 'request_id' => $id, "chat_request_without_customer" => $chat_request_without_customer, 'chat_request' => $chat_request, "file_chat_array" => $file_chat_array, 'alldesigners' => $alldesigners));
        $this->load->view('qa/qa_footer');
    }

    public function view_files($id = null) {
        $titlestatus = "dashboard";
        $this->myfunctions->checkloginuser("qa");
        if ($id == "") {
            redirect(base_url() . "qa/dashboard");
        }
        $login_user_id = $this->load->get_var('login_user_id');
        $this->Welcome_model->update_data("request_files", array("qa_seen" => 1), array('id' => $id));
        $url = "qa/dashboard/view_files/" . $id . "?id=" . $_GET['id'];
        $this->Admin_model->update_data("request_file_chat", array("qa_seen" => '1'), array("request_file_id" => $id));
        $this->Welcome_model->update_data("message_notification", array("shown" => 1), array("user_id" => $login_user_id, "url" => $url));
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url));

        if (!empty($_POST)) {
            $success = $this->Welcome_model->update_data("request_files", array("grade" => $_POST['grade']), array('id' => $_POST['id']));
            if ($success) {
                $this->session->set_flashdata('message_success', 'Grade is Submitted Successfully.!', 5);

                redirect(base_url() . "qa/dashboard/view_files/" . $id . "?id=" . $_GET['id']);
            } else {
                $this->session->set_flashdata('message_error', 'Grade is Not Submitted Successfully.!', 5);

                redirect(base_url() . "qa/dashboard/view_files/" . $id . "?id=" . $_GET['id']);
            }
        }

        $designer_file = $this->Admin_model->get_requested_files($_GET['id'], "", "designer", "1");
//         echo "<pre>";
//         print_r($designer_file);
        //exit;
        $data['request'] = $this->Request_model->get_request_by_id($_GET['id']);
        $data['main_id'] = $id;

        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id'], 'DESC');

            $designer_file[$i]['modified_date'] = $this->onlytimezone($designer_file[$i]['modified']);
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified_date'];

            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
             $designer_file[$i]['chat'][$j]['replies'] = $this->Request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
                $designer_file[$i]['chat'][$j]['created'] = $this->onlytimezone($designer_file[$i]['chat'][$j]['created']);
            }
            // echo "</pre>";print_r($designer_file[$i]['chat']);
        }
        $dataa = $this->Request_model->get_request_by_id($_GET['id']);
        $designer = $this->Account_model->getuserbyid($dataa[0]['designer_id']);
        $customer = $this->Account_model->getuserbyid($dataa[0]['customer_id']);
        $data['user'] = $this->Account_model->getuserbyid($_SESSION['user_id']);
        if (!empty($designer)) {
            $data['data'][0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
            $data['data'][0]['designer_image'] = $designer[0]['profile_picture'];
            $data['data'][0]['title'] = $dataa[0]['title'];
            $data['data'][0]['designer_sortname'] = substr($designer[0]['first_name'], 0, 1) . substr($designer[0]['last_name'], 0, 1);
        }
        if (!empty($customer)) {
            $data['data'][0]['customer_name'] = $customer[0]['first_name'] . " " . $designer[0]['last_name'];
            $data['data'][0]['customer_image'] = $customer[0]['profile_picture'];
            $data['data'][0]['customer_sortname'] = substr($customer[0]['first_name'], 0, 1) . substr($customer[0]['last_name'], 0, 1);
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $data['chat_request'] = $this->Request_model->get_chat_request_by_id($_GET['id']);

        $data['designer_file'] = $designer_file;
        $this->load->view('qa/qa_header', array("edit_profile" => $profile_data, 'titlestatus' => $titlestatus));
        $this->load->view('qa/view_files', $data);
        $this->load->view('qa/qa_footer');
    }

    public function view_clients() {
        //die('fklgjfh');
        $titlestatus = "view_clients";
        $this->myfunctions->checkloginuser("qa");
        $data = array();
        $mydesigner = $this->Request_model->get_designer_list_for_qa();
        // echo "<pre>";print_R($mydesigner);exit;
        if ($mydesigner) {
            $countcustomer = $this->Request_model->get_customer_list_for_qa_count("array", $mydesigner);
            $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
            for ($i = 0; $i < sizeof($data); $i++) {
                $user = $this->Account_model->get_all_request_by_customer($data[$i]['id']);
                $active = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('active', 'disapprove'), $data[$i]['id']);
                $inque = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('assign'), $data[$i]['id']);
                $revision = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('disapprove'), $data[$i]['id']);
                $review = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('checkforapprove'), $data[$i]['id']);
                $complete = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('approved'), $data[$i]['id']);
                //echo $complete;exit;
                // $user = $this->Account_model->get_all_active_view_request_by_customer_for_qa($activeClients[$i]['id']);
                $data[$i]['created'] = $this->onlytimezone($trailClients[$i]['created']);
                $data[$i]['active'] = $active;
                $data[$i]['inque_request'] = $inque;
                $data[$i]['revision_request'] = $revision;
                $data[$i]['review_request'] = $review;
                $data[$i]['complete_request'] = $complete;
                $data[$i]['no_of_request'] = sizeof($user);
                $user = $this->Account_model->get_all_active_request_by_customer($data[$i]['id']);
                $data[$i]['active_request'] = sizeof($user);

                $data[$i]['designer_name'] = "";
                if ($data[$i]['designer_id'] != 0) {
                    $data[$i]['designer'] = $this->Request_model->get_user_by_id($data[$i]['designer_id']);
                } else {
                    $data[$i]['designer'] = "";
                }

                $designer_name = $this->Account_model->getuserbyid($data[$i]['designer_id']);
                if (!empty($designer_name)) {
                    $data[$i]['designer_name'] = $designer_name[0]['first_name'];
                }
            }
        }

        for ($i = 0; $i < sizeof($data); $i++) {
            $data[$i]['total_rating'] = $this->Request_model->get_customer_average_rating_qa_dashboard($data[$i]['id']);
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $all_designers = $this->Request_model->get_all_designers();
        for ($i = 0; $i < sizeof($all_designers); $i++) {

            $user = $this->Account_model->get_all_active_request_by_designer($all_designers[$i]['id']);
            $all_designers[$i]['active_request'] = sizeof($user);
        }
        $this->load->view('qa/qa_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, 'titlestatus' => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view('qa/view_clients', array("data" => $data, 'all_designers' => $all_designers, 'countcustomer' => $countcustomer));
        $this->load->view('qa/qa_footer');
    }

    public function client_projects($id = null) {
        $titlestatus = "view_clients";
        $this->myfunctions->checkloginuser("qa");
        if ($id == "") {
            redirect(base_url() . "qa/dashboard");
        }
        $data['userdata'] = $this->Account_model->getuserbyid($id);

        if (empty($data['userdata'])) {
            redirect(base_url() . "qa/dashboard");
        }
        $active_project = $this->Account_model->get_all_active_request_by_customer($id);

        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['priority'] = $active_project[$i]['priority'];
            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($id, $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "qa");
            $active_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($active_project[$i]['id'], $_SESSION['user_id'], "qa");
            $active_project[$i]['total_files'] = $this->Request_model->get_files_count($active_project[$i]['id'], $active_project[$i]['designer_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($active_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
                if ($active_project[$i]['expected_date'] == '' || $active_project[$i]['expected_date'] == NULL) {
                    $active_project[$i]['expected'] = $this->myfunctions->check_timezone($active_project[$i]['latest_update'], $active_project[$i]['current_plan_name']);
                } else {
                    $active_project[$i]['expected'] = $this->onlytimezone($active_project[$i]['expected_date']);
                }
            }
            $active_project[$i]['comment_count'] = $commentcount;
            $active_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($active_project[$i]['id']);
            $designer = $this->Request_model->getuserbyid($active_project[$i]['designer_id']);
            $customer = $this->Request_model->getuserbyid($active_project[$i]['customer_id']);
            if (!empty($designer)) {
                $active_project[$i]['designer_first_name'] = $designer[0]['first_name'];
                $active_project[$i]['designer_last_name'] = $designer[0]['last_name'];
                $active_project[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $active_project[$i]['designer_first_name'] = "";
                $active_project[$i]['designer_last_name'] = "";
                $active_project[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $active_project[$i]['customer_first_name'] = $customer[0]['first_name'];
                $active_project[$i]['customer_last_name'] = $customer[0]['last_name'];
                $active_project[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $active_project[$i]['customer_first_name'] = "";
                $active_project[$i]['customer_last_name'] = "";
                $active_project[$i]['plan_turn_around_days'] = 0;
            }
        }

        $check_approve_project = $this->Request_model->get_request_list_for_qa($id, array('checkforapprove'));

        for ($i = 0; $i < sizeof($check_approve_project); $i++) {
            $check_approve_project[$i]['total_chat'] = $this->Request_model->get_chat_number($id, $check_approve_project[$i]['customer_id'], $check_approve_project[$i]['designer_id'], "qa");
            $check_approve_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($id, $_SESSION['user_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($check_approve_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $check_approve_project[$i]['deliverydate'] = $this->onlytimezone($check_approve_project[$i]['modified']);
            //$check_approve_project[$i]['deliverydate'] = $this->onlytimezone($check_approve_project[$i]['latest_update']);
            $check_approve_project[$i]['comment_count'] = $commentcount;
            $check_approve_project[$i]['total_files'] = $this->Request_model->get_files_count($check_approve_project[$i]['id'], $check_approve_project[$i]['designer_id'], "qa");
            $check_approve_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($check_approve_project[$i]['id']);
        }

        $disapprove_project = $this->Request_model->get_request_list_for_qa($id, array('disapprove'));

        for ($i = 0; $i < sizeof($disapprove_project); $i++) {
            $disapprove_project[$i]['total_chat'] = $this->Request_model->get_chat_number($id, $disapprove_project[$i]['customer_id'], $disapprove_project[$i]['designer_id'], "qa");
            $disapprove_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($id, $_SESSION['user_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($disapprove_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $disapprove_project[$i]['comment_count'] = $commentcount;
            $disapprove_project[$i]['total_files'] = $this->Request_model->get_files_count($disapprove_project[$i]['id'], $disapprove_project[$i]['designer_id'], "qa");
            $disapprove_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($disapprove_project[$i]['id']);
        }

        $pending_project = $this->Request_model->get_request_list_for_qa($id, array('pending', 'assign'), "", "index_number");

        for ($i = 0; $i < sizeof($pending_project); $i++) {
            $pending_project[$i]['total_chat'] = $this->Request_model->get_chat_number($pending_project[$i]['id'], $pending_project[$i]['customer_id'], $pending_project[$i]['designer_id'], "qa");
            $pending_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($pending_project[$i]['id'], $_SESSION['user_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($pending_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $pending_project[$i]['comment_count'] = $commentcount;
            $pending_project[$i]['total_files'] = $this->Request_model->get_files_count($pending_project[$i]['id'], $pending_project[$i]['designer_id'], "qa");
            $pending_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($pending_project[$i]['id']);
        }


        $complete_project = $this->Request_model->get_request_list_for_qa($id, array('approved'), "", "");
        for ($i = 0; $i < sizeof($complete_project); $i++) {
            $complete_project[$i]['total_chat'] = $this->Request_model->get_chat_number($complete_project[$i]['id'], $complete_project[$i]['customer_id'], $complete_project[$i]['designer_id'], "qa");
            $complete_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($complete_project[$i]['id'], $_SESSION['user_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($complete_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $complete_project[$i]['approvddate'] = $this->onlytimezone($complete_project[$i]['approvaldate']);
            $complete_project[$i]['comment_count'] = $commentcount;
            $complete_project[$i]['total_files'] = $this->Request_model->get_files_count($complete_project[$i]['id'], $complete_project[$i]['designer_id'], "qa");
            $complete_project[$i]['total_files_count'] = $this->Request_model->get_files_count_all($complete_project[$i]['id']);
        }

        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $designer_list = $this->Request_model->get_designer_list_for_qa("array");
        $this->load->view('qa/qa_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, 'titlestatus' => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view('qa/client_projects', array("active_project" => $active_project, "complete_project" => $complete_project, "pending_project" => $pending_project, "disapprove_project" => $disapprove_project, "check_approve_project" => $check_approve_project, "userdata" => $data['userdata'], "designer_list" => $designer_list));
        $this->load->view('qa/qa_footer');
    }

    public function chat($user_type = null) {
        $this->myfunctions->checkloginuser("qa");
        if ($user_type == "customer" || $user_type == "") {
            $mydesigner = $this->Request_model->get_designer_list_for_qa();
            $data['customer'] = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);

            // $data['customer'] = $this->Account_model->getall_customer();
            $data['active_tab'] = "customer";
            $second = "customer_id";
        }
        if ($user_type == "designer") {
            $data['customer'] = $this->Request_model->get_designer_list_for_qa("array");
            //$data['customer'] = $this->Account_model->getall_designer();
            $second = "designer_id";
            $data['active_tab'] = "designer";
        }
        if ($user_type == "va") {
            $data['customer'] = $this->Account_model->getva_member();
            $second = "va_id";
            $data['active_tab'] = "va";
        }
        for ($i = 0; $i < sizeof($data['customer']); $i++) {
            $data['customer'][$i]['room_id'] = $this->Account_model->chat_data("qa_id", $_SESSION['user_id'], $second, $data['customer'][$i]['id']);
            $data['customer'][$i]['last_message'] = $this->Account_model->get_last_message_room($data['customer'][$i]['room_id']);
            $user_data = $this->Account_model->getuserbyid($data['customer'][$i]['id']);
            if (!empty($user_data)) {
                $data['customer'][$i]['image'] = $user_data[0]['profile_picture'];
                $data['customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                if ($user_data[0]['online'] == 1) {
                    $data['customer'][$i]['status'] = "Online";
                    $data['customer'][$i]['status_class'] = "greentext";
                } else {
                    $data['customer'][$i]['status'] = "Offline";
                    $data['customer'][$i]['status_class'] = "orangetext";
                }
                $data['customer'][$i]['title'] = $user_data[0]['first_name'] . " " . $user_data[0]['last_name'];
            }
        }
        $data['room_no'] = "";
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('qa/qa_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "edit_profile" => $profile_data));
        $this->load->view('qa/chat', $data);
        $this->load->view('qa/qa_footer');
    }

    public function room($id = null) {
        if ($id == "" || !isset($_GET['user_type'])) {
            redirect(base_url() . "qa/dashboard/chat");
        }
        $first = "qa_id";

        if ($_GET['user_type'] == "customer") {
            $second = "customer_id";
            $active_tab = "customer";
        }
        if ($_GET['user_type'] == "designer") {
            $second = "designer_id";
            $active_tab = "designer";
        }
        if ($_GET['user_type'] == "va") {
            $second = "va_id";
            $active_tab = "va";
        }

        $room_no = $this->Account_model->chat_data($first, $_SESSION['user_id'], $second, $id);

        if ($room_no == "") {
            redirect(base_url() . "qa/dashboard/chat");
        }
        redirect(base_url() . "qa/dashboard/room_chat/" . $room_no . "?user_type=" . $active_tab);
    }

    public function room_chat($room_no = null) {
        if ($room_no == "" || !isset($_GET['user_type'])) {
            redirect(base_url() . "qa/dashboard/chat");
        }

        if ($_GET['user_type'] == "customer") {
            $second = "customer_id";
            $data['active_tab'] = "customer";
            $data['customer'] = $this->Account_model->getall_customer();
        }
        if ($_GET['user_type'] == "designer") {
            $second = "designer_id";
            $data['active_tab'] = "designer";
            $data['customer'] = $this->Account_model->getall_designer();
        }
        if ($_GET['user_type'] == "va") {
            $second = "va_id";
            $data['active_tab'] = "va";
            $data['customer'] = $this->Account_model->getva_member();
        }
        $data['userdata'] = $this->Account_model->get_userdata_from_room_id($room_no);
        $data['chat'] = $this->Account_model->get_chat($room_no);
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);

        $data['room_no'] = $room_no;

        $this->load->view('qa/qa_header', array("edit_profile" => $profile_data));
        $this->load->view('qa/chat', $data);
        $this->load->view('qa/qa_footer');
    }

    public function view_designer() {
        $titlestatus = "view_designer";
        $qaID = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : '';
        $this->myfunctions->checkloginuser("qa");

        if (!empty($_POST)) {

            if ($_POST['password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata('message_error', 'Password and Confirm Password is not Match.!', 5);

                redirect(base_url() . "qa/dashboard/view_designer");
            }
            $email_check = $this->Account_model->getuserbyemail($_POST['email']);
            if (!empty($email_check)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available.!', 5);

                redirect(base_url() . "qa/dashboard/view_designer");
            }
            $success = $this->Welcome_model->insert_data("users", array("first_name" => $_POST['first_name'], "last_name" => $_POST['last_name'], "email" => $_POST['email'], "phone" => $_POST['phone'], 'new_password' => $_POST['password'], "status" => "1", "role" => "designer", "created" => date("Y-m-d H:i:s")));
            if ($success) {
                $this->session->set_flashdata('message_success', 'Designer Added Successfully.!', 5);

                redirect(base_url() . "qa/dashboard/view_designer");
            } else {
                $this->session->set_flashdata('message_error', 'Designer Not Added Successfully.!', 5);

                redirect(base_url() . "qa/dashboard/view_designer");
            }
        }
        $designerscount = $this->Account_model->designerlistfor_qa_load_more($qaID, true);
        $designers = $this->Account_model->designerlistfor_qa_load_more($qaID);

        for ($i = 0; $i < sizeof($designers); $i++) {
            $designers[$i]['total_rating'] = $this->Request_model->get_average_rating($designers[$i]['id']);

            $user = $this->Account_model->get_all_request_by_designer($designers[$i]['id']);
            $designers[$i]['handled'] = sizeof($user);

            $user = $this->Account_model->get_all_active_request_by_designer($designers[$i]['id']);
            $designers[$i]['active_request'] = sizeof($user);
        }
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $this->load->view('qa/qa_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "titlestatus" => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view('qa/view_designer', array('designers' => $designers, 'designerscount' => $designerscount));
        $this->load->view('qa/qa_footer');
    }

//    public function send_message() {
//        //print_r($_POST);
//        if ($_POST['sender_role'] == "customer") {
//            $request_id = $_POST['request_id'];
//            $request_data = $this->Request_model->get_request_by_id($_POST['request_id']);
//            $customer_data = $this->Admin_model->getuser_data($request_data[0]['customer_id']);
//            $subcat_data = $this->Category_model->get_category_byID($request_data[0]['subcategory_id']);
//            $expected = $this->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $customer_data[0]['plan_name'], $request_data[0]['category_bucket'], $subcat_data[0]['timeline']);
//            unset($_POST['request_id']);
//            $_POST['created'] = date("Y-m-d H:i:s");
//            if ($this->Welcome_model->insert_data("request_file_chat", $_POST)) {
//                //if($request_data[0]['status'] != "approved" && $request_data[0]['status'] != "draft" && $request_data[0]['status'] != "assign"){
//                if ($request_data[0]['status'] == "checkforapprove") {
//                    $this->Welcome_model->update_data("requests", array("status" => "disapprove", "status_designer" => "disapprove", "status_admin" => "disapprove", "status_qa" => "disapprove", "who_reject" => 1, "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $request_id));
//                }
//                echo $this->db->insert_id();
//            } else {
//                echo '0';
//            }
//        } else {
//            $_POST['created'] = date("Y-m-d H:i:s");
//            if ($this->Welcome_model->insert_data("request_file_chat", $_POST)) {
//                echo $this->db->insert_id();
//            } else {
//                echo '0';
//            }
//        }
//        exit;
//    }
    public function send_message() {
        $admin_id = $this->load->get_var('admin_id');
        $request_files = $this->Admin_model->get_request_files_byid($_POST['request_file_id']);
        $request_data = $this->Request_model->get_request_by_id($request_files[0]['request_id']);
        $login_user_id = $this->load->get_var('login_user_id');
        if (isset($_POST['shared_user_name']) && $_POST['shared_user_name'] != '') {
            $_POST['sender_id'] = '';
        } else {
            $_POST['sender_id'] = $_POST['sender_id'];
        }

        $_POST['created'] = date("Y-m-d H:i:s");
        $_POST['shared_user_name'] = isset($_POST['shared_user_name']) ? $_POST['shared_user_name'] : '';
        if ($this->Welcome_model->insert_data("request_file_chat", $_POST)) {
            echo $this->db->insert_id();
        } else {
            echo '0';
        }

        /*         * *****insert messages notifications****** */

        if ($request_files[0]['status'] != 'pending' && $request_files[0]['status'] != 'Reject') {
            $this->myfunctions->show_messages_notifications($request_data[0]['title'], $_POST['message'], $request_files[0]['request_id'], $login_user_id, "customer/request/project_image_view/" . $_POST['request_file_id'] . "?id=" . $request_files[0]['request_id'], $_POST['receiver_id']);
        }
        /* Message notification for qa */
        $this->myfunctions->show_messages_notifications($request_data[0]['title'], $_POST['message'], $request_files[0]['request_id'], $login_user_id, "designer/request/project_image_view/" . $_POST['request_file_id'] . "?id=" . $request_files[0]['request_id'], $request_data[0]['designer_id']);
        /* Message notification for admin */
        $this->myfunctions->show_messages_notifications($request_data[0]['title'], $_POST['message'], $request_files[0]['request_id'], $login_user_id, "admin/dashboard/view_files/" . $_POST['request_file_id'] . "?id=" . $request_files[0]['request_id'], $admin_id);

        /*         * *****end insert messages notifications****** */

        exit;
    }

    public function send_message_request() {
        // print_r($_POST);
        $admin_id = $this->load->get_var('admin_id');
        $login_user_id = $this->load->get_var('login_user_id');
        $sender_type = $_POST['sender_type'];
        if ($sender_type == 'designer') {
            $_POST['designer_seen'] = '1';
        } elseif ($sender_type == 'customer') {
            $_POST['customer_seen'] = '1';
        }
        //die();
        $designer_id = $_POST['designer_id'];
        unset($_POST['designer_id']);
        $_POST['created'] = date("Y-m-d H:i:s");
        // $_POST['designer_seen'] = "1";
        if ($this->Welcome_model->insert_data("request_discussions", $_POST)) {
            echo '1';

            $rtitle = $this->Request_model->get_request_by_id($_POST['request_id']);


            if (!empty($rtitle)) {
                /*                 * *****insert messages notifications****** */

                if ($_POST['with_or_not_customer'] == "1") {

                    /* Message notification for customer */
                    $this->myfunctions->show_messages_notifications($rtitle[0]['title'], $_POST['message'], $_POST['request_id'], $login_user_id, "customer/request/project_info/" . $_POST['request_id'], $_POST['reciever_id']);
                }
                /* Message notification for qa */
                $this->myfunctions->show_messages_notifications($rtitle[0]['title'], $_POST['message'], $_POST['request_id'], $login_user_id, "designer/request/project_info/" . $_POST['request_id'], $designer_id);
                /* Message notification for admin */
                $this->myfunctions->show_messages_notifications($rtitle[0]['title'], $_POST['message'], $_POST['request_id'], $login_user_id, "admin/dashboard/view_request/" . $_POST['request_id'], $admin_id);

                /*                 * *****end insert messages notifications****** */
            } else {
                $data['title'] = "New Message Arrived";
                $data['created'] = date("Y:m:d H:i:s");
                $this->Welcome_model->insert_data("notification", $data);
            }
            $this->Request_model->get_notifications($_SESSION['user_id']);
        } else {
            echo '0';
        }
    }

    public function active_view_designer($id) {
        $titlestatus = 'view_designer';
        $sql = "select * from users where designer_id= '" . $id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $designers = $this->Account_model->getuserbyid($id);

        $skills = $this->Request_model->getallskills($id);
        $datadesigner['activeproject'] = array();
        $datadesigner['inqueueproject'] = array();
        $datadesigner['pendingreviewproject'] = array();
        $datadesigner['pendingproject'] = array();
        $datadesigner['completeproject'] = array();
        // Active Designer Project Section Start Here

        $datadesigner['activeproject'] = $this->Request_model->get_request_list_for_qa("", array('active', 'disapprove'), $id);
        for ($i = 0; $i < sizeof($datadesigner['activeproject']); $i++) {
            $datadesigner['activeproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['activeproject'][$i]['id'], $datadesigner['activeproject'][$i]['customer_id'], $datadesigner['activeproject'][$i]['designer_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['activeproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            if ($datadesigner['activeproject'][$i]['expected_date'] == '' || $datadesigner['activeproject'][$i]['expected_date'] == NULL) {
                $datadesigner['activeproject'][$i]['expected'] = $this->myfunctions->check_timezone($datadesigner['activeproject'][$i]['latest_update'], $datadesigner['activeproject'][$i]['current_plan_name']);
            } else {
                $datadesigner['activeproject'][$i]['expected'] = $this->onlytimezone($datadesigner['activeproject'][$i]['expected_date']);
            }
            $datadesigner['activeproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['activeproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['activeproject'][$i]['id'], $_SESSION['user_id'], "qa");
            $datadesigner['activeproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['activeproject'][$i]['id'], $datadesigner['activeproject'][$i]['designer_id'], "qa");
            $datadesigner['activeproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['activeproject'][$i]['id']);
        }

        // Active Designer Project Section End Here
        // In-Queue Designer Project Section Start Here

        $datadesigner['inqueueproject'] = $this->Request_model->get_request_list_for_qa("", array('assign', 'pending'), $id);
        for ($i = 0; $i < sizeof($datadesigner['inqueueproject']); $i++) {
            $datadesigner['inqueueproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['inqueueproject'][$i]['id'], $datadesigner['inqueueproject'][$i]['customer_id'], $datadesigner['inqueueproject'][$i]['designer_id'], "qa");
            $datadesigner['inqueueproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['inqueueproject'][$i]['id'], $_SESSION['user_id'], "qa");
            $datadesigner['inqueueproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['inqueueproject'][$i]['id'], $datadesigner['inqueueproject'][$i]['designer_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['inqueueproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $datadesigner['inqueueproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['inqueueproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['inqueueproject'][$i]['id']);
        }
        // In-Queue Designer Project Section End Here
        // Pending Review Designer Project Section Start Here

        $datadesigner['pendingreviewproject'] = $this->Request_model->get_request_list_for_qa("", array('pendingrevision'), $id);
        for ($i = 0; $i < sizeof($datadesigner['pendingreviewproject']); $i++) {
            $datadesigner['pendingreviewproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['pendingreviewproject'][$i]['id'], $datadesigner['pendingreviewproject'][$i]['customer_id'], $datadesigner['pendingreviewproject'][$i]['designer_id'], "qa");
            $datadesigner['pendingreviewproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['pendingreviewproject'][$i]['id'], $_SESSION['user_id'], "qa");
            $datadesigner['pendingreviewproject'][$i]['revisiondate'] = $this->onlytimezone($datadesigner['pendingreviewproject'][$i]['modified']);
            $datadesigner['pendingreviewproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['pendingreviewproject'][$i]['id'], $datadesigner['pendingreviewproject'][$i]['designer_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['pendingreviewproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $datadesigner['pendingreviewproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['pendingreviewproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['pendingreviewproject'][$i]['id']);
        }
        // Pending Review Designer Project Section End Here
        // Pending Approval Designer Project Section Start Here

        $datadesigner['pendingproject'] = $this->Request_model->get_request_list_for_qa("", array('checkforapprove'), $id);
        for ($i = 0; $i < sizeof($datadesigner['pendingproject']); $i++) {
            $datadesigner['pendingproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['pendingproject'][$i]['id'], $datadesigner['pendingproject'][$i]['customer_id'], $datadesigner['pendingproject'][$i]['designer_id'], "qa");
            $datadesigner['pendingproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['pendingproject'][$i]['id'], $_SESSION['user_id'], "qa");
            $datadesigner['pendingproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['pendingproject'][$i]['id'], $datadesigner['pendingproject'][$i]['designer_id'], "qa");
            $datadesigner['pendingproject'][$i]['reviewdate'] = $this->onlytimezone($datadesigner['pendingproject'][$i]['modified']);
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['pendingproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $datadesigner['pendingproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['pendingproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['pendingproject'][$i]['id']);
        }
        // Pending Approval Designer Project Section End Here
        // Complete Designer Project Section Start Here

        $datadesigner['completeproject'] = $this->Request_model->get_request_list_for_qa("", array('approved'), $id);
        for ($i = 0; $i < sizeof($datadesigner['completeproject']); $i++) {
            $datadesigner['completeproject'][$i]['total_chat'] = $this->Request_model->get_chat_number($datadesigner['completeproject'][$i]['id'], $datadesigner['completeproject'][$i]['customer_id'], $datadesigner['completeproject'][$i]['designer_id'], "qa");
            $datadesigner['completeproject'][$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($datadesigner['completeproject'][$i]['id'], $_SESSION['user_id'], "qa");
            $datadesigner['completeproject'][$i]['total_files'] = $this->Request_model->get_files_count($datadesigner['completeproject'][$i]['id'], $datadesigner['completeproject'][$i]['designer_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($datadesigner['completeproject'][$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $datadesigner['completeproject'][$i]['approvedate'] = $this->onlytimezone($datadesigner['completeproject'][$i]['approvaldate']);
            $datadesigner['completeproject'][$i]['comment_count'] = $commentcount;
            $datadesigner['completeproject'][$i]['total_files_count'] = $this->Request_model->get_files_count_all($datadesigner['completeproject'][$i]['id']);
        }
        // Complete Designer Project Section End Here

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $alldesigners = $this->Request_model->get_all_designer_for_qa($_SESSION['user_id']);
        for ($i = 0; $i < sizeof($alldesigners); $i++) {

            $user = $this->Account_model->get_all_active_request_by_designer($alldesigners[$i]['id']);
            $alldesigners[$i]['active_request'] = sizeof($user);
        }
        if ($_POST) {
            $designer_assigned = $this->Admin_model->assign_designer_by_qa($_POST['assign_designer'], $_POST['request_id']);
            redirect(base_url() . "qa/dashboard/active_view_designer/" . $_POST['assign_designer']);
        }
        $this->load->view('qa/qa_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "titlestatus" => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view('qa/active_view_designer', array("data" => $result, "designers" => $designers, 'datadesigner' => $datadesigner, 'alldesigners' => $alldesigners, 'skills' => $skills));
        $this->load->view('qa/qa_footer');
    }

    public function get_chat_ajax() {
        $this->myfunctions->checkloginuser("qa");
        $this->Welcome_model->update_online();
        $this->Welcome_model->update_data("message_notification", array("shown" => 1), array("url" => "customer/profile/room_chat/" . $_POST['roomid']));
        $chat = $this->Account_model->get_chat($_POST['roomid']);
        $data['room_no'] = $_POST['roomid'];
        $room_no = $_POST['roomid'];
        $data['chat_customer'] = $this->Account_model->get_user_for_chat("customer_id", $_SESSION['user_id']);
        $userdata = $this->Account_model->get_userdata_from_room_id($_POST['roomid']);

        for ($i = 0; $i < sizeof($data['chat_customer']); $i++) {
            $data['chat_customer'][$i]['last_message'] = $this->Account_model->get_last_message_room($data['chat_customer'][$i]['room_id']);
            if ($data['chat_customer'][$i]['designer_id'] != "") {
                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['designer_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['qa_id'] != "") {
                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['va_id'] != "") {
                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
        }
        $user = $this->Account_model->getuserbyid($_SESSION['user_id']);
        $data['myname'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
        $myname = $data['myname'];
        $chat_customer = $data['chat_customer']
        ?>

        <div class="chatpage-tit">
            <h4><?php
                if (isset($chat_customer[0]['chat_name'])) {
                    echo $chat_customer[0]['chat_name'];
                }
                ?></h4>
        </div>

        <div class="chat-main">

            <?php if (isset($chat)) { ?>

                <div class="myscroller2 roomchat messagediv_<?php echo $room_no; ?>" style="height:500px; overflow-y: scroll;">
                    <?php for ($i = 0; $i < sizeof($chat); $i++) { ?>


                        <!--<div>-->
                        <?php if ($_SESSION['user_id'] == $chat[$i]['sender_id']) { ?>


                            <div class="chatbox2 right">
                                <!--<div class="avatar">
                                        &nbsp;
                                </div>-->

                                <div class="message-text-wrapper messageright">
                                    <p class="description">
                                        <?php
                                        if ($chat[$i]['isimage'] == "1") {
                                            echo '<img src="' . FS_PATH_PUBLIC_UPLOADS_REQUESTS . $chat[$i]['message'] . '" />';
                                        } else {
                                            echo $chat[$i]['message'];
                                        }
                                        ?>
                                    </p>
                                    <p class="posttime">
                                        <?php
                                        $date = strtotime($chat[$i]['created']);
                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date;
                                        $minutes = floor($diff / 60);
                                        $hours = floor($diff / 3600);
                                        if ($hours == 0) {
                                            echo $minutes . " Minutes Ago";
                                        } else {
                                            $days = floor($diff / (60 * 60 * 24));
                                            if ($days == 0) {
                                                echo $hours . " Hours Ago";
                                            } else {
                                                echo $days . " Days Ago";
                                            }
                                        }
                                        ?>
                                    </p>
                                </div>
                            </div>


                        <?php } else { ?>

                            <div class="chatbox left">
                                <div class="avatar">
                                    <img src="<?php echo base_url(); ?>public/img/img/logo.png" style="border-radius:25px;padding: 0px;" />
                                </div>

                                <div class="message-text-wrapper messageleft">

                                    <p class="title">
                                        <?php echo $userdata[$chat[$i]['sender_id']]; ?>
                                    </p>
                                    <p class="description">
                                        <?php
                                        if ($chat[$i]['isimage'] == "1") {
                                            echo '<img src="' . FS_PATH_PUBLIC_UPLOADS_REQUESTS . $chat[$i]['message'] . '" />';
                                        } else {
                                            echo $chat[$i]['message'];
                                        }
                                        ?>
                                    </p>
                                    <p class="posttime">
                                        <?php
                                        $date = strtotime($chat[$i]['created']);
                                        $diff = strtotime(date("Y:m:d H:i:s")) - $date;
                                        $minutes = floor($diff / 60);
                                        $hours = floor($diff / 3600);
                                        if ($hours == 0) {
                                            echo $minutes . " Minutes Ago";
                                        } else {
                                            $days = floor($diff / (60 * 60 * 24));
                                            if ($days == 0) {
                                                echo $hours . " Hours Ago";
                                            } else {
                                                echo $days . " Days Ago";
                                            }
                                        }
                                        ?>
                                    </p>

                                </div>
                            </div>
                        <?php } ?>
                        <!--</div>-->

                    <?php } ?>
                </div>


            <?php } ?>

        </div>

        <div class="chatbtn-boxmain">
            <form onsubmit="myform_notsubmit(event, this);" class="myform" method="post" action="" enctype="multipart/form-data"  style="clear: both;position: absolute;bottom: -7px;z-index: 9999999999999;">
                <span class="chatcamera"><i class="fa fa-paperclip" id="openimageupload" onclick="$('#fileinput').click();"></i></span>
                <div class="hiddenfile">
                    <input name="upload" type="file" id="fileinput" onchange="validateAndUpload(this);"/>
                </div>
                <input type="hidden" value="<?php echo $room_no; ?>" class="hidden_room_no" name="room_no"/>
                <button type="submit" style="margin-top: -16px; margin-right: 15px;display: none;" class="myfilesubmit">Image Submit</button>
            </form>
            <input type='text' class='form-control chatlargeinp myText room_chat_text text_<?php echo $room_no; ?>' placeholder="Type a message here" />
            <span style="margin-right: 15px;" class="chatsendbtn send_room_chat"  data-room="<?php echo $room_no; ?>" data-sendername="<?php echo $myname; ?>"  onclick="send_room_chat();">Send</span>
        </div>

        <?php
    }

    public function get_chat_qa_ajex() {
        $this->myfunctions->checkloginuser("qa");
        $this->Welcome_model->update_online();

        $data['room_no'] = $_POST['roomid'];
        $room_no = $_POST['roomid'];

        $data['chat_customer'] = $this->Account_model->get_user_for_chat("customer_id", $_SESSION['user_id']);
        for ($i = 0; $i < sizeof($data['chat_customer']); $i++) {

            $data['chat_customer'][$i]['last_message'] = $this->Account_model->get_last_message_room($data['chat_customer'][$i]['room_id']);
            if ($data['chat_customer'][$i]['designer_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['designer_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['designer_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }
            }
            if ($data['chat_customer'][$i]['qa_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['qa_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
            if ($data['chat_customer'][$i]['va_id'] != "") {
                $user_data = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                if (!empty($user_data)) {
                    $data['chat_customer'][$i]['image'] = $user_data[0]['profile_picture'];
                    $data['chat_customer'][$i]['sort_name'] = substr($user_data[0]['first_name'], 0, 1) . substr($user_data[0]['last_name'], 0, 1);
                    if ($user_data[0]['online'] == 1) {
                        $data['chat_customer'][$i]['status'] = "Online";
                        $data['chat_customer'][$i]['status_class'] = "greentext";
                    } else {
                        $data['chat_customer'][$i]['status'] = "Offline";
                        $data['chat_customer'][$i]['status_class'] = "orangetext";
                    }
                }

                if ($data['chat_customer'][$i]['room_id'] == $data['room_no']) {
                    $user = $this->Account_model->getuserbyid($data['chat_customer'][$i]['va_id']);
                    $data['chat_customer'][0]['chat_name'] = $user[0]['first_name'] . " " . $user[0]['last_name'];
                }
            }
        }
        $chat_customer = $data['chat_customer'];
        $myname = "";
        for ($i = 0; $i < sizeof($chat_customer); $i++) {
            ?>



            <div data-roomid="<?php echo $chat_customer[$i]['room_id']; ?>" onclick="chatlistbox('<?php echo $chat_customer[$i]['room_id']; ?>')" class="chatlistbox <?php
            if ($chat_customer[$i]['room_id'] == $room_no) {
                echo 'active';
            }
            ?>">
                <div class="avatar">
                    <?php if ($chat_customer[$i]['image']) { ?>
                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $chat_customer[$i]['image']; ?>" class="img-responsive" />
                    <?php } else { ?>
                        <div class="myprofilebackground" style="width:50px; height:50px;border-radius:50%; background: #0190ff;border: 3px solid #0190ff; text-align: center; font-size: 15px; color: #fff;"><p style="font-size: 24px;padding-top: 7px;">
                                <?php echo $chat_customer[$i]['sort_name']; ?></p></div>
                    <?php } ?>
                </div>
                <div class="message-text-wrapper topspacen">
                    <div class="posttime">Dec 11</div>
                    <p class="title"><?php echo $chat_customer[$i]['title']; ?></p>
                    <p class="message graytext"><?php echo substr($chat_customer[$i]['last_message'], 0, 20); ?></p>
                    <!--<p class="<?php echo $chat_customer[$i]['status_class']; ?> status"><?php echo $chat_customer[$i]['status']; ?></p>-->
                    <div class="<?php echo strtolower($chat_customer[$i]['status']); ?>"><i class="fa fa-circle"></i></div>
                </div>
                <div class="clear"></div>
            </div>
            <?php
        }
    }

    public function send_room_message() {
        $this->myfunctions->checkloginuser("qa");
        $this->Welcome_model->update_online();
        if (!empty($_POST)) {
            $_POST['created'] = date("Y-m-d H:i:s");

            $room_data = $this->Account_model->get_userdata_from_room_id($_POST['room_id']);
            $not_data['url'] = "designer/profile/room_chat/" . $_POST['room_id'];
            $not_data['created'] = date("Y:m:d H:i:s");
            $not_data['heading'] = "";
            $not_data['title'] = substr($_POST['message'], 0, 30) . "...";

            $ids = array();
            foreach ($room_data as $id => $name) {
                if ($id == $_SESSION['user_id']) {
                    $not_data['heading'] = $name;
                } else {
                    $ids[] = $id;
                }
            }
            for ($i = 0; $i < sizeof($ids); $i++) {
                $not_data['user_id'] = $ids[$i];
                $mn = $this->Request_model->get_request_by_data($not_data['url'], $not_data['heading'], $not_data['user_id'], 0);
                if (empty($mn)) {
                    $this->Welcome_model->insert_data("message_notification", $not_data);
                } else {
                    $this->Welcome_model->update_data("message_notification", array("count" => $mn[0]['count'] + 1, "created" => date("Y:m:d H:i:s"), "title" => $not_data['title']), array("url" => $not_data['url'], "heading" => $not_data['heading'], "user_id" => $not_data['user_id'], "shown" => 0));
                }
            }
            $success = $this->Welcome_model->insert_data("room_chat", $_POST);

            echo $success;
            exit;
        }
    }

    public function imgupload() {
        if (!empty($_FILES)) {
            if ($_FILES['upload']['name'] != "") {
                $config2 = array(
                    'upload_path' => './public/uploads/requests/',
                    'allowed_types' => "*",
                    'encrypt_name' => TRUE
                );
                $config2['preview_file'] = $_FILES['upload']['name'];

                $_FILES['src_file']['name'] = $_FILES['upload']['name'];
                $_FILES['src_file']['type'] = $_FILES['upload']['type'];
                $_FILES['src_file']['tmp_name'] = $_FILES['upload']['tmp_name'];
                $_FILES['src_file']['error'] = $_FILES['upload']['error'];
                $_FILES['src_file']['size'] = $_FILES['upload']['size'];
                $this->load->library('upload', $config2);

                if ($this->upload->do_upload('upload')) {
                    $data = array($this->upload->data());
                    $data = $this->Welcome_model->insert_data("room_chat", array("room_id" => $_POST['room_no'], "sender_id" => $_SESSION['user_id'], "message" => $data[0]['file_name'], "isimage" => 1, "created" => date("Y:m:d H:i:s")));
                    echo $data;
                } else {
                    echo '0';
                }
            }
        } else {
            echo '0';
        }
    }

    public function request_response() {
        $result = array();
        $this->myfunctions->checkloginuser("qa");
        $this->Welcome_model->update_online();
        //print_r($_POST); 
        $login_user_id = $this->load->get_var('login_user_id');
        $admin_id = $this->load->get_var('admin_id');
        $request_data = $this->Request_model->get_request_by_id($_POST['request_id']);
        $email = $this->Request_model->getuserbyid($request_data[0]['customer_id']);
        $email_designer = $this->Request_model->getuserbyid($request_data[0]['designer_id']);
        $is_approveDraftemailbyadmin = $this->Admin_model->approveDraftemailbyadmin();
        $is_approveDraftemailbyuser = $this->Admin_model->approveDraftemailbyuser($request_data[0]['customer_id']);
        //$success = $this->Welcome_model->update_data("request_files", array("status" => $_POST['value']), array("request_id" => $_POST['request_id']));
        $success = $this->Welcome_model->update_data("request_files", array("modified" => date("Y:m:d H:i:s"), "status" => $_POST['value']), array("id" => $_POST['fileid']));
        if ($success) {

            if ($_POST['value'] == "Reject") {
                $_POST['value'] = "disapprove";
                $this->Welcome_model->update_data("requests", array("status_designer" => $_POST['value'], "status_qa" => $_POST['value'], "status_admin" => $_POST['value'], "who_reject" => 0), array("id" => $_POST['request_id']));
                $this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'','reject_design_draft','qa_request_response',$login_user_id,'0',"1","1");
                $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Design For \"" . $request_data[0]['title'] . "\" is Rejected..!", "designer/request/project_info/" . $_POST['request_id'], $request_data[0]['designer_id']);
            } elseif ($_POST['value'] == "customerreview") {
                $_POST['value'] = "checkforapprove";
                if ($request_data[0]['status'] != 'approved') {
                    $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status" => $_POST['value'], "status_qa" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                } else {
                    $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status_qa" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                }
                $this->myfunctions->send_email_after_admin_approval($request_data[0]['title'], $request_data[0]['id'], $email[0]['email'], $_POST['fileid']);
                //send email to sub user after qa approve
                $send_emailto_subusers = $this->myfunctions->send_emails_to_sub_user($request_data[0]['customer_id'], 'approve/revision_requests', $is_approveDraftemailbyadmin, $request_data[0]['id']);
                $subdomain_url = $this->myfunctions->dynamic_urlforsubuser_inemail($request_data[0]['customer_id'], $email[0]['plan_name']);
                if (!empty($send_emailto_subusers)) {
                    foreach ($send_emailto_subusers as $send_emailto_subuser) {
                        $this->myfunctions->send_email_after_admin_approval($request_data[0]['title'], $request_data[0]['id'], $send_emailto_subuser['email'], $_POST['fileid'], $subdomain_url);
                    }
                }
               // $request2 = $this->Admin_model->get_all_request_count(array('active'), $request_data[0]['customer_id']);
               // $request3 = $this->Admin_model->get_all_request_count(array('checkforapprove', 'disapprove'), $request_data[0]['customer_id']);
                $ActiveReq = $this->myfunctions->CheckActiveRequest($request_data[0]['customer_id']);
                if ($ActiveReq == 1) {
                    $this->myfunctions->move_project_inqueqe_to_inprogress($request_data[0]['customer_id'],"","qa_request_response");
                }
                /** capture project event **/
                $this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'','approved_draft_design','qa_request_response',$login_user_id,'0','','1');
                $this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'',$request_data[0]['status_qa'].'_to_'.$_POST['value'],'qa_request_response',$login_user_id,'1','','0');
                $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Upload New File For \"" . $request_data[0]['title'] . "\" . Please Check And Approve..!", "customer/request/project_info/" . $_POST['request_id'], $request_data[0]['customer_id']);
                $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Approved your design for \"" . $request_data[0]['title'] . "\"", "designer/request/project_info/" . $_POST['request_id'], $request_data[0]['designer_id']);
            } else {

                $_POST['value'] = "approved";
                $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status" => $_POST['value'], "status_designer" => $_POST['value'], "status_qa" => $_POST['value'], "status_admin" => $_POST['value'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                $this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'',$request_data[0]['status_qa'].'_to_'.$_POST['value'],'qa_request_response',$login_user_id);
                $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Approved your design for \"" . $request_data[0]['title'] . "\"", "designer/request/project_info/" . $_POST['request_id'], $request_data[0]['designer_id']);



                $request2 = $this->Admin_model->get_all_request_count(array("active"), $request_data[0]['customer_id']);

                if (sizeof($request2) < 1) {
                    $all_requests = $this->Request_model->getall_request($request_data[0]['customer_id'], "'pending','assign'", "", "index_number");
                    if (isset($all_requests[0]['id'])) {
                        $success = $this->Welcome_model->update_data("requests", array("status" => "active", "status_admin" => "active", "status_designer" => "active", "status_qa" => "active", "dateinprogress" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $all_requests[0]['id']));
                    }
                }
            }
        }
        $modified_date = $this->onlytimezone(date("Y:m:d H:i:s"));
        $result['modified'] = date("M d, Y h:i A", strtotime($modified_date));
        $result['success'] = $success;
        echo json_encode($result);
        exit;
    }

    public function qaprofile() {
        $this->myfunctions->checkloginuser("qa");
        $titlestatus = "qaprofile";
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $user_id = $_SESSION['user_id'];
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $timezone = $this->Admin_model->get_timezone();
        if ($this->input->post('submit')) {
            $fndlname = explode(" ", $this->input->post('fndlname'));
            if ($_FILES['qaprofilepic']['name'] != "") {
                if (!empty($_FILES)) {
                    $file = pathinfo($_FILES['qaprofilepic']['name']);
                    $s3_file = $file['filename'] . '-' . rand(1000, 1) . '.' . $file['extension'];
                    $config = array(
                        'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_PROFILE,
                        'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                        'file_name' => $s3_file
                    );
                    $check = $this->myfunctions->CustomFileUpload($config, 'qaprofilepic');
                    if ($check['status'] == 1) {
                        $profile_pic = array(
                            'profile_picture' => $s3_file,
                        );
                        $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
                        if ($update_data_result) {
                            $this->session->set_flashdata('message_success', 'User Profile Picture Update Successfully.!', 5);
                        } else {
                            $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!', 5);
                        }
                    } else {
                        $this->session->set_flashdata('message_error', $check['msg'], 5);
                    }
                }
            }
            if (!empty($this->input->post())) {
                $profile_pic = array(
                    'first_name' => $fndlname[0],
                    'last_name' => $fndlname[1],
                    'email' => $this->input->post('email'),
                );
                $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
                if ($update_data_result) {
                    $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
                } else {
                    $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
                }
            }
            redirect(base_url() . "qa/dashboard/profile");
        }
        $this->load->view('qa/qa_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "titlestatus" => $titlestatus, "edit_profile" => $profile_data));
        $this->load->view("qa/qa_profile", array("edit_profile" => $profile_data, "timezone" => $timezone));
        $this->load->view("qa/qa_footer");
    }

    public function send_email_when_inqueue_to_inprogress($project_title, $id, $custemail) {
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        //$query = $this->db->select('email')->from('users')->where('id',$custid)->get();
        //$query->last_query();
        //$email = $query->result();
        $receiver = $custemail;
        //$query->last_query();
        $query1 = $this->db->select('first_name')->from('users')->where('email', $email)->get();
        $fname = $query1->result();
        $data = array(
            'title' => $project_title,
            'project_id' => $id,
            'fname' => isset($fname[0]->first_name) ? $fname[0]->first_name : ''
        );
        $body = $this->load->view('emailtemplate/inqueue_template', $data, TRUE);
        $subject = 'Designer has Started Working on your New Project.';

        $message = $body;

        $title = 'GraphicsZoo';

        //SM_sendMail($receiver, $subject, $message, $title);
    }

    public function send_email_after_adminapprove($project_title, $id, $email) {
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $data = array(
            'title' => $project_title,
            'project_id' => $id
        );
        $body = $this->load->view('emailtemplate/qa_approved_template', $data, TRUE);
        $receiver = $email;

        $subject = 'Admin Approved';

        $message = $body;

        $title = 'GraphicsZoo';

        // SM_sendMail($receiver, $subject, $message, $title);
        //    $config = $this->config->item('email_smtp');
        // $from = $this->config->item('from');
        //    $this->load->library('email', $config);
        //    $this->email->set_newline("\r\n");
        //    $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        //    $this->email->set_header('Content-type', 'text/html');
        //    $this->email->from($from, 'GraphicsZoo');
        //    $data = array(
        //         'title'=> $project_title,
        //         'project_id' => $id
        //             );
        //    $this->email->to($email);  // replace it with receiver mail id
        //    $this->email->subject("Admin Approved"); // replace it with relevant subject 
        //    $body = $this->load->view('emailtemplate/qa_approved_template',$data,TRUE);
        //    $this->email->message($body);   
        //    $this->email->send();       
    }

    public function editqadetaiL() {
        $user_id = $_SESSION['user_id'];
        $data = array(
            'paypal_email_address' => $this->input->post('paypalid'),
            'phone' => $this->input->post('phone'),
            'title' => $this->input->post('title'),
            'address_line_1' => $this->input->post('address'),
            'city' => $this->input->post('city'),
            'state' => $this->input->post('state'),
            'zip' => $this->input->post('zip'),
            'timezone' => $this->input->post('timezone'),
        );
        $update_data_result = $this->Admin_model->designer_update_profile($data, $user_id);
        if ($update_data_result) {
            $this->session->set_userdata('timezone', $data['timezone']);
            $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
        } else {
            $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
        }
        redirect(base_url() . "qa/dashboard/profile");
    }

    public function change_password_old() {
        $this->myfunctions->checkloginuser("qa");
        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

            if ($user_data[0]['new_password'] != $_POST['old_password']) {
                $this->session->set_flashdata("message_error", "Old Password is not correct..!", 5);
                redirect(base_url() . "qa/dashboard/profile");
            }
            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "qa/dashboard/profile");
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $_SESSION['user_id']))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "qa/dashboard/profile");
        }
    }

    public function load_messages_from_all_user() {

        $project_id = $_POST['project_id'];
        $customer_id = $_POST['cst_id'];
        $designer_id = $_POST['desg_id'];
        $msg_seen = $_POST['qa_seen'];
        $chat_request_msg = $this->Request_model->get_qa_unseen_msg($project_id, $customer_id, $designer_id, $msg_seen);
        if (!empty($chat_request_msg)) {
            $message['message'] = $chat_request_msg[0]['message'];
            $message['profile_picture'] = isset($chat_request_msg[0]['profile_picture']) ? $chat_request_msg[0]['profile_picture'] : 'user-admin.png';
            $message['typee'] = $chat_request_msg[0]['sender_type'];
            $message['with_or_not'] = $chat_request_msg[0]['with_or_not_customer'];
            $message['msg_first_name'] = $chat_request_msg[0]['first_name'];
            echo json_encode($message);
            $this->Admin_model->update_data("request_discussions", array("qa_seen" => 1), array("request_id" => $project_id));
        } else {
            // $message = "Hello Everyone";
            // echo $message;
        }
    }

    public function assign_designer_permanent($id) {
        $data = $this->Admin_model->assign_designer_confirm($id);
        if ($data) {
            redirect(base_url() . "qa/dashboard");
        } else {
            redirect(base_url() . "qa/dashboard");
        }
    }

    public function assign_designer_ajax() {
        if (!empty($_POST['request_id'])) {
            $login_user_id = $this->load->get_var('login_user_id');
            $reqIDs = explode(',', $_POST['request_id']);
            $requests_data = $this->Request_model->get_request_by_id('','',$reqIDs);
            $this->Admin_model->assign_designer_by_admin($_POST['assign_designer'], $reqIDs);
            /**cature assign designer**/
            foreach ($requests_data as $request) {
                if($request['designer_id'] != '0'){
                  $this->myfunctions->capture_project_activity($request['id'],'',$request['designer_id'],'reassign_designer','assign_designer_ajax',$login_user_id);
                }else{
                  $this->myfunctions->capture_project_activity($request['id'],'','','assign_designer','assign_designer_ajax',$login_user_id);  
                }
            }
            $this->session->set_flashdata('message_success', "Designer assigned successfully!");
            redirect(base_url() . "admin/dashboard");
        } else {
            $this->session->set_flashdata('message_error', "Sorry,Designer not assigned!");
            redirect(base_url() . "admin/dashboard");
        }
    }

    public function search_ajax() {
        //print_r($this->input->get());
        $allcustomer = array();
        $dataArray = $this->input->get('title');
        $dataStatus = $this->input->get('status');
        $dataStatus = explode(",", $dataStatus);
        $mycustomer = $this->Request_model->get_customer_list_for_qa_section($_SESSION['user_id']);
        foreach ($mycustomer as $value) {
            $allcustomer[] = $value['id'];
        }
        $rows = $this->Request_model->GetAutocomplete(array('keyword' => $dataArray), $dataStatus, $allcustomer, "");
        //echo "<pre>";print_r($rows);
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rows[$i]->total_chat = $this->Request_model->get_chat_number($rows[$i]->id, $rows[$i]->customer_id, $rows[$i]->designer_id, "qa");
            $rows[$i]->total_chat_all = $this->Request_model->get_total_chat_number($rows[$i]->id, $_SESSION['user_id'], "qa");
            $getfileid = $this->Request_model->get_attachment_files($rows[$i]->id, "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {
                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            $rows[$i]->comment_count = $commentcount;
            if ($rows[$i]->status_qa == "active" || $rows[$i]->status_qa == "disapprove") {
                if ($rows[$i]->expected_date == '' || $rows[$i]->expected_date == NULL) {
                    $rows[$i]->expected = $this->myfunctions->check_timezone($rows[$i]->latest_update, $rows[$i]->current_plan_name);
                } else {
                    $rows[$i]->expected = $this->onlytimezone($rows[$i]->expected_date);
                }
            } elseif ($rows[$i]->status_qa == "checkforapprove") {
                $rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->modified);
            } elseif ($rows[$i]->status_qa == "approved") {
                $rows[$i]->approvddate = $this->onlytimezone($rows[$i]->approvaldate);
            } elseif ($rows[$i]->status_qa == "pendingrevision") {
                $rows[$i]->revisiondate = $this->onlytimezone($rows[$i]->modified);
            }
            $rows[$i]->total_files = $this->Request_model->get_files_count($rows[$i]->id, $rows[$i]->designer_id, 'qa');
            $rows[$i]->total_files_count = $this->Request_model->get_files_count_all($rows[$i]->id);
        }
        for ($i = 0; $i < sizeof($rows); $i++) {
            // For Active Tab
            if ($rows[$i]->status_qa == "disapprove" || $rows[$i]->status_qa == "active") {
                ?>
                <!--QA Active Section Start Here -->
                <!-- <div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel"> -->
                <?php for ($i = 0; $i < sizeof($rows); $i++) {
                    ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/1'">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">
                                    <?php
                                    if ($rows[$i]->status == "active") {
                                        echo "Expected on";
                                    } elseif ($rows[$i]->status_qa == "disapprove") {
                                        echo "Expected on";
                                    }
                                    ?>
                                </p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php
                                    if ($rows[$i]->status == "active") {
                                        echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                    } elseif ($rows[$i]->status_qa == "disapprove") {
                                        echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 30%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/1"><?php echo $rows[$i]->title; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                    </div>

                                    <div class="cell-col col-w1">
                                        <p class="neft text-center">
                                            <?php if ($rows[$i]->status_qa == "active") { ?>
                                                <span class="green text-uppercase text-wrap">In-Progress</span>
                                            <?php } elseif ($rows[$i]->status_qa == "disapprove" && $rows[$i]->who_reject == 1) { ?>
                                                <span class="red orangetext text-uppercase text-wrap">REVISION</span>
                                            <?php } elseif ($rows[$i]->status_qa == "disapprove" && $rows[$i]->who_reject == 0) { ?>
                                                <span class="red text-uppercase text-wrap">Quality Revision</span>
                                            <?php } ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#" >
                                            <figure class="pro-circle-img">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                           ">
                                               <?php echo $rows[$i]->customer_first_name; ?>
                                               <?php
                                               if (strlen($rows[$i]->customer_last_name) > 5) {
                                                   echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                               } else {
                                                   echo $rows[$i]->customer_last_name;
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#" class="<?php echo $rows[$i]->id; ?>">
                                            <figure class="pro-circle-img">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                        <p class="text-h text-wrap"title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>
                                           ">
                                               <?php echo $rows[$i]->designer_first_name; ?>
                                               <?php
                                               if (strlen($rows[$i]->designer_first_name) > 5) {
                                                   echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                               } else {
                                                   echo $rows[$i]->designer_last_name;
                                               }
                                               ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rows[$i]->id; ?>" data-designerid= "<?php echo $rows[$i]->designer_id; ?>">
                                            <span class="sma-red">+</span> Add Designer
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/1'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/1">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $rows[$i]->total_chat_all;   ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/1'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/1">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                            <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                        <?php echo count($rows[$i]->total_files_count); ?>
                                    </a></p>
                            </div>
                        </div>
                    </div>
                    <!-- End Row -->
                <?php } ?>
                <!-- </div> -->
                <!--QA Active Section End Here --> 
                <?php
                // End Active Tab
            } elseif ($rows[$i]->status_qa == "assign" || $rows[$i]->status_qa == "pending") {
                ?>
                <!--QA IN Queue Section Start Here -->
                <!-- <div class="tab-pane content-datatable datatable-width" id="Qa_in_queue" role="tabpanel"> -->
                <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/2'">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">In Queue</p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 36%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/2'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/2"><?php echo $rows[$i]->title; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                    </div>

                                    <div class="cell-col" >
                                        <?php echo isset($rows[$i]->priority) ? $rows[$i]->priority : ''; ?>
                                    </div>

                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="gray text-uppercase">IN-queue</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $rows[$i]->id; ?>/2'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#" >
                                            <figure class="pro-circle-img">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                           ">
                                               <?php echo $rows[$i]->customer_first_name; ?>
                                               <?php
                                               if (strlen($rows[$i]->customer_last_name) > 5) {
                                                   echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                               } else {
                                                   echo $rows[$i]->customer_last_name;
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 19%;">
                            <div class="cli-ent-xbox text-left p-left1">
                                <div class="cell-row">
                                    <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/2">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $rows[$i]->total_chat_all;   ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/2">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                            <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                        <?php echo count($rows[$i]->total_files_count); ?>
                                    </a></p>
                            </div>
                        </div>
                    </div> <!-- End Row -->
                <?php } ?>    
                <!-- </div> -->
                <!--QA IN Queue Section End Here --> 
            <?php } elseif ($rows[$i]->status_qa == "pendingrevision") { ?>
                <!--QA Pending review Section Start Here -->
                <!-- <div class="tab-pane content-datatable datatable-width" id="Qa_pending_review" role="tabpanel"> -->
                <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3'">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">Expected on</p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($rows[$i]->revisiondate));
                                    ?>
                                </p>
                                <!-- <p class="pro-b">May 30, 2018</p> -->
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 30%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3"><?php echo $rows[$i]->title; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                    </div>

                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="lightbluetext text-uppercase text-wrap">Pending Review</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#"><figure class="pro-circle-img">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure></a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                           ">
                                               <?php echo $rows[$i]->customer_first_name; ?>
                                               <?php
                                               if (strlen($rows[$i]->customer_last_name) > 5) {
                                                   echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                               } else {
                                                   echo $rows[$i]->customer_last_name;
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#" class="<?php echo $rows[$i]->id; ?>""><figure class="pro-circle-img">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure></a>
                                    </div>
                                    <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                            <?php echo $rows[$i]->designer_first_name; ?>
                                            <?php
                                            if (strlen($rows[$i]->designer_last_name) > 5) {
                                                echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                            } else {
                                                echo $rows[$i]->designer_last_name;
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <?php if ($rows[$i]->designer_assign_or_not == 0) { ?>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rows[$i]->id; ?>" data-designerid= "<?php echo $rows[$i]->designer_id; ?>">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $rows[$i]->total_chat_all;   ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13">  <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                        <?php echo count($rows[$i]->total_files_count); ?></a></p>
                            </div>
                        </div>
                    </div> 
                    <!-- End Row -->
                <?php } ?>
                <!-- </div> -->
                <!--QA Pending review Section End Here -->
            <?php } elseif ($rows[$i]->status_qa == "checkforapprove") { ?>
                <!--QA Pending Approval Section Start Here -->
                <!-- <div class="tab-pane content-datatable datatable-width" id="Qa_pending_approval" role="tabpanel"> -->
                <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 15%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4'">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">Delivered on</p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($rows[$i]->reviewdate));
                                    ?>
                                </p>
                                <!-- <p class="pro-b">May 30, 2018</p> -->
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 35%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4"><?php echo $rows[$i]->title; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                    </div>

                                    <div class="cell-col col-w1">
                                        <p class="neft text-center">
                                            <span class="red bluetext text-uppercase text-wrap">Pending-approval</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#"><figure class="pro-circle-img">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure></a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                           ">
                                               <?php echo $rows[$i]->customer_first_name; ?>
                                               <?php
                                               if (strlen($rows[$i]->customer_last_name) > 5) {
                                                   echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                               } else {
                                                   echo $rows[$i]->customer_last_name;
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#" class="<?php echo $rows[$i]->id; ?>""><figure class="pro-circle-img">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure></a>
                                    </div>
                                    <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                            <?php echo $rows[$i]->designer_first_name; ?>
                                            <?php
                                            if (strlen($rows[$i]->designer_last_name) > 5) {
                                                echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                            } else {
                                                echo $rows[$i]->designer_last_name;
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <?php if ($rows[$i]->designer_assign_or_not == 0) { ?>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rows[$i]->id; ?>" data-designerid= "<?php echo $rows[$i]->designer_id; ?>">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $rows[$i]->total_chat_all;    ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13">
                                            <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                        <?php echo count($rows[$i]->total_files_count); ?></a></p>
                            </div>
                        </div>
                    </div> 
                    <!-- End Row -->
                <?php } ?>
                <!-- </div> -->
                <!--QA Pending Approval Section End Here -->
            <?php } elseif ($rows[$i]->status_qa == "approved") { ?>
                <!--QA Completed Section Start Here -->
                <!-- <div class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel"> -->
                <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                    <!-- Start Row -->
                    <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/5'"style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 13%;">
                            <div class="cli-ent-xbox">
                                <h3 class="app-roved green">Approved on</h3>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($rows[$i]->approvddate));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 37%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/5"><?php echo $rows[$i]->title; ?></a></h3>
                                        <p class="pro-b">
                                            <?php echo substr($rows[$i]->description, 0, 30); ?>
                                        </p>
                                    </div>

                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#"><figure class="pro-circle-img">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure></a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                            <?php echo $rows[$i]->customer_first_name; ?>
                                            <?php
                                            if (strlen($rows[$i]->customer_last_name) > 5) {
                                                echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                            } else {
                                                echo $rows[$i]->customer_last_name;
                                            }
                                            ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="#"><figure class="pro-circle-img">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->profile_picture ?>" class="img-responsive">
                                                <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                <?php } ?>
                                            </figure></a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?> ">
                                            <?php echo $rows[$i]->designer_first_name; ?>
                                            <?php
                                            if (strlen($rows[$i]->designer_last_name) > 5) {
                                                echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                            } else {
                                                echo $rows[$i]->designer_last_name;
                                            }
                                            ?>

                                        </p>
                                        <p class="pro-b">Designer</p>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/5">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $rows[$i]->total_chat_all;    ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/5">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> 
                                            <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                        <?php echo count($rows[$i]->total_files_count); ?></a></p>
                            </div>
                        </div>
                    </div> <!-- End Row -->
                <?php } ?>
                <!-- </div> -->
                <!--QA Completed Section End Here -->
                <?php
            }
        }
    }

    public function search_ajax_client() {
        $dataArray = $this->input->get('name');
        if ($dataArray != "") {
            $data = $this->Request_model->get_all_clients_for_qa(array('keyword' => $dataArray), $_SESSION['user_id']);
            for ($i = 0; $i < sizeof($data); $i++) {
                $user = $this->Account_model->get_all_request_by_customer($data[$i]['id']);
                $active = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('active', 'disapprove'), $data[$i]['id']);
                $inque = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('assign'), $data[$i]['id']);
                $revision = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('disapprove'), $data[$i]['id']);
                $review = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('checkforapprove'), $data[$i]['id']);
                $complete = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('approved'), $data[$i]['id']);
                $user = $this->Account_model->get_all_active_view_request_by_customer_for_qa($activeClients[$i]['id']);
                $data[$i]['created'] = $this->onlytimezone($data[$i]['created']);
                $data[$i]['active'] = $active;
                $data[$i]['inque_request'] = $inque;
                $data[$i]['revision_request'] = $revision;
                $data[$i]['review_request'] = $review;
                $data[$i]['complete_request'] = $complete;
                $data[$i]['no_of_request'] = sizeof($user);
                $data[$i]['no_of_request'] = sizeof($user);

                $user = $this->Account_model->get_all_active_request_by_customer($data[$i]['id']);
                $data[$i]['active_request'] = sizeof($user);

                $data[$i]['designer_name'] = "";
                if ($data[$i]['designer_id'] != 0) {
                    $data[$i]['designer'] = $this->Request_model->get_user_by_id($data[$i]['designer_id']);
                } else {
                    $data[$i]['designer'] = "";
                }
                $designer_name = $this->Account_model->getuserbyid($data[$i]['designer_id']);
                if (!empty($designer_name)) {
                    $data[$i]['designer_name'] = $designer_name[0]['first_name'];
                }
            }
        } else {
            $data = array();
            $mydesigner = $this->Request_model->get_designer_list_for_qa();
            if ($mydesigner) {
                $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                for ($i = 0; $i < sizeof($data); $i++) {
                    $user = $this->Account_model->get_all_request_by_customer($data[$i]['id']);
                    $data[$i]['no_of_request'] = sizeof($user);

                    $user = $this->Account_model->get_all_active_request_by_customer($data[$i]['id']);
                    $data[$i]['active_request'] = sizeof($user);

                    $data[$i]['designer_name'] = "";
                    $data[$i]['designer_name'] = "";
                    if ($data[$i]['designer_id'] != 0) {
                        $data[$i]['designer'] = $this->Request_model->get_user_by_id($data[$i]['designer_id']);
                    } else {
                        $data[$i]['designer'] = "";
                    }
                    $designer_name = $this->Account_model->getuserbyid($data[$i]['designer_id']);
                    if (!empty($designer_name)) {
                        $data[$i]['designer_name'] = $designer_name[0]['first_name'];
                    }
                }
            }
        }
        for ($i = 0; $i < sizeof($data); $i++) {
            ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 60%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" style="width: 80px; padding-right: 15px;">
                                <?php if (($data[$i]['profile_picture'] != "") && (file_exists(base_url() . "uploads/profile_picture/" . $data[$i]['profile_picture']))) { ?>
                                    <figure class="cli-ent-img circle one">
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $data[$i]['profile_picture']; ?>" class="img-responsive one">
                                    </figure>
                                <?php } else { ?>
                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 20px;">
                                        <?php echo ucwords(substr($data[$i]['first_name'], 0, 1)) . ucwords(substr($data[$i]['last_name'], 0, 1)); ?>
                                    </figure>
                                <?php } ?>
                            </div>

                            <div class="cell-col" style="width: 185px;">
                                <h3 class="pro-head-q"><a href="<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>"><?php echo $data[$i]['first_name'] . " " . $data[$i]['last_name'] ?></a></h3>
                                <p class="pro-a"><?php echo $data[$i]['email']; ?></p>
                            </div>

                            <div class="cell-col">
                                <p class="neft">
                                    <span class="blue text-uppercase">
                                        <?php
                                        if ($data[$i]['plan_turn_around_days'] == "1") {
                                            echo "Premium Member";
                                        } elseif ($data[$i]['plan_turn_around_days'] == "3") {
                                            echo "Standard Member";
                                        } else {
                                            echo "No Member";
                                        }
                                        ?>
                                    </span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Sign Up Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b"><?php echo date("M/d/Y", strtotime($data[$i]['created'])) ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Next Billing Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b">
                            <?php
                            $date = "";
                            if (checkdate(date("m", strtotime($data[$i]['modified'])), date("d", strtotime($data[$i]['modified'])), date("Y", strtotime($data[$i]['modified'])))) {
                                $over_date = "";
                                if ($data[$i]['plan_turn_around_days']) {
                                    $date = date("Y-m-d", strtotime($data[$i]['modified']));
                                    $time = date("h:i:s", strtotime($data[$i]['modified']));
                                    $data[$i]['plan_turn_around_days'] = "2";
                                    $date = date("m/d/Y g:i a", strtotime($date . " " . $data[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                    $diff = date_diff(date_create(date("Y-m-d", strtotime($data[$i]['modified']))), date_create($date));
                                }
                            }
                            $current_date = strtotime(date("Y-m-d"));
                            $now = time(); // or your date as well

                            $expiration_date = strtotime($date);
                            $datediff = $now - $expiration_date;
                            $date_due_day = round($datediff / (60 * 60 * 24));
                            $color = "";
                            $text_color = "white";
                            if ($date_due_day == 0) {
                                $date_due_day = "Due  </br>Today";
                                $color = "#f7941f";
                            } else
                            if ($date_due_day == (-1)) {
                                $date_due_day = "Due  </br>Tomorrow";
                                $color = "#98d575";
                            } else
                            if ($date_due_day > 0) {
                                $date_due_day = "Over Due";
                                $color = "red";
                            } else if ($date_due_day < 0) {
                                $date_due_day = "Due from </br>" . number_format($date_due_day) . " days";
                                $text_color = "black";
                            }
                            ?>
                            <?php echo date_format(date_create($date), "M /d /Y"); ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 5%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active Requests</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $data[$i]['active']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 5%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">In Queue Requests</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $data[$i]['inque_request']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 5%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Pending Approval Requests</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $data[$i]['review_request']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="min-width: 171px; max-width: 171px; width: 10%;">
                    <?php if (!empty($data[$i]['designer'])) { ?>
                        <div class="cli-ent-xbox">
                            <div class="cell-row">
                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                    <a href="">
                                        <?php if ($data[$i]['designer'][0]['profile_picture'] != "") { ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $data[$i]['designer'][0]['profile_picture']; ?>" class="img-responsive">
                                            </figure>
                                        <?php } else { ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                            </figure>
                                        <?php } ?>
                                    </a>
                                </div>
                                <div class="cell-col">
                                    <p class="text-h text-wrap" title="<?php echo $data[$i]['designer'][0]['first_name'] . " " . $data[$i]['designer'][0]['last_name']; ?>
                                       ">
                                           <?php echo $data[$i]['designer'][0]['first_name']; ?>
                                           <?php
                                           if (strlen($data[$i]['designer'][0]['last_name']) > 5) {
                                               echo ucwords(substr($data[$i]['designer'][0]['last_name'], 0, 1));
                                           } else {
                                               echo $data[$i]['designer'][0]['last_name'];
                                           }
                                           ?>

                                    </p>
                                    <p class="pro-b">Designer</p>
                                    <p class="space-a"></p>
                                    <a href="#" class="addde-signersk1 permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $data[$i]['id']; ?>">
                                        <span class="sma-red">+</span> Add Designer
                                    </a>
                                </div>
                            </div> 
                        </div>
                    <?php } else { ?>
                        <div class="cli-ent-xbox">
                            <a href="#" class="upl-oadfi-le noborder permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign" data-customerid="<?php echo $data[$i]['id']; ?>">
                                <span class="icon-crss-3">
                                    <span class="icon-circlxx55 margin5">+</span>
                                    <p class="attachfile">Assign designer<br> permanently</p>
                                </span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- End Row -->
            <?php
        }
    }

    public function search_ajax_designer() {
        $dataArray = $this->input->get('name');
        if ($dataArray != "") {
            $designers = $this->Request_model->get_all_designer_for_qa_search_ajax(array('keyword' => $dataArray), $_SESSION['user_id']);

            for ($i = 0; $i < sizeof($designers); $i++) {
                $designers[$i]['total_rating'] = $this->Request_model->get_average_rating($designers[$i]['id']);

                $user = $this->Account_model->get_all_request_by_designer($designers[$i]['id']);
                $designers[$i]['handled'] = sizeof($user);

                $user = $this->Account_model->get_all_active_request_by_designer($designers[$i]['id']);
                $designers[$i]['active_request'] = sizeof($user);
            }
        } else {
            $designers = $this->Request_model->get_designer_list_for_qa("array");

            for ($i = 0; $i < sizeof($designers); $i++) {
                $designers[$i]['created'] = $this->onlytimezone($designers[$i]['created']);
                $designers[$i]['total_rating'] = $this->Request_model->get_average_rating($designers[$i]['id']);

                $user = $this->Account_model->get_all_request_by_designer($designers[$i]['id']);
                $designers[$i]['handled'] = sizeof($user);

                $user = $this->Account_model->get_all_active_request_by_designer($designers[$i]['id']);
                $designers[$i]['active_request'] = sizeof($user);
            }
        }
        for ($i = 0; $i < sizeof($designers); $i++) {
            ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'" style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 10%;">
                    <?php
                    if ($designers[$i]['profile_picture']) {
                        ?>
                        <figure class="cli-ent-img circle one">
                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $designers[$i]['profile_picture']; ?>" class="img-responsive one">
                        </figure>
                    <?php } else { ?>
                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 20px;font-family: GothamPro-Medium;">
                            <?php echo ucwords(substr($designers[$i]['first_name'], 0, 1)) . ucwords(substr($designers[$i]['last_name'], 0, 1)); ?>
                        </figure>
                    <?php } ?>
                </div>
                <div class="cli-ent-col td" style="width: 35%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">

                            <div class="cell-col">
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>"><?php echo $designers[$i]['first_name'] . " " . $designers[$i]['last_name'] ?></a></h3>
                                <p class="pro-b"><?php echo $designers[$i]['email']; ?></p>
                            </div>

                            <!--<div class="cell-col col-w1">
                                <p class="neft text-center"><span class="green text-uppercase text-wrap">In-Progress</span></p>
                            </div>-->
                        </div>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 15%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Sign Up Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b"><?php echo date("M /d /Y", strtotime($designers[$i]['created'])) ?></p>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Requests Being Handled</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="green text-uppercase"><?php echo $designers[$i]['handled']; ?></span></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active Requests</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $designers[$i]['active_request']; ?></span></p>
                    </div>
                </div>
            </div> <!-- End Row -->
            <?php
        }
    }

    public function search_ajax_customer_project() {
        $dataArray = $this->input->get('title');
        $dataStatus = $this->input->get('status');
        $datauserid = $this->input->get('id');
        $dataStatus = explode(",", $dataStatus);
        $rows = $this->Request_model->GetAutocomplete_qa_customer(array('keyword' => $dataArray), $dataStatus, $datauserid, "customer");
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rows[$i]->total_chat = $this->Request_model->get_chat_number($rows[$i]->id, $rows[$i]->customer_id, $rows[$i]->designer_id, "qa");
            $rows[$i]->total_chat_all = $this->Request_model->get_total_chat_number($rows[$i]->id, $_SESSION['user_id'], "qa");
            $rows[$i]->total_files = $this->Request_model->get_files_count($rows[$i]->id, $rows[$i]->designer_id, "qa");
            $getfileid = $this->Request_model->get_attachment_files($rows[$i]->id, "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            if ($rows[$i]->status == "active" || $rows[$i]->status == "disapprove") {
                if ($rows[$i]->expected_date == '' || $rows[$i]->expected_date == NULL) {
                    $rows[$i]->expected = $this->myfunctions->check_timezone($rows[$i]->latest_update, $rows[$i]->current_plan_name);
                } else {
                    $rows[$i]->expected = $this->onlytimezone($rows[$i]->expected_date);
                }
            } elseif ($rows[$i]->status == "checkforapprove") {
                //$rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->latest_update);
                $rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->modified);
            } elseif ($rows[$i]->status == "approved") {
                $rows[$i]->approvddate = $this->onlytimezone($rows[$i]->approvaldate);
            }
            if ($rows[$i]->status_qa == "pendingrevision") {
                $rows[$i]->revisiondate = $this->onlytimezone($rows[$i]->modified);
            }
            $rows[$i]->comment_count = $commentcount;
            $rows[$i]->total_files_count = $this->Request_model->get_files_count_all($rows[$i]->id);
        }
        for ($i = 0; $i < sizeof($rows); $i++) {
            if ($rows[$i]->status == "disapprove" || $rows[$i]->status == "active" || $rows[$i]->status == "assign") {
                ?>
                <!--QA Active Section Start Here -->
                <div class="tab-pane active content-datatable datatable-width" id="Qa_client_active" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) {
                        ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/1'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 12%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">
                                        <?php
                                        if ($rows[$i]->status == "active") {
                                            echo "Expected on";
                                        } elseif ($rows[$i]->status == "disapprove") {
                                            echo "Expected on";
                                        } elseif ($rows[$i]->status == "assign") {
                                            echo "In-Queue";
                                        }
                                        ?>
                                    </p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php
                                        if ($rows[$i]->status == "active") {
                                            echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                        } elseif ($rows[$i]->status == "disapprove") {
                                            echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                        }
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 48%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $rows[$i]->id; ?>/1"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 50); ?></p>
                                        </div>
                    <!--                                        <div class="cell-col"><?php // echo isset($rows[$i]->priority) ? $rows[$i]->priority : '';       ?></div>-->
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center">
                                                <?php if ($rows[$i]->status == "active" || $rows[$i]->status == "checkforapprove") { ?>
                                                    <span class="green text-uppercase">In-Progress</span>
                                                <?php } elseif ($rows[$i]->status == "disapprove") { ?>
                                                    <span class="red orangetext text-uppercase">REVISION</span>
                                                <?php } elseif ($rows[$i]->status == "assign") { ?>
                                                    <span class="gray text-uppercase">In-Queue</span>
                                                <?php } ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 20%;">
                                <div class="cli-ent-xbox text-left p-left1">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                            <a href="<?php echo base_url() . "qa/dashboard/view_project/" . $rows[$i]->id; ?>/1">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $rows[$i]->profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rows[$i]->designer_first_name, 0, 1)) . ucwords(substr($rows[$i]->designer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h">
                                                <?php
                                                if ($rows[$i]->designer_first_name) {
                                                    echo $rows[$i]->designer_first_name;
                                                } else {
                                                    echo "No Designer";
                                                }
                                                ?>  
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $rows[$i]->id; ?>/1">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                                <?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?>
                                            </span>
                                            <?php //echo $rows[$i]->total_chat_all; ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $rows[$i]->id; ?>/1">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Active Section End Here -->
            <?php } elseif ($rows[$i]->status == "checkforapprove") { ?>
                <!--QA Pending Approval Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_client_pending" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) {
                        ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/2'"  style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">
                                        Delivered on
                                    </p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rows[$i]->reviewdate));
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col">
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $rows[$i]->id; ?>/2"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 50); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center">
                                                <span class="red bluetext text-uppercase text-wrap">Pending-approval</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url() . "qa/dashboard/view_project/" . $rows[$i]->id; ?>/2">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->customer_profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rows[$i]->customer_first_name, 0, 1)) . ucwords(substr($rows[$i]->customer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                <?php echo $rows[$i]->customer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->customer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->customer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url() . "qa/dashboard/view_project/" . $rows[$i]->id; ?>/2">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rows[$i]->designer_first_name, 0, 1)) . ucwords(substr($rows[$i]->designer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $rows[$i]->id; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span>
                                            <?php //echo $rows[$i]->total_chat_all;   ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $rows[$i]->id; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Pending Approval Section End Here -->
            <?php } elseif ($rows[$i]->status == "approved") { ?>
                <!--QA Completed Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_client_completed" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 13%;">
                                <div class="cli-ent-xbox">
                                    <h3 class="app-roved green">Approved on</h3>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rows[$i]->approvddate));
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 37%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col">
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 50); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3">
                                                <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->customer_profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rows[$i]->customer_first_name, 0, 1)) . ucwords(substr($rows[$i]->customer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title=" <?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                <?php echo $rows[$i]->customer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->customer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->customer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3">
                                                <?php if ($rows[$i]->profile_picture != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->profile_picture; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 15px; width: 50px; height: 50px;">
                                                        <?php echo ucwords(substr($rows[$i]->designer_first_name, 0, 1)) . ucwords(substr($rows[$i]->designer_last_name, 0, 1)); ?>
                                                    </figure>
                                                <?php } ?></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span>
                                            <?php //echo $rows[$i]->total_chat_all;  ?></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div><!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Completed Section End Here -->
                <?php
            }
        }
    }

    public function search_ajax_designer_project() {
        $dataArray = $this->input->get('title');
        $dataStatus = $this->input->get('status');
        $datauserid = $this->input->get('id');
        $dataStatus = explode(",", $dataStatus);
        $rows = $this->Request_model->GetAutocomplete_designer(array('keyword' => $dataArray), $dataStatus, $datauserid, "designer");
        for ($i = 0; $i < sizeof($rows); $i++) {
            $rows[$i]->total_chat = $this->Request_model->get_chat_number($rows[$i]->id, $rows[$i]->customer_id, $rows[$i]->designer_id, "qa");
            $rows[$i]->total_chat_all = $this->Request_model->get_total_chat_number($rows[$i]->id, $_SESSION['user_id'], "qa");
            $rows[$i]->total_files = $this->Request_model->get_files_count($rows[$i]->id, $rows[$i]->designer_id, "qa");
            $getfileid = $this->Request_model->get_attachment_files($rows[$i]->id, "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "qa");
            }
            if ($rows[$i]->status == "active" || $rows[$i]->status == "disapprove") {
                if ($rows[$i]->expected_date == '' || $rows[$i]->expected_date == NULL) {
                    $rows[$i]->expected = $this->myfunctions->check_timezone($rows[$i]->latest_update, $rows[$i]->current_plan_name);
                } else {
                    $rows[$i]->expected = $this->onlytimezone($rows[$i]->expected_date);
                }
            } elseif ($rows[$i]->status == "checkforapprove") {
                $rows[$i]->reviewdate = $this->onlytimezone($rows[$i]->modified);
            } elseif ($rows[$i]->status == "approved") {
                $rows[$i]->approvddate = $this->onlytimezone($rows[$i]->approvaldate);
            }
            if ($rows[$i]->status_qa == "pendingrevision") {
                $rows[$i]->revisiondate = $this->onlytimezone($rows[$i]->modified);
            }
            $rows[$i]->comment_count = $commentcount;
            $rows[$i]->total_files_count = $this->Request_model->get_files_count_all($rows[$i]->id);
        }
        for ($i = 0; $i < sizeof($rows); $i++) {
            if ($rows[$i]->status_designer == "disapprove" || $rows[$i]->status_designer == "active") {
                ?>
                <!--QA Active Section Start Here -->
                <div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/1'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 12%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">
                                        <?php
                                        if ($rows[$i]->status == "active") {
                                            echo "Expected on";
                                        } elseif ($rows[$i]->status_qa == "disapprove") {
                                            echo "Expected on";
                                        }
                                        ?>
                                    </p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php
                                        if ($rows[$i]->status == "active") {
                                            echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                        } elseif ($rows[$i]->status_qa == "disapprove") {
                                            echo date('M d, Y h:i A', strtotime($rows[$i]->expected));
                                        }
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 48%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/1"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center">
                                                <?php if ($rows[$i]->status_designer == "active") { ?>
                                                    <span class="green text-uppercase text-wrap">In-Progress</span>
                                                <?php } elseif ($rows[$i]->status_designer == "disapprove" && $rows[$i]->who_reject == 1) { ?>
                                                    <span class="red orangetext text-uppercase text-wrap">REVISION</span>
                                                <?php } elseif ($rows[$i]->status_designer == "disapprove" && $rows[$i]->who_reject == 0) { ?>
                                                    <span class="red text-uppercase text-wrap">Quality Revision</span>
                                                <?php } ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 20%;">
                                <div class="cli-ent-xbox text-left p-left1">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                            <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/1">
                                                <figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>

                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/1">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/1">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Active Section End Here -->
            <?php } elseif ($rows[$i]->status_designer == "assign" || $rows[$i]->status_designer == "pending") { ?>
                <!--QA InQueue Section Start Here -->
                <div class="tab-pane active content-datatable datatable-width" id="Qa_active" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/2'" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 12%;">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">In Queue</p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 48%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/2"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="gray text-uppercase">IN-queue</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 20%;">
                                <div class="cli-ent-xbox text-left p-left1">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 15px;">
                                            <a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/2">
                                                <figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>

                                            </p>
                                            <p class="pro-b">Designer</p>
                                            <p class="space-a"></p>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $rows[$i]->id; ?>" data-designerid= "<?php echo $rows[$i]->designer_id; ?>">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 10%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/2">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA InQueue Section End Here -->
            <?php } elseif ($rows[$i]->status_designer == "pendingrevision") { ?>
                <!--QA Pending Review Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_pending_review" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3'">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">Expected on</p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rows[$i]->revisiondate)); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center">
                                                <span class="lightbluetext text-uppercase text-wrap">Pending Review</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href=""><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                               ">
                                                   <?php echo $rows[$i]->customer_first_name; ?>
                                                   <?php
                                                   if (strlen($rows[$i]->customer_last_name) > 5) {
                                                       echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                   } else {
                                                       echo $rows[$i]->customer_last_name;
                                                   }
                                                   ?>

                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="" class="<?php echo $rows[$i]->id; ?>""><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/3">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> 
                        <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Pending Review Section End Here -->
            <?php } elseif ($rows[$i]->status_designer == "checkforapprove") { ?>
                <!--QA Pending Approval Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_pending_approval" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4'">
                                <div class="cli-ent-xbox">
                                    <p class="pro-a">Delivered on</p>
                                    <p class="space-a"></p>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rows[$i]->reviewdate)); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 40%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b"><?php echo substr($rows[$i]->description, 0, 30); ?></p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center">
                                                <span class="red bluetext text-uppercase text-wrap">Pending-approval</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4'">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href=""><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>
                                               ">
                                                   <?php echo $rows[$i]->customer_first_name; ?>
                                                   <?php
                                                   if (strlen($rows[$i]->customer_last_name) > 5) {
                                                       echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                   } else {
                                                       echo $rows[$i]->customer_last_name;
                                                   }
                                                   ?>

                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="" class="<?php echo $rows[$i]->id; ?>""><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col <?php echo $rows[$i]->id; ?>">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?>">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>'">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/4">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> 
                        <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Pending Approval Section End Here -->
            <?php } elseif ($rows[$i]->status_designer == "approved") { ?>
                <!--QA Completed Section Start Here -->
                <div class="tab-pane content-datatable datatable-width" id="Qa_completed" role="tabpanel">
                    <?php for ($i = 0; $i < sizeof($rows); $i++) { ?>
                        <!-- Start Row -->
                        <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/5'"style="cursor: pointer;">
                            <div class="cli-ent-col td" style="width: 13%;">
                                <div class="cli-ent-xbox">
                                    <h3 class="app-roved green">Approved on</h3>
                                    <p class="pro-b">
                                        <?php echo date('M d, Y h:i A', strtotime($rows[$i]->approvddate)); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 37%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">

                                        <div class="cell-col" >
                                            <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/5"><?php echo $rows[$i]->title; ?></a></h3>
                                            <p class="pro-b">
                                                <?php echo substr($rows[$i]->description, 0, 30); ?>
                                            </p>
                                        </div>

                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="green text-uppercase text-wrap">completed</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href=""><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->customer_profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->customer_profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->customer_first_name . " " . $rows[$i]->customer_last_name; ?>">
                                                <?php echo $rows[$i]->customer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->customer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->customer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->customer_last_name;
                                                }
                                                ?>

                                            </p>
                                            <p class="pro-b">Client</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="cli-ent-col td" style="width: 16%;">
                                <div class="cli-ent-xbox text-left">
                                    <div class="cell-row">
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href=""><figure class="pro-circle-img">
                                                    <?php if ($rows[$i]->profile_picture != "") { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $rows[$i]->profile_picture ?>" class="img-responsive">
                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    <?php } ?>
                                                </figure></a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap" title="<?php echo $rows[$i]->designer_first_name . " " . $rows[$i]->designer_last_name; ?> ">
                                                <?php echo $rows[$i]->designer_first_name; ?>
                                                <?php
                                                if (strlen($rows[$i]->designer_last_name) > 5) {
                                                    echo ucwords(substr($rows[$i]->designer_last_name, 0, 1));
                                                } else {
                                                    echo $rows[$i]->designer_last_name;
                                                }
                                                ?>

                                            </p>
                                            <p class="pro-b">Designer</p>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/5">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($rows[$i]->total_chat + $rows[$i]->comment_count != 0) { ?>
                                                    <span class="numcircle-box">
                                                        <?php echo $rows[$i]->total_chat + $rows[$i]->comment_count; ?>
                                                    </span>
                                                <?php } ?></span></a></p>
                                </div>
                            </div>
                            <div class="cli-ent-col td" style="width: 9%;">
                                <div class="cli-ent-xbox text-center">
                                    <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $rows[$i]->id; ?>/5">
                                            <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($rows[$i]->total_files) != 0) { ?>
                                                    <span class="numcircle-box"><?php echo count($rows[$i]->total_files); ?></span><?php } ?></span>
                                            <?php echo count($rows[$i]->total_files_count); ?></a></p>
                                </div>
                            </div>
                        </div> <!-- End Row -->
                    <?php } ?>
                </div>
                <!--QA Completed Section End Here -->
                <?php
            }
        }
    }

    public function downloadzip() {
        if (isset($_POST)) {
            $file_name = FS_PATH;
            $source_filename = substr($_POST['srcfile'], strrpos($_POST['srcfile'], '/') + 1);
            $this->load->helper('download');
            $path = file_get_contents(FS_PATH . $_POST['srcfile']); // get file name
            $name = $source_filename; // new name for your file
            force_download($name, $path);
        }
    }

    public function gz_delete_files() {
        if (isset($_POST)) {
            $login_user_id = $this->load->get_var('login_user_id'); 
            $id = $_POST['requests_files'];
            $request_id = $_POST['request_id'];
            $delete = $this->Admin_model->delete_data('request_files', $id);
            if ($delete) {
                if (array_key_exists('prefile', $_POST)) {
                    $src_filename = $_POST['srcfile'];
                    $pre_filename = $_POST['prefile'];
                    $thumb_prefile = $_POST['thumb_prefile'];
                    $this->myfunctions->capture_project_activity($request_id,$id,'','delete_draft_by_qa','gz_delete_files',$login_user_id,'0','1','1');
                    redirect(base_url() . 'qa/dashboard/view_project/' . $request_id . '/1');
                }
            }
        }
    }

    public function onlytimezone($date_zone) {
        //echo $_SESSION['timezone'];
        $nextdate = $this->myfunctions->onlytimezoneforall($date_zone);
        //echo $nextdate;exit;
        /* $nextdate = $date_zone; */
        return $nextdate;
    }

    public function assign_designer_for_customer() {
        $login_user_id = $this->load->get_var('login_user_id');
        $success_add = $this->Admin_model->update_data("users", array('designer_id' => $_POST['assign_designer']), array("id" => $_POST['customer_id']));
        $all_request = $this->Request_model->get_request_by_customer_id($_POST['customer_id']);
        foreach ($all_request as $request) {
            $request_id[] = $request['id'];
            if($request['designer_id'] != '0'){
                $this->myfunctions->capture_project_activity($request['id'],'',$request['designer_id'],'reassign_designer','assign_designer_for_customer',$login_user_id);
            }else{
                $this->myfunctions->capture_project_activity($request['id'],'','','assign_designer','assign_designer_for_customer',$login_user_id);  
            }
        }
        $this->Request_model->update_request_admin($request_id, array('designer_id' => $_POST['assign_designer']));
        if ($success_add) {
            $this->myfunctions->capture_project_activity('','',$_POST['assign_designer'],'assign_paramanent_designer','assign_designer_for_customer',$login_user_id,'','','',$_POST['customer_id']);
            $this->session->set_flashdata('message_success', "Assigned designer Successfully", 5);
            redirect(base_url() . "qa/dashboard/view_clients");
        }
    }

//    public function change_project_status() {
//        $request = $this->Request_model->get_request_by_id($_POST['request_id']);
//        $customer_data = $this->Admin_model->getuser_data($request[0]['customer_id']);
//        $subcat_data = $this->Category_model->get_category_byID($request[0]['subcategory_id']);
//        $admin_id = $this->load->get_var('admin_id');
//        $login_user_id = $this->load->get_var('login_user_id');
//        $getsubscriptionplan = $this->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
//        if ($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])) {
//            $turn_around_days = $customer_data[0]['turn_around_days'];
//        } else {
//            $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
//        }
//        $expected = $this->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $customer_data[0]['plan_name'], $request[0]['category_bucket'], $subcat_data[0]['timeline'], $turn_around_days);
//        //echo "<pre>";print_r($customer_data);exit;
//        if ($request[0]['status'] == 'assign') {
//            if (isset($_POST['value'])) {
//                $deletedpriority = isset($request[0]['priority']) ? $request[0]['priority'] : '';
//                $user_id = isset($request[0]['customer_id']) ? $request[0]['customer_id'] : '';
//                $priorfrom = $deletedpriority + 1;
//                if ($request[0]['latest_update'] == '' || $request[0]['latest_update'] == null) {
//                    $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
//                } else {
//                    $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
//                }
//                $this->Admin_model->update_priority_after_approved_qa(1, $priorfrom, $user_id);
//            }
//        }
//
//        if ($_POST['value'] == "assign") {
//            $priority = $this->Welcome_model->priority($request[0]['customer_id']);
//            $data2['priority'] = $priority[0]['priority'] + 1;
//            $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => $data2['priority']), array("id" => $_POST['request_id']));
//            if ($success_add) {
//                echo "1";
//            } else {
//                echo "0";
//            }
//        } elseif ($_POST['value'] == "active") {
//            $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
//            $message_count = $this->Request_model->request_discussions_count_for_qa_message($_POST['request_id']);
//            if ($message_count == 0) {
//                $this->myfunctions->SendQAmessagetoInprogressReq($_POST['request_id'], $customer_data[0]['qa_id'], $request[0]['customer_id']);
//                //$this->send_email_when_inqueue_to_inprogress($request[0]['title'], $request[0]['id'], $customer_data[0]['email']);
//            }
//            if ($success_add) {
//                //NOTE : i think below function will never run 
//                // because project status already updated but in below function it is getting only assign status requests to update
//                // $this->Admin_model->update_priority_after_approved_qa('1', $request[0]['priority']);
//                echo "1";
//            } else {
//                echo "0";
//            }
//        } elseif ($_POST['value'] == "disapprove") {
//
//            $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
//            if ($success_add) {
//                //NOTE : i think below function will never run 
//                // because project status already updated but in below function it is getting only assign status requests to update
//                // $this->Admin_model->update_priority_after_approved_qa('1', $request[0]['priority']);
//
//                $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "\"" . $request[0]['title'] . "\" Project goes into revision  from " . $request[0]['status'] . "..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
//                $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "\"" . $request[0]['title'] . "\" Project goes into revision  from " . $request[0]['status_admin'] . "..!", "admin/dashboard/view_request/" . $_POST['request_id'], $admin_id);
//                $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "\"" . $request[0]['title'] . "\" Project goes into revision  from " . $request[0]['status_designer'] . "..!", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
//
//                echo "1";
//            } else {
//                echo "0";
//            }
//        } elseif ($_POST['value'] == "checkforapprove") {
//            $success_add = $this->Admin_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), 'status' => $_POST['value'], 'latest_update' => date("Y-m-d H:i:s"), 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1,), array("id" => $_POST['request_id']));
//            //$this->send_email_after_qa_approval($request[0]['title'], $request[0]['id'], $customer_data[0]['email']);
//            if ($success_add) {
//                ///$this->Admin_model->update_priority_after_approved_qa('1',$request[0]['priority']);
//                $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Upload New File For \"" . $request[0]['title'] . "\" . Please Check And Approve..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
//                $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Approved your design For \"" . $request[0]['title'] . "\"", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
//                echo "1";
//            } else {
//                echo "0";
//            }
//        } elseif ($_POST['value'] == "approved") {
//            $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "approvaldate" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
//            if ($success_add) {
//                $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Approved..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
//                $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Approved..!", "admin/dashboard/view_request/" . $_POST['request_id'], $admin_id);
//                $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Approved..!", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
//                echo "1";
//            } else {
//                echo "0";
//            }
//        } else {
//            $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'latest_update' => date("Y-m-d H:i:s"), 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1,), array("id" => $_POST['request_id']));
//            if ($success_add) {
//                ///$this->Admin_model->update_priority_after_approved_qa('1',$request[0]['priority']);
//                echo "1";
//            } else {
//                echo "0";
//            }
//        }
//    }
    
    public function change_project_status() {
            $this->myfunctions->checkloginuser("qa");
            $login_user_id = $this->load->get_var('login_user_id');
            $request = $this->Request_model->get_request_by_id($_POST['request_id']);
            $subcat_data = $this->Category_model->get_category_byID($request[0]['subcategory_id']);
            $customer_data = $this->Admin_model->getuser_data($request[0]['customer_id']);
            $getsubscriptionplan = $this->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
            $getlatestfiledraft = $this->Request_model->get_latestdraft_file($_POST['request_id']);
            if ($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])) {
                $turn_around_days = $customer_data[0]['turn_around_days'];
            } else {
                $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
            }
            $expected = $this->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $customer_data[0]['plan_name'], $request[0]['category_bucket'], $subcat_data[0]['timeline'], $turn_around_days);
            if ($request[0]['status'] == 'assign') {
                if (isset($_POST['value'])) {
                    $deletedpriority = isset($request[0]['priority']) ? $request[0]['priority'] : '';
                    $user_id = isset($request[0]['customer_id']) ? $request[0]['customer_id'] : '';
                    $priorfrom = $deletedpriority + 1;
                    if ($request[0]['latest_update'] == '' || $request[0]['latest_update'] == null) {
                        $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    } else {
                        $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    }
                    $this->Admin_model->update_priority_after_approved_qa(1, $priorfrom, $user_id);
                }
            }

            if ($_POST['value'] == "assign") {
                $priority = $this->Welcome_model->priority($request[0]['customer_id']);
                $data2['priority'] = $priority[0]['priority'] + 1;
                $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => $data2['priority'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                   $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_qa'].'_to_'.$_POST['value'],'qa_change_project_status',$login_user_id);
                    echo "1";
                } else {
                    echo "0";
                }
            } elseif ($_POST['value'] == "active") {
                $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                $ActiveReq = $this->myfunctions->CheckActiveRequest($request[0]['customer_id']);
                if ($ActiveReq == 1) {
                    $this->myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','change_project_status');
                }
                $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_qa'].'_to_'.$_POST['value'],'qa_change_project_status',$login_user_id);
                $message_count = $this->Request_model->request_discussions_count_for_qa_message($_POST['request_id']);
                if ($message_count == 0) {
                    $this->myfunctions->SendQAmessagetoInprogressReq($_POST['request_id'], $customer_data[0]['qa_id'], $request[0]['customer_id']);
                }
                if ($success_add) {
                    echo "1";
                } else {
                    echo "0";
                }
            } elseif ($_POST['value'] == "disapprove") {
                $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                    $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_qa'].'_to_'.$_POST['value'],'qa_change_project_status',$login_user_id);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "\"" . $request[0]['title'] . "\" Project goes into revision  from " . $request[0]['status'] . "..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "\"" . $request[0]['title'] . "\" Project goes into revision  from " . $request[0]['status_qa'] . "..!", "qa/dashboard/view_project/" . $_POST['request_id'], $customer_data[0]['qa_id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "\"" . $request[0]['title'] . "\" Project goes into revision  from " . $request[0]['status_designer'] . "..!", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
                   echo "1";
                } else {
                    echo "0";
                }
            } elseif ($_POST['value'] == "checkforapprove") {
                $success_add = $this->Admin_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), 'status' => $_POST['value'], 'latest_update' => date("Y-m-d H:i:s"), 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1,), array("id" => $_POST['request_id']));
                if ($success_add) {
                    $ActiveReq = $this->myfunctions->CheckActiveRequest($request[0]['customer_id']);
                     if ($ActiveReq == 1) {
                         $this->myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','change_project_status');
                     }
                     $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_qa'].'_to_'.$_POST['value'],'qa_change_project_status',$login_user_id);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Upload New File For \"" . $request[0]['title'] . "\" . Please Check And Approve..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Approved your design For \"" . $request[0]['title'] . "\"", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
                    echo "1";
                } else {
                    echo "0";
                }
            } elseif ($_POST['value'] == "approved") {
                $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "approvaldate" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Approved..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Approved..!", "qa/dashboard/view_project/" . $_POST['request_id'], $customer_data[0]['qa_id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Approved..!", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
                    $ActiveReq = $this->myfunctions->CheckActiveRequest($request[0]['customer_id']);
                    if ($ActiveReq == 1) {
                        $this->myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','change_project_status');
                    }
                    $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_qa'].'_to_'.$_POST['value'],'qa_change_project_status',$login_user_id);
                    echo "1";
                } else {
                    echo "0";
                }
            }elseif ($_POST['value'] == "cancel") {
                $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "approvaldate" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Cancelled..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Cancelled..!", "qa/dashboard/view_project/" . $_POST['request_id'], $customer_data[0]['qa_id']);
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Cancelled..!", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
                    $ActiveReq = $this->myfunctions->CheckActiveRequest($request[0]['customer_id']);
                    if ($ActiveReq == 1) {
                        $this->myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','change_project_status');
                    }
                    $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_qa'].'_to_'.$_POST['value'],'qa_change_project_status',$login_user_id);
                    echo "1";
                } else {
                    echo "0";
                }
            } else {
                $success_add = $this->Admin_model->update_data("requests", array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "who_reject" => 1, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                $this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_qa'].'_to_'.$_POST['value'],'qa_change_project_status',$login_user_id);
                    echo "1";
                } else {
                    echo "0";
                }
            }
        }

    public function send_email_after_qa_approval($project_title, $id, $email) {
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $query1 = $this->db->select('first_name')->from('users')->where('email', $email)->get();
        $fname = $query1->result();
        $data = array(
            'title' => $project_title,
            'project_id' => $id,
            'fname' => isset($fname[0]->first_name) ? $fname[0]->first_name : ''
        );
        $body = $this->load->view('emailtemplate/qa_approved_template', $data, TRUE);
        $receiver = $email;

        $subject = 'Design is Ready for Review.';

        $message = $body;

        $title = 'GraphicsZoo';

        SM_sendMail($receiver, $subject, $message, $title);
    }

    public function send_email_after_qa_approval_for_designer($file_id, $project_id, $status, $approveby, $email) {

        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $query1 = $this->db->select('first_name')->from('users')->where('email', $email)->get();
        $fname = $query1->result();
        $data = array(
            'file_id' => $file_id,
            'project_id' => $project_id,
            'status' => $status,
            'appby' => $approveby,
            'fname' => isset($fname[0]->first_name) ? $fname[0]->first_name : ''
        );
        $body = $this->load->view('emailtemplate/qa_approvel_for_designer_template', $data, TRUE);
        $receiver = $email;

        $subject = 'Qa approved your design';

        $message = $body;

        $title = 'GraphicsZoo';

        SM_sendMail($receiver, $subject, $message, $title);
    }

    public function load_more() {
        $allcustomer = array();
        $group_no = $this->input->post('group_no');
        $status = $this->input->post('status');
        if ($status == 'active,disapprove') {
            $status = explode(',', $status);
        }
        $content_per_page = 6;
        $start = ceil($group_no * $content_per_page);
        $mycustomer = $this->Request_model->get_customer_list_for_qa_section($_SESSION['user_id']);

        foreach ($mycustomer as $value) {
            $allcustomer[] = $value['id'];
        }
        if (is_array($status)) {
            // echo "<pre>";print_r($status);
            $all_content = $this->Request_model->get_request_list_for_qa_section_scroll($allcustomer, $status, $start, $content_per_page);
        } else {
            $all_content = $this->Request_model->get_request_list_for_qa_section_scroll($allcustomer, array($status), $start, $content_per_page);
        }
        for ($i = 0; $i < sizeof($all_content); $i++) {
            $all_content[$i]['total_chat'] = $this->Request_model->get_chat_number($all_content[$i]['id'], $all_content[$i]['customer_id'], $all_content[$i]['designer_id'], "admin");
            $all_content[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($all_content[$i]['id'], $_SESSION['user_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($all_content[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            if ($all_content[$i]['status_qa'] == "active" || $all_content[$i]['status_qa'] == "disapprove") {
                if ($all_content[$i]['expected_date'] == '' || $all_content[$i]['expected_date'] == NULL) {
                    $all_content[$i]['expected'] = $this->myfunctions->check_timezone($all_content[$i]['latest_update'], $all_content[$i]['current_plan_name']);
                } else {
                    $all_content[$i]['expected'] = $this->onlytimezone($all_content[$i]['expected_date']);
                }
            } elseif ($all_content[$i]['status_qa'] == "checkforapprove") {
                $all_content[$i]['reviewdate'] = $this->onlytimezone($all_content[$i]['modified']);
            } elseif ($all_content[$i]['status_qa'] == "approved") {
                $all_content[$i]['approvddate'] = $this->onlytimezone($all_content[$i]['approvaldate']);
            } elseif ($all_content[$i]['status_qa'] == "pendingrevision") {
                $all_content[$i]['revisiondate'] = $this->onlytimezone($all_content[$i]['modified']);
            }
            $all_content[$i]['designer_project_count'] = $this->Request_model->get_designer_active_request($all_content[$i]['designer_id']);
            $all_content[$i]['comment_count'] = $commentcount;
            $all_content[$i]['total_file'] = $this->Request_model->get_files_count($all_content[$i]['id'], $all_content[$i]['designer_id'], "admin");
            $all_content[$i]['total_files_count'] = $this->Request_model->get_files_count_all($all_content[$i]['id']);
        }
        //echo "<pre>";print_r($all_content);

        if (isset($all_content) && is_array($all_content) && count($all_content)) {
            for ($i = 0; $i < count($all_content); $i++) {
                if ($all_content[$i]['status_qa'] == "active" || $all_content[$i]['status_qa'] == "disapprove") {
                    ?>
                    <div class="cli-ent-row tr brdr">
                        <div class="cli-ent-col td" style="width: 10%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/1'">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">Expected on</p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['expected']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 30%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/1'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/1"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                    <?php if ($all_content[$i]['status_qa'] == "active") { ?>
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="green text-uppercase text-wrap">In-Progress</span></p>
                                        </div>
                                    <?php } elseif ($all_content[$i]['status_qa'] == "disapprove" && $all_content[$i]['who_reject'] == 1) { ?>
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="red orangetext text-uppercase text-wrap">REVISION</span></p>
                                        </div>
                                    <?php } elseif ($all_content[$i]['status_admin'] == "disapprove" && $all_content[$i]['who_reject'] == 0) { ?>
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="red  text-uppercase text-wrap">Quality Revision</span></p>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/1'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?>
                                           ">
                                               <?php echo $all_content[$i]['customer_first_name']; ?>
                                               <?php
                                               if (strlen($all_content[$i]['customer_last_name']) > 5) {
                                                   echo ucwords(substr($all_content[$i]['customer_last_name'], 0, 1));
                                               } else {
                                                   echo $all_content[$i]['customer_last_name'];
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <!-- <span class="count_project"><?php // echo $all_content[$i]['designer_project_count'];     ?></span> -->
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)" class="<?php echo $all_content[$i]['id']; ?>"> 
                                            <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col <?php echo $all_content[$i]['id']; ?>">

                                        <p class="text-h text-wrap"title="<?php echo $all_content[$i]['designer_first_name'] . " " . $all_content[$i]['designer_last_name']; ?>
                                           ">
                                               <?php echo $all_content[$i]['designer_first_name']; ?>
                                               <?php
                                               if (strlen($all_content[$i]['designer_first_name']) > 5) {
                                                   echo ucwords(substr($all_content[$i]['designer_last_name'], 0, 1));
                                               } else {
                                                   echo $all_content[$i]['designer_last_name'];
                                               }
                                               ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <?php
                                        if ($all_content[$i]['status_qa'] == "active") {
                                            if ($all_content[$i]['designer_skills_matched'] == 0) {
                                                ?>
                                                                                           <!--  <div class="checkbox custom-checkbox mob-txt-center  text-left " id="<?php //echo $all_content[$i]['id'];      ?>">
                                                                                                <label>
                                                                                                    <input type="checkbox" class="checkcorrect" value="1"  data-request="<?php //echo $all_content[$i]['id'];     ?>" onchange="check(this.value, '<?php //echo $all_content[$i]['id'];     ?>' );"> 
                                                                                                    <span class="cr right-cr">
                                                                                                        <i class="cr-icon fa fa-check"></i>
                                                                                                    </span> <br>
                                                                                                </label>
                                                                                            </div> -->
                                                <?php
                                            }
                                        }
                                        ?>
                                        <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-target="#AddDesign" data-requestid="<?php echo $all_content[$i]['id']; ?>" data-designerid= "<?php echo $all_content[$i]['designer_id']; ?>">
                                            <span class="sma-red">+</span> Add Designer
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/1'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>">
                                        <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>public/admin/images/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?>
                                        </span>
                                        <?php //echo $all_content[$i]['total_chat_all']   ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/1'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/1">
                                        <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>public/admin/images/icon-file.png" class="img-responsive" width="13"> 
                                            <?php if (count($all_content[$i]['total_file']) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($all_content[$i]['total_file']); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($all_content[$i]['total_files_count']); ?></a></p>
                            </div>
                        </div>
                    </div> 
                <?php } elseif ($all_content[$i]['status_qa'] == "assign") {
                    ?>
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">In Queue</p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 36%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col col-w1">
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/2"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>

                                    <div class="cell-col priority-sec" >
                                        <?php echo isset($all_content[$i]['priority']) ? $all_content[$i]['priority'] : ''; ?>
                                    </div>

                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="gray text-uppercase">IN-queue</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 16%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?>">
                                            <?php echo $all_content[$i]['customer_first_name']; ?>
                                            <?php
                                            if (strlen($all_content[$i]['customer_last_name']) > 5) {
                                                echo ucwords(substr($all_content[$i]['customer_last_name'], 0, 1));
                                            } else {
                                                echo $all_content[$i]['customer_last_name'];
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 19%;">
                            <div class="cli-ent-xbox text-left p-left1">
                                <div class="cell-row">
                                    <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/2">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/icon-chat.png" class="img-responsive" width="21"><?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $all_content[$i]['total_chat_all'];    ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 10%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($all_content[$i]['total_file']) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($all_content[$i]['total_file']); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($all_content[$i]['total_files_count']); ?></a></p>
                            </div>
                        </div>
                    </div> <!-- End Row -->
                    <?php
                } elseif ($all_content[$i]['status_qa'] == "pendingrevision") {
                    ?>
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/3'">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">Expected on</p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['revisiondate']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 30%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/3'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/3"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                    <?php if ($all_content[$i]['status_qa'] == "pendingrevision") { ?>
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="red lightbluetext text-uppercase text-wrap">Pending Review</span></p>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/3'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?>
                                           ">
                                               <?php echo $all_content[$i]['customer_first_name']; ?>
                                               <?php
                                               if (strlen($all_content[$i]['customer_last_name']) > 5) {
                                                   echo ucwords(substr($all_content[$i]['customer_last_name'], 0, 1));
                                               } else {
                                                   echo $all_content[$i]['customer_last_name'];
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <!-- <span class="count_project"><?php //echo $all_content[$i]['designer_project_count'];      ?></span> -->
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)" class="<?php echo $all_content[$i]['id']; ?>"> 
                                            <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col <?php echo $all_content[$i]['id']; ?>">
                                        <p class="text-h text-wrap"title="<?php echo $all_content[$i]['designer_first_name'] . " " . $all_content[$i]['designer_last_name']; ?>
                                           ">
                                               <?php echo $all_content[$i]['designer_first_name']; ?>
                                               <?php
                                               if (strlen($all_content[$i]['designer_first_name']) > 5) {
                                                   echo ucwords(substr($all_content[$i]['designer_last_name'], 0, 1));
                                               } else {
                                                   echo $all_content[$i]['designer_last_name'];
                                               }
                                               ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <?php if ($all_content[$i]['designer_assign_or_not'] == 0) { ?>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-requestid="<?php echo $all_content[$i]['id']; ?>" data-designerid= "<?php echo $all_content[$i]['designer_id']; ?>" data-target="#AddDesign">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_project/" . $all_content[$i]['id']; ?>/3'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>3">
                                        <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>public/admin/images/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span></span>
                                        <?php //echo $all_content[$i]['total_chat_all']   ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_project/" . $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/3">
                                        <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>public/admin/images/icon-file.png" class="img-responsive" width="13"> <?php if (count($all_content[$i]['total_file']) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($all_content[$i]['total_file']); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($all_content[$i]['total_files_count']); ?></a></p>
                            </div>
                        </div>
                    </div>
                <?php } elseif ($all_content[$i]['status_qa'] == "checkforapprove") { ?>  
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 10%;cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox">
                                <p class="pro-a">Delivered on</p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['reviewdate']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 30%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                    <?php if ($all_content[$i]['status_qa'] == "checkforapprove") { ?>
                                        <div class="cell-col col-w1">
                                            <p class="neft text-center"><span class="red bluetext text-uppercase text-wrap">Pending-Approval</span></p>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap">
                                            <?php echo $all_content[$i]['customer_first_name']; ?>
                                            <?php
                                            if (strlen($all_content[$i]['customer_last_name']) > 5) {
                                                echo ucwords(substr($all_content[$i]['customer_last_name'], 0, 1));
                                            } else {
                                                echo $all_content[$i]['customer_last_name'];
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <!-- <span class="count_project"><?php //echo $all_content[$i]['designer_project_count'];      ?></span> -->
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)" class="<?php echo $all_content[$i]['id']; ?>">
                                            <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col <?php echo $all_content[$i]['id']; ?>">
                                        <p class="text-h text-wrap">
                                            <?php echo $all_content[$i]['designer_first_name']; ?>
                                            <?php
                                            if (strlen($all_content[$i]['designer_last_name']) > 5) {
                                                echo ucwords(substr($all_content[$i]['designer_last_name'], 0, 1));
                                            } else {
                                                echo $all_content[$i]['designer_last_name'];
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Designer</p>
                                        <p class="space-a"></p>
                                        <?php if ($all_content[$i]['designer_assign_or_not'] == 0) { ?>
                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-requestid="<?php echo $all_content[$i]['id']; ?>" data-designerid= "<?php echo $all_content[$i]['designer_id']; ?>" data-target="#AddDesign">
                                                <span class="sma-red">+</span> Add Designer
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_request/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_request/" . $all_content[$i]['id']; ?>/4">
                                        <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>public/admin/images/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?>
                                        </span>
                                        <?php //echo $all_content[$i]['total_chat_all']   ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4">
                                        <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>public/admin/images/icon-file.png" class="img-responsive" width="13"> <?php if (count($all_content[$i]['total_file']) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($all_content[$i]['total_file']); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($all_content[$i]['total_files_count']); ?></a></p>
                            </div>
                        </div>
                    </div>
                <?php } elseif ($all_content[$i]['status'] == "approved") { ?>
                    <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                        <div class="cli-ent-col td" style="width: 13%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cli-ent-xbox">
                                <h3 class="app-roved green">Approved on</h3>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['approvddate']));
                                    ?>
                                </p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 37%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                    <div class="cell-col col-w1">
                                        <p class="neft text-center"><span class="green text-uppercase text-wrap">COMPLETED</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap"><?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?></p>
                                        <p class="pro-b">Client</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <!-- <span class="count_project"><?php //echo $all_content[$i]['designer_project_count'];     ?></span> -->
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)">
                                            <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?><?php echo $all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap"><?php echo $all_content[$i]['designer_first_name'] . " " . $all_content[$i]['designer_last_name']; ?></p>
                                        <p class="pro-b">Designer</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5">
                                        <span class="inline-imgsssx"><img src="<?php echo base_url(); ?>public/admin/images/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $completed[$i]['total_chat_all']  ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/5">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"><?php if (count($all_content[$i]['total_file']) != 0) { ?>
                                                <span class="numcircle-box"><?php echo count($all_content[$i]['total_file']); ?></span></span>
                                        <?php } ?>
                                            <?php echo count($all_content[$i]['total_files_count']); ?></a></p>
                            </div>
                        </div>
                    </div> 
                    <?php
                }
            }
        }
    }

    public function load_more_customer() {
        $group_no = $this->input->post('group_no');
        $content_per_page = 4;
        $start = ceil($group_no * $content_per_page);
        $data = array();
        $mydesigner = $this->Request_model->get_designer_list_for_qa();
        if ($mydesigner) {
            $data = $this->Request_model->get_customer_list_for_qa_scroll("array", $mydesigner, $start, $content_per_page);
            for ($i = 0; $i < sizeof($data); $i++) {
                $user = $this->Account_model->get_all_request_by_customer($data[$i]['id']);
                $active = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('active', 'disapprove'), $data[$i]['id']);
                $inque = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('assign'), $data[$i]['id']);
                $revision = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('disapprove'), $data[$i]['id']);
                $review = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('checkforapprove'), $data[$i]['id']);
                $complete = $this->Account_model->get_all_active_view_request_by_customer_for_qa(array('approved'), $data[$i]['id']);
                //$user = $this->Account_model->get_all_active_view_request_by_customer_for_qa($activeClients[$i]['id']);
                $data[$i]['created'] = $this->onlytimezone($data[$i]['created']);
                $data[$i]['active'] = $active;
                $data[$i]['inque_request'] = $inque;
                $data[$i]['revision_request'] = $revision;
                $data[$i]['review_request'] = $review;
                $data[$i]['complete_request'] = $complete;
                $data[$i]['no_of_request'] = sizeof($user);
                $data[$i]['no_of_request'] = sizeof($user);

                $user = $this->Account_model->get_all_active_request_by_customer($data[$i]['id']);
                $data[$i]['active_request'] = sizeof($user);

                $data[$i]['designer_name'] = "";
                if ($data[$i]['designer_id'] != 0) {
                    $data[$i]['designer'] = $this->Request_model->get_user_by_id($data[$i]['designer_id']);
                } else {
                    $data[$i]['designer'] = "";
                }

                $designer_name = $this->Account_model->getuserbyid($data[$i]['designer_id']);
                if (!empty($designer_name)) {
                    $data[$i]['designer_name'] = $designer_name[0]['first_name'];
                }
            }
        }
        for ($i = 0; $i < count($data); $i++) {
            ?>

            <!-- Start Row -->
            <div class="cli-ent-row tr brdr" style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 32%;"  onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'" >
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">
                            <div class="cell-col" style="width: 80px; padding-right: 15px;">
                                <?php if (($data[$i]['profile_picture'] != "") && (file_exists(base_url() . "uploads/profile_picture/" . $data[$i]['profile_picture']))) { ?>
                                    <figure class="cli-ent-img circle one">
                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $data[$i]['profile_picture']; ?>" class="img-responsive one">
                                    </figure>
                                <?php } else { ?>
                                    <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 20px;">
                                        <?php echo ucwords(substr($data[$i]['first_name'], 0, 1)) . ucwords(substr($data[$i]['last_name'], 0, 1)); ?>
                                    </figure>
                                <?php } ?>
                            </div>

                            <div class="cell-col" style="width: 185px;">
                                <h3 class="pro-head-q"><a href="<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>"><?php echo $data[$i]['first_name'] . " " . $data[$i]['last_name'] ?></a></h3>
                                <p class="pro-a"> <?php echo $data[$i]['email']; ?></p>
                            </div>

                            <div class="cell-col">
                                <p class="neft">
                                    <span class="blue text-uppercase">
                                        <?php
                                        if ($data[$i]['plan_turn_around_days'] == "1") {
                                            echo "Premium Member";
                                        } elseif ($data[$i]['plan_turn_around_days'] == "3") {
                                            echo "Standard Member";
                                        } else {
                                            echo "No Member";
                                        }
                                        ?>
                                    </span>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 10%;"  onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'" >
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Sign Up Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b"><?php echo date("M/d/Y", strtotime($data[$i]['created'])) ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 12%;"  onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'" >
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Next Billing Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b">
                            <?php
                            $date = "";
                            if (checkdate(date("m", strtotime($data[$i]['modified'])), date("d", strtotime($data[$i]['modified'])), date("Y", strtotime($data[$i]['modified'])))) {
                                $over_date = "";
                                if ($data[$i]['plan_turn_around_days']) {
                                    $date = date("Y-m-d", strtotime($data[$i]['modified']));
                                    $time = date("h:i:s", strtotime($data[$i]['modified']));
                                    $data[$i]['plan_turn_around_days'] = "2";
                                    $date = date("m/d/Y g:i a", strtotime($date . " " . $data[$i]['plan_turn_around_days'] . " weekdays " . $time));
                                    $diff = date_diff(date_create(date("Y-m-d", strtotime($data[$i]['modified']))), date_create($date));
                                }
                            }
                            $current_date = strtotime(date("Y-m-d"));
                            $now = time(); // or your date as well

                            $expiration_date = strtotime($date);
                            $datediff = $now - $expiration_date;
                            $date_due_day = round($datediff / (60 * 60 * 24));
                            $color = "";
                            $text_color = "white";
                            if ($date_due_day == 0) {
                                $date_due_day = "Due  </br>Today";
                                $color = "#f7941f";
                            } else
                            if ($date_due_day == (-1)) {
                                $date_due_day = "Due  </br>Tomorrow";
                                $color = "#98d575";
                            } else
                            if ($date_due_day > 0) {
                                $date_due_day = "Over Due";
                                $color = "red";
                            } else if ($date_due_day < 0) {
                                $date_due_day = "Due from </br>" . number_format($date_due_day) . " days";
                                $text_color = "black";
                            }
                            ?>
                            <?php echo date_format(date_create($date), "M /d /Y"); ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 5%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $data[$i]['active']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 8%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">In Queue</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $data[$i]['inque_request']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 13%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/client_projects/<?php echo $data[$i]['id']; ?>'">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Pending Approval</p>
                        <p class="space-a"></p>
                        <p class="pro-a"><?php echo $data[$i]['review_request']; ?></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 15%;">

                    <?php if (!empty($data[$i]['designer'])) { ?>
                        <div class="cli-ent-xbox">
                            <div class="cell-row">
                                <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                    <a href="">
                                        <?php if ($data[$i]['designer'][0]['profile_picture'] != "") { ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $data[$i]['designer'][0]['profile_picture']; ?>" class="img-responsive">
                                            </figure>
                                        <?php } else { ?>
                                            <figure class="pro-circle-img">
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                            </figure>
                                        <?php } ?>
                                    </a>
                                </div>
                                <div class="cell-col">
                                    <p class="text-h text-wrap" title="<?php echo $data[$i]['designer'][0]['first_name'] . " " . $data[$i]['designer'][0]['last_name']; ?>
                                       ">
                                           <?php echo $data[$i]['designer'][0]['first_name']; ?>
                                           <?php
                                           if (strlen($data[$i]['designer'][0]['last_name']) > 5) {
                                               echo ucwords(substr($data[$i]['designer'][0]['last_name'], 0, 1));
                                           } else {
                                               echo $data[$i]['designer'][0]['last_name'];
                                           }
                                           ?>

                                    </p>
                                    <p class="pro-b">Designer</p>
                                    <p class="space-a"></p>
                                    <a href="#" class="addde-signersk1 permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign"  data-customerid="<?php echo $data[$i]['id']; ?>">
                                        <span class="sma-red">+</span> Add Designer
                                    </a>
                                </div>
                            </div> 
                        </div>
                    <?php } else { ?>
                        <div class="cli-ent-xbox">
                            <a href="#" class="upl-oadfi-le noborder permanent_assign_design" data-toggle="modal" data-target="#AddPermaDesign" data-customerid="<?php echo $data[$i]['id']; ?>">
                                <span class="icon-crss-3">
                                    <span class="icon-circlxx55 margin5">+</span>
                                    <p class="attachfile">Assign designer<br> permanently</p>
                                </span>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!-- End Row -->
            <?php
        }
    }

    public function load_more_designer() {
        //echo "<pre>";print_r($_POST);
        $group_no = $this->input->post('group_no');
        $dataStatus = $this->input->post('status');
        $search = $this->input->post('search');
        $content_per_page = LIMIT_QA_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $user_id = $_SESSION['user_id'];
        $designerscount = $this->Account_model->designerlistfor_qa_load_more($user_id, true, $start, $content_per_page, "", $search);
        $designers = $this->Account_model->designerlistfor_qa_load_more($user_id, false, $start, $content_per_page, "", $search);
        // echo "<pre/>";print_r($designers);
        //$designers = $this->Request_model->get_designer_list_for_qa_scroll("array", $start, $content_per_page);
        for ($i = 0; $i < sizeof($designers); $i++) {
            $designers[$i]['total_rating'] = $this->Request_model->get_average_rating($designers[$i]['id']);
            $user = $this->Account_model->get_all_request_by_designer($designers[$i]['id']);
            $designers[$i]['handled'] = sizeof($user);
            $user = $this->Account_model->get_all_active_request_by_designer($designers[$i]['id']);
            $designers[$i]['active_request'] = sizeof($user);
        }
        for ($i = 0; $i < sizeof($designers); $i++) {
            ?>
            <!-- Start Row -->
            <div class="cli-ent-row tr brdr" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>'" style="cursor: pointer;">
                <div class="cli-ent-col td" style="width: 10%;">
                    <?php
                    if ($designers[$i]['profile_picture']) {
                        ?>
                        <figure class="cli-ent-img circle one">
                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $designers[$i]['profile_picture']; ?>" class="img-responsive one">
                        </figure>
                    <?php } else { ?>
                        <figure class="cli-ent-img circle one" style="background: #0190ff; text-align: center; font-size: 15px; color: #fff;padding-top: 20px;font-family: GothamPro-Medium;">
                            <?php echo ucwords(substr($designers[$i]['first_name'], 0, 1)) . ucwords(substr($designers[$i]['last_name'], 0, 1)); ?>
                        </figure>
                    <?php } ?>
                </div>
                <div class="cli-ent-col td" style="width: 35%;">
                    <div class="cli-ent-xbox text-left">
                        <div class="cell-row">

                            <div class="cell-col">
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/active_view_designer/<?php echo $designers[$i]['id']; ?>"><?php echo $designers[$i]['first_name'] . " " . $designers[$i]['last_name'] ?></a></h3>
                                <p class="pro-b"><?php echo $designers[$i]['email']; ?></p>
                            </div>

                            <!--<div class="cell-col col-w1">
                                <p class="neft text-center"><span class="green text-uppercase text-wrap">In-Progress</span></p>
                            </div>-->
                        </div>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 15%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Sign Up Date</p>
                        <p class="space-a"></p>
                        <p class="pro-b"><?php echo date("M /d /Y", strtotime($designers[$i]['created'])) ?></p>
                    </div>
                </div>

                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Requests Being Handled</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="green text-uppercase"><?php echo $designers[$i]['handled']; ?></span></p>
                    </div>
                </div>
                <div class="cli-ent-col td" style="width: 20%;">
                    <div class="cli-ent-xbox text-center">
                        <p class="word-wrap-one">Active Requests</p>
                        <p class="space-a"></p>
                        <p class="neft text-center"><span class="red text-uppercase"><?php echo $designers[$i]['active_request']; ?></span></p>
                    </div>
                </div>
            </div> 
            <!-- End Row -->
        <?php }
        ?><span id="loadingAjaxCount" data-value="<?php echo $designerscount; ?>"></span>
        <?php
    }

    public function load_more_qa() {
        $group_no = $this->input->post('group_no');
        $dataStatusText = $this->input->post('status');
        $dataStatus = explode(",", $dataStatusText);
        $bucket_type = (!empty($_POST['bucket_type'])) ? $_POST['bucket_type'] : '';
        $search = $this->input->post('search');
        $content_per_page = LIMIT_QA_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $user_id = $_SESSION['user_id'];
        $allcustomer = array();
        $mycustomer = $this->Request_model->get_customer_list_for_qa_section($user_id);
        foreach ($mycustomer as $value) {
            array_push($allcustomer, $value['id']);
        }
        $total_result_count = $this->Request_model->qa_load_more($dataStatus, $allcustomer, true, $start, $content_per_page, "", $search, $bucket_type);
        $all_content = $this->Request_model->qa_load_more($dataStatus, $allcustomer, false, $start, $content_per_page, "", $search, $bucket_type);
        for ($i = 0; $i < sizeof($all_content); $i++) {
            $all_content[$i]['category_data'] = $this->Request_model->getRequestCatbyid($all_content[$i]['id'], $all_content[$i]['category_id']);
            if (isset($all_content[$i]['category_data'][0]['category']) && $all_content[$i]['category_data'][0]['category'] != '') {
                $all_content[$i]['category_name'] = $all_content[$i]['category_data'][0]['category'];
            } else {
                $all_content[$i]['category_name'] = $all_content[$i]['category_data'][0]['cat_name'];
            }
            $all_content[$i]['subcategory_name'] = $this->Request_model->getRequestsubCatbyid($all_content[$i]['id'], $all_content[$i]['category_id'], $all_content[$i]['subcategory_id']);
            $all_content[$i]['total_chat'] = $this->Request_model->get_chat_number($all_content[$i]['id'], $all_content[$i]['customer_id'], $all_content[$i]['designer_id'], "admin");
            $all_content[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number($all_content[$i]['id'], $_SESSION['user_id'], "admin");
            $getfileid = $this->Request_model->get_attachment_files($all_content[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {
                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "admin");
            }
            if ($all_content[$i]['status_qa'] == "active" || $all_content[$i]['status_qa'] == "disapprove") {
                if ($all_content[$i]['expected_date'] == '' || $all_content[$i]['expected_date'] == NULL) {
                    $all_content[$i]['expected'] = $this->myfunctions->check_timezone($all_content[$i]['latest_update'], $all_content[$i]['current_plan_name']);
                } else {
                    $all_content[$i]['expected'] = $this->onlytimezone($all_content[$i]['expected_date']);
                }
            } 
            elseif ($all_content[$i]['status_qa'] == "checkforapprove") {
                $all_content[$i]['reviewdate'] = $this->onlytimezone($all_content[$i]['modified']);
            } elseif ($all_content[$i]['status_qa'] == "approved") {
                $all_content[$i]['approvddate'] = $this->onlytimezone($all_content[$i]['approvaldate']);
            } elseif ($all_content[$i]['status_qa'] == "pendingrevision") {
                $all_content[$i]['revisiondate'] = $this->onlytimezone($all_content[$i]['modified']);
            }

            $all_content[$i]['designer_project_count'] = $this->Request_model->get_designer_active_request($all_content[$i]['designer_id']);
            $all_content[$i]['comment_count'] = $commentcount;
            $all_content[$i]['total_file'] = $this->Request_model->get_files_count($all_content[$i]['id'], $all_content[$i]['designer_id'], "admin");
            $all_content[$i]['total_files_count'] = $this->Request_model->get_files_count_all($all_content[$i]['id']);
            $all_content[$i]['usertimezone_date'] = $this->Request_model->get_files_count_all($all_content[$i]['user_timezone']);
        }
        //echo "<pre>";print_r($all_content);

        if (isset($all_content) && is_array($all_content) && count($all_content)) {
            for ($i = 0; $i < count($all_content); $i++) {
                if ($all_content[$i]['status_qa'] == "active" || $all_content[$i]['status_qa'] == "disapprove") {
                    ?>
                    <div class="cli-ent-row tr brdr">
                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                        <div class="cli-ent-col td">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" >
                                        <input type="checkbox" name="project" class="selected_pro in_active"  value="<?php echo $all_content[$i]['id']; ?>"/> 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 28%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/1'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/1"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <?php if ($all_content[$i]['category_name'] != '' || $all_content[$i]['subcategory_name'] != '') { ?>
                                            <p class="pro-b"><b><?php echo $all_content[$i]['category_name']; ?></b> <b><?php echo isset($all_content[$i]['subcategory_name']) ? "> " . $all_content[$i]['subcategory_name'] : ''; ?></b></p>
                                        <?php } ?>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/1'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?>
                                           ">
                                               <?php echo $all_content[$i]['customer_first_name']; ?>
                                               <?php
                                               if (strlen($all_content[$i]['customer_last_name']) > 5) {
                                                   echo ucwords(substr($all_content[$i]['customer_last_name'], 0, 1));
                                               } else {
                                                   echo $all_content[$i]['customer_last_name'];
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                        <div class="sub_user_req_info qa-sub-user">
                                            <p class="text-h text-wrap">
                                                <?php echo $all_content[$i]['sub_first_name']; ?>
                                                <?php
                                                if (strlen($all_content[$i]['sub_last_name']) > 5) {
                                                    echo ucwords(substr($all_content[$i]['sub_last_name'], 0, 1));
                                                } else {
                                                    echo $all_content[$i]['sub_last_name'];
                                                }
                                                ?>
                                            </p>
                                            <?php
                                            if ($all_content[$i]['sub_first_name'] != '') {
                                                ?>
                                                <p class="pro-b">Sub User</p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/1'">
                            <?php if ($all_content[$i]['status_qa'] == "active") { ?>
                                <div class="cell-col col-w1">
                                    <p class="neft text-center"><span class="green text-uppercase text-wrap">In-Progress</span></p>
                                </div>
                            <?php } elseif ($all_content[$i]['status_qa'] == "disapprove" && $all_content[$i]['who_reject'] == 1) { ?>
                                <div class="cell-col col-w1">
                                    <p class="neft text-center"><span class="red orangetext text-uppercase text-wrap">REVISION</span></p>
                                </div>
                            <?php } elseif ($all_content[$i]['status_admin'] == "disapprove" && $all_content[$i]['who_reject'] == 0) { ?>
                                <div class="cell-col col-w1">
                                    <p class="neft text-center"><span class="red  text-uppercase text-wrap">Quality Revision</span></p>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="cli-ent-col td" style="width: 12%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <?php if ($all_content[$i]['designer_id'] != "" && $all_content[$i]['designer_id'] != 0) { ?>
                                                <!-- <span class="count_project"><?php // echo $all_content[$i]['designer_project_count'];         ?></span> -->
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="javascript:void(0)" class="<?php echo $all_content[$i]['id']; ?>"> 
                                                <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col <?php echo $all_content[$i]['id']; ?>">
                                            <p class="text-h text-wrap"title="<?php echo $all_content[$i]['designer_first_name'] . " " . $all_content[$i]['designer_last_name']; ?>
                                               ">
                                                   <?php echo $all_content[$i]['designer_first_name']; ?>
                                                   <?php
                                                   if (strlen($all_content[$i]['designer_first_name']) > 5) {
                                                       echo ucwords(substr($all_content[$i]['designer_last_name'], 0, 1));
                                                   } else {
                                                       echo $all_content[$i]['designer_last_name'];
                                                   }
                                                   ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                            <p class="space-a"></p>
                                            <?php
                                            if ($all_content[$i]['status_qa'] == "active") {
                                                if ($all_content[$i]['designer_skills_matched'] == 0) {
                                                    ?>
                                                                                                                       
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    <?php } else { ?>
                                        <div class="cell-col <?php echo $all_content[$i]['id']; ?>">No Designer Assigned</div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/1'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?>
                                        </span>
                                        </a></p>
                            </div>
                        </div>
                        <?php
                        if ($all_content[$i]['verified'] == 1) {
                            $class = 'verified_by_admin';
                            $disable = '';
                        } elseif ($all_content[$i]['verified'] == 2) {
                            $class = 'verified_by_designer';
                            $disable = 'disabled';
                        } else {
                            $class = '';
                            $disable = '';
                        }
                        ?>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox">
                                <div class="cell-row">
                                    <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $all_content[$i]['id']; ?>">
                                        <label><input type="checkbox" name="project" class="verified" data-pid="<?php echo $all_content[$i]['id']; ?>" <?php echo ($all_content[$i]['verified'] == 1 || $all_content[$i]['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable; ?>/>
                                            <i class="fa fa-circle-thin" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle" aria-hidden="true"></i></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 13%;">
                            <?php if ($all_content[$i]['is_trail'] == 1) { ?>
                                <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                            <?php } ?>
                            <div class="cli-ent-xbox">
                                <a href="javascript:void(0)" class="editexpected" date-expected="<?php echo date('M d, Y h:i A', strtotime($all_content[$i]['expected'])); ?>" date-reqid="<?php echo $all_content[$i]['id']; ?>">
                                    <i class='fa fa-pencil-square-o'></i> 
                                </a>
                                <p class="pro-b">
                                <p id="demo_<?php echo $all_content[$i]['id']; ?>" class="expectd_date" data-id="<?php echo $all_content[$i]['id']; ?>" data-date="<?php echo date('M d, Y h:i:A', strtotime($all_content[$i]['expected'])); ?>"></p>
                                <?php echo date('M d, Y h:i:A', strtotime($all_content[$i]['expected'])); ?>
                            </div>
                        </div>
                    </div> 
                    <?php
                } 
                elseif ($all_content[$i]['status_qa'] == "assign") {
                    ?>
                    <div class="cli-ent-row tr brdr">
                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                        <div class="cli-ent-col td">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" >
                                        <input type="checkbox" name="project" class="selected_pro in_queue"  value="<?php echo $all_content[$i]['id']; ?>"/> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 34%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/2"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <?php if ($all_content[$i]['category_name'] != '' || $all_content[$i]['subcategory_name'] != '') { ?>
                                            <p class="pro-b"><b><?php echo $all_content[$i]['category_name']; ?></b> <b><?php echo isset($all_content[$i]['subcategory_name']) ? "> " . $all_content[$i]['subcategory_name'] : ''; ?></b></p>
                                        <?php } ?>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td col-w1" style="width: 14%;" onclick="window.location.href = '<?php echo base_url(); ?>admin/dashboard/view_request/<?php echo $all_content[$i]['id']; ?>/2'">
                            <p class="neft text-center"><span class="gray text-uppercase">IN-queue</span></p>
                            <div class="cell-col priority-sec" >
                                <?php echo isset($all_content[$i]['priority']) ? $all_content[$i]['priority'] : ''; ?>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 14%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?>">
                                            <?php echo $all_content[$i]['customer_first_name']; ?>
                                            <?php
                                            if (strlen($all_content[$i]['customer_last_name']) > 5) {
                                                echo ucwords(substr($all_content[$i]['customer_last_name'], 0, 1));
                                            } else {
                                                echo $all_content[$i]['customer_last_name'];
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Client</p>
                                        <div class="sub_user_req_info qa-sub-user">
                                            <p class="text-h text-wrap">
                                                <?php echo $all_content[$i]['sub_first_name']; ?>
                                                <?php
                                                if (strlen($all_content[$i]['sub_last_name']) > 5) {
                                                    echo ucwords(substr($all_content[$i]['sub_last_name'], 0, 1));
                                                } else {
                                                    echo $all_content[$i]['sub_last_name'];
                                                }
                                                ?>
                                            </p>
                                            <?php
                                            if ($all_content[$i]['sub_first_name'] != '') {
                                                ?>
                                                <p class="pro-b">Sub User</p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 12%;">
                            <div class="cli-ent-xbox text-left p-left1">
                                <div class="cell-row">
                                    <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                </div>
                            </div>
                        </div>
                        <?php
                        if ($all_content[$i]['verified'] == 1) {
                            $class = 'verified_by_admin';
                            $disable = '';
                        } elseif ($all_content[$i]['verified'] == 2) {
                            $class = 'verified_by_designer';
                            $disable = 'disabled';
                        } else {
                            $class = '';
                            $disable = '';
                        }
                        ?>
                        <div class="cli-ent-col td" style="width: 9%;">
                            <div class="cli-ent-xbox">
                                <div class="cell-row">
                                    <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $all_content[$i]['id']; ?>">
                                        <label><input type="checkbox" name="project" class="verified" data-pid="<?php echo $all_content[$i]['id']; ?>" <?php echo ($all_content[$i]['verified'] == 1 || $all_content[$i]['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable; ?>/>
                                            <i class="fa fa-circle-thin" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle" aria-hidden="true"></i></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%;" onclick="window.location.href = '<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url(); ?>qa/dashboard/view_project/<?php echo $all_content[$i]['id']; ?>/2">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21"><?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                            </div>
                        </div>


                    </div> <!-- End Row -->
                    <?php
                } 
                elseif ($all_content[$i]['status_qa'] == "pendingrevision") {
                    ?>
                    <div class="cli-ent-row tr brdr">
                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                        <div class="cli-ent-col td">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col">
                                        <input type="checkbox" name="project" class="selected_pro pending_rev" value="<?php echo $pending_review_request[$i]['id']; ?>"/> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 28%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/3'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/3"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <?php if ($all_content[$i]['category_name'] != '' || $all_content[$i]['subcategory_name'] != '') { ?>
                                            <p class="pro-b"><b><?php echo $all_content[$i]['category_name']; ?></b> <b><?php echo isset($all_content[$i]['subcategory_name']) ? "> " . $all_content[$i]['subcategory_name'] : ''; ?></b></p>
                                        <?php } ?>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/3'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap" title="<?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?>
                                           ">
                                               <?php echo $all_content[$i]['customer_first_name']; ?>
                                               <?php
                                               if (strlen($all_content[$i]['customer_last_name']) > 5) {
                                                   echo ucwords(substr($all_content[$i]['customer_last_name'], 0, 1));
                                               } else {
                                                   echo $all_content[$i]['customer_last_name'];
                                               }
                                               ?>

                                        </p>
                                        <p class="pro-b">Client</p>
                                        <div class="sub_user_req_info qa-sub-user">
                                            <p class="text-h text-wrap">
                                                <?php echo $all_content[$i]['sub_first_name']; ?>
                                                <?php
                                                if (strlen($all_content[$i]['sub_last_name']) > 5) {
                                                    echo ucwords(substr($all_content[$i]['sub_last_name'], 0, 1));
                                                } else {
                                                    echo $all_content[$i]['sub_last_name'];
                                                }
                                                ?>
                                            </p>
                                            <?php
                                            if ($all_content[$i]['sub_first_name'] != '') {
                                                ?>
                                                <p class="pro-b">Sub User</p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td client-project" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_request/" . $pendingreview[$i]['id']; ?>/3'">
                            <?php if ($all_content[$i]['status_qa'] == "pendingrevision") { ?>
                                <div class="cell-col col-w1">
                                    <p class="neft text-center"><span class="red lightbluetext text-uppercase text-wrap">Pending Review</span></p>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="cli-ent-col td" style="width: 16%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <?php if ($all_content[$i]['designer_id'] != "" && $all_content[$i]['designer_id'] != 0) { ?>
                                                <!-- <span class="count_project"><?php //echo $all_content[$i]['designer_project_count'];       ?></span> -->
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="javascript:void(0)" class="<?php echo $all_content[$i]['id']; ?>"> 
                                                <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col <?php echo $all_content[$i]['id']; ?>">
                                            <p class="text-h text-wrap"title="<?php echo $all_content[$i]['designer_first_name'] . " " . $all_content[$i]['designer_last_name']; ?>
                                               ">
                                                   <?php echo $all_content[$i]['designer_first_name']; ?>
                                                   <?php
                                                   if (strlen($all_content[$i]['designer_first_name']) > 5) {
                                                       echo ucwords(substr($all_content[$i]['designer_last_name'], 0, 1));
                                                   } else {
                                                       echo $all_content[$i]['designer_last_name'];
                                                   }
                                                   ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                            <p class="space-a"></p>
                                            <?php if ($all_content[$i]['designer_assign_or_not'] == 0) { ?>
                                                <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-requestid="<?php echo $all_content[$i]['id']; ?>" data-designerid= "<?php echo $all_content[$i]['designer_id']; ?>" data-target="#AddDesign">
                                                    <span class="sma-red">+</span> Add Designer
                                                </a>
                                            <?php } ?>
                                        </div>
                                    <?php } else { ?>
                                        <div class="cell-col <?php echo $all_content[$i]['id']; ?>">No Designer Assigned</div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_project/" . $all_content[$i]['id']; ?>/3'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>3">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span></span>
                                        <?php //echo $all_content[$i]['total_chat_all']      ?></a></p>
                            </div>
                        </div>
                    <!--                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "admin/dashboard/view_project/" . $all_content[$i]['id']; ?>/2'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/3">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($all_content[$i]['total_file']) != 0) { ?>
                                                                    <span class="numcircle-box"><?php echo count($all_content[$i]['total_file']); ?></span></span>
                        <?php } ?>
                        <?php echo count($all_content[$i]['total_files_count']); ?></a></p>
                            </div>
                        </div>-->
                        <?php
                        if ($all_content[$i]['verified'] == 1) {
                            $class = 'verified_by_admin';
                            $disable = '';
                        } elseif ($all_content[$i]['verified'] == 2) {
                            $class = 'verified_by_designer';
                            $disable = 'disabled';
                        } else {
                            $class = '';
                            $disable = '';
                        }
                        ?>
                        <div class="cli-ent-col td" style="width: 10%;">
                            <div class="cli-ent-xbox">
                                <div class="cell-row">
                                    <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $all_content[$i]['id']; ?>">
                                        <label><input type="checkbox" name="project" class="verified" data-pid="<?php echo $all_content[$i]['id']; ?>" <?php echo ($all_content[$i]['verified'] == 1 || $all_content[$i]['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable; ?>/>
                                            <i class="fa fa-circle-thin" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle" aria-hidden="true"></i></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 13%;cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/3'">
                            <?php if ($all_content[$i]['is_trail'] == 1) { ?>
                                <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                            <?php } ?>
                            <div class="cli-ent-xbox">
                                <p class="pro-a">Expected on</p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['revisiondate']));
                                    ?>
                                </p>
                            </div>
                        </div>

                    </div>
                    <?php
                } 
                elseif ($all_content[$i]['status_qa'] == "checkforapprove") {
                    ?>  
                    <div class="cli-ent-row tr brdr">
                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                        <div class="cli-ent-col td">
                            <div class="cell-row">
                                <div class="cell-col" >
                                    <input type="checkbox" name="project" class="selected_pro pending_app" value="<?php echo $all_content[$i]['id']; ?>"/> 
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 28%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <?php if ($all_content[$i]['category_name'] != '' || $all_content[$i]['subcategory_name'] != '') { ?>
                                            <p class="pro-b"><b><?php echo $all_content[$i]['category_name']; ?></b> <b><?php echo isset($all_content[$i]['subcategory_name']) ? "> " . $all_content[$i]['subcategory_name'] : ''; ?></b></p>
                                        <?php } ?>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap">
                                            <?php echo $all_content[$i]['customer_first_name']; ?>
                                            <?php
                                            if (strlen($all_content[$i]['customer_last_name']) > 5) {
                                                echo ucwords(substr($all_content[$i]['customer_last_name'], 0, 1));
                                            } else {
                                                echo $all_content[$i]['customer_last_name'];
                                            }
                                            ?>
                                        </p>
                                        <p class="pro-b">Client</p>
                                        <div class="sub_user_req_info qa-sub-user">
                                            <p class="text-h text-wrap">
                                                <?php echo $all_content[$i]['sub_first_name']; ?>
                                                <?php
                                                if (strlen($all_content[$i]['sub_last_name']) > 5) {
                                                    echo ucwords(substr($all_content[$i]['sub_last_name'], 0, 1));
                                                } else {
                                                    echo $all_content[$i]['sub_last_name'];
                                                }
                                                ?>
                                            </p>
                                            <?php
                                            if ($all_content[$i]['sub_first_name'] != '') {
                                                ?>
                                                <p class="pro-b">Sub User</p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4'">
                            <?php if ($all_content[$i]['status_qa'] == "checkforapprove") { ?>
                                <div class="cell-col col-w1">
                                    <p class="neft text-center"><span class="red bluetext text-uppercase text-wrap">Pending-Approval</span></p>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="cli-ent-col td" style="width: 12%;">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <?php if ($all_content[$i]['designer_id'] != "" && $all_content[$i]['designer_id'] != 0) { ?>
                                                <!-- <span class="count_project"><?php //echo $all_content[$i]['designer_project_count'];       ?></span> -->
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="javascript:void(0)" class="<?php echo $all_content[$i]['id']; ?>">
                                                <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col <?php echo $all_content[$i]['id']; ?>">
                                            <p class="text-h text-wrap">
                                                <?php echo $all_content[$i]['designer_first_name']; ?>
                                                <?php
                                                if (strlen($all_content[$i]['designer_last_name']) > 5) {
                                                    echo ucwords(substr($all_content[$i]['designer_last_name'], 0, 1));
                                                } else {
                                                    echo $all_content[$i]['designer_last_name'];
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">Designer</p>
                                            <p class="space-a"></p>
                                            <?php //if ($all_content[$i]['designer_assign_or_not'] == 0) {   ?>
                        <!--                                            <a href="#" class="addde-signersk1 adddesinger" data-toggle="modal" data-requestid="<?php echo $all_content[$i]['id']; ?>" data-designerid= "<?php echo $all_content[$i]['designer_id']; ?>" data-target="#AddDesign">
                                                    <span class="sma-red">+</span> Add Designer
                                                </a>-->
                                            <?php //}    ?>
                                        </div>
                                    <?php } else { ?>
                                        <div class="cell-col <?php echo $all_content[$i]['id']; ?>">No Designer Assigned</div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_request/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_request/" . $all_content[$i]['id']; ?>/4">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?>
                                        </span>
                                        <?php //echo $all_content[$i]['total_chat_all']     ?></a></p>
                            </div>
                        </div>
                    <!--                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-file.png" class="img-responsive" width="13"> <?php if (count($all_content[$i]['total_file']) != 0) { ?>
                                                                    <span class="numcircle-box"><?php echo count($all_content[$i]['total_file']); ?></span></span>
                        <?php } ?>
                        <?php echo count($all_content[$i]['total_files_count']); ?></a></p>
                            </div>
                        </div>-->
                        <?php
                        if ($all_content[$i]['verified'] == 1) {
                            $class = 'verified_by_admin';
                            $disable = '';
                        } elseif ($all_content[$i]['verified'] == 2) {
                            $class = 'verified_by_designer';
                            $disable = 'disabled';
                        } else {
                            $class = '';
                            $disable = '';
                        }
                        ?>
                        <div class="cli-ent-col td" style="width: 10%;">
                            <div class="cli-ent-xbox">
                                <div class="cell-row">
                                    <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $all_content[$i]['id']; ?>">
                                        <label><input type="checkbox" name="project" class="verified" data-pid="<?php echo $all_content[$i]['id']; ?>" <?php echo ($all_content[$i]['verified'] == 1 || $all_content[$i]['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable; ?>/>
                                            <i class="fa fa-circle-thin" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle" aria-hidden="true"></i></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 13%;cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/4'">
                            <?php if ($all_content[$i]['is_trail'] == 1) { ?>
                                <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                            <?php } ?>
                            <div class="cli-ent-xbox">
                                <p class="pro-a">Delivered on</p>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['reviewdate'])); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php
                } 
                elseif ($all_content[$i]['status'] == "approved") {
                    ?>
                    <div class="cli-ent-row tr brdr">
                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                        <div class="cli-ent-col td">
                            <div class="cell-row">
                                <div class="cell-col" >
                                    <input type="checkbox" name="project" class="selected_pro completed" value="<?php echo $all_content[$i]['id']; ?>"/> 
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 28%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <?php if ($all_content[$i]['category_name'] != '' || $all_content[$i]['subcategory_name'] != '') { ?>
                                            <p class="pro-b"><b><?php echo $all_content[$i]['category_name']; ?></b> <b><?php echo isset($all_content[$i]['subcategory_name']) ? "> " . $all_content[$i]['subcategory_name'] : ''; ?></b></p>
                                        <?php } ?>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap"><?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?></p>
                                        <p class="pro-b">Client</p>
                                        <div class="sub_user_req_info qa-sub-user">
                                            <p class="text-h text-wrap">
                                                <?php echo $all_content[$i]['sub_first_name']; ?>
                                                <?php
                                                if (strlen($all_content[$i]['sub_last_name']) > 5) {
                                                    echo ucwords(substr($all_content[$i]['sub_last_name'], 0, 1));
                                                } else {
                                                    echo $all_content[$i]['sub_last_name'];
                                                }
                                                ?>
                                            </p>
                                            <?php
                                            if ($all_content[$i]['sub_first_name'] != '') {
                                                ?>
                                                <p class="pro-b">Sub User</p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cell-col col-w1">
                                <p class="neft text-center"><span class="green text-uppercase text-wrap">COMPLETED</span></p>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 12%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <?php if ($all_content[$i]['designer_id'] != "" && $all_content[$i]['designer_id'] != 0) { ?>
                                                <!-- <span class="count_project"><?php //echo $all_content[$i]['designer_project_count'];        ?></span> -->
                                        <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                            <a href="javascript:void(0)">
                                                <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                    </figure>
                                                <?php } else { ?>
                                                    <figure class="pro-circle-img">
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                    </figure>
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <div class="cell-col">
                                            <p class="text-h text-wrap"><?php echo $all_content[$i]['designer_first_name'] . " " . $all_content[$i]['designer_last_name']; ?></p>
                                            <p class="pro-b">Designer</p>
                                        </div>
                                    <?php } else { ?>
                                        <div class="cell-col <?php echo $all_content[$i]['id']; ?>">No Designer Assigned</div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span></a>
                            </div>
                        </div>

                        <?php
                        if ($all_content[$i]['verified'] == 1) {
                            $class = 'verified_by_admin';
                            $disable = '';
                        } elseif ($all_content[$i]['verified'] == 2) {
                            $class = 'verified_by_designer';
                            $disable = 'disabled';
                        } else {
                            $class = '';
                            $disable = '';
                        }
                        ?>
                        <div class="cli-ent-col td" style="width: 10%;">
                            <div class="cli-ent-xbox">
                                <div class="cell-row">
                                    <div class="cell-col is_verified <?php echo $class; ?>" id="verified_<?php echo $all_content[$i]['id']; ?>">
                                        <label><input type="checkbox" name="project" class="verified" data-pid="<?php echo $all_content[$i]['id']; ?>" <?php echo ($all_content[$i]['verified'] == 1 || $all_content[$i]['verified'] == 2) ? 'checked' : ''; ?> <?php echo $disable; ?>/>
                                            <i class="fa fa-circle-thin" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle" aria-hidden="true"></i></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 13%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <?php if ($all_content[$i]['is_trail'] == 1) { ?>
                                <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                            <?php } ?>
                            <div class="cli-ent-xbox">
                                <h3 class="app-roved green">Approved on</h3>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['approvddate']));
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div> 
                    <?php
                } 
                elseif ($all_content[$i]['status'] == "hold") {
                    ?>
                    <div class="cli-ent-row tr brdr">
                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                        <div class="cli-ent-col td">
                            <div class="cell-row">
                                <div class="cell-col" >
                                    <input type="checkbox" name="project" class="selected_pro completed" value="<?php echo $all_content[$i]['id']; ?>"/> 
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 28%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap"><?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?></p>
                                        <p class="pro-b">Client</p>
                                        <div class="sub_user_req_info qa-sub-user">
                                            <p class="text-h text-wrap">
                                                <?php echo $all_content[$i]['sub_first_name']; ?>
                                                <?php
                                                if (strlen($all_content[$i]['sub_last_name']) > 5) {
                                                    echo ucwords(substr($all_content[$i]['sub_last_name'], 0, 1));
                                                } else {
                                                    echo $all_content[$i]['sub_last_name'];
                                                }
                                                ?>
                                            </p>
                                            <?php
                                            if ($all_content[$i]['sub_first_name'] != '') {
                                                ?>
                                                <p class="pro-b">Sub User</p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6'">
                            <div class="cell-col col-w1">
                                <p class="neft text-center"><span class="holdcolor text-uppercase text-wrap">On Hold</span></p>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 12%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <?php if ($all_content[$i]['designer_id'] != "" && $all_content[$i]['designer_id'] != 0) { ?>
                                    <!-- <span class="count_project"><?php //echo $all_content[$i]['designer_project_count'];      ?></span> -->
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)">
                                            <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap"><?php echo $all_content[$i]['designer_first_name'] . " " . $all_content[$i]['designer_last_name']; ?></p>
                                        <p class="pro-b">Designer</p>
                                    </div>
                                    <?php }else{ ?>
                                            <div class="cell-col <?php echo  $all_content[$i]['id'];?>">No Designer Assigned</div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $completed[$i]['total_chat_all']    ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 13%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <?php if ($all_content[$i]['is_trail'] == 1) { ?>
                                <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                            <?php } ?>
                            <div class="cli-ent-xbox">
                                <h3 class="app-roved holdcolors">Hold on</h3>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['modified']));
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div> 
                    <?php
                }
                elseif ($all_content[$i]['status'] == "cancel") {
                    ?>
                    <div class="cli-ent-row tr brdr">
                        <div class="mobile-visibles"><i class="fa fa-plus-circle"></i></div>
                        <div class="cli-ent-col td">
                            <div class="cell-row">
                                <div class="cell-col" >
                                    <input type="checkbox" name="project" class="selected_pro cancel" value="<?php echo $all_content[$i]['id']; ?>"/> 
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 28%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">

                                    <div class="cell-col" >
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6"><?php echo $all_content[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo substr($all_content[$i]['description'], 0, 30); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)">
                                            <?php if ($all_content[$i]['customer_profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['customer_profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap"><?php echo $all_content[$i]['customer_first_name'] . " " . $all_content[$i]['customer_last_name']; ?></p>
                                        <p class="pro-b">Client</p>
                                        <div class="sub_user_req_info qa-sub-user">
                                            <p class="text-h text-wrap">
                                                <?php echo $all_content[$i]['sub_first_name']; ?>
                                                <?php
                                                if (strlen($all_content[$i]['sub_last_name']) > 5) {
                                                    echo ucwords(substr($all_content[$i]['sub_last_name'], 0, 1));
                                                } else {
                                                    echo $all_content[$i]['sub_last_name'];
                                                }
                                                ?>
                                            </p>
                                            <?php
                                            if ($all_content[$i]['sub_first_name'] != '') {
                                                ?>
                                                <p class="pro-b">Sub User</p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 16%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6'">
                            <div class="cell-col col-w1">
                                <p class="neft text-center"><span class="holdcolor text-uppercase text-wrap">Cancelled</span></p>
                            </div>
                        </div>

                        <div class="cli-ent-col td" style="width: 12%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6'">
                            <div class="cli-ent-xbox text-left">
                                <div class="cell-row">
                                    <?php if ($all_content[$i]['designer_id'] != "" && $all_content[$i]['designer_id'] != 0) { ?>
                                    <!-- <span class="count_project"><?php //echo $all_content[$i]['designer_project_count'];      ?></span> -->
                                    <div class="cell-col" style="width: 36px; padding-right: 10px;">
                                        <a href="javascript:void(0)">
                                            <?php if ($all_content[$i]['profile_picture'] != "") { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $all_content[$i]['profile_picture']; ?>" class="img-responsive">
                                                </figure>
                                            <?php } else { ?>
                                                <figure class="pro-circle-img">
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE; ?>user-admin.png" class="img-responsive">
                                                </figure>
                                            <?php } ?>
                                        </a>
                                    </div>
                                    <div class="cell-col">
                                        <p class="text-h text-wrap"><?php echo $all_content[$i]['designer_first_name'] . " " . $all_content[$i]['designer_last_name']; ?></p>
                                        <p class="pro-b">Designer</p>
                                    </div>
                                    <?php }else{ ?>
                                            <div class="cell-col <?php echo  $all_content[$i]['id'];?>">No Designer Assigned</div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 9%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6'">
                            <div class="cli-ent-xbox text-center">
                                <p class="pro-a inline-per"><a href="<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/6">
                                        <span class="inline-imgsssx"><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>img/qa/icon-chat.png" class="img-responsive" width="21">
                                            <?php if ($all_content[$i]['total_chat'] + $all_content[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $all_content[$i]['total_chat'] + $all_content[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span>
                                        <?php //echo $completed[$i]['total_chat_all']    ?></a></p>
                            </div>
                        </div>
                        <div class="cli-ent-col td" style="width: 13%; cursor: pointer;" onclick="window.location.href = '<?php echo base_url() . "qa/dashboard/view_project/" . $all_content[$i]['id']; ?>/5'">
                            <?php if ($all_content[$i]['is_trail'] == 1) { ?>
                                <div class="promo-req"><?php echo PROMO_TEXT; ?></div>
                            <?php } ?>
                            <div class="cli-ent-xbox">
                                <h3 class="app-roved holdcolors">Cancelled on</h3>
                                <p class="space-a"></p>
                                <p class="pro-b">
                                    <?php echo date('M d, Y h:i A', strtotime($all_content[$i]['modified']));
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div> 
                    <?php
                }
            }
        }
        ?><span id="loadingAjaxCount" data-value="<?php echo $total_result_count; ?>"></span>
        <script>countTimer();</script>    
        <?php
    }

    public function change_expectedDate() {

        $reqid = isset($_POST['edit_reqid']) ? $_POST['edit_reqid'] : '';
        $expected = isset($_POST['edit_expected']) ? $_POST['edit_expected'] : '';
        $expected_format = date('Y-m-d H:i:s', strtotime($expected));
        $gmttime = $this->myfunctions->onlytimezoneforall($expected_format, $_SESSION['timezone']);
        $gmt_format = date('Y-m-d H:i:s', strtotime($gmttime));
        $expectedsec = $this->myfunctions->onlytimezoneforall($gmt_format, 'UTC');
        $expectedsave = date('Y-m-d H:i:s', strtotime($expectedsec));
        $request = $this->Request_model->get_request_by_id($reqid);
        $role['expected_date'] = $expectedsave;
        $role_id = $this->Welcome_model->update_data("requests", $role, array("id" => $reqid));
        if ($role_id) {
            $this->session->set_flashdata('message_success', "Expected date updated successfully!");
            redirect(base_url() . "qa/dashboard");
        }
    }

    public function request_verified_by_qa() {
        $data = array();
        $ischecked = $_POST['ischecked'];
        $requestId = $_POST['data_id'];
        $data['is_verified_by_qa'] = date("Y:m:d H:i:s");
        $data['verified'] = $ischecked;
        $success = $this->Welcome_model->update_data("requests", $data, array("id" => $requestId));
        if ($success) {
            echo json_encode($data);
        }
    }

}
