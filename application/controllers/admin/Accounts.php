<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('Myfunctions');
        $this->load->library('Customfunctions');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->model('Welcome_model');
        $this->load->model('Account_model');
        $this->load->model('Request_model');
        $this->load->model('Stripe');
    }

    public function checkloginuser() {
        if (!$this->session->userdata('user_id')) {
            redirect(base_url());
        }
        if ($this->session->userdata('role') != "admin") {
            redirect(base_url());
        }
    }

    public function create_customer() {
        $this->myfunctions->checkloginuser("admin");
        $planlist = $this->Stripe->getallsubscriptionlist();
        $rendomly_qa_id = $this->Admin_model->getqa_id();
        if (!empty($_POST)) {
            $checkemail = $this->Request_model->getuserbyemail($_POST['email']);
            if (!empty($checkemail)) {
                $this->session->set_flashdata('message_error', "Email Address is already available.!", 5);
                redirect(base_url() . "admin/dashboard/view_clients");
            }
            if (!isset($_POST['bypasspayment'])) {
                $expir_date = explode("/", $_POST['expir_date']);
                $stripe_customer = $this->Stripe->createnewCustomer($_POST['email'], "");

                if (!$stripe_customer['status']) {
                    $this->session->set_flashdata('message_error', "Customer Not Created Successfully.!", 5);
                    redirect(base_url() . "admin/dashboard/view_clients");
                }
                $customer_id = $stripe_customer['data']['customer_id'];
                $card_number = trim($_POST['card_number']);
                $expiry_month = $expir_date[0];
                $expiry_year = $expir_date[1];
                $cvc = $_POST['cvc'];

                $stripe_card = $this->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);

                if (!$stripe_card['status']):
                    $this->session->set_flashdata('message_error', $stripe_card['message'], 5);
                    redirect(base_url() . "admin/dashboard/view_clients");
                endif;

                $subscription_plan_id = $_POST['plan_name'];

                $stripe_subscription = '';
                if ($subscription_plan_id) {
                    $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($stripe_customer['data']['customer_id'], $subscription_plan_id);
                }

                if (!$stripe_subscription['status']) {
                    $this->session->set_flashdata('message_error', $stripe_subscription['message'], 5);
                    redirect(base_url() . "admin/dashboard/view_clients");
                }
            } else {
                $stripe_subscription['data']['subscription_id'] = null;
                $stripe_customer['data']['customer_id'] = null;
                $subscription_plan_id = $_POST['plan_name'];
            }
            date_default_timezone_set('America/Chicago');
            $current_date = date('Y-m-d H:i:s');
            $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($set), 0, 20);
            $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
            $customer['customer_id'] = $stripe_customer['data']['customer_id'];
            $customer['plan_name'] = $subscription_plan_id;
            $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);
            // $customer['activation'] = $code;
            $customer['is_active'] = 1;
            $customer['plan_amount'] = $plandetails['amount'] / 100;
            $customer['plan_interval'] = $plandetails['interval'];
            $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
            $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
            $customer['display_plan_name'] = $plandetails['name'];
            $customer['email'] = $_POST['email'];
            $customer['total_inprogress_req'] = TOTAL_INPROGRESS_REQUEST;
            $customer['total_active_req'] = TOTAL_ACTIVE_REQUEST;
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['new_password'] = md5($_POST['password']);
            $customer['state'] = isset($_POST['state']) ? $_POST['state'] : '';
            $customer['role'] = "customer";
            $customer['qa_id'] = $rendomly_qa_id[0]['id'];
            $customer['phone'] = $_POST['phone'];   
            $customer['created'] = $current_date;
//            echo "<pre/>";print_R($customer);exit;
            $id = $this->Welcome_model->insert_data("users", $customer);
            if ($id) {
                //$this->send_email_after_signup($code,$id);
                $this->session->set_flashdata('message_success', "User created successfully and subscribed successfully..!", 5);
                redirect(base_url() . "admin/dashboard/view_clients");
            } else {
                $this->session->set_flashdata('message_error', "User can not created successfully ..!", 5);
                redirect(base_url() . "admin/dashboard/view_clients");
            }
        }

        $subscription_plan_list = array();
        $j = 0;
        for ($i = 0; $i < sizeof($planlist); $i++) {
            if ($planlist[$i]['id'] == '30291' || $planlist[$i]['id'] == 'A21481D' || $planlist[$i]['id'] == 'A1900S' || $planlist[$i]['id'] == 'M99S' || $planlist[$i]['id'] == 'M199G' || $planlist[$i]['id'] == 'M1991D' || $planlist[$i]['id'] == 'M199S' || $planlist[$i]['id'] == 'A10683D' || $planlist[$i]['id'] == 'M993D') {
                continue;
            }
            $subscription_plan_list[$j]['id'] = $planlist[$i]['id'];
            $subscription_plan_list[$j]['name'] = $planlist[$i]['name'] . " - $" . $planlist[$i]['amount'] / 100 . "/" . $planlist[$i]['interval'];
            $subscription_plan_list[$j]['amount'] = $planlist[$i]['amount'] / 100;
            $subscription_plan_list[$j]['interval'] = $planlist[$i]['interval'];
            $subscription_plan_list[$j]['trial_period_days'] = $planlist[$i]['trial_period_days'];
            $j++;
        }

        $this->load->view('front_end/header', array('current_action' => 'signup'));
        $this->load->view('front_end/signup', array("planlist" => $subscription_plan_list));
        $this->load->view('front_end/footer');
    }

    public function index() {
        $this->myfunctions->checkloginuser("admin");
        $today_customer = $this->Admin_model->get_today_customer();
        $all_customer = $this->Admin_model->get_total_customer();
        $today_requests = $this->Admin_model->get_today_request();
        $total_requests = $this->Admin_model->get_total_request();

        $this->load->view('admin/admin_header');
        $this->load->view('admin/index', array('today_customer' => $today_customer, 'all_customer' => $all_customer, 'today_requests' => $today_requests, 'total_requests' => $total_requests));
        $this->load->view('admin/admin_footer');
    }

    public function view_client_profile($id) {
        $this->myfunctions->checkloginuser("admin");
        $titlestatus = "view_clients";
        $data = $this->Admin_model->getuser_data($id);
        //echo "<pre/>";print_R($data);exit;
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',"Clients &nbsp; >" => base_url().'admin/dashboard/view_clients',"Client Profile" => base_url().'accounts/view_client_profile/'.$id,);
        if($data[0]['parent_id'] == 0){
            $mainuser = $data[0]['id'];
        }else{
            $mainuser = $data[0]['parent_id'];
        }
//        echo "<pre/>";print_R($data);exit;
        $data[0]['profile_picture'] = $this->customfunctions->getprofileimageurl($data[0]['profile_picture']);
        if($data[0]['billing_start_date'] != "" && $data[0]['billing_end_date'] != ""){
        $data[0]['billing_start_date'] = $this->myfunctions->onlytimezoneforall($data[0]['billing_start_date']);
        $data[0]['billing_end_date'] = $this->myfunctions->onlytimezoneforall($data[0]['billing_end_date']);
        }
        $user_management = $this->Request_model->getAllsubUsers($data[0]['id'],"manager");
        $client_management = $this->Request_model->getAllsubUsers($data[0]['id'],"client");
        if($data[0]['parent_id'] == 0){
            $brandprofile = $this->Request_model->get_brand_profile_by_user_id($mainuser);
        }else{
            $brandprofile = $this->Request_model->get_brand_profile_by_user_id($mainuser,'created_by');
        }
        for($i=0; $i<sizeof($brandprofile);$i++){
            $brand_materials_files = $this->Request_model->get_brandprofile_materials_files($brandprofile[$i]['id']);
            for ($j = 0; $j < sizeof($brand_materials_files); $j++) {
                if ($brand_materials_files[$j]['file_type'] == 'logo_upload') {
                    $brandprofile[$i]['latest_logo'] = $brand_materials_files[$j]['filename'];
                    break;
                }
            }
        }
//        echo "<pre/>";print_R($brandprofile);exit;
        $plan_data = $this->Request_model->getsubscriptionlistbyplanid($data[0]['plan_name']);
        //$plan_data[0]['tier_prices'] = $this->Request_model->gettierpriceofplan($data[0]['plan_name']);
        
        if ($plan_data[0]['new_plan_id'] != '') {
            $new_plan_data = $this->Request_model->getsubscriptionlistbyplanid($plan_data[0]['new_plan_id']);
        }
        $total_inprogress_req = $data[0]['total_inprogress_req'];
        if (($data[0]['current_plan'] != "" || $data[0]['current_plan'] != null ) || (in_array($data[0]['plan_name'], NEW_PLANS))) {
            if (in_array($data[0]['plan_name'], NEW_PLANS)) {
                $card_details = $this->get_customer_info_frm_customer_id($data[0]['customer_id']);
            } else {
                $card_details = $this->get_customer_detail_from_strip($data[0]['current_plan']);
            }
            $plan_details = $this->Request_model->getsubscriptionlistbyplanid($data[0]['plan_name']);
        } else {
            $card_details = [];
            $plan_details = [];
        }
        if ($data[0]['next_plan_name'] != '') {
            $nextplan_details = $this->Request_model->getsubscriptionlistbyplanid($data[0]['next_plan_name']);
        } else {
            $nextplan_details = [];
        }
        if($data[0]['overwrite'] == 0){
            $data[0]['sub_user_count'] = $plan_data[0]['total_sub_user'];
            $data[0]['brand_profile_count'] = $plan_data[0]['brand_profile_count'];
            $data[0]['file_management'] = $plan_data[0]['file_management'];
            $data[0]['file_sharing'] = $plan_data[0]['file_sharing'];
            $data[0]['turn_around_days'] = $plan_data[0]['turn_around_days'];
        }
        $subusers = $this->Admin_model->sumallsubuser_requestscount($mainuser);
        $main_active_req = $data[0]['total_active_req'];
        $main_inprogress_req = $data[0]['total_inprogress_req'];
        $subtotal_active_req = $subusers['total_active_req'];
        $subtotal_inprogress_req = $subusers['total_inprogress_req'];
        $checksubuser_requests = array('main_active_req' => $main_active_req,
            'main_inprogress_req' => $main_inprogress_req,
            'subtotal_active_req' => isset($subtotal_active_req)?$subtotal_active_req:0,
            'subtotal_inprogress_req' => isset($subtotal_inprogress_req)?$subtotal_inprogress_req:0);
        //$designer_list = $this->Request_model->get_designer_list_for_qa("array");
        $this->load->view('admin/admin_header_1',array("breadcrumbs" => $breadcrumbs));
        $this->load->view('admin/accounts/view_client_profile', array("data" => $data, "plan_data" => $plan_data, "new_plan_data" => $new_plan_data, "nextplan_details" => $nextplan_details, 'card' => $card_details, 'plan' => $plan_details, 'total_inprogress_req' => $total_inprogress_req,"user_management" => $user_management,"client_management" => $client_management,"brandprofile" => $brandprofile,"checksubuser_requests" => $checksubuser_requests));
        $this->load->view('admin/admin_footer');
    }

    public function edit_profile_image_form($id) {
        $this->myfunctions->checkloginuser("admin");
        $useremail = isset($_POST['notification_email']) ? $_POST['notification_email'] : '';
        $user_id = $id;
        $checkuser_by_email = $this->Admin_model->check_user_by_email($useremail, $user_id);
        if ($_FILES['profile']['name'] != "" || isset($_POST)) {
            $fname = explode(' ', $_POST['first_name']);
            if (!empty($_FILES) || isset($_POST)) {
                $config = array(
                    'upload_path' => './uploads/profile_picture',
                    'allowed_types' => "gif|jpg|png|jpge",
                    'encrypt_name' => TRUE
                );
                //$config2['profile'] = $_FILES['profile']['name'];

                $this->load->library('upload', $config);
                // $this->upload->initialize($config2);
                if ($this->upload->do_upload('profile')) {
                    $data = array($this->upload->data());
                    $profile_pic = array(
                        'profile_picture' => $data[0]['file_name'],
                        'first_name' => $fname[0],
                        'last_name' => $fname[1],
                        'email' => $_POST['notification_email'],
                    );

                    $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
                    if ($update_data_result) {
                        $this->session->set_flashdata('message_success', 'User Profile Picture Update Successfully.!', 5);
                    } else {
                        $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!', 5);
                    }
                } else {
                    if (!empty($checkuser_by_email)) {
                        $this->session->set_flashdata('message_error', 'Email Address is already available!', 5);
                    } else {
                        $profile_pic = array(
                            'first_name' => $fname[0],
                            'last_name' => $fname[1],
                            'email' => $_POST['notification_email'],
                        );

                        $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
                        if ($update_data_result) {
                            $this->session->set_flashdata('message_success', 'User Profile Picture Update Successfully.!', 5);
                        } else {
                            $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!', 5);
                        }
                    }
                }
            }
        }
    }

    public function change_password_front($id) {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($id);

            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "admin/accounts/view_client_profile/" . $id);
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $id))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "admin/accounts/view_client_profile/" . $id);
        }
    }

    public function designer_change_password_profile($id) {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($id);

            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "admin/accounts/view_designer_profile/" . $id);
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $id))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "admin/accounts/view_designer_profile/" . $id);
        }
    }

    public function change_password_old($id) {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $user_data = $this->Admin_model->getuser_data($id);

            if ($_POST['new_password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "admin/accounts/view_qa_profile/" . $id);
            }
            if ($this->Admin_model->update_data("users", array("new_password" => md5($_POST['new_password'])), array("id" => $id))) {
                $this->session->set_flashdata("message_success", "Password is changed successfully..!", 5);
            } else {
                $this->session->set_flashdata("message_error", "Password is not changed successfully..!", 5);
            }
            redirect(base_url() . "admin/accounts/view_qa_profile/" . $id);
        }
    }

    public function view_designer_profile($id) {
        $this->myfunctions->checkloginuser("admin");
        $this->Welcome_model->update_online();
        $user_id = $_SESSION['user_id'];
        $skills = $this->Request_model->getallskills($id);
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',"Designers &nbsp; >" => base_url().'admin/dashboard/view_designers',"Designer Profile" => base_url().'accounts/view_designer_profile/'.$id,);
        $data = $this->Admin_model->getuser_data($id);
        $data[0]['profile_picture'] = $this->customfunctions->getprofileimageurl($data[0]['profile_picture']);
//        $profile_data = $this->Admin_model->getuser_data($user_id);
//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $designer_list = $this->Request_model->get_designer_list_for_qa("array");
        $this->load->view('admin/admin_header_1',array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/accounts/view_designer_profile', array('data' => $data, 'skills' => $skills));
        $this->load->view('admin/admin_footer');
    }

    public function designer_edit_profile_pic($id) {
        $this->myfunctions->checkloginuser("admin");
        $user_id = $id;
        if (isset($_POST['submit_pic'])) {
            if ($_FILES['profile']['name'] != "") {
                $file = pathinfo($_FILES['profile']['name']);
                $s3_file = $file['filename'] . '-' . rand(1000, 1) . '.' . $file['extension'];
                $config = array(
                    'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_PROFILE,
                    'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                    'file_name' => $s3_file
                );
                $check = $this->myfunctions->CustomFileUpload($config, 'profile');
//                echo "<pre/>";print_R($check);exit;
                if ($check['status'] == 1) {
                    $profile_pic = array(
                        'profile_picture' => $s3_file,
                    );
                    $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
                    if ($update_data_result) {
                        $this->session->set_flashdata('message_success', 'User Profile Picture Update Successfully.!');
                    } else {
                        $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!');
                    }
                } else {
                    $this->session->set_flashdata('message_error', $check['msg'], 5);
                    //$this->session->set_flashdata('message_error', $data, 5);
                }
            }
            $name = explode(" ", $_POST['first_last_name']);
            $profileUpdate = array(
                'first_name' => $name[0],
                'last_name' => $name[1],
            );
            $update_data_result = $this->Admin_model->designer_update_profile($profileUpdate, $user_id);
            if ($update_data_result) {
                $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
            } else {
                $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
            }
        }
        redirect(base_url() . "admin/accounts/view_designer_profile/" . $id);
    }

    public function addskills($id) {
        $this->myfunctions->checkloginuser("admin");
        $user_id = $id;
        if (!empty($_POST)) {
            $skills = array(
                'brandin_logo' => $this->input->post('brandin_logo'),
                'ads_banner' => $this->input->post('ads_banner'),
                'email_marketing' => $this->input->post('email_marketing'),
                'webdesign' => $this->input->post('webdesign'),
                'appdesign' => $this->input->post('appdesign'),
                'graphics' => $this->input->post('graphics'),
                'illustration' => $this->input->post('illustration'),
                'user_id' => $user_id,
                'created' => date("Y:m:d H:i:s"),
            );
            $updateskills = array(
                'brandin_logo' => $this->input->post('brandin_logo'),
                'ads_banner' => $this->input->post('ads_banner'),
                'email_marketing' => $this->input->post('email_marketing'),
                'webdesign' => $this->input->post('webdesign'),
                'appdesign' => $this->input->post('appdesign'),
                'graphics' => $this->input->post('graphics'),
                'illustration' => $this->input->post('illustration'),
                'user_id' => $user_id,
                'modified' => date("Y:m:d H:i:s"),
            );
            $getskills = $this->Request_model->getallskills($user_id);
            if (empty($getskills)) {
                $success = $this->Welcome_model->insert_data("user_skills", $skills);
            } else {
                $upsuccess = $this->Admin_model->update_data('user_skills', $updateskills, array("user_id" => $user_id));
            }
            if ($success) {
                $this->session->set_flashdata('message_success', 'Skills added successfully', 5);
            } else {
                if ($upsuccess) {
                    $this->session->set_flashdata('message_success', 'Skills Update successfully', 5);
                } else {
                    $this->session->set_flashdata('message_error', 'Skills not update successfully', 5);
                }
            }

            redirect(base_url() . "admin/accounts/view_designer_profile/" . $id.'#skills');
        }
    }

    public function designer_edit_profile($id) {
        $this->myfunctions->checkloginuser("admin");
        $user_id = $id;
        if (isset($_POST['submit'])) {
           // echo "<pre/>";print_R($_POST);exit;
            $profileUpdate = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'paypal_email_address' => $this->input->post('paypal_id'),
                'phone' => $this->input->post('phone_number'),
                'address_line_1' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'zip' => $this->input->post('zip_code'),
            );
            $skillsadd = array(
                'hobbies' => $this->input->post('hobbies'),
                'about' => $this->input->post('about'),
              );
            $update_data_result = $this->Admin_model->designer_update_profile($profileUpdate, $user_id);
            $update_data_result =  $this->Admin_model->designer_update_hobbiesabout($skillsadd, $user_id);
            if ($update_data_result) {
                
                $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
            } else {
                $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
            }
            redirect(base_url() . "admin/accounts/view_designer_profile/" . $id);
        }
    }

    public function view_qa_profile($id) {
        $this->myfunctions->checkloginuser("admin");
        $user_id = $id;
        $profile_data = $this->Admin_model->getuser_data($user_id);
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',"QA &nbsp; >" => base_url().'admin/dashboard/view_qa',"QA Profile" => base_url().'accounts/view_qa_profile/'.$id,);
        if ($this->input->post('submit')) {
            $fndlname = explode(" ", $this->input->post('fndlname'));
            if ($_FILES['qaprofilepic']['name'] != "") {
                if (!empty($_FILES)) {
                    $config = array(
                        'upload_path' => './uploads/profile_picture',
                        'allowed_types' => "gif|jpg|png|jpge",
                        'encrypt_name' => TRUE
                    );
                    //$config2['profile'] = $_FILES['profile']['name'];

                    $this->load->library('upload', $config);
                    // $this->upload->initialize($config2);
                    if ($this->upload->do_upload('qaprofilepic')) {
                        $data = array($this->upload->data());
                        $profile_pic = array(
                            'profile_picture' => $data[0]['file_name'],
                        );
                        $update_data_result = $this->Admin_model->designer_update_profile($profile_pic, $user_id);
                        if ($update_data_result) {
                            $this->session->set_flashdata('message_success', 'User Profile Picture Update Successfully.!', 5);
                        } else {
                            $this->session->set_flashdata('message_error', 'User Profile Picture Not Update.!', 5);
                        }
                    } else {
                        $data = array('error' => $this->upload->display_errors());
                        $this->session->set_flashdata('message_error', $data, 5);
                    }
                }
            }
            if (!empty($this->input->post())) {
//                $data = array(
//                    'first_name' => $fndlname[0],
//                    'last_name' => $fndlname[1],
//                    'email' => $this->input->post('email'),
//                );
                $update_data_result = $this->Admin_model->designer_update_profile($data, $user_id);
                if ($update_data_result) {
                    $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
                } else {
                    $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
                }
            }
            redirect(base_url() . "admin/accounts/view_qa_profile/" . $id);
        }
        $profile_data = $this->Admin_model->getuser_data($id);
//        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
//        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
//        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $designer_list = $this->Request_model->get_designer_list_for_qa("array");
        $this->load->view('admin/admin_header_1',array("breadcrumbs" => $breadcrumbs));
        $this->load->view('admin/accounts/view_qa_profile', array("edit_profile" => $profile_data));
        $this->load->view('admin/admin_footer');
    }

    public function editqadetaiL($id) {
        $this->myfunctions->checkloginuser("admin");
        // echo "<pre/>";print_R($_POST);exit;
        $user_id = $id;
        $data = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'paypal_email_address' => $this->input->post('paypalid'),
            'phone' => $this->input->post('phone'),
            'title' => $this->input->post('title'),
            'address_line_1' => $this->input->post('address'),
            'city' => $this->input->post('city'),
            'state' => $this->input->post('state'), 
            'zip' => $this->input->post('zip'),
        );
        $update_data_result = $this->Admin_model->designer_update_profile($data, $user_id);
        if ($update_data_result) {
            $this->session->set_flashdata('message_success', 'Profile Updated Successfully.!', 5);
        } else {
            $this->session->set_flashdata('message_error', 'Profile Not Updated.!', 5);
        }
        redirect(base_url() . "admin/accounts/view_qa_profile/" . $id);
    }

    public function customer_edit_profile($id) {
        $this->myfunctions->checkloginuser("admin");
        $user_id = $id;
        $profileUpdate = array(
            'first_name' => $_POST['first_name'],
            'last_name' => $_POST['last_name'],
            'email' => $_POST['notification_email'],
            'company_name' => $_POST['company_name'],
            'phone' => $_POST['phone'],
            'address_line_1' => $_POST['address_line_1'],
            'title' => $_POST['title'],
            'city' => $_POST['city'],
            'state' => isset($_POST['state']) ? $_POST['state'] : '',
            'zip' => $_POST['zip'],
        );
        $update_data_result = $this->Admin_model->designer_update_profile($profileUpdate, $user_id);
        if ($update_data_result) {
            $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
        } else {
            $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
        }
        redirect(base_url() . "admin/accounts/view_client_profile/".$id);
    }

    public function customer_active_requests($id) {
        $this->myfunctions->checkloginuser("admin");
        $user_id = $id;
       
        $subuser_count = isset($_POST["users_size"])?$_POST["users_size"]:0;
        $brand_count = isset($_POST["brand_size"])?$_POST["brand_size"]:0;
        $filemanagement = isset($_POST['file_management']) ? $_POST['file_management'] : 0;
        $file_sharing = isset($_POST['file_sharing']) ? $_POST['file_sharing'] : 0;
        $user_data = $this->Admin_model->getuser_data($user_id);
        $user_info_data = $this->Admin_model->getextrauser_data($user_id);
        $globaluserinfo = $this->Request_model->getsubscriptionlistbyplanid($user_data[0]['plan_name']);
        
        $user_setting = array(
            'total_active_req' => isset($_POST['total_req_count']) ? $_POST['total_req_count'] : 0,
            'total_inprogress_req' => isset($_POST['inprogess_req_count']) ? $_POST['inprogess_req_count'] : 0,
            'total_requests' => isset($_POST['total_requests']) ? $_POST['total_requests'] : 0,
            'billing_cycle_request' => isset($_POST['billing_cycle_request']) ? $_POST['billing_cycle_request'] : 0,
            'turn_around_days' => isset($_POST['turn_around_days']) ? $_POST['turn_around_days'] : 0,
            'modified' => date("Y:m:d H:i:s")
        );
        /** extra user info array **/
        
        $user_info = array("sub_user_count" => $subuser_count,
            "brand_profile_count" => $brand_count,
            "file_management" => $filemanagement,
            "file_sharing" => $file_sharing,
            'modified' => date("Y:m:d H:i:s"));
        
        /** end extra user info array **/
        if ($globaluserinfo[0]['global_active_request'] != $user_setting['total_active_req'] || $globaluserinfo[0]['global_inprogress_request'] != $user_setting['total_inprogress_req'] || $globaluserinfo[0]['active_request'] != $user_setting['total_requests'] || $globaluserinfo[0]['turn_around_days'] != $user_setting['turn_around_days'] || $globaluserinfo[0]['turn_around_days'] != $user_setting['turn_around_days'] || $globaluserinfo[0]['total_sub_user'] != $user_info['sub_user_count'] || $globaluserinfo[0]['brand_profile_count'] != $user_info['brand_profile_count'] || $globaluserinfo[0]['file_management'] != $user_info['file_management'] || $globaluserinfo[0]['file_sharing'] != $user_info['file_sharing']) {
            $user_setting['overwrite'] = 1;
        }
        $update_data_result = $this->Admin_model->designer_update_profile($user_setting, $user_id);
        if ($update_data_result) {
            /** save extra user info **/
            if(empty($user_info_data)){
                $user_info['user_id'] = $user_id;
                $user_info['created'] = date("Y:m:d H:i:s");
                $this->Welcome_model->insert_data("users_info", $user_info); 
            }else{
//                 echo $user_id."<pre>";print_R($user_info);exit;
                $this->Welcome_model->update_data("users_info", $user_info, array('user_id' => $user_id));
            }
            /** end save extra user info **/
            $this->myfunctions->moveinqueuereq_toactive_after_updrage_plan($user_id, $_POST['inprogess_req_count']);
            $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
        } else {
            $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
        }
    }

    public function view_clients() {
        $this->myfunctions->checkloginuser("admin");

        $data = $this->Account_model->getall_customer("assigned");
        for ($i = 0; $i < sizeof($data); $i++) {
            $data[$i]['total_rating'] = $this->Request_model->get_average_rating($data[$i]['id']);
        }

        $data2 = $this->Account_model->getall_customer("unassigned");
        for ($i = 0; $i < sizeof($data2); $i++) {
            $data2[$i]['total_rating'] = $this->Request_model->get_average_rating($data2[$i]['id']);
        }

        $designer_list = $this->Account_model->getall_designer();

        // $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        // $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        // $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        // $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $this->load->view('admin/admin_header');
        //$this->load->view('admin/admin_header', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number));
        //$this->load->view('admin/accounts/view_clients',array("data"=>$data));
        $this->load->view('admin/accounts/view_clients', array("data" => $data, "data2" => $data2, "designer_list" => $designer_list));
        $this->load->view('admin/admin_footer');
    }

    public function client_info($id) {
        $this->myfunctions->checkloginuser("admin");
        $data = $this->Account_model->getuserbyid($id);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/accounts/client_info', array("data" => $data));
        $this->load->view('admin/admin_footer');
    }

    public function view_designers() {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {

            if ($_POST['password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata('message_error', 'Password and Confirm Password is not Match.!', 5);

                redirect(base_url() . "admin/accounts/view_designers");
            }
            $email_check = $this->Account_model->getuserbyemail($_POST['email']);
            if (!empty($email_check)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available.!', 5);

                redirect(base_url() . "admin/accounts/view_designers");
            }
            $success = $this->Welcome_model->insert_data("users", array("first_name" => $_POST['first_name'], "last_name" => $_POST['last_name'], "email" => $_POST['email'], "phone" => $_POST['phone'], 'new_password' => md5($_POST['password']), "status" => "1", "is_active" => "1", "role" => "designer", "created" => date("Y-m-d H:i:s")));
            if ($success) {
                //   echo "<pre>";print_r($success);
                $this->session->set_flashdata('message_success', 'Designer Added Successfully.!', 5);

                redirect(base_url() . "admin/accounts/view_designers");
            } else {
                $this->session->set_flashdata('message_error', 'Designer Not Added Successfully.!', 5);

                redirect(base_url() . "admin/accounts/view_designers");
            }
        }
        $designers = $this->Account_model->getall_designer();

        for ($i = 0; $i < sizeof($designers); $i++) {
            $designers[$i]['total_rating'] = $this->Request_model->get_average_rating($designers[$i]['id']);
            //echo $designers[$i]['id']."</br>";
        }

        $this->load->view('admin/admin_header');
        $this->load->view('admin/accounts/view_designer', array("designers" => $designers));
        $this->load->view('admin/admin_footer');
    }

    public function view_qa_va() {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            if ($_POST['password'] != $_POST['confirm_password']) {
                $this->session->set_flashdata('message_error', 'Password and Confirm Password is not Match.!', 5);

                redirect(base_url() . "admin/accounts/view_qa_va");
            }
            $email_check = $this->Account_model->getuserbyemail($_POST['email']);
            if (!empty($email_check)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available.!', 5);

                redirect(base_url() . "admin/accounts/view_qa_va");
            }
            $success = $this->Welcome_model->insert_data("users", array("first_name" => $_POST['first_name'], "last_name" => $_POST['last_name'], "email" => $_POST['email'], "phone" => $_POST['phone'], 'new_password' => md5($_POST['password']), "status" => "1", "role" => $_POST['role'], "created" => date("Y-m-d H:i:s")));
            if ($success) {
                $this->session->set_flashdata('message_success', strtoupper($_POST['role']) . ' Added Successfully.!', 5);

                redirect(base_url() . "admin/accounts/view_qa_va");
            } else {
                $this->session->set_flashdata('message_error', strtoupper($_POST['role']) . ' Not Added Successfully.!', 5);

                redirect(base_url() . "admin/accounts/view_qa_va");
            }
        }

        $qa = $this->Account_model->getqa_member();

        /* new */
        for ($i = 0; $i < sizeof($qa); $i++) {
            $designers = $this->Request_model->get_designer_list_for_qa("array", $qa[$i]['id']);
            $qa[$i]['total_designer'] = sizeof($designers);

            $mydesigner = $this->Request_model->get_designer_list_for_qa("", $qa[$i]['id']);


            if ($mydesigner) {
                $data = $this->Request_model->get_customer_list_for_qa("array", $mydesigner);
                $qa[$i]['total_clients'] = sizeof($data);
            } else {
                $qa[$i]['total_clients'] = 0;
            }
        }


        $va = $this->Account_model->getva_member();
        /* new */
        for ($i = 0; $i < sizeof($va); $i++) {
            $designers = $this->Request_model->get_designer_list_for_qa("array", $va[$i]['id']);
            $va[$i]['total_designer'] = sizeof($designers);

            $mydesignerr = $this->Request_model->get_designer_list_for_qa("", $va[$i]['id']);


            if ($mydesignerr) {
                $data = $this->Request_model->get_customer_list_for_qa("array", $mydesignerr);
                $va[$i]['total_clients'] = sizeof($data);
            } else {
                $va[$i]['total_clients'] = 0;
            }
        }


        $this->load->view('admin/admin_header');
        $this->load->view('admin/accounts/view_qa_va', array("qa" => $qa, "va" => $va));
        $this->load->view('admin/admin_footer');
    }

    public function edit_customer($customer_id = null) {
        $this->myfunctions->checkloginuser("admin");
        if ($customer_id == "") {
            redirect(base_url() . "admin/all_customer");
        }
        $customer = $this->Admin_model->getuser_data($customer_id);
        if (empty($customer)) {
            $this->session->set_flashdata('message_error', 'No Customer Found.!', 5);
            redirect(base_url() . "admin/all_customer");
        }
        $subscription_plan_list = array();
        $subscription = array();
        $planid = "";
        if ($customer[0]['current_plan'] != "") {
            $getcurrentplan = $this->Stripe->getcurrentplan($customer[0]['current_plan']);
            print_r($getcurrentplan);
            if (!empty($getcurrentplan)) {
                $plandetails = $getcurrentplan->items->data[0]->plan;
                $planid = $plandetails->id;
            } else {
                $plandetails = "";
                $planid = "";
            }
        }
        $allplan = $this->Stripe->getallsubscriptionlist();
        $subscription[] = "Select Plan";
        for ($i = 0; $i < sizeof($allplan); $i++) {
            $subscription[$allplan[$i]['id']] = $allplan[$i]['name'] . " - $" . $allplan[$i]['amount'] / 100 . "/" . $allplan[$i]['interval'];
        }
        $carddetails = array();
        $carddetails['last4'] = "";
        $carddetails['exp_month'] = "";
        $carddetails['exp_year'] = "";
        $carddetails['brand'] = "";
        $plan = array();
        if ($customer[0]['current_plan'] != "" && $customer[0]['current_plan'] != "0") {
            $getcurrentplan = $this->Stripe->getcurrentplan($customer[0]['current_plan']);
            if (!empty($getcurrentplan)) {
                $plan['userid'] = $getcurrentplan->id;
                $plan['current_period_end'] = $getcurrentplan->current_period_end;
                $plan['current_period_start'] = $getcurrentplan->current_period_start;
                $plan['billing'] = $getcurrentplan->billing;
                $plan['trial_end'] = $getcurrentplan->trial_end;
                $plan['trial_start'] = $getcurrentplan->trial_start;


                $plandetails = $getcurrentplan->items->data[0]->plan;
                $plan['planname'] = $plandetails->name;
                $plan['planid'] = $plandetails->id;
                $plan['interval'] = $plandetails->interval;
                $plan['amount'] = $plandetails->amount / 100;
                $plan['created'] = $plandetails->created;

                $customerdetails = $this->Stripe->getcustomerdetail($customer[0]['current_plan']);
                if (!empty($customerdetails)) {
                    $carddetails['last4'] = $customerdetails->sources->data[0]->last4;
                    $carddetails['exp_month'] = $customerdetails->sources->data[0]->exp_month;
                    $carddetails['exp_year'] = $customerdetails->sources->data[0]->exp_year;
                    $carddetails['brand'] = $customerdetails->sources->data[0]->brand;
                }
            }
        } else {
            $plan['planid'] = "";
        }
        $designer_list = $this->Admin_model->get_total_customer("designer");
        $designs_requested = $this->Admin_model->get_all_requested_designs(array(), $customer_id, "no");
        $designs_approved = $this->Admin_model->get_all_requested_designs(array("approved"), $customer_id, "", "", "", "status_admin");
        $this->load->view('admin/admin_header');
        $this->load->view('admin/edit_customer', array("customer" => $customer, "subscription" => $subscription, "planid" => $planid, "carddetails" => $carddetails, "designer_list" => $designer_list, "designs_requested" => $designs_requested, "designs_approved" => $designs_approved));
        $this->load->view('admin/admin_footer');
    }

    public function view_request($id) {
        $this->myfunctions->checkloginuser("admin");
        if ($id == "") {
            redirect(base_url() . "admin/all_requests");
        }
        $request = $this->Admin_model->get_all_requested_designs(array(), "", "", $id);

        //$this->RequestDiscussions->updateAll(array('admin_seen' => 1),array('request_id' => $request_id));
        $request_id = $request[0]['id'];

        if ($_POST) {

            if (isset($_POST['changestate'])) {

                if ($_POST['state_id'] != '0') {
                    $update_data = array('status' => $_POST['state_id'], 'status_designer' => $_POST['state_id'], 'status_admin' => $_POST['state_id'], 'modified' => date("Y-m-d h:i:s"));
                    if ($_POST['state_id'] == "active") {
                        $update_data['dateinprogress'] = date("Y-m-d h:i:s");
                    }
                    if ($_POST['state_id'] == "disapprove") {
                        $update_data['dateinprogress'] = date("Y-m-d h:i:s");
                    }
                    if ($this->Requests->updateAll($update_data, array('id' => $request_id))) {
                        $this->Flash->success(__('State Change Successfully.'));
                    } else {
                        $this->Flash->error(__('State is not Changed'));
                    }
                } else {
                    $this->Flash->error(__('Please Select State.'));
                }
                $this->redirect();
            } else {
                if ($_POST['designer_id'] != "" && $_POST['designer_id'] != 0) {
                    if ($request->id) {
                        $designer_id = $_POST["designer_id"];
                        $request_id = $request->id;
                        if ($request->status == "pending" || $request->status == "running" || $request->status == "active" || $request->status == "assign" || $request->status == "pendingforapprove" || $request->status == "checkforapprove") {
                            if (isset($_POST['assigndesigner'])) {

                                $activerequest = $this->Requests
                                        ->find()
                                        ->where(['Requests.status' => "active", 'Requests.customer_id' => $request->customer_id, 'Requests.id !=' => $request_id])
                                        ->all()
                                        ->toArray();


                                if (sizeof($activerequest) > 0) {
                                    if ($this->Requests->updateAll(array('designer_id' => $designer_id, 'status' => 'assign', 'status_designer' => 'assign', 'status_admin' => 'assign', 'modified' => date("Y-m-d h:i:s")), array('id' => $request_id))) {
                                        $this->Flash->success(__('Designer Assign Successfully.'));
                                        //return $this->redirect(\Cake\Routing\Router::url($this->referer('/', TRUE), TRUE));
                                    } else {
                                        $this->Flash->error(__('Not Savesss.'));
                                    }
                                } else {
                                    if ($this->Requests->updateAll(array('designer_id' => $designer_id, 'status' => 'active', 'status_designer' => 'active', 'status_admin' => 'active', 'modified' => date("Y-m-d h:i:s"), 'dateinprogress' => date("Y-m-d h:i:s")), array('id' => $request_id))) {
                                        $this->Flash->success(__('Designer Assign Successfully.'));
                                        //return $this->redirect(\Cake\Routing\Router::url($this->referer('/', TRUE), TRUE));
                                    } else {
                                        $this->Flash->error(__('Not Savesss.'));
                                    }
                                }
                            } elseif (isset($_POST["activedesigner"])) {
                                if ($this->Requests->updateAll(array('status' => "active", 'status_designer' => 'active', 'dateinprogress' => date("Y-m-d h:i:s")), array('id' => $request_id))) {
                                    $this->Flash->success(__('Designer Active Successfully.'));
                                    //return $this->redirect(\Cake\Routing\Router::url($this->referer('/', TRUE), TRUE));
                                } else {
                                    $this->Flash->error(__('Not Save.'));
                                }
                            }
                        } elseif ($request->status == "active") {
                            $this->Flash->error(__('Design Already in Progress.'));
                        }
                    }
                } else {
                    $this->Flash->error(__('Please choose Designer. Please, try again.'));
                }
            }
        }

        $request_files = $this->Admin_model->get_requested_files($request_id, $request[0]['customer_id'], "customer");

        $admin_request_files = $this->Admin_model->get_requested_files($request_id, "", "designer");

        $customer_additional_files = $this->Admin_model->get_requested_files($request_id, "", "customer");

        $admin_files = $this->Admin_model->get_requested_files($request_id, "", "admin");

        $designer_list = $this->Admin_model->get_total_customer("designer");

        $request = $this->Admin_model->get_all_requested_designs(array(), "", "", $id, "");

        $this->load->view('admin/admin_header');
        $this->load->view('admin/view_request', array("request" => $request, "request_files" => $request_files, "admin_request_files" => $admin_request_files, "designer_list" => $designer_list, "customer_additional_files" => $customer_additional_files, "admin_files" => $admin_files));
        $this->load->view('admin/admin_footer');
    }

    public function all_designer() {
        $this->myfunctions->checkloginuser("admin");
        $designers = $this->Admin_model->get_total_customer("designer");

        for ($i = 0; $i < sizeof($designers); $i++) {
            $customer = $this->Admin_model->get_total_customer("customer", $designers[$i]['id']);
            $designer_customer[$designers[$i]['id']] = sizeof($customer);
        }
        //print_r($designers); print_r($designer_customer);

        $this->load->view('admin/admin_header');
        $this->load->view('admin/all_designer', array("designers" => $designers, "designer_customer" => $designer_customer));
        $this->load->view('admin/admin_footer');
    }

    public function view_designer($designer_id = null) {
        $this->myfunctions->checkloginuser("admin");
        if ($designer_id == "") {
            redirect(base_url() . "admin/all_designer");
        }
        if (!empty($_POST)) {
            $checkuser_by_email = $this->Admin_model->check_user_by_email($_POST['email'], $designer_id);
            if (!empty($check_user_by_email)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            }
            $_POST['profile_picture'] = "";
            if (isset($_FILES["profile_picture1"]["name"]) && !empty($_FILES["profile_picture1"]["name"])) {
                $config = array(
                    'upload_path' => "public/profile",
                    'allowed_types' => "jpg|png|jpeg|pdf",
                    'encrypt_name' => TRUE
                );

                $new_name = time() . $_FILES["profile_picture1"]['name'];
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("profile_picture1")) {
                    $data = array($this->upload->data());
                    $_POST['profile_picture'] = $data[0]['file_name'];
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message_error', $data['error'], 5);
                    redirect(base_url() . "admin/view_designer/" . $designer_id);
                }
            }
            $data = array("first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "phone" => $_POST['phone'],
                "email" => $_POST['email'],
                "profile_picture" => $_POST['profile_picture'],
                "modified" => date("Y-m-d H:i:s"));

            $success = $this->Welcome_model->update_data("users", $data, array('id' => $designer_id));
            if ($success) {
                $this->session->set_flashdata('message_success', 'Designer Updated Successfully.!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            } else {
                $this->session->set_flashdata('message_error', 'Designer Updated Not Successfully.!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            }
        }
        $designer = $this->Admin_model->getuser_data($designer_id);

        $designs_queue = $this->Admin_model->get_all_requested_designs("", "", "", "", $designer_id, "status_admin");

        $designs_approved = $this->Admin_model->get_all_requested_designs("approved", "", "", "", $designer_id, "status_admin");

        $customer = $this->Admin_model->get_total_customer("customer", $designer_id);

        $this->load->view('admin/admin_header');
        $this->load->view('admin/view_designer', array("designer" => $designer, "designs_queue" => $designs_queue, "customer" => $customer, "designer" => $designer, "designs_approved" => $designs_approved));
        $this->load->view('admin/admin_footer');
    }

    public function add_designer() {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $checkuser_by_email = $this->Admin_model->check_user_by_email($_POST['email']);
            if (!empty($check_user_by_email)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available!', 5);
                redirect(base_url() . "admin/add_designer");
            }
            $_POST['profile_picture'] = "";
            if (isset($_FILES["profile_picture"]["name"]) && !empty($_FILES["profile_picture"]["name"])) {
                $config = array(
                    'upload_path' => "public/profile",
                    'allowed_types' => "jpg|png|jpeg|pdf",
                    'encrypt_name' => TRUE
                );

                $new_name = time() . $_FILES["profile_picture"]['name'];
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("profile_picture")) {
                    $data = array($this->upload->data());
                    $_POST['profile_picture'] = $data[0]['file_name'];
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message_error', $data['error'], 5);
                    redirect(base_url() . "admin/add_designer");
                }
            }
            $data = array("first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "phone" => $_POST['phone'],
                "email" => $_POST['email'],
                "new_password" => md5($_POST['password']),
                "profile_picture" => $_POST['profile_picture'],
                "role" => "designer",
                "is_active" => 1,
                "created" => date("Y-m-d H:i:s"));

            $success = $this->Welcome_model->insert_data("users", $data);
            if ($success) {
                $this->session->set_flashdata('message_success', 'Designer Added Successfully.!', 5);
                redirect(base_url() . "admin/add_designer");
            } else {
                $this->session->set_flashdata('message_error', 'Designer Added Not Successfully.!', 5);
                redirect(base_url() . "admin/add_designer");
            }
        }
        $this->load->view('admin/admin_header');
        $this->load->view('admin/add_designer');
        $this->load->view('admin/admin_footer');
    }

    public function add_customer() {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $customer = array();
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['email'] = $_POST['email'];
            $customer['password'] = $_POST['password'];
            $c_password = $_POST['confirm_password'];
            $customer['phone'] = $_POST['phone'];
            $customer['role'] = "customer";
            $exist = $this->Account_model->get_exist_user($_POST['email']);
            if (empty($exist)) {
                if ($customer['password'] == $c_password) {
                    $cusomer_save = $this->Welcome_model->insert_data("users", $customer);
                    if ($cusomer_save) {
                        $this->session->set_flashdata('message_success', 'Customer Added Successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_clients");
                    } else {
                        $this->session->set_flashdata('message_error', 'Customer Added Not Successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_clients");
                    }
                } else {
                    $this->session->set_flashdata('message_error', 'Password does not match', 5);
                    redirect(base_url() . "admin/dashboard/view_clients");
                }
            } else {
                $this->session->set_flashdata('message_error', 'Email is already exist', 5);
                redirect(base_url() . "admin/dashboard/view_clients");
            }
        }
        $this->load->view('admin/admin_header');
        $this->load->view('admin/add_customer', array("subscription" => $subscription));
        $this->load->view('admin/admin_footer');
    }

    public function chat($user_type = null) {
        $this->myfunctions->checkloginuser("admin");
        if ($user_type == "customer" || $user_type == "") {
            $data['customer'] = $this->Account_model->getall_customer();
            $data['active_tab'] = "customer";
        }
        if ($user_type == "designer") {
            $data['customer'] = $this->Account_model->getall_designer();
            $data['active_tab'] = "designer";
        }
        if ($user_type == "qa") {
            $data['customer'] = $this->Account_model->getqa_member();
            $data['active_tab'] = "qa";
        }
        if ($user_type == "va") {
            $data['customer'] = $this->Account_model->getqa_member();
            $data['active_tab'] = "va";
        }
        $this->load->view('admin/admin_header');
        $this->load->view('admin/chat', $data);
        $this->load->view('admin/admin_footer');
    }

    public function room($id = null) {
        if ($id == "" || !isset($_GET['user_type'])) {
            redirect(base_url() . "admin/accounts/chat");
        }
        $first = "admin_id";

        if ($_GET['user_type'] == "customer") {
            $second = "customer_id";
            $active_tab = "customer";
        }
        if ($_GET['user_type'] == "designer") {
            $second = "designer_id";
            $active_tab = "designer";
        }
        if ($_GET['user_type'] == "qa") {
            $second = "qa_id";
            $active_tab = "qa";
        }
        if ($_GET['user_type'] == "va") {
            $second = "va_id";
            $active_tab = "va";
        }

        $room_no = $this->Account_model->chat_data($first, $_SESSION['user_id'], $second, $id);

        if ($room_no == "") {
            redirect(base_url() . "admin/accounts/chat");
        }
        redirect(base_url() . "admin/accounts/room_chat/" . $room_no . "?user_type=" . $active_tab);
    }

    public function room_chat($room_no = null) {
        if ($room_no == "" || !isset($_GET['user_type'])) {
            redirect(base_url() . "admin/accounts/chat");
        }

        if ($_GET['user_type'] == "customer") {
            $second = "customer_id";
            $data['active_tab'] = "customer";
            $data['customer'] = $this->Account_model->getall_customer();
        }
        if ($_GET['user_type'] == "designer") {
            $second = "designer_id";
            $data['active_tab'] = "designer";
            $data['customer'] = $this->Account_model->getall_designer();
        }
        if ($_GET['user_type'] == "qa") {
            $second = "qa_id";
            $data['active_tab'] = "qa";
            $data['customer'] = $this->Account_model->getqa_member();
        }
        if ($_GET['user_type'] == "va") {
            $second = "va_id";
            $data['active_tab'] = "va";
            $data['customer'] = $this->Account_model->getva_member();
        }
        $data['userdata'] = $this->Account_model->get_userdata_from_room_id($room_no);
        $data['chat'] = $this->Account_model->get_chat($room_no);


        $data['room_no'] = $room_no;

        $this->load->view('admin/admin_header');
        $this->load->view('admin/chat', $data);
        $this->load->view('admin/admin_footer');
    }

    public function send_room_message() {
        
        if (!empty($_POST)) {
            $_POST['created'] = date("Y-m-d H:i:s");
            $success = $this->Welcome_model->insert_data("room_chat", $_POST);

            echo $success;
            exit;
        }
    }

    public function edit_qa($qa_id) {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $customer = array();
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['email'] = $_POST['email'];
            $customer['phone'] = $_POST['phone'];
            if ($_POST['password'] != "") {
                if ($_POST['password'] == $_POST['confirm_password']) {
                    $customer['new_password'] = md5($_POST['password']);
                    $success = $this->Admin_model->update_data('users', $customer, array('id' => $qa_id));
                    if ($success) {
                        $this->session->set_flashdata('message_success', 'QA profile edit successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_qa");
                    } else {
                        $this->session->set_flashdata('message_error', 'QA profile not edit successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_qa");
                    }
                } else {
                    $this->session->set_flashdata('message_error', 'Password dose not match', 5);
                    redirect(base_url() . "admin/dashboard/view_qa");
                }
            } else {
                $success = $this->Admin_model->update_data('users', $customer, array('id' => $qa_id));
                if ($success) {
                    $this->session->set_flashdata('message_success', 'QA profile edit successfully.!', 5);
                    redirect(base_url() . "admin/dashboard/view_qa");
                } else {
                    $this->session->set_flashdata('message_error', 'QA profile not edit successfully.!', 5);
                    redirect(base_url() . "admin/dashboard/view_qa");
                }
            }
        }
    }

    public function edit_va($qa_id) {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $customer = array();
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['email'] = $_POST['email'];
            $customer['phone'] = $_POST['phone'];
            if ($_POST['password'] != "") {
                if ($_POST['password'] == $_POST['confirm_password']) {
                    $customer['new_password'] = md5($_POST['password']);
                    $success = $this->Admin_model->update_data('users', $customer, array('id' => $qa_id));
                    if ($success) {
                        $this->session->set_flashdata('message_success', 'QA profile edit successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_qa");
                    } else {
                        $this->session->set_flashdata('message_error', 'QA profile not edit successfully.!', 5);
                        redirect(base_url() . "admin/dashboard/view_qa");
                    }
                } else {
                    $this->session->set_flashdata('message_error', 'Password dose not match', 5);
                    redirect(base_url() . "admin/dashboard/view_qa");
                }
            } else {
                $success = $this->Admin_model->update_data('users', $customer, array('id' => $qa_id));
                if ($success) {
                    $this->session->set_flashdata('message_success', 'QA profile edit successfully.!', 5);
                    redirect(base_url() . "admin/dashboard/view_qa");
                } else {
                    $this->session->set_flashdata('message_error', 'QA profile not edit successfully.!', 5);
                    redirect(base_url() . "admin/dashboard/view_qa");
                }
            }
        }
    }

    public function get_customer_info_frm_customer_id($plan_id) {
        $this->myfunctions->checkloginuser("admin");
        $cust_info = $this->Stripe->getcustomerInfoUsingCustomerId($plan_id);
        return $card = $cust_info->sources->data;
    }

    public function get_customer_detail_from_strip($plan_id) {
        $this->myfunctions->checkloginuser("admin");
        $card_detail = $this->Stripe->getcustomerdetail($plan_id);
        return $card = $card_detail->sources->data;
    }

    public function getdetailplan($plan_id) {
        $this->myfunctions->checkloginuser("admin");
        $card_detail = $this->Stripe->getcurrentplan($plan_id);
        return $card_detail->items->data[0]->plan;
    }

    public function edit_designer_about_info($id) {
        $this->myfunctions->checkloginuser("admin");
        $array = array();
        $hobbies = isset($_POST['hobbies']) ? $_POST['hobbies'] : '';
        $about = isset($_POST['about']) ? $_POST['about'] : '';
        if (!empty($_POST)) {
            $array['hobbies'] = $hobbies;
            $array['about'] = $about;
            $getskills = $this->Request_model->getallskills($id);
            if (empty($getskills)) {
                $success = $this->Welcome_model->insert_data("user_skills", $array);
            } else {
                $success = $this->Admin_model->update_data('user_skills', $array, array("user_id" => $id));
            }
            if ($success) {
                $this->session->set_flashdata('message_success', 'About info updated successfully', 5);
            }
            redirect(base_url() . "admin/accounts/view_designer_profile/" . $id);
        }
    }

    public function cancel_subscription() {
        $role = array();
        $return = array();
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        $role['is_cancel_subscription'] = 1;
        $subscription_id = isset($_POST['subscription_id']) ? $_POST['subscription_id'] : '';
        if ($subscription_id) {
            $result = $this->Stripe->cancel_subscription($subscription_id);
            if ($result['status'] == 1) {
                $success = $this->Admin_model->update_data("users", $role, array("id" => $user_id));
                if ($success) {
                    $updated_data = $this->myfunctions->IsSubscriptionCancel($user_id, 'from_admin_cancel');
                    $return['status'] = 1;
                }
            } else {
                $return['status'] = 0;
                $return['msg'] = $result['msg'];
            }
        } else {
            $return['status'] = 0;
            $return['msg'] = "Subscription is not cancelled";
        }
        echo json_encode($return);
    }

    public function cancel_subscription_from_stripe() {
        $subscription_id = isset($_POST['subscription_id']) ? $_POST['subscription_id'] : '';
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        if ($subscription_id) {
            $result = $this->Stripe->cancel_subscription($subscription_id);
            if ($result) {
                $this->session->set_flashdata('message_success', 'Subscription is cancelled from stripe successfully!');
                redirect(base_url() . "admin/accounts/view_client_profile/" . $user_id);
            } else {
                $this->session->set_flashdata('message_error', 'Subscription is not cancelled from stripe!');
                redirect(base_url() . "admin/accounts/view_client_profile/" . $user_id);
            }
        } else {
            $this->session->set_flashdata('message_error', 'Subscription is not cancelled from stripe!');
            redirect(base_url() . "admin/accounts/view_client_profile/" . $user_id);
        }
    }

    public function switchtouseraccount($id) {
        $this->myfunctions->checkloginuser("admin");
        $user = $this->Admin_model->getuser_data($id);
        if ($_SESSION['switchtouseracc'] != 1) {
            $this->session->set_userdata('switchtouseracc', 1);
            $this->session->set_userdata('switch_userid', $id);
            $this->session->set_userdata('switchfrom_id', $_SESSION['user_id']);

            if ($_SESSION['switchtouseracc'] == 1 && $_SESSION['switch_userid'] == $id) {
                $this->session->set_userdata('email', $user[0]['email']);
                $this->session->set_userdata('timezone', $user[0]['timezone']);
                $this->session->set_userdata('role', $user[0]['role']);
                $this->session->set_userdata('first_name', $user[0]['first_name']);
                $this->session->set_userdata('last_name', $user[0]['last_name']);
                $this->session->set_userdata('user_id', $user[0]['id']);
                $this->session->set_userdata('tour', $user[0]['tour']);
                $this->session->set_userdata('qa_id', $user[0]['qa_id']);
                redirect(base_url() . 'customer/request/design_request');
            }
        } else {
            redirect(base_url() . 'admin/dashboard');
        }
        //  $this->myfunctions->checkloginuser("customer");
    }

    public function backtoownaccount($id,$from) {
        $userid = $_SESSION["user_id"];
        $clientdata = $this->Admin_model->getuser_data($userid,"parent_id,user_flag");
        $user = $this->Admin_model->getuser_data($id);
        $bkuserid = $_SESSION['switch_userid'];
        $this->session->set_userdata('email', $user[0]['email']);
        $this->session->set_userdata('timezone', $user[0]['timezone']);
        $this->session->set_userdata('role', $user[0]['role']);
        $this->session->set_userdata('first_name', $user[0]['first_name']);
        $this->session->set_userdata('last_name', $user[0]['last_name']);
        $this->session->set_userdata('user_id', $user[0]['id']);
        $this->session->set_userdata('tour', $user[0]['tour']);
        $this->session->set_userdata('qa_id', $user[0]['qa_id']);
        $this->session->set_userdata('switchtouseracc', 0);
        $this->session->set_userdata('switch_userid', '');
        $this->session->set_userdata('switchfrom_id', '');
        if($from == 'admin'){
          redirect(base_url() . 'admin/dashboard');
        }else{
            if ($clientdata[0]["user_flag"] == "client") {
                redirect(base_url() . 'admin/accounts/view_client_profile/' . $clientdata[0]["parent_id"] . "/#subUser");
            } else {
                redirect(base_url() . 'admin/accounts/view_client_profile/' . $bkuserid);
            }
        }
    }
    
    public function exituseraccess(){
         $this->load->view('admin/exitaccess');
     }
     
     public function show_hide_user_managemnt() {
        $this->myfunctions->checkloginuser("admin");
        $cust_status = $_POST['status'];
        $users_size = isset($_POST["usersize"])?$_POST["usersize"]:""; 
        $userid = $_POST['data_id'];
        $setting = $_POST['setting'];
        $user_data = $this->Admin_model->getuser_data($userid);
        $globaluserinfo = $this->Request_model->getsubscriptionlistbyplanid($user_data[0]['plan_name']);
        if($setting == 'custom'){
          //  $subusers_manager = $this->Request_model->getAllsubUsers($userid, "manager", "active");
            if ($cust_status == 'true') {
                $user_setting = array("show_usr_mngmnt" => 1,
                    "sub_user_count" => $users_size);
            } else if ($cust_status == 'false') {
                $user_setting = array("show_usr_mngmnt" => 2);
//                    if(!empty($subusers_manager)){
//                        foreach ($subusers_manager as $subusers_manager_d) {
//                            $update_user =  $this->Welcome_model->update_data("users", array("is_active" => 0), array("id" => $subusers_manager_d['id']));
//                        }
//                    }
            }
        }else{
            if($cust_status == 'false'){
                $user_setting = array("show_usr_mngmnt" => 1);
            }else{
                $user_setting = array("show_usr_mngmnt" => 0);
            }
        }
             $update_user =  $this->Welcome_model->update_data("users", $user_setting, array("id" => $userid));
        if($update_user){
         echo 1;
        }
    }
    
    public function add_usernotes() {
        $notes_data = array(
        "user_id" => isset($_POST["user_id"])?$_POST["user_id"]:"",
        "sender_id" => isset($_POST["sender_id"])?$_POST["sender_id"]:"",
        "message" => isset($_POST["notes"])?$_POST["notes"]:"",
        "created" => date('Y-m-d H:i:s'));
        //echo "<pre>";print_r($notes_data);
        $id = $this->Welcome_model->insert_data("user_notes", $notes_data);
        if($id){
            echo "success";
        }
    }
    public function onlytimezone($date_zone) {
        $nextdate = $this->myfunctions->onlytimezoneforall($date_zone);
        return $nextdate;
    }
    public function get_usernotes() {
        $output = array();
        $user_id = isset($_POST["user_id"])?$_POST["user_id"]:"";
        $notes = $this->Admin_model->get_users_notes($user_id);
        $i=0;
        foreach ($notes as $notesdate) {
            $notes[$i]['created'] = $this->onlytimezone($notesdate['created']);
            $i++;
        }
        if(!empty($notes)){
            $output['notes'] = $notes;
        }else{
            $output['notes'] = 0;
        }
        echo json_encode($output);
    }
    
//    public function addsubuseronuserlevel() {
//       $users_id = isset($_POST["settinguser_id"])?$_POST["settinguser_id"]:""; 
//       $users_size = isset($_POST["users_size"])?$_POST["users_size"]:""; 
//       $update_user =  $this->Welcome_model->update_data("users", array("sub_user_count" => $users_size), array("id" => $users_id));
//       if($update_user){
//           $this->session->set_flashdata('message_success', "User management setting updated successsfully..!", 5);
//           redirect(base_url() . "admin/accounts/view_client_profile/" . $users_id);
//       }
//    }

}
