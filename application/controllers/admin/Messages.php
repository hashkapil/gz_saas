<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messages extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('Myfunctions');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Welcome_model');
        $this->load->model('Request_model');
        $this->load->model('Stripe');
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }
    
//    public function checkloginuser() {
//        if (!$this->session->userdata('user_id')) {
//            redirect(base_url());
//        }
//        if ($this->session->userdata('role') != "admin") {
//            redirect(base_url());
//        }
//    }
    
    public function load_more_messages(){
        //$this->myfunctions->checkloginuser("admin");
        $seen = isset($_POST['data_seen'])?$_POST['data_seen']:'unread';
        if($seen == "unread"){
            $msgflag = 0;
        }elseif($seen == "read"){
            $msgflag = 1;
        }elseif($seen == "is_star"){
            $msgflag = 2;
        }
        $data_count = isset($_POST['data_count'])?$_POST['data_count']:'';
        if($data_count != ''){
            $per_page = 150;
            $start = ($data_count * $per_page);
            //$limit = ($data_count +1) * $per_page;
            $get_requests_chat_msg = $this->Request_model->get_all_latest_message($start,$per_page,'',$msgflag);
            foreach ($get_requests_chat_msg as $element) {
                $hash = $element['request_id'];
                $unique_array[$hash] = $element;
                $unique_array[$hash]['req_last_update'] = $this->myfunctions->check_timezone($element['req_last_update'],$element['user_current_plan']);
                $unique_array[$hash]['req_approvaldate'] = $this->onlytimezone($element['req_approvaldate']);
                $unique_array[$hash]['req_modified'] = $this->onlytimezone($element['req_modified']);
                $unique_array[$hash]['user_profile_picture'] = $this->customfunctions->getprofileimageurl($element['user_profile_picture']);
            }
            $this->load->view('admin/messages_list_ajax', array('project_list' =>$unique_array));
        }
        
    }
        public function message_list($seen="unread"){
        if($seen == "unread"){
            $msgflag = 0;
        }elseif($seen == "read"){
            $msgflag = 1;
        }elseif($seen == "is_star"){
            $msgflag = 2;
        }
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
            "Messages " => base_url().'admin/messages/message_list');
        $login_user_id = $this->load->get_var('login_user_id');
        $ajax_type = isset($_POST['type'])?$_POST['type']:"1";
        $get_requests_chat_msg = $this->Request_model->get_all_latest_message(0,150,'',$msgflag);
        $count_requests_unreadchat_msg = $this->Request_model->count_all_unseen_latest_message();
        $count_star_msg = $this->Request_model->count_all_star_message();
       foreach ($get_requests_chat_msg as $element) {
           
           
            $hash = $element['request_id'];
            $unique_array[$hash] = $element;
            $unique_array[$hash]['req_last_update'] = $this->myfunctions->check_timezone($element['req_last_update'],$element['user_current_plan']);
            $unique_array[$hash]['req_approvaldate'] = $this->onlytimezone($element['req_approvaldate']);
            $unique_array[$hash]['req_modified'] = $this->onlytimezone($element['req_modified']);
            $unique_array[$hash]['user_profile_picture'] = $this->customfunctions->getprofileimageurl($element['user_profile_picture']);
        }
       $chat_request = $this->Request_model->get_chat_request_by_id($get_requests_chat_msg[0]['request_id']);
       $count_mainchat_unread_msg = $this->Request_model->get_main_messsage_chat($get_requests_chat_msg[0]['request_id'], "admin");
       $data = $this->Request_model->get_request_by_id_admin($get_requests_chat_msg[0]['request_id']);
       $request_files = $this->Admin_model->get_requested_files_admin($get_requests_chat_msg[0]['request_id']);
       for ($j = 0; $j < count($chat_request); $j++) {
            $chat_request[$j]['sender_data'] = $this->Admin_model->getuser_data($chat_request[$j]['sender_id']);
            $chat_request[$j]['msg_created'] = $this->onlytimezone($chat_request[$j]['created']);
            $chat_request[$j]['chat_created_date'] = $chat_request[$j]['msg_created'];
        }
        for ($i = 0; $i < count($data[0]['designer_attachment']); $i++) {        
            $data[0]['designer_attachment'][$i]['img_created'] = $this->onlytimezone($data1[0]['designer_attachment'][$i]['created']);
            $data[0]['designer_attachment'][$i]['created'] =   date('d M, Y', strtotime($data1[0]['designer_attachment'][$i]['img_created']));
        }
        $files = $this->Request_model->get_attachment_files($get_requests_chat_msg[0]['request_id'], "designer","1");
        $file_chat_array = array();
        for ($i = 0; $i < sizeof($files); $i++) {
            $chat_of_file = $this->Request_model->get_file_chat($files[$i]['id'], "admin");
            if ($chat_of_file) {
                $file_chat_array[$i]['file_name'] = $files[$i]['file_name'];
                $file_chat_array[$i]['count'] = $chat_of_file;
                $file_chat_array[$i]['id'] = $files[$i]['id'];
                $files[$i]['chat_count'] = $chat_of_file;
            } else {
                $files[$i]['chat_count'] = "";
                $file_chat_array[$i]['id'] = "";
            }
        }
        $profile_data = $this->Admin_model->getuser_data($login_user_id); 
//        if($count_mainchat_unread_msg <= 0){
//            redirect(base_url() . 'admin/messages/message_list/read');
//        }
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/messages', array('project_list' =>$unique_array,"count_mainchat_unread_msg" => $count_mainchat_unread_msg, "draft_array" => $files, 'count_star_msg' => $count_star_msg,'count_unreadchat_msg' => $count_requests_unreadchat_msg,"chat_request" => $chat_request,"profile" => $profile_data,"data" => $data,"request_files" => $request_files,"file_chat_array" => $file_chat_array,'ajax_type'=> $ajax_type,'firstrequest_id' => $get_requests_chat_msg[0]['request_id'],"seen" => $seen));
        $this->load->view('admin/admin_footer');
    }
    
    public function message_chat(){
       $login_user_id = $this->load->get_var('login_user_id');
       $project_id = isset($_POST['project_id'])?$_POST['project_id']:"";
       $ajax_type = isset($_POST['type'])?$_POST['type']:"1";
       $chat_request = $this->Request_model->get_chat_request_by_id($project_id);
       $count_mainchat_unread_msg = $this->Request_model->get_main_messsage_chat($project_id, "admin");
       $data = $this->Request_model->get_request_by_id_admin($project_id);
       $request_files = $this->Admin_model->get_requested_files_admin($project_id);
       for ($j = 0; $j < count($chat_request); $j++) {
            $chat_request[$j]['sender_data'] = $this->Admin_model->getuser_data($chat_request[$j]['sender_id']);
            $chat_request[$j]['msg_created'] = $this->onlytimezone($chat_request[$j]['created']);
            $chat_request[$j]['chat_created_date'] = $chat_request[$j]['msg_created'];
        }
        for ($i = 0; $i < count($data[0]['designer_attachment']); $i++) {        
        $data[0]['designer_attachment'][$i]['img_created'] = $this->onlytimezone($data1[0]['designer_attachment'][$i]['created']);
        $data[0]['designer_attachment'][$i]['created'] =   date('d M, Y', strtotime($data1[0]['designer_attachment'][$i]['img_created']));
        }
        $files = $this->Request_model->get_attachment_files($project_id, "designer","1");
        $file_chat_array = array();
        for ($i = 0; $i < sizeof($files); $i++) {
            $chat_of_file = $this->Request_model->get_file_chat($files[$i]['id'], "admin");
            if ($chat_of_file) {
                $file_chat_array[$i]['file_name'] = $files[$i]['file_name'];
                $file_chat_array[$i]['count'] = $chat_of_file;
                $file_chat_array[$i]['id'] = $files[$i]['id'];
                $files[$i]['chat_count'] = $chat_of_file;
            } else {
                $files[$i]['chat_count'] = "";
                $file_chat_array[$i]['id'] = "";
            }
        }
        $profile_data = $this->Admin_model->getuser_data($login_user_id);
        $this->load->view('admin/message_chat',array("chat_request" => $chat_request,"draft_array" => $files,"count_mainchat_unread_msg" => $count_mainchat_unread_msg, "profile" => $profile_data,"data" => $data,"request_files" => $request_files,"file_chat_array" => $file_chat_array,"ajax_type" => $ajax_type)); 
        
        
        }
    
    public function project_comment_chat(){
        //$this->myfunctions->checkloginuser("admin");
        $login_user_id = $this->load->get_var('login_user_id');
        $project_id = isset($_POST['project_id'])?$_POST['project_id']:"";
        $image_of_file = $this->Request_model->get_image_by_request_file_id($project_id);
        $comment_chat_of_files = $this->Request_model->get_commentfile_chat($project_id, "admin");
        $req_data = $this->Request_model->get_request_by_id_admin($image_of_file[0]['request_id']);
        for ($j=0; $j < sizeof($comment_chat_of_files); $j++) {
            $comment_chat_of_files[$j]['created'] = $this->onlytimezone($comment_chat_of_files[$j]['created']);
        }
       // echo "<pre>";print_r($image_of_file);
        $profile_data = $this->Admin_model->getuser_data($login_user_id);
       // $this->Welcome_model->update_data("request_file_chat", array("admin_seen" => 1), array('request_file_id' => $project_id));
        $this->load->view('admin/comment_chat',array("image_of_file" => $image_of_file,"req_data" => $req_data,"comment_chat_of_files" => $comment_chat_of_files,"user" => $profile_data)); 
        
    }
    
    public function onlytimezone($date_zone) {
        $nextdate = $this->myfunctions->onlytimezoneforall($date_zone);  
        return $nextdate;
    }
    public function mark_read_unread() {
        $project_id = isset($_POST['project_id'])?$_POST['project_id']:'';
        $status = isset($_POST['status'])?$_POST['status']:'';
        $active_project = isset($_POST['active_project'])?$_POST['active_project']:'';
        $active_chat = isset($_POST['active_chat'])?$_POST['active_chat']:'';
        $data = $this->Request_model->get_request_by_id_admin($project_id);
        for ($i = 0; $i < count($data[0]['designer_attachment']); $i++) {        
        $ap = $data[0]['designer_attachment'][$i]['status'];
         if (($ap != "pending") && ($ap != "Reject")) {
             $request_files_id[] = $data[0]['designer_attachment'][$i]['id'];
         }
        }
        if($status == 'mark_read'){
             $mark_read = $this->Request_model->mark_read_main_chat_msg($project_id,array("admin_seen"=>1,"designer_seen"=>1,"qa_seen"=>1));
            if($request_files_id != ''){
               $mark_read_comment = $this->Request_model->mark_read_comment_chat_msg($request_files_id,array("admin_seen"=>1,"designer_seen"=>1,"qa_seen"=>1)); 
            }
        }
        elseif($status == 'mark_unread'){
            if($active_chat != '' && $active_chat == "main_chat"){
            $mark_read = $this->Request_model->mark_read_main_chat_msg($project_id,array("admin_seen"=> 0));
            }elseif($active_chat != '' && $active_chat == "comment_chat"){
              $mark_read = $this->Request_model->mark_read_comment_chat_msg(array($active_project),array("admin_seen"=>0));  
            }
        }elseif($status == 'mark_star'){
           $mark_read = $this->Admin_model->update_data("requests", array("is_star" => 1), array("id" => $project_id)); 
        }elseif($status == 'remove_star'){
            $mark_read = $this->Admin_model->update_data("requests", array("is_star" => 0), array("id" => $project_id));
        }
        echo $mark_read;
    }
    
}
    ?> 