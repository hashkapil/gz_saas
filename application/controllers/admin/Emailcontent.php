<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailcontent extends CI_Controller {
    
    public function __construct(){
	  parent::__construct();
		$this->load->library('javascript');
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('Admin_model');
		$this->load->model('Welcome_model');
		$this->load->model('Request_model');
		$this->load->model('Stripe');
                $this->load->library('Myfunctions');
		
	}
        
     public function editemail(){
         $this->myfunctions->checkloginuser("admin");
         $id = isset($_POST['id'])?$_POST['id']:'';
         $selecteddata = $this->Admin_model->get_emailtemplate_byID($id);
        if(isset($_POST['submit'])){
            $_POST['created'] = date("Y-m-d H:i:s");
                $updatearray = array('email_name' => $_POST['email_name'],'email_subject' => $_POST['subject'],'email_text' => $_POST['description']);

                if($this->Admin_model->update_data('email_template',$updatearray,array("id" => $id)))
                {
                    $this->session->set_flashdata('message_success', "Update Successfully", 5); 
                    redirect(base_url()."admin/contentmanagement/email");
                }else
                {
                    $this->session->set_flashdata('message_error', "Update Not Successfully", 5);
                    redirect( base_url()."admin/contentmanagement/email");
                }
        }
        echo json_encode($selecteddata);
     }
     
      public function delete_file_req() {
        $this->myfunctions->checkloginuser("admin");
        $reqid = isset($_POST['request_id']) ? $_POST['request_id'] : '';
        $filename = isset($_POST['filename']) ? $_POST['filename'] : '';
        $updatearray = array('email_img' => '');
        $success = $this->Admin_model->update_data('email_template',$updatearray,array("id" => $reqid));
        $this->session->set_flashdata('message_success', "File is Deleted Successfully.!", 5);
    }
}