<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('Myfunctions');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Welcome_model');
        $this->load->model('Request_model');
        $this->load->model('Category_model');
        $this->load->model('Stripe');
    }
	
	public function checkloginuser() {
        if (!$this->session->userdata('user_id')) {
            redirect(base_url());
        }
        if ($this->session->userdata('role') != "admin") {
            redirect(base_url());
        }
    }
	
	public function cat_listing(){
        $data = array();
        $this->myfunctions->checkloginuser("admin");
        $breadcrumbs = array("Dashboard &nbsp; >" => base_url().'admin/dashboard',
        "Content Management &nbsp; >" => base_url().'admin/Contentmanagement/index',
        "Category Listing" => base_url().'admin/categories/cat_listing',
        );
        $login_user_id = $_SESSION['user_id'];
        $data['cat_data'] = $this->Category_model->get_categories(0);
        //$data['buckets'] = $this->Category_model->get_buckets();
        $profile_data = $this->Admin_model->getuser_data($login_user_id);
//        $notifications = $this->Request_model->get_notifications($login_user_id);
//        $notification_number = $this->Request_model->get_notifications_number($login_user_id);
//        $messagenotifications = $this->Request_model->get_messagenotifications($login_user_id);
//        $messagenotification_number = $this->Request_model->get_messagenotifications_number($login_user_id);
        $this->load->view('admin/admin_header_1', array('breadcrumbs' => $breadcrumbs));
        $this->load->view('admin/categories/cat_listing', $data);
        $this->load->view('admin/admin_footer',array('cat_data' => $data['cat_data']));
	}
        
        public function add_category(){
             $CI = & get_instance();
            $this->myfunctions->checkloginuser("admin");
//            echo "<pre/>";print_R($_POST);exit;
            if(isset($_POST['save'])){
               $data['parent_id'] = isset($_POST['parent_cat'])? $_POST['parent_cat']:'';
               $data['name'] = isset($_POST['cat_name'])? $_POST['cat_name']:'';
               $data['position'] = isset($_POST['position'])? $_POST['position']:'';
               $data['bucket_type'] = isset($_POST['bucket_type'])? $_POST['bucket_type']:'';
               $data['timeline'] = isset($_POST['timeline'])? $_POST['timeline']:'';
               $data['created'] = date("Y-m-d H:i:s");
               if($_POST['active'] == 'on'){
                   $data['is_active'] = 1;
               }else{
                   $data['is_active'] = 0;
               }
            if ($_FILES['cat_image']['name'] != "") {
                $file = pathinfo($_FILES['cat_image']['name']);
                $s3_file = $file['filename'] . '-' . rand(1000, 1) . '.' . $file['extension'];
                $config = array(
                    'upload_path' => FS_UPLOAD_PUBLIC.'assets/img/customer/customer-images/',
                    'allowed_types' => array("jpg", "jpeg", "png"),
                    'file_name' => $s3_file
                );
                $CI->load->library('upload');
                $CI->upload->initialize($config);
                if ($CI->upload->do_upload('cat_image')) {
                    $output['status'] = true;
                    $output['msg'] = 'uploaded file successfully';
                } else {
                    $output['status'] = false;
                    $output['msg'] = $CI->upload->display_errors();
                }
                $data['image_url'] = $s3_file;
            }
            $id = $this->Welcome_model->insert_data("request_categories", $data);
            if($id){
                $this->session->set_flashdata('message_success', 'Request category added Successfully.!');
                 redirect(base_url() . "admin/categories/cat_listing/");
            }else{
                $this->session->set_flashdata('message_error', 'Request category Not Added!');
                 redirect(base_url() . "admin/categories/cat_listing/");
            }

        }
}

        public function edit_detailcat(){
            $this->myfunctions->checkloginuser("admin");
            $category_id = isset($_POST['category_id'])?$_POST['category_id']:'';
            $cat_data = $this->Category_model->get_category_byID($category_id);
            echo json_encode($cat_data);
        }
        
        public function update_category(){ 
//            echo "<pre/>";print_R($_POST);exit;
            $CI = & get_instance();
            $this->myfunctions->checkloginuser("admin");
            $catid = isset($_POST['cat_id'])?$_POST['cat_id']:'';
            $getAllcat = $this->Category_model->get_category_byID($catid);
//            echo "subcat_id".$catid."<br/>";
//            echo "parent_id".$_POST['parent_cat']."<br/>";
//            exit;
            if($getAllcat[0]['parent_id'] != 0){
                if($getAllcat[0]['bucket_type'] != $_POST['bucket_type']){
                    $allReq = $this->Category_model->getAllrequestsBysubcatID($catid);
//                    echo "<pre/>";print_R($allReq);exit;
                    foreach($allReq as $kk => $vv){
                        if($vv['latest_update'] == '' || $vv['latest_update'] == NULL){
                            $latest_update = date("Y-m-d H:i:s");
                        }else{
                            $latest_update = $vv['latest_update'];
                        }
                    $expctddate = $this->myfunctions->getexpected_datefromtimezone($latest_update,$vv['plan_name'],$_POST['bucket_type'],$vv['timeline']);
                    $success = $this->Welcome_model->update_data("requests", array("expected_date" => $expctddate,"category_bucket" => $_POST['bucket_type']),array("id"=> $vv['request_id']));
                    }
                }
            }
            $parentID = ($catid !== $_POST['parent_cat']) ? $_POST['parent_cat'] : '0';
            $data['parent_id'] = $parentID;
            $data['name'] = isset($_POST['cat_name'])? $_POST['cat_name']:'';
            $data['position'] = isset($_POST['position'])? $_POST['position']:'';
            $data['bucket_type'] = isset($_POST['bucket_type'])? $_POST['bucket_type']:'';
            $data['timeline'] = isset($_POST['timeline'])? $_POST['timeline']:'';
            $data['created'] = date("Y-m-d H:i:s");
            if($_POST['active'] == 'on'){
                $data['is_active'] = 1;
            }else{
                $data['is_active'] = 0;
            }
            if ($_FILES['cat_image']['name'] != "") {
                $file = pathinfo($_FILES['cat_image']['name']);
                $s3_file = $file['filename'] . '-' . rand(1000, 1) . '.' . $file['extension'];
                $config = array(
                    'upload_path' => FS_UPLOAD_PUBLIC.'assets/img/customer/customer-images/',
                    'allowed_types' => array("jpg", "jpeg", "png"),
                    'file_name' => $s3_file
                );
                $CI->load->library('upload');
                $CI->upload->initialize($config);
                if ($CI->upload->do_upload('cat_image')) {
                    $output['status'] = true;
                    $output['msg'] = 'uploaded file successfully';
                } else {
                    $output['status'] = false;
                    $output['msg'] = $CI->upload->display_errors();
                }
                $data['image_url'] = $s3_file;
            }
            
            $success = $this->Welcome_model->update_data("request_categories",$data, array("id" => $catid));
            if($success){
                $this->session->set_flashdata('message_success', 'Request category updated Successfully.!');
                redirect(base_url() . "admin/categories/cat_listing/");
            }else{
                $this->session->set_flashdata('message_error', 'Request category Not updated!');
                redirect(base_url() . "admin/categories/cat_listing/");
            }
            
        }


        public function delete_category(){
            $this->myfunctions->checkloginuser("admin");
            $category_id = isset($_POST['category_id'])?$_POST['category_id']:'';
            $delete = $this->Admin_model->delete_data('request_categories', $category_id);
            if($delete){
                $this->session->set_flashdata('message_success', 'Request category deleted Successfully.!');
            }else{
            $this->session->set_flashdata('message_error', 'Request category Not Deleted!');
            }
           redirect(base_url() . "admin/categories/cat_listing/");
        }
}
