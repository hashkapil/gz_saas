<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('Myfunctions');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->model('Welcome_model');
        $this->load->model('Account_model');
        $this->load->model('Request_model');
        $this->load->model('Stripe');
    }

    public function checkloginuser() {
        if (!$this->session->userdata('user_id')) {
            redirect(base_url());
        }
        if ($this->session->userdata('role') != "admin") {
            redirect(base_url());
        }
    }

    public function index() {
        $this->myfunctions->checkloginuser("admin");
        // $today_customer = $this->Admin_model->get_today_customer();
        // $all_customer = $this->Admin_model->get_total_customer();
        // $today_requests = $this->Admin_model->get_today_request();
        // $total_requests = $this->Admin_model->get_total_request();
        $active_project = $this->Request_model->getall_request("", "'active','disapprove'");
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($active_project[$i]['id'], $active_project[$i]['designer_id']);

            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
        }
        $data['active_project'] = $active_project;

        $check_approve_project = $this->Request_model->getall_request('', "'checkforapprove'");

        for ($i = 0; $i < sizeof($check_approve_project); $i++) {
            $check_approve_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($check_approve_project[$i]['id'], $check_approve_project[$i]['designer_id']);

            $check_approve_project[$i]['total_chat'] = $this->Request_model->get_chat_number($check_approve_project[$i]['id'], $check_approve_project[$i]['customer_id'], $check_approve_project[$i]['designer_id'], "customer");
        }
        $data['check_approve_project'] = $check_approve_project;

        $disapprove_project = $this->Request_model->getall_request("", "'disapprove'");

        for ($i = 0; $i < sizeof($disapprove_project); $i++) {
            $disapprove_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($disapprove_project[$i]['id'], $disapprove_project[$i]['designer_id']);
            $disapprove_project[$i]['total_chat'] = $this->Request_model->get_chat_number($disapprove_project[$i]['id'], $disapprove_project[$i]['customer_id'], $disapprove_project[$i]['designer_id'], "customer");
        }
        $data['disapprove_project'] = $disapprove_project;

        $pending = $this->Request_model->getall_request('', "'pending','assign'", "", "index_number");

        for ($i = 0; $i < sizeof($pending); $i++) {
            $pending[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($pending[$i]['id'], $pending[$i]['designer_id']);

            $pending[$i]['total_chat'] = $this->Request_model->get_chat_number($pending[$i]['id'], $pending[$i]['customer_id'], $pending[$i]['designer_id'], "customer");
        }
        $data['pending'] = $pending;


        $completed = $this->Request_model->getall_request('', "'approved'");
        for ($i = 0; $i < sizeof($completed); $i++) {
            $completed[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($completed[$i]['id'], $completed[$i]['designer_id']);

            $completed[$i]['total_chat'] = $this->Request_model->get_chat_number($completed[$i]['id'], $completed[$i]['customer_id'], $completed[$i]['designer_id'], "customer");
        }
        $data['completed'] = $completed;

        $data['heading'] = "incoming";
        $this->load->view('admin/admin_header');
        $this->load->view('admin/index', $data);
        // $this->load->view('admin/index', array('today_customer' => $today_customer, 'all_customer' => $all_customer, 'today_requests' => $today_requests, 'total_requests' => $total_requests));
        $this->load->view('admin/admin_footer');
    }

    public function client_projects($id = null) {
        $this->myfunctions->checkloginuser("admin");
        if ($id == "") {
            redirect(base_url() . "admin/accounts/view_clients");
        }
        $data['userdata'] = $this->Account_model->getuserbyid($id);

        if (empty($data['userdata'])) {
            redirect(base_url() . "admin/accounts/view_clients");
        }

        $data['incoming_request'] = $this->Request_model->getall_request($id, "'pending','assign'");

        $data['ongoing_request'] = $this->Request_model->getall_request($id, "'active','pendingforapprove','checkforapprove'");
        $data['pending_request'] = $this->Request_model->getall_request($id, "'pending','assign'");
        $data['approved_request'] = $this->Request_model->getall_request($id, "'approved'");

        for ($i = 0; $i < sizeof($data['incoming_request']); $i++) {
            $data['incoming_request'][$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($data['incoming_request'][$i]['id'], $data['incoming_request'][$i]['designer_id']);
        }
        for ($i = 0; $i < sizeof($data['ongoing_request']); $i++) {
            $data['ongoing_request'][$i]['rate'] = $this->Request_model->get_average_rating_for_request($data['ongoing_request'][$i]['id'], $data['ongoing_request'][$i]['designer_id']);
        }
        for ($i = 0; $i < sizeof($data['pending_request']); $i++) {
            $data['pending_request'][$i]['rating_star'] = $this->Request_model->get_average_rating_for_request($data['pending_request'][$i]['id'], $data['pending_request'][$i]['designer_id']);
        }
        for ($i = 0; $i < sizeof($data['approved_request']); $i++) {
            $data['approved_request'][$i]['all_rating_star'] = $this->Request_model->get_average_rating_for_request($data['approved_request'][$i]['id'], $data['approved_request'][$i]['designer_id']);
        }
        $this->load->view('admin/admin_header');
        $this->load->view('admin/request/client_projects', $data);
        $this->load->view('admin/admin_footer');
    }

    public function incoming_request() {
        $this->myfunctions->checkloginuser("admin");

        $active_project = $this->Request_model->getall_request("", "'active','disapprove'");
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($active_project[$i]['id'], $active_project[$i]['designer_id']);

            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
        }
        $data['active_project'] = $active_project;

        $check_approve_project = $this->Request_model->getall_request('', "'checkforapprove'");

        for ($i = 0; $i < sizeof($check_approve_project); $i++) {
            $check_approve_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($check_approve_project[$i]['id'], $check_approve_project[$i]['designer_id']);

            $check_approve_project[$i]['total_chat'] = $this->Request_model->get_chat_number($check_approve_project[$i]['id'], $check_approve_project[$i]['customer_id'], $check_approve_project[$i]['designer_id'], "customer");
        }
        $data['check_approve_project'] = $check_approve_project;

        $disapprove_project = $this->Request_model->getall_request("", "'disapprove'");

        for ($i = 0; $i < sizeof($disapprove_project); $i++) {
            $disapprove_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($disapprove_project[$i]['id'], $disapprove_project[$i]['designer_id']);
            $disapprove_project[$i]['total_chat'] = $this->Request_model->get_chat_number($disapprove_project[$i]['id'], $disapprove_project[$i]['customer_id'], $disapprove_project[$i]['designer_id'], "customer");
        }
        $data['disapprove_project'] = $disapprove_project;

        $pending_project = $this->Request_model->getall_request('', "'pending','assign'", "", "index_number");

        for ($i = 0; $i < sizeof($pending_project); $i++) {
            $pending_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($pending_project[$i]['id'], $pending_project[$i]['designer_id']);

            $pending_project[$i]['total_chat'] = $this->Request_model->get_chat_number($pending_project[$i]['id'], $pending_project[$i]['customer_id'], $pending_project[$i]['designer_id'], "customer");
        }
        $data['pending_project'] = $pending_project;


        $complete_project = $this->Request_model->getall_request('', "'approved'");
        for ($i = 0; $i < sizeof($complete_project); $i++) {
            $complete_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($complete_project[$i]['id'], $complete_project[$i]['designer_id']);

            $complete_project[$i]['total_chat'] = $this->Request_model->get_chat_number($complete_project[$i]['id'], $complete_project[$i]['customer_id'], $complete_project[$i]['designer_id'], "customer");
        }
        $data['complete_project'] = $complete_project;

        $data['heading'] = "incoming";

        $this->load->view('admin/admin_header');
        $this->load->view('admin/request/requests', $data);
        $this->load->view('admin/admin_footer');
    }

    public function ongoing_request() {
        $this->myfunctions->checkloginuser("admin");

        $active_project = $this->Request_model->getall_request("", "'active','disapprove'");
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($active_project[$i]['id'], $active_project[$i]['designer_id']);

            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
        }
        $data['active_project'] = $active_project;

        $check_approve_project = $this->Request_model->getall_request('', "'checkforapprove'");

        for ($i = 0; $i < sizeof($check_approve_project); $i++) {
            $check_approve_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($check_approve_project[$i]['id'], $check_approve_project[$i]['designer_id']);

            $check_approve_project[$i]['total_chat'] = $this->Request_model->get_chat_number($check_approve_project[$i]['id'], $check_approve_project[$i]['customer_id'], $check_approve_project[$i]['designer_id'], "customer");
        }
        $data['check_approve_project'] = $check_approve_project;

        $disapprove_project = $this->Request_model->getall_request("", "'disapprove'");

        for ($i = 0; $i < sizeof($disapprove_project); $i++) {
            $disapprove_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($disapprove_project[$i]['id'], $disapprove_project[$i]['designer_id']);
            $disapprove_project[$i]['total_chat'] = $this->Request_model->get_chat_number($disapprove_project[$i]['id'], $disapprove_project[$i]['customer_id'], $disapprove_project[$i]['designer_id'], "customer");
        }
        $data['disapprove_project'] = $disapprove_project;

        $pending_project = $this->Request_model->getall_request('', "'pending','assign'", "", "index_number");

        for ($i = 0; $i < sizeof($pending_project); $i++) {
            $pending_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($pending_project[$i]['id'], $pending_project[$i]['designer_id']);

            $pending_project[$i]['total_chat'] = $this->Request_model->get_chat_number($pending_project[$i]['id'], $pending_project[$i]['customer_id'], $pending_project[$i]['designer_id'], "customer");
        }
        $data['pending_project'] = $pending_project;


        $complete_project = $this->Request_model->getall_request('', "'approved'");
        for ($i = 0; $i < sizeof($complete_project); $i++) {
            $complete_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($complete_project[$i]['id'], $complete_project[$i]['designer_id']);

            $complete_project[$i]['total_chat'] = $this->Request_model->get_chat_number($complete_project[$i]['id'], $complete_project[$i]['customer_id'], $complete_project[$i]['designer_id'], "customer");
        }
        $data['complete_project'] = $complete_project;

        //$data['request'] = $this->Request_model->getall_request("","'pending','assign'");
        //$data['on_going_request'] = $this->Request_model->getall_request("","'active','pendingforapprove','checkforapprove'");
        //$data['pending_request'] = $this->Request_model->getall_request("","'draft'");
        //$data['approved_request'] = $this->Request_model->getall_request("","'approved'");

        $data['heading'] = "on_going";

        $this->load->view('admin/admin_header');
        $this->load->view('admin/request/requests', $data);
        $this->load->view('admin/admin_footer');
    }

    public function pending_request() {
        $this->myfunctions->checkloginuser("admin");

        $active_project = $this->Request_model->getall_request("", "'active','disapprove'");
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($active_project[$i]['id'], $active_project[$i]['designer_id']);

            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
        }
        $data['active_project'] = $active_project;

        $check_approve_project = $this->Request_model->getall_request('', "'checkforapprove'");

        for ($i = 0; $i < sizeof($check_approve_project); $i++) {
            $check_approve_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($check_approve_project[$i]['id'], $check_approve_project[$i]['designer_id']);

            $check_approve_project[$i]['total_chat'] = $this->Request_model->get_chat_number($check_approve_project[$i]['id'], $check_approve_project[$i]['customer_id'], $check_approve_project[$i]['designer_id'], "customer");
        }
        $data['check_approve_project'] = $check_approve_project;

        $disapprove_project = $this->Request_model->getall_request("", "'disapprove'");

        for ($i = 0; $i < sizeof($disapprove_project); $i++) {
            $disapprove_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($disapprove_project[$i]['id'], $disapprove_project[$i]['designer_id']);
            $disapprove_project[$i]['total_chat'] = $this->Request_model->get_chat_number($disapprove_project[$i]['id'], $disapprove_project[$i]['customer_id'], $disapprove_project[$i]['designer_id'], "customer");
        }
        $data['disapprove_project'] = $disapprove_project;

        $pending_project = $this->Request_model->getall_request('', "'pending','assign'", "", "index_number");

        for ($i = 0; $i < sizeof($pending_project); $i++) {
            $pending_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($pending_project[$i]['id'], $pending_project[$i]['designer_id']);

            $pending_project[$i]['total_chat'] = $this->Request_model->get_chat_number($pending_project[$i]['id'], $pending_project[$i]['customer_id'], $pending_project[$i]['designer_id'], "customer");
        }
        $data['pending_project'] = $pending_project;


        $complete_project = $this->Request_model->getall_request('', "'approved'");
        for ($i = 0; $i < sizeof($complete_project); $i++) {
            $complete_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($complete_project[$i]['id'], $complete_project[$i]['designer_id']);

            $complete_project[$i]['total_chat'] = $this->Request_model->get_chat_number($complete_project[$i]['id'], $complete_project[$i]['customer_id'], $complete_project[$i]['designer_id'], "customer");
        }
        $data['complete_project'] = $complete_project;

        $data['heading'] = "pending";

        $this->load->view('admin/admin_header');
        $this->load->view('admin/request/requests', $data);
        $this->load->view('admin/admin_footer');
    }

    public function approved_request() {
        $this->myfunctions->checkloginuser("admin");

        $active_project = $this->Request_model->getall_request("", "'active','disapprove'");
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($active_project[$i]['id'], $active_project[$i]['designer_id']);

            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
        }
        $data['active_project'] = $active_project;

        $check_approve_project = $this->Request_model->getall_request('', "'checkforapprove'");

        for ($i = 0; $i < sizeof($check_approve_project); $i++) {
            $check_approve_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($check_approve_project[$i]['id'], $check_approve_project[$i]['designer_id']);

            $check_approve_project[$i]['total_chat'] = $this->Request_model->get_chat_number($check_approve_project[$i]['id'], $check_approve_project[$i]['customer_id'], $check_approve_project[$i]['designer_id'], "customer");
        }
        $data['check_approve_project'] = $check_approve_project;

        $disapprove_project = $this->Request_model->getall_request("", "'disapprove'");

        for ($i = 0; $i < sizeof($disapprove_project); $i++) {
            $disapprove_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($disapprove_project[$i]['id'], $disapprove_project[$i]['designer_id']);
            $disapprove_project[$i]['total_chat'] = $this->Request_model->get_chat_number($disapprove_project[$i]['id'], $disapprove_project[$i]['customer_id'], $disapprove_project[$i]['designer_id'], "customer");
        }
        $data['disapprove_project'] = $disapprove_project;

        $pending_project = $this->Request_model->getall_request('', "'pending','assign'", "", "index_number");

        for ($i = 0; $i < sizeof($pending_project); $i++) {
            $pending_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($pending_project[$i]['id'], $pending_project[$i]['designer_id']);

            $pending_project[$i]['total_chat'] = $this->Request_model->get_chat_number($pending_project[$i]['id'], $pending_project[$i]['customer_id'], $pending_project[$i]['designer_id'], "customer");
        }
        $data['pending_project'] = $pending_project;


        $complete_project = $this->Request_model->getall_request('', "'approved'");
        for ($i = 0; $i < sizeof($complete_project); $i++) {
            $complete_project[$i]['total_rating'] = $this->Request_model->get_average_rating_for_request($complete_project[$i]['id'], $complete_project[$i]['designer_id']);

            $complete_project[$i]['total_chat'] = $this->Request_model->get_chat_number($complete_project[$i]['id'], $complete_project[$i]['customer_id'], $complete_project[$i]['designer_id'], "customer");
        }
        $data['complete_project'] = $complete_project;

        $data['heading'] = "approved";

        $this->load->view('admin/admin_header');
        $this->load->view('admin/request/requests', $data);
        $this->load->view('admin/admin_footer');
    }

    public function edit_customer($customer_id = null) {
        $this->myfunctions->checkloginuser("admin");
        if ($customer_id == "") {
            redirect(base_url() . "admin/all_customer");
        }
        $customer = $this->Admin_model->getuser_data($customer_id);
        if (empty($customer)) {
            $this->session->set_flashdata('message_error', 'No Customer Found.!', 5);
            redirect(base_url() . "admin/all_customer");
        }
        $subscription_plan_list = array();
        $subscription = array();
        $planid = "";
        if ($customer[0]['current_plan'] != "") {
            $getcurrentplan = $this->Stripe->getcurrentplan($customer[0]['current_plan']);
            print_r($getcurrentplan);
            if (!empty($getcurrentplan)) {
                $plandetails = $getcurrentplan->items->data[0]->plan;
                $planid = $plandetails->id;
            } else {
                $plandetails = "";
                $planid = "";
            }
        }
        $allplan = $this->Stripe->getallsubscriptionlist();
        $subscription[] = "Select Plan";
        for ($i = 0; $i < sizeof($allplan); $i++) {
            $subscription[$allplan[$i]['id']] = $allplan[$i]['name'] . " - $" . $allplan[$i]['amount'] / 100 . "/" . $allplan[$i]['interval'];
        }
        $carddetails = array();
        $carddetails['last4'] = "";
        $carddetails['exp_month'] = "";
        $carddetails['exp_year'] = "";
        $carddetails['brand'] = "";
        $plan = array();
        if ($customer[0]['current_plan'] != "" && $customer[0]['current_plan'] != "0") {
            $getcurrentplan = $this->Stripe->getcurrentplan($customer[0]['current_plan']);
            if (!empty($getcurrentplan)) {
                $plan['userid'] = $getcurrentplan->id;
                $plan['current_period_end'] = $getcurrentplan->current_period_end;
                $plan['current_period_start'] = $getcurrentplan->current_period_start;
                $plan['billing'] = $getcurrentplan->billing;
                $plan['trial_end'] = $getcurrentplan->trial_end;
                $plan['trial_start'] = $getcurrentplan->trial_start;


                $plandetails = $getcurrentplan->items->data[0]->plan;
                $plan['planname'] = $plandetails->name;
                $plan['planid'] = $plandetails->id;
                $plan['interval'] = $plandetails->interval;
                $plan['amount'] = $plandetails->amount / 100;
                $plan['created'] = $plandetails->created;

                $customerdetails = $this->Stripe->getcustomerdetail($customer[0]['current_plan']);
                if (!empty($customerdetails)) {
                    $carddetails['last4'] = $customerdetails->sources->data[0]->last4;
                    $carddetails['exp_month'] = $customerdetails->sources->data[0]->exp_month;
                    $carddetails['exp_year'] = $customerdetails->sources->data[0]->exp_year;
                    $carddetails['brand'] = $customerdetails->sources->data[0]->brand;
                }
            }
        } else {
            $plan['planid'] = "";
        }
        $designer_list = $this->Admin_model->get_total_customer("designer");
        $designs_requested = $this->Admin_model->get_all_requested_designs(array(), $customer_id, "no");
        $designs_approved = $this->Admin_model->get_all_requested_designs(array("approved"), $customer_id, "", "", "", "status_admin");
        $this->load->view('admin/admin_header');
        $this->load->view('admin/edit_customer', array("customer" => $customer, "subscription" => $subscription, "planid" => $planid, "carddetails" => $carddetails, "designer_list" => $designer_list, "designs_requested" => $designs_requested, "designs_approved" => $designs_approved));
        $this->load->view('admin/admin_footer');
    }

    public function view_request($id) {
        $this->myfunctions->checkloginuser("admin");
        if ($id == "") {
            redirect(base_url() . "admin/all_requests");
        }
        
        $request = $this->Admin_model->get_all_requested_designs(array(), "", "", $id);
        $request2 = $this->Admin_model->get_all_requested_designs(array("pending", "assign", "active"), $request[0]['customer_id']);
        //$this->RequestDiscussions->updateAll(array('admin_seen' => 1),array('request_id' => $request_id));
        $request_id = $request[0]['id'];
		
        if (!empty($_POST)) {
			
            if (isset($_POST['designer_name']) && $_POST['designer_name'] != "") {
			
				if($_POST['designer_name'] != $request[0]['designer_id']){
					
					$status = $this->Request_model->new_designer_get_status($_POST['designer_name'], $request[0]['customer_id']);

					$this->Welcome_model->update_data("requests",array("status"=>$_POST['status'],"status_admin"=>$_POST['status'],"status_designer"=>$_POST['status']),array("id"=>$request[0]['id']));

					$success = $this->Welcome_model->update_data("requests", array("designer_id" => $_POST['designer_name']), array("id" => $request2[0]['id']));

					if ($success) {
						$status = $this->Request_model->new_designer_get_status($_POST['designer_name'], $request[0]['customer_id']);
						if ($status == "active") {
							$this->Welcome_model->update_data("requests", array("status" => "active", "status_admin" => "active", "status_designer" => "active", "dateinprogress" => date("Y-m-d H:i:s")), array("id" => $request2[0]['id']));
							$this->Welcome_model->update_data("requests", array("status" => "assign", "status_admin" => "assign", "status_designer" => "assign", "designer_id" => $_POST['designer_name']), array("id!=" => $request2[0]['id'], "status" => "pending", "customer_id" => $request[0]['customer_id']));
							$this->Welcome_model->update_data("requests", array("status" => "assign", "status_admin" => "assign", "status_designer" => "assign", "designer_id" => $_POST['designer_name']), array("id!=" => $request2[0]['id'], "status" => "assign", "customer_id" => $request[0]['customer_id']));
						} else {
							$this->Welcome_model->update_data("requests", array("status" => "assign", "status_admin" => "assign", "status_designer" => "assign", "designer_id" => $_POST['designer_name']), array("status" => "pending", "customer_id" => $request[0]['customer_id']));
							$this->Welcome_model->update_data("requests", array("status" => "assign", "status_admin" => "assign", "status_designer" => "assign", "designer_id" => $_POST['designer_name']), array("status" => "assign", "customer_id" => $request[0]['customer_id']));
						}
						$this->session->set_flashdata('message_success', 'Designer Updated Successfully.!', 5);

						redirect(base_url() . "admin/request/view_request/" . $id);
					} else {
						$this->session->set_flashdata('message_error', 'Designer Updated Successfully.!', 5);

						redirect(base_url() . "admin/request/view_request/" . $id);
					}
				}else{
					if($this->Welcome_model->update_data("requests",array("status"=>$_POST['status'],"status_admin"=>$_POST['status'],"status_designer"=>$_POST['status']),array("id"=>$request[0]['id']))){
						
						$this->session->set_flashdata('message_success', 'Request Updated Successfully.!', 5);
						redirect(base_url() . "admin/request/view_request/" . $id);
					}else{
						
						$this->session->set_flashdata('message_error', 'Request Not Updated Successfully.!', 5);
						redirect(base_url() . "admin/request/view_request/" . $id);
					}
				}
            }
        }

        $request_files = $this->Admin_model->get_requested_files($request_id, $request[0]['customer_id'], "customer");

        $admin_request_files = $this->Admin_model->get_requested_files($request_id, "", "designer");

        $customer_additional_files = $this->Admin_model->get_requested_files($request_id, "", "customer");

        $admin_files = $this->Admin_model->get_requested_files($request_id, "", "admin");

        $designer_list = $this->Admin_model->get_total_customer("designer");

        $request = $this->Request_model->get_request_by_id($id);

        $customer = $this->Account_model->getuserbyid($request[0]['customer_id']);
        if (!empty($customer)) {
            $request[0]['customer_name'] = $customer[0]['first_name'] . " " . $customer[0]['last_name'];
        }

        $designer = $this->Account_model->getuserbyid($request[0]['designer_id']);
        if (!empty($designer)) {
            $request[0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
            $request[0]['designer_image'] = $designer[0]['profile_picture'];
            $request[0]['designer_sortname'] = substr($designer[0]['first_name'], 0, 1) . substr($designer[0]['last_name'], 0, 1);
        }
        else 
        {
            $request[0]['designer_name'] = "";
            $request[0]['designer_image'] = "";
            $request[0]['designer_sortname'] = "";
        }
        
         $files = $this->Request_model->get_attachment_files($id, "designer");
        $file_chat_array = array();
        for ($i = 0; $i < sizeof($files); $i++) {
            $chat_of_file = $this->Request_model->get_file_chat($files[$i]['id'], "customer");
            if (!empty($chat_of_file)) {
                $file_chat_array[$i]['file_name'] = $files[$i]['file_name'];
                $file_chat_array[$i]['count'] = sizeof($chat_of_file);
                $file_chat_array[$i]['id'] = $files[$i]['id'];
            } else {
                $file_chat_array[$i]['id'] = "";
            }
        }
        
        
        $chat_request = $this->Request_model->get_chat_request_by_id($id);

        $this->load->view('admin/admin_header');
//        $this->load->view('qa/view_project', array("data" => $request, "request_files" => $request_files, "admin_request_files" => $admin_request_files, "designer_list" => $designer_list, "customer_additional_files" => $customer_additional_files, "admin_files" => $admin_files, "designer_preview_file" => $designer_preview_file, "designer_source_file" => $designer_source_file, 'request_id' => $id, 'chat_request' => $chat_request, "file_chat_array" => $file_chat_array));
        $this->load->view('admin/view_request', array("data" => $request, "request_files" => $request_files, "admin_request_files" => $admin_request_files, "designer_list" => $designer_list, "customer_additional_files" => $customer_additional_files, "admin_files" => $admin_files, "chat_request" => $chat_request, $request_id => $id,"file_chat_array" => $file_chat_array));
        $this->load->view('admin/admin_footer');
    }

    public function view_files($id = null) {
        $this->myfunctions->checkloginuser("admin");
        if ($id == "") {
            redirect(base_url() . "admin/request/incoming_request");
        }

        $designer_file = $this->Admin_model->get_requested_files($_GET['id'], "", "designer", "1");
        $data['request'] = $this->Request_model->get_request_by_id($_GET['id']);
        $data['main_id'] = $id;

        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id']);
        }
        $dataa = $this->Request_model->get_request_by_id($_GET['id']);
        $designer = $this->Account_model->getuserbyid($dataa[0]['designer_id']);
        $data['user'] = $this->Account_model->getuserbyid($_SESSION['user_id']);

        if (!empty($designer)) {
            $data['data'][0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
            $data['data'][0]['designer_image'] = $designer[0]['profile_picture'];
            $data['data'][0]['designer_sortname'] = substr($designer[0]['first_name'], 0, 1) . substr($designer[0]['last_name'], 0, 1);
        }

        $data['chat_request'] = $this->Request_model->get_chat_request_by_id($_GET['id']);

        $data['designer_file'] = $designer_file;
        $this->load->view('admin/admin_header');
        $this->load->view('admin/request/view_files', $data);
        $this->load->view('admin/admin_footer');
    }

    public function all_designer() {
        $this->myfunctions->checkloginuser("admin");
        $designers = $this->Admin_model->get_total_customer("designer");

        for ($i = 0; $i < sizeof($designers); $i++) {
            $customer = $this->Admin_model->get_total_customer("customer", $designers[$i]['id']);
            $designer_customer[$designers[$i]['id']] = sizeof($customer);
        }
        //print_r($designers); print_r($designer_customer);

        $this->load->view('admin/admin_header');
        $this->load->view('admin/all_designer', array("designers" => $designers, "designer_customer" => $designer_customer));
        $this->load->view('admin/admin_footer');
    }

    public function view_designer($designer_id = null) {
        $this->myfunctions->checkloginuser("admin");
        if ($designer_id == "") {
            redirect(base_url() . "admin/all_designer");
        }
        if (!empty($_POST)) {
            $checkuser_by_email = $this->Admin_model->check_user_by_email($_POST['email'], $designer_id);
            if (!empty($check_user_by_email)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            }
            $_POST['profile_picture'] = "";
            if (isset($_FILES["profile_picture1"]["name"]) && !empty($_FILES["profile_picture1"]["name"])) {
                $config = array(
                    'upload_path' => "public/profile",
                    'allowed_types' => "jpg|png|jpeg|pdf",
                    'encrypt_name' => TRUE
                );

                $new_name = time() . $_FILES["profile_picture1"]['name'];
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("profile_picture1")) {
                    $data = array($this->upload->data());
                    $_POST['profile_picture'] = $data[0]['file_name'];
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message_error', $data['error'], 5);
                    redirect(base_url() . "admin/view_designer/" . $designer_id);
                }
            }
            $data = array("first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "phone" => $_POST['phone'],
                "email" => $_POST['email'],
                "profile_picture" => $_POST['profile_picture'],
                "modified" => date("Y-m-d H:i:s"));

            $success = $this->Welcome_model->update_data("users", $data, array('id' => $designer_id));
            if ($success) {
                $this->session->set_flashdata('message_success', 'Designer Updated Successfully.!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            } else {
                $this->session->set_flashdata('message_error', 'Designer Updated Not Successfully.!', 5);
                redirect(base_url() . "admin/view_designer/" . $designer_id);
            }
        }
        $designer = $this->Admin_model->getuser_data($designer_id);

        $designs_queue = $this->Admin_model->get_all_requested_designs("", "", "", "", $designer_id, "status_admin");

        $designs_approved = $this->Admin_model->get_all_requested_designs("approved", "", "", "", $designer_id, "status_admin");

        $customer = $this->Admin_model->get_total_customer("customer", $designer_id);

        $this->load->view('admin/admin_header');
        $this->load->view('admin/view_designer', array("designer" => $designer, "designs_queue" => $designs_queue, "customer" => $customer, "designer" => $designer, "designs_approved" => $designs_approved));
        $this->load->view('admin/admin_footer');
    }

    public function add_designer() {
        $this->myfunctions->checkloginuser("admin");
        if (!empty($_POST)) {
            $checkuser_by_email = $this->Admin_model->check_user_by_email($_POST['email']);
            if (!empty($check_user_by_email)) {
                $this->session->set_flashdata('message_error', 'Email Address is already available!', 5);
                redirect(base_url() . "admin/add_designer");
            }
            $_POST['profile_picture'] = "";
            if (isset($_FILES["profile_picture"]["name"]) && !empty($_FILES["profile_picture"]["name"])) {
                $config = array(
                    'upload_path' => "public/profile",
                    'allowed_types' => "jpg|png|jpeg|pdf",
                    'encrypt_name' => TRUE
                );

                $new_name = time() . $_FILES["profile_picture"]['name'];
                $config['file_name'] = $new_name;

                $this->load->library('upload', $config);
                if ($this->upload->do_upload("profile_picture")) {
                    $data = array($this->upload->data());
                    $_POST['profile_picture'] = $data[0]['file_name'];
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('message_error', $data['error'], 5);
                    redirect(base_url() . "admin/add_designer");
                }
            }
            $data = array("first_name" => $_POST['first_name'],
                "last_name" => $_POST['last_name'],
                "phone" => $_POST['phone'],
                "email" => $_POST['email'],
                "new_password" => $_POST['password'],
                "profile_picture" => $_POST['profile_picture'],
                "role" => "designer",
                "created" => date("Y-m-d H:i:s"));

            $success = $this->Welcome_model->insert_data("users", $data);
            if ($success) {
                $this->session->set_flashdata('message_success', 'Designer Added Successfully.!', 5);
                redirect(base_url() . "admin/add_designer");
            } else {
                $this->session->set_flashdata('message_error', 'Designer Added Not Successfully.!', 5);
                redirect(base_url() . "admin/add_designer");
            }
        }
        $this->load->view('admin/admin_header');
        $this->load->view('admin/add_designer');
        $this->load->view('admin/admin_footer');
    }

    public function add_customer() {
        $this->myfunctions->checkloginuser("admin");
        $allplan = $this->Stripe->getallsubscriptionlist();

        $subscription[] = "Select Plan";
        for ($i = 0; $i < sizeof($allplan); $i++) {
            $subscription[$allplan[$i]['id']] = $allplan[$i]['name'] . " - $" . $allplan[$i]['amount'] / 100 . "/" . $allplan[$i]['interval'];
        }

        if (!empty($_POST)) {
            $customer = array();
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['email'] = $_POST['email'];
            $customer['password'] = $_POST['password'];

            $customer['current_plan'] = $_POST['subscription_plans_id'];

            $customer['role'] = "customer";


            //$stripe_controller = new StripesController();
            $stripe_customer = $stripe_controller->createCustomer($customer->email);
            if (!$stripe_customer['status']) {

                $cusomer_save = $this->Welcome_model->insert_data("users", $customer);
                //$customer->current_plan = $stripe_customer['data']['customer_id'];
                if ($cusomer_save <= 0) {
                    $this->Flash->error('Customer cannot ne created.');
                    return $this->redirect(Router::url(['action' => 'index'], TRUE));
                }


                $this->Users->delete($customer);
                $this->Flash->error($stripe_customer['message']);
                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }

            $customer_id = $stripe_customer['data']['customer_id'];
            $card_number = trim($this->request->getData('last4'));
            $expiry = explode('/', $this->request->getData('exp_month'));
            $expiry_month = $expiry[0];
            $expiry_year = $expiry[1];
            $cvc = $this->request->getData('cvc');



            $stripe_card = $stripe_controller->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);

            if (!$stripe_card['status']) {
                $this->Users->delete($customer);
                $this->Flash->error($stripe_card['message']);
                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }

            $subscription_plan_id = $this->request->getData('subscription_plans_id');

            if ($subscription_plan_id) {
                $stripe_subscription = '';
                $stripe_subscription = $stripe_controller->create_cutomer_subscribePlan($stripe_customer['data']['customer_id'], $subscription_plan_id);
            }

            //Add Plan to a customer
            if (!$stripe_subscription['status']) {
                $this->Users->delete($customer);
                $this->Flash->error($stripe_subscription['message']);
                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }


            $customer->current_plan = $stripe_subscription['data']['subscription_id'];

            $customer->customer_id = $stripe_customer['data']['customer_id'];
            $customer->plan_name = $this->request->getData('subscription_plans_id');
            $plandetails = $stripe_controller->retrieveoneplan($this->request->getData('subscription_plans_id'));
            $customer->plan_amount = $plandetails['amount'] / 100;
            $customer->plan_interval = $plandetails['interval'];
            $customer->plan_trial_period_days = $plandetails['trial_period_days'];
            $customer->plan_turn_around_days = $plandetails['metadata']->turn_around_days;

            $is_customer_save = $this->Users->save($customer);
            // $is_customer_save = "1";

            if ($is_customer_save) {
                $email_data = new \stdClass();
                $email_data->to_email = $customer->email;
                $email_data->subject = 'Welcome to Graphics Zoo!';
                $html = "";

                $html.= '<div style="background: #fff;">
								<div style="width:800px;border: 2px solid #f23f5b;margin: auto;border-radius:20px;">
									<div style="text-align:center;margin-bottom: 20px;">
										<img src="' . SITE_IMAGES_URL . 'img/logo.png" height=80px; style="padding: 25px;"></img>
									</div>
									<div style="margin-top:10px;">
										<p style="padding-left: 25px;">Hi ' . $customer->first_name . ', </p>
									</div>
									<div style="margin-top:10px;text-align:justify;">
										<p style="padding: 0px 25px;">We are very excited that you have decided to join us for your design needs. As you get started, expect to have your designs automatically assigned to the best suited designer. you will continue working with that dedicated designer for all your needs. if you have any questions or concerns please fell free to contact us at <u>hello@graphicszoo.com</u> and our team will find a solution for you. We are here to help you with all your needs.</p>
									</div>
									<div style="margin-top:20px;text-align:justify;;padding-bottom:5px;">
										<p style="padding: 0px 25px;">Now, Let\'s go your first design request started. Click below to login and submit a request.</p>
									</div>
									<div style="margin-top:10px;text-align:justify;display: flex;">
										<a href="http://graphicszoo.com/login" style="margin: auto;"><button style="padding:10px;font-size:22px;font-weight:600;color:#fff;background:#f23f5b;border: none;margin: auto;">Request Design</button></a>
									</div>
									
									<div style="margin-top:10px;text-align:justify;display: flex;">
										<p style="padding-left: 25px;">Thank you,</p>
									</div>
									
									<div style="text-align:justify;display: flex;">
										<p style="padding-left: 25px;">Andrew Jones</br>Customer Relations</p>
									</div>
								</div>
							</div>';
                $email_data->template = $html;

                $this->sendMail($email_data);
// exit;
                $plan_amount = 0;

                switch ($subscription_plan_id):
                    case STRIPE_MONTH_SILVER_PLAN_ID:
                        $plan_amount = STRIPE_MONTH_SILVER_PLAN_AMOUNT / 100;
                        break;
                    case STRIPE_MONTH_GOLDEN_PLAN_ID:
                        $plan_amount = STRIPE_MONTH_GOLDEN_PLAN_AMOUNT / 100;
                        break;
                    case STRIPE_YEAR_SILVER_PLAN_ID:
                        $plan_amount = STRIPE_YEAR_SILVER_PLAN_AMOUNT / 100;
                        break;
                    case STRIPE_YEAR_GOLDEN_PLAN_ID:
                        $plan_amount = STRIPE_YEAR_GOLDEN_PLAN_AMOUNT / 100;
                        break;
                    default :
                        break;
                endswitch;

                $this->request->session()->write('plan_amount', $plan_amount);

                $this->Flash->success('User created successfully and subscribed successfully.');

                return $this->redirect(Router::url(['action' => 'index'], TRUE));
            }else {
                $this->Flash->error('User cannot be created.');
            }
        }
        $this->load->view('admin/admin_header');
        $this->load->view('admin/add_customer', array("subscription" => $subscription));
        $this->load->view('admin/admin_footer');
    }

    public function add_request(){
        $allcategories = $this->Category_model->get_categories(0,1);
        
        $created_user = $this->load->get_var('main_user');
        $this->load->view('admin/admin_header_1');
        $this->load->view('admin/add_request',array("data" => $_POST,'existeddata' => $existeddata,'duplicatedata' => $duplicatedata,'profile_data' => $profile_data, 'user_profile_brands' => $user_profile_brands,'canuseradd' => $canuseradd,'allcategories'=>$allcategories,'useraddrequest' => $useraddrequest));
        $this->load->view('admin/admin_footer');
    }
    
    public function Editmsg(){
        $editid = isset($_POST['editid'])?$_POST['editid']:''; 
        $msg = isset($_POST['msg'])?$_POST['msg']:''; 
        $data['is_edited'] = 1;
        $data['message'] = $msg;
          $success = $this->Welcome_model->update_data("request_discussions", $data, array("id" => $editid));
          if($success){
              echo json_encode($editid);
          }
        }
        
    public function Deletemsg(){
          $dele_id = isset($_POST['delid'])?$_POST['delid']:'';
          $data['is_deleted'] = 1;
          $success = $this->Welcome_model->update_data("request_discussions", $data, array("id" => $dele_id));
          if($success){
              echo json_encode($dele_id);
          }
      }

}
