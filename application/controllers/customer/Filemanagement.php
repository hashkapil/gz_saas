<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Filemanagement extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/indexd
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Myfunctions');
        $this->load->library('Customfunctions');
        $this->load->model('Welcome_model');
        $this->load->model('Request_model');
        $this->load->model('Clients_model');
        $this->load->model('File_management');
        $this->load->helper('url');
    }

  public  function index(){
        $isshow_file_mngmnt = $this->is_show_file_mngmnt();
        $this->load->view('customer/customer_header_1');
        $this->load->view('customer/File_management',array("isshow_file_mngmnt" => $isshow_file_mngmnt));
        $this->load->view('customer/footer_customer');
    }

    /** ***file mangement*********** */

    public function getAllRequestsUser() {
        
        $role = isset($_POST['role']) ? $_POST['role'] : '';
        $self_id = isset($_POST['self_id']) ? $_POST['self_id'] : '';
        $main_user = $this->load->get_var('main_user');
        if ($role == "projects") {
            $user_files = $this->File_management->getRequestfilesofUser($main_user);
             
            if($user_files["count"] ==""){
                $user_files["count"] ="0";
            }
        } 
        else if ($role == "brands") {
             $user_files = $this->File_management->get_brand_profile_by_user_id($main_user,'','','brand_permission');
        } 
        else if ($role == "default_folder") {      
            $user_files = $this->File_management->getFolderDefaultStructure($main_user,$folderType ="default");

        }else if ($role == "custom_structure") {
          $result = $this->customfunctions->getfolderfilesofusers();  
//          echo "<pre>"; print_r($result['folder']); exit; 
          $allfiles = $this->File_management->get_custom_files($self_id="0");
          foreach($result['subfolder'] as $k => $subFolders){
              $user_files["foldercount"][$k] = count($subFolders);
           
          }
          foreach ($result['folder'] as $k => $mainFolder){
              $user_files["filecount"][$mainFolder["folder_id"]] = $this->File_management->count_files_underFolder($mainFolder["folder_id"]);
          } 
          $user_files["folder"] = $result['folder'];
          $user_files["files"]= $allfiles['files'];
           // echo "<pre>"; print_r($user_files["folder"][0]['folder_id']); exit; 
          if(!empty($user_files["foldercount"])){
            $user_files["foldercount"]; 
          } 

          if(!empty($user_files["filecount"])){
           $user_files["filecount"]; 
          } 
          
          
        }
        else 
        {   
            $user_files = $this->File_management->getFolderStructure_New($self_id);
        }
         
        echo json_encode($user_files);
    }
    public function getAllRequestFiles() {
        
        $id = $this->input->post("reqid"); 
        $type = isset($_POST['type']) ? $_POST['type'] : '';
        $title = isset($_POST['title']) ? $_POST['title'] : '';
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        if ($type == "brands") {
            $allfiles = $this->File_management->get_brandprofile_materials_files_fmgt($id,$title);
            $allfiles["folder_typ"] = "brand";
        } elseif ($type == "projects") {
            $role = isset($_POST['role']) ? $_POST['role'] : '';
            $allfiles = $this->File_management->getallRequestfiles($id,$role);           
            $allfiles["folder_typ"] = "project"; 
        }elseif ($type == "default") { 
            $allfiles = $this->File_management->getallRequestDefaultfiles($title,$user_id);
           // if($title == "Favorite"){
           //     $allfiles["custom_files"] = $this->File_management->getfavfilefromfolders();
           //     $allfiles["brand_files"] = $this->File_management->getfavfilefrombrands();
           // }
            $allfiles["folder_typ"] = "default"; 
        }
        else {
            $allfiles = $this->File_management->getFolderStructure_New($id);
            $allfiles["folder_typ"] = "other"; 
            
        }
         
        //echo "<pre>"; print_r($allfiles); exit;
       echo json_encode($allfiles); exit; 
    }

    public function createFolder() {
        $data = array();
        $login_user_id = $this->load->get_var('login_user_id');
        $folder_id = isset($_POST['folder_id']) ? $_POST['folder_id'] : '';
        $folder_name = isset($_POST['folder_name']) ? $_POST['folder_name'] : '';
        $folder_nest = isset($_POST['folder_nest']) ? $_POST['folder_nest'] : '';
        if ($folder_id) {
            $data['parent_folder_id'] = $folder_id;
        } else {
            $data['parent_folder_id'] = 0;
        }
        $data['user_id'] = $login_user_id;
        $data['folder_name'] = $folder_name;
        $savefolder = $this->Welcome_model->insert_data("folder_structure", $data);
        $data['folder_id'] = $savefolder;
        if ($savefolder) {
            echo json_encode($data);
        }
    }
    public  function copy_file_to_folder()
        {
            $folder_id = $this->input->post('folder_id');
            $req_id = $this->input->post('req_id');
            $file_id = $this->input->post('file_id');
            $file_name = $this->input->post('file_name');
            $Resopnce = $this->File_management->CopyFolder($folder_id,$req_id,$file_id,$file_name);
            echo $Resopnce; die;
        }

             

    public function copy_filesor_folder() {

        parse_str($_POST['formdata'], $params);

        $fromid = isset($params['fromid']) ? $params['fromid'] : '';
        $fromtype = isset($params['fromtype']) ? $params['fromtype'] : '';
        $fromtitle = isset($params['fromtitle']) ? $params['fromtitle'] : '';
        $folder_id = isset($params['folder_id']) ? $params['folder_id'] : '';
        if ($fromtype == 'files') {
            $allfiles = $this->File_management->getfilesbyfolderid($fromid);
            $files['request_id'] = $allfiles[0]['request_id'];
            $files['file_id'] = $allfiles[0]['file_id'];
            $files['file_name'] = $allfiles[0]['file_name'];
            $files['folder_id'] = $folder_id;
            $id = $this->Welcome_model->insert_data("folder_file_structure", $files);
        } else {
            $allfolders = $this->File_management->getfolderbyfolderid($fromid);
            $allfiles = $this->File_management->getfilesbyfolderid('', $fromid);
            if (!empty($allfolders)) {
                foreach ($allfolders as $index => $value) {
                    $folders['user_id'] = $value['user_id'];
                    $folders['folder_name'] = $value['folder_name'];
                    $folders['parent_folder_id'] = $folder_id;
                    $id = $this->Welcome_model->insert_data("folder_structure", $folders);
                }
            }
            if (!empty($allfiles)) {
                foreach ($allfiles as $index => $value) {
                    $files['request_id'] = $value['request_id'];
                    $files['file_id'] = $value['file_id'];
                    $files['file_name'] = $value['file_name'];
                    $files['folder_id'] = $folder_id;
                    $id = $this->Welcome_model->insert_data("folder_file_structure", $files);
                }
            }
        }
    }

    public function Custom_folder_delete(){
         
        $folder_id = $this->input->post('folderid'); 
        $confirm = $this->input->post('confirmDelete');
        $resp = $this->File_management->check_sub_childs_under_customFolder($folder_id,$confirm);
        echo $resp; die; 
         


    }
    
    public function  checkingForChildOrSubchild(){
        $folder_id = $this->input->post('folderid');
        $res = $this->File_management->checkingForChildOrSubchild($folder_id); 
       echo $res; exit;
    }

    /* 29-9-19 create*/

    public function checkForFavOrUnFav(){
          $id = $this->input->post("projectID");
          $reqid = $this->input->post("user_id");
          $filename = $this->input->post("filename");
          $role = $this->input->post("role");
          $res = $this->File_management->checkForFavOrUnFav($id,$reqid,$filename,$role);
          echo $res; exit; 
           
    }
    public function rename_files(){
          $login_user_id = $this->load->get_var('login_user_id');
          $folderid = $this->input->post("folderID");
          $folderNewname = $this->input->post("newName");
          $res = $this->File_management->rename_folder($folderid,$login_user_id,$folderNewname);
          echo $res; exit; 
           
    }
    
    
    
    public function is_show_file_mngmnt(){
        $users_data = $this->load->get_var('parent_user_data');
        $login_user_data = $this->load->get_var('login_user_data');
        $user_plan_data = $this->load->get_var('parent_user_plan');
        $canfile_mngmnt_access = $this->myfunctions->isUserPermission('file_management');
        if ($users_data[0]['overwrite'] == 1) {
            if ($users_data[0]['file_management'] == 1) {
                if($login_user_data[0]['parent_id'] != 0 && $login_user_data[0]['user_flag'] != 'client' && $canfile_mngmnt_access != 1){
                    $return  = 0;
                }else{
                    $return  = 1;
                }
            }else{
                $return  = 0;
            }
        }else{
            if ($user_plan_data[0]['file_management'] == 1) {
                $return  = 1;
            }else{
                $return  = 0;
            }
        }
        if($login_user_data[0]['parent_id'] != 0 && $login_user_data[0]['user_flag'] == 'client' && $return == 1){
            $loginuser_pln = $this->Clients_model->getsubsbaseduserbyplanid($login_user_data[0]['plan_name'],"",$users_data[0]['id']);
            if ($login_user_data[0]['overwrite'] == 1) {
                if ($login_user_data[0]['file_management'] == 1) {
                    $return = 1;
                } else {
                    $return = 0;
                }
            } else {
                if ($loginuser_pln[0]['file_management'] == 1) {
                    $return = 1;
                } else {
                    $return = 0;
                }
            }
        }  
        return $return;
    }
    
    public function getting_folder_dir(){
        $folderid = $this->input->post("folderID");
        $user_files_dir= $this->File_management->getFolderStructure_New($folderid);
         echo json_encode($user_files_dir);exit; 
    }
    

    public function getting_main_folder(){
        $result = $this->customfunctions->getfolderfilesofusers();
        echo json_encode($result["main_folder"]);
        exit;  
        
    }

    public function deletefolderfiles(){
        $id = isset($_POST['id'])?$_POST['id']:'';
        $delete = $this->File_management->deletefilesfromfolder($id);
        if($delete){
            echo 1;
        }
    }

  /* update work on filemanagement 26-sep-19 */
   public function search_fm(){
        $serachKeyWord = $this->input->post("search_keyword");
        $result = $this->File_management->search_fmModel($serachKeyWord);
        echo json_encode($result); 
      }
      
      public function Uploadbrandprofile() {
            $id = isset($_POST["id"])?$_POST["id"]:"";
            $file_type = isset($_POST["file_type"])?$_POST["file_type"]:"";
            $files = isset($_POST["delete_file"])?$_POST["delete_file"]:"";
                $is_uploaded = $this->customfunctions->movefilestos3($id,"public/uploads/brand_profile/");
                if (($is_uploaded['status'] == 1 || isset($is_uploaded['filepath'])) && !empty($files)) {
                        foreach ($files as $file) {
                            $request_files_data = array("brand_id" => $id,
                                "filename" => $file,
                                "file_type" => $file_type,
                                "created" => date("Y-m-d H:i:s"));
                            $this->Welcome_model->insert_data("brand_profile_material", $request_files_data);
                        }
                        $result = 1;
                } else {
                    $result = 0;
                }
                echo json_encode($result);    
      } 
      
      public function upload_custom_files() {
            $folder_id = (isset($_POST["folder_id"]) && $_POST["folder_id"] != "")?$_POST["folder_id"]:0;
            $files = isset($_POST["delete_file"])?$_POST["delete_file"]:"";
            $login_user_id=$this->load->get_var('login_user_id');
//            echo "folder_id - ".$folder_id;exit;
            $is_uploaded = $this->customfunctions->movefilestos3($folder_id,"public/uploads/custom_files/");
                if (($is_uploaded['status'] == 1 || isset($is_uploaded['filepath'])) && !empty($files)) {
                        foreach ($files as $file) {
                            $files_data = array("folder_id" => $folder_id,
                                "file_name" => $file,
                                "user_id" => $login_user_id);
                            $this->Welcome_model->insert_data("folder_file_structure", $files_data);
                        }
                        $result = 1;
                } else {
                    $result = 0;
                }
                echo json_encode($result);    
      }

}
