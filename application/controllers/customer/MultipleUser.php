<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MultipleUser extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/indexd
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('Myfunctions');
        $this->load->helper('form');
        $this->load->library('s3_upload');
        $this->load->helper('url');
        $this->load->model('Request_model');
        $this->load->model('Welcome_model');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Stripe');
        $this->load->library('zip');

        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    } 

    public function sub_user_list(){
        
       $login_user_id = $this->load->get_var('login_user_id');
       $profile_data = $this->Admin_model->getuser_data($login_user_id);
       $sub_user_data = $this->Request_model->getuserbyparentid($login_user_id);
       
      // echo "<pre>";print_R($sub_user_data);
       $this->load->view('customer/customer_header_1',array('profile'=>$profile_data));
       $this->load->view('customer/sub_user_list',array('profile'=> $profile_data,'sub_user_data'=> $sub_user_data)); 
       $this->load->view('customer/footer_customer');
    }

    public function sub_user(){
        $this->myfunctions->checkloginuser("customer");
        $main_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $parent_user_data = $this->load->get_var('parent_user_data');
        $parent_name = isset($parent_user_data[0]['first_name']) ? $parent_user_data[0]['first_name']:'';
        $subdomain_url = $this->myfunctions->dynamic_urlforsubuser_inemail($main_user,$parent_user_data[0]['plan_name'],'login');

        if(isset($_POST['save_subuser'])){
            $edit_user_id = isset($_POST['cust_id']) ? $_POST['cust_id']: '';
            $cust_url = isset($_POST['cust_url']) ? $_POST['cust_url']: '';
            $password = isset($_POST['password']) ? $_POST['password']: '';
            if (!empty($_POST)) { 
                $emailpassword = $this->myfunctions->random_password();
                $user_flag = isset($_POST['user_flag']) ? $_POST['user_flag']: '';
                $customer['is_active'] = 1; 
                $customer['email'] = $_POST['email'];
                $customer['first_name'] = $_POST['first_name'];
                $customer['last_name'] = $_POST['last_name'];
                $customer['user_flag'] = $user_flag;
                $customer['phone'] = $_POST['phone'];
                $customer['role'] = "customer";
                $customer['last_update'] = date("Y:m:d H:i:s");
                $customer['parent_id'] = $_SESSION['user_id'];
                $customer['modified'] = date("Y:m:d H:i:s");
                if($user_flag == 'client'){
                
                    $customer['requests_type'] = $_POST['requests_type'];
                        
                        if($customer['requests_type'] == 'one_time'){
                           $customer['total_requests'] = isset($_POST['total_requests'])?$_POST['total_requests']:0;
                           $customer['billing_cycle_request'] = 0;
                           $customer['total_active_req'] = 0;
                           $customer['total_inprogress_req'] = 0; 
                        }elseif($customer['requests_type'] == 'per_month'){
                            $customer['total_requests'] = 0;
                            $customer['billing_cycle_request'] = isset($_POST['billing_cycle_requests'])?$_POST['billing_cycle_requests']:0;
                            $customer['total_active_req'] = isset($_POST['total_active_req'])?$_POST['total_active_req']*TOTAL_ACTIVE_REQUEST:0;
                            $customer['total_inprogress_req'] = isset($_POST['total_active_req'])?$_POST['total_active_req']:0;  
                        }else{
                            $customer['billing_cycle_request'] = 0;
                            $customer['total_requests'] = 0;
                            $customer['total_active_req'] = isset($_POST['total_active_req'])?$_POST['total_active_req']*TOTAL_ACTIVE_REQUEST:0;
                            $customer['total_inprogress_req'] = isset($_POST['total_active_req'])?$_POST['total_active_req']:0;    
                        }
                        if($customer['total_inprogress_req'] < 0){
                            $customer['total_inprogress_req'] = 0;
                        }
                }else {
                  $customer['total_active_req'] = TOTAL_ACTIVE_REQUEST;
                  $customer['total_inprogress_req'] = TOTAL_INPROGRESS_REQUEST;  
                }
                if(isset($_POST['genrate_password']) && $_POST['genrate_password'] == 'on'){
                    $genrate_password = 1;
                }
                if(isset($_POST['add_requests']) && $_POST['add_requests'] == 'on'){
                $add_requests = 1;
                }
                if(isset($_POST['view_only']) && $_POST['view_only'] == 'on'){
                $view_only = 1;
                }
                if(isset($_POST['del_requests']) && $_POST['del_requests'] == 'on'){
                    $del_requests = 1;
                }
                if(isset($_POST['app_requests']) && $_POST['app_requests'] == 'on'){
                    $app_requests = 1;
                }
                if(isset($_POST['downld_requests']) && $_POST['downld_requests'] == 'on'){
                    $downld_requests = 1;
                }
                if(isset($_POST['comnt_requests']) && $_POST['comnt_requests'] == 'on'){
                    $comnt_requests = 1;
                }
                if(isset($_POST['billing_module']) && $_POST['billing_module'] == 'on'){
                    $billing_module = 1;
                }
                if(isset($_POST['add_brand_pro']) && $_POST['add_brand_pro'] == 'on'){
                    $add_brand_pro = 1;
                }
                if(isset($_POST['access_brand_pro']) && $_POST['access_brand_pro'] == 'on'){
                    $access_brand_pro = 1;
                }
		if(isset($_POST['manage_priorities']) && $_POST['manage_priorities'] == 'on'){
                    $manage_priorities = 1;
                }
                if(isset($_POST['file_management']) && $_POST['file_management'] == 'on'){
                    $file_management = 1;
                }
                if(isset($_POST['white_label']) && $_POST['white_label'] == 'on'){
                    $white_label = 1;
                }
                if($edit_user_id == ''){
                    $checkemail = $this->Request_model->getuserbyemail($_POST['email']);
                    if (!empty($checkemail)) {
                       $error_msg = $this->session->set_flashdata('message_error', 'Email Address is already taken!', 5);
                       redirect(base_url() . $cust_url);
                    }else{
                        if($genrate_password != 1 && $password != ""){
                            $emailpassword = $password;
                            $customer['password'] = md5($password);
                            $customer['new_password'] = $customer['password'];
                        }else{
                            $customer['password'] = md5($emailpassword);
                            $customer['new_password'] = md5($emailpassword);
                        }
                    $customer['created'] = date("Y:m:d H:i:s");
                    $time = strtotime($customer['created']);
                    if($user_flag == 'client' && $customer['requests_type'] == 'per_month'){
                    $customer['billing_start_date'] = $customer['created'];
                    $customer['billing_end_date'] = date("Y:m:d H:i:s", strtotime("+1 month",$time));
                    }
                    $customer['ip_address'] = $_SERVER['REMOTE_ADDR'];
                    $id = $this->Welcome_model->insert_data("users", $customer);
                    if($id){
                      $array = array('CUSTOMER_NAME' => $customer['first_name'],
                            'CUSTOMER_EMAIL' => $customer['email'],
                            'CUSTOMER_PASSWORD' => $emailpassword,
                                'PARENT_USER' => $parent_name,
                            'LOGIN_URL' => $subdomain_url);
                    $this->myfunctions->send_email_to_users_by_template($main_user, 'welcome_email_to_sub_user', $array, $customer['email']);
                     // echo $emailpassword; die();
                    }
                    }
                }else {
                   // die("edit");
                      $success = $this->Welcome_model->update_data("users", $customer, array("id" => $edit_user_id));
                }
                $brandIDs = $this->Request_model->selectedBrandsforuser($edit_user_id);
//                echo "<pre/>";print_r($brandIDs);exit;
                $BIDs = array_column($brandIDs,'brand_id');
                if($customer['user_flag'] == 'client'){
                 $role['brand_profile_access'] = 0;   
                 $role['add_requests'] =  1; 
                 $role['delete_req'] = 1;
                 $role['approve/revision_requests'] = 1;
                 $role['download_file'] = 1;
                 $role['comment_on_req'] = 1;
                 $role['billing_module'] = 0;
                 $role['add_brand_pro'] = 1;
                 $role['manage_priorities'] = 1;
                 $role['file_management'] = 1;
                 $role['white_label'] = 0;
                }else{
                 $role['brand_profile_access'] = isset($access_brand_pro)?$access_brand_pro:0;   
                 $role['add_requests'] = isset($add_requests)? $add_requests : 0; 
                 $role['delete_req'] = isset($del_requests) ? $del_requests:0;
                 $role['approve/revision_requests'] = isset($app_requests)?$app_requests:0;
                 $role['download_file'] = isset($downld_requests)?$downld_requests:0;
                 $role['comment_on_req'] = isset($comnt_requests)?$comnt_requests:0;
                 $role['billing_module'] = isset($billing_module)?$billing_module:0;
                 $role['add_brand_pro'] = isset($add_brand_pro)?$add_brand_pro:0;
                 $role['manage_priorities'] = isset($manage_priorities)?$manage_priorities:0;
                 $role['file_management'] = isset($file_management)?$file_management:0;
                 $role['white_label'] = isset($white_label)?$white_label:0;
                }
                $role['view_only'] = isset($view_only)?$view_only:0;
                $role['modified'] = date("Y:m:d H:i:s");
            if($edit_user_id == ''){
                 if($id != '')  {
                 $role['customer_id'] = $id; 
                 $role['created'] = date("Y:m:d H:i:s");
                 $role_id = $this->Welcome_model->insert_data("user_permissions", $role); 
                 //echo $role_id;exit;
                 $isbrandselected = $this->Request_model->isBrandselected($role['customer_id']);
                 if($isbrandselected){
                     if($_POST['brandids']){
                        foreach($_POST['brandids'] as $val){
                            $role1['user_id'] = $role['customer_id'];
                            $role1['brand_id'] = $val;
                            $role1['created'] = date("Y:m:d H:i:s");
                            $this->Welcome_model->insert_data("user_brand_profiles", $role1);
                        } 
                     }
                 }
                 if($genrate_password == 1){
                  $this->session->set_flashdata('message_success', 'User created successfully! An email is sent to the address with a temporary password.', 5);
                 }else{
                   $this->session->set_flashdata('message_success', 'User created successfully! An email is sent to the address with password.', 5);  
                 }
                 }
            }else{
               $role['customer_id'] = $edit_user_id; 
               $dataexist = $this->Request_model->ifuserNotinpermision($edit_user_id);
               if($dataexist){
                    $role_id = $this->Welcome_model->update_data("user_permissions", $role, array("customer_id" => $edit_user_id));
               }else{
                  $role_id = $this->Welcome_model->insert_data("user_permissions", $role);
               }
               $isbrandselected = $this->Request_model->isBrandselected($role['customer_id']);
                if($isbrandselected){
                     if($_POST['brandids']){
                         $isdel = $this->Request_model->deleteAlluserbrands($role['customer_id']);
                         if($isdel){
                        foreach($_POST['brandids'] as $val){
                            $role1['user_id'] = $role['customer_id'];
                            $role1['brand_id'] = $val;
                            $role1['created'] = date("Y:m:d H:i:s");
                            $this->Welcome_model->insert_data("user_brand_profiles", $role1);
                        } 
                     }
                     }
                 }else{
                     $this->Request_model->deleteAlluserbrands($role['customer_id']);
                 }
               $this->session->set_flashdata('message_success', 'User Updated successfully..!', 5);
            }
            }
            if ($role_id) {
                redirect(base_url() . $cust_url);
            }
             else {
               $this->session->set_flashdata('message_error', 'Something went wrong,Please Try again ..!', 5);
               redirect(base_url() . $cust_url);
            }
        }
        $sub_user_data = $this->Admin_model->getuser_data($edit_user_id);
        $sub_user_permissions = $this->Request_model->get_sub_user_permissions($edit_user_id);
        $brandprofile = $this->Request_model->get_brand_profile_by_user_id($main_user);
//        echo "<pre/>";print_r($brandprofile);exit;
        $profile_data = $this->Admin_model->getuser_data($login_user_id);
        $this->load->view('customer/customer_header_1',array('profile'=>$profile_data,'canaddbrandprofile' => $canaddbrandprofile));
        $this->load->view('customer/footer_customer');
        $this->load->view('customer/add_new_user',array('brandprofile' => $brandprofile,'sub_user_data' => $sub_user_data,'sub_user_permissions' =>$sub_user_permissions,'selectedbrandIDs' => $BIDs));
    }

    public function delete_sub_user($url,$id) {
        //echo $url.$id;exit;
        $sub_user_permissions = $this->Request_model->delete_user_permission($id);
        if ($sub_user_permissions) {
            $sub_user_data = $this->Request_model->delete_user("users", $id);
        }
        $this->session->set_flashdata('message_success', 'User deleted Successfully', 5);
        if($url == "setting_view"){
            redirect(base_url() . "customer/setting-view#management");
        }else{
            redirect(base_url() . "customer/client_management");
        }
//        header("Refresh:0");
    }
    
//    public function checkloginuser() {
//        if (!$this->session->userdata('user_id')) {
//            $protocol = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
//            $base_url = $protocol . "://" . $_SERVER['HTTP_HOST'];
//            $complete_url = $_SERVER["REQUEST_URI"];
//            $requrl = ltrim($complete_url, '/');
//            if($_SERVER['HTTP_HOST'] == DOMAIN_NAME){ 
//                $url = base_url() . 'login';
//            }else{
//                $url = base_url();
//            }
//            redirect($url.'?url=' . $requrl);
//        }
//        if ($this->session->userdata('role') != "customer") {
//            redirect(base_url());
//        }
//    }
    

}
