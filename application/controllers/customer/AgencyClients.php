<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AgencyClients extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/indexd
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('Myfunctions');
        $this->load->helper('form');
        $this->load->library('s3_upload');
        $this->load->helper('url');
        $this->load->model('Request_model');
        $this->load->model('Category_model');
        $this->load->model('Clients_model');
        $this->load->model('Welcome_model');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->library('Stripeauth');
        $this->load->library('Customfunctions');
        $this->load->model('Stripe');
        $this->load->library('zip');
        $config_email = $this->config->item('email_smtp');
        define('MAIL_SENDER', $config_email['sender']);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
    }
    
    public function index($id=""){
        
        $this->myfunctions->checkloginuser("customer");
        $main_user = $this->load->get_var('main_user');
        $main_userdata = $this->Admin_model->getuser_data($main_user);
        $active_subscriptions = $this->Clients_model->getsubscriptionbaseduser($main_user,'',1);
        $customer_data = $this->Request_model->getAllsubUsers($main_user,"client");
        $useroptions_data = $this->Request_model->getAgencyuserinfo($main_user);
        if(!empty($useroptions_data)){
            $action = "update";
        }else{
            $action = "insert";
        }
        $clientplans = $this->getstripeplans($useroptions_data[0]['stripe_api_key']);
        $stripconnect_link = "";
        if($useroptions_data[0]['stripe_api_key'] == ""){
            $stripconnect_link = $this->stripeauth->strip_auth_link();
            $stripeconnect = $this->stripeauth->strip_auth($main_user,$action); 
        }else{
            $strpe_acc_info = $this->get_stripe_account_info($useroptions_data[0]['stripe_api_key'],$useroptions_data[0]['stripe_account_id']);
        }
        foreach ($active_subscriptions as $key => $userplan){
            $active_subscriptions[$key]['client_slot'] = $this->customfunctions->checkclientaddornot($main_userdata,$userplan);
            if($userplan['plan_type'] == "unlimited"){
               $active_subscriptions[$key]['plan_type'] = $userplan["global_inprogress_request"]." Request"; 
            }
        }
        $req_count = $this->customfunctions->checkclientaddornot($main_userdata);
        if($id){
            $clientdata = $this->getclientprofileinfo($id,$useroptions_data[0]['stripe_api_key'],$main_user);
            $clientdata[0]['req_count'] = $req_count;
        }else{
            $clientdata[0]['apikey'] = $this->Stripe->setapikey(API_KEY);
            $clientdata = array();
        }
        $useroptions_data[0]['pending_reserve_count'] = $req_count['pendreserve_count'];
        $brandprofile = $this->Request_model->get_brand_profile_by_user_id($main_user);
       foreach ($customer_data as $userskey => $usersvalue) {
           $customer_data[$userskey]['profile_picture'] = $this->customfunctions->getprofileimageurl($usersvalue['profile_picture']); 
           $customer_data[$userskey]['active'] = $this->Request_model->getAllrequestscountofSubuser($main_user,$usersvalue['id'],array('active','disapprove','checkforapprove'));
           $customer_data[$userskey]['inqueue'] = $this->Request_model->getAllrequestscountofSubuser($main_user,$usersvalue['id'],array('assign'));
           $customer_data[$userskey]['draft'] = $this->Request_model->getAllrequestscountofSubuser($main_user,$usersvalue['id'],array('draft'));
           $customer_data[$userskey]['complete'] = $this->Request_model->getAllrequestscountofSubuser($main_user,$usersvalue['id'],array('approved'));
           $customer_data[$userskey]['cancel'] = $this->Request_model->getAllrequestscountofSubuser($main_user,$usersvalue['id'],array('cancel'));
           $customer_data[$userskey]['total_req'] = ($customer_data[$userskey]['active'] + $customer_data[$userskey]['inqueue'] +  $customer_data[$userskey]['draft'] + $customer_data[$userskey]['complete'] + $customer_data[$userskey]['cancel']);
           $customer_data[$userskey]['mode'] = $this->Clients_model->getsubsbaseduserbyplanid($usersvalue["plan_name"],"plan_id,payment_mode");
       }
       $subscriptions = $this->Clients_model->getsubscriptionbaseduser($main_user); 
       
       foreach ($subscriptions as $key => $subs){
            $subscriptions[$key]['shared_user_type'] = $this->plan_usertype($subs['shared_user_type']);
       }
       /**client dashboard **/
       $report = $this->client_dashboard($main_user,sizeof($customer_data),$useroptions_data[0]['stripe_api_key']);
       
       $this->load->view('customer/customer_header_1');
       $this->load->view('customer/agency_user/client_management',array("brandprofile" => $brandprofile,"clients_data" => $customer_data,'checksubuser_requests' => $req_count,"stripeconnect"=>$stripeconnect,"stripconnect_link"=>$stripconnect_link,"subscriptions"=>$subscriptions,"active_subscriptions"=>$active_subscriptions,"clientdata" => $clientdata,"report" => $report,"agency_data" => $useroptions_data,"strpe_acc_info" => $strpe_acc_info,"clientplans" => $clientplans)); 
       $this->load->view('customer/footer_customer');
    }
    
    public function getstripeplans($stripeid=""){
        $client_plans = array();
        if($stripeid != ""){
            $this->Stripe->setapikey($stripeid);
            $plans = $this->Stripe->getclientplans();
            if (!empty($plans['plan'])) {
                foreach ($plans['plan'] as $k => $plan) {
                    $client_plans[$k]['id'] = $plan->id;
                    if ($plan['nickname'] != "") {
                        $client_plans[$k]['plan_name'] = $plan['nickname'];
                    } else {
                        $client_plans[$k]['plan_name'] = $plan['name'];
                    }
                    $client_plans[$k]['price'] = $plan['amount'];
                    $client_plans[$k]['interval'] = $plan['interval'];
                }
            }
        }
       return $client_plans;
    }
    
    public function addclientsubscription(){
        if (!empty($_POST)) { 
            $result = array();
            $login_user_id = $this->load->get_var('main_user');
            $paymode = (isset($_POST['subs_payment_mode']) && $_POST['subs_payment_mode'] == "on")?1:0;
            $subs_id = (isset($_POST['f_subs_id']) && $_POST['f_subs_id'] != "")?$_POST['f_subs_id']:"";
            if (!empty($_POST['subs_plan_features'])) {
                $subs_plan_features = implode('_', $_POST['subs_plan_features']);
            }
            $plan_type_name = isset($_POST['subs_plan_type_name'])?$_POST['subs_plan_type_name']:'';
            if($plan_type_name == "one_time"){
                $global_inprogress_request = isset($_POST['total_requests'])?$_POST['total_requests']:0;
                $global_active_request = 0;
                $plan_type = 'unlimited';
                $user_type = 2; //for one_time users
                if($subs_id == ""){
                    $plan_id = uniqid();
                }else{
                    $id = $this->Clients_model->getsubscriptionbaseduser("",$subs_id,"","plan_id");
                    $plan_id = $id[0]['plan_id'];
                }
            }else{
                if ($paymode == 1) {
                    $plan_id = (isset($_POST['subs_planid']) && $_POST['subs_planid'] != "") ? $_POST['subs_planid'] : "";
                } else {
                    $plan_id = (isset($_POST['subs_plan_id']) && $_POST['subs_plan_id'] != "") ? $_POST['subs_plan_id'] : "";
                }
                //$plan_id = isset($_POST['subs_plan_id'])?$_POST['subs_plan_id']:'';
                $user_type = isset($_POST['subs_plan_user_type'])?$_POST['subs_plan_user_type']:'';
                $global_inprogress_request = 1;
                $global_active_request = $global_inprogress_request*TOTAL_ACTIVE_REQUEST;
                $plan_type = isset($_POST['subs_plan_type'])?$_POST['subs_plan_type']:'';
                
            }
            if($subs_id == ""){
                $isplanexist = $this->Clients_model->getsubsbaseduserbyplanid($plan_id,"id",$login_user_id);
                if(!empty($isplanexist)){
                    $result['status'] = 0;
                    $result['msg'] = 'This stripe plan id is already exist. Please use unique ID';
                    echo json_encode($result);exit;
                }
            }
            $subscrption = array("user_id" => $login_user_id,
                "plan_id" => $plan_id,
                "plan_name" => isset($_POST['subs_plan_name'])?$_POST['subs_plan_name']:'',
                "features" => $subs_plan_features,
                "plan_type" => $plan_type,
                "shared_user_type" => $user_type,
                "plan_type_name" => isset($_POST['subs_plan_type_name'])?$_POST['subs_plan_type_name']:'',
                "plan_price" => isset($_POST['subs_plan_price'])?$_POST['subs_plan_price']:'',
                "global_inprogress_request" => $global_inprogress_request,
                "global_active_request" => $global_active_request,
                "is_active" => (isset($_POST['f_subs_plan_active']) && $_POST['f_subs_plan_active'] == "on")?1:0,
                "file_sharing" => (isset($_POST['f_file_sharing']) && $_POST['f_file_sharing'] == "on")?1:0,
                "file_management" => (isset($_POST['f_file_management']) && $_POST['f_file_management'] == "on")?1:0,
                "apply_coupon" => (isset($_POST['f_apply_coupon']) && $_POST['f_apply_coupon'] == "on")?1:0,
                "payment_mode" => $paymode);
            if($subs_id == ""){
                $id = $this->Welcome_model->insert_data("agency_subscription_plan", $subscrption);
                $msg = "Added Subscription successfully";
            }else{
                $id = $this->Welcome_model->update_data("agency_subscription_plan", $subscrption,array("id" => $subs_id));
                $msg = "Edit Subscription successfully";
            }
            if($id) {
               $this->session->set_flashdata('message_success', $msg, 5);
               $result['status'] = 1;
               echo json_encode($result);exit;
            }else{
                $result['status'] = 0;
                $result['msg'] = 'Something went wrong,Please Try again ..!';
                echo json_encode($result);exit;
            }
        }
    }
    
    public function getclientsubscription(){
        $id = isset($_POST['id'])?$_POST['id']:'';
        $subscriptions = $this->Clients_model->getsubscriptionbaseduser('',$id); 
        echo json_encode($subscriptions[0]);
    }
    
    public function addclientwithsubscription(){
        $output = array();
        
        $login_user_id = $this->load->get_var('main_user');
        $useroptions_data = $this->Request_model->getAgencyuserinfo($login_user_id,'');
        $userdata = $this->load->get_var('login_user_data');
        $parent_name = isset($userdata[0]['first_name']) ? $userdata[0]['first_name']:'';
        $pass = isset($_POST['password'])?$_POST['password']:"";
        $bypas_paymnt = (isset($_POST['f_bypass_payment']) && $_POST['f_bypass_payment'] == "on")?1:0;
        if(isset($_POST['genrate_password']) && $_POST['genrate_password'] == "on"){
            $genrate_password = 1;
        }
        if($genrate_password != 1 && $pass != ""){
            $password = $pass;
        }else{
            $password = $this->myfunctions->random_password(); 
        }
        $email = isset($_POST['email'])?$_POST['email']:"";
        
        $planname = isset($_POST['requests_type'])?$_POST['requests_type']:"";
        $subscriptions = $this->Clients_model->getsubsbaseduserbyplanid($planname,"plan_id,plan_name,global_inprogress_request,global_active_request,plan_price,plan_type,plan_type_name,shared_user_type,payment_mode");
   //     $shared_user = $subscriptions[0]['shared_user_type'];
        
        $checkemail = $this->Request_model->getuserbyemail($email);
        if (!empty($checkemail)) {
           $output['msg'] = 'Email Address is already available.!';
           $output['status'] = 0;
           echo json_encode($output);exit;
        }
        
        $this->Stripe->setapikey($useroptions_data[0]['stripe_api_key']); 
        $stripe_customer = $this->Stripe->createnewCustomer($email); 
        if (!$stripe_customer['status']) {
           $output['msg'] = $stripe_customer['message'];
           $output['status'] = 0;
           echo json_encode($output);exit;
        }
        $customer_id = $stripe_customer['data']['customer_id'];
        
        $customer = array("parent_id" => $login_user_id,
            "first_name" => isset($_POST['first_name'])?$_POST['first_name']:"",
            "last_name" => isset($_POST['last_name'])?$_POST['last_name']:"",
            "email" => $email,
            "phone" => isset($_POST['phone'])?$_POST['phone']:"",
            "password" => md5($password),
            "new_password" => md5($password),
            "ip_address" => $_SERVER['REMOTE_ADDR'],
            "plan_name" => $planname,
            "plan_amount" => $subscriptions[0]['plan_price'],
            "display_plan_name" => $subscriptions[0]['plan_name'],
            "requests_type" => $subscriptions[0]['plan_type_name'],
            "customer_id" => $customer_id,
            "is_active" => 1,
            "is_logged_in" => 0,
            "user_flag" => "client",
            "role" => "customer",
            "modified" => date("Y:m:d H:i:s"),
            "created" => date("Y:m:d H:i:s"),
            "total_active_req" => isset($subscriptions[0]['global_active_request'])?$subscriptions[0]['global_active_request']:0,
            "total_inprogress_req" => isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0
            );
        if ($subscriptions[0]['payment_mode'] != 0) {
            $this->Stripe->setapikey($useroptions_data[0]['stripe_api_key']);
            $stripe_customer = $this->Stripe->createnewCustomer($email);
            if (!$stripe_customer['status']) {
                $output['msg'] = $stripe_customer['message'];
                $output['status'] = 0;
                echo json_encode($output);
                exit;
            }
            $customer_id = $stripe_customer['data']['customer_id'];
            
            if ($bypas_paymnt == 0) {
                $customer = $this->notbypasspayment($customer_id, $subscriptions, $planname, $customer);
            } else {
                $customer['payment_status'] = 0;
            }
        } else {
            $customer = $this->customfunctions->offlinepayment_process($subscriptions, $planname, $customer);
            $customer_id = "";
        }
        $customer['customer_id'] = $customer_id;
        $subdomain_url = $this->myfunctions->dynamic_urlforsubuser_inemail($login_user_id,$userdata[0]['plan_name'],'login');
        if (empty($checkemail)) {
            $id = $this->Welcome_model->insert_data("users", $customer);
            if($id){
                $this->Stripe->setapikey(API_KEY);
                /** user permissions **/
                $role['brand_profile_access'] = 0;   
                $role['add_requests'] =  1; 
                $role['delete_req'] = 1;
                $role['approve/revision_requests'] = 1;
                $role['download_file'] = 1;
                $role['comment_on_req'] = 1;
                $role['billing_module'] = 0;
                $role['add_brand_pro'] = 1;
                $role['manage_priorities'] = 1;
                $role['customer_id'] = $id; 
                $role['created'] = date("Y:m:d H:i:s");
                $this->Welcome_model->insert_data("user_permissions", $role); 
                /** end user permissions **/
                
                /** extra user info **/
                if($subscriptions[0]['payment_mode'] != 0){
                    $user_info['user_id'] = $id;
                    $user_info["bypass_payment"] = $bypas_paymnt;
                    $user_info['created'] = date("Y:m:d H:i:s");
                    $this->Welcome_model->insert_data("users_info", $user_info); 
                }
                /** end extra user info **/
                $array = array('CUSTOMER_NAME' => $customer['first_name'],
                        'CUSTOMER_EMAIL' => $customer['email'],
                        'CUSTOMER_PASSWORD' => $password,
                            'PARENT_USER' => $parent_name,
                        'LOGIN_URL' => $subdomain_url);
                $this->myfunctions->send_email_to_users_by_template($login_user_id, 'welcome_email_to_sub_user', $array, $customer['email']);
                
                if($genrate_password == 1){
                    $this->session->set_flashdata('message_success', 'User created successfully! An email is sent to the address with a temporary password.', 5);
                    $output['msg'] = "User created successfully! An email is sent to the address with a temporary password.";
                    $output['status'] = 1;
                    echo json_encode($output);exit;
                }else{
                    $this->session->set_flashdata('message_success', 'User created successfully! An email is sent to the address with password.', 5);
                    $output['msg'] = "'User created successfully! An email is sent to the address with password.";
                    $output['status'] = 1;
                    echo json_encode($output);exit;
                 }
            }
        }
    }
    
    public function notbypasspayment($customer_id,$subscriptions,$planname,$customer) {
       
        $u_data = $this->Request_model->get_userdata_bycustomerid($customer_id);
        $expir_date = explode("/", $_POST['expir_date']);
        $expiry_month = $expir_date[0];
        $expiry_year = $expir_date[1];
        $card_number = trim($_POST['card_number']);
        $cvc = $_POST['cvc'];
        $output = array();
        
        $stripe_card = $this->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);
        if (!$stripe_card['status']){
           $output['msg'] = $stripe_card['message'];
           $output['status'] = 0;
           echo json_encode($output);exit;
        }
        
        if($subscriptions[0]['plan_type_name'] == 'one_time'){
            $customer['total_requests'] = isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0;
            $customer['total_active_req'] = 0;
            $customer['total_inprogress_req'] = 0; 
            $customer['billing_start_date'] = date('Y-m-d H:i:s');
            $invoice_created = $this->Stripe->create_invoice($customer_id,0,$subscriptions[0]['plan_price']);
            if($invoice_created['status'] != true){
                 $output['message'] = $invoice_created['message'];
                 $output['status'] = 0;
                 echo json_encode($output);exit;
            }
        }
        else{
            $customer['total_requests'] = 0;
            $billing_start = ($u_data[0]['billing_start_date'] != "")?strtotime($u_data[0]['billing_start_date']):"";
            $customer['total_active_req'] = isset($subscriptions[0]['global_active_request'])?$subscriptions[0]['global_active_request']:0;
            $customer['total_inprogress_req'] = isset($subscriptions[0]['global_inprogress_request'])?$subscriptions[0]['global_inprogress_request']:0;
            $stripe_subscription = "";
            if ($planname) {
                $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($customer_id, $planname, $subscriptions[0]['global_inprogress_request'],"",$billing_start);
                if ($stripe_subscription['status'] == 0) {
                    $output['msg'] = $stripe_subscription['message'];
                    $output['status'] = 0;
                    echo json_encode($output);exit;
                }else{
                    $customer['billing_start_date'] = date('Y-m-d H:i:s',$stripe_subscription['data']['period_start']);
                    $customer['billing_end_date'] = date('Y-m-d H:i:s',$stripe_subscription['data']['period_end']);
                    $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
                    $plandetails = $this->Stripe->retrieveoneplan($planname);
                    $customer['plan_interval'] = $plandetails['interval'];
                    $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
                    $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
                }
            }
            
        }
        $customer["requests_type"] = $subscriptions[0]['plan_type_name'];
        $customer["plan_amount"] = $subscriptions[0]['plan_price'];
        $customer["plan_name"] = $subscriptions[0]['plan_id']; 
        $customer["display_plan_name"] = $subscriptions[0]['plan_name'];  
        return $customer;
    }
    
    public function payment_process() {
        $customer_data = array();
        $output =array();
        $planname = isset($_POST['plan_name'])?$_POST['plan_name']:"";
        $customer_id = isset($_POST['customer_id'])?$_POST['customer_id']:"";
        $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
        $subscriptions = $this->Clients_model->getsubsbaseduserbyplanid($planname,"plan_id,plan_name,global_inprogress_request,global_active_request,plan_price,plan_type,plan_type_name,shared_user_type,payment_mode");
        $customer = $this->notbypasspayment($customer_id,$subscriptions,$planname,$customer_data);
        if(!empty($customer) && $user_id != ""){
            $update = $this->Welcome_model->update_data("users", $customer, array('id' => $user_id));
            if($update){
                $this->Welcome_model->update_data("users_info", array("bypass_payment" => 0), array('user_id' => $user_id));
                $output['message'] = "Your Subscription is activated";
                $output['url'] = base_url()."customer/request/design_request";
                $output['status'] = 1;
                echo json_encode($output);exit;
            }else{
                $output['message'] = "Customer not updated successfully";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
        }else{
                $output['message'] = "Subscription not created";
                $output['status'] = 0;
                echo json_encode($output);exit;
        }
    }
    public function customer_edit_profile($id) {
        $this->myfunctions->checkloginuser("customer");
        $user_id = $id;
        $profileUpdate = array(
            'first_name' => $_POST['first_name'],
            'last_name' => $_POST['last_name'],
            'email' => $_POST['notification_email'],
            'company_name' => $_POST['company_name'],
            'phone' => $_POST['phone'],
            'address_line_1' => $_POST['address_line_1'],
            'title' => $_POST['title'],
            'city' => $_POST['city'],
            'state' => isset($_POST['state']) ? $_POST['state'] : '',
            'zip' => $_POST['zip'],
            'modified' => date("Y:m:d H:i:s")
        );
        $update_data_result = $this->Admin_model->designer_update_profile($profileUpdate, $user_id);
        if ($update_data_result) {
            $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
        } else {
            $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
        }
        redirect(base_url() . "customer/client_management/".$id."#client_management");
    }
    
    public function customer_active_requests($user_id) {
        $this->myfunctions->checkloginuser("customer");
        $parent_user_data = $this->load->get_var('parent_user_data');
        $brand_count = isset($_POST["brand_size"])?$_POST["brand_size"]:0;
        $filemanagement = (isset($_POST['file_management']) && $_POST['file_management'] == "on") ? 1 : 0;
        $file_sharing = (isset($_POST['file_sharing']) && $_POST['file_sharing'] == "on") ? 1 : 0;
        $user_data = $this->Admin_model->getuser_data($user_id);
        $user_info_data = $this->Admin_model->getextrauser_data($user_id);
        $globaluserinfo = $this->Clients_model->getsubsbaseduserbyplanid($user_data[0]['plan_name']);
        $shared_user = $globaluserinfo[0]['shared_user_type'];
        
        $user_setting = array(
            'total_requests' => isset($_POST['total_requests']) ? $_POST['total_requests'] : 0,
            'billing_cycle_request' => isset($_POST['billing_cycle_request']) ? $_POST['billing_cycle_request'] : 0,
            'modified' => date("Y:m:d H:i:s")
        );
        
        if($shared_user != 1){
            $user_setting['total_inprogress_req'] = isset($_POST['inprogess_req_count']) ? $_POST['inprogess_req_count'] : 0;
            $user_setting['total_active_req'] = isset($_POST['total_req_count']) ? $_POST['total_req_count'] : 0;
        }
        /** extra user info array **/
        
        $user_info = array("brand_profile_count" => $brand_count,
            "file_management" => $filemanagement,
            "file_sharing" => $file_sharing,
            'modified' => date("Y:m:d H:i:s"));
        
        /** end extra user info array **/
        if($user_setting['total_inprogress_req'] > 0){
            $clientslot = $this->customfunctions->checkclientaddornot($parent_user_data);
            $shared_ratio = $clientslot['shared_ratio'];
            
            if($shared_user == 0){
               $inprogress_req = $user_setting['total_inprogress_req']*$shared_ratio;
               $error_msg = "You can't add more dedicated designer";
            }else{
                $inprogress_req = $user_setting['total_inprogress_req'];
                $error_msg = "You can't add more shared designer";
            }
                $subuseraddreq = $clientslot['addedsbrqcount'] - $user_data[0]['total_inprogress_req'];
//                $pending_inprog_req = ($pending_req > 0)?$pending_req:0;
                $canadd_req = $subuseraddreq + $inprogress_req;
                
            if($user_data[0]['requests_type'] != "one_time"){
                
                if($clientslot['main_dedicated_designr'] == 1 && $shared_user == 0){
                    $this->session->set_flashdata('message_error',"You cann't add dedicated designer, you can add only shared user.");
                    redirect(base_url() . "customer/client_management/".$user_id."#client_management");
                }else if($canadd_req > $clientslot['main_inprogress_req']){
                    //$this->session->set_flashdata('message_error', 'As per you plan, '.$main_inprogress_req.' dedicated designers assigned to your account. Out of which you already assigned '.$subusers_reqcount.' designers to your existing clients. So you can assign '.$pending_inprog_req.' more designers to new customers.', 5);
                    $this->session->set_flashdata('message_error',$error_msg);
                    redirect(base_url() . "customer/client_management/".$user_id."#client_management");
                }
            }
        }
        if ($globaluserinfo[0]['global_active_request'] != $user_setting['total_active_req'] || $globaluserinfo[0]['global_inprogress_request'] != $user_setting['total_inprogress_req'] || $globaluserinfo[0]['active_request'] != $user_setting['total_requests'] || $globaluserinfo[0]['turn_around_days'] != $user_setting['turn_around_days'] || $globaluserinfo[0]['turn_around_days'] != $user_setting['turn_around_days'] || $globaluserinfo[0]['brand_profile_count'] != $user_info['brand_profile_count'] || $globaluserinfo[0]['file_management'] != $user_info['file_management'] || $globaluserinfo[0]['file_sharing'] != $user_info['file_sharing']) {
            $user_setting['overwrite'] = 1;
        }
        $update_data_result = $this->Admin_model->designer_update_profile($user_setting, $user_id);
        if ($update_data_result) {
            /** save extra user info **/
            if(empty($user_info_data)){
                $user_info['user_id'] = $user_id;
                $user_info['created'] = date("Y:m:d H:i:s");
                $this->Welcome_model->insert_data("users_info", $user_info); 
            }else{
//                 echo $user_id."<pre>";print_R($user_info);exit;
                $this->Welcome_model->update_data("users_info", $user_info, array('user_id' => $user_id));
            }
            /** end save extra user info **/
            $this->myfunctions->moveinqueuereq_toactive_after_updrage_plan($user_id, $_POST['inprogess_req_count']);
            $this->session->set_flashdata('message_success', 'User Profile Update Successfully.!', 5);
        } else {
            $this->session->set_flashdata('message_error', 'User Profile Not Update.!', 5);
        }
        redirect(base_url() . "customer/client_management/".$user_id."#client_management");
    } 
    
    public function get_customer_detail_from_strip($plan_id,$customer_id="") {
        $card_detail = $this->Stripe->getcustomerdetail($plan_id,$customer_id);
        $card = $card_detail->sources['data'];
        return $card;
    }
    
    public function change_client_plan() {
        
        $customer_id = isset($_POST['client_id']) ? $_POST['client_id'] : '';
        $user_data = $this->Admin_model->getuser_data($customer_id);
        $main_user = $this->load->get_var('main_user');
        $back_url = isset($_POST['back_url']) ? $_POST['back_url'] : '';
        $current_plan_price = isset($_POST['plan_price']) ? $_POST['plan_price'] : '';
        $current_plan_name = $user_data[0]['plan_name'];
        $next_plan_name = $user_data[0]['next_plan_name'];
        $subscription_plan_id = isset($_POST['plan_name']) ? $_POST['plan_name'] : '';
        
        $plan_info = $this->Clients_model->getsubsbaseduserbyplanid($subscription_plan_id,"plan_id,plan_name,global_inprogress_request,global_active_request,plan_price,plan_type,plan_type_name,shared_user_type,payment_mode",$main_user);
        $inprogress_request = isset($_POST['in_progress_request']) ? $_POST['in_progress_request'] :1;
        $total_active_req = $plan_info[0]['global_active_request'];
        $userstate = $user_data[0]['state'];
        $display_name = isset($_POST['display_name']) ? $_POST['display_name'] : $plan_info[0]['plan_name'];
        $useroptions_data = $this->Request_model->getAgencyuserinfo($main_user,'');
        $this->Stripe->setapikey($useroptions_data[0]['stripe_api_key']);
        $url = substr($back_url, strrpos($back_url, '/') + 1);
        if($url == 'setting-view'){
            $bckurl = $back_url.'#billing';
        }else{
            $bckurl = $back_url;
        }
        if (array_key_exists($userstate, STATE_TEXAS)) {
            $state_tax = STATE_TEXAS[$userstate];
        } else {
            $state_tax = 0;
        }
        $customer = array('plan_amount' => $current_plan_price,
            'display_plan_name' => $display_name,
            'plan_name' => $subscription_plan_id);
        $updateuserplan = array();
        //if user upgrade 49 plan
        if(!empty($plan_info) && $plan_info[0]['payment_mode'] != 0){
            if ($plan_info[0]['plan_type_name'] == 'one_time') {
                $updateuserplan = $this->Stripe->create_invoice($user_data[0]['customer_id'], 0, $current_plan_price);
                if ($updateuserplan['status'] != true) {
                    $updateuserplan['message'] = $updateuserplan['message'];
                    $updateuserplan[0] = 'error';
                } else {
                    $updateuserplan[0] = 'success';
                    $customer['total_inprogress_req'] = 0;
                    $customer['total_active_req'] = 0;
                    $customer['total_requests'] = $user_data[0]['total_requests']+($plan_info[0]['global_inprogress_request'] * $inprogress_request);
                    $customer['invoice'] = $updateuserplan['id'];
                    $customer['billing_start_date'] = date('Y-m-d H:i:s');
                }
            } else {
                if ($user_data[0]['invoice'] == '' || $user_data[0]['invoice'] == NULL || $user_data[0]['invoice'] == 1) {
                    $current_plan_weight = $this->Clients_model->getsubsbaseduserbyplanid($current_plan_name,"",$main_user);
                } elseif ($user_data[0]['invoice'] == 0) {
                    $current_plan_weight = $this->Clients_model->getsubsbaseduserbyplanid($next_plan_name,"",$main_user);
                }
                $post_plan_weight = $this->Clients_model->getsubsbaseduserbyplanid($subscription_plan_id,"",$main_user);
                if ($current_plan_weight[0]['plan_price'] <= $post_plan_weight[0]['plan_price']) {
                    $invoice = 1;
                } else {
                    $invoice = 0;
                }
                $updateuserplan = $this->Stripe->updateuserplan($user_data[0]['customer_id'], $subscription_plan_id, $inprogress_request, $invoice);
                $subscription = $updateuserplan['data']['subscriptions'];
                $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);
                $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
                if ($invoice == 1 || ($invoice != 1 && $user_data[0]['is_cancel_subscription'] == 1)) {
                        $customer['billing_cycle_request'] = 0;
                    if ($invoice == 1) {
                        $this->Stripe->create_invoice($user_data[0]['customer_id'], $state_tax);
                    }
                    
                    $customer['current_plan'] = $subscription['data'][0]->id;
                    $customer['billing_start_date'] = date('Y-m-d H:i:s', $subscription['data']['0']->current_period_start);
                    $customer['billing_end_date'] = date('Y-m-d H:i:s', $subscription['data']['0']->current_period_end);
                    $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
                    $customer['total_inprogress_req'] = $inprogress_request;
                    $customer['total_active_req'] = $inprogress_request*$total_active_req;
                    $customer['total_requests'] = 0;
                    
                    $customer['invoice'] = 1;
                    $customer['plan_interval'] = $plandetails['interval'];
                    if ($user_data[0]['is_cancel_subscription'] == 1) {
                        $customer['is_cancel_subscription'] = 0;
                        $flag = isset($_POST['reactivate_pro']) ? $_POST['reactivate_pro'] : '';
                        if ($flag == 'yes') {
                            $this->myfunctions->movecancelprojecttopreviousstatus($main_user,$customer_id);
                        }
                    }

                } else if ($invoice == 0) {
                    //if invoice not created
                    $customer['next_plan_name'] = $subscription_plan_id;
                    $customer['invoice'] = 0;
                    $customer['next_current_plan'] = $subscription['data'][0]->id;
                    $customer['display_plan_name'] = "";
                    $customer['plan_amount'] = "";
                    $customer['plan_name'] = "";
                }
            }
        }else{
            $login_user_data = $this->load->get_var('login_user_data');
            $parent_user_data = $this->load->get_var('parent_user_data');
            $currentplan_name = $this->Clients_model->getsubsbaseduserbyplanid($current_plan_name,"plan_name",$main_user);
            if($login_user_data[0]['user_flag'] == 'client'){
                $array = array("CUSTOMER_NAME" => $parent_user_data[0]['first_name'],
                    "REQUEST_FROM" => $login_user_data[0]['first_name']." ".$login_user_data[0]['last_name'],
                    "PLAN_NAME" => $plan_info[0]["plan_name"],
                    "CURRENT_PLAN_NAME" => $currentplan_name[0]["plan_name"],
                    "REQUEST_COUNT" => $inprogress_request,
                    "URL" => base_url()."customer/client_management/".$login_user_data[0]['id']);
                $this->myfunctions->send_email_to_users_by_template($main_user, 'change_plan_request', $array, $parent_user_data[0]['email']);
                $this->session->set_flashdata('message_success', "Your request for changing the plan has been submitted to successfully.Your account manager will reply within 1-2 business days. ", 5);
                redirect($bckurl);
            }else{
                $customer = $this->customfunctions->offlinepayment_process($plan_info, $subscription_plan_id, $customer,$user_data[0]['total_requests']);
                $updateuserplan[0] = 'success';
            }
        }
        $customer['requests_type'] = $plan_info[0]['plan_type_name'];
        $customer['modified'] = date("Y:m:d H:i:s");
        if ($updateuserplan[0] != 'success') {
            $this->Stripe->setapikey(API_KEY);
            $this->session->set_flashdata('message_error', $updateuserplan['message'], 5);
             redirect($bckurl);
        } else {
            $id = $this->Welcome_model->update_data("users", $customer, array("id" => $customer_id));
            if ($id) {
                $this->session->set_flashdata('message_success', "Your plan is updated successfuly", 5);
               redirect($bckurl);
            }
        }
    }
    
    public function getclientprofileinfo($id,$stripe_key="",$main_user="") {
        $clientdata = $this->Admin_model->getuser_data($id); 
        $clientdata[0]['profile_picture'] = $this->customfunctions->getprofileimageurl($clientdata[0]['profile_picture']);
        $clntsubscriptions = $this->Clients_model->getsubsbaseduserbyplanid($clientdata[0]['plan_name'],"",$main_user);
        $clientdata[0]['plan_data'] = $clntsubscriptions[0];
        $clientdata[0]['apikey'] = $this->Stripe->setapikey($stripe_key);
        
        if($clientdata[0]['overwrite'] != 1){
            $clientdata[0]['total_inprogress_req'] = $clntsubscriptions[0]['global_inprogress_request'];
            $clientdata[0]['total_active_req'] = $clntsubscriptions[0]['global_active_request'];
            $clientdata[0]['total_requests'] = $clntsubscriptions[0]['global_inprogress_request'];
            $clientdata[0]['file_management'] = $clntsubscriptions[0]['file_management'];
            $clientdata[0]['file_sharing'] = $clntsubscriptions[0]['file_sharing'];
            $clientdata[0]['brand_profile_count'] = $clntsubscriptions[0]['brand_profile_count'];
        }
        if($clientdata[0]['parent_id'] != $main_user){
            redirect(base_url() . "customer/client_management");
        }
        if ($clientdata[0]['customer_id'] != "" || $clientdata[0]['customer_id'] != null) {
            $card_details = $this->get_customer_detail_from_strip($clientdata[0]['current_plan'],$clientdata[0]['customer_id']);
        } else {
            $card_details = [];
        }
        $clientdata[0]['card'] = $card_details;
        if ($clntsubscriptions[0]['plan_type_name'] == "one_time") {
            $all_request = $this->Request_model->getall_request_for_all_status($id,"","","created_by");
            $request['added_request'] = sizeof($all_request);
            $pending_req = $data[0]['total_requests'] - $request['added_request'];
            $request['pending_req'] = ($pending_req > 0) ? $pending_req : 0;
        } else {
            $request = [];
        }
        $clientdata[0]['request'] = $request;
        $shareduser = $clntsubscriptions[0]['shared_user_type'];
        $clientdata[0]['usertype'] = $this->plan_usertype($shareduser);
        return $clientdata;
    }
    
    public function plan_usertype($shareduser) {
        if($shareduser == 1){
            $type = "Shared";
        }elseif($shareduser == 0){
            $type = "Dedicated";
        }else{
            $type = "Request Based";
        }
        return $type;
    }
    
    public function upgradeclient_plan() {
        $result = array();
        
        if (!empty($_POST)) {
            $subscription_plan_id = isset($_POST['plan_name'])?$_POST['plan_name']:"";
            $plan_price = isset($_POST['plan_price'])?$_POST['plan_price']:"";
            $final_plan_price = isset($_POST['final_plan_price'])?$_POST['final_plan_price']:"";
            $user_id = isset($_POST['customer_id'])?$_POST['customer_id']:"";
            $discount = isset($_POST['discount'])?$_POST['discount']:"";
            $is_coupon_valid = isset($_POST['couponinserted_ornot'])?$_POST['couponinserted_ornot']:'';
            $state = isset($_POST['state'])? $_POST['state'] : '';
            $expirdate = isset($_POST['expir_date']) ? $_POST['expir_date'] : '';
            $card_number = isset($_POST['card_number'])?trim($_POST['card_number']):"";
            $cvc = isset($_POST['cvc']) ? $_POST['cvc'] : '';
            $inprogress = isset($_POST['in_progress_request']) ? $_POST['in_progress_request'] :1;
            
            $main_user = $this->load->get_var('main_user');
            $useroptions_data = $this->Request_model->getAgencyuserinfo($main_user,'');
            $this->Stripe->setapikey($useroptions_data[0]['stripe_api_key']);
            $user_data = $this->Admin_model->getuser_data($user_id);
            $inprogress_request = $inprogress;
            
            $getsubscriptionplan = $this->Clients_model->getsubsbaseduserbyplanid($subscription_plan_id,"",$main_user);
                if ($user_data[0]['overwrite'] == 1 && isset($user_data[0]['turn_around_days'])) {
                    $turn_around_days = $user_data[0]['turn_around_days'];
                } else {
                    $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
                }
            $customer = array();
            if (!empty($getsubscriptionplan) && $getsubscriptionplan[0]['payment_mode'] != 0) {
                if($is_coupon_valid == 0){
                    $stripe_customer = $this->Stripe->createnewCustomer($user_data[0]['email']);
                }else{
                    $stripe_customer = $this->Stripe->createnewCustomer($user_data[0]['email'], trim($discount));
                }
                if (!$stripe_customer['status']) {
                    $result['error'] = "Error occurred while upgrading plan!";
                    $result['status'] = 0;
                    echo json_encode($result);exit;
                }

                $expir_date = explode("/", $expirdate);
                $customer_id = $stripe_customer['data']['customer_id'];
                $expiry_month = $expir_date[0];
                $expiry_year = $expir_date[1];
                $stripe_card = $this->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);
                if (!$stripe_card['status']):
                    $result['error'] = $stripe_card['message'];
                    $result['status'] = 0;
                    echo json_encode($result);exit;
                endif;

                if($getsubscriptionplan[0]['plan_type_name'] == "one_time"){
                    $invoice_created = $this->Stripe->create_invoice($customer_id,0,$plan_price);
                    if($invoice_created['status'] != true){
                        $result['message'] = $invoice_created['message'];
                        $result['status'] = 0;
                    }else{
                        $customer['total_requests'] = $inprogress_request*$getsubscriptionplan[0]['global_inprogress_request'];
                        $customer['total_inprogress_req'] = 0;    
                        $customer['total_active_req'] = 0;
                        $customer['payment_status'] = 1;
                        $customer['billing_start_date'] = date('Y-m-d H:i:s');
                    }
                }else{
                $stripe_subscription = '';
                if ($subscription_plan_id) {
                    $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($stripe_customer['data']['customer_id'], $subscription_plan_id, $inprogress_request, 0);
                    if (!$stripe_subscription['status']) {
                        $result['error'] = $stripe_subscription['message'];
                        $result['status'] = 0;
                        echo json_encode($result);exit;
                    }else{
                    $customer['billing_start_date'] = date('Y-m-d H:i:s', $stripe_subscription['data']['period_start']);
                    $customer['billing_end_date'] = date('Y-m-d H:i:s', $stripe_subscription['data']['period_end']);
                    $customer['total_inprogress_req'] = $inprogress_request;
                    $customer['total_active_req'] = $getsubscriptionplan[0]['global_active_request'];
                    $customer['total_requests'] = 0;
                    $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
                    $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);   
                    $customer['plan_interval'] = $plandetails['interval'];
                    $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
                    $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
                    }
                }
                }
                $customer['customer_id'] = $stripe_customer['data']['customer_id'];
            }else{
                $customer['customer_id'] = "";
                $customer = $this->customfunctions->offlinepayment_process($getsubscriptionplan, $subscription_plan_id, $customer);
            }
            $customer['plan_name'] = $subscription_plan_id;
            $customer['requests_type'] = $getsubscriptionplan[0]['plan_type_name'];
            $customer['is_active'] = 1;
            $customer['is_trail'] = 0;
            $customer['without_pay_user'] = 0;
            $customer['state'] = $state;
            $customer['plan_amount'] = $final_plan_price;
            $customer['display_plan_name'] = $getsubscriptionplan[0]['plan_name'];
            $customer['modified'] = date("Y:m:d H:i:s");
            
            $id = $this->Welcome_model->update_data("users", $customer, array("id" => $_POST['customer_id']));
            if ($id) {
                $this->Stripe->setapikey(API_KEY);
                $result['success'] = "Account upgraded successfully!";
                $result['status'] = 1;
                echo json_encode($result);exit;
            } else {
                $result['error'] = "Error occurred while upgrading account, please try again or contact us!";
                $result['status'] = 0;
                echo json_encode($result);exit;
            }
        }
        
    }
    
    public function client_dashboard($main_user,$userscount,$stripe_key){
        $report = array();
        $today = date('m/d/Y');
        $ts = strtotime($today);
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
        $start_date = date('Y-m-d', $start);
        $end_date = date('Y-m-d', strtotime('next saturday', $start));
        
        $report['user_count'] = $userscount;
        $report['requests_count'] = $this->Clients_model->count_requests_basedon_customer($main_user);
        $report['brand_count'] = $this->Request_model->get_brand_profile_by_user_id($main_user,"","1");
        if($stripe_key != ""){
         //   echo $stripe_key."<br>";
        $charges = $this->Stripe->getallcharge($stripe_key);
        //echo "<pre>";print_R($charges);
        foreach ($charges['charge'] as $key => $charge){
            $report['amount'][$key] = $charge['amount']/100;
        }
            $report['earn'] = array_sum($report['amount']);
        }else{
            $report['earn'] = 0;
        }
        $report['recentuser'] = $this->Request_model->getAllsubUsers($main_user,"client","","","",$start_date);
        foreach ($report['recentuser'] as $userskey => $usersvalue) {
           $report['recentuser'][$userskey]['profile_picture'] = $this->customfunctions->getprofileimageurl($usersvalue['profile_picture']); 
        }
        $report['newuser'] = $this->Request_model->getAllsubUsers($main_user,"client","",""," DAYNAME(created) as Day,count(created) as Count",$start_date,$end_date,"chart");
        foreach ($report['newuser'] as $userskey => $usersvalue) {
          foreach ($usersvalue as $key => $value) {
               $a = [];
               $a['v'] = $value;
               $report['newusers'][$userskey]['c'][] = $a;
           }
        }
        //echo "<pre>";print_r($report['newuser']);
        return $report;
    }
    
    public function getweeklyusers(){
        $main_user = $this->load->get_var('main_user');
        $start_date = isset($_POST['start'])?$_POST['start']:"";
        $end_date = isset($_POST['end'])?$_POST['end']:"";
        $newusers = array();
        $newuser = $this->Request_model->getAllsubUsers($main_user,"client",1,""," DAYNAME(created) as Day,count(created) as Count",$start_date,$end_date,"chart");
        foreach ($newuser as $userskey => $usersvalue) {
          foreach ($usersvalue as $value) {
               $a = [];
               $a['v'] = $value;
               $newusers[$userskey]['c'][] = $a;
           }
        }
        echo json_encode($newusers);exit;
    }
    
    public function save_addition_setting(){
        $main_user = $this->load->get_var('main_user');
        $no_of_assign_user = isset($_POST['no_of_assign_user'])?$_POST['no_of_assign_user']:"";
        $reserve_count = isset($_POST['reserve_count'])?$_POST['reserve_count']:"";
        $default_active = (isset($_POST['default_active']) && $_POST['default_active'] == "on")?1:0;
        $online_payment = (isset($_POST['online_payment']) && $_POST['online_payment'] == "on")?1:0;
        $show_signup = (isset($_POST['show_signup']) && $_POST['show_signup'] == "on")?1:0;
        $shared_designer = (isset($_POST['shared_designer']) && $_POST['shared_designer'] == "on")?1:0;
        $show_billing = (isset($_POST['show_billing']) && $_POST['show_billing'] == "on")?1:0;
        $useroptions_data = $this->Request_model->getAgencyuserinfo($main_user);
        $info = array("no_of_assign_user" => $no_of_assign_user,
            "reserve_count" => $reserve_count,
            "default_active" => $default_active,
            "online_payment" => $online_payment,
            "show_signup" => $show_signup,
            "shared_designer" => $shared_designer,
            "show_billing" => $show_billing,
             "modified" => date("Y:m:d H:i:s"));
        if(empty($useroptions_data)){
            $info["user_id"] = $main_user;
            $info["created"] = date("Y:m:d H:i:s");
           $id = $this->Welcome_model->insert_data("agency_user_options", $info); 
        }else{
           $id = $this->Welcome_model->update_data("agency_user_options", $info,array("user_id" => $main_user));
        }
        if ($id) {
            if($shared_designer == 0){
                $ids = $this->Clients_model->getsharedsubscription($main_user);
                $subscription = array("is_active" => 0);
                $this->Clients_model->updateclientusers($subscription,$ids['id']);
            }
            $this->session->set_flashdata('message_success', 'Successfully Updated.!', 5);
        } else {
            $this->session->set_flashdata('message_error', 'Not Update. Please check and try again.!', 5);
        }
        redirect(base_url() . "customer/client_management/#addi_setting");
    }
    
    public function enable_disable_subscription(){
        $status = isset($_POST['status'])?$_POST['status']:'';
        $subsid = isset($_POST['data_id'])?$_POST['data_id']:'';
        if($status == 1){
            $msg = "Your plan is active";
        }else{
            $msg = "Your plan is deactive";
        }
        if($subsid != ""){
         $this->Welcome_model->update_data("agency_subscription_plan", array("is_active" => $status),array("id" => $subsid));
         $this->session->set_flashdata('message_success', $msg, 5);
         //redirect(base_url() . "customer/client_management#subscription_management");
        }
    }
    
    public function get_stripe_account_info($apikey,$account_id){
        $account_info = $this->Stripe->stripe_account_info($apikey,$account_id);
        return $account_info;
    }
    
    public function activeclientbilling(){
        $main_user = $this->load->get_var('main_user');
        $client_id = isset($_POST['billing_client_id'])?$_POST['billing_client_id']:'';
        $stripe_api_key = isset($_POST['stripe_key'])?$_POST['stripe_key']:'';
        $plan = isset($_POST['selct_clintsubs'])?$_POST['selct_clintsubs']:'';
        $active_plan_type = isset($_POST['active_plan_type'])?$_POST['active_plan_type']:'';
        $start_date = isset($_POST['start_subs_picker'])?$_POST['start_subs_picker']:'';
        $startdate = date("Y:m:d H:i:s", strtotime($start_date));
         $user_data = $this->Admin_model->getuser_data($client_id);
        if($plan != ""){
            $subscriptions = $this->Clients_model->getsubsbaseduserbyplanid($plan,"plan_price,plan_name,plan_type_name,global_inprogress_request",$main_user);
            if($client_id != ""){
                $customer = array("plan_name" => $plan,
                "plan_amount" => $subscriptions[0]['plan_price'],
                "display_plan_name" => $subscriptions[0]['plan_name'],
                "requests_type" => $subscriptions[0]['plan_type_name'],
                "billing_start_date" => $startdate,
                "is_active" => 1,
                "modified" => date("Y:m:d H:i:s"));
//                 $user_info = $this->Admin_model->getuser_data($client_id);
                 $user_info = array("user_id" => $client_id,
                        "modified" => date("Y:m:d H:i:s"));
                 
                
                if($active_plan_type == "one_time"){
                    $customer['total_requests'] = $subscriptions[0]['global_inprogress_request'];
                    $customer['total_active_req'] = 0;
                    $customer['total_inprogress_req'] = 0;
                    $customer['payment_status'] = 1;
                    $user_info["bypass_payment"] = 0;
                }else{
                   
                    $this->Stripe->setapikey($stripe_api_key); 
                    $stripe_customer = $this->Stripe->createnewCustomer($user_data[0]['email']); 
                    if (!$stripe_customer['status']) {
                       $output['msg'] = $stripe_customer['message'];
                       $output['status'] = 0;
                       $this->session->set_flashdata('message_success', $output['msg'], 5);
                       redirect(base_url() . "customer/client_management#client_management");
                    }
                    $user_info["bypass_payment"] = 1;
                    $customer["customer_id"] = $stripe_customer['data']['customer_id'];
                }
                

                $update = $this->Welcome_model->update_data("users", $customer, array("id" => $client_id));

                /** extra user info **/
                if($update){
                    $user_info_data = $this->Admin_model->getextrauser_data($client_id);  
                    if(empty($user_info_data)){
                        $user_info["created"] = date("Y:m:d H:i:s"); 
                       $this->Welcome_model->insert_data("users_info", $user_info);
                    }else{
                       $this->Welcome_model->update_data("users_info", $user_info, array("user_id" => $client_id));
                    }
                    $this->session->set_flashdata('message_success', "Activated billing account", 5);
                   redirect(base_url() . "customer/client_management#client_management");
                }
                /** end extra user info **/
            }
        }else{
                $this->session->set_flashdata('message_success', "Please select subscription", 5);
                redirect(base_url() . "customer/client_management#client_management");
        }
    }
    
    public function reset_client_password(){
        if(!empty($_POST)){
            $main_user = $this->load->get_var('main_user');
            $client_id = isset($_POST['reset_client'])?$_POST['reset_client']:"";
            $pass = isset($_POST['password'])?$_POST['password']:"";
            $email = isset($_POST['reset_email'])?$_POST['reset_email']:"";
            if(isset($_POST['genrate_password']) && $_POST['genrate_password'] == "on"){
                $genrate_password = 1;
            }
            if($genrate_password != 1 && $pass != ""){
                $password = $pass;
            }else{
                $password = $this->myfunctions->random_password(); 
            }
            $customer = array("new_password" => md5($password));
            $update = $this->Welcome_model->update_data("users", $customer, array('id' => $client_id));
            if($update){
                $arraysub = array('CUSTOMER_NAME' => $fname[0]['first_name'],
                        'PASSWORD' => $password);
                $this->myfunctions->send_email_to_users_by_template($main_user,'reset_client_password_email',$arraysub,$email);
              $this->session->set_flashdata('message_success', 'Password updated successfully! An email is sent to the address with password.', 5);
              redirect(base_url() . "customer/client_management/".$client_id."#client_management");
            }
        }
        
    }

}