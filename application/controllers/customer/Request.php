    <?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Request extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/indexd
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('Myfunctions');
        $this->load->helper('form');
        $this->load->library('s3_upload');
        $this->load->helper('url');
        $this->load->model('Request_model');
        $this->load->model('Category_model');
        $this->load->model('Welcome_model');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Clients_model');
        $this->load->model('Stripe');
        $this->load->library('zip');
        $this->load->helper('download');

        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }

//    public function checkloginuser() {
//        if (!$this->session->userdata('user_id')) {
//            if(isset($_COOKIE['is_remember']) && $_COOKIE['is_remember'] == 1){
//                $encid = isset($_COOKIE['rememberID'])? $_COOKIE['rememberID']:'';
//                $id = $this->Request_model->my_decrypt($encid,BASE64KEY);
//                $pwd = (isset($user[0]['password']) && $user[0]['password'] != '') ? $user[0]['password']: $user[0]['new_password'];
//                $user = $this->Request_model->getuserbyid($id);
//                $this->session->set_userdata('email', $user[0]['email']);
//                $this->session->set_userdata('password', $pwd);
//                $this->session->set_userdata('role', $user[0]['role']);
//                $this->session->set_userdata('timezone', $user[0]['timezone']);
//                $this->session->set_userdata('first_name', $user[0]['first_name']);
//                $this->session->set_userdata('last_name', $user[0]['last_name']);
//                $this->session->set_userdata('user_id', $user[0]['id']);
//                $this->session->set_userdata('tour', $user[0]['tour']);
//                $this->session->set_userdata('qa_id', $user[0]['qa_id']);
////                print_R($_SESSION);exit;
//                redirect(current_url());
//            }else{
//                $protocol = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
//                $base_url = $protocol . "://" . $_SERVER['HTTP_HOST'];
//                $complete_url = $_SERVER["REQUEST_URI"];
//                $requrl = ltrim($complete_url, '/');
//                if($_SERVER['HTTP_HOST'] == DOMAIN_NAME){ 
//                    $url = base_url() . 'login';
//                }else{
//                    $url = base_url();
//                }
//                redirect($url.'?url=' . $requrl);
//            }
//        }
//        if ($this->session->userdata('role') != "customer") {
//            redirect(base_url());
//        }
//    }

    public function index() {
        $this->myfunctions->checkloginuser("customer");
        $created_user = $this->load->get_var('main_user');
        redirect(base_url() . "customer/request/design_request");
        $today_customer = $this->Admin_model->get_today_customer();
        $all_customer = $this->Admin_model->get_total_customer();
        $today_requests = $this->Admin_model->get_today_request();
        $total_requests = $this->Admin_model->get_all_requested_designs(array("active", "assign", "checkforapprove"), $created_user);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/index', array('today_customer' => $today_customer, 'all_customer' => $all_customer, 'today_requests' => $today_requests, 'total_requests' => $total_requests));
        $this->load->view('customer/footer_customer');
    }
    

    public function add_new_request(){ 
//        echo "<pre>";print_r($_POST);exit;
        $allcategories = $this->Category_model->get_categories(0,1);
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        $request_for_all_status = $this->Request_model->getall_request_for_trial_user($created_user);
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $profile_data = $this->Admin_model->getuser_data($login_user_id);
        if($canbrand_profile_access != 0){
        $user_profile_brands = $this->Request_model->get_brand_profile_by_user_id($created_user);
        }elseif($canbrand_profile_access == 0 && $profile_data[0]['user_flag'] == 'client'){
           $user_profile_brands = $this->Request_model->get_brand_profile_by_user_id($login_user_id,'created_by');
        }else{
        $user_profile_brand = $this->Request_model->get_user_brand_profile_by_user_id($login_user_id);
            //echo "<pre>";print_r($user_profile_brand);
        if(sizeof($user_profile_brand) > 0){
            foreach ($user_profile_brand as $user_brands_info) {
              $brand_id[] =  $user_brands_info['brand_id']; 
            }  
           // echo "<pre>";print_r($brand_id);
            $user_profile_brands = $this->Request_model->get_brandprofile_by_id('',$brand_id);
        }
        }
        $canuseradd = $this->myfunctions->isUserPermission('add_requests');
        $parent_id = $this->load->get_var('parent_user_id');
        $created_by = ($parent_id != 0) ? $login_user_id: 0;
	 
        /******************************************/
        
        $reqid = isset($_GET['reqid'])?$_GET['reqid']:"";
        $duplid = isset($_GET['did'])?$_GET['did']:"";
        $trail = isset($profile_data[0]['is_trail'])?$profile_data[0]['is_trail']:'';
        
		/*****************trail and 99 user add request more than limit*************************/
        $trail_user = $this->Request_model->getuserbyid($created_user);
        $request_for_all_status_acc_billing_cycle = $this->Request_model->getall_request_for_all_status($created_user,$trail_user[0]['billing_start_date'],$trail_user[0]['billing_end_date']);
       // echo "<pre>";print_r($request_for_all_status_acc_billing_cycle);
        $useraddrequest = $this->myfunctions->canUserAddNewRequest($trail_user,$request_for_all_status_acc_billing_cycle,$request_for_all_status);
        if ($useraddrequest == 0 || $useraddrequest == 1) {
            if ($reqid && $duplid == '') {
                    //edit case will not redirect the user in any case.
            } else {
                    $this->session->set_flashdata('message_error', 'You have already added allowed requests count in current billing cycle.', 5);
                    redirect(base_url() . "customer/request/design_request");
            }
        }
        /************** start add request **********************/
        if($reqid == '' && $duplid == ''){
            $allqust = $this->Request_model->getallquestions('1','0');
            $subcat_id = isset($_POST['subcategory_id']) ? $_POST['subcategory_id'] : '';
            $subcat_data = $this->Category_model->get_category_byID($subcat_id);
            $bucketType = $subcat_data[0]['bucket_type'];
            $samples = $this->Category_model->getSampleMaterialsbycat_subcat(1,0);
            $getminMax = $this->Category_model->getSampleMaterialsbycat_subcat(1,0,true);
            if ($profile_data[0]['is_trail'] == 1 && sizeof($request_for_all_status) >= 1) {
                    redirect(base_url() . "customer/request/design_request");
            }
            //die("add");
            $this->myfunctions->checkloginuser("customer");
            $fileType="";
            if(!empty($_POST['color_pref'])){
                if(in_array('Let Designer Choose',$_POST['color_pref'])){
                     $plate = 'Let Designer Choose';
                     $color_values = '';
                     
                }else{
                     $plate = isset($_POST['color_pref'][1]) ? $_POST['color_pref'][1] : '';
                     $color_values = implode(",", $_POST['color_values']);
                }
            }
            if (isset($_POST['filetype'])) {
                $fileType = implode(",", $_POST['filetype']);
            }
            $data = array("customer_id" => $created_user,
            "category" => isset($_POST['category'])?$_POST['category']:'',
            "logo_brand" => isset($_POST['logo-brand'])?$_POST['logo-brand']:'',
            "brand_id" => isset($_POST['brand_name']) ? $_POST['brand_name'] : '',
            "category_id" => isset($_POST['category_id'])?$_POST['category_id']:'',
            "subcategory_id" => isset($_POST['subcategory_id'])?$_POST['subcategory_id']:'',
            "category_bucket" => $bucketType,
            "title" => isset($_POST['title'])?$_POST['title']:'',
            "business_industry" => isset($_POST['target_market'])?$_POST['target_market']:'',
            "created" => date("Y-m-d H:i:s"),
            "status" => "draft",
            "designer_id" => 0,
            "priority" => 0,
            "deliverables" => $fileType,
            "created_by" => $created_by,
            "last_modified_by" => $_SESSION['user_id'],
            "color_pref" => $plate,
            "color_values" => $color_values 
            );
            
            /*******Save & Exit ***************/
            if(isset($_POST['request_exit'])){
                $success = $this->Welcome_model->insert_data("requests", $data);
                if($success){
//                    echo "<pre/>";print_R($_POST['sample_subcat']);exit;
                    $smplesave = array();
                    if(!empty($_POST['sample_subcat'])){
                    foreach($_POST['sample_subcat'] as $skey => $sval){
                        $smplesave['request_id'] = $success;
                        $smplesave['material_id'] = $sval;
                        $smplesave['created'] = date("Y-m-d H:i:s");
                        $savedsample = $this->Welcome_model->insert_data("request_sample_data", $smplesave);
                    }
                }
                }
                if (!empty($_POST['quest'])) {
                    foreach ($_POST['quest'] as $kqust => $vqust) {
                        if (is_array($vqust)) {
                            $vqust = implode(',', $vqust);
                        } else {
                            $vqust = $vqust;
                        }
                        $catquest['question_id'] = $kqust;
                        $catquest['request_id'] = $success;
                        $catquest['customer_id'] = $created_user;
                        $catquest['answer'] = $vqust;
                        $catquest['created'] = date("Y-m-d H:i:s");
                        $catquest['modified'] = date("Y-m-d H:i:s");
                        $meta = $this->Welcome_model->insert_data("requests_meta", $catquest);
                    }
                }
                $id = $success;
                $this->addFileRequest($id);
                $this->myfunctions->capture_project_activity($success,'','','project_created','add_new_request',$login_user_id,1);
                $this->session->set_flashdata('message_success', 'We\'re now reviewing your request and your designer will get something back to you soon', 5);
                redirect(base_url()."customer/request/design_request");
            }
             /*******Final Submit ***************/
            if(isset($_POST['request_submit'])){
                $success = $this->Welcome_model->insert_data("requests", $data);
//                echo $success;
                /*********question*******/
                if (!empty($_POST['quest'])) {
                    foreach ($_POST['quest'] as $kqust => $vqust) {
                        if (is_array($vqust)) {
                            $vqust = implode(',', $vqust);
                        } else {
                            $vqust = $vqust;
                        }
                    $catquest['question_id'] = $kqust;
                    $catquest['request_id'] = $success;
                    $catquest['customer_id'] = $created_user;
                    $catquest['answer'] = $vqust;
                    $catquest['created'] = date("Y-m-d H:i:s");
                    $catquest['modified'] = date("Y-m-d H:i:s");
//                    echo "<pre/>";print_R($catquest);exit;
                    $meta = $this->Welcome_model->insert_data("requests_meta", $catquest);
                    }
                }
                /****save samples *********/
                if($success){
                    $smplesave = array();
//                    echo "<pre/>";print_R($_POST);exit;
                    if(!empty($_POST['sample_subcat'])){
                    foreach($_POST['sample_subcat'] as $skey => $sval){
                        $smplesave['request_id'] = $success;
                        $smplesave['material_id'] = $sval;
                        $smplesave['created'] = date("Y-m-d H:i:s");
                        $savedsample = $this->Welcome_model->insert_data("request_sample_data", $smplesave);
                    }
                }
                }
               // echo $success;exit;
                $id = $success;
                $this->addFileRequest($id);
                $this->myfunctions->capture_project_activity($success,'','','project_created','add_new_request',$login_user_id,1);
                $update_data = $this->myfunctions->changerequeststatusafteradd($created_user,$id,$bucketType,$subcat_data,$profile_data);
                    $newrequestdata = $this->Request_model->get_request_by_id($id);
                    if($trail == 1){
                    $this->load->library('drip');
                    $this->load->library('datasett');
                    $this->load->library('response');
                    $Drip = new Drip('5791e6d8913a121374b5e55904342eb3','1975472');
                    $data = new Datasett('tags', [
                        'email' => $profile_data[0]['email'],
                        'tag' => "Submitted Free Design Request",
                    ]);
                    $Drip->post('tags', $data);
                    redirect(base_url()."customer/request/design_request");
                    }
                    if ($update_data) {
                        $notifications = $this->Request_model->get_notifications($login_user_id);
                        $notification_number = $this->Request_model->get_notifications_number($login_user_id);
                        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
                        $this->load->view('customer/footer_customer');
                        $this->session->set_flashdata('message_success', 'We\'re now reviewing your request and your designer will get something back to you soon', 5);
                        redirect(base_url()."customer/request/design_request");
                    }
            }
            /************** end add request **********************/
        }
        /************** start edit request **********************/
        if ($reqid && $duplid == '') {
            $subcat_id = isset($_POST['subcategory_id']) ? $_POST['subcategory_id'] : '';
            $subcat_data = $this->Category_model->get_category_byID($subcat_id);
            $bucketType = $subcat_data[0]['bucket_type'];
            $existeddata = $this->Request_model->get_request_by_id($reqid);
            $samples = $this->Category_model->getSampleMaterialsbycat_subcat($existeddata[0]['category_id'],$existeddata[0]['subcategory_id']);
            $getminMax = $this->Category_model->getSampleMaterialsbycat_subcat($existeddata[0]['category_id'],$existeddata[0]['subcategory_id'],true);
            $selectedsamples = $this->Request_model->getSamplematerials($reqid);
//            echo "<pre/>";print_r($existeddata);
            if(!empty($selectedsamples)){
                $selectdmaterials = array();
                foreach($selectedsamples as $smkey => $smval){
                    $selectdmaterials[]  = $smval['material_id'];
                }
//                echo "<pre/>";print_R($selectedsamples);exit;
            }
            $allqust = $this->Request_model->getallquestions($existeddata[0]['category_id'], $existeddata[0]['subcategory_id']);
            foreach ($allqust as $ak => $av) {
                $getquestans = $this->Category_model->get_quest_Ans($reqid, $av['id']);
                $allqust[$ak]['answer'] = $getquestans[0]['answer'];
                $allqust[$ak]['description'] = $getquestans[0]['description'];
                $allqust[$ak]['design_dimension'] = $getquestans[0]['design_dimension'];
            }
            $this->myfunctions->checkloginuser("customer");
            $fileType="";
            if(in_array('Let Designer Choose',$_POST['color_pref'])){
                     $plate = 'Let Designer Choose';
                     $color_values = '';
                }else{
                     $plate = isset($_POST['color_pref'][1]) ? $_POST['color_pref'][1] : '';
                     $color_values = implode(",", $_POST['color_values']);
            }
            if (isset($_POST['filetype'])) {
                $fileType = implode(",", $_POST['filetype']);
            }
            /*************save & exit in edit case *****************/
            if(isset($_POST['request_exit'])){
                $data = array("customer_id" => $created_user,
                    "category" => isset($_POST['category'])?$_POST['category']:"",
                    "logo_brand" => isset($_POST['logo-brand'])?$_POST['logo-brand']:'',
                    "brand_id" => isset($_POST['brand_name']) ? $_POST['brand_name'] : '',
                    "category_id" => isset($_POST['category_id'])?$_POST['category_id']:'',
                    "subcategory_id" => isset($_POST['subcategory_id'])?$_POST['subcategory_id']:'',
                    "category_bucket" => $bucketType,
                    "title" => isset($_POST['title'])?$_POST['title']:'',
                    "business_industry" => isset($_POST['target_market'])?$_POST['target_market']:'',
                    "deliverables" => $fileType,
                    "last_modified_by" => $_SESSION['user_id'],
                     "color_pref" => $plate,
                    "color_values" => $color_values
                );
            $success = $this->Welcome_model->update_data("requests", $data, array("id" => $reqid));
            $meta_ans = $this->Category_model->getMetasbyReqID($reqid);
            foreach ($meta_ans as $mk => $mv) {
                $datadeleted['is_deleted'] = 1;
                // $this->Welcome_model->update_data("requests_meta", $datadeleted, array("request_id" => $reqid));
                $this->Admin_model->delete_data('requests_meta', $mv['id']);
            }
            if (!empty($_POST['quest'])) {
                foreach ($_POST['quest'] as $kqust => $vqust) {
                    if (is_array($vqust)) {
                        $vqust = implode(',', $vqust);
                    } else {
                        $vqust = $vqust;
                    }
                    $catquest['question_id'] = $kqust;
                    $catquest['request_id'] = $reqid;
                    $catquest['customer_id'] = $created_user;
                    $catquest['answer'] = $vqust;
                    $catquest['modified'] = date("Y-m-d H:i:s");
                    $isdata = $this->Category_model->getMetasbyReqID($reqid, $kqust);
                    if (!empty($isdata)) {
                        $meta = $this->Welcome_model->update_data("requests_meta", $catquest, array("question_id" => $kqust, "request_id" => $reqid));
                    } else {
                        $meta = $this->Welcome_model->insert_data("requests_meta", $catquest);
                    }
                }
            }
            $selectedsamples = $this->Request_model->getSamplematerials($reqid);
            foreach ($selectedsamples as $slk => $slv) {
                $this->Admin_model->delete_data('request_sample_data', $slv['id']);
            }
            $smplesave = array();
                if(!empty($_POST['sample_subcat'])){
                foreach($_POST['sample_subcat'] as $skey => $sval){
                    $smplesave['request_id'] = $reqid;
                    $smplesave['material_id'] = $sval;
                    $smplesave['created'] = date("Y-m-d H:i:s");
                    $savedsample = $this->Welcome_model->insert_data("request_sample_data", $smplesave);
                }
            }
            $id = $reqid;
            $this->addFileRequest($id);
            $this->myfunctions->capture_project_activity($reqid,'','','edit_project','add_new_request',$login_user_id,1);
            $this->session->set_flashdata('message_success', 'We\'re now reviewing your request and your designer will get something back to you soon', 5);
            redirect(base_url()."customer/request/design_request");
            }
            /*************save in edit case *****************/
            if(isset($_POST['request_submit'])){
                // echo "<pre/>";print_r($existeddata);
                //echo "<pre/>";print_r($_POST);exit;
                $data = array("customer_id" => $created_user,
                    "category" => isset($_POST['category'])?$_POST['category']:'',
                    "logo_brand" => isset($_POST['logo-brand'])?$_POST['logo-brand']:'',
                    "brand_id" => isset($_POST['brand_name']) ? $_POST['brand_name'] : '',
                    "category_id" => isset($_POST['category_id'])?$_POST['category_id']:'',
                    "subcategory_id" => isset($_POST['subcategory_id'])?$_POST['subcategory_id']:'',
                     "category_bucket" => $bucketType,
                    "title" => isset($_POST['title'])?$_POST['title']:'',
                    "business_industry" => isset($_POST['target_market'])?$_POST['target_market']:'',
                    "deliverables" => $fileType,
                    "last_modified_by" => $_SESSION['user_id'],
                    "color_pref" => $plate,
                    "color_values" => $color_values
                );
                $meta_ans = $this->Category_model->getMetasbyReqID($reqid);
                foreach ($meta_ans as $mk => $mv) {
                    $this->Admin_model->delete_data('requests_meta', $mv['id']);
                }
                if (!empty($_POST['quest'])) {
                    foreach ($_POST['quest'] as $kqust => $vqust) {
                        if (is_array($vqust)) {
                            $vqust = implode(',', $vqust);
                        } else {
                            $vqust = $vqust;
                        }
                        $catquest['question_id'] = $kqust;
                        $catquest['request_id'] = $reqid;
                        $catquest['customer_id'] = $created_user;
                        $catquest['is_deleted'] = '0';
                        $catquest['answer'] = $vqust;
                        $catquest['modified'] = date("Y-m-d H:i:s");
                        $isdata = $this->Category_model->getMetasbyReqID($reqid, $kqust);
                        if (!empty($isdata)) {
                            $meta = $this->Welcome_model->update_data("requests_meta", $catquest, array("question_id" => $kqust, "request_id" => $reqid));
                        } else {
                            $meta = $this->Welcome_model->insert_data("requests_meta", $catquest);
                        }
                    }
                }
                $success = $this->Welcome_model->update_data("requests", $data, array("id" => $reqid));
                if($success){
                $selectedsamples = $this->Request_model->getSamplematerials($reqid);
                foreach ($selectedsamples as $slk => $slv) {
                    $this->Admin_model->delete_data('request_sample_data', $slv['id']);
                }
                $smplesave = array();
                    if(!empty($_POST['sample_subcat'])){
                    foreach($_POST['sample_subcat'] as $skey => $sval){
                        $smplesave['request_id'] = $reqid;
                        $smplesave['material_id'] = $sval;
                        $smplesave['created'] = date("Y-m-d H:i:s");
                        $savedsample = $this->Welcome_model->insert_data("request_sample_data", $smplesave);
                    }
                }
                }
                $id = $reqid;
                $this->addFileRequest($id);
                $this->myfunctions->capture_project_activity($reqid,'','','edit_project','add_new_request',$login_user_id,1);
                if($existeddata[0]['status'] == 'draft'){
                $update_data = $this->myfunctions->changerequeststatusafteradd($created_user,$id,$bucketType,$subcat_data,$profile_data);
                    $newrequestdata = $this->Request_model->get_request_by_id($id);
                    if ($update_data) {
                        $notifications = $this->Request_model->get_notifications($login_user_id);
                        $notification_number = $this->Request_model->get_notifications_number($login_user_id);
                        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
                        $this->load->view('customer/footer_customer');
                        $this->session->set_flashdata('message_success', 'We\'re now reviewing your request and your designer will get something back to you soon', 5);
                        redirect(base_url()."customer/request/design_request");
                    }
                }
                if($id){
                $this->session->set_flashdata('message_success', 'We\'re now reviewing your request and your designer will get something back to you soon', 5);
                redirect(base_url()."customer/request/design_request");
                }
            }
        }
        /************** end edit request **********************/
        /************** start duplicate request **********************/
        if($duplid && $reqid == ''){
            $subcat_id = isset($_POST['subcategory_id']) ? $_POST['subcategory_id'] : '';
            $subcat_data = $this->Category_model->get_category_byID($subcat_id);
            $bucketType = $subcat_data[0]['bucket_type'];
            $duplicatedata = $this->Request_model->get_request_by_id($duplid);
            $allqust = $this->Request_model->getallquestions($duplicatedata[0]['category_id'], $duplicatedata[0]['subcategory_id']);
            $samples = $this->Category_model->getSampleMaterialsbycat_subcat($duplicatedata[0]['category_id'],$duplicatedata[0]['subcategory_id']);
            $getminMax = $this->Category_model->getSampleMaterialsbycat_subcat($duplicatedata[0]['category_id'],$duplicatedata[0]['subcategory_id'],true);
            $selectedsamples = $this->Request_model->getSamplematerials($duplid);
            if(!empty($selectedsamples)){
                $selectdmaterials = array();
                foreach($selectedsamples as $smkey => $smval){
                    $selectdmaterials[]  = $smval['material_id'];
                }
//                echo "<pre/>";print_R($selectedsamples);exit;
            }
            foreach ($allqust as $ak => $av) {
                $getquestans = $this->Category_model->get_quest_Ans($duplid, $av['id']);
                $allqust[$ak]['answer'] = $getquestans[0]['answer'];
                $allqust[$ak]['description'] = $getquestans[0]['description'];
                $allqust[$ak]['design_dimension'] = $getquestans[0]['design_dimension'];
            }
            $files = $this->Request_model->get_requestfiles_by_id($duplid,'customer');
            $this->myfunctions->checkloginuser("customer");
            $fileType="";
            if(in_array('Let Designer Choose',$_POST['color_pref'])){
                $plate = 'Let Designer Choose';
                $color_values = '';
            }else{
                $plate = isset($_POST['color_pref'][1]) ? $_POST['color_pref'][1] : '';
                $color_values = implode(",", $_POST['color_values']);
            }
            if (isset($_POST['filetype'])) {
                $fileType = implode(",", $_POST['filetype']);
            }
            $data = array("customer_id" => $created_user,
            "category" => $_POST['category'],
            "logo_brand" => $_POST['logo-brand'],
            "brand_id" => isset($_POST['brand_name']) ? $_POST['brand_name'] : '',
            "category_id" => isset($_POST['category_id'])?$_POST['category_id']:'',
            "subcategory_id" => isset($_POST['subcategory_id'])?$_POST['subcategory_id']:'',
            "category_bucket" => $bucketType,
            "title" => $_POST['title'],
            "business_industry" => isset($_POST['target_market'])?$_POST['target_market']:'',
            "created" => date("Y-m-d H:i:s"),
            "status" => "draft",
            "designer_id" => 0,
            "priority" => 0,
            "deliverables" => $fileType,
            "created_by" => $created_by,
            "last_modified_by" => $_SESSION['user_id'],
            "color_pref" => $plate,
            "color_values" => $color_values
            );
            /*******Save & Exit ***************/
            if(isset($_POST['request_exit'])){
                //echo "<pre>";print_r($_POST);exit;
                $success = $this->Welcome_model->insert_data("requests", $data);
                if (!empty($_POST['quest'])) {
                    foreach ($_POST['quest'] as $kqust => $vqust) {
                        if (is_array($vqust)) {
                            $vqust = implode(',', $vqust);
                        } else {
                            $vqust = $vqust;
                        }
                        $catquest['question_id'] = $kqust;
                        $catquest['request_id'] = $success;
                        $catquest['customer_id'] = $created_user;
                        $catquest['answer'] = $vqust;
                        $catquest['created'] = date("Y-m-d H:i:s");
                        $catquest['modified'] = date("Y-m-d H:i:s");
                        $meta = $this->Welcome_model->insert_data("requests_meta", $catquest);
                    }
                }
                if($success){
//                    echo "<pre/>";print_R($_POST['sample_subcat']);exit;
                    $smplesave = array();
                    if(!empty($_POST['sample_subcat'])){
                    foreach($_POST['sample_subcat'] as $skey => $sval){
                        $smplesave['request_id'] = $success;
                        $smplesave['material_id'] = $sval;
                        $smplesave['created'] = date("Y-m-d H:i:s");
                        $savedsample = $this->Welcome_model->insert_data("request_sample_data", $smplesave);
                    }
                }
                }
                $id = $success;
                if (!is_dir('public/uploads/requests/' . $id)) {
                $data = mkdir('./public/uploads/requests/' . $id, 0777, TRUE);
                }
                if(UPLOAD_FILE_SERVER == 'bucket'){
                //$srcpath = scandir("public/uploads/requests/".$duplid);
                $srcpath = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$duplid.'/';
                $destpath = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$id.'/';
                foreach ($files as $file) {
                $res = $this->s3_upload->copyexistfiles($srcpath,$destpath,$file);
                if($res['status'] == 1){
                $request_files_data = array("request_id" => $id,
                    "user_id" => $created_user,
                    "user_type" => "customer",
                    "file_name" => $file,
                    "created" => date("Y-m-d H:i:s")
                );
                    $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                }
                }
                }else{
                $srcpath = FCPATH.FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$duplid.'/';
                $destpath = FCPATH.FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$id.'/';
                foreach ($files as $file) {
                    copy($srcpath.$file, $destpath.$file);
                    $request_files_data = array("request_id" => $id,
                    "user_id" => $created_user,
                    "user_type" => "customer",
                    "file_name" => $file,
                    "created" => date("Y-m-d H:i:s")
                    );
                    $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                }
                }
                $this->addFileRequest($id);
                $this->myfunctions->capture_project_activity($success,'','','project_created','add_new_request',$login_user_id,1);
                $this->session->set_flashdata('message_success', 'We\'re now reviewing your request and your designer will get something back to you soon', 5);
                redirect(base_url()."customer/request/design_request");
            }
            /*******Final Submit ***************/
            if(isset($_POST['request_submit'])){
                $data = array("customer_id" => $created_user,
                    "category" => $_POST['category'],
                    "logo_brand" => $_POST['logo-brand'],
                     "brand_id" => isset($_POST['brand_name']) ? $_POST['brand_name'] : '',
                    "category_id" => isset($_POST['category_id'])?$_POST['category_id']:'',
            "subcategory_id" => isset($_POST['subcategory_id'])?$_POST['subcategory_id']:'',
            "category_bucket" => $bucketType,
                    "title" => $_POST['title'],
                    "business_industry" => isset($_POST['target_market'])?$_POST['target_market']:'',
                    "deliverables" => $fileType,
                    "color_pref" => $plate,
                     "color_values   " => $color_values,
                    "created_by" => $created_by,
                    "last_modified_by" => $_SESSION['user_id'],
                    "created" => date("Y-m-d H:i:s")
                );
               // echo "<pre>";print_r($data['category']);exit;
                $success = $this->Welcome_model->insert_data("requests", $data);
                if (!empty($_POST['quest'])) {
                    foreach ($_POST['quest'] as $kqust => $vqust) {
                        if (is_array($vqust)) {
                            $vqust = implode(',', $vqust);
                        } else {
                            $vqust = $vqust;
                        }
                        $catquest['question_id'] = $kqust;
                        $catquest['request_id'] = $success;
                        $catquest['customer_id'] = $created_user;
                        $catquest['answer'] = $vqust;
                        $catquest['created'] = date("Y-m-d H:i:s");
                        $catquest['modified'] = date("Y-m-d H:i:s");
                        $meta = $this->Welcome_model->insert_data("requests_meta", $catquest);
                    }
                }
                if($success){
//                    echo "<pre/>";print_R($_POST['sample_subcat']);exit;
                    $smplesave = array();
                    if(!empty($_POST['sample_subcat'])){
                    foreach($_POST['sample_subcat'] as $skey => $sval){
                        $smplesave['request_id'] = $success;
                        $smplesave['material_id'] = $sval;
                        $smplesave['created'] = date("Y-m-d H:i:s");
                        $savedsample = $this->Welcome_model->insert_data("request_sample_data", $smplesave);
                    }
                }
                }
                $id = $success;
               // echo $id;exit;
                if (!is_dir('public/uploads/requests/' . $id)) {
                $data_dir = mkdir('./public/uploads/requests/' . $id, 0777, TRUE);
                }
                if(UPLOAD_FILE_SERVER == 'bucket'){
                $srcpath = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$duplid.'/';
                $destpath = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$id.'/';
                foreach ($files as $file) {
                $res = $this->s3_upload->copyexistfiles($srcpath,$destpath,$file);
                if($res['status'] == 1){
                $request_files_data = array("request_id" => $id,
                    "user_id" => $created_user,
                    "user_type" => "customer",
                    "file_name" => $file,
                    "created" => date("Y-m-d H:i:s"));
                    $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                }
                }
                }else{
                $srcpath = FCPATH.FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$duplid.'/';
                $destpath = FCPATH.FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$id.'/';
                foreach ($files as $file) {
                    copy($srcpath.$file, $destpath.$file);
                    $request_files_data = array("request_id" => $id,
                    "user_id" => $created_user,
                    "user_type" => "customer",
                    "file_name" => $file,
                    "created" => date("Y-m-d H:i:s"));
                    $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                }
                }
                $this->addFileRequest($id);
                $this->myfunctions->capture_project_activity($success,'','','project_created','add_new_request',$login_user_id,1);
                $update_data = $this->myfunctions->changerequeststatusafteradd($created_user,$id,$bucketType,$subcat_data,$profile_data);
                    $newrequestdata = $this->Request_model->get_request_by_id($id);
                    if ($update_data) {
                        $notifications = $this->Request_model->get_notifications($login_user_id);
                        $notification_number = $this->Request_model->get_notifications_number($login_user_id);
                        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
                        $this->load->view('customer/footer_customer');
                        $this->session->set_flashdata('message_success', 'We\'re now reviewing your request and your designer will get something back to you soon', 5);
                        redirect(base_url()."customer/request/design_request");
                    }
            }
            }
//        $planlist = $this->Stripe->getallsubscriptionlist();
//        $AmountData = array();
//        $infinite_val = array();
//        for ($i = 0; $i < sizeof($planlist); $i++) {
//            if($planlist[$i]['id'] == AGENCY_PLAN){
//                $oldTierValue = 2;
//                foreach($planlist[$i]['tiers'] as $tierskey=>$tiersval){
//                    $pId = ($tiersval['up_to'] == '')?"infinite":$tiersval['up_to']; 
//                    if($tiersval['up_to']!= 'infinite'){
//                        for ($ii = $oldTierValue; $ii <= $tiersval['up_to']; $ii++) {
//                            //echo $ii.'<br>';
//                            $AmountData[$ii] = $tiersval;
//                        }
//                        $oldTierValue = $tiersval['up_to']+1;
//                    } else{
//                        $infinite_val = $tiersval;
//                    }
//                }
//            }
//        }
//        $subscription_list = json_encode($AmountData);
        /************** end duplicate request **********************/
        $this->load->view('customer/customer_header_1',array('profile'=>$profile_data));
         $this->load->view('customer/add_new_request',array("data" => $_POST,'existeddata' => $existeddata,'duplicatedata' => $duplicatedata,'profile_data' => $profile_data, 'user_profile_brands' => $user_profile_brands,'canuseradd' => $canuseradd,'allcategories'=>$allcategories,'useraddrequest' => $useraddrequest,
             'allqust' => $allqust,'getquestans' => $getquestans,'samples' => $samples,'minval' => $getminMax[0]['minimum_sample'],'maxval' => $getminMax[0]['maximum_sample'],'selectdmaterials' => $selectdmaterials));
         $this->load->view('customer/footer_customer');
    }
    
    public function addFileRequest($success){
        $uid = $this->load->get_var('main_user');
        if (!is_dir('public/uploads/requests/' . $success)) {
                $data = mkdir('./public/uploads/requests/' . $success, 0777, TRUE);
            }
            
            //Move files from temp folder
            // Get array of all source files
            $uploadPATH = $_SESSION['temp_folder_names'];
            $uploadPATHs = str_replace("./","",$uploadPATH);
            
            //echo $uploadPATHs;exit;
            $files = scandir($uploadPATH);
            // Identify directories
            $source = $uploadPATH.'/';
            
           // echo "<pre>"; print_r($files);exit;
            $destination = './public/uploads/requests/' . $success.'/';
            
            if(UPLOAD_FILE_SERVER == 'bucket'){
            
            foreach ($files as $file) {
                
            if (in_array($file, array(".",".."))) continue;
                
            $staticName = base_url().$uploadPATHs.'/'.$file;
           
            $config = array(
                'upload_path' => 'public/uploads/requests/'. $success.'/',
                'file_name' => $file
            );
            
            $this->s3_upload->initialize($config);
           $is_file_uploaded = $this->s3_upload->upload_multiple_file($staticName);
         //  echo $is_file_uploaded."<pre>";print_r($is_file_uploaded);exit;
           if($is_file_uploaded['status'] == 1){
             $delete[] = $source.$file;  
             unlink('./tmp/tmpfile/' . basename($staticName));
            }
            }
            }else{
                 //echo $files.'test';exit;
              foreach ($files as $file) {
              if (in_array($file, array(".",".."))) continue;
              $filepath = APPPATH.$source.$file;
              if (copy($source.$file, $destination.$file)) {
                $delete[] = $source.$file;
              }
            }
            }
            foreach ($delete as $file) {
              unlink($file);
            }
           rmdir($source);
            unset($_SESSION['temp_folder_name']);
            //if($is_file_uploaded['status'] == 1){
            if(isset($_POST['delete_file']) && $_POST['delete_file'] != ''){
            foreach($_POST['delete_file'] as $SubFIlekey => $GetFIleName)
            {
                 $request_files_data = array("request_id" => $success,
                         "user_id" => $uid,
                         "user_type" => "customer",
                         "file_name" => $GetFIleName,
                         "image_link" => $_POST['image_link'][$SubFIlekey],
                         "created" => date("Y-m-d H:i:s"));
                 $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
            }
            }
            //}
             /* else{
               $this->session->set_flashdata('message_error', "Something went wrong! Please Upload Preview  File Again.", 5);
               redirect(base_url() . "customer/request/design_request");
            }*/
    }

    //new_request_brief
    public function new_request_brief_update($id = "") {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if (!empty($_POST)) {
            redirect(base_url() . "customer/request/add_new_request");
            if (isset($_POST['proceed'])) {
                $plate = "";
                $fileType = "";
                if (isset($_POST['designColors'])) {
                    $plate = implode(",", $_POST['designColors']);
                }
                if (isset($_POST['filetype'])) {
                    $fileType = implode(",", $_POST['filetype']);
                }
                $data = array("customer_id" => $_SESSION['user_id'],
                    "title" => $_POST['project_title'],
                    "business_industry" => $_POST['industry_design'],
                    "description" => $_POST['description'],
                    "designer" => $this->input->post('designer'),
                    "deliverables" => $fileType,
                    "design_colors" => $plate
                );

                $success = $this->Welcome_model->update_data("requests", $data, array("id" => $id));

                if (!is_dir('public/uploads/requests/' . $success)) {
                    $data = mkdir('./public/uploads/requests/' . $success, 0777, TRUE);
                }
                //Move files from temp folder
                // Get array of all source files
                $uploadPATH = $_SESSION['temp_folder_names'];
                $files = scandir($uploadPATH);
                $uploadPATHs = str_replace("./","",$uploadPATH);
                // Identify directories
                $source = $uploadPATH . '/';
                $destination = './public/uploads/requests/' . $id . '/';
                // Cycle through all source files
                if(UPLOAD_FILE_SERVER == 'bucket'){
                    foreach ($files as $file) {

                        if (in_array($file, array(".", "..")))
                            continue;

                        $staticName = base_url() . $uploadPATHs . '/' . $file;
                        //echo $staticName;exit;
                        $config = array(
                            'upload_path' => 'public/uploads/requests/' . $id . '/',
                            'file_name' => $file
                        );
                       
                        $this->s3_upload->initialize($config);
                        $is_file_uploaded = $this->s3_upload->upload_multiple_file($staticName);
                        if($is_file_uploaded['status'] == 1){
                            $delete[] = $source . $file;
                            unlink('./tmp/tmpfile/' . basename($staticName));
                        }
                        
                    }
                }else {
                foreach ($files as $file) {
                    if (in_array($file, array(".", "..")))
                        continue;
                    // If we copied this successfully, mark it for deletion
                    if (copy($source . $file, $destination . $file)) {
                        $delete[] = $source . $file;
                    }
                }
                }
                // Delete all successfully-copied files
                foreach ($delete as $file) {
                    unlink($file);
                }
                rmdir($source);
                unset($_SESSION['temp_folder_name']);
 // echo "<pre/>";print_r($_POST['delete_file']);exit;
                // Insert data into database      
                if (isset($_POST['delete_file']) && $_POST['delete_file'] != '') {

                    foreach ($_POST['delete_file'] as $SubFIlekey => $GetFIleName) {
                        $request_files_data = array("request_id" => $id,
                            "user_id" => $_SESSION['user_id'],
                            "user_type" => "customer",
                            "file_name" => $GetFIleName,
                            "created" => date("Y-m-d H:i:s"));
                        $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                    }
                }
                if (isset($_GET['rep']) && $_GET['rep'] == 1) { 
                    redirect(base_url() . "customer/request/new_request_review/" . $id . "?rep=1");
                } else {
                    redirect(base_url() . "customer/request/new_request_review/" . $id);
                }
            }
        }
        redirect(base_url() . "customer/request/add_new_request");
    }

    public function approve($id) {
        $uid = $this->load->get_var('main_user');
        //$uid = isset($_SESSION['id']) ? $_SESSION['id'] : '';
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if ($id == "") {
            redirect(base_url() . "customer/request/design_request");
        }
        $currentDateTime = date("Y-m-d H:i:s");
        $success = $this->Welcome_model->update_data("requests", array("status" => "approved", "status_admin" => "approved", "status_designer" => "approved", "status_qa" => "approved", 'dateinprogress' => $currentDateTime, 'latest_update' => $currentDateTime, 'approvaldate' => $currentDateTime, 'modified' => $currentDateTime), array("id" => $id));
        //$this->refreshStatus($uid);
        if ($success) {
            $this->session->set_flashdata('message_success', "Request approved successfully.", 5);
            redirect(base_url() . "customer/request/project_info/" . $id);
        } else {
            $this->session->set_flashdata('message_error', "Error while updating the request status.", 5);
            redirect(base_url() . "customer/request/project_info/" . $id);
        }
    }

    public function markAsCompleted($reqid,$permissiondata = NULL,$userid = NULL) {
        if ($reqid != 0 || $reqid == '') {
            if($userid != ""){
                $front_userdata = $this->Admin_model->getuser_data($userid);
                if($front_userdata[0]['parent_id'] == 0){
                        $uid = $userid;
                }else{
                        $uid = $front_userdata[0]['parent_id'];
                }
                $login_user_id = $uid;
            }else{
                    $uid = $this->load->get_var('main_user');
                    $login_user_id = $this->load->get_var('login_user_id'); 
            }
            
           // $uid = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : '';
            $currentDateTime = date("Y-m-d H:i:s");
            $request_data = $this->Request_model->get_request_by_id($reqid);
            $customer_data = $this->Admin_model->getuser_data($request_data[0]['customer_id']);
            $login_user_id = $this->load->get_var('login_user_id'); 
            $admin_id = $this->load->get_var('admin_id');
            $success = $this->Welcome_model->update_data("requests", array("status" => "approved","priority" => 0,"sub_user_priority" => 0, "status_admin" => "approved", "status_designer" => "approved", "status_qa" => "approved", 'dateinprogress' => $currentDateTime, 'latest_update' => $currentDateTime, 'approvaldate' => $currentDateTime, 'modified' => $currentDateTime), array("id" => $reqid));
            $this->refreshStatus($uid,'markAsCompleted',$reqid);
            $this->myfunctions->capture_project_activity($reqid,'',$request_data[0]['designer_id'],$request_data[0]['status'].'_to_approved','markAsCompleted',$login_user_id,1);
            if ($success) {
                
                /**  save notifications when upload draft design * */
                if($request_data[0]['dummy_request'] != 1){
                $this->myfunctions->show_notifications($reqid, $login_user_id, "Approved Project..!", "admin/dashboard/view_request/" . $reqid, $admin_id);
                $this->myfunctions->show_notifications($reqid, $login_user_id, "Approved Project..!", "qa/dashboard/view_project/" . $reqid, $customer_data[0]['qa_id']);
                $this->myfunctions->show_notifications($reqid, $login_user_id, "Approved Project..!", "designer/request/project_info/" . $reqid, $customer_data[0]['designer_id']);
                }
                /** end save notifications when upload draft design * */
                $this->session->set_flashdata('message_success', "Request approved successfully.", 5);
                if(isset($permissiondata) && $permissiondata != ''){
                   // die($permissiondata);
                if($permissiondata == $reqid){
                        redirect(base_url() . "project_info_view/" . $permissiondata.'?id='.$userid);
                }else{
                redirect(base_url() . "project-info/" . $permissiondata);
				   }				   
                }else{
                    redirect(base_url() . "customer/request/project_info/" . $reqid);
                }
            } else {
                $this->session->set_flashdata('message_error', "Error while updating the request status.", 5);
                redirect(base_url() . "customer/request/project_info/" . $reqid);
            }
        }
    }

    public function markAsRevision($reqid) {
        //echo $reqid;exit;
        if ($reqid != 0 || $reqid == '') {
            $uid = $this->load->get_var('main_user');
            
           // $uid = isset($_SESSION['id']) ? $_SESSION['id'] : '';
            $currentDateTime = date("Y-m-d H:i:s");
            $customer_data = $this->Admin_model->getuser_data($uid);
            $getsubscriptionplan = $this->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
            if($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])){
                $turn_around_days = $customer_data[0]['turn_around_days'];
            }else{
                $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
            }
            $request_data = $this->Request_model->get_request_by_id($reqid);
            $subcat_data = $this->Category_model->get_category_byID($request_data[0]['subcategory_id']);
            $expected = $this->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"),$customer_data[0]['plan_name'],$request_data[0]['category_bucket'],$subcat_data[0]['timeline'],$turn_around_days);
            $success = $this->Welcome_model->update_data("requests", array("status" => "disapprove", "status_admin" => "disapprove", "status_designer" => "disapprove", "status_qa" => "disapprove", 'dateinprogress' => $currentDateTime,"who_reject" => 1, 'latest_update' => $currentDateTime,"expected_date" => $expected, 'modified' => $currentDateTime), array("id" => $reqid));
            $this->refreshStatus($uid,'markAsrevesion',$reqid);
            if ($success) {
                $this->session->set_flashdata('message_success', "Status successfully changed..", 5);
                redirect(base_url() . "customer/request/project_info/" . $reqid);
            } else {
                $this->session->set_flashdata('message_error', "Error while updating status.", 5);
                redirect(base_url() . "customer/request/project_info/" . $reqid);
            }
        }
    }

    public function disapprove($id) {

        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if ($id == "") {
            redirect(base_url() . "customer/request/design_request");
        }
        $success = $this->Welcome_model->update_data("requests", array("status" => "disapprove", "status_admin" => "disapprove", "status_designer" => "disapprove","who_reject" => 1), array("id" => $id));
        if ($success) {
            $this->session->set_flashdata('message_success', "Request is Disapproved Successfully..!", 5);
            redirect(base_url() . "customer/request/project_info/" . $id);
        } else {
            $this->session->set_flashdata('message_error', "Request is not Disapproved Successfully..!", 5);
            redirect(base_url() . "customer/request/project_info/" . $id);
        }
    }

    public function request_response() { 
      //  echo "hello";exit;
        $result = array();
       // $this->myfunctions->checkloginuser("customer");
        if(isset($_POST['front_user_id']) && $_POST['front_user_id'] != ""){
            $front_userdata = $this->Admin_model->getuser_data($_POST['front_user_id']);
            if($front_userdata[0]['parent_id'] == 0){
                    $uid = $_POST['front_user_id'];
            }else{
                    $uid = $front_userdata[0]['parent_id'];
            }
            $login_user_id = $uid;
        }else{
            $uid = $this->load->get_var('main_user');
            $login_user_id = $this->load->get_var('login_user_id'); 
        } 
        $this->Welcome_model->update_online();
        $is_reveiw = $this->Request_model->get_reviewfeedback($_POST['fileid'],'customer');
        $request_data = $this->Request_model->get_request_by_id($_POST['request_id']);
        $success = $this->Welcome_model->update_data("request_files", array("modified" => date("Y:m:d H:i:s"), "status" => $_POST['filestatus']), array("id" => $_POST['fileid']));
        if ($success) {
            if ($_POST['value'] == "Reject") {
                $_POST['value'] = "disapprove";
                $this->Welcome_model->update_data("requests", array("status" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "status_qa" => $_POST['value'],"who_reject" => 1), array("id" => $_POST['request_id']));
                if($request_data[0]['dummy_request'] != 1){
                $this->myfunctions->show_notifications($_POST['request_id'],$login_user_id,"Your Design For \"".$request_data[0]['title']."\" is Rejected..!","designer/request/project_info/" . $_POST['request_id'],$request_data[0]['designer_id']);
                }
            } else {
                $_POST['value'] = "approved";
                $this->Welcome_model->update_data("requests", array("approvaldate" => date("Y:m:d H:i:s"), "status" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "status_qa" => $_POST['value'], "approvaldate" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                $customer_data = $this->Admin_model->getuser_data($uid);
//                $request2 = $this->Admin_model->get_all_requested_designs(array("active"), $request_data[0]['customer_id']);
//                $request3 = $this->Admin_model->get_all_requested_designs(array("checkforapprove,disapprove"), $request_data[0]['customer_id']);
//                $updatepriority = $this->Request_model->getall_newrequest($uid, array('pending', 'assign'), "", "priority");
                $ActiveReq = $this->myfunctions->CheckActiveRequest($uid);
                if ($ActiveReq == 1) {
                    $this->myfunctions->move_project_inqueqe_to_inprogress($uid,'','request_response_incustomer',$_POST['request_id']);
                }
                $this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'','approved_design_by_customer','request_response_incustomer',$login_user_id);
                if($request_data[0]['dummy_request'] != 1){
                $this->myfunctions->show_notifications($_POST['request_id'],$login_user_id,"Your Project \"".$request_data[0]['title']."\" has been Approved..!","designer/request/project_info/" . $_POST['request_id'],$request_data[0]['designer_id']);
                $this->myfunctions->show_notifications($_POST['request_id'],$login_user_id,"Your Project \"".$request_data[0]['title']."\" has been Approved..!","admin/dashboard/view_request/" . $_POST['request_id'],$admin_id);
                $this->myfunctions->show_notifications($_POST['request_id'],$login_user_id,"Your Project \"".$request_data[0]['title']."\" has been Approved..!","qa/dashboard/view_project/" . $_POST['request_id'],$customer_data[0]['qa_id']);
                }
            }
            
        }
        $modified_date = $this->onlytimezone(date("Y:m:d H:i:s"));
        $result['modified'] = date("M d, Y h:i A", strtotime($modified_date));
        $result['success'] = $success;
        if($is_reveiw){
        $result['is_reveiw'] = $is_reveiw;
        }
        echo json_encode($result);
        exit;
    }

    public function design_rating() {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if (!empty($_POST)) {
            if ($this->Admin_model->update_data("request_files", array("customer_grade" => $_POST['customer_rating']), array("id" => $_POST['id']))) {
                $this->session->set_flashdata('message_success', "Rating Success..!", 5);
            } else {
                $this->session->set_flashdata('message_error', "Rating Not Success..!", 5);
            }
            redirect(base_url() . "customer/request/project_image_view/" . $_POST['id'] . "?id=" . $_POST['request_id']);
        }
    }

    public function submit_grade_from_view_request() {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if (!empty($_POST)) {
            $this->Admin_model->update_data("request_files", array("customer_grade" => $_POST['number']), array("id" => $_POST['id']));
        }
    }

    public function designer_rating_ajax() {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        echo $this->Admin_model->update_data("request_files", array("customer_grade" => $_POST['value']), array("id" => $_POST['fileid']));
    }

    public function customer_chat_ajax() {

        $designer_file = $this->Admin_model->get_requested_files_ajax($_POST['fileid'], $_POST['id'], "", "designer", "1");
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id'], "desc");
        }
        $user = $this->Account_model->getuserbyid($_SESSION['user_id']);
        $request = $this->Request_model->get_request_by_id($_POST['id']);
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        $ap = $designer_file[0]['status'];
        if ($ap == "Approve") {
            echo '<span class="label">Approved</span>';
        }
        if ($ap == "Reject") {
            echo '<span class="label" style ="background:#e42244 !important">Rejected</span>';
        }
        ?>
        <div class="col-md-12 "  style="padding:10px;">
            <image src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $_POST['id'] . "/" . $designer_file[0]['file_name']; ?>" style="max-height:100%;width:100%;max-height: 600px;height: 600px;margin: auto;display: block;"  />
         <!--   <div onclick="$('.open_<?php echo $designer_file[0]['id']; ?>').toggle();" style="height:20px;width:20px;border-radius:50%;    background: red;position: absolute;top: 50px;left: 50px;"></div>-->
        </div>
        <div class="appenddot_<?php echo $designer_file[0]['id']; ?>">
            <!-- Chat toolkit start-->
            <?php
            for ($j = 0; $j < sizeof($designer_file[0]['chat']); $j++) {
                if ($designer_file[0]['chat'][$j]['sender_role'] == "customer") {
                    $background = "pinkbackground";
                } else {
                    $background = "bluebackground";
                }
                ?>
                <?php
                if ($designer_file[0]['chat'][0]['xco'] != "") {
                    if ($designer_file[0]['chat'][$j]['sender_role'] == "customer") {
                        $image1 = "dot_image1.png";
                        $image2 = "dot_image2.png";
                    } else {
                        $image1 = "designer_image1.png";
                        $image2 = "designer_image2.png";
                    }
                    ?>
                    <div class="<?php //echo $background;          ?>" style="position:absolute;left:<?php echo $designer_file[0]['chat'][$j]['xco']; ?>px;top:<?php echo $designer_file[0]['chat'][$j]['yco']; ?>px;width:25px;z-index:99999999999999999;height:25px;" onclick='show_customer_chat(<?php echo $designer_file[0]['chat'][$j]['id']; ?>);'>

                        <img src="<?php echo base_url() . "public/" . $image1; ?>" class="customer_chatimage1 customer_chatimage<?php echo $designer_file[0]['chat'][$j]['id']; ?>"/>
                        <img style="display:none;" src="<?php echo base_url() . "public/" . $image2; ?>" class="customer_chatimage2 customer_chatimage<?php echo $designer_file[0]['chat'][$j]['id']; ?>"/>
                    </div>
                    <div class="customer_chat customer_chat<?php echo $designer_file[0]['chat'][$j]['id']; ?>" style="display:none;background:#fff;position:absolute;left:<?php echo $designer_file[0]['chat'][$j]['xco'] + 25; ?>px;top:<?php echo $designer_file[0]['chat'][$j]['yco'] + 10; ?>px;padding: 5px;border-radius: 5px;">
                        <label><?php echo $designer_file[0]['chat'][$j]['message']; ?></label>
                    </div>
                <?php }
                ?>
            <?php } ?> 
        </div>
        <div class="openchat" style="width:35%;position:absolute;display:none;padding:10px 20px;background:#fff;">
            <textarea onClick="event.stopPropagation();" class='form-control text_write text_<?php echo $designer_file[0]['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px;margin-bottom:5px;" placeholder="Leave a comment" onkeydown="javascript: if (event.keyCode == 13) {
                                $('.send_text<?php echo $designer_file[0]['id']; ?>').click();
                            }"></textarea>
            <button onClick="send_request_img_chat('<?php echo $designer_file[0]['id']; ?>');
                            event.stopPropagation();" class="pinkbackground send_request_img_chat send_text send_text<?php echo $designer_file[0]['id']; ?>" style="border: none;border-radius: 5px;padding: 1px 10px;" data-fileid="<?php echo $designer_file[0]['id']; ?>" 
                    data-senderrole="customer" 
                    data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                    data-receiverid="<?php echo $request[0]['designer_id']; ?>" 
                    data-receiverrole="designer"
                    data-customername="<?php echo $user[0]['first_name'] . " " . $user[0]['last_name']; ?>"
                    data-xco="" data-yco=""><i class="fa fa-send whitetext"></i></button>
            <span onclick="mycancellable(<?php echo $designer_file[0]['id']; ?>);
                            event.stopPropagation();" class="greytext mycancellable mycancellable<?php echo $designer_file[0]['id']; ?> " style="font-size:10px;cursor:pointer;">Cancel</span>
        </div>
        <?php
    }

    public function design_rating2($id, $file_id, $no) {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if ($this->Admin_model->update_data("request_files", array("customer_grade" => $no), array("id" => $file_id))) {
            $this->session->set_flashdata('message_success', "Rating Success..!", 5);
        } else {
            $this->session->set_flashdata('message_error', "Rating Not Success..!", 5);
        }
        redirect(base_url() . "customer/request/project_image_view/" . $file_id . "?id=" . $id);
    }

    public function log_out() {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_data("users", array("online" => 0), array("id" => $_SESSION['user_id']));
        $this->Request_model->logout();
        redirect(base_url());
    }

    public function suggest_view() {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        $success = $this->Welcome_model->update_data("request_files", array("status" => "Reject"), array("request_id" => $_POST['requestid']));

        if ($success) {

            $_POST['value'] = "disapprove";

            $this->Welcome_model->update_data("requests", array("status" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value']), array("id" => $_POST['requestid']));

            $this->Welcome_model->insert_data("request_discussions", array("request_id" => $_POST['requestid'], "sender_type" => $_POST['senderrole'], "sender_id" => $_POST['senderid'], "reciever_id" => $_POST['receiverid'], "reciever_type" => $_POST['receiverrole'], "message" => $_POST['suggest'], "created" => date('Y-m-d H:i:s')));

            $this->session->set_flashdata('message_error', 'Desigen reject is Successfully.!', 5);
            redirect(base_url() . "customer/request/project_image_view/" . $_POST['fileid'] . "?id=" . $_POST['requestid']);
        }
        echo $success;
    }

    public function load_messages_from_designer() {
        $project_id = $_POST['project_id'];
        $customer_id = $_POST['cst_id'];
        $designer_id = $_POST['desg_id'];
        $msg_seen = $_POST['customerseen'];
        $chat_request_msg = $this->Request_model->get_customer_unseen_msg($project_id, $customer_id, $designer_id, $msg_seen);
        //  echo "<pre>";print_R($chat_request_msg);
        if (!empty($chat_request_msg)) {
            $message['message'] = $chat_request_msg[0]['message'];
            $message['profile_picture'] = isset($chat_request_msg[0]['profile_picture']) ? $chat_request_msg[0]['profile_picture'] : 'user-admin.png';
            $message['typee'] = $chat_request_msg[0]['sender_type'];
            $message['msg_first_name'] = $chat_request_msg[0]['first_name'];
            echo json_encode($message);
            $this->Admin_model->update_data("request_discussions", array("customer_seen" => 1), array("request_id" => $project_id));
        } else {
            // $message = "Hello Everyone";
            // echo $message;
        }
    }

    public function design_request_1() {
        $this->myfunctions->checkloginuser("customer");
        $created_user = $this->load->get_var('main_user');
        $parent_user_plan = $this->load->get_var('parent_user_plan');
        $login_user_id = $this->load->get_var('login_user_id'); 
        $profile_data = $this->Admin_model->getuser_data($login_user_id);
        $this->Welcome_model->update_online();
        $userid = $_SESSION['user_id'];
        $canuserdel = $this->myfunctions->isUserPermission('delete_req');
        $canaccessallbrands = $this->myfunctions->isUserPermission('brand_profile_access');
	$can_manage_priorities = $this->myfunctions->isUserPermission('manage_priorities');
        $canuseradd = $this->myfunctions->isUserPermission('add_requests');
        $trail_user = $this->Request_model->getuserbyid($created_user);
        $brand_id = isset($_GET['brand_id']) ? $_GET['brand_id'] : "";
        $client_id = isset($_GET['client_id']) ? $_GET['client_id'] : "";
        $count_active_project = $this->Request_model->getall_request_count($created_user, array(), '', 'priority',$brand_id,$client_id);
        $active_project = $this->Request_model->getall_request($created_user, array(), '', 'priority',$brand_id,$client_id);
        $request_for_trial = $this->Request_model->getall_request_for_trial_user($created_user);
        $request_for_all_status_acc_billing_cycle = $this->Request_model->getall_request_for_all_status($created_user,$trail_user[0]['billing_start_date'],$trail_user[0]['billing_end_date']);
        $useraddrequest = $this->myfunctions->canUserAddNewRequest($trail_user,$request_for_all_status_acc_billing_cycle,$request_for_trial);
        $assign_requests = $this->Request_model->getallrequestcount_accstatus($created_user, array('assign'));
        $subuser_assign_requests = $this->Request_model->getallrequestcount_accstatus($created_user, array('assign'),$login_user_id);
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['user_data'] = $profile_data;
            $active_project[$i]['customer_variables'] = $this->myfunctions->customerVariables($active_project[$i]['status']);
            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
            $getlatestfiledraft = $this->Request_model->get_latestdraft_file($active_project[$i]['id']);
            $active_project[$i]['latest_draft'] = $getlatestfiledraft[0]['file_name'];
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
               if($active_project[$i]['expected_date'] == '' || $active_project[$i]['expected_date'] == NULL){
                $active_project[$i]['expected'] = $this->myfunctions->check_timezone($active_project[$i]['latest_update'],$parent_user_plan[0]['plan_id']);
                }else{
                $active_project[$i]['expected'] = $this->onlytimezone($active_project[$i]['expected_date']);
                }
            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                $active_project[$i]['deliverydate'] = $this->onlytimezone($active_project[$i]['approvaldate']);
            }
            $active_project[$i]['max_priority'] = $assign_requests;
            $active_project[$i]['subuser_max_priority'] = $subuser_assign_requests;
            $id = $active_project[$i]['id'];
            $data = $this->Request_model->get_request_by_id($id);
            $active_project[$i]['client_name'] = $this->Request_model->getuserbyid($active_project[$i]['created_by'],'client');
        }
        $draft_project_count = $this->Request_model->getalldraft_request_count($brand_id,$client_id);
        $draft_project = $this->Request_model->getalldraft_request($brand_id,$client_id);
        for ($i = 0; $i < sizeof($draft_project); $i++) {
            $getlatestfile = $this->Request_model->get_latestdraft_file($draft_project[$i]['id']);
            $draft_project[$i]['customer_variables'] = $this->myfunctions->customerVariables($draft_project[$i]['status']);
            $draft_project[$i]['latest_draft'] = $getlatestfile[0]['file_name'];
            $draft_project[$i]['total_chat'] = $this->Request_model->get_chat_number($draft_project[$i]['id'], $draft_project[$i]['customer_id'], $draft_project[$i]['designer_id'], "customer");
            $d_id = $draft_project[$i]['id'];
            $draft_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number_customer($draft_project[$i]['id'], $_SESSION['user_id'], "customer", "1");
            $data = $this->Request_model->get_request_by_id($d_id);
            $draft_project[$i]['client_name'] = $this->Request_model->getuserbyid($draft_project[$i]['created_by'],'client');
            //echo "<pre/>";print_r($draft_project);
        }
        $complete_project_count = $this->Request_model->getallcomplete_request_count($brand_id,$client_id);
        $complete_project = $this->Request_model->getallcomplete_request($brand_id,'approved',$client_id);
        for ($i = 0; $i < sizeof($complete_project); $i++) {
            $getlatestfilecomplete = $this->Request_model->get_latestdraft_file($complete_project[$i]['id']);
            $complete_project[$i]['customer_variables'] = $this->myfunctions->customerVariables($complete_project[$i]['status']);
            $complete_project[$i]['latest_draft'] = $getlatestfilecomplete[0]['file_name'];
            $complete_project[$i]['total_chat'] = $this->Request_model->get_chat_number($complete_project[$i]['id'], $complete_project[$i]['customer_id'], $complete_project[$i]['designer_id'], "customer");
            $c_id = $complete_project[$i]['id'];
            if ($complete_project[$i]['status'] == "approved") {
                $complete_project[$i]['approvddate'] = $this->onlytimezone($complete_project[$i]['approvaldate']);
            }
            $complete_project[$i]['comment_count'] = $commentcount;
            $complete_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number_customer($complete_project[$i]['id'], $_SESSION['user_id'], "customer", "1");
            $data = $this->Request_model->get_request_by_id($c_id);
            $complete_project[$i]['client_name'] = $this->Request_model->getuserbyid($complete_project[$i]['created_by'],'client');
        }
        $cancel_project_count = $this->Request_model->getallcancel_request_count($brand_id,$client_id);
        $cancel_project = $this->Request_model->getallcancel_request($brand_id,'cancel',$client_id);
//        echo "<pre/>";print_r($cancel_project);exit;
        for ($i = 0; $i < sizeof($cancel_project); $i++) {
            $getlatestfilecomplete = $this->Request_model->get_latestdraft_file($cancel_project[$i]['id']);
             $cancel_project[$i]['customer_variables'] = $this->myfunctions->customerVariables($cancel_project[$i]['status']);
            $cancel_project[$i]['latest_draft'] = $getlatestfilecomplete[0]['file_name'];
            $cancel_project[$i]['total_chat'] = $this->Request_model->get_chat_number($cancel_project[$i]['id'], $cancel_project[$i]['customer_id'], $cancel_project[$i]['designer_id'], "customer");
            $c_id = $cancel_project[$i]['id'];
            $getfileid = $this->Request_model->get_attachment_files($cancel_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) 
            {
                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            if ($cancel_project[$i]['status'] == "cancel") {
                $cancel_project[$i]['modified'] = $this->onlytimezone($cancel_project[$i]['modified']);
            }
            $cancel_project[$i]['comment_count'] = $commentcount;
            $cancel_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number_customer($cancel_project[$i]['id'], $login_user_id, "customer", "1");
            $data = $this->Request_model->get_request_by_id($c_id);
            $cancel_project[$i]['files'] = $this->Request_model->get_attachment_files_for_customer($cancel_project[$i]['id'], array('cancel'), "designer");
            $cancel_project[$i]['files_new'] = $this->Request_model->get_new_attachment_files_for_customer($cancel_project[$i]['id'], array('cancel'));
            $cancel_project[$i]['client_name'] = $this->Request_model->getuserbyid($cancel_project[$i]['created_by'],'client');
        }
        
         $hold_project_count = $this->Request_model->getallhold_request_count($brand_id);
        $hold_project = $this->Request_model->getallhold_request($brand_id, 'hold');
        for ($i = 0; $i < sizeof($hold_project); $i++) {
            $hold_project[$i]['user_data'] = $profile_data;
            $hold_project[$i]['customer_variables'] = $this->myfunctions->customerVariables($hold_project[$i]['status']);
            $hold_project[$i]['total_chat'] = $this->Request_model->get_chat_number($hold_project[$i]['id'], $hold_project[$i]['customer_id'], $hold_project[$i]['designer_id'], "customer");
            $getlatestfiledraft = $this->Request_model->get_latestdraft_file($hold_project[$i]['id']);
            $hold_project[$i]['latest_draft'] = $getlatestfiledraft[0]['file_name'];
            if ($hold_project[$i]['status'] == "active" || $hold_project[$i]['status'] == "disapprove") {
                if ($hold_project[$i]['expected_date'] == '' || $hold_project[$i]['expected_date'] == NULL) {
                    $hold_project[$i]['expected'] = $this->myfunctions->check_timezone($hold_project[$i]['latest_update'], $parent_user_plan[0]['plan_id']);
                } else {
                    $hold_project[$i]['expected'] = $this->onlytimezone($hold_project[$i]['expected_date']);
                }
            } elseif ($hold_project[$i]['status'] == "checkforapprove") {
                $hold_project[$i]['deliverydate'] = $this->onlytimezone($hold_project[$i]['approvaldate']);
            }
            $id = $hold_project[$i]['id'];
            $data = $this->Request_model->get_request_by_id($id);
            $hold_project[$i]['max_priority'] = $assign_requests;
            $hold_project[$i]['subuser_max_priority'] = $subuser_assign_requests;
            $hold_project[$i]['client_name'] = $this->Request_model->getuserbyid($hold_project[$i]['created_by'],'client');

        }

        $notifications = $this->Request_model->get_notifications($login_user_id);
        $notification_number = $this->Request_model->get_notifications_number($login_user_id);
        $messagenotifications = $this->Request_model->get_messagenotifications($login_user_id);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($login_user_id);
        
//        $planlist = $this->Stripe->getallsubscriptionlist();
//        $AmountData = array();
//        $infinite_val = array();
//        for ($i = 0; $i < sizeof($planlist); $i++) {
//            if($planlist[$i]['id'] == AGENCY_PLAN){
//                $oldTierValue = 2;
//                foreach($planlist[$i]['tiers'] as $tierskey=>$tiersval){
//                    $pId = ($tiersval['up_to'] == '')?"infinite":$tiersval['up_to']; 
//                    if($tiersval['up_to']!= 'infinite'){
//                        for ($ii = $oldTierValue; $ii <= $tiersval['up_to']; $ii++) {
//                            $AmountData[$ii] = $tiersval;
//                        }
//                        $oldTierValue = $tiersval['up_to']+1;
//                    } else{
//                        $infinite_val = $tiersval;
//                    }
//                }
//            }
//        }
//        $subscription_list = json_encode($AmountData);
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        if($canbrand_profile_access != 0){
        $user_profile_brands = $this->Request_model->get_brand_profile_by_user_id($created_user);
        }elseif($canbrand_profile_access == 0 && $profile_data[0]['user_flag'] == 'client'){
           $user_profile_brands = $this->Request_model->get_brand_profile_by_user_id($login_user_id,'created_by');
        }else{
        $user_profile_brand = $this->Request_model->get_user_brand_profile_by_user_id($login_user_id);
        if(sizeof($user_profile_brand) > 0){
            foreach ($user_profile_brand as $user_brands_info) {
               $brand_ids[] =  $user_brands_info['brand_id'];
            }
            $user_profile_brands = $this->Request_model->get_brandprofile_by_id('',$brand_ids);
        }
        }
        $subusersdata = array();
        if($profile_data[0]['user_flag'] != 'client'){
          $subusersdata = $this->Request_model->getAllsubUsers($created_user,"client","","","id,first_name,last_name,profile_picture");
        }
        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));
        $this->load->view('customer/design_request_1', array("active_project" => $active_project, "count_active" => $count_active_project, "complete_project" => $complete_project,"draft_project" => $draft_project, 'count_active_project' => $count_active_project, 'draft_count' => $draft_project_count, 'complete_project_count' => $complete_project_count, 'trial' => $trail_user, 'request_for_trial' => $request_for_trial,'billing_cycle_request' => $request_for_all_status_acc_billing_cycle,'canuseraddrequest' => $useraddrequest, 'user_profile_brands' => $user_profile_brands,'canuserdel' => $canuserdel,'canuseradd' => $canuseradd,'canaccessallbrands'=>$canaccessallbrands,'can_manage_priorities'=>$can_manage_priorities,'cancel_project_count'=>$cancel_project_count,'cancel_project'=>$cancel_project,'is_cancel_subscription'=>$is_cancel_subscription,'profile_data'=>$profile_data,"subusersdata"=>$subusersdata,'hold_project_count' => $hold_project_count,'hold_project' => $hold_project));
        $this->load->view('customer/footer_customer',array('data' => $trail_user));
    }
    
    
//    public function canUserAddNewRequest($trail_user,$billing_cycle_request,$request_for_trial){
//      //   echo $trail_user[0]['plan_name'];exit;
//      $login_user_id = $this->load->get_var('login_user_id');
//      $star_date = $trail_user[0]['billing_start_date'];
//      $payment_status = $trail_user[0]['payment_status'];
//      $new_time = date("Y-m-d H:i:s", strtotime('+12 hours', strtotime($star_date)));
//       $current_date = date("Y:m:d H:i:s");
//        if (($trail_user[0]['is_trail'] == 1 && sizeof($request_for_trial) >= 1)) {
//            return 0;  //if user trail
//        }
//        if ($trail_user[0]['is_cancel_subscription'] == 1) {
//            return 3;  //if user subscription is canceled
//        }
//        if($trail_user[0]['billing_cycle_request'] > 0){
//            if (sizeof($billing_cycle_request) >= $trail_user[0]['billing_cycle_request']) {
//               return 1; //if user have $99 plan
//           }else{
//               return 2; // user can add request
//           }  
//        }
//       
//        if(in_array($trail_user[0]['plan_name'],NEW_PLANS)){
//            //echo "true";exit;
//            $all_request = $this->Request_model->getall_request_for_all_status($trail_user[0]['id']);
//            if (!empty($all_request) && sizeof($all_request) >= $trail_user[0]['total_requests']) {
//             return 4; //if user have new plan  and user  cann't add request
//            }else{
//             return 2; // user can add request
//            }
//        }
//         
//        $customer_data = $this->Admin_model->getuser_data($login_user_id);
//       // echo "<pre>";print_r($customer_data);
//        if (in_array($trail_user[0]['plan_name'], AGENCY_USERS) && $customer_data[0]['parent_id'] != 0 && $customer_data[0]['user_flag'] == 'client') {
//
//            if($customer_data[0]['requests_type'] == 'per_month'){
//            
//                $all_request = $this->Request_model->getall_request_for_all_status($login_user_id,$customer_data[0]['billing_start_date'],$customer_data[0]['billing_end_date'],'created');
//                $total_requests = $customer_data[0]['billing_cycle_request'];
//          
//            }else if($customer_data[0]['requests_type'] == 'one_time'){
//                $total_requests = $customer_data[0]['total_requests'];
//                $all_request = $this->Request_model->getall_request_for_all_status($login_user_id,'','','created');  
//               // echo sizeof($all_request).' - '.$total_requests;
//            }else{
//                return 2; // user can add request
//            }
//           
//            if (sizeof($all_request) >= $total_requests) {
//                return 5; //user  cann't add request because cross sub user limit for add request
//            } else {
//                return 2; // user can add request
//            }
//        } else {
//            return 2; // user can add request
//        }
//    }

    public function set_priority() {
        $parent_user_plan = $this->load->get_var('parent_user_plan');
        $priorityfrom = isset($_GET['priorityfrom'])? $_GET['priorityfrom'] : '';
        $priorityto = isset($_GET['priorityto'])? $_GET['priorityto'] : '';
        $priority_user_type = isset($_GET['priority_user_type'])? $_GET['priority_user_type'] : '';
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $userid = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        if($priority_user_type != ""){
        $mainfrom_prority = $this->Request_model->getmain_priorityfrom_requests($priorityfrom,$login_user_id);
        $mainto_prority = $this->Request_model->getmain_priorityfrom_requests($priorityto,$login_user_id);
        }
        echo $priorityfrom.'-from,to-'.$priorityto;
        echo "<pre>";print_r($mainfrom_prority);
        echo "<pre>";print_r($mainto_prority);
        //$userid = ($_SESSION['user_id']) ? $_SESSION['user_id'] : ''; 
        if (isset($userid) && $userid != '') {
            // die($userid);
            if ($priorityfrom < $priorityto) {
                for ($i = $priorityfrom + 1; $i <= $priorityto; $i++) {
                    if($priority_user_type == ""){
                     $this->Request_model->update_priority($i, $i - 1, '', $userid);
                    }else{
                     $this->Request_model->update_sub_userpriority($i, $i - 1, '', $login_user_id);    
                    }
                }
                //$this->Request_model->update_priority($priorityfrom,$priorityto, $id);
            } else if ($priorityfrom > $priorityto) {
                for ($i = $priorityfrom - 1; $i >= $priorityto; $i--) {
                    if($priority_user_type == ""){
                     $this->Request_model->update_priority($i, $i + 1, '', $userid);
                    }else{
                     $this->Request_model->update_sub_userpriority($i, $i + 1, '', $login_user_id);    
                    }
                }
                // $this->Request_model->update_priority($priorityfrom,$priorityto, $id);
            }
        }
        if($priority_user_type == ""){
        $this->Request_model->update_priority($priorityfrom, $priorityto, $id);
        }else{
           
           //update sub user priority
           $this->Request_model->update_sub_userpriority($priorityfrom, $priorityto, $id); 
           
           //update main user priority
           $this->Request_model->update_priority($mainfrom_prority[0]['priority'], $mainto_prority[0]['priority'], $mainfrom_prority[0]['id']);
           $this->Request_model->update_priority($mainto_prority[0]['priority'], $mainfrom_prority[0]['priority'], $mainto_prority[0]['id']);
        }
        $active_project = $this->Request_model->getall_request($userid, array(''), '', 'priority');
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
            $getfileid = $this->Request_model->get_attachment_files($active_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
               if ($active_project[$i]['expected_date'] == '' || $active_project[$i]['expected_date'] == NULL) {
                   $active_project[$i]['expected'] = $this->myfunctions->check_timezone($active_project[$i]['latest_update'],$parent_user_plan[0]['plan_id']);
                } else {
                   $active_project[$i]['expected'] = $this->onlytimezone($active_project[$i]['expected_date']);
                }
            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                $active_project[$i]['deliverydate'] = $this->onlytimezone($active_project[$i]['latest_update']);
            } elseif ($active_project[$i]['status'] == "approved") {
                $active_project[$i]['approvddate'] = $this->onlytimezone($active_project[$i]['approvaldate']);
            }
            $active_project[$i]['comment_count'] = $commentcount;
            /* new */
            $id = $active_project[$i]['id'];
            $data = $this->Request_model->get_request_by_id($id);
            $active_project[$i]['files_new'] = $this->Request_model->get_new_attachment_files_for_customer($active_project[$i]['id'], array('customerreview', 'Approve'));
            $active_project[$i]['files'] = $this->Request_model->get_attachment_files_for_customer($active_project[$i]['id'], array('customerreview', 'Approve'), "designer");
            // for ($j = 0; $j < sizeof($active_project[$i]['files']); $j++) {
            // }
        }
        $activeTab = 0;
        foreach ($active_project as $project) {
            if ($project['status'] == "assign") {
                $activeTab++;
            }
        }
        for ($i = 0; $i < count($active_project); $i++) {
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "checkforapprove" || $active_project[$i]['status'] == "disapprove") {
                ?>
                <!-- Pro Box Start -->
                <div class="col-md-2 col-sm-6">
                    <div class="pro-productss pro-box-r1">
                        <div class="pro-box-r2">
                            <div class="pro-inbox-r1">
                                <div class="pro-inleftbox">
                                    <p class="pro-a">
                                        <?php
                                        if ($active_project[$i]['status'] == "active") {
                                            echo "Expected on";
                                        } elseif ($active_project[$i]['status'] == "checkforapprove") {
                                            echo "Delivered on";
                                        } elseif ($active_project[$i]['status'] == "disapprove") {
                                            echo "Expected on";
                                        }
                                        ?>
                                    </p>
                                    <p class="pro-b"><a href="">
                                            <?php
                                            if ($active_project[$i]['status'] == "active") {
                                                echo date('m/d/Y', strtotime($active_project[$i]['expected']));
                                            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                                                echo date('m/d/Y', strtotime($active_project[$i]['deliverydate']));
                                            } elseif ($active_project[$i]['status'] == "disapprove") {
                                                echo date('m/d/Y', strtotime($active_project[$i]['expected']));
                                            }
                                            ?>
                                        </a></p>
                                </div>
                                <div class="pro-inrightbox">
                                    <?php if ($active_project[$i]['status'] != "assign") { ?>
                                        <p class="green">Active</p>
                                    <?php } else { ?>
                                        <p class="pro-a">Priority </p>
                                        <p class="select-pro-a">
                                            <select onchange="prioritize(this.value, <?php echo $active_project[$i]['priority'] ?>,<?php echo $active_project[$i]['id'] ?>)">
                                                <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                                    <option value="<?php echo $j; ?>"
                                                    <?php
                                                    if ($j == $active_project[$i]['priority']) {
                                                        echo "selected";
                                                    }
                                                    ?>
                                                            ><?php echo $j; ?></option>                                             
                                        <?php } ?>
                                            </select>
                                        </p>
                <?php } ?>
                                </div>
                            </div>
                            <hr class="hr-a">   
                            <!-- <script type="text/javascript">alert("<?php echo $active_project[$i]['id']; ?>")</script> -->
                            <a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>">
                                <h3 class="pro-head space-b"><?php echo $active_project[$i]['title']; ?></h3>
                                <p class="pro-b"><?php echo $active_project[$i]['category']; ?></p>
                                <p class="space-a"></p>
                                <p class="neft">
                                    <?php
                                    if ($active_project[$i]['status'] == "checkforapprove") {
                                        $status = "Review your design";
                                        $color = "bluetext";
                                    } elseif ($active_project[$i]['status'] == "active") {
                                        $status = "Design In Progress";
                                        $color = "greentext";
                                    } elseif ($active_project[$i]['status'] == "disapprove") {
                                        $status = "Revision In Progress";
                                        $color = "orangetext ";
                                    } elseif ($active_project[$i]['status'] == "pending" || $active_project[$i]['status'] == "assign") {
                                        $status = "In Queue";
                                        $color = "greentext ";
                                    } else {
                                        $status = "";
                                        $color = "greentext ";
                                    }
                                    //echo $status;
                                    ?>
                                        <?php if ($status == 'In Queue'): ?>
                                        <span class="gray text-uppercase">
                                        <?php echo $status; ?>
                                        </span>
                                        <?php else: ?>
                                        <span class="green <?php echo $color; ?> text-uppercase">
                                        <?php echo $status; ?>
                                        </span>
                <?php endif ?>
                                </p>
                            </a>
                        </div>

                        <div class="pro-box-r3">
                            <p class="pro-a">Designer</p>
                            <p class="space-a"></p>
                            <div class="pro-circle-list clearfix">
                                <div class="pro-circle-box">
                                    <a href=""><figure class="pro-circle-img">
                                            <?php if ($active_project[$i]['profile_picture']) { ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $active_project[$i]['profile_picture']; ?>" class="img-responsive" />
                                            <?php } else { ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                <?php } ?>
                                        </figure>
                                        <p class="pro-circle-txt text-center"><?php echo $active_project[$i]['designer_first_name']; ?></p></a>
                                </div>
                            </div>
                        </div>

                        <div class="pro-box-r4">
                            <div class="pro-inbox-r1">
                                <div class="pro-inleftbox">
                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php if ($active_project[$i]['total_chat'] + $active_project[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                <?php echo $active_project[$i]['total_chat'] + $active_project[$i]['comment_count']; ?>
                                                </span>
                <?php } ?></a>
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        <a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13"> 
                                                    <?php if (count($active_project[$i]['files_new']) != 0) { ?>
                                                    <span class="numcircle-box">
                                                    <?php echo count($active_project[$i]['files_new']); ?>
                                                    </span>
                                            <?php } ?>
                                            </span>
                <?php echo count($active_project[$i]['files']); ?>
                                        </a>
                                    </p>
                                </div>
                                <div class="pro-inrightbox">
                                    <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>customer/request/deleteproject/<?php echo $active_project[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- Pro Box End -->
                <?php
            }
        }
        for ($i = 0; $i < count($active_project); $i++) {
            if ($active_project[$i]['status'] != "active" && $active_project[$i]['status'] != "checkforapprove" && $active_project[$i]['status'] != "disapprove") {
                ?>
                <!-- Pro Box Start -->
                <div class="col-md-2 col-sm-6">
                    <div class="pro-productss pro-box-r1">
                        <div class="pro-box-r2">
                            <div class="pro-inbox-r1"  style="padding: 7px 0px;">
                                <div class="pro-inleftbox">
                                    <p class="pro-a">In-Queue</p>
                                    <!-- <p class="pro-b"><a href="">
                                    <?php
                                    // $created_date =  date($active_project[$i]['dateinprogress']);
                                    // $expected_date = date('m/d/Y', strtotime($created_date . ' +1 day'));
                                    // if ( $active_project[ $i ][ 'modified' ] == "" ) {
                                    //     echo $expected_date;
                                    // } else {
                                    //     echo date( "m/d/Y", strtotime( $active_project[ $i ][ 'modified' ] ) );
                                    // }
                                    ?>
                                    </a></p> -->
                                </div>
                                <div class="pro-inrightbox">
                                    <?php if ($active_project[$i]['status'] != "assign") { ?>
                                        <p class="green">Active</p>
                <?php } else { ?>
                                        <p class="select-pro-a pro-a">
                                            Priority
                                            <select onchange="prioritize(this.value, <?php echo $active_project[$i]['priority'] ?>,<?php echo $active_project[$i]['id'] ?>)">
                                                <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                                    <option value="<?php echo $j; ?>"
                                                    <?php
                                                    if ($j == $active_project[$i]['priority']) {
                                                        echo "selected";
                                                    }
                                                    ?>
                                                            ><?php echo $j; ?></option>                                             
                    <?php } ?>
                                            </select>
                                        </p>
                <?php } ?>
                                </div>
                            </div>
                            <hr class="hr-a">   
                            <!-- <script type="text/javascript">alert("<?php echo $active_project[$i]['id']; ?>")</script> -->
                            <a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>">
                                <h3 class="pro-head space-b"><?php echo $active_project[$i]['title']; ?></h3>
                                <p class="pro-b"><?php echo $active_project[$i]['category']; ?></p>
                                <p class="space-a"></p>
                                <p class="neft">
                                    <?php
                                    if ($active_project[$i]['status'] == "checkforapprove") {
                                        $status = "Review your design";
                                        $color = "bluetext";
                                    } elseif ($active_project[$i]['status'] == "active") {
                                        $status = "Design In Progress";
                                        $color = "greentext";
                                    } elseif ($active_project[$i]['status'] == "disapprove") {
                                        $status = "Revision In Progress";
                                        $color = "orangetext ";
                                    } elseif ($active_project[$i]['status'] == "pending" || $active_project[$i]['status'] == "assign") {
                                        $status = "In Queue";
                                        $color = "greentext ";
                                    } else {
                                        $status = "";
                                        $color = "greentext ";
                                    }
                                    //echo $status;
                                    ?>
                                    <?php if ($status == 'In Queue'): ?>
                                        <span class="gray text-uppercase">
                                            <?php echo $status; ?>
                                        </span>
                                    <?php else: ?>
                                        <span class="green <?php echo $color; ?> text-uppercase">
                    <?php echo $status; ?>
                                        </span>
                <?php endif ?>
                                </p>
                            </a>
                        </div>

                        <div class="pro-box-r3">
                            <p class="pro-a">Designer</p>
                            <p class="space-a"></p>
                            <div class="pro-circle-list clearfix">
                                <div class="pro-circle-box">
                                    <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                    <!--  <a href=""><figure class="pro-circle-img">
                                    <?php if ($active_project[$i]['profile_picture']) { ?>
                                         <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $active_project[$i]['profile_picture']; ?>" class="img-responsive" />
                <?php } else { ?>
                                         <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                <?php } ?>
                                     </figure>
                                     <p class="pro-circle-txt text-center"><?php echo $active_project[$i]['designer_first_name']; ?></p></a> -->
                                </div>
                            </div>
                        </div>

                        <div class="pro-box-r4">
                            <div class="pro-inbox-r1">
                                <div class="pro-inleftbox">
                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php if ($active_project[$i]['total_chat'] + $active_project[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                    <?php echo $active_project[$i]['total_chat'] + $active_project[$i]['comment_count']; ?>
                                                </span>
                                                <?php } ?></a>
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        <a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                                                <?php if (count($active_project[$i]['files_new']) != 0) { ?>
                                                    <span class="numcircle-box">
                                                <?php echo count($active_project[$i]['files_new']); ?>
                                                    </span>
                <?php } ?>
                                            </span>
                <?php echo count($active_project[$i]['files']); ?></a>
                                    </p>
                                </div>
                                <div class="pro-inrightbox">
                                    <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>customer/request/deleteproject/<?php echo $active_project[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- Pro Box End -->
                <?php
            }
        }
    }

    public function set_priority_for_listview($priorityto, $priorityfrom, $id) {
        $parent_user_plan = $this->load->get_var('parent_user_plan');
        $userid = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        //$userid = $_SESSION['user_id'];
        $active_projects = $this->Request_model->getall_request($userid, array(), '', 'priority');
        $activeTab = 0;
        foreach ($active_projects as $project) {
            if ($project['status'] == "assign") {
                $activeTab++;
            }
        }
        if ($priorityfrom < $priorityto) {
            for ($i = $priorityfrom + 1; $i <= $priorityto; $i++) {
                $this->Request_model->update_priority($i, $i - 1);
            }
            $this->Request_model->update_priority($priorityfrom, $priorityto, $id);
        } else if ($priorityfrom > $priorityto) {
            for ($i = $priorityfrom - 1; $i >= $priorityto; $i--) {
                $this->Request_model->update_priority($i, $i + 1);
            }
            $this->Request_model->update_priority($priorityfrom, $priorityto, $id);
        }

        $active_project = $this->Request_model->getall_request($userid, array(), '', 'priority');
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
            $getfileid = $this->Request_model->get_attachment_files($active_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
                if($active_project[$i]['expected_date'] == '' || $active_project[$i]['expected_date'] == NULL){
                  $active_project[$i]['expected'] = $this->myfunctions->check_timezone($active_project[$i]['latest_update'],$parent_user_plan[0]['plan_id']);
                }else{
                  $active_project[$i]['expected'] = $this->onlytimezone($active_project[$i]['expected_date']);
                }
            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                $active_project[$i]['deliverydate'] = $this->onlytimezone($active_project[$i]['latest_update']);
            } elseif ($active_project[$i]['status'] == "approved") {
                $active_project[$i]['approvddate'] = $this->onlytimezone($active_project[$i]['approvaldate']);
            }
            $active_project[$i]['comment_count'] = $commentcount;
            /* new */
            $id = $active_project[$i]['id'];
            $data = $this->Request_model->get_request_by_id($id);
            $active_project[$i]['files_new'] = $this->Request_model->get_new_attachment_files_for_customer($active_project[$i]['id'], array('customerreview', 'Approve'));
            $active_project[$i]['files'] = $active_project[$i]['files'] = $this->Request_model->get_attachment_files_for_customer($active_project[$i]['id'], array('customerreview', 'Approve'), "designer");
            // for ($j = 0; $j < sizeof($active_project[$i]['files']); $j++) {
            // }
        }
        for ($i = 0; $i < count($active_project); $i++) {
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "checkforapprove" || $active_project[$i]['status'] == "disapprove") {
                ?>
                <!-- Pro Box Start -->
                <div class="pro-desh-row">
                    <div class="pro-desh-box delivery-desh">
                        <p class="pro-a">
                            <?php
                            if ($active_project[$i]['status'] == "active") {
                                echo "Expected on";
                            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                                echo "Delivered on";
                            } elseif ($active_project[$i]['status'] == "disapprove") {
                                echo "Expected on";
                            }
                            ?> 
                        </p>
                        <p class="pro-b">
                            <?php
                            if ($active_project[$i]['status'] == "active") {
                                echo date('m/d/Y', strtotime($active_project[$i]['expected']));
                            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                                echo date('m/d/Y', strtotime($active_project[$i]['deliverydate']));
                            } elseif ($active_project[$i]['status'] == "disapprove") {
                                echo date('m/d/Y', strtotime($active_project[$i]['expected']));
                            }
                            ?>                      
                        </p>
                        <div class="pro-inrightbox">
                <?php if ($active_project[$i]['status'] != "assign") { ?>
                                <p class="green">Active</p>
                                        <?php } else { ?>
                                <p class="pro-a">Priority
                                    <span class="select-pro-a">
                                        <select onchange="prioritize(this.value, <?php echo $active_project[$i]['priority'] ?>,<?php echo $active_project[$i]['id'] ?>)">
                                            <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                                <option value="<?php echo $j; ?>"
                                                        <?php
                                                        if ($j == $active_project[$i]['priority']) {
                                                            echo "selected";
                                                        }
                                                        ?>
                                                        ><?php echo $j; ?></option>                                             
                    <?php } ?>
                                        </select>
                                    </span>
                                </p>
                <?php } ?>
                        </div>
                    </div>

                    <div class="pro-desh-box dcol-1"  style="cursor:pointer;" onclick="window.location = '<?php echo base_url(); ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>'">
                        <div class="desh-head-wwq">
                            <div class="desh-inblock">
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>"><?php echo $active_project[$i]['title']; ?></a></h3>
                                <p class="pro-b"><?php echo $active_project[$i]['category']; ?></p>
                            </div>
                            <div class="desh-inblock">
                                <p class="neft pull-right">
                                    <?php
                                    if ($active_project[$i]['status'] == "checkforapprove") {
                                        $status = "Review your design";
                                        $color = "bluetext";
                                    } elseif ($active_project[$i]['status'] == "active") {
                                        $status = "Design In Progress";
                                        $color = "greentext";
                                    } elseif ($active_project[$i]['status'] == "disapprove") {
                                        $status = "Revision In Progress";
                                        $color = "orangetext ";
                                    } elseif ($active_project[$i]['status'] == "pending" || $active_project[$i]['status'] == "assign") {
                                        $status = "In Queue";
                                        $color = "greentext ";
                                    } else {
                                        $status = "";
                                        $color = "greentext ";
                                    }
                                    ?>
                                        <?php if ($status == 'In Queue'): ?>
                                        <span class="gray text-uppercase">
                                        <?php echo $status; ?>
                                        </span>
                <?php else: ?>
                                        <span class="green <?php echo $color; ?> text-uppercase">
                    <?php echo $status; ?>
                                        </span>
                <?php endif ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="pro-desh-box dcol-2">
                        <div class="pro-circle-list clearfix">
                            <div class="pro-circle-box">
                                <a href=""><figure class="pro-circle-img">
                                        <?php if ($active_project[$i]['profile_picture']) { ?>
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $active_project[$i]['profile_picture']; ?>" class="img-responsive" />
                <?php } else { ?>
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                <?php } ?>
                                    </figure>
                                    <p class="pro-circle-txt text-center">
                <?php echo $active_project[$i]['designer_first_name']; ?>
                                    </p></a>
                            </div>
                        </div>
                    </div>

                    <div class="pro-desh-box">
                        <div class="pro-desh-r1">
                            <div class="pro-inleftbox">
                                <p class="pro-a inline-per"><a href="javascript:void(0)"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php if ($active_project[$i]['total_chat'] + $active_project[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                <?php echo $active_project[$i]['total_chat'] + $active_project[$i]['comment_count']; ?>
                                                </span>
                                                <?php } ?></span></a></p>
                            </div>
                            <div class="pro-inrightbox">
                                <p class="pro-a inline-per"><a href="javascript:void(0)"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                                        <?php if (count($active_project[$i]['files_new']) != 0) { ?>
                                                <span class="numcircle-box">
                    <?php echo count($active_project[$i]['files_new']); ?>
                                                </span>
                <?php } ?>
                                        </span>
                <?php echo count($active_project[$i]['files']); ?>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- Pro Box End -->
                <?php
            }
        }
        for ($i = 0; $i < count($active_project); $i++) {
            if ($active_project[$i]['status'] != "active" && $active_project[$i]['status'] != "checkforapprove" && $active_project[$i]['status'] != "disapprove") {
                ?>
                <!-- Pro Box Start -->
                <div class="pro-desh-row">
                    <div class="pro-desh-box delivery-desh" style="padding-right: 0px; padding-left: 15px;">
                        <!-- <p class="pro-a">Delivery</p>
                        <p class="pro-b"> -->
                        <?php
                        // $created_date =  date($active_project[$i]['dateinprogress']);
                        // $expected_date = date('m/d/Y', strtotime($created_date . ' +1 day'));
                        // if ( $active_project[ $i ][ 'modified' ] == "" ) {
                        //     echo $expected_date;
                        // } else {
                        //     echo date( "m/d/Y", strtotime( $active_project[ $i ][ 'modified' ] ) );
                        // }
                        ?>                          
                        <!-- </p> -->
                        <p class="pro-a"">
                                    <?php if ($active_project[$i]['status'] != "assign") { ?>
                            <p class="green">Active</p>
                                    <?php } else { ?>
                            <p class="pro-a">Priority
                                <span class="select-pro-a">
                                    <select onchange="prioritize(this.value, <?php echo $active_project[$i]['priority'] ?>,<?php echo $active_project[$i]['id'] ?>)">
                                                <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                            <option value="<?php echo $j; ?>"
                        <?php
                        if ($j == $active_project[$i]['priority']) {
                            echo "selected";
                        }
                        ?>
                                                    ><?php echo $j; ?></option>                                             
                    <?php } ?>
                                    </select>
                                </span>
                            </p>
                <?php } ?>
                        </p>
                    </div>

                    <div class="pro-desh-box dcol-1"  style="cursor:pointer;" onclick="window.location = '<?php echo base_url(); ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>'">
                        <div class="desh-head-wwq">
                            <div class="desh-inblock">
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $active_project[$i]['id']; ?>"><?php echo $active_project[$i]['title']; ?></a></h3>
                                <p class="pro-b"><?php echo $active_project[$i]['category']; ?></p>
                            </div>
                            <div class="desh-inblock">
                                <p class="neft pull-right">
                                    <?php
                                    if ($active_project[$i]['status'] == "checkforapprove") {
                                        $status = "Review your design";
                                        $color = "bluetext";
                                    } elseif ($active_project[$i]['status'] == "active") {
                                        $status = "Design In Progress";
                                        $color = "greentext";
                                    } elseif ($active_project[$i]['status'] == "disapprove") {
                                        $status = "Revision In Progress";
                                        $color = "orangetext ";
                                    } elseif ($active_project[$i]['status'] == "pending" || $active_project[$i]['status'] == "assign") {
                                        $status = "In Queue";
                                        $color = "greentext ";
                                    } else {
                                        $status = "";
                                        $color = "greentext ";
                                    }
                                    ?>
                                    <?php if ($status == 'In Queue'): ?>
                                        <span class="gray text-uppercase">
                    <?php echo $status; ?>
                                        </span>
                <?php else: ?>
                                        <span class="green <?php echo $color; ?> text-uppercase">
                    <?php echo $status; ?>
                                        </span>
                <?php endif ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="pro-desh-box dcol-2">
                        <div class="pro-circle-list clearfix">
                            <div class="pro-circle-box">
                                <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                <!-- <a href=""><figure class="pro-circle-img">
                <?php if ($active_project[$i]['profile_picture']) { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $active_project[$i]['profile_picture']; ?>" class="img-responsive" />
                <?php } else { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                <?php } ?>
                                </figure>
                                <p class="pro-circle-txt text-center">
                <?php echo $active_project[$i]['designer_first_name']; ?>
                                </p></a> -->
                            </div>
                        </div>
                    </div>

                    <div class="pro-desh-box">
                        <div class="pro-desh-r1">
                            <div class="pro-inleftbox">
                                <p class="pro-a inline-per"><a href="javascript:void(0)"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php if ($active_project[$i]['total_chat'] + $active_project[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $active_project[$i]['total_chat'] + $active_project[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span></a></p>
                            </div>
                            <div class="pro-inrightbox">
                                <p class="pro-a inline-per"><a href="javascript:void(0)"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                <?php if (count($active_project[$i]['files_new']) != 0) { ?>
                                                <span class="numcircle-box">
                    <?php echo count($active_project[$i]['files_new']); ?>
                                                </span>
                <?php } ?>
                                        </span>
                <?php echo count($active_project[$i]['files']); ?></a></p>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- Pro Box End -->
                <?php
            }
        }
    }

    public function new_request_category() {

        $this->myfunctions->checkloginuser("customer");

        $this->Welcome_model->update_online();

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);

        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);

        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);

        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        $request_for_all_status = $this->Request_model->getall_request_for_trial_user($_SESSION['user_id']);
        //echo "<pre>";print_r($profile_data);
        if ($profile_data[0]['is_trail'] == 1 && sizeof($request_for_all_status) >= 1) {
            redirect(base_url() . "customer/request/design_request");
        }


        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));

        $this->load->view('customer/new_request_category');
        $this->load->view('customer/footer_customer');
    }

    public function process() {
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output['error'] = 'Error while uploading the files. Please try again.';
        $dateFolder = strtotime(date('Y-m-d H:i:s'));
        if (!(isset($_SESSION['temp_folder_names']) && $_SESSION['temp_folder_names'] != '')) {
            if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                $_SESSION['temp_folder_names'] = './public/uploads/temp/' . $dateFolder;
            } else {
                $_SESSION['temp_folder_names'] = '';
            }
        } else {
            if (!is_dir($_SESSION['temp_folder_names'])) {
                if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                    $_SESSION['temp_folder_names'] = './public/uploads/temp/' . $dateFolder;
                } else {
                    $_SESSION['temp_folder_names'] = '';
                }
            }
        }
        if ($_SESSION['temp_folder_names'] != '') {
            $files = $_FILES;
            $cpt = count($_FILES['file-upload']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['file-upload']['name'] = $files['file-upload']['name'][$i];
                $_FILES['file-upload']['type'] = $files['file-upload']['type'][$i];
                $_FILES['file-upload']['tmp_name'] = $files['file-upload']['tmp_name'][$i];
                $_FILES['file-upload']['error'] = $files['file-upload']['error'][$i];
                $_FILES['file-upload']['size'] = $files['file-upload']['size'][$i];
                $config = array(
                    'upload_path' => $_SESSION['temp_folder_names'],
                    'allowed_types' => ALLOWED_FILE_TYPES,
                    'max_size' => '0',
                    'overwrite' => FALSE
                );
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload("file-upload")) {
                    $data = array($this->upload->data());
                    $data[0]['error'] = false;
                    $output['files'][] = $data;
                    $output['status'] = true;
                    $output['error'] = '';
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $error_data = array();
                    $error_data[0] = array();
                    $error_data[0]['file_name'] = $files['file-upload']['name'][$i];
                    $error_data[0]['error'] = true;
                    $error_data[0]['error_msg'] = strip_tags($data['error']);
                    $output['files'][] = $error_data;
                    $output['status'] = true;
                    $output['error'] = '';
                }
            }
        } else {
            $output['error'] = 'Directory not writable. Please try again.';
        }
        echo json_encode($output);
        exit;
    }

    public function delete_file_req() {
        $reqid = isset($_POST['request_id']) ? $_POST['request_id'] : '';
        $filename = isset($_POST['filename']) ? $_POST['filename'] : '';
        $success = $this->Request_model->delete_request_file('request_files', $reqid, $filename);
        if ($success) {
            $dir = FCPATH . '/public/uploads/requests/' . $reqid;
            unlink($dir . "/" . $filename); //gi
        }
        $this->session->set_flashdata('message_success', "File is Deleted Successfully.!", 5);
    }

    public function delete_file_from_folder() {
        $folderPath = $_SESSION['temp_folder_names'];
        $folderPaths = $_SESSION['temp_folder_names'] . '/*';
        if (!empty($_POST)) {
            $data = $_POST['file_name'];
            $files = glob($folderPaths); // get all file names
            $dir = FCPATH . $folderPath;
            $dirHandle = opendir($dir);
            while ($file = readdir($dirHandle)) {
                if ($file == $data) {
                    unlink($dir . "/" . $file); //give correct path,
                } else {}
            }
        }
    }

    // new_request_brief_update
    public function new_request_brief($id = "") {
        
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        redirect(base_url() . "customer/request/add_new_request");
        if (!empty($_POST)) {
            if (isset($_POST['proceed'])) {

                $plate = "";
                $fileType = "";
                if (isset($_POST['designColors'])) {
                    $plate = implode(",", $_POST['designColors']);
                }
                if (isset($_POST['filetype'])) {
                    $fileType = implode(",", $_POST['filetype']);
                }

                $data = array("customer_id" => $_SESSION['user_id'],
                    "title" => $_POST['project_title'],
                    // "design_dimension" => $_POST['design_dimension'],
                    "business_industry" => $_POST['industry_design'],
                    "description" => $_POST['description'],
                    // "other_info" => $_POST['additional_info'],
                    "category" => $_POST['category'],
                    "created" => date("Y-m-d H:i:s"),
                    "status" => "draft",
                    "priority" => 0,
                    "logo_brand" => $_POST['logo-brand'],
                    "designer" => $this->input->post('designer'),
                    "deliverables" => $fileType,
                    "design_colors" => $plate
                );

                $success = $this->Welcome_model->insert_data("requests", $data);

                if (!is_dir('public/uploads/requests/' . $success)) {
                    $data = mkdir('./public/uploads/requests/' . $success, 0777, TRUE);
                }
                //Move files from temp folder
                // Get array of all source files
                $uploadPATH = $_SESSION['temp_folder_names'];
                $uploadPATHs = str_replace("./","",$uploadPATH);
                $files = scandir($uploadPATH);
                // Identify directories
                $source = $uploadPATH . '/';
                $destination = './public/uploads/requests/' . $success . '/';
                // Cycle through all source files
                if(UPLOAD_FILE_SERVER == 'bucket'){
                    foreach ($files as $file) {

                        if (in_array($file, array(".", "..")))
                            continue;

                        $staticName = base_url() . $uploadPATHs . '/' . $file;
                        //echo $staticName;exit;
                        $config = array(
                            'upload_path' => 'public/uploads/requests/' . $success . '/',
                            'file_name' => $file
                        );
                        
                        $this->s3_upload->initialize($config);
                        $is_file_uploaded = $this->s3_upload->upload_multiple_file($staticName);
                        if($is_file_uploaded['status'] == 1){
                             $delete[] = $source . $file;
                            unlink('./tmp/tmpfile/' . basename($staticName));
                        }
                       
                    }
                }else {
                foreach ($files as $file) {
                    if (in_array($file, array(".", "..")))
                        continue;
                    // If we copied this successfully, mark it for deletion
                    if (copy($source . $file, $destination . $file)) {
                        $delete[] = $source . $file;
                    }
                }
                }
                // Delete all successfully-copied files
                foreach ($delete as $file) {
                    unlink($file);
                }
                rmdir($source);
                unset($_SESSION['temp_folder_name']);

                // Insert data into database      
                if (isset($_POST['delete_file']) && $_POST['delete_file'] != '') {
                    foreach ($_POST['delete_file'] as $SubFIlekey => $GetFIleName) {
                        $request_files_data = array("request_id" => $success,
                            "user_id" => $_SESSION['user_id'],
                            "user_type" => "customer",
                            "file_name" => $GetFIleName,
                            "created" => date("Y-m-d H:i:s"));
                        $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                    }
                }
                redirect(base_url() . "customer/request/new_request_review/" . $success);
            }
        }
        $data = array();
        if ($id) {
            $data = $this->Request_model->get_request_by_id($id);
        }
        if (!isset($_POST['category']) && empty($data)) {
            redirect(base_url() . "customer/request/new_request/category");
        }

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));
        $this->load->view('customer/new_request_brief', array("data" => $data));
        $this->load->view('customer/footer_customer');
    }

    public function new_request_review($id = null) {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        redirect(base_url() . "customer/request/add_new_request");
        if ($id == "") {
            redirect(base_url() . "customer/request/add_new_request");
            //redirect(base_url() . "customer/request/new_request/category");
        }
        if (isset($_GET['red']) && $_GET['red'] == "d") {
            redirect(base_url() . "customer/request/design_request");
        }
        $data = $this->Request_model->get_request_by_id($id);

        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);
        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));
        $this->load->view('customer/new_request_review', array("data" => $data, 'id' => $id));
        $this->load->view('customer/footer_customer');
        // $this->load->view('customer/customer_footer');
    }

    public function dashboard_list_view() {
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        $userid = $_SESSION['user_id'];

        $active_project = $this->Request_model->getall_request($_SESSION['user_id'], '', '', 'priority');
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
            $active_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number_customer($active_project[$i]['id'], $_SESSION['user_id'], "customer", "1");
            /* new */
            $getfileid = $this->Request_model->get_attachment_files($active_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
                if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
                $active_project[$i]['expected'] = $this->myfunctions->check_timezone($active_project[$i]['latest_update']);
            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                $active_project[$i]['deliverydate'] = $this->onlytimezone($active_project[$i]['latest_update']);
            }
            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                $active_project[$i]['deliverydate'] = $this->onlytimezone($active_project[$i]['latest_update']);
            }
            $active_project[$i]['comment_count'] = $commentcount;
            $id = $active_project[$i]['id'];
            $data = $this->Request_model->get_request_by_id($id);
            $active_project[$i]['files_new'] = $this->Request_model->get_new_attachment_files_for_customer($active_project[$i]['id'], array('customerreview', 'Approve'));
            $active_project[$i]['files'] = $this->Request_model->get_attachment_files_for_customer($active_project[$i]['id'], array('customerreview', 'Approve'), "designer");
            // $active_project[$i]['files']= $this->Request_model->get_attachment_files($id, "designer");
            // for ($j = 0; $j < sizeof($active_project[$i]['files']); $j++) {
            // }
        }
        $draft_project = $this->Request_model->getalldraft_request();
        for ($i = 0; $i < sizeof($draft_project); $i++) {
            $draft_project[$i]['total_chat'] = $this->Request_model->get_chat_number($draft_project[$i]['id'], $draft_project[$i]['customer_id'], $draft_project[$i]['designer_id'], "customer");
            $draft_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number_customer($draft_project[$i]['id'], $_SESSION['user_id'], "customer", "1");
            $d_id = $draft_project[$i]['id'];
            $data = $this->Request_model->get_request_by_id($d_id);
            $draft_project[$i]['files'] = $this->Request_model->get_attachment_files($id, "designer");
        }

        $complete_project = $this->Request_model->getallcomplete_request();
        for ($i = 0; $i < sizeof($complete_project); $i++) {
            $complete_project[$i]['total_chat'] = $this->Request_model->get_chat_number($complete_project[$i]['id'], $complete_project[$i]['customer_id'], $complete_project[$i]['designer_id'], "customer");
            $complete_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number_customer($complete_project[$i]['id'], $_SESSION['user_id'], "customer", "1");
            $c_id = $complete_project[$i]['id'];
            $getfileid = $this->Request_model->get_attachment_files($complete_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            $complete_project[$i]['approvddate'] = $this->onlytimezone($complete_project[$i]['approvaldate']);
            $complete_project[$i]['comment_count'] = $commentcount;
            $data = $this->Request_model->get_request_by_id($c_id);
            $complete_project[$i]['files_new'] = $this->Request_model->get_new_attachment_files_for_customer($complete_project[$i]['id'], array('Approve'));
            $complete_project[$i]['files'] = $this->Request_model->get_attachment_files_for_customer($complete_project[$i]['id'], array('Approve'), "designer");
        }
        $notifications = $this->Request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->Request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->Request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($_SESSION['user_id']);

        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));

        $this->load->view('customer/dashboard-list-view', array("active_project" => $active_project, "complete_project" => $complete_project, "draft_project" => $draft_project));
        $this->load->view('customer/footer_customer');
    }

    public function project_image_view($id = null) {
        $reqid = isset($_GET['id'])?$_GET['id']:'';
        $this->myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        $user_id = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        
//        echo "<pre/>";print_r($alldraftpubliclinks);
        $sharedreqUser = $this->Request_model->getSharedpeopleinfobyreq_draftID($reqid,0);
        $shareddraftUser = $this->Request_model->getSharedpeopleinfobyreq_draftID($reqid,$id);  
        if ($id == "") {
            redirect(base_url() . "customer/request/design_request");
        }
        $checkUser = $this->Request_model->checkValidUser($_GET['id']);
        if ($checkUser != 1) {
            redirect(base_url() . "customer/request/design_request");
        }
        $data['cancomment_on_req'] = $this->myfunctions->isUserPermission('comment_on_req');
        $data['candownload_file'] = $this->myfunctions->isUserPermission('download_file');
        $data['canapprove_file'] = $this->myfunctions->isUserPermission('approve/revision_requests');
        $request_file = $this->Request_model->get_image_by_request_file_id($id);
        if($request_file[0]['customer_seen'] == '0'){
        $this->Welcome_model->update_data("request_files", array("customer_seen" => 1), array('id' => $id));
        $this->myfunctions->capture_project_activity($reqid,$id,'','view_project_by_customer','project_image_view',$login_user_id);
        }
        $this->Admin_model->update_data("request_file_chat", array("customer_seen" => '1'), array("request_file_id" => $id));
        // echo $this->db->last_query();
        $url = "customer/request/project_image_view/" . $id . "?id=" . $_GET['id'];
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url));
        $this->Welcome_model->update_data("message_notification", array("shown" => 1), array("user_id" => $login_user_id, "url" => $url));
        $designer_file = $this->Admin_model->get_requested_files($_GET['id'], "", "designer", array('1', '2'));
        $data['request'] = $this->Request_model->get_request_by_id($_GET['id']);
        if (empty($data['request'])) {
            redirect(base_url() . "customer/request/design_request");
        }
        $designer_file_count = $this->Admin_model->get_requested_files_count($_GET['id'], "", "designer", array('1', '2'), array('Reject', 'pending'));
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            //$this->publiclinksave($_GET['id'],$designer_file[$i]['id']);
            $designer_file[$i]['modified_date'] = $this->onlytimezone($designer_file[$i]['modified']);
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified_date'];
            
            $designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id'], "desc");
            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
            $designer_file[$i]['chat'][$j]['replies'] = $this->Request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
            $designer_file[$i]['chat'][$j]['profile_picture'] = $this->customfunctions->getprofileimageurl($designer_file[$i]['chat'][$j]['profile_picture']);
            $designer_file[$i]['chat'][$j]['created'] = $this->onlytimezone($designer_file[$i]['chat'][$j]['created']);
            foreach ($designer_file[$i]['chat'][$j]['replies'] as $key => $value) {
                $designer_file[$i]['chat'][$j]['replies'][$key]['profile_picture'] = $this->customfunctions->getprofileimageurl($value['profile_picture']);
            }   
//            $designer_file[$i]['chat'][$j]['msg_created'] = $this->onlytimezone($designer_file[$i]['chat'][$j]['created']);
//            $designer_file[$i]['chat'][$j]['created'] =   date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
                
            }
        }
        
        $alldraftpubliclinks = $this->Request_model->getallPubliclinkinfo($reqid);
        $dataa = $this->Request_model->get_request_by_id($_GET['id']);
       // $userstate = $this->Request_model->get_user_by_id($_SESSION['user_id']);
        $data['data'] = $this->customfunctions->getprojectteammembers($dataa[0]['customer_id'], $dataa[0]['designer_id']);
        //echo "<pre/>";print_r($dataa);
//        $customer = $this->Account_model->getuserbyid($dataa[0]['customer_id']);
//        if (!empty($customer)) {
//            $data['data'][0]['customer_name'] = $customer[0]['first_name'] . " " . $customer[0]['last_name'];
//			$data['data'][0]['is_trail'] = $customer[0]['is_trail'];
//            $data['data'][0]['phone'] = $customer[0]['phone'];
//            $data['data'][0]['can_trialdownload'] = $customer[0]['can_trialdownload'];
//            $data['data'][0]['is_trailverified'] = $customer[0]['is_trailverified'];
//        }
//        $designer = $this->Account_model->getuserbyid($dataa[0]['designer_id']);
//        if (!empty($designer)) {
//            $data['data'][0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
//            $data['data'][0]['designer_image'] = $designer[0]['profile_picture'];
//            $data['data'][0]['designer_sortname'] = substr($designer[0]['first_name'], 0, 1) . substr($designer[0]['last_name'], 0, 1);
//        }
//        $qa = array();
//            $qa = $this->Account_model->getuserbyid($customer[0]['qa_id']);
//            if (!empty($qa)) {
//                $data['data'][0]['qa_name'] = $qa[0]['first_name'] . " " . $qa[0]['last_name'];
//                $data['data'][0]['qa_image'] = $qa[0]['profile_picture'];
//            }
//        if (empty($qa)) {
//            $data['data'][0]['qa_name'] = '';
//            $data['data'][0]['qa_image'] = '';
//        }
//        
        $data['data'][0]['status'] = $dataa[0]['status'];
        $data['data'][0]['title'] = $dataa[0]['title'];
        $data['data'][0]['id'] = $dataa[0]['id'];
        $data['user'] = $this->Account_model->getuserbyid($login_user_id);
        $data['user'][0]['profile_picture'] = $this->customfunctions->getprofileimageurl($data['user'][0]['profile_picture']);
        $data['chat_request'] = $this->Request_model->get_chat_request_by_id($_GET['id']);
        $data['designer_file'] = $designer_file;
        $data['main_id'] = $id;
        $show_filesharing = $this->is_show_file_sharing();
        $data['designer_file_count'] = $designer_file_count;
        $notifications = $this->Request_model->get_notifications($login_user_id);
        $notification_number = $this->Request_model->get_notifications_number($login_user_id);
        $messagenotifications = $this->Request_model->get_messagenotifications($login_user_id);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($login_user_id);

        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);
        $main_user_data = $this->Admin_model->getuser_data($user_id);
        $data['main_user_data'] = $main_user_data;
        $data['alldraftpubliclinks'] = $alldraftpubliclinks;
        $data['sharedreqUser'] = $sharedreqUser;
        $data['shareddraftUser'] = $shareddraftUser;
        $data['draftrequest_id'] = $reqid;
//         echo "<pre/>";print_R($data);

        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data,"popup" => 0,"show_filesharing" => $show_filesharing));

        // $this->load->view('customer/view_files', $data);
        $this->load->view('customer/project-image-view', $data);
        $this->load->view('customer/customer_footer_1');
    }
    
    public function attach_file_process() {
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output['error'] = 'Error while uploading the files. Please try again.';
        $dateFolder = strtotime(date('Y-m-d H:i:s'));
        if (!(isset($_SESSION['temp_attachfolder_names']) && $_SESSION['temp_attachfolder_names'] != '')) {
            if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                $_SESSION['temp_attachfolder_names'] = './public/uploads/temp/' . $dateFolder;
            } else {
                $_SESSION['temp_attachfolder_names'] = '';
            }
        } else {
            if (!is_dir($_SESSION['temp_attachfolder_names'])) {
                if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                    $_SESSION['temp_attachfolder_names'] = './public/uploads/temp/' . $dateFolder;
                } else {
                    $_SESSION['temp_attachfolder_names'] = '';
                }
            }
        }
        if ($_SESSION['temp_attachfolder_names'] != '') {
            $files = $_FILES;
            $cpt = count($_FILES['preview_file']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['preview_file']['name'] = $files['preview_file']['name'][$i];
                $_FILES['preview_file']['type'] = $files['preview_file']['type'][$i];
                $_FILES['preview_file']['tmp_name'] = $files['preview_file']['tmp_name'][$i];
                $_FILES['preview_file']['error'] = $files['preview_file']['error'][$i];
                $_FILES['preview_file']['size'] = $files['preview_file']['size'][$i];
                $config = array(
                    'upload_path' => $_SESSION['temp_attachfolder_names'],
                    'allowed_types' => ALLOWED_FILE_TYPES,
                    'max_size' => '0',
                    'overwrite' => FALSE
                );
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload("preview_file")) {
                    $data = array($this->upload->data());
                    $data[0]['error'] = false;
                    $output['files'][] = $data;
                    $output['status'] = true;
                    $output['error'] = '';
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $error_data = array();
                    $error_data[0] = array();
                    $error_data[0]['file_name'] = $files['preview_file']['name'][$i];
                    $error_data[0]['error'] = true;
                    $error_data[0]['error_msg'] = strip_tags($data['error']);
                    $output['files'][] = $error_data;
                    $output['status'] = true;
                    $output['error'] = '';
                }
            }
        } else {
            $output['error'] = 'Directory not writable. Please try again.';
        }
        echo json_encode($output);
        exit;
    }
    
     public function delete_attachfile_from_folder() {
        $folderPath = $_SESSION['temp_attachfolder_names'];
        $folderPaths = $_SESSION['temp_attachfolder_names'] . '/*';
        if (!empty($_POST)) {
            $data = $_POST['file_name'];
            $files = glob($folderPaths); // get all file names
            $dir = FCPATH . $folderPath;
            $dirHandle = opendir($dir);
            while ($file = readdir($dirHandle)) {
                if ($file == $data) {
                    unlink($dir . "/" . $file); //give correct path,
                } else {}
            }
        }
    }

    public function project_info($id, $sec_id = '') {
        $this->myfunctions->checkloginuser("customer");
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        $this->Welcome_model->update_online();
        //$this->publiclinksave($id,'0');
        $publiclink = $this->Request_model->getPubliclinkinfo($id);
        $publicpermissions = $this->Request_model->getpublicpermissions($publiclink['id']);
        $sharedUser = $this->Request_model->getSharedpeopleinfobyreqID($id); 
        $canapprove_reject = $this->myfunctions->isUserPermission('approve/revision_requests');
        $cancomment_on_req = $this->myfunctions->isUserPermission('comment_on_req');
        $request_meta = $this->Category_model->get_quest_Ans($id);
        $sampledata = $this->Request_model->getSamplematerials($id);
        if ($id == "") {
            redirect(base_url() . "customer/request/design_request");
        }
        $checkUser = $this->Request_model->checkValidUser($id);
        if ($checkUser != 1) {
            redirect(base_url() . "customer/request/design_request");
        }
        $url = "customer/request/project_info/" . $id;
        $this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url));
        $this->Welcome_model->update_data("message_notification", array("shown" => 1), array("user_id" => $login_user_id, "url" => $url));
        $files = $this->Request_model->get_attachment_files($id, "designer");
        $file_chat_array = array();
        for ($i = 0; $i < sizeof($files); $i++) {
            $chat_of_file = $this->Request_model->get_file_chat($files[$i]['id'], "customer");
            if ($chat_of_file) {
                $file_chat_array[$i]['file_name'] = $files[$i]['file_name'];
                $file_chat_array[$i]['count'] = $chat_of_file;
                $file_chat_array[$i]['id'] = $files[$i]['id'];
                $files[$i]['chat_count'] = $chat_of_file;
            } else {
                $files[$i]['chat_count'] = "";
                $file_chat_array[$i]['id'] = "";
            }
            $notifications = $this->Request_model->get_notifications($login_user_id);
            $notification_number = $this->Request_model->get_notifications_number($login_user_id);
        }
        $this->Admin_model->update_data("request_discussions", array("customer_seen" => 1), array("request_id" => $id));
        if ($_POST['attach_file']) {
            if ($_FILES['file-upload']['name'] != "") {
                if (!is_dir('public/uploads/requests/' . $id)) {
                    $data = mkdir('./public/uploads/requests/' . $id, 0777, TRUE);
                }
                $uploadPATH = $_SESSION['temp_folder_names'];
                $uploadPATHs = str_replace("./","",$uploadPATH);
                $files = scandir($uploadPATH);
                $source = $uploadPATH.'/';
                $destination = './public/uploads/requests/' . $id.'/';
                if(UPLOAD_FILE_SERVER == 'bucket'){
                foreach ($files as $file) {

                if (in_array($file, array(".",".."))) continue;

                $staticName = base_url().$uploadPATHs.'/'.$file;
               //echo $staticName;exit;
                $config = array(
                    'upload_path' => 'public/uploads/requests/'. $id.'/',
                    'file_name' => $file
                );
               
                $this->s3_upload->initialize($config);
                $is_uploaded = $this->s3_upload->upload_multiple_file($staticName);
                if($is_uploaded['status'] == 1){
                   $delete[] = $source.$file; 
                   unlink('./tmp/tmpfile/' . basename($staticName));
                }
                //echo "<pre/>";print_r($is_uploaded);exit;
                
                }
                }else{
                foreach ($files as $file) {
                  if (in_array($file, array(".",".."))) continue;
                  // If we copied this successfully, mark it for deletion
                  //echo APPPATH.$source.$file;
                  $filepath = APPPATH.$source.$file;
                  if (copy($source.$file, $destination.$file)) {
                    $delete[] = $source.$file;
                  }
                }
                }
                // Delete all successfully-copied files
                foreach ($delete as $file) {
                  unlink($file);
                }
                rmdir($source);
                
                unset($_SESSION['temp_folder_names']);
                 if($is_uploaded['status'] == 1 || isset($filepath)){
                 if (isset($_POST['delete_file']) && $_POST['delete_file'] != '') {
                   // echo "<pre>"; print_R($_POST['delete_FL']);exit;
                    foreach ($_POST['delete_file'] as $SubFIlekey => $GetFIleName) {
                        // $GetFIleName = substr($SubFIleName, strrpos($SubFIleName, '/') + 1);
                        $request_files_data = array("request_id" => $id,
                            "user_id" => $created_user,
                            "user_type" => "customer",
                            "file_name" => $GetFIleName,
							"image_link" => $_POST['image_link'][$SubFIlekey],
                            "created" => date("Y-m-d H:i:s"));
                        //print_r($request_files_data);exit;
                        $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                    }
                    $this->myfunctions->capture_project_activity($id,'','','add_attachment','project_info',$login_user_id,1);
                }
                }else{
                    $this->session->set_flashdata('message_error', "Your file is not uploaded, Try Again!", 5);
                   redirect(base_url() . "customer/request/project-info/" . $id . '/' . $_POST['tab_status']);
                }
                redirect(base_url() . "customer/request/project-info/" . $id . '/' . $_POST['tab_status']);
            } else {
                $this->session->set_flashdata('message_error', "Please Upload Preview  Files.!", 5);
                redirect(base_url() . "customer/request/project-info/" . $id . '/' . $_POST['tab_status']);
            }
        }
        $data1 = $this->Request_model->get_request_by_id($id);
        $data1['customer_variables'] = $this->myfunctions->customerVariables($data1[0]['status']);
        $cat_data = $this->Category_model->get_category_byID($data1[0]['category_id']);
        $cat_name = $cat_data[0]['name'];
        $subcat_data = $this->Category_model->get_category_byID($data1[0]['subcategory_id']);
        $subcat_name = $subcat_data[0]['name'];
        $data1['cat_name'] = $cat_name;
        $data1['subcat_name'] = $subcat_name;
        $branddata = $this->Request_model->get_brandprofile_by_id($data1[0]['brand_id']);
        $brand_materials_files = $this->Request_model->get_brandprofile_materials_files($branddata[0]['id']);
        
        for($i=0;$i< sizeof($brand_materials_files);$i++){
          if($brand_materials_files[$i]['file_type'] == 'logo_upload'){
            $brand_materials_files['latest_logo'] =  $brand_materials_files[$i]['filename'];
               break;
          }
        }
        $team_member = $this->customfunctions->getprojectteammembers($data1[0]['customer_id'], $data1[0]['designer_id']);
        //echo "<pre>";print_r($brand_materials_files);
//        $customer = $this->Account_model->getuserbyid($data1[0]['customer_id']);
//        $qa = array();
//        if (!empty($customer)) {
//            $data1[0]['customer_name'] = $customer[0]['first_name'] . " " . $customer[0]['last_name'];
//            $qa = $this->Account_model->getuserbyid($customer[0]['qa_id']);
//            if (!empty($qa)) {
//                $data1[0]['qa_name'] = $qa[0]['first_name'] . " " . $qa[0]['last_name'];
//                $data1[0]['qa_image'] = $qa[0]['profile_picture'];
//            }
//        }
//        if (empty($qa)) {
//            $data1[0]['qa_name'] = '';
//            $data1[0]['qa_image'] = '';
//        }


    //    $designer = $this->Account_model->getuserbyid($data1[0]['designer_id']);

//        if (!empty($designer)) {
//            $data1[0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
//            $data1[0]['designer_image'] = $designer[0]['profile_picture'];
//            $data1[0]['designer_sortname'] = substr($designer[0]['first_name'], 0, 1) . substr($designer[0]['last_name'], 0, 1);
//        } else {
//            $request[0]['designer_name'] = "";
//            $request[0]['designer_image'] = "";
//            $request[0]['designer_sortname'] = "";
//        }
        //$chat_request = $this->Request_model->get_chat_request_by_id($id);
        $chat_request = $this->Request_model->get_customer_mainChat_request_by_id($id); 
        $timediff = $datetimediff = '';
        for ($j = 0; $j < count($chat_request); $j++) {
            if($chat_request[$j]['with_or_not_customer'] == 1){
            $chat_request[$j]['sender_data'] = $this->Admin_model->getuser_data($chat_request[$j]['sender_id']);
            //$chat_request[$j]['profile_picture'] = $this->customfunctions->getprofileimageurl($chat_request[$j]['profile_picture']);
            $chat_request[$j]['msg_created'] = $this->onlytimezone($chat_request[$j]['chat_created_date']);
            //$chat_request[$j]['chat_created_date'] = $chat_request[$j]['msg_created'];
            
            $chat_request[$j]['chat_created_date1'] =   date('M-d-Y h:i A', strtotime($chat_request[$j]['msg_created']));
            //echo "<pre/>";print_r($chat_request[$j]['chat_created_date1']);
            $currentdate = date('M-d-Y');
            $datetime = new DateTime($chat_request[$j]['chat_created_date1']);
            $date = $datetime->format('M-d-Y');
            $time = $datetime->format('h:i A');
            //$chat_request[$j]['chat_created_time'] = $time;
            
            if($timediff == '' && $datetimediff = ''){
             $chat_request[$j]['chat_created_datee'] =   $date; 
             $timediff =  $chat_request[$j]['chat_created_datee'];
             $datetimediff =   $chat_request[$j]['chat_created_date1'];
            }
            $sincedate = $chat_request[$j]['chat_created_date1'];
            $datetime1 = new DateTime($datetimediff);
            $datetime2 = new DateTime($sincedate);
            $mindiff = $datetime1->diff($datetime2);
            if ($mindiff->i >= BATCH_MESSAGE_TIMELIMIT || $chat_request[$j]['sender_id'] != $chat_request[$j - 1]['sender_id']) {
                $datetimediff = $sincedate;
                $timeee = date('h:i A', strtotime($sincedate));
                $chat_request[$j]['chat_created_time'] = $timeee;
                $chat_request[$j]['profile_picture'] = $this->customfunctions->getprofileimageurl($chat_request[$j]['profile_picture']);
                $chat_request[$j]['fname'] = $chat_request[$j]['first_name'];
                $chat_request[$j]['shared_name'] = $chat_request[$j]['shared_user_name'];
            }
            if($date != $timediff){
                $timediff = $date;
                $chat_request[$j]['chat_created_datee'] = $date;
                if($date == $currentdate){
                    $chat_request[$j]['chat_created_datee'] = "Today";
                }
            }
        }
        }
//        echo "<pre>";print_r($data1[0]['designer_attachment']);
        for ($i = 0; $i < count($data1[0]['designer_attachment']); $i++) {  
            if($data1[0]['designer_attachment'][$i]['status'] != 'pending' && $data1[0]['designer_attachment'][$i]['status'] != 'Reject'){
                $data1[0]['approved_attachment'] =  $data1[0]['designer_attachment'][$i]['id']; break;
            }
            $data1[0]['designer_attachment'][$i]['img_created'] = $this->onlytimezone($data1[0]['designer_attachment'][$i]['created']);
            $data1[0]['designer_attachment'][$i]['created'] =   date('d M, Y', strtotime($data1[0]['designer_attachment'][$i]['img_created']));
        }
        
//echo "<pre/>";print_R($data1[0]['approved_attachment']);exit;
//         echo "<pre>";print_r($data1);
        $request_id = $data1[0]['id'];
        $activities = $this->myfunctions->projectactivities($id, 'user_shown');
        $show_filesharing = $this->is_show_file_sharing();
        $notifications = $this->Request_model->get_notifications($login_user_id);
        $notification_number = $this->Request_model->get_notifications_number($login_user_id);
        $messagenotifications = $this->Request_model->get_messagenotifications($login_user_id);
        $messagenotification_number = $this->Request_model->get_messagenotifications_number($login_user_id);
        $main_user_data = $this->Admin_model->getuser_data($created_user);

        $profile_data = $this->Admin_model->getuser_data($_SESSION['user_id']);

        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));
        $this->load->view('customer/project-info', array("data" => $data1, "mainchat_request" => $chat_request,'publiclink' => $publiclink,'publicpermissions' => $publicpermissions,
            'sharedUser' => $sharedUser,"branddata" => $branddata, "brand_materials_files" => $brand_materials_files,  "request_id" => $request_id, "chat_request" => $chat_request, "file_chat_array" => $file_chat_array,'canapprove_reject' => $canapprove_reject,'cancomment_on_req' => $cancomment_on_req,'main_user_data' => $main_user_data,"team_member"=>$team_member,'activities' => $activities,
            "show_filesharing" => $show_filesharing,
            "request_meta" => $request_meta,
            "sampledata" => $sampledata));
        $this->load->view('customer/footer_customer');
    }

    public function send_message_request() {
        // print_r($_POST);
        if(isset($_POST['shared_user_name']) && $_POST['shared_user_name'] != ''  && $_POST['shared_user_name'] != '1'){
          $_POST['sender_id'] = '';  
        }else{
            $_POST['sender_id'] = $_POST['sender_id'];
        }
        $admin_id = $this->load->get_var('admin_id');
        $login_user_id = $this->load->get_var('login_user_id'); 
        $sender_type = $_POST['sender_type'];
        if ($sender_type == 'designer') {
            $_POST['designer_seen'] = '1';
        } elseif ($sender_type == 'customer') {
            $_POST['customer_seen'] = '1';
        }
        //die();
        
        $_POST['created'] = date("Y-m-d H:i:s");
        $_POST['shared_user_name'] = isset($_POST['shared_user_name']) ? $_POST['shared_user_name']: '';
        $request_data = $this->Request_model->get_request_by_id($_POST['request_id']);
        if ($login_user_id != "") {
            $u_id = $login_user_id;
        } else {
            if ($request_data[0]["created_by"] == 0) {
                $u_id = $request_data[0]["customer_id"];
            } else {
                $u_id = $request_data[0]["created_by"];
            }
        }
        $count_cust_rev = $request_data[0]['count_customer_revision'];
        $customer_data = $this->Admin_model->getuser_data($request_data[0]['customer_id']);
        $getsubscriptionplan = $this->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
        if($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])){
            $turn_around_days = $customer_data[0]['turn_around_days'];
        }else{
            $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
        }
        $success = $this->Welcome_model->insert_data("request_discussions", $_POST);
        // $_POST['designer_seen'] = "1";
        if($success) {
            if ($sender_type == 'customer') {
                $subcat_data = $this->Category_model->get_category_byID($request_data[0]['subcategory_id']);
                $customer_data = $this->Admin_model->getuser_data($request_data[0]['customer_id']);
                if ($request_data[0]['status'] == 'checkforapprove') {
                    $count = $count_cust_rev + 1;
                    $expected = $this->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"),$customer_data[0]['plan_name'],$request_data[0]['category_bucket'],$subcat_data[0]['timeline'],$turn_around_days);
                    $this->Welcome_model->update_data("requests", array("status" => "disapprove", "status_designer" => "disapprove", "status_admin" => "disapprove", "status_qa" => "disapprove", "who_reject" => 1, "count_customer_revision" => $count,"latest_update" => date("Y-m-d H:i:s"),"expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    $this->myfunctions->capture_project_activity($_POST['request_id'],'','','checkforapprove_to_disapprove','send_message_request',$u_id,1);
                    echo "yes_";
                    
                    /* save notifications when upload draft design */
                    if($request_data[0]['dummy_request'] != 1){
                    $this->myfunctions->show_notifications($_POST['request_id'],$u_id,"\"".$request_data[0]['title']."\" Project goes in revision  from checkforapprove..!","admin/dashboard/view_request/" . $_POST['request_id'],$admin_id);
                    $this->myfunctions->show_notifications($_POST['request_id'],$u_id,"\"".$request_data[0]['title']."\" Project goes in revision  from checkforapprove..!","qa/dashboard/view_project/" . $_POST['request_id'],$_SESSION['qa_id']);
                    $this->myfunctions->show_notifications($_POST['request_id'],$u_id,"\"".$request_data[0]['title']."\" Project goes in revision  from checkforapprove..!","designer/request/project_info/" . $_POST['request_id'],$_POST['reciever_id']);
                    }
                    /** end save notifications when upload draft design * */
                }else{
                 echo "no_";
                }
            }
           // $rtitle = $this->Request_model->get_request_by_id($_POST['request_id']);
            if (!empty($request_data)) {
                /*******insert messages notifications*******/ 
                if($request_data[0]['dummy_request'] != 1){
                /* Message notification for customer */
                $this->myfunctions->show_messages_notifications($request_data[0]['title'],$_POST['message'],$_POST['request_id'],$u_id,$_POST['reciever_type'] . "/request/project_info/" . $_POST['request_id'],$_POST['reciever_id']);
                /* Message notification for qa */
                $this->myfunctions->show_messages_notifications($request_data[0]['title'],$_POST['message'],$_POST['request_id'],$u_id,"qa/dashboard/view_project/" . $_POST['request_id'],$_SESSION['qa_id']);
                /* Message notification for admin */
                $this->myfunctions->show_messages_notifications($request_data[0]['title'],$_POST['message'],$_POST['request_id'],$u_id,"admin/dashboard/view_request/" . $_POST['request_id'],$admin_id);
                }
                /*******end insert messages notifications*******/
            } else {
                $data['title'] = "New Message Arrived";
                $data['created'] = date("Y:m:d H:i:s");
                $this->Welcome_model->insert_data("notification", $data);
            }
            $this->Request_model->get_notifications($login_user_id);
        } else {
            echo 'no_';
        }
        echo $success;
    }
    
    public function send_message() {
        $admin_id = $this->load->get_var('admin_id');
        $login_user_id = $this->load->get_var('login_user_id');
        $userdata = $this->Admin_model->getuser_data($_POST['receiver_id']);
       // echo "<pre>";print_r($_POST);exit;
        if ($_POST['sender_role'] == "customer") {
            $request_id = $_POST['request_id'];
            $request_data = $this->Request_model->get_request_by_id($_POST['request_id']);
            if ($login_user_id != "") {
                $u_id = $login_user_id;
            } else {
                if ($request_data[0]["created_by"] == 0) {
                    $u_id = $request_data[0]["customer_id"];
                } else {
                    $u_id = $request_data[0]["created_by"];
                }
            }
            $count_cust_rev = $request_data[0]['count_customer_revision'];
            $customer_data = $this->Admin_model->getuser_data($request_data[0]['customer_id']);
            $getsubscriptionplan = $this->Request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
             if($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])){
                $turn_around_days = $customer_data[0]['turn_around_days'];
            }else{
                $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
            }
            $subcat_data = $this->Category_model->get_category_byID($request_data[0]['subcategory_id']);
            $expected = $this->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"),$customer_data[0]['plan_name'],$request_data[0]['category_bucket'],$subcat_data[0]['timeline'],$turn_around_days);
            unset($_POST['request_id']);
            $_POST['created'] = date("Y-m-d H:i:s");
            if ($this->Welcome_model->insert_data("request_file_chat", $_POST)) {
                //if($request_data[0]['status'] != "approved" && $request_data[0]['status'] != "draft" && $request_data[0]['status'] != "assign"){
                if ($request_data[0]['status'] == "checkforapprove") {
                    $count = $count_cust_rev + 1;
                    $this->Welcome_model->update_data("requests", array("status" => "disapprove", "status_designer" => "disapprove", "status_admin" => "disapprove", "status_qa" => "disapprove", "who_reject" => 1, "count_customer_revision" => $count,"latest_update" => date("Y-m-d H:i:s"),"expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $request_id));
                    $this->myfunctions->capture_project_activity($request_id,$_POST['request_file_id'],'','checkforapprove_to_disapprove','send_message',$u_id,1);
                if($request_data[0]['dummy_request'] != 1){
                 $this->myfunctions->show_notifications($_POST['request_id'],$u_id,"\"".$request_data[0]['title']."\" Project goes into revision  from checkforapprove.!","admin/dashboard/view_request/" . $_POST['request_id'],$admin_id);
                 $this->myfunctions->show_notifications($_POST['request_id'],$u_id,"\"".$request_data[0]['title']."\" Project goes into revision  from checkforapprove..!","qa/dashboard/view_project/" . $_POST['request_id'],$userdata[0]['qa_id']);
                 $this->myfunctions->show_notifications($_POST['request_id'],$u_id,"\"".$request_data[0]['title']."\" Project goes into revision  from checkforapprove..!","designer/request/project_info/" . $_POST['request_id'],$_POST['receiver_id']);   
                }
                 echo "yes_";
                }else{
                    echo "no_";
                }
                echo $this->db->insert_id();
            } else {
                echo "no_";
            }
            /*******insert messages notifications*******/
            if($request_data[0]['dummy_request'] != 1){
            /* Message notification for customer */
            $this->myfunctions->show_messages_notifications($request_data[0]['title'], $_POST['message'], $request_id, $u_id, "designer/request/project_image_view/".$_POST['request_file_id']."?id=".$request_id, $_POST['receiver_id']);
            /* Message notification for qa */
            $this->myfunctions->show_messages_notifications($request_data[0]['title'], $_POST['message'], $request_id, $u_id, "qa/dashboard/view_files/".$_POST['request_file_id']."?id=".$request_id, $userdata[0]['qa_id']);
            /* Message notification for admin */
            $this->myfunctions->show_messages_notifications($request_data[0]['title'], $_POST['message'], $request_id, $u_id, "admin/dashboard/view_files/".$_POST['request_file_id']."?id=".$request_id, $admin_id);
            }

            /*******end insert messages notifications*******/
        } 
        exit;
    }
    
        public function downloadzip() {
        if (isset($_POST)) {
            if(isset($_POST['front_user_id']) && $_POST['front_user_id'] != ""){
                $login_user_id = $_POST['front_user_id'];
            }else{
                $login_user_id = $this->load->get_var('login_user_id'); 
            }
            $this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['draft_id'],'','project_download_by_customer','downloadzip',$login_user_id);
            if(UPLOAD_FILE_SERVER == 'bucket'){
            $file_name = FS_PATH;
            $s3Path = str_replace("https://", "http://", $file_name);
            $source_filename = substr($_POST['srcfile'], strrpos($_POST['srcfile'], '/') + 1);
            $this->load->helper('download');
            $path = file_get_contents(FS_PATH . $_POST['srcfile']); // get file name
            $name = $source_filename; // new name for your file
            force_download($name, $path);
            }else{
            $file_name = FS_PATH;
            $s3Path = str_replace("https://", "http://", $file_name);
            $this->zip->read_file($s3Path . $_POST['srcfile']);
            $this->zip->read_file($s3Path . $_POST['prefile']);
            if ($_POST['prefile'] && $_POST['srcfile'] == '') {
                $this->zip->download($_POST['prefile'] . ".zip");
            } else {
                $this->zip->download($_POST['project_name'] . ".zip");
            }
            }
        }
    }
 
    
   

    public function get_ajax_all_request() {
        $dataArray = $this->input->get('title');
        $dataStatus = $this->input->get('status');
        $dataStatus = explode(",", $dataStatus);
        $customerRequest = $this->Request_model->ajax_getall_request(array('keyword' => $dataArray), $dataStatus, 'priority');
        $activeTab = 0;
        foreach ($customerRequest as $project) {
            if ($project['status'] == "assign") {
                $activeTab++;
            }
        }
        for ($i = 0; $i < sizeof($customerRequest); $i++) {
            $customerRequest[$i]['total_chat'] = $this->Request_model->get_chat_number_customer($customerRequest[$i]['id'], $_SESSION['user_id'], "customer", "1");
            $customerRequest[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number_customer($customerRequest[$i]['id'], $_SESSION['user_id'], "customer", "1");
            /* new */
            $id = $customerRequest[$i]['id'];
            $data = $this->Request_model->get_request_by_id($id);
            $customerRequest[$i]['files_new'] = $this->Request_model->get_new_attachment_files_for_customer($customerRequest[$i]['id'], array('customerreview', 'Approve'));
            $getfileid = $this->Request_model->get_attachment_files($customerRequest[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            if ($customerRequest[$i]['status'] == "active" || $customerRequest[$i]['status'] == "disapprove") {
                $customerRequest[$i]['expected'] = $this->myfunctions->check_timezone($customerRequest[$i]['latest_update']);
            } elseif ($customerRequest[$i]['status'] == "checkforapprove") {
                $customerRequest[$i]['deliverydate'] = $this->onlytimezone($customerRequest[$i]['latest_update']);
            } elseif ($customerRequest[$i]['status'] == "approved") {
                $customerRequest[$i]['approvddate'] = $this->onlytimezone($customerRequest[$i]['approvaldate']);
            }
            $customerRequest[$i]['comment_count'] = $commentcount;
            $customerRequest[$i]['files'] = $this->Request_model->get_attachment_files_for_customer($customerRequest[$i]['id'], array('customerreview', 'Approve'), "designer");
        }
        for ($i = 0; $i < sizeof($customerRequest); $i++) {
            if ($customerRequest[$i]['status'] == 'assign' || $customerRequest[$i]['status'] == 'checkforapprove' || $customerRequest[$i]['status'] == 'active' || $customerRequest[$i]['status'] == 'disapprove' || $customerRequest[$i]['status'] == 'pending') {
                ?>                            
                <div id="prior_data">

                <?php
                for ($i = 0; $i < count($customerRequest); $i++) {
                    if ($customerRequest[$i]['status'] == "active" || $customerRequest[$i]['status'] == "checkforapprove" || $customerRequest[$i]['status'] == "disapprove") {
                        ?>
                            <!-- Pro Box Start -->
                            <div class="col-md-2 col-sm-6">
                                <div class="pro-productss pro-box-r1">
                                    <div class="pro-box-r2">
                                        <div class="pro-inbox-r1">
                                            <div class="pro-inleftbox">
                                                <p class="pro-a">
                                                    <?php
                                                    if ($customerRequest[$i]['status'] == "active") {
                                                        echo "Expected on";
                                                    } elseif ($customerRequest[$i]['status'] == "checkforapprove") {
                                                        echo "Delivered on";
                                                    } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                        echo "Expected on";
                                                    }
                                                    ?>
                                                </p>
                                                <p class="pro-b"><a href="#">
                                                        <?php
                                                        if ($customerRequest[$i]['status'] == "active") {
                                                            echo date('m/d/Y', strtotime($customerRequest[$i]['expected']));
                                                        } elseif ($customerRequest[$i]['status'] == "checkforapprove") {
                                                            echo date('m/d/Y', strtotime($customerRequest[$i]['deliverydate']));
                                                        } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                            echo date('m/d/Y', strtotime($customerRequest[$i]['expected']));
                                                        }
                                                        ?>
                                                    </a></p>
                                            </div>
                                            <div class="pro-inrightbox">
                        <?php if ($customerRequest[$i]['status'] != "assign") { ?>
                                                    <p class="green">Active </p>
                        <?php } ?>
                                            </div>
                                        </div>
                                        <hr class="hr-a">   
                                        <!-- <script type="text/javascript">alert("<?php echo $customerRequest[$i]['id']; ?>")</script> -->
                                        <a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/1">
                                            <h3 class="pro-head space-b" title="<?php echo $customerRequest[$i]['title']; ?>"><?php echo substr($customerRequest[$i]['title'], 0, 40); ?></h3>
                                            <p class="pro-b"><?php echo $customerRequest[$i]['category']; ?></p>
                                            <p class="space-a"></p>
                                            <p class="neft">
                                                <?php
                                                if ($customerRequest[$i]['status'] == "checkforapprove") {
                                                    $status = "Review your design";
                                                    $color = "bluetext";
                                                } elseif ($customerRequest[$i]['status'] == "active") {
                                                    $status = "Design In Progress";
                                                    $color = "blacktext";
                                                } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                    $status = "Revision In Progress";
                                                    $color = "orangetext ";
                                                } elseif ($customerRequest[$i]['status'] == "pending" || $customerRequest[$i]['status'] == "assign") {
                                                    $status = "In Queue";
                                                    $color = "greentext ";
                                                } else {
                                                    $status = "";
                                                    $color = "greentext ";
                                                }
                                                //echo $status;
                                                ?>
                                                <?php if ($status == 'In Queue'): ?>
                                                    <span class="gray text-uppercase">
                            <?php echo $status; ?>
                                                    </span>
                        <?php else: ?>
                                                    <span class="green <?php echo $color; ?> text-uppercase">
                            <?php echo $status; ?>
                                                    </span>
                        <?php endif ?>
                                            </p>
                                        </a>
                                    </div>

                                    <div class="pro-box-r3">
                                        <p class="pro-a">Designer</p>
                                        <p class="space-a"></p>
                                        <div class="pro-circle-list clearfix">
                                            <div class="pro-circle-box">
                                                <a href="#"><figure class="pro-circle-img">
                        <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                        <?php } else { ?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                        <?php } ?>
                                                    </figure>
                                                    <p class="pro-circle-txt text-center"><?php echo $customerRequest[$i]['designer_first_name']; ?></p></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="pro-box-r4">
                                        <div class="pro-inbox-r1">
                                            <div class="pro-inleftbox">
                                                <p class="pro-a inline-per">
                                                    <a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/1">
                                                        <span class="inline-imgsssx">
                                                            <img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                        <?php if ($customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count'] != 0) { ?>
                                                                <span class="numcircle-box">
                            <?php echo $customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count']; ?>
                                                                </span>
                                                            <?php } ?>
                                                        </span>
                                                    </a>
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <a  style="position: relative;"  href="<?php echo base_url() ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/1">
                                                        <span class="inline-imgsssx">
                                                            <img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                        <?php if (count($customerRequest[$i]['files_new']) != 0) { ?>
                                                                <span class="numcircle-box">
                            <?php echo count($customerRequest[$i]['files_new']); ?>
                                                                </span>
                        <?php } ?>
                                                        </span>
                        <?php echo count($customerRequest[$i]['files']); ?>
                                                    </a>
                                                </p>
                                            </div>
                                            <div class="pro-inrightbox">
                                                <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>customer/request/deleteproject/<?php echo $customerRequest[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <!-- Pro Box End -->
                        <?php
                    }
                }
                for ($i = 0; $i < count($customerRequest); $i++) {
                    if ($customerRequest[$i]['status'] != "active" && $customerRequest[$i]['status'] != "checkforapprove" && $customerRequest[$i]['status'] != "disapprove") {
                        ?>
                            <!-- Pro Box Start -->
                            <div class="col-md-2 col-sm-6">
                                <div class="pro-productss pro-box-r1">
                                    <div class="pro-box-r2">
                                        <div class="pro-inbox-r1" style="padding: 7px 0px;">
                                            <div class="pro-inleftbox">
                                                <p class="pro-a">In-Queue</p>
                                                <!-- <p class="pro-b"><a href="#"> -->
                                                <?php
                                                // $created_date =  date($customerRequest[$i]['dateinprogress']);
                                                // $expected_date = date('m/d/Y', strtotime($created_date . ' +1 day'));
                                                // if ( $customerRequest[ $i ][ 'modified' ] == "" ) {
                                                //     echo $expected_date;
                                                // } else {
                                                //     echo date( "m/d/Y", strtotime( $customerRequest[ $i ][ 'modified' ] ) );
                                                // }
                                                ?>
                                                <!-- </a></p> -->
                                            </div>
                                            <div class="pro-inrightbox">
                                                        <?php if ($customerRequest[$i]['status'] == "assign") { ?>
                                                    <p class="select-pro-a pro-a">
                                                        Priority
                                                        <select onchange="prioritize(this.value, <?php echo $customerRequest[$i]['priority'] ?>,<?php echo $customerRequest[$i]['id'] ?>)">
                            <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                                                <option value="<?php echo $j; ?>"
                                                        <?php
                                                        if ($j == $customerRequest[$i]['priority']) {
                                                            echo "selected";
                                                        }
                                                        ?>
                                                                        ><?php echo $j; ?></option>                                             
                            <?php } ?>
                                                        </select>

                                                    </p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <hr class="hr-a">   
                                        <!-- <script type="text/javascript">alert("<?php echo $customerRequest[$i]['id']; ?>")</script> -->
                                        <a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/1">
                                            <h3 class="pro-head space-b" title="<?php echo $customerRequest[$i]['title']; ?>"><?php echo substr($customerRequest[$i]['title'], 0, 40); ?></h3>
                                            <p class="pro-b"><?php echo $customerRequest[$i]['category']; ?></p>
                                            <p class="space-a"></p>
                                            <p class="neft">
                                                <?php
                                                if ($customerRequest[$i]['status'] == "checkforapprove") {
                                                    $status = "Review your design";
                                                    $color = "greentext";
                                                } elseif ($customerRequest[$i]['status'] == "active") {
                                                    $status = "Design In Progress";
                                                    $color = "blacktext";
                                                } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                    $status = "Revision In Progress";
                                                    $color = "orangetext ";
                                                } elseif ($customerRequest[$i]['status'] == "pending" || $customerRequest[$i]['status'] == "assign") {
                                                    $status = "In Queue";
                                                    $color = "greentext ";
                                                } else {
                                                    $status = "";
                                                    $color = "greentext ";
                                                }
                                                //echo $status;
                                                ?>
                        <?php if ($status == 'In Queue'): ?>
                                                    <span class="gray text-uppercase">
                            <?php echo $status; ?>
                                                    </span>
                        <?php else: ?>
                                                    <span class="green text-uppercase">
                            <?php echo $status; ?>
                                                    </span>
                        <?php endif ?>
                                            </p>
                                        </a>
                                    </div>

                                    <div class="pro-box-r3">
                                        <p class="pro-a">Designer</p>
                                        <p class="space-a"></p>
                                        <div class="pro-circle-list clearfix">
                                            <div class="pro-circle-box">
                                                <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                                <!-- <a href="#"><figure class="pro-circle-img">
                        <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                        <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                        <?php } ?>
                                                </figure>
                                                <p class="pro-circle-txt text-center"><?php echo $customerRequest[$i]['designer_first_name']; ?></p></a> -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="pro-box-r4">
                                        <div class="pro-inbox-r1">
                                            <div class="pro-inleftbox">
                                                <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/1"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php if ($customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count'] != 0) { ?>
                                                            <span class="numcircle-box">
                                                                    <?php echo $customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count']; ?>
                                                            </span>
                                                            <?php } ?></a>
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/1">
                                                        <span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                        <?php if (count($customerRequest[$i]['files_new']) != 0) { ?>
                                                                <span class="numcircle-box">
                            <?php echo count($customerRequest[$i]['files_new']); ?>
                                                                </span>
                        <?php } ?>
                                                        </span>
                        <?php echo count($customerRequest[$i]['files']); ?>
                                                    </a>
                                                </p>
                                            </div>
                                            <div class="pro-inrightbox">
                                                <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>customer/request/deleteproject/<?php echo $customerRequest[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <!-- Pro Box End -->
                                <?php }
                            }
                            ?>  

                </div>  
                <p class="space-e"></p>        
            <?php } elseif ($customerRequest[$i]['status'] == 'draft') { ?>
                <div class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">
                    <!-- Drafts -->
                    <div class="product-list-show">
                        <div class="row">
                            <!-- Pro Box Start -->  
                <?php for ($i = 0; $i < sizeof($customerRequest); $i++) { ?>
                                <div class="col-md-2 col-sm-6">         
                                    <div class="pro-productss pro-box-r1">
                                        <div class="pro-box-r2">
                                            <div class="pro-inbox-r1">
                                                <div class="pro-inleftbox">
                                                    <p class="pro-a">Drafts <?php //echo $i;   ?></p>
                                                </div>
                                            </div>
                                            <hr class="hr-a">   
                                            <a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/2">
                                                <h3 class="pro-head space-b" title="<?php echo $customerRequest[$i]['title']; ?>"><?php echo substr($customerRequest[$i]['title'], 0, 40); ?></h3>
                                                <!-- <p class="pro-b"><?php echo $dp['category']; ?></p> -->
                                                <p class="space-a"></p>
                                                <p class="neft"><span class="gray text-uppercase">
                    <?php echo $customerRequest[$i]['status']; ?>
                                                    </span></p>
                                            </a>
                                        </div>

                                        <div class="pro-box-r3">
                                                            <?php if ($customerRequest[$i]['designer_first_name']) { ?>
                                                <p class="pro-a">Designer</p>
                                                <p class="space-a"></p>

                                                <div class="pro-circle-list clearfix">
                                                    <div class="pro-circle-box">
                                                        <a href="#"><figure class="pro-circle-img">

                        <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                                                <?php } ?>
                                                                    <!-- <img src="<?php echo base_url() ?>theme/customer-assets/images/pro1.png" class="img-responsive">  -->
                                                            </figure>
                                                            <p class="pro-circle-txt text-center"><?php echo $customerRequest[$i]['designer_first_name']; ?></p></a>


                                                    </div>

                                                </div>
                                                                <?php } else { ?>
                                                <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                                            <?php } ?>
                                        </div>

                                        <div class="pro-box-r4">
                                            <div class="pro-inbox-r1">
                                                <div class="pro-inleftbox">
                                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/2"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                    <?php if ($customerRequest[$i]['total_chat'] != 0) { ?>
                                                                <span class="numcircle-box">
                        <?php echo $customerRequest[$i]['total_chat']; ?>
                                                                </span>
                    <?php } ?></a>
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        <a href="<?php echo base_url() ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/2"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13"><?php if (isset($customerRequest[$i]['file_output'])): echo count($customerRequest[$i]['file_output']);
                    endif;
                    ?></a>

                                                    </p>
                                                </div>
                                                <div class="pro-inrightbox">
                                                    <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>customer/request/deleteproject/<?php echo $customerRequest[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- Pro Box End -->
                <?php } ?>          
                        </div>
                    </div>

                </div>
                                                <?php }elseif ($customerRequest[$i]['status'] == 'approved') { ?>
                <div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
                    <!-- Complete Projects -->
                    <div class="product-list-show">
                        <div class="row">
                            <!-- Pro Box Start -->
                <?php for ($i = 0; $i < count($customerRequest); $i++) { ?>
                                <div class="col-md-2 col-sm-6">                 
                                    <div class="pro-productss pro-box-r1">
                                        <div class="pro-box-r2">
                                            <div class="pro-inbox-r1">
                                                <div class="pro-inleftbox">
                                                    <p class="pro-a">
                    <?php
                    if ($customerRequest[$i]['status'] == "approved") {
                        echo "Approved on";
                    }
                    ?>
                                                    </p>
                                                    <p class="pro-b"><a href="">
                    <?php
                    if ($customerRequest[$i]['status'] == "approved") {
                        echo date('m/d/Y', strtotime($customerRequest[$i]['approvddate']));
                    }
                    ?>
                                                        </a></p>
                                                </div>                                      
                                            </div>
                                            <hr class="hr-a">   
                                            <a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/3">
                                                <h3 class="pro-head space-b" title="<?php echo $customerRequest[$i]['title']; ?>"><?php echo substr($customerRequest[$i]['title'], 0, 40); ?></h3>
                                                <p class="pro-b"><?php echo $customerRequest[$i]['category']; ?></p>
                                                <p class="space-a"></p>
                                                <p class="neft"><span class="green text-uppercase">
                                                        <?php
                                                        if ($customerRequest[$i]['status'] == " checkforapprove") {
                                                            $status = "Review your design";
                                                            $color = "greentext";
                                                        } elseif ($customerRequest[$i]['status'] == "active") {
                                                            $status = "Design In Progress";
                                                            $color = "blacktext";
                                                        } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                            $status = "Revision In Progress";
                                                            $color = "orangetext ";
                                                        } elseif ($customerRequest[$i]['status'] == "pending" || $customerRequest[$i]['status'] == "assign") {
                                                            $status = "In Queue ";
                                                            $color = "greentext ";
                                                        } else {
                                                            $status = "Completed";
                                                            $color = "greentext ";
                                                        }
                                                        echo $status;
                                                        ?>
                                                    </span></p></a>
                                        </div>

                                        <div class="pro-box-r3">
                                            <p class="pro-a">Designed by</p>
                                            <p class="space-a"></p>
                                            <div class="pro-circle-list clearfix">
                                                <div class="pro-circle-box">
                                                    <a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/3"><figure class="pro-circle-img">
                    <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                    <?php } else { ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                    <?php } ?>
                                                                <!-- <img src="<?php echo base_url() ?>theme/customer-assets/images/pro1.png" class="img-responsive"> -->
                                                        </figure>
                                                        <p class="pro-circle-txt text-center"><?php echo $customerRequest[$i]['designer_first_name']; ?></p></a>
                                                </div>

                                                <!-- <div class="pro-circle-box">
                                                    <a href=""><figure class="pro-circle-plus">
                                                        <span class="plus-text"><span class="plus-sizebig">+</span> <br> 05</span>
                                                    </figure></a>
                                                </div> -->

                                            </div>
                                        </div>

                                        <div class="pro-box-r4">
                                            <div class="pro-inbox-r1">
                                                <div class="pro-inleftbox">
                                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/3"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                                                                <?php if ($customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count'] != 0) { ?>
                                                                <span class="numcircle-box">
                                                                <?php echo $customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count']; ?>
                                                                </span>
                    <?php } ?></a></p>
                                                </div>
                                                <div class="pro-inrightbox">
                                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/3"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                                <?php if (count($customerRequest[$i]['files_new']) != 0) { ?>
                                                                    <span class="numcircle-box">
                        <?php echo count($customerRequest[$i]['files_new']); ?>
                                                                    </span>
                    <?php } ?>
                                                            </span>
                    <?php echo count($customerRequest[$i]['files']); ?>
                                                        </a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- Pro Box End -->
                <?php } ?>          
                        </div>
                    </div>
                    <p class="space-e"></p>

                    <!-- <div class="loader">
                        <a class="loader-link text-uppercase" href=""><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-loader.png" class="img-responsive" width="28"> Loading</a>
                    </div> -->
                </div>
                <?php
            }
        }
    }

    public function get_ajax_all_request_list() {
        $dataArray = $this->input->get('title');
        $dataStatus = $this->input->get('status');
        $dataStatus = explode(",", $dataStatus);
        $customerRequest = $this->Request_model->ajax_getall_request(array('keyword' => $dataArray), $dataStatus, 'priority');
        $activeTab = 0;
        foreach ($customerRequest as $project) {
            if ($project['status'] == "assign") {
                $activeTab++;
            }
        }
        for ($i = 0; $i < sizeof($customerRequest); $i++) {
            $customerRequest[$i]['total_chat'] = $this->Request_model->get_chat_number($customerRequest[$i]['id'], $customerRequest[$i]['customer_id'], $customerRequest[$i]['designer_id'], "customer");
            $customerRequest[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number_customer($customerRequest[$i]['id'], $_SESSION['user_id'], "customer", "1");
            /* new */
            $id = $customerRequest[$i]['id'];
            $data = $this->Request_model->get_request_by_id($id);
            $customerRequest[$i]['files_new'] = $this->Request_model->get_new_attachment_files_for_customer($customerRequest[$i]['id'], array('customerreview', 'Approve'));
            $getfileid = $this->Request_model->get_attachment_files($customerRequest[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->Request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            if ($customerRequest[$i]['status'] == "active" || $customerRequest[$i]['status'] == "disapprove") {
                $customerRequest[$i]['expected'] = $this->myfunctions->check_timezone($customerRequest[$i]['latest_update']);
            } elseif ($customerRequest[$i]['status'] == "checkforapprove") {
                $customerRequest[$i]['deliverydate'] = $this->onlytimezone($customerRequest[$i]['latest_update']);
            } elseif ($customerRequest[$i]['status'] == "approved") {
                $customerRequest[$i]['approvddate'] = $this->onlytimezone($customerRequest[$i]['approvaldate']);
            }
            $customerRequest[$i]['comment_count'] = $commentcount;
            $customerRequest[$i]['files'] = $this->Request_model->get_attachment_files_for_customer($customerRequest[$i]['id'], array('customerreview', 'Approve'), "designer");
        }
        for ($i = 0; $i < sizeof($customerRequest); $i++) {
            if ($customerRequest[$i]['status'] == 'assign' || $customerRequest[$i]['status'] == 'checkforapprove' || $customerRequest[$i]['status'] == 'active' || $customerRequest[$i]['status'] == 'disapprove' || $customerRequest[$i]['status'] == 'pending') {
                ?>
                <!-- Design Request Tab -->
                <div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                    <div class="pro-deshboard-list">
                        <div class="cli-ent-add-row">
                            <a href="<?php echo base_url() ?>customer/request/new_request/category" class="cli-ent-add-col" >
                                <span class="cli-ent-add-icon">+</span>
                            </a>
                        </div>

                        <!-- For Active Projects -->
                        <div id="prior_data">
                                        <?php
                                        for ($i = 0; $i < count($customerRequest); $i++) {
                                            if ($customerRequest[$i]['status'] == "active" || $customerRequest[$i]['status'] == "checkforapprove" || $customerRequest[$i]['status'] == "disapprove") {
                                                ?>
                                    <!-- List Desh Product -->
                                    <div class="pro-desh-row">
                                        <div class="pro-desh-box delivery-desh">
                                            <p class="pro-a">
                                                <?php
                                                if ($customerRequest[$i]['status'] == "active") {
                                                    echo "Expected on";
                                                } elseif ($customerRequest[$i]['status'] == "checkforapprove") {
                                                    echo "Delivered on";
                                                } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                    echo "Expected on";
                                                } elseif ($customerRequest[$i]['status'] == "approved") {
                                                    echo "Approved on";
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">
                                            <?php
                                            if ($customerRequest[$i]['status'] == "active") {
                                                echo date('m/d/Y', strtotime($customerRequest[$i]['expected']));
                                            } elseif ($customerRequest[$i]['status'] == "checkforapprove") {
                                                echo date('m/d/Y', strtotime($customerRequest[$i]['deliverydate']));
                                            } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                echo date('m/d/Y', strtotime($customerRequest[$i]['expected']));
                                            }
                                            ?>                         
                                            </p>
                                                                <?php if ($customerRequest[$i]['status'] != "assign") { ?>
                                                <p class="green">Active</p>
                        <?php } else { ?>
                                                <p class="pro-a">Priority
                                                    <span class="select-pro-a">
                                                        <select onchange="prioritize(this.value, <?php echo $customerRequest[$i]['priority'] ?>,<?php echo $customerRequest[$i]['id'] ?>)">
                            <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                                                <option value="<?php echo $j; ?>"
                                <?php
                                if ($j == $customerRequest[$i]['priority']) {
                                    echo "selected";
                                }
                                ?>
                                                                        ><?php echo $j; ?></option>                                             
                            <?php } ?>
                                                        </select>
                                                    </span>
                                                </p>
                                                        <?php } ?>
                                        </div>

                                        <div class="pro-desh-box dcol-1" style="cursor:pointer;" onclick="window.location = '<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>'">
                                            <div class="desh-head-wwq">
                                                <div class="desh-inblock">
                                                    <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>">
                                                        <?php echo $customerRequest[$i]['title']; ?>
                                                        </a></h3>
                                                    <p class="pro-b"><?php echo $customerRequest[$i]['category']; ?></p>
                                                </div>
                                                <div class="desh-inblock">
                                                    <p class="neft pull-right">
                                                        <?php
                                                        if ($customerRequest[$i]['status'] == "checkforapprove") {
                                                            $status = "Review your design";
                                                            $color = "bluetext";
                                                        } elseif ($customerRequest[$i]['status'] == "active") {
                                                            $status = "Design In Progress";
                                                            $color = "blacktext";
                                                        } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                            $status = "Revision In Progress";
                                                            $color = "orangetext ";
                                                        } elseif ($customerRequest[$i]['status'] == "pending" || $customerRequest[$i]['status'] == "assign") {
                                                            $status = "In Queue";
                                                            $color = "greentext ";
                                                        } else {
                                                            $status = "";
                                                            $color = "greentext ";
                                                        }
                                                        ?>
                        <?php if ($status == 'In Queue'): ?>
                                                            <span class="gray text-uppercase">
                            <?php echo $status; ?>
                                                            </span>
                                                            <?php else: ?>
                                                            <span class="green <?php echo $color; ?> text-uppercase">
                                                                <?php echo $status; ?>
                                                            </span>
                                                            <?php endif ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pro-desh-box dcol-2">
                                            <div class="pro-circle-list clearfix">
                                                <div class="pro-circle-box">
                                                    <a href=""><figure class="pro-circle-img">
                        <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                                                                <?php } else { ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                                                                    <?php } ?>
                                                        </figure>
                                                        <p class="pro-circle-txt text-center">
                                                                <?php echo $customerRequest[$i]['designer_first_name']; ?>
                                                        </p></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pro-desh-box">
                                            <div class="pro-desh-r1">
                                                <div class="pro-inleftbox">
                                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                                                            <?php if ($customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count'] != 0) { ?>
                                                                    <span class="numcircle-box">
                            <?php echo $customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count']; ?>
                                                                    </span>
                        <?php } ?></span></a></p>
                                                </div>
                                                <div class="pro-inrightbox">
                                                    <p class="pro-a inline-per"><a style="position: relative;"  href="<?php echo base_url() ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                                    <?php if (count($customerRequest[$i]['files_new']) != 0) { ?>
                                                                    <span class="numcircle-box">
                                        <?php echo count($customerRequest[$i]['files_new']); ?>
                                                                    </span>
                                    <?php } ?>
                                                            </span>
                        <?php echo count($customerRequest[$i]['files']); ?>
                                                        </a>
                                                        <a href="<?php echo base_url(); ?>customer/request/deleteproject/<?php echo $customerRequest[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <!-- -->
                                            <?php
                                        }
                                    }
                                    for ($i = 0; $i < count($customerRequest); $i++) {
                                        if ($customerRequest[$i]['status'] != "active" && $customerRequest[$i]['status'] != "checkforapprove" && $customerRequest[$i]['status'] != "disapprove") {
                                            ?>
                                    <!-- List Desh Product -->
                                    <div class="pro-desh-row">
                                        <div class="pro-desh-box delivery-desh" style="padding-right: 0px; padding-left: 15px;">
                                            <!-- <p class="pro-a">Delivery</p> -->
                                            <!-- <p class="pro-b"> -->
                                                        <?php
                                                        // $created_date =  date($customerRequest[$i]['dateinprogress']);
                                                        // $expected_date = date('m/d/Y', strtotime($created_date . ' +1 day'));
                                                        // if ( $customerRequest[ $i ][ 'modified' ] == "" ) {
                                                        //     echo $expected_date;
                                                        // } else {
                                                        //     echo date( "m/d/Y", strtotime( $customerRequest[ $i ][ 'modified' ] ) );
                                                        // }
                                                        ?>                          
                                            <!-- </p> -->
                                            <?php if ($customerRequest[$i]['status'] == "assign") { ?>
                                                <p class="pro-a">Priority
                                                    <span class="select-pro-a">
                                                        <select onchange="prioritize(this.value, <?php echo $customerRequest[$i]['priority'] ?>,<?php echo $customerRequest[$i]['id'] ?>)">
                            <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                                                <option value="<?php echo $j; ?>"
                                                                    <?php
                                                                    if ($j == $customerRequest[$i]['priority']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>
                                                                        ><?php echo $j; ?></option>                                             
                                                            <?php } ?>
                                                        </select>
                                                    </span>
                                                </p>
                                                        <?php } ?>
                                        </div>

                                        <div class="pro-desh-box dcol-1" style="cursor:pointer;" onclick="window.location = '<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>'">
                                            <div class="desh-head-wwq">
                                                <div class="desh-inblock">
                                                    <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>">
                                                        <?php echo $customerRequest[$i]['title']; ?>
                                                        </a></h3>
                                                    <p class="pro-b"><?php echo $customerRequest[$i]['category']; ?></p>
                                                </div>
                                                <div class="desh-inblock">
                                                    <p class="neft pull-right">
                                                        <?php
                                                        if ($customerRequest[$i]['status'] == "checkforapprove") {
                                                            $status = "Review your design";
                                                            $color = "bluetext";
                                                        } elseif ($customerRequest[$i]['status'] == "active") {
                                                            $status = "Design In Progress";
                                                            $color = "blacktext";
                                                        } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                            $status = "Revision In Progress";
                                                            $color = "orangetext ";
                                                        } elseif ($customerRequest[$i]['status'] == "pending" || $customerRequest[$i]['status'] == "assign") {
                                                            $status = "In Queue";
                                                            $color = "greentext ";
                                                        } else {
                                                            $status = "";
                                                            $color = "greentext ";
                                                        }
                                                        ?>
                        <?php if ($status == 'In Queue'): ?>
                                                            <span class="gray text-uppercase">
                                                        <?php echo $status; ?>
                                                            </span>
                                                    <?php else: ?>
                                                            <span class="green <?php echo $color; ?> text-uppercase">
                                                        <?php echo $status; ?>
                                                            </span>
                        <?php endif ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pro-desh-box dcol-2">
                                            <div class="pro-circle-list clearfix">
                                                <div class="pro-circle-box">
                                                    <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                                    <!-- <a href=""><figure class="pro-circle-img">
                                                                <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                                                                <?php } ?>
                                                    </figure>
                                                    <p class="pro-circle-txt text-center">
                        <?php echo $customerRequest[$i]['designer_first_name']; ?>
                                                    </p></a> -->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pro-desh-box">
                                            <div class="pro-desh-r1">
                                                <div class="pro-inleftbox">
                                                    <p class="pro-a inline-per"><a  style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                                                            <?php if ($customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count'] != 0) { ?>
                                                                    <span class="numcircle-box">
                            <?php echo $customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count']; ?>
                                                                    </span>
                        <?php } ?>
                                                            </span>
                                                        </a>
                                                    </p>
                                                </div>
                                                <div class="pro-inrightbox">
                                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                        <?php if (count($customerRequest[$i]['files_new']) != 0) { ?>
                                                                    <span class="numcircle-box">
                            <?php echo count($customerRequest[$i]['files_new']); ?>
                                                                    </span>
                        <?php } ?>
                                                            </span>
                        <?php echo count($customerRequest[$i]['files']); ?>
                                                        </a>
                                                        <a href="<?php echo base_url(); ?>customer/request/deleteproject/<?php echo $customerRequest[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <!-- -->
                    <?php }
                }
                ?>
                        </div>
                    </div>
                </div>
            <?php } elseif ($customerRequest[$i]['status'] == 'draft') { ?>
                <!-- In Progress Request Tab-->
                <div class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">
                    <div class="cli-ent-add-row">
                        <a href="<?php echo base_url() ?>customer/request/new_request/category" class="cli-ent-add-col" >
                            <span class="cli-ent-add-icon">+</span>
                        </a>
                    </div>
                    <!-- For Drafted Projects -->
                <?php for ($i = 0; $i < count($customerRequest); $i++) { ?>
                        <!-- List Desh Product -->
                        <div class="pro-desh-row">
                            <div class="pro-desh-box delivery-desh">
                                <h3 class="app-roved gray">DRAFT</h3>
                            </div>

                            <div class="pro-desh-box dcol-1"  style="cursor:pointer;" onclick="window.location = '<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>'">
                                <div class="desh-head-wwq">
                                    <div class="desh-inblock">
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>"><?php echo $customerRequest[$i]['title']; ?></a></h3>
                    <?php if (!empty($customerRequest[$i]['category'])): ?>
                                        <?php $cat = $customerRequest[$i]['category']; ?>
                                    <?php else: ?>
                                            $cat = "Not Categorized";
                                                <?php endif; ?>
                            <!-- <p class="pro-b"><?php echo $cat; ?></p> -->
                                    </div>
                                    <div class="desh-inblock">
                                        <p class="neft pull-right">
                                            <span class="gray text-uppercase">
                    <?php echo $customerRequest[$i]['status']; ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="pro-desh-box dcol-2">
                                <div class="pro-circle-list clearfix">
                    <?php if ($customerRequest[$i]['designer_first_name']): ?>
                                        <div class="pro-circle-box">
                                            <a href=""><figure class="pro-circle-img">
                        <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                        <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                                                    <?php } ?>
                                                </figure>
                                                <p class="pro-circle-txt text-center">
                                                        <?php echo $customerRequest[$i]['designer_first_name']; ?>
                                                </p></a>
                                        </div>
                    <?php else: ?>
                                        <h4 class="head-c draft_no">No Designer assigned yet</h4>

                                            <?php endif ?>

                                </div>
                            </div>

                            <div class="pro-desh-box">
                                <div class="pro-desh-r1">
                                    <div class="pro-inleftbox">
                                        <p class="pro-a inline-per"><a style="position: relative;" href="javascript:void(0)"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                    <?php if ($customerRequest[$i]['total_chat'] != 0) { ?>
                                                    <span class="numcircle-box">
                        <?php echo $customerRequest[$i]['total_chat']; ?>
                                                    </span>
                    <?php } ?></a></p>
                                    </div>
                                    <div class="pro-inrightbox">
                                        <p class="pro-a inline-per"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                    <?php echo count($customerRequest[$i]['files']); ?>
                                            <a href="<?php echo base_url(); ?>customer/request/deleteproject/<?php echo $customerRequest[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <!-- -->
                                <?php } ?>

                    <p class="space-e"></p>
                </div>
                            <?php } elseif ($customerRequest[$i]['status'] == 'approved') { ?>
                <!-- Approved Designs Tab -->
                <div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
                    <div class="cli-ent-add-row">
                        <a href="<?php echo base_url() ?>customer/request/new_request/category" class="cli-ent-add-col" >
                            <span class="cli-ent-add-icon">+</span>
                        </a>
                    </div>
                    <!-- For Completed Projects -->
                <?php for ($i = 0; $i < count($customerRequest); $i++) { ?>
                        <!-- List Desh Product -->
                        <div class="pro-desh-row">
                            <div class="pro-desh-box delivery-desh">
                                <p class="pro-a"><?php
                    if ($customerRequest[$i]['status'] == "approved") {
                        echo "Approved on";
                    }
                    ?></p>
                                <p class="pro-b">
                                                <?php
                                                if ($customerRequest[$i]['status'] == "approved") {
                                                    echo date('m/d/Y', strtotime($customerRequest[$i]['approvddate']));
                                                }
                                                ?>                          
                                </p>
                            </div>

                            <div class="pro-desh-box dcol-1"  style="cursor:pointer;" onclick="window.location = '<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>'">
                                <div class="desh-head-wwq">
                                    <div class="desh-inblock">
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>customer/request/project-info/<?php echo $customerRequest[$i]['id']; ?>"><?php echo $customerRequest[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo $customerRequest[$i]['category']; ?></p>
                                    </div>
                                    <div class="desh-inblock">
                                        <p class="neft pull-right"><span class="green text-uppercase">
                                                <?php
                                                if ($customerRequest[$i]['status'] == " checkforapprove") {
                                                    $status = "Review your design";
                                                    $color = "greentext";
                                                } elseif ($customerRequest[$i]['status'] == "active") {
                                                    $status = "Design In Progress";
                                                    $color = "blacktext";
                                                } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                    $status = "Revision In Progress";
                                                    $color = "orangetext ";
                                                } elseif ($customerRequest[$i]['status'] == "pending" || $customerRequest[$i]['status'] == "assign") {
                                                    $status = "In Queue ";
                                                    $color = "greentext ";
                                                } else {
                                                    $status = "Completed";
                                                    $color = "greentext ";
                                                }
                                                echo $status;
                                                ?>
                                            </span></p>
                                    </div>
                                </div>
                            </div>

                            <div class="pro-desh-box dcol-2">
                                <div class="pro-circle-list clearfix">
                                    <div class="pro-circle-box">
                                        <a href=""><figure class="pro-circle-img">
                                                    <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                                                        <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                                                    <?php } ?>
                                            </figure>
                                            <p class="pro-circle-txt text-center">
                    <?php echo $customerRequest[$i]['designer_first_name']; ?>
                                            </p></a>
                                    </div>
                                </div>
                            </div>

                            <div class="pro-desh-box">
                                <div class="pro-desh-r1">
                                    <div class="pro-inleftbox">
                                        <p class="pro-a inline-per"><a style="position: relative;" href=""><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                                                <?php if ($customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count'] != 0) { ?>
                                                        <span class="numcircle-box">
                        <?php echo $customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count']; ?>
                                                        </span>
                    <?php } ?>
                                                </span>
                                            </a></p>
                                    </div>
                                    <div class="pro-inrightbox">
                                        <p class="pro-a inline-per"><a style="position: relative;" href="javascript:void(0)"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">

                    <?php if (count($customerRequest[$i]['files_new']) != 0) { ?>
                                                        <span class="numcircle-box">
                        <?php echo count($customerRequest[$i]['files_new']); ?>
                                                        </span>
                    <?php } ?>
                                                </span>
                    <?php echo count($customerRequest[$i]['files']); ?>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <!-- -->
                <?php } ?>

                    <p class="space-e"></p>

                    <!-- <div class="loader">
                        <a class="loader-link text-uppercase" href=""><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-loader.png" class="img-responsive" width="28"> Loading</a>
                    </div> -->
                </div>
                <?php
            }
        }
    }

    public function deleteCustomeImg($id) {
        $success = $this->Admin_model->delete_data('request_files', $id);
        if ($success) {
            $this->session->set_flashdata('message_success', "Your Image Deleted Successfully", 5);
            redirect(base_url() . "customer/request/project-info/" . $_GET['requests']);
        } else {
            $this->session->set_flashdata('message_error', "Something went wrong.", 5);
            redirect(base_url() . "customer/request/project-info/" . $_GET['requests']);
        }
    }

    public function onlytimezone($date_zone) {
        $nextdate = $this->myfunctions->onlytimezoneforall($date_zone);
        return $nextdate;
    }

    public function deleteproject($id) {
       
        $user_id = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
      //  $login_user_id = $this->load->get_var('login_user_id'); 
        $path = FCPATH . "public/uploads/requests/" . $id . "/*";
        $path1 = FCPATH . "public/uploads/requests/" . $id;
        $files = glob($path); // get all file names
        if (file_exists($path1)) {
            foreach ($files as $file) { // iterate files
                if (is_file($file))
                    unlink($file); // delete file
            }
            rmdir($path1);
        }
        //$user_id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : '';
        $requests = $this->Request_model->get_request_by_id($id);
//        echo "<pre/>";print_r($requests);
        $deletedpriority = isset($requests[0]['priority']) ? $requests[0]['priority'] : '';
        $success = $this->Admin_model->delete_request("requests", array('id' => $id));
      //  die($deletedpriority."test");
        if ($deletedpriority != 0) {
            $deletedsubpriority = isset($requests[0]['sub_user_priority']) ? $requests[0]['sub_user_priority'] : '';
            $priorfrom = $deletedpriority + 1;
            $subpriorfrom = $deletedsubpriority + 1;
//            $allinqueuerequest = $this->Request_model->get_inqueuereq_by_customerID($user_id);
//            //echo "<pre/>";print_r($allinqueuerequest);exit;
//            foreach ($allinqueuerequest as $kk => $vv) {
//                if ($vv['priority'] >= $priorfrom) {
//                    $this->Admin_model->update_priority_after_approved_qa(1, $vv['priority'], $user_id);
//                    //redirect(base_url() . "customer/request/design_request");
//                    // $this->Request_model->update_priority($vv['priority'],$vv['priority']-1,'',$user_id);
//                } 
//            }
            $this->Admin_model->update_priority_after_approved_qa(1, $priorfrom, $user_id);
            $this->Admin_model->update_subuserpriority_after_approved_qa(1, $user_id,$requests[0]['created_by'],$subpriorfrom);
        }
        if ($success) {
            if (!empty($requests[0]['designer_attachment'])) {
                foreach ($requests[0]['designer_attachment'] as $data) {
                    $this->Admin_model->delete_request_file_data("request_file_chat", $data['id']);
                }
            }
            $this->Admin_model->delete_request_data("request_discussions", $id);
            $this->Admin_model->delete_request_data("message_notification", $id);
            $this->Admin_model->delete_request_data("notification", $id);
            $this->Admin_model->delete_request_data("request_files", $id);
            $customer_data = $this->Admin_model->getuser_data($user_id);
            if($customer_data[0]['is_cancel_subscription'] != 1){
            $ActiveReq = $this->myfunctions->CheckActiveRequest($user_id);
            if($ActiveReq == 1){
               $this->myfunctions->move_project_inqueqe_to_inprogress($user_id,'','deleteproject',$id);
            }
            }
            
            $this->myfunctions->capture_project_activity($id,'','','delete_project','deleteproject',$login_user_id);
            $this->session->set_flashdata('message_success', "Your project deleted successfully", 5);
            redirect(base_url() . "customer/request/design_request");
        }
    }


    public function send_email_when_inqueue_to_inprogress($project_title, $id) {
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $receiver = $_SESSION['email'];
        $query1 = $this->db->select('first_name')->from('users')->where('email', $receiver)->get();
        $fname = $query1->result();
        $data = array(
            'title' => $project_title,
            'project_id' => $id,
            'fname' => isset($fname[0]->first_name) ? $fname[0]->first_name : ''
        );
        $body = $this->load->view('emailtemplate/inqueue_template', $data, TRUE);
        $subject = 'Designer has Started Working on your New Project.';

        $message = $body;

        $title = 'GraphicsZoo';

        //SM_sendMail($receiver,$subject,$message,$title);          
    }

    public function user_update_tour() {
        $this->Welcome_model->update_data("users", array("tour" => 1), array("id" => $_SESSION['user_id']));
    }

    public function load_more_customer() { 
        $parent_user_plan = $this->load->get_var('parent_user_plan');
        $group_no = $this->input->post('group_no');
        $viewtype = $this->input->post('viewtype');
        $status = $this->input->post('status');
        $search = $this->input->post('search');
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        $isSearch = (trim($search) != '') ? true : false;
        $clientid = $this->input->post('client_id');
        $client_id = ($clientid != '') ? $clientid : "";
        $brandid = $this->input->post('brand_id');
        $brand_id = ($brandid != '') ? $brandid : "";
        $content_per_page = LIMIT_CUSTOMER_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $canuserdel = $this->myfunctions->isUserPermission('delete_req');
        $canuseradd = $this->myfunctions->isUserPermission('add_requests');
        $canaccessallbrands = $this->myfunctions->isUserPermission('brand_profile_access');
	$can_manage_priorities = $this->myfunctions->isUserPermission('manage_priorities');
        if ($this->input->post('status') == "active,disapprove,assign,pending,checkforapprove") {
            $statusArray = explode(',', $status);
//            $checkpriority = $this->Request_model->customer_load_more($statusArray, $created_user, false, $start, $content_per_page, 'priority', $search,$brand_id);
            $active_project = $this->Request_model->customer_load_more($statusArray, $created_user, false, $start, $content_per_page, 'priority', $search,$brand_id,$client_id);
            $count_total_projects = $this->Request_model->customer_load_more($statusArray, $created_user, true, $start, $content_per_page, 'priority', $search,$brand_id,$client_id);
            $assign_requests = $this->Request_model->getallrequestcount_accstatus($created_user, array('assign'));
            $subuser_assign_requests = $this->Request_model->getallrequestcount_accstatus($created_user, array('assign'),$login_user_id);
            for ($i = 0; $i < sizeof($active_project); $i++) {
                $active_project[$i]['customer_variables'] = $this->myfunctions->customerVariables($active_project[$i]['status']);
                $active_project[$i]['total_chat'] = $this->Request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
                $getlatestfiledraft = $this->Request_model->get_latestdraft_file($active_project[$i]['id']);
                $active_project[$i]['latest_draft'] = $getlatestfiledraft[0]['file_name'];
                if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
                    if ($active_project[$i]['expected_date'] == '' || $active_project[$i]['expected_date'] == NULL) {
                        $active_project[$i]['expected'] = $this->myfunctions->check_timezone($active_project[$i]['latest_update'],$parent_user_plan);
                    } else {
                        $active_project[$i]['expected'] = $this->onlytimezone($active_project[$i]['expected_date']);
                    } 
                } elseif ($active_project[$i]['status'] == "checkforapprove") {
                    $active_project[$i]['deliverydate'] = $this->onlytimezone($active_project[$i]['approvaldate']);
                }
                $id = $active_project[$i]['id'];
                $data = $this->Request_model->get_request_by_id($id);
                $active_project[$i]['max_priority'] = $assign_requests;
                $active_project[$i]['subuser_max_priority'] = $subuser_assign_requests;
                $active_project[$i]['client_name'] = $this->Request_model->getuserbyid($active_project[$i]['created_by'],'client');
            }
            $activeTab = 0;
            foreach ($active_project as $project) {
                if ($project['status'] == "assign") {
                    $activeTab++;
                }
            }
            for ($i = 0; $i < count($active_project); $i++) {
                if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "checkforapprove" || $active_project[$i]['status'] == "disapprove") 
                    {
                        $data['projects'] = $active_project[$i];
                        $data['canuserdel'] = $canuserdel;
                        $data['canuseradd'] = $canuseradd;
                        $data['canaccessallbrands'] = $canaccessallbrands;
                        $data['can_manage_priorities'] = $can_manage_priorities;
                        if($viewtype == 'grid'){
//                            echo $viewtype."<br/>";
//                            echo "<pre/>";print_r($data);
                        $this->load->view('customer/load_more_customer',$data);  
                        }else{
//                            echo $viewtype."<br/>";
//                            echo "<pre/>";print_r($data);
                         $this->load->view('customer/projects_list_view',$data);    
                        }      
                    }
                elseif($active_project[$i]['status'] != "active" && $active_project[$i]['status'] != "checkforapprove" && $active_project[$i]['status'] != "disapprove"){
                        $data['projects'] = $active_project[$i]; 
                        $data['canaccessallbrands'] = $this->myfunctions->isUserPermission('brand_profile_access');
                        $data['can_manage_priorities'] = $this->myfunctions->isUserPermission('manage_priorities');
                        $data['canuserdel'] = $canuserdel;
                        $data['canuseradd'] = $canuseradd;
                        $data['canaccessallbrands'] = $canaccessallbrands;
                        $data['can_manage_priorities'] = $can_manage_priorities;
                        $data['activeTab'] = $activeTab;
                        if($viewtype == 'grid'){
//                            echo $viewtype."<br/>";
//                            echo "<pre/>";print_r($data);
                        $this->load->view('customer/load_more_customer',$data); 
                        }else{
//                            echo $viewtype."<br/>";
//                            echo "<pre/>";print_r($data);
                           $this->load->view('customer/projects_list_view',$data);   
                        } 
                    }
            }
            ?>
            <span id="loadingAjaxCount" data-value="<?php echo $count_total_projects; ?>"></span>
            <?php
        } 
        elseif ($this->input->post('status') == "draft") {
            $group_no = $this->input->post('group_no');
            $status = $this->input->post('status');
            $content_per_page = LIMIT_CUSTOMER_LIST_COUNT;
            $start = ceil($group_no * $content_per_page);
            $draft_project = $this->Request_model->customer_load_more('draft', $created_user, false, $start, $content_per_page, 'index_number', $search,$brand_id,$client_id);
            $count_total_projects = $this->Request_model->customer_load_more('draft', $created_user, true, $start, $content_per_page, 'index_number', $search,$brand_id,$client_id);
            for ($i = 0; $i < sizeof($draft_project); $i++) {
                $draft_project[$i]['customer_variables'] = $this->myfunctions->customerVariables($draft_project[$i]['status']);
                $draft_project[$i]['total_chat'] = $this->Request_model->get_chat_number($draft_project[$i]['id'], $draft_project[$i]['customer_id'], $draft_project[$i]['designer_id'], "customer");
                $d_id = $draft_project[$i]['id'];
                $draft_project[$i]['total_chat_all'] = $this->Request_model->get_total_chat_number_customer($draft_project[$i]['id'], $_SESSION['user_id'], "customer", "1");
                $data = $this->Request_model->get_request_by_id($d_id);
                $getlatestfiledraft = $this->Request_model->get_latestdraft_file($draft_project[$i]['id']);
                $draft_project[$i]['latest_draft'] = $getlatestfiledraft[0]['file_name'];
                $draft_project[$i]['client_name'] = $this->Request_model->getuserbyid($draft_project[$i]['created_by'],'client');
            }
            foreach ($draft_project as $dp) {
                $data['projects'] = $dp;  
                $data['canuserdel'] = $canuserdel;
                $data['canuseradd'] = $canuseradd;
                $data['canaccessallbrands'] = $canaccessallbrands;
                $data['can_manage_priorities'] = $can_manage_priorities;
                if($viewtype == 'grid'){
                $this->load->view('customer/load_more_customer',$data);  
                }else{
                  $this->load->view('customer/projects_list_view',$data);    
                }    
            }
            ?><span id="loadingAjaxCount" data-value="<?php echo $count_total_projects; ?>"></span><?php
        }
        else if ($this->input->post('status') == "approved") { 
            $group_no = $this->input->post('group_no');
            $status = $this->input->post('status');
            $content_per_page = LIMIT_CUSTOMER_LIST_COUNT;
            $start = ceil($group_no * $content_per_page);
            $complete_project = $this->Request_model->customer_load_more('approved',$created_user, false, $start, $content_per_page, 'index_number', $search,$brand_id,$client_id);
            $count_total_projects = $this->Request_model->customer_load_more('approved',$created_user, true, $start, $content_per_page, 'index_number', $search,$brand_id,$client_id);
            for ($i = 0; $i < sizeof($complete_project); $i++) {
                $complete_project[$i]['customer_variables'] = $this->myfunctions->customerVariables($complete_project[$i]['status']);
                $complete_project[$i]['total_chat'] = $this->Request_model->get_chat_number($complete_project[$i]['id'], $complete_project[$i]['customer_id'], $complete_project[$i]['designer_id'], "customer");
                $c_id = $complete_project[$i]['id'];
                $getlatestfiledraft = $this->Request_model->get_latestdraft_file($complete_project[$i]['id']);
                $complete_project[$i]['latest_draft'] = $getlatestfiledraft[0]['file_name'];
                if ($complete_project[$i]['status'] == "approved") {
                    $complete_project[$i]['approvddate'] = $this->onlytimezone($complete_project[$i]['approvaldate']);
                }
                $data = $this->Request_model->get_request_by_id($c_id);
                $complete_project[$i]['client_name'] = $this->Request_model->getuserbyid($complete_project[$i]['created_by'],'client');
            }
            for ($i = 0; $i < count($complete_project); $i++) {
                $data['projects'] = $complete_project[$i];  
                $data['canuserdel'] = $canuserdel;
                $data['canuseradd'] = $canuseradd;
                $data['canaccessallbrands'] = $canaccessallbrands;
                $data['can_manage_priorities'] = $can_manage_priorities;
                if($viewtype == 'grid'){
                    $this->load->view('customer/load_more_customer',$data);  
                    }else{
                      $this->load->view('customer/projects_list_view',$data);    
                    }               
           }
            ?><span id="loadingAjaxCount" data-value="<?php echo $count_total_projects; ?>"></span><?php
        }
        else if ($this->input->post('status') == "cancel") { 
            $group_no = $this->input->post('group_no');
            $status = $this->input->post('status');
            $content_per_page = LIMIT_CUSTOMER_LIST_COUNT;
            $start = ceil($group_no * $content_per_page);
            $cancel_project = $this->Request_model->customer_load_more('cancel',$created_user, false, $start, $content_per_page, 'index_number', $search,$brand_id,$client_id);
            $count_total_projects = $this->Request_model->customer_load_more('cancel',$created_user, true, $start, $content_per_page, 'index_number', $search,$brand_id,$client_id);
            for ($i = 0; $i < sizeof($cancel_project); $i++) {
                $cancel_project[$i]['customer_variables'] = $this->myfunctions->customerVariables($cancel_project[$i]['status']);
                $cancel_project[$i]['total_chat'] = $this->Request_model->get_chat_number($cancel_project[$i]['id'], $cancel_project[$i]['customer_id'], $cancel_project[$i]['designer_id'], "customer");
                $c_id = $cancel_project[$i]['id'];
                $getlatestfiledraft = $this->Request_model->get_latestdraft_file($cancel_project[$i]['id']);
                $cancel_project[$i]['latest_draft'] = $getlatestfiledraft[0]['file_name'];
                if ($cancel_project[$i]['status'] == "modified") {
                    $cancel_project[$i]['modified'] = $this->onlytimezone($cancel_project[$i]['modified']);
                }
                $data = $this->Request_model->get_request_by_id($c_id);
                $cancel_project[$i]['client_name'] = $this->Request_model->getuserbyid($cancel_project[$i]['created_by'],'client');
            }
            for ($i = 0; $i < count($cancel_project); $i++) {
                $data['projects'] = $cancel_project[$i];  
                $data['canuserdel'] = $canuserdel;
                $data['canuseradd'] = $canuseradd;
                $data['canaccessallbrands'] = $canaccessallbrands;
                $data['can_manage_priorities'] = $can_manage_priorities;
                if($viewtype == 'grid'){
                    $this->load->view('customer/load_more_customer',$data);  
                    }else{
                      $this->load->view('customer/projects_list_view',$data);    
                    }               
           }
            ?><span id="loadingAjaxCount" data-value="<?php echo $count_total_projects; ?>"></span><?php
        }
        else if($this->input->post('status') == "hold"){
          $group_no = $this->input->post('group_no');
            $status = $this->input->post('status');
            $content_per_page = LIMIT_CUSTOMER_LIST_COUNT;
            $start = ceil($group_no * $content_per_page);
            $hold_project = $this->Request_model->customer_load_more('hold', $created_user, false, $start, $content_per_page, 'index_number', $search, $brand_id);
            $count_total_projects = $this->Request_model->customer_load_more('hold', $created_user, true, $start, $content_per_page, 'index_number', $search, $brand_id);
            for ($i = 0; $i < sizeof($hold_project); $i++) {
                $hold_project[$i]['user_data'] = $profile_data;
                $hold_project[$i]['customer_variables'] = $this->myfunctions->customerVariables($hold_project[$i]['status']);
                $hold_project[$i]['total_chat'] = $this->Request_model->get_chat_number($hold_project[$i]['id'], $hold_project[$i]['customer_id'], $hold_project[$i]['designer_id'], "customer");
                $c_id = $hold_project[$i]['id'];
                $getlatestfiledraft = $this->Request_model->get_latestdraft_file($hold_project[$i]['id']);
                $hold_project[$i]['latest_draft'] = $getlatestfiledraft[0]['file_name'];
                if ($hold_project[$i]['status'] == "modified") {
                    $hold_project[$i]['modified'] = $this->onlytimezone($hold_project[$i]['modified']);
                }
                $hold_project[$i]['client_name'] = $this->Request_model->getuserbyid($hold_project[$i]['created_by'],'client');
                $data = $this->Request_model->get_request_by_id($c_id);
            }
            for ($i = 0; $i < count($hold_project); $i++) {
                $data['projects'] = $hold_project[$i];
                $data['canuserdel'] = $canuserdel;
                $data['canuseradd'] = $canuseradd;
                $data['canaccessallbrands'] = $canaccessallbrands;
                $data['can_manage_priorities'] = $can_manage_priorities;
                $data['user_data'] = $hold_project[$i]['user_data'];
                if ($viewtype == 'grid') {
                    $this->load->view('customer/load_more_customer', $data);
                } else {
                    $this->load->view('customer/projects_list_view', $data);
                }
            }
            ?><span id="loadingAjaxCount" data-value="<?php echo $count_total_projects; ?>"></span><?php  
        }
    }

    public function upgrADE_account() {
        $result = array();
        $plan_name = $_POST['plan_name'];
        $inprogress_request = $_POST['in_progress_request'];
        $plan_price = $_POST['plan_price'];
        $titlestatus = "";
        $planlist = $this->Stripe->getallsubscriptionlist();
        $user_data = $this->Admin_model->getuser_data($_POST['customer_id']);
        $getsubscriptionplan = $this->Request_model->getsubscriptionlistbyplanid($user_data[0]['plan_name']);
        if($user_data[0]['overwrite'] == 1 && isset($user_data[0]['turn_around_days'])){
            $turn_around_days = $user_data[0]['turn_around_days'];
        }else{
            $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
        }
        $rendomly_qa_id = $this->Admin_model->getqa_id();
        if (!empty($_POST)) {
            if(isset($_POST['state_tax']) && $_POST['state_tax'] != 0){
                $state_tax = $_POST['state_tax'];
            }else{
                $state_tax = 0;
            }
            $state = isset($_POST['state'])?$_POST['state']:'';
            $stripe_customer = $this->Stripe->createnewCustomer($_POST['email'], trim($_POST['discount'])); 
            if (!$stripe_customer['status']) {
                $this->session->set_flashdata('message_error', "Error occurred while upgrading plan!", 5);
                $result['error'] = $stripe_customer['message'];
				echo json_encode($result);exit;
            }

            $expir_date = explode("/", $_POST['expir_date']);
            $customer_id = $stripe_customer['data']['customer_id'];
            $card_number = trim($_POST['card_number']);
            $expiry_month = $expir_date[0];
            $expiry_year = $expir_date[1];
            $cvc = $_POST['cvc'];
            $stripe_card = $this->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);
            if (!$stripe_card['status']):
                $this->session->set_flashdata('message_error', $stripe_card['message'], 5);
                $result['error'] = $stripe_card['message'];
				echo json_encode($result);exit;
            endif;

            $subscription_plan_id = $plan_name;
            if($subscription_plan_id == SUBSCRIPTION_99_PLAN){
                $current_date = date("Y:m:d H:i:s");
                $monthly_date = strtotime("+1 months", strtotime($current_date)); // returns timestamp
                $billing_expire = date('Y:m:d H:i:s',$monthly_date);
                $customer['billing_cycle_request'] = 3;
                $customer['payment_status'] = 2;
           }else {
               $customer['billing_cycle_request'] = 0;
           }

            $stripe_subscription = '';
            if ($subscription_plan_id) {
                $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($stripe_customer['data']['customer_id'], $subscription_plan_id,$inprogress_request,$state_tax);
                $current_date = date('Y-m-d H:i:s',$stripe_subscription['data']['period_start']);
                $billing_expire = date('Y-m-d H:i:s',$stripe_subscription['data']['period_end']);
            }

            if (!$stripe_subscription['status']) {

                $this->session->set_flashdata('message_error', $stripe_subscription['message'], 5);
                $result['error'] = $stripe_subscription['message'];
				echo json_encode($result);exit;
            }
            

            $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($set), 0, 20);
            $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
            $customer['billing_start_date'] = $current_date;
            $customer['billing_end_date'] = $billing_expire;
            $customer['customer_id'] = $stripe_customer['data']['customer_id'];
            $customer['plan_name'] = $subscription_plan_id;
            $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);
            $customer['is_active'] = 1;
            $customer['is_trail'] = 0;
            $customer['without_pay_user'] = 0;
            $customer['state'] = $state;
            
//            $customer['payment_status'] = 2;
            $customer['plan_amount'] = $plan_price;
            $customer['plan_interval'] = $plandetails['interval'];
            $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
            $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
            $customer['display_plan_name'] = $plandetails['name'];
            $customer['total_inprogress_req'] = $inprogress_request;
            $customer['total_active_req'] = $inprogress_request * TOTAL_ACTIVE_REQUEST;
//            $customer['timezone'] = $timezone;
            //echo '<pre>';print_r($customer);  exit();
            $id = $this->Welcome_model->update_data("users", $customer, array("id" => $_POST['customer_id']));
            if ($id) {
                
                $request = $this->Request_model->get_dummy_request($_POST['customer_id'],1,'draft');
                if(!empty($request)){
                    $subcat_data = $this->Category_model->get_category_byID($request[0]['subcategory_id']);
                    $expected = $this->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $subscription_plan_id,$subcat_data[0]['bucket_type'], $subcat_data[0]['timeline'],$turn_around_days);
                    $this->Welcome_model->update_data("requests", array("status" => "active", "status_admin" => "active", "status_designer" => "active", "status_qa" => "active","dummy_request"=> 0, "dateinprogress" => date("Y-m-d H:i:s"), "priority" => 0, "latest_update" => date("Y-m-d H:i:s"),"expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $request[0]['id']));
                    $this->myfunctions->SendQAmessagetoInprogressReq($request[0]['id'],$user_data[0]['qa_id'],$_POST['customer_id']);
                }
                /** delete dummy request **/
                $this->myfunctions->delete_dummy_request($_POST['customer_id']);
                /** delete dummy request **/
                $result['success'] = "Account upgraded successfully!";
            } else {
                $result['error'] = "Error occurred while upgrading account, please try again or contact us!";
                $this->session->set_flashdata('message_error', "Error occurred while upgrading account, please try again or contact us!", 5);
                //redirect(base_url() . "customer/request/design_request");
            }
        }
        echo json_encode($result);
        exit;
    }

    // Assign designer
    public function assign_designer($category) {
        $get_all_designers_info = $this->Request_model->get_designers_info($category);
        foreach ($get_all_designers_info as $designers) {
            $project_count = $this->Request_model->get_request_count_by_designer_id($designers['user_id']);
            $count_min_request[$designers['user_id']] = $project_count;
        }
        return $count = array_keys($count_min_request, min($count_min_request));
    }

    public function refreshStatus($user_id = "",$actionfrom = "",$lastupdtreq = "") {
        $uid = $this->load->get_var('main_user');
        $userId = ($user_id == '') ? $uid : $user_id;
        $get_inqueue_request = $this->Admin_model->get_all_request_count(array("assign"), $userId);
        if(!empty($get_inqueue_request)){
            
            $ActiveReq = $this->myfunctions->CheckActiveRequest($userId);
            if($ActiveReq == 1){
                $this->myfunctions->move_project_inqueqe_to_inprogress($userId,'',$actionfrom,$lastupdtreq);
            }
              
        }else {
        }
    }
    
     public function verifyPhoneNumber() {
        if(isset($_POST['verfyphn_number'])){
        $this->Welcome_model->update_data("users", array("phone" => $_POST['verfy_number'],"is_trailverified" => 1), array("id" => $_SESSION['user_id']));
        $receiver = SUPPORT_EMAIL;
        $array = array('PROJECT_LINK' => base_url()."admin/dashboard/view_request/".$_POST['req_id']);
        $this->myfunctions->send_email_to_users_by_template($_SESSION['user_id'], 'verify_phone_to_approve_design', $array, $receiver);
        redirect(base_url() . 'customer/request/project_image_view/' . $_POST['request_url']);
        }
        
    }
    
    public function see_more_notifications($id){
        $notifications = $this->Request_model->get_notifications($id);
        $notification_number = $this->Request_model->get_notifications_number($id);
        $this->load->view('customer/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
        $this->load->view('customer/see_more_notifications'); 
    }
    
    public function sharerequest(){
        $login_user_id = $this->load->get_var('login_user_id');
        $request_id = isset($_POST['request_id'])? $_POST['request_id']:'';
        $user_name = isset($_POST['user_name'])? $_POST['user_name']:'';
        $draft_id = isset($_POST['draft_id'])? $_POST['draft_id']:'';
        $shared_from = isset($_POST['shared_from'])? $_POST['shared_from']:'';
        $email = isset($_POST['email'])? $_POST['email']:'';
        $emaillists = explode(',',$email);
        $permissions = $_POST['link_permissions'];
        $request = $this->Request_model->get_request_by_id($request_id);
        $savedata = array();
        if(!empty($permissions)){
        foreach($emaillists as $kk => $vv){
            $checkemailexist = $this->Request_model->isSheredEmaiexists($vv,$request_id,$draft_id);
            if($checkemailexist){
               $this->session->set_flashdata('message_error', "Email address you entered is already exists!", 5);
            }else{
            $data['share_key'] = $this->myfunctions->random_sharekey();
            $data['request_id'] = $request_id;
            $data['draft_id'] = $draft_id;
            $data['share_type'] = 'private';
            $data['is_disabled'] = 0;
            $data['user_name'] = $user_name;
            $data['email'] = $vv;
            $data['shared_by'] = $login_user_id;
            $data['created'] = date("Y:m:d H:i:s");
            $link_share_id  = $this->Welcome_model->insert_data("link_sharing", $data);
            if($link_share_id){
                    $permis_data['sharing_id'] = $link_share_id;
                    $permis_data['can_view'] = 1;
                    $permis_data['created'] = date("Y:m:d H:i:s");
                    if(in_array('mark_as_completed',$permissions)){
                        $permis_data['mark_as_completed'] = 1;
                    }if(in_array('download_source_file',$permissions)){
                         $permis_data['download_source_file'] = 1;
                    }if(in_array('comment_revision',$permissions)){
                         $permis_data['commenting'] = 1;
                    }
                    $permis_data['created'] = date("Y:m:d H:i:s");
            $link_sharing_permission_id  = $this->Welcome_model->insert_data("link_sharing_permissions", $permis_data); 
            $issend = $this->send_permission_email($login_user_id, $data['share_key'],$vv,$user_name,$request[0]['title']);
            if($issend){
                die("send");
            }
            $this->myfunctions->capture_project_activity($request_id,$draft_id,'','share_private_link','sharerequest',$login_user_id);
            $this->session->set_flashdata('message_success', "Link for the request shared successfully!", 5);
             //redirect(base_url() . "customer/request/project-info/".$request_id);
            }else{
                $this->session->set_flashdata('message_error', "Something went wrong, Please try Again!", 5);
                //redirect(base_url() . "customer/request/project_info/".$request_id);
           }
            }
        }
        }else{
          $this->session->set_flashdata('message_error', "You must choose atleast one permissions!", 5);  
        }
        if($draft_id == "" && $shared_from == ""){
          redirect(base_url() . "customer/request/project_info/".$request_id."#peoples_tab");
        }else if($draft_id != "" && $shared_from == "popup"){
          redirect(base_url() . "customer/request/project_info/".$request_id);
        }else{
          redirect(base_url() . "customer/request/project_image_view/".$draft_id."?id=".$request_id."#peoples_tab");  
        }
}

    public function getallprivateaccount(){
        $share_type = isset($_POST['share_type'])? $_POST['share_type']:'';
        $reqid = isset($_POST['reqid'])? $_POST['reqid']:'';
        $draftid = isset($_POST['draftid'])? $_POST['draftid']:'0';
//        echo "reqid".$reqid."<br/>";
//        echo "draftid".$draftid;exit;
        $shared_data = $this->Request_model->getlinkSharedprivateData($reqid,$draftid);
        echo json_encode($shared_data);
     }
     
     public function editsharerequest(){
        $sharekeys = isset($_POST['sharekeys'])? $_POST['sharekeys']:'';
        $shareID = isset($_POST['shareID'])? $_POST['shareID']:'';
        $data = array();
            $data['can_view'] = '1';
            if(in_array('mark_as_completed', $sharekeys)){
                $data['mark_as_completed'] = '1';
            }else{
                 $data['mark_as_completed'] = '0';
            }if(in_array('download_source_file', $sharekeys)){
                $data['download_source_file'] = '1';
            }else{
                 $data['download_source_file'] = '0';
            }if(in_array('comment_revision', $sharekeys)){
                $data['commenting'] = '1';
            }else{
                 $data['commenting'] = '0';
            }
            $success = $this->Welcome_model->update_data("link_sharing_permissions", $data, array("sharing_id" => $shareID));
            if($success){
                echo 'saved';
//              die("saved");
            }else{
                echo 'not saved';
//                die("not saved");
            }
            
        //}
     }
     
     public function getAllselectedpermissions(){
         $shareID = isset($_POST['shareID'])? $_POST['shareID']:'';
         $selectedpermissions = $this->Request_model->getAllpermissionsByshareID($shareID);
         //echo "<pre/>";print_r($selectedpermissions);
         echo json_encode($selectedpermissions);
     }
     
      public function publiclinksave($reqid = "", $draf_tid = "") {
        $created_user = $this->load->get_var('main_user');
        $user_data = $this->Admin_model->getuser_data($created_user);
        $proID = isset($_POST['proID']) ? $_POST['proID'] : $reqid;
        $draftid = isset($_POST['draftid']) ? $_POST['draftid'] : $draf_tid;
        $publiclinkexists = $this->Request_model->publicLinkexists($proID, $draftid);
        if (empty($publiclinkexists)) {
            $data = $output = array();
            $share_key = $this->myfunctions->random_sharekey();
            $data['request_id'] = $proID;
            $data['draft_id'] = $draftid;
            $data['share_type'] = 'public';
            $data['is_disabled'] = 1;
            $data['share_key'] = $share_key;
            $data['created'] = date("Y-m-d H:i:s");
            $subdomain_url = $this->myfunctions->dynamic_urlforsubuser_inemail($created_user, $user_data[0]['plan_name']);
            $data['public_link'] = $subdomain_url . 'project-info/' . $share_key;
            $success = $this->Welcome_model->insert_data("link_sharing", $data);
            
            if ($success) {
                $output['publiclinkexists'] = $data['public_link'];
                $output['is_disabled'] = 1;
                $output['issaved'] = "1";
                $permsn['sharing_id'] = $success;
                $permsn['can_view'] = 1;
                $permsn['mark_as_completed'] = 1;
                $permsn['download_source_file'] = 1;
                $permsn['commenting'] = 1;
                $permsn['created'] = date("Y:m:d H:i:s");
                $success = $this->Welcome_model->insert_data("link_sharing_permissions", $permsn);
            }
        } else {
            
            $output['publiclinkexists'] = $publiclinkexists[0]['public_link'];
            $output['is_disabled'] = $publiclinkexists[0]['is_disabled'];
            $output['issaved'] = "1"; 
        }
        $permissions = $this->Request_model->getpublicpermissions($publiclinkexists[0]['id']);
        $output['permissions'] = $permissions;
        if ($draf_tid == "") {
            echo json_encode($output);
        }
    }
     
     
     public function changepublicpermissions(){
         $publink = isset($_POST['publink'])? $_POST['publink']:'';
         $permissions = isset($_POST['permissions'])? $_POST['permissions']:'';
         $linkshareID = $this->Request_model->changepublicpermissions($publink);
         $data = array();
         if($linkshareID){
             if(in_array('download_source_file',$permissions)){
               $data['download_source_file'] =  1;
             }else{
               $data['download_source_file'] =  0;  
             }if(in_array('mark_as_completed',$permissions)){
               $data['mark_as_completed'] =  1;
             }else{
               $data['mark_as_completed'] =  0;  
             }
             if(in_array('comment_revision',$permissions)){
                $data['commenting'] =  1;  
             }else{
               $data['commenting'] =  0;  
             }
             $success = $this->Welcome_model->update_data("link_sharing_permissions", $data, array("sharing_id" => $linkshareID[0]['id']));
             if($success){
                 echo "1";
             }
             }
     }

     public function changepublicpermissionsforpeople(){
         $share_id = isset($_POST['share_id'])? $_POST['share_id']:'';
         $share_value = isset($_POST['share_value'])? $_POST['share_value']:'';
         $share_checked = isset($_POST['share_checked'])? $_POST['share_checked']:'';
         $data = array($share_value => $share_checked);
         $success = $this->Welcome_model->update_data("link_sharing_permissions", $data, array("sharing_id" => $share_id));
         if ($success) {
            echo "1";
        }
    }
     
     public function deleteShareduser(){
         $sharedID = isset($_POST['shared_id'])?$_POST['shared_id']:'';
         $deldata = $this->Request_model->deleteShareduser($sharedID);
         echo $deldata;
     }
     
     public function send_permission_email($id,$key,$email,$user_name = NULL,$title = NULL) {
         
        $created_user = $this->load->get_var('main_user');
        $user_data = $this->Admin_model->getuser_data($created_user);
        $senderId = ($id)?$id:$_SESSION['user_id'];
        $query1 = $this->db->select('first_name')->from('users')->where('id', $senderId)->get();
        $subdomain_url = $this->myfunctions->dynamic_urlforsubuser_inemail($created_user,$user_data[0]['plan_name']);
        $fname = $query1->result();
         $array = array('CUSTOMER_NAME' => $user_name,
                            'SENDER_NAME' => isset($fname[0]->first_name) ? $fname[0]->first_name : '',
                            'PROJECT_LINK' => $subdomain_url."project-info/".$key,
                                'PROJECT_TITLE' => $title);
        $this->myfunctions->send_email_to_users_by_template($id, 'link_sharing_email', $array, $email);
               
    }
    
    public function add_request_todraft_withoutpayuser() {
        $output = array();
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $subcat_id = isset($_POST['subcategory_id']) ? $_POST['subcategory_id'] : '';
        $subcat_data = $this->Category_model->get_category_byID($subcat_id);
        $bucketType = $subcat_data[0]['bucket_type'];
        $fileType="";
        if($_POST['color_pref']){
        if(in_array('Let Designer Choose',$_POST['color_pref'])){
            $plate = 'Let Designer Choose';
        }}else{
            $plate = isset($_POST['HexCodeVal']) ? $_POST['HexCodeVal'] : 'Let Designer Choose';
        }
        if (isset($_POST['filetype'])) {
            $fileType = implode(",", $_POST['filetype']);
        }
        //echo "<pre>";print_r($_POST);exit;
        $data = array("customer_id" => $created_user,
            "category" => isset($_POST['category'])?$_POST['category']:'',
            "logo_brand" => isset($_POST['logo-brand'])?$_POST['logo-brand']:'',
            "brand_id" => isset($_POST['brand_name']) ? $_POST['brand_name'] : '',
            "category_id" => isset($_POST['category_id'])?$_POST['category_id']:'',
            "subcategory_id" => isset($_POST['subcategory_id'])?$_POST['subcategory_id']:'',
            "category_bucket" => $bucketType,
            "title" => isset($_POST['title'])?$_POST['title']:'',
            "business_industry" => isset($_POST['target_market'])?$_POST['target_market']:'',
            "design_dimension" => isset($_POST['dimension'])?$_POST['dimension']:'',
            "description" => isset($_POST['description'])?$_POST['description']:'',
            "created" => date("Y-m-d H:i:s"),
            "status" => "draft",
            "designer_id" => 0,
            "dummy_request" =>1,
            "priority" => 0,
            "deliverables" => $fileType,
            "created_by" => $login_user_id,
            "last_modified_by" => $login_user_id,
            "color_pref" => $plate
            );
        
        $success = $this->Welcome_model->insert_data("requests", $data);
        //echo $success;
        if(isset($success)){
        $id = $success;
        $this->addFileRequest($id);
        $output['status'] = "1";
        }else{
            $output['status'] = "0";
        }
      echo  json_encode($output);
    }
    
    
    public function markAsInProgress($reqID, $flag = "") {
        if ($flag == "yes") {
            $url = base_url() . "customer/request/design_request";
        } else {
            $url = base_url() . "customer/request/project_info/" . $reqID;
        }
        $uid = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        if ($reqID != 0 || $reqID == '') {
            $currentDateTime = date("Y-m-d H:i:s");
            $request_data = $this->Request_model->get_request_by_id($reqID);
            if($request_data[0]["status"] == "approved"){
                $prerq_status = "disapprove_disapprove_disapprove_disapprove";
                $isactive = $this->myfunctions->CheckActiveRequest($uid,"","","","","",'from_hold',"disapprove");
            }else{
                $isactive = $this->myfunctions->CheckActiveRequest($uid);
            }
            if ($isactive == 1) {
                $success = $this->myfunctions->move_project_inqueqe_to_inprogress($uid, $reqID,'markAsInProgress',$reqID);
            } else {
                $priority = $this->Welcome_model->priority($uid);
                $sub_user_priority = $this->Welcome_model->sub_user_priority($request_data[0]['created_by']);
                $priorityupdate = $priority[0]['priority'] + 1;
                $subpriorityupdate = $sub_user_priority[0]['sub_user_priority'] + 1;
                $updt_reqdata = array("status" => "assign", "status_admin" => "assign", "status_designer" => "assign", "status_qa" => "assign", "priority" => $priorityupdate, "sub_user_priority" => $subpriorityupdate, "created" => $currentDateTime, "latest_update" => $currentDateTime, "modified" => $currentDateTime);
                if($prerq_status != ""){
                    $updt_reqdata["previous_status"] = $prerq_status;
                }
                $success = $this->Welcome_model->update_data("requests", $updt_reqdata, array("id" => $reqID));
                $this->myfunctions->capture_project_activity($reqID,'','',$request_data[0]['status'].'_to_assign','markAsInProgress',$login_user_id);
                $this->myfunctions->update_priority_after_unhold_project($priorityupdate, '1', $reqID, $request_data[0]['customer_id']);
                if ($subpriorityupdate > 1) {
                    $this->myfunctions->update_sub_userpriority_after_unhold_project($subpriorityupdate, '1', $reqID, $login_user_id);
                }
            }
            if ($success) {
                $this->session->set_flashdata('message_success', "Status successfully changed..", 5);
                redirect($url);
            } else {
                $this->session->set_flashdata('message_error', "Error while updating status.", 5);
                redirect($url);
            }
        }
    }
    
    public function cron_updatebillingcycle_forsubuser() {

        $todaydate = date("Y-m-d H:i:s");
        $offline_users = $this->Request_model->getAgencyuserinfo("","",1);
        
        $subusers = $this->Admin_model->getallsubuser_data($offline_users['user_id'],1);
        if (!empty($subusers)) {
            foreach ($subusers as $subusers_data) {
                $time = strtotime($subusers_data['billing_end_date']);
                if($subusers_data['plan_type'] == "monthly"){
                    $enddate = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", $time) . " +1 month"));
                }else{
                    $enddate = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", $time) . " + 1 year"));
                }
                if ($subusers_data['billing_end_date'] != '' && strtotime($todaydate) >= $time) {

                    $data = array('billing_start_date' => $subusers_data['billing_end_date'],
                        'billing_end_date' => $enddate);
                    $this->Welcome_model->update_data("users", $data, array("id" => $subusers_data['id']));
                }
            }
        }
    
    }
     
    public function reactivate_subscription() {
        
         $planname = isset($_POST['parent_plan_name'])?$_POST['parent_plan_name']:'';
         $customerid = isset($_POST['customer_id'])?$_POST['customer_id']:'';
         $inprogress_req = isset($_POST['total_inprogress_req'])?$_POST['total_inprogress_req']:'';
         $user_state = isset($_POST['user_state'])?$_POST['user_state']:'';
         $flag = isset($_POST['reactivate_pro'])?$_POST['reactivate_pro']:'';
         $userid = $this->load->get_var('main_user');
         if(array_key_exists($user_state, STATE_TEXAS)){
                $state_tax = STATE_TEXAS[$user_state];
            }else{
                $state_tax = 0; 
            }
         $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($customerid, $planname, $inprogress_req,$state_tax);
         if($stripe_subscription['status'] == 1){
             $current_date = date('Y-m-d H:i:s',$stripe_subscription['data']['period_start']);
             $billing_expire = date('Y-m-d H:i:s',$stripe_subscription['data']['period_end']);
             $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
             $customer['billing_start_date'] = $current_date;
             $customer['billing_end_date'] = $billing_expire;
             $customer['is_cancel_subscription'] = 0;
             $success = $this->Welcome_model->update_data("users", $customer,array('customer_id' => $customerid));
             if ($success) {
                if($flag == 'yes'){
                 $this->myfunctions->movecancelprojecttopreviousstatus($userid);
                }
                $this->session->set_flashdata('message_success', "Sucessfully reactivate your plan..", 5);
                redirect(base_url() . "customer/request/design_request");
                
         }
         
     }
     else{
         $this->session->set_flashdata('message_error', $stripe_subscription['message'], 5);
         redirect(base_url() . "customer/request/design_request");
     }
    }
    
    public function cancelproject($id) {
        $user_id = $this->load->get_var('main_user');
        $customer = $this->Admin_model->getuser_data($user_id);
        $login_user_id = $this->load->get_var('login_user_id'); 
        $requests = $this->Request_model->get_request_by_id($id);
        $success = $this->Welcome_model->update_data("requests", array("status" => "cancel", "status_designer" => "cancel", "status_admin" => "cancel", "status_qa" => "cancel","previous_status"=>"", "who_reject" => 0,"priority" => 0,"sub_user_priority" => 0, "latest_update" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $id));
        $ActiveReq = $this->myfunctions->CheckActiveRequest($user_id);
        if ($success) {
            $cancelpriority = isset($requests[0]['priority']) ? $requests[0]['priority'] : '';
            if ($cancelpriority != 0) {
                $cancelsubpriority = isset($requests[0]['sub_user_priority']) ? $requests[0]['sub_user_priority'] : '';
                $priorfrom = $cancelpriority + 1;
                $subpriorfrom = $cancelsubpriority + 1;
                $this->Admin_model->update_priority_after_approved_qa(1, $priorfrom, $user_id);
                $this->Admin_model->update_subuserpriority_after_approved_qa(1, $user_id,$requests[0]['created_by'],$subpriorfrom);
            }
            if($ActiveReq == 1 && $customer[0]['is_cancel_subscription'] != 1){
             $this->myfunctions->move_project_inqueqe_to_inprogress($user_id,"","cancelproject",$id);
            }
            $this->myfunctions->capture_project_activity($id,'','',$requests[0]['status'].'_to_cancel','cancelproject',$login_user_id);
            $this->session->set_flashdata('message_success', "Your project cancelled successfully", 5);
            redirect(base_url() . "customer/request/design_request");
        }
    }

      
      /*******call different views for left & right message body********/

      public function Deletemsg(){
          $dele_id = isset($_POST['delid'])?$_POST['delid']:'';
          $data['is_deleted'] = 1;
          $success = $this->Welcome_model->update_data("request_discussions", $data, array("id" => $dele_id));
          if($success){
              echo json_encode($dele_id);
          }
      }
      public function Editmsg(){
        $editid = isset($_POST['editid'])?$_POST['editid']:''; 
        $msg = isset($_POST['msg'])?$_POST['msg']:''; 
        $data['is_edited'] = 1;
        $data['message'] = $msg;
          $success = $this->Welcome_model->update_data("request_discussions", $data, array("id" => $editid));
          if($success){
              echo json_encode($editid);
          }
        }
        
        public function isDisabledlink(){
            $reqid = isset($_POST['reqid'])?$_POST['reqid']:'';
            $draftid = isset($_POST['draftid'])?$_POST['draftid']:'';
            $is_disabled = isset($_POST['is_disabled'])?$_POST['is_disabled']:'';
            $login_user_id = $this->load->get_var('login_user_id'); 
            $data['is_disabled'] = $is_disabled;
            $success = $this->Welcome_model->update_data("link_sharing", $data, array("request_id" => $reqid,"draft_id" => $draftid,"share_type" => "public"));
            if($success){
            if($is_disabled == 0){
                $this->myfunctions->capture_project_activity($reqid,$draftid,'','enable_public_link','isDisabledlink',$login_user_id);
              }else{
                $this->myfunctions->capture_project_activity($reqid,$draftid,'','disable_public_link','isDisabledlink',$login_user_id);  
              }
              echo json_encode($reqid);
          }
        }
        
        public function draftsharelinkpermissions(){
            $reqid = isset($_POST['reqid'])?$_POST['reqid']:'';
            $draft_id = isset($_POST['draft_id'])?$_POST['draft_id']:'';
          //  echo "<pre>";print_r($_POST);
//            $success = $this->Welcome_model->update_data("link_sharing", $data, array("request_id" => $reqid,"share_type" => "public"));
//            if($success){
//              echo json_encode($reqid);
//          }
        }
        public function download_projectfile($id,$filename="") {
             $filename = $_GET['imgpath'];
            $file_name1 = FS_PATH_PUBLIC_UPLOADS_REQUESTS.$id."/".$filename;
            //$file_name1 = str_replace(" ", "_", $file_name1);
            $this->load->helper('download');
            $path = file_get_contents($file_name1); // get file name
            $name = $filename; // new name for your file
            force_download($name, $path);
            
            redirect(base_url() . "customer/request/project_info/" . $id);
        }
        
//        public function mergedotcomments() {
//        $box_width = 925;
//        $offset_value = 6;
//        // get all the design drafts files for active projects (pending_approval, revision, in progress)
//        // exclude files uploaded by customer
//        // foreach loop of all the images 
//        // update the final values in the DB ($xco_new, $yco_new)
//        $allfiles = $this->Request_model->getallfilefordotcomment();
//        $i = 0;
//        foreach ($allfiles as $allfiles_d) {
//            $currentxco_yco = $this->Request_model->getxco_ycofordotcomment($allfiles_d['id']);
//            if (!empty($currentxco_yco)) {
//                if ($allfiles_d['actual_image_width'] == NULL || $allfiles_d['actual_image_height'] == NULL) {
//                    list($img_width, $img_height) = getimagesize("https://s3.us-east-2.amazonaws.com/graphics-zoo-ohio/public/uploads/requests/" . $allfiles_d['request_id'] . "/" . $allfiles_d['file_name']);
//                
//                $data1 = array("actual_image_width" => $img_width,
//                        "actual_image_height" => $img_height);
//                $this->Welcome_model->update_data("request_files", $data1, array("id" => $allfiles_d['id']));
//                }else{
//                    $img_width = $allfiles_d['actual_image_width'];
//                    $img_height = $allfiles_d['actual_image_height'];
//                }
//                foreach ($currentxco_yco as $getcurrentxco_yco) {
//                    // these values will be from database
//                    $xco_current = $getcurrentxco_yco['xco'] + $offset_value;
//                    $yco_current = $getcurrentxco_yco['yco'] + $offset_value;
//
//                    // calculate aspect ratio
//                    // Get the image scale.
//                    $box_scale = $img_width / $img_height;
//                    if ($img_width < $box_width) {
//                        // need to check this case if existing image is less than 925 width
//                        continue;
//                    }
//                    $box_height = $box_width / $box_scale;
//                    $width_scale = ($xco_current / $box_width) * 100;
//                    $height_scale = ($yco_current / $box_height) * 100;
//
//                    $xco_new = ($img_width / 100) * $width_scale;
//                    $yco_new = ($img_height / 100) * $height_scale;
//                    $data = array("xco_new" => $xco_new,
//                        "yco_new" => $yco_new,
//                        "is_updatecomment_pos" => 1);
//                    if($getcurrentxco_yco['is_updatecomment_pos'] != 1){
//                    $this->Welcome_model->update_data("request_file_chat", $data, array("id" => $getcurrentxco_yco['id']));
//                    }
//                    echo $i++;
//                    }
//               
//            }
//        }
//    }
        
//    public function moveallapprovedfiles() {
//        $approvedfiles = $this->Request_model->getapprovedfiles();
//       // echo "<pre>";print_r($approvedfiles);exit;
//        foreach ($approvedfiles as $file) {
//        $srcpath = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$file['request_id'].'/';
//        $destpath = FS_UPLOAD_PUBLIC_UPLOADS.'approved_files/';
//        $res = $this->s3_upload->copyexistfiles($srcpath,$destpath,$file['file_name']);
//        if($res['status'] == 1){
//            echo $file['request_id']."/".$file['file_name']." - success<br>";
//        }else{
//            echo $file['request_id']."/".$file['file_name']." - error<br>"; 
//        }
//        }
//    }
 
    public function closeSitetour(){
        $login_user_id = $this->load->get_var('login_user_id');
        $success = $this->Welcome_model->update_data("users", array('is_logged_in' => 1), array('id' => $login_user_id));
        echo json_encode($success);
    }
    
    public function is_show_file_sharing() {
        $users_data = $this->load->get_var('parent_user_data');
        $login_user_data = $this->load->get_var('login_user_data');
        if ($users_data[0]['overwrite'] == 1) {
            if ($users_data[0]['file_sharing'] == 1) {
                $return = 1;
            } else {
                $return = 0;
            }
        } else {
            $user_plan_data = $this->load->get_var('parent_user_plan');
            if ($user_plan_data[0]['file_sharing'] == 1) {
                $return = 1;
            } else {
                $return = 0;
            }
        }
        if ($login_user_data[0]['parent_id'] != 0 && $login_user_data[0]['user_flag'] == 'client' && $return == 1) {
            if ($login_user_data[0]['overwrite'] == 1) {
                if ($login_user_data[0]['file_sharing'] == 1) {
                    $return = 1;
                } else {
                    $return = 0;
                }
            } else {
                $loginuser_pln = $this->Clients_model->getsubsbaseduserbyplanid($login_user_data[0]['plan_name'],"",$users_data[0]['id']);
                if ($loginuser_pln[0]['file_sharing'] == 1) {
                    $return = 1;
                } else {
                    $return = 0;
                }
            }
        }
        return $return;
    }

    public function sharing_permision_ajaxCall(){
       $reqid = $this->input->post("request_id");
       $draftid = $this->input->post("draftid");
       $result["reqid"] = $reqid;
       $result["alldraftpubliclinks"] = $this->Request_model->getallPubliclinkinfo($reqid,$draftid);
       $result["shareddraftUser"] =  $this->Request_model->getSharedpeopleinfobyreq_draftID($reqid,$draftid); 
//       echo "<pre>"; print_r($result); die; 
       $HTml = $this->load->view("customer/ShareLinkhtml",$result);
       
     
    }
    
    public function getCategoryquestions() {
        $output = array();
        $cat_id = $this->input->post('catID');
        $subcat_id = $this->input->post('subcat');
        $edit_id = $this->input->post('edit_id');
        $catid = (isset($cat_id) && $cat_id != '') ? $cat_id : '';
        $subcatid = (isset($subcat_id) || $subcat_id != '') ? $subcat_id : 0;
        $editid = (isset($edit_id) || $edit_id != '') ? $edit_id : '';
        if ($editid) {
            $all_quest = $this->Request_model->getallquestions($catid, $subcatid);
//            echo "<pre/>";print_R($all_quest);

            /*             * *****get questions******* */
            $getrec = array();
            foreach ($all_quest as $qk => $qv) {
                $getrec = $this->Category_model->get_quest_Ans($editid, $qv['id']);
                $all_quest[$qk]['answer'] = $getrec[0]['answer'];
//               echo "<pre/>";print_R($getrec);
            }
//            echo "<pre/>";print_R($all_quest);
            /*             * *****get answers******* */
        } else {
            $all_quest = $this->Request_model->getallquestions($catid, $subcatid);
        }
//        echo "<pre/>";print_R($all_quest);
        if (!empty($all_quest)) {
            $output['status'] = 'success';
            $output['data'] = $all_quest;
        } else {
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }
    
    
    /*******samples********/
    public function getSamples(){
        $output = array();
        $cat_id = $this->input->post('catID');
        $subcat_id = $this->input->post('subcat');
        $edit_id = $this->input->post('edit_id');
        $catid = (isset($cat_id) && $cat_id != '') ? $cat_id : '';
        $subcatid = (isset($subcat_id) || $subcat_id != '') ? $subcat_id : 0;
        $editid = (isset($edit_id) || $edit_id != '') ? $edit_id : '';
        $all_samples = $this->Category_model->getSampleMaterialsbycat_subcat($catid, $subcatid);
        if (!empty($all_samples)) {
            $output['status'] = 'success';
            $output['data'] = $all_samples;
        } else {
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }


    /* pixabay &unsplash api code start here */
    
    public function get_all_imagesPost() {
        $res = array();
        $res2 = array();
        $API_KEY_pixabay = '14096718-167c5c7a1912fadb24a99bd94';
       // live $API_KEY_unsplash = 'a62312dc9af687253e0724d98ba1b3d64850674a230413619f6ef1e2a9581ecd';
        // $API_KEY_pixabay = '13639244-b7e97276b9fe1886d2c5f3e8d';
        $API_KEY_unsplash = 'd13cd5fc39d464bd270f1eb92354ea7b83a482279f8df76c7330da632618a686';
        $search_keyword = $this->input->post("keyword");
        $filter = $this->input->post("filter");
        $filter_unsplash = $this->input->post("filter_un");
        $load_more = $this->input->post("load_more");
        $URL_pixabay = "https://pixabay.com/api/?key=" . $API_KEY_pixabay . "&q=" . urlencode($search_keyword) . "&per_page=25&page=" . $load_more . "&orientation=" . $filter;
        $URL_unsplash = "https://api.unsplash.com/search/photos?client_id=" . $API_KEY_unsplash . "&query=" . urlencode($search_keyword) . "&per_page=25&page=" .
                $load_more . "&orientation=" . $filter_unsplash;
        $res[] = $this->getDatafromCurl($URL_pixabay);
        $res[] = $this->getDatafromCurl($URL_unsplash);
        $newRes = json_encode($res);
        echo $newRes;
        exit;
    }

    public function getDatafromCurl($url) {
        $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        // Execute
        $result = curl_exec($ch);
        // Closing
        curl_close($ch);
        // Will dump a beauty json :3
        return json_decode($result);
    }

    public function download_images() {
        $req_fromPage = $this->input->post("req_fromPage");
        $image_url = $this->input->post("file-upload");
        $this->load->helper('download');
        $path = array();
        $userName =  $_SESSION["first_name"].'_'.$_SESSION["last_name"];
        if(sizeof($image_url)>1){
        foreach ($image_url as $k => $images) {
            $imgN = $this->addHttps($images);
            $this->zip->read_file($imgN);        
          }
         $this->zip->download($userName.'_photos.zip'); 
         }else{
            $imgN = $this->addHttps($image_url[0]);
            // echo $imgN; exit;  
            $baseName = basename($imgN);
            $url = file_get_contents($imgN);
            force_download($baseName,$url);
         }   
      }

    public function download_remote_server(){
//          echo "<pre>"; print_r($_POST); die;
        $req_fromPage =  $this->input->post("req_fromPage");
        $image_url =  $this->input->post("file-upload");
        $page_url = $this->input->post("pageurl");
        $image_id =  $this->input->post("ids");
        $search_keyword =  $this->input->post("search_keyword");
        $userName =  $_SESSION["first_name"].$_SESSION["last_name"];
        $realPath = dirname(dirname(dirname(dirname(__FILE__))));  
        $nextPAth ="/public/uploads/temp/";
        $dateFolder = strtotime(date('Y-m-d H:i:s'));
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output["img_link"] = array();
        $output['error'] = 'Error while uploading the files. Please try again.';
        $dateFolder = strtotime(date('Y-m-d H:i:s'));

        // echo "<pre>"; print_r($page_url); die; 
        if (!isset($_SESSION['temp_folder_names']) && $_SESSION['temp_folder_names'] != '') {
            // echo "ifmake"; die; 
            if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                $_SESSION['temp_folder_names'] = './public/uploads/temp/' . $dateFolder;
            } else {
                $_SESSION['temp_folder_names'] = '';
            }
        } else {
            // echo "else"; die; 
            if (!is_dir($_SESSION['temp_folder_names'])) {
                if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                    $_SESSION['temp_folder_names'] = './public/uploads/temp/' . $dateFolder;
                    
                } else {
                    $_SESSION['temp_folder_names'] = '';
                }
            }
        }
        $landingPath = $_SESSION['temp_folder_names'] . "/";
        // echo $landingPath; die; 
        $filename = array();

        foreach ($image_url as $ky => $img) {
            $imgN = $this->addHttps($img);
            $page_urlN = $this->addHttps($page_url[$ky]);
            $count =  $image_id[$ky];
            $ext = $this->get_extension($img);
             
            if (is_numeric($count)) {
                $nameEx = "px_";
            } else {
                $nameEx = "un_";
            }
            $save_name = $nameEx.$count.".".$ext;
            $link_array = explode('/',$_SESSION['temp_folder_names']);
            $folderCr = end($link_array);
             
           
            if(file_exists($landingPath.$save_name) != 1){
                file_put_contents($landingPath.$save_name,file_get_contents($imgN));  
                $file_size[$ky] = filesize($landingPath.$save_name);      
                $output['files'][$ky] = $save_name; 
                $output["file_size"] = $file_size;
                $output["img_link"][$ky] = $page_urlN;
                $output['status'] = true;
                $output['error'] = false;
                $output['path']="";
            } else {
                $file_size[$ky] = "";
                $output['files'][$ky] = "";
                $output["file_size"] = "";
                $output["img_link"][$ky] = "";
                $output['status'] = false;
                $output['filename'][$ky] = $save_name;  
                $output['path'][$ky] = base_url()."public/uploads/temp/".$folderCr."/"; 
            }
        }
           $output['error'] = $output['filename'];
           $output['error'] = $output['path']; 
           
        //redirect(base_url().'/customer/request/'.$req_fromPage);
        echo json_encode($output);
    }

     /* add https to url */
    public function addHttps($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) { 
            // If not exist then add http 
            $url = "https://" . $url; 
        } 

        return $url; 
    }

    public function get_extension($file) {
        if (strpos($file, 'fm') !== false) {
            preg_match_all('#&fm=([^s]+)&#', $file, $matches);
            $findout = implode(' ', $matches[1]);
            $extension = substr($findout, 0, strpos($findout, "&"));
            return $extension ? $extension : false;
        } else {
            $extension = end(explode(".", $file));
            return $extension ? $extension : false;
        }
    }

    public function mark_as_favorite()
        {
           $reqType =  $this->input->post('mark');
           $projectid = $this->input->post('projectID');
           $folder_id = $this->input->post('folder_id');
           $id = $projectid;
           $req_id = $this->input->post('user_id');
           $brand_id = $req_id;
           $filename = $this->input->post('filename');
           $role = $this->input->post('role');
           
           if($projectid != 0 && $req_id != 0 && $role != "brand"){
            $condiARR = array(
                'id' => $projectid,
                'request_id' => $req_id
            );
            $table = "request_files";
           }else if($role == "brand"){
               $condiARR = array(
                 'brand_id' =>  $id,
                 'filename' => $filename
                );
                $table = "brand_profile_material";
           }else{
               $condiARR = array(
                 'folder_id' =>  $folder_id,
                   'file_name' => $filename
                );
                $table = "folder_file_structure";
            }
           if($reqType =='unfav'){
             $data = array('mark_as_fav' => 0); 
           }else{
             $data = array('mark_as_fav' => 1); 
           }
            $this->db->where($condiARR);
            $this->db->update($table,$data);
             //echo $this->db->last_query(); die;
            $afftectedRows = $this->db->affected_rows();
            echo $afftectedRows;    
    }

    public function open_file_type() {
        $product_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        $imgurl = isset($_POST['url']) ? $_POST['url'] : '';
        if ($imgurl == "") {
            $url = FS_PATH_PUBLIC_UPLOADS_REQUESTS . $product_id . '/' . $file;
        } else {
            $url = $imgurl;
        }
        $file = isset($_POST['file']) ? $_POST['file'] : basename($url);
        $ext = strtolower($this->get_extension($file));
//        echo "url".$url."<br/>";
//        echo $ext;exit;
        $docArrOffice = array('odt', 'ods', 'xlsx', 'xls', 'txt');
        $docArrgoogleoffcie = array("docx",'doc','ai', 'pdf', 'ppt', 'pptx', 'pps', 'ppsx', 'tiff', 'xxx', 'eps');
        $font_file = array('ttf', 'otf', 'woff', 'eot', 'svg');
        $vedioArr = array('mp4', 'Mov');
        $audioArr = array('mp3');
        $ImageArr = array('jpg', 'png', 'gif', 'jpeg', 'bmp');

        if (in_array($ext, $docArrOffice)) {
            echo '<iframe src="https://view.officeapps.live.com/op/view.aspx?src=' . $url . '" style="width:600px; height:500px;" frameborder="0"></iframe>';
        } elseif (in_array($ext, $docArrgoogleoffcie)) {
            echo '<iframe src="https://docs.google.com/viewer?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
        } else if (in_array($ext, $vedioArr)) {

            echo '<video width="400" controls>
                          <source src="' . $url . '" type="video/' . $ext . '">
                          Your browser does not support HTML5 video.
                      </video>';
        } else if (in_array($ext, $audioArr)) {
            echo '<audio controls>
                              <source src="' . $url . '" type="audio/' . $ext . '">        
                            Your browser does not support the audio element.
                          </audio>';
        } elseif ($ext == 'zip') {
            echo "0";
        } elseif ($ext == 'psd') {

            echo "0";
            /* echo '<iframe src="https://www.photopea.com/#'.base64_encode($json_code).'" style="width:600px; height:500px;" frameborder="0"></iframe>'; */
        } elseif (in_array($ext, $font_file)) {
            $data['extension'] = $ext;
            $data['url'] = $url;
            $data['font_name'] = $file;
            $this->load->view('customer/font_family', $data);
        } elseif (in_array($ext, $ImageArr)) {
            echo "<img src='". $url ."'>";
        }
    }
    /* pixabay &unsplash api code Ends here  */
    
    public function get_designfeedback() {
        $id = isset($_POST["id"])?$_POST["id"]:"";
        $role = isset($_POST["role"])?$_POST["role"]:"";
        $output = array();
        if($id != ""){
            if($role == "admin"){
                $output["customer"] = $this->Request_model->get_reviewfeedback($id,"customer");
                $output["admin"] = $this->Request_model->get_reviewfeedback($id,"admin");
            }else{
                $output = $this->Request_model->get_reviewfeedback($id,"customer");
            }
            echo json_encode($output);exit;
        }
        
    }
    
    /********download file from main chat******/
     public function download_stuffFromChat() {
        
        $req_fromPage = $this->input->post("req_fromPage");
        //$image_url = $this->input->post("file-upload");
        $image_url = $this->input->get("file-upload");
        //$ext = $this->input->post("ext");
        $ext = $this->input->get("ext");

      $docArrOffice = array('doc', 'docx', 'odt', 'ods', 'xlsx', 'xls', 'txt','ai', 'pdf', 'ppt', 'pptx', 'pps', 'ppsx', 'tiff', 'xxx', 'eps');
        $font_file = array('ttf', 'otf', 'woff', 'eot', 'svg');
        $vedioArr = array('mp4', 'Mov');
        $audioArr = array('mp3');
        $ImageArr = array('jpg', 'png', 'gif', 'jpeg', 'bmp');
        $zipArr = array('zip', 'rar');
 
    $arrContextOptions = array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false));  
    
    
  //  ini_set('display_errors', 1);
     if (in_array($ext, $docArrOffice)) { 
        // echo  $image_url."<br>"; 
                 $this->load->helper('download');
                $baseName = basename($image_url);
                $url = file_get_contents($image_url,false, stream_context_create($arrContextOptions));
                // echo $url; exit; 
                 force_download($baseName,$url);
              
            }elseif (in_array($ext,$font_file)){
                 $this->load->helper('download');
                $baseName = basename($image_url);
                $url = file_get_contents($image_url,false, stream_context_create($arrContextOptions));
                force_download($baseName,$url);
            }elseif (in_array($ext,$zipArr)){
                
                 $this->zip->read_file($image_url);      
                 $baseName = basename($image_url); 
                 $this->zip->download($baseName);  
            }elseif (in_array($ext,$ImageArr)){
                $this->load->helper('download');
                $baseName = basename($image_url);
                $url = file_get_contents($image_url,false, stream_context_create($arrContextOptions) );
                force_download($baseName,$url);
                 
            }

        }
}
