<style type="text/css">
  #myBtnContainer .active{
    border-bottom: 2px solid #e83833;
    border-radius: 0px;
  }
</style>
<section id="hero_section" class="wow fadeIn pb-5">
  <div class="hero-container">
    <div class="container">
      <div class="row">
        <div class="col-md-12 mt-5 pt-5">
          <div class="section-title text-center">
            <h2>Portfolio</h2>
          </div>
        </div>
        <div class="pt-5 pb-5 mt-5 mb-5"></div>
      </div>
      <div class="row">
        <h2 style="color: #87909E; width: 100%; text-align:center; font-weight:600">See a glimpse of our creative design work</h2>
      </div>
      <!-- <div id="myBtnContainer" class="portfolio_list">
        <button class="btn btn-default filter-button active" onclick="filterSelection('all')"> Show all</button>
        <button class="btn btn-default filter-button" onclick="filterSelection('logo')"> Logo</button>
        <button class="btn btn-default filter-button" onclick="filterSelection('advertising')"> Advertising</button>
        <button class="btn btn-default filter-button" onclick="filterSelection('shirt')"> Shirt Design</button>
        <button class="btn btn-default filter-button" onclick="filterSelection('poster')"> Posters & Flyers</button>
        <button class="btn btn-default filter-button" onclick="filterSelection('other')"> Others</button>
        <button class="btn btn-default filter-button" onclick="filterSelection('packaging')"> Packaging</button>
        <button class="btn btn-default filter-button" onclick="filterSelection('book')"> Book Cover</button>
        <button class="btn btn-default filter-button" onclick="filterSelection('web')"> Web Design</button>
      </div> -->
      </div>
    </div>
  </div>
</section><!-- #hero -->
<section class="portfolio_items_">
  <div class="container">
    <div class="row">
      <?php for ($i=0; $i < count($data); $i++) { ?>
      <div class="column <?php echo $data[$i]['category']; ?>">
        <div class="content">
          <img src="<?php echo base_url();?>/public/img/<?php echo $data[$i]['image']; ?>" alt="logo" style="width:100%">
          <div class="portfolio-overlay"></div>
          <h4><?php echo $data[$i]['title']; ?></h4>
          <p><?php echo substr( strip_tags($data[$i]['body']), 0, 30); ?></p>
        </div>
      </div>
    <?php } ?>
      <!-- <div class="column logo">
        <div class="content">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/portfolio1.jpg" alt="logo" style="width:100%">
          <div class="portfolio-overlay"></div>
          <h4>T-Shirt Design</h4>
          <p>Lorem ipsum simply design</p>
        </div>
      </div>
      <div class="column book">
       <div class="content">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/portfolio5.jpg" alt="logo" style="width:100%">
          <div class="portfolio-overlay"></div>
          <h4>T-Shirt Design</h4>
          <p>Lorem ipsum simply design</p>
        </div>
      </div>

      <div class="column book">
        <div class="content">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/portfolio1.jpg" alt="logo" style="width:100%">
          <div class="portfolio-overlay"></div>
          <h4>T-Shirt Design</h4>
          <p>Lorem ipsum simply design</p>
        </div>
      </div>
      <div class="column web">
        <div class="content">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/portfolio5.jpg" alt="logo" style="width:100%">
          <div class="portfolio-overlay"></div>
          <h4>T-Shirt Design</h4>
          <p>Lorem ipsum simply design</p>
        </div>
      </div>
      <div class="column advertising">
        <div class="content">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/portfolio1.jpg" alt="logo" style="width:100%">
          <div class="portfolio-overlay"></div>
          <h4>T-Shirt Design</h4>
          <p>Lorem ipsum simply design</p>
        </div>
      </div>

      <div class="column packaging">
        <div class="content">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/portfolio5.jpg" alt="logo" style="width:100%">
          <div class="portfolio-overlay"></div>
          <h4>T-Shirt Design</h4>
          <p>Lorem ipsum simply design</p>
        </div>
      </div>
      <div class="column shirt">
        <div class="content">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/portfolio5.jpg" alt="logo" style="width:100%">
          <div class="portfolio-overlay"></div>
          <h4>T-Shirt Design</h4>
          <p>Lorem ipsum simply design</p>
        </div>
      </div>
      <div class="column shirt">
        <div class="content">
          <img src="<?php echo base_url(); ?>public/front_end/Updated_Design/img/portfolio/portfolio1.jpg" alt="logo" style="width:100%">
          <div class="portfolio-overlay"></div>
          <h4>T-Shirt Design</h4>
          <p>Lorem ipsum simply design</p>
        </div>
      </div> -->
    <!-- END GRID -->
    </div>
  </div>
</section>