<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 public function __construct(){
	  parent::__construct();
		$this->load->library('javascript');
		$this->load->library('form_validation');
		$this->load->library('email');
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('Admin_model');
		$this->load->model('Welcome_model');
		$this->load->model('Stripe');
		$this->load->model('Request_model');
		$this->load->model('Account_model');
	}
	
	public function checkloginuser()
	{
		if(!$this->session->userdata('user_id')){
			redirect(base_url() );
		}
		if($this->session->userdata('role') != "va"){
			redirect(base_url() );
		}
	}
	
	public function index()
	{		
	$this->checkloginuser();
		
		$data['incoming_request'] = $this->Request_model->getall_request("","'pending','assign'");
		
		$data['ongoing_request'] = $this->Request_model->getall_request("","'active','pendingforapprove','checkforapprove'");
		$data['pending_request'] = $this->Request_model->getall_request("","'pending','assign'");
		$data['approved_request'] = $this->Request_model->getall_request("","'approved'");
		
		$this->load->view('va/va_header');
		$this->load->view('va/index',$data);
		$this->load->view('va/va_footer');
	}
	
	public function view_project($id = null){
		
		$this->checkloginuser();
		if($id==""){
			redirect(base_url()."va/dashboard");
		}
		
		$request = $this->Request_model->get_request_by_id($id);
		
		$request_id = $request[0]['id'];
		
		$request_files = $this->Admin_model->get_requested_files($request_id,$request[0]['customer_id'],"customer");
								
		$admin_request_files = $this->Admin_model->get_requested_files($request_id,"","designer");		
		
		$customer_additional_files = $this->Admin_model->get_requested_files($request_id,"","customer");
				
		$admin_files = $this->Admin_model->get_requested_files($request_id,"","admin");
		
		$designer_list = $this->Admin_model->get_total_customer("designer");
		$designer_preview_file = $this->Admin_model->get_requested_files($id,"","designer","1");
		$designer_source_file = $this->Admin_model->get_requested_files($id,"","designer");
		
		$customer = $this->Account_model->getuserbyid($request[0]['customer_id']);
		if(!empty($customer)){
			$request[0]['customer_name'] = $customer[0]['first_name']." ".$customer[0]['last_name'];
		}
		
		$designer = $this->Account_model->getuserbyid($request[0]['designer_id']);
		if(!empty($designer)){
			$request[0]['designer_name'] = $designer[0]['first_name']." ".$designer[0]['last_name'];
		}
		
		$chat_request = $this->Request_model->get_chat_request_by_id($request_id);
		
		$this->load->view('va/va_header');
		$this->load->view('va/view_project',array("data"=>$request,"request_files"=>$request_files,"admin_request_files"=>$admin_request_files,"designer_list"=>$designer_list,"customer_additional_files"=>$customer_additional_files,"admin_files"=>$admin_files,"designer_preview_file"=>$designer_preview_file,"designer_source_file"=>$designer_source_file,'request_id'=>$id,"chat_request"=>$chat_request));
		$this->load->view('va/va_footer');
		
	}
	
	public function view_files($id = null){
		$this->checkloginuser();
		if($id==""){
			redirect(base_url()."va/dashboard");
		}
			$designer_file = $this->Admin_model->get_requested_files($_GET['id'],"","designer","1");
		$data['request'] = $this->Request_model->get_request_by_id($_GET['id']);
		
		for($i=0;$i<sizeof($designer_file);$i++){
			$designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id']);
			
		}
		
		$data['designer_file'] = $designer_file;
		$this->load->view('va/va_header');
		$this->load->view('va/view_files',$data);
		$this->load->view('va/va_footer');
		
	}
	
	public function view_clients()
	{
		$this->checkloginuser();
		$data = $this->Account_model->getall_customer();
		$this->load->view('va/va_header');
		$this->load->view('va/view_clients',array("data"=>$data));
		$this->load->view('va/va_footer');
	}
	
	public function client_projects($id = null){
		$this->checkloginuser();
		if($id==""){
			redirect(base_url()."va/dashboard");
		}
		$data['userdata'] = $this->Account_model->getuserbyid($id);
		
		if(empty($data['userdata'])){
			redirect(base_url()."va/dashboard");
		}
		
		$data['incoming_request'] = $this->Request_model->getall_request($id,"'pending','assign'");
		
		$data['ongoing_request'] = $this->Request_model->getall_request($id,"'active','pendingforapprove','checkforapprove'");
		$data['pending_request'] = $this->Request_model->getall_request($id,"'pending','assign'");
		$data['approved_request'] = $this->Request_model->getall_request($id,"'approved'");
		$this->load->view('va/va_header');
		$this->load->view('va/client_projects',$data);
		$this->load->view('va/va_footer');
		
	}
	
	public function chat($user_type = null)
	{
		$this->checkloginuser();
		if($user_type == "customer" || $user_type==""){
			$data['customer'] = $this->Account_model->getall_customer();
			$data['active_tab'] = "customer";
		}
		if($user_type == "designer"){
			$data['customer'] = $this->Account_model->getall_designer();
			$data['active_tab'] = "designer";
		}
		if($user_type == "qa"){
			$data['customer'] = $this->Account_model->getqa_member();
			$data['active_tab'] = "qa";
		}
		$this->load->view('va/va_header');
		$this->load->view('va/chat',$data);
		$this->load->view('va/va_footer');
	}
	
	public function room($id = null){
		if($id=="" || !isset($_GET['user_type'])){
			redirect(base_url()."va/dashboard/chat");
		}
		$first = "va_id";
		
		if($_GET['user_type'] == "customer"){
			$second = "customer_id";
			$active_tab = "customer";
		}
		if($_GET['user_type'] == "designer"){
			$second = "designer_id";
			$active_tab = "designer";
		}
		if($_GET['user_type'] == "qa"){
			$second = "qa_id";
			$active_tab = "qa";
		}
		
		$room_no = $this->Account_model->chat_data($first,$_SESSION['user_id'],$second,$id);
		
		if($room_no==""){
			redirect(base_url()."va/dashboard/chat");
		}
		redirect(base_url()."va/dashboard/room_chat/".$room_no."?user_type=".$active_tab);
	}
	
	public function room_chat($room_no=null)
	{
		if($room_no=="" || !isset($_GET['user_type'])){
			redirect(base_url()."va/dashboard/chat");
		}
		
		if($_GET['user_type'] == "customer"){
			$second = "customer_id";
			$data['active_tab'] = "customer";
			$data['customer'] = $this->Account_model->getall_customer();
		}
		if($_GET['user_type'] == "designer"){
			$second = "designer_id";
			$data['active_tab'] = "designer";
			$data['customer'] = $this->Account_model->getall_designer();
		}
		if($_GET['user_type'] == "qa"){
			$second = "qa_id";
			$data['active_tab'] = "qa";
			$data['customer'] = $this->Account_model->getqa_member();
		}
		$data['userdata'] = $this->Account_model->get_userdata_from_room_id($room_no);
		$data['chat'] = $this->Account_model->get_chat($room_no);
		
		
		$data['room_no'] = $room_no;
		
		$this->load->view('va/va_header');
		$this->load->view('va/chat',$data);
		$this->load->view('va/va_footer');
	}
	
	public function view_designer()
	{
		$this->checkloginuser();
		
		if(!empty($_POST)){
			
			if($_POST['password'] != $_POST['confirm_password']){
				$this->session->set_flashdata('message_error', 'Password and Confirm Password is not Match.!', 5);
				
				redirect(base_url()."qa/dashboard/view_designer");
			}
			$email_check = $this->Account_model->getuserbyemail($_POST['email']);
			if(!empty($email_check)){
				$this->session->set_flashdata('message_error', 'Email Address is already available.!', 5);
				
				redirect(base_url()."qa/dashboard/view_designer");
			}
			$success = $this->Welcome_model->insert_data("users",array("first_name"=>$_POST['first_name'],"last_name"=>$_POST['last_name'],"email"=>$_POST['email'],"phone"=>$_POST['phone'],'new_password'=>$_POST['password'],"status"=>"1","role"=>"designer","created"=>date("Y-m-d H:i:s")));
			if($success){
				$this->session->set_flashdata('message_success', 'Designer Added Successfully.!', 5);
				
				redirect(base_url()."qa/dashboard/view_designer");
			}else{
				$this->session->set_flashdata('message_error', 'Designer Not Added Successfully.!', 5);
				
				redirect(base_url()."qa/dashboard/view_designer");
			}
		}
		$designers = $this->Account_model->getall_designer();
		$this->load->view('va/va_header');
		$this->load->view('va/view_designer',array('designers'=>$designers));
		$this->load->view('va/va_footer');
	}
	
	public function send_room_message()
	{
		if(!empty($_POST)){
			$_POST['created'] = date("Y-m-d H:i:s");
			$success = $this->Welcome_model->insert_data("room_chat",$_POST);
			
			echo $success;
			exit;
		}
	}
	
	public function view_qa()
	{
		$this->checkloginuser();
		$data = $this->Account_model->getallqa();
		$this->load->view('va/va_header');
		$this->load->view('va/view_qa',array("data"=>$data));
		$this->load->view('va/va_footer');
	}
	
	public function rating()
	{
		$this->load->view('va/va_header');
		$this->load->view('va/rating');
		$this->load->view('va/va_footer');
	}
}