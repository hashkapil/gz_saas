<?php
//die("dfgjkh");
defined('BASEPATH') OR exit('No direct script access allowed');

class Request_by_email extends CI_Controller {
    
    public function __construct() { 
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('Myfunctions');
        $this->load->helper('form');
        $this->load->library('s3_upload');
        $this->load->helper('url');
        $this->load->model('Request_model');
         $this->load->model('Category_model');
        $this->load->model('Welcome_model');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Stripe');
        $this->load->library('zip');

        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }
    
    
    public function index(){
      // set user to check
      $strUser     = SET_REQUEST_EMAIL;//  newprojectrequest@graphicszoo.com
      $strPassword = SET_REQUEST_PASSWORD;// 4yX=%z!^jmat
      
      // open
     $hMail = imap_open ("{imap.gmail.com:993/debug/imap/ssl/novalidate-cert}INBOX", "$strUser", "$strPassword");
     
     // get headers
    $aHeaders = imap_headers( $hMail );
    
    // get message count
    $objMail = imap_mailboxmsginfo( $hMail );
    
    // process messages
        for ($idxMsg = 1; $idxMsg <= $objMail->Nmsgs; $idxMsg++) {
            
            // get header info
            $objHeader = imap_headerinfo($hMail, $idxMsg);
           // get from object array
            $structure = imap_fetchstructure($hMail, $idxMsg);
            $aFrom = $objHeader->from;
            $fromEmail = imap_utf8($aFrom[0]->mailbox) .'@'. imap_utf8($aFrom[0]->host);
            $aSubject = imap_utf8($objHeader->subject);
            $body = quoted_printable_decode(imap_fetchbody($hMail,$idxMsg,1.1));    
            if($body == ""){
               $body = imap_fetchbody($hMail, $idxMsg, 1); 
            }


            $attachments = array();
            if (isset($structure->parts) && count($structure->parts)) {
                for ($i = 0; $i < count($structure->parts); $i++) {
                    $attachments[$i] = array(
                        'is_attachment' => false,
                        'filename' => '',
                        'name' => '',
                        'attachment' => '');

                    if ($structure->parts[$i]->ifdparameters) {
                        foreach ($structure->parts[$i]->dparameters as $object) {
                            if (strtolower($object->attribute) == 'filename') {
                                $attachments[$i]['is_attachment'] = true;
                                $attachments[$i]['filename'] = $object->value;
                            }
                        }
                    }

                    if ($structure->parts[$i]->ifparameters) {
                        foreach ($structure->parts[$i]->parameters as $object) {
                            if (strtolower($object->attribute) == 'name') {
                                $attachments[$i]['is_attachment'] = true;
                                $attachments[$i]['name'] = $object->value;
                            }
                        }
                    }

                    if ($attachments[$i]['is_attachment']) {
                        $attachments[$i]['attachment'] = imap_fetchbody($hMail, $idxMsg, $i + 1);
                        if ($structure->parts[$i]->encoding == 3) { // 3 = BASE64
                            $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                        } elseif ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
                            $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                        }
                    }
                } // for($i = 0; $i < count($structure->parts); $i++)
            }
            
            
            
            $checkemail = $this->Request_model->getuserbyemail($fromEmail);
            if (empty($checkemail)) {
                //send return mail to use registeration mail address
                $this->myfunctions->send_email_when_addrequest_by_mail('','',$fromEmail,'reg_mail',$aSubject);
            }elseif($body == "" || $aSubject == "") {
                //send return mail to get required info
               // echo "send return mail to get required info";exit;
                $this->myfunctions->send_email_when_addrequest_by_mail('','',$fromEmail,'blank',$aSubject);
            } else {
              // echo $checkemail[0]['id'];
                $userid = $checkemail[0]['id'];
                $parentid = $checkemail[0]['parent_id'];
                if ($parentid == 0) {
                    $created_user = $userid;
                } else {
                    $created_user = $parentid;
                }
                $canuseradd = $this->myfunctions->isUserPermission('add_requests',$userid);
                $trail_user = $this->Request_model->getuserbyid($created_user);
                if($canuseradd == 0){
                    //send return mail, user have not permissions to add request
                    $this->myfunctions->send_email_when_addrequest_by_mail('','',$fromEmail,'no_permission',$aSubject);
                }else{
                $subcat_data = $this->Category_model->get_category_byID(23);   
                $request_for_all_status = $this->Request_model->getall_request_for_trial_user($created_user);
                $request_for_all_status_acc_billing_cycle = $this->Request_model->getall_request_for_all_status($created_user, $trail_user[0]['billing_start_date'], $trail_user[0]['billing_end_date']);
                $useraddrequest = $this->myfunctions->canUserAddNewRequest($trail_user, $request_for_all_status_acc_billing_cycle, $request_for_all_status);
               
                if ($useraddrequest == 0) {
                    
                     //send return mail, trial user not add request more than one
                    $this->myfunctions->send_email_when_addrequest_by_mail('','',$fromEmail,'trial',$aSubject);
                }
                elseif($useraddrequest == 1) {
                    
                    //send return mail, 99 user not add request more 3 according current billing cycle
                    $this->myfunctions->send_email_when_addrequest_by_mail('','',$fromEmail,'99user',$aSubject);
                }
                elseif($useraddrequest == 3) {
                    
                    //send return mail, cancel subscription user that cann't add request 
                    $this->myfunctions->send_email_when_addrequest_by_mail('','',$fromEmail,'cancelsubs',$aSubject);
                }
                elseif($useraddrequest == 4) {
                    
                    //send return mail, if user have new plan  and user  cann't add request
                    $this->myfunctions->send_email_when_addrequest_by_mail('','',$fromEmail,'newplan',$aSubject);
                }
                elseif($useraddrequest == 5) {
                    
                    //send return mail, user  cann't add request because cross sub user limit for add request
                    $this->myfunctions->send_email_when_addrequest_by_mail('','',$fromEmail,'subuserlimit',$aSubject);
                }
                else{
                    
                    //add request 
                    $data = array("customer_id" => $created_user,
                        "category" => NULL,
                        "logo_brand" => NULL,
                        "brand_id" => NULL,
                        "category_id" => 1,
                        "subcategory_id" => 23,
                        "category_bucket" => 1,
                        "title" => isset($aSubject)?$aSubject:'',
                        "description" => isset($body)?$body:'',
                        "created" => date("Y-m-d H:i:s"),
                        "status" => "draft",
                        "designer_id" => 0,
                        "priority" => 0,
                        "deliverables" => "SourceFiles",
                        "created_by" => $userid,
                        "add_request_by_email" => 1,
                        "color_pref" => "Let Designer Choose"
                        );
                 $success = $this->Welcome_model->insert_data("requests", $data);
                 $this->myfunctions->capture_project_activity($success,'','','project_created','request_by_email',$userid);
                 if($success){
                    if (count($attachments) != 0) {
                                $attach = 0;
                                foreach ($attachments as $at) {
                                    if ($at['is_attachment'] == 1) {
                                        if (!is_dir('/public/uploads/requests/' . $success)) {
                                            $dirpath = mkdir('./public/uploads/requests/' . $success, 0777, TRUE);
                                        }
                                        $filename = FCPATH . 'public/uploads/requests/' . $success . '/' . $at['filename'];
                                        $uploadfile = file_put_contents($filename, $at['attachment']);
                                        if($uploadfile != ''){
                                        $uploaded_file =  base_url().FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$success . '/' . $at['filename'];
                                        
                                        if (UPLOAD_FILE_SERVER == 'bucket') {
                                            $config = array(
                                                    'upload_path' => 'public/uploads/requests/' . $success . '/',
                                                    'file_name' => $at['filename']
                                                );

                                                $this->s3_upload->initialize($config);
                                                $test = $this->s3_upload->upload_multiple_file($uploaded_file);
                                                 unlink($filename);
                                        } 
                                        $request_files_data = array("request_id" => $success,
                                            "user_id" => $created_user,
                                            "user_type" => "customer",
                                            "file_name" => $at['filename'],
                                            "created" => date("Y-m-d H:i:s"));
                                        $data1 = $this->Welcome_model->insert_data("request_files", $request_files_data);
                                       //echo $data.'<br>';
                                        }
                                        $attach++;
                                        
                                    }
                                }
                    }
                    $update_status = $this->myfunctions->changerequeststatusafteradd($created_user,$success,2,$subcat_data);
                    
                    if($update_status == 1){
                        $this->myfunctions->send_email_when_addrequest_by_mail($data['title'],$success,$fromEmail,'req_added',$aSubject);
                    }
                }
                }
                
                }
                
            }
            // delete message
            imap_delete( $hMail, $idxMsg );
        }
        imap_close($hMail);
    }
    
    
}