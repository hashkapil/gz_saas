<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Sitemap extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Welcome_model');
        $this->load->helper('url');
    }
    /**
     * Index Page for this controller.
     *
     */
    public function index()
    {
        $data = $this->Welcome_model->get_blog_category();
        $this->load->view('front_end/sitemap',array("data"=>$data));
    }
}