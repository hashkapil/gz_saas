<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class S_link_sharing extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('user_agent');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('Myfunctions');
        $this->load->helper('form');
        $this->load->helper('url');
         $this->load->model('Affiliated_model');
        $this->load->model('Welcome_model');
        $this->load->model('Clients_model');
        $this->load->model('account/S_admin_model');
        $this->load->model('Category_model');
        $this->load->model('account/S_request_model');
        $this->load->model('Account_model');
        $this->load->model('Stripe');
        $this->load->helper(array('meta'));
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
    }
    
    public function s_project_info($sharekey = NULL){
        $sharekey = isset($sharekey)? $sharekey : '';
        $permissiondata = $this->Welcome_model->getpermissionsbySherekey($sharekey);
        if($permissiondata[0]['is_disabled'] == 0){
           $checkusernamenotexist = (isset($permissiondata[0]['user_name']) && $permissiondata[0]['user_name']!= '')?$permissiondata[0]['user_name']:$_COOKIE['name'];
        if($permissiondata[0]['commenting'] == '1'){
           $checkusernamenotexist = (isset($permissiondata[0]['user_name']) && $permissiondata[0]['user_name']!= '')?$permissiondata[0]['user_name']:$_COOKIE['name'];
        }
        $chat_request = $this->S_request_model->get_chat_request_by_id($permissiondata[0]['request_id'],'desc');
        $request_data = $this->S_request_model->get_request_by_id($permissiondata[0]['request_id']);
        $request_meta = $this->Category_model->get_quest_Ans($permissiondata[0]['request_id']);
        $sampledata = $this->S_request_model->getSamplematerials($permissiondata[0]['request_id']);
        $userdata = $this->S_admin_model->getuser_data($request_data[0]['customer_id']);
        $iscanel = $this->checkuserplaniscancel($userdata);
        if($iscanel == 1){
            redirect(base_url()."accessdenied");
        }
        $timediff = '';
        for ($j = 0; $j < count($chat_request); $j++) {
            $chat_request[$j]['sender_data'] = $this->S_admin_model->getuser_data($chat_request[$j]['sender_id']);
            $chat_request[$j]['msg_created'] = $this->myfunctions->onlytimezoneforall($chat_request[$j]['created']);
            $chat_request[$j]['chat_created_date'] = $chat_request[$j]['msg_created'];
        }
        $data1 = $this->S_request_model->get_request_by_id($permissiondata[0]['request_id']);
        $cat_data = $this->Category_model->get_category_byID($data1[0]['category_id']);
        $cat_name = $cat_data[0]['name'];
        $subcat_data = $this->Category_model->get_category_byID($data1[0]['subcategory_id']);
        $subcat_name = $subcat_data[0]['name'];
        $data1['cat_name'] = $cat_name;
        $data1['subcat_name'] = $subcat_name;
//        echo $permissiondata[0]['request_id'];
        $designer_file = $this->S_admin_model->get_requested_files($permissiondata[0]['request_id'], "", "designer", array('1', '2'),true);
        $data1['count_designerfile'] = count($designer_file);
//        echo "<pre/>";print_R($designer_file);exit;
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified'];
            $designer_file[$i]['chat'] = $this->S_request_model->get_chat_by_id($designer_file[$i]['id'], "desc");
            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
            $designer_file[$i]['chat'][$j]['created'] = $this->myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
            $designer_file[$i]['chat'][$j]['msg_created'] = $this->myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
            $designer_file[$i]['chat'][$j]['replies'] = $this->S_request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
            $designer_file[$i]['chat'][$j]['created'] =   date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
            }
        }
//        echo "<pre/>";print_R($data1);exit;
        for ($i = 0; $i < count($data1[0]['designer_attachment']); $i++) {  
        if($data1[0]['designer_attachment'][$i]['status'] != 'pending' && $data1[0]['designer_attachment'][$i]['status'] != 'Reject'){
            $data1[0]['approved_attachment'] =  $data1[0]['designer_attachment'][$i]['id']; break;
        }
        }
//         echo "<pre/>";print_R($data1);exit;
        $draft_id = $permissiondata[0]['draft_id'];
        if($permissiondata[0]['request_id'] && $permissiondata[0]['draft_id'] == 0){
            $this->load->view('customer/customer_header_1', array("popup" => 0));
            $this->load->view('front_end/project_info', array('permissiondata'=> $permissiondata,'data' => $data1,'chat_request'=>$chat_request,'checkusernamenotexist'=>$checkusernamenotexist,'userdata' =>$userdata,
                "request_meta" => $request_meta,
            "sampledata" => $sampledata));  
        }else
        {
            $this->load->view('customer/customer_header_1', array("popup" => 0));
            $this->load->view('front_end/project_image_view', array('permissiondata'=>$permissiondata,'draft_id'=>$draft_id,'designer_file' => $designer_file,'chat_request'=>$chat_request,'userdata' =>$userdata,'checkusernamenotexist'=>$checkusernamenotexist,'request_data'=>$request_data));  
        }
     }else{
         redirect(base_url()."accessdenied");
     }
    }
    
    
    public function checkuserplaniscancel($userdata="") {
//        echo "<pre/>";print_R($userdata);exit;
       if($userdata[0]['parent_id'] != 0){
            $mainuser = $userdata[0]['parent_id'];
//            echo $mainuser;exit;
            $customer_data = $this->S_admin_model->getuser_data($mainuser,'id,is_cancel_subscription,plan_name');
        }else{
            $customer_data = $userdata;
        }
        if($customer_data[0]['is_cancel_subscription'] == 1){
            return true;
        } 
    }
}