<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('user_agent');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('Myfunctions');
        $this->load->helper('form');
        $this->load->helper('url');
         $this->load->model('Affiliated_model');
        $this->load->model('Welcome_model');
        $this->load->model('Clients_model');
        $this->load->model('Admin_model');
        $this->load->model('Category_model');
        $this->load->model('Request_model');
        $this->load->model('Account_model');
        $this->load->model('Stripe');
        $this->load->helper(array('meta'));
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
    }

//        public function index()
//	{
//		$titlestatus = "";
//		if(isset($_SESSION['role']) != ""){
//			$role_id = $_SESSION['role'];
//			if($role_id == "admin"){
//				redirect(base_url()."admin/dashboard");
//			}
//			if($role_id == "customer"){
//				redirect(base_url()."customer/profile/dashboard");
//                                //redirect(base_url());
//			}
//			if($role_id == "designer"){
//				//redirect(base_url()."designer/profile/dashboard");
//				redirect(base_url()."designer/request");
//			}
//			if($role_id == "qa"){
//				redirect(base_url()."qa/dashboard");
//			}
//			if($role_id == "va"){
//				redirect(base_url()."va/dashboard");
//			}
//		}else{
//			$data = $this->Admin_model->get_all_portfolio();
//			$data_blog = $this->Admin_model->get_all_blog();
//			$this->load->view('front_end/headerNew',array('current_action'=>'home', 'id'=>'index_page','titlestatus' => $titlestatus));
//			$this->load->view('front_end/homeNew',array("data"=>$data,'blog_data' =>$data_blog));
//			$this->load->view('front_end/footerNew');
//		}
//	}


    public function index() { 
            $titlestatus = "";
            $role_id = isset($_SESSION['role'])?$_SESSION['role']:"";
            $data = $this->Admin_model->get_all_portfolio();
            $data_blog = $this->Admin_model->get_all_blog();
           // $this->cron_mail_to_customer_for_unseen_message();
            $this->load->view('front_end/headerNew', array("role_id" => $role_id,'current_action' => 'home', 'id' => 'index_page', 'titlestatus' => $titlestatus));
            $this->load->view('front_end/homeNew', array("data" => $data, 'blog_data' => $data_blog));
            $this->load->view('front_end/footerNew');
     }

    public function set_online_to_offline() {
        $online_data = $this->Welcome_model->set_online_to_offline();
        for ($i = 0; $i < sizeof($online_data); $i++) {

            $date = strtotime($online_data[$i]['last_update']);
            $diff = strtotime(date("Y:m:d H:i:s")) - $date;
            $minutes = floor($diff / 60);
            $hours = floor($diff / 3600);
            if ($minutes > 5) {
                echo "Run successfully..!";
                $this->Welcome_model->update_data("users", array("online" => 0), array("id" => $online_data[$i]['id']));
            }
        }
    }
    
     public function newsletter(){
        $email = isset($_POST['newsletter_email'])?$_POST['newsletter_email']:'';
        /****drip start********/
        $this->load->library('drip');
        $this->load->library('datasett');
        $this->load->library('response');
        // create new subscriber
        $Drip = new Drip('5791e6d8913a121374b5e55904342eb3','1975472');
        $data = new Datasett('subscribers', [
            'email' => $email,                          
        ]);
        $Response = $Drip->post('subscribers', $data);
        // assign free design user tag
        $Drip = new Drip('5791e6d8913a121374b5e55904342eb3','1975472');
        $data = new Datasett('tags', [
                'email' => $email,
                'tag' => "From Graphics Newsletter",
        ]);
        $Drip->post('tags', $data);
        echo "You have subscribed successfully.!";
       // json_encode("You have subscribed successfully.!");
        //$this->session->set_flashdata('message_success', "You have subscribed successfully.!", 5);
       // redirect(base_url());
//        $this->load->view('front_end/headerNew', array("role_id" => $role_id,'current_action' => 'home', 'id' => 'index_page', 'titlestatus' => $titlestatus));
//        $this->load->view('front_end/homeNew1', array("data" => $data, 'blog_data' => $data_blog));
//        $this->load->view('front_end/footerNew');  
     }

    public function aboutus() {
        $titlestatus = "about";
        $this->load->view('front_end/headerNew', array('page' => 'aboutus', 'current_action' => 'aboutus', 'id' => 'about-page', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/aboutusNew');
        $this->load->view('front_end/footerNew');
    }

    public function testimonials() {
        $titlestatus = "testimonial";
        $this->load->view('front_end/headerNew', array('titlestatus' => $titlestatus));
        $this->load->view('front_end/testimonial');
        $this->load->view('front_end/footerNew');
    }

    public function career() {
        $titlestatus = "career";
        $this->load->view('front_end/headerNew', array('id' => 'career-page', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/career');
        $this->load->view('front_end/footerNew');
    }

    public function affiliare_program() {
        $titlestatus = "affiliare_program";
        $this->load->view('front_end/headerNew', array('id' => 'affiliate-marketing-page', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/affiliateNew');
        $this->load->view('front_end/footerNew');
    }

    public function news() {
        $titlestatus = "news";
        $this->load->view('front_end/headerNew', array('id' => 'news-page', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/news');
        $this->load->view('front_end/footerNew');
    }
     public function features() {
        $titlestatus = "features";
        $this->load->view('front_end/headerNew', array('id' => 'features', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/features');
        $this->load->view('front_end/footerNew');
    }

    public function blog_detail($id = null) {
        $titlestatus = "blog";
        $sidebaar = $this->Welcome_model->get_blog_category();
        
        if ($id == null) {
            redirect(base_url() . "view_blog");
        }
        $data = $this->Welcome_model->get_blog_by_id($id);
        $recomended = $this->Welcome_model->get_blog_by_cat("recomended");
        $recomdata = $this->Welcome_model->get_recommended_blogData($data[0]['recommand_blog']);
//       echo "<pre>"; print_R($data);
        $data[0]['tags'] = explode(',', $data[0]['tags']);
        if (empty($data)) {
            redirect(base_url() . "view_blog");
        }
        $this->load->view('front_end/blogheader', array('id' => 'single-blog-page', 'titlestatus' => $titlestatus,'data'=>$data));
        $this->load->view('front_end/blogDetail', array("data" => $data, "sidebar" => $sidebaar,"recomended"=>$recomended,'recomdata'=>$recomdata));
        $this->load->view('front_end/footerNew');
    }
    

    public function termsandconditions() {
        $titlestatus = "termsandconditions";
        $this->load->view('front_end/headerNew', array('current_action' => 'termsandconditions', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/termsandconditions');
        $this->load->view('front_end/footerNew');
    }

    public function privacypolicy() {
        $titlestatus = "privacy";
        $this->load->view('front_end/headerNew', array('current_action' => 'privacypolicy', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/privacypolicy');
        $this->load->view('front_end/footerNew');
    }

    public function faq() {
        $titlestatus = "faq";
        $this->load->view('front_end/headerNew', array('current_action' => 'faq', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/faq');
        $this->load->view('front_end/footerNew');
    }

    public function blog() {
        $titlestatus = "blog";
        $tags = $this->Request_model->get_all_tags();
        $all_tags = array();

        foreach ($tags as $tag) {
            foreach ($tag as $t) {
                $all_tags[] = $t;
            }
        }
        $category = $this->Request_model->get_all_category();
        $all_cate = array();
        foreach ($category as $cate) {
            foreach ($cate as $c) {
                $all_cate[] = $c;
            }
        }

        $all_tags_data = implode(",", $all_tags);
        $tagsdata = explode(',', $all_tags_data);
        $finalTags = array_unique($tagsdata);
        $data = $this->Welcome_model->get_blog_category();

        $this->load->view('front_end/headerNew', array('current_action' => 'blog', 'data' => $data, 'id' => 'single-blog-page', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/blogNew', array('finalTags' => $finalTags, 'all_cate' => $all_cate));
        $this->load->view('front_end/footerNew');
    }

    public function view_blog($id = null) {
        if ($id == null) {
            redirect(base_url() . "view_blog");
        }

        $data = $this->Welcome_model->get_blog_by_id($id);
        if (empty($data)) {
            redirect(base_url() . "view_blog");
        }
        $this->load->view('front_end/header', array('current_action' => 'blog'));
        $this->load->view('front_end/view_blog', array("data" => $data));
        $this->load->view('front_end/footer');
    }

    public function categories() {
        $titlestatus = "";
        $this->load->view('front_end/headerNew', array('current_action' => 'portfolio', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/categories');
        $this->load->view('front_end/footerNew');
    }

    public function login() {
        $titlestatus = "";
        if (!empty($_POST)) {
//            echo "<pre/>";print_R($_POST);exit;
            $email = isset($_POST['email'])?$_POST['email']:'';
            if(isset($_POST['remember_me']) && $_POST['remember_me'] == 'on'){
                $res = $this->Request_model->getuserIDbyemail($email);
                //echo "<pre/>";print_R($res);exit;
                $encr_id = $this->Request_model->my_encrypt($res[0]['id'],BASE64KEY);
                setcookie("is_remember", 1,time() + (10 * 365 * 24 * 60 * 60));
                setcookie("rememberID", $encr_id,time() + (10 * 365 * 24 * 60 * 60));
            }
           $result = $this->Welcome_model->login();
        }
        $this->load->view('front_end/headerNew', array('current_action' => 'login', 'id' => 'signin-page', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/login');
        $this->load->view('front_end/footerNew', array('current_action' => 'login'));
    }

      public function forgot_password() {
        if (!empty($_POST)) {
            $currnt = current_url();
            $url_without_last_part = substr($currnt, 0, strrpos($currnt, "/"));
            $baseseturl = substr($url_without_last_part, 0, strrpos($url_without_last_part, "/"));
            $loginurl = $baseseturl.'/login';
            $random = uniqid();
            $CI = & get_instance();
            $temp_pass = md5(uniqid());
            $subuser_pass = md5($random);
            $email = isset($_POST['email'])?$_POST['email']:"";
            $fromdomain = isset($_POST['fromdomain'])?$_POST['fromdomain']:"";
            $fname = $CI->Request_model->getuserbyemail($email);
          //  echo "<pre>";print_r($fname);
            if($fname[0]['parent_id'] == 0){
                $main_id = $fname[0]['id'];
            }else{
                $main_id = $fname[0]['parent_id'];
            }
            $userplan = $this->Admin_model->getuser_data($main_id,'plan_name');
            $plan_data = $this->Request_model->getsubscriptionlistbyplanid($userplan[0]['plan_name']);
         //   echo $userplan[0]['plan_name']."<pre/>";print_r($plan_data);exit;
            if($fromdomain == 1){
                $url = base_url();
                $getuser = $CI->Request_model->getuserinfobydomain(HOST_NAME);
//                echo "<pre>";print_r($getuser);
//                echo "<pre>";print_r($plan_data);exit;
                if(empty($getuser) || ($getuser[0]["user_id"] != $fname[0]["parent_id"] && $fname[0]["parent_id"] != 0) || ($getuser[0]["user_id"] != $fname[0]["id"] && $fname[0]["parent_id"] == 0)){
                    $this->session->set_flashdata('message_error', $email." is not belong to this domain", 5);
                    redirect($url);
                }
            }else{
                $url = base_url() . "login";
            }
            $success = $this->Welcome_model->update_data("users", array('password_reset' => $temp_pass), array('email' => $email));
            if ($success) {
           //     echo "just testing";
            //    echo $plan_data[0]['is_agency']." -agencyor not";
                if($plan_data[0]['is_agency'] == 1){
                    //$poweredby = ($fname[0]['parent_id'] == 0 || $fname[0]['user_flag'] == 'manager') ? 'Powered by graphicszoo<br/>If you have any query please contact us at:'.SUPPORT_EMAIL : '';
                    //if($fname[0]['parent_id'] == 0){
                        $arraysub = array('CUSTOMER_NAME' => $fname[0]['first_name'],
                        'PASSWORD' => $random,
                        'LOGIN_LINK' => $url,
                     //   'MAIN_USER_DATA' => $poweredby
                        );
                       // echo "<pre>";print_R($arraysub);exit;
                        $success = $this->Welcome_model->update_data("users", array('new_password' => $subuser_pass), array('email' => $email));
                        $this->myfunctions->send_email_to_users_by_template($main_id,'agency_forget_password_email',$arraysub,$email);
                    //}
                    }else{
                     //   echo $success." plan<br>".$fname[0]['first_name']."<br>";
                    $array = array('CUSTOMER_NAME' => $fname[0]['first_name'],
                    'CHANGE_PASSWORD_LINK' => base_url().'welcome/changepassword/'.$temp_pass);
                   // echo "<pre>";print_R($array);exit;
                    $this->myfunctions->send_email_to_users_by_template($fname[0]['id'],'forget_password_email',$array,$email);
                }
               // exit;
                $this->session->set_flashdata('message_success', "We have just sent you an email to reset your password.", 5);
                redirect($url);
                // }
            } else {
                $this->session->set_flashdata('message_error', "Error", 5);
                redirect($url);
            }
        }
    }

   public function portfolio() {
        //$titlestatus = "portfolio";
        $datacount = $this->Welcome_model->portfolio_load_more('',true);
        $data = $this->Welcome_model->portfolio_load_more('','');
//        echo "tedt".$datacount;exit;
        //$data = $this->Admin_model->get_all_portfolio();
//        echo"<pre>";print_R($data);echo"</pre>";
        $this->load->view('front_end/headerNew', array('current_action' => 'portfolio', 'data' => $data,'datacount' => $datacount, 'id' => 'portfolio-page', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/portfolioNew', array("data" => $data));
        $this->load->view('front_end/footerNew');
    }
    
        public function load_more_portfolio(){
        $count_load_more = '';
        $group_no = $this->input->post('group_no');
        $id = $this->input->post('id');
        //$status = explode(",", $dataStatusText);
        $content_per_page = LIMIT_ADMIN_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $count_load_more = $this->Welcome_model->portfolio_load_more($id,true);
        $alldata = $this->Welcome_model->portfolio_load_more($id,'',$start,$content_per_page);
        //echo "<pre/>";print_r($alldata);
        foreach($alldata as $key => $val){ ?>
            <li class="item thumb">
                <div class="content box-overflow">
                    <a class="thumbnail" href="#" data-image-id="<?php echo $val['id']; ?>" data-toggle="modal" data-title="" data-image="<?php echo FS_PATH_PUBLIC_UPLOADS_PORTFOLIOS.$val['image']; ?>" data-target="#image-gallery"></a>
                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PORTFOLIOS.$val['image']; ?>" class="img-responsive">
                    <div class="hover-txt">
                        <span><img src="<?php echo FS_PATH_PUBLIC_ASSETS; ?>front_end/Updated_Design/img/search-icon.png"></span>
                        <p><?php echo $val['title'];?></p>
                    </div>
                </div>
            </li>
    <?php } ?> <span id="loadingAjaxCount" data-value="<?php echo $count_load_more; ?>"></span>
        <script>
        let modalId = $('#image-gallery');
        $(document).ready(function () {
            loadGallery(true, 'a.thumbnail');
        });
    </script>
   <?php }
    
    public function portfoliotwo() {
       
           $titlestatus = "portfolio";
           $data = $this->Admin_model->get_all_portfolio();
         //  echo"<pre>";print_R($data);echo"</pre>";
           $this->load->view('front_end/headerNew', array('current_action' => 'portfolio', 'data' => $data, 'id' => 'portfolio-page', 'titlestatus' => $titlestatus));
           $this->load->view('front_end/portfolioNew_two', array("data" => $data));
           $this->load->view('front_end/footerNew');
       }
    public function pricing() {
        $titlestatus = "pricing";
        $plans_prices = array();
        $prcs = array();
        $montly_tier_price = $this->load->get_var('subscription_data');
            $plans_prices['business_price'] = $montly_tier_price[SUBSCRIPTION_MONTHLY];
            $plans_prices['company_price'] = $montly_tier_price[COMPANY_PLAN];
            $plans_prices['agency_price'] = $montly_tier_price[AGENCY_PLAN];
        
        $plan_prcs = $this->load->get_var('annual_subscription_data');
            $prcs['business_price'] = $plan_prcs[ANNUAL_BUSINESS_PLAN];
            $prcs['company_price'] = $plan_prcs[ANNUAL_COMPANY_PLAN];
            $prcs['agency_price'] = $plan_prcs[ANNUAL_AGENCY_PLAN];
        $this->load->view('front_end/headerNew', array('current_action' => 'pricing', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/pricing_new',array('plan_price' => $plans_prices,'agency_price' => $prcs));
        $this->load->view('front_end/footerNew');
    }
    
    public function pricing_new() {
        $this->load->view('front_end/headerNew', array());
        $this->load->view('front_end/pricing_new');
        $this->load->view('front_end/footerNew');
    }

    public function signup() {
        $titlestatus = "";
        $planlist = $this->Stripe->getallsubscriptionlist();
        $rendomly_qa_id = $this->Admin_model->getqa_id();
        if (!empty($_POST)) {
            //if(isset($_POST['']))
            $checkemail = $this->Request_model->getuserbyemail($_POST['email']);
            if (!empty($checkemail)) {
                $this->session->set_flashdata('message_error', "Email Address is already available.!", 5);
                redirect(base_url() . "signup");
            }
            $stripe_customer = $this->Stripe->createnewCustomer($_POST['email'], "");
            $expir_date = explode("/", $_POST['expir_date']);
            if (!$stripe_customer['status']) {
                $this->session->set_flashdata('message_error', "Customer Not Created Successfully.!", 5);
                redirect(base_url() . "signup");
            }
            $customer_id = $stripe_customer['data']['customer_id'];
            $card_number = trim($_POST['card_number']);
            $expiry_month = $expir_date[0];
            $expiry_year = $expir_date[1];
            //$expiry_month = $_POST['expiry-month'];
            //$expiry_year = $_POST['expiry-year'];
            $cvc = $_POST['cvc'];

            $stripe_card = $this->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);
            if (!$stripe_card['status']):
                $this->session->set_flashdata('message_error', $stripe_card['message'], 5);
                redirect(base_url() . "signup");
            endif;

            $subscription_plan_id = $_POST['plan_name'];

            $stripe_subscription = '';
            if ($subscription_plan_id) {
                $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($stripe_customer['data']['customer_id'], $subscription_plan_id);
            }

            if (!$stripe_subscription['status']) {
                $this->session->set_flashdata('message_error', $stripe_subscription['message'], 5);
                redirect(base_url() . "signup");
            }

            $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($set), 0, 20);
            $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
            $customer['customer_id'] = $stripe_customer['data']['customer_id'];
            $customer['plan_name'] = $subscription_plan_id;
            $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);
            $customer['is_active'] = 1;
            //$customer['activation'] = $code;
            $customer['activation'] = '';
            $customer['plan_amount'] = $plandetails['amount'] / 100;
            $customer['plan_interval'] = $plandetails['interval'];
            $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
            $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
            $customer['display_plan_name'] = $plandetails['name'];
            $customer['email'] = $_POST['email'];
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['new_password'] = md5($_POST['password']);
            $customer['role'] = "customer";
             $customer['created'] = date("Y:m:d H:i:s");
            $customer['qa_id'] = $rendomly_qa_id[0]['id'];
            $id = $this->Welcome_model->insert_data("users", $customer);
            if ($id) {
                //$this->send_email_after_signup($code,$id,$_POST['email']);

                /* get timezone */
                $ip = $_SERVER['REMOTE_ADDR'];
                $curl = curl_init();
                //122.160.138.23
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://ip-api.com/json/" . $ip,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache",
                        "postman-token: 92b0963f-d661-849d-9ca9-734180411906"
                    ),
                ));

                $ipInfo = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    //echo "cURL Error #:" . $err;
                }
                $ipInfo = json_decode($ipInfo);
                $timezone = $ipInfo->timezone;
                /* timezone ends */

                /* set user session data for login */
                $this->session->set_userdata('user_id', $id);
                $this->session->set_userdata('timezone', $timezone);
                $this->session->set_userdata('role', $customer['role']);
                $this->session->set_userdata('first_name', $customer['first_name']);
                $this->session->set_userdata('last_name', $customer['last_name']);
                $this->session->set_userdata('email', $customer['email']);
                $this->session->set_userdata('tour', 0);
                $this->session->set_userdata('qa_id', $customer['qa_id']);
                $this->welcome_email($customer['email'],$customer['first_name']);
                $this->session->set_flashdata('message_success', "User created successfully and subscribed successfully..!", 5);
                //redirect(base_url() . "customer/profile/dashboard");
                redirect(base_url() . "thank-you-signup");
                //redirect(base_url() . "signup");
                /* set user session data for login ends */
            } else {
                $this->session->set_flashdata('message_error', "User can not created successfully ..!", 5);
                redirect(base_url() . "signup");
            }
        }

        $subscription_plan_list = array();
        $j = 0;
        for ($i = 0; $i < sizeof($planlist); $i++) {
            if ($planlist[$i]['id'] == '30291' || $planlist[$i]['id'] == 'A21481D' || $planlist[$i]['id'] == 'A1900S' || $planlist[$i]['id'] == 'M99S' || $planlist[$i]['id'] == 'M199G' || $planlist[$i]['id'] == 'M1991D' || $planlist[$i]['id'] == 'M199S' || $planlist[$i]['id'] == 'A10683D' || $planlist[$i]['id'] == 'M993D') {
                continue;
            }
            $subscription_plan_list[$j]['id'] = $planlist[$i]['id'];
            $subscription_plan_list[$j]['name'] = $planlist[$i]['name'] . " - $" . $planlist[$i]['amount'] / 100 . "/" . $planlist[$i]['interval'];
            $subscription_plan_list[$j]['amount'] = $planlist[$i]['amount'] / 100;
            $subscription_plan_list[$j]['interval'] = $planlist[$i]['interval'];
            $subscription_plan_list[$j]['trial_period_days'] = $planlist[$i]['trial_period_days'];
            $j++;
        }

        $this->load->view('front_end/headerNew', array('current_action' => 'signup'));
        $this->load->view('front_end/signup', array("planlist" => $subscription_plan_list, 'titlestatus' => $titlestatus));
        $this->load->view('front_end/footerNew');
    }
    
     public function signupnew() {
        $titlestatus = "";
        $subs_list = $this->Welcome_model->getsubscriptionlist();
        $rendomly_qa_id = $this->Admin_model->getqa_id();
        $is_coupon_valid = $_POST['couponinserted_ornot'];
        $is_reffered = (isset($_COOKIE['affiliated_key']) && $_COOKIE['affiliated_key'] != '') ? $_COOKIE['affiliated_key'] : '';
        $keyexist = $this->Affiliated_model->IsAffiliated_keyexists($is_reffered);
        $reffered_by = (isset($keyexist[0]['user_id']) && $keyexist[0]['user_id'] != 0) ? $keyexist[0]['user_id'] : 0;
        $is_gz_user = $this->Admin_model->getuser_data($reffered_by,'id,email,plan_amount');
        $fullurl = base_url().'signup?referral='.$is_reffered;
        /***start track code *****/
        $myIP = $_SERVER['REMOTE_ADDR'];
        $checkalreadyvisit = $this->Affiliated_model->Checkvisitexist($reffered_by,$myIP);
        if($checkalreadyvisit == '' && $reffered_by != 0){
            $track['user_id'] = $reffered_by;
            $track['ip_address'] = $myIP;
            $track['created'] = date("Y:m:d H:i:s");
            $track_success = $this->Welcome_model->insert_data("affiliation_visits", $track);
        }
        /*** end track code*****/
        if (!empty($_POST)) {
            if(isset($_POST['first_name']) && $_POST['first_name']==""){
                $this->session->set_flashdata('message_error', "Please enter First Name!", 5);
                redirect(base_url() . "signup");
            }
            if(isset($_POST['last_name']) && $_POST['last_name']==""){
                $this->session->set_flashdata('message_error', "Please enter Last Name!", 5);
                redirect(base_url() . "signup");
            }
            if(isset($_POST['email']) && $_POST['email']==""){
                $this->session->set_flashdata('message_error', "Please enter Email !", 5);
                redirect(base_url() . "signup");
            }
            if(isset($_POST['password']) && $_POST['password']==""){
                $this->session->set_flashdata('message_error', "Please enter Password !", 5);
                redirect(base_url() . "signup");
            }
            if(isset($_POST['card_number']) && $_POST['card_number']==""){
                $this->session->set_flashdata('message_error', "Please enter Card Number !", 5);
                redirect(base_url() . "signup");
            }
            if(isset($_POST['expir_date']) && $_POST['expir_date']==""){
                $this->session->set_flashdata('message_error', "Please enter Expire Date !", 5);
                redirect(base_url() . "signup");
            }
            if(isset($_POST['cvc']) && $_POST['cvc']==""){
                $this->session->set_flashdata('message_error', "Please enter CVC Number !", 5);
                redirect(base_url() . "signup");
            }
            if(isset($_POST['term']) && $_POST['term']==""){
                $this->session->set_flashdata('message_error', "Please accept the term and condition !", 5);
                redirect(base_url() . "signup");
            }
            if(isset($_POST['state']) && $_POST['state']==""){
                $this->session->set_flashdata('message_error', "Please select state !", 5);
                redirect(base_url() . "signup");
            }
            if(isset($_POST['state_tax']) && $_POST['state_tax'] != 0){
                $state_tax = $_POST['state_tax'];
            }else{
                $state_tax = '';
            }
            $checkemail = $this->Request_model->getuserbyemail($_POST['email']);
            if (!empty($checkemail)) {
                $this->session->set_flashdata('message_error', "There is already an account associated with this email.", 5);
                redirect(base_url() . "signup");
            }
            if($is_coupon_valid == 0){
                $stripe_customer = $this->Stripe->createnewCustomer($_POST['email']);
            }else{
                $stripe_customer = $this->Stripe->createnewCustomer($_POST['email'], $_POST['Discount']);
            }
            $expir_date = explode("/", $_POST['expir_date']);
            if (!$stripe_customer['status']) {
                $this->session->set_flashdata('message_error', $stripe_customer['message'], 5);
                redirect(base_url() . "signup");
            }
            $customer_id = $stripe_customer['data']['customer_id'];
            $card_number = trim($_POST['card_number']);
            $expiry_month = $expir_date[0];
            $expiry_year = $expir_date[1];
            $cvc = $_POST['cvc'];

            $stripe_card = $this->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);
            if (!$stripe_card['status']):
                $this->session->set_flashdata('message_error', $stripe_card['message'], 5);
                redirect(base_url() . "signup");
            endif;

            $subscription_plan_id = $_POST['plan_name'];
            $inprogress_request = $_POST['in_progress_request'];
            $final_plan_price = $_POST['final_plan_price'];
            $actual_plan_price = $_POST['selected_price'];
            if($subscription_plan_id == SUBSCRIPTION_99_PLAN){
                $current_date = date("Y:m:d H:i:s");
                $monthly_date = strtotime("+1 months", strtotime($current_date)); // returns timestamp
                $billing_expire = date('Y:m:d H:i:s',$monthly_date);
                $customer['billing_cycle_request'] = 3;
           }else {
               $customer['billing_cycle_request'] = 0;
           }

            $stripe_subscription = '';
            if ($subscription_plan_id) {
                $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($stripe_customer['data']['customer_id'], $subscription_plan_id, $inprogress_request,$state_tax);
                $current_date = date('Y-m-d H:i:s',$stripe_subscription['data']['period_start']);
             $billing_expire = date('Y-m-d H:i:s',$stripe_subscription['data']['period_end']);
            }

            if (!$stripe_subscription['status']) {
                $this->session->set_flashdata('message_error', $stripe_subscription['message'], 5);
                redirect(base_url() . "signup");
            }
            
            /* get timezone */
                $ip = $_SERVER['REMOTE_ADDR'];
                $curl = curl_init();
                //122.160.138.23
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://ip-api.com/json/" . $ip,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache",
                        "postman-token: 92b0963f-d661-849d-9ca9-734180411906"
                    ),
                ));

                $ipInfo = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    //echo "cURL Error #:" . $err;
                }
                $ipInfo = json_decode($ipInfo);
                if($_POST['state'] != ""){
                    $state = $_POST['state'];
                }else{
                    $state = $ipInfo->regionName;
                }
                $timezone = $ipInfo->timezone;
                /* timezone ends */


            $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($set), 0, 20);
            $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
            $customer['billing_start_date'] = $current_date;
            $customer['billing_end_date'] = $billing_expire;
            $customer['customer_id'] = $stripe_customer['data']['customer_id'];
            $customer['plan_name'] = $subscription_plan_id;
            $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);
            $customer['is_active'] = 1;
            $customer['activation'] = '';
            $customer['plan_amount'] = $actual_plan_price;
            $customer['plan_interval'] = $plandetails['interval'];
            $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
            $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
            $customer['display_plan_name'] = $plandetails['name'];
            $customer['total_inprogress_req'] = $inprogress_request;
            $customer['total_active_req'] = $inprogress_request * TOTAL_ACTIVE_REQUEST;
            $customer['email'] = $_POST['email'];
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['new_password'] = md5($_POST['password']);
            $customer['role'] = "customer";
            $customer['created'] = date("Y:m:d H:i:s");
            $customer['modified'] = date("Y:m:d H:i:s");
            $customer['timezone'] = $timezone;
            $customer['state'] = $state;
            $customer['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $customer['last_update'] = date("Y:m:d H:i:s");
            $customer['is_logged_in'] = 0;
            $customer['is_affiliated'] = 0;
            $customer['qa_id'] = $rendomly_qa_id[0]['id'];
            $customer['YOUR_SOURCE_FIELD'] = isset($_POST['YOUR_SOURCE_FIELD'])?$_POST['YOUR_SOURCE_FIELD']:'';
            $customer['YOUR_MEDIUM_FIELD'] = isset($_POST['YOUR_MEDIUM_FIELD'])?$_POST['YOUR_MEDIUM_FIELD']:'';
            $customer['YOUR_CAMPAIGN_FIELD'] = isset($_POST['YOUR_CAMPAIGN_FIELD'])?$_POST['YOUR_CAMPAIGN_FIELD']:'';
            $customer['YOUR_CONTENT_FIELD'] = isset($_POST['YOUR_CONTENT_FIELD'])?$_POST['YOUR_CONTENT_FIELD']:'';
            $customer['YOUR_TERM_FIELD'] = isset($_POST['YOUR_TERM_FIELD'])?$_POST['YOUR_TERM_FIELD']:'';
            $customer['VISITS'] =  isset($_POST['VISITS'])?$_POST['VISITS']:'';
            if (empty($checkemail)) {
            $id = $this->Welcome_model->insert_data("users", $customer);
            if ($id) {
                
                /* affiliate user transaction entry */
                if($is_reffered){
                    $aff_data = array();
                    $paid_amount = $this->Admin_model->getuser_data($id,'id,plan_amount');
                    $comm_type = COMMISION_TYPE;
                    $comm_fees = COMMISION_FEES;
                    if($comm_type == 'percentage'){
                      $amnt = (($comm_fees / 100) * $paid_amount[0]['plan_amount']);
                    }
                    $aff_data['user_id'] = $reffered_by;
                    $aff_data['type'] = 'credit';
                    $aff_data['signup_user_id'] = $id;
                    $aff_data['amount'] = $amnt;
                    $aff_data['created'] = date("Y:m:d H:i:s");
                    $aff_inst_id = $this->Welcome_model->insert_data("affiliate_transaction_history", $aff_data);
                    $aff_user_data = $this->Admin_model->getuser_data($reffered_by,'id,email,first_name');
                    $signup_user_data = $this->Admin_model->getuser_data($id,'id,email,first_name');
                    $array = array('CUSTOMER_NAME' => $aff_user_data[0]['first_name'],
                    'SIGNUP_USER_NAME' => $signup_user_data[0]['first_name']);
                    if($aff_inst_id){
                         $this->myfunctions->send_email_to_users_by_template($reffered_by,'affiliated_user_email',$array,$aff_user_data[0]['email']);
                    }
                }
                /* end affiliate user entry */
                
                //$this->send_email_after_signup($code,$id,$_POST['email']);
                /* add dummy request */
                $this->myfunctions->add_dummy_request($id);
                /* end add dummy request */

                /* set user session data for login */
                $this->session->set_userdata('user_id', $id);
                $this->session->set_userdata('timezone', $timezone);
                $this->session->set_userdata('role', $customer['role']);
                $this->session->set_userdata('first_name', $customer['first_name']);
                $this->session->set_userdata('last_name', $customer['last_name']);
                $this->session->set_userdata('email', $customer['email']);
                $this->session->set_userdata('tour', 0);
                $this->session->set_userdata('qa_id', $customer['qa_id']);
                $array = array('CUSTOMER_NAME' => $customer['first_name'],
                    'PROJECT_LINK' => base_url().'customer/request/add_new_request');
                $this->session->set_flashdata('message_success', "User created successfully and subscribed successfully..!", 5);
                redirect(base_url() . "thank-you-signup");
                /* set user session data for login ends */
            } else {
                $this->session->set_flashdata('message_error', "Error occurred while creating new account, please verify the info and try again!", 5);
                redirect(base_url() . "signup");
            }
            }else{
                $this->session->set_flashdata('message_error', "There is already an account associated with this email.", 5);
                redirect(base_url() . "signup");
            }
        }
        $this->load->view('front_end/signup_new', array('titlestatus' => $titlestatus, 'subscription_list' => $subs_list));
       // $this->load->view('front_end/footerNew');
    }
    
    public function CheckCouponValidation() {
        $apply_couponor_not = isset($_POST['apply_couponor_not'])?$_POST['apply_couponor_not']:'';
        $userid = isset($_POST['userid'])?$_POST['userid']:'';
        $userdata = $this->Admin_model->getuser_data($userid,'id,parent_id,user_flag');
        if($userdata[0]['parent_id'] != 0 && $userdata[0]['user_flag'] == 'client'){
            $useroptions_data = $this->Request_model->getAgencyuserinfo($userdata[0]['parent_id']);
            if($useroptions_data[0]['stripe_api_key'] != ""){
                $this->Stripe->setapikey($useroptions_data[0]['stripe_api_key']);
            }
        }
        if($apply_couponor_not == 0){
            $output['status'] = 0;
            $output['message'] = "This Coupon is not applicable for selected plan.";
            $output['inertcoupon'] = 0;
            $output['return_data'] = '';
        }else{
            if(!empty($_POST['dicountCode'])){
                $output = array();
                $stripe_customer = $this->Stripe->checkcouponvalid_ornot($_POST['dicountCode']);
                $output['status'] = $stripe_customer['status'];
                $output['message'] = $stripe_customer['message'];
                $output['inertcoupon'] = 1;
                $output['return_data'] = $stripe_customer['data'];
                
            }
        }
        echo json_encode($output);
                exit;
        
    }
    
    public function free_design_signup() {
        //redirect(base_url());
        $titlestatus = "";
        $rendomly_qa_id = $this->Admin_model->getqa_id();
        if (!empty($_POST)) {
            $checkemail = $this->Request_model->getuserbyemail($_POST['email']);
            if (!empty($checkemail)) {
                $this->session->set_flashdata('message_error', "There is already an account associated with this email.", 5);
                redirect(base_url() . "free-design-signup");
            }
            /* get timezone */
                $ip = $_SERVER['REMOTE_ADDR'];
                $curl = curl_init();
                //122.160.138.23
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://ip-api.com/json/" . $ip,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache",
                        "postman-token: 92b0963f-d661-849d-9ca9-734180411906"
                    ),
                ));

                $ipInfo = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                if ($err) {
                    //echo "cURL Error #:" . $err;
                }
                $ipInfo = json_decode($ipInfo);
                $state = $ipInfo->regionName;
                $timezone = $ipInfo->timezone;
                /* timezone ends */
                
            $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($set), 0, 20);
            $customer['is_active'] = 1;
            $customer['is_trail'] = 1;
            $customer['account'] = 'trial';
            $customer['activation'] = $code;   
            $customer['email'] = $_POST['email'];
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['phone'] = $_POST['phone'];
            $customer['is_logged_in'] = 0;
            $customer['new_password'] = md5($_POST['password']);
            $customer['role'] = "customer";
            $customer['created'] = date("Y:m:d H:i:s");
            $customer['modified'] = date("Y:m:d H:i:s");
            $customer['last_update'] = date("Y:m:d H:i:s");
            $customer['timezone'] = $timezone;
            $customer['state'] = $state;
            $customer['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $customer['qa_id'] = $rendomly_qa_id[0]['id'];
            $customer['tour'] = '1';
            $customer['total_inprogress_req'] = TOTAL_INPROGRESS_REQUEST;
            $customer['total_active_req'] = TOTAL_ACTIVE_REQUEST;
            $customer['design_count'] = isset($_POST['design_count'])?$_POST['design_count']:'';
            $customer['business_detail'] = isset($_POST['business_detail'])?$_POST['business_detail']:'';
            $customer['company_role'] = isset($_POST['company_role'])?$_POST['company_role']:'';
            $customer['plan_amnt'] = isset($_POST['plan_amount'])?$_POST['plan_amount']:'';
            $customer['YOUR_SOURCE_FIELD'] = isset($_POST['YOUR_SOURCE_FIELD'])?$_POST['YOUR_SOURCE_FIELD']:'';
            $customer['YOUR_MEDIUM_FIELD'] = isset($_POST['YOUR_MEDIUM_FIELD'])?$_POST['YOUR_MEDIUM_FIELD']:'';
            $customer['YOUR_CAMPAIGN_FIELD'] = isset($_POST['YOUR_CAMPAIGN_FIELD'])?$_POST['YOUR_CAMPAIGN_FIELD']:'';
            $customer['YOUR_CONTENT_FIELD'] = isset($_POST['YOUR_CONTENT_FIELD'])?$_POST['YOUR_CONTENT_FIELD']:'';
            $customer['YOUR_TERM_FIELD'] = isset($_POST['YOUR_TERM_FIELD'])?$_POST['YOUR_TERM_FIELD']:'';
            $customer['VISITS'] =  isset($_POST['VISITS'])?$_POST['VISITS']:'';
            $id = $this->Welcome_model->insert_data("users", $customer);
            //echo $id;
            if ($id) { 
                /****drip start********/
                $this->load->library('drip');
                $this->load->library('datasett');
                $this->load->library('response');
				// create new subscriber
                $Drip = new Drip('5791e6d8913a121374b5e55904342eb3','1975472');
                $data = new Datasett('subscribers', [
                    'email' => $_POST['email'],
                ]);
                $Response = $Drip->post('subscribers', $data);
				// assign free design user tag
				$Drip = new Drip('5791e6d8913a121374b5e55904342eb3','1975472');
				$data = new Datasett('tags', [
					'email' => $_POST['email'],
					'tag' => "Free Design Sign-up",
				]);
				$Drip->post('tags', $data);
                //echo "<pre>";print_r($Drip);exit;
                //$this->send_email_after_signup($code,$id,$_POST['email']);
                                
                /* add dummy request */
                $this->myfunctions->add_dummy_request($id);
                /* end add dummy request */               
                                
                /* set user session data for login */
                $this->session->set_userdata('user_id', $id);
                $this->session->set_userdata('timezone', $timezone);
                $this->session->set_userdata('role', $customer['role']);
                $this->session->set_userdata('first_name', $customer['first_name']);
                $this->session->set_userdata('last_name', $customer['last_name']);
                $this->session->set_userdata('email', $customer['email']);
                $this->session->set_userdata('tour', 0);
                $this->session->set_userdata('qa_id', $customer['qa_id']);
                $array = array('CUSTOMER_NAME' => $customer['first_name'],
                    'PROJECT_LINK' => base_url().'customer/request/add_new_request');
                $this->myfunctions->send_email_to_users_by_template($id,'welcome_email_to_trail_user',$array,$customer['email']);
                $this->session->set_flashdata('message_success', "User created successfully and subscribed successfully..!", 5);
                redirect(base_url() . "thank-you-free-design");
				//redirect(base_url(). "customer/profile/thank_you?id=".$id);
                //redirect(base_url() . "customer/profile/dashboard");
                //redirect(base_url() . "signup");
                /* set user session data for login ends */
            } else {
                $this->session->set_flashdata('message_error', "User can not created successfully ..!", 5);
                redirect(base_url() . "free-design-signup");
            }
        }
        
      // echo "<pre>";print_r($subscription_plan_list); exit();
        //$this->load->view('front_end/headerNew', array('current_action' => 'trial_account'));
        $this->load->view('front_end/free-design-signup', array("planlist" => $subscription_plan_list, 'titlestatus' => $titlestatus));
        //$this->load->view('front_end/footerNew');
    }

    public function contactus() {
        $titlestatus = "contactus";
        $this->load->view('front_end/header', array('current_action' => 'contactus', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/contactus');
        $this->load->view('front_end/footer');
    }

    public function send_email_after_signup($activation_code, $id, $email) {
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
//        $data = array(
//            'activation' => $activation_code,
//            'user_id' => $id,
//        );
        $body = $this->load->view('emailtemplate/signup_template', '', TRUE);
        $receiver = $email;

        $subject = 'GraphicsZoo Account confirmation.';

        $message = $body;

        $title = 'GraphicsZoo';

        SM_sendMail($receiver, $subject, $body, $title);

        // $config = $this->config->item('email_smtp');
        // $from = $this->config->item('from');
        //       $this->load->library('email', $config);
        //    $this->email->set_newline("\r\n");
        //    $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
        // $this->email->set_header('Content-type', 'text/html');
        //       $this->email->from($from, 'GraphicsZoo');
        //       $data = array(
        //            'activation'=> $activation_code,
        //            'user_id' => $id,
        //                );
        //       $this->email->to($email);  // replace it with receiver mail id
        //    $this->email->subject("GraphicsZoo Account confirmation."); // replace it with relevant subject 
        //       $body = $this->load->view('emailtemplate/signup_template',$data,TRUE);
        //    $this->email->message($body);   
        //       @$this->email->send();	       
    }

    public function activation() {
        $id = $this->uri->segment(3);
        $code = $this->uri->segment(4);
        $user = $this->Request_model->getuserbyid($id);
        if ($user[0]['id'] == $id || $user[0]['activation'] == $code) { 
            $query = $this->Admin_model->update_data("users", array('is_emailverified' => 1, 'activation' => null), array('id' => $id));
            //$this->session->set_flashdata('message_success', 'Your account has activated successfully!',5);
            //redirect(base_url() . "customer/request/design_request");
            if($_SESSION['user_id']){
                 $this->session->set_flashdata('message_success', 'Your account has been activated successfully!');
            redirect(base_url() . "customer/request/add_new_request");
           }else{
               $this->session->set_flashdata('message_success', 'Your account has been activated successfully!');
              redirect(base_url() . "login/"); 
           }
        } 
    }

    public function email() {
        $this->load->view('emailtemplate/supportEmail');
    }

    public function send_email_for_new_total_project() {
        $requests = $this->Request_model->get_all_designers_for_today_request();
        for ($i = 0; $i < count($requests); $i++) {
            if (!empty($requests[$i]['allrequest'])) {

                $request_count = count($requests[$i]['allrequest']);

                require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
                require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
                require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
                $data = array(
                    'allrequest' => $requests[$i]['allrequest'],
                    'designer_id' => $requests[$i]['id'],
                    'count' => $request_count
                );
                $body = $this->load->view('emailtemplate/assign_design_by_today', $data, TRUE);
                $receiver = $email;

                $subject = 'GraphicsZoo Account confirmation.';

                $message = $body;

                $title = 'GraphicsZoo';

                SM_sendMail($receiver, $subject, $message, $title);

                // $config = $this->config->item('email_smtp');
                // $from = $this->config->item('from');
                //    $this->load->library('email', $config);
                //    $this->email->set_newline("\r\n");
                //    $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
                //    $this->email->set_header('Content-type', 'text/html');
                //    $this->email->from($from, 'GraphicsZoo');
                //    $data = array(
                //         'allrequest'=> $requests[$i]['allrequest'],
                //         'designer_id' => $requests[$i]['id'],
                //         'count' => $request_count
                //             );
                //    $this->email->to('jatinder.kumar@softradix.com');  // replace it with receiver mail id
                //    $this->email->subject("GraphicsZoo Testing Email."); // replace it with relevant subject 
                //   	$body = $this->load->view('emailtemplate/assign_design_by_today',$data,TRUE);
                //    $this->email->message($body);   
                //    if($this->email->send())
                //    {
                //    	echo "Mail Sent";
                //    }  
                //    else{
                //    	echo "Not Sent";
                //    }
                //print_r($send);
            }
        }
    }

    public function support() {
        $titlestatus = "contactus";
        $this->load->view('front_end/headerNew', array('id' => 'support-page', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/support');
        $this->load->view('front_end/footerNew');
    }
    
    
     public function dripchk() {
        $titlestatus = "drip";
        $this->load->view('front_end/headerNew', array('id' => 'dripchk', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/dripchk');
        $this->load->view('front_end/footerNew');
    }

    public function how_it_work() {
        $titlestatus = "how_it_work";
        $this->load->view('front_end/headerNew', array('id' => 'how-it-work', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/how_it_work');
        $this->load->view('front_end/footerNew');
    }

     public function send_email_support() {
        
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        if(isset($_POST)){
			$fname = isset($_POST['name'])?$_POST['name']:'';
                        $lname = isset($_POST['last-name'])?$_POST['last-name']:'';
                        $name = $fname.' '.$lname;
			$email = isset($_POST['email'])?$_POST['email']:'';
			$sub = isset($_POST['subject'])?$_POST['subject']:'';
			$msg = isset($_POST['message'])?$_POST['message']:'';
            if($name == "") {
             $this->session->set_flashdata("message_error", "Name should not be empty", 5);
             redirect(base_url() . "support");
            }elseif($email == ''){
              $this->session->set_flashdata("message_error", "email should not be empty", 5);
              redirect(base_url() . "support");
            }elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
               $this->session->set_flashdata("message_error", "email must be valid", 5);
               redirect(base_url() . "support");
            }elseif($sub == ''){
               $this->session->set_flashdata("message_error", "subject should not be empty", 5);
               redirect(base_url() . "support");
            }elseif($msg == ''){
              $this->session->set_flashdata("message_error", "message should not be empty", 5);
              redirect(base_url() . "support");
            }else{
				//print_r($_POST);exit;
				if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])){
					$secret = '6LdJZ4sUAAAAAAnCSkVTURJQlcxljclQALu6GdsF';
					$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
					$responseData = json_decode($verifyResponse);
					if($responseData->success){
						$data = array(
							'name' => $_POST['name'],
							'email' => $_POST['email'],
							'subject' => $_POST['subject'],
							'message' => $_POST['message']
						); 
						$body = $this->load->view('emailtemplate/supportEmail', $data, TRUE);
						$receiver = SUPPORT_EMAIL;
//						$receiver = 'hashmonika@gmail.com';
						$subject = 'GraphicsZoo Support Email';
						$message = $body;
						$title = 'GraphicsZoo';
						$array = array('NAME' => $name,
                                                'SUBJECT' => $sub,
                                                'EMAIL' => $email,
                                                'MESSAGE' => $msg
                                                     );
                                                $this->myfunctions->send_email_to_users_by_template('0', 'support_email', $array, $receiver);
						$this->session->set_flashdata("message_success", "We received your request and we'll get back to you soon. Thank you!", 5);
						redirect(base_url() . "support");
					} else {
						$this->session->set_flashdata("message_error", "Robot verification failed, please try again.", 8);
						redirect(base_url() . "support");
					}
				} else {
					$this->session->set_flashdata("message_error", "Robot verification failed, please try again.", 8);
					redirect(base_url() . "support");
				}
			}
		}
    }

    public function changepassword($key) {
        if (!empty($_POST)) {
            $password = md5($_POST['password']);
            if ($_POST['password'] != $_POST['cpassword']) {
                $this->session->set_flashdata("message_error", "New And Confirm Password is not match..!", 5);
                redirect(base_url() . "welcome/changepassword/" . $key);
            } else {

                //$success = $this->Welcome_model->update_data("users",array('password_reset' => null,'new_password'=>md5($password)),array('password_reset'=>$key));
                $success = $this->Welcome_model->update_data("users", array('password_reset' => null, 'new_password' => $password), array('password_reset' => $key));
                redirect(base_url() . "login");
            }
        }
        $this->load->view('front_end/headerNew');
        $this->load->view('front_end/changepassword');
    }

    public function migrat_db_c() {
        $this->Admin_model->migrat_db();
    }

    public function migrat_db_request() {
        $this->Admin_model->migrat_db_requ();
    }

    public function migrat_db_request_dis() {
        $this->Admin_model->migrat_db_requ_diss();
    }

    public function migrat_db_blog() {
        $this->Admin_model->migrat_db_blog_m();
    }

    public function migrat_db_portfolio() {
        $this->Admin_model->migrat_db_port();
    }

    public function migrat_db_request_file() {
        $this->Admin_model->migrat_db_request_file_m();
    }

    public function send_forgot_password_email_to_all_users() {
        $users = $this->Request_model->get_all_users();
        for ($i = 0; $i < count($users); $i++) {
            require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
            require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
            require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
            $data = array(
                'password_reset' => $users[$i]['password_reset']
            );
            $body = $this->load->view('emailtemplate/all_users_forgot_pass_template', $data, TRUE);
            $receiver = $users[$i]['email'];

            $subject = 'GraphicsZoo forgot password email';

            $message = $body;

            $title = 'GraphicsZoo';

            SM_sendMail($receiver, $subject, $message, $title);
        }
    }

    public function welcome_email($email,$customer,$is_trial=null)
	{
        require_once(APPPATH."libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH."libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH."libraries/portal_lan/email/class.smtp.php");
        $body = $this->load->view('emailtemplate/welcome_email_template',array('fname'=>$customer,'is_trial'=>$is_trial),TRUE);
        //echo "<pre>";print_r($body);exit;
        $receiver = $email;
        
        $subject = 'GraphicsZoo - Congratulations you are signed up!';
        
        $message = $body;
        
        $title = 'Welcome to GraphicsZoo';
        
        SM_sendMail($receiver,$subject,$message,$title);
	}
        
    public function cron_mail_to_customer_for_unseen_message() {
          //  echo "fghlkj";exit;
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        //SM_sendMail('hashkapilkalra@gmail.com', 'New Chat Message', 'testing unseen message', 'GraphicsZoo');
        $currentTime_For_CRON = date("Y-m-d H:i:s");
        $dateTimeMinutesAgo = new DateTime("30 minutes ago");
        $dateTimeMinutesAgo = $dateTimeMinutesAgo->format("Y-m-d H:i:s");
        $get_requests_chat_msg = $this->Request_model->get_customer_unseen_msg_for_cron_mail($dateTimeMinutesAgo);
        $get_requests_comment_chat_msg = $this->Request_model->get_customer_unseen_msg_for_cron_mail_in_comment_chat($dateTimeMinutesAgo);
//        echo "<pre>";print_r($get_requests_comment_chat_msg);
//        echo "<pre>";print_r($get_requests_chat_msg);exit;
        $arr_chat = array();

        if (!empty($get_requests_chat_msg)) {
            foreach ($get_requests_chat_msg as $get_requests_val) {
                if (!isset($arr_chat[$get_requests_val['reciever_id']])) {
                    $arr_chat[$get_requests_val['reciever_id']] = array();
                    $arr_chat[$get_requests_val['reciever_id']]['first_name'] = $get_requests_val['first_name'];
                    $arr_chat[$get_requests_val['reciever_id']]['last_name'] = $get_requests_val['last_name'];
                    $arr_chat[$get_requests_val['reciever_id']]['email'] = $get_requests_val['email'];
                    $arr_chat[$get_requests_val['reciever_id']]['id'] = $get_requests_val['reciever_id'];
                    $arr_chat[$get_requests_val['reciever_id']]['message'] = $get_requests_val['message'];
                    $arr_chat[$get_requests_val['reciever_id']]['requests'] = array();
                }
                if (!isset($arr_chat[$get_requests_val['reciever_id']]['requests'][$get_requests_val['request_id']])) {
                    //$arr_chat[$get_requests_val['reciever_id']][$get_requests_val['request_id']] = array();
                    $arr_chat_inner = array();
                    $arr_chat_inner['request_id'] = $get_requests_val['request_id'];
                    $arr_chat_inner['title'] = $get_requests_val['title'];
                    $arr_chat_inner['flg'] = 1;
                    $arr_chat[$get_requests_val['reciever_id']]['requests'][$get_requests_val['request_id']] = $arr_chat_inner;
                }

            }
        }

        if (!empty($get_requests_comment_chat_msg)) {
            foreach ($get_requests_comment_chat_msg as $get_requests_comment_chat_msg_val) {
                if (!isset($arr_chat[$get_requests_comment_chat_msg_val['receiver_id']])) {
                    $arr_chat[$get_requests_comment_chat_msg_val['receiver_id']] = array();
                    $arr_chat[$get_requests_comment_chat_msg_val['receiver_id']]['first_name'] = $get_requests_comment_chat_msg_val['first_name'];
                    $arr_chat[$get_requests_comment_chat_msg_val['receiver_id']]['last_name'] = $get_requests_comment_chat_msg_val['last_name'];
                    $arr_chat[$get_requests_comment_chat_msg_val['receiver_id']]['email'] = $get_requests_comment_chat_msg_val['email'];
                    $arr_chat[$get_requests_comment_chat_msg_val['receiver_id']]['id'] = $get_requests_comment_chat_msg_val['receiver_id'];
                    $arr_chat[$get_requests_comment_chat_msg_val['receiver_id']]['message'] = $get_requests_comment_chat_msg_val['message'];
                    $arr_chat[$get_requests_comment_chat_msg_val['receiver_id']]['requests'] = array();
                }
                if (!isset($arr_chat[$get_requests_comment_chat_msg_val['receiver_id']]['requests'][$get_requests_comment_chat_msg_val['request_id']])) {
                    $arr_chat_inner = array();
                    $arr_chat_inner['request_id'] = $get_requests_comment_chat_msg_val['request_id'];
                    $arr_chat_inner['title'] = $get_requests_comment_chat_msg_val['title'];
                    $arr_chat_inner['flg'] = 0;
                    $arr_chat_inner['request_file_id'] = $get_requests_comment_chat_msg_val['request_file_id'];
                    $arr_chat_inner['request_file_status'] = $get_requests_comment_chat_msg_val['request_file_status'];
                    $arr_chat[$get_requests_comment_chat_msg_val['receiver_id']]['requests'][$get_requests_comment_chat_msg_val['request_id']] = $arr_chat_inner;
                }
            }
            
        }
       // echo "<pre>";print_r($arr_chat);exit;
        if (!empty($arr_chat)) { 
        foreach ($arr_chat as $Chatmsg) {
         
           $cus_id = $Chatmsg['id'];
           $userdata = $this->Request_model->getuserbyid($cus_id);
           $subdomain_url = $this->myfunctions->dynamic_urlforsubuser_inemail($cus_id,$userdata[0]['plan_name']);
           $cus_name = $Chatmsg['first_name'];
           $cus_lname = $Chatmsg['last_name'];
           $cus_email = $Chatmsg['email'];
           $cus_message = $Chatmsg['message'];
           $cus_msg = "<p class='content_text' style='margin: 0px 0px 0px 35px;'>";
           $cus_msg .= "<div style='background: #f5f5f5;padding: 20px; margin: 10px 0;margin-left: 35px;margin-bottom: 30px;margin-right:35px'>";
           foreach ($Chatmsg['requests'] as $Chatvalue_msg) {
             $cus_request_id = $Chatvalue_msg['request_id'];
             $cus_flg = $Chatvalue_msg['flg'];
             $req_title = $Chatvalue_msg['title'];
             $cus_msg .= "<div style=''><div class='req_name_wth_msg'>";
             $cus_msg .= "<h3 style='margin: 15px 0px 0px 0; font: normal 15px/30px 'GothamPro-Medium',sans-serif;color: #20364c;'><b>Project : ".$req_title."</b> </h3> ";

             if($cus_flg == 1){
               $mainchat_msg = $this->Request_model->get_customer_unseen_msg_of_mainchat($dateTimeMinutesAgo,$cus_request_id);  
               
               foreach ($mainchat_msg as $mainmsg) {
                $cus_msg .= "<p style='border-radius: 8px;padding: 8px;background: #fff;color: #20364c;'>".$mainmsg['message']."</p> ";   
               }
               $cus_msg .= "</div><p><a href='".$subdomain_url."project_info_view/".$cus_request_id."?id={{CUSTOMER_ID}}' style='background: #e8304d;color: #fff; text-decoration: none;font-size:14px;widith:120px;display: inline-block; text-align:center;padding: 9px 20px;border-radius: 8px;font-weight: bold;'>View Project</a></p>";
             }
             else{
                 $cus_request_file_id = $Chatvalue_msg['request_file_id'];
                 $mainchat_msg = $this->Request_model->get_customer_unseen_msg_of_commentchat($dateTimeMinutesAgo,$cus_request_file_id);
                 if($request_file_status != 'pending' && $request_file_status != 'Reject'){
                 foreach ($mainchat_msg as $mainmsg) {
                            $cus_msg .= "<p style='border-radius: 8px;padding: 8px;background: #fff;color: #20364c;'>" . $mainmsg['message'] . "</p> ";
                  }
                $cus_msg .= "</div><p><a href='".$subdomain_url."project_view/".$cus_request_file_id."?id={{CUSTOMER_ID}}' style='background: #e8304d;color: #fff; text-decoration: none;font-size:14px;widith:120px;display: inline-block; text-align:center;padding: 9px 20px;border-radius: 8px;font-weight: bold;'>View Project </a></p>";
                 }
             }
            }
            $cus_msg .= "</div>";
            $cus_msg .= "</p>";
            $data = array(
             'cus_name'=> $cus_name,
             'cus_request_id' => $cus_request_id,
             'cus_request_file_id' => $cus_request_file_id,
                'cus_msg' => $cus_msg
                 );
        $body = $cus_msg;
        $is_cronenable = $this->Admin_model->emailgoingbyCron($cus_id);
        //$allsubuserbyparentid = $this->Request_model->getAllsubUsers($cus_id);
        if((isset($Chatvalue_msg['request_file_id']) && $Chatvalue_msg['request_file_status'] != 'pending') || $Chatvalue_msg['flg'] == 1){
        $send_emailto_subusers = $this->myfunctions->send_emails_to_sub_user($cus_id,'comment_on_req','',$cus_request_id);
        if(!empty($send_emailto_subusers)){
        foreach ($send_emailto_subusers as $send_emailto_subuser) {
        $subuser_email = $this->Request_model->getuserbyid($send_emailto_subuser['cus_id']);
        $body_updated = str_replace('{{CUSTOMER_ID}}', $send_emailto_subuser['cus_id'], $body);
        $array = array('CUSTOMER_NAME' => $subuser_email[0]['first_name'],
                    'MESSAGE' => $body_updated);
            $this->myfunctions->send_email_to_users_by_template($cus_id,'new_message_email',$array,$send_emailto_subuser['email']);
        }
        }
        if($is_cronenable){
             $body = str_replace('{{CUSTOMER_ID}}', $cus_id, $body);
             $array1 = array('CUSTOMER_NAME' => $cus_name,
                    'MESSAGE' => $body);
           // SM_sendMail($cus_email, 'New Chat Message', $body, 'GraphicsZoo');
            $this->myfunctions->send_email_to_users_by_template($cus_id,'new_message_email',$array1,$cus_email);
            //echo "sent mail".$cus_email;
        }
        }
    }
       

        }
    }

    public function thank_you_signup(){
        $login_user_id = $this->load->get_var('login_user_id'); 
        $user_data = $this->Welcome_model->getuserbyid($login_user_id);
        $cust_id = isset($user_data[0]['customer_id'])?$user_data[0]['customer_id']:'0';
        $taxpaid = $this->Welcome_model->getTaxamntofuser($cust_id); 
        $user_data[0]['taxpaid'] = isset($taxpaid[0]['tax_amount'])?$taxpaid[0]['tax_amount']:'0';
        $user_data = (!empty($user_data))? $user_data[0] :array();
        $this->load->view('front_end/thank-you-signup', array("user_data" => $user_data));
            
    }
    public function requests_thankyou(){
        $login_user_id = $this->load->get_var('login_user_id'); 
       // echo $login_user_id.'sdagdf';
        $user_data = $this->Welcome_model->getuserbyid($login_user_id);
        $plan_details = $this->Request_model->getsubscriptionlistbyplanid($user_data[0]['plan_name']);
        $stripe_customer = $this->Stripe->getalldetailsofcustomer($user_data[0]['customer_id'],$user_data[0]['invoice']);
       // echo "<pre>";print_r($plan_details);exit;
        $user_data = (!empty($user_data))? $user_data[0] :array();
        $this->load->view('front_end/requests_thankyou', array("user_data" => $user_data,"current_plan_details" => $stripe_customer,"plan_details"=>$plan_details));
    }

    public function thank_you_free_design(){
        $user_data = $this->Welcome_model->getuserbyid($_SESSION['user_id']);
        $user_data = (!empty($user_data))? $user_data[0] :array();
            $this->load->view('front_end/thank-you-free-design', array("user_data" => $user_data));
            
    }
    
	
    public function stripe_webhook(){
        $customer = [];
        $stripe_hook = $this->Stripe->accesswebhook_event(); 
        require_once(APPPATH."libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH."libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH."libraries/portal_lan/email/class.smtp.php");
        $subject = 'Live - GraphicsZoo - Stripe Event just';
        $title = 'Lve - Stripe Event';
        $userdata = $this->Request_model->get_userdata_bycustomerid($stripe_hook['data']['customer_id']);
        if(isset($userdata[0]["is_saas"]) && $userdata[0]["is_saas"] == 1){
           $checkusrpln = $this->Request_model->get_userdata_bysubs_id($userdata[0]["id"],$stripe_hook['data']['sub_id']); 
           if($checkusrpln["subs_type"] == "gz"){
               $userdata["u_tble"] = "users";
               $userdata["p_tble"] = "subscription_plan";
               $userdata["where"] = array("customer_id" => $stripe_hook['data']['customer_id']);
           }else{
               $userdata["u_tble"] = "s_users_setting";
               $userdata["p_tble"] = "s_subscription_plan";
               $userdata["where"] = array("user_id" => $userdata[0]["id"]);
           }
        }else{
            $userdata["u_tble"] = "users";
            $userdata["p_tble"] = "subscription_plan";
            $userdata["where"] = array("customer_id" => $stripe_hook['data']['customer_id']);
        }
        
        $plan_details = array();
        SM_sendMail('hashrajpreet@gmail.com',"new stripe webhook updates",$stripe_hook['result'],$title);
        if($stripe_hook['data']['customer_id'] != ''){
            if($stripe_hook['data']['event_type'] == 'customer.subscription.updated') {
                 SM_sendMail('hashrajpreet@gmail.com',"new stripe webhook after update customer_subscription",$stripe_hook['result'],$title);
                if($userdata[0]['next_plan_name'] != "" && $userdata["u_tble"] != "s_users_setting"){
                   $plan_details = $this->Request_model->getsubscriptionlistbyplanid($userdata[0]['next_plan_name']);
                }
                $this->updateusronsubsupdate($stripe_hook,$userdata,$plan_details);
                if($userdata["u_tble"] != "s_users_setting"){
                    $iscouponapplicable = $this->Account_model->isCancelCouponApplicable($stripe_hook['data']['customer_id']);
                    if($iscouponapplicable){
                        $this->Stripe->updateCustomer($stripe_hook['data']['customer_id'],'',CANCEL_OFFER);
                    }
                }
            }
        }
        if($stripe_hook['data']['event_type'] == 'charge.succeeded'){
            $customer['plan_amount'] = $stripe_hook['data']['amount_paid'];
            $customer['payment_status'] = $stripe_hook['data']['payment_status'];
            $this->Welcome_model->update_data($userdata["u_tble"], $customer, $userdata["where"]);
            
            SM_sendMail('hashkapilkalra@gmail.com','live invoice.payment_succeeded',$stripe_hook['result'],'graphicszoo event');
            if($userdata[0]["is_saas"] != 1){
                
                /*** start affiliate refunded *******/
                $paid_amount = $this->Admin_model->getuser_data($userdata[0]['id'],'id,plan_amount');
                $comm_type = COMMISION_TYPE;
                $comm_fees = COMMISION_FEES;
                if($comm_type == 'percentage'){
                  $amnt = (($comm_fees / 100) * $paid_amount[0]['plan_amount']);
                }
                    $insert_aff_trans = array();
                    $insert_aff_trans['user_id'] = $affiliate_user_info[0]['user_id'];
                    $insert_aff_trans['signup_user_id'] = $userdata[0]['id'];
                    $insert_aff_trans['type'] = 'credit';
                    $insert_aff_trans['amount'] = $amnt;
                    $insert_aff_trans['created'] = date("Y:m:d H:i:s");
                    $this->Welcome_model->insert_data("affiliate_transaction_history", $insert_aff_trans);
                /*** end affiliate refunded *******/
            }
        }
        else if($stripe_hook['data']['event_type'] == 'charge.failed'){
             $customer['payment_status'] = $stripe_hook['data']['payment_status'];
              $this->Welcome_model->update_data($userdata["u_tble"], $customer, $userdata["where"]);
        }
        if($stripe_hook['data']['event_type'] == 'invoice.payment_succeeded'){
             $customer['plan_amount'] = $stripe_hook['data']['amount_paid'];
             $customer['payment_status'] = $stripe_hook['data']['payment_status'];
             $this->Welcome_model->update_data($userdata["u_tble"], $customer, $userdata["where"]);
             SM_sendMail('hashkapilkalra@gmail.com','live invoice.payment_succeeded',$stripe_hook['result'],'graphicszoo event');
        }
        
        if($userdata["u_tble"] != "s_users_setting"){
            if($stripe_hook['data']['event_type'] == 'charge.refunded'){
                 //sleep(50);
                $data = $this->Welcome_model->getuserStatebyCustomer_id($stripe_hook['invoice']['customer_id']);
                //SM_sendMail('hashgaurav@gmail.com','occur event when charge refund from live',$stripe_hook['result'],'graphicszoo event');

                /*****insert sales tax info*******/
               // $cal_tax = $current_plan_price*TEXAS/100;
                $sales['user_id'] = $stripe_hook['invoice']['customer_id'];
                $sales['type'] = "Refund Payment";
                $sales['actual_amount'] = $stripe_hook['invoice']['refund_amount'];
                $sales['tax_amount'] = 0;
                $sales['state'] = $data;
                $sales['debit_credit'] = 'debit';
                $sales['created'] = date("Y:m:d H:i:s");
                //echo "<pre>";print_r($sales);exit;
                $sales_tax_id = $this->Welcome_model->insert_data("sales_tax", $sales);
                /*****end insert sales tax info*******/
                /*** start affiliate refunded *******/
                $this->manageAffiliationHistory($affiliate_user_info[0]['user_id'],'debit',$userdata[0]['id']);
                /*** end affiliate refunded *******/
            }
            if($stripe_hook['data']['event_type'] == 'invoice.created'){
                // sleep(50);
                $data = $this->Welcome_model->getuserStatebyCustomer_id($stripe_hook['invoice']['customer_id']);
               // SM_sendMail('hashgaurav@gmail.com','Invoice created on live site',$stripe_hook['result'],'graphicszoo event');

                /*****insert sales tax info*******/
               // $cal_tax = $current_plan_price*TEXAS/100;
                $sales['user_id'] = $stripe_hook['invoice']['customer_id'];
                $sales['type'] = $stripe_hook['invoice']['billing_reason'];
                $sales['actual_amount'] = $stripe_hook['invoice']['amount'];
                $sales['tax_amount'] = $stripe_hook['invoice']['tax'];
                $sales['state'] = $data;
                $sales['debit_credit'] = 'credit';
                $sales['created'] = date("Y:m:d H:i:s");
                //echo "<pre>";print_r($sales);exit;
                if($stripe_hook['invoice']['tax'] != null || $stripe_hook['invoice']['tax'] != 0){
                $sales_tax_id = $this->Welcome_model->insert_data("sales_tax", $sales);
                }

                /*****end insert sales tax info*******/
            }
            if($stripe_hook['data']['event_type'] == 'customer.subscription.deleted'){
               $array = array('CUSTOMER_NAME' => $userdata[0]['first_name']);
                $subscription_cancel['user_id'] = $stripe_hook['data']['customer_id'];
                $subscription_cancel['subscription_id'] = $stripe_hook['data']['sub_id'];
                $customer['is_cancel_subscription'] =  1;
                $this->Welcome_model->update_data("users", $customer, array("customer_id" => $stripe_hook['data']['customer_id'],"current_plan" => $stripe_hook['data']['sub_id']));
                if($stripe_hook['data']['sub_id'] == $userdata[0]['current_plan']){
                $this->myfunctions->IsSubscriptionCancel($userdata[0]['id'],'from_admin_cancel');
                $this->myfunctions->send_email_to_users_by_template($userdata[0]['id'],'email_after_cancelled_subscription',$array,$userdata[0]['email']);
                }
            }

            if($stripe_hook['data']['event_type'] == 'invoice.payment_failed'){
                $array = array('CUSTOMER_NAME' => $userdata[0]['first_name']);
                if($stripe_hook['invoice']['attempt_count'] == 1){
                    $this->myfunctions->send_email_to_users_by_template($userdata[0]['id'],'payment_failed_retry_1st',$array,$userdata[0]['email']);
                }
                else if($stripe_hook['invoice']['attempt_count'] == 2){
                    $this->myfunctions->send_email_to_users_by_template($userdata[0]['id'],'payment_failed_retry_2nd',$array,$userdata[0]['email']);
                }
                else if($stripe_hook['invoice']['attempt_count'] == 3){
                    $this->myfunctions->send_email_to_users_by_template($userdata[0]['id'],'payment_failed_retry_3rd',$array,$userdata[0]['email']);
                }

            }
        }
    
    }
    
    public function stripewebhook_frconnecteduser(){
//        die("fgkjhk");
        $stripe_hook = $this->Stripe->access_connectuserwebhook(); 
        require_once(APPPATH."libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH."libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH."libraries/portal_lan/email/class.smtp.php");
        $subject = 'GraphicsZoo - Connected Users Stripe Events';
        $title = 'Connected Users - Stripe Events';
		$plan_details = array();
       // mail('hashrajpreet@gmail.com',$subject,"hey testing",$title);
        //echo "working";
       // mail('hashrajpreet@gmail.com',$subject,$stripe_hook['result'],$title);
        $userdata = $this->Request_model->get_userdata_bycustomerid($stripe_hook['data']['customer_id']);
       // SM_sendMail('hashgaurav@gmail.com',$subject,$stripe_hook['result'],$title);
        if($stripe_hook['data']['customer_id'] != ''){
            if($stripe_hook['data']['event_type'] == 'customer.subscription.updated') {
		if($userdata[0]['next_plan_name'] != ""){
                  $plan_details = $this->Clients_model->getsubsbaseduserbyplanid($userdata[0]['next_plan_name']);
		}
                $this->updateusronsubsupdate($stripe_hook,$userdata,$plan_details,$plan_details[0]['global_inprogress_request']);
                mail('hashrajpreet@gmail.com',"live customer.subscription.updated for connect users",$stripe_hook['data'].'<br>'.$stripe_hook['data']['customer_id'],$title);
            }
        }
        if($stripe_hook['data']['event_type'] == 'charge.succeeded'){
            $customer['plan_amount'] = $stripe_hook['data']['amount_paid'];
            $customer['payment_status'] = $stripe_hook['data']['payment_status'];
             $this->Welcome_model->update_data("users", $customer, array("customer_id" => $stripe_hook['data']['customer_id']));
             mail('hashrajpreet@gmail.com',"live charge.succeeded for connect users",$stripe_hook['data']['payment_status'].'<br>'.$stripe_hook['data']['customer_id'],$title);
        }
        else if($stripe_hook['data']['event_type'] == 'charge.failed'){
             $customer['payment_status'] = $stripe_hook['data']['payment_status'];
              $this->Welcome_model->update_data("users", $customer, array("customer_id" => $stripe_hook['data']['customer_id']));
              mail('hashrajpreet@gmail.com',"live charge.failed for connect users",$stripe_hook['data']['payment_status'].'<br>'.$stripe_hook['data']['customer_id'],$title);
        }
        if($stripe_hook['data']['event_type'] == 'invoice.payment_succeeded'){
             $customer['plan_amount'] = $stripe_hook['data']['amount_paid'];
             $customer['payment_status'] = $stripe_hook['data']['payment_status'];
             $this->Welcome_model->update_data("users", $customer, array("customer_id" => $stripe_hook['data']['customer_id']));
             mail('hashrajpreet@gmail.com',"live invoice.payment_succeeded for connect users",$stripe_hook['data']['payment_status'].'<br>'.$stripe_hook['data']['customer_id'],$title);
        }
        if($stripe_hook['data']['event_type'] == 'charge.refunded'){
             //sleep(50);
            $data = $this->Welcome_model->getuserStatebyCustomer_id($stripe_hook['invoice']['customer_id']);
            //SM_sendMail('hashgaurav@gmail.com','occur event when charge refund from live',$stripe_hook['result'],'graphicszoo event');
            
            /*****insert sales tax info*******/
           // $cal_tax = $current_plan_price*TEXAS/100;
            $sales['user_id'] = $stripe_hook['invoice']['customer_id'];
            $sales['type'] = "Refund Payment";
            $sales['actual_amount'] = $stripe_hook['invoice']['refund_amount'];
            $sales['tax_amount'] = 0;
            $sales['state'] = $data;
            $sales['debit_credit'] = 'debit';
            $sales['created'] = date("Y:m:d H:i:s");
            //echo "<pre>";print_r($sales);exit;
            $sales_tax_id = $this->Welcome_model->insert_data("sales_tax", $sales);
            
            mail('hashrajpreet@gmail.com',"live charge.refunded for connect users",$stripe_hook['invoice'].'<br>'.$stripe_hook['data']['customer_id'],$title);
            /*****end insert sales tax info*******/
        }
        if($stripe_hook['data']['event_type'] == 'invoice.created'){
            // sleep(50);
            $data = $this->Welcome_model->getuserStatebyCustomer_id($stripe_hook['invoice']['customer_id']);
           // SM_sendMail('hashgaurav@gmail.com','Invoice created on live site',$stripe_hook['result'],'graphicszoo event');
            
            /*****insert sales tax info*******/
           // $cal_tax = $current_plan_price*TEXAS/100;
            $sales['user_id'] = $stripe_hook['invoice']['customer_id'];
            $sales['type'] = $stripe_hook['invoice']['billing_reason'];
            $sales['actual_amount'] = $stripe_hook['invoice']['amount'];
            $sales['tax_amount'] = $stripe_hook['invoice']['tax'];
            $sales['state'] = $data;
            $sales['debit_credit'] = 'credit';
            $sales['created'] = date("Y:m:d H:i:s");
            //echo "<pre>";print_r($sales);exit;
            if($stripe_hook['invoice']['tax'] != null || $stripe_hook['invoice']['tax'] != 0){
            $sales_tax_id = $this->Welcome_model->insert_data("sales_tax", $sales);
            }
            mail('hashrajpreet@gmail.com',"live invoice.created for connect users",$stripe_hook['invoice'].'<br>'.$stripe_hook['data']['customer_id'],$title);
            /*****end insert sales tax info*******/
        }
        if($stripe_hook['data']['event_type'] == 'customer.subscription.deleted'){
           $array = array('CUSTOMER_NAME' => $userdata[0]['first_name']);
            $subscription_cancel['user_id'] = $stripe_hook['data']['customer_id'];
            $subscription_cancel['subscription_id'] = $stripe_hook['data']['sub_id'];
            $customer['is_cancel_subscription'] =  1;
            $this->Welcome_model->update_data("users", $customer, array("customer_id" => $stripe_hook['data']['customer_id'],"current_plan" => $stripe_hook['data']['sub_id']));
            if($stripe_hook['data']['sub_id'] == $userdata[0]['current_plan']){
            $this->myfunctions->IsSubscriptionCancel($userdata[0]['parent_id'],'from_admin_cancel',$userdata[0]['id']);
            $this->myfunctions->send_email_to_users_by_template($userdata[0]['id'],'email_after_cancelled_subscription',$array,$userdata[0]['email']);
            mail('hashrajpreet@gmail.com',"live customer.subscription.deleted for connect users",$stripe_hook['data'].'<br>'.$stripe_hook['data']['customer_id'],$title);
            }
        }
        
        if($stripe_hook['data']['event_type'] == 'invoice.payment_failed'){
            $array = array('CUSTOMER_NAME' => $userdata[0]['first_name']);
            if($stripe_hook['invoice']['attempt_count'] == 1){
                $this->myfunctions->send_email_to_users_by_template($userdata[0]['id'],'payment_failed_retry_1st',$array,$userdata[0]['email']);
            }
            else if($stripe_hook['invoice']['attempt_count'] == 2){
                $this->myfunctions->send_email_to_users_by_template($userdata[0]['id'],'payment_failed_retry_2nd',$array,$userdata[0]['email']);
            }
            else if($stripe_hook['invoice']['attempt_count'] == 3){
                $this->myfunctions->send_email_to_users_by_template($userdata[0]['id'],'payment_failed_retry_3rd',$array,$userdata[0]['email']);
            }
            mail('hashrajpreet@gmail.com',"live invoice.payment_failed for connect users",$stripe_hook['invoice'].'<br>'.$stripe_hook['data']['customer_id'],$title);
        }
    }
    
    public function updateusronsubsupdate($stripe_hook,$nextplan_name = array(),$plan_details = array(),$active_req =""){
        $customer['billing_start_date'] = $stripe_hook['data']['start_period'];
        $customer['billing_end_date'] = $stripe_hook['data']['end_period'];
        if (strtotime($nextplan_name[0]['billing_end_date']) != strtotime($customer['billing_end_date'])) {
            if ($stripe_hook['data']['previous_plan_amount'] != '') {
                if ($stripe_hook['data']['plan_amount'] < $stripe_hook['data']['previous_plan_amount']) {
                    $customer['payment_status'] = '1';
                } else {
                    $customer['payment_status'] = '2';
                }
            } else {
                $customer['payment_status'] = '2';
            }
        }
        if (($stripe_hook['data']['previous_start_period'] != '' || $stripe_hook['data']['previous_start_period'] != NULL) && ($stripe_hook['data']['previous_end_period'] != '' || $stripe_hook['data']['previous_end_period'] != NULL)) {
            //  $nextplan_name = $this->Request_model->get_userdata_bycustomerid($stripe_hook['data']['customer_id']);
            if ($nextplan_name[0]['next_plan_name'] != NULL && $nextplan_name[0]['next_plan_name'] == $stripe_hook['data']['plan_name'] && $userdata["u_tble"] != "s_users_setting") {
                if ($nextplan_name[0]['next_plan_name'] == SUBSCRIPTION_99_PLAN) {
                    $current_date = date("Y:m:d H:i:s");
                    $monthly_date = strtotime("+1 months", strtotime($current_date)); // returns timestamp
                    $billing_expire = date('Y:m:d H:i:s', $monthly_date);
                    $customer['billing_cycle_request'] = 3;
                } else {
                    $customer['billing_cycle_request'] = 0;
                }
                $customer['plan_name'] = $stripe_hook['data']['plan_name'];
                $customer['current_plan'] = $stripe_hook['data']['sub_id'];
                $customer['plan_trial_period_days'] = $stripe_hook['data']['trial_period_days'];
                $customer['display_plan_name'] = $stripe_hook['data']['display_plan_name'];
                $customer['plan_amount'] = $plan_details[0]['plan_price'];
                $customer['plan_interval'] = $stripe_hook['data']['interval'];
                $customer['next_plan_name'] = "";
                $customer['next_current_plan'] = "";
                $customer['invoice'] = 1;
                $customer['total_inprogress_req'] = ($stripe_hook['data']['plan_quantity'] != "")?$stripe_hook['data']['plan_quantity']:$plan_details[0]['in_progress_request'];
                if($active_req != ""){
                    $customer['total_active_req'] = $active_req;
                }else{
                    $customer['total_active_req'] = $plan_details[0]['in_progress_request'] * TOTAL_ACTIVE_REQUEST;
                }
            }
        }
        if($nextplan_name["u_tble"] == "s_users_setting"){
          $this->Welcome_model->update_data("s_users_setting", $customer, array("user_id" => $nextplan_name[0]['id'], "subscription_id" => $stripe_hook['data']['sub_id']));
        }else{
          $this->Welcome_model->update_data("users", $customer, array("customer_id" => $stripe_hook['data']['customer_id'], "current_plan" => $stripe_hook['data']['sub_id']));  
        }
    }
    
    
    public function project_info($sharekey = NULL){
        $sharekey = isset($sharekey)? $sharekey : '';
        $permissiondata = $this->Welcome_model->getpermissionsbySherekey($sharekey);
        if($permissiondata[0]['is_disabled'] == 0){
           $checkusernamenotexist = (isset($permissiondata[0]['user_name']) && $permissiondata[0]['user_name']!= '')?$permissiondata[0]['user_name']:$_COOKIE['name'];
        if($permissiondata[0]['commenting'] == '1'){
           $checkusernamenotexist = (isset($permissiondata[0]['user_name']) && $permissiondata[0]['user_name']!= '')?$permissiondata[0]['user_name']:$_COOKIE['name'];
        }
        $chat_request = $this->Request_model->get_chat_request_by_id($permissiondata[0]['request_id'],'desc');
        $request_data = $this->Request_model->get_request_by_id($permissiondata[0]['request_id']);
        $request_meta = $this->Category_model->get_quest_Ans($permissiondata[0]['request_id']);
        $sampledata = $this->Request_model->getSamplematerials($permissiondata[0]['request_id']);
        $userdata = $this->Admin_model->getuser_data($request_data[0]['customer_id']);
        $iscanel = $this->checkuserplaniscancel($userdata);
        if($iscanel == 1){
            redirect(base_url()."accessdenied");
        }
        $timediff = '';
        for ($j = 0; $j < count($chat_request); $j++) {
            $chat_request[$j]['sender_data'] = $this->Admin_model->getuser_data($chat_request[$j]['sender_id']);
            $chat_request[$j]['msg_created'] = $this->myfunctions->onlytimezoneforall($chat_request[$j]['created']);
            $chat_request[$j]['chat_created_date'] = $chat_request[$j]['msg_created'];
        }
        $data1 = $this->Request_model->get_request_by_id($permissiondata[0]['request_id']);
        $cat_data = $this->Category_model->get_category_byID($data1[0]['category_id']);
        $cat_name = $cat_data[0]['name'];
        $subcat_data = $this->Category_model->get_category_byID($data1[0]['subcategory_id']);
        $subcat_name = $subcat_data[0]['name'];
        $data1['cat_name'] = $cat_name;
        $data1['subcat_name'] = $subcat_name;
//        echo $permissiondata[0]['request_id'];
        $designer_file = $this->Admin_model->get_requested_files($permissiondata[0]['request_id'], "", "designer", array('1', '2'),true);
        $data1['count_designerfile'] = count($designer_file);
//        echo "<pre/>";print_R($designer_file);exit;
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified'];
            $designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id'], "desc");
            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
            $designer_file[$i]['chat'][$j]['created'] = $this->myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
            $designer_file[$i]['chat'][$j]['msg_created'] = $this->myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
            $designer_file[$i]['chat'][$j]['replies'] = $this->Request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
            $designer_file[$i]['chat'][$j]['created'] =   date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
            }
        }
//        echo "<pre/>";print_R($data1);exit;
        for ($i = 0; $i < count($data1[0]['designer_attachment']); $i++) {  
        if($data1[0]['designer_attachment'][$i]['status'] != 'pending' && $data1[0]['designer_attachment'][$i]['status'] != 'Reject'){
            $data1[0]['approved_attachment'] =  $data1[0]['designer_attachment'][$i]['id']; break;
        }
        }
//         echo "<pre/>";print_R($data1);exit;
        $draft_id = $permissiondata[0]['draft_id'];
        if($permissiondata[0]['request_id'] && $permissiondata[0]['draft_id'] == 0){
            $this->load->view('customer/customer_header_1', array("popup" => 0));
            $this->load->view('front_end/project_info', array('permissiondata'=> $permissiondata,'data' => $data1,'chat_request'=>$chat_request,'checkusernamenotexist'=>$checkusernamenotexist,'userdata' =>$userdata,
                "request_meta" => $request_meta,
            "sampledata" => $sampledata));  
        }else
        {
//            die("xcvxcvxcvxcv");
            $this->load->view('customer/customer_header_1', array("popup" => 0));
            $this->load->view('front_end/project_image_view', array('permissiondata'=>$permissiondata,'draft_id'=>$draft_id,'designer_file' => $designer_file,'chat_request'=>$chat_request,'userdata' =>$userdata,'checkusernamenotexist'=>$checkusernamenotexist,'request_data'=>$request_data));  
        }
     }else{
         redirect(base_url()."accessdenied");
     }
    }
    
    public function project_image_view($key){
        $permissiondata = $this->Welcome_model->getpermissionsbySherekey($key);
        if($permissiondata[0]['is_disabled'] == 0){
        $designer_file = $this->Admin_model->get_requested_files($permissiondata[0]['request_id'], "", "designer", array('1', '2'));
        $request_data = $this->Request_model->get_request_by_id($permissiondata[0]['request_id']);
        $userdata = $this->Admin_model->getuser_data($request_data[0]['customer_id']);
        $iscanel = $this->checkuserplaniscancel($userdata);
        if($iscanel == 1){
            redirect(base_url()."accessdenied");
        }
        $checkusernamenotexist = (isset($permissiondata[0]['user_name']) && $permissiondata[0]['user_name'] != '') ? $permissiondata[0]['user_name'] : $_COOKIE['name'];
        if($permissiondata[0]['commenting'] == '1'){
            $checkusernamenotexist = (isset($permissiondata[0]['user_name']) && $permissiondata[0]['user_name'] != '') ? $permissiondata[0]['user_name'] : $_COOKIE['name'];
        }
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified'];
            $designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id'], "desc");
            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
            $designer_file[$i]['chat'][$j]['created'] = $this->myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
            $designer_file[$i]['chat'][$j]['msg_created'] = $this->myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
            $designer_file[$i]['chat'][$j]['replies'] = $this->Request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
            $designer_file[$i]['chat'][$j]['created'] =   date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
            }
        }
        $this->load->view('customer/customer_header_1', array("popup" => 0));
        $this->load->view('front_end/project_image_view', array('permissiondata'=>$permissiondata,'designer_file' => $designer_file,'checkusernamenotexist'=>$checkusernamenotexist,'request_data'=> $request_data,'userdata' => $userdata));  
    }else{
      redirect(base_url()."accessdenied");  
    }
    }
    
    public function project_info_for_unseenmsg($id) {
        // if no user ID then redirect the user to access denied page
        $userid = isset($_GET['id']) ? $_GET['id'] : 0;
        if ($userid == 0) {
            redirect(base_url() . "accessdenied");
        }
        $permissiondata[0]['request_id'] = $id;
        $permissiondata[0]['share_key'] = $id;
        $permissiondata[0]['draft_id'] = 0;
        $permissiondata[0]['cron_msg'] = 1;
        $data1 = $this->Request_model->get_request_by_id($id);
        // for some cases $data1 is used but for some other checks $request_data is used / gaurav
        $request_data = $data1;
        $chat_request = $this->Request_model->get_chat_request_by_id($id);
        $userdata = $this->Admin_model->getuser_data($userid);
        $iscanel = $this->checkuserplaniscancel($userdata);
        $request_meta = $this->Category_model->get_quest_Ans($permissiondata[0]['request_id']);
        $sampledata = $this->Request_model->getSamplematerials($permissiondata[0]['request_id']);
        if ($iscanel == 1) {
            redirect(base_url() . "accessdenied");
        }
        // if user ID is of parent user but user is not related to that request
        if ($userdata[0]['parent_id'] == 0 && $userid != $request_data[0]['customer_id']) {
            redirect(base_url() . "accessdenied");
        }
        // if user ID is of sub user but parent user is not related to that request 
        if ($userdata[0]['parent_id'] != 0 && $userdata[0]['parent_id'] != $request_data[0]['customer_id']) {
            redirect(base_url() . "accessdenied");
        }

        //for sub user permission
        if ($userdata[0]['parent_id'] != 0) {
            $permissiondata[0]['cancomment_on_req'] = $this->myfunctions->isUserPermission('comment_on_req', $userid);
            $permissiondata[0]['candownload_file'] = $this->myfunctions->isUserPermission('download_file', $userid);
            $permissiondata[0]['canapprove_file'] = $this->myfunctions->isUserPermission('approve/revision_requests', $userid);
            $permissiondata[0]['canbrand_profile_access'] = $this->myfunctions->isUserPermission('brand_profile_access', $userid);
            if ($permissiondata[0]['canbrand_profile_access'] != 1 && $data1[0]['created_by'] == $userid) {
                $permissiondata[0]['canbrand_profile_access'] = 1;
            } else if ($permissiondata[0]['canbrand_profile_access'] != 1) {
                $permissiondata[0]['sub_usersbrands'] = $this->Request_model->getuserspermisionBrands($userid);

                if (!empty($permissiondata[0]['sub_usersbrands'])) {
                    if (in_array($request_data[0]['brand_id'], $permissiondata[0]['sub_usersbrands'])) {
                        $permissiondata[0]['see_draft'] = 1;
                    } else {
                        $permissiondata[0]['see_draft'] = 0;
                        redirect(base_url());
                    }
                } elseif ($request_data[0]['created_by'] == $userid) {
                    $permissiondata[0]['see_draft'] = 1;
                } else {
                    $permissiondata[0]['see_draft'] = 0;
                    redirect(base_url());
                }
            }
        } else {
            $permissiondata[0]['cancomment_on_req'] = 1;
            $permissiondata[0]['candownload_file'] = 1;
            $permissiondata[0]['canapprove_file'] = 1;
            $permissiondata[0]['canbrand_profile_access'] = 1;
        }


        $timediff = '';
        for ($j = 0; $j < count($chat_request); $j++) {
            $chat_request[$j]['sender_data'] = $this->Admin_model->getuser_data($chat_request[$j]['sender_id']);
            $chat_request[$j]['msg_created'] = $this->myfunctions->onlytimezoneforall($chat_request[$j]['created']);
            $chat_request[$j]['chat_created_date'] = $chat_request[$j]['msg_created'];
        }

        $cat_data = $this->Category_model->get_category_byID($data1[0]['category_id']);
        $cat_name = $cat_data[0]['name'];
        $subcat_data = $this->Category_model->get_category_byID($data1[0]['subcategory_id']);
        $subcat_name = $subcat_data[0]['name'];
        $data1['cat_name'] = $cat_name;
        $data1['subcat_name'] = $subcat_name;
        $designer_file = $this->Admin_model->get_requested_files($id, "", "designer", array('1', '2'));
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified'];
            $designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id'], "asc");
            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
                $designer_file[$i]['chat'][$j]['created'] = $this->myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
                $designer_file[$i]['chat'][$j]['msg_created'] = $this->myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
                $designer_file[$i]['chat'][$j]['created'] = date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
                $designer_file[$i]['chat'][$j]['replies'] = $this->Request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
                $designer_file[$i]['chat'][$j]['created'] =   date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
                
            }
        }
        $draft_id = $permissiondata[0]['draft_id'];

        $this->load->view('customer/customer_header_1', array("popup" => 0));
        $this->load->view('front_end/project_info', array('login_user_id' => $userid, 'checkusernamenotexist' => 1, 'permissiondata' => $permissiondata, 'userdata' => $userdata, 'data' => $data1, 'chat_request' => $chat_request, 'userid' => $userid,
            "request_meta" => $request_meta,
            "sampledata" => $sampledata));
    }
    
    public function project_image_view_afteraprovedesign($id) {
        // if no user ID then redirect the user to access denied page
        $userid = isset($_GET['id']) ? $_GET['id'] : 0;
        if ($userid == 0) {
            redirect(base_url() . "accessdenied");
        }
        $requestidbydraftid = $this->Request_model->get_image_by_request_file_id($id);
        $permissiondata[0]['request_id'] = $requestidbydraftid[0]['request_id'];
        $permissiondata[0]['draft_id'] = $id;
        $permissiondata[0]['admin_approvedraft'] = 1;
        $designer_file = $this->Admin_model->get_requested_files($requestidbydraftid[0]['request_id'], "", "designer", array('1', '2'));
        $request_data = $this->Request_model->get_request_by_id($requestidbydraftid[0]['request_id']);

        $userdata = $this->Admin_model->getuser_data($userid);
        $iscanel = $this->checkuserplaniscancel($userdata);
        // if user subscription cancel then redirec the user to access denied page
        if ($iscanel == 1) {
            redirect(base_url() . "accessdenied");
        }
        // if user ID is of parent user but user is not related to that request
        if ($userdata[0]['parent_id'] == 0 && $userid != $request_data[0]['customer_id']) {
            redirect(base_url() . "accessdenied");
        }
        // if user ID is of sub user but parent user is not related to that request 
        if ($userdata[0]['parent_id'] != 0 && $userdata[0]['parent_id'] != $request_data[0]['customer_id']) {
            redirect(base_url() . "accessdenied");
        }

        //for sub user permission
        if ($userdata[0]['parent_id'] != 0) {
            $permissiondata[0]['cancomment_on_req'] = $this->myfunctions->isUserPermission('comment_on_req', $userid);
            $permissiondata[0]['candownload_file'] = $this->myfunctions->isUserPermission('download_file', $userid);
            $permissiondata[0]['canapprove_file'] = $this->myfunctions->isUserPermission('approve/revision_requests', $userid);
            $permissiondata[0]['canbrand_profile_access'] = $this->myfunctions->isUserPermission('brand_profile_access', $userid);
            if ($permissiondata[0]['canbrand_profile_access'] != 1 && $data1[0]['created_by'] == $userid) {
                $permissiondata[0]['canbrand_profile_access'] = 1;
            } else if ($permissiondata[0]['canbrand_profile_access'] != 1) {
            
                $permissiondata[0]['sub_usersbrands'] = $this->Request_model->getuserspermisionBrands($userid);

                if (!empty($permissiondata[0]['sub_usersbrands'])) {
                    if (in_array($request_data[0]['brand_id'], $permissiondata[0]['sub_usersbrands'])) {
                        $permissiondata[0]['see_draft'] = 1;
                    } else {
                        $permissiondata[0]['see_draft'] = 0;
                        redirect(base_url());
                    }
                } elseif ($request_data[0]['created_by'] == $userid) {
                    $permissiondata[0]['see_draft'] = 1;
                } else {
                    $permissiondata[0]['see_draft'] = 0;
                    redirect(base_url());
                }
            }
        } else {
            $permissiondata[0]['cancomment_on_req'] = 1;
            $permissiondata[0]['candownload_file'] = 1;
            $permissiondata[0]['canapprove_file'] = 1;
            $permissiondata[0]['canbrand_profile_access'] = 1;
        }
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified'];
            $designer_file[$i]['chat'] = $this->Request_model->get_chat_by_id($designer_file[$i]['id'], "DESC");
            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
                $designer_file[$i]['chat'][$j]['created'] = $this->myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
                $designer_file[$i]['chat'][$j]['msg_created'] = $this->myfunctions->onlytimezoneforall($designer_file[$i]['chat'][$j]['created']);
                $designer_file[$i]['chat'][$j]['created'] = date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
                $designer_file[$i]['chat'][$j]['replies'] = $this->Request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
                $designer_file[$i]['chat'][$j]['created'] =   date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
            }
        }
        $this->load->view('customer/customer_header_1', array("popup" => 0));
        $this->load->view('front_end/project_image_view', array('login_user_id' => $userid, 'checkusernamenotexist' => 1, 'permissiondata' => $permissiondata, 'designer_file' => $designer_file, 'data' => $request_data, 'userdata' => $userdata,'request_data' => $request_data));
    }
    
    public function SaveSharedusername(){
        if(isset($_POST['savename'])){
           $user_name = isset($_POST['user_name'])?$_POST['user_name']:'';
           $share_key = isset($_POST['share_key'])?$_POST['share_key']:'';
           setcookie("name", $user_name, time() + (86400 * 30), "/");          
          if(isset($_COOKIE['name']) && $_COOKIE['name'] != ''){
              redirect(base_url() . "project-info/".$share_key);
          }
           $this->session->set_flashdata('message_success','Congratulations! you can do comments.', 5);
          redirect(base_url() . "project-info/".$share_key);
        }
    }
    
    public function without_pay_signup() {
        $titlestatus = "";
        $rendomly_qa_id = $this->Admin_model->getqa_id();
        if (!empty($_POST)) {
           // echo "<pre>";print_r($_POST);exit;
            $checkemail = $this->Request_model->getuserbyemail($_POST['email']);
            if (!empty($checkemail)) {
                $this->session->set_flashdata('message_error', "There is already an account associated with this email.", 5);
                redirect(base_url() . "paywall_signup");
            }
            /* get timezone */
                $ipInfo = $this->myfunctions->gettimezone();
                $state = $ipInfo->regionName;
                $timezone = $ipInfo->timezone;
            /* timezone ends */
                
            //$set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($set), 0, 20);
            $customer['is_active'] = 1;
            $customer['is_trail'] = 0;
            $customer['without_pay_user'] = 1;
           //$customer['activation'] = $code;   
            $customer['email'] = $_POST['email'];
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['phone'] = $_POST['phone'];
            $customer['new_password'] = md5($_POST['password']);
            $customer['role'] = "customer";
            $customer['created'] = date("Y:m:d H:i:s");
            $customer['modified'] = date("Y:m:d H:i:s");
            $customer['last_update'] = date("Y:m:d H:i:s");
            $customer['timezone'] = $timezone;
            $customer['state'] = $state;
            $customer['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $customer['qa_id'] = $rendomly_qa_id[0]['id'];
            $customer['tour'] = '1';
            $customer['total_inprogress_req'] = TOTAL_INPROGRESS_REQUEST;
            $customer['total_active_req'] = TOTAL_ACTIVE_REQUEST;	
            $customer['is_logged_in'] = 0;
            $customer['YOUR_SOURCE_FIELD'] = isset($_POST['YOUR_SOURCE_FIELD'])?$_POST['YOUR_SOURCE_FIELD']:'';
            $customer['YOUR_MEDIUM_FIELD'] = isset($_POST['YOUR_MEDIUM_FIELD'])?$_POST['YOUR_MEDIUM_FIELD']:'';
            $customer['YOUR_CAMPAIGN_FIELD'] = isset($_POST['YOUR_CAMPAIGN_FIELD'])?$_POST['YOUR_CAMPAIGN_FIELD']:'';
            $customer['YOUR_CONTENT_FIELD'] = isset($_POST['YOUR_CONTENT_FIELD'])?$_POST['YOUR_CONTENT_FIELD']:'';
            $customer['YOUR_TERM_FIELD'] = isset($_POST['YOUR_TERM_FIELD'])?$_POST['YOUR_TERM_FIELD']:'';
            $customer['VISITS'] =  isset($_POST['VISITS'])?$_POST['VISITS']:'';
            $id = $this->Welcome_model->insert_data("users", $customer);
            //echo $id;
            if ($id) { 
                /****drip start********/
                $this->load->library('drip');
                $this->load->library('datasett');
                $this->load->library('response');
				// create new subscriber
                $Drip = new Drip('5791e6d8913a121374b5e55904342eb3','1975472');
                $data = new Datasett('subscribers', [
                    'email' => $_POST['email'],
                ]);
                $Response = $Drip->post('subscribers', $data);
				// assign free design user tag
				$Drip = new Drip('5791e6d8913a121374b5e55904342eb3','1975472');
				$data = new Datasett('tags', [
					'email' => $_POST['email'],
					'tag' => "Free Design Sign-up",
				]);
				$Drip->post('tags', $data);
                //$this->send_email_after_signup($code,$id,$_POST['email']);
                                
                /* set user session data for login */
                $this->session->set_userdata('user_id', $id);
                $this->session->set_userdata('timezone', $timezone);
                $this->session->set_userdata('role', $customer['role']);
                $this->session->set_userdata('first_name', $customer['first_name']);
                $this->session->set_userdata('last_name', $customer['last_name']);
                $this->session->set_userdata('email', $customer['email']);
                $this->session->set_userdata('tour', 0);
                $this->session->set_userdata('qa_id', $customer['qa_id']);
                $array = array('CUSTOMER_NAME' => $customer['first_name'],
                    'PROJECT_LINK' => base_url().'customer/request/add_new_request');
               // $this->myfunctions->send_email_to_users_by_template($id,'welcome_email_to_without_pay_user',$array,$customer['email']);
                
                /* set user session data for login ends */
                
               /* add dummy request for without pay user */
                $this->myfunctions->add_dummy_request($id);
               /* end add dummy request for without pay user */
                
                //$this->session->set_flashdata('message_success', "User created successfully and subscribed successfully..!", 5);
                redirect(base_url() . "customer/request/design_request");
                
            } else {
                $this->session->set_flashdata('message_error', "User can not created successfully ..!", 5);
                redirect(base_url() . "paywall_signup");
            }
        }
        
        $this->load->view('front_end/without_pay_signup', array("planlist" => $subscription_plan_list, 'titlestatus' => $titlestatus));
    }
    
    public function single_request_signup(){
        $output = array();
        $titlestatus = "";
        $rendomly_qa_id = $this->Admin_model->getqa_id();
        if (!empty($_POST)) {
            parse_str($_POST['formdata'], $params);
            
            if($_POST['submit_form'] == 1){
            $is_coupon_valid = $params['couponinserted_ornot'];
            $checkemail = $this->Request_model->getuserbyemail($params['email']);
            if (!empty($checkemail)) {
                $output['message'] = "There is already an account associated with this email.";
                $output['status'] = false;
                echo json_encode($output);exit;
            }
            if($is_coupon_valid == 0)
            {
                $stripe_customer = $this->Stripe->createnewCustomer($params['email']);
            }else{
                $stripe_customer = $this->Stripe->createnewCustomer($params['email'], $params['Discount']);   
             }
            $expir_date = explode("/", $params['expir_date']);
            if (!$stripe_customer['status']) {
                $output['message'] = $stripe_customer['message'];
                $output['status'] = false;
                echo json_encode($output);exit;
            }
            $customer_id = $stripe_customer['data']['customer_id'];
            $card_number = trim($params['card_number']);
            $expiry_month = $expir_date[0];
            $expiry_year = $expir_date[1];
            $cvc = $params['cvc'];

            $stripe_card = $this->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);
            if (!$stripe_card['status']){
                $output['message'] = $stripe_card['message'];
                $output['status'] = false;
                echo json_encode($output);exit;
            }else{
            $output['message'] = "Successfully created customer.";
            $output['status'] = true;
            $output['customer_id'] = $stripe_customer['data']['customer_id'];
            }
            }
            if($_POST['submit_form'] == 0){
             //   echo "<pre>";print_r($params);print_r($_POST);
            $customer_id = isset($_POST['customer_id'])?$_POST['customer_id']:'';
            $plan_name = isset($_POST['plan_name'])?$_POST['plan_name']:'';
            $inprogress_request = isset($_POST['inprogres_req'])?$_POST['inprogres_req']:'';
            $plan_amount = isset($_POST['plan_price'])?$_POST['plan_price']:'';
            $state_tax = isset($params['state_tax'])?$params['state_tax']:0;

            if($plan_name == SUBSCRIPTION_MONTHLY){
            $customer['total_inprogress_req'] = $inprogress_request;
            $customer['total_active_req'] = $inprogress_request * TOTAL_ACTIVE_REQUEST;
            $stripe_subscription = '';
                $stripe_customer = $this->Stripe->updateCustomer($customer_id,'',DISCOUNT_CODE);
                $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($customer_id, $plan_name, $inprogress_request,$state_tax);
                $current_date = date('Y-m-d H:i:s',$stripe_subscription['data']['period_start']);
                $billing_expire = date('Y-m-d H:i:s',$stripe_subscription['data']['period_end']);
                $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
                $customer['billing_start_date'] = $current_date;
                $customer['billing_end_date'] = $billing_expire;
                $plandetails = $this->Stripe->retrieveoneplan($plan_name);
                $customer['plan_interval'] = $plandetails['interval'];
                $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
                $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
                $customer['display_plan_name'] = $plandetails['name'];
            if (!$stripe_subscription['status']) {
                 $output['message'] = $stripe_subscription['message'];
                 $output['status'] = false;
                 echo json_encode($output);exit;
            }
            }
            else{
            $customer['total_inprogress_req'] = 0;    
            $customer['total_requests'] = $inprogress_request;
            $customer['payment_status'] = 2;
            
            $customer['total_active_req'] = 0;
            $customer['billing_start_date'] = date('Y-m-d H:i:s');
            $customer['display_plan_name'] = $_POST['display_name'];
            $invoice_created = $this->Stripe->create_invoice($customer_id,$state_tax,$plan_amount);
            $customer['invoice'] = $invoice_created['id'];
            //echo $invoice_created['id'].'test-'.$invoice_created->id."<pre>";print_r($invoice_created);
            if($invoice_created['status'] != true){
                 $output['message'] = $invoice_created['message'];
                 $output['status'] = $invoice_created['status'];
                 echo json_encode($output);exit;
                }
            }
            
            /* get timezone */
                $ipInfo = $this->myfunctions->gettimezone();
                $state = $ipInfo->regionName;
                $timezone = $ipInfo->timezone;
            /* timezone ends */
                
             if($params['state'] != ''){
                    $state = $params['state'];
                }else{
                    $state = $ipInfo->regionName;
                }

            $customer['customer_id'] = $customer_id;
            $customer['plan_name'] = $plan_name;
            $customer['is_single_request_signup'] = "1";
            $customer['is_active'] = 1;
            $customer['activation'] = '';
            $customer['plan_amount'] = $plan_amount;
            $customer['email'] = $params['email'];
            $customer['first_name'] = $params['first_name'];
            $customer['last_name'] = $params['last_name'];
            $customer['new_password'] = md5($params['password']);
            $customer['role'] = "customer";
            $customer['created'] = date("Y:m:d H:i:s");
            $customer['modified'] = date("Y:m:d H:i:s");
            $customer['timezone'] = $timezone;
            $customer['state'] = $state;
            $customer['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $customer['is_logged_in'] = 0;
            $customer['last_update'] = date("Y:m:d H:i:s");
            $customer['qa_id'] = $rendomly_qa_id[0]['id'];
            $customer['YOUR_SOURCE_FIELD'] = isset($params['YOUR_SOURCE_FIELD'])?$params['YOUR_SOURCE_FIELD']:'';
            $customer['YOUR_MEDIUM_FIELD'] = isset($params['YOUR_MEDIUM_FIELD'])?$params['YOUR_MEDIUM_FIELD']:'';
            $customer['YOUR_CAMPAIGN_FIELD'] = isset($params['YOUR_CAMPAIGN_FIELD'])?$params['YOUR_CAMPAIGN_FIELD']:'';
            $customer['YOUR_CONTENT_FIELD'] = isset($params['YOUR_CONTENT_FIELD'])?$params['YOUR_CONTENT_FIELD']:'';
            $customer['YOUR_TERM_FIELD'] = isset($params['YOUR_TERM_FIELD'])?$params['YOUR_TERM_FIELD']:'';
            $customer['VISITS'] =  isset($params['VISITS'])?$params['VISITS']:'';
           // echo "<pre>";print_r($customer);
            $id = $this->Welcome_model->insert_data("users", $customer);
            
            /*****end insert sales tax info*******/
            if ($id) {
                if(in_array($plan_name, NEW_PLANS)){
                if($plan_name == 'plan_49REQ'){
                    $tag = '$49 Design';
                }elseif($plan_name == 'plan_149REQ'){
                    $tag = '$149 Designs';
                }else{
                    $tag = '$99 Designs';
                    
                }    
                /****drip start********/
                $this->load->library('drip');
                $this->load->library('datasett');
                $this->load->library('response');
		// create new subscriber
                $Drip = new Drip('5791e6d8913a121374b5e55904342eb3','1975472');
                $data = new Datasett('subscribers', [
                    'email' => $params['email'],
                ]);
                $Response = $Drip->post('subscribers', $data);
                // assign free design user tag
                $Drip = new Drip('5791e6d8913a121374b5e55904342eb3','1975472');
                $data = new Datasett('tags', [
                        'email' => $params['email'],
                        'tag' => $tag,
                ]);
               // echo "<pre>";print_r($data);exit;
		$Drip->post('tags', $data);
                }
                /****drip end********/
                
                /* add dummy request */
                $this->myfunctions->add_dummy_request($id);
                /* end add dummy request */
                
                /* set user session data for login */
                $this->session->set_userdata('user_id', $id);
                $this->session->set_userdata('timezone', $timezone);
                $this->session->set_userdata('role', $customer['role']);
                $this->session->set_userdata('first_name', $customer['first_name']);
                $this->session->set_userdata('last_name', $customer['last_name']);
                $this->session->set_userdata('email', $customer['email']);
                $this->session->set_userdata('tour', 0);
                $this->session->set_userdata('qa_id', $customer['qa_id']);
                $array = array('CUSTOMER_NAME' => $customer['first_name'],
                    'PROJECT_LINK' => base_url().'customer/request/add_new_request');
                //$this->myfunctions->send_email_to_users_by_template($id,'welcome_email_to_user',$array,$customer['email']);
                $output['message'] = "User created successfully and subscribed successfully..!";
                $output['status'] = true;
                /* set user session data for login ends */
            } else {
                $output['message'] = "User can not created successfully ..!";
                $output['status'] = false;
            }
            }
            echo json_encode($output);
            exit;
        }
        $this->load->view('front_end/single_request_signup');
    }
    
    public function loginforsubdomain() {
        $subdomain_name = (explode(".",HOST_NAME));
        $getuser = $this->Request_model->getuserinfobydomain(HOST_NAME);
      //  echo "<pre>";print_R($getuser);exit;
        $domnname = $subdomain_name[0].'.';
        if(empty($getuser)){
            redirect("https://".DOMAIN_NAME); 
        }
        if(!isset($_SERVER["HTTPS"]) && $getuser[0]['ssl_or_not'] == 1){
            redirect("https://".HOST_NAME); 
        }
        $titlestatus = "";
        if (!empty($_POST)) {
            $result = $this->Welcome_model->subdomainlogin($getuser[0]['customer_id']);
        }
        if($result){
             redirect(base_url() . "payment");
        }
        
        $this->load->view('front_end/subdomain_login');
        
    }
    
    public function bypasspayment_process() {
        $userid = $_SESSION['user_id'];
        $userdata = $this->Admin_model->getuser_data($userid);
        $current_plan = $this->Clients_model->getsubsbaseduserbyplanid($userdata[0]['plan_name'],"plan_id,payment_mode");
        $subscription_list = $this->Clients_model->getsubscriptionbaseduser($userdata[0]['parent_id'],"",1,"",$current_plan[0]["payment_mode"]); 
       // echo "<pre>";print_r($subscription_list);
        $this->load->view('front_end/bypass_payment',array("user" => $userdata,"subscription_list" => $subscription_list));
    }
    
    public function signupforsubdomain() {
        $subdomain_name = (explode(".",HOST_NAME));
        $getuser = $this->Request_model->getuserinfobydomain(HOST_NAME);
        if (empty($getuser)) {
            redirect("https://".DOMAIN_NAME); 
        }
        if(!isset($_SERVER["HTTPS"]) && $getuser[0]['ssl_or_not'] == 1){
            redirect("https://".HOST_NAME); 
        }
        if (!empty($_POST)) {
            
            $is_coupon_valid = isset($_POST['couponinserted_ornot'])?$_POST['couponinserted_ornot']:'';
            $plan_type = isset($_POST['plan_type_name'])?$_POST['plan_type_name']:'';
            if(isset($_POST['first_name']) && $_POST['first_name']==""){
                $this->session->set_flashdata('message_error', "Please enter First Name!", 5);
                redirect(base_url() . "user_signup");
            }
            if(isset($_POST['last_name']) && $_POST['last_name']==""){
                $this->session->set_flashdata('message_error', "Please enter Last Name!", 5);
                redirect(base_url() . "user_signup");
            }
            if(isset($_POST['phone_number']) && $_POST['phone_number']==""){
                $this->session->set_flashdata('message_error', "Please enter Phone Number!", 5);
                redirect(base_url() . "user_signup");
            }
            if(isset($_POST['email']) && $_POST['email']==""){
                $this->session->set_flashdata('message_error', "Please enter Email !", 5);
                redirect(base_url() . "user_signup");
            }
            if(isset($_POST['password']) && $_POST['password']==""){
                $this->session->set_flashdata('message_error', "Please enter Password !", 5);
                redirect(base_url() . "user_signup");
            }
            $checkemail = $this->Request_model->getuserbyemail($_POST['email']);
            if (!empty($checkemail)) {
                $this->session->set_flashdata('message_error', "There is already an account associated with this email.", 5);
                redirect(base_url() . "user_signup");
            }
            
            $data = $this->Admin_model->getuser_data($getuser[0]['user_id']);
            $clientslot = $this->customfunctions->checkclientaddornot($data);
            $subscription_plan_id = $_POST['plan_name'];
            $clntsubscriptions = $this->Clients_model->getsubsbaseduserbyplanid($subscription_plan_id);
            $inprogress_request = $clntsubscriptions[0]['global_inprogress_request'];
            if($clntsubscriptions[0]["shared_user_type"] == 0){
                $inprogressrequest = $inprogress_request*$getuser[0]['no_of_assign_user'];
            }else{
                $inprogressrequest = $inprogress_request;
            }
            $canaddreq = $clientslot['addedsbrqcount']+$inprogressrequest;
//            echo "<pre>";print_R($clientslot);exit;
            if($plan_type != "one_time"){
                $this->checkuserslotforadduser($clientslot,$clntsubscriptions[0]["shared_user_type"],$canaddreq,$getuser);
            }
            $this->Stripe->setapikey($getuser[0]['stripe_api_key']);
            if($is_coupon_valid == 0){
                 $stripe_customer = $this->Stripe->createnewCustomer($_POST['email']);
            }else{
                $stripe_customer = $this->Stripe->createnewCustomer($_POST['email'], $_POST['Discount']);
            }
            if (!$stripe_customer['status']) {
                $this->session->set_flashdata('message_error', $stripe_customer['message'], 5);
                redirect(base_url() . "user_signup");
            }
            $customer_id = $stripe_customer['data']['customer_id'];
            $card_number = trim($_POST['card_number']);
            $expir_date = explode("/", $_POST['expir_date']);
            $expiry_month = $expir_date[0];
            $expiry_year = $expir_date[1];
            $cvc = $_POST['cvc'];

            $stripe_card = $this->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);
            if (!$stripe_card['status']):
                $this->session->set_flashdata('message_error', $stripe_card['message'], 5);
                redirect(base_url() . "user_signup");
            endif;
            $discounted_price = $_POST['selected_price'];
            $actual_plan_price = $_POST['final_plan_price'];
            if($plan_type == "one_time"){
                $invoice_created = $this->Stripe->create_invoice($customer_id,0,$actual_plan_price);
                if($invoice_created['status'] != true){
                 $output['message'] = $invoice_created['message'];
                 $output['status'] = $invoice_created['status'];
                 $this->session->set_flashdata('message_error', $invoice_created['message'], 5);
                    redirect(base_url() . "user_signup");
                }else{
                    $customer['total_requests'] = $inprogress_request;
                    $customer['total_inprogress_req'] = 0;    
                    $customer['total_active_req'] = 0;
                    $customer['payment_status'] = 1;
                    $customer['billing_start_date'] = date('Y-m-d H:i:s');
                }
            }else{
                $stripe_subscription = '';
                if ($subscription_plan_id) {
                    $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($customer_id, $subscription_plan_id, $inprogress_request);
                    $current_date = date('Y-m-d H:i:s',$stripe_subscription['data']['period_start']);
                    $billing_expire = date('Y-m-d H:i:s',$stripe_subscription['data']['period_end']);
                }
                if (!$stripe_subscription['status']) {
                    $this->session->set_flashdata('message_error', $stripe_subscription['message'], 5);
                    redirect(base_url() . "user_signup");
                }
                $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);
                $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
                $customer['billing_start_date'] = $current_date;
                $customer['billing_end_date'] = $billing_expire;
                $customer['plan_interval'] = $plandetails['interval'];
                $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
                $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
                $customer['total_inprogress_req'] = $inprogress_request;
                $customer['total_active_req'] = $inprogress_request * TOTAL_ACTIVE_REQUEST;
            }
            
            $user_timezone = $this->myfunctions->gettimezone();
            $state = $user_timezone->regionName;
            $timezone = $user_timezone->timezone;
            
            if($getuser[0]["default_active"] == 1){
                $customer['is_active'] = 1;
            }else{
                $customer['is_active'] = 0;
            }
            
            $customer['plan_name'] = $subscription_plan_id;
            $customer['plan_amount'] = $discounted_price;
            $customer['display_plan_name'] = $clntsubscriptions[0]['plan_name'];
            $customer['customer_id'] = $customer_id;
            $customer['email'] = $_POST['email'];
            $customer['first_name'] = $_POST['first_name'];
            $customer['parent_id'] = $getuser[0]['customer_id'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['phone'] = $_POST['phone_number'];
            $customer['new_password'] = md5($_POST['password']);
            $customer['role'] = "customer";
            $customer['created'] = date("Y:m:d H:i:s");
            $customer['modified'] = date("Y:m:d H:i:s");
            $customer['timezone'] = $timezone;
            $customer['state'] = $state;
            $customer['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $customer['last_update'] = date("Y:m:d H:i:s");
            $customer['user_flag'] = 'client';
            $customer['requests_type'] = $plan_type;
            
            $id = $this->Welcome_model->insert_data("users", $customer);
            if ($id) {
                $role['customer_id'] = $id; 
                $role['add_requests'] = 1; 
                $role['delete_req'] = 1;
                $role['approve/revision_requests'] = 1;
                $role['download_file'] = 1;
                $role['comment_on_req'] = 1;
                $role['billing_module'] = 0;
                $role['add_brand_pro'] = 1;
                $role['brand_profile_access'] = 0;
                $role['manage_priorities'] = 1;
                $role['view_only'] = 0;
                $role_id = $this->Welcome_model->insert_data("user_permissions", $role);
                if($role_id && $getuser[0]['brand_ids']){
                    $brandIDs = explode(',',$getuser[0]['brand_ids']);
                    foreach($brandIDs as $val){
                            $role1['user_id'] = $role['customer_id'];
                            $role1['brand_id'] = $val;
                            $role1['created'] = date("Y:m:d H:i:s");
                            $this->Welcome_model->insert_data("user_brand_profiles", $role1);
                        }
                }
                
                /** extra user info **/
//                $user_info['user_id'] = $id;
//                $user_info["is_shared_user"] = $getuser[0]["is_shared_user"];
//                $user_info['created'] = date("Y:m:d H:i:s");
//                $this->Welcome_model->insert_data("users_info", $user_info); 
                /** end extra user info **/
                
                //send notification to new users
                $this->myfunctions->show_notifications(0,$id,"You have received a new signup request from your white label graphicszoo account.","customer/setting-view?status=client#management",$getuser[0]['customer_id']);
                $array = array('CUSTOMER_NAME' => $customer['first_name'],
                    'ACTIVATE_URL' => base_url().'customer/setting-view#management');
                if($getuser[0]["default_active"] == 1){
                  $this->myfunctions->send_email_to_users_by_template($getuser[0]['customer_id'],'notify_agency_admin_after_register_user',$array,$getuser[0]['useremail']);
                }else{
                  $this->myfunctions->send_email_to_users_by_template($getuser[0]['customer_id'],'email_to_active_register_user',$array,$getuser[0]['useremail']);
                }
                /* set user session data for login */
                $this->session->set_userdata('user_id', $id);
                $this->session->set_userdata('timezone', $timezone);
                $this->session->set_userdata('role', $customer['role']);
                $this->session->set_userdata('first_name', $customer['first_name']);
                $this->session->set_userdata('last_name', $customer['last_name']);
                $this->session->set_userdata('email', $customer['email']);
                $this->session->set_userdata('tour', 0);
                $this->session->set_userdata('qa_id', $customer['qa_id']);
                $array1 = array('CUSTOMER_NAME' => $customer['first_name'],
                    'PROJECT_LINK' => base_url() . "customer/request/add_new_request");
                if($getuser[0]["default_active"] == 1){
                    $this->myfunctions->send_email_to_users_by_template($getuser[0]['customer_id'],'welcome_email_to_active_user_from_subdomain',$array1,$customer['email']);
                    $this->session->set_flashdata('message_success', "Account created successfully.", 5);
                    redirect(base_url() . "customer/request/design_request");
                }else{
                    $this->myfunctions->send_email_to_users_by_template($getuser[0]['customer_id'],'welcome_email_to_user_from_subdomain',$array1,$customer['email']);
                    $this->session->set_flashdata('message_success', "Account created successfully, your account request is under approval and we will get back to you soon.", 5);
                    redirect(base_url() . "user_signup");
                }
            } else {
                $this->session->set_flashdata('message_error', "User can not created successfully ..!", 5);
                redirect(base_url() . "user_signup");
            }
          } 
        $subscription_list = $this->Clients_model->getsubscriptionbaseduser($getuser[0]['user_id'],"",1,"",$getuser[0]["online_payment"]); 
        $policy_array = array("show_term_privacy" => $getuser[0]['show_term_privacy'],
            "privacy_policy" => $getuser[0]['privacy_policy'],
            "term_conditions" => $getuser[0]['term_conditions']
            );
//        echo "<pre>";print_r($subscription_list);
        $this->load->view('front_end/subdomain_signup',array("policy_array" => $policy_array,"subscription_list" => $subscription_list,"agency_livechat_script" => $getuser[0]['live_script'],"user_id" => $getuser[0]['user_id'],"online_payment" => $getuser[0]["online_payment"]));
        
    } 
    public function saas_signup(){
        
        if (!empty($_POST)) {
            $output = array();
            $rendomly_qa_id = $this->Admin_model->getqa_id();
            $is_coupon_valid = isset($_POST['couponinserted_ornot'])?$_POST['couponinserted_ornot']:'';
            if(isset($_POST['first_name']) && $_POST['first_name']==""){
                $output['message'] = "Please enter First Name!";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            if(isset($_POST['last_name']) && $_POST['last_name']==""){
                $output['message'] = "Please enter Last Name!";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            if(isset($_POST['phone_number']) && $_POST['phone_number']==""){
                $output['message'] = "Please enter Phone Number!";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            if(isset($_POST['email']) && $_POST['email']==""){
                $output['message'] = "Please enter Email !";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            if(isset($_POST['password']) && $_POST['password']==""){
                $output['message'] = "Please enter Password !";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            
            if(isset($_POST['card_number']) && $_POST['card_number']==""){
                $output['message'] = "Please enter Card Number !";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            if(isset($_POST['expir_date']) && $_POST['expir_date']==""){
                $output['message'] = "Please enter Expire Date !";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            if(isset($_POST['cvc']) && $_POST['cvc']==""){
                $output['message'] = "Please enter CVC Number !";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            if(isset($_POST['term']) && $_POST['term']==""){
                $output['message'] = "Please accept the term and condition !";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            if(isset($_POST['state']) && $_POST['state']==""){
                $output['message'] = "Please select state !";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            
            $checkemail = $this->Request_model->getuserbyemail($_POST['email']);
            if (!empty($checkemail)) {
                $output['message'] = "There is already an account associated with this email.";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            
            if($is_coupon_valid == 0){
                $stripe_customer = $this->Stripe->createnewCustomer($_POST['email']);
            }else{
                $stripe_customer = $this->Stripe->createnewCustomer($_POST['email'], $_POST['Discount']);
            }
            $expir_date = explode("/", $_POST['expir_date']);
            if (!$stripe_customer['status']) {
                $output['message'] = $stripe_customer['message'];
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            $customer_id = $stripe_customer['data']['customer_id'];
            $card_number = trim($_POST['card_number']);
            $expiry_month = $expir_date[0];
            $expiry_year = $expir_date[1];
            $cvc = $_POST['cvc'];

            $stripe_card = $this->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);
            if (!$stripe_card['status']):
                $output['message'] = $stripe_card['message'];
                $output['status'] = 0;
                echo json_encode($output);exit;
            endif;

            $subscription_plan_id = $_POST['plan_name'];
            $actual_plan_price = $_POST['plan_price'];

            $stripe_subscription = '';
            if ($subscription_plan_id) {
                $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($customer_id, $subscription_plan_id);
                $current_date = date('Y-m-d H:i:s',$stripe_subscription['data']['period_start']);
                $billing_expire = date('Y-m-d H:i:s',$stripe_subscription['data']['period_end']);
            }

            if (!$stripe_subscription['status']) {
                $output['message'] = $stripe_card['message'];
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            
            /* get timezone */
                $ip = $_SERVER['REMOTE_ADDR'];
                $curl = curl_init();
                //122.160.138.23
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://ip-api.com/json/" . $ip,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache",
                        "postman-token: 92b0963f-d661-849d-9ca9-734180411906"
                    ),
                ));

                $ip_detl = curl_exec($curl);
                curl_error($curl);

                curl_close($curl);

                $ipInfo = json_decode($ip_detl);
                if($_POST['state'] != ""){
                    $state = $_POST['state'];
                }else{
                    $state = $ipInfo->regionName;
                }
                $timezone = $ipInfo->timezone;
                /* timezone ends */
            
            $customer['customer_id'] = $customer_id;
            $customer['is_active'] = 1;
            $customer['email'] = $_POST['email'];
            $customer['first_name'] = $_POST['first_name'];
            $customer['last_name'] = $_POST['last_name'];
            $customer['new_password'] = md5($_POST['password']);
            $customer['role'] = "customer";
            $customer['created'] = date("Y:m:d H:i:s");
            $customer['modified'] = date("Y:m:d H:i:s");
            $customer['timezone'] = $timezone;
            $customer['state'] = $state;
            $customer['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $customer['last_update'] = date("Y:m:d H:i:s");
            $customer['created'] = date("Y:m:d H:i:s");
            $customer['modified'] = date("Y:m:d H:i:s");
            $customer['is_logged_in'] = 0;
            $customer['is_saas'] = 1;
            $customer['qa_id'] = $rendomly_qa_id[0]['id'];
            $customer['YOUR_SOURCE_FIELD'] = isset($_POST['YOUR_SOURCE_FIELD'])?$_POST['YOUR_SOURCE_FIELD']:'';
            $customer['YOUR_MEDIUM_FIELD'] = isset($_POST['YOUR_MEDIUM_FIELD'])?$_POST['YOUR_MEDIUM_FIELD']:'';
            $customer['YOUR_CAMPAIGN_FIELD'] = isset($_POST['YOUR_CAMPAIGN_FIELD'])?$_POST['YOUR_CAMPAIGN_FIELD']:'';
            $customer['YOUR_CONTENT_FIELD'] = isset($_POST['YOUR_CONTENT_FIELD'])?$_POST['YOUR_CONTENT_FIELD']:'';
            $customer['YOUR_TERM_FIELD'] = isset($_POST['YOUR_TERM_FIELD'])?$_POST['YOUR_TERM_FIELD']:'';
            $customer['VISITS'] =  isset($_POST['VISITS'])?$_POST['VISITS']:'';
            if (empty($checkemail)) {
             $id = $this->Welcome_model->insert_data("users", $customer);
             if($id){
                $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);
                $s_customer['user_id'] = $id;
                $s_customer['subscription_id'] = $stripe_subscription['data']['subscription_id'];
                $s_customer['plan_id'] = $subscription_plan_id;
                $s_customer['plan_name'] = $plandetails['name'];
                $s_customer['plan_amount'] = $actual_plan_price;
                $s_customer['billing_start_date'] = $current_date;
                $s_customer['billing_end_date'] = $billing_expire;
                $s_customer['created'] = date("Y:m:d H:i:s");
                $s_customer['modified'] = date("Y:m:d H:i:s");
                $this->Welcome_model->insert_data("s_users_setting", $s_customer);
                
                
                
                $ag_customer['user_id'] = $id;
                $ag_customer['default_active'] = 1;
                $ag_customer['show_signup'] = 1;
                $ag_customer['online_payment'] = 0;
                $ag_customer['shared_designer'] = 0;
                $ag_customer['show_billing'] = 1;
                $ag_customer['is_manual'] = 1;
                $ag_customer['default_status'] = "active";
                $ag_customer['is_approval_from_qa'] = 0;
                $ag_customer['created'] = date("Y:m:d H:i:s");
                $ag_customer['modified'] = date("Y:m:d H:i:s");
                $this->Welcome_model->insert_data("agency_user_options", $ag_customer);
                
                /* set user session data for login */
                $this->session->set_userdata('user_id', $id);
                $this->session->set_userdata('is_saas', 1);
                $this->session->set_userdata('timezone', $timezone);
                $this->session->set_userdata('role', $customer['role']);
                $this->session->set_userdata('first_name', $customer['first_name']);
                $this->session->set_userdata('last_name', $customer['last_name']);
                $this->session->set_userdata('email', $customer['email']);
                $this->session->set_userdata('tour', 0);
                $this->session->set_userdata('qa_id', $customer['qa_id']);
//                $array = array('CUSTOMER_NAME' => $customer['first_name'],
//                    'PROJECT_LINK' => base_url().'customer/request/add_new_request');
                $output['message'] = "User created successfully and subscribed successfully..!";
                $output['status'] = 1;
                echo json_encode($output);exit;
             }else {
                $output['message'] = "Error occurred while creating new account, please verify the info and try again!";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
            }else{
                $output['message'] = "There is already an account associated with this email.";
                $output['status'] = 0;
                echo json_encode($output);exit;
            }
        }
        $subscription_list = $this->Welcome_model->getsaas_subscriptions();
        $this->load->view('front_end/saas_signup',array("subscription_list" => $subscription_list));  
    }
    
    public function getsaas_subscriptions(){
        $planid = isset($_POST['planid'])?$_POST['planid']:"";
        $plan_data = $this->Welcome_model->getsaas_subscriptions($planid);
        echo json_encode($plan_data);exit;
    }
    
    public function checkuserslotforadduser($clientslot,$shared_user_type,$canaddreq,$getuser) {
        if ($clientslot['main_dedicated_designr'] == 1 && $shared_user_type == 0) {
            $msg = "You cann't add dedicated designer, you can add only shared user.";
            $null_slot = 1;
        } else if ($canaddreq > $clientslot['main_inprogress_req']) {
            $msg = "An error occured. Please contract admin/support for help.";
            $null_slot = 1;
        }
        if ($null_slot == 1) {
              $this->session->set_flashdata('message_error', $msg,5);
              redirect(base_url() . "user_signup");  
        }else{
            return 1;
        }
    }
    public function checkuserplaniscancel($userdata="") {
       if($userdata[0]['parent_id'] != 0){
            $mainuser = $userdata[0]['parent_id'];
            $customer_data = $this->Admin_model->getuser_data($mainuser,'id,is_cancel_subscription,plan_name');
        }else{
            $customer_data = $userdata;
        }
        if($customer_data[0]['is_cancel_subscription'] == 1){
            return true;
        } 
    }
    
    public function accessdenied($userdata="") {
        $this->load->view('access_denied');
    }
    
    public function getfraturesofplan() {
        $planid = isset($_POST['planid'])?$_POST['planid']:"";
        if($planid != ""){
            $plan_data = $this->Request_model->getsubscriptionlistbyplanid($planid,"features,plan_type_name,plan_type,global_inprogress_request");
            if($plan_data[0]['plan_type_name'] == 'agency'){
                $plan_data[0]['is_agency'] = 1;
            }
        }
       // echo "<pre>";print_r($plan_data);exit;
        echo json_encode($plan_data);exit;
    }
    
    public function getplantierprices() {
        $prcs = array();
        $type = isset($_POST['type'])?$_POST['type']:"";
        if($type == 'yearly'){
            $plan_prcs = $this->load->get_var('annual_subscription_data');
            $prcs['business_price'] = $plan_prcs[ANNUAL_BUSINESS_PLAN];
            $prcs['company_price'] = $plan_prcs[ANNUAL_COMPANY_PLAN];
            $prcs['agency_price'] = $plan_prcs[ANNUAL_AGENCY_PLAN];
            
        }else{
            $plan_prcs = $this->load->get_var('subscription_data');
            $prcs['business_price'] = $plan_prcs[SUBSCRIPTION_MONTHLY];
            $prcs['company_price'] = $plan_prcs[COMPANY_PLAN];
            $prcs['agency_price'] = $plan_prcs[AGENCY_PLAN];
        }
        echo json_encode($prcs);exit;
    }
    
    public function getsubsbaseduser_byplanid() {
      $planid = isset($_POST['planid'])?$_POST['planid']:"";
      $user_id = isset($_POST['user_id'])?$_POST['user_id']:"";
      $plan_data = $this->Clients_model->getsubsbaseduserbyplanid($planid,"plan_id,features,plan_type,global_inprogress_request,plan_type_name,apply_coupon",$user_id);
      echo json_encode($plan_data);exit;
    }
    
    
    function Capture_cart_entry_moments() {
        $filed_name = $this->input->post("filed_name");
        $filed_value = $this->input->post("filed_value"); 
        $sess = $this->input->post("sess");
        $plan_fieldname = $this->input->post("plan_field");
        $planName = $this->input->post("plan");
        $ip = $this->input->post("ip");
        $from_page = $this->input->post("from_page");

        if ($filed_name == "card_number") {
            $var = $this->ccMasking($filed_value, $maskingCharacter = '*');
            $filed_value = $var;
        } else if ($filed_name == "cvc") {
            $var = $filed_value;
            $var = substr_replace($var, str_repeat("*", 3), 0, 3);
            $filed_value = $var;
        } else {
            $filed_value = $this->input->post("filed_value");
        }
        if ($sess == "0") {
            $checkIdExist = "0";
            $n = "created_at";
        } else {
            $n = "modified_at";
            $checkIdExist = $this->session->userdata('vistedid');
        }
        $data = array($filed_name => $filed_value, $plan_fieldname => $planName, "ip_address" => $ip, "from_page" => $from_page, $n => date("Y-m-d H:i:s"));
        $checkingForIp = $this->Welcome_model->select_data($select = "ip_address", $table = "cart", $where = array("ip_address" => $ip));

        if (!empty($checkIdExist) && $checkIdExist != "0") {
            $where = array("id" => $checkIdExist);
            $this->Welcome_model->update_data($table = "cart", $data, $where);
        } else {

            $insid = $this->Welcome_model->insert_data("cart", $data);
            $Sessdata = array("vistedid" => $insid);
            $this->session->set_userdata($Sessdata);
        }
    }

    function ccMasking($number, $maskingCharacter = '*') {
        return substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 8) . substr($number, -4);
    }
    
    
     public function cronset(){
        $req_id = $this->Request_model->getrequestsid();
//        echo "<pre/>";print_R($req_id);
        foreach($req_id as $id){
//            echo "<pre/>";print_R($id);exit;
            $draft_ids = $this->Request_model->getdraftsofreq($id['id']);
//            echo "<pre/>";print_R($draft_ids);exit;
            if($draft_ids['status'] == 'success'){
            $update = $this->Welcome_model->update_data("requests",array('count_customer_revision' => $draft_ids['count']),array('id' => $id['id']));
             if($update){
                 echo "updated".$id['id']."<br/>";
             }
            }else{
                echo "not updated".$id['id']."<br/>";
            }
        }
    }
    
    /****************Affiliate code****************/
     public function affiliate_signup(){
        $user_data = $user_aff_data = array();
        if (!empty($_POST)) {
            $randomkey = $this->myfunctions->random_sharekey();
//            echo "<pre/>";print_R($_POST);exit;
            $fullurl = base_url().'signup?referral='.$randomkey;
            $checkemail = $this->Request_model->getuserbyemail($_POST['email']);
            $keyexist = $this->Affiliated_model->IsAffiliated_keyexists($randomkey);
            if (!empty($checkemail)) {
                $this->session->set_flashdata('message_error', "There is already an account associated with this email.", 5);
                redirect(base_url() . "affiliate_signup");
            }elseif(!empty($keyexist)){
                $this->session->set_flashdata('message_error', "There is already the same key exists.", 5);
                redirect(base_url() . "affiliate_signup");
            }else{
            $user_data['first_name'] = (isset($_POST['first_name']) && $_POST['first_name'] != '') ? $_POST['first_name'] : '';
            $user_data['last_name'] = (isset($_POST['last_name']) && $_POST['last_name'] != '') ? $_POST['last_name'] : '';
            $user_data['email'] = (isset($_POST['email']) && $_POST['email'] != '') ? $_POST['email'] : '';
            $user_data['new_password'] = (isset($_POST['password']) && $_POST['password'] != '') ? md5($_POST['password']) : '';
            $user_data['role'] = 'customer';
            $user_data['is_affiliated'] = 2;
            $user_data['is_active'] = 1;
            $user_data['created'] = date("Y:m:d H:i:s");
            $insid = $this->Welcome_model->insert_data("users",$user_data);
            if($insid){
                $user_aff_data['user_id'] = $insid;
                $user_aff_data['shared_url'] = $fullurl;
                //$user_aff_data['shared_shorturl'] = isset($shortenurl)?$shortenurl:'';
                $user_aff_data['paypal_id'] = (isset($_POST['email']) && $_POST['email'] != '') ? $_POST['email'] : '';
                $user_aff_data['commision_type'] = (isset($_POST['commision_type']) && $_POST['commision_type'] != '') ? $_POST['commision_type'] : '';
                $user_aff_data['commision_fees'] = (isset($_POST['commision_fees']) && $_POST['commision_fees'] != '') ? $_POST['commision_fees'] : '';
                $user_aff_data['affiliated_key'] = $randomkey;
                $user_aff_data['created'] = date("Y:m:d H:i:s");
//                echo "<pre/>";print_R($user_aff_data);exit;
                $insaffid = $this->Welcome_model->insert_data("affiliate_user_info",$user_aff_data);
                if($insaffid){
                    $this->session->set_flashdata('message_success', "Affiliated user created successfully..!", 5);
                    redirect(base_url() . "affiliate_signup");
                }
            }
        }
        }
        $this->load->view('front_end/affiliate_signup');
    }
    
    public function affiliate(){
        $titlestatus = "affiliate";
        $this->load->view('front_end/headerNew', array('current_action' => 'affiliate', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/affiliate'); 
        $this->load->view('front_end/footerNew');
    }
    
    public function manageAffiliationHistory($user_id,$type,$signupid){
        //$dataaff = $this->Affiliated_model->captureAffiliatedfund($user_id);
        //$alreadycredited = $this->Affiliated_model->checkAlreadyCredit($dataaff[0]['signup_user_id'],$dataaff[0]['user_id']);
        $paid_amount = $this->Admin_model->getuser_data($signupid,'id,plan_amount');
        $comm_type = COMMISION_TYPE;
        $comm_fees = COMMISION_FEES;
        if($comm_type == 'percentage'){
          $amnt = (($comm_fees / 100) * $paid_amount[0]['plan_amount']);
        }
        //if($dataaff){
            $insert_aff_trans = array();
            $insert_aff_trans['user_id'] = $user_id;
            $insert_aff_trans['signup_user_id'] = $signupid;
            $insert_aff_trans['type'] = $type;
            $insert_aff_trans['amount'] = $amnt;
            $insert_aff_trans['created'] = date("Y:m:d H:i:s");
            $this->Welcome_model->insert_data("affiliate_transaction_history", $insert_aff_trans);
        //}
    }

    public function saas() {
        $titlestatus = "affiliare_program";
        $this->load->view('front_end/headerNew', array('id' => 'affiliate-marketing-page', 'titlestatus' => $titlestatus));
        $this->load->view('front_end/saas_landing_page');
        $this->load->view('front_end/footerNew');
    }
    
}
