<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class My404controller extends CI_Controller {

  function __construct() {
    parent::__construct();
     $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('Welcome_model');
        $this->load->model('Admin_model');
        $this->load->model('Request_model');
       
  }

  public function index() {
   //$this->uri->segment(1) is *original* controller segment before routing.
            $titlestatus = "";
            $role_id = isset($_SESSION['role'])?$_SESSION['role']:"";
  // If there's no match to existing controller, default to generic 404 page view.
  $this->load->view('front_end/header404');
//   $this->load->view('front_end/headerNew', array("role_id" => $role_id,'current_action' => 'home', 'id' => 'index_page', 'titlestatus' => $titlestatus, 'page' => '404'));
  $this->load->view('errors/html/error_404');
  $this->load->view('front_end/footerNew');

  }
}