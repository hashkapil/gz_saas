<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Affiliate extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/indexd
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('Myfunctions');
        $this->load->library('Customfunctions');
        $this->load->model('Welcome_model');
        $this->load->model('Affiliated_model');
        $this->load->model('Request_model');
        $this->load->model('Clients_model');
        $this->load->model('File_management');
        $this->load->helper('url');
        $this->load->library('bitly');
    }

  public  function index(){
      $this->myfunctions->checkloginuser("customer");
        $login_user_id = $this->load->get_var('login_user_id'); 
        $user_data = $this->Welcome_model->getuserbyid($login_user_id);
        $aff_info = $this->Affiliated_model->IsAffiliated_keyexists('',$login_user_id);
        $frstdaycurrnt = date('Y-m-01');
        $currntDate = date("Y-m-d");
        $day = date('w');
        $week_start = date('Y-m-d', strtotime('-'.$day.' days'));
        $todaycomm = $this->Affiliated_model->get_totalAffiliatedCommision($login_user_id,$currntDate);
        $weekcomm = $this->Affiliated_model->get_monthlyAffiliatedCommision($login_user_id,$week_start,$currntDate);
        $monthcomm = $this->Affiliated_model->get_monthlyAffiliatedCommision($login_user_id,$frstdaycurrnt,$currntDate);
        $totalavailablecomm = $this->Affiliated_model->get_totalAffiliatedCommision($login_user_id,'',true);
        $totalcomm = $this->Affiliated_model->get_totalAffiliatedCommision($login_user_id);
        $payment_history = $this->Affiliated_model->paymentHistory($login_user_id);
        $totaltodayusers = $this->Affiliated_model->getRecordsofaffiliatedsignup($login_user_id,$currntDate);
        $totalweeklyusers = $this->Affiliated_model->getRecordsofaffiliatedsignup($login_user_id,'',$week_start,$currntDate);
        $totalmonthusers = $this->Affiliated_model->getRecordsofaffiliatedsignup($login_user_id,'',$frstdaycurrnt,$currntDate);
        $totalusers = $this->Affiliated_model->getRecordsofaffiliatedsignup($login_user_id);
        $paymnt_his = array();
        for ($i = 0; $i < sizeof($payment_history); $i++) {
            $paymnt_his[$i]['name'] = $payment_history[$i]['fname'].' '.$payment_history[$i]['lname'];
            $paymnt_his[$i]['email'] = $payment_history[$i]['email'];
            $paymnt_his[$i]['com_amnt'] = $payment_history[$i]['com_amnt'];
            $paymnt_his[$i]['type'] = ucfirst($payment_history[$i]['type']);
            $paymnt_his[$i]['signup_date'] = $this->onlytimezone($payment_history[$i]['signup_date']);
            $paymnt_his[$i]['debited_date'] = $this->onlytimezone($payment_history[$i]['debited_date']);
        }
        $this->load->view('customer/customer_header_1');
        $this->load->view('customer/affiliate',array('aff_info' => $aff_info,'totalcomm' => $totalcomm,
        'payment_history' => $paymnt_his,'todaycomm' => $todaycomm,'monthcomm' => $monthcomm,'totalavailablecomm' => $totalavailablecomm,
        'weekcomm' => $weekcomm,'totaltodayusers' => $totaltodayusers,'totalweeklyusers'=> $totalweeklyusers,
        'totalmonthusers' => $totalmonthusers,'totalusers' => $totalusers));
        $this->load->view('customer/footer_customer');
  }
  
  public function BecomeaffiliatedUser(){
      $this->myfunctions->checkloginuser("customer");
      $data = array();
      $aff_key = $this->myfunctions->random_password();
      $fullurl = base_url().'signup?referral='.$aff_key;
      $shortenurl = $this->bitly->shorten($fullurl);
      $login_user_id = $this->load->get_var('login_user_id'); 
      $user_data = $this->Welcome_model->getuserbyid($login_user_id);
      $update_user = $this->Welcome_model->update_data("users", array("is_affiliated" => 1), array("id" => $user_data[0]['id']));
      if($update_user){
           $data['user_id'] = $login_user_id;
           $data['affiliated_key'] = $aff_key;
           $data['paypal_id'] = $user_data[0]['email'];
           $data['shared_url'] = $fullurl;
           //$data['shared_shorturl'] = isset($shortenurl)?$shortenurl:'';
           $data['commision_type'] = COMMISION_TYPE;
           $data['commision_fees'] = COMMISION_FEES;
           $data['created'] = date("Y:m:d H:i:s");
           $id = $this->Welcome_model->insert_data("affiliate_user_info", $data);
           $this->session->set_flashdata('message_success','Congratulations! you have become the affiliate member.', 5);
           redirect(base_url() . "customer/affiliate");
      }else{
           $this->session->set_flashdata('message_error','Something went wrong,Try again!', 5);
           redirect(base_url() . "customer/affiliate");
      }
         
  }
  
  public function UpdatePaypalID(){
      $this->myfunctions->checkloginuser("customer");
      $login_user_id = $this->load->get_var('login_user_id'); 
      $paypal_id = isset($_POST['paypal_id']) ? $_POST['paypal_id'] : '';
      $update_user = $this->Welcome_model->update_data("affiliate_user_info", array("paypal_id" => $paypal_id), array("user_id" => $login_user_id));
      if($update_user){
         $this->session->set_flashdata('message_success','Paypal id updated successfully!', 5);
           redirect(base_url() . "customer/affiliate#payment_settings"); 
      }else{
          $this->session->set_flashdata('message_error','Something went wrong,Try again!', 5);
           redirect(base_url() . "customer/affiliate#payment_settings"); 
      }
  }
  
   public function generateshortenUrl(){
       $this->myfunctions->checkloginuser("customer");
      $output = array();
      $fullURL = isset($_POST['full_url'])?$_POST['full_url']:'';
      $shorturl = $this->bitly->shorten($fullURL);
      $update_aff = $this->Welcome_model->update_data("affiliate_user_info", array("shared_shorturl" => $shorturl), array("shared_url" => $fullURL));
      if($update_aff){
         $output['status'] = 'success'; 
         $output['short_url'] = $shorturl; 
      }else{
         $output['status'] = 'fail';  
      }
      echo json_encode($output);
  }
  
   public function onlytimezone($date_zone) {
        $nextdate = $this->myfunctions->onlytimezoneforall($date_zone);
        return $nextdate;
    }
  
}