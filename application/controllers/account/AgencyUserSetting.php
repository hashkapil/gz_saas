<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AgencyUserSetting extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/indexd
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('account/S_myfunctions');
        $this->load->helper('form');
        $this->load->library('s3_upload');
        $this->load->helper('url');
        $this->load->model('account/S_request_model');
         $this->load->model('Category_model');
        $this->load->model('Welcome_model');
//        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('account/S_admin_model');
        $this->load->model('Stripe');
        $this->load->library('zip');
        $config_email = $this->config->item('email_smtp');
        define('MAIL_SENDER', $config_email['sender']);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
    }
    
    public function index(){ 
//        die("hello");
//        $this->myfunctions->checkloginuser("customer");
        $config_email = $this->config->item('email_smtp');
        $login_user_id = $this->load->get_var('login_user_id'); 
        $main_user = $this->load->get_var('main_user');
        $profile_data = $this->S_admin_model->getuser_data($main_user);
//        echo "<pre/>";print_R($profile_data);exit;
        $getsubscriptionplan = $this->S_request_model->getsubscriptionlistbyplanid($profile_data[0]['plan_name']);
        if($profile_data[0]['is_saas'] != 1){
            redirect(base_url().'account/setting-view'); 
        }
        $changeinfo = $this->S_request_model->getAgencyuserinfo($main_user,'');
        $getemailofuser = $this->S_admin_model->get_template_byid($main_user);
        $useroptions_data = $this->S_request_model->getAgencyuserinfo($main_user,'');
        foreach ($getemailofuser as $getmail_template) {
              $email_slug[] =  $getmail_template['email_slug']; 
        } 
        $getemailofslug = $this->S_admin_model->get_template_byid('',$email_slug);
        $emailslist = array_merge($getemailofuser,$getemailofslug);
        foreach ($emailslist as $email_header_footer){
            if($email_header_footer['email_slug'] == 'email_header'){
                $email_header_footer_sec['header_part_id'] = $email_header_footer['id'];
               $email_header_footer_sec['header_part'] = $email_header_footer['email_text'];
            }elseif($email_header_footer['email_slug'] == 'email_footer'){
              $email_header_footer_sec['footer_part_id'] = $email_header_footer['id'];
              $email_header_footer_sec['footer_part'] = $email_header_footer['email_text'];
            }
            
             if($email_header_footer['email_slug'] != 'email_header' && $email_header_footer['email_slug'] != 'email_footer'){
                 $emailslist_temps[] = $email_header_footer;
        }
        }
        
        $data = $profile_data;
        $subusers = $this->S_admin_model->sumallsubuser_requestscount($main_user);
        $main_active_req = $data[0]['total_active_req'];
        $main_inprogress_req = $data[0]['total_inprogress_req'];
        $subtotal_active_req = $subusers['total_active_req'];
        $subtotal_inprogress_req = $subusers['total_inprogress_req'];
        $checksubuser_requests = array('main_active_req' => $main_active_req,
                                'main_inprogress_req' => $main_inprogress_req,
                                'subtotal_active_req' => isset($subtotal_active_req)?$subtotal_active_req:0,
                                'subtotal_inprogress_req' => isset($subtotal_inprogress_req)?$subtotal_inprogress_req:0);
        
       $customer_data = $this->S_request_model->getAllsubUsers($main_user,"client");
       foreach ($customer_data as $userskey => $usersvalue) {
           $customer_data[$userskey]['profile_picture'] = $this->customfunctions->getprofileimageurl($usersvalue['profile_picture']); 
           $customer_data[$userskey]['active'] = $this->S_request_model->getAllrequestscountofSubuser($main_user,$usersvalue['id'],array('active','disapprove','checkforapprove','hold'));
           $customer_data[$userskey]['inqueue'] = $this->S_request_model->getAllrequestscountofSubuser($main_user,$usersvalue['id'],array('assign'));
           $customer_data[$userskey]['draft'] = $this->S_request_model->getAllrequestscountofSubuser($main_user,$usersvalue['id'],array('draft'));
           $customer_data[$userskey]['complete'] = $this->S_request_model->getAllrequestscountofSubuser($main_user,$usersvalue['id'],array('approved'));
           $customer_data[$userskey]['cancel'] = $this->S_request_model->getAllrequestscountofSubuser($main_user,$usersvalue['id'],array('cancel'));
           $customer_data[$userskey]['total_req'] = ($customer_data[$userskey]['active'] + $customer_data[$userskey]['inqueue'] +  $customer_data[$userskey]['draft'] + $customer_data[$userskey]['complete'] + $customer_data[$userskey]['cancel']);
       }
       $brandprofile = $this->S_request_model->get_brand_profile_by_user_id($main_user);
       $this->load->view('account/customer_header_1', array("profile" => $profile_data));
       $this->load->view('account/agency_user/setting',array('profile'=> $profile_data,'changeinfo' => $changeinfo,'email_header_footer_sec' => $email_header_footer_sec,'emailslist' => $emailslist_temps,'brandprofile' => $brandprofile,'useroptions_data' => $useroptions_data,"clients_data" => $customer_data,'checksubuser_requests' => $checksubuser_requests,'config_email' => $config_email)); 
       $this->load->view('account/footer_customer');
    }
    
    public function change_agency_user_general_setting(){
         $result = array();
         $domain_name = isset($_POST['domain_name'])?$_POST['domain_name']:'';
         $sub_domain_name = isset($_POST['sub_domain_name'])?$_POST['sub_domain_name']:'';
         $domain_or_subdomain = (isset($_POST['slct_domain_subdomain']) && $_POST['slct_domain_subdomain'] == "on")?1:0;
         $login_user_id = $this->load->get_var('main_user');
         $userdata = $this->load->get_var('login_user_data');
         $useroptions_data = $this->S_request_model->getAgencyuserinfo($login_user_id,'');
         $email_headers = $this->S_admin_model->get_template_byslug_and_userid('email_header',$login_user_id);
         if($domain_or_subdomain == 1){
            $fstchr = substr($domain_name, 0, 4);
             if($fstchr == 'http'){
              $this->session->set_flashdata('message_error', "Please enter valid domain name!",5);
               redirect(base_url().'account/user_setting#general');   
             }
            
            $dm_name = $domain_name;
            if($useroptions_data[0]['domain_name'] != $dm_name || $useroptions_data[0]['ssl_or_not'] == 0){
                $ssl = 0;
            }else{
                $ssl = 1;
            }
         }else{
            $dm_name = $sub_domain_name.'.'.MAIN_DOMAIN_NAME;
            $ssl = 1;
         }
         if($dm_name != ""){
            $validdomain = $this->is_valid_domain_name($dm_name);
            if($validdomain != 1){
               $this->session->set_flashdata('message_error', "Please enter valid domain name!",5);
               redirect(base_url().'account/user_setting#general');
            }
         }
         $checkdomainexist = $this->S_request_model->getAgencyuserinfo($login_user_id,$dm_name);
         if ($_FILES['profile']['name'] != "") {
             
            if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_USER_LOGO . $login_user_id)) {
                $data = mkdir('./'.FS_UPLOAD_PUBLIC_UPLOADS_USER_LOGO . $login_user_id, 0777, TRUE);
            }
            $file = pathinfo($_FILES['profile']['name']);
            $filename = str_replace(" ","_",$file['filename']);
            $s3_file = $filename . '-' . rand(1000, 1) . '.' . $file['extension'];
            $config = array(
                'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_USER_LOGO . $login_user_id.'/',
                'allowed_types' => array("jpg", "jpeg", "png", "gif","svg"),
                'file_name' => $s3_file
            );
            $check = $this->myfunctions->CustomFileUpload($config, 'profile');
            if ($check['status'] == 1) {
                $user_options = array(
                    'logo' => $s3_file,
                );
                
                $email_text = '<div class="header_section" style="padding:25px">
                 <img class="header_img" style="max-height: 100px;max-width:225px;text-align: center;width:auto;height:auto;" src="'.FS_PATH_PUBLIC_UPLOADS_USER_LOGO . $login_user_id.'/'.$s3_file.'"></div>';
                if($email_headers[0]['user_id'] == 0){
                $updatearray = array('email_name' => $email_headers[0]['email_name'],
                    'email_slug' => $email_headers[0]['email_slug'],
                    'email_subject' => $email_headers[0]['email_subject'],
                    'email_text' => $email_text,
                    'email_description' => $email_headers[0]['email_description'],
                    'user_id' => $login_user_id);
                $update_email_logo = $this->Welcome_model->insert_data("email_template", $updatearray);
                }else{
                   $updatearray = array('email_text' => $email_text);
                   $update_email_logo = $this->Welcome_model->update_data("email_template", $updatearray, array("user_id" => $login_user_id,"email_slug" => $email_headers[0]['email_slug']));
                }
            }else{
                $this->session->set_flashdata('message_error', '<strong>Logo:</strong>  '.$check['msg'],5);
                redirect(base_url().'account/user_setting#general');
            }
        }
        if ($_FILES['favicon']['name'] != "") {
             
            if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_USER_LOGO . $login_user_id.'/favicon')) {
                $data = mkdir('./'.FS_UPLOAD_PUBLIC_UPLOADS_USER_LOGO . $login_user_id.'/favicon', 0777, TRUE);
            }
            $favicon_file = pathinfo($_FILES['favicon']['name']);
            $fav_filename = str_replace(" ","_",$favicon_file['filename']);
            $s3_favfile = $fav_filename . '-' . rand(1000, 1) . '.' . $favicon_file['extension'];
            $config = array(
                'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_USER_LOGO . $login_user_id.'/favicon/',
                'allowed_types' => array("ico","png"),
                'file_name' => $s3_favfile
            );
            $check_favfile = $this->myfunctions->CustomFileUpload($config, 'favicon');
            if ($check_favfile['status'] == 1) {
                $user_options['favicon'] = $s3_favfile;
            }else{
                $this->session->set_flashdata('message_error', '<strong>Favicon:</strong>  '.$check_favfile['msg'],5);
                redirect(base_url().'account/user_setting#general');
            }
        }
            $user_options['domain_name'] = $dm_name;
            $user_options['is_main_domain'] = $domain_or_subdomain;
            $user_options['ssl_or_not'] = $ssl;
            $user_options['primary_color'] = isset($_POST['primary_color'])?$_POST['primary_color']:'';
            $user_options['secondary_color'] = isset($_POST['secondary_color'])?$_POST['secondary_color']:'';
            $user_options = $this->addDefaultAgencySettings($user_options); 
              
        if(empty($checkdomainexist) && !in_array($sub_domain_name, RESTEICTED_DOMAIN)){
        if(!empty($useroptions_data)){
         $saveoption = $this->Welcome_model->update_data("agency_user_options", $user_options, array("user_id" => $login_user_id));   
        }else{
         $saveoption = $this->Welcome_model->insert_data("agency_user_options", $user_options);
        }
        if($useroptions_data[0]['domain_name'] != $dm_name && $domain_or_subdomain == 1){
            if($ssl == 1){
                $agency_domain = "https://".$dm_name;
            }else{
                $agency_domain = "http://".$dm_name;
            }
            $array = array("CUSTOMER_NAME" => $userdata[0]['first_name'].' '.$userdata[0]['last_name'],
                "DOMAIN_URL" => $agency_domain);
            $this->myfunctions->send_email_to_users_by_template($login_user_id, 'new_domain_register', $array, ADMIN_EMAIL);
        }
        if ($saveoption) {
            $result['status'] = 'success';
            $result['msg'] = 'Branding settings updating successfully.';
                $this->session->set_flashdata('message_success', $result['msg'],5);
               redirect(base_url().'account/user_setting#general');
            } else {
             $result['status'] = 'error';
             $result['msg'] = 'Error occurred while updating branding successfully.';
                $this->session->set_flashdata('message_error', $result['msg'],5);
                redirect(base_url().'account/user_setting#general');
            }
        }else{
          $result['status'] = 'error';
          $result['msg'] = 'This domain is already exist!';
         $this->session->set_flashdata('message_error', $result['msg'],5);
         redirect(base_url().'account/user_setting#general');
        }
    }
    
    public function change_agency_user_personal_setting(){
        $result = array();
        $login_user_id = $this->load->get_var('main_user');
        $useroptions_data = $this->S_request_model->getAgencyuserinfo($login_user_id,'');
        $user_options['physical_address'] = isset($_POST['physical_address'])?$_POST['physical_address']:'';
        $user_options['contact_number'] = isset($_POST['contact_number'])?$_POST['contact_number']:'';
        $user_options['email_address'] = isset($_POST['email_address'])?$_POST['email_address']:'';
        $company_name = isset($_POST['company_name'])?$_POST['company_name']:'';
        $user_options = $this->addDefaultAgencySettings($user_options);
        if(!empty($useroptions_data)){
         $saveoption = $this->Welcome_model->update_data("agency_user_options", $user_options, array("user_id" => $login_user_id));   
        }else{
         $saveoption = $this->Welcome_model->insert_data("agency_user_options", $user_options);
        }
        if($company_name != ""){
            $this->Welcome_model->update_data("users", array('company_name' => $company_name), array("id" => $login_user_id));
        }
        if ($saveoption) {
        $result['status'] = 'success';
        $result['msg'] = 'Contact info updated successfully.';
            $this->session->set_flashdata('message_success', $result['msg'],5);
           redirect(base_url().'account/user_setting#personal');
        } else {
         $result['status'] = 'error';
         $result['msg'] = 'Error occurred while updating contact info.';
            $this->session->set_flashdata('message_error', $result['msg'],5);
            redirect(base_url().'account/user_setting#personal');
        }
    }
    
    public function change_agency_user_button_setting(){
        $result = array();
        $login_user_id = $this->load->get_var('main_user');
        $useroptions_data = $this->S_request_model->getAgencyuserinfo($login_user_id,'');
        $user_options['inprog_font_color'] = isset($_POST['inprog_font_color'])?$_POST['inprog_font_color']:'';
        $user_options['revsion_font_color'] = isset($_POST['revsion_font_color'])?$_POST['revsion_font_color']:'';
        $user_options['review_font_color'] = isset($_POST['review_font_color'])?$_POST['review_font_color']:'';
        $user_options['inqueue_font_color'] = isset($_POST['inqueue_font_color'])?$_POST['inqueue_font_color']:'';
        $user_options['draft_font_color'] = isset($_POST['draft_font_color'])?$_POST['draft_font_color']:'';
        $user_options['hold_font_color'] = isset($_POST['hold_font_color'])?$_POST['hold_font_color']:'';
        $user_options['cancel_font_color'] = isset($_POST['cancel_font_color'])?$_POST['cancel_font_color']:'';
        $user_options['complete_font_color'] = isset($_POST['complete_font_color'])?$_POST['complete_font_color']:'';
        $user_options['inprog_bkg_color'] = isset($_POST['inprog_bkg_color'])?$_POST['inprog_bkg_color']:'';
        $user_options['revsion_bkg_color'] = isset($_POST['revsion_bkg_color'])?$_POST['revsion_bkg_color']:'';
        $user_options['review_bkg_color'] = isset($_POST['review_bkg_color'])?$_POST['review_bkg_color']:'';
        $user_options['inqueue_bkg_color'] = isset($_POST['inqueue_bkg_color'])?$_POST['inqueue_bkg_color']:'';
        $user_options['draft_bkg_color'] = isset($_POST['draft_bkg_color'])?$_POST['draft_bkg_color']:'';
        $user_options['hold_bkg_color'] = isset($_POST['hold_bkg_color'])?$_POST['hold_bkg_color']:'';
        $user_options['cancel_bkg_color'] = isset($_POST['cancel_bkg_color'])?$_POST['cancel_bkg_color']:'';
        $user_options['complete_bkg_color'] = isset($_POST['complete_bkg_color'])?$_POST['complete_bkg_color']:'';
        $user_options['approve_button_color'] = isset($_POST['approve_button_color'])?$_POST['approve_button_color']:'';
        $user_options['approve_font_color'] = isset($_POST['approve_font_color'])?$_POST['approve_font_color']:'';
        $user_options = $this->addDefaultAgencySettings($user_options);
        if(!empty($useroptions_data)){
         $saveoption = $this->Welcome_model->update_data("agency_user_options", $user_options, array("user_id" => $login_user_id));   
        }else{
         $saveoption = $this->Welcome_model->insert_data("agency_user_options", $user_options);
        }
        if ($saveoption) {
        $result['status'] = 'success';
        $result['msg'] = 'Button color options updated successfully.';
            $this->session->set_flashdata('message_success', $result['msg']);
           redirect(base_url().'account/user_setting#button_theme');
        } else {
         $result['status'] = 'error';
         $result['msg'] = 'Error occurred while updating button colors.';
            $this->session->set_flashdata('message_error', $result['msg']);
            redirect(base_url().'account/user_setting#button_theme');
        }
    }
    
    public function change_agency_user_notification_setting(){
        $result = array();
        $login_user_id = $this->load->get_var('main_user');
        $useroptions_data = $this->S_request_model->getAgencyuserinfo($login_user_id,'');
        $user_options['success_msg_color'] = isset($_POST['success_msg_color'])?$_POST['success_msg_color']:'';
        $user_options['error_msg_color'] = isset($_POST['error_msg_color'])?$_POST['error_msg_color']:'';
        $user_options['success_msg_bg_color'] = isset($_POST['success_msg_bg_color'])?$_POST['success_msg_bg_color']:'';
        $user_options['error_msg_bg_color'] = isset($_POST['error_msg_bg_color'])?$_POST['error_msg_bg_color']:'';
	$user_options = $this->addDefaultAgencySettings($user_options);
        if(!empty($useroptions_data)){
         $saveoption = $this->Welcome_model->update_data("agency_user_options", $user_options, array("user_id" => $login_user_id));   
        }else{
         $saveoption = $this->Welcome_model->insert_data("agency_user_options", $user_options);
        }
        if ($saveoption) {
        $result['status'] = 'success';
        $result['msg'] = 'Notification settings updated successfully.';
            $this->session->set_flashdata('message_success', $result['msg'],3000);
           redirect(base_url().'account/user_setting#button_theme');
        } else {
         $result['status'] = 'error';
         $result['msg'] = 'Error occurred while updating notification settings.';
            $this->session->set_flashdata('message_error', $result['msg'],3000);
            redirect(base_url().'account/user_setting#button_theme');
        }
    }
    
    public function change_agency_user_default_privillages(){
        $login_user_id = $this->load->get_var('main_user');
        $useroptions_data = $this->S_request_model->getAgencyuserinfo($login_user_id,'');
        if(isset($_POST['save_default_permission'])){
            if (!empty($_POST)) {
                if(isset($_POST['add_requests']) && $_POST['add_requests'] == 'on'){
                $add_requests = 1;
                }
                if(isset($_POST['view_only']) && $_POST['view_only'] == 'on'){
                $view_only = 1;
                }
                if(isset($_POST['del_requests']) && $_POST['del_requests'] == 'on'){
                    $del_requests = 1;
                }
                if(isset($_POST['app_requests']) && $_POST['app_requests'] == 'on'){
                    $app_requests = 1;
                }
                if(isset($_POST['downld_requests']) && $_POST['downld_requests'] == 'on'){
                    $downld_requests = 1;
                }
                if(isset($_POST['comnt_requests']) && $_POST['comnt_requests'] == 'on'){
                    $comnt_requests = 1;
                }
                if(isset($_POST['billing_module']) && $_POST['billing_module'] == 'on'){
                    $billing_module = 1;
                }
                if(isset($_POST['add_brand_pro']) && $_POST['add_brand_pro'] == 'on'){
                    $add_brand_pro = 1;
                }
                if(isset($_POST['access_brand_pro']) && $_POST['access_brand_pro'] == 'on'){
                    $access_brand_pro = 1;
                }
                if(isset($_POST['manage_priorities']) && $_POST['manage_priorities'] == 'on'){
                    $manage_priorities = 1;
                }
                if($_POST['brandids']){
                    $brandIDs = implode(',',$_POST['brandids']) ;
                }
                $role['add_requests'] = isset($add_requests)? $add_requests : 0; 
                $role['delete_req'] = isset($del_requests) ? $del_requests:0;
                $role['approve_revision_requests'] = isset($app_requests)?$app_requests:0;
                $role['download_file'] = isset($downld_requests)?$downld_requests:0;
                $role['comment_on_req'] = isset($comnt_requests)?$comnt_requests:0;
                $role['billing_module'] = isset($billing_module)?$billing_module:0;
                $role['add_brand_pro'] = isset($add_brand_pro)?$add_brand_pro:0;
                $role['brand_profile_access'] = isset($access_brand_pro)?$access_brand_pro:0;
                $role['manage_priorities'] = isset($manage_priorities)?$manage_priorities:0;
                $role['view_only'] = isset($view_only)?$view_only:0;
                $role['brand_ids'] = isset($brandIDs)?$brandIDs:'';
                $role['created'] = date("Y:m:d H:i:s");
                if(!empty($useroptions_data)){
                        $saveoption = $this->Welcome_model->update_data("agency_user_options", $role, array("user_id" => $login_user_id));   
                }else{
                        $saveoption = $this->Welcome_model->insert_data("agency_user_options", $role);
                }
                if($saveoption){
                    $this->session->set_flashdata('message_success', 'Default Permissions saved successfully!');
                    redirect(base_url().'account/user_setting#default_privillages');
                }else{
                    $this->session->set_flashdata('message_error', 'Error occurred while saving default permissions!');
                    redirect(base_url().'account/user_setting#default_privillages');
                }
               
            }
        }
    }
    
    public function Save_LiveChat_Data(){
        $result = array();
        $login_user_id = $this->load->get_var('main_user');
        $useroptions_data = $this->S_request_model->getAgencyuserinfo($login_user_id,'');
        $user_options['live_script'] = isset($_POST['live_script'])?$_POST['live_script']:'';
        $user_options = $this->addDefaultAgencySettings($user_options);
        if(!empty($useroptions_data)){
         $saveoption = $this->Welcome_model->update_data("agency_user_options", $user_options, array("user_id" => $login_user_id));   
        }else{
         $saveoption = $this->Welcome_model->insert_data("agency_user_options", $user_options);
        }
        if ($saveoption) {
        $result['status'] = 'success';
        $result['msg'] = 'Live chat settings updated successfully.';
            $this->session->set_flashdata('message_success', $result['msg']);
           redirect(base_url().'account/user_setting#live_chat');
        } else {
         $result['status'] = 'error';
         $result['msg'] = 'Error occurred while updating live chat settings.';
            $this->session->set_flashdata('message_error', $result['msg']);
            redirect(base_url().'account/user_setting#live_chat');
        }
    }
    
    public function tracking_code($deletescript=""){
        $result = array();
        $login_user_id = $this->load->get_var('main_user');
        $agncy_scrpt = $this->S_request_model->getAgencyuserinfo($login_user_id,'');
        
        if($deletescript != ""){
            $scrpt_del_key = unserialize($agncy_scrpt[0]['script_data']);
            unset($scrpt_del_key[$deletescript]);
            $user_options['script_data'] = serialize($scrpt_del_key);
        }else{
            $script_data = [];
            foreach ($_POST as $key => $value) {
                foreach ($_POST[$key] as $key1 => $value1) {
                    if($key == "tracking_script"){
                      $script_data[$key1][$key] = base64_encode($value1);
                    }else{
                      $script_data[$key1][$key] = $value1;   
                    }
                }
            }
        $user_options['script_data'] = serialize($script_data);
        }
        $user_options['user_id'] = $login_user_id;
        if(!empty($agncy_scrpt)){
         $saveoption = $this->Welcome_model->update_data("agency_user_options", $user_options, array("user_id" => $login_user_id));   
        }else{
         $saveoption = $this->Welcome_model->insert_data("agency_user_options", $user_options);
        }
        if ($saveoption) {
        $result['status'] = 'success';
        $result['msg'] = 'Script is updated successfully.';
            $this->session->set_flashdata('message_success', $result['msg']);
           redirect(base_url().'account/user_setting#live_chat');
        } else {
         $result['status'] = 'error';
         $result['msg'] = 'Error occurred while updating script.';
            $this->session->set_flashdata('message_error', $result['msg']);
            redirect(base_url().'account/user_setting#live_chat');
        }
    }
    
    public function Save_term_privacy(){
        $result = array();
        $login_user_id = $this->load->get_var('main_user');
        $useroptions_data = $this->S_request_model->getAgencyuserinfo($login_user_id,'');
        $user_options['show_term_privacy'] = (isset($_POST['show_term_privacy']) && $_POST['show_term_privacy'] == 'on')?1:0;
        $user_options['term_conditions'] = isset($_POST['term_conditions'])?$_POST['term_conditions']:'';
        $user_options['privacy_policy'] = isset($_POST['privacy_policy'])?$_POST['privacy_policy']:'';
        $user_options = $this->addDefaultAgencySettings($user_options);
        if(!empty($useroptions_data)){
         $saveoption = $this->Welcome_model->update_data("agency_user_options", $user_options, array("user_id" => $login_user_id));   
        }else{
         $saveoption = $this->Welcome_model->insert_data("agency_user_options", $user_options);
        }
        if ($saveoption) {
        $result['status'] = 'success';
        $result['msg'] = 'Privacy policy setting updated successfully.';
            $this->session->set_flashdata('message_success', $result['msg']);
           redirect(base_url().'account/user_setting#live_chat');
        } else {
         $result['status'] = 'error';
         $result['msg'] = 'Error occurred while updating privacy policy settings.';
            $this->session->set_flashdata('message_error', $result['msg']);
            redirect(base_url().'account/user_setting#live_chat');
        }
    }
	
    function addDefaultAgencySettings($settings) {
        $login_user_id = $this->load->get_var('main_user');
        $profile_data = $this->S_admin_model->getuser_data($login_user_id);
        if ($settings['email_address'] != '' || $settings['email_address'] != NULL) {
            $email_address = $settings['email_address'];
        } else {
            $email_address = $profile_data[0]['email'];
        }
        $email_footer = $this->S_admin_model->get_template_byslug_and_userid('email_footer', $login_user_id);
        $email_text = '<div class="footer_section" style="text-align:center; margin-bottom:30px"><img src="' . FS_PATH_PUBLIC_ASSETS . 'img/emailtemplate_img/client_footer-layer.jpg" style="max-width:80%;margin:auto;"></div><div class="footer_copy_text"><p class="content_text" style="font:normal 12px/22px GothamPro-Medium, sans-serif;color: #20364c; padding:0 35px 20px 35px">If you have any questions or concerns, feel free to email us at "' . $email_address . '" , fill out the form on the Contact Us page, or Chat with us live on our site.</p></div>';
        if ($email_footer[0]['user_id'] == 0) {
            $updatearray = array('email_name' => $email_footer[0]['email_name'],
                'email_slug' => $email_footer[0]['email_slug'],
                'email_subject' => $email_footer[0]['email_subject'],
                'email_text' => $email_text,
                'email_description' => $email_footer[0]['email_description'],
                'user_id' => $login_user_id);
            $update_email_logo = $this->Welcome_model->insert_data("email_template", $updatearray);
        } else {
            $updatearray = array('email_text' => $email_text);
            $update_email_logo = $this->Welcome_model->update_data("email_template", $updatearray, array("user_id" => $login_user_id, "email_slug" => $email_footer[0]['email_slug']));
        }
        if (is_array($settings)) {
            $settings['add_requests'] = 1;
            $settings['delete_req'] = 1;
            $settings['approve_revision_requests'] = 1;
            $settings['download_file'] = 1;
            $settings['comment_on_req'] = 1;
            $settings['billing_module'] = 1;
            $settings['add_brand_pro'] = 1;
            $settings['brand_profile_access'] = 1;
            $settings['manage_priorities'] = 1;
            $settings['view_only'] = 0;
            $settings['brand_ids'] = '';
            $settings['created'] = date("Y:m:d h:i:s");
          //  $login_user_id = $this->load->get_var('login_user_id');
            $settings['user_id'] = $login_user_id;
        }
        return $settings;
    }

    public function change_agency_user_email_setting(){
        $result = array();
        $login_user_id = $this->load->get_var('main_user');
        $useroptions_data = $this->S_request_model->getAgencyuserinfo($login_user_id,'');
        $user_options['reply_to_name'] = isset($_POST['reply_name'])?$_POST['reply_name']:'';
        $user_options['reply_to_email'] = isset($_POST['reply_email'])?$_POST['reply_email']:'';
	$user_options['is_smtp_enable'] = (isset($_POST['is_smtp_enable']) && $_POST['is_smtp_enable'] == 'on') ? 1: 0;
        if($_POST['is_smtp_enable'] == 'on'){
            $user_options['from_name'] = isset($_POST['from_name'])?$_POST['from_name']:'';
            $user_options['from_email'] = isset($_POST['from_email'])?$_POST['from_email']:'';
            $user_options['port'] = isset($_POST['port'])?$_POST['port']:'';
            $user_options['host'] = isset($_POST['host'])?$_POST['host']:'';
            $user_options['host_username'] = isset($_POST['host_username'])?$_POST['host_username']:'';
            if(isset($_POST['host_password']) && $_POST['host_password'] != "" && $_POST['host_password'] != '.....'){
                $passwrd = $_POST['host_password'];
                $user_options['host_password'] = $this->S_request_model->my_encrypt($passwrd, BASE64KEY);
            }
            $user_options['mail_secure'] = isset($_POST['email_secure'])?$_POST['email_secure']:'';
        }else{ 
            $user_options['from_name'] = '';
            $user_options['from_email'] = '';
            $user_options['port'] = '';
            $user_options['host'] = '';
            $user_options['host_username'] = '';
            $user_options['host_password'] = '';
            $user_options['mail_secure'] = '';
        }
        $user_options = $this->addDefaultAgencySettings($user_options);
        if(!empty($useroptions_data)){
         $saveoption = $this->Welcome_model->update_data("agency_user_options", $user_options, array("user_id" => $login_user_id));   
        }else{
         $saveoption = $this->Welcome_model->insert_data("agency_user_options", $user_options);
        }
        if ($saveoption) {
            $result['status'] = 'success';
                $result['msg'] = 'Info updated successfully. You may verify the settings by sending a test email.';
                $this->session->set_flashdata('message_success', $result['msg']);
                redirect(base_url() . 'account/user_setting#email_settings');
        } else {
            $result['status'] = 'error';
            $result['msg'] = 'Error occurred while updating email settings.';
            $this->session->set_flashdata('message_error', $result['msg']);
            redirect(base_url().'account/user_setting#email_settings');
        }
    }
    
    public function Send_testemail_toAgencyUser($email=""){
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $send_email = isset($_POST['user_email'])?$_POST['user_email']:$email;
        $login_user_id = $this->load->get_var('main_user');
        $is_send = SM_sendMail($send_email,'Test Email','This is test email for checking Agency email settings.','Agency Testing','',true,$login_user_id);
        if($email != "" && $is_send == 'success'){
            return true;
        }else{
        if($is_send == 'success'){
            $this->session->set_flashdata('message_success', "Test email send successfully!");
            redirect(base_url().'account/user_setting#email_settings');  
        }else{
            $this->session->set_flashdata('message_error', "Some of the host information is incorrect, Please check once!");
            redirect(base_url().'account/user_setting#email_settings');  
        }
        }
    }
    
    function is_valid_domain_name($domain_name)
    {
        return (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain_name) //valid chars check
                && preg_match("/^.{1,253}$/", $domain_name) //overall length check
                && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain_name)   ); //length of each label
    }
     
}
