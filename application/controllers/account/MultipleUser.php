<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MultipleUser extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/indexd
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('Myfunctions');
        $this->load->library('account/S_myfunctions');
        $this->load->helper('form');
        $this->load->library('s3_upload');
        $this->load->helper('url');
        $this->load->model('Request_model');
        $this->load->model('Welcome_model');
        $this->load->model('Admin_model');
        $this->load->model('Account_model');
        $this->load->model('Stripe');
        $this->load->library('zip');

        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    } 

    public function sub_user_list(){
        
       $login_user_id = $this->load->get_var('login_user_id');
       $profile_data = $this->S_admin_model->getuser_data($login_user_id);
       $sub_user_data = $this->S_request_model->getuserbyparentid($login_user_id);
       
       $this->load->view('account/customer_header_1',array('profile'=>$profile_data));
       $this->load->view('account/sub_user_list',array('profile'=> $profile_data,'sub_user_data'=> $sub_user_data)); 
       $this->load->view('account/footer_customer');
    }

    public function sub_user(){
        $this->s_myfunctions->checkloginuser("customer");
        $main_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $parent_user_data = $this->load->get_var('parent_user_data');
        $parent_name = isset($parent_user_data[0]['first_name']) ? $parent_user_data[0]['first_name']:'';
        $subdomain_url = $this->s_myfunctions->dynamic_urlforsubuser_inemail($main_user,$parent_user_data[0]['plan_name'],'login');
        if(isset($_POST['save_subuser'])){
//            echo "<pre/>";print_R($_POST);exit;
            $edit_user_id = isset($_POST['cust_id']) ? $_POST['cust_id']: '';
            $cust_url = isset($_POST['cust_url']) ? $_POST['cust_url']: '';
            $password = isset($_POST['password']) ? $_POST['password']: '';
//           echo "edit_user_id".$edit_user_id;exit;
            if (!empty($_POST)) { 
                $emailpassword = $this->s_myfunctions->random_password();
                $customer['is_active'] = 1; 
                $customer['email'] = $_POST['email'];
                $customer['first_name'] = $_POST['first_name'];
                $customer['last_name'] = $_POST['last_name'];
                $customer['phone'] = $_POST['phone'];
                $customer['role'] = "manager";
                $customer['last_update'] = date("Y:m:d H:i:s");
                $customer['parent_id'] = $login_user_id;
                $customer['modified'] = date("Y:m:d H:i:s");
                $customer['total_active_req'] = TOTAL_ACTIVE_REQUEST;
                $customer['total_inprogress_req'] = TOTAL_INPROGRESS_REQUEST; 
                if(isset($_POST['genrate_password']) && $_POST['genrate_password'] == 'on'){
                    $genrate_password = 1;
                }
                if(isset($_POST['add_requests']) && $_POST['add_requests'] == 'on'){
                $add_requests = 1;
                }
                if(isset($_POST['view_only']) && $_POST['view_only'] == 'on'){
                $view_only = 1;
                }
                if(isset($_POST['del_requests']) && $_POST['del_requests'] == 'on'){
                    $del_requests = 1;
                }
                if(isset($_POST['approve_reject']) && $_POST['approve_reject'] == 'on'){
                    $app_requests = 1;
                }
                if(isset($_POST['show_all_project']) && $_POST['show_all_project'] == 'on'){
                    $show_all_project = 1;
                }
                if(isset($_POST['comnt_requests']) && $_POST['comnt_requests'] == 'on'){
                    $comnt_requests = 1;
                }
                if(isset($_POST['billing_module']) && $_POST['billing_module'] == 'on'){
                    $billing_module = 1;
                }
                if(isset($_POST['add_brand_pro']) && $_POST['add_brand_pro'] == 'on'){
                    $add_brand_pro = 1;
                }
                if(isset($_POST['access_brand_pro']) && $_POST['access_brand_pro'] == 'on'){
                    $access_brand_pro = 1;
                }
		if(isset($_POST['upload_draft']) && $_POST['upload_draft'] == 'on'){
                    $upload_draft = 1;
                }
                if(isset($_POST['file_management']) && $_POST['file_management'] == 'on'){
                    $file_management = 1;
                }
                if(isset($_POST['white_label']) && $_POST['white_label'] == 'on'){
                    $white_label = 1;
                }
                if(isset($_POST['assign_designer_to_client']) && $_POST['assign_designer_to_client'] == 'on'){
                    $assign_designer_to_client = 1;
                }
                if(isset($_POST['assign_designer_to_project']) && $_POST['assign_designer_to_project'] == 'on'){
                    $assign_designer_to_project = 1;
                }
                
                if($edit_user_id == ''){
                    $checkemail = $this->S_request_model->getuserbyemail($_POST['email']);
                    if (!empty($checkemail)) {
                       $error_msg = $this->session->set_flashdata('message_error', 'Email Address is already taken!', 5);
                       redirect(base_url() . $cust_url);
                    }else{
                        if($genrate_password != 1 && $password != ""){
                            $emailpassword = $password;
                            $customer['new_password'] = md5($password);
                        }else{
                            $customer['new_password'] = md5($emailpassword);
                        }
                    $customer['created'] = date("Y:m:d H:i:s");
                    $time = strtotime($customer['created']);
                    $customer['ip_address'] = $_SERVER['REMOTE_ADDR'];
                    $id = $this->Welcome_model->insert_data("s_users", $customer);
                    if($id){
                      $array = array('CUSTOMER_NAME' => $customer['first_name'],
                            'CUSTOMER_EMAIL' => $customer['email'],
                            'CUSTOMER_PASSWORD' => $emailpassword,
                                'PARENT_USER' => $parent_name,
                            'LOGIN_URL' => $subdomain_url);
                    $this->s_myfunctions->send_email_to_users_by_template($main_user, 'welcome_email_to_sub_user', $array, $customer['email']);
                    }
                    }
                }else {
                      $this->Welcome_model->update_data("s_users", $customer, array("id" => $edit_user_id));
                }
                 $role['brand_profile_access'] = isset($access_brand_pro)?$access_brand_pro:0;   
                 $role['add_requests'] = isset($add_requests)? $add_requests : 0; 
                 $role['delete_req'] = isset($del_requests) ? $del_requests:0;
                 $role['approve_reject'] = isset($app_requests)?$app_requests:0;
                 $role['show_all_project'] = isset($show_all_project)?$show_all_project:0;
                 $role['comment_on_req'] = isset($comnt_requests)?$comnt_requests:0;
                 $role['billing_module'] = isset($billing_module)?$billing_module:0;
                 $role['add_brand_pro'] = isset($add_brand_pro)?$add_brand_pro:0;
                 $role['upload_draft'] = isset($upload_draft)?$upload_draft:0;
                 $role['file_management'] = isset($file_management)?$file_management:0;
                 $role['white_label'] = isset($white_label)?$white_label:0;
                 $role['assign_designer_to_client'] = isset($assign_designer_to_client)?$assign_designer_to_client:0;
                 $role['assign_designer_to_project'] = isset($assign_designer_to_project)?$assign_designer_to_project:0;
                 $role['view_only'] = isset($view_only)?$view_only:0;
                 $role['modified'] = date("Y:m:d H:i:s");
                if($edit_user_id == ''){
                 if($id != '')  {
                 $role['customer_id'] = $id; 
                 $role['created'] = date("Y:m:d H:i:s");
                 $role_id = $this->Welcome_model->insert_data("s_user_permissions", $role); 
                 if($genrate_password == 1){
                  $this->session->set_flashdata('message_success', 'User created successfully! An email is sent to the address with a temporary password.', 5);
                 }else{
                   $this->session->set_flashdata('message_success', 'User created successfully! An email is sent to the address with password.', 5);  
                 }
                 }
            }else{
               $role['customer_id'] = $edit_user_id; 
               $dataexist = $this->S_request_model->ifuserNotinpermision($edit_user_id);
               if($dataexist){
                    $role_id = $this->Welcome_model->update_data("s_user_permissions", $role, array("customer_id" => $edit_user_id));
               }else{
                  $role_id = $this->Welcome_model->insert_data("user_permissions", $role);
               }
               $this->session->set_flashdata('message_success', 'User Updated successfully..!', 5);
            }
            }
            if ($role_id) {
                redirect(base_url() . $cust_url);
            }
            else {
                $this->session->set_flashdata('message_error', 'Something went wrong,Please Try again ..!', 5);
                redirect(base_url() . $cust_url);
            }
        }
        $sub_user_data = $this->S_admin_model->getuser_data($edit_user_id);
        $sub_user_permissions = $this->S_request_model->get_sub_user_permissions($edit_user_id);
//        $brandprofile = $this->Request_model->get_brand_profile_by_user_id($main_user);
        $profile_data = $this->S_admin_model->getuser_data($login_user_id);
        $this->load->view('account/customer_header_1',array('profile'=>$profile_data));
        $this->load->view('account/footer_customer');
        $this->load->view('account/add_new_user',array('sub_user_data' => $sub_user_data,'sub_user_permissions' =>$sub_user_permissions));
    }

    public function delete_sub_user($url,$id) {
        //echo $url.$id;exit;
        $sub_user_permissions = $this->Request_model->delete_user_permission($id);
        if ($sub_user_permissions) {
            $sub_user_data = $this->Request_model->delete_user("users", $id);
        }
        $this->session->set_flashdata('message_success', 'User deleted Successfully', 5);
        if($url == "setting_view"){
            redirect(base_url() . "account/setting-view#management");
        }else{
            redirect(base_url() . "account/client_management");
        }
    }

    //  public function delete_sub_user_designer($url,$id) {
    //     //  $id= $this->input->post("id"); 
    //    //  $designer = array( 
    //    //      "is_delete" => 1,
    //    //      "is_active" => 0,
    //    //      "modified" => date("Y:m:d H:i:s")
    //    //      );

    //    // $re =  $this->Welcome_model->update_data("s_users", $designer,array("id" =>$id));
    //    // if($re){
    //    //    $this->session->set_flashdata('message_success', 'User deleted Successfully', 5);
    //    //   redirect(base_url() . "account/setting-view?".$url);
    //    // }else{
    //    //    $this->session->set_flashdata('message_success', 'User not deleted', 5);
    //    // }
      
        
    // }

     public function delete_sub_user_designer() {
            // $this->myfunctions->checkloginuser("admin");
            $all_request_id = array();
            $file_id = array();
            $file_name = array();
            $all_requests = $this->S_admin_model->get_all_request_by_customer($_POST['id']);
            $CheckForMainORsub = $this->S_admin_model->CheckForMainORsub($_POST['id']);

            foreach ($all_requests as $request) {
                $all_request_id[] = $request['id'];
            }
            if (!empty($all_request_id)) {
                $this->S_admin_model->delete_request_data("s_request_discussions", $all_request_id);
                $this->S_admin_model->delete_request_data("s_message_notification", $all_request_id);
                $this->S_admin_model->delete_request_data("s_notification", $all_request_id);

                $all_files = $this->S_admin_model->get_all_files_rid($all_request_id);
                foreach ($all_files as $files) {
                    $file_id[] = $files['id'];
                }
            }

            if (!empty($all_requests)) {
                foreach ($all_requests as $request_all) {
                //echo $request_all;
                    $all_files_nm = $this->S_admin_model->get_all_files_rid($request_all['id']);
                    if($CheckForMainORsub['is_saas'] == 1 ){

                    $folderNmae = 'public/uploads/requests/' . $request_all['id'];
                    }else{

                    $folderNmae = 'design_requests/public/uploads/requests/' . $request_all['id'];
                    }
                    foreach ($all_files_nm as $files) {
                        $file_name[] = $files['file_name'];
                        $file_name[] = $files['src_file'];
                    }
                // echo "<pre>";print_r($file_name);exit;
                    foreach ($file_name as $req_files) {
                        if (in_array($req_files, array(".", "..")))
                            continue;
                        $this->load->library('s3_upload');
                        $this->s3_upload->delete_file_from_s3($folderNmae . '/' . $req_files);
                        $this->s3_upload->delete_file_from_s3($folderNmae . '/_thumb/' . $req_files);
                        $this->s3_upload->delete_file_from_s3($folderNmae);
                    }

                // $path = FCPATH . "public/uploads/requests/" . $request_all['id'] . "/*";
                // $path1 = FCPATH . "public/uploads/requests/" . $request_all['id'];
                // if (file_exists($path1)) {
                // $files = glob($path); // get all file names
                // foreach ($files as $file) { // iterate files
                // if (is_file($file))
                // unlink($file); // delete file
                // }
                // rmdir($path1);
                    $all_requests_id[] = $request_all['id'];
                // }
                }
            }
            if (!empty($file_id)) {
                $this->S_admin_model->delete_request_file_data("s_request_file_chat", $file_id);
                $this->S_admin_model->delete_request_data("s_request_files", $all_request_id);
            }
            if (!empty($all_request_id)) {
                $this->S_admin_model->alldelete_request('s_requests', $all_request_id);
            }
            $this->S_admin_model->customer_delete($_POST['id']);
                $output['msg'] = "user Deleted successfully!";
                $output['status'] = 1;
                echo json_encode($output);exit;
        }
}
