<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Request extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/indexd
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->library('account/S_myfunctions');
        $this->load->library('account/S_customfunctions');
        $this->load->helper('form');
        $this->load->library('s3_upload');
        $this->load->helper('url');
        $this->load->model('account/S_request_model');
        $this->load->model('account/S_category_model');
//        $this->load->model('S_request_model');
//        $this->load->model('S_category_model');
        $this->load->model('Welcome_model');
//        $this->load->model('S_admin_model');
        $this->load->model('account/S_admin_model');
        $this->load->model('Account_model');
        $this->load->model('Clients_model');
        $this->load->model('Stripe');
        $this->load->library('zip');
        $this->load->helper('download');
        $config_email = $this->config->item('email_smtp');
        define('MAIL_AUTH', true);
        define('MAIL_SECURE', $config_email['sequre']);
        define('MAIL_HOST', $config_email['host']);
        define('MAIL_PORT', $config_email['port']);
        define('MAIL_USERNAME', $config_email['hostusername']);
        define('MAIL_PASSWORD', $config_email['hostpassword']);
        define('MAIL_SENDER', $config_email['sender']);
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 3000);
        ini_set('max_execution_time', 3000);
    }

    public function index() {
        $this->s_myfunctions->checkloginuser("customer");
        $created_user = $this->load->get_var('main_user');
               
        redirect(base_url() . "account/request/design_request");
        $today_customer = $this->S_admin_model->get_today_customer();
        $all_customer = $this->S_admin_model->get_total_customer();
        $today_requests = $this->S_admin_model->get_today_request();
        $total_requests = $this->S_admin_model->get_all_requested_designs(array("active", "assign", "checkforapprove"), $created_user);
        $this->load->view('admin/admin_header');
        $this->load->view('admin/index', array('today_customer' => $today_customer, 'all_customer' => $all_customer, 'today_requests' => $today_requests, 'total_requests' => $total_requests));
        $this->load->view('account/footer_customer');
    }
    
    public function add_new_request(){ 
//        echo "<pre/>";print_R($_POST);exit;
        $allcategories = $this->S_category_model->get_categories(0,1);
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        
        $user_permsn = $this->S_request_model->getAgencyuserinfo($created_user);
        $ismanual = $user_permsn[0]['is_manual'];
        $defult_status = (isset($user_permsn[0]['default_status']) && $user_permsn[0]['default_status'] != '') ? $user_permsn[0]['default_status']:'';
        $customer_status = (isset($_POST['status']) && $_POST['status']!= 1 && $_POST['status']!= '') ? $_POST['status'] : $defult_status;
        //echo "customer_status".$customer_status;
        $canbrand_profile_access = $this->s_myfunctions->isUserPermission('brand_profile_access',$created_user);
//       echo "canbrand_profile_access".$canbrand_profile_access;exit;
        $profile_data = $this->S_admin_model->getuser_data($login_user_id);
        //echo "<pre/>";print_R($profile_data);exit;
        $file_add = (isset($_POST['is_transfer'])) ? $_POST['is_transfer'] : 'no'; 
        if(isset($profile_data[0]['is_saas']) && $profile_data[0]['is_saas'] == 1 && $profile_data[0]['role'] == 'customer'){
          $cusTomr_ID = $profile_data[0]['id'];
          $parent_customer_id = 0;  
          $cReatedBy = 0;
        }elseif(!isset($profile_data[0]['is_saas']) && $profile_data[0]['role'] == 'customer'){
           $cusTomr_ID = $profile_data[0]['id'];
           $parent_customer_id = $profile_data[0]['parent_id'];  
           $cReatedBy = 0;
        }else{
           $cusTomr_ID = $profile_data[0]['parent_id'];
           $parent_customer_id = 0;  
           $cReatedBy = $profile_data[0]['id'];
        }
        $priority_val = $this->S_request_model->PriorityBasedonRole($login_user_id);
//        echo "canbrand_profile_access".$login_user_id;
        /*if have brand for main user****/
        if($canbrand_profile_access != 0 && $profile_data[0]['role'] == 'customer' && $profile_data[0]['is_saas'] == 1){
          $user_profile_brands = $this->S_request_model->get_brand_profile_by_user_id($created_user,'','','','user');
        }
          /*if have brand for manager user having permission****/
        elseif($canbrand_profile_access == 0 && $profile_data[0]['role'] == 'manager'){
           $user_profile_brands = $this->S_request_model->get_brand_profile_by_user_id($created_user,'','','','manager');
        }else{
           $user_profile_brands = $this->S_request_model->get_brand_profile_by_user_id($created_user,'','','','customer');
           $user_profile_brand = $this->S_request_model->get_user_brand_profile_by_user_id($login_user_id);
        }
        if(sizeof($user_profile_brand) > 0){
            foreach ($user_profile_brand as $user_brands_info) {
              $brand_id[] =  $user_brands_info['brand_id']; 
            }  
            $user_profile_brands = $this->S_request_model->get_brandprofile_by_id('',$brand_id);
        }
        $canuseradd = $this->s_myfunctions->isUserPermission('add_requests');
        $parent_id = $this->load->get_var('parent_user_id');
        $created_by = ($parent_id != 0) ? $login_user_id: 0;
        /******************************************/
        $reqid = isset($_GET['reqid'])?$_GET['reqid']:"";
        $duplid = isset($_GET['did'])?$_GET['did']:"";
        $trail = isset($profile_data[0]['is_trail'])?$profile_data[0]['is_trail']:'';
	/*****************trail and 99 user add request more than limit*************************/
        $trail_user = $this->S_request_model->getuserbyid($created_user);
//         echo "<pre>";print_r($trail_user);
        $request_for_all_status_acc_billing_cycle = $this->S_request_model->getall_request_for_all_status($created_user,$trail_user[0]['billing_start_date'],$trail_user[0]['billing_end_date']);
        $useraddrequest = $this->s_myfunctions->canUserAddNewRequest($trail_user,$request_for_all_status_acc_billing_cycle,$request_for_all_status);
        if ($useraddrequest == 0 || $useraddrequest == 1) {
            if ($reqid && $duplid == '') {
               //edit case will not redirect the user in any case.
            } else {
                $this->session->set_flashdata('message_error', 'You have already added allowed requests count in current billing cycle.', 5);
                redirect(base_url() . "account/dashboard");
            }
        }
        /************** start add request **********************/
        if($reqid == '' && $duplid == ''){
            $allqust = $this->S_request_model->getallquestions('1','0');
            $subcat_id = isset($_POST['subcategory_id']) ? $_POST['subcategory_id'] : '';
            $subcat_data = $this->S_category_model->get_category_byID($subcat_id);
            $bucketType = $subcat_data[0]['bucket_type'];
            $samples = $this->S_category_model->getSampleMaterialsbycat_subcat(1,0);
            $getminMax = $this->S_category_model->getSampleMaterialsbycat_subcat(1,0,true);
            $expected = $this->s_myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $profile_data[0]['plan_name'], $bucketType, $subcat_data[0]['timeline'], $profile_data[0]['turn_around_days']);
            if($customer_status == 'active' || $customer_status == 'disapprove'){
              $expected = $expected;  
              $approval = "";
            }else if($customer_status == 'checkforapprove'){
                $expected = ''; 
                $approval = date("Y-m-d H:i:s");
            }else{
               $expected = '';
               $approval = "";
            }
            $fileType="";
            if(!empty($_POST['color_pref'])){
                if(in_array('Let Designer Choose',$_POST['color_pref'])){
                     $plate = 'Let Designer Choose';
                     $color_values = '';
                }else{
                     $plate = isset($_POST['color_pref'][1]) ? $_POST['color_pref'][1] : '';
                     $color_values = implode(",", $_POST['color_values']);
                }
            }
            if (isset($_POST['filetype'])) {
                $fileType = implode(",", $_POST['filetype']);
            }
            $data = array("customer_id" => $cusTomr_ID,
            "brand_id" => isset($_POST['brand_name']) ? $_POST['brand_name'] : '',
            "category_id" => isset($_POST['category_id'])?$_POST['category_id']:'',
            "subcategory_id" => isset($_POST['subcategory_id'])?$_POST['subcategory_id']:'',
            "category_bucket" => $bucketType,
            "parent_customer_id" => $parent_customer_id,
            "title" => isset($_POST['title'])? $_POST['title']:'',
            "created" => date("Y-m-d H:i:s"),
            "status" => $customer_status,
            "status_admin" => $customer_status,
            "status_designer" => $customer_status,
            "expected_date" => $expected,
            "is_transfer" => 1,
            "designer_id" => 0,
            "priority" => 0,
            "deliverables" => $fileType,
            "created_by" => $cReatedBy,
            "approvaldate" => $approval,
            "last_modified_by" => $_SESSION['user_id'],
            "color_pref" => $plate,
            "color_values" => $color_values,
            "modified" => date("Y-m-d H:i:s"),
            );
            if(isset($_POST['is_transfer']) && $_POST['is_transfer'] == 'yes'){
                unset($data['parent_customer_id']);
            }else{
                unset($data['is_transfer']);
            }
            if($customer_status == 'assign'){
                $data['priority'] = $priority_val+1;
            }
             /*******Final Submit ***************/
            if(isset($_POST['request_submit'])){
                if($_POST['is_transfer'] == 'yes'){
                    $success = $this->Welcome_model->insert_data("requests", $data); 
                }else{
                    $success = $this->Welcome_model->insert_data("s_requests", $data); 
                }
                /*********question*******/
                if (!empty($_POST['quest'])) {
                    foreach ($_POST['quest'] as $kqust => $vqust) {
                    if (is_array($vqust)) {
                        $vqust = implode(',', $vqust);
                    } else {
                        $vqust = $vqust;
                    }
                    $catquest['question_id'] = $kqust;
                    $catquest['request_id'] = $success;
                    $catquest['customer_id'] = $created_user;
                    $catquest['answer'] = $vqust;
                    $catquest['created'] = date("Y-m-d H:i:s");
                    $catquest['modified'] = date("Y-m-d H:i:s");
//                    echo "<pre/>";print_R($catquest);exit;
                    if($_POST['is_transfer'] == 'yes'){
                    $meta = $this->Welcome_model->insert_data("requests_meta", $catquest);
                    } else{
                      $meta = $this->Welcome_model->insert_data("s_requests_meta", $catquest);  
                    }
//                    echo $meta;exit;
                }}
                
                /****save samples *********/
                if($success){
                    $smplesave = array();
//                    echo "<pre/>";print_R($_POST);exit;
                    if(!empty($_POST['sample_subcat'])){
                    foreach($_POST['sample_subcat'] as $skey => $sval){
                        $smplesave['request_id'] = $success;
                        $smplesave['material_id'] = $sval;
                        $smplesave['created'] = date("Y-m-d H:i:s");
                        if($_POST['is_transfer'] == 'yes'){
                        $savedsample = $this->Welcome_model->insert_data("request_sample_data", $smplesave);
                        }else{
                            $savedsample = $this->Welcome_model->insert_data("s_request_sample_data", $smplesave);
                        }
                    }
                    }
                     
                }
                $id = $success;
                $this->addFileRequest($id,$file_add);
                //$this->s_myfunctions->capture_project_activity($success,'','','project_created','add_new_request',$login_user_id,1);
                if($ismanual == 0){
                    $update_data = $this->s_myfunctions->changerequeststatusafteradd($created_user,$id,$bucketType,$subcat_data,$profile_data);
                }   
                $newrequestdata = $this->S_request_model->get_request_by_id($id);
                    if($trail == 1){
                    $this->load->library('drip');
                    $this->load->library('datasett');
                    $this->load->library('response');
                    $Drip = new Drip('5791e6d8913a121374b5e55904342eb3','1975472');
                    $data = new Datasett('tags', [
                        'email' => $profile_data[0]['email'],
                        'tag' => "Submitted Free Design Request",
                    ]);
                    $Drip->post('tags', $data);
                    redirect(base_url()."account/dashboard");
                    }
                    if($update_data || $id) {
                    $this->session->set_flashdata('message_success', 'We\'re now reviewing your request and your designer will get something back to you soon', 5);
                    redirect(base_url()."account/dashboard");
                    }
            }

            /************** end add request **********************/
        }
        /************** start edit request **********************/
        if ($reqid && $duplid == '') {
            $subcat_id = isset($_POST['subcategory_id']) ? $_POST['subcategory_id'] : '';
            $subcat_data = $this->S_category_model->get_category_byID($subcat_id);
            $bucketType = $subcat_data[0]['bucket_type'];
            $existeddata = $this->S_request_model->get_request_by_id($reqid);
           // echo "<pre/>";print_R($_POST);exit;
            $samples = $this->S_category_model->getSampleMaterialsbycat_subcat($existeddata[0]['category_id'],$existeddata[0]['subcategory_id']);
            $getminMax = $this->S_category_model->getSampleMaterialsbycat_subcat($existeddata[0]['category_id'],$existeddata[0]['subcategory_id'],true);
            $selectedsamples = $this->S_request_model->getSamplematerials($reqid);
//            echo "<pre/>";print_r($selectedsamples);
            if(!empty($selectedsamples)){
                $selectdmaterials = array();
                foreach($selectedsamples as $smkey => $smval){
                    $selectdmaterials[]  = $smval['material_id'];
                }
//                echo "<pre/>";print_R($selectedsamples);exit;
            }
            $allqust = $this->S_request_model->getallquestions($existeddata[0]['category_id'], $existeddata[0]['subcategory_id']);
            foreach ($allqust as $ak => $av) {
                $getquestans = $this->S_category_model->get_quest_Ans($reqid, $av['id']);
                $allqust[$ak]['answer'] = $getquestans[0]['answer'];
                $allqust[$ak]['description'] = $getquestans[0]['description'];
                $allqust[$ak]['design_dimension'] = $getquestans[0]['design_dimension'];
            }
            $this->s_myfunctions->checkloginuser($login_user_data[0]['role']);
            $fileType="";
            if(in_array('Let Designer Choose',$_POST['color_pref'])){
                     $plate = 'Let Designer Choose';
                     $color_values = '';
                }else{
                     $plate = isset($_POST['color_pref'][1]) ? $_POST['color_pref'][1] : '';
                     $color_values = implode(",", $_POST['color_values']);
            }
            if (isset($_POST['filetype'])) {
                $fileType = implode(",", $_POST['filetype']);
            }
            /*************save in edit case *****************/
            if(isset($_POST['request_submit'])){
                $data = array(
                    //"customer_id" => $created_user,
                    "brand_id" => isset($_POST['brand_name']) ? $_POST['brand_name'] : '',
                    "category_id" => isset($_POST['category_id'])?$_POST['category_id']:'',
                    "subcategory_id" => isset($_POST['subcategory_id'])?$_POST['subcategory_id']:'',
                    "category_bucket" => $bucketType,
                    "title" => isset($_POST['title'])?$_POST['title']:'',
                    "status" => $customer_status,
                    "status_admin" => $customer_status,
                    "status_designer" => $customer_status,
                    "expected_date" => $expected,
                    "deliverables" => $fileType,
                    "last_modified_by" => $_SESSION['user_id'],
                    "color_pref" => $plate,
                    "color_values" => $color_values
                );
                if($customer_status == 'assign'){
                    $data['priority'] = $priority_val+1;
                }
                $meta_ans = $this->S_category_model->getMetasbyReqID($reqid);
                $from_table = $this->S_request_model->getDataBasedonSAAS($reqid);
                foreach ($meta_ans as $mk => $mv) {
                    if($from_table == 'requests'){
                    $this->S_admin_model->delete_data('requests_meta', $mv['id']);
                    }else{
                      $this->S_admin_model->delete_data('s_requests_meta', $mv['id']);   
                    }
                }
                if (!empty($_POST['quest'])) {
                    foreach ($_POST['quest'] as $kqust => $vqust) {
                        if (is_array($vqust)) {
                            $vqust = implode(',', $vqust);
                        } else {
                            $vqust = $vqust;
                        }
                        $catquest['question_id'] = $kqust;
                        $catquest['request_id'] = $reqid;
                        $catquest['customer_id'] = $created_user;
                        $catquest['is_deleted'] = '0';
                        $catquest['answer'] = $vqust;
                        $catquest['modified'] = date("Y-m-d H:i:s");
                        $isdata = $this->S_category_model->getMetasbyReqID($reqid, $kqust);
                        if (!empty($isdata)) {
                            if($from_table == 'requests'){
                                $meta = $this->Welcome_model->update_data("requests_meta", $catquest, array("question_id" => $kqust, "request_id" => $reqid));
                            }else{
                                $meta = $this->Welcome_model->update_data("s_requests_meta", $catquest, array("question_id" => $kqust, "request_id" => $reqid));
                            }
                        } else {
                            if($from_table == 'requests'){
                               $meta = $this->Welcome_model->insert_data("requests_meta", $catquest);  
                            }else{
                               $meta = $this->Welcome_model->insert_data("s_requests_meta", $catquest);     
                            }
                        }
                    }
                }
                if($_POST['is_transfer'] == 'yes'){
//                    echo "<pre/>";print_R($data);exit;
                    $success = $this->Welcome_model->update_data("requests", $data, array("id" => $reqid));
                }else{
                    $success = $this->Welcome_model->update_data("s_requests", $data, array("id" => $reqid));
                }
                if($success){
                $selectedsamples = $this->S_request_model->getSamplematerials($reqid);
                foreach ($selectedsamples as $slk => $slv) {
                    if($from_table == 'requests'){
                     $this->S_admin_model->delete_data('request_sample_data', $slv['id']);
                    }else{
                      $this->S_admin_model->delete_data('s_request_sample_data', $slv['id']);  
                    }
                }
                $smplesave = array();
                    if(!empty($_POST['sample_subcat'])){
                    foreach($_POST['sample_subcat'] as $skey => $sval){
                        $smplesave['request_id'] = $reqid;
                        $smplesave['material_id'] = $sval;
                        $smplesave['created'] = date("Y-m-d H:i:s");
                        if($from_table == 'requests'){
                        $savedsample = $this->Welcome_model->insert_data("request_sample_data", $smplesave);
                        }else{
                         $savedsample = $this->Welcome_model->insert_data("s_request_sample_data", $smplesave);   
                        }
                    }
                }
                }
                $id = $reqid;
                $this->addFileRequest($id,$file_add);
                //$this->s_myfunctions->capture_project_activity($reqid,'','','edit_project','add_new_request',$login_user_id,1);
                if($ismanual == 0){
                $update_data = $this->s_myfunctions->changerequeststatusafteradd($created_user,$id,$bucketType,$subcat_data,$profile_data);
                    $newrequestdata = $this->S_request_model->get_request_by_id($id);
                    if ($update_data) {
                        $notifications = $this->S_request_model->get_notifications($login_user_id);
                        $notification_number = $this->S_request_model->get_notifications_number($login_user_id);
                        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
                        $this->load->view('account/footer_customer');
                        $this->session->set_flashdata('message_success', 'We\'re now reviewing your request and your designer will get something back to you soon', 5);
                        redirect(base_url()."account/dashboard");
                    }
                }
                if($id){
                $this->session->set_flashdata('message_success', 'We\'re now reviewing your request and your designer will get something back to you soon', 5);
                redirect(base_url()."account/dashboard");
                }
            }
        }
        /************** end edit request **********************/
        /************** start duplicate request **********************/
        if($duplid && $reqid == ''){
//            echo "<pre/>";print_R($_POST);exit;
            $subcat_id = isset($_POST['subcategory_id']) ? $_POST['subcategory_id'] : '';
            $subcat_data = $this->S_category_model->get_category_byID($subcat_id);
            $bucketType = $subcat_data[0]['bucket_type'];
            $duplicatedata = $this->S_request_model->get_request_by_id($duplid);
            $allqust = $this->S_request_model->getallquestions($duplicatedata[0]['category_id'], $duplicatedata[0]['subcategory_id']);
            $expected = $this->s_myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $profile_data[0]['plan_name'], $bucketType, $subcat_data[0]['timeline'], $profile_data[0]['turn_around_days']);
            if($customer_status == 'active' || $customer_status == 'disapprove'){
               $expected = $expected;  
               $approval = "";
             }else if($customer_status == 'checkforapprove'){
                 $expected = ''; 
                 $approval = date("Y-m-d H:i:s");
             }else{
                $expected = '';
                $approval = "";
             }
            $samples = $this->S_category_model->getSampleMaterialsbycat_subcat($duplicatedata[0]['category_id'],$duplicatedata[0]['subcategory_id']);
            $getminMax = $this->S_category_model->getSampleMaterialsbycat_subcat($duplicatedata[0]['category_id'],$duplicatedata[0]['subcategory_id'],true);
            $selectedsamples = $this->S_request_model->getSamplematerials($duplid);
            if(!empty($selectedsamples)){
                $selectdmaterials = array();
                foreach($selectedsamples as $smkey => $smval){
                    $selectdmaterials[]  = $smval['material_id'];
                }
//                echo "<pre/>";print_R($selectedsamples);exit;
            }
            foreach ($allqust as $ak => $av) {
                $getquestans = $this->S_category_model->get_quest_Ans($duplid, $av['id']);
                $allqust[$ak]['answer'] = $getquestans[0]['answer'];
                $allqust[$ak]['description'] = $getquestans[0]['description'];
                $allqust[$ak]['design_dimension'] = $getquestans[0]['design_dimension'];
            }
            $files = $this->S_request_model->get_requestfiles_by_id($duplid,'customer');
            $this->s_myfunctions->checkloginuser($login_user_data[0]['role']);
            $fileType="";
            if(in_array('Let Designer Choose',$_POST['color_pref'])){
                $plate = 'Let Designer Choose';
                $color_values = '';
            }else{
                $plate = isset($_POST['color_pref'][1]) ? $_POST['color_pref'][1] : '';
                $color_values = implode(",", $_POST['color_values']);
            }
            if (isset($_POST['filetype'])) {
                $fileType = implode(",", $_POST['filetype']);
            }
            $data = array(
                "customer_id" => $cusTomr_ID,
                "brand_id" => isset($_POST['brand_name']) ? $_POST['brand_name'] : '',
                "category_id" => isset($_POST['category_id'])?$_POST['category_id']:'',
                "subcategory_id" => isset($_POST['subcategory_id'])?$_POST['subcategory_id']:'',
                "category_bucket" => $bucketType,
                "expected_date" => $expected,
                "status" => $customer_status,
                "status_admin" => $customer_status,
                "status_designer" => $customer_status,
                "title" => $_POST['title'],
                "deliverables" => $fileType,
                "approvaldate" => $approval,
                "color_pref" => $plate,
                "color_values   " => $color_values,
                "created_by" => $cReatedBy,
                "last_modified_by" => $_SESSION['user_id'],
                "created" => date("Y-m-d H:i:s"),
                "modified" => date("Y-m-d H:i:s")
            );
            if(isset($_POST['is_transfer']) && $_POST['is_transfer'] == 'yes'){
                unset($data['parent_customer_id']);
            }
            if($customer_status == 'assign'){
                $data['priority'] = $priority_val+1;
            }
            /*******Final Submit ***************/
            if(isset($_POST['request_submit'])){
                if($_POST['is_transfer'] == 'yes'){
                    $success = $this->Welcome_model->insert_data("requests", $data);
                }else{
                    $success = $this->Welcome_model->insert_data("s_requests", $data);
                }
                if (!empty($_POST['quest'])) {
                    foreach ($_POST['quest'] as $kqust => $vqust) {
                        if (is_array($vqust)) {
                            $vqust = implode(',', $vqust);
                        } else {
                            $vqust = $vqust;
                        }
                        $catquest['question_id'] = $kqust;
                        $catquest['request_id'] = $success;
                        $catquest['customer_id'] = $created_user;
                        $catquest['answer'] = $vqust;
                        $catquest['created'] = date("Y-m-d H:i:s");
                        $catquest['modified'] = date("Y-m-d H:i:s");
                        if($_POST['is_transfer'] == 'yes'){
                        $meta = $this->Welcome_model->insert_data("requests_meta", $catquest);
                        } else{
                          $meta = $this->Welcome_model->insert_data("s_requests_meta", $catquest);  
                        }
                    }
                }
                if($success){
//                    echo "<pre/>";print_R($_POST['sample_subcat']);exit;
                    $smplesave = array();
                    if(!empty($_POST['sample_subcat'])){
                    foreach($_POST['sample_subcat'] as $skey => $sval){
                        $smplesave['request_id'] = $success;
                        $smplesave['material_id'] = $sval;
                        $smplesave['created'] = date("Y-m-d H:i:s");
                        if($_POST['is_transfer'] == 'yes'){
                        $savedsample = $this->Welcome_model->insert_data("request_sample_data", $smplesave);
                        }else{
                        $savedsample = $this->Welcome_model->insert_data("s_request_sample_data", $smplesave);
                        }
                    }
                }
                }
                $id = $success;
               // echo $id;exit;
                if (!is_dir('public/uploads/requests/' . $id)) {
                $data_dir = mkdir('./public/uploads/requests/' . $id, 0777, TRUE);
                }
                $CheckForMainORsub = $this->S_admin_model->CheckForMainORsub($_SESSION['user_id']); 
                if(UPLOAD_FILE_SERVER == 'bucket'){
                    if($CheckForMainORsub['is_saas'] == 1){
                        $uploadPath = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS;
                    }else{
                        $uploadPath = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS_SAAS;
                    }
                    $srcpath = $uploadPath.$duplid.'/';
                    $destpath = $uploadPath.$id.'/';
                    foreach ($files as $file) {
                    $res = $this->s3_upload->copyexistfiles($srcpath,$destpath,$file);
                        if($res['status'] == 1){
                        $request_files_data = array("request_id" => $id,
                            "user_id" => $created_user,
                            "user_type" => "customer",
                            "file_name" => $file,
                            "created" => date("Y-m-d H:i:s"));
                            $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                        }
                    }
                }else{
                $srcpath = FCPATH.FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$duplid.'/';
                $destpath = FCPATH.FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$id.'/';
                foreach ($files as $file) {
                    copy($srcpath.$file, $destpath.$file);
                    $request_files_data = array("request_id" => $id,
                    "user_id" => $created_user,
                    "user_type" => "customer",
                    "file_name" => $file,
                    "created" => date("Y-m-d H:i:s"));
                    $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                }
                }
                $this->addFileRequest($id,$file_add);
                //$this->s_myfunctions->capture_project_activity($success,'','','project_created','add_new_request',$login_user_id,1);
                 if($ismanual == 0){
                    $update_data = $this->s_myfunctions->changerequeststatusafteradd($created_user,$id,$bucketType,$subcat_data,$profile_data);
                 }
                $newrequestdata = $this->S_request_model->get_request_by_id($id);
                if ($update_data || $id) {
                    $notifications = $this->S_request_model->get_notifications($login_user_id);
                    $notification_number = $this->S_request_model->get_notifications_number($login_user_id);
                    $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
                    $this->load->view('account/footer_customer');
                    $this->session->set_flashdata('message_success', 'We\'re now reviewing your request and your designer will get something back to you soon', 5);
                    redirect(base_url()."account/dashboard");
                }
            }
            }                      
        /************** end duplicate request **********************/
        $this->load->view('account/customer_header_1',array('profile'=>$profile_data));
         $this->load->view('account/add_new_request',array("data" => $_POST,'existeddata' => $existeddata,'duplicatedata' => $duplicatedata,'profile_data' => $profile_data, 'user_profile_brands' => $user_profile_brands,'canuseradd' => $canuseradd,'allcategories'=>$allcategories,'useraddrequest' => $useraddrequest,
             'allqust' => $allqust,'getquestans' => $getquestans,'samples' => $samples,'minval' => $getminMax[0]['minimum_sample'],'maxval' => $getminMax[0]['maximum_sample'],'selectdmaterials' => $selectdmaterials,'defult_status' => $defult_status,'ismanual' => $ismanual));
         $this->load->view('account/footer_customer');
    }
    
    public function addFileRequest($success,$is_transfer = ""){
        $uid = $this->load->get_var('main_user');
        if (!is_dir('design_requests/public/uploads/requests/' . $success)) {
                $data = mkdir('.design_requests/public/uploads/requests/' . $success, 0777, TRUE);
            }
            
            //Move files from temp folder
            // Get array of all source files
            $uploadPATH = $_SESSION['temp_folder_names'];
            $uploadPATHs = str_replace("./","",$uploadPATH);
            
            //echo $uploadPATHs;exit;
            $files = scandir($uploadPATH);
            // Identify directories
            $source = $uploadPATH.'/';
            
           // echo "<pre>"; print_r($files);exit;
            $destination = '.design_requests/public/uploads/requests/' . $success.'/';
            
            if(UPLOAD_FILE_SERVER == 'bucket'){
            
            foreach ($files as $file) {
                
            if (in_array($file, array(".",".."))) continue;
                
            $staticName = base_url().$uploadPATHs.'/'.$file;
           
            $config = array(
                'upload_path' => 'design_requests/public/uploads/requests/'. $success.'/',
                'file_name' => $file
            );
            
            $this->s3_upload->initialize($config);
           $is_file_uploaded = $this->s3_upload->upload_multiple_file($staticName);
         //  echo $is_file_uploaded."<pre>";print_r($is_file_uploaded);exit;
           if($is_file_uploaded['status'] == 1){
             $delete[] = $source.$file;  
             unlink('./tmp/tmpfile/' . basename($staticName));
            }
            }
            }else{
                 //echo $files.'test';exit;
              foreach ($files as $file) {
              if (in_array($file, array(".",".."))) continue;
              $filepath = APPPATH.$source.$file;
              if (copy($source.$file, $destination.$file)) {
                $delete[] = $source.$file;
              }
            }
            }
            foreach ($delete as $file) {
              unlink($file);
            }
           rmdir($source);
            unset($_SESSION['temp_folder_name']);
            //if($is_file_uploaded['status'] == 1){
            if(isset($_POST['delete_file']) && $_POST['delete_file'] != ''){
            foreach($_POST['delete_file'] as $SubFIlekey => $GetFIleName)
            {
                 $request_files_data = array("request_id" => $success,
                         "user_id" => $uid,
                         "user_type" => "customer",
                         "file_name" => $GetFIleName,
                         "image_link" => $_POST['image_link'][$SubFIlekey],
                         "created" => date("Y-m-d H:i:s"));
                 if($is_transfer == 'yes'){
                    $data = $this->Welcome_model->insert_data("request_files", $request_files_data);  
                 }else{
                    $data = $this->Welcome_model->insert_data("s_request_files", $request_files_data);
                 }
            }
            }
            //}
             /* else{
               $this->session->set_flashdata('message_error', "Something went wrong! Please Upload Preview  File Again.", 5);
               redirect(base_url() . "account/request/design_request");
            }*/
    }

    //new_request_brief
    public function new_request_brief_update($id = "") {
        $this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if (!empty($_POST)) {
            redirect(base_url() . "account/request/add_new_request");
            if (isset($_POST['proceed'])) {
                $plate = "";
                $fileType = "";
                if (isset($_POST['designColors'])) {
                    $plate = implode(",", $_POST['designColors']);
                }
                if (isset($_POST['filetype'])) {
                    $fileType = implode(",", $_POST['filetype']);
                }
                $data = array("customer_id" => $_SESSION['user_id'],
                    "title" => $_POST['project_title'],
                    "business_industry" => $_POST['industry_design'],
                    "description" => $_POST['description'],
                    "designer" => $this->input->post('designer'),
                    "deliverables" => $fileType,
                    "design_colors" => $plate
                );

                $success = $this->Welcome_model->update_data("requests", $data, array("id" => $id));

                if (!is_dir('public/uploads/requests/' . $success)) {
                    $data = mkdir('./public/uploads/requests/' . $success, 0777, TRUE);
                }
                //Move files from temp folder
                // Get array of all source files
                $uploadPATH = $_SESSION['temp_folder_names'];
                $files = scandir($uploadPATH);
                $uploadPATHs = str_replace("./","",$uploadPATH);
                // Identify directories
                $source = $uploadPATH . '/';
                $destination = './public/uploads/requests/' . $id . '/';
                // Cycle through all source files
                if(UPLOAD_FILE_SERVER == 'bucket'){
                    foreach ($files as $file) {

                        if (in_array($file, array(".", "..")))
                            continue;

                        $staticName = base_url() . $uploadPATHs . '/' . $file;
                        //echo $staticName;exit;
                        $config = array(
                            'upload_path' => 'public/uploads/requests/' . $id . '/',
                            'file_name' => $file
                        );
                       
                        $this->s3_upload->initialize($config);
                        $is_file_uploaded = $this->s3_upload->upload_multiple_file($staticName);
                        if($is_file_uploaded['status'] == 1){
                            $delete[] = $source . $file;
                            unlink('./tmp/tmpfile/' . basename($staticName));
                        }
                        
                    }
                }else {
                foreach ($files as $file) {
                    if (in_array($file, array(".", "..")))
                        continue;
                    // If we copied this successfully, mark it for deletion
                    if (copy($source . $file, $destination . $file)) {
                        $delete[] = $source . $file;
                    }
                }
                }
                // Delete all successfully-copied files
                foreach ($delete as $file) {
                    unlink($file);
                }
                rmdir($source);
                unset($_SESSION['temp_folder_name']);
 // echo "<pre/>";print_r($_POST['delete_file']);exit;
                // Insert data into database      
                if (isset($_POST['delete_file']) && $_POST['delete_file'] != '') {

                    foreach ($_POST['delete_file'] as $SubFIlekey => $GetFIleName) {
                        $request_files_data = array("request_id" => $id,
                            "user_id" => $_SESSION['user_id'],
                            "user_type" => "customer",
                            "file_name" => $GetFIleName,
                            "created" => date("Y-m-d H:i:s"));
                        $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                    }
                }
                if (isset($_GET['rep']) && $_GET['rep'] == 1) { 
                    redirect(base_url() . "account/request/new_request_review/" . $id . "?rep=1");
                } else {
                    redirect(base_url() . "account/request/new_request_review/" . $id);
                }
            }
        }
        redirect(base_url() . "account/request/add_new_request");
    }

    public function approve($id) {
        $uid = $this->load->get_var('main_user');
        //$uid = isset($_SESSION['id']) ? $_SESSION['id'] : '';
        $this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if ($id == "") {
            redirect(base_url() . "account/request/design_request");
        }
        $currentDateTime = date("Y-m-d H:i:s");
        $success = $this->Welcome_model->update_data("requests", array("status" => "approved", "status_admin" => "approved", "status_designer" => "approved", "status_qa" => "approved", 'dateinprogress' => $currentDateTime, 'latest_update' => $currentDateTime, 'approvaldate' => $currentDateTime, 'modified' => $currentDateTime), array("id" => $id));
        //$this->refreshStatus($uid);
        if ($success) {
            $this->session->set_flashdata('message_success', "Request approved successfully.", 5);
            redirect(base_url() . "account/request/project_info/" . $id);
        } else {
            $this->session->set_flashdata('message_error', "Error while updating the request status.", 5);
            redirect(base_url() . "account/request/project_info/" . $id);
        }
    }

    public function markAsCompleted($reqid,$permissiondata = NULL,$userid = NULL) {
        if ($reqid != 0 || $reqid == '') {
            if($userid != ""){
                $front_userdata = $this->S_admin_model->getuser_data($userid);
                if($front_userdata[0]['parent_id'] == 0){
                        $uid = $userid;
                }else{
                        $uid = $front_userdata[0]['parent_id'];
                }
                $login_user_id = $uid;
            }else{
                    $uid = $this->load->get_var('main_user');
                    $login_user_id = $this->load->get_var('login_user_id'); 
            }
            
           // $uid = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : '';
            $currentDateTime = date("Y-m-d H:i:s");
            $request_data = $this->S_request_model->get_request_by_id($reqid);
          //  $customer_data = $this->S_admin_model->getuser_data($request_data[0]['customer_id']);
            $ismanual = $this->s_myfunctions->checkmanualorauto($request_data);
            $table = $this->S_request_model->getDataBasedonSAAS($reqid);
            $login_user_id = $this->load->get_var('login_user_id'); 
          //  $admin_id = $this->load->get_var('admin_id');
            $success = $this->Welcome_model->update_data($table, array("status" => "approved","priority" => 0,"sub_user_priority" => 0, "status_admin" => "approved", "status_designer" => "approved", "status_qa" => "approved", 'dateinprogress' => $currentDateTime, 'latest_update' => $currentDateTime, 'approvaldate' => $currentDateTime, 'modified' => $currentDateTime), array("id" => $reqid));
            if($ismanual != 1){
                $this->refreshStatus($uid,'markAsCompleted',$reqid);
               // $this->s_myfunctions->capture_project_activity($reqid,'',$request_data[0]['designer_id'],$request_data[0]['status'].'_to_approved','markAsCompleted',$login_user_id,1);
            }
            if ($success) {
                
                /**  save notifications when upload draft design * */
//                if($request_data[0]['dummy_request'] != 1){
//                $this->s_myfunctions->show_notifications($reqid, $login_user_id, "Approved Project..!", "admin/dashboard/view_request/" . $reqid, $admin_id);
//                $this->s_myfunctions->show_notifications($reqid, $login_user_id, "Approved Project..!", "qa/dashboard/view_project/" . $reqid, $customer_data[0]['qa_id']);
//                $this->s_myfunctions->show_notifications($reqid, $login_user_id, "Approved Project..!", "designer/request/project_info/" . $reqid, $customer_data[0]['designer_id']);
//                }
                /** end save notifications when upload draft design * */
                $this->session->set_flashdata('message_success', "Request approved successfully.", 5);
                if(isset($permissiondata) && $permissiondata != ''){
                   // die($permissiondata);
                if($permissiondata == $reqid){
                        redirect(base_url() . "project_info_view/" . $permissiondata.'?id='.$userid);
                }else{
                redirect(base_url() . "project-info/" . $permissiondata);
				   }				   
                }else{
                    redirect(base_url() . "account/request/project_info/" . $reqid);
                }
            } else {
                $this->session->set_flashdata('message_error', "Error while updating the request status.", 5);
                redirect(base_url() . "account/request/project_info/" . $reqid);
            }
        }
    }

    public function markAsRevision($reqid) {
        //echo $reqid;exit;
        if ($reqid != 0 || $reqid == '') {
            $uid = $this->load->get_var('main_user');
            
           // $uid = isset($_SESSION['id']) ? $_SESSION['id'] : '';
            $currentDateTime = date("Y-m-d H:i:s");
            $customer_data = $this->S_admin_model->getuser_data($uid);
            $getsubscriptionplan = $this->S_request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
            if($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])){
                $turn_around_days = $customer_data[0]['turn_around_days'];
            }else{
                $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
            }
            $request_data = $this->S_request_model->get_request_by_id($reqid);
            $subcat_data = $this->S_category_model->get_category_byID($request_data[0]['subcategory_id']);
            $expected = $this->s_myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"),$customer_data[0]['plan_name'],$request_data[0]['category_bucket'],$subcat_data[0]['timeline'],$turn_around_days);
            $success = $this->Welcome_model->update_data("requests", array("status" => "disapprove", "status_admin" => "disapprove", "status_designer" => "disapprove", "status_qa" => "disapprove", 'dateinprogress' => $currentDateTime,"who_reject" => 1, 'latest_update' => $currentDateTime,"expected_date" => $expected, 'modified' => $currentDateTime), array("id" => $reqid));
            $this->refreshStatus($uid,'markAsrevesion',$reqid);
            if ($success) {
                $this->session->set_flashdata('message_success', "Status successfully changed..", 5);
                redirect(base_url() . "account/request/project_info/" . $reqid);
            } else {
                $this->session->set_flashdata('message_error', "Error while updating status.", 5);
                redirect(base_url() . "account/request/project_info/" . $reqid);
            }
        }
    }

    public function disapprove($id) {

        $this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if ($id == "") {
            redirect(base_url() . "account/request/design_request");
        }
        $success = $this->Welcome_model->update_data("requests", array("status" => "disapprove", "status_admin" => "disapprove", "status_designer" => "disapprove","who_reject" => 1), array("id" => $id));
        if ($success) {
            $this->session->set_flashdata('message_success', "Request is Disapproved Successfully..!", 5);
            redirect(base_url() . "account/request/project_info/" . $id);
        } else {
            $this->session->set_flashdata('message_error', "Request is not Disapproved Successfully..!", 5);
            redirect(base_url() . "account/request/project_info/" . $id);
        }
    }

    public function request_response() { 
//        echo "<pre/>";print_R($_POST);exit;
      //  echo "hello";exit;
        $result = array();
       // $this->s_myfunctions->checkloginuser("customer");
        if(isset($_POST['front_user_id']) && $_POST['front_user_id'] != ""){
            $front_userdata = $this->S_admin_model->getuser_data($_POST['front_user_id']);
            if($front_userdata[0]['parent_id'] == 0){
                    $uid = $_POST['front_user_id'];
            }else{
                    $uid = $front_userdata[0]['parent_id'];
            }
            $login_user_id = $uid;
        }else{
            $uid = $this->load->get_var('main_user');
            $login_user_id = $this->load->get_var('login_user_id'); 
        } 
        $this->Welcome_model->update_online();
        $is_reveiw = $this->S_request_model->get_reviewfeedback($_POST['fileid'],'customer');
        $request_data = $this->S_request_model->get_request_by_id($_POST['request_id']);
        $ismanual = $this->s_myfunctions->checkmanualorauto($request_data);
        $table = $this->S_request_model->getDataBasedonSAAS($_POST['request_id']);
        if($table == "requests"){
            $tb = "request_files";
        }else{
            $tb = "s_request_files";
        }
        $success = $this->Welcome_model->update_data($tb, array("modified" => date("Y:m:d H:i:s"), "status" => $_POST['filestatus']), array("id" => $_POST['fileid']));
        if ($success) {
            if ($_POST['value'] == "Reject") {
                $_POST['value'] = "disapprove";
                $this->Welcome_model->update_data($table, array("status" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "status_qa" => $_POST['value'],"who_reject" => 1), array("id" => $_POST['request_id']));
                if($request_data[0]['dummy_request'] != 1){
               // $this->s_myfunctions->show_notifications($_POST['request_id'],$login_user_id,"Your Design For \"".$request_data[0]['title']."\" is Rejected..!","designer/request/project_info/" . $_POST['request_id'],$request_data[0]['designer_id']);
                }
            } else {
                $_POST['value'] = "approved";
                $this->Welcome_model->update_data($table, array("approvaldate" => date("Y:m:d H:i:s"), "status" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "status_qa" => $_POST['value'], "approvaldate" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if($ismanual != 1){
                    $customer_data = $this->S_admin_model->getuser_data($uid);
                    $ActiveReq = $this->s_myfunctions->CheckActiveRequest($uid);
                    if ($ActiveReq == 1) {
                        $this->s_myfunctions->move_project_inqueqe_to_inprogress($uid,'','request_response_incustomer',$_POST['request_id']);
                    }
//                $this->s_myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'','approved_design_by_customer','request_response_incustomer',$login_user_id);
//                if($request_data[0]['dummy_request'] != 1){
//                $this->myfunctions->show_notifications($_POST['request_id'],$login_user_id,"Your Project \"".$request_data[0]['title']."\" has been Approved..!","designer/request/project_info/" . $_POST['request_id'],$request_data[0]['designer_id']);
//                $this->s_myfunctions->show_notifications($_POST['request_id'],$login_user_id,"Your Project \"".$request_data[0]['title']."\" has been Approved..!","admin/dashboard/view_request/" . $_POST['request_id'],$admin_id);
//                $this->s_myfunctions->show_notifications($_POST['request_id'],$login_user_id,"Your Project \"".$request_data[0]['title']."\" has been Approved..!","qa/dashboard/view_project/" . $_POST['request_id'],$customer_data[0]['qa_id']);
//                }
                }
            }
            
        }
        $modified_date = $this->onlytimezone(date("Y:m:d H:i:s"));
        $result['modified'] = date("M d, Y h:i A", strtotime($modified_date));
        $result['success'] = $success;
        if($is_reveiw){
        $result['is_reveiw'] = $is_reveiw;
        }
        echo json_encode($result);
        exit;
    }

    public function design_rating() {
        $this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if (!empty($_POST)) {
            if ($this->S_admin_model->update_data("request_files", array("customer_grade" => $_POST['customer_rating']), array("id" => $_POST['id']))) {
                $this->session->set_flashdata('message_success', "Rating Success..!", 5);
            } else {
                $this->session->set_flashdata('message_error', "Rating Not Success..!", 5);
            }
            redirect(base_url() . "account/request/project_image_view/" . $_POST['id'] . "?id=" . $_POST['request_id']);
        }
    }

    public function submit_grade_from_view_request() {
        $this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if (!empty($_POST)) {
            $this->S_admin_model->update_data("request_files", array("customer_grade" => $_POST['number']), array("id" => $_POST['id']));
        }
    }

    public function designer_rating_ajax() {
        $this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        echo $this->S_admin_model->update_data("request_files", array("customer_grade" => $_POST['value']), array("id" => $_POST['fileid']));
    }

    public function customer_chat_ajax() {

        $designer_file = $this->S_admin_model->get_requested_files_ajax($_POST['fileid'], $_POST['id'], "", "designer", "1");
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            $designer_file[$i]['chat'] = $this->S_request_model->get_chat_by_id($designer_file[$i]['id'], "desc");
        }
        $user = $this->Account_model->getuserbyid($_SESSION['user_id']);
        $request = $this->S_request_model->get_request_by_id($_POST['id']);
        $this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        $ap = $designer_file[0]['status'];
        if ($ap == "Approve") {
            echo '<span class="label">Approved</span>';
        }
        if ($ap == "Reject") {
            echo '<span class="label" style ="background:#e42244 !important">Rejected</span>';
        }
        ?>
        <div class="col-md-12 "  style="padding:10px;">
            <image src="<?php echo FS_PATH_PUBLIC_UPLOADS_REQUESTS . $_POST['id'] . "/" . $designer_file[0]['file_name']; ?>" style="max-height:100%;width:100%;max-height: 600px;height: 600px;margin: auto;display: block;"  />
         <!--   <div onclick="$('.open_<?php echo $designer_file[0]['id']; ?>').toggle();" style="height:20px;width:20px;border-radius:50%;    background: red;position: absolute;top: 50px;left: 50px;"></div>-->
        </div>
        <div class="appenddot_<?php echo $designer_file[0]['id']; ?>">
            <!-- Chat toolkit start-->
            <?php
            for ($j = 0; $j < sizeof($designer_file[0]['chat']); $j++) {
                if ($designer_file[0]['chat'][$j]['sender_role'] == "customer") {
                    $background = "pinkbackground";
                } else {
                    $background = "bluebackground";
                }
                ?>
                <?php
                if ($designer_file[0]['chat'][0]['xco'] != "") {
                    if ($designer_file[0]['chat'][$j]['sender_role'] == "customer") {
                        $image1 = "dot_image1.png";
                        $image2 = "dot_image2.png";
                    } else {
                        $image1 = "designer_image1.png";
                        $image2 = "designer_image2.png";
                    }
                    ?>
                    <div class="<?php //echo $background;          ?>" style="position:absolute;left:<?php echo $designer_file[0]['chat'][$j]['xco']; ?>px;top:<?php echo $designer_file[0]['chat'][$j]['yco']; ?>px;width:25px;z-index:99999999999999999;height:25px;" onclick='show_customer_chat(<?php echo $designer_file[0]['chat'][$j]['id']; ?>);'>

                        <img src="<?php echo base_url() . "public/" . $image1; ?>" class="customer_chatimage1 customer_chatimage<?php echo $designer_file[0]['chat'][$j]['id']; ?>"/>
                        <img style="display:none;" src="<?php echo base_url() . "public/" . $image2; ?>" class="customer_chatimage2 customer_chatimage<?php echo $designer_file[0]['chat'][$j]['id']; ?>"/>
                    </div>
                    <div class="customer_chat customer_chat<?php echo $designer_file[0]['chat'][$j]['id']; ?>" style="display:none;background:#fff;position:absolute;left:<?php echo $designer_file[0]['chat'][$j]['xco'] + 25; ?>px;top:<?php echo $designer_file[0]['chat'][$j]['yco'] + 10; ?>px;padding: 5px;border-radius: 5px;">
                        <label><?php echo $designer_file[0]['chat'][$j]['message']; ?></label>
                    </div>
                <?php }
                ?>
            <?php } ?> 
        </div>
        <div class="openchat" style="width:35%;position:absolute;display:none;padding:10px 20px;background:#fff;">
            <textarea onClick="event.stopPropagation();" class='form-control text_write text_<?php echo $designer_file[0]['id']; ?>' style="border: 1px solid !important;border-radius: 6px !important;padding-right:30px;margin-bottom:5px;" placeholder="Leave a comment" onkeydown="javascript: if (event.keyCode == 13) {
                                $('.send_text<?php echo $designer_file[0]['id']; ?>').click();
                            }"></textarea>
            <button onClick="send_request_img_chat('<?php echo $designer_file[0]['id']; ?>');
                            event.stopPropagation();" class="pinkbackground send_request_img_chat send_text send_text<?php echo $designer_file[0]['id']; ?>" style="border: none;border-radius: 5px;padding: 1px 10px;" data-fileid="<?php echo $designer_file[0]['id']; ?>" 
                    data-senderrole="customer" 
                    data-senderid="<?php echo $_SESSION['user_id']; ?>" 
                    data-receiverid="<?php echo $request[0]['designer_id']; ?>" 
                    data-receiverrole="designer"
                    data-customername="<?php echo $user[0]['first_name'] . " " . $user[0]['last_name']; ?>"
                    data-xco="" data-yco=""><i class="fa fa-send whitetext"></i></button>
            <span onclick="mycancellable(<?php echo $designer_file[0]['id']; ?>);
                            event.stopPropagation();" class="greytext mycancellable mycancellable<?php echo $designer_file[0]['id']; ?> " style="font-size:10px;cursor:pointer;">Cancel</span>
        </div>
        <?php
    }

    public function design_rating2($id, $file_id, $no) {
        $this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        if ($this->S_admin_model->update_data("request_files", array("customer_grade" => $no), array("id" => $file_id))) {
            $this->session->set_flashdata('message_success', "Rating Success..!", 5);
        } else {
            $this->session->set_flashdata('message_error', "Rating Not Success..!", 5);
        }
        redirect(base_url() . "account/request/project_image_view/" . $file_id . "?id=" . $id);
    }

    public function log_out() {
        //$this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_data("users", array("online" => 0), array("id" => $_SESSION['user_id']));
        $this->S_request_model->logout();
        redirect(base_url());
    }

    public function suggest_view() {
        $this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        $success = $this->Welcome_model->update_data("request_files", array("status" => "Reject"), array("request_id" => $_POST['requestid']));

        if ($success) {

            $_POST['value'] = "disapprove";

            $this->Welcome_model->update_data("requests", array("status" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value']), array("id" => $_POST['requestid']));

            $this->Welcome_model->insert_data("request_discussions", array("request_id" => $_POST['requestid'], "sender_type" => $_POST['senderrole'], "sender_id" => $_POST['senderid'], "reciever_id" => $_POST['receiverid'], "reciever_type" => $_POST['receiverrole'], "message" => $_POST['suggest'], "created" => date('Y-m-d H:i:s')));

            $this->session->set_flashdata('message_error', 'Desigen reject is Successfully.!', 5);
            redirect(base_url() . "account/request/project_image_view/" . $_POST['fileid'] . "?id=" . $_POST['requestid']);
        }
        echo $success;
    }

    public function load_messages_from_designer() {
        $project_id = $_POST['project_id'];
        $customer_id = $_POST['cst_id'];
        $designer_id = $_POST['desg_id'];
        $msg_seen = $_POST['customerseen'];
        $chat_request_msg = $this->S_request_model->get_customer_unseen_msg($project_id, $customer_id, $designer_id, $msg_seen);
        //  echo "<pre>";print_R($chat_request_msg);
        if (!empty($chat_request_msg)) {
            $message['message'] = $chat_request_msg[0]['message'];
            $message['profile_picture'] = isset($chat_request_msg[0]['profile_picture']) ? $chat_request_msg[0]['profile_picture'] : 'user-admin.png';
            $message['typee'] = $chat_request_msg[0]['sender_type'];
            $message['msg_first_name'] = $chat_request_msg[0]['first_name'];
            echo json_encode($message);
            $this->S_admin_model->update_data("request_discussions", array("customer_seen" => 1), array("request_id" => $project_id));
        } else {
            // $message = "Hello Everyone";
            // echo $message;
        }
    }

    public function design_request_1() {
        $this->s_myfunctions->checkloginuser("customer");
        $created_user = $this->load->get_var('main_user');
        $parent_user_plan = $this->load->get_var('parent_user_plan');
        $login_user_id = $this->load->get_var('login_user_id'); 
        $profile_data = $this->S_admin_model->getuser_data($login_user_id);
        $userid = $_SESSION['user_id'];
        $canuserdel = $this->s_myfunctions->isUserPermission('delete_req');
        $canaccessallbrands = $this->s_myfunctions->isUserPermission('brand_profile_access');
	    $can_manage_priorities = $this->s_myfunctions->isUserPermission('manage_priorities');
        $canuseradd = $this->s_myfunctions->isUserPermission('add_requests');
        $assign_designer_toproject = $this->s_myfunctions->isUserPermission('assign_designer_to_project');
        $change_project_status = $this->s_myfunctions->isUserPermission('change_project_status');
        
//        echo $canuseradd;exit;
        $trail_user = $this->S_request_model->getuserbyid($created_user);
        $brand_id = isset($_GET['brand_id']) ? $_GET['brand_id'] : "";
        $client_id = isset($_GET['client_id']) ? $_GET['client_id'] : "";
        
        $all_status = $this->get_statustab_acc_user();
        $count_active_project = $this->S_request_model->customer_load_more($all_status["active"], $login_user_id, true, '', '', 'created','','','',$all_status["by"]);
        $active_project = $this->S_request_model->customer_load_more($all_status["active"], $login_user_id,'', '', '', 'created','','','',$all_status["by"]);
        //echo "<pre>";print_r($active_project);
        $request_for_trial = $this->S_request_model->getall_request_for_trial_user($created_user);
        $request_for_all_status_acc_billing_cycle = $this->S_request_model->getall_request_for_all_status($created_user,$trail_user[0]['billing_start_date'],$trail_user[0]['billing_end_date']);
        $useraddrequest = $this->s_myfunctions->canUserAddNewRequest($trail_user,$request_for_all_status_acc_billing_cycle,$request_for_trial);
        $assign_requests = $this->S_request_model->getallrequestcount_accstatus($login_user_id, array('assign'));
        $subuser_assign_requests = $this->S_request_model->getallrequestcount_accstatus($created_user, array('assign'),$login_user_id);
       
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['user_data'] = $profile_data;
            $active_project[$i]['customer_variables'] = $this->s_myfunctions->customerVariables($active_project[$i]['status']);
            $active_project[$i]['total_chat'] = $this->S_request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
            $getlatestfiledraft = $this->S_request_model->get_latestdraft_file($active_project[$i]['id']);
            $active_project[$i]['latest_draft'] = $getlatestfiledraft[0]['file_name'];
            if ($active_project[$i][$all_status["by"]] == "active" || $active_project[$i][$all_status["by"]] == "disapprove") {
               if($active_project[$i]['expected_date'] == '' || $active_project[$i]['expected_date'] == NULL){
                $active_project[$i]['expected'] = $this->s_myfunctions->check_timezone($active_project[$i]['latest_update'],$parent_user_plan[0]['plan_id']);
                }else{
                $active_project[$i]['expected'] = $this->onlytimezone($active_project[$i]['expected_date']);
                }
            } elseif ($active_project[$i][$all_status["by"]] == "checkforapprove") {
                $active_project[$i]['deliverydate'] = $this->onlytimezone($active_project[$i]['approvaldate']);
            }
            $active_project[$i]['max_priority'] = $assign_requests;
            $active_project[$i]['subuser_max_priority'] = $subuser_assign_requests;
            $id = $active_project[$i]['id'];
            $data = $this->S_request_model->get_request_by_id($id);
            $active_project[$i]['c_name'] = $this->S_request_model->getuserbyid($active_project[$i]['customer_id'],'client');
            $active_project[$i]['client_name'] = $this->S_request_model->getuserbyid($active_project[$i]['created_by'],'client');
            $active_project[$i]['al_status'] = $all_status;
            $active_project[$i]['addClass'] = $this->adminCheckClassforDesignerAssign($active_project[$i][$all_status["by"]]);
        }
        $draft_project_count = $this->S_request_model->customer_load_more(array("draft"), $login_user_id, true, '', '', 'created','','','',$all_status["by"]);
        $complete_project_count = $this->S_request_model->customer_load_more(array("approved"), $login_user_id, true, '', '', 'created','','','',$all_status["by"]);
        $cancel_project_count = $this->S_request_model->customer_load_more(array("cancel"), $login_user_id, true, '', '', 'created','','','',$all_status["by"]);
        $hold_project_count = $this->S_request_model->customer_load_more(array("hold"), $login_user_id, true, '', '', 'created','','','',$all_status["by"]);
        $count_project_i = $this->S_request_model->customer_load_more(array('assign', 'pending'), $login_user_id, true, '', '', 'created','','','',$all_status["by"]);
        $count_project_p = $this->S_request_model->customer_load_more(array('pendingrevision'), $login_user_id, true, '', '', 'created','','','',$all_status["by"]);
        $count_project_checkforap = $this->S_request_model->customer_load_more(array("pendingforapprove", "checkforapprove"), $login_user_id, true, '', '', 'created','','','',$all_status["by"]);

        $notifications = $this->S_request_model->get_notifications($login_user_id);
        $notification_number = $this->S_request_model->get_notifications_number($login_user_id);
        $messagenotifications = $this->S_request_model->get_messagenotifications($login_user_id);
        $messagenotification_number = $this->S_request_model->get_messagenotifications_number($login_user_id);
        $canbrand_profile_access = $this->s_myfunctions->isUserPermission('brand_profile_access');
        if($canbrand_profile_access != 0){
        $user_profile_brands = $this->S_request_model->get_brand_profile_by_user_id($created_user);
        }elseif($canbrand_profile_access == 0 && $profile_data[0]['user_flag'] == 'client'){
           $user_profile_brands = $this->S_request_model->get_brand_profile_by_user_id($login_user_id,'created_by');
        }else{
        $user_profile_brand = $this->S_request_model->get_user_brand_profile_by_user_id($login_user_id);
        if(sizeof($user_profile_brand) > 0){
            foreach ($user_profile_brand as $user_brands_info) {
               $brand_ids[] =  $user_brands_info['brand_id'];
            }
            $user_profile_brands = $this->S_request_model->get_brandprofile_by_id('',$brand_ids);
        }
        }
        $subusersdata = array();
        if($profile_data[0]['user_flag'] != 'client'){
          $subusersdata = $this->S_request_model->getAllsubUsers($created_user,"client","","","id,first_name,last_name,profile_picture");
        }
        $designer_list = $this->S_admin_model->get_total_customer("designer");
//        echo "<pre/>";print_R($designer_list);
        for ($i = 0; $i < sizeof($designer_list); $i++) {
            $user = $this->S_request_model->get_all_active_request_by_designer($designer_list[$i]['id']);
            $designer_list[$i]['profile_picture'] = $this->s_customfunctions->getprofileimageurl($data['designer_list'][$i]['profile_picture']);
            $designer_list[$i]['active_request'] = sizeof($user);
        }
        
        $index_data = array("active_project" => $active_project,'count_active_project' => $count_active_project, 'draft_count' => $draft_project_count, 'complete_project_count' => $complete_project_count, 'trial' => $trail_user, 'request_for_trial' => $request_for_trial,'billing_cycle_request' => $request_for_all_status_acc_billing_cycle,'canuseraddrequest' => $useraddrequest, 'user_profile_brands' => $user_profile_brands,'canuserdel' => $canuserdel,'canuseradd' => $canuseradd,'canaccessallbrands'=>$canaccessallbrands,'can_manage_priorities'=>$can_manage_priorities,'cancel_project_count'=>$cancel_project_count,'profile_data'=>$profile_data,"subusersdata"=>$subusersdata,'hold_project_count' => $hold_project_count,"count_project_i" => $count_project_i,"count_project_p" => $count_project_p,"count_project_checkforap" => $count_project_checkforap,"u_role" => $all_status["user"],"designer_list" => $designer_list,"assign_designer_toproject" => $assign_designer_toproject,"change_project_status" => $change_project_status);
        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));
        $this->load->view('account/footer_customer',array('data' => $trail_user));
        if($profile_data[0]['role'] == 'customer' || $profile_data[0]['role'] == 'manager'){
            $this->load->view('account/design_request_1',$index_data);
        }else{
            $this->load->view('account/designer_project_list',$index_data);
        }
    }
    
    public function set_priority() {
        $parent_user_plan = $this->load->get_var('parent_user_plan');
        $priorityfrom = isset($_GET['priorityfrom'])? $_GET['priorityfrom'] : '';
        $priorityto = isset($_GET['priorityto'])? $_GET['priorityto'] : '';
        $priority_user_type = isset($_GET['priority_user_type'])? $_GET['priority_user_type'] : '';
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $userid = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        if($priority_user_type != ""){
        $mainfrom_prority = $this->S_request_model->getmain_priorityfrom_requests($priorityfrom,$login_user_id);
        $mainto_prority = $this->S_request_model->getmain_priorityfrom_requests($priorityto,$login_user_id);
        }
//        echo $priorityfrom.'-from,to-'.$priorityto;
//        echo "<pre>";print_r($mainfrom_prority);
//        echo "<pre>";print_r($mainto_prority);
        //$userid = ($_SESSION['user_id']) ? $_SESSION['user_id'] : ''; 
        if (isset($userid) && $userid != '') {
            // die($userid);
            if ($priorityfrom < $priorityto) {
                for ($i = $priorityfrom + 1; $i <= $priorityto; $i++) {
                    if($priority_user_type == ""){
                     $this->S_request_model->update_priority($i, $i - 1, '', $userid);
                    }else{
                     $this->S_request_model->update_sub_userpriority($i, $i - 1, '', $login_user_id);    
                    }
                }
                //$this->S_request_model->update_priority($priorityfrom,$priorityto, $id);
            } else if ($priorityfrom > $priorityto) {
                for ($i = $priorityfrom - 1; $i >= $priorityto; $i--) {
                    if($priority_user_type == ""){
                     $this->S_request_model->update_priority($i, $i + 1, '', $userid);
                    }else{
                     $this->S_request_model->update_sub_userpriority($i, $i + 1, '', $login_user_id);    
                    }
                }
                // $this->S_request_model->update_priority($priorityfrom,$priorityto, $id);
            }
        }
        if($priority_user_type == ""){
        $this->S_request_model->update_priority($priorityfrom, $priorityto, $id);
        }else{
           
           //update sub user priority
           $this->S_request_model->update_sub_userpriority($priorityfrom, $priorityto, $id); 
           
           //update main user priority
           $this->S_request_model->update_priority($mainfrom_prority[0]['priority'], $mainto_prority[0]['priority'], $mainfrom_prority[0]['id']);
           $this->S_request_model->update_priority($mainto_prority[0]['priority'], $mainfrom_prority[0]['priority'], $mainto_prority[0]['id']);
        }
        $active_project = $this->S_request_model->getall_request($userid, array(''), '', 'priority');
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['total_chat'] = $this->S_request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
            $getfileid = $this->S_request_model->get_attachment_files($active_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->S_request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
               if ($active_project[$i]['expected_date'] == '' || $active_project[$i]['expected_date'] == NULL) {
                   $active_project[$i]['expected'] = $this->s_myfunctions->check_timezone($active_project[$i]['latest_update'],$parent_user_plan[0]['plan_id']);
                } else {
                   $active_project[$i]['expected'] = $this->onlytimezone($active_project[$i]['expected_date']);
                }
            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                $active_project[$i]['deliverydate'] = $this->onlytimezone($active_project[$i]['latest_update']);
            } elseif ($active_project[$i]['status'] == "approved") {
                $active_project[$i]['approvddate'] = $this->onlytimezone($active_project[$i]['approvaldate']);
            }
            $active_project[$i]['comment_count'] = $commentcount;
            /* new */
            $id = $active_project[$i]['id'];
            $data = $this->S_request_model->get_request_by_id($id);
            $active_project[$i]['files_new'] = $this->S_request_model->get_new_attachment_files_for_customer($active_project[$i]['id'], array('customerreview', 'Approve'));
            $active_project[$i]['files'] = $this->S_request_model->get_attachment_files_for_customer($active_project[$i]['id'], array('customerreview', 'Approve'), "designer");
            // for ($j = 0; $j < sizeof($active_project[$i]['files']); $j++) {
            // }
        }
        $activeTab = 0;
        foreach ($active_project as $project) {
            if ($project['status'] == "assign") {
                $activeTab++;
            }
        }
        for ($i = 0; $i < count($active_project); $i++) {
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "checkforapprove" || $active_project[$i]['status'] == "disapprove") {
                ?>
                <!-- Pro Box Start -->
                <div class="col-md-2 col-sm-6">
                    <div class="pro-productss pro-box-r1">
                        <div class="pro-box-r2">
                            <div class="pro-inbox-r1">
                                <div class="pro-inleftbox">
                                    <p class="pro-a">
                                        <?php
                                        if ($active_project[$i]['status'] == "active") {
                                            echo "Expected on";
                                        } elseif ($active_project[$i]['status'] == "checkforapprove") {
                                            echo "Delivered on";
                                        } elseif ($active_project[$i]['status'] == "disapprove") {
                                            echo "Expected on";
                                        }
                                        ?>
                                    </p>
                                    <p class="pro-b"><a href="">
                                            <?php
                                            if ($active_project[$i]['status'] == "active") {
                                                echo date('m/d/Y', strtotime($active_project[$i]['expected']));
                                            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                                                echo date('m/d/Y', strtotime($active_project[$i]['deliverydate']));
                                            } elseif ($active_project[$i]['status'] == "disapprove") {
                                                echo date('m/d/Y', strtotime($active_project[$i]['expected']));
                                            }
                                            ?>
                                        </a></p>
                                </div>
                                <div class="pro-inrightbox">
                                    <?php if ($active_project[$i]['status'] != "assign") { ?>
                                        <p class="green">Active</p>
                                    <?php } else { ?>
                                        <p class="pro-a">Priority </p>
                                        <p class="select-pro-a">
                                            <select onchange="prioritize(this.value, <?php echo $active_project[$i]['priority'] ?>,<?php echo $active_project[$i]['id'] ?>)">
                                                <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                                    <option value="<?php echo $j; ?>"
                                                    <?php
                                                    if ($j == $active_project[$i]['priority']) {
                                                        echo "selected";
                                                    }
                                                    ?>
                                                            ><?php echo $j; ?></option>                                             
                                        <?php } ?>
                                            </select>
                                        </p>
                <?php } ?>
                                </div>
                            </div>
                            <hr class="hr-a">   
                            <!-- <script type="text/javascript">alert("<?php echo $active_project[$i]['id']; ?>")</script> -->
                            <a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $active_project[$i]['id']; ?>">
                                <h3 class="pro-head space-b"><?php echo $active_project[$i]['title']; ?></h3>
                                <p class="pro-b"><?php echo $active_project[$i]['category']; ?></p>
                                <p class="space-a"></p>
                                <p class="neft">
                                    <?php
                                    if ($active_project[$i]['status'] == "checkforapprove") {
                                        $status = "Review your design";
                                        $color = "bluetext";
                                    } elseif ($active_project[$i]['status'] == "active") {
                                        $status = "Design In Progress";
                                        $color = "greentext";
                                    } elseif ($active_project[$i]['status'] == "disapprove") {
                                        $status = "Revision In Progress";
                                        $color = "orangetext ";
                                    } elseif ($active_project[$i]['status'] == "pending" || $active_project[$i]['status'] == "assign") {
                                        $status = "In Queue";
                                        $color = "greentext ";
                                    } else {
                                        $status = "";
                                        $color = "greentext ";
                                    }
                                    //echo $status;
                                    ?>
                                        <?php if ($status == 'In Queue'): ?>
                                        <span class="gray text-uppercase">
                                        <?php echo $status; ?>
                                        </span>
                                        <?php else: ?>
                                        <span class="green <?php echo $color; ?> text-uppercase">
                                        <?php echo $status; ?>
                                        </span>
                <?php endif ?>
                                </p>
                            </a>
                        </div>

                        <div class="pro-box-r3">
                            <p class="pro-a">Designer</p>
                            <p class="space-a"></p>
                            <div class="pro-circle-list clearfix">
                                <div class="pro-circle-box">
                                    <a href=""><figure class="pro-circle-img">
                                            <?php if ($active_project[$i]['profile_picture']) { ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $active_project[$i]['profile_picture']; ?>" class="img-responsive" />
                                            <?php } else { ?>
                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                <?php } ?>
                                        </figure>
                                        <p class="pro-circle-txt text-center"><?php echo $active_project[$i]['designer_first_name']; ?></p></a>
                                </div>
                            </div>
                        </div>

                        <div class="pro-box-r4">
                            <div class="pro-inbox-r1">
                                <div class="pro-inleftbox">
                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>account/request/project-info/<?php echo $active_project[$i]['id']; ?>"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php if ($active_project[$i]['total_chat'] + $active_project[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                <?php echo $active_project[$i]['total_chat'] + $active_project[$i]['comment_count']; ?>
                                                </span>
                <?php } ?></a>
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        <a style="position: relative;" href="<?php echo base_url() ?>account/request/project-info/<?php echo $active_project[$i]['id']; ?>"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13"> 
                                                    <?php if (count($active_project[$i]['files_new']) != 0) { ?>
                                                    <span class="numcircle-box">
                                                    <?php echo count($active_project[$i]['files_new']); ?>
                                                    </span>
                                            <?php } ?>
                                            </span>
                <?php echo count($active_project[$i]['files']); ?>
                                        </a>
                                    </p>
                                </div>
                                <div class="pro-inrightbox">
                                    <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>account/request/deleteproject/<?php echo $active_project[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- Pro Box End -->
                <?php
            }
        }
        for ($i = 0; $i < count($active_project); $i++) {
            if ($active_project[$i]['status'] != "active" && $active_project[$i]['status'] != "checkforapprove" && $active_project[$i]['status'] != "disapprove") {
                ?>
                <!-- Pro Box Start -->
                <div class="col-md-2 col-sm-6">
                    <div class="pro-productss pro-box-r1">
                        <div class="pro-box-r2">
                            <div class="pro-inbox-r1"  style="padding: 7px 0px;">
                                <div class="pro-inleftbox">
                                    <p class="pro-a">In-Queue</p>
                                    <!-- <p class="pro-b"><a href="">
                                    <?php
                                    // $created_date =  date($active_project[$i]['dateinprogress']);
                                    // $expected_date = date('m/d/Y', strtotime($created_date . ' +1 day'));
                                    // if ( $active_project[ $i ][ 'modified' ] == "" ) {
                                    //     echo $expected_date;
                                    // } else {
                                    //     echo date( "m/d/Y", strtotime( $active_project[ $i ][ 'modified' ] ) );
                                    // }
                                    ?>
                                    </a></p> -->
                                </div>
                                <div class="pro-inrightbox">
                                    <?php if ($active_project[$i]['status'] != "assign") { ?>
                                        <p class="green">Active</p>
                <?php } else { ?>
                                        <p class="select-pro-a pro-a">
                                            Priority
                                            <select onchange="prioritize(this.value, <?php echo $active_project[$i]['priority'] ?>,<?php echo $active_project[$i]['id'] ?>)">
                                                <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                                    <option value="<?php echo $j; ?>"
                                                    <?php
                                                    if ($j == $active_project[$i]['priority']) {
                                                        echo "selected";
                                                    }
                                                    ?>
                                                            ><?php echo $j; ?></option>                                             
                    <?php } ?>
                                            </select>
                                        </p>
                <?php } ?>
                                </div>
                            </div>
                            <hr class="hr-a">   
                            <!-- <script type="text/javascript">alert("<?php echo $active_project[$i]['id']; ?>")</script> -->
                            <a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $active_project[$i]['id']; ?>">
                                <h3 class="pro-head space-b"><?php echo $active_project[$i]['title']; ?></h3>
                                <p class="pro-b"><?php echo $active_project[$i]['category']; ?></p>
                                <p class="space-a"></p>
                                <p class="neft">
                                    <?php
                                    if ($active_project[$i]['status'] == "checkforapprove") {
                                        $status = "Review your design";
                                        $color = "bluetext";
                                    } elseif ($active_project[$i]['status'] == "active") {
                                        $status = "Design In Progress";
                                        $color = "greentext";
                                    } elseif ($active_project[$i]['status'] == "disapprove") {
                                        $status = "Revision In Progress";
                                        $color = "orangetext ";
                                    } elseif ($active_project[$i]['status'] == "pending" || $active_project[$i]['status'] == "assign") {
                                        $status = "In Queue";
                                        $color = "greentext ";
                                    } else {
                                        $status = "";
                                        $color = "greentext ";
                                    }
                                    //echo $status;
                                    ?>
                                    <?php if ($status == 'In Queue'): ?>
                                        <span class="gray text-uppercase">
                                            <?php echo $status; ?>
                                        </span>
                                    <?php else: ?>
                                        <span class="green <?php echo $color; ?> text-uppercase">
                    <?php echo $status; ?>
                                        </span>
                <?php endif ?>
                                </p>
                            </a>
                        </div>

                        <div class="pro-box-r3">
                            <p class="pro-a">Designer</p>
                            <p class="space-a"></p>
                            <div class="pro-circle-list clearfix">
                                <div class="pro-circle-box">
                                    <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                    <!--  <a href=""><figure class="pro-circle-img">
                                    <?php if ($active_project[$i]['profile_picture']) { ?>
                                         <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $active_project[$i]['profile_picture']; ?>" class="img-responsive" />
                <?php } else { ?>
                                         <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                <?php } ?>
                                     </figure>
                                     <p class="pro-circle-txt text-center"><?php echo $active_project[$i]['designer_first_name']; ?></p></a> -->
                                </div>
                            </div>
                        </div>

                        <div class="pro-box-r4">
                            <div class="pro-inbox-r1">
                                <div class="pro-inleftbox">
                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>account/request/project-info/<?php echo $active_project[$i]['id']; ?>"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php if ($active_project[$i]['total_chat'] + $active_project[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                    <?php echo $active_project[$i]['total_chat'] + $active_project[$i]['comment_count']; ?>
                                                </span>
                                                <?php } ?></a>
                                        &nbsp;
                                        &nbsp;
                                        &nbsp;
                                        <a style="position: relative;" href="<?php echo base_url() ?>account/request/project-info/<?php echo $active_project[$i]['id']; ?>"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                                                <?php if (count($active_project[$i]['files_new']) != 0) { ?>
                                                    <span class="numcircle-box">
                                                <?php echo count($active_project[$i]['files_new']); ?>
                                                    </span>
                <?php } ?>
                                            </span>
                <?php echo count($active_project[$i]['files']); ?></a>
                                    </p>
                                </div>
                                <div class="pro-inrightbox">
                                    <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>account/request/deleteproject/<?php echo $active_project[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- Pro Box End -->
                <?php
            }
        }
    }

    public function set_priority_for_listview($priorityto, $priorityfrom, $id) {
        $parent_user_plan = $this->load->get_var('parent_user_plan');
        $userid = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        //$userid = $_SESSION['user_id'];
        $active_projects = $this->S_request_model->getall_request($userid, array(), '', 'priority');
        $activeTab = 0;
        foreach ($active_projects as $project) {
            if ($project['status'] == "assign") {
                $activeTab++;
            }
        }
        if ($priorityfrom < $priorityto) {
            for ($i = $priorityfrom + 1; $i <= $priorityto; $i++) {
                $this->S_request_model->update_priority($i, $i - 1);
            }
            $this->S_request_model->update_priority($priorityfrom, $priorityto, $id);
        } else if ($priorityfrom > $priorityto) {
            for ($i = $priorityfrom - 1; $i >= $priorityto; $i--) {
                $this->S_request_model->update_priority($i, $i + 1);
            }
            $this->S_request_model->update_priority($priorityfrom, $priorityto, $id);
        }

        $active_project = $this->S_request_model->getall_request($userid, array(), '', 'priority');
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['total_chat'] = $this->S_request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
            $getfileid = $this->S_request_model->get_attachment_files($active_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->S_request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
                if($active_project[$i]['expected_date'] == '' || $active_project[$i]['expected_date'] == NULL){
                  $active_project[$i]['expected'] = $this->s_myfunctions->check_timezone($active_project[$i]['latest_update'],$parent_user_plan[0]['plan_id']);
                }else{
                  $active_project[$i]['expected'] = $this->onlytimezone($active_project[$i]['expected_date']);
                }
            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                $active_project[$i]['deliverydate'] = $this->onlytimezone($active_project[$i]['latest_update']);
            } elseif ($active_project[$i]['status'] == "approved") {
                $active_project[$i]['approvddate'] = $this->onlytimezone($active_project[$i]['approvaldate']);
            }
            $active_project[$i]['comment_count'] = $commentcount;
            /* new */
            $id = $active_project[$i]['id'];
            $data = $this->S_request_model->get_request_by_id($id);
            $active_project[$i]['files_new'] = $this->S_request_model->get_new_attachment_files_for_customer($active_project[$i]['id'], array('customerreview', 'Approve'));
            $active_project[$i]['files'] = $active_project[$i]['files'] = $this->S_request_model->get_attachment_files_for_customer($active_project[$i]['id'], array('customerreview', 'Approve'), "designer");
            // for ($j = 0; $j < sizeof($active_project[$i]['files']); $j++) {
            // }
        }
        for ($i = 0; $i < count($active_project); $i++) {
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "checkforapprove" || $active_project[$i]['status'] == "disapprove") {
                ?>
                <!-- Pro Box Start -->
                <div class="pro-desh-row">
                    <div class="pro-desh-box delivery-desh">
                        <p class="pro-a">
                            <?php
                            if ($active_project[$i]['status'] == "active") {
                                echo "Expected on";
                            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                                echo "Delivered on";
                            } elseif ($active_project[$i]['status'] == "disapprove") {
                                echo "Expected on";
                            }
                            ?> 
                        </p>
                        <p class="pro-b">
                            <?php
                            if ($active_project[$i]['status'] == "active") {
                                echo date('m/d/Y', strtotime($active_project[$i]['expected']));
                            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                                echo date('m/d/Y', strtotime($active_project[$i]['deliverydate']));
                            } elseif ($active_project[$i]['status'] == "disapprove") {
                                echo date('m/d/Y', strtotime($active_project[$i]['expected']));
                            }
                            ?>                      
                        </p>
                        <div class="pro-inrightbox">
                <?php if ($active_project[$i]['status'] != "assign") { ?>
                                <p class="green">Active</p>
                                        <?php } else { ?>
                                <p class="pro-a">Priority
                                    <span class="select-pro-a">
                                        <select onchange="prioritize(this.value, <?php echo $active_project[$i]['priority'] ?>,<?php echo $active_project[$i]['id'] ?>)">
                                            <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                                <option value="<?php echo $j; ?>"
                                                        <?php
                                                        if ($j == $active_project[$i]['priority']) {
                                                            echo "selected";
                                                        }
                                                        ?>
                                                        ><?php echo $j; ?></option>                                             
                    <?php } ?>
                                        </select>
                                    </span>
                                </p>
                <?php } ?>
                        </div>
                    </div>

                    <div class="pro-desh-box dcol-1"  style="cursor:pointer;" onclick="window.location = '<?php echo base_url(); ?>account/request/project-info/<?php echo $active_project[$i]['id']; ?>'">
                        <div class="desh-head-wwq">
                            <div class="desh-inblock">
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $active_project[$i]['id']; ?>"><?php echo $active_project[$i]['title']; ?></a></h3>
                                <p class="pro-b"><?php echo $active_project[$i]['category']; ?></p>
                            </div>
                            <div class="desh-inblock">
                                <p class="neft pull-right">
                                    <?php
                                    if ($active_project[$i]['status'] == "checkforapprove") {
                                        $status = "Review your design";
                                        $color = "bluetext";
                                    } elseif ($active_project[$i]['status'] == "active") {
                                        $status = "Design In Progress";
                                        $color = "greentext";
                                    } elseif ($active_project[$i]['status'] == "disapprove") {
                                        $status = "Revision In Progress";
                                        $color = "orangetext ";
                                    } elseif ($active_project[$i]['status'] == "pending" || $active_project[$i]['status'] == "assign") {
                                        $status = "In Queue";
                                        $color = "greentext ";
                                    } else {
                                        $status = "";
                                        $color = "greentext ";
                                    }
                                    ?>
                                        <?php if ($status == 'In Queue'): ?>
                                        <span class="gray text-uppercase">
                                        <?php echo $status; ?>
                                        </span>
                <?php else: ?>
                                        <span class="green <?php echo $color; ?> text-uppercase">
                    <?php echo $status; ?>
                                        </span>
                <?php endif ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="pro-desh-box dcol-2">
                        <div class="pro-circle-list clearfix">
                            <div class="pro-circle-box">
                                <a href=""><figure class="pro-circle-img">
                                        <?php if ($active_project[$i]['profile_picture']) { ?>
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $active_project[$i]['profile_picture']; ?>" class="img-responsive" />
                <?php } else { ?>
                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                <?php } ?>
                                    </figure>
                                    <p class="pro-circle-txt text-center">
                <?php echo $active_project[$i]['designer_first_name']; ?>
                                    </p></a>
                            </div>
                        </div>
                    </div>

                    <div class="pro-desh-box">
                        <div class="pro-desh-r1">
                            <div class="pro-inleftbox">
                                <p class="pro-a inline-per"><a href="javascript:void(0)"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php if ($active_project[$i]['total_chat'] + $active_project[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                <?php echo $active_project[$i]['total_chat'] + $active_project[$i]['comment_count']; ?>
                                                </span>
                                                <?php } ?></span></a></p>
                            </div>
                            <div class="pro-inrightbox">
                                <p class="pro-a inline-per"><a href="javascript:void(0)"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                                        <?php if (count($active_project[$i]['files_new']) != 0) { ?>
                                                <span class="numcircle-box">
                    <?php echo count($active_project[$i]['files_new']); ?>
                                                </span>
                <?php } ?>
                                        </span>
                <?php echo count($active_project[$i]['files']); ?>
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- Pro Box End -->
                <?php
            }
        }
        for ($i = 0; $i < count($active_project); $i++) {
            if ($active_project[$i]['status'] != "active" && $active_project[$i]['status'] != "checkforapprove" && $active_project[$i]['status'] != "disapprove") {
                ?>
                <!-- Pro Box Start -->
                <div class="pro-desh-row">
                    <div class="pro-desh-box delivery-desh" style="padding-right: 0px; padding-left: 15px;">
                        <!-- <p class="pro-a">Delivery</p>
                        <p class="pro-b"> -->
                        <?php
                        // $created_date =  date($active_project[$i]['dateinprogress']);
                        // $expected_date = date('m/d/Y', strtotime($created_date . ' +1 day'));
                        // if ( $active_project[ $i ][ 'modified' ] == "" ) {
                        //     echo $expected_date;
                        // } else {
                        //     echo date( "m/d/Y", strtotime( $active_project[ $i ][ 'modified' ] ) );
                        // }
                        ?>                          
                        <!-- </p> -->
                        <p class="pro-a"">
                                    <?php if ($active_project[$i]['status'] != "assign") { ?>
                            <p class="green">Active</p>
                                    <?php } else { ?>
                            <p class="pro-a">Priority
                                <span class="select-pro-a">
                                    <select onchange="prioritize(this.value, <?php echo $active_project[$i]['priority'] ?>,<?php echo $active_project[$i]['id'] ?>)">
                                                <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                            <option value="<?php echo $j; ?>"
                        <?php
                        if ($j == $active_project[$i]['priority']) {
                            echo "selected";
                        }
                        ?>
                                                    ><?php echo $j; ?></option>                                             
                    <?php } ?>
                                    </select>
                                </span>
                            </p>
                <?php } ?>
                        </p>
                    </div>

                    <div class="pro-desh-box dcol-1"  style="cursor:pointer;" onclick="window.location = '<?php echo base_url(); ?>account/request/project-info/<?php echo $active_project[$i]['id']; ?>'">
                        <div class="desh-head-wwq">
                            <div class="desh-inblock">
                                <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $active_project[$i]['id']; ?>"><?php echo $active_project[$i]['title']; ?></a></h3>
                                <p class="pro-b"><?php echo $active_project[$i]['category']; ?></p>
                            </div>
                            <div class="desh-inblock">
                                <p class="neft pull-right">
                                    <?php
                                    if ($active_project[$i]['status'] == "checkforapprove") {
                                        $status = "Review your design";
                                        $color = "bluetext";
                                    } elseif ($active_project[$i]['status'] == "active") {
                                        $status = "Design In Progress";
                                        $color = "greentext";
                                    } elseif ($active_project[$i]['status'] == "disapprove") {
                                        $status = "Revision In Progress";
                                        $color = "orangetext ";
                                    } elseif ($active_project[$i]['status'] == "pending" || $active_project[$i]['status'] == "assign") {
                                        $status = "In Queue";
                                        $color = "greentext ";
                                    } else {
                                        $status = "";
                                        $color = "greentext ";
                                    }
                                    ?>
                                    <?php if ($status == 'In Queue'): ?>
                                        <span class="gray text-uppercase">
                    <?php echo $status; ?>
                                        </span>
                <?php else: ?>
                                        <span class="green <?php echo $color; ?> text-uppercase">
                    <?php echo $status; ?>
                                        </span>
                <?php endif ?>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="pro-desh-box dcol-2">
                        <div class="pro-circle-list clearfix">
                            <div class="pro-circle-box">
                                <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                <!-- <a href=""><figure class="pro-circle-img">
                <?php if ($active_project[$i]['profile_picture']) { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $active_project[$i]['profile_picture']; ?>" class="img-responsive" />
                <?php } else { ?>
                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                <?php } ?>
                                </figure>
                                <p class="pro-circle-txt text-center">
                <?php echo $active_project[$i]['designer_first_name']; ?>
                                </p></a> -->
                            </div>
                        </div>
                    </div>

                    <div class="pro-desh-box">
                        <div class="pro-desh-r1">
                            <div class="pro-inleftbox">
                                <p class="pro-a inline-per"><a href="javascript:void(0)"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php if ($active_project[$i]['total_chat'] + $active_project[$i]['comment_count'] != 0) { ?>
                                                <span class="numcircle-box">
                                                    <?php echo $active_project[$i]['total_chat'] + $active_project[$i]['comment_count']; ?>
                                                </span>
                                            <?php } ?></span></a></p>
                            </div>
                            <div class="pro-inrightbox">
                                <p class="pro-a inline-per"><a href="javascript:void(0)"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                <?php if (count($active_project[$i]['files_new']) != 0) { ?>
                                                <span class="numcircle-box">
                    <?php echo count($active_project[$i]['files_new']); ?>
                                                </span>
                <?php } ?>
                                        </span>
                <?php echo count($active_project[$i]['files']); ?></a></p>
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- Pro Box End -->
                <?php
            }
        }
    }

    public function new_request_category() {

        $this->s_myfunctions->checkloginuser("customer");

        $this->Welcome_model->update_online();

        $notifications = $this->S_request_model->get_notifications($_SESSION['user_id']);

        $notification_number = $this->S_request_model->get_notifications_number($_SESSION['user_id']);

        $messagenotifications = $this->S_request_model->get_messagenotifications($_SESSION['user_id']);

        $messagenotification_number = $this->S_request_model->get_messagenotifications_number($_SESSION['user_id']);

        $profile_data = $this->S_admin_model->getuser_data($_SESSION['user_id']);
        $request_for_all_status = $this->S_request_model->getall_request_for_trial_user($_SESSION['user_id']);
        //echo "<pre>";print_r($profile_data);
        if ($profile_data[0]['is_trail'] == 1 && sizeof($request_for_all_status) >= 1) {
            redirect(base_url() . "account/request/design_request");
        }


        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));

        $this->load->view('account/new_request_category');
        $this->load->view('account/footer_customer');
    }

    public function process() {
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output['error'] = 'Error while uploading the files. Please try again.';
        $dateFolder = strtotime(date('Y-m-d H:i:s'));
        if (!(isset($_SESSION['temp_folder_names']) && $_SESSION['temp_folder_names'] != '')) {
            if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                $_SESSION['temp_folder_names'] = './public/uploads/temp/' . $dateFolder;
            } else {
                $_SESSION['temp_folder_names'] = '';
            }
        } else {
            if (!is_dir($_SESSION['temp_folder_names'])) {
                if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                    $_SESSION['temp_folder_names'] = './public/uploads/temp/' . $dateFolder;
                } else {
                    $_SESSION['temp_folder_names'] = '';
                }
            }
        }
        if ($_SESSION['temp_folder_names'] != '') {
            $files = $_FILES;
            $cpt = count($_FILES['file-upload']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['file-upload']['name'] = $files['file-upload']['name'][$i];
                $_FILES['file-upload']['type'] = $files['file-upload']['type'][$i];
                $_FILES['file-upload']['tmp_name'] = $files['file-upload']['tmp_name'][$i];
                $_FILES['file-upload']['error'] = $files['file-upload']['error'][$i];
                $_FILES['file-upload']['size'] = $files['file-upload']['size'][$i];
                $config = array(
                    'upload_path' => $_SESSION['temp_folder_names'],
                    'allowed_types' => ALLOWED_FILE_TYPES,
                    'max_size' => '0',
                    'overwrite' => FALSE
                );
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload("file-upload")) {
                    $data = array($this->upload->data());
                    $data[0]['error'] = false;
                    $output['files'][] = $data;
                    $output['status'] = true;
                    $output['error'] = '';
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $error_data = array();
                    $error_data[0] = array();
                    $error_data[0]['file_name'] = $files['file-upload']['name'][$i];
                    $error_data[0]['error'] = true;
                    $error_data[0]['error_msg'] = strip_tags($data['error']);
                    $output['files'][] = $error_data;
                    $output['status'] = true;
                    $output['error'] = '';
                }
            }
        } else {
            $output['error'] = 'Directory not writable. Please try again.';
        }
        echo json_encode($output);
        exit;
    }

    public function delete_file_req() {
        $reqid = isset($_POST['request_id']) ? $_POST['request_id'] : '';
        $filename = isset($_POST['filename']) ? $_POST['filename'] : '';
        $success = $this->S_request_model->delete_request_file('request_files', $reqid, $filename);
        if ($success) {
            $dir = FCPATH . '/public/uploads/requests/' . $reqid;
            unlink($dir . "/" . $filename); //gi
        }
        $this->session->set_flashdata('message_success', "File is Deleted Successfully.!", 5);
    }

    public function delete_file_from_folder() {
        $folderPath = $_SESSION['temp_folder_names'];
        $folderPaths = $_SESSION['temp_folder_names'] . '/*';
        if (!empty($_POST)) {
            $data = $_POST['file_name'];
            $files = glob($folderPaths); // get all file names
            $dir = FCPATH . $folderPath;
            $dirHandle = opendir($dir);
            while ($file = readdir($dirHandle)) {
                if ($file == $data) {
                    unlink($dir . "/" . $file); //give correct path,
                } else {}
            }
        }
    }

    // new_request_brief_update
    public function new_request_brief($id = "") {
        
        $this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        redirect(base_url() . "account/request/add_new_request");
        if (!empty($_POST)) {
            if (isset($_POST['proceed'])) {

                $plate = "";
                $fileType = "";
                if (isset($_POST['designColors'])) {
                    $plate = implode(",", $_POST['designColors']);
                }
                if (isset($_POST['filetype'])) {
                    $fileType = implode(",", $_POST['filetype']);
                }

                $data = array("customer_id" => $_SESSION['user_id'],
                    "title" => $_POST['project_title'],
                    // "design_dimension" => $_POST['design_dimension'],
                    "business_industry" => $_POST['industry_design'],
                    "description" => $_POST['description'],
                    // "other_info" => $_POST['additional_info'],
                    "category" => $_POST['category'],
                    "created" => date("Y-m-d H:i:s"),
                    "status" => "draft",
                    "priority" => 0,
                    "logo_brand" => $_POST['logo-brand'],
                    "designer" => $this->input->post('designer'),
                    "deliverables" => $fileType,
                    "design_colors" => $plate
                );

                $success = $this->Welcome_model->insert_data("requests", $data);

                if (!is_dir('public/uploads/requests/' . $success)) {
                    $data = mkdir('./public/uploads/requests/' . $success, 0777, TRUE);
                }
                //Move files from temp folder
                // Get array of all source files
                $uploadPATH = $_SESSION['temp_folder_names'];
                $uploadPATHs = str_replace("./","",$uploadPATH);
                $files = scandir($uploadPATH);
                // Identify directories
                $source = $uploadPATH . '/';
                $destination = './public/uploads/requests/' . $success . '/';
                // Cycle through all source files
                if(UPLOAD_FILE_SERVER == 'bucket'){
                    foreach ($files as $file) {

                        if (in_array($file, array(".", "..")))
                            continue;

                        $staticName = base_url() . $uploadPATHs . '/' . $file;
                        //echo $staticName;exit;
                        $config = array(
                            'upload_path' => 'public/uploads/requests/' . $success . '/',
                            'file_name' => $file
                        );
                        
                        $this->s3_upload->initialize($config);
                        $is_file_uploaded = $this->s3_upload->upload_multiple_file($staticName);
                        if($is_file_uploaded['status'] == 1){
                             $delete[] = $source . $file;
                            unlink('./tmp/tmpfile/' . basename($staticName));
                        }
                       
                    }
                }else {
                foreach ($files as $file) {
                    if (in_array($file, array(".", "..")))
                        continue;
                    // If we copied this successfully, mark it for deletion
                    if (copy($source . $file, $destination . $file)) {
                        $delete[] = $source . $file;
                    }
                }
                }
                // Delete all successfully-copied files
                foreach ($delete as $file) {
                    unlink($file);
                }
                rmdir($source);
                unset($_SESSION['temp_folder_name']);

                // Insert data into database      
                if (isset($_POST['delete_file']) && $_POST['delete_file'] != '') {
                    foreach ($_POST['delete_file'] as $SubFIlekey => $GetFIleName) {
                        $request_files_data = array("request_id" => $success,
                            "user_id" => $_SESSION['user_id'],
                            "user_type" => "customer",
                            "file_name" => $GetFIleName,
                            "created" => date("Y-m-d H:i:s"));
                        $data = $this->Welcome_model->insert_data("request_files", $request_files_data);
                    }
                }
                redirect(base_url() . "account/request/new_request_review/" . $success);
            }
        }
        $data = array();
        if ($id) {
            $data = $this->S_request_model->get_request_by_id($id);
        }
        if (!isset($_POST['category']) && empty($data)) {
            redirect(base_url() . "account/request/new_request/category");
        }

        $notifications = $this->S_request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->S_request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->S_request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->S_request_model->get_messagenotifications_number($_SESSION['user_id']);

        $profile_data = $this->S_admin_model->getuser_data($_SESSION['user_id']);

        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));
        $this->load->view('account/new_request_brief', array("data" => $data));
        $this->load->view('account/footer_customer');
    }

    public function new_request_review($id = null) {
        $this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        redirect(base_url() . "account/request/add_new_request");
        if ($id == "") {
            redirect(base_url() . "account/request/add_new_request");
            //redirect(base_url() . "account/request/new_request/category");
        }
        if (isset($_GET['red']) && $_GET['red'] == "d") {
            redirect(base_url() . "account/request/design_request");
        }
        $data = $this->S_request_model->get_request_by_id($id);

        $notifications = $this->S_request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->S_request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->S_request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->S_request_model->get_messagenotifications_number($_SESSION['user_id']);
        $profile_data = $this->S_admin_model->getuser_data($_SESSION['user_id']);

        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));
        $this->load->view('account/new_request_review', array("data" => $data, 'id' => $id));
        $this->load->view('account/footer_customer');
        // $this->load->view('account/customer_footer');
    }

    public function dashboard_list_view() {
        $this->s_myfunctions->checkloginuser("customer");
        $this->Welcome_model->update_online();
        $userid = $_SESSION['user_id'];

        $active_project = $this->S_request_model->getall_request($_SESSION['user_id'], '', '', 'priority');
        for ($i = 0; $i < sizeof($active_project); $i++) {
            $active_project[$i]['total_chat'] = $this->S_request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
            $active_project[$i]['total_chat_all'] = $this->S_request_model->get_total_chat_number_customer($active_project[$i]['id'], $_SESSION['user_id'], "customer", "1");
            /* new */
            $getfileid = $this->S_request_model->get_attachment_files($active_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->S_request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
                if ($active_project[$i]['status'] == "active" || $active_project[$i]['status'] == "disapprove") {
                $active_project[$i]['expected'] = $this->s_myfunctions->check_timezone($active_project[$i]['latest_update']);
            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                $active_project[$i]['deliverydate'] = $this->onlytimezone($active_project[$i]['latest_update']);
            }
            } elseif ($active_project[$i]['status'] == "checkforapprove") {
                $active_project[$i]['deliverydate'] = $this->onlytimezone($active_project[$i]['latest_update']);
            }
            $active_project[$i]['comment_count'] = $commentcount;
            $id = $active_project[$i]['id'];
            $data = $this->S_request_model->get_request_by_id($id);
            $active_project[$i]['files_new'] = $this->S_request_model->get_new_attachment_files_for_customer($active_project[$i]['id'], array('customerreview', 'Approve'));
            $active_project[$i]['files'] = $this->S_request_model->get_attachment_files_for_customer($active_project[$i]['id'], array('customerreview', 'Approve'), "designer");
            // $active_project[$i]['files']= $this->S_request_model->get_attachment_files($id, "designer");
            // for ($j = 0; $j < sizeof($active_project[$i]['files']); $j++) {
            // }
        }
        $draft_project = $this->S_request_model->getalldraft_request();
        for ($i = 0; $i < sizeof($draft_project); $i++) {
            $draft_project[$i]['total_chat'] = $this->S_request_model->get_chat_number($draft_project[$i]['id'], $draft_project[$i]['customer_id'], $draft_project[$i]['designer_id'], "customer");
            $draft_project[$i]['total_chat_all'] = $this->S_request_model->get_total_chat_number_customer($draft_project[$i]['id'], $_SESSION['user_id'], "customer", "1");
            $d_id = $draft_project[$i]['id'];
            $data = $this->S_request_model->get_request_by_id($d_id);
            $draft_project[$i]['files'] = $this->S_request_model->get_attachment_files($id, "designer");
        }

        $complete_project = $this->S_request_model->getallcomplete_request();
        for ($i = 0; $i < sizeof($complete_project); $i++) {
            $complete_project[$i]['total_chat'] = $this->S_request_model->get_chat_number($complete_project[$i]['id'], $complete_project[$i]['customer_id'], $complete_project[$i]['designer_id'], "customer");
            $complete_project[$i]['total_chat_all'] = $this->S_request_model->get_total_chat_number_customer($complete_project[$i]['id'], $_SESSION['user_id'], "customer", "1");
            $c_id = $complete_project[$i]['id'];
            $getfileid = $this->S_request_model->get_attachment_files($complete_project[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->S_request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            $complete_project[$i]['approvddate'] = $this->onlytimezone($complete_project[$i]['approvaldate']);
            $complete_project[$i]['comment_count'] = $commentcount;
            $data = $this->S_request_model->get_request_by_id($c_id);
            $complete_project[$i]['files_new'] = $this->S_request_model->get_new_attachment_files_for_customer($complete_project[$i]['id'], array('Approve'));
            $complete_project[$i]['files'] = $this->S_request_model->get_attachment_files_for_customer($complete_project[$i]['id'], array('Approve'), "designer");
        }
        $notifications = $this->S_request_model->get_notifications($_SESSION['user_id']);
        $notification_number = $this->S_request_model->get_notifications_number($_SESSION['user_id']);
        $messagenotifications = $this->S_request_model->get_messagenotifications($_SESSION['user_id']);
        $messagenotification_number = $this->S_request_model->get_messagenotifications_number($_SESSION['user_id']);

        $profile_data = $this->S_admin_model->getuser_data($_SESSION['user_id']);
        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data));
        $this->load->view('account/dashboard-list-view', array("active_project" => $active_project, "complete_project" => $complete_project, "draft_project" => $draft_project));
        $this->load->view('account/footer_customer');
    }

    public function project_image_view($id = null) {
        $reqid = isset($_GET['id'])?$_GET['id']:'';
//        $this->s_myfunctions->checkloginuser("customer");
//        $this->Welcome_model->update_online();
        $user_id = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $login_user_data = $this->load->get_var('login_user_data'); 
        $this->s_myfunctions->checkloginuser($login_user_data[0]['role']);
        $allroles = $this->get_statustab_acc_user();
//        echo "<pre/>";print_r($allroles);exit;
        $sharedreqUser = $this->S_request_model->getSharedpeopleinfobyreq_draftID($reqid,0);
        $shareddraftUser = $this->S_request_model->getSharedpeopleinfobyreq_draftID($reqid,$id);  
        $table =  $this->S_request_model->getDataBasedonSAAS($id);
        if($table == 'requests'){
           $req_files = 'request_files';
            $request_file_chat = 'request_file_chat';
        }else{
           $req_files = 's_request_files'; 
            $request_file_chat = 's_request_file_chat';
        }
        if ($id == "") {
            redirect(base_url() . "account/request/design_request");
        }
//        $checkUser = $this->S_request_model->checkValidUser($_GET['id']);
//        if ($checkUser != 1) {
//            redirect(base_url() . "account/request/design_request");
//        }
        $data['cancomment_on_req'] = $this->s_myfunctions->isUserPermission('comment_on_req');
        $data['candownload_file'] = $this->s_myfunctions->isUserPermission('download_file');
        $data['canapprove_file'] = $this->s_myfunctions->isUserPermission('approve_revise');
        $data['approve_reject'] = $this->s_myfunctions->isUserPermission('approve_reject');
        $data['allroles'] = $allroles;
        //echo "<pre/>";print_R($data);exit;
        $request_file = $this->S_request_model->get_image_by_request_file_id($id,$reqid);
        if($request_file[0][$allroles['seen_by']] == '0'){
        $this->Welcome_model->update_data($req_files, array($allroles['seen_by'] => 1), array('id' => $id));
        //$this->s_myfunctions->capture_project_activity($reqid,$id,'','view_project_by_customer','project_image_view',$login_user_id);
        }
        $this->S_admin_model->update_data($request_file_chat, array($allroles['seen_by'] => '1'), array("request_file_id" => $id));
        // echo $this->db->last_query();
        $url = "account/request/project_image_view/" . $id . "?id=" . $_GET['id'];
        //$this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url));
        //$this->Welcome_model->update_data("message_notification", array("shown" => 1), array("user_id" => $login_user_id, "url" => $url));
        $designer_file = $this->S_admin_model->get_requested_files($_GET['id'], "", "designer", array('1', '2'));
//        echo "<pre>";print_R($designer_file);exit;
        $data['request'] = $this->S_request_model->get_request_by_id($_GET['id']);
        if (empty($data['request'])) {
            redirect(base_url() . "account/request/design_request");
        }
        $designer_file_count = $this->S_admin_model->get_requested_files_count($_GET['id'], "", "designer", array('1', '2'), array('Reject', 'pending'));
        for ($i = 0; $i < sizeof($designer_file); $i++) {
            //$this->publiclinksave($_GET['id'],$designer_file[$i]['id']);
            $designer_file[$i]['modified_date'] = $this->onlytimezone($designer_file[$i]['modified']);
            $designer_file[$i]['approve_date'] = $designer_file[$i]['modified_date'];
            
            $designer_file[$i]['chat'] = $this->S_request_model->get_chat_by_id($designer_file[$i]['id'], "desc");
            for ($j = 0; $j < count($designer_file[$i]['chat']); $j++) {
            $designer_file[$i]['chat'][$j]['replies'] = $this->S_request_model->get_chat_by_parentid($designer_file[$i]['chat'][$j]['id'], 'ASC');
            $designer_file[$i]['chat'][$j]['profile_picture'] = $this->s_customfunctions->getprofileimageurl($designer_file[$i]['chat'][$j]['profile_picture']);
            $designer_file[$i]['chat'][$j]['created'] = $this->onlytimezone($designer_file[$i]['chat'][$j]['created']);
            foreach ($designer_file[$i]['chat'][$j]['replies'] as $key => $value) {
                $designer_file[$i]['chat'][$j]['replies'][$key]['profile_picture'] = $this->s_customfunctions->getprofileimageurl($value['profile_picture']);
            }   
//            $designer_file[$i]['chat'][$j]['msg_created'] = $this->onlytimezone($designer_file[$i]['chat'][$j]['created']);
//            $designer_file[$i]['chat'][$j]['created'] =   date('M d, Y h:i:s', strtotime($designer_file[$i]['chat'][$j]['msg_created']));
            }
        }
        
        $alldraftpubliclinks = $this->S_request_model->getallPubliclinkinfo($reqid);
        $dataa = $this->S_request_model->get_request_by_id($_GET['id']);
       // $userstate = $this->S_request_model->get_user_by_id($_SESSION['user_id']);
        $data['data'] = $this->s_customfunctions->getprojectteammembers($dataa[0]['customer_id'], $dataa[0]['designer_id']);
        //echo "<pre/>";print_r($dataa);
//        $customer = $this->Account_model->getuserbyid($dataa[0]['customer_id']);
//        if (!empty($customer)) {
//            $data['data'][0]['customer_name'] = $customer[0]['first_name'] . " " . $customer[0]['last_name'];
//			$data['data'][0]['is_trail'] = $customer[0]['is_trail'];
//            $data['data'][0]['phone'] = $customer[0]['phone'];
//            $data['data'][0]['can_trialdownload'] = $customer[0]['can_trialdownload'];
//            $data['data'][0]['is_trailverified'] = $customer[0]['is_trailverified'];
//        }
//        $designer = $this->Account_model->getuserbyid($dataa[0]['designer_id']);
//        if (!empty($designer)) {
//            $data['data'][0]['designer_name'] = $designer[0]['first_name'] . " " . $designer[0]['last_name'];
//            $data['data'][0]['designer_image'] = $designer[0]['profile_picture'];
//            $data['data'][0]['designer_sortname'] = substr($designer[0]['first_name'], 0, 1) . substr($designer[0]['last_name'], 0, 1);
//        }
//        $qa = array();
//            $qa = $this->Account_model->getuserbyid($customer[0]['qa_id']);
//            if (!empty($qa)) {
//                $data['data'][0]['qa_name'] = $qa[0]['first_name'] . " " . $qa[0]['last_name'];
//                $data['data'][0]['qa_image'] = $qa[0]['profile_picture'];
//            }
//        if (empty($qa)) {
//            $data['data'][0]['qa_name'] = '';
//            $data['data'][0]['qa_image'] = '';
//        }
//        
        $data['data'][0]['status'] = $dataa[0]['status'];
        $data['data'][0]['title'] = $dataa[0]['title'];
        $data['data'][0]['id'] = $dataa[0]['id'];
        $data['user'] = $this->Account_model->getuserbyid($login_user_id);
        $data['user'][0]['profile_picture'] = $this->s_customfunctions->getprofileimageurl($data['user'][0]['profile_picture']);
        $data['chat_request'] = $this->S_request_model->get_chat_request_by_id($_GET['id']);
        $data['designer_file'] = $designer_file;
        $data['main_id'] = $id;
        $show_filesharing = $this->is_show_file_sharing();
        $data['designer_file_count'] = $designer_file_count;
        //$notifications = $this->S_request_model->get_notifications($login_user_id);
        //$notification_number = $this->S_request_model->get_notifications_number($login_user_id);
        //$messagenotifications = $this->S_request_model->get_messagenotifications($login_user_id);
        //$messagenotification_number = $this->S_request_model->get_messagenotifications_number($login_user_id);

        $profile_data = $this->S_admin_model->getuser_data($_SESSION['user_id']);
        $main_user_data = $this->S_admin_model->getuser_data($user_id);
        $data['main_user_data'] = $main_user_data;
        $data['alldraftpubliclinks'] = $alldraftpubliclinks;
        $data['sharedreqUser'] = $sharedreqUser;
        $data['shareddraftUser'] = $shareddraftUser;
        $data['draftrequest_id'] = $reqid;
//         echo "<pre/>";print_R($data);
        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number, "messagenotifications" => $messagenotifications, "messagenotification_number" => $messagenotification_number, "profile" => $profile_data,"popup" => 0,"show_filesharing" => $show_filesharing));
        $this->load->view('account/project-image-view', $data);
        $this->load->view('account/customer_footer_1');
    }
    
    public function attach_file_process() {
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output['error'] = 'Error while uploading the files. Please try again.';
        $dateFolder = strtotime(date('Y-m-d H:i:s'));
        if (!(isset($_SESSION['temp_attachfolder_names']) && $_SESSION['temp_attachfolder_names'] != '')) {
            if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                $_SESSION['temp_attachfolder_names'] = './public/uploads/temp/' . $dateFolder;
            } else {
                $_SESSION['temp_attachfolder_names'] = '';
            }
        } else {
            if (!is_dir($_SESSION['temp_attachfolder_names'])) {
                if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                    $_SESSION['temp_attachfolder_names'] = './public/uploads/temp/' . $dateFolder;
                } else {
                    $_SESSION['temp_attachfolder_names'] = '';
                }
            }
        }
        if ($_SESSION['temp_attachfolder_names'] != '') {
            $files = $_FILES;
            $cpt = count($_FILES['preview_file']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['preview_file']['name'] = $files['preview_file']['name'][$i];
                $_FILES['preview_file']['type'] = $files['preview_file']['type'][$i];
                $_FILES['preview_file']['tmp_name'] = $files['preview_file']['tmp_name'][$i];
                $_FILES['preview_file']['error'] = $files['preview_file']['error'][$i];
                $_FILES['preview_file']['size'] = $files['preview_file']['size'][$i];
                $config = array(
                    'upload_path' => $_SESSION['temp_attachfolder_names'],
                    'allowed_types' => ALLOWED_FILE_TYPES,
                    'max_size' => '0',
                    'overwrite' => FALSE
                );
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload("preview_file")) {
                    $data = array($this->upload->data());
                    $data[0]['error'] = false;
                    $output['files'][] = $data;
                    $output['status'] = true;
                    $output['error'] = '';
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $error_data = array();
                    $error_data[0] = array();
                    $error_data[0]['file_name'] = $files['preview_file']['name'][$i];
                    $error_data[0]['error'] = true;
                    $error_data[0]['error_msg'] = strip_tags($data['error']);
                    $output['files'][] = $error_data;
                    $output['status'] = true;
                    $output['error'] = '';
                }
            }
        } else {
            $output['error'] = 'Directory not writable. Please try again.';
        }
        echo json_encode($output);
        exit;
    }
    
     public function delete_attachfile_from_folder() {
        $folderPath = $_SESSION['temp_attachfolder_names'];
        $folderPaths = $_SESSION['temp_attachfolder_names'] . '/*';
        if (!empty($_POST)) {
            $data = $_POST['file_name'];
            $files = glob($folderPaths); // get all file names
            $dir = FCPATH . $folderPath;
            $dirHandle = opendir($dir);
            while ($file = readdir($dirHandle)) {
                if ($file == $data) {
                    unlink($dir . "/" . $file); //give correct path,
                } else {}
            }
        }
    }

    public function project_info($id, $sec_id = '') {
        $login_user_data = $this->load->get_var('login_user_data'); 
        $this->s_myfunctions->checkloginuser($login_user_data[0]['role']);
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        $publiclink = $this->S_request_model->getPubliclinkinfo($id);
        $publicpermissions = $this->S_request_model->getpublicpermissions($publiclink['id']);
        $sharedUser = $this->S_request_model->getSharedpeopleinfobyreqID($id); 
        $canapprove_projct = $this->s_myfunctions->isUserPermission('approve_revise');
        $cancomment_on_req = $this->s_myfunctions->isUserPermission('comment_on_req');
        $upload_draft = $this->s_myfunctions->isUserPermission('upload_draft');
        $assigndes_to_project = $this->s_myfunctions->isUserPermission('assign_designer_to_project');
        $s_user_permsn = array(
            "upload_draft" => $upload_draft,
            "assigndes_to_project" => $assigndes_to_project,
            "canapprove_projct" => $canapprove_projct,
            "change_project_status" => $this->s_myfunctions->isUserPermission('change_project_status'),
            "review_design" => $this->s_myfunctions->isUserPermission('review_design')
        );
        $request_meta = $this->S_category_model->get_quest_Ans($id);
        $sampledata = $this->S_request_model->getSamplematerials($id);
        $allroles = $this->get_statustab_acc_user();
        $table =  $this->S_request_model->getDataBasedonSAAS($id);
        if($table == 'requests'){
            $up_tb = 'request_discussions';
        }else{
           $up_tb = 's_request_discussions'; 
        }
       // echo "<pre/>";print_R($allroles);
        if ($id == "") {
            redirect(base_url() . "account/request/design_request");
        }
//        $checkUser = $this->S_request_model->checkValidUser($id);
//        if ($checkUser != 1) {
//            redirect(base_url() . "account/request/design_request");
//        }
        $url = "account/request/project_info/" . $id;
        //$this->Welcome_model->update_data("notification", array("shown" => 1), array("url" => $url));
       // $this->Welcome_model->update_data("message_notification", array("shown" => 1), array("user_id" => $login_user_id, "url" => $url));
        $files = $this->S_request_model->get_attachment_files($id, "designer");
//        echo "<pre/>";print_R($files);exit;
        $file_chat_array = array();
        for ($i = 0; $i < sizeof($files); $i++) {
            $chat_of_file = $this->S_request_model->get_file_chat($files[$i]['id'], $allroles['user'],$id);
            if ($chat_of_file) {
                $file_chat_array[$i]['file_name'] = $files[$i]['file_name'];
                $file_chat_array[$i]['count'] = $chat_of_file;
                $file_chat_array[$i]['id'] = $files[$i]['id'];
                $files[$i]['chat_count'] = $chat_of_file;
            } else {
                $files[$i]['chat_count'] = "";
                $file_chat_array[$i]['id'] = "";
            }
           // $notifications = $this->S_request_model->get_notifications($login_user_id);
            //$notification_number = $this->S_request_model->get_notifications_number($login_user_id);
        }
        $this->S_admin_model->update_data($up_tb, array($allroles['seen_by'] => 1), array("request_id" => $id));
        $data1 = $this->S_request_model->get_request_by_id($id);
        if(isset($data1[0]["is_transfer"]) && $data1[0]["is_transfer"] == 1){
            $tb = "request_files";
        }else{
            $tb = "s_request_files";
        }
        if ($_POST['attach_file']) {
       
            if ($_FILES['file-upload']['name'] != "") {
                if (!is_dir('public/uploads/requests/' . $id)) {
                    $data = mkdir('./public/uploads/requests/' . $id, 0777, TRUE);
                }
                $uploadPATH = $_SESSION['temp_folder_names'];
                $uploadPATHs = str_replace("./","",$uploadPATH);
                $files = scandir($uploadPATH);
                $source = $uploadPATH.'/';
                $destination = './public/uploads/requests/' . $id.'/';
                $CheckForMainORsub = $this->S_admin_model->CheckForMainORsub($_SESSION['user_id']);

                if(UPLOAD_FILE_SERVER == 'bucket'){
                    if($CheckForMainORsub['is_saas'] == 1){
                        $uploadPath = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS;
                    }else{
                        $uploadPath = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS_SAAS;
                    }
                foreach ($files as $file) {
                if (in_array($file, array(".",".."))) continue;
                $staticName = base_url().$uploadPATHs.'/'.$file;
               //echo $staticName;exit;
                $config = array(
                    'upload_path' => $uploadPath. $id.'/',
                    'file_name' => $file
                );
               
                $this->s3_upload->initialize($config);
                $is_uploaded = $this->s3_upload->upload_multiple_file($staticName);
                if($is_uploaded['status'] == 1){
                   $delete[] = $source.$file; 
                   unlink('./tmp/tmpfile/' . basename($staticName));
                }
                //echo "<pre/>";print_r($is_uploaded);exit;
                
                }
                }else{
                foreach ($files as $file) {
                  if (in_array($file, array(".",".."))) continue;
                  // If we copied this successfully, mark it for deletion
                  //echo APPPATH.$source.$file;
                  $filepath = APPPATH.$source.$file;
                  if (copy($source.$file, $destination.$file)) {
                    $delete[] = $source.$file;
                  }
                }
                }
                // Delete all successfully-copied files
                foreach ($delete as $file) {
                  unlink($file);
                }
                rmdir($source);
                
                unset($_SESSION['temp_folder_names']);
                 if($is_uploaded['status'] == 1 || isset($filepath)){
                 if (isset($_POST['delete_file']) && $_POST['delete_file'] != '') {
                   // echo "<pre>"; print_R($_POST['delete_FL']);exit;
                    foreach ($_POST['delete_file'] as $SubFIlekey => $GetFIleName) {
                        // $GetFIleName = substr($SubFIleName, strrpos($SubFIleName, '/') + 1);
                        $request_files_data = array("request_id" => $id,
                            "user_id" => $created_user,
                            "user_type" => "customer",
                            "file_name" => $GetFIleName,
                            "image_link" => $_POST['image_link'][$SubFIlekey],
                            "created" => date("Y-m-d H:i:s"));
                        //print_r($request_files_data);exit;
                          $data = $this->Welcome_model->insert_data($tb, $request_files_data);
                       
                    }
                    //$this->s_myfunctions->capture_project_activity($id,'','','add_attachment','project_info',$login_user_id,1);
                }
                }else{
                    $this->session->set_flashdata('message_error', "Your file is not uploaded, Try Again!", 5);
                   redirect(base_url() . "account/request/project-info/" . $id . '/' . $_POST['tab_status']);
                }
                redirect(base_url() . "account/request/project-info/" . $id . '/' . $_POST['tab_status']);
            } else {
                $this->session->set_flashdata('message_error', "Please Upload Preview  Files.!", 5);
                redirect(base_url() . "account/request/project-info/" . $id . '/' . $_POST['tab_status']);
            }
        }

        
        // die; 
        $data1['customer_variables'] = $this->s_myfunctions->customerVariables($data1[0]['status']);
        $cat_data = $this->S_category_model->get_category_byID($data1[0]['category_id']);
        $cat_name = $cat_data[0]['name'];
        $subcat_data = $this->S_category_model->get_category_byID($data1[0]['subcategory_id']);
        $subcat_name = $subcat_data[0]['name'];
        $data1['cat_name'] = $cat_name;
        $data1['subcat_name'] = $subcat_name;
        $branddata = $this->S_request_model->get_brandprofile_by_id($data1[0]['brand_id']);
        $brand_materials_files = $this->S_request_model->get_brandprofile_materials_files($branddata[0]['id']);
        for($i=0;$i< sizeof($brand_materials_files);$i++){
          if($brand_materials_files[$i]['file_type'] == 'logo_upload'){
            $brand_materials_files['latest_logo'] =  $brand_materials_files[$i]['filename'];
               break;
          }
        }
        $team_member = $this->s_customfunctions->getprojectteammembers($data1[0]['customer_id'], $data1[0]['designer_id']);
        $chat_request = $this->S_request_model->get_customer_mainChat_request_by_id($id); 

        $timediff = $datetimediff = '';
        for ($j = 0; $j < count($chat_request); $j++) {
            if($chat_request[$j]['with_or_not_customer'] == 1){
            $chat_request[$j]['sender_data'] = $this->S_admin_model->getuser_data($chat_request[$j]['sender_id']);
            //$chat_request[$j]['profile_picture'] = $this->s_customfunctions->getprofileimageurl($chat_request[$j]['profile_picture']);
            $chat_request[$j]['msg_created'] = $this->onlytimezone($chat_request[$j]['chat_created_date']);
            //$chat_request[$j]['chat_created_date'] = $chat_request[$j]['msg_created'];
            
            $chat_request[$j]['chat_created_date1'] =   date('M-d-Y h:i A', strtotime($chat_request[$j]['msg_created']));
            //echo "<pre/>";print_r($chat_request[$j]['chat_created_date1']);
            $currentdate = date('M-d-Y');
            $datetime = new DateTime($chat_request[$j]['chat_created_date1']);
            $date = $datetime->format('M-d-Y');
            $time = $datetime->format('h:i A');
            //$chat_request[$j]['chat_created_time'] = $time;
            
            if($timediff == '' && $datetimediff = ''){
             $chat_request[$j]['chat_created_datee'] =   $date; 
             $timediff =  $chat_request[$j]['chat_created_datee'];
             $datetimediff =   $chat_request[$j]['chat_created_date1'];
            }
            $sincedate = $chat_request[$j]['chat_created_date1'];
            $datetime1 = new DateTime($datetimediff);
            $datetime2 = new DateTime($sincedate);
            $mindiff = $datetime1->diff($datetime2);
            if ($mindiff->i >= BATCH_MESSAGE_TIMELIMIT || $chat_request[$j]['sender_id'] != $chat_request[$j - 1]['sender_id']) {
                $datetimediff = $sincedate;
                $timeee = date('h:i A', strtotime($sincedate));
                $chat_request[$j]['chat_created_time'] = $timeee;
                $chat_request[$j]['profile_picture'] = $this->s_customfunctions->getprofileimageurl($chat_request[$j]['profile_picture']);
                $chat_request[$j]['fname'] = $chat_request[$j]['first_name'];
                $chat_request[$j]['shared_name'] = $chat_request[$j]['shared_user_name'];
            }
            if($date != $timediff){
                $timediff = $date;
                $chat_request[$j]['chat_created_datee'] = $date;
                if($date == $currentdate){
                    $chat_request[$j]['chat_created_datee'] = "Today";
                }
            }
        }
        }
//        echo "<pre>";print_r($data1);
        for ($i = 0; $i < count($data1[0]['designer_attachment']); $i++) {  
            if($data1[0]['designer_attachment'][$i]['status'] != 'pending' && $data1[0]['designer_attachment'][$i]['status'] != 'Reject'){
                $data1[0]['approved_attachment'] =  $data1[0]['designer_attachment'][$i]['id']; break;
            }
            $data1[0]['designer_attachment'][$i]['img_created'] = $this->onlytimezone($data1[0]['designer_attachment'][$i]['created']);
            $data1[0]['designer_attachment'][$i]['created'] =   date('d M, Y', strtotime($data1[0]['designer_attachment'][$i]['img_created']));
        }
//echo "<pre/>";print_R($data1[0]['approved_attachment']);exit;
//         echo "<pre>";print_r($data1);
        $request_id = $data1[0]['id'];
//        $activities = $this->s_myfunctions->projectactivities($id, 'user_shown');
        $show_filesharing = $this->is_show_file_sharing();
//        $notifications = $this->S_request_model->get_notifications($login_user_id);
//        $notification_number = $this->S_request_model->get_notifications_number($login_user_id);
//        $messagenotifications = $this->S_request_model->get_messagenotifications($login_user_id);
//        $messagenotification_number = $this->S_request_model->get_messagenotifications_number($login_user_id);
        $main_user_data = $this->S_admin_model->getuser_data($created_user);
        $profile_data = $this->S_admin_model->getuser_data($_SESSION['user_id']);
//        $s_user_permsn = $this->S_request_model->get_sub_user_permissions($login_user_id);
//        echo "<pre/>";print_R($s_user_permsn);
        $designer_list = $this->S_admin_model->get_total_customer("designer");
        for ($i = 0; $i < sizeof($designer_list); $i++) {
            $user = $this->S_request_model->get_all_active_request_by_designer($designer_list[$i]['id']);
            $designer_list[$i]['profile_picture'] = $this->s_customfunctions->getprofileimageurl($data['designer_list'][$i]['profile_picture']);
            $designer_list[$i]['active_request'] = sizeof($user);
        }
          $CheckForMainORsub = $this->S_admin_model->CheckForMainORsub($_SESSION['user_id']); 
//        echo "<pre/>";print_R($data1);exit;
        $this->load->view('account/customer_header_1',array("profile" => $profile_data));
        $this->load->view('account/project-info', array("data" => $data1, "mainchat_request" => $chat_request,'publiclink' => $publiclink,'publicpermissions' => $publicpermissions,
            'sharedUser' => $sharedUser,"branddata" => $branddata, "brand_materials_files" => $brand_materials_files,  "request_id" => $request_id, "chat_request" => $chat_request, "file_chat_array" => $file_chat_array,'cancomment_on_req' => $cancomment_on_req,'main_user_data' => $main_user_data,"team_member"=>$team_member,'activities' => $activities,
            "show_filesharing" => $show_filesharing,
            "request_meta" => $request_meta,
            "sampledata" => $sampledata,'designer_list' => $designer_list,'s_user_permsn' => $s_user_permsn,"CheckForMainORsub"=>$CheckForMainORsub,'allroles' => $allroles));
        $this->load->view('account/footer_customer');
    }

    public function send_message_request() {
         $login_user_id = $this->load->get_var('login_user_id'); 
         $login_user_data = $this->S_admin_model->getuser_data($login_user_id);
//         print_r($login_user_data);exit;
        if(isset($_POST['shared_user_name']) && $_POST['shared_user_name'] != ''  && $_POST['shared_user_name'] != '1'){
          $_POST['sender_id'] = '';  
        }else{
            $_POST['sender_id'] = $_POST['sender_id'];
        }
       // $admin_id = $this->load->get_var('admin_id');
       
        $sender_type = $_POST['sender_type'];
        if ($sender_type == 'designer') {
            $_POST['designer_seen'] = '1';
        } elseif ($sender_type == 'customer' && $login_user_data[0]['is_saas'] == 1) {
            $_POST['admin_seen'] = '1';
        }else{
            $_POST['customer_seen'] = '1';
        }
        $_POST['created'] = date("Y-m-d H:i:s");
        $_POST['shared_user_name'] = isset($_POST['shared_user_name']) ? $_POST['shared_user_name']: '';
        $request_data = $this->S_request_model->get_request_by_id($_POST['request_id']);
        if ($login_user_id != "") {
            $u_id = $login_user_id;
        } else {
            if ($request_data[0]["created_by"] == 0) {
                $u_id = $request_data[0]["customer_id"];
            } else {
                $u_id = $request_data[0]["created_by"];
            }
        }
       // $count_cust_rev = $request_data[0]['count_customer_revision'];
        $customer_data = $this->S_admin_model->getuser_data($request_data[0]['customer_id']);
       
//        $getsubscriptionplan = $this->S_request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
//        if($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])){
//            $turn_around_days = $customer_data[0]['turn_around_days'];
//        }else{
//            $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
//        
//        }
        $table = $this->S_request_model->getDataBasedonSAAS($_POST['request_id']);
        if($table == 'requests'){
            $tbup = 'request_discussions';
        }else{
            $tbup = 's_request_discussions';
        }
        $success = $this->Welcome_model->insert_data($tbup, $_POST);
        // $_POST['designer_seen'] = "1";
        if($success) {
            if ($sender_type == 'customer' && !(isset($login_user_data[0]['is_saas']))) {
                $subcat_data = $this->S_category_model->get_category_byID($request_data[0]['subcategory_id']);
                $customer_data = $this->S_admin_model->getuser_data($request_data[0]['customer_id']);
                if ($request_data[0]['status'] == 'checkforapprove') {
                    //$count = $count_cust_rev + 1;
                    $expected = $this->s_myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"));
                    $this->Welcome_model->update_data($table, array("status" => "disapprove", "status_designer" => "disapprove", "status_admin" => "disapprove", "status_qa" => "disapprove", "who_reject" => 1, "count_customer_revision" => $count,"latest_update" => date("Y-m-d H:i:s"),"expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    //$this->s_myfunctions->capture_project_activity($_POST['request_id'],'','','checkforapprove_to_disapprove','send_message_request',$u_id,1);
                    echo "yes_";
                    /* save notifications when upload draft design */
//                    if($request_data[0]['dummy_request'] != 1){
//                   // $this->s_myfunctions->show_notifications($_POST['request_id'],$u_id,"\"".$request_data[0]['title']."\" Project goes in revision  from checkforapprove..!","admin/dashboard/view_request/" . $_POST['request_id'],$admin_id);
//                    $this->s_myfunctions->show_notifications($_POST['request_id'],$u_id,"\"".$request_data[0]['title']."\" Project goes in revision  from checkforapprove..!","qa/dashboard/view_project/" . $_POST['request_id'],$_SESSION['qa_id']);
//                    $this->s_myfunctions->show_notifications($_POST['request_id'],$u_id,"\"".$request_data[0]['title']."\" Project goes in revision  from checkforapprove..!","designer/request/project_info/" . $_POST['request_id'],$_POST['reciever_id']);
//                    }
                    /** end save notifications when upload draft design * */
                }else{
                 echo "no_";
                }
            }
           // $rtitle = $this->S_request_model->get_request_by_id($_POST['request_id']);
//            if (!empty($request_data)) {
//                /*******insert messages notifications*******/ 
//                if($request_data[0]['dummy_request'] != 1){
//                /* Message notification for customer */
//                $this->s_myfunctions->show_messages_notifications($request_data[0]['title'],$_POST['message'],$_POST['request_id'],$u_id,$_POST['reciever_type'] . "/request/project_info/" . $_POST['request_id'],$_POST['reciever_id']);
//                /* Message notification for qa */
//                $this->s_myfunctions->show_messages_notifications($request_data[0]['title'],$_POST['message'],$_POST['request_id'],$u_id,"qa/dashboard/view_project/" . $_POST['request_id'],$_SESSION['qa_id']);
//                /* Message notification for admin */
//               //$this->s_myfunctions->show_messages_notifications($request_data[0]['title'],$_POST['message'],$_POST['request_id'],$u_id,"admin/dashboard/view_request/" . $_POST['request_id'],$admin_id);
//                }
                /*******end insert messages notifications*******/
//            } else {
//                $data['title'] = "New Message Arrived";
//                $data['created'] = date("Y:m:d H:i:s");
//                $this->Welcome_model->insert_data("notification", $data);
//            }
            //$this->S_request_model->get_notifications($login_user_id);
        }else {
            echo 'no_';
        }
        echo $success;
    }
    
    public function send_message() {
        $admin_id = $this->load->get_var('admin_id');
        $login_user_id = $this->load->get_var('login_user_id');
        $userdata = $this->S_admin_model->getuser_data($_POST['receiver_id']);
        $table = $this->S_request_model->getDataBasedonSAAS($_POST['request_id']);
        if($table == 'requests'){
            $tb = 'request_file_chat';
        }else{
            $tb = 's_request_file_chat';
        }
       // echo "<pre>";print_r($_POST);exit;
        //if ($_POST['sender_role'] == "customer") {
            $request_id = $_POST['request_id'];
            $request_data = $this->S_request_model->get_request_by_id($_POST['request_id']);
            if ($login_user_id != "") {
                $u_id = $login_user_id;
            } else {
                if ($request_data[0]["created_by"] == 0) {
                    $u_id = $request_data[0]["customer_id"];
                } else {
                    $u_id = $request_data[0]["created_by"];
                }
            }
            $count_cust_rev = $request_data[0]['count_customer_revision'];
            $customer_data = $this->S_admin_model->getuser_data($request_data[0]['customer_id']);
//            $getsubscriptionplan = $this->S_request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
//             if($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])){
//                $turn_around_days = $customer_data[0]['turn_around_days'];
//            }else{
//                $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
//            }
            $subcat_data = $this->S_category_model->get_category_byID($request_data[0]['subcategory_id']);
            $expected = $this->myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"),"",$request_data[0]['category_bucket'],$subcat_data[0]['timeline'],$turn_around_days);
            unset($_POST['request_id']);
            $_POST['created'] = date("Y-m-d H:i:s");
            if ($this->Welcome_model->insert_data($tb, $_POST)) {
                //if($request_data[0]['status'] != "approved" && $request_data[0]['status'] != "draft" && $request_data[0]['status'] != "assign"){
                if ($request_data[0]['status'] == "checkforapprove") {
                    $count = $count_cust_rev + 1;
                    $this->Welcome_model->update_data($table, array("status" => "disapprove", "status_designer" => "disapprove", "status_admin" => "disapprove", "status_qa" => "disapprove", "who_reject" => 1, "count_customer_revision" => $count,"latest_update" => date("Y-m-d H:i:s"),"expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $request_id));
                   // $this->myfunctions->capture_project_activity($request_id,$_POST['request_file_id'],'','checkforapprove_to_disapprove','send_message',$u_id,1);
//                if($request_data[0]['dummy_request'] != 1){
//                 $this->myfunctions->show_notifications($_POST['request_id'],$u_id,"\"".$request_data[0]['title']."\" Project goes into revision  from checkforapprove.!","admin/dashboard/view_request/" . $_POST['request_id'],$admin_id);
//                 $this->myfunctions->show_notifications($_POST['request_id'],$u_id,"\"".$request_data[0]['title']."\" Project goes into revision  from checkforapprove..!","qa/dashboard/view_project/" . $_POST['request_id'],$userdata[0]['qa_id']);
//                 $this->myfunctions->show_notifications($_POST['request_id'],$u_id,"\"".$request_data[0]['title']."\" Project goes into revision  from checkforapprove..!","designer/request/project_info/" . $_POST['request_id'],$_POST['receiver_id']);   
//                }
                 echo "yes_";
                }else{
                    echo "no_";
                }
                echo $this->db->insert_id();
            } else {
                echo "no_";
            }
            /*******insert messages notifications*******/
//            if($request_data[0]['dummy_request'] != 1){
//            /* Message notification for customer */
//            $this->myfunctions->show_messages_notifications($request_data[0]['title'], $_POST['message'], $request_id, $u_id, "designer/request/project_image_view/".$_POST['request_file_id']."?id=".$request_id, $_POST['receiver_id']);
//            /* Message notification for qa */
//            $this->myfunctions->show_messages_notifications($request_data[0]['title'], $_POST['message'], $request_id, $u_id, "qa/dashboard/view_files/".$_POST['request_file_id']."?id=".$request_id, $userdata[0]['qa_id']);
//            /* Message notification for admin */
//            $this->myfunctions->show_messages_notifications($request_data[0]['title'], $_POST['message'], $request_id, $u_id, "admin/dashboard/view_files/".$_POST['request_file_id']."?id=".$request_id, $admin_id);
//            }

            /*******end insert messages notifications*******/
        //} 
        exit;
    }
    
    public function downloadzip() {
        if (isset($_POST)) {
            if(isset($_POST['front_user_id']) && $_POST['front_user_id'] != ""){
                $login_user_id = $_POST['front_user_id'];
            }else{
                $login_user_id = $this->load->get_var('login_user_id'); 
            }
            //$this->s_myfunctions->capture_project_activity($_POST['request_id'],$_POST['draft_id'],'','project_download_by_customer','downloadzip',$login_user_id);
            if(UPLOAD_FILE_SERVER == 'bucket'){
            $file_name = FS_PATH;
            $s3Path = str_replace("https://", "http://", $file_name);
            $source_filename = substr($_POST['srcfile'], strrpos($_POST['srcfile'], '/') + 1);
            $this->load->helper('download');
            $path = file_get_contents(FS_PATH . $_POST['srcfile']); // get file name
//            echo "<pre/>";print_R($_POST);
//            echo "FS_PATH".FS_PATH."<br/>".$path;exit;
            $name = $source_filename; // new name for your file
            force_download($name, $path);
            }else{
            $file_name = FS_PATH;
            $s3Path = str_replace("https://", "http://", $file_name);
            $this->zip->read_file($s3Path . $_POST['srcfile']);
            $this->zip->read_file($s3Path . $_POST['prefile']);
            if ($_POST['prefile'] && $_POST['srcfile'] == '') {
                $this->zip->download($_POST['prefile'] . ".zip");
            } else {
                $this->zip->download($_POST['project_name'] . ".zip");
            }
            }
        }
    }
 
    public function get_ajax_all_request() {
        $dataArray = $this->input->get('title');
        $dataStatus = $this->input->get('status');
        $dataStatus = explode(",", $dataStatus);
        $customerRequest = $this->S_request_model->ajax_getall_request(array('keyword' => $dataArray), $dataStatus, 'priority');
        $activeTab = 0;
        foreach ($customerRequest as $project) {
            if ($project['status'] == "assign") {
                $activeTab++;
            }
        }
        for ($i = 0; $i < sizeof($customerRequest); $i++) {
            $customerRequest[$i]['total_chat'] = $this->S_request_model->get_chat_number_customer($customerRequest[$i]['id'], $_SESSION['user_id'], "customer", "1");
            $customerRequest[$i]['total_chat_all'] = $this->S_request_model->get_total_chat_number_customer($customerRequest[$i]['id'], $_SESSION['user_id'], "customer", "1");
            /* new */
            $id = $customerRequest[$i]['id'];
            $data = $this->S_request_model->get_request_by_id($id);
            $customerRequest[$i]['files_new'] = $this->S_request_model->get_new_attachment_files_for_customer($customerRequest[$i]['id'], array('customerreview', 'Approve'));
            $getfileid = $this->S_request_model->get_attachment_files($customerRequest[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->S_request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            if ($customerRequest[$i]['status'] == "active" || $customerRequest[$i]['status'] == "disapprove") {
                $customerRequest[$i]['expected'] = $this->s_myfunctions->check_timezone($customerRequest[$i]['latest_update']);
            } elseif ($customerRequest[$i]['status'] == "checkforapprove") {
                $customerRequest[$i]['deliverydate'] = $this->onlytimezone($customerRequest[$i]['latest_update']);
            } elseif ($customerRequest[$i]['status'] == "approved") {
                $customerRequest[$i]['approvddate'] = $this->onlytimezone($customerRequest[$i]['approvaldate']);
            }
            $customerRequest[$i]['comment_count'] = $commentcount;
            $customerRequest[$i]['files'] = $this->S_request_model->get_attachment_files_for_customer($customerRequest[$i]['id'], array('customerreview', 'Approve'), "designer");
        }
        for ($i = 0; $i < sizeof($customerRequest); $i++) {
            if ($customerRequest[$i]['status'] == 'assign' || $customerRequest[$i]['status'] == 'checkforapprove' || $customerRequest[$i]['status'] == 'active' || $customerRequest[$i]['status'] == 'disapprove' || $customerRequest[$i]['status'] == 'pending') {
                ?>                            
                <div id="prior_data">

                <?php
                for ($i = 0; $i < count($customerRequest); $i++) {
                    if ($customerRequest[$i]['status'] == "active" || $customerRequest[$i]['status'] == "checkforapprove" || $customerRequest[$i]['status'] == "disapprove") {
                        ?>
                            <!-- Pro Box Start -->
                            <div class="col-md-2 col-sm-6">
                                <div class="pro-productss pro-box-r1">
                                    <div class="pro-box-r2">
                                        <div class="pro-inbox-r1">
                                            <div class="pro-inleftbox">
                                                <p class="pro-a">
                                                    <?php
                                                    if ($customerRequest[$i]['status'] == "active") {
                                                        echo "Expected on";
                                                    } elseif ($customerRequest[$i]['status'] == "checkforapprove") {
                                                        echo "Delivered on";
                                                    } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                        echo "Expected on";
                                                    }
                                                    ?>
                                                </p>
                                                <p class="pro-b"><a href="#">
                                                        <?php
                                                        if ($customerRequest[$i]['status'] == "active") {
                                                            echo date('m/d/Y', strtotime($customerRequest[$i]['expected']));
                                                        } elseif ($customerRequest[$i]['status'] == "checkforapprove") {
                                                            echo date('m/d/Y', strtotime($customerRequest[$i]['deliverydate']));
                                                        } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                            echo date('m/d/Y', strtotime($customerRequest[$i]['expected']));
                                                        }
                                                        ?>
                                                    </a></p>
                                            </div>
                                            <div class="pro-inrightbox">
                        <?php if ($customerRequest[$i]['status'] != "assign") { ?>
                                                    <p class="green">Active </p>
                        <?php } ?>
                                            </div>
                                        </div>
                                        <hr class="hr-a">   
                                        <!-- <script type="text/javascript">alert("<?php echo $customerRequest[$i]['id']; ?>")</script> -->
                                        <a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/1">
                                            <h3 class="pro-head space-b" title="<?php echo $customerRequest[$i]['title']; ?>"><?php echo substr($customerRequest[$i]['title'], 0, 40); ?></h3>
                                            <p class="pro-b"><?php echo $customerRequest[$i]['category']; ?></p>
                                            <p class="space-a"></p>
                                            <p class="neft">
                                                <?php
                                                if ($customerRequest[$i]['status'] == "checkforapprove") {
                                                    $status = "Review your design";
                                                    $color = "bluetext";
                                                } elseif ($customerRequest[$i]['status'] == "active") {
                                                    $status = "Design In Progress";
                                                    $color = "blacktext";
                                                } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                    $status = "Revision In Progress";
                                                    $color = "orangetext ";
                                                } elseif ($customerRequest[$i]['status'] == "pending" || $customerRequest[$i]['status'] == "assign") {
                                                    $status = "In Queue";
                                                    $color = "greentext ";
                                                } else {
                                                    $status = "";
                                                    $color = "greentext ";
                                                }
                                                //echo $status;
                                                ?>
                                                <?php if ($status == 'In Queue'): ?>
                                                    <span class="gray text-uppercase">
                            <?php echo $status; ?>
                                                    </span>
                        <?php else: ?>
                                                    <span class="green <?php echo $color; ?> text-uppercase">
                            <?php echo $status; ?>
                                                    </span>
                        <?php endif ?>
                                            </p>
                                        </a>
                                    </div>

                                    <div class="pro-box-r3">
                                        <p class="pro-a">Designer</p>
                                        <p class="space-a"></p>
                                        <div class="pro-circle-list clearfix">
                                            <div class="pro-circle-box">
                                                <a href="#"><figure class="pro-circle-img">
                        <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                        <?php } else { ?>
                                                            <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                        <?php } ?>
                                                    </figure>
                                                    <p class="pro-circle-txt text-center"><?php echo $customerRequest[$i]['designer_first_name']; ?></p></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="pro-box-r4">
                                        <div class="pro-inbox-r1">
                                            <div class="pro-inleftbox">
                                                <p class="pro-a inline-per">
                                                    <a style="position: relative;" href="<?php echo base_url() ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/1">
                                                        <span class="inline-imgsssx">
                                                            <img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                        <?php if ($customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count'] != 0) { ?>
                                                                <span class="numcircle-box">
                            <?php echo $customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count']; ?>
                                                                </span>
                                                            <?php } ?>
                                                        </span>
                                                    </a>
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <a  style="position: relative;"  href="<?php echo base_url() ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/1">
                                                        <span class="inline-imgsssx">
                                                            <img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                        <?php if (count($customerRequest[$i]['files_new']) != 0) { ?>
                                                                <span class="numcircle-box">
                            <?php echo count($customerRequest[$i]['files_new']); ?>
                                                                </span>
                        <?php } ?>
                                                        </span>
                        <?php echo count($customerRequest[$i]['files']); ?>
                                                    </a>
                                                </p>
                                            </div>
                                            <div class="pro-inrightbox">
                                                <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>account/request/deleteproject/<?php echo $customerRequest[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <!-- Pro Box End -->
                        <?php
                    }
                }
                for ($i = 0; $i < count($customerRequest); $i++) {
                    if ($customerRequest[$i]['status'] != "active" && $customerRequest[$i]['status'] != "checkforapprove" && $customerRequest[$i]['status'] != "disapprove") {
                        ?>
                            <!-- Pro Box Start -->
                            <div class="col-md-2 col-sm-6">
                                <div class="pro-productss pro-box-r1">
                                    <div class="pro-box-r2">
                                        <div class="pro-inbox-r1" style="padding: 7px 0px;">
                                            <div class="pro-inleftbox">
                                                <p class="pro-a">In-Queue</p>
                                                <!-- <p class="pro-b"><a href="#"> -->
                                                <?php
                                                // $created_date =  date($customerRequest[$i]['dateinprogress']);
                                                // $expected_date = date('m/d/Y', strtotime($created_date . ' +1 day'));
                                                // if ( $customerRequest[ $i ][ 'modified' ] == "" ) {
                                                //     echo $expected_date;
                                                // } else {
                                                //     echo date( "m/d/Y", strtotime( $customerRequest[ $i ][ 'modified' ] ) );
                                                // }
                                                ?>
                                                <!-- </a></p> -->
                                            </div>
                                            <div class="pro-inrightbox">
                                                        <?php if ($customerRequest[$i]['status'] == "assign") { ?>
                                                    <p class="select-pro-a pro-a">
                                                        Priority
                                                        <select onchange="prioritize(this.value, <?php echo $customerRequest[$i]['priority'] ?>,<?php echo $customerRequest[$i]['id'] ?>)">
                            <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                                                <option value="<?php echo $j; ?>"
                                                        <?php
                                                        if ($j == $customerRequest[$i]['priority']) {
                                                            echo "selected";
                                                        }
                                                        ?>
                                                                        ><?php echo $j; ?></option>                                             
                            <?php } ?>
                                                        </select>

                                                    </p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <hr class="hr-a">   
                                        <!-- <script type="text/javascript">alert("<?php echo $customerRequest[$i]['id']; ?>")</script> -->
                                        <a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/1">
                                            <h3 class="pro-head space-b" title="<?php echo $customerRequest[$i]['title']; ?>"><?php echo substr($customerRequest[$i]['title'], 0, 40); ?></h3>
                                            <p class="pro-b"><?php echo $customerRequest[$i]['category']; ?></p>
                                            <p class="space-a"></p>
                                            <p class="neft">
                                                <?php
                                                if ($customerRequest[$i]['status'] == "checkforapprove") {
                                                    $status = "Review your design";
                                                    $color = "greentext";
                                                } elseif ($customerRequest[$i]['status'] == "active") {
                                                    $status = "Design In Progress";
                                                    $color = "blacktext";
                                                } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                    $status = "Revision In Progress";
                                                    $color = "orangetext ";
                                                } elseif ($customerRequest[$i]['status'] == "pending" || $customerRequest[$i]['status'] == "assign") {
                                                    $status = "In Queue";
                                                    $color = "greentext ";
                                                } else {
                                                    $status = "";
                                                    $color = "greentext ";
                                                }
                                                //echo $status;
                                                ?>
                        <?php if ($status == 'In Queue'): ?>
                                                    <span class="gray text-uppercase">
                            <?php echo $status; ?>
                                                    </span>
                        <?php else: ?>
                                                    <span class="green text-uppercase">
                            <?php echo $status; ?>
                                                    </span>
                        <?php endif ?>
                                            </p>
                                        </a>
                                    </div>

                                    <div class="pro-box-r3">
                                        <p class="pro-a">Designer</p>
                                        <p class="space-a"></p>
                                        <div class="pro-circle-list clearfix">
                                            <div class="pro-circle-box">
                                                <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                                <!-- <a href="#"><figure class="pro-circle-img">
                        <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                        <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                        <?php } ?>
                                                </figure>
                                                <p class="pro-circle-txt text-center"><?php echo $customerRequest[$i]['designer_first_name']; ?></p></a> -->
                                            </div>
                                        </div>
                                    </div>

                                    <div class="pro-box-r4">
                                        <div class="pro-inbox-r1">
                                            <div class="pro-inleftbox">
                                                <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/1"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21"><?php if ($customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count'] != 0) { ?>
                                                            <span class="numcircle-box">
                                                                    <?php echo $customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count']; ?>
                                                            </span>
                                                            <?php } ?></a>
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <a style="position: relative;" href="<?php echo base_url() ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/1">
                                                        <span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                        <?php if (count($customerRequest[$i]['files_new']) != 0) { ?>
                                                                <span class="numcircle-box">
                            <?php echo count($customerRequest[$i]['files_new']); ?>
                                                                </span>
                        <?php } ?>
                                                        </span>
                        <?php echo count($customerRequest[$i]['files']); ?>
                                                    </a>
                                                </p>
                                            </div>
                                            <div class="pro-inrightbox">
                                                <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>account/request/deleteproject/<?php echo $customerRequest[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <!-- Pro Box End -->
                                <?php }
                            }
                            ?>  

                </div>  
                <p class="space-e"></p>        
            <?php } elseif ($customerRequest[$i]['status'] == 'draft') { ?>
                <div class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">
                    <!-- Drafts -->
                    <div class="product-list-show">
                        <div class="row">
                            <!-- Pro Box Start -->  
                <?php for ($i = 0; $i < sizeof($customerRequest); $i++) { ?>
                                <div class="col-md-2 col-sm-6">         
                                    <div class="pro-productss pro-box-r1">
                                        <div class="pro-box-r2">
                                            <div class="pro-inbox-r1">
                                                <div class="pro-inleftbox">
                                                    <p class="pro-a">Drafts <?php //echo $i;   ?></p>
                                                </div>
                                            </div>
                                            <hr class="hr-a">   
                                            <a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/2">
                                                <h3 class="pro-head space-b" title="<?php echo $customerRequest[$i]['title']; ?>"><?php echo substr($customerRequest[$i]['title'], 0, 40); ?></h3>
                                                <!-- <p class="pro-b"><?php echo $dp['category']; ?></p> -->
                                                <p class="space-a"></p>
                                                <p class="neft"><span class="gray text-uppercase">
                    <?php echo $customerRequest[$i]['status']; ?>
                                                    </span></p>
                                            </a>
                                        </div>

                                        <div class="pro-box-r3">
                                                            <?php if ($customerRequest[$i]['designer_first_name']) { ?>
                                                <p class="pro-a">Designer</p>
                                                <p class="space-a"></p>

                                                <div class="pro-circle-list clearfix">
                                                    <div class="pro-circle-box">
                                                        <a href="#"><figure class="pro-circle-img">

                        <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                                                <?php } else { ?>
                                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                                                <?php } ?>
                                                                    <!-- <img src="<?php echo base_url() ?>theme/customer-assets/images/pro1.png" class="img-responsive">  -->
                                                            </figure>
                                                            <p class="pro-circle-txt text-center"><?php echo $customerRequest[$i]['designer_first_name']; ?></p></a>


                                                    </div>

                                                </div>
                                                                <?php } else { ?>
                                                <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                                            <?php } ?>
                                        </div>

                                        <div class="pro-box-r4">
                                            <div class="pro-inbox-r1">
                                                <div class="pro-inleftbox">
                                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/2"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                    <?php if ($customerRequest[$i]['total_chat'] != 0) { ?>
                                                                <span class="numcircle-box">
                        <?php echo $customerRequest[$i]['total_chat']; ?>
                                                                </span>
                    <?php } ?></a>
                                                        &nbsp;
                                                        &nbsp;
                                                        &nbsp;
                                                        <a href="<?php echo base_url() ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/2"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13"><?php if (isset($customerRequest[$i]['file_output'])): echo count($customerRequest[$i]['file_output']);
                    endif;
                    ?></a>

                                                    </p>
                                                </div>
                                                <div class="pro-inrightbox">
                                                    <a href="javascript:void(0)" data-url="<?php echo base_url(); ?>account/request/deleteproject/<?php echo $customerRequest[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- Pro Box End -->
                <?php } ?>          
                        </div>
                    </div>

                </div>
                                                <?php }elseif ($customerRequest[$i]['status'] == 'approved') { ?>
                <div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
                    <!-- Complete Projects -->
                    <div class="product-list-show">
                        <div class="row">
                            <!-- Pro Box Start -->
                <?php for ($i = 0; $i < count($customerRequest); $i++) { ?>
                                <div class="col-md-2 col-sm-6">                 
                                    <div class="pro-productss pro-box-r1">
                                        <div class="pro-box-r2">
                                            <div class="pro-inbox-r1">
                                                <div class="pro-inleftbox">
                                                    <p class="pro-a">
                    <?php
                    if ($customerRequest[$i]['status'] == "approved") {
                        echo "Approved on";
                    }
                    ?>
                                                    </p>
                                                    <p class="pro-b"><a href="">
                    <?php
                    if ($customerRequest[$i]['status'] == "approved") {
                        echo date('m/d/Y', strtotime($customerRequest[$i]['approvddate']));
                    }
                    ?>
                                                        </a></p>
                                                </div>                                      
                                            </div>
                                            <hr class="hr-a">   
                                            <a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/3">
                                                <h3 class="pro-head space-b" title="<?php echo $customerRequest[$i]['title']; ?>"><?php echo substr($customerRequest[$i]['title'], 0, 40); ?></h3>
                                                <p class="pro-b"><?php echo $customerRequest[$i]['category']; ?></p>
                                                <p class="space-a"></p>
                                                <p class="neft"><span class="green text-uppercase">
                                                        <?php
                                                        if ($customerRequest[$i]['status'] == " checkforapprove") {
                                                            $status = "Review your design";
                                                            $color = "greentext";
                                                        } elseif ($customerRequest[$i]['status'] == "active") {
                                                            $status = "Design In Progress";
                                                            $color = "blacktext";
                                                        } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                            $status = "Revision In Progress";
                                                            $color = "orangetext ";
                                                        } elseif ($customerRequest[$i]['status'] == "pending" || $customerRequest[$i]['status'] == "assign") {
                                                            $status = "In Queue ";
                                                            $color = "greentext ";
                                                        } else {
                                                            $status = "Completed";
                                                            $color = "greentext ";
                                                        }
                                                        echo $status;
                                                        ?>
                                                    </span></p></a>
                                        </div>

                                        <div class="pro-box-r3">
                                            <p class="pro-a">Designed by</p>
                                            <p class="space-a"></p>
                                            <div class="pro-circle-list clearfix">
                                                <div class="pro-circle-box">
                                                    <a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/3"><figure class="pro-circle-img">
                    <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                    <?php } else { ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                    <?php } ?>
                                                                <!-- <img src="<?php echo base_url() ?>theme/customer-assets/images/pro1.png" class="img-responsive"> -->
                                                        </figure>
                                                        <p class="pro-circle-txt text-center"><?php echo $customerRequest[$i]['designer_first_name']; ?></p></a>
                                                </div>

                                                <!-- <div class="pro-circle-box">
                                                    <a href=""><figure class="pro-circle-plus">
                                                        <span class="plus-text"><span class="plus-sizebig">+</span> <br> 05</span>
                                                    </figure></a>
                                                </div> -->

                                            </div>
                                        </div>

                                        <div class="pro-box-r4">
                                            <div class="pro-inbox-r1">
                                                <div class="pro-inleftbox">
                                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/3"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                                                                <?php if ($customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count'] != 0) { ?>
                                                                <span class="numcircle-box">
                                                                <?php echo $customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count']; ?>
                                                                </span>
                    <?php } ?></a></p>
                                                </div>
                                                <div class="pro-inrightbox">
                                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>/3"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                                <?php if (count($customerRequest[$i]['files_new']) != 0) { ?>
                                                                    <span class="numcircle-box">
                        <?php echo count($customerRequest[$i]['files_new']); ?>
                                                                    </span>
                    <?php } ?>
                                                            </span>
                    <?php echo count($customerRequest[$i]['files']); ?>
                                                        </a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- Pro Box End -->
                <?php } ?>          
                        </div>
                    </div>
                    <p class="space-e"></p>

                    <!-- <div class="loader">
                        <a class="loader-link text-uppercase" href=""><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-loader.png" class="img-responsive" width="28"> Loading</a>
                    </div> -->
                </div>
                <?php
            }
        }
    }

    public function get_ajax_all_request_list() {
        $dataArray = $this->input->get('title');
        $dataStatus = $this->input->get('status');
        $dataStatus = explode(",", $dataStatus);
        $customerRequest = $this->S_request_model->ajax_getall_request(array('keyword' => $dataArray), $dataStatus, 'priority');
        $activeTab = 0;
        foreach ($customerRequest as $project) {
            if ($project['status'] == "assign") {
                $activeTab++;
            }
        }
        for ($i = 0; $i < sizeof($customerRequest); $i++) {
            $customerRequest[$i]['total_chat'] = $this->S_request_model->get_chat_number($customerRequest[$i]['id'], $customerRequest[$i]['customer_id'], $customerRequest[$i]['designer_id'], "customer");
            $customerRequest[$i]['total_chat_all'] = $this->S_request_model->get_total_chat_number_customer($customerRequest[$i]['id'], $_SESSION['user_id'], "customer", "1");
            /* new */
            $id = $customerRequest[$i]['id'];
            $data = $this->S_request_model->get_request_by_id($id);
            $customerRequest[$i]['files_new'] = $this->S_request_model->get_new_attachment_files_for_customer($customerRequest[$i]['id'], array('customerreview', 'Approve'));
            $getfileid = $this->S_request_model->get_attachment_files($customerRequest[$i]['id'], "designer");
            $commentcount = 0;
            for ($j = 0; $j < sizeof($getfileid); $j++) {

                $commentcount += $this->S_request_model->get_file_chat($getfileid[$j]['id'], "customer");
            }
            if ($customerRequest[$i]['status'] == "active" || $customerRequest[$i]['status'] == "disapprove") {
                $customerRequest[$i]['expected'] = $this->s_myfunctions->check_timezone($customerRequest[$i]['latest_update']);
            } elseif ($customerRequest[$i]['status'] == "checkforapprove") {
                $customerRequest[$i]['deliverydate'] = $this->onlytimezone($customerRequest[$i]['latest_update']);
            } elseif ($customerRequest[$i]['status'] == "approved") {
                $customerRequest[$i]['approvddate'] = $this->onlytimezone($customerRequest[$i]['approvaldate']);
            }
            $customerRequest[$i]['comment_count'] = $commentcount;
            $customerRequest[$i]['files'] = $this->S_request_model->get_attachment_files_for_customer($customerRequest[$i]['id'], array('customerreview', 'Approve'), "designer");
        }
        for ($i = 0; $i < sizeof($customerRequest); $i++) {
            if ($customerRequest[$i]['status'] == 'assign' || $customerRequest[$i]['status'] == 'checkforapprove' || $customerRequest[$i]['status'] == 'active' || $customerRequest[$i]['status'] == 'disapprove' || $customerRequest[$i]['status'] == 'pending') {
                ?>
                <!-- Design Request Tab -->
                <div class="tab-pane active content-datatable datatable-width" id="designs_request_tab" role="tabpanel">
                    <div class="pro-deshboard-list">
                        <div class="cli-ent-add-row">
                            <a href="<?php echo base_url() ?>account/request/new_request/category" class="cli-ent-add-col" >
                                <span class="cli-ent-add-icon">+</span>
                            </a>
                        </div>

                        <!-- For Active Projects -->
                        <div id="prior_data">
                                        <?php
                                        for ($i = 0; $i < count($customerRequest); $i++) {
                                            if ($customerRequest[$i]['status'] == "active" || $customerRequest[$i]['status'] == "checkforapprove" || $customerRequest[$i]['status'] == "disapprove") {
                                                ?>
                                    <!-- List Desh Product -->
                                    <div class="pro-desh-row">
                                        <div class="pro-desh-box delivery-desh">
                                            <p class="pro-a">
                                                <?php
                                                if ($customerRequest[$i]['status'] == "active") {
                                                    echo "Expected on";
                                                } elseif ($customerRequest[$i]['status'] == "checkforapprove") {
                                                    echo "Delivered on";
                                                } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                    echo "Expected on";
                                                } elseif ($customerRequest[$i]['status'] == "approved") {
                                                    echo "Approved on";
                                                }
                                                ?>
                                            </p>
                                            <p class="pro-b">
                                            <?php
                                            if ($customerRequest[$i]['status'] == "active") {
                                                echo date('m/d/Y', strtotime($customerRequest[$i]['expected']));
                                            } elseif ($customerRequest[$i]['status'] == "checkforapprove") {
                                                echo date('m/d/Y', strtotime($customerRequest[$i]['deliverydate']));
                                            } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                echo date('m/d/Y', strtotime($customerRequest[$i]['expected']));
                                            }
                                            ?>                         
                                            </p>
                                                                <?php if ($customerRequest[$i]['status'] != "assign") { ?>
                                                <p class="green">Active</p>
                        <?php } else { ?>
                                                <p class="pro-a">Priority
                                                    <span class="select-pro-a">
                                                        <select onchange="prioritize(this.value, <?php echo $customerRequest[$i]['priority'] ?>,<?php echo $customerRequest[$i]['id'] ?>)">
                            <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                                                <option value="<?php echo $j; ?>"
                                <?php
                                if ($j == $customerRequest[$i]['priority']) {
                                    echo "selected";
                                }
                                ?>
                                                                        ><?php echo $j; ?></option>                                             
                            <?php } ?>
                                                        </select>
                                                    </span>
                                                </p>
                                                        <?php } ?>
                                        </div>

                                        <div class="pro-desh-box dcol-1" style="cursor:pointer;" onclick="window.location = '<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>'">
                                            <div class="desh-head-wwq">
                                                <div class="desh-inblock">
                                                    <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>">
                                                        <?php echo $customerRequest[$i]['title']; ?>
                                                        </a></h3>
                                                    <p class="pro-b"><?php echo $customerRequest[$i]['category']; ?></p>
                                                </div>
                                                <div class="desh-inblock">
                                                    <p class="neft pull-right">
                                                        <?php
                                                        if ($customerRequest[$i]['status'] == "checkforapprove") {
                                                            $status = "Review your design";
                                                            $color = "bluetext";
                                                        } elseif ($customerRequest[$i]['status'] == "active") {
                                                            $status = "Design In Progress";
                                                            $color = "blacktext";
                                                        } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                            $status = "Revision In Progress";
                                                            $color = "orangetext ";
                                                        } elseif ($customerRequest[$i]['status'] == "pending" || $customerRequest[$i]['status'] == "assign") {
                                                            $status = "In Queue";
                                                            $color = "greentext ";
                                                        } else {
                                                            $status = "";
                                                            $color = "greentext ";
                                                        }
                                                        ?>
                        <?php if ($status == 'In Queue'): ?>
                                                            <span class="gray text-uppercase">
                            <?php echo $status; ?>
                                                            </span>
                                                            <?php else: ?>
                                                            <span class="green <?php echo $color; ?> text-uppercase">
                                                                <?php echo $status; ?>
                                                            </span>
                                                            <?php endif ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pro-desh-box dcol-2">
                                            <div class="pro-circle-list clearfix">
                                                <div class="pro-circle-box">
                                                    <a href=""><figure class="pro-circle-img">
                        <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                                                                <?php } else { ?>
                                                                <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                                                                    <?php } ?>
                                                        </figure>
                                                        <p class="pro-circle-txt text-center">
                                                                <?php echo $customerRequest[$i]['designer_first_name']; ?>
                                                        </p></a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pro-desh-box">
                                            <div class="pro-desh-r1">
                                                <div class="pro-inleftbox">
                                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                                                            <?php if ($customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count'] != 0) { ?>
                                                                    <span class="numcircle-box">
                            <?php echo $customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count']; ?>
                                                                    </span>
                        <?php } ?></span></a></p>
                                                </div>
                                                <div class="pro-inrightbox">
                                                    <p class="pro-a inline-per"><a style="position: relative;"  href="<?php echo base_url() ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                                    <?php if (count($customerRequest[$i]['files_new']) != 0) { ?>
                                                                    <span class="numcircle-box">
                                        <?php echo count($customerRequest[$i]['files_new']); ?>
                                                                    </span>
                                    <?php } ?>
                                                            </span>
                        <?php echo count($customerRequest[$i]['files']); ?>
                                                        </a>
                                                        <a href="<?php echo base_url(); ?>account/request/deleteproject/<?php echo $customerRequest[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <!-- -->
                                            <?php
                                        }
                                    }
                                    for ($i = 0; $i < count($customerRequest); $i++) {
                                        if ($customerRequest[$i]['status'] != "active" && $customerRequest[$i]['status'] != "checkforapprove" && $customerRequest[$i]['status'] != "disapprove") {
                                            ?>
                                    <!-- List Desh Product -->
                                    <div class="pro-desh-row">
                                        <div class="pro-desh-box delivery-desh" style="padding-right: 0px; padding-left: 15px;">
                                            <!-- <p class="pro-a">Delivery</p> -->
                                            <!-- <p class="pro-b"> -->
                                                        <?php
                                                        // $created_date =  date($customerRequest[$i]['dateinprogress']);
                                                        // $expected_date = date('m/d/Y', strtotime($created_date . ' +1 day'));
                                                        // if ( $customerRequest[ $i ][ 'modified' ] == "" ) {
                                                        //     echo $expected_date;
                                                        // } else {
                                                        //     echo date( "m/d/Y", strtotime( $customerRequest[ $i ][ 'modified' ] ) );
                                                        // }
                                                        ?>                          
                                            <!-- </p> -->
                                            <?php if ($customerRequest[$i]['status'] == "assign") { ?>
                                                <p class="pro-a">Priority
                                                    <span class="select-pro-a">
                                                        <select onchange="prioritize(this.value, <?php echo $customerRequest[$i]['priority'] ?>,<?php echo $customerRequest[$i]['id'] ?>)">
                            <?php for ($j = 1; $j <= $activeTab; $j++) { ?>
                                                                <option value="<?php echo $j; ?>"
                                                                    <?php
                                                                    if ($j == $customerRequest[$i]['priority']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>
                                                                        ><?php echo $j; ?></option>                                             
                                                            <?php } ?>
                                                        </select>
                                                    </span>
                                                </p>
                                                        <?php } ?>
                                        </div>

                                        <div class="pro-desh-box dcol-1" style="cursor:pointer;" onclick="window.location = '<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>'">
                                            <div class="desh-head-wwq">
                                                <div class="desh-inblock">
                                                    <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>">
                                                        <?php echo $customerRequest[$i]['title']; ?>
                                                        </a></h3>
                                                    <p class="pro-b"><?php echo $customerRequest[$i]['category']; ?></p>
                                                </div>
                                                <div class="desh-inblock">
                                                    <p class="neft pull-right">
                                                        <?php
                                                        if ($customerRequest[$i]['status'] == "checkforapprove") {
                                                            $status = "Review your design";
                                                            $color = "bluetext";
                                                        } elseif ($customerRequest[$i]['status'] == "active") {
                                                            $status = "Design In Progress";
                                                            $color = "blacktext";
                                                        } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                            $status = "Revision In Progress";
                                                            $color = "orangetext ";
                                                        } elseif ($customerRequest[$i]['status'] == "pending" || $customerRequest[$i]['status'] == "assign") {
                                                            $status = "In Queue";
                                                            $color = "greentext ";
                                                        } else {
                                                            $status = "";
                                                            $color = "greentext ";
                                                        }
                                                        ?>
                        <?php if ($status == 'In Queue'): ?>
                                                            <span class="gray text-uppercase">
                                                        <?php echo $status; ?>
                                                            </span>
                                                    <?php else: ?>
                                                            <span class="green <?php echo $color; ?> text-uppercase">
                                                        <?php echo $status; ?>
                                                            </span>
                        <?php endif ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pro-desh-box dcol-2">
                                            <div class="pro-circle-list clearfix">
                                                <div class="pro-circle-box">
                                                    <h4 class="head-c draft_no">No Designer assigned yet</h4>
                                                    <!-- <a href=""><figure class="pro-circle-img">
                                                                <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                                                                    <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                                                                <?php } ?>
                                                    </figure>
                                                    <p class="pro-circle-txt text-center">
                        <?php echo $customerRequest[$i]['designer_first_name']; ?>
                                                    </p></a> -->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pro-desh-box">
                                            <div class="pro-desh-r1">
                                                <div class="pro-inleftbox">
                                                    <p class="pro-a inline-per"><a  style="position: relative;" href="<?php echo base_url() ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                                                            <?php if ($customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count'] != 0) { ?>
                                                                    <span class="numcircle-box">
                            <?php echo $customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count']; ?>
                                                                    </span>
                        <?php } ?>
                                                            </span>
                                                        </a>
                                                    </p>
                                                </div>
                                                <div class="pro-inrightbox">
                                                    <p class="pro-a inline-per"><a style="position: relative;" href="<?php echo base_url() ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                        <?php if (count($customerRequest[$i]['files_new']) != 0) { ?>
                                                                    <span class="numcircle-box">
                            <?php echo count($customerRequest[$i]['files_new']); ?>
                                                                    </span>
                        <?php } ?>
                                                            </span>
                        <?php echo count($customerRequest[$i]['files']); ?>
                                                        </a>
                                                        <a href="<?php echo base_url(); ?>account/request/deleteproject/<?php echo $customerRequest[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <!-- -->
                    <?php }
                }
                ?>
                        </div>
                    </div>
                </div>
            <?php } elseif ($customerRequest[$i]['status'] == 'draft') { ?>
                <!-- In Progress Request Tab-->
                <div class="tab-pane content-datatable datatable-width" id="inprogressrequest" role="tabpanel">
                    <div class="cli-ent-add-row">
                        <a href="<?php echo base_url() ?>account/request/new_request/category" class="cli-ent-add-col" >
                            <span class="cli-ent-add-icon">+</span>
                        </a>
                    </div>
                    <!-- For Drafted Projects -->
                <?php for ($i = 0; $i < count($customerRequest); $i++) { ?>
                        <!-- List Desh Product -->
                        <div class="pro-desh-row">
                            <div class="pro-desh-box delivery-desh">
                                <h3 class="app-roved gray">DRAFT</h3>
                            </div>

                            <div class="pro-desh-box dcol-1"  style="cursor:pointer;" onclick="window.location = '<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>'">
                                <div class="desh-head-wwq">
                                    <div class="desh-inblock">
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>"><?php echo $customerRequest[$i]['title']; ?></a></h3>
                    <?php if (!empty($customerRequest[$i]['category'])): ?>
                                        <?php $cat = $customerRequest[$i]['category']; ?>
                                    <?php else: ?>
                                            $cat = "Not Categorized";
                                                <?php endif; ?>
                            <!-- <p class="pro-b"><?php echo $cat; ?></p> -->
                                    </div>
                                    <div class="desh-inblock">
                                        <p class="neft pull-right">
                                            <span class="gray text-uppercase">
                    <?php echo $customerRequest[$i]['status']; ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="pro-desh-box dcol-2">
                                <div class="pro-circle-list clearfix">
                    <?php if ($customerRequest[$i]['designer_first_name']): ?>
                                        <div class="pro-circle-box">
                                            <a href=""><figure class="pro-circle-img">
                        <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                        <?php } else { ?>
                                                        <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                                                    <?php } ?>
                                                </figure>
                                                <p class="pro-circle-txt text-center">
                                                        <?php echo $customerRequest[$i]['designer_first_name']; ?>
                                                </p></a>
                                        </div>
                    <?php else: ?>
                                        <h4 class="head-c draft_no">No Designer assigned yet</h4>

                                            <?php endif ?>

                                </div>
                            </div>

                            <div class="pro-desh-box">
                                <div class="pro-desh-r1">
                                    <div class="pro-inleftbox">
                                        <p class="pro-a inline-per"><a style="position: relative;" href="javascript:void(0)"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                    <?php if ($customerRequest[$i]['total_chat'] != 0) { ?>
                                                    <span class="numcircle-box">
                        <?php echo $customerRequest[$i]['total_chat']; ?>
                                                    </span>
                    <?php } ?></a></p>
                                    </div>
                                    <div class="pro-inrightbox">
                                        <p class="pro-a inline-per"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">
                    <?php echo count($customerRequest[$i]['files']); ?>
                                            <a href="<?php echo base_url(); ?>account/request/deleteproject/<?php echo $customerRequest[$i]['id']; ?>" class="reddelete"><i class="fa fa-trash " aria-hidden="true"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <!-- -->
                                <?php } ?>

                    <p class="space-e"></p>
                </div>
                            <?php } elseif ($customerRequest[$i]['status'] == 'approved') { ?>
                <!-- Approved Designs Tab -->
                <div class="tab-pane content-datatable datatable-width" id="approved_designs_tab" role="tabpanel">
                    <div class="cli-ent-add-row">
                        <a href="<?php echo base_url() ?>account/request/new_request/category" class="cli-ent-add-col" >
                            <span class="cli-ent-add-icon">+</span>
                        </a>
                    </div>
                    <!-- For Completed Projects -->
                <?php for ($i = 0; $i < count($customerRequest); $i++) { ?>
                        <!-- List Desh Product -->
                        <div class="pro-desh-row">
                            <div class="pro-desh-box delivery-desh">
                                <p class="pro-a"><?php
                    if ($customerRequest[$i]['status'] == "approved") {
                        echo "Approved on";
                    }
                    ?></p>
                                <p class="pro-b">
                                                <?php
                                                if ($customerRequest[$i]['status'] == "approved") {
                                                    echo date('m/d/Y', strtotime($customerRequest[$i]['approvddate']));
                                                }
                                                ?>                          
                                </p>
                            </div>

                            <div class="pro-desh-box dcol-1"  style="cursor:pointer;" onclick="window.location = '<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>'">
                                <div class="desh-head-wwq">
                                    <div class="desh-inblock">
                                        <h3 class="pro-head space-b"><a href="<?php echo base_url(); ?>account/request/project-info/<?php echo $customerRequest[$i]['id']; ?>"><?php echo $customerRequest[$i]['title']; ?></a></h3>
                                        <p class="pro-b"><?php echo $customerRequest[$i]['category']; ?></p>
                                    </div>
                                    <div class="desh-inblock">
                                        <p class="neft pull-right"><span class="green text-uppercase">
                                                <?php
                                                if ($customerRequest[$i]['status'] == " checkforapprove") {
                                                    $status = "Review your design";
                                                    $color = "greentext";
                                                } elseif ($customerRequest[$i]['status'] == "active") {
                                                    $status = "Design In Progress";
                                                    $color = "blacktext";
                                                } elseif ($customerRequest[$i]['status'] == "disapprove") {
                                                    $status = "Revision In Progress";
                                                    $color = "orangetext ";
                                                } elseif ($customerRequest[$i]['status'] == "pending" || $customerRequest[$i]['status'] == "assign") {
                                                    $status = "In Queue ";
                                                    $color = "greentext ";
                                                } else {
                                                    $status = "Completed";
                                                    $color = "greentext ";
                                                }
                                                echo $status;
                                                ?>
                                            </span></p>
                                    </div>
                                </div>
                            </div>

                            <div class="pro-desh-box dcol-2">
                                <div class="pro-circle-list clearfix">
                                    <div class="pro-circle-box">
                                        <a href=""><figure class="pro-circle-img">
                                                    <?php if ($customerRequest[$i]['profile_picture']) { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE . $customerRequest[$i]['profile_picture']; ?>" class="img-responsive" />
                                                        <?php } else { ?>
                                                    <img src="<?php echo FS_PATH_PUBLIC_UPLOADS_PROFILE."user-admin.png" ?>" class="img-responsive" />
                                                    <?php } ?>
                                            </figure>
                                            <p class="pro-circle-txt text-center">
                    <?php echo $customerRequest[$i]['designer_first_name']; ?>
                                            </p></a>
                                    </div>
                                </div>
                            </div>

                            <div class="pro-desh-box">
                                <div class="pro-desh-r1">
                                    <div class="pro-inleftbox">
                                        <p class="pro-a inline-per"><a style="position: relative;" href=""><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-chat.png" class="img-responsive" width="21">
                                                <?php if ($customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count'] != 0) { ?>
                                                        <span class="numcircle-box">
                        <?php echo $customerRequest[$i]['total_chat'] + $customerRequest[$i]['comment_count']; ?>
                                                        </span>
                    <?php } ?>
                                                </span>
                                            </a></p>
                                    </div>
                                    <div class="pro-inrightbox">
                                        <p class="pro-a inline-per"><a style="position: relative;" href="javascript:void(0)"><span class="inline-imgsssx"><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-file.png" class="img-responsive" width="13">

                    <?php if (count($customerRequest[$i]['files_new']) != 0) { ?>
                                                        <span class="numcircle-box">
                        <?php echo count($customerRequest[$i]['files_new']); ?>
                                                        </span>
                    <?php } ?>
                                                </span>
                    <?php echo count($customerRequest[$i]['files']); ?>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <!-- -->
                <?php } ?>

                    <p class="space-e"></p>

                    <!-- <div class="loader">
                        <a class="loader-link text-uppercase" href=""><img src="<?php echo base_url() ?>theme/customer-assets/images/icon-loader.png" class="img-responsive" width="28"> Loading</a>
                    </div> -->
                </div>
                <?php
            }
        }
    }

    public function deleteCustomeImg($id) {
        $success = $this->S_admin_model->delete_data('request_files', $id);
        if ($success) {
            $this->session->set_flashdata('message_success', "Your Image Deleted Successfully", 5);
            redirect(base_url() . "account/request/project-info/" . $_GET['requests']);
        } else {
            $this->session->set_flashdata('message_error', "Something went wrong.", 5);
            redirect(base_url() . "account/request/project-info/" . $_GET['requests']);
        }
    }

    public function onlytimezone($date_zone) {
        $nextdate = $this->s_myfunctions->onlytimezoneforall($date_zone);
        return $nextdate;
    }

    public function deleteproject($id) {
       
        $user_id = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        $login_user_data = $this->load->get_var('login_user_data'); 
        $user_permsn = $this->S_request_model->getAgencyuserinfo($user_id);
        $ismanual = $user_permsn[0]['is_manual'];
        if($login_user_data[0]["role"] == "manager"){
            $u_id = $user_id;
        }else{
            $u_id = $login_user_id;
        }
        $path = FCPATH . "public/uploads/requests/" . $id . "/*";
        $path1 = FCPATH . "public/uploads/requests/" . $id;
        $files = glob($path); // get all file names
        if (file_exists($path1)) {
            foreach ($files as $file) { // iterate files
                if (is_file($file))
                    unlink($file); // delete file
            }
            rmdir($path1);
        }
        $table =  $this->S_request_model->getDataBasedonSAAS($id);
        if($table == "requests"){
            $rfc = "request_file_chat";
            $rf = "request_files";
            $rd = "request_discussions";
        }else{
            $rfc = "s_request_file_chat";
            $rf = "s_request_files";
            $rd = "s_request_discussions";
        }
        $requests = $this->S_request_model->get_request_by_id($id);
        $deletedpriority = isset($requests[0]['priority']) ? $requests[0]['priority'] : '';
        $success = $this->S_admin_model->delete_request($table, array('id' => $id));
        if ($deletedpriority != 0 && $ismanual != 1) {
            $deletedsubpriority = isset($requests[0]['sub_user_priority']) ? $requests[0]['sub_user_priority'] : '';
            $priorfrom = $deletedpriority + 1;
            $subpriorfrom = $deletedsubpriority + 1;
            $this->S_admin_model->update_priority_after_approved_qa(1, $priorfrom, $u_id,$table);
            $this->S_admin_model->update_subuserpriority_after_approved_qa(1, $u_id,$requests[0]['created_by'],$subpriorfrom,$table);
        }
        if ($success) {
            if (!empty($requests[0]['designer_attachment'])) {
                foreach ($requests[0]['designer_attachment'] as $data) {
                    $this->S_admin_model->delete_request_file_data($rfc, $data['id']);
                }
            }
            $this->S_admin_model->delete_request_data($rd, $id);
//            $this->S_admin_model->delete_request_data("message_notification", $id);
//            $this->S_admin_model->delete_request_data("notification", $id);
            $this->S_admin_model->delete_request_data($rf, $id);
            $customer_data = $this->S_admin_model->getuser_data($u_id);
            if($customer_data[0]['is_cancel_subscription'] != 1 && $ismanual != 1){
            $ActiveReq = $this->s_myfunctions->CheckActiveRequest($u_id);
            if($ActiveReq == 1){
               $this->s_myfunctions->move_project_inqueqe_to_inprogress($u_id,'','deleteproject',$id);
            }
            }
            
           // $this->s_myfunctions->capture_project_activity($id,'','','delete_project','deleteproject',$login_user_id);
            $this->session->set_flashdata('message_success', "Your project deleted successfully", 5);
            redirect(base_url() . "account/dashboard");
        }
    }


    public function send_email_when_inqueue_to_inprogress($project_title, $id) {
        require_once(APPPATH . "libraries/portal_lan/email/class.email.function.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.phpmailer.php");
        require_once(APPPATH . "libraries/portal_lan/email/class.smtp.php");
        $receiver = $_SESSION['email'];
        $query1 = $this->db->select('first_name')->from('users')->where('email', $receiver)->get();
        $fname = $query1->result();
        $data = array(
            'title' => $project_title,
            'project_id' => $id,
            'fname' => isset($fname[0]->first_name) ? $fname[0]->first_name : ''
        );
        $body = $this->load->view('emailtemplate/inqueue_template', $data, TRUE);
        $subject = 'Designer has Started Working on your New Project.';

        $message = $body;

        $title = 'GraphicsZoo';

        //SM_sendMail($receiver,$subject,$message,$title);          
    }

    public function user_update_tour() {
        $this->Welcome_model->update_data("users", array("tour" => 1), array("id" => $_SESSION['user_id']));
    }

    public function load_more_customer() { 
        $parent_user_plan = $this->load->get_var('parent_user_plan');
        $group_no = $this->input->post('group_no');
        $viewtype = $this->input->post('viewtype');
        $status = $this->input->post('status');
        $search = $this->input->post('search');
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id'); 
        $isSearch = (trim($search) != '') ? true : false;
        $clientid = $this->input->post('client_id');
        $client_id = ($clientid != '') ? $clientid : "";
        $brandid = $this->input->post('brand_id');
        $brand_id = ($brandid != '') ? $brandid : "";
        $content_per_page = LIMIT_ALL_LIST_COUNT;
        $start = ceil($group_no * $content_per_page);
        $canuserdel = $this->s_myfunctions->isUserPermission('delete_req');
        $canuseradd = $this->s_myfunctions->isUserPermission('add_requests');
        $canaccessallbrands = $this->s_myfunctions->isUserPermission('brand_profile_access');
	$can_manage_priorities = $this->s_myfunctions->isUserPermission('manage_priorities');
        $assign_designer_toproject = $this->s_myfunctions->isUserPermission('assign_designer_to_project');
        $change_project_status = $this->s_myfunctions->isUserPermission('change_project_status');
        $all_status = $this->get_statustab_acc_user();
            $statusArray = explode(',', $status);
            $active_project = $this->S_request_model->customer_load_more($statusArray, $login_user_id, false, $start, $content_per_page, 'priority', $search,$brand_id,$client_id,$all_status["by"]);
            $count_total_projects = $this->S_request_model->customer_load_more($statusArray, $login_user_id, true, $start, $content_per_page, 'priority', $search,$brand_id,$client_id,$all_status["by"]);
            $assign_requests = $this->S_request_model->getallrequestcount_accstatus($login_user_id, array('assign'));
            $subuser_assign_requests = $this->S_request_model->getallrequestcount_accstatus($login_user_id, array('assign'),1);
            $activeTab = 0;
            if(!empty($active_project)){
            for ($i = 0; $i < sizeof($active_project); $i++) {
                $active_project[$i]['customer_variables'] = $this->s_myfunctions->customerVariables($active_project[$i]['status']);
                $active_project[$i]['total_chat'] = $this->S_request_model->get_chat_number($active_project[$i]['id'], $active_project[$i]['customer_id'], $active_project[$i]['designer_id'], "customer");
                $getlatestfiledraft = $this->S_request_model->get_latestdraft_file($active_project[$i]['id']);
                $active_project[$i]['latest_draft'] = $getlatestfiledraft[0]['file_name'];
                if ($active_project[$i][$all_status["by"]] == "active" || $active_project[$i][$all_status["by"]] == "disapprove") {
                    if ($active_project[$i]['expected_date'] == '' || $active_project[$i]['expected_date'] == NULL) {
                        $active_project[$i]['expected'] = $this->s_myfunctions->check_timezone($active_project[$i]['latest_update'],$parent_user_plan);
                    } else {
                        $active_project[$i]['expected'] = $this->onlytimezone($active_project[$i]['expected_date']);
                    } 
                } elseif ($active_project[$i][$all_status["by"]] == "checkforapprove") {
                    $active_project[$i]['deliverydate'] = $this->onlytimezone($active_project[$i]['approvaldate']);
                } elseif ($active_project[$i][$all_status["by"]] == "approved") {
                    $active_project[$i]['approvddate'] = $this->onlytimezone($active_project[$i]['approvaldate']);
                } elseif ($active_project[$i][$all_status["by"]] == "pendingrevision") {
                    $active_project[$i]['revisiondate'] = $this->onlytimezone($active_project[$i]['modified']);
                }
                
                $id = $active_project[$i]['id'];
                $data = $this->S_request_model->get_request_by_id($id);
                $active_project[$i]['max_priority'] = $assign_requests;
                $active_project[$i]['subuser_max_priority'] = $subuser_assign_requests;
                $active_project[$i]['c_name'] = $this->S_request_model->getuserbyid($active_project[$i]['customer_id'],'client');
                $active_project[$i]['client_name'] = $this->S_request_model->getuserbyid($active_project[$i]['created_by'],'client');
                $active_project[$i]['al_status'] = $all_status;
                if ($active_project[$i]['status'] == "assign") {
                    $activeTab++;
                }
                $active_project[$i]['addClass'] = $this->adminCheckClassforDesignerAssign($active_project[$i][$all_status["by"]]);
                $data['projects'] = $active_project[$i];
                $data['canuserdel'] = $canuserdel;
                $data['canuseradd'] = $canuseradd;
                $data['canaccessallbrands'] = $canaccessallbrands;
                $data['can_manage_priorities'] = $can_manage_priorities;
                $data['assign_designer_toproject'] = $assign_designer_toproject;
                $data['change_project_status'] = $change_project_status;
                if($viewtype == 'grid'){
//                    echo "<pre>";print_R($data);
                $this->load->view('account/load_more_customer',$data);  
                }else{
                 $this->load->view('account/projects_list_view',$data);    
                }
            }
            ?>
            <span id="loadingAjaxCount" data-value="<?php echo $count_total_projects; ?>"></span>
            <?php
            }
    }

    public function upgrADE_account() {
        $result = array();
        $plan_name = $_POST['plan_name'];
        $inprogress_request = $_POST['in_progress_request'];
        $plan_price = $_POST['plan_price'];
        $titlestatus = "";
        $planlist = $this->Stripe->getallsubscriptionlist();
        $user_data = $this->S_admin_model->getuser_data($_POST['customer_id']);
        $getsubscriptionplan = $this->S_request_model->getsubscriptionlistbyplanid($user_data[0]['plan_name']);
        if($user_data[0]['overwrite'] == 1 && isset($user_data[0]['turn_around_days'])){
            $turn_around_days = $user_data[0]['turn_around_days'];
        }else{
            $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
        }
        $rendomly_qa_id = $this->S_admin_model->getqa_id();
        if (!empty($_POST)) {
            if(isset($_POST['state_tax']) && $_POST['state_tax'] != 0){
                $state_tax = $_POST['state_tax'];
            }else{
                $state_tax = 0;
            }
            $state = isset($_POST['state'])?$_POST['state']:'';
            $stripe_customer = $this->Stripe->createnewCustomer($_POST['email'], trim($_POST['discount'])); 
            if (!$stripe_customer['status']) {
                $this->session->set_flashdata('message_error', "Error occurred while upgrading plan!", 5);
                $result['error'] = $stripe_customer['message'];
				echo json_encode($result);exit;
            }

            $expir_date = explode("/", $_POST['expir_date']);
            $customer_id = $stripe_customer['data']['customer_id'];
            $card_number = trim($_POST['card_number']);
            $expiry_month = $expir_date[0];
            $expiry_year = $expir_date[1];
            $cvc = $_POST['cvc'];
            $stripe_card = $this->Stripe->createStripeCard($customer_id, $card_number, $expiry_month, $expiry_year, $cvc);
            if (!$stripe_card['status']):
                $this->session->set_flashdata('message_error', $stripe_card['message'], 5);
                $result['error'] = $stripe_card['message'];
				echo json_encode($result);exit;
            endif;

            $subscription_plan_id = $plan_name;
            if($subscription_plan_id == SUBSCRIPTION_99_PLAN){
                $current_date = date("Y:m:d H:i:s");
                $monthly_date = strtotime("+1 months", strtotime($current_date)); // returns timestamp
                $billing_expire = date('Y:m:d H:i:s',$monthly_date);
                $customer['billing_cycle_request'] = 3;
                $customer['payment_status'] = 2;
           }else {
               $customer['billing_cycle_request'] = 0;
           }

            $stripe_subscription = '';
            if ($subscription_plan_id) {
                $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($stripe_customer['data']['customer_id'], $subscription_plan_id,$inprogress_request,$state_tax);
                $current_date = date('Y-m-d H:i:s',$stripe_subscription['data']['period_start']);
                $billing_expire = date('Y-m-d H:i:s',$stripe_subscription['data']['period_end']);
            }

            if (!$stripe_subscription['status']) {

                $this->session->set_flashdata('message_error', $stripe_subscription['message'], 5);
                $result['error'] = $stripe_subscription['message'];
		echo json_encode($result);exit;
            }
            

            $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($set), 0, 20);
            $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
            $customer['billing_start_date'] = $current_date;
            $customer['billing_end_date'] = $billing_expire;
            $customer['customer_id'] = $stripe_customer['data']['customer_id'];
            $customer['plan_name'] = $subscription_plan_id;
            $plandetails = $this->Stripe->retrieveoneplan($subscription_plan_id);
            $customer['is_active'] = 1;
            $customer['is_trail'] = 0;
            $customer['without_pay_user'] = 0;
            $customer['state'] = $state;
            
//            $customer['payment_status'] = 2;
            $customer['plan_amount'] = $plan_price;
            $customer['plan_interval'] = $plandetails['interval'];
            $customer['plan_trial_period_days'] = $plandetails['trial_period_days'];
            $customer['plan_turn_around_days'] = $plandetails['metadata']->turn_around_days;
            $customer['display_plan_name'] = $plandetails['name'];
            $customer['total_inprogress_req'] = $inprogress_request;
            $customer['total_active_req'] = $inprogress_request * TOTAL_ACTIVE_REQUEST;
//            $customer['timezone'] = $timezone;
            //echo '<pre>';print_r($customer);  exit();
            $id = $this->Welcome_model->update_data("users", $customer, array("id" => $_POST['customer_id']));
            if ($id) {
                
                $request = $this->S_request_model->get_dummy_request($_POST['customer_id'],1,'draft');
                if(!empty($request)){
                    $subcat_data = $this->S_category_model->get_category_byID($request[0]['subcategory_id']);
                    $expected = $this->s_myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), $subscription_plan_id,$subcat_data[0]['bucket_type'], $subcat_data[0]['timeline'],$turn_around_days);
                    $this->Welcome_model->update_data("requests", array("status" => "active", "status_admin" => "active", "status_designer" => "active", "status_qa" => "active","dummy_request"=> 0, "dateinprogress" => date("Y-m-d H:i:s"), "priority" => 0, "latest_update" => date("Y-m-d H:i:s"),"expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $request[0]['id']));
                    $this->s_myfunctions->SendQAmessagetoInprogressReq($request[0]['id'],$user_data[0]['qa_id'],$_POST['customer_id']);
                }
                /** delete dummy request **/
                $this->s_myfunctions->delete_dummy_request($_POST['customer_id']);
                /** delete dummy request **/
                $result['success'] = "Account upgraded successfully!";
            } else {
                $result['error'] = "Error occurred while upgrading account, please try again or contact us!";
                $this->session->set_flashdata('message_error', "Error occurred while upgrading account, please try again or contact us!", 5);
                //redirect(base_url() . "account/request/design_request");
            }
        }
        echo json_encode($result);
        exit;
    }

    // Assign designer
    public function assign_designer($category) {
        $get_all_designers_info = $this->S_request_model->get_designers_info($category);
        foreach ($get_all_designers_info as $designers) {
            $project_count = $this->S_request_model->get_request_count_by_designer_id($designers['user_id']);
            $count_min_request[$designers['user_id']] = $project_count;
        }
        return $count = array_keys($count_min_request, min($count_min_request));
    }

    public function refreshStatus($user_id = "",$actionfrom = "",$lastupdtreq = "") {
        $uid = $this->load->get_var('main_user');
        $userId = ($user_id == '') ? $uid : $user_id;
        $get_inqueue_request = $this->S_admin_model->get_all_request_count(array("assign"), $userId);
        if(!empty($get_inqueue_request)){
            
            $ActiveReq = $this->s_myfunctions->CheckActiveRequest($userId);
            if($ActiveReq == 1){
                $this->s_myfunctions->move_project_inqueqe_to_inprogress($userId,'',$actionfrom,$lastupdtreq);
            }
              
        }else {
        }
    }
    
     public function verifyPhoneNumber() {
        if(isset($_POST['verfyphn_number'])){
        $this->Welcome_model->update_data("users", array("phone" => $_POST['verfy_number'],"is_trailverified" => 1), array("id" => $_SESSION['user_id']));
        $receiver = SUPPORT_EMAIL;
        $array = array('PROJECT_LINK' => base_url()."admin/dashboard/view_request/".$_POST['req_id']);
        $this->s_myfunctions->send_email_to_users_by_template($_SESSION['user_id'], 'verify_phone_to_approve_design', $array, $receiver);
        redirect(base_url() . 'account/request/project_image_view/' . $_POST['request_url']);
        }
        
    }
    
    public function see_more_notifications($id){
        $notifications = $this->S_request_model->get_notifications($id);
        $notification_number = $this->S_request_model->get_notifications_number($id);
        $this->load->view('account/customer_header_1', array("notifications" => $notifications, "notification_number" => $notification_number));
        $this->load->view('account/see_more_notifications'); 
    }
    
    public function sharerequest(){
        $login_user_id = $this->load->get_var('login_user_id');
        $request_id = isset($_POST['request_id'])? $_POST['request_id']:'';
        $user_name = isset($_POST['user_name'])? $_POST['user_name']:'';
        $draft_id = isset($_POST['draft_id'])? $_POST['draft_id']:'';
        $shared_from = isset($_POST['shared_from'])? $_POST['shared_from']:'';
        $email = isset($_POST['email'])? $_POST['email']:'';
        $emaillists = explode(',',$email);
        $permissions = $_POST['link_permissions'];
        $request = $this->S_request_model->get_request_by_id($request_id);
        $table =  $this->S_request_model->getDataBasedonSAAS($request_id);
        if($table == 'requests'){
          $linkTB = 'link_sharing';
          $linkPrmsn = 'link_sharing_permissions';
        }else{
          $linkTB = 's_link_sharing'; 
          $linkPrmsn = 's_link_sharing_permissions';
        }
        $savedata = array();
        if(!empty($permissions)){
        foreach($emaillists as $kk => $vv){
            $checkemailexist = $this->S_request_model->isSheredEmaiexists($vv,$request_id,$draft_id);
            if($checkemailexist){
               $this->session->set_flashdata('message_error', "Email address you entered is already exists!", 5);
            }else{
            $data['share_key'] = $this->s_myfunctions->random_sharekey();
            $data['request_id'] = $request_id;
            $data['draft_id'] = $draft_id;
            $data['share_type'] = 'private';
            $data['is_disabled'] = 0;
            $data['user_name'] = $user_name;
            $data['email'] = $vv;
            $data['shared_by'] = $login_user_id;
            $data['created'] = date("Y:m:d H:i:s");
            $link_share_id  = $this->Welcome_model->insert_data($linkTB, $data);
            if($link_share_id){
                    $permis_data['sharing_id'] = $link_share_id;
                    $permis_data['can_view'] = 1;
                    $permis_data['created'] = date("Y:m:d H:i:s");
                    if(in_array('mark_as_completed',$permissions)){
                        $permis_data['mark_as_completed'] = 1;
                    }if(in_array('download_source_file',$permissions)){
                         $permis_data['download_source_file'] = 1;
                    }if(in_array('comment_revision',$permissions)){
                         $permis_data['commenting'] = 1;
                    }
                    $permis_data['created'] = date("Y:m:d H:i:s");
            $link_sharing_permission_id  = $this->Welcome_model->insert_data($linkPrmsn, $permis_data); 
            $issend = $this->send_permission_email($login_user_id, $data['share_key'],$vv,$user_name,$request[0]['title']);
            if($issend){
                die("send");
            }
           // $this->s_myfunctions->capture_project_activity($request_id,$draft_id,'','share_private_link','sharerequest',$login_user_id);
            $this->session->set_flashdata('message_success', "Link for the request shared successfully!", 5);
             //redirect(base_url() . "account/request/project-info/".$request_id);
            }else{
                $this->session->set_flashdata('message_error', "Something went wrong, Please try Again!", 5);
                //redirect(base_url() . "account/request/project_info/".$request_id);
           }
            }
        }
        }else{
          $this->session->set_flashdata('message_error', "You must choose atleast one permissions!", 5);  
        }
        if($draft_id == "" && $shared_from == ""){
          redirect(base_url() . "account/request/project_info/".$request_id."#peoples_tab");
        }else if($draft_id != "" && $shared_from == "popup"){
          redirect(base_url() . "account/request/project_info/".$request_id);
        }else{
          redirect(base_url() . "account/request/project_image_view/".$draft_id."?id=".$request_id."#peoples_tab");  
        }
}

    public function getallprivateaccount(){
        $share_type = isset($_POST['share_type'])? $_POST['share_type']:'';
        $reqid = isset($_POST['reqid'])? $_POST['reqid']:'';
        $draftid = isset($_POST['draftid'])? $_POST['draftid']:'0';
//        echo "reqid".$reqid."<br/>";
//        echo "draftid".$draftid;exit;
        $shared_data = $this->S_request_model->getlinkSharedprivateData($reqid,$draftid);
        echo json_encode($shared_data);
     }
     
     public function editsharerequest(){
        $sharekeys = isset($_POST['sharekeys'])? $_POST['sharekeys']:'';
        $shareID = isset($_POST['shareID'])? $_POST['shareID']:'';
        $data = array();
            $data['can_view'] = '1';
            if(in_array('mark_as_completed', $sharekeys)){
                $data['mark_as_completed'] = '1';
            }else{
                 $data['mark_as_completed'] = '0';
            }if(in_array('download_source_file', $sharekeys)){
                $data['download_source_file'] = '1';
            }else{
                 $data['download_source_file'] = '0';
            }if(in_array('comment_revision', $sharekeys)){
                $data['commenting'] = '1';
            }else{
                 $data['commenting'] = '0';
            }
            $success = $this->Welcome_model->update_data("link_sharing_permissions", $data, array("sharing_id" => $shareID));
            if($success){
                echo 'saved';
//              die("saved");
            }else{
                echo 'not saved';
//                die("not saved");
            }
            
        //}
     }
     
     public function getAllselectedpermissions(){
//         echo "<pre/>";print_R($_POST);exit;
         $shareID = isset($_POST['shareID'])? $_POST['shareID']:'';
         $reqID = isset($_POST['req_id'])? $_POST['req_id']:'';
         $selectedpermissions = $this->S_request_model->getAllpermissionsByshareID($shareID,$reqID);
         //echo "<pre/>";print_r($selectedpermissions);
         echo json_encode($selectedpermissions);
     }
     
      public function publiclinksave($reqid = "", $draf_tid = "") {
        $table =  $this->S_request_model->getDataBasedonSAAS($reqid);
        if($table == 'requests'){
            $linkTB = 'link_sharing';
            $linkPrmsn = 'link_sharing_permissions';
        }else{
          $linkTB = 's_link_sharing'; 
          $linkPrmsn = 's_link_sharing_permissions';
        }
        $created_user = $this->load->get_var('main_user');
        $login_user = $this->load->get_var('login_user_id');
//        echo "created_user".$created_user;exit;
        $user_data = $this->S_admin_model->getuser_data($login_user);
        $proID = isset($_POST['proID']) ? $_POST['proID'] : $reqid;
        $draftid = isset($_POST['draftid']) ? $_POST['draftid'] : $draf_tid;
        $publiclinkexists = $this->S_request_model->publicLinkexists($proID, $draftid);
        if (empty($publiclinkexists)) {
            $data = $output = array();
            $share_key = $this->s_myfunctions->random_sharekey();
            $data['request_id'] = $proID;
            $data['draft_id'] = $draftid;
            $data['share_type'] = 'public';
            $data['is_disabled'] = 1;
            $data['share_key'] = $share_key;
            $data['created'] = date("Y-m-d H:i:s");
            $subdomain_url = $this->s_myfunctions->dynamic_urlforsubuser_inemail($created_user, $user_data[0]['plan_name']);
            $data['public_link'] = $subdomain_url . 's_project-info/' . $share_key;
            $success = $this->Welcome_model->insert_data($linkTB, $data);
            if ($success) {
                $output['publiclinkexists'] = $data['public_link'];
                $output['is_disabled'] = 1;
                $output['issaved'] = "1";
                $permsn['sharing_id'] = $success;
                $permsn['can_view'] = 1;
                $permsn['mark_as_completed'] = 1;
                $permsn['download_source_file'] = 1;
                $permsn['commenting'] = 1;
                $permsn['created'] = date("Y:m:d H:i:s");
                $success = $this->Welcome_model->insert_data($linkPrmsn, $permsn);
            }
        } else {
            $output['publiclinkexists'] = $publiclinkexists[0]['public_link'];
            $output['is_disabled'] = $publiclinkexists[0]['is_disabled'];
            $output['issaved'] = "1"; 
        }
        $permissions = $this->S_request_model->getpublicpermissions($publiclinkexists[0]['id'],$proID);
//        echo "<pre/>";print_R($publiclinkexists);exit;
        $output['permissions'] = $permissions;
//        echo "<pre/>";print_R($output);exit;
        if ($draf_tid == "") {
            echo json_encode($output);
        }
    }
     
     
     public function changepublicpermissions(){
         $publink = isset($_POST['publink'])? $_POST['publink']:'';
         $permissions = isset($_POST['permissions'])? $_POST['permissions']:'';
         $reqID = isset($_POST['reqID'])? $_POST['reqID']:'';
         $linkshareID = $this->S_request_model->changepublicpermissions($publink);
         $table =  $this->S_request_model->getDataBasedonSAAS($reqID);
            if($table == 'requests'){
                $up_tb = "link_sharing_permissions";
            }else{
               $up_tb = "s_link_sharing_permissions"; 
            }
         $data = array();
         if($linkshareID){
             if(in_array('download_source_file',$permissions)){
               $data['download_source_file'] =  1;
             }else{
               $data['download_source_file'] =  0;  
             }if(in_array('mark_as_completed',$permissions)){
               $data['mark_as_completed'] =  1;
             }else{
               $data['mark_as_completed'] =  0;  
             }
             if(in_array('comment_revision',$permissions)){
                $data['commenting'] =  1;  
             }else{
               $data['commenting'] =  0;  
             }
             $success = $this->Welcome_model->update_data($up_tb, $data, array("sharing_id" => $linkshareID[0]['id']));
             if($success){
                 echo "1";
             }
             }
     }

     public function changepublicpermissionsforpeople(){
        $share_id = isset($_POST['share_id'])? $_POST['share_id']:'';
        $share_value = isset($_POST['share_value'])? $_POST['share_value']:'';
        $req_id = isset($_POST['req_id'])? $_POST['req_id']:'';
        $share_checked = isset($_POST['share_checked'])? $_POST['share_checked']:'';
        $table =  $this->S_request_model->getDataBasedonSAAS($req_id);
        if($table == 'requests'){
            $up_tb = 'link_sharing_permissions';
        }else{
           $up_tb = 's_link_sharing_permissions'; 
        }
        $data = array($share_value => $share_checked);
        $success = $this->Welcome_model->update_data($up_tb, $data, array("sharing_id" => $share_id));
        if ($success) {
            echo "1";
        }
    }
     
     public function deleteShareduser(){
         $sharedID = isset($_POST['shared_id'])?$_POST['shared_id']:'';
         $deldata = $this->S_request_model->deleteShareduser($sharedID);
         echo $deldata;
     }
     
     public function send_permission_email($id,$key,$email,$user_name = NULL,$title = NULL) {
        $created_user = $this->load->get_var('main_user');
        $user_data = $this->S_admin_model->getuser_data($created_user);
        $senderId = ($id)?$id:$_SESSION['user_id'];
        $query1 = $this->db->select('first_name')->from('users')->where('id', $senderId)->get();
        $subdomain_url = $this->s_myfunctions->dynamic_urlforsubuser_inemail($created_user,$user_data[0]['plan_name']);
        $fname = $query1->result();
         $array = array('CUSTOMER_NAME' => $user_name,
                            'SENDER_NAME' => isset($fname[0]->first_name) ? $fname[0]->first_name : '',
                            'PROJECT_LINK' => $subdomain_url."project-info/".$key,
                                'PROJECT_TITLE' => $title);
        $this->s_myfunctions->send_email_to_users_by_template($id, 'link_sharing_email', $array, $email);
               
    }
    
    public function add_request_todraft_withoutpayuser() {
        $output = array();
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $subcat_id = isset($_POST['subcategory_id']) ? $_POST['subcategory_id'] : '';
        $subcat_data = $this->S_category_model->get_category_byID($subcat_id);
        $bucketType = $subcat_data[0]['bucket_type'];
        $fileType="";
        if($_POST['color_pref']){
        if(in_array('Let Designer Choose',$_POST['color_pref'])){
            $plate = 'Let Designer Choose';
        }}else{
            $plate = isset($_POST['HexCodeVal']) ? $_POST['HexCodeVal'] : 'Let Designer Choose';
        }
        if (isset($_POST['filetype'])) {
            $fileType = implode(",", $_POST['filetype']);
        }
        //echo "<pre>";print_r($_POST);exit;
        $data = array("customer_id" => $created_user,
            "category" => isset($_POST['category'])?$_POST['category']:'',
            "logo_brand" => isset($_POST['logo-brand'])?$_POST['logo-brand']:'',
            "brand_id" => isset($_POST['brand_name']) ? $_POST['brand_name'] : '',
            "category_id" => isset($_POST['category_id'])?$_POST['category_id']:'',
            "subcategory_id" => isset($_POST['subcategory_id'])?$_POST['subcategory_id']:'',
            "category_bucket" => $bucketType,
            "title" => isset($_POST['title'])?$_POST['title']:'',
            "business_industry" => isset($_POST['target_market'])?$_POST['target_market']:'',
            "design_dimension" => isset($_POST['dimension'])?$_POST['dimension']:'',
            "description" => isset($_POST['description'])?$_POST['description']:'',
            "created" => date("Y-m-d H:i:s"),
            "status" => "draft",
            "designer_id" => 0,
            "dummy_request" =>1,
            "priority" => 0,
            "deliverables" => $fileType,
            "created_by" => $login_user_id,
            "last_modified_by" => $login_user_id,
            "color_pref" => $plate
            );
        
        $success = $this->Welcome_model->insert_data("requests", $data);
        //echo $success;
        if(isset($success)){
        $id = $success;
        $this->addFileRequest($id);
        $output['status'] = "1";
        }else{
            $output['status'] = "0";
        }
      echo  json_encode($output);
    }
    
    
    public function markAsInProgress($reqID, $flag = "") {
        if ($flag == "yes") {
            $url = base_url() . "account/dashboard";
        } else {
            $url = base_url() . "account/request/project_info/" . $reqID;
        }
        $table =  $this->S_request_model->getDataBasedonSAAS($reqID);
        $uid = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        if ($reqID != 0 || $reqID == '') {
            $currentDateTime = date("Y-m-d H:i:s");
            $request_data = $this->S_request_model->get_request_by_id($reqID);
            $ismanual = $this->s_myfunctions->checkmanualorauto($request_data);
            $uid = $request_data[0]["customer_id"];
            if($ismanual == 1){
                $updt_reqdata = array("status" => "active", "status_admin" => "active", "status_designer" => "active", "status_qa" => "active", "priority" => 0, "sub_user_priority" => 0, "created" => $currentDateTime, "latest_update" => $currentDateTime, "modified" => $currentDateTime);
                $success = $this->Welcome_model->update_data($table, $updt_reqdata, array("id" => $reqID));
            }else{
                if($request_data[0]["status"] == "approved"){
                    $prerq_status = "disapprove_disapprove_disapprove_disapprove";
                    $isactive = $this->s_myfunctions->CheckActiveRequest($uid,"","","","","",'from_hold',"disapprove");
                }else{
                    $isactive = $this->s_myfunctions->CheckActiveRequest($uid);
                }
                if ($isactive == 1) {
                    $success = $this->s_myfunctions->move_project_inqueqe_to_inprogress($uid, $reqID,'markAsInProgress',$reqID);
                } else {
                    $priority = $this->Welcome_model->priority($uid,$table);
                    $sub_user_priority = $this->Welcome_model->sub_user_priority($request_data[0]['created_by'],$table);
                    $priorityupdate = $priority[0]['priority'] + 1;
                    $subpriorityupdate = $sub_user_priority[0]['sub_user_priority'] + 1;
                    $updt_reqdata = array("status" => "assign", "status_admin" => "assign", "status_designer" => "assign", "status_qa" => "assign", "priority" => $priorityupdate, "sub_user_priority" => $subpriorityupdate, "created" => $currentDateTime, "latest_update" => $currentDateTime, "modified" => $currentDateTime);
                    if($prerq_status != ""){
                        $updt_reqdata["previous_status"] = $prerq_status;
                    }
                    $success = $this->Welcome_model->update_data($table, $updt_reqdata, array("id" => $reqID));
                    //$this->s_myfunctions->capture_project_activity($reqID,'','',$request_data[0]['status'].'_to_assign','markAsInProgress',$login_user_id);
                    $this->s_myfunctions->update_priority_after_unhold_project($priorityupdate, '1', $reqID, $request_data[0]['customer_id']);
                    if ($subpriorityupdate > 1) {
                        $this->s_myfunctions->update_sub_userpriority_after_unhold_project($subpriorityupdate, '1', $reqID, $login_user_id);
                    }
                }
            }
            if ($success) {
                $this->session->set_flashdata('message_success', "Status successfully changed..", 5);
                redirect($url);
            } else {
                $this->session->set_flashdata('message_error', "Error while updating status.", 5);
                redirect($url);
            }
        }
    }
    
    public function cron_updatebillingcycle_forsubuser() {

        $todaydate = date("Y-m-d H:i:s");
        $offline_users = $this->S_request_model->getAgencyuserinfo("","",1);
        
        $subusers = $this->S_admin_model->getallsubuser_data($offline_users['user_id'],1);
        if (!empty($subusers)) {
            foreach ($subusers as $subusers_data) {
                $time = strtotime($subusers_data['billing_end_date']);
                if($subusers_data['plan_type'] == "monthly"){
                    $enddate = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", $time) . " +1 month"));
                }else{
                    $enddate = date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", $time) . " + 1 year"));
                }
                if ($subusers_data['billing_end_date'] != '' && strtotime($todaydate) >= $time) {

                    $data = array('billing_start_date' => $subusers_data['billing_end_date'],
                        'billing_end_date' => $enddate);
                    $this->Welcome_model->update_data("users", $data, array("id" => $subusers_data['id']));
                }
            }
        }
    
    }
     
    public function reactivate_subscription() {
         $planname = isset($_POST['parent_plan_name'])?$_POST['parent_plan_name']:'';
         $customerid = isset($_POST['customer_id'])?$_POST['customer_id']:'';
         $inprogress_req = isset($_POST['total_inprogress_req'])?$_POST['total_inprogress_req']:'';
         $user_state = isset($_POST['user_state'])?$_POST['user_state']:'';
         $flag = isset($_POST['reactivate_pro'])?$_POST['reactivate_pro']:'';
         $userid = $this->load->get_var('main_user');
         if(array_key_exists($user_state, STATE_TEXAS)){
                $state_tax = STATE_TEXAS[$user_state];
            }else{
                $state_tax = 0; 
            }
         $stripe_subscription = $this->Stripe->create_cutomer_subscribePlan($customerid, $planname, $inprogress_req,$state_tax);
         if($stripe_subscription['status'] == 1){
             $current_date = date('Y-m-d H:i:s',$stripe_subscription['data']['period_start']);
             $billing_expire = date('Y-m-d H:i:s',$stripe_subscription['data']['period_end']);
             $customer['current_plan'] = $stripe_subscription['data']['subscription_id'];
             $customer['billing_start_date'] = $current_date;
             $customer['billing_end_date'] = $billing_expire;
             $customer['is_cancel_subscription'] = 0;
             $success = $this->Welcome_model->update_data("users", $customer,array('customer_id' => $customerid));
             if ($success) {
                if($flag == 'yes'){
                 $this->s_myfunctions->movecancelprojecttopreviousstatus($userid);
                }
                $this->session->set_flashdata('message_success', "Sucessfully reactivate your plan..", 5);
                redirect(base_url() . "account/request/design_request");
                
         }
         
     }
     else{
         $this->session->set_flashdata('message_error', $stripe_subscription['message'], 5);
         redirect(base_url() . "account/request/design_request");
     }
    }
    
    public function cancelproject($id) {
        $user_id = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $login_user_data = $this->load->get_var('login_user_data'); 
        $table =  $this->S_request_model->getDataBasedonSAAS($id);
        $user_permsn = $this->S_request_model->getAgencyuserinfo($user_id);
        $ismanual = $user_permsn[0]['is_manual'];
        if($login_user_data[0]["role"] == "manager"){
            $u_id = $user_id;
        }else{
            $u_id = $login_user_id;
        }
        $customer = $this->S_admin_model->getuser_data($u_id);
        $requests = $this->S_request_model->get_request_by_id($id);
        $success = $this->Welcome_model->update_data($table, array("status" => "cancel", "status_designer" => "cancel", "status_admin" => "cancel", "status_qa" => "cancel","previous_status"=>"", "who_reject" => 0,"priority" => 0,"sub_user_priority" => 0, "latest_update" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $id));
        $ActiveReq = $this->s_myfunctions->CheckActiveRequest($u_id);
        if ($success) {
            $cancelpriority = isset($requests[0]['priority']) ? $requests[0]['priority'] : '';
            if ($cancelpriority != 0 && $ismanual != 1) {
                $cancelsubpriority = isset($requests[0]['sub_user_priority']) ? $requests[0]['sub_user_priority'] : '';
                $priorfrom = $cancelpriority + 1;
                $subpriorfrom = $cancelsubpriority + 1;
                $this->S_admin_model->update_priority_after_approved_qa(1, $priorfrom, $user_id,$table);
                $this->S_admin_model->update_subuserpriority_after_approved_qa(1, $user_id,$requests[0]['created_by'],$subpriorfrom,$table);
            }
            if($ActiveReq == 1 && $customer[0]['is_cancel_subscription'] != 1 && $ismanual != 1){
             $this->s_myfunctions->move_project_inqueqe_to_inprogress($user_id,"","cancelproject",$id);
            }
           // $this->s_myfunctions->capture_project_activity($id,'','',$requests[0]['status'].'_to_cancel','cancelproject',$login_user_id);
            $this->session->set_flashdata('message_success', "Your project cancelled successfully", 5);
            redirect(base_url() . "account/dashboard");
        }
    }

      
      /*******call different views for left & right message body********/

      public function Deletemsg($delreq_id = NULL){
          $table =  $this->S_request_model->getDataBasedonSAAS($delreq_id);
        if($table == 'requests'){
            $up_tb = 'request_discussions';
        }else{
           $up_tb = 's_request_discussions'; 
        }
          $dele_id = isset($_POST['delid'])?$_POST['delid']:'';
          $data['is_deleted'] = 1;
          $success = $this->Welcome_model->update_data($up_tb, $data, array("id" => $dele_id));
          if($success){
              echo json_encode($dele_id);
          }
      }
      public function Editmsg($req_id = NUll){
        $table =  $this->S_request_model->getDataBasedonSAAS($req_id);
        if($table == 'requests'){
            $up_tb = 'request_discussions';
        }else{
           $up_tb = 's_request_discussions'; 
        }
        $editid = isset($_POST['editid'])?$_POST['editid']:''; 
        $msg = isset($_POST['msg'])?$_POST['msg']:''; 
        $data['is_edited'] = 1;
        $data['message'] = $msg;
          $success = $this->Welcome_model->update_data($up_tb, $data, array("id" => $editid));
          if($success){
              echo json_encode($editid);
          }
        }
        
        public function isDisabledlink(){
            $reqid = isset($_POST['reqid'])?$_POST['reqid']:'';
            $draftid = isset($_POST['draftid'])?$_POST['draftid']:'';
            $table =  $this->S_request_model->getDataBasedonSAAS($reqid);
            if($table == 'requests'){
                $up_tb = 'link_sharing';
            }else{
               $up_tb = 's_link_sharing'; 
            }
            $is_disabled = isset($_POST['is_disabled'])?$_POST['is_disabled']:'';
            $login_user_id = $this->load->get_var('login_user_id'); 
            $data['is_disabled'] = $is_disabled;
            $success = $this->Welcome_model->update_data($up_tb, $data, array("request_id" => $reqid,"draft_id" => $draftid,"share_type" => "public"));
            if($success){
            if($is_disabled == 0){
                //$this->s_myfunctions->capture_project_activity($reqid,$draftid,'','enable_public_link','isDisabledlink',$login_user_id);
              }else{
               // $this->s_myfunctions->capture_project_activity($reqid,$draftid,'','disable_public_link','isDisabledlink',$login_user_id);  
              }
              echo json_encode($reqid);
          }
        }
        
        public function draftsharelinkpermissions(){
            $reqid = isset($_POST['reqid'])?$_POST['reqid']:'';
            $draft_id = isset($_POST['draft_id'])?$_POST['draft_id']:'';
          //  echo "<pre>";print_r($_POST);
//            $success = $this->Welcome_model->update_data("link_sharing", $data, array("request_id" => $reqid,"share_type" => "public"));
//            if($success){
//              echo json_encode($reqid);
//          }
        }
        public function download_projectfile($id,$filename="") {
             $filename = $_GET['imgpath'];
            $file_name1 = FS_PATH_PUBLIC_UPLOADS_REQUESTS.$id."/".$filename;
            //$file_name1 = str_replace(" ", "_", $file_name1);
            $this->load->helper('download');
            $path = file_get_contents($file_name1); // get file name
            $name = $filename; // new name for your file
            force_download($name, $path);
            
            redirect(base_url() . "account/request/project_info/" . $id);
        }
        
//        public function mergedotcomments() {
//        $box_width = 925;
//        $offset_value = 6;
//        // get all the design drafts files for active projects (pending_approval, revision, in progress)
//        // exclude files uploaded by customer
//        // foreach loop of all the images 
//        // update the final values in the DB ($xco_new, $yco_new)
//        $allfiles = $this->S_request_model->getallfilefordotcomment();
//        $i = 0;
//        foreach ($allfiles as $allfiles_d) {
//            $currentxco_yco = $this->S_request_model->getxco_ycofordotcomment($allfiles_d['id']);
//            if (!empty($currentxco_yco)) {
//                if ($allfiles_d['actual_image_width'] == NULL || $allfiles_d['actual_image_height'] == NULL) {
//                    list($img_width, $img_height) = getimagesize("https://s3.us-east-2.amazonaws.com/graphics-zoo-ohio/public/uploads/requests/" . $allfiles_d['request_id'] . "/" . $allfiles_d['file_name']);
//                
//                $data1 = array("actual_image_width" => $img_width,
//                        "actual_image_height" => $img_height);
//                $this->Welcome_model->update_data("request_files", $data1, array("id" => $allfiles_d['id']));
//                }else{
//                    $img_width = $allfiles_d['actual_image_width'];
//                    $img_height = $allfiles_d['actual_image_height'];
//                }
//                foreach ($currentxco_yco as $getcurrentxco_yco) {
//                    // these values will be from database
//                    $xco_current = $getcurrentxco_yco['xco'] + $offset_value;
//                    $yco_current = $getcurrentxco_yco['yco'] + $offset_value;
//
//                    // calculate aspect ratio
//                    // Get the image scale.
//                    $box_scale = $img_width / $img_height;
//                    if ($img_width < $box_width) {
//                        // need to check this case if existing image is less than 925 width
//                        continue;
//                    }
//                    $box_height = $box_width / $box_scale;
//                    $width_scale = ($xco_current / $box_width) * 100;
//                    $height_scale = ($yco_current / $box_height) * 100;
//
//                    $xco_new = ($img_width / 100) * $width_scale;
//                    $yco_new = ($img_height / 100) * $height_scale;
//                    $data = array("xco_new" => $xco_new,
//                        "yco_new" => $yco_new,
//                        "is_updatecomment_pos" => 1);
//                    if($getcurrentxco_yco['is_updatecomment_pos'] != 1){
//                    $this->Welcome_model->update_data("request_file_chat", $data, array("id" => $getcurrentxco_yco['id']));
//                    }
//                    echo $i++;
//                    }
//               
//            }
//        }
//    }
        
//    public function moveallapprovedfiles() {
//        $approvedfiles = $this->S_request_model->getapprovedfiles();
//       // echo "<pre>";print_r($approvedfiles);exit;
//        foreach ($approvedfiles as $file) {
//        $srcpath = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS.$file['request_id'].'/';
//        $destpath = FS_UPLOAD_PUBLIC_UPLOADS.'approved_files/';
//        $res = $this->s3_upload->copyexistfiles($srcpath,$destpath,$file['file_name']);
//        if($res['status'] == 1){
//            echo $file['request_id']."/".$file['file_name']." - success<br>";
//        }else{
//            echo $file['request_id']."/".$file['file_name']." - error<br>"; 
//        }
//        }
//    }
 
    public function closeSitetour(){
        $login_user_id = $this->load->get_var('login_user_id');
        $success = $this->Welcome_model->update_data("users", array('is_logged_in' => 1), array('id' => $login_user_id));
        echo json_encode($success);
    }
    
    public function is_show_file_sharing() {
        $users_data = $this->load->get_var('parent_user_data');
        $login_user_data = $this->load->get_var('login_user_data');
//        echo "<pre/>";print_R($login_user_data);exit;
        if($login_user_data[0]['role'] == 'customer' && $login_user_data[0]['is_saas'] == 1){
            $return = 1;
        }
//        if ($users_data[0]['overwrite'] == 1) {
//            if ($users_data[0]['file_sharing'] == 1) {
//                $return = 1;
//            } else {
//                $return = 0;
//            }
//        } else {
//            $user_plan_data = $this->load->get_var('parent_user_plan');
//            if ($user_plan_data[0]['file_sharing'] == 1) {
//                $return = 1;
//            } else {
//                $return = 0;
//            }
//        }
        if ($login_user_data[0]['parent_id'] != 0 && $login_user_data[0]['role'] == 'customer') {
            if ($login_user_data[0]['overwrite'] == 1) {
                if ($login_user_data[0]['file_sharing'] == 1) {
                    $return = 1;
                } else {
                    $return = 0;
                }
            } else {
                $loginuser_pln = $this->Clients_model->getsubsbaseduserbyplanid($login_user_data[0]['plan_name'],"",$users_data[0]['id']);
                if ($loginuser_pln[0]['file_sharing'] == 1) {
                    $return = 1;
                } else {
                    $return = 0;
                }
            }
        }
        return $return;
    }

    public function sharing_permision_ajaxCall(){
       $reqid = $this->input->post("request_id");
       $draftid = $this->input->post("draftid");
       $result["reqid"] = $reqid;
       $result["alldraftpubliclinks"] = $this->S_request_model->getallPubliclinkinfo($reqid,$draftid);
       $result["shareddraftUser"] =  $this->S_request_model->getSharedpeopleinfobyreq_draftID($reqid,$draftid); 
//       echo "<pre>"; print_r($result); die; 
       $HTml = $this->load->view("account/ShareLinkhtml",$result);
       
     
    }
    
    public function getCategoryquestions() {
        $output = array();
        $cat_id = $this->input->post('catID');
        $subcat_id = $this->input->post('subcat');
        $edit_id = $this->input->post('edit_id');
        $catid = (isset($cat_id) && $cat_id != '') ? $cat_id : '';
        $subcatid = (isset($subcat_id) || $subcat_id != '') ? $subcat_id : 0;
        $editid = (isset($edit_id) || $edit_id != '') ? $edit_id : '';
        if ($editid) {
            $all_quest = $this->S_request_model->getallquestions($catid, $subcatid);
//            echo "<pre/>";print_R($all_quest);

            /*             * *****get questions******* */
            $getrec = array();
            foreach ($all_quest as $qk => $qv) {
                $getrec = $this->S_category_model->get_quest_Ans($editid, $qv['id']);
                $all_quest[$qk]['answer'] = $getrec[0]['answer'];
//               echo "<pre/>";print_R($getrec);
            }
//            echo "<pre/>";print_R($all_quest);
            /*             * *****get answers******* */
        } else {
            $all_quest = $this->S_request_model->getallquestions($catid, $subcatid);
        }
//        echo "<pre/>";print_R($all_quest);
        if (!empty($all_quest)) {
            $output['status'] = 'success';
            $output['data'] = $all_quest;
        } else {
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }
    
    
    /*******samples********/
    public function getSamples(){
        $output = array();
        $cat_id = $this->input->post('catID');
        $subcat_id = $this->input->post('subcat');
        $edit_id = $this->input->post('edit_id');
        $catid = (isset($cat_id) && $cat_id != '') ? $cat_id : '';
        $subcatid = (isset($subcat_id) || $subcat_id != '') ? $subcat_id : 0;
        $editid = (isset($edit_id) || $edit_id != '') ? $edit_id : '';
        $all_samples = $this->S_category_model->getSampleMaterialsbycat_subcat($catid, $subcatid);
        if (!empty($all_samples)) {
            $output['status'] = 'success';
            $output['data'] = $all_samples;
        } else {
            $output['status'] = 'error';
        }
        echo json_encode($output);
    }


    /* pixabay &unsplash api code start here */
    
    public function get_all_imagesPost() {
        $res = array();
        $res2 = array();
        $API_KEY_pixabay = '14096718-167c5c7a1912fadb24a99bd94';
       // live $API_KEY_unsplash = 'a62312dc9af687253e0724d98ba1b3d64850674a230413619f6ef1e2a9581ecd';
        // $API_KEY_pixabay = '13639244-b7e97276b9fe1886d2c5f3e8d';
        $API_KEY_unsplash = 'd13cd5fc39d464bd270f1eb92354ea7b83a482279f8df76c7330da632618a686';
        $search_keyword = $this->input->post("keyword");
        $filter = $this->input->post("filter");
        $filter_unsplash = $this->input->post("filter_un");
        $load_more = $this->input->post("load_more");
        $URL_pixabay = "https://pixabay.com/api/?key=" . $API_KEY_pixabay . "&q=" . urlencode($search_keyword) . "&per_page=25&page=" . $load_more . "&orientation=" . $filter;
        $URL_unsplash = "https://api.unsplash.com/search/photos?client_id=" . $API_KEY_unsplash . "&query=" . urlencode($search_keyword) . "&per_page=25&page=" .
                $load_more . "&orientation=" . $filter_unsplash;
        $res[] = $this->getDatafromCurl($URL_pixabay);
        $res[] = $this->getDatafromCurl($URL_unsplash);
        $newRes = json_encode($res);
        echo $newRes;
        exit;
    }

    public function getDatafromCurl($url) {
        $ch = curl_init();
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);
        // Execute
        $result = curl_exec($ch);
        // Closing
        curl_close($ch);
        // Will dump a beauty json :3
        return json_decode($result);
    }

    public function download_images() {
        $req_fromPage = $this->input->post("req_fromPage");
        $image_url = $this->input->post("file-upload");
        $this->load->helper('download');
        $path = array();
        $userName =  $_SESSION["first_name"].'_'.$_SESSION["last_name"];
        if(sizeof($image_url)>1){
        foreach ($image_url as $k => $images) {
            $imgN = $this->addHttps($images);
            $this->zip->read_file($imgN);        
          }
         $this->zip->download($userName.'_photos.zip'); 
         }else{
            $imgN = $this->addHttps($image_url[0]);
            // echo $imgN; exit;  
            $baseName = basename($imgN);
            $url = file_get_contents($imgN);
            force_download($baseName,$url);
         }   
      }

    public function download_remote_server(){
//          echo "<pre>"; print_r($_POST); die;
        $req_fromPage =  $this->input->post("req_fromPage");
        $image_url =  $this->input->post("file-upload");
        $page_url = $this->input->post("pageurl");
        $image_id =  $this->input->post("ids");
        $search_keyword =  $this->input->post("search_keyword");
        $userName =  $_SESSION["first_name"].$_SESSION["last_name"];
        $realPath = dirname(dirname(dirname(dirname(__FILE__))));  
        $nextPAth ="/public/uploads/temp/";
        $dateFolder = strtotime(date('Y-m-d H:i:s'));
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output["img_link"] = array();
        $output['error'] = 'Error while uploading the files. Please try again.';
        $dateFolder = strtotime(date('Y-m-d H:i:s'));

        // echo "<pre>"; print_r($page_url); die; 
        if (!isset($_SESSION['temp_folder_names']) && $_SESSION['temp_folder_names'] != '') {
            // echo "ifmake"; die; 
            if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                $_SESSION['temp_folder_names'] = './public/uploads/temp/' . $dateFolder;
            } else {
                $_SESSION['temp_folder_names'] = '';
            }
        } else {
            // echo "else"; die; 
            if (!is_dir($_SESSION['temp_folder_names'])) {
                if (@mkdir('./public/uploads/temp/' . $dateFolder, 0777, TRUE)) {
                    $_SESSION['temp_folder_names'] = './public/uploads/temp/' . $dateFolder;
                    
                } else {
                    $_SESSION['temp_folder_names'] = '';
                }
            }
        }
        $landingPath = $_SESSION['temp_folder_names'] . "/";
        // echo $landingPath; die; 
        $filename = array();

        foreach ($image_url as $ky => $img) {
            $imgN = $this->addHttps($img);
            $page_urlN = $this->addHttps($page_url[$ky]);
            $count =  $image_id[$ky];
            $ext = $this->get_extension($img);
             
            if (is_numeric($count)) {
                $nameEx = "px_";
            } else {
                $nameEx = "un_";
            }
            $save_name = $nameEx.$count.".".$ext;
            $link_array = explode('/',$_SESSION['temp_folder_names']);
            $folderCr = end($link_array);
             
           
            if(file_exists($landingPath.$save_name) != 1){
                file_put_contents($landingPath.$save_name,file_get_contents($imgN));  
                $file_size[$ky] = filesize($landingPath.$save_name);      
                $output['files'][$ky] = $save_name; 
                $output["file_size"] = $file_size;
                $output["img_link"][$ky] = $page_urlN;
                $output['status'] = true;
                $output['error'] = false;
                $output['path']="";
            } else {
                $file_size[$ky] = "";
                $output['files'][$ky] = "";
                $output["file_size"] = "";
                $output["img_link"][$ky] = "";
                $output['status'] = false;
                $output['filename'][$ky] = $save_name;  
                $output['path'][$ky] = base_url()."public/uploads/temp/".$folderCr."/"; 
            }
        }
           $output['error'] = $output['filename'];
           $output['error'] = $output['path']; 
           
        //redirect(base_url().'/account/request/'.$req_fromPage);
        echo json_encode($output);
    }

     /* add https to url */
    public function addHttps($url) {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) { 
            // If not exist then add http 
            $url = "https://" . $url; 
        } 

        return $url; 
    }

    public function get_extension($file) {
        if (strpos($file, 'fm') !== false) {
            preg_match_all('#&fm=([^s]+)&#', $file, $matches);
            $findout = implode(' ', $matches[1]);
            $extension = substr($findout, 0, strpos($findout, "&"));
            return $extension ? $extension : false;
        } else {
            $extension = end(explode(".", $file));
            return $extension ? $extension : false;
        }
    }

    public function mark_as_favorite()
        {
           $reqType =  $this->input->post('mark');
           $projectid = $this->input->post('projectID');
           $folder_id = $this->input->post('folder_id');
           $id = $projectid;
           $req_id = $this->input->post('user_id');
           $brand_id = $req_id;
           $filename = $this->input->post('filename');
           $role = $this->input->post('role');
           
           if($projectid != 0 && $req_id != 0 && $role != "brand"){
            $condiARR = array(
                'id' => $projectid,
                'request_id' => $req_id
            );
            $table = "request_files";
           }else if($role == "brand"){
               $condiARR = array(
                 'brand_id' =>  $id,
                 'filename' => $filename
                );
                $table = "brand_profile_material";
           }else{
               $condiARR = array(
                 'folder_id' =>  $folder_id,
                   'file_name' => $filename
                );
                $table = "folder_file_structure";
            }
           if($reqType =='unfav'){
             $data = array('mark_as_fav' => 0); 
           }else{
             $data = array('mark_as_fav' => 1); 
           }
            $this->db->where($condiARR);
            $this->db->update($table,$data);
             //echo $this->db->last_query(); die;
            $afftectedRows = $this->db->affected_rows();
            echo $afftectedRows;    
    }

    public function open_file_type() {
        $product_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        $imgurl = isset($_POST['url']) ? $_POST['url'] : '';
        if ($imgurl == "") {
            $url = FS_PATH_PUBLIC_UPLOADS_REQUESTS . $product_id . '/' . $file;
        } else {
            $url = $imgurl;
        }
        $file = isset($_POST['file']) ? $_POST['file'] : basename($url);
        $ext = strtolower($this->get_extension($file));
//        echo "url".$url."<br/>";
//        echo $ext;exit;
        $docArrOffice = array('odt', 'ods', 'xlsx', 'xls', 'txt');
        $docArrgoogleoffcie = array("docx",'doc','ai', 'pdf', 'ppt', 'pptx', 'pps', 'ppsx', 'tiff', 'xxx', 'eps');
        $font_file = array('ttf', 'otf', 'woff', 'eot', 'svg');
        $vedioArr = array('mp4', 'Mov');
        $audioArr = array('mp3');
        $ImageArr = array('jpg', 'png', 'gif', 'jpeg', 'bmp');

        if (in_array($ext, $docArrOffice)) {
            echo '<iframe src="https://view.officeapps.live.com/op/view.aspx?src=' . $url . '" style="width:600px; height:500px;" frameborder="0"></iframe>';
        } elseif (in_array($ext, $docArrgoogleoffcie)) {
            echo '<iframe src="https://docs.google.com/viewer?url=' . $url . '&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>';
        } else if (in_array($ext, $vedioArr)) {

            echo '<video width="400" controls>
                          <source src="' . $url . '" type="video/' . $ext . '">
                          Your browser does not support HTML5 video.
                      </video>';
        } else if (in_array($ext, $audioArr)) {
            echo '<audio controls>
                              <source src="' . $url . '" type="audio/' . $ext . '">        
                            Your browser does not support the audio element.
                          </audio>';
        } elseif ($ext == 'zip') {
            echo "0";
        } elseif ($ext == 'psd') {

            echo "0";
            /* echo '<iframe src="https://www.photopea.com/#'.base64_encode($json_code).'" style="width:600px; height:500px;" frameborder="0"></iframe>'; */
        } elseif (in_array($ext, $font_file)) {
            $data['extension'] = $ext;
            $data['url'] = $url;
            $data['font_name'] = $file;
            $this->load->view('account/font_family', $data);
        } elseif (in_array($ext, $ImageArr)) {
            echo "<img src='". $url ."'>";
        }
    }
    /* pixabay &unsplash api code Ends here  */
    
    public function get_designfeedback() {
        $id = isset($_POST["id"])?$_POST["id"]:"";
        $role = isset($_POST["role"])?$_POST["role"]:"";
        $output = array();
        if($id != ""){
            if($role == "admin"){
                $output["customer"] = $this->S_request_model->get_reviewfeedback($id,"customer");
                $output["admin"] = $this->S_request_model->get_reviewfeedback($id,"admin");
            }else{
                $output = $this->S_request_model->get_reviewfeedback($id,"customer");
            }
            echo json_encode($output);exit;
        }
        
    }
    
    /********download file from main chat******/
     public function download_stuffFromChat() {
        
        $req_fromPage = $this->input->post("req_fromPage");
        //$image_url = $this->input->post("file-upload");
        $image_url = $this->input->get("file-upload");
        //$ext = $this->input->post("ext");
        $ext = $this->input->get("ext");

      $docArrOffice = array('doc', 'docx', 'odt', 'ods', 'xlsx', 'xls', 'txt','ai', 'pdf', 'ppt', 'pptx', 'pps', 'ppsx', 'tiff', 'xxx', 'eps');
        $font_file = array('ttf', 'otf', 'woff', 'eot', 'svg');
        $vedioArr = array('mp4', 'Mov');
        $audioArr = array('mp3');
        $ImageArr = array('jpg', 'png', 'gif', 'jpeg', 'bmp');
        $zipArr = array('zip', 'rar');
 
    $arrContextOptions = array("ssl"=>array("verify_peer"=>false,"verify_peer_name"=>false));  
    
    
  //  ini_set('display_errors', 1);
     if (in_array($ext, $docArrOffice)) { 
        // echo  $image_url."<br>"; 
                 $this->load->helper('download');
                $baseName = basename($image_url);
                $url = file_get_contents($image_url,false, stream_context_create($arrContextOptions));
                // echo $url; exit; 
                 force_download($baseName,$url);
              
            }elseif (in_array($ext,$font_file)){
                 $this->load->helper('download');
                $baseName = basename($image_url);
                $url = file_get_contents($image_url,false, stream_context_create($arrContextOptions));
                force_download($baseName,$url);
            }elseif (in_array($ext,$zipArr)){
                
                 $this->zip->read_file($image_url);      
                 $baseName = basename($image_url); 
                 $this->zip->download($baseName);  
            }elseif (in_array($ext,$ImageArr)){
                $this->load->helper('download');
                $baseName = basename($image_url);
                $url = file_get_contents($image_url,false, stream_context_create($arrContextOptions) );
                force_download($baseName,$url);
                 
            }

        }
        
     public function get_statustab_acc_user() {
         $login_user_data = $this->load->get_var('login_user_data'); 
         $status = array();
         if(($login_user_data[0]["role"] == "customer" && $login_user_data[0]["is_saas"] == 1)){
             $status["active"] = array('active', 'disapprove');
             $status["by"] = "status_admin";
             $status["user"] = "s_user";
             $status["role"] = "admin";
             $status["seen_by"] = "admin_seen";
         }else if($login_user_data[0]["role"] == "manager"){
             $status["active"] = array('active', 'disapprove');
             $status["by"] = "status_admin";
             $status["user"] = "s_user";
             $status["role"] = "manager";
              $status["seen_by"] = "admin_seen";
         }else if($login_user_data[0]["role"] == "designer"){
             $status["active"] = array('active','disapprove');
             $status["by"] = "status_designer";
             $status["user"] = "designer";
             $status["role"] = "designer";
             $status["seen_by"] = "designer_seen";
         }else{
             $status["active"] = array("active","disapprove","assign","pending","checkforapprove");
             $status["by"] = "status";
             $status["user"] = "customer";
             $status["seen_by"] = "customer_seen";
             $status["role"] = "customer";
         }
         return $status;
     }
     
    public function request_verified_by_admin() {
       // $this->s_myfunctions->checkloginuser("admin");
        $data = array();
        $ischecked = $_POST['ischecked'];
        $requestId = $_POST['data_id'];
        $data['is_verified_by_admin'] = date("Y:m:d H:i:s");
        $data['verified'] = $ischecked;
        $table =  $this->S_request_model->getDataBasedonSAAS($requestId);
        //if($ischecked){
        $success = $this->Welcome_model->update_data($table, $data, array("id" => $requestId));
        if ($success) {
            echo json_encode($data);
        }
    }
    
    public function adminCheckClassforDesignerAssign($reqstatus) {
            if ($reqstatus == 'active' || $reqstatus == 'disapprove'){
                $addClass = 'in_active';
            }elseif($reqstatus == 'assign'){
                 $addClass = 'in_queue';
            }elseif($reqstatus == 'pendingrevision'){
                $addClass = 'pending_rev';
            }elseif($reqstatus == 'checkforapprove'){
                $addClass = 'pending_app';
            }elseif($reqstatus == 'approved'){
                $addClass = 'completed';
            }elseif($reqstatus == 'hold'){
                $addClass = 'hold';
            }elseif($reqstatus == 'cancel'){
                $addClass = 'cancel';
            }
            return $addClass;
        }
        
    public function assign_designer_ajax() {
        //$this->s_myfunctions->checkloginuser("admin");
//        $login_user_id = $this->load->get_var('login_user_id');
        $reqIDs = explode(',', $_POST['request_id']);
        $assign_designer = $this->input->post("assign_designer");
        $output = array();
        if ((isset($_POST['request_id']) && $_POST['request_id'] != '')) {
           $this->S_request_model->getsaasrequests($reqIDs);
            $succ = $this->S_admin_model->assign_designer_by_admin($assign_designer, $reqIDs);
            if ($succ) {
//                foreach ($requests_data as $request) {
//
//                    $assign_id = $this->input->post('assign_designer');
//                    $checkDraft_id = $this->Welcome_model->select_data("draft_id,designer_id", "activity_timeline", "request_id='" . $request['id'] . "' AND slug ='assign_designer' AND action_perform_by ='" . $login_user_id . "'", $limit = "", $start = "", "desc", "draft_id");
//                    $checkDraft_idForReas = $this->Welcome_model->select_data("draft_id,designer_id", "activity_timeline", "request_id='" . $request['id'] . "' AND slug ='reassign_designer' AND action_perform_by ='" . $login_user_id . "' ", $limit = "", $start = "", "desc", "id");
//
//                    if (!empty($checkDraft_id)) {
//                        if (empty($checkDraft_idForReas) && $checkDraft_id[0]['draft_id'] == 0 && $checkDraft_id[0]['designer_id'] != 0) {
//                            $new_assign_id = $assign_id;
//                            $assign_id = $checkDraft_id[0]['designer_id'];
//                            $slug = 'reassign_designer';
//                        } else if ($checkDraft_idForReas[0]['draft_id'] != 0 && $checkDraft_idForReas[0]['designer_id'] != 0) {
//                            $new_assign_id = $assign_id;
//                            $assign_id = $checkDraft_idForReas[0]['draft_id'];
//                            $slug = 'reassign_designer';
//                        }
//                    } else {
//                        $new_assign_id = 0;
//                        $assign_id = $assign_id;
//                        $slug = 'assign_designer';
//                    }
//                    $this->s_myfunctions->capture_project_activity($request['id'], $new_assign_id, $assign_id, $slug, 'assign_designer_ajax', $login_user_id);
//                }
                $output['status'] = "success";
                $output['msg'] = "designer updated successfully...!";
            } else {
                $output['status'] = "error";
                $output['msg'] = "Please select atleat one request for assign designer..!";
            }
        }
        echo json_encode($output);
        exit;
    }
    
    
    public function assignDesignerToRequest(){
//        echo "<pre/>";print_R($_POST);exit;
        $reqIDs = (isset($_POST['request_id']) && $_POST['request_id'] != '') ? $_POST['request_id'] : '';
        $assign_designer = $this->input->post("assign_designer");
        $output = array();
        if ((isset($_POST['request_id']) && $_POST['request_id'] != '') && $_POST['assign_designer'] != '') {
           $this->S_request_model->getsaasrequests($reqIDs);
            $succ = $this->S_admin_model->assign_designer_by_admin($assign_designer, $reqIDs);
            if ($succ) {
                $output['status'] = "success";
                $output['msg'] = "designer updated successfully...!";
            } else {
                $output['status'] = "error";
                $output['msg'] = "Please select atleat one request for assign designer..!";
            }
            if($output['status'] == 'success'){
                $this->session->set_flashdata('message_success', "Designer updated successfully!", 5);
                redirect(base_url() . "account/request/project-info/".$reqIDs);
            }else{
                $this->session->set_flashdata('message_error', "Designer not updated successfully!", 5);
            redirect(base_url() . "account/request/project-info/".$reqIDs);
            }
        }else{
           $this->session->set_flashdata('message_error', "Please choose designer!", 5);
            redirect(base_url() . "account/request/project-info/".$reqIDs); 
        }
    }
    
    
     public function change_project_status() {
            $login_user_id = $this->load->get_var('login_user_id');
            $request = $this->S_request_model->get_request_by_id($_POST['request_id']);
            $count_quality_rev = $request[0]['count_quality_revision'];
            $subcat_data = $this->S_category_model->get_category_byID($request[0]['subcategory_id']);
            $customer_data = $this->S_admin_model->getuser_data($request[0]['customer_id']);
            $getlatestfiledraft = $this->S_request_model->get_latestdraft_file($_POST['request_id']);
            $table =  $this->S_request_model->getDataBasedonSAAS($_POST['request_id']);
            $ismanual = $this->s_myfunctions->checkmanualorauto($request);
//            echo "<pre/>";print_R($request);exit;
//            if ($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])) {
//                $turn_around_days = $customer_data[0]['turn_around_days'];
//            } else {
//                $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
//            }
            $expected = $this->s_myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), '', $request[0]['category_bucket'], $subcat_data[0]['timeline']);
            if ($request[0]['status'] == 'assign') {
                if (isset($_POST['value'])) {
                    $deletedpriority = isset($request[0]['priority']) ? $request[0]['priority'] : '';
                    $user_id = isset($request[0]['customer_id']) ? $request[0]['customer_id'] : '';
                    $priorfrom = $deletedpriority + 1;
                    if ($request[0]['latest_update'] == '' || $request[0]['latest_update'] == null) {
                        $success_add = $this->S_admin_model->update_data($table, array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0,"sub_user_priority" => 0, "who_reject" => 1, "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    } else {
                        $success_add = $this->S_admin_model->update_data($table, array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0,"sub_user_priority" => 0, "who_reject" => 1, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    }
                    if($ismanual != 1){
                        $this->S_admin_model->update_priority_after_approved_qa(1, $priorfrom, $user_id,$table);
                    }
                }
            }

            if ($_POST['value'] == "assign") {
                $priority = $this->Welcome_model->priority($request[0]['customer_id'],$table);
                $data2['priority'] = $priority[0]['priority'] + 1;
                if($request[0]['created_by'] != $request[0]['customer_id'] || $request[0]['created_by'] != 0){
                    $sub_user_priority = $this->Welcome_model->sub_user_priority($request[0]['customer_id'],$table);
                    $data2['sub_user_priority'] = $sub_user_priority[0]['sub_user_priority'] + 1;
                }else{
                    $data2['sub_user_priority'] = 0;
                }
                $success_add = $this->S_admin_model->update_data($table, array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => $data2['priority'],"sub_user_priority" => $data2['sub_user_priority'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                   //$this->s_myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    echo "1";
                } else {
                    echo "0";
                }
            } elseif ($_POST['value'] == "active") {
                $success_add = $this->S_admin_model->update_data($table, array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0,"sub_user_priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if($ismanual != 1){
                    $ActiveReq = $this->s_myfunctions->CheckActiveRequest($request[0]['customer_id']);
                    if ($ActiveReq == 1) {
                        $this->s_myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','change_project_status',$_POST['request_id']);
                    }
                    $this->s_myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    $message_count = $this->S_request_model->request_discussions_count_for_qa_message($_POST['request_id']);
                    if ($message_count == 0) {
                        $this->s_myfunctions->SendQAmessagetoInprogressReq($_POST['request_id'], $customer_data[0]['qa_id'], $request[0]['customer_id']);
                    }
                }
                if ($success_add) {
                    echo "1";
                } else {
                    echo "0";
                }
            } elseif ($_POST['value'] == "disapprove") {
                $count_rev = $count_quality_rev + 1;
                $success_add = $this->S_admin_model->update_data($table, array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0, "sub_user_priority" => 0,"who_reject" => 1,"count_quality_revision" => $count_rev, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                    //$this->s_myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    //$this->s_myfunctions->show_notifications($_POST['request_id'], $login_user_id, "\"" . $request[0]['title'] . "\" Project goes into revision  from " . $request[0]['status'] . "..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
                    //$this->s_myfunctions->show_notifications($_POST['request_id'], $login_user_id, "\"" . $request[0]['title'] . "\" Project goes into revision  from " . $request[0]['status_qa'] . "..!", "qa/dashboard/view_project/" . $_POST['request_id'], $customer_data[0]['qa_id']);
                    //$this->s_myfunctions->show_notifications($_POST['request_id'], $login_user_id, "\"" . $request[0]['title'] . "\" Project goes into revision  from " . $request[0]['status_designer'] . "..!", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
                   echo "1";
                } else {
                    echo "0";
                }
            } elseif ($_POST['value'] == "checkforapprove") {
                $success_add = $this->S_admin_model->update_data($table, array("approvaldate" => date("Y:m:d H:i:s"), 'status' => $_POST['value'], 'latest_update' => date("Y-m-d H:i:s"), 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0,"sub_user_priority" => 0, "who_reject" => 1,), array("id" => $_POST['request_id']));
                if ($success_add) {
                    if($ismanual != 1){
                        $ActiveReq = $this->s_myfunctions->CheckActiveRequest($request[0]['customer_id']);
                         if ($ActiveReq == 1) {
                             $this->s_myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','change_project_status',$_POST['request_id']);
                         }
                    }
                    //$this->myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    //$this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Upload New File For \"" . $request[0]['title'] . "\" . Please Check And Approve..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
                    //$this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Approved your design For \"" . $request[0]['title'] . "\"", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
                    echo "1";
                } else {
                    echo "0";
                }
            } elseif ($_POST['value'] == "approved") {
                $success_add = $this->S_admin_model->update_data($table, array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0,"sub_user_priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "approvaldate" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                    //$this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Approved..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
                    //$this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Approved..!", "qa/dashboard/view_project/" . $_POST['request_id'], $customer_data[0]['qa_id']);
                    //$this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Approved..!", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
                    if($ismanual != 1){
                        $ActiveReq = $this->s_myfunctions->CheckActiveRequest($request[0]['customer_id']);
                        if ($ActiveReq == 1) {
                            $this->s_myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','change_project_status',$_POST['request_id']);
                        }
                    }
                    //$this->s_myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    echo "1";
                } else {
                    echo "0";
                }
            }elseif ($_POST['value'] == "cancel") {
                $success_add = $this->S_admin_model->update_data($table, array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0,"sub_user_priority" => 0, "who_reject" => 1, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "approvaldate" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                    //$this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Cancelled..!", "customer/request/project_info/" . $_POST['request_id'], $customer_data[0]['id']);
                    //$this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Cancelled..!", "qa/dashboard/view_project/" . $_POST['request_id'], $customer_data[0]['qa_id']);
                    //$this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Project \"" . $request[0]['title'] . "\" has been Cancelled..!", "designer/request/project_info/" . $_POST['request_id'], $customer_data[0]['designer_id']);
                    if($ismanual != 1){
                        $ActiveReq = $this->s_myfunctions->CheckActiveRequest($request[0]['customer_id']);
                        if ($ActiveReq == 1) {
                            $this->s_myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','change_project_status',$_POST['request_id']);
                        }
                    }
                    //$this->s_myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    echo "1";
                } else {
                    echo "0";
                }
            } else {
                $success_add = $this->S_admin_model->update_data($table, array('status' => $_POST['value'], 'status_admin' => $_POST['value'], 'status_designer' => $_POST['value'], 'status_qa' => $_POST['value'], "priority" => 0,"sub_user_priority" => 0, "who_reject" => 1, "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                if ($success_add) {
                //$this->s_myfunctions->capture_project_activity($_POST['request_id'],$getlatestfiledraft[0]['id'],'',$request[0]['status_admin'].'_to_'.$_POST['value'],'admin_change_project_status',$login_user_id);
                    echo "1";
                } else {
                    echo "0";
                }
            }
        }
        
        
    public function hold_unhold_project_status() {
        $status_h = (isset($_POST['status']) && $_POST['status'] == 'true') ? "hold" : "unhold";
        $request_id = (isset($_POST['data_id'])) ? $_POST['data_id'] : '';
        $login_user_id = $this->load->get_var('login_user_id');
      //  $login_user_data = $this->S_admin_model->getuser_data($login_user_id);
        $request = $this->S_request_model->get_request_by_id_admin($request_id);
        $customer_data = $this->S_admin_model->getuser_data($request[0]['customer_id']);
     //   $pervious_status = array("active","disapprove","checkforapprove");
        if($customer_data[0]['is_saas'] == 1){
            $plan_name = $this->S_request_model->getsaasuserplan($request[0]['customer_id']);
            $getsubscriptionplan = $this->S_request_model->getsubscriptionlistbyplanid($plan_name[0]["plan_id"],"s_subscription_plan");
        }else{
            $getsubscriptionplan = $this->S_request_model->getsubscriptionlistbyplanid($customer_data[0]['plan_name']);
        }
        $ismanual = $this->s_myfunctions->checkmanualorauto($request);
        $table = $this->S_request_model->getDataBasedonSAAS($request_id);
//        if ($customer_data[0]['overwrite'] == 1 && isset($customer_data[0]['turn_around_days'])) {
//            $turn_around_days = $customer_data[0]['turn_around_days'];
//        } else {
//            $turn_around_days = $getsubscriptionplan[0]['turn_around_days'];
//        }
        
        if ($status_h == "hold") {
            
            $check_status = $request[0]['status'];
            $check_priority = $request[0]['priority'];
            $subcheck_priority = $request[0]['sub_user_priority'];
            $statuses = ($request[0]['status']."_".$request[0]['status_admin']."_".$request[0]['status_designer']."_".$request[0]['status_qa']);
            $success = $this->Welcome_model->update_data($table, array("previous_status" => $statuses, "status" => "hold", "status_admin" => "hold", "status_designer" => "hold", "status_qa" => "hold","priority" => 0,"sub_user_priority" => 0,"modified" => date("Y-m-d H:i:s")), array("id" => $request_id));
            if($ismanual != 1){
                if($check_status == "assign"){
                    $priorfrom = $check_priority + 1;
                    $this->S_admin_model->update_priority_after_approved_qa(1, $priorfrom, $request[0]['customer_id'],$table);
                    if($subcheck_priority > 0){
                        $subpriorfrom = $subcheck_priority + 1;
                        $this->S_admin_model->update_subuserpriority_after_approved_qa(1, $request[0]['customer_id'], $request[0]['created_by'],$subpriorfrom,$table);
                    }
                }
                $ActiveReq = $this->s_myfunctions->CheckActiveRequest($request[0]['customer_id']);
                if ($ActiveReq == 1) {
                    $this->s_myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','hold_unhold_project_status',$request_id);
                }
            }
           // $this->s_myfunctions->capture_project_activity($request_id,'',$request[0]['designer_id'],$request[0]['status'].'_to_hold','hold_unhold_project_status',$login_user_id);
        } else if ($status_h == "unhold") {
            $allpreviousstatuses = $this->s_customfunctions->allStatusafterUnhold($request[0]['previous_status']);       
            $ActiveReq = $this->s_myfunctions->CheckActiveRequest($request[0]['customer_id'],'','','','','','from_hold',$allpreviousstatuses['status']);
            $subusersdata = $this->S_request_model->getAllsubUsers($request[0]['customer_id'], "client", "active");
            if($ismanual != 1){
            if ((!empty($subusersdata) && $getsubscriptionplan[0]['is_agency'] == 1) || ($getsubscriptionplan[0]['is_agency'] == 1 && $customer_data[0]['parent_id'] == 0)) {
                if ($ActiveReq == 1) {
                    $project_user = ($request[0]['created_by'] == 0) ? $request[0]['customer_id'] : $request[0]['created_by'];
                    $pro_userdata = $this->S_admin_model->getuser_data($project_user);
                    $checkslot = $this->s_myfunctions->getslotofallusers($customer_data, $pro_userdata);
                    $checkslot['project_user'] = $project_user;
                    if ($pro_userdata[0]['user_flag'] == 'client') {
                        $logn_usr_data = $pro_userdata;
                    }else{
                        $logn_usr_data = $customer_data;
                    }
                    $subActiveReq = $this->s_myfunctions->checkactiveslotaccuser($request[0]['customer_id'],$checkslot,$logn_usr_data);
//                    if ($login_user_data[0]['requests_type'] == 'one_time') {
//                        $subActiveReq = $this->s_myfunctions->CheckActiveRequest($login_user_id, 'created', '', 'one_time','','','from_hold',$allpreviousstatuses['status']);
//                    } else {
//                        $subActiveReq = $this->s_myfunctions->CheckActiveRequest($login_user_id, 'created','','','','','from_hold',$allpreviousstatuses['status']);
//                    }
                    if ($subActiveReq == 1) {
                        $status = $allpreviousstatuses['status'];
                        $status_admin = $allpreviousstatuses['status_admin'];
                        $status_designer = $allpreviousstatuses['status_designer'];
                        $status_qa = $allpreviousstatuses['status_qa'];
                        $moveabletoactive = 1;
                    } else {
                        $status = "assign";
                        $status_admin = "assign";
                        $status_designer = "assign";
                        $status_qa = "assign";
                        $moveabletoactive = 0;
                        $priority = $this->Welcome_model->priority($request[0]['customer_id'],$table);
                        $priorityupdate = $priority[0]['priority'] + 1;
                        $sub_user_priority = $this->Welcome_model->sub_user_priority($login_user_id,$table);
                        $sub_user_priority = $sub_user_priority[0]['sub_user_priority'] + 1;
                    }
                } else {
                    $status = "assign";
                    $status_admin = "assign";
                    $status_designer = "assign";
                    $status_qa = "assign";
                    $moveabletoactive = 0;
                    $priority = $this->Welcome_model->priority($request[0]['customer_id'],$table);
                    $priorityupdate = $priority[0]['priority'] + 1;
                    $sub_userpriority = $this->Welcome_model->sub_user_priority($login_user_id,$table);
                    $sub_user_priority = $sub_userpriority[0]['sub_user_priority'] + 1;
                }
            } else {
                if ($ActiveReq == 1) {
                    $status = $allpreviousstatuses['status'];
                    $status_admin = $allpreviousstatuses['status_admin'];
                    $status_designer = $allpreviousstatuses['status_designer'];
                    $status_qa = $allpreviousstatuses['status_qa'];
                    $moveabletoactive = 1;
                } else {
                    $status = "assign";
                    $status_admin = "assign";
                    $status_designer = "assign";
                    $status_qa = "assign";
                    $moveabletoactive = 0;
                    $priority = $this->Welcome_model->priority($request[0]['customer_id'],$table);
                    $priorityupdate = $priority[0]['priority'] + 1;
                    $sub_user_priority = 0;
                }
            }
            }else{
                $status = $allpreviousstatuses['status'];
                $status_admin = $allpreviousstatuses['status_admin'];
                $status_designer = $allpreviousstatuses['status_designer'];
                $status_qa = $allpreviousstatuses['status_qa'];
                $moveabletoactive = 1;
            }
        if ($moveabletoactive == 1) {
            $subcat_data = $this->S_category_model->get_category_byID($request[0]['subcategory_id']);
            $expected = $this->s_myfunctions->getexpected_datefromtimezone(date("Y-m-d H:i:s"), "", $request[0]['category_bucket'], $subcat_data[0]['timeline']);
            $success = $this->Welcome_model->update_data($table, array("status" => $status, "status_admin" => $status_admin, "status_designer" => $status_designer, "status_qa" => $status_qa, "dateinprogress" => date("Y-m-d H:i:s"), "latest_update" => date("Y-m-d H:i:s"), "expected_date" => $expected, "priority" => 0, "sub_user_priority" => 0, "modified" => date("Y-m-d H:i:s")), array("id" => $request_id));
            //$this->s_myfunctions->capture_project_activity($request_id,'',$request[0]['designer_id'],'hold_to_'.$status,'hold_unhold_project_status',$login_user_id,'1');
        } else {
            $success = $this->Welcome_model->update_data($table, array("status" => $status, "status_admin" => $status_admin, "status_designer" => $status_designer, "status_qa" => $status_qa, "modified" => date("Y-m-d H:i:s"), "priority" => $priorityupdate, "sub_user_priority" => $sub_user_priority), array("id" => $request_id));
            $this->s_myfunctions->update_priority_after_unhold_project($priorityupdate, '1', $request_id, $request[0]['customer_id']);
         //   $this->s_myfunctions->capture_project_activity($request_id,'',$request[0]['designer_id'],'hold_to_'.$status,'hold_unhold_project_status',$login_user_id,'1');
            if ($sub_user_priority > 1) {
                $this->s_myfunctions->update_sub_userpriority_after_unhold_project($sub_user_priority, '1', $request_id, $login_user_id);
            }
        }
    }
    }
    
    public function uploadDraft($id){
        $table = $this->S_request_model->getDataBasedonSAAS($id);
        $login_user_data = $this->load->get_var('login_user_data'); 
        $request = $this->S_request_model->get_request_by_id_admin($id);
//        echo "<pre/>";print_R($request);exit;
        $ismanual = $this->s_myfunctions->checkmanualorauto($request);
        if($table == 'requests'){
            $tb = 'request_files';
        }else{
            $tb = 's_request_files'; 
        }
        if($login_user_data[0]['role'] == 'customer' && $login_user_data[0]['is_saas'] == 1){
          $seen_by = 'admin_seen';
          $req_file_status = 'customerreview';
        }elseif($login_user_data[0]['role'] == 'manager'){
          $seen_by = 'qa_seen';  
          $req_file_status = 'customerreview';
        }else{
          $seen_by = 'designer_seen'; 
          $req_file_status = 'pending';
        }
        if (!empty($_FILES)) {
//            echo "<pre/>";print_R($_FILES);exit;
            if ($_FILES['src_file']['name'] != "" && $_FILES['preview_file']['name'] != "") {
                $config['image_library'] = 'gd2';
                if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS_SAAS . $id)) {
                    $data = mkdir('./' . FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS_SAAS . $id, 0777, TRUE);
                }
                if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS_SAAS . $id . '/_thumb')) {
                    $data = mkdir('./' . FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS_SAAS . $id . '/_thumb', 0777, TRUE);
                }
                $this->load->library('image_lib', $config);
                $requestforfile = $this->S_request_model->get_request_by_id($id);
                $file_name_title = substr($requestforfile[0]['title'], 0, 15);
                $file_name_title = str_replace(" ", "_", $file_name_title);
                $file_name_title = preg_replace('/[^A-Za-z0-9\_]/', '', $file_name_title);
                $six_digit_random_number = mt_rand(100, 999);
                $ext = strtolower(pathinfo($_FILES['src_file']['name'], PATHINFO_EXTENSION));
                $ext1 = strtolower(pathinfo($_FILES['preview_file']['name'], PATHINFO_EXTENSION));
                $sourcename = $file_name_title . '_' . $six_digit_random_number . '.' . $ext;
                $previewname = $file_name_title . '_' . $six_digit_random_number . '1.' . $ext1;
                if (!empty($_FILES)) {
                    $config = array(
                        'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS_SAAS . $id . '/',
                        'allowed_types' => '*',
                        'file_name' => $sourcename,
                    );
                    $config2 = array(
                        'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS_SAAS . $id . '/',
                        'allowed_types' => array("jpg", "jpeg", "png", "gif"),
                        'max_size' => '5000000',
                        'file_name' => $previewname,
                    );

                    if (($_FILES['preview_file']['size'] >= $config2['max_size']) || ($_FILES['preview_file']['size'] == 0)) {
                        $this->session->set_flashdata('message_error', "File too large. File must be less than 5 MB.", 5);
                        redirect(base_url() . "account/request/project-info/" . $id);
                    }
                    if (in_array($ext1, $config2['allowed_types'])) {
                        $check_src_file = $this->s_myfunctions->CustomFileUpload($config, 'src_file');
                        $src_data = array();
                        $src_data[0]['file_name'] = "";
                        if ($check_src_file['status'] == 1) {
                            // echo $check_src_file['msg'];
                        } else {
                            //$data = array('error' => $this->upload->display_errors());
                            $this->session->set_flashdata('message_error', $check_src_file['msg'], 5);
                        }
                        //$config2['preview_file'] = $_FILES['preview_file']['name'];
                        $_FILES['src_file']['name'] = $file_name_title . '_' . $six_digit_random_number . '.' . $ext1;
                        $_FILES['src_file']['type'] = $_FILES['preview_file']['type'];
                        $_FILES['src_file']['tmp_name'] = $_FILES['preview_file']['tmp_name'];
                        $_FILES['src_file']['error'] = $_FILES['preview_file']['error'];
                        $_FILES['src_file']['size'] = $_FILES['preview_file']['size'];
                        $check_preview_file = $this->s_myfunctions->CustomFileUpload($config2, 'preview_file');
                        if ($check_preview_file['status'] == 1) {
                            //echo $check_src_file['msg'];exit;
                            $thumb_tmp_path = FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS_SAAS . $id . '/_thumb/';
                            $tmp = $_FILES['preview_file']['tmp_name'];
                            $thumb = $this->s_myfunctions->make_thumb($tmp, $thumb_tmp_path . $previewname, 600, $ext1);
                            if (UPLOAD_FILE_SERVER == 'bucket') {
                                $staticName = base_url() . $thumb_tmp_path . $previewname;
                                //echo $staticName;exit;
                                $config = array(
                                    'upload_path' => FS_UPLOAD_PUBLIC_UPLOADS_REQUESTS_SAAS . $id . '/_thumb/',
                                    'file_name' => $previewname
                                );
                                $this->load->library('s3_upload');
                                $this->s3_upload->initialize($config);
                                $uplod_thumb = $this->s3_upload->upload_multiple_file($staticName);
                                if ($uplod_thumb['status'] == 1) {
                                    $delete = $thumb_tmp_path . $previewname;
                                    unlink($delete);
                                    unlink('./tmp/tmpfile/' . basename($staticName));

                                    rmdir($thumb_tmp_path);
                                    rmdir($thumb_dummy_path);
                                }
                            }
                           
                            $request_files_data = array("request_id" => $id,
                                "user_id" => $_SESSION['user_id'],
                                "user_type" => "designer",
                                "file_name" => $previewname,
                                "preview" => 1,
                                 $seen_by => 1,
                                "status" => $req_file_status,
                                "created" => date("Y-m-d H:i:s"),
                                "modified" => date("Y-m-d H:i:s"),
                                "src_file" => $sourcename);
//                             echo "<pre/>";print_R($request_files_data);exit;
                            $data = $this->Welcome_model->insert_data($tb, $request_files_data);
                            //$subdomain_url = $this->s_myfunctions->dynamic_urlforsubuser_inemail($request[0]['customer_id'], $customer[0]['plan_name']);
                            if(($login_user_data[0]['role'] == 'customer' && $login_user_data[0]['is_saas'] == 1) || $login_user_data[0]['role'] == 'manager'){
                            $this->s_myfunctions->send_email_after_admin_approval($request[0]['title'], $request[0]['id'], $customer[0]['email'], $data);
                                $send_emailto_subusers = $this->s_myfunctions->send_emails_to_sub_user($request[0]['customer_id'], 'approve_revise', $is_approveDraftemailbyadmin, $request[0]['id']);
                                if (!empty($send_emailto_subusers)) {
                                    foreach ($send_emailto_subusers as $send_emailto_subuser) {
                                        $this->s_myfunctions->send_email_after_admin_approval($request[0]['title'], $request[0]['id'], $send_emailto_subuser['email'], $data, $subdomain_url);
                                    }
                                }
                            }
                            $this->Welcome_model->update_data($table, array("approvaldate" => date("Y:m:d H:i:s"), "status" => "checkforapprove", "status_qa" => "checkforapprove", "status_designer" => "checkforapprove", "status_admin" => "checkforapprove", "priority" => 0,"sub_user_priority" => 0, "modified" => date("Y-m-d H:i:s")), array("id" => $id));
                            if($ismanual != 1){
                                $ActiveReq = $this->s_myfunctions->CheckActiveRequest($request[0]['customer_id']);
                                if ($ActiveReq == 1) {
                                    $this->s_myfunctions->move_project_inqueqe_to_inprogress($request[0]['customer_id'],'','view_request',$id);
                                }
                            }
                             /**capture upload draft event**/
                            //$this->s_myfunctions->capture_project_activity($id,$data,$request[0]['designer_id'],'admin_upload_draft','view_request',$login_user_id,'0','1','1');
                            //$this->s_myfunctions->capture_project_activity($id,$data,$request[0]['designer_id'],$request[0]['status'].'_to_checkforapprove','view_request',$login_user_id,'1','1','0');
                            /* save notifications when upload draft design */

                            //$this->s_myfunctions->show_notifications($id, $login_user_id, "Upload New File For \"" . $request[0]['title'] . "\" . Please Check And Approve..!", "customer/request/project_info/" . $id, $request[0]['customer_id']);
                            //$this->s_myfunctions->show_notifications($id, $login_user_id, "Approved your design For \"" . $request[0]['title'] . "\"", "designer/request/project_info/" . $id, $request[0]['designer_id']);
                            /* end save notifications when upload draft design */
                            $this->session->set_flashdata('message_success', "File is Uploaded Successfully.!", 5);
                           redirect(base_url() . "account/request/project-info/" . $id);
                        } else {
                            $this->session->set_flashdata('message_error', $check_preview_file['msg'], 5);
                        }
                    } else {
                        $this->session->set_flashdata('message_error', "Please upload valid files", 5);
                        redirect(base_url() . "account/request/project-info/" . $id);
                    }
                }
            } else {
                    $this->session->set_flashdata('message_error', "Please Upload Preview or Source Both Files.!", 5);
                    redirect(base_url() . "account/request/project-info/" . $id);
            }
        }
        
    }
    
    public function uploadmainChatfiles($reqid){
//        die($reqid);
        $arr = array();
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output['error'] = 'Error while uploading the files. Please try again.';
        if (!(isset($_SESSION['chatreqtemp_folder_names'])) && ($_SESSION['chatreqtemp_folder_names'] == '')) {
            if (@mkdir('./public/uploads/chatreqtemp/' . $reqid, 0777, TRUE)) {
                $_SESSION['chatreqtemp_folder_names'] = './public/uploads/chatreqtemp/' . $reqid;
            } else {
                $_SESSION['chatreqtemp_folder_names'] = './public/uploads/chatreqtemp/' . $reqid;
            }
        } else {
            if (!is_dir($_SESSION['chatreqtemp_folder_names'])) {
                if (@mkdir('./public/uploads/chatreqtemp/' . $reqid, 0777, TRUE)) {
                    $_SESSION['chatreqtemp_folder_names'] = './public/uploads/chatreqtemp/' . $reqid;
                } else {
                    $_SESSION['chatreqtemp_folder_names'] = './public/uploads/chatreqtemp/' . $reqid;
                }
            }
        }
        
        if ($_SESSION['chatreqtemp_folder_names'] != '') {
            $files = $_FILES;
            $cpt = count($_FILES['shre_file']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['shre_file']['name'] = str_replace(" ","_",$files['shre_file']['name'][$i]);
                $_FILES['shre_file']['type'] = $files['shre_file']['type'][$i];
                $_FILES['shre_file']['tmp_name'] = $files['shre_file']['tmp_name'][$i];
                $_FILES['shre_file']['error'] = $files['shre_file']['error'][$i];
                $_FILES['shre_file']['size'] = $files['shre_file']['size'][$i];
                $config = array(
                    'upload_path' => $_SESSION['chatreqtemp_folder_names'],
                    'allowed_types' => ALLOWED_FILE_TYPES,
                    'max_size' => '0',
                    'overwrite' => FALSE
                );
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload("shre_file")) {
                    
                   // echo $thumb. "-thumb";exit;
                    $data = array($this->upload->data());
                    $data[0]['error'] = false;
                    $output['files'][] = $data;
                    $output['status'] = true;
                    $output['error'] = '';
                $result = $this->addMainchatFileRequest($reqid,$_FILES['shre_file']['name'],$_POST,$_FILES['shre_file']['tmp_name']);
                $arr[$i] = $result;
                } else {
//                    $data = array('error' => $this->upload->display_errors());
//                    $error_data = array();
//                    $error_data[0] = array();
//                    $error_data[0]['file_name'] = $files['shre_file']['name'][$i];
//                    $error_data[0]['error'] = true;
//                    $error_data[0]['error_msg'] = strip_tags($data['error']);
//                    $output['files'][] = $error_data;
//                    $output['status'] = true;
//                    $output['error'] = '';
                    $data = array('error' => $this->upload->display_errors());
                    $output['error_msg'] = strip_tags($data['error']);
                    $output['files'][] = $data;
                    $output['status'] = "error";
                    $output['error'] = true;
                    $arr[$i] = $output;
                }
            }
            $uploadPATH = $_SESSION['chatreqtemp_folder_names'];
        } else {
            $output['error'] = 'Directory not writable. Please try again.';
        }
        rmdir($uploadPATH."/");
        unset($_SESSION['chatreqtemp_folder_names']);
        echo json_encode($arr);
        
    }
    
    public function addMainchatFileRequest($success,$filename,$data,$tmp) {
//        echo "<pre/>";print_R($data);exit;
        $table = $this->S_request_model->getDataBasedonSAAS($success);
        if($table == 'requests'){
            $tb = 'request_discussions';
        }else{
            $tb = 's_request_discussions'; 
        }
        $login_user_id = $this->load->get_var('login_user_id');
        $login_user_data = $this->load->get_var('login_user_data'); 
        if($login_user_data[0]['role'] == 'customer' && $login_user_data[0]['is_saas'] == 1){
          $seen_by = 'admin_seen';
        }elseif($login_user_data[0]['role'] == 'manager'){
          $seen_by = 'qa_seen';  
        }elseif($login_user_data[0]['role'] == 'designer'){
          $seen_by = 'designer_seen';  
        }else{
         $seen_by = 'customer_seen';     
        }
        $output = array();
        $uid = $this->load->get_var('main_user');
        if (!is_dir('design_requests/public/uploads/requestmainchat/' . $success)) {
            mkdir('.design_requests/public/uploads/requestmainchat/' . $success, 0777, TRUE);
        }
        $uploadPATH = $_SESSION['chatreqtemp_folder_names'];
        $uploadPATHs = str_replace("./", "", $uploadPATH);
        $files = scandir($uploadPATH);
        $source = $uploadPATH . '/';
        $destination = '.design_requests/public/uploads/requestmainchat/' . $success . '/';
        $st = $data['data'];
//        echo "<pre>";print_r($files);exit;
        if (UPLOAD_FILE_SERVER == 'bucket') {
//            foreach ($files as $file) {
//                if (in_array($file, array(".", "..")))
//                    continue;
//                echo $uploadPATHs."<br>";
                $staticName = base_url() . $uploadPATHs . '/' . $filename;
                $config = array(
                    'upload_path' => 'design_requests/public/uploads/requestmainchat/' . $success.'/',
                    'file_name' => $filename
                );
                $this->s3_upload->initialize($config);
                $is_file_uploaded = $this->s3_upload->upload_multiple_file($staticName);
//                echo "<pre>";print_r($is_file_uploaded);
                if ($is_file_uploaded['status'] == 1) {
                    
                    $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
                    $img_arry = array("jpeg","jpg","png","gif");
                    if(in_array($ext,$img_arry)){
                        if (!is_dir('public/uploads/chatreqtemp/' . $success . '/_thumb')) {
                            $data = mkdir('./public/uploads/chatreqtemp/' . $success . '/_thumb', 0777, TRUE);
                        }
                        $thumb_tmp_path = 'public/uploads/chatreqtemp/' . $success . '/_thumb/';
                        $thumb = $this->myfunctions->make_thumb($tmp, $thumb_tmp_path . $filename, 600, $ext);
                        if($thumb){
                            $staticName1 = base_url() . $thumb_tmp_path . $filename;
                            $config = array(
                                'upload_path' => 'design_requests/public/uploads/requestmainchat/' . $success . '/_thumb/',
                                'file_name' => $filename
                            );
                            $this->load->library('s3_upload');
                            $this->s3_upload->initialize($config);
                            $uplod_thumb = $this->s3_upload->upload_multiple_file($staticName1);
                            if ($uplod_thumb['status'] == 1) {
                                $output['file_status'] = 1;
                                $deletee = $thumb_tmp_path . $filename;
                                unlink($deletee);
                                unlink('./tmp/tmpfile/' . basename($staticName1));
                            }else{
                                $output['file_status'] = 0;
                                $deletee = $thumb_tmp_path . $filename;
                                unlink($deletee);
                                unlink('./tmp/tmpfile/' . basename($staticName1));
                            }
                        }
                    }else{
                        $output['file_status'] = 0;
                    }
                    $delete[] = $source . $filename;
                    unlink('./tmp/tmpfile/' . basename($staticName)); 
                    $request_files_data = array("request_id" => $success,
                    "sender_id" => $login_user_id,
                    "reciever_id" => $st['reciever_id'],
                    "reciever_type" => $st['reciever_role'],
                    "sender_type" => $st['sender_role'],
                    "message" => $filename,
                    "with_or_not_customer" => 1,
                     $seen_by => 1,
                    "is_filetype" => 1,
                    "created" => date("Y-m-d H:i:s"));
//                    echo "<pre>";print_r($request_files_data);
                $succ_id = $this->Welcome_model->insert_data($tb, $request_files_data);
                if($succ_id){
                    $output['status'] = "success";
                    $output['req_id'] = $success;
                    $output['file'] = $filename;
                    $output['id'] = $succ_id;
                }else{
                   $output['status'] = "error";
                }
                }
//            }
           // rmdir($thumb_tmp_path);
            
        } 
        else {
            foreach ($files as $file) {
                if (in_array($file, array(".", "..")))
                    continue;
                $filepath = APPPATH . $source . $file;
                if (copy($source . $file, $destination . $file)) {
                    $delete[] = $source . $file;
                    unlink($source.$file);
                }
                $is_file_uploaded['status'] = 1;
            }
        }
        foreach ($delete as $file) {
            unlink($file);
        }
         $config['image_library'] = 'gd2';
//        if ($is_file_uploaded['status'] == 1) {
          return $output;
//        }
        }
        
        public function gz_delete_files() {
        if (isset($_POST)) {
//            echo "<pre/>";print_R($_POST);exit;
            $login_user_id = $this->load->get_var('login_user_id'); 
            $id = $_POST['requests_files'];
            $request_id = $_POST['request_id'];
             $table = $this->S_request_model->getDataBasedonSAAS($request_id);
            if($table == 'requests'){
                $tb = 'request_files';
            }else{
                $tb = 's_request_files'; 
            }
            $delete = $this->S_admin_model->delete_data($tb, $id);
            if ($delete) {
                if (array_key_exists('prefile', $_POST)) {
                    $src_filename = $_POST['srcfile'];
                    $pre_filename = $_POST['prefile'];
                    $thumb_prefile = $_POST['thumb_prefile'];
                    //$this->myfunctions->capture_project_activity($request_id,$id,'','delete_draft_by_qa','gz_delete_files',$login_user_id,'0','1','1');
                    redirect(base_url() . 'account/request/project_image_view/' . $id . '?id='.$request_id);
                }
            }
        }
    }
    
    public function editComment() {
        $data = array();
        $data['id'] = isset($_POST['request_file_id']) ? $_POST['request_file_id'] : '';
        $data['message'] = isset($_POST['updated_msg']) ? $_POST['updated_msg'] : '';
        $reqID = isset($_POST['reqID']) ? $_POST['reqID'] : '';
        $table = $this->S_request_model->getDataBasedonSAAS($reqID);
        if($table == 'requests'){
            $tb = 'request_file_chat';
        }else{
            $tb = 's_request_file_chat'; 
        }
        $success = $this->Welcome_model->update_data($tb, $data, array("id" => $data['id']));
        if ($success) {
        //die("update");
            echo json_encode($data);
        }
    }
    
    public function deleteRequestmsg() {
        $reqID = isset($_POST['reqID']) ? $_POST['reqID'] : '';
        $output = array();
        $request_file_id = isset($_POST['request_file_id']) ? $_POST['request_file_id'] : '';
        if ($request_file_id) {
            $success = $this->S_request_model->deleteMsg($request_file_id,$reqID);
            if ($success) {
                $output['status'] = "success";
                $output['msg'] = "message deleted successfully";
            }
        }
        echo json_encode($output);
    }
    
    /**********file uploading in draft chat*********/
    public function uploaddraftChatfiles(){
        $arr = array();
        $output = array();
        $output['files'] = array();
        $output['status'] = false;
        $output['error'] = 'Error while uploading the files. Please try again.';
        $reqid = isset($_POST['data']['request_id'])?$_POST['data']['request_id']:'';
        $reqfileid = isset($_POST['data']['request_file_id'])?$_POST['data']['request_file_id']:'';
        if (!(isset($_SESSION['chatreqdrafttemp_folder_names'])) && ($_SESSION['chatreqdrafttemp_folder_names'] == '')) {
              if(@mkdir('./public/uploads/chatreqdraftemp/' . $reqid. '/' .$reqfileid , 0777, TRUE)){
                $_SESSION['chatreqdrafttemp_folder_names'] = './public/uploads/chatreqdraftemp/' . $reqid.'/'.$reqfileid;
              } else {
                $_SESSION['chatreqdrafttemp_folder_names'] = './public/uploads/chatreqdraftemp/' . $reqid.'/'.$reqfileid;
              }
        } else {
              if(@mkdir('./public/uploads/chatreqdraftemp/' . $reqid. '/' .$reqfileid , 0777, TRUE)){
                $_SESSION['chatreqdrafttemp_folder_names'] = './public/uploads/chatreqdraftemp/' . $reqid.'/'.$reqfileid;
              } else {
                $_SESSION['chatreqdrafttemp_folder_names'] = './public/uploads/chatreqdraftemp/' . $reqid.'/'.$reqfileid;
              }
        }
        if ($_SESSION['chatreqdrafttemp_folder_names'] != '') {
            $files = $_FILES;
            $cpt = count($_FILES['shre_main_draft_file']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $_FILES['shre_main_draft_file']['name'] = str_replace(" ","_",$files['shre_main_draft_file']['name'][$i]);
                $_FILES['shre_main_draft_file']['type'] = $files['shre_main_draft_file']['type'][$i];
                $_FILES['shre_main_draft_file']['tmp_name'] = $files['shre_main_draft_file']['tmp_name'][$i];
                $_FILES['shre_main_draft_file']['error'] = $files['shre_main_draft_file']['error'][$i];
                $_FILES['shre_main_draft_file']['size'] = $files['shre_main_draft_file']['size'][$i];
                $config = array(
                    'upload_path' => $_SESSION['chatreqdrafttemp_folder_names'],
                    'allowed_types' => ALLOWED_FILE_TYPES,
                    'max_size' => '0',
                    'overwrite' => FALSE
                );

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload("shre_main_draft_file")) {
                    $data = array($this->upload->data());
                    $data[0]['error'] = false;
                    $output['files'][] = $data;
                    $output['status'] = true;
                    $output['error'] = '';
                $result = $this->adddarftchatFileRequest($reqid,$reqfileid,$_FILES['shre_main_draft_file']['name'],$_POST,$_FILES['shre_main_draft_file']['tmp_name']);
//                echo "<pre/>";print_R($result);exit;
                $arr[$i] = $result;
                } else {
                    $data = array('error' => $this->upload->display_errors());
                    $output['error_msg'] = strip_tags($data['error']);
                    $output['files'][] = $data;
                    $output['status'] = "error";
                    $output['error'] = true;
                    $arr[$i] = $output;
                }
            }
            $uploadPATH = $_SESSION['chatreqdrafttemp_folder_names'];
        } else {
            $output['error'] = 'Directory not writable. Please try again.';
        }
        rmdir($uploadPATH."/");
        unset($_SESSION['chatreqdrafttemp_folder_names']);
        echo json_encode($arr);
        
    }
    
public function adddarftchatFileRequest($success,$reqfileid,$filename,$data,$tmp) {
    $output = array();
    $uid = $this->load->get_var('main_user');
    if (!is_dir('design_requests/public/uploads/requestdraftmainchat/' . $success)) {
        if (!is_dir('design_requests/public/uploads/requestdraftmainchat/' . $success.'/'.$reqfileid)) {
         mkdir('.design_requests/public/uploads/requestdraftmainchat/' . $success.'/'.$reqfileid, 0777, TRUE);
        }
    }
    $table = $this->S_request_model->getDataBasedonSAAS($success);
        if($table == 'requests'){
            $tb = 'request_file_chat';
        }else{
            $tb = 's_request_file_chat'; 
        }
    $uploadPATH = $_SESSION['chatreqdrafttemp_folder_names'];
    $uploadPATHs = str_replace("./", "", $uploadPATH);
    $files = scandir($uploadPATH);
    $source = $uploadPATH . '/';
    $destination = '.design_requests/public/uploads/requestdraftmainchat/' . $success . '/'.$reqfileid.'/';
//    if (!is_dir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES . $success . '/'.$reqfileid . '/_thumb')) {
//        mkdir(FS_UPLOAD_PUBLIC_UPLOADS_REQUESTDRAFTCHATFILES . $success . '/'.$reqfileid . '/_thumb', 0777, TRUE);
//    }
    $st = $data['data'];
    $config['image_library'] = 'gd2';
    $img_arry = array("jpeg", "jpg", "png", "gif");
    $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
//    echo "<pre/>";print_R($files);
    if (UPLOAD_FILE_SERVER == 'bucket') {
        $staticdraftName = base_url() . $uploadPATHs . '/' . $filename;
        $config = array(
            'upload_path' => 'design_requestspublic/uploads/requestdraftmainchat/' . $success . '/'.$reqfileid.'/',
            'file_name' => $filename
        );
        $this->s3_upload->initialize($config);
        $is_file_uploaded = $this->s3_upload->upload_multiple_file($staticdraftName);
        if ($is_file_uploaded['status'] == 1) {
            if (in_array($ext, $img_arry)) {
                if (!is_dir('public/uploads/chatreqdraftemp/' . $success . '/'.$reqfileid.'/_thumb')) {
                    $data = mkdir('./public/uploads/chatreqdraftemp/' . $success . '/'.$reqfileid.'/_thumb', 0777, TRUE);
                }
                $thumb_tmp_path = 'public/uploads/chatreqdraftemp/' . $success . '/'.$reqfileid.'/_thumb/';
                $thumb_create = $this->myfunctions->make_thumb($tmp, $thumb_tmp_path . $filename, 600, $ext);
                if($thumb_create){
                    $staticdraftName1 = base_url() . $thumb_tmp_path . $filename;
                    //echo $staticdraftName1;
                        $config = array(
                            'upload_path' => 'design_requests/public/uploads/requestdraftmainchat/' . $success . '/'.$reqfileid.'/_thumb/',
                            'file_name' => $filename
                        );
                       // echo "<pre>";print_r($config);
                        $this->load->library('s3_upload');
                        $this->s3_upload->initialize($config);
                        $uplod_thumb = $this->s3_upload->upload_multiple_file($staticdraftName1);
                        if ($uplod_thumb['status'] == 1) {
                            $output['file_status'] = 1;
                            $deletee = $thumb_tmp_path . $filename;
                            unlink($deletee);
                            unlink('./tmp/tmpfile/' . basename($staticdraftName1));
                        } else {
                            $output['file_status'] = 0;
                            $deletee = $thumb_tmp_path . $filename;
                            unlink($deletee);
                            unlink('./tmp/tmpfile/' . basename($staticdraftName1));
                        }
                }
            }else{
                $output['file_status'] = 0;
            }
            $delete[] = $source . $filename;
            unlink('./tmp/tmpfile/' . basename($staticName));
            
            $request_files_data = array("request_file_id" => $reqfileid,
            "sender_role" => $st['sender_role'],
            "sender_id" => $uid,
            "receiver_role" => $st['reciever_role'],
            "parent_id" => $st['parentid'],
            "receiver_id" => $st['reciever_id'],
            "message" => $filename,
            "admin_seen" => ($st['seenby'] == 'admin_seen')?'1':'0',
            "customer_seen" => ($st['seenby'] == 'customer_seen')?'1':'0',
            "designer_seen" => ($st['seenby'] == 'designer_seen')?'1':'0',
            "is_filetype" => 1,
            "created" => date("Y-m-d H:i:s"));
        $succ_id = $this->Welcome_model->insert_data($tb, $request_files_data);
        if($succ_id){
            $output['status'] = "success";
            $output['req_id'] = $success;
            $output['file_id'] = $reqfileid;
            $output['file'] = $filename;
            $output['id'] = $succ_id;
        }else{
            $output['status'] = "error";
        }
        }
    }
    else {
        foreach ($files as $file) {
            if (in_array($file, array(".", "..")))
                continue;
            $filepath = APPPATH . $source . $file;
            if (copy($source . $file, $destination . $file)) {
                $delete[] = $source . $file;
                unlink($source.$file);
            }
            $is_file_uploaded['status'] = 1;
        }
      if($is_file_uploaded['status'] == 1){
            $this->load->library('image_lib', $config);
        if (in_array($ext, $img_arry)) {
            $thumb_tmp_path = './public/uploads/requestdraftmainchat/' . $success .'/'.$reqfileid.'/_thumb/';
            $thumb = $this->myfunctions->make_thumb($tmp, $thumb_tmp_path . $filename, 600, $ext);
        }else{
            $filename = preg_replace('/\s+/', '_', $filename);
            $previewname = $filename;
        }
        $request_files_data = array("request_file_id" => $reqfileid,
            "sender_role" => $st['sender_role'],
            "sender_id" => $uid,
            "receiver_role" => $st['reciever_role'],
            "parent_id" => $st['parentid'],
            "receiver_id" => $st['reciever_id'],
            "message" => $filename,
            "admin_seen" => ($st['seenby'] == 'admin_seen')?'1':'0',
            "customer_seen" => ($st['seenby'] == 'customer_seen')?'1':'0',
            "designer_seen" => ($st['seenby'] == 'designer_seen')?'1':'0',
            "is_filetype" => 1,
            "created" => date("Y-m-d H:i:s"));
        $succ_id = $this->Welcome_model->insert_data($tb, $request_files_data);
        if($succ_id){
            $output['status'] = "success";
            $output['req_id'] = $success;
            $output['file_id'] = $reqfileid;
            $output['file'] = $filename;
            $output['id'] = $succ_id;
        }else {
            $output['status'] = "error";
        }
    }
        
    }
        return $output;
    } 
    
    
            public function request_response_by_admin() {
            $result = array();
//            echo "<pre/>";print_R($_POST);exit;
//            $this->myfunctions->checkloginuser("admin");
//            $this->Welcome_model->update_online();
            $login_user_id = $this->load->get_var('login_user_id');
            $table = $this->S_request_model->getDataBasedonSAAS($_POST['request_id']);
            if($table == 'requests'){
                $tb = 'request_files';
            }else{
                $tb = 's_request_files'; 
            }
            $request_data = $this->S_request_model->get_request_by_id($_POST['request_id']);
            $count_quality_rev = $request_data[0]['count_quality_revision'];
            $email = $this->S_request_model->getuserbyid($request_data[0]['customer_id']);
            $is_approveDraftemailbyadmin = $this->S_admin_model->approveDraftemailbyadmin();
           // $email_designer = $this->S_request_model->getuserbyid($request_data[0]['designer_id']);
            $request_file = $this->S_request_model->get_image_by_request_file_id($_POST['fileid']);
            $success = $this->Welcome_model->update_data($tb, array("modified" => date("Y:m:d H:i:s"), "status" => $_POST['value']), array("id" => $_POST['fileid']));
            if ($success) {
                if ($_POST['value'] == "Reject") {
                    $_POST['value'] = "disapprove";
                    $count_rev = $count_quality_rev + 1;
                    $this->Welcome_model->update_data($table, array("status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "who_reject" => 0,"count_quality_revision" => $count_rev), array("id" => $_POST['request_id']));
                    //$this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'','reject_design_draft','admin_request_response',$login_user_id,'0','1','1');
                    //$this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Design For \"" . $request_data[0]['title'] . "\" is Rejected..!", "designer/request/project_info/" . $_POST['request_id'], $request_data[0]['designer_id']);
                } elseif ($_POST['value'] == "customerreview") {
                    $_POST['value'] = "checkforapprove";
                    if ($request_data[0]['status'] != 'approved') {
                        $this->Welcome_model->update_data($table, array("approvaldate" => date("Y:m:d H:i:s"), "status" => $_POST['value'], "status_qa" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    } else {
                        $this->Welcome_model->update_data($table, array("approvaldate" => date("Y:m:d H:i:s"), "status_qa" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    }
                    $ActiveReq = $this->myfunctions->CheckActiveRequest($request_data[0]['customer_id']);
                    if ($ActiveReq == 1) {
                        $this->myfunctions->move_project_inqueqe_to_inprogress($request_data[0]['customer_id'],'','request_response',$_POST['request_id']);
                    }
                    $subdomain_url = $this->myfunctions->dynamic_urlforsubuser_inemail($request_data[0]['customer_id'], $email[0]['plan_name']);
                    $this->myfunctions->send_email_after_admin_approval($request_data[0]['title'], $request_data[0]['id'], $email[0]['email'], $_POST['fileid'],$subdomain_url);
                    $send_emailto_subusers = $this->myfunctions->send_emails_to_sub_user($request_data[0]['customer_id'], 'approve/revision_requests', $is_approveDraftemailbyadmin, $request_data[0]['id']);
                    if (!empty($send_emailto_subusers)) {
                        foreach ($send_emailto_subusers as $send_emailto_subuser) {
                            $this->myfunctions->send_email_after_admin_approval($request_data[0]['title'], $request_data[0]['id'], $send_emailto_subuser['email'], $_POST['fileid'], $subdomain_url);
                        }
                    }
                    //$this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'','approved_draft_design','admin_request_response',$login_user_id,'0','','1');
                    //$this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'',$request_data[0]['status_admin'].'_to_'.$_POST['value'],'admin_request_response',$login_user_id,'1','','0');
                    //$this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Upload New File For \"" . $request_data[0]['title'] . "\". Please Check And Approve..!", "customer/request/project_info/" . $_POST['request_id'], $request_data[0]['customer_id']);
                    //$this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Approved your \"" . $request_data[0]['title'] . "\" design", "designer/request/project_info/" . $_POST['request_id'], $request_data[0]['designer_id']);
                } else {
                    $_POST['value'] = "approved";
                    $this->Welcome_model->update_data($table, array("approvaldate" => date("Y:m:d H:i:s"), "status" => $_POST['value'], "status_designer" => $_POST['value'], "status_admin" => $_POST['value'], "modified" => date("Y-m-d H:i:s")), array("id" => $_POST['request_id']));
                    //$this->myfunctions->capture_project_activity($_POST['request_id'],$_POST['fileid'],'',$request_data[0]['status_admin'].'_to_'.$_POST['value'],'admin_request_response',$login_user_id,'1');
                    $this->myfunctions->show_notifications($_POST['request_id'], $login_user_id, "Your Design \"" . $request_data[0]['title'] . "\" is Approved..!", "designer/request/project_info/" . $_POST['request_id'], $request_data[0]['designer_id']);
                    $request2 = $this->S_admin_model->get_all_request_count(array("active"), $request_data[0]['customer_id']);
                    if (sizeof($request2) <= 1) {
                        $all_requests = $this->S_request_model->getall_request($request_data[0]['customer_id'], "'pending','assign'", "", "index_number");
                        if (isset($all_requests[0]['id'])) {
                            $success = $this->Welcome_model->update_data($table, array("status" => "active", "status_admin" => "active", "status_designer" => "active", "status_qa" => "active", "dateinprogress" => date("Y-m-d H:i:s"), "modified" => date("Y-m-d H:i:s")), array("id" => $all_requests[0]['id']));
                        }
                    }
                }
                
//                $feedbackColl = $_POST["form-input"];
//                $dataArr = array(
//                    "request_id" => $_POST['request_id'],
//                    "draft_id" => $_POST['fileid'],
//                    "user_role" => "admin",
//                    "designer_id" => $request_data[0]['designer_id'],
//                    "qa_id" => $login_user_id,
//                    "q_feedback" => 1,
//                    "q_instructions" => $feedbackColl[3]['value'],
//                    "q_brand_guidelines" => $feedbackColl[4]['value'],
//                    "q_attachments" => $feedbackColl[5]['value'],
//                    "q_dimensions" => $feedbackColl[6]['value'],
//                    "q_color_preferences" => $feedbackColl[7]['value'],
//                    "q_required_text" => $feedbackColl[8]['value'],
//                    "q_source_files" => $feedbackColl[9]['value'],
//                    "q_font_files" => $feedbackColl[10]['value'],
//                    "q_quality_review" => $feedbackColl[11]['value'],
//                    "qa_feedback_date" => date("Y-m-d H:i:s"),
//                );
//
//                $data_q = $this->Welcome_model->select_data("id", "request_files_review", "request_id = '" . $_POST['request_id'] . "' AND draft_id = '" . $_POST['fileid'] . "'");
//                if (empty($data_q)) {
//                    $this->Welcome_model->insert_data("request_files_review", $dataArr);
//                } else {
//                    $this->Welcome_model->update_data("request_files_review", $dataArr, array("request_id" => $_POST['request_id'], "draft_id" => $_POST['fileid']));
//                }
            }
            $modified_date = $this->onlytimezone(date("Y:m:d H:i:s"));
            $result['modified'] = date("M d, Y h:i A", strtotime($modified_date));
            $result['success'] = $success;
            echo json_encode($result);
            exit;
        }
}
