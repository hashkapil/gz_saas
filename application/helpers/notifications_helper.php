<?php
if (!function_exists('helpr')) {
        function helpr() {
                $ci = &get_instance();
                $urlopen = parse_url(current_url());
                $getuser = $ci->Request_model->getuserinfobydomain($urlopen['host']);
                    if (!empty($getuser) && !isset($_SERVER["HTTPS"]) && $getuser[0]['ssl_or_not'] == 1) {
                        redirect(str_replace("http://", "https://", current_url()));
                    }
                    
                $notifications = $ci->Request_model->get_notifications($_SESSION['user_id']);
                foreach ($notifications as $key => $value) {
                    $notifications[$key]['profile_picture'] = $ci->customfunctions->getprofileimageurl($value['profile_picture']);
                }
                $notification_number = $ci->Request_model->get_notifications_number($_SESSION['user_id']);

                $messagenotifications = $ci->Request_model->get_messagenotifications($_SESSION['user_id']);
                foreach ($messagenotifications as $key => $value) {
                    $messagenotifications[$key]['profile_picture'] = $ci->customfunctions->getprofileimageurl($value['profile_picture']);
                }

                $messagenotification_number = $ci->Request_model->get_messagenotifications_number($_SESSION['user_id']);

                $profile_data = $ci->Admin_model->getuser_data($_SESSION['user_id']);
                $profile_data[0]['profile_picture'] = $ci->customfunctions->getprofileimageurl($profile_data[0]['profile_picture']);
                return array('notifications' => $notifications, 'notification_number' => $notification_number, 'messagenotifications' => $messagenotifications, 'messagenotification_number' => $messagenotification_number, 'profile_data' => $profile_data);
        }
}