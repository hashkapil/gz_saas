<?php
    function meta() {
        $ci =& get_instance();
        $page = $ci->router->fetch_method();
        $meta = '<meta charset="utf-8">';
        switch ($page) {
            case 'home':
                $meta .= '<title>Graphics Zoo Offers Unlimited Graphic Design Services & Packages in Houston</title>';
                $meta .= '<meta name="description" property="og:description" content="Are you on a lookout for an unlimited professional graphic design company in Houston? Graphics Zoo excels in expert graphic design services within your budget. />';
                $meta .= '<meta name="site_name" property="og:site_name" content="Graphicszoo.com" />';
                $meta .= '<meta name="keywords"  content="on demand graphic design, graphic design services list, on demand design, outsource graphic design, affordable graphic design, marketing graphic design, professional graphic design services, graphic design company, graphic design firms, on demand design services, unlimited graphic design, unlimited graphic design packages, unlimited graphics, unlimited design, unlimited design services" />';
                break;
            case 'aboutus':
                $meta .= '<title>Graphics Zoo: An Ultimate Graphic Design Company & Tools in Houston</title>';
                
                $meta .= '<meta name="description" property="og:description" content="Graphics Zoo provides you with excellent graphic design services and tools in Houston. Get in touch to turn your ideas into life./>';
                $meta .= '<meta name="site_name" property="og:site_name" content="Graphicszoo.com" />';
                $meta .= '<meta name="keywords"  content="graphic design services, graphic design tools, how to find a graphic designer, graphic design houston, houston graphic design firm, graphic design company in houston" />';
                break;
            case 'portfolio':
                $meta .= '<title>Portfolio | Graphics Zoo</title>';
//                $meta .= '<title>Graphics Zoo: A One Stop Company for Online Logo & T-shirt Graphic Design</title>';
                $meta .= '<meta name="description" property="og:description" content="Find a reputable t-shirt graphic design company online or hire logo designers in Houston to match your style and create an excellent impact on the industry. />';
                $meta .= '<meta name="site_name" property="og:site_name" content="Graphicszoo.com" />';
                $meta .= '<meta name="keywords"  content="find a graphic designer, online graphic design services, hire logo designer, artwork design services, hire logo designer, t shirt graphic design, t shirt graphic design company" />';
                break;
             case 'pricing':
                $meta .= '<title>Graphics Zoo Offers Website Design Subscription & Digital Graphic Design Tools </title>';
                $meta .= '<meta name="description" property="og:description" content="Are you on a hunt for digital graphic design tools or website design subscription services? Get in touch with Graphics Zoo. We offer cost-effective monthly packages for digital graphic design./>';
                $meta .= '<meta name="site_name" property="og:site_name" content="Graphicszoo.com" />';
                $meta .= '<meta name="keywords"  content="digital graphic design tools, website design subscription service" />';
            default:
                $meta .= '<title>Graphics Zoo Offers Unlimited Graphic Design Services & Packages in Houston</title>';
                
                $meta .= '<meta name="description" property="og:description" content="Are you on a lookout for an unlimited professional graphic design company in Houston? Graphics Zoo excels in expert graphic design services within your budget. />';
                $meta .= '<meta name="site_name" property="og:site_name" content="Graphicszoo.com" />';
                $meta .= '<meta name="keywords"  content="on demand graphic design, graphic design services list, on demand design, outsource graphic design, affordable graphic design, marketing graphic design, professional graphic design services, graphic design company, graphic design firms, on demand design services, unlimited graphic design, unlimited graphic design packages, unlimited graphics, unlimited design, unlimited design services" />';
            
                break;
        }
        return $meta;
    }
?>