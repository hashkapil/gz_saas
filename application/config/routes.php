<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//echo $_SERVER['HTTP_HOST']."<br/>".DOMAIN_NAME;
if($_SERVER['HTTP_HOST'] == DOMAIN_NAME || $_SERVER['HTTP_HOST'] == 'graphicszoo.com'){  
	$route['default_controller'] = 'welcome';
         $route['404_override'] = 'my404controller';
}else{
	$route['default_controller'] = 'welcome/loginforsubdomain'; 
	$route['user_signup'] = 'welcome/signupforsubdomain';
        $route['404_override'] = 'subdomain404controller';
}
//$route['404_override'] = 'my404controller';
$route['translate_uri_dashes'] = FALSE;
$route['sitemap\.xml'] = "Sitemap/index";

$route['pricing'] = "welcome/pricing";
$route['affiliate_signup'] = "welcome/affiliate_signup";
$route['affiliate'] = "welcome/affiliate";
$route['saas'] = "welcome/saas";
//$route['cron'] = "welcome/cronset";
$route['portfolio'] = "welcome/portfolio";
$route['portfoliotwo'] = "welcome/portfoliotwo";
$route['features'] = "welcome/features";
$route['article/(:any)'] = "welcome/blog_detail/$1";
$route['blog'] = "welcome/blog";
$route['pricing_new'] = "welcome/pricing_new";
$route['aboutus'] = "welcome/aboutus";
$route['signup'] = "welcome/signupnew";
$route['saas_signup'] = "welcome/saas_signup";
$route['requests_signup'] = "welcome/single_request_signup";
$route['cron_add_request_by_mail'] = "Request_by_email/index";
//$route['signupnew'] = "welcome/signupnew";
$route['requests_thankyou'] = "welcome/requests_thankyou";
$route['accessdenied'] = "Welcome/accessdenied";
$route['thank-you-signup'] = "welcome/thank_you_signup";
$route['thank-you-free-design'] = "welcome/thank_you_free_design";
$route['project-info/(:any)'] = "welcome/project_info/$1";
$route['payment'] = "welcome/bypasspayment_process";


$route['free-design-signup'] = "welcome/free_design_signup";
$route['paywall_signup'] = "welcome/without_pay_signup";
$route['trial_account'] = "welcome/trial_account";
$route['cron_mail'] = "welcome/cron_mail_to_customer_for_unseen_message";
$route['project_view/(:any)'] = "welcome/project_image_view_afteraprovedesign/$1";
$route['project_info_view/(:any)'] = "welcome/project_info_for_unseenmsg/$1";
$route['login'] = "welcome/login";
$route['view_blog/(:num)'] = "welcome/blog_detail/$1";
$route['faq'] = "welcome/faq";
$route['contactus'] = "welcome/contactus";
$route['termsandconditions'] = "welcome/termsandconditions";
$route['privacypolicy'] = "welcome/privacypolicy";
$route['categories'] = "welcome/categories";
$route['testimonials'] = "welcome/testimonials";
$route['career'] = "welcome/career";
$route['affiliare_program'] = "welcome/affiliare_program";
$route['news'] = "welcome/news";
$route['blog_detail'] = "welcome/blog_detail";
$route['support'] = "welcome/support";
$route['webhook'] = "welcome/stripe_webhook";
$route['coonectuser_webhook'] = "welcome/stripewebhook_frconnecteduser";
$route['dripchk'] = "welcome/dripchk";
$route['how_it_work'] = "welcome/how_it_work";

//Logged Customer Routes
$route['customer/client_management'] = "customer/AgencyClients/index";
$route['customer/client_management/(:any)'] = "customer/AgencyClients/index/$1";
$route['customer/user_setting'] = "customer/AgencyUserSetting/index";
$route['customer/request/design_request'] = "customer/Request/design_request_1";
$route['customer/request/add_brand_profile'] = "customer/Request/add_brand_profile";
$route['customer/request/new_request/category'] = "customer/Request/new_request_category";
$route['customer/request/new_request/brief/(:any)'] = "customer/Request/new_request_brief/$1";
$route['customer/request/new_request/review/(:any)'] = "customer/Request/new_request_review/$1";
$route['customer/request/client-profile'] = "customer/Request/client_profile";
$route['customer/request/dashboard-list-view'] = "customer/Request/dashboard_list_view";
$route['cron_subuser_billingcycle'] = "customer/Request/cron_updatebillingcycle_forsubuser";
//$route['customer/request/project-image-view'] = "customer/Request/project_image_view";

//echo "<script>alert('In Routes')</script>";
$route['customer/request/project-info/(:num)'] = "customer/Request/project_info/$1";
$route['customer/request/project-info/(:num)/(:num)'] = "customer/Request/project_info/$1/$2";
$route['designer/request/project-info/(:num)'] = "designer/Request/project_info/$1";
$route['designer/request/project-info/(:num)/(:num)'] = "designer/Request/project_info/$1/$2";
//echo "<script>alert('out Routes')</script>";

$route['customer/setting-view'] = "customer/ChangeProfile/setting_view";
$route['customer/sub_user_list'] = "customer/MultipleUser/sub_user_list";
$route['customer/sub_user'] = "customer/MultipleUser/sub_user"; 
$route['customer/add_brand_profile'] = "customer/BrandProfile/add_brand_profile";
$route['customer/brand_profiles'] = "customer/BrandProfile/brand_profiles";
$route['customer/new-setting-view'] = "customer/ChangeProfile/new_setting_view";
$route['customer/setting-edit'] = "customer/ChangeProfile/setting_edit";

$route['customer/request/design_request_old'] = "customer/Request/design_request";

$route['customer/request/prioritySet/(:num)/(:num)/(:num)'] = "customer/Request/set_priority/$1/$2/$3";
$route['customer/request/prioritySetforlistview/(:num)/(:num)/(:num)'] = "customer/Request/set_priority_for_listview/$1/$2/$3";
$route["qa/dashboard/profile"] = "qa/dashboard/qaprofile";
$route["qa/dashboard/editprofile"] = "qa/dashboard/editqaprofile";
$route["exitaccess"] = "admin/accounts/exituseraccess";

/************SAAS REDIRECT*********************/
$route['account/dashboard'] = "account/Request/design_request_1";
$route['account/brand_profiles'] = "account/BrandProfile/brand_profiles";
$route['account/user_setting'] = "account/AgencyUserSetting/index";
$route['account/setting-view'] = "account/ChangeProfile/setting_view";
$route['account/client_management'] = "account/AgencyClients/index";
$route['account/client_management/(:any)'] = "account/AgencyClients/index/$1";
$route['account/sub_user'] = "account/MultipleUser/sub_user"; 
$route['account/designer_management'] = "account/DesignerManagement/index";
$route['account/request/project-info/(:num)'] = "account/Request/project_info/$1";
$route['account/request/project-info/(:num)/(:num)'] = "account/Request/project_info/$1/$2";
$route['account/add_brand_profile'] = "account/BrandProfile/add_brand_profile";
$route['s_project-info/(:any)'] = "S_link_sharing/s_project_info/$1";


