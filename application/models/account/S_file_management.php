<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class S_file_management extends CI_Model {

    public function getRequestfilesofUser($userid) { 
        
        $login_user_id = $this->load->get_var('login_user_id');
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $this->db->select("rq.id as request_id,rq.title as name,rf.user_id as user_id,rq.status as status");
        $this->db->join('request_files as rf', 'rf.request_id = rq.id', 'INNER');
        $this->db->where("rq.customer_id", $userid);
        if(!empty($sub_usersbrands)){
            $this->db->where("((rq.brand_id IN (".implode(',',$sub_usersbrands).")) or (rq.brand_id = 0 and rq.created_by='".$login_user_id."'))");
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
            $this->db->where("rq.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("rq.sub_user_priority", "ASC");
        }
        $this->db->group_by('rq.id');
        $this->db->from("s_requests as rq");
        $data = $this->db->get();
//         echo $this->db->last_query();exit;
        $result = $data->result_array();

    //    echo "<pre>"; print_r($result); die; 
    
        foreach ($result as $k => $folder) { 
            $result['status'][] = $this->myfunctions->customerVariables($result[$k]['status']);
            $result1 = $this->getallRequestfiles($folder['request_id']);
            $count[] = $result1['count_files'];
           } 
            $result['count'] =  $count; 
           // echo "<pre>"; print_r($result); die;
            
         return $result;    
    }

    public function get_mark_as_fav_document($userid){
        $this->db->select("rq.id as request_id,rq.title as name,rf.user_id as user_id,rf.src_file");
        $this->db->where("rq.customer_id", $userid);
        $this->db->where("rf.mark_as_fav", 1);
        $this->db->join('request_files as rf', 'rf.request_id = rq.id', 'left');
        $this->db->from("s_requests as rq");
        $data = $this->db->get();
        // echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
        // echo "<pre>"; print_r($result);exit;
     }

    public function getallRequestfiles($reqID, $role = "") {
        $this->db->select("*");
        $this->db->where("request_id", $reqID);
        $this->db->order_by("user_type", "ASC");
        if ($role != "") {
            // $this->db->where("user_type",$role);
        }
        $data = $this->db->get("request_files");
        // echo $this->db->last_query();exit;
        $result['files'] = $data->result_array();
        $result['count_files'] = !empty($result)? count($result['files']) : count($result = array());  
        return $result;
    }

    public function getSubFolderbyID($parent_id) {
        $this->db->select("*");
        $this->db->where("parent_folder_id", $parent_id);
        $this->db->from("s_folder_structure");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }

    public function getFolderStructure($self_id="",$type="") {
        $login_user_id = $this->load->get_var('login_user_id');
        $output = array();
        $this->db->select("fs.folder_name,fs.id,fs.parent_folder_id,fs.user_id,sj.folder_name as parent_name,fs.folder_type");
        if ($self_id != '' || $self_id != NULL) {
            $this->db->where("fs.parent_folder_id", $self_id);
            $this->db->where("fs.user_id", $login_user_id);
            $this->db->join('s_folder_structure as sj', 'sj.id = fs.parent_folder_id', 'LEFT');
        }
        $this->db->from("s_folder_structure as fs");
        $data = $this->db->get();
        $result = $data->result_array();
        // get only folders main n inside 
        if ($self_id == 0) {
            $output['folder'] = $result;
            // check files exist in same folder
        } else {
            $this->db->select("ffs.id as id,ffs.file_id as file_id,ffs.file_name as img_name,ffs.request_id as reqid,ffs.mark_as_fav as markasfav,rf.file_name as image");
            $this->db->join('request_files as rf', 'rf.id = ffs.file_id', 'LEFT');
            $this->db->where("ffs.folder_id", $self_id);
            $this->db->from("s_folder_file_structure as ffs");
            $data = $this->db->get();
            $result1 = $data->result_array();
            $output['folder'] = $result;
            $output['files'] = $result1;
        }
        return $output;
    }

   public function getFolderStructure_New($self_id){
        $output = array();
        $this->db->select("fs.folder_name,fs.id,fs.parent_folder_id,fs.user_id,sj.folder_name as parent_name,fs.folder_type");
        if ($self_id != '' || $self_id != NULL) {
            $this->db->where("fs.parent_folder_id", $self_id);
            $this->db->join('s_folder_structure as sj', 'sj.id = fs.parent_folder_id', 'LEFT');
        }
        $this->db->from("s_folder_structure as fs");

        $data = $this->db->get();
        //echo $this->db->last_query()."<br>"; 
        $result = $data->result_array();
        // get only folders main n inside 
        if ($self_id == 0) {
            $output['folder'] = $result;
            // check files exist in same folder
        } else {
            $this->db->select("ffs.id as id,ffs.folder_id as folder_id,ffs.file_id as file_id,ffs.file_name as img_name,ffs.request_id as reqid,ffs.mark_as_fav as markasfav,rf.file_name as image,rf.mark_as_fav as mark_as_fav");
            $this->db->join('request_files as rf', 'rf.id = ffs.file_id', 'LEFT');
            $this->db->where("ffs.folder_id", $self_id);
            $this->db->from("s_folder_file_structure as ffs");
            $data = $this->db->get();
            // echo $this->db->last_query()."<br>"; 
            $result1 = $data->result_array();
            $output['folder'] = $result;
            $output['files'] = $result1;
        }
     
        return $output; 
      }

    public function getFolderDefaultStructure($usrid,$folderType,$Parentid="")
    {
      // echo "here"; exit; 
            $this->db->select("*");
            $this->db->from("s_folder_structure");
            $this->db->where("folder_type",$folderType);
            $data = $this->db->get();
            $result = $data->result_array();
            $output['user_id'] =$usrid; 
            $output['folder'] =$result; 
            $output['parent_id'] =$Parentid; 
            foreach ($output['folder'] as $k => $folder) { 
                $result1 = $this->getallRequestDefaultfiles($folder['folder_name'],$usrid);
                $count[] =!empty($result1['image'])?count($result1['image']):count($result1['image'] = array()); 
           } 
            $output['count'] =  $count;  
            return $output;
        }
    

    public function getfilesbyfolderid($id = "", $folderid = "") {
        $this->db->select("*");
        if ($folderid == "") {
            $this->db->where("id", $id);
        } else {
            $this->db->where("folder_id", $folderid);
        }
        $this->db->from("s_folder_file_structure");
        $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
        return $result;
    }

    public function getfolderbyfolderid($id = "") {
        $this->db->select("*");
        $this->db->where("id", $id);
        $this->db->from("s_folder_structure");
        $data = $this->db->get();
        //echo $this->db->last_query();
        $result = $data->result_array();
        return $result;
    }

    public function getallRequestDefaultfiles($title=null,$user_id=null)
        {
           $login_user_id=$this->load->get_var('login_user_id');
           $sub_usersbrands=$this->load->get_var('sub_usersbrands');
           $canbrand_profile_access=$this->myfunctions->isUserPermission('brand_profile_access');
           if($title == "Approved"){
            $condArr = array('rf.status' =>'Approve');
           }elseif($title == "Rejected"){
            $condArr  = array('rq.status' => 'approved','rf.status' =>'customerreview');
           }elseif($title == "Pending review"){
            $condArr  = array('rf.status' => 'customerreview');
           }elseif($title == "Un-Approved"){
            // $condArr  ="rf.status != 'approve'";
            $condArr  = array('rq.status' => 'approved','rf.status' =>'customerreview');
           }
           else {
            $condArr = array('rf.mark_as_fav' =>'1');
           }
            $this->db->select("rq.id as request_id,rq.title as name,rf.user_id as user_id,rf.file_name,rf.status as status,rf.id as project_id,rf.mark_as_fav as favStatus,'projects' as from_data");
            $this->db->where("rq.customer_id", $user_id);
            $this->db->where($condArr);
            $this->db->join('request_files as rf', 'rf.request_id = rq.id', 'left');
            
            if (!empty($sub_usersbrands)) {
            $this->db->where("((rq.brand_id IN (" . implode(',', $sub_usersbrands) . ")) or (rq.brand_id = 0 and rq.created_by='" . $login_user_id . "'))");
            } elseif (empty($sub_usersbrands) && $canbrand_profile_access == 0) {
                $this->db->where("rq.created_by = '" . $login_user_id . "'");
                $this->db->order_by("rq.sub_user_priority", "ASC");
            }
            $this->db->from("s_requests as rq");
            $data = $this->db->get();
//            echo $this->db->last_query();exit;
            $result = $data->result_array();
            if($title == "Favorite"){
                $customfiles = $this->getfavfilefromfolders();
                $brands = $this->getfavfilefrombrands();
                $output['image'] = array_merge($result,$customfiles, $brands);
                $output['count'] = sizeof($result)+sizeof($customfiles)+sizeof($brands);
            }else{
                $output['count'] = !empty($result)? count($result) : count($result = array()); 
                $output['image'] = $result; 
            }
            return $output;
        }

        public function CopyFolder($folder_id,$req_id,$file_id,$file_name){
            $data = array(
                    'folder_id' => $folder_id,
                    'request_id' => $req_id, 
                    'file_id' =>$file_id, 
                    'file_name' =>$file_name
                    );
            $this->db->select("*");
            $this->db->from("s_folder_file_structure");
            $this->db->where($data);
            $sql =  $this->db->get(); 
            $result = $sql->num_rows();
            if($result == 0){
                $this->db->insert('s_folder_file_structure',$data);
               $afftectedRows = $this->db->affected_rows();
               return $afftectedRows;
            }else{
                return 0; 
            }
           
        }

    /* 29-09-019*/

        public function checkForFavOrUnFav($projectId,$requestedID,$filename,$role){
           $this->db->select('mark_as_fav');
           if($role !="brand"){
                
           if($projectId !="0" && $requestedID !="0"){
               $this->db->from('request_files');
               $condArr = array('id'=>$projectId,'request_id'=>$requestedID);
           }else{
               $this->db->from("s_folder_file_structure");
                $condArr = array('file_id'=>$projectId,'request_id'=>$requestedID,'file_name'=>$filename);
           }
           }
           else{
            $this->db->from("brand_profile_material");
            $condArr = array('brand_id'=>$requestedID,'id'=>$projectId);
           }
           
           $this->db->where($condArr);
            
           $sql= $this->db->get(); 
            //echo $this->db->last_query();exit;
           $reslt = $sql->result();
           
           return $reslt[0]->mark_as_fav; 
        }
        
         public function rename_folder($foldID,$userid,$filename){
            $conArr =  array('id' =>$foldID,'user_id'=>$userid);
            $this->db->set('folder_name',$filename);
            $this->db->where($conArr);
            $this->db->update('s_folder_structure');
            $afftectedRows = $this->db->affected_rows();
            return $afftectedRows;
        }
        
        
     public function checkingForChildOrSubchild($foldID){    
            $reslt = array();
            $reslt = $this->check_sub_childs_under_customFolder($foldID);
            if(!empty($reslt)){
                 
                $parent_child_listing =  array_unique($reslt['parent_key']);
                $child_listing =  array_unique($reslt['child_key']);
                $folder_name =  array_unique($reslt['folder_name']);
      
                $sub_childs =array();
                foreach($child_listing as $ky => $Child){
             
                $return = $this->check_sub_childs_under_customFolder($Child);
                
                    if(!empty($return['child_key'])){
                        $sub_childs[] = $return['child_key']; 
                    }else if(!empty($child_listing[$ky])) {
                       $sub_childs[] =$child_listing[$ky]; 
                    }else{
                        $sub_childs[] ="no sub child"; 
                    }
//                      
                    $folder_nameARr[] = $folder_name[$ky];
                    }
                      
                        $folder_name = implode(',', $folder_nameARr);
                        echo $folder_name; die; 
            }
                
            }
        
        public function check_sub_childs_under_customFolder($id,$confirm=""){  
           
            $conArr =  array('parent_folder_id' => $id);
            $this->db->select("id,folder_name");
            $this->db->from("s_folder_structure");
            $this->db->where($conArr);
            $sql= $this->db->get();          
            $reslt = $sql->result_array(); 
            $t = [];
            $return_res = array();
             if(!empty($reslt) && count($reslt) > 0){
             foreach($reslt as $k => $cid) { 
                
                 $this->check_sub_childs_under_customFolder($cid["id"]); 
                 $group['child']= $reslt[$k];
                 $t[][$id] =$group; 
                   
                 
              }
             }
             $output =array();
              foreach ($t as $k => $val){
                   foreach ($val as $ka => $valS){
                        
                       if(!empty($val)){
                         $output["parent_key"][] = $ka;
                         $output["child_key"][]= $valS['child']['id'];
                         $output["folder_name"][] = $valS['child']['folder_name'];
                          
                       }     
                   }
            }
            
              if($confirm == 1){
                         if(!empty($output["parent_key"]) && !empty($output["child_key"])){
                            $res = $this->deleteFolders($output["parent_key"],$output["child_key"]);
                       echo "1"; die; 
                            
                         }else{
                            $res = $this->deleteFolders($id); 
                            
                            echo "1"; die; 
                         }
                       
                    }
              if(!empty($output)){
                 return $output; 
              } 
            
        }  
        
        
       public function deleteFolders($parent="",$child="")
        {
      
            if(!empty($child)){
             for ($i = 0; $i <= count($child); $i++)
               {
                $this->db->where_in('id',$child[$i]);
                $this->db->delete('s_folder_structure');
                $afftectedRows = $this->db->affected_rows();
               }
                 if($afftectedRows == 1 || !empty($parent)){
                     $this->db->where_in('id',$parent);
                     $this->db->delete('s_folder_structure');
                     $Compleated = $this->db->affected_rows();
                 }
            }
            if(!empty($parent)){
                $this->db->where_in('id',$parent);
                $this->db->delete('s_folder_structure');
                $Compleated = $this->db->affected_rows();
            }
            return $Compleated;
        }
        
        public function get_brand_profile_by_user_id($id,$created_by = "",$count="",$brand_permission = "") {
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        if(!empty($sub_usersbrands)){
            $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        }else{
            $sub_usersbrands = array("0"=>"0"); 
        }   
        $login_user_id = $this->load->get_var('login_user_id');
        $this->db->select("*");
        if($brand_permission != ""){
           if($canbrand_profile_access == 0){
             $this->db->where("((id IN (".implode(',',$sub_usersbrands).")) or (created_by='".$login_user_id."'))");
            }else{
                $this->db->where("customer_id = '" . $id . "'");
            } 
        }else{
            if($created_by != ""){
                $this->db->where("created_by = '" . $id . "'");  
            }else{
                $this->db->where("customer_id = '" . $id . "'");
            }
        }
         $this->db->where("is_saas = '1'");
        if($count != ""){
        $result = $this->db->count_all_results('brand_profile');
        }else{
         $data = $this->db->get("brand_profile"); 
         $result = $data->result_array();
        }
       // echo $this->db->last_query();exit; 
        $count =array(); 
        foreach ($result as $k => $folder) { 
         $result1 = $this->get_brandprofile_materials_files($folder['id']);
//         echo "<pre>"; print_r($result1); 
             $count[] = $result1['count_files'];
        } 
        $result['count'] =  $count; 
        return $result;   
    } 
    
    public function get_brandprofile_materials_files($brand_id,$title="") {
        $this->db->select("*");
        $this->db->where("brand_id = $brand_id ");
        $this->db->order_by("file_type","ASC");
        $data = $this->db->get("brand_profile_material");
//        echo $this->db->last_query();exit;
        $result['files']= $data->result_array();
        $result['count_files'] = !empty($result)? count($result['files']) : count($result = array()); 
        if (empty($result)) {
            return array();
        }
   
        return $result;
    }
 
    
    
    public function get_custom_files($self_id){
            $login_user_id=$this->load->get_var('login_user_id');
            $this->db->select("ffs.id as id,ffs.folder_id as folder_id,ffs.file_id as file_id,ffs.file_name as img_name,ffs.mark_as_fav as markasfav,ffs.request_id as reqid,rf.file_name as image,rf.status as status,rf.mark_as_fav as mark_as_fav");
            $this->db->join('request_files as rf', 'rf.id = ffs.file_id', 'LEFT');
            //$this->db->join('s_folder_structure as fs', 'fs.id = ffs.folder_id', 'LEFT');
            $cond = array("ffs.folder_id" => $self_id,"rf.user_id"=>$login_user_id);
//            "fs.user_id" =>$login_user_id
            $this->db->where($cond);
            $this->db->from("s_folder_file_structure as ffs");
            $data = $this->db->get();
           // echo $this->db->last_query();exit;
            $result1 = $data->result_array();
//            $output['folder'] = $result;
            $output['files'] = $result1;
            return $output; 
    }
    
    public function deletefilesfromfolder($id) {
        $this->db->where('id', $id);
        $result = $this->db->delete('s_folder_file_structure');
        return $result;
    }
    
    public function getfavfilefromfolders() {
        $login_user_id=$this->load->get_var('login_user_id');
        $this->db->select("ffs.folder_id,ffs.request_id,ffs.file_id,ffs.file_name,ffs.mark_as_fav,'custom' as from_data");
        $this->db->where('mark_as_fav = 1 AND fs.user_id="'.$login_user_id.'"' );
        $this->db->join('s_folder_structure as fs', 'fs.id = ffs.folder_id', 'left');
        $this->db->from("s_folder_file_structure as ffs");
        $result = $this->db->get();
       // echo $this->db->last_query();exit;
        $data = $result->result_array();
        return $data;
    }
//    public function getfavfilefrombrands() {
//        $login_user_data=$this->load->get_var('login_user_data');
//        
//        $this->db->select("bpm.brand_id,bpm.filename,bpm.mark_as_fav,'brands' as from_data");
//        if($login_user_data[0]['parent_id'] != 0){
//            $this->db->where('mark_as_fav = 1 AND bp.customer_id="'.$login_user_data[0]['parent_id'].'" AND bp.created_by = "'.$login_user_data[0]['id'].'"');
//        }else{
//          $this->db->where('mark_as_fav = 1 AND bp.customer_id="'.$login_user_data[0]['id'].'"');
//        }
//        $this->db->join('brand_profile as bp', 'bp.id = bpm.brand_id', 'left');
//        $this->db->from("brand_profile_material as bpm");
//        $result = $this->db->get();
//        //echo $this->db->last_query();exit;
//        $data = $result->result_array();
//        return $data;
//    }
    
    public function getfavfilefrombrands() {
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        if(!empty($sub_usersbrands)){
            $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        }else{
            $sub_usersbrands = array("0"=>"0"); 
        }   
        $login_user_data = $this->load->get_var('login_user_data');
        
        $this->db->select("bpm.brand_id,bpm.filename,bpm.mark_as_fav,'brands' as from_data");
        if($login_user_data[0]['parent_id'] != 0 && $login_user_data[0]['user_flag'] == 'client'){
            $this->db->where('mark_as_fav = 1 AND bp.customer_id="'.$login_user_data[0]['parent_id'].'" AND bp.created_by = "'.$login_user_data[0]['id'].'"');
        }else{
            if ($canbrand_profile_access == 0) {
                $this->db->where("bpm.mark_as_fav = 1 AND ((bpm.brand_id IN (" . implode(',', $sub_usersbrands) . ")) or (bp.created_by='" . $login_user_data[0]['id'] . "'))");
            } else {
                $this->db->where("bpm.mark_as_fav = 1 AND bp.customer_id = '" . $login_user_data[0]['id'] . "'");
            }
        }
        
        $this->db->join('brand_profile as bp', 'bp.id = bpm.brand_id', 'left');
        $this->db->from("brand_profile_material as bpm");
        $result = $this->db->get();
        //echo $this->db->last_query();exit;
        $data = $result->result_array();
        return $data;
    }
    
    public function get_brandprofile_materials_files_fmgt($brand_id,$title=""){
        $this->db->select("*");
        $this->db->where("brand_id = $brand_id ");
        $this->db->order_by("file_type","ASC");
        $data = $this->db->get("brand_profile_material");
       // echo $this->db->last_query();exit;
        $result['files']= $data->result_array();
        $result['count_files'] = !empty($result)? count($result['files']) : count($result = array()); 
        if (empty($result)) {
            return array();
        }       
        return $result;
}
 /* update work on filemanagement 26-sep-19 */
      public function search_fmModel($serachKeyWord){
        $login_user_id = $this->load->get_var('login_user_id');
        $reult =array(); 
        if(empty($serachKeyWord)){
         return array();
        }else{
           $reult["folderName"] =$this->search_for_folders($login_user_id,$serachKeyWord);
           $reult["fileName_ffs"] =$this->search_for_ffs_files($login_user_id,$serachKeyWord);
           $reult["fileName"] =$this->search_for_files($login_user_id,$serachKeyWord);
        }
        return $reult;  
      }
      public function search_for_folders($login_user_id,$serachKeyWord){
        $this->db->select("fs.folder_name as folder_name,fs.parent_folder_id as parent_id,fs.id as id, fs.folder_type as type");
        $this->db->like('fs.folder_name',$serachKeyWord,"both");
        $cond =  "(( fs.user_id='".$login_user_id."' AND fs.parent_folder_id='0') OR fs.folder_type='default')";
        $this->db->where($cond);
        $this->db->from("s_folder_structure as fs");
        $this->db->limit(5);
        $this->db->order_by("fs.id", "DESC");
        $sql= $this->db->get();
        //echo $this->db->last_query();exit;
        $reslt = $sql->result_array(); 
        return $reslt; 
      }
      public function search_for_files($login_user_id,$serachKeyWord){
          
        $login_user_id=$this->load->get_var('login_user_id');
        $sub_usersbrands=$this->load->get_var('sub_usersbrands');
        $canbrand_profile_access=$this->myfunctions->isUserPermission('brand_profile_access');
           if($title == "Approved"){
                    $condArr = array('rf.status' =>'Approve');
               }elseif($title == "Rejected"){
                    $condArr  = array('rq.status' => 'approved','rf.status' =>'customerreview');
               }elseif($title == "Pending review"){
                    $condArr  = array('rf.status' => 'customerreview');
               }elseif($title == "Un-Approved"){
                    $condArr  = array('rq.status' => 'approved','rf.status' =>'customerreview');
               }
               else {
                   // $condArr = array('rf.mark_as_fav' =>'1');
                    $condArr = array();
               }
            $this->db->select("rq.id as request_id,rq.title as name,rf.user_id as user_id,rf.file_name,rf.status as status,rf.id as project_id,rf.mark_as_fav as favStatus");
            $this->db->like('file_name',$serachKeyWord,"both");
            $this->db->where("rq.customer_id", $login_user_id);
            $this->db->where($condArr);
            $this->db->join('request_files as rf', 'rf.request_id = rq.id', 'left');
            if (!empty($sub_usersbrands)) {
            $this->db->where("((rq.brand_id IN (" . implode(',', $sub_usersbrands) . ")) or (rq.brand_id = 0 and rq.created_by='" . $login_user_id . "'))");
            } elseif (empty($sub_usersbrands) && $canbrand_profile_access == 0) {
                $this->db->where("rq.created_by = '" . $login_user_id . "'");
                $this->db->order_by("rq.sub_user_priority", "ASC");
            }
            $this->db->from("s_requests as rq");
            $this->db->limit(5);
            $this->db->order_by("rq.id", "DESC");
            $data = $this->db->get();
            //echo $this->db->last_query();exit;
            $result = $data->result_array();
            return $result;
      }
      
      public function SearchforProJectFolder($login_user_id,$serachKeyWord){
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $this->db->select("rq.id as request_id,rq.title as folder_name,rq.status as status");
        //$this->db->join('request_files as rf', 'rf.request_id = rq.id', 'INNER');
        $this->db->like('rq.title',$serachKeyWord,"both");
        $this->db->where("rq.customer_id", $login_user_id);
        if(!empty($sub_usersbrands)){
            $this->db->where("((rq.brand_id IN (".implode(',',$sub_usersbrands).")) or (rq.brand_id = 0 and rq.created_by='".$login_user_id."'))");
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
            $this->db->where("rq.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("rq.sub_user_priority", "ASC");
        }
        //$this->db->group_by('rq.id');
        $this->db->from("s_requests as rq");
        $this->db->limit(5);
        $data = $this->db->get();
        //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result; 
      }
      
      
      public function search_for_ffs_files($login_user_id,$serachKeyWord){
           
        //$this->db->select("ffs.id as id,ffs.request_id as reqid, ffs.file_name as file_name,fs.folder_name as folder_name,fs.parent_folder_id as parent_id");
        $this->db->select("fs.folder_name as folder_name,fs.parent_folder_id as parent_id,fs.id as id, ffs.file_name as file_name,ffs.request_id as reqid");
        $this->db->join('s_folder_structure as fs', 'fs.id = ffs.folder_id', 'LEFT');
        $this->db->like('ffs.file_name',$serachKeyWord,"both");
        $cond = array("fs.user_id" =>$login_user_id,"fs.parent_folder_id" => "0");
        $this->db->where($cond);
        $this->db->from("s_folder_file_structure as ffs");
        $this->db->limit(5);
        $this->db->order_by("ffs.id", "DESC");
        $sql= $this->db->get();
        //echo $this->db->last_query();exit;
        $reslt = $sql->result_array(); 
        return $reslt; 
      }
      
      public function  count_files_underFolder($folderid){
        $login_user_id=$this->load->get_var('login_user_id');
        $this->db->select("fs.folder_name as folder_name,fs.parent_folder_id as parent_id,fs.id as id, ffs.file_name as file_name");
        $this->db->join('s_folder_structure as fs', 'fs.id = ffs.folder_id', 'LEFT');
        $cond = array("fs.user_id" =>$login_user_id,"ffs.folder_id" => $folderid);
        $this->db->where($cond);
        $this->db->from("s_folder_file_structure as ffs");
         $sql= $this->db->get();
//     echo $this->db->last_query();exit;
        $reslt = $sql->result_array(); 
        $countFiles =  count($reslt);
        return $countFiles; 
      }
}
