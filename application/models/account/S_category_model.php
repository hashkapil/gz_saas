<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class S_category_model extends CI_Model {	
    
	function get_categories($parent_id = null,$active = null){
                $login_user_id = $this->load->get_var('login_user_id');
                $profile_data = $this->Admin_model->getuser_data($login_user_id);
//                echo "<pre/>";print_R($profile_data);exit;
                if(!in_array($profile_data[0]['plan_name'],AGENCY_USERS) && $profile_data[0]['role'] != 'admin'){
                  $agency_only = '0';  
                }else{
                   $agency_only = '';   
                }
		$this->db->select("*");
		if($active !== null){
			$this->db->where("is_active",$active);
		}
		if($parent_id !== null){
			$this->db->where("parent_id",$parent_id);
		}
                if($agency_only == '0'){
                $this->db->where("agency_only",$agency_only);
                }
		$this->db->order_by('parent_id', 'DESC');
		$this->db->order_by('position', 'ASC');
		$data = $this->db->get("request_categories");
//                echo $this->db->last_query();exit;
		$result = $data->result_array();
		
		if(!empty($result)){
                    for ($j = 0; $j < count($result); $j++) {
                        if($result[$j]['parent_id'] == "0"){
                                $result[$j]['child'] = $this->get_categories($result[$j]['id'],$active);
                        }
                    }
		}
		return $result;
	}
	
	function get_buckets(){
		$this->db->select("*");
		$data = $this->db->get("request_buckets");
		$result = $data->result_array();
		return $result;
	}
        
        function get_category_byID($id=null){
            $this->db->select("*");
            $this->db->where('id', $id);
            $data = $this->db->get("request_categories");
//             echo $this->db->last_query();exit;
            $result = $data->result_array();
            return $result;
        }
        
        function getAllrequestsBysubcatID($id){
            $this->db->select("r.id as request_id,r.latest_update as latest_update,u.timezone,r.category_bucket as bucket_type,u.plan_name,rc.timeline as timeline");
            $this->db->where('subcategory_id',$id);
            $this->db->join('users as u', 'r.customer_id = u.id','left');
            $this->db->join('request_categories as rc', 'rc.id = r.subcategory_id','left');
            $data = $this->db->get("requests as r");
            //echo $this->db->last_query();exit; 
            $result = $data->result_array();
            return $result;
        }
        /**questions start******/
        function getallQuestions($catid = NULL,$subcatid = NULL,$role = NULL,$allCountOnly = false,$start = 0,$limit = 0,$search="",$is_default=false){
            $this->db->select("rq.*");
            if($catid){
                $this->db->where('rq.category_id', $catid);
            }
            if($subcatid){
               $this->db->where('rq.subcategory_id', $subcatid); 
            }
            $this->db->join('request_categories as rc', 'rc.id = rq.category_id','left');
            if($role != 'admin'){
            $this->db->where('rq.is_active', '1'); 
            }
            $this->db->where('rq.is_deleted', 0);
            if($is_default){
              $this->db->where('rq.is_default',1); 
            }
            $this->db->order_by('rq.id', 'DESC');
            if($search){
                $this->db->group_start();
                $this->db->like('rq.question_label', $search, 'both');
                $this->db->group_end();
            }
            if(!$allCountOnly && !$search){
                $limit = ($limit == 0) ? LIMIT_ADMIN_LIST_COUNT : $limit;
                $this->db->limit($limit, $start);
            }
            $data = $this->db->get("request_questions as rq");
            //echo $this->db->last_query();exit;
            $result = $data->result_array();
            if($result){
                if($allCountOnly){
                   return count($result); 
                } else{
                    return $result;
                }
            }
        }

        function getcat_subcatName($catid = NULL,$subcatid = NULL){
           $this->db->select("name"); 
           if($catid && $subcatid == 0){
                $this->db->where('id', $catid);
                $this->db->where('parent_id', '0');
            }elseif($catid && $subcatid){
               $this->db->where('id', $subcatid); 
               $this->db->where('parent_id', $catid);
            }
            $data = $this->db->get("request_categories");
//             echo $this->db->last_query();
            $result = $data->result_array();
            return $result[0]['name'];
        }
        
        function deleteQuestions($table,$id){
            $this->db->where('id',$id);
            $this->db->set('is_deleted', 1);
            $data = $this->db->update($table); 
//           echo  $this->db->last_query(); exit;
            return $data;
	}
        
        function getQuestdata($quest_id){
            $this->db->select("*");
            $this->db->where('id', $quest_id); 
//            $this->db->where('is_active', '1'); 
            $this->db->where('is_deleted', 0);
            $data = $this->db->get("request_questions");
//            echo $this->db->last_query();exit;
            $result = $data->result_array();
//            echo "<pre/>";print_r($result);exit;
            return $result;
        }
        
        function get_quest_Ans($reqid,$qus_id = NULL){
            $this->load->model('S_request_model');
            $from_table = $this->S_request_model->getDataBasedonSAAS($reqid);
            $this->db->select("acm.request_id,acm.question_id,cbq.question_label,acm.answer");
            if($qus_id){
                $this->db->where('acm.request_id', $reqid);
                $this->db->where('acm.question_id', $qus_id);
            }else{
                $this->db->where('acm.request_id', $reqid);
            }
            if($from_table == 'requests'){
            $this->db->join('requests as rq', 'rq.id = acm.request_id','left');
            }else{
             $this->db->join('s_requests as rq', 'rq.id = acm.request_id','left');   
            }
            $this->db->where('acm.is_deleted', 0);
            $this->db->join('request_questions as cbq', 'cbq.id = acm.question_id','left');
             if($from_table == 'requests'){
            $data = $this->db->get("requests_meta as acm");
             }else{
            $data = $this->db->get("s_requests_meta as acm");  
             }
//            echo $this->db->last_query();
            $result = $data->result_array();
//            echo "<pre/>";print_r($result);
            return $result;
        }
        
         function getMetasbyReqID($reqid,$qus_id = NULL){
            $this->load->model('S_request_model');
            $from_table = $this->S_request_model->getDataBasedonSAAS($reqid);
            $this->db->select("id");
            $this->db->where('is_deleted', 0);
            $this->db->where('request_id', $reqid);
            if($qus_id){
            $this->db->where('question_id', $qus_id);
            }
            if($from_table == 'requests'){
            $data = $this->db->get("requests_meta");
            }else{
            $data = $this->db->get("s_requests_meta");    
            }
//            echo $this->db->last_query()."<br/>";
            $result = $data->result_array();
//            echo "<pre/>";print_R($result);
            if(!empty($result)){
                return $result;
            }
        }
        
        /*******sample work*****/
        
        function getSampledata($sample_id){
            $this->db->select("*");
            $this->db->where('id', $sample_id); 
//            $this->db->where('is_active', '1'); 
            $this->db->where('is_deleted', 0);
            $data = $this->db->get("sample_data");
//            echo $this->db->last_query();exit;
            $result = $data->result_array();
//            echo "<pre/>";print_r($result);exit;
            return $result;
        }
        
        function getallSampledata($catid = NULL,$subcatid = NULL,$role = NULL,$allCountOnly = false,$start = 0,$limit = 0,$search=""){
            $this->db->select("sd.*");
            if($catid){
                $this->db->where('sd.category_id', $catid);
            }
            if($subcatid){
               $this->db->where('sd.subcategory_id', $subcatid); 
            }
            $this->db->join('request_categories as rq', 'rq.id = sd.category_id','left');
            if($role != 'admin'){
            $this->db->where('sd.is_active', '1'); 
            }
            $this->db->where('sd.is_deleted', 0);
            $this->db->order_by('sd.id', 'DESC');
            $this->db->group_by('sd.id');
            if($search){
                $this->db->group_start();
                $this->db->like('sd.name', $search, 'both');
                $this->db->group_end();
            }
            if(!$allCountOnly && !$search){
                $limit = ($limit == 0) ? LIMIT_ADMIN_LIST_COUNT : $limit;
                $this->db->limit($limit, $start);
            }
            $data = $this->db->get("sample_data as sd");
//            echo $this->db->last_query();exit;
            $result = $data->result_array();
            if($result){
                if($allCountOnly){
                   return count($result); 
                } else{
                    return $result;
                }
            }
        }
        
        function getSamplematerialdata($sampleid){
            $this->db->select("*");
            $this->db->where('sample_id', $sampleid); 
//            $this->db->where('is_active', '1'); 
//            $this->db->where('is_deleted', 0);
            $data = $this->db->get("sample_material_data");
//            echo $this->db->last_query();exit;
            $result = $data->result_array();
//            echo "<pre/>";print_r($result);exit;
            return $result;
        }
        
        function getSampleMaterialsbycat_subcat($catid,$subcatid = NULL,$minmax = false){
            if(!$minmax){
                $this->db->select("sd.id as sample_id,smd.id as material_id,sd.category_id,sd.subcategory_id,sd.minimum_sample,sd.maximum_sample,smd.sample_material");
            }else{
                $this->db->select("sd.minimum_sample,sd.maximum_sample");
            }
            $this->db->where('category_id', $catid); 
            $this->db->where('subcategory_id', $subcatid); 
            $this->db->join('sample_material_data as smd', 'smd.sample_id = sd.id','left');
            $this->db->where('is_active', '1'); 
//            $this->db->order_by('sd.position', 'ASC');
//            $this->db->where('is_deleted', 0);
            $data = $this->db->get("sample_data as sd");
//            echo $this->db->last_query();exit;
            $result = $data->result_array();
//            echo "<pre/>";print_r($result);exit;
            return $result;
            
        }
        
}