<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class S_admin_model extends CI_Model {
    	
	public function get_today_customer() 
	{      
		$this->db->where('Date(created)', date("Y-m-d"));
		$data = $this->db->get('users');
		$result = $data->result_array();
		return $result;
	}
	
	public function insert_data($table,$value)
	{
		$data = $this->db->insert($table,$value);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
	
	public function delete_data($table,$value)
	{
		$this -> db -> where('id', $value);
		$result = $this -> db -> delete($table);
		return $result;
	}

	public function delete_request($table,$where)
	{
		$this->db->where($where);
		$result = $this->db->delete($table);
		return $result;
	}
	public function alldelete_request($table,$where)
	{
		$this->db->where_in('id',$where);
		$result = $this->db->delete($table);
		return $result;
	}
	public function delete_request_data($table,$value=array())
	{
		$this->db->where_in('request_id',$value);
		$result = $this->db->delete($table);
		return $result;
	}
	public function delete_request_file_data($table,$value)
	{
		$this->db->where_in('request_file_id',$value);
		$result = $this->db->delete($table);
		return $result;
	}
	public function delete_file_chat($id){
		$this->db->where('id', $id);
	   $result = $this->db->delete('request_file_chat'); 
	   return $result;
	}
	public function delete_blog($table,$id)
	{
	   $this->db->where('id', $id);
	   $result = $this->db->delete($table); 
	   return $result;
	}
	public function update_plan($table,$data,$where)
	{
		$this->db->where('id',$where);
		$this->db->set($data);
		$data = $this->db->update($table); 
		$this->db->last_query();
		return $data;
	}	
	public function update_data($table,$data,$where)
	{
		foreach($where as $key=>$value){
			$this->db->where($key,$value);
		}		
		//$this->db->set($data);
		$data = $this->db->update($table, $data); 
		//echo $this->db->last_query();exit;	
		return $data;
		 
	}
	public function get_blog_by_id($id){
		$this->db->select('*');
		$this->db->from("blog");
		$query = $this->db->get();
		$row= $query->result_array();
		return $row;
	}
	public function get_portfolio_by_id($id){
		$this->db->select('*');
		$this->db->from("protfolio");
		$query = $this->db->get();
		$row= $query->result_array();
		return $row;
	}
	
	public function get_total_customer($role=null,$designer_id=null) 
	{      
		if($role){
			$this->db->where('role', $role);
                        if($role == "designer"){
                            $this->db->where('is_active', '1');
                        }
		}
		if($designer_id){
			$this->db->where('designer_id',$designer_id);
		}
		$data = $this->db->get('s_users');
//                echo $this->db->last_query();exit;
		$result = $data->result_array();
		for($i=0;$i<sizeof($result);$i++){
			$designer = $this->getuser_data($result[$i]['designer_id']);
			if(!empty($designer)){
				$result[$i]['designer'] = $designer[0];
			}else{
				$result[$i]['designer'] = "";
			}
		}
		return $result;
	}
	
	public function get_today_request() 
	{      
		$this->db->where('Date(created)', date("Y-m-d"));
		$data = $this->db->get('requests');
		$result = $data->result_array();
		return $result;
	}
	
	public function get_total_request() 
	{      
		$data = $this->db->get('requests');
		$result = $data->result_array();
		return $result;
	}
//	public function get_all_request_count($status = array(),$customer_id,$createdby = "",$mainuser = ""){
//		$this->db->select("*");
//		$this->db->where_in('status', $status);
//		if($createdby == ""){
//                    if($customer_id != ""){
//                            $this->db->where('customer_id', $customer_id);
//                    }
//                }else if($createdby == "main"){
//                    $this->db->where("(customer_id = ".$mainuser.") AND (created_by IN (".implode(',',$customer_id).") or created_by = 0)");
//                }else{
//                    $this->db->where('created_by', $customer_id);
//                }
//		$query = $this->db->get('requests');
//		$result = $query->result_array();
//		//echo $this->db->last_query();
//		//exit;
//		return $result;
//	}
        
        public function get_all_request_count($status = array(), $customer_id, $createdby = "",$mainuser = ""){
		$this->db->select("*");
		$this->db->where_in('status', $status);
		if($createdby == ""){
                    if($customer_id != ""){
                            $this->db->where('customer_id', $customer_id);
                    }
                }else if($createdby == "main"){
                    $this->db->where("(customer_id = ".$mainuser.") AND (created_by IN (".implode(',',$customer_id).") or created_by = 0)");
                }else{
                    $this->db->where_in("created_by", $customer_id);
                }
                $this->db->order_by("priority","ASC");
		$query = $this->db->get('requests');
		$result = $query->result_array();
		return $result;
	}
	public function count_active_project($status=array(),$customer_id=null,$not_approve=null,$request_id=null,$designer_id=null,$status_which=null,$orderby="")
	{	
		$this->db->select('*');
		if(!empty($status)){
			if($status_which == ""){
				$this->db->where_in('status', $status);
			}else{
				$this->db->where_in($status_which, $status);
			}
		}
		if($customer_id != ""){
			$this->db->where('customer_id', $customer_id);
		}
		if($designer_id != ""){
			$this->db->where('designer_id', $designer_id);
		}
		if($not_approve=="no"){
			$this->db->where('status!="approved"');
		}
		if($request_id != ""){
			$this->db->where('id="'.$request_id.'"');
		}
		$num_rows = $this->db->count_all_results('requests');
		return $num_rows;
	}
	public function get_all_requested_designs($status=array(),$customer_id=null,$not_approve=null,$request_id=null,$designer_id=null,$status_which=null,$orderby="")
	{
		$this->db->select('*');
		if(!empty($status)){
                   // echo "<pre/>";print_r($status);
//                    if($status[0] == 'active'){
//                            $this->db->order_by("latest_update","ASC");
//                        }
//                        else{
//                           // $this->db->order_by("Date(created)","DESC");
//                        }
//			if($status_which == ""){
//				$this->db->where_in('status', $status);
//			}else{
//				$this->db->where_in($status_which, $status);
//			}
                    if($status[0] == 'active' || $status[1] == 'disapprove'){
                        $this->db->order_by("latest_update","ASC");
                    }elseif($status[0] == 'assign' || $status[1] == 'pending'){
                        $this->db->order_by("priority","ASC");
                    }elseif ($status[0] == "pendingrevision") {
                        $this->db->order_by("modified", "ASC");
                    } elseif ($status[0] == 'checkforapprove') {
                        $this->db->order_by("modified","ASC");
                    }
                   elseif ($status[0] == "approved") {
                   $this->db->order_by("approvaldate", "DESC");
                 } else {
                        $this->db->order_by("modified", "DESC");
                    }
                    if($status_which == ""){
				$this->db->where_in('status', $status);
			}else{
				$this->db->where_in($status_which, $status);
			}
		}
		if($customer_id != ""){
			$this->db->where('customer_id', $customer_id);
		}
		if($designer_id != ""){
			$this->db->where('designer_id', $designer_id);
		}
		if($not_approve=="no"){
			$this->db->where('status!="approved"');
		}
		if($request_id != ""){
			$this->db->where('id="'.$request_id.'"');
		}
                
		$this->db->limit(6, 0);		
		$data = $this->db->get('requests');
               //echo $this->db->last_query();
		$data = $data->result_array();
		for($i=0;$i<sizeof($data);$i++){
			$customer = $this->getuser_data($data[$i]['customer_id']);
			if(!empty($customer)){
				$data[$i]['customer'] = $customer;
			}else{
				$data[$i]['customer'] = "";
			}
			$designer = $this->getuser_data($data[$i]['designer_id']);
			if(!empty($designer)){
				$data[$i]['designer'] = $designer;
			}else{
				$data[$i]['designer'] = "";
			}
		}
		return $data;
		
	}
	public function get_all_requested_designs_scroll($status,$customer_id=null,$not_approve=null,$request_id=null,$designer_id=null,$status_which=null,$orderby="",$start,$limit)
	{
		$this->db->select('*');
		if(!empty($status)){
			if(!empty($status)){
			if($status_which == ""){
				$this->db->where_in('status', $status);
			}else{
				$this->db->where_in($status_which, $status);
			}
		       }
                        if($status[0] == 'assign'){
                            $this->db->order_by("priority","ASC");
                        }elseif($status[0] == 'active'){
                            $this->db->order_by("latest_update","ASC");
                        }elseif($status[0] == 'pendingrevision'){
                            $this->db->order_by("modified", "ASC");
                        }elseif($status[0] == 'checkforapprove'){
                            $this->db->order_by("modified", "ASC");
                        }elseif ($status[0] == "approved") {
                           $this->db->order_by("approvaldate", "DESC"); 
                        }
                        else{
                            $this->db->order_by("DATE(latest_update)", "ASC");
                        }
		}
		if($customer_id != ""){
			$this->db->where('customer_id', $customer_id);
		}
		if($designer_id != ""){
			$this->db->where('designer_id', $designer_id);
		}
		if($not_approve=="no"){
			$this->db->where('status!="approved"');
		}
		if($request_id != ""){
			$this->db->where('id="'.$request_id.'"');
		}
                $this->db->group_by('id');
		$this->db->limit($limit, $start);		
		$data = $this->db->get('requests');
               //echo $this->db->last_query();
               // print_r($data);
		$dataa = $data->result_array();
                
		for($i=0;$i<sizeof($dataa);$i++){
			$customer = $this->getuser_data($dataa[$i]['customer_id']);
                        //echo "<pre>";print_r($dataa);
			if(!empty($customer)){
				$dataa[$i]['customer'] = $customer;
			}else{
				$dataa[$i]['customer'] = "";
			}
			$designer = $this->getuser_data($dataa[$i]['designer_id']);
			if(!empty($designer)){
				$dataa[$i]['designer'] = $designer;
			}else{
				$dataa[$i]['designer'] = "";
			}
		}
		return $dataa;
		
	}
	
	
        
        public function getextrauser_data($id)
	{       
                $this->db->select("id,bypass_payment");
		$this->db->where('user_id', $id);
		$data = $this->db->get('users_info');
		$result = $data->result_array();
//                echo $this->db->last_query();
		return $result;
	}
        
        public function get_timezone()
	{
		$this->db->select('zone_name');
                $this->db->order_by("zone_name","ASC");
		$data = $this->db->get('timezone');
                
		$result = $data->result_array();
		return $result;
	}

	public function getclients_data($id)
	{
		$sql = "select * from users where designer_id =".$id;
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}
	
	public function get_RequestDiscussions_admin($request_id,$whose_seen,$status)
	{
		$this->db->where('request_id', $request_id);
		$this->db->where($whose_seen, $status);
		$data = $this->db->get('request_discussions');
		$result = $data->result_array();
		return $result;
	}
	
	public function get_requested_files($request_id,$user_id=null,$user_type,$preview=null,$notapproval=null)
	{
            $table = $this->S_request_model->getDataBasedonSAAS($request_id);
            if($table == "requests"){
                $tb_rq = "request_files";
            }else{
                $tb_rq = "s_request_files";
            }
            $this->db->where('request_id', $request_id);
            if($user_id != ""){
                    $this->db->where('user_id', $user_id);
            }
            if($preview){
                    $this->db->where_in('preview', $preview);
            }
            if($notapproval){
                $this->db->where_not_in('status','pending');
                $this->db->where_not_in('status','reject');
            }
            $this->db->where('user_type', $user_type);
            $this->db->order_by("created","DESC");
            $data = $this->db->get($tb_rq);
//            echo $this->db->last_query();exit;
            $result = $data->result_array();
            return $result;
	}
        
	public function get_requested_files_admin($request_id)
	{
		$this->db->where('request_id', $request_id);
		$this->db->where('src_file !=', "");
		$this->db->order_by("created","DESC");
		$data = $this->db->get('request_files');
		$result = $data->result_array();
		return $result;
	}
	public function get_requested_files_count($request_id,$user_id=null,$user_type,$preview=null,$status=array())
	{
		$this->db->where('request_id', $request_id);
		if($user_id != ""){
			$this->db->where('user_id', $user_id);
		}
		if($preview){
			$this->db->where_in('preview', $preview);
		}else{
			$this->db->where('preview', '0');
		}
		$this->db->where_not_in('status',$status);
		$this->db->where('user_type', $user_type);
		$this->db->order_by("Date(created)","DESC");
		$data = $this->db->get('request_files');
		$result = $data->result_array();
		return $result;
	}
	
	public function get_requested_files_ajax($id,$request_id,$user_id=null,$user_type,$preview=null)
	{
		$this->db->where('request_id', $request_id);
		if($user_id != ""){
			$this->db->where('user_id', $user_id);
		}
		if($preview){
			$this->db->where('preview', $preview);
		}else{
			$this->db->where('preview', '0');
		}
		$this->db->where('user_type', $user_type);
		$this->db->where("id",$id);
		$this->db->order_by("Date(created)","DESC");
		$data = $this->db->get('request_files');
		$result = $data->result_array();
		return $result;
	}
	
	public function check_user_by_email($email,$id=null)
	{
		if($id){ 
                $this->db->where('id!="'.$id.'"'); }
		$this->db->where('email', $email);
		$data = $this->db->get('users');
                //echo $this->db->last_query();exit;
		$result = $data->result_array();
		return $result;
	}
	
	public function get_all_blog(){
            $this->db->order_by("Date(created)","DESC");
            $data = $this->db->get('blog');
//          echo $this->db->last_query();exit;
            $result = $data->result_array();
            return $result;
	}
	
	public function get_all_portfolio(){
		$data = $this->db->get('protfolio');
		$result = $data->result_array();
		return $result;
	}
	
	public function get_protfolio_category()
	{
		$sql = "select * from protfolio_categories";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}

	public function designer_update_profile($data, $user_id){
		$this->db->where('id',$user_id);
		$data = $this->db->update('s_users', $data); 
		// echo  $this->db->last_query(); exit; 
		return $data;
	}

	public function getdesigner_id(){
		$this->db->order_by('id', 'RANDOM');
		$this->db->where('role','designer');
		$this->db->limit(1);
	    $query = $this->db->get('users');
	    return $query->result_array();
	}
	public function getqa_id(){
		$this->db->order_by('id', 'RANDOM');
		$this->db->where('role','qa');
		$this->db->limit(1);
	    $query = $this->db->get('s_users');
	    return $query->result_array();
	}
	public function assign_designer_confirm($id = null)
	{
            if($id != null || $id != ''){
		$data = $this->db->set('designer_assign_or_not',"1")->where('id',$id)->update('requests');
		return $data; 
            }    
	}
	public function assign_designer_by_qa($designer_id, $request_id)
	{       if($designer_id != '' && $request_id != ''){
		$data = $this->db->set(['designer_id'=>$designer_id,'designer_assign_or_not'=>"1"])->where('id',$request_id)->update('requests');
		return $data;
                }
	}

	public function update_priority_after_approved($points,$id,$prev_priority="")
	{
               $this->db->set('priority', 'priority - ' . (int) $points, FALSE);
		$this->db->where('customer_id',$id);
		$this->db->where('status','assign');
                if($prev_priority != ''){
                $this->db->where('priority > '.$prev_priority);
                }
                $data = $this->db->update('requests');
                //echo $this->db->last_query();exit;
                return $data;
	}
        
        public function update_subuserpriority_after_approved($points,$id,$created_by="",$prev_priority="")
	{
		$this->db->set('sub_user_priority', 'sub_user_priority - ' . (int) $points, FALSE);
		$this->db->where('customer_id',$id);
                $this->db->where('created_by',$created_by);
		$this->db->where('status','assign');
                if($prev_priority != ''){
                $this->db->where('sub_user_priority > '.$prev_priority);
                }
                $data = $this->db->update('requests');
                return $data;
	}
        
        public function update_priority_after_approved_qa($points,$pre,$userid,$table="")
	{
		$this->db->set('priority', 'priority - ' . (int) $points, FALSE);
		$this->db->where('priority >=',$pre);
                $this->db->where('customer_id',$userid);
		$this->db->where('status','assign');
                if($table != ""){
                    $data = $this->db->update($table);
                }else{
                    $data = $this->db->update('requests');
                }
                return $data;
	}
        public function update_subuserpriority_after_approved_qa($points,$id,$created_by="",$prev_priority="",$table="")
	{
		$this->db->set('sub_user_priority', 'sub_user_priority - ' . (int) $points, FALSE);
		$this->db->where('customer_id',$id);
                $this->db->where('created_by',$created_by);
		$this->db->where('status','assign');
                $this->db->where('sub_user_priority >= '.$prev_priority);
                if($table != ""){
                    $data = $this->db->update($table);
                }else{
                    $data = $this->db->update('requests');
                }
                //echo $this->db->last_query();exit;
                return $data;
	}
	public function customer_delete($id){
		$this->db->where('id',$id);
		$result = $this->db->delete('s_users');
		return $result;
	}
	public function get_all_files_rid($ids)
	{
		$this->db->select('*');
		$this->db->where_in('request_id',$ids);
		$query = $this->db->get('s_request_files');
	    return $query->result_array();
	}
	
	public function migrat_db()
	{
		$cake_data = $this->db->query('SELECT * FROM `users_cake`');
		$data = $cake_data->result_array();
		// echo count($data);
		for ($i=0; $i < count($data); $i++) { 
			$data[$i]['new_password'] = md5("Admin@123");
			$data[$i]['password_reset'] = md5(uniqid());
			$data[$i]['is_active'] = 1;
			$data[$i]['address_line_1'] = null;
			$data[$i]['city'] = null;
			$data[$i]['state'] = null;
			$data[$i]['zip'] = null;
			$data[$i]['designer_since'] = null;
			$data[$i]['qa_id'] = null;
			$data[$i]['next_billing'] = null;
			$data[$i]['about_me'] = '';
			$data[$i]['specializations'] = '';
			$data[$i]['tour'] = 0;
			$data[$i]['online'] = 0;
			$data[$i]['title'] = '';
			$data[$i]['last_update'] = $data[$i]['modified'];
			$this->db->insert("users",$data[$i]);
		}
		echo "<pre>";
		print_r($data);
		exit;
	}

	public function migrat_db_requ()
	{
		$cake_data = $this->db->query('SELECT * FROM `requests_cake`');
		$data = $cake_data->result_array();
		for ($i=0; $i < count($data); $i++) { 
			$data[$i]['copy_design'] = null;
			$data[$i]['software'] = null;
			$data[$i]['file_output'] = null;
			$data[$i]['status_qa'] = "approved";
			$data[$i]['status_admin'] = "approved";
			$data[$i]['status_designer'] = "approved";
			$data[$i]['who_reject'] = 0;
			$data[$i]['designer_assign_or_not'] = 1;
			$data[$i]['latest_update'] = null;
			$data[$i]['designer'] = "test";
			$data[$i]['deliverables'] = "Testing";
			$data[$i]['index_number'] = 0;
			$data[$i]['priority'] = -1;
			$this->db->insert("requests",$data[$i]);
		}
		echo "<pre>";
		print_r($data);
		exit;
	}
	public function migrat_db_requ_diss()
	{
		$cake_data = $this->db->query('SELECT * FROM `request_discussions_cake`');
		$data = $cake_data->result_array();
		for ($i=0; $i < count($data); $i++) { 
			$data[$i]['with_or_not_customer'] = 1;
			$data[$i]['qa_seen'] = 0;
			$data[$i]['va_seen'] = 0;
			$this->db->insert("request_discussions",$data[$i]);
		}
		echo "<pre>";
		print_r($data);
		exit;
	}
	public function migrat_db_blog_m()
	{
		$cake_data = $this->db->query('SELECT * FROM `posts`');
		$data = $cake_data->result_array();
		for ($i=0; $i < count($data); $i++) { 
			$data[$i]['category'] = "";
			$data[$i]['tags'] = "";
			$this->db->insert("blog",$data[$i]);
		}
		echo "<pre>";
		print_r($data);
		exit;
	}
	public function migrat_db_port()
	{

		$cake_data = $this->db->query('SELECT * FROM `portfolios_cake`');
		$data = $cake_data->result_array();
		echo "<pre>";
		foreach ($data as $value) {
			$protfolio_id[] = $value['id'];
		}


		$this->db->select("*");
		$this->db->from('portfolio_images as a');
		$this->db->join('portfolios_cake as b','b.id = a.portfolio_id');
		$this->db->order_by("a.created","DESC");
		$data = $this->db->get();
		$result = $data->result_array();
		for ($i=0; $i < count($result); $i++) { 
			
			$this->db->select('*');
			$this->db->where('id', $result[$i]['id']);
			$exist = $this->db->get('protfolio');
			$eresult = $exist->result_array();
			$portfolio[$i]['id'] = $result[$i]['id'];
			$portfolio[$i]['image'] = $result[$i]['image'];
			$portfolio[$i]['title'] = $result[$i]['title'];
			$portfolio[$i]['body'] = $result[$i]['body'];
			$portfolio[$i]['category'] = $result[$i]['potfolio_category_id'];
			$portfolio[$i]['modified'] = $result[$i]['modified'];
			$portfolio[$i]['created'] = $result[$i]['created'];
			if(empty($eresult)){
				$this->db->insert("protfolio",$portfolio[$i]);
			}else{
				print_r($eresult);
			} 
		}

		//print_r($portfolio);
	}
	public function migrat_db_request_file_m()
	{
		$cake_data = $this->db->query('SELECT * FROM `request_files_cake`');
		$data = $cake_data->result_array();
		for ($i=0; $i < count($data); $i++) { 
			$data[$i]['grade'] = 0;
			$data[$i]['customer_grade'] = null;
			$data[$i]['status'] = "Approve";
			$data[$i]['customer_seen'] = 1;
			$data[$i]['designer_seen'] = 1;
			$data[$i]['qa_seen'] = 1;
			$data[$i]['admin_seen'] = 1;
			$this->db->insert("request_files",$data[$i]);
		}
		echo "<pre>";
		print_r($data);
		exit;
	}
         public function downloadTrial(){
            $this->db->select("u.id, u.first_name, u.last_name, u.email, u.created,(select count(*) from requests as r where r.customer_id = u.id) as total_requests");
            $this->db->where("u.`is_trail` = 1 and u.role = 'customer'");
            $this->db->order_by("u.created","DESC");
            $data = $this->db->get("users as u");
            //$this->db->last_query();
            $result = $data->result_array();
            return $result;
        }
		
        public function downloadActiveUser(){
            $this->db->select("u.id, u.first_name, u.last_name, u.email,u.display_plan_name,concat(users.first_name,' ',users.last_name) as designer_name, u.created");
            $this->db->join('users',"u.designer_id = users.id",'left');
            $this->db->where("u.`is_trail` = 0 and u.parent_id = 0 and u.role = 'customer' and u.is_active = '1' and u.is_cancel_subscription = 0");
            $this->db->order_by("u.created","DESC");
            $data = $this->db->get("users as u");
//		echo $this->db->last_query();exit;
            $result = $data->result_array();
            return $result;
        }
        
	public function approveDraftemailbyadmin(){
		$this->db->select("s.value");
		$this->db->where("s.`key` = 'DRAFT_APPROVED_EMAIL_TO_USER' and s.`status` = '1'");
		$data = $this->db->get("settings as s");
		$result = $data->result_array();
		if($result[0]['value'] == '1'){
			return true;
		}
	}
    
    public function approveDraftemailbyuser($uID){
        $this->db->select("s.DRAFT_APPROVED_EMAIL_TO_USER");
        $this->db->where("s.`id` = $uID");
        $data = $this->db->get("users as s");
        $result = $data->result_array();
             if($result[0]['DRAFT_APPROVED_EMAIL_TO_USER'] == '1'){
                return true;
            }
    }
        
        /**************10.01.2018********************/
        public function emailgoingbyCron($uID){
        $this->db->select("s.SEND_CRON_EMAIL_ON_MESSAGE");
        $this->db->where("s.`id` = $uID");
        $data = $this->db->get("users as s");
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
             if($result[0]['SEND_CRON_EMAIL_ON_MESSAGE'] == '1'){
                return true;
            }
    }
    
     public function emailgoesfromadmin(){
        $this->db->select("s.SEND_CRON_EMAIL_ON_MESSAGE");
        $data = $this->db->get("settings as s");
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
             if($result[0]['SEND_CRON_EMAIL_ON_MESSAGE'] == '1'){
                return true;
            }
    }
    
    public function getSettingfields(){
        $this->db->select("s.*");
        $data = $this->db->get("settings as s");
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
            return $result; 
    }
    
    public function get_all_emails(){
        $this->db->select("*");
        $this->db->from('email_template as et');
        $this->db->where('et.user_id',"0");
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }
    
    public function get_template_byid($main_user = NULL,$email_slug = NULL){
        $this->db->select("*");
        $this->db->from('email_template as et');
        if($main_user != NULL || $main_user != ''){
        $this->db->where('et.user_id',$main_user);
        }else{
         $this->db->where_not_in('et.email_slug', $email_slug); 
         $this->db->where('et.usercanedit_email = 1');
         $this->db->where('et.user_id',0);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $query->result_array();
        return $result;
    }
    
    public function get_template_byslug_and_userid($email_slug,$main_user = NULL){
      $this->db->select("*");
      $this->db->from('email_template as et'); 
      if($main_user != NULL){
        $this->db->where('et.email_slug',$email_slug);
        $this->db->where('et.user_id',$main_user);  
      }else{
        $this->db->where('et.email_slug',$email_slug);
        $this->db->where('et.user_id',"0"); 
      }
      $query = $this->db->get();
      //echo $this->db->last_query();
      $result = $query->result_array();
      if(empty($result)){
        $this->db->select("*");
        $this->db->from('email_template as et'); 
        $this->db->where('et.email_slug',$email_slug);
        $this->db->where('et.user_id',"0"); 
      $query = $this->db->get();
      $result = $query->result_array();
      //echo $this->db->last_query();
      }
      
      return $result;
    }
    
    public function get_emailtemplate_byID($id){
        $this->db->select("*");
        $this->db->where("`id` = $id");
        $data = $this->db->get('email_template');
        $result = $data->result_array();
        return $result;
    }
   
    public function assign_designer_by_admin($designer_id, $request_ids)
	{
        if(!empty($request_ids)){
            $data = $this->db->set(['designer_id'=>$designer_id,'designer_assign_or_not'=>"1"])->where_in('id',$request_ids)->update('s_requests');
//            echo $this->db->last_query();exit;
            return $data;
        }
	}
        
        public function delete_emailtemp_file($table, $id, $filename) {
            $this->db->where('id',$id);
            $this->db->set('email_img', '');
            $data = $this->db->update($table); 
//           echo  $this->db->last_query(); exit;
            return $data;
        }
        
        public function getNumberofusersbyState() {
            $this->db->select("count(*) as count,state");
            $this->db->group_by('state');
            $this->db->order_by('state','ASC');
            $data = $this->db->get('users');
           // echo $this->db->last_query();exit;
            $result = $data->result_array();
            return $result;
        }
        
    //get request_files by id
    public function get_request_files_byid($id) {
       //die("fljghoifiy");
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('request_files');
       // echo $this->db->last_query();exit;
        $result = $query->result_array();
        return $result; 
    }
    
    /**********************tax reporting***********************/
    public function get_tax_report($start_date,$end_date) {
       //echo $today.'<br>'.$filterdate;
        $this->db->select('sum(tax_amount) as y,DATE_FORMAT(created,"%d %b") as label');
       // $this->db->where('tax_amount != 0');
        $this->db->where("created >= '".$start_date."' AND created <= '".$end_date."' AND tax_amount != 0");
        //$this->db->where('created >='. $filterdate.' between created <='. $today);
        $this->db->group_by('DATE_FORMAT(created, "%d %b")');
        $query = $this->db->get('sales_tax');
       // echo $this->db->last_query();
        $result = $query->result_array();
//        $return['result'] = array_column($result,'y');
//       $return['result_x'] = array_column($result,'label');
        return $result; 
    }
    
    public function refund_tax_report() {
        $this->db->select('st.*,u.first_name');
        $this->db->from('sales_tax as st');
        $this->db->join('users as u', 'u.customer_id = st.user_id', 'LEFT');
        $this->db->where("st.tax_amount = 0");
       // echo $this->db->last_query();
        $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
        return $result; 
    }
    
    /********all messages track functions************/      
   
    public function getallmesaagesfromreqdiscussion($date,$enddate="",$msgby="",$notmsgby="",$groupby=""){
        $this->db->select("*");
        $this->db->where("date(created) >=", $date);
        if($enddate != ""){
            $this->db->where("date(created) <=", $enddate); 
        }
        if($msgby != ""){
         $this->db->where("sender_type", $msgby);
        }
        if($notmsgby != ""){
            $this->db->where("sender_type !=", $notmsgby);
        }
        if($groupby != ""){
            $this->db->group_by($groupby);
            $this->db->from('request_discussions');
            $data = $this->db->get();
            $result = $data->result_array();
            $num_rows = sizeof($result);
        }else{
        $countmsgofreqdiscussion = $this->db->count_all_results('request_discussions');
       
      //  $result = $data->result_array();
        
        $countmsgofreqtfiles = $this->getallmesaagesfromrequestfiles($date,$enddate,$msgby,$notmsgby);
        
        $num_rows = $countmsgofreqdiscussion + $countmsgofreqtfiles;
       }
        //echo $this->db->last_query();
        return $num_rows;
    }
    
    
    public function getallmesaagesfromrequestfiles($date,$enddate="",$msgby="",$notmsgby=""){
        $this->db->select("*");
        $this->db->where("date(created) >=", $date);
        if($enddate != ""){
            $this->db->where("date(created) <=", $enddate); 
        }
        if($msgby != ""){
         $this->db->where("sender_role", $msgby);
        }
        if($notmsgby != ""){
            $this->db->where("sender_role !=", $notmsgby);
        }
        $num_rows = $this->db->count_all_results('request_file_chat');
        //echo $this->db->last_query();exit;
        return $num_rows;
    }
    
    
    public function getavgmesaagestimefromreqdiscussion($date,$enddate=""){
        $this->db->select("rd.request_id,rd.sender_type as customer_sender_type,max(rd.created) as customer_created ,rd1.request_id,rd1.sender_type,min(rd1.created) as reply_created,
         TIMESTAMPDIFF(SECOND,rd.created,rd1.created) AS time_difference");
        $this->db->from('request_discussions as rd');
        $this->db->join('request_discussions as rd1', 'rd1.request_id = rd.request_id and rd1.sender_type != "customer" and rd1.created >= rd.created', 'inner');
        $this->db->where("rd.sender_type =", 'customer');
        $this->db->where("date(rd.created) >=", $date);
        if($enddate != ""){
            $this->db->where("date(rd.created) <=", $enddate); 
        }
        $this->db->group_by('rd.request_id');
        $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
        return $result;
    }
    
    public function get_edit_protfolio_category($id){
        $this->db->select('pc.id,p.position');
        $this->db->from('protfolio as p');
        $this->db->join('protfolio_categories as pc', 'pc.id = p.category', 'LEFT');
         $this->db->where("p.id = $id");
        $data = $this->db->get();
        $result = $data->result_array();
        //echo "<pre>";print_r($result);
        return $result[0]; 
    }
    
       public function get_designer_n_project($start = NULL,$end = NULL) {
        $output = array();
        $this->db->select("u.id,u.first_name,u.last_name,u.profile_picture");
        $this->db->from('users as u');
        $this->db->where('u.role', 'designer');
        $this->db->group_by('u.id');
        $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
        $check_status_array = array("active","checkforapprove","disapprove");
//        return $result;
        foreach($result as $resk => $resv){
            $output[$resk] = $resv;        
            $this->db->select("r.designer_id,r.status,count(status) as total,dateinprogress");
            $this->db->from('requests as r');
            $this->db->where('r.designer_id',$resv['id']);
            $this->db->group_by('r.status');
            $this->db->order_by('dateinprogress','ASC');
            $data1 = $this->db->get();
            $resultdata = $data1->result_array();
            if(!empty($resultdata)){
                $avgdesigns = $this->getAvgdesignsperday($resv['id'],$start,$end);
                $totalreq = count($avgdesigns);
                $dateinprogress = date("Y-m-d",strtotime($resultdata[0]['dateinprogress']));
                $currentdate = date("Y-m-d");
                $earlier = new DateTime($dateinprogress);
                $later = new DateTime($currentdate);
                $diff = $later->diff($earlier)->format("%a");
//                echo "avg".$totalreq."<br/>";
//                echo "diff".$diff."<br/>";
                $avgperday = round($totalreq/$diff);
                $avgperday = is_finite($avgperday)?$avgperday:0;
                //$currentproject = count($resultdata);
                $currentproject = $this->getCurrentProjectAssignedtodesigner($resv['id']);
                $gettimeReviews = $this->getTimeQualityReview($resv['id'],$start,$end);
                $getlateprojectCount = $this->getLateprojectofDesigner($resv['id']);
                $permanentCustomerCount = $this->getCustomercount_having_permanentDesigner($resv['id']);
                $daysdifftocomplete = $this->Avgdaystocompletedesign($resv['id'],$start,$end);
                $completedPro = $this->getAllcompletedprojects($resv['id'],$start,$end);
                $avgdaysforcompletion = round($daysdifftocomplete/$completedPro);
                $avgdaysforcompletion = is_finite($avgdaysforcompletion)?$avgdaysforcompletion:0;
                $getallrequests = $this->getAllprojectsofDesigner($resv['id'],$start,$end);
                $getalldraft = $this->getAlldraftsubmitted($resv['id'],$start,$end);
                $avgdraft = round($getalldraft/$getallrequests);
                $avgdraft = is_finite($avgdraft)?$avgdraft:0;
                $output[$resk]['avgperday'] = $avgperday;
                $output[$resk]['gettimeReviews'] = $gettimeReviews;
                $output[$resk]['currentproject'] = $currentproject;
                $output[$resk]['lateprojectCount'] = $getlateprojectCount;
                $output[$resk]['permamnentCustomercount'] = $permanentCustomerCount;
                $output[$resk]['avgdaysforcompletion'] = $avgdaysforcompletion;
                $output[$resk]['avgdraft'] = $avgdraft;
                $total = 0;
                foreach($resultdata as $k=>$v){
                    $status = $v['status'];
                    $output[$resk][$status] = $v['total'];
                    if(in_array($status, $check_status_array)){                        
                        $total = $total+$v['total'];    
                    }               
                }
                $output[$resk]['total'] = $total;
            }
             $output[$resk]['average_total_quality'] = 0;
            if(isset($output[$resk]['active'])){
                $avgtotalquality = round($gettimeReviews/$output[$resk]['active']);
                $avgtotalquality = is_finite($avgtotalquality)?$avgtotalquality:0;
                $output[$resk]['average_total_quality'] = $avgtotalquality;
            }
            
         }
        // echo "<pre/>";print_r($output);
        return $output; 
    }
    
    
     public function get_report_based_onstatus($status,$start_date,$end_date) {
       //echo $today.'<br>'.$filterdate;
        $this->db->select('sum(tax_amount) as y,DATE_FORMAT(created,"%d %b") as label');
       // $this->db->where('tax_amount != 0');
        $this->db->where("created >= '".$start_date."' AND created <= '".$end_date."' AND tax_amount != 0");
        //$this->db->where('created >='. $filterdate.' between created <='. $today);
        $this->db->group_by('DATE_FORMAT(created, "%d %b")');
        $query = $this->db->get('sales_tax');
       // echo $this->db->last_query();
        $result = $query->result_array();
        $return['result'] = array_column($result,'y');
        $return['result_x'] = array_column($result,'label');
        return $return; 
    }
    
     public function getLinechartdata($start_date,$end_date,$selected_project_category) {
        $start_date = date("Y-m-d", strtotime($start_date));
        $end_date = date("Y-m-d", strtotime($end_date));
        if($selected_project_category == 'new_project') {
            $this->db->select("count(r.created) as y, DAYNAME(r.created) as label");
        }else{
            $this->db->select("count(r.created) as y, DAYNAME(r.dateinprogress) as label");
        }
        $this->db->from('requests as r');
        if($selected_project_category == 'new_project'){
            $this->db->where('DATE_FORMAT(r.created,"%Y-%m-%d") >="' .$start_date. '" and DATE_FORMAT(r.created,"%Y-%m-%d") <="' .$end_date.'"');
            $this->db->group_by('CAST(r.created AS DATE)');
            $this->db->order_by('CAST(r.created AS DATE)','ASC');
        }elseif($selected_project_category == 'in_progress'){
            $this->db->where('DATE_FORMAT(r.dateinprogress,"%Y-%m-%d") >="' .$start_date. '" and DATE_FORMAT(r.dateinprogress,"%Y-%m-%d") <="' .$end_date.'"');
            $this->db->group_by('CAST(r.dateinprogress AS DATE)');
            $this->db->order_by('CAST(r.dateinprogress AS DATE)','ASC');
        }
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
     }
     
     public function getusersbasedcreated($start_date,$end_date) {
//         echo $start_date.'<br>'.$end_date;
         $start_date = date("Y-m-d", strtotime($start_date));
         $end_date = date("Y-m-d", strtotime($end_date));
         $this->db->select("count(u.created) as y, DAYNAME(u.created) as label");
         $this->db->from('users as u');
         $this->db->where('DATE_FORMAT(u.created,"%Y-%m-%d") >="' .$start_date. '" and DATE_FORMAT(u.created,"%Y-%m-%d") <="' .$end_date.'"');
         $this->db->group_by('CAST(u.created AS DATE)');
         $this->db->order_by('CAST(u.created AS DATE)','ASC');
         $query = $this->db->get();
         $result = $query->result_array();
        // echo $this->db->last_query();
         return $result;
         
     }
     
     public function getutmusersbasedcreated($start_date,$end_date) {
//         echo $start_date.'<br>'.$end_date;
         $start_date = date("Y-m-d", strtotime($start_date));
         $end_date = date("Y-m-d", strtotime($end_date));
         $this->db->select("count(u.created) as y, u.YOUR_SOURCE_FIELD as label");
         $this->db->from('users as u');
         $this->db->where('DATE_FORMAT(u.created,"%Y-%m-%d") >="' .$start_date. '" and DATE_FORMAT(u.created,"%Y-%m-%d") <="' .$end_date.'"');
         $this->db->where('u.YOUR_SOURCE_FIELD != "NULL" and u.YOUR_SOURCE_FIELD != ""');
         $this->db->group_by('u.YOUR_SOURCE_FIELD');
         $query = $this->db->get();
         $result = $query->result_array();
         //echo $this->db->last_query();
         return $result;
         
     }
     
     public function getAvgdesignsperday($id,$start=NULL,$end=NULL){
        $this->db->select('*');
        $this->db->from('requests');
        $this->db->where("designer_id",$id);
         if($start && $end){
            $this->db->where('created >=',$start);
            $this->db->where('created <=',$end);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $query->result_array();
        return $result;
     }
     
     public function getall_customer(){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where("is_active",'1');
        $this->db->where("role",'customer');
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $query->result_array();
        return count($result); 
     }
     
     public function getall_Internationalcustomer(){
        $usa_states = implode(',',USA_STATES);
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where("is_active",'1');
        $this->db->where("role",'customer');
        $this->db->where("state NOT IN ('$usa_states') ");
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $query->result_array();
        return count($result); 
     }
     
     public function getall_usacustomer(){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where("is_active",'1');
        $this->db->where("role",'customer');
        $this->db->where_in("state",USA_STATES);
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $query->result_array();
        return count($result); 
     }
     
      public function getalltaxcustomer(){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where("is_active",'1');
        $this->db->where("role",'customer');
        $this->db->where("state IN ('texas')");
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $query->result_array();
        return count($result); 
     }
     
     public function getalltaxamount(){
        $this->db->select('SUM(tax_amount) as totaltax');
        $this->db->from('sales_tax');
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $query->result_array();
        return $result[0]['totaltax']; 
     }
     
     public function getTimeQualityReview($designerID,$start = NULL,$end = NULL){
        $this->db->select('*');
        $this->db->from('request_files');
        if($designerID){
        $this->db->where("user_id",$designerID);
        $this->db->where("status","Reject");
        }
        if($start && $end){
            $this->db->where('created >=',$start);
            $this->db->where('created <=',$end);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $query->result_array();
        return count($result); 
     }
     
     public function getLateprojectofDesigner($designerID){
        $current_date = date('Y-m-d');
        $this->db->select('*');
        $this->db->from('requests');
        $this->db->where("designer_id",$designerID);
        $this->db->where_in("status",array('active','disapprove'));
        $this->db->where("DATE(expected_date) <=' ".$current_date."'" );
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $query->result_array();
        return count($result); 
     }
     
     //Customer has permanent designer in users table 
        public function getCustomercount_having_permanentDesigner($designer_id){
            $this->db->select("*");
            $this->db->from('users');
            $this->db->where("designer_id",$designer_id); 
            $data = $this->db->get();
//          echo $this->db->last_query();exit;
            $result = $data->result_array();
            //echo "<pre>";print_r($result);
            return count($result);  
        }
        
        // all projects assigned to designer that are not completed
        public function getCurrentProjectAssignedtodesigner($designer_id){
            $this->db->select("*");
            $this->db->from('requests');
            $this->db->where("designer_id",$designer_id); 
             $this->db->where("status != ","approved"); 
            $data = $this->db->get();
//             echo $this->db->last_query();exit;
            $result = $data->result_array();
            //echo "<pre>";print_r($result);
            return count($result);  
        }
        
        //Avg. Days to complete design
        public function getAllcompletedprojects($designer_id,$start = NULL,$end = NULL){
            $this->db->select("*");
            $this->db->from('requests');
            $this->db->where("designer_id",$designer_id); 
            $this->db->where("status","approved"); 
            if($start && $end){
                $this->db->where('created >=',$start);
                $this->db->where('created <=',$end);
            }
            $data = $this->db->get();
//             echo $this->db->last_query();exit;
            $result = $data->result_array();
            //echo "<pre>";print_r($result);
            return count($result);
        }
        public function Avgdaystocompletedesign($designer_id,$start = NULL,$end = NULL){
            $this->db->select("SUM(DATEDIFF(r.approvaldate,r.dateinprogress)) as total");
            $this->db->from('requests as r');
            $this->db->where("designer_id",$designer_id);
            if($start && $end){
            $this->db->where('r.created >=',$start);
            $this->db->where('r.created <=',$end);
        }
            $data = $this->db->get();
//             echo $this->db->last_query();exit;
            $result = $data->result_array();
//            echo "<pre>";print_r($result);
             return $result[0]['total'];
        }
        
        //average number of design drafts
        public function getAllprojectsofDesigner($designer_id,$start = NULL,$end = NULL){
            $this->db->select("*");
            $this->db->from('requests');
            $this->db->where("designer_id",$designer_id);
            if($start && $end){
            $this->db->where('created >=',$start);
            $this->db->where('created <=',$end);
            }
            $data = $this->db->get();
//             echo $this->db->last_query();exit;
            $result = $data->result_array();
//            echo "<pre>";print_r($result);
             return count($result);
        }
        
        public function getAlldraftsubmitted($designer_id,$start = NULL,$end = NULL){
            $this->db->select("*");
            $this->db->from('request_files');
            $this->db->where("user_id",$designer_id);
            $this->db->where("user_type","designer");
            if($start && $end){
                $this->db->where('created >=',$start);
                $this->db->where('created <=',$end);
            }
            $data = $this->db->get();
//             echo $this->db->last_query();exit;
            $result = $data->result_array();
//            echo "<pre>";print_r($result);
             return count($result);
        }
        
        
       /*******new function for update billing cycle of sub users**********/
        public function getallagencyuser_data()
	{          
                $this->db->select("id");
                $this->db->from('users');
		$this->db->where_in('plan_name', AGENCY_USERS);
		$data = $this->db->get();
		$result = $data->result_array();
                //echo $this->db->last_query();
		return $result;
	}
        public function getallsubuser_data($users=array(),$offline="")
	{     
                $this->db->select("u.id,u.billing_start_date,u.billing_end_date,sp.plan_type_name");
                $this->db->from('users as u');
                $this->db->join('agency_subscription_plan as sp','sp.plan_id = u.plan_name','inner');
                if($offline != ""){
                    $this->db->where_not_in('u.parent_id',$users);
                }else{
                   $this->db->where('u.parent_id != 0');  
                }
                $this->db->where('u.is_active', 1);
                $this->db->where('u.user_flag', 'client');
                $this->db->where('u.requests_type != "one_time"');
		$data = $this->db->get();
                //echo $this->db->last_query();exit;
		$result = $data->result_array();
                
		return $result;
	}
//         public function sumallsubuser_requestscount($subuserid)
//	{ 
//             $this->db->select_sum("total_active_req");
//             $this->db->select_sum("total_inprogress_req");
//             $this->db->from('users');
//             $this->db->where_in('parent_id', $subuserid);
//             $this->db->where('user_flag', 'client');
//             $data = $this->db->get();
//             $result['total_active_req'] = $data->row()->total_active_req;
//             $result['total_inprogress_req'] = $data->row()->total_inprogress_req;
//             
//            return $result;
//        }
        
        public function sumallsubuser_requestscount($subuserid,$shareduser="")
	{ 
             $this->db->select_sum("u.total_active_req");
             $this->db->select_sum("u.total_inprogress_req");
             $this->db->join('agency_subscription_plan as asp','asp.plan_id = u.plan_name','inner');
             $this->db->from('users as u');
             $this->db->where('u.parent_id', $subuserid);
             $this->db->where('asp.shared_user_type', $shareduser);
             $this->db->where('u.user_flag', 'client');
             $data = $this->db->get();
             //echo $this->db->last_query()."<br>";
             $result['total_active_req'] = $data->row()->total_active_req;
             $result['total_inprogress_req'] = $data->row()->total_inprogress_req;
            return $result;
        }
        
        public function getTotalTaxes($start,$end){
            $start_date = date("Y-m-d", strtotime($start));
            $end_date = date("Y-m-d", strtotime($end));
            $this->db->select("ROUND(SUM(st.tax_amount)) as totaltax,st.state");
            $this->db->where('DATE_FORMAT(st.created,"%Y-%m-%d") >="' .$start_date. '" and DATE_FORMAT(st.created,"%Y-%m-%d") <="' .$end_date.'" and st.debit_credit = "credit"');
            $this->db->group_by('st.state');
            $this->db->from('sales_tax as st');
            $query = $this->db->get();
//            echo $this->db->last_query();exit;
            $result = $query->result_array();
            //echo "<pre/>";print_r($result);
            return $result;
        }
        
        public function get_users_notes($userid){
            $this->db->select("*");
            $this->db->where('user_id',$userid);
            $this->db->from('user_notes');
            $this->db->order_by("created", "DESC"); 
            $query = $this->db->get();
           // echo $this->db->last_query();
            $result = $query->result_array();
            //echo "<pre/>";print_r($result);
            return $result;
        }
    public function assign_va_touser($va_id, $customer_ids){
        if(!empty($customer_ids)){
            $data = $this->db->set(['va_id'=>$va_id])->where_in('id',$customer_ids)->update('s_users');
            return $data;
        }
    }
    public function designer_update_hobbiesabout($data, $user_id){
        $this->db->where('user_id',$user_id);
        $result = $this->db->update('s_user_skills', $data); 
        // echo $this->db->last_query();exit;
        return $result;
    }
    
    public function checkparameter($userparameter="",$userinfoper=""){
            $user_array = array();
            $user_info_array = array();
            if($userparameter != ""){
                $explode_user = explode(",", $userparameter);
                $user_array = array_map(function($value) { return ' u.'.$value; }, $explode_user);
            }
            if($userinfoper != ""){
                $explode_userinfo = explode(",", $userinfoper);
                $user_info_array = array_map(function($value) { return ' ui.'.$value; }, $explode_userinfo);
            }
            $merge_parameter = array_merge($user_array,$user_info_array);
            $parameter = implode(",",$merge_parameter);
            return $parameter;
    }
        
    public function overwriteuserdata()
    {
            $this->db->select("u.id,u.plan_name,sp.total_sub_user,sp.brand_profile_count");
            $this->db->where('u.overwrite',1);
            $this->db->where('u.role','customer');
            $this->db->join('subscription_plan as sp','sp.plan_id = u.plan_name','left');
            $this->db->from('users as u');
            $query = $this->db->get();
            $result = $query->result_array();
            return $result;
    }
        
   public function cancelledRequest()  {
            $this->db->select("u.id,cs.id as cid,u.first_name,u.last_name,cs.is_designer_switch,cs.reason,cs.created,u.email,cs.coupon_code,cs.cancel_feedback,cs.why_reason,cs.cancel_status");
            $this->db->join('users as u','cs.customer_id = u.id','left');
            $this->db->where('cs.is_agency', 0);
            $this->db->from('cancelled_subscriptions as cs');
            $this->db->order_by("cs.created", "DESC"); 
            $query = $this->db->get();
//            echo $this->db->last_query();exit;
            $result = $query->result_array();
            return $result;
        }
    
    /*************Project Reports**************/
        
    public function averageRevisionperDesign(){
        $this->db->select_sum("count_customer_revision");
        $this->db->select("count(*) as count");
        $this->db->where('dummy_request', 0);
        $this->db->where("status NOT IN ('assign','hold','cancel','draft')");
        $this->db->from('requests');
//        $this->db->where('status', 'disapprove');
        $data = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
//        echo "<pre/>";print_R($result);exit;
        $avrg = ($result[0]['count_customer_revision']/$result[0]['count']);
        return number_format((float)$avrg, 2, '.', '');
    }
    
    public function SumofRevisionbasedonNumber($num){
        $this->db->select('count(*) as count');
        $this->db->from('requests');
        $this->db->where('dummy_request', 0);
        $this->db->where('count_customer_revision >=', $num);
//        $this->db->where('status', 'disapprove');
        $data = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
//      echo "<pre/>";print_R($result);
        return $result[0]['count'];
    }
    
     public function SumofQualityRevisionbasedonNumber($num){
        $this->db->select('count(*) as count');
        $this->db->from('requests');
        $this->db->where('dummy_request', 0);
        $this->db->where('count_quality_revision >=', $num);
//        $this->db->where('status', 'disapprove');
        $data = $this->db->get();
//      echo $this->db->last_query();exit;
        $result = $data->result_array();
//      echo "<pre/>";print_R($result);
        return $result[0]['count'];
    }
    
    public function DelayedProjectsCount(){
        $todaytime = date('Y-m-d H:i:s');
        $today_time = date('Y-m-d H:i:s',mktime(0,0,0));
        $start_time =  date('Y-m-d H:i:s', strtotime($today_time. '+5 hours +00 minutes +00 seconds')); 
        $end_time = date("Y-m-d H:i:s", strtotime($start_time. '+23 hours +59 minutes +59 seconds'));
        $this->db->select('count(*) as count');
//        $this->db->select("title,expected_date,approvaldate,status_admin");
        $this->db->from('requests');
        $this->db->where('dummy_request', 0);
        $this->db->where("expected_date >=",$start_time);
        $this->db->where("expected_date <=",$todaytime);
        $this->db->where("status_admin IN ('active','disapprove','approved')");
        $this->db->where("expected_date < approvaldate");
        $data = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
//      echo "<pre/>";print_R($result);
        return $result[0]['count'];
    }
    
    public function lateProjectsCount(){
        $todaytime = date('Y-m-d H:i:s');
        $today_time = date('Y-m-d H:i:s',mktime(0,0,0));
        $start_time =  date('Y-m-d H:i:s', strtotime($today_time. '+5 hours +00 minutes +00 seconds')); 
        $end_time = date("Y-m-d H:i:s", strtotime($start_time. '+23 hours +59 minutes +59 seconds'));
        $this->db->select('count(*) as count');
        $this->db->from('requests');
        $this->db->where('dummy_request', 0);
        $this->db->where("expected_date <=",$todaytime);
        $this->db->where("status_admin IN ('active','disapprove')");
        $data = $this->db->get();
//      echo $this->db->last_query();exit;
        $result = $data->result_array();
//      echo "<pre/>";print_R($result);
        return $result[0]['count'];
    }
    
    public function unverifiedProjectsCount(){
        $this->db->select('count(*) as count');
        $this->db->from('requests');
        $this->db->where('dummy_request', 0);
        $this->db->where("verified",0); 
        $this->db->where("status_admin IN ('active','disapprove')");
        $data = $this->db->get();
//      echo $this->db->last_query();exit;
        $result = $data->result_array();
//      echo "<pre/>";print_R($result);
        return $result[0]['count'];
    }
    
     public function activeProjectsCount(){
        $todaytime = date('Y-m-d H:i:s',mktime(0,0,0));
        $start_time =  date('Y-m-d H:i:s', strtotime($todaytime. '+5 hours +00 minutes +00 seconds')); 
        $end_time = date("Y-m-d H:i:s", strtotime($start_time. '+23 hours +59 minutes +59 seconds'));
        $this->db->select('count("r.id") as count');
        $this->db->from('requests as r');
        $this->db->where('r.dummy_request', 0);
        $this->db->where("r.status NOT IN ('cancel','hold','draft','assign')");
        $this->db->where("r.expected_date >=",$start_time); 
        $this->db->where("r.expected_date <=",$end_time); 
        $data = $this->db->get();
        $result = $data->result_array();
         return $result[0]['count'];
    }
    

    public function activebutnotcompletedProjectsCount(){
        $todaytime = date('Y-m-d H:i:s',mktime(0,0,0));
        $start_time =  date('Y-m-d H:i:s', strtotime($todaytime. '+5 hours +00 minutes +00 seconds')); 
        $end_time = date("Y-m-d H:i:s", strtotime($start_time. '+23 hours +59 minutes +59 seconds'));
        $this->db->select('count(*) as count');
        $this->db->from('request_files as rf');
        $this->db->where("r.status NOT IN ('cancel','hold','draft','assign')");
        $this->db->where('r.dummy_request',0);
        $this->db->where("r.expected_date >=",$start_time); 
        $this->db->where("r.expected_date <=",$end_time); 
        $this->db->where("rf.created >=",$start_time);
        $this->db->where("rf.created <=",$end_time);
        $this->db->where("rf.status IN ('Approve','customerreview')");
        $this->db->join('requests as r',"rf.request_id = r.id",'inner');
        $this->db->group_by('rf.request_id');
        $data = $this->db->get();
        $result = $data->result_array();
        return sizeof($result);
    }
    
    public function OverAllCustmoreFeedBack($startDate,$endDate,$limit="",$start=""){
       $this->db->select('COUNT(rfr.id) AS total_drafts,rc.name AS parent_cat_name,rsc.name AS sub_cat_name,FORMAT(AVG(rfr.satisfied_with_design),2) AS total_rating');
       $this->db->from('request_files_review as rfr');
       $this->db->where("rfr.user_id != '0' && rfr.user_feedback_date >= '".$startDate." 00:00:00' && rfr.user_feedback_date <= '".$endDate." 00:00:00' && rc.name!='' && rsc.name!=''");
       $this->db->join('requests as r',"r.id =  rfr.request_id",'inner');
       $this->db->join('request_categories as rc',"rc.id =  r.category_id",'left');
       $this->db->join('request_categories as rsc',"rsc.id =  r.subcategory_id",'left');
      // $this->db->join('request_files as rf',"rf.request_id =  r.id",'left');
       $this->db->group_by('r.subcategory_id');
       $this->db->order_by('total_drafts','DESC'); 
       // $this->db->limit($limit,$start);
       $data = $this->db->get();
       //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }

     public function FeedbackColumns_desinger($order,$limit=""){
 
       $this->db->select("CONCAT(IFNULL(u.first_name,''),' ',IFNULL(u.last_name,'')) AS name,u.is_active, COUNT(u.id) as total_design, FORMAT(AVG(rfr.satisfied_with_design),2) as total_avg, u.id as d_id");
       $this->db->from('request_files_review as rfr');
       $this->db->where("rfr.user_id != '0'");
         $this->db->join('users as u',"u.id =  rfr.designer_id",'inner');  
       $this->db->group_by('rfr.designer_id');
       if($order == ""){
       	$this->db->order_by('total_design',"DESC"); 
       }else{
       	$this->db->order_by('total_design',$order); 

       }
       if($limit != ""){
           $this->db->limit($limit,0);
       }
       $data = $this->db->get();
//         echo $this->db->last_query();exit;
       $result = $data->result_array();
       return $result; 
   
    }
   

     public function FeedbackColumns_customer(){
     	// 
      $this->db->select("CONCAT(IFNULL(u.first_name,''),' ',IFNULL(u.last_name,'')) AS name,u.is_active, COUNT(rfr.user_id) as total_design, FORMAT(AVG(rfr.satisfied_with_design),2) as total_avg, u.display_plan_name as plan_name, u.plan_amount as plan_amount,
       	(select count(id) from request_files rf2 where rf2.request_id = rfr.request_id) as no_drafts,
       	rfr.user_id as customer_id,u.parent_id as parentid,u.id as id,u2.first_name as parent_name");
       $this->db->from('request_files_review as rfr');
       $this->db->where("rfr.user_id != '0'");
       $this->db->join('users as u',"u.id =  rfr.user_id",'inner'); 
       $this->db->join('users as u2',"u2.id =  u.parent_id",'left'); 
       $this->db->group_by('rfr.user_id');
       $this->db->order_by('total_design','DESC'); 
      // $this->db->limit($limit,$start);
       $data = $this->db->get();
       //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }
     public function FeedbackColumns_qa(){
     	$this->db->select("CONCAT(IFNULL(u.first_name,''),' ',IFNULL(u.last_name,'')) AS name, COUNT(u.id) as total_design, FORMAT(AVG(rfr.satisfied_with_design),2) as total_avg,rfr.qa_id as qa_id");
     	$this->db->from('request_files_review as rfr');
     	$this->db->where("rfr.user_id != '0'");
     	$this->db->join('users as u',"u.id =  rfr.qa_id",'inner');  
     	$this->db->group_by('rfr.qa_id');
     	$this->db->order_by('total_design','DESC'); 
      // $this->db->limit($limit,$start);
     	$data = $this->db->get();
        //echo $this->db->last_query();exit;
     	$result = $data->result_array();
     	return $result;
    }
    
    public function AjaxForSubCustomer($parentid){
      $this->db->select('u.first_name as name , u.id as id ,avg(rfr.satisfied_with_design) as totalavg, u.display_plan_name as plan_name, u.plan_amount as plan_amount');
       $this->db->from('request_files_review as rfr');
       $this->db->where("rfr.user_id != '0' && u.parentid = '".$parentid."'");
       $this->db->join('users as u',"u.id =  rfr.user_id",'inner'); 
      // $this->db->limit($limit,$start);
       $data = $this->db->get();
        //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }


    public function rating_details($withtext="",$start="",$limit=""){
         $this->db->select("CONCAT(c.first_name,' ',c.last_name) as customer_name,c.id as c_id,
			r.title as request_name,
			(select count(id) from request_files rf2 where rf2.request_id = rfr.request_id) as no_drafts,
			rf.file_name as design_name, rfr.created,rfr.user_feedback_date,rfr.draft_id,rfr.request_id as req_id,
			FORMAT(avg(rfr.satisfied_with_design),2) as satisfied_with_design,
			rfr.additional_notes,
			CONCAT(IFNULL(d.first_name,''),' ',IFNULL(d.last_name,'')) as designer_name,
			CONCAT(IFNULL(q.first_name,''),' ' ,IFNULL(q.last_name,'')) as qa_name,q.id as q_id,d.id as d_id ");
         $this->db->from('request_files_review as rfr');
         $this->db->join('users  as c',"c.id = rfr.user_id",'left');  
         $this->db->join('users as d',"d.id = rfr.designer_id",'left');  
         $this->db->join('users as q',"q.id = rfr.qa_id",'left');  
         $this->db->join('requests as r',"r.id = rfr.request_id",'left');  
         $this->db->join('request_files as rf',"rf.id = rfr.draft_id",'left');  
         $this->db->where("rfr.user_id != '0'");
         $this->db->group_by('c.id');

         $this->db->order_by('no_drafts','DESC');
         if($withtext == 1){
           $this->db->where('rfr.additional_notes != "" ');
         }$this->db->limit($limit,$start);
          
         
        $data = $this->db->get();
       //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }

    public function fetch_dataForExcelFile()
    { 
    	$this->db->select("CONCAT(c.first_name,' ',c.last_name) as 'Customer Name',
    	r.title as 'No of Drafts',
    	(select count(id) from request_files rf2 where rf2.request_id = rfr.request_id) as no_drafts,
    	rf.file_name as 'No Of Design',
    	rfr.created as 'Date Delivered',
    	rfr.draft_id as 'Design Drafts',
    	FORMAT(avg(rfr.satisfied_with_design),2) as 'Design Rating',rfr.additional_notes as 'Rating Text',
    	CONCAT(d.first_name,' ',d.last_name) as 'Desinger Name',
    	CONCAT(q.first_name,' ',q.last_name) as 'Qa Name'");
    	$this->db->from('request_files_review as rfr');
    	$this->db->join('users  as c',"c.id = rfr.user_id",'left');  
    	$this->db->join('users as d',"d.id = rfr.designer_id",'left');  
    	$this->db->join('users as q',"q.id = rfr.qa_id",'left');  
    	$this->db->join('requests as r',"r.id = rfr.request_id",'left');  
    	$this->db->join('request_files as rf',"rf.id = rfr.draft_id",'left');  
    	$this->db->where("rfr.user_id != '0'");
    	$this->db->order_by('rfr.draft_id','DESC');
    	$this->db->group_by('c.id');
    	$data = $this->db->get();
      	  // echo $this->db->last_query();exit;
    	$result = $data->result_array();
    	return $result;
    }

    public function rating_details_AvgCustomer(){
       $this->db->select("CONCAT(u.first_name,' ',u.last_name) as name,FORMAT(AVG(rfr.satisfied_with_design),2) as total_avg,rfr.user_role as role,(select count(id) from request_files rf2 where rf2.request_id = rfr.request_id) as no_drafts,rfr.user_id as customer_id ,rfr.designer_id as d_id,rfr.qa_id as qa_id");
       $this->db->from('request_files_review as rfr');
       $this->db->where("rfr.user_id != '0'");
       $this->db->join('users as u',"u.id =  rfr.user_id",'left');  
       $this->db->group_by('u.id');
       $this->db->order_by('no_drafts','DESC');
      // $this->db->limit($limit,$start);
       $data = $this->db->get();
       //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }
    public function rating_details_AvgDesinger(){
       $this->db->select("CONCAT(u.first_name,' ',u.last_name) as name,FORMAT(AVG(rfr.satisfied_with_design),2) as total_avg,rfr.user_role as role,(select count(id) from request_files rf2 where rf2.request_id = rfr.request_id) as no_drafts, u.id as d_id");
       $this->db->from('request_files_review as rfr');
       $this->db->where("rfr.user_id != '0'");
       $this->db->join('users as u',"u.id =  rfr.designer_id",'left');  
       $this->db->group_by('u.id');
       $this->db->order_by('no_drafts','DESC');
      // $this->db->limit($limit,$start);
       $data = $this->db->get();
     // echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }
     public function rating_details_qa(){
       $this->db->select("CONCAT(u.first_name,' ',u.last_name) as name,FORMAT(AVG(rfr.satisfied_with_design),2) as total_avg,rfr.user_role as role,(select count(id) from request_files rf2 where rf2.request_id = rfr.request_id) as no_drafts,");
       $this->db->from('request_files_review as rfr');
       $this->db->where("rfr.user_id != '0'");
       $this->db->join('users as u',"u.id =  rfr.qa_id",'left');  
       $this->db->group_by('u.id');
       $this->db->order_by('no_drafts','DESC');
      // $this->db->limit($limit,$start);
       $data = $this->db->get();
       //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }

    public function LikeORdislIke($condition,$starfrom="",$staronend=""){
       $this->db->select('rfr.user_id as userid, AVG(rfr.satisfied_with_design) AS average, COUNT(rfr.user_id) as total_design');
       $this->db->from('request_files_review as rfr');
       if($starfrom!="" && $staronend!=""){
       	$this->db->where("rfr.user_id != '0' && rfr.user_feedback_date > '".$starfrom." 00:00:00' 
		     && rfr.user_feedback_date < '".$staronend." 00:00:00'");
       }else{
       $this->db->where("rfr.user_id != '0'");
       }
 
      // $this->db->join('users as u',"u.id =  rfr.user_id",'inner'); 
       $this->db->group_by('rfr.user_id');
       $this->db->having('AVG(rfr.satisfied_with_design)'.$condition.'');
      // $this->db->limit($limit,$start);
       $data = $this->db->get();
        //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }


    public function select_custom($select,$table)
        {
               $this->db->select($select);
               $this->db->from($table);
               $sql = $this->db->get(); 
              //echo  $this->db->last_query();exit;  
               $result = $sql->result_array();
               if(!empty($result)){
               	foreach ($result as $key => $value) {
               		return $value; 
               	}
               }else{
               	return $result =0;  
               }
               		die(); 
           // return $result;
        } 

        public function ajaxForTopNav($starfrom,$staronend){
 
        	$this->db->select('IFNULL( FORMAT(AVG(rfr.satisfied_with_design),2),"0") AS customer_rating');
        	$this->db->from('request_files_review as rfr');
        	$this->db->where("rfr.user_id != '0' && rfr.user_feedback_date > '".$starfrom." 00:00:00' 
		     && rfr.user_feedback_date < '".$staronend." 00:00:00'
        		");
        	$data = $this->db->get();
          //  echo $this->db->last_query();exit;
        	$result = $data->result_array();


        	if(!empty($result)){
        	return $result;
           }else{
           	return $result = 0;
           }

        }
        public function ratingByplanName(){
        	
        	$this->db->select("s.plan_type_name as plan_name, count(rfr.id) as total_designs, FORMAT(avg(rfr.satisfied_with_design),2) as avg_rating");
        	$this->db->from("request_files_review as rfr");
        	$this->db->join("requests as r","r.id =  rfr.request_id",'inner');  
        	$this->db->join("users as u","u.id =  r.customer_id",'inner');  
        	$this->db->join("subscription_plan as s","s.plan_id =  u.plan_name",'inner'); 
        	$this->db->where("rfr.user_id !=0"); 
            $this->db->group_by('s.plan_type_name');
            $data = $this->db->get();
          //  echo $this->db->last_query();exit;
        	$result = $data->result_array();
        	return $result;
   
        }

        public function CheckForMainORsub($userid){
 			$this->db->select("is_saas as saas_user");
        	$this->db->from("users");
        	$this->db->where("id = '".$userid."' and is_saas = 1 "); 
            $userdata = $this->db->get();
            $userdata = $userdata->result_array();

            if(empty($userdata)){
            	$this->db->select("id");
            	$this->db->from("s_users");
            	$this->db->where("id = '".$userid."'"); 
            	$data = $this->db->get();
                //echo $this->db->last_query();exit;
            	$result = $data->result_array();
            	$result['is_saas'] = 0 ; 
            	
            }else{
            	$result['is_saas'] = 1;
            }
            return $result;
        }
        
    /***********SAAS functions************/
    public function getuser_data($id,$userparameter="",$userinfoper="")
        {   
        $this->db->reset_query();
        if($userparameter != "" || $userinfoper != ""){
            $parameter = $this->checkparameter($userparameter,$userinfoper);
        }
        if($parameter != ""){
         $this->db->select($parameter);
        }else{
           $this->db->select("u.*,ui.sub_user_count,ui.brand_profile_count,ui.file_management,ui.file_sharing");  
        }
        $this->db->where('u.id', $id);
        $this->db->join('users_info as ui','ui.user_id = u.id','left');
        $data = $this->db->get('users as u');
//        echo $this->db->last_query().'<br>';
        $result = $data->result_array();
        if($result[0]['is_saas'] == 1){
            return $result;
        }else{
            if($parameter != ""){
            $this->db->select($parameter);
           }else{
              $this->db->select("su.*,sui.sub_user_count,sui.brand_profile_count,sui.file_management,sui.file_sharing");  
           }
            $this->db->where('su.id', $id);
            $this->db->join('s_users_info as sui','sui.user_id = su.id','left');
            $data_s = $this->db->get('s_users as su');
//            echo $this->db->last_query().'<br>'; exit;
            // $data_s['from'] = "saas_user";  
            $result_s =  $data_s->result_array();  
//            echo "<pre/>";print_R($result_s);
            return $result_s;
        }
    }


    public function getuserPaymentpermiSsion($userid){ 
    	$this->db->select("*");  
    	$this->db->from("agency_user_options as au"); 
    	$this->db->where('au.user_id',$userid);
    	$data= $this->db->get();
//	 echo $this->db->last_query().'<br>'; exit;
    	$result =  $data->result_array();  
    	return $result;
    }

    public function get_all_request_by_customer($customer_id)
	{
		$this->db->select("*");
		$this->db->where("customer_id = '".$customer_id."'");
		$data = $this->db->get("s_requests");
		$result = $data->result_array();
		if(empty($result)){
			return array();
		}
		return $result;
	}

	 public function totalSubUserunderSAAS($userid,$role){ 
    	$this->db->select("count(su.id) as total_users");  
    	$this->db->from("s_users as su"); 
    	$this->db->where('su.parent_id= "'.$userid.'" && role="'.$role.'"');
    	$data= $this->db->get();
//	 echo $this->db->last_query().'<br>'; exit;
    	$result =  $data->result_array();  
    	return $result;
    }

}
