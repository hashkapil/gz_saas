<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class S_users_model extends CI_Model {	
    
    public function get_Susers($role="",$active="",$loginUser=""){
        $this->db->select("su.*");
        $this->db->from('s_users as su');
        if($role!=""){
        $this->db->where("su.role = '" . $role . "'");
        }
        if($active){
         $this->db->where("su.is_active = 1");
        }
        $data = $this->db->get();
      // echo $this->db->last_query(); exit; 
        $result = $data->result_array();
        //if($result){
        return $result;
        //}
    }
    
    public function getalsaasubscription(){
        $this->db->select("*");
        $this->db->from("s_subscription_plan");
        $this->db->where("is_active",1);
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }

}