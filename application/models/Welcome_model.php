<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome_model extends CI_Model {		

	public function set_online_to_offline()
	{
		$sql = "select * from users where online = 1";
		$query = $this->db->query($sql);
		$result = $query->result_array(); 
		return $result;
	}

	public function insert_data($table,$value)
	{
		$data = $this->db->insert($table,$value);
		$insert_id = $this->db->insert_id();
//         echo $this->db->last_query()."<br>";
        //   exit;
		return  $insert_id;
	
	}
        
	public function get_protfolio_category()
	{
		$sql = "select * from protfolio";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}
        
	public function get_blog_category()
	{
		$sql = "select * from blog order by id desc";
		$query = $this->db->query($sql);
//                echo $this->db->last_query();exit;
		$result = $query->result_array(); 
		return $result; 
	}
        
	public function get_style_suggestion_category($id)
	{
		$sql = "select * from style_suggestion";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}
        
	public function update_data($table,$data,$where)
	{
		foreach($where as $key=>$value){
			$this->db->where($key,$value);
		}		
		$data = $this->db->update($table, $data); 
//               echo $this->db->last_query().exit;
		return $data;
	}
	
	public function update_online()
	{
		$this->update_data("users",array("online"=>1,"last_update"=>date("Y:m:d H:i:s")),array("id"=>$_SESSION['user_id']));
	}

	public function priority($id,$table="")
	{
            if($table == ""){
                $table = "priority";
            }
            $sql = "select max(priority) as ".$table." from requests where customer_id =".$id." AND status= 'assign' ";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
	}
        
        public function sub_user_priority($id,$table="")
	{
            if($table == ""){
                $table = "sub_user_priority";
            }
            $sql = "select max(sub_user_priority) as ".$table." from requests where created_by =".$id." AND status= 'assign' ";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result;
	}
        
        
        
	public function delete_data($table,$id)
	{
	   $this->db->where('id', $id);
	   $del_entry = $this->db->delete($table); 
	   return $del_entry;
	}
	
	public function get_blog_by_id($id){
		$this->db->where("slug='".$id."'");
		$data = $this->db->get('blog');
		$result = $data->result_array();
		return $result;
	}
        public function get_blog_by_cat($catname){
		$sql = "select * from blog where category ='$catname'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;
	}
	public function last_index()
	{
		$sql = "SELECT index_number FROM requests ORDER BY id DESC LIMIT 1";
		$query = $this->db->query($sql);
		$result = $query->result_array(); 
		return $result;
	}
        public function getsubscriptionlist(){
            
            $this->db->select("*");
            $this->db->where("is_active = '1'");
			$this->db->order_by("position", "asc");
            $data = $this->db->get("subscription_plan");
            $result = $data->result_array();
            return $result;
                
        }
        
        
public function login() {
        $user_name = $this->input->post('email');
        $password = $this->input->post('password');
        $this->db->where('email', $user_name);
        $this->db->where('new_password', md5($password));
        $data = $this->db->get('users');
        $result = $data->result();
        $data = $data->num_rows();
        $saasuser = ($result[0]->is_saas == 1) ? 1 : 0;
        if($saasuser == 1){
        if ($data != 0) {
//            die("users");
            if($result[0]->is_active == 1){
                $user_id = $result[0]->id;
                $role_id = $result[0]->role;
                $this->Welcome_model->update_data("users", array("is_logged_in" => 1,"lastlogin_date" => date("Y:m:d H:i:s")), array("id" => $user_id));
                $this->session->set_userdata('user_id', $user_id);
                $this->session->set_userdata('role', $role_id);
                $this->session->set_userdata('timezone', $result[0]->timezone);
                $this->session->set_userdata('first_name', $result[0]->first_name);
                $this->session->set_userdata('last_name', $result[0]->last_name);
                $this->session->set_userdata('email', $result[0]->email);
                $this->session->set_userdata('tour', $result[0]->tour);
                $this->session->set_userdata('is_saas', $result[0]->is_saas);
                $this->session->set_userdata('qa_id', $result[0]->qa_id);
                $url = explode('=', $this->input->post('url'));
                $redirect_url = $url[1];
            if($result[0]->timezone == ''){
                $ip = $_SERVER['REMOTE_ADDR'];
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://ip-api.com/json/" . $ip,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache",
                        "postman-token: 92b0963f-d661-849d-9ca9-734180411906"
                    ),
                ));
                $ipInfo = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
                $ipInfo = json_decode($ipInfo);
                $timezone = $ipInfo->timezone;
                $this->session->set_userdata('timezone', $timezone);
                $this->update_data("users",array("timezone"=>$timezone),array("id"=>$user_id));
        }
        if($redirect_url){
                redirect(base_url().$redirect_url);
        }
        if($role_id == "admin"){
                redirect(base_url()."admin/dashboard");
        }
        if($role_id == "customer" && ($saasuser == 0)){
            redirect(base_url()."customer/request/design_request");
        }
        if($role_id == "designer"){
                redirect(base_url()."designer/request");
        }
        if($role_id == "qa"){
                redirect(base_url()."qa/dashboard");
        }
        if($role_id == "va"){
                redirect(base_url()."va/dashboard");
        }
        if(($role_id == "customer") && ($saasuser == 1)){
          redirect(base_url()."account/dashboard");   
        }
        }else {
             $this->session->set_flashdata('message_error', "Your Account is Deactivated", 5);
            redirect(base_url()."login");
        }}else{
            $this->session->set_flashdata('message_error', 'You have provided invalid username or password!', 5);
            redirect(base_url()."login");
        }
        }else{
//             die("s_users");
        $user_name_s = $this->input->post('email');
        $password_s = $this->input->post('password');
        $this->db->where('email', $user_name_s);
        $this->db->where('new_password', md5($password_s));
        $data_s = $this->db->get('s_users');
//      echo $this->db->last_query();exit;
        $result_s = $data_s->result();
//       echo "<pre/>";print_R($result_s);exit;
        $data_s = $data_s->num_rows();
        if($data_s != 0){
            if($result_s[0]->is_active == 1){
//                echo "<pre/>";print_R($result_s[0]);exit;
                $user_id = $result_s[0]->id;
                $role_id = $result_s[0]->role;
                $this->Welcome_model->update_data("s_users", array("is_logged_in" => 1,"lastlogin_date" => date("Y:m:d H:i:s")), array("id" => $user_id));
                $this->session->set_userdata('user_id', $user_id);
                $this->session->set_userdata('role', $role_id);
                $this->session->set_userdata('timezone', $result_s[0]->timezone);
                $this->session->set_userdata('first_name', $result_s[0]->first_name);
                $this->session->set_userdata('last_name', $result_s[0]->last_name);
                $this->session->set_userdata('email', $result_s[0]->email);
                $this->session->set_userdata('tour', $result_s[0]->tour);
                $this->session->set_userdata('is_saas', 0);
                $this->session->set_userdata('qa_id', $result_s[0]->qa_id);
                $url = explode('=', $this->input->post('url'));
                $redirect_url = $url[1];
            if($result_s[0]->timezone == ''){
                $ip = $_SERVER['REMOTE_ADDR'];
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://ip-api.com/json/" . $ip,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache",
                        "postman-token: 92b0963f-d661-849d-9ca9-734180411906"
                    ),
                ));

                $ipInfo = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);
                if ($err) {
                    //echo "cURL Error #:" . $err;
                }
                $ipInfo = json_decode($ipInfo);
                $timezone = $ipInfo->timezone;
                $this->session->set_userdata('timezone', $timezone);
                $this->update_data("users",array("timezone"=>$timezone),array("id"=>$user_id));
        }
//       echo "<pre/>";print_R($result_s);exit;
//        echo $role_id;exit;
        if($redirect_url){
                redirect(base_url().$redirect_url);
        }
        if($role_id == "admin"){
                redirect(base_url()."admin/dashboard");
        }
        if($role_id == "designer"){
                redirect(base_url()."account/dashboard");
        }
        if($role_id == "qa"){
                redirect(base_url()."qa/dashboard");
        }
        if($role_id == "va"){
                redirect(base_url()."va/dashboard");
        }
        if($role_id == "customer" || $role_id == "manager" || $role_id == "designer"){
//            die($role_id);
           redirect(base_url()."account/dashboard");  
        }
            }else {
            $this->session->set_flashdata('message_error', "Your Account is Deactivated", 5);
           redirect(base_url()."login");
        }
        }else{
            $this->session->set_flashdata('message_error', 'You have provided invalid username or password!', 5);
            redirect(base_url()."login");
        }
    }
    }
	
	public function logout() 
	{
        $this->session->sess_destroy();
	}
	
		public function getuserbyid($id)
	{
                    $this->db->select("*");
		//$this->db->select("id,first_name,last_name");
		$this->db->where("id = '".$id."'");
		$data = $this->db->get("users");
		$result = $data->result_array();
		return $result;
	}
        /**********sharing function*************/
            public function getpermissionsbySherekey($sharekey){
                $saas_share =  $this->get_table_bySherekey($sharekey);
                if($saas_share == 'link_sharing'){
                   $linkshrtb = 'link_sharing'; 
                   $linkshrprmstb = 'link_sharing_permissions'; 
                }else{
                   $linkshrtb = 's_link_sharing'; 
                   $linkshrprmstb = 's_link_sharing_permissions'; 
                }
                $this->db->select("ls.request_id,ls.share_key,ls.user_name,ls.draft_id,ls.is_disabled,ls.share_type,ls.email,lsp.mark_as_completed,lsp.download_source_file,lsp.commenting");  
                $this->db->where("share_key = '".$sharekey."'");
                $this->db->join($linkshrprmstb. ' as lsp', 'lsp.sharing_id = ls.id','left');
                $data = $this->db->get("$linkshrtb as ls");
//              echo $this->db->last_query();exit;
                $result = $data->result_array();
                return $result;
            }
            
            public function get_recommended_blogData($recomIds = NULL){
                if($recomIds != ''){
                        $this->db->select("title,image,slug");
                        $this->db->where("(`id` IN ($recomIds))");
                        $data = $this->db->get("blog");
                        $result = $data->result_array();
                        return $result;
            }
            }
            
        public function portfolio_load_more($category = "", $allCountOnly = false, $start = 0, $limit = 0){
            $this->db->select("p.*");
            if($category != 0){
             $this->db->where('category', $category);
            }
            $this->db->from('protfolio as p');
            if(!$allCountOnly){
                $limit = ($limit == 0) ? LIMIT_ADMIN_LIST_COUNT : $limit;
                $this->db->limit($limit, $start);
            }
            $data = $this->db->get();
    //        echo $this->db->last_query();
            if($allCountOnly){
                return $data->num_rows();
            }
            $result = $data->result_array();
               if($result){
                    return $result;
               }
    }
    
    /***********************subdomain functions *************/
    
    public function subdomainlogin($domain_id,$user_timezone = NULL) 
    {
        $user_name = $this->input->post('email');
        $password = $this->input->post('password');
        if($domain_id != ''){
		$this->db->where('email', $user_name);
		$this->db->where('new_password = "'.md5($password).'" and (parent_id = '.$domain_id.' or id = '.$domain_id.')');
               // $this->db->where("parent_id = $domain_id or id = $domain_id");
                $data = $this->db->get('users');
               // echo $this->db->last_query();exit;
		$result = $data->result();
		$data = $data->num_rows();
        }else{
            $data = 0;
        }
		if ($data != 0) {
                $url = explode('=', $this->input->post('url'));
                $redirect_url = $url[1];
			if($result[0]->is_active == 1){
                            $role_id = $result[0]->role;
                               $this->setsessionforloginuser($result,$role_id,$user_timezone);
                               $user_info_data = $this->Admin_model->getextrauser_data($result[0]->id);
                               //echo "<pre>";print_r($user_info_data);exit;
                               if($result[0]->user_flag == "client"){
                                   $useroptions_data = $this->Request_model->getAgencyuserinfo($result[0]->parent_id);
                                   if($useroptions_data[0]['stripe_api_key'] == "" || $result[0]->plan_name == ""){
                                       $this->session->set_flashdata('message_error', "An error occured. Please contact admin/support for help.", 5);
                                        redirect(base_url());
                                   }
                               }
                               if ($user_info_data[0]['bypass_payment'] == 1) {
                                        return $result[0]->id;
                                    } else {
                                        if ($role_id == "customer") {
                                            //echo $role_id;
                                            if ($redirect_url) {
                                                //echo $redirect_url;exit;
                                                redirect(base_url() . $redirect_url);
                                            }
                                            redirect(base_url() . "customer/request/design_request");
                                        }
                                    }
                               } else {
				$this->session->set_flashdata('message_error', "Your Account is Deactivated", 5);
                               redirect(base_url());
			}
		}else{
			$this->session->set_flashdata('message_error', 'You have provided invalid username or password!', 5);
			redirect(base_url());
		}
	}
        
        
     public function setsessionforloginuser($result,$role_id,$ipInfo) {
        $user_id = $result[0]->id;
        $this->session->set_userdata('user_id', $user_id);
        $this->session->set_userdata('role', $role_id);
        $this->session->set_userdata('timezone', $result[0]->timezone);
        $this->session->set_userdata('first_name', $result[0]->first_name);
        $this->session->set_userdata('last_name', $result[0]->last_name);
        $this->session->set_userdata('email', $result[0]->email);
        $this->session->set_userdata('tour', $result[0]->tour);
        $this->session->set_userdata('qa_id', $result[0]->qa_id);
        //$this->session->set_flashdata('message_success', 'Login Success!', 5);
        if ($result[0]->timezone == '') {
            $timezone = $ipInfo->timezone;
            $this->session->set_userdata('timezone', $timezone);
            $this->update_data("users", array("timezone" => $timezone), array("id" => $user_id));
        }
    }
    public function getuserStatebyCustomer_id($id){
         $this->db->select("id,state");
         $this->db->where('customer_id', $id);
         $this->db->from('users');
         $data = $this->db->get();
         $result = $data->result_array();
         //echo "<pre/>";print_r($state);
         return $result[0]['state'];
    }
    
    public function getTaxamntofuser($custID){
        $this->db->select("tax_amount");
         $this->db->where('user_id=', $custID);
        $this->db->from('sales_tax');
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }
    
     public function select_data($select,$table,$where="",$limit="",$start="",$order="",$orderby="")
        {
               $this->db->select($select);
               $this->db->from($table);
               if($orderby=="" && $order ==""){
               $this->db->order_by("id","asc");
               }else{
                  $this->db->order_by($orderby,$order); 
               }
               if($where!=""){
                 $this->db->where($where);
               }
                if($limit!="" && $start!=""){
                $this->db->limit($limit, $start);
                }
               $sql = $this->db->get(); 
            //echo  $this->db->last_query(); exit; 
               $result = $sql->result_array();
           return $result;
        }
        
    public function getsaas_subscriptions($id="") {
        $this->db->select("*");
        if($id != ""){
          $this->db->where("plan_id", $id);    
        }
        $this->db->where("is_active", 1);   
        $this->db->from("s_subscription_plan");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result; 
    }
    
    /**********SAAS link sharing **************/
    public function get_table_bySherekey($sharekey){
        $this->db->select("ls.id");  
        $this->db->where("share_key = '".$sharekey."'");
        $data = $this->db->get("link_sharing as ls");
        $result = $data->result_array();
        if($result){
            return 'link_sharing';
        }else{
            $this->db->select("ls.id");  
            $this->db->where("share_key = '".$sharekey."'");
            $data = $this->db->get("s_link_sharing as ls");
            $result = $data->result_array();
            if($result){
             return 's_link_sharing';       
            }
        }
       
        
    }
}
