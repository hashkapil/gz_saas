<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Request_model extends CI_Model {
    
    public function checkValidUser($reqID,$role = NULL,$userID = NULL){
        //echo "<pre/>";print_r($_SESSION);
        $created_user = $this->load->get_var('main_user');
        $uID = isset($userID) ? $userID : $_SESSION['user_id'];
        $role = isset($role) ? $role : $_SESSION['role'];
        $this->db->select("*");
        if($role == 'customer'){
        $this->db->where("requests.customer_id = '" . $created_user . "'");
        }elseif($role == 'designer'){
        $this->db->where("requests.designer_id = '" . $created_user . "'");
        }
        $this->db->where("requests.id = '" . $reqID . "'");
        $data = $this->db->get("requests");
        //echo $this->db->last_query();exit; 
        $result = $data->result_array();
        //echo "<pre/>";print_r($result);exit;
        if(!empty($result)){
            return true;
        }
    }

    public function getallactive_request() {
        $this->db->select("*");
        $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
        $this->db->where("status!='approved'");
        $this->db->where("status!='draft'");
        $this->db->order_by("index_number", "ASC");
        $data = $this->db->get("requests");
        $result = $data->result_array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
            }
        }
        return $result;
    }
    
    public function get_inqueuereq_by_customerID($uid){
        $this->db->select("*");
        $this->db->where('status', 'assign');
        $this->db->where("customer_id = '" . $uid . "'");
        $this->db->order_by('priority', "ASC");
        $data = $this->db->get("requests");
        //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }

    public function getalldraft_request_count($brand_id = "",$client_id = "") {
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $this->db->select("*");
        if ($brand_id != '') {
        $this->db->where("requests.brand_id = '" . $brand_id . "'" );
        }
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        $login_user_data = $this->load->get_var('login_user_data');
        if ($client_id != '') {
        $this->db->where("requests.created_by = '" . $client_id . "'" );
           if($login_user_data[0]['user_flag'] == 'manager' && !empty($sub_usersbrands)){
                //if($sub_usersbrands != 'all'){
                $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0))");
               // }
            }
        }else{
        if(!empty($sub_usersbrands)){
          $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0 and requests.created_by='".$login_user_id."'))");
            
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
           
            $this->db->where("requests.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("sub_user_priority", "ASC");
            
        }
        }
        $this->db->where("requests.customer_id = '" . $created_user . "'");
        $this->db->where("status='draft'");
        $this->db->where("dummy_request != 1");
        $this->db->order_by("index_number", "ASC");
        $num_rows = $this->db->count_all_results('requests');
       // echo $this->db->last_query();exit;
        return $num_rows;
    }
    
    public function delete_request_file($table, $id, $filename) {
        $this->db->where('request_id', $id);
        $this->db->where('file_name', $filename);
        $result = $this->db->delete($table);
        //$this->db->last_query();exit;
        return $result;
        }

    public function getalldraft_request($brand_id = "",$client_id = "") {
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $this->db->select("*");
        if ($brand_id != '') {
        $this->db->where("requests.brand_id = '" . $brand_id . "'" );
        }
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        $login_user_data = $this->load->get_var('login_user_data');
        if ($client_id != '') {
        $this->db->where("requests.created_by = '" . $client_id . "'" );
           if($login_user_data[0]['user_flag'] == 'manager' && !empty($sub_usersbrands)){
               // if($sub_usersbrands != 'all'){
                $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0))");
               // }
            }
        }else{
        if(!empty($sub_usersbrands)){
            $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0 and requests.created_by='".$login_user_id."'))");
            
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
           
            $this->db->where("requests.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("sub_user_priority", "ASC");
            
        }
        }
        $this->db->where("requests.customer_id = '" . $created_user . "'");
        $this->db->where("status='draft'");
        $this->db->where("dummy_request != 1");
        $this->db->order_by("index_number", "ASC");
        $this->db->limit(LIMIT_CUSTOMER_LIST_COUNT, 0);
        $data = $this->db->get("requests");
        
        $result = $data->result_array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
            }
        }
        return $result;
    }

    public function getalldraft_request_pagination($start, $limit) {
        $this->db->select("*");
        $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
        $this->db->where('status', 'draft');
        $this->db->order_by("index_number", "ASC");
        $this->db->limit($limit, $start);
        $data = $this->db->get("requests");
        $result = $data->result_array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
            }
        }
        return $result;
    }

    public function getallcomplete_request_pagination($start, $limit) {
        $this->db->select("*");
        $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
        $this->db->where("status='approved'");
        $this->db->order_by("index_number", "ASC");
        $this->db->limit($limit, $start);
        $data = $this->db->get("requests");
        $result = $data->result_array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
            }
        }
        return $result;
    }

    public function getallcomplete_request_count($brand_id = "",$client_id = "") {
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $this->db->select("*");
        if($brand_id != ''){
         $this->db->where("requests.brand_id = '" . $brand_id . "'");   
        }
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        $login_user_data = $this->load->get_var('login_user_data');
        if ($client_id != '') {
        $this->db->where("requests.created_by = '" . $client_id . "'" );
            if($login_user_data[0]['user_flag'] == 'manager' && !empty($sub_usersbrands)){
               // if($sub_usersbrands != 'all'){
                $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0))");
               // }
            }
        }else{
        if(!empty($sub_usersbrands)){
            $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0 and requests.created_by='".$login_user_id."'))");
            
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
           
            $this->db->where("requests.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("sub_user_priority", "ASC");
            
        }
        }
        $this->db->where("requests.customer_id = '" . $created_user . "'");
        $this->db->where("status='approved'");
        $this->db->order_by("index_number", "ASC");
        $num_rows = $this->db->count_all_results('requests');
        
        return $num_rows;
    }

    public function getallcomplete_request($brand_id = "",$status = NULL,$client_id = "") {
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $this->db->select("*");
        if($brand_id != ''){
         $this->db->where("requests.brand_id = '" . $brand_id . "'");   
        }
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        $login_user_data = $this->load->get_var('login_user_data');
        if ($client_id != '') {
        $this->db->where("requests.created_by = '" . $client_id . "'" );
            if($login_user_data[0]['user_flag'] == 'manager' && !empty($sub_usersbrands)){
               // if($sub_usersbrands != 'all'){
                $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0))");
              //  }
            }
        }else{
        if(!empty($sub_usersbrands)){
            $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0 and requests.created_by='".$login_user_id."'))");
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
           
            $this->db->where("requests.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("sub_user_priority", "ASC");
            
        }
        }
        $this->db->where("requests.customer_id = '" . $created_user . "'");
        $this->db->where("status='approved'");
        if($status == 'approved'){
          $this->db->order_by("approvaldate", "DESC");  
        }else{
            $this->db->order_by("index_number", "ASC");
        }
        $this->db->limit(LIMIT_CUSTOMER_LIST_COUNT, 0);
        $data = $this->db->get("requests");
        
        $result = $data->result_array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
            }
        }
        return $result;
    }
    
    public function getallcancel_request_count($brand_id = "",$client_id = "") {
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $this->db->select("*");
        if($brand_id != ''){
         $this->db->where("requests.brand_id = '" . $brand_id . "'");   
        }
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        $login_user_data = $this->load->get_var('login_user_data');
        if ($client_id != '') {
        $this->db->where("requests.created_by = '" . $client_id . "'" );
            if($login_user_data[0]['user_flag'] == 'manager' && !empty($sub_usersbrands)){
               // if($sub_usersbrands != 'all'){
                $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0))");
               // }
            }
        }else{
        if(!empty($sub_usersbrands)){
           // $this->db->where_in('requests.brand_id', $sub_usersbrands);
            $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0 and requests.created_by='".$login_user_id."'))");
            
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
           
            $this->db->where("requests.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("sub_user_priority", "ASC");
            
        }
        }
        $this->db->where("requests.customer_id = '" . $created_user . "'");
        $this->db->where("status='cancel'");
        $this->db->order_by("index_number", "ASC");
        $num_rows = $this->db->count_all_results('requests');
        return $num_rows;
    }

    public function getallcancel_request($brand_id = "",$status = NULL,$client_id = "") {
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $this->db->select("*");
        if($brand_id != ''){
         $this->db->where("requests.brand_id = '" . $brand_id . "'");
        }
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        $login_user_data = $this->load->get_var('login_user_data');
        if ($client_id != '') {
        $this->db->where("requests.created_by = '" . $client_id . "'" );
            if($login_user_data[0]['user_flag'] == 'manager' && !empty($sub_usersbrands)){
               // if($sub_usersbrands != 'all'){
                $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0))");
               // }
            }
        }else{
        if(!empty($sub_usersbrands)){
          //  $this->db->where_in('requests.brand_id', $sub_usersbrands);
            $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0 and requests.created_by='".$login_user_id."'))");
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
           
            $this->db->where("requests.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("sub_user_priority", "ASC");
            
        }
        }
        $this->db->where("requests.customer_id = '" . $created_user . "'");
        $this->db->where("status='cancel'");
        if($status == 'cancel'){
          $this->db->order_by("modified", "ASC");  
        }else{
            $this->db->order_by("index_number", "ASC");
        }
        $this->db->limit(LIMIT_CUSTOMER_LIST_COUNT, 0);
        $data = $this->db->get("requests");
        $result = $data->result_array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
            }
        }
        return $result;
    }

    public function getall_request_designer($customer_id = null, $status = array(), $designer_id = "", $orderby = "") {
//       echo "<pre>";var_dump($status);
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where("requests.customer_id = '" . $customer_id . "'");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id = '" . $designer_id . "'");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }
        
        if (!empty($status)) {
            $newStatus = implode("','",$status);
            $newStatus =  "'".$newStatus."'";
            $this->db->where("status_designer IN (" . $newStatus . ")");
        }
        if ($orderby == "") {
           // $this->db->order_by("DATE(created)", "ASC");
            if($status[0] == 'active' || $status[1] == 'disapprove'){
                $this->db->order_by("latest_update","ASC");
                }elseif($status[0] == 'assign'){
                $this->db->order_by("priority","ASC");
                }elseif ($status[0] == "pendingrevision") {
                $this->db->order_by("modified", "ASC");
                } elseif ($status[0] == 'checkforapprove') {
                $this->db->order_by("modified","ASC");
                }
               elseif ($status[0] == "approved") {
               $this->db->order_by("approvaldate", "DESC");
               }else {
                    $this->db->order_by("modified", "DESC");
                }
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo "<pre>";print_r($status);
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }
    
     public function get_user_by_plan_name($planid) {
          $this->db->select('*');
        $this->db->where('plan_name', $planid);
        $data = $this->db->get('users');
        $result = $data->result_array();
       // echo  $this->db->last_query(); 
        return $result;
    }

    public function getall_request_count($customer_id = null, $status = array(), $designer_id = "", $orderby = "",$brand_id = "",$client_id="") {
       
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $login_user_id = $this->load->get_var('login_user_id');
        $login_user_data = $this->load->get_var('login_user_data');
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
       // echo $client_id;exit;
        //echo "<pre>";print_r($login_user_data);
        $this->db->select("*");
        
        if ($brand_id != '') {
        $this->db->where("brand_id = '" . $brand_id . "'" );
        }
        if ($client_id != '') {
     //  echo "created_by = '" . $client_id . "'<br>";
            $this->db->where("created_by = '" . $client_id . "'" );
            if($login_user_data[0]['user_flag'] == 'manager' && !empty($sub_usersbrands)){
           //    echo "((brand_id IN (".implode(',',$sub_usersbrands).")) or (brand_id = 0))";
               // if($sub_usersbrands != 'all'){
                $this->db->where("((brand_id IN (".implode(',',$sub_usersbrands).")) or (brand_id = 0))");
             //   }
            }
          //  exit;
        }else{
        if(!empty($sub_usersbrands)){
            $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0 and requests.created_by='".$login_user_id."'))");
            
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
           
            $this->db->where("requests.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("sub_user_priority", "ASC");
            
        }
        }
        if ($customer_id) {
            $this->db->where("requests.customer_id = '" . $customer_id . "'");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id = '" . $designer_id . "'");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }

        /* if(count($status)>0){
          }else{ */
        $status = "'draft','cancel','approved','hold'";
        //  }

        if (!empty($status)) {
            $this->db->where("status NOT IN (" . $status . ")");
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(created)", "ASC");
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $num_rows = $this->db->count_all_results('requests');
        
       // echo $this->db->last_query();exit;
        return $num_rows;

        //echo $this->db->last_query();
    }
    public function getall_request_for_trial_user($customer_id = null){
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where("requests.customer_id = '" . $customer_id . "'" );            
        }
        $data = $this->db->get("requests");
        $result = $data->result_array();
        return $result;
        
    }
    
    public function getall_request_for_all_status($customer_id = null,$billing_start_date = null,$billing_end_date = null,$createdby = null){
        $this->db->select("*");
        if ($customer_id) {
            if($createdby != ''){
               $this->db->where("requests.created_by = '" . $customer_id . "'" );  
            }else{
            $this->db->where("requests.customer_id = '" . $customer_id . "'" );
            if($billing_start_date != null && $billing_end_date != null){
            $this->db->where("requests.created >= '" . $billing_start_date . "'" );     
            $this->db->where("requests.created <= '" . $billing_end_date . "'" );  
            } 
            }
            $this->db->where('requests.dummy_request', '0');
        }
        $data = $this->db->get("requests");
        $result = $data->result_array();
        return $result;
        
    }
            

    public function getall_request($customer_id = null, $status = array(), $designer_id = "", $orderby = "",$brand_id = "",$client_id = "") {
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $login_user_data = $this->load->get_var('login_user_data');
        $this->db->select("*");
        if ($brand_id != '') { 
        $this->db->where("requests.brand_id = '" . $brand_id . "'" );
        }
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        if ($client_id != '') {
        $this->db->where("requests.created_by = '" . $client_id . "'" );
            if($login_user_data[0]['user_flag'] == 'manager' && !empty($sub_usersbrands)){
            //    if($sub_usersbrands != 'all'){
                $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0))");
            //    }
            }
        }else{
        if(!empty($sub_usersbrands)){
            $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0 and requests.created_by='".$login_user_id."'))");
            
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
           
            $this->db->where("requests.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("sub_user_priority", "ASC");
            
        }
        }
        if ($customer_id) {
            $this->db->where("requests.customer_id = '" . $customer_id . "'" );
            $this->db->order_by("priority", "ASC");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id = '" . $designer_id . "'");
        } else {
            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $created_user . "'");
            }
        }
        $status = "'draft','cancel','approved','hold'";
        if (!empty($status)) {
            $this->db->where("status NOT IN (" . $status . ")");
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(created)", "DESC");
        } else {
            $this->db->order_by($orderby, "DESC");
        }
        $this->db->limit(LIMIT_CUSTOMER_LIST_COUNT, 0);
        $data = $this->db->get("requests");
        
        $result = $data->result_array();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
                $result[$i]['is_trail'] = $customer[0]['is_trail'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
                $result[$i]['is_trail'] = "";
            }
        }
        return $result;
    }
    
    
    /****************************start my new functions*******************************************/
    
        public function customer_load_more($status, $customer_id, $allCountOnly = false, $start = 0, $limit = 0, $orderby = "",$search="",$brand_id = "",$client_id = "") {
        $login_user_id = $this->load->get_var('login_user_id');
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        
        $this->db->select("r.*,d.first_name as designer_first_name, d.last_name as designer_last_name, d.profile_picture as profile_picture,"
           . "c.first_name as customer_first_name, c.last_name as customer_last_name, c.profile_picture as customer_profile_picture, c.plan_turn_around_days as plan_turn_around_days");
        $this->db->from('requests as r');
        $this->db->join('users as d', 'd.id = r.designer_id', 'LEFT');
        $this->db->join('users as c', 'c.id = r.customer_id', 'LEFT');
        $this->db->where_in('r.status', $status);
        $this->db->where("r.customer_id = '" . $customer_id . "'");
        if ($brand_id != '') {
        $this->db->where("r.brand_id = '" . $brand_id . "'" );
        }
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        $login_user_data = $this->load->get_var('login_user_data');
        if ($client_id != '') {
        $this->db->where("r.created_by = '" . $client_id . "'" );
            if($login_user_data[0]['user_flag'] == 'manager' && !empty($sub_usersbrands)){
              //  if($sub_usersbrands != 'all'){
                $this->db->where("((r.brand_id IN (".implode(',',$sub_usersbrands).")) or (r.brand_id = 0))");
              //  }
            }
        }else{
        if(!empty($sub_usersbrands)){
            $this->db->where("((r.brand_id IN (".implode(',',$sub_usersbrands).")) or (r.brand_id = 0 and r.created_by='".$login_user_id."'))");
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
           
            $this->db->where("r.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("r.sub_user_priority", "ASC");
            
        }
        }
        if($status == 'draft'){
            $this->db->where('r.dummy_request', '0');
        }
        $this->db->group_by('r.id');
        if($search){
            $this->db->group_start();
            $this->db->or_like('r.title', $search, 'both');
            $this->db->or_like('r.description', $search, 'both');
            $this->db->or_like('d.first_name', $search, 'both');
            $this->db->or_like('d.last_name', $search, 'both');
            //$this->db->or_like('d.email', $search, 'both');
            $this->db->group_end();
        }
        if(!$allCountOnly){
            $limit = ($limit == 0) ? LIMIT_CUSTOMER_LIST_COUNT : $limit;
            $this->db->limit($limit, $start);
            if($status == 'approved'){
               $this->db->order_by("DATE(approvaldate)", "DESC");  
            }
            if ($orderby == "") {
                $this->db->order_by("DATE(created)", "ASC");
            }else {
                $this->db->order_by($orderby, "ASC");
            }
        }
        $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
        if($result){
            if($allCountOnly){
               return count($result); 
            } else{
                return $result;
            }
        }
    }
    
    public function designer_load_more($status, $designer_id, $allCountOnly = false, $start = 0, $limit = 0, $orderby = "",$search="") {
        if($allCountOnly){
            $this->db->select("count(*) as total_count");   
        } else{
            $this->db->select("r.*, c.first_name as customer_first_name,c.timezone as user_timezone, c.last_name as customer_last_name,c.plan_name as current_plan_name, c.is_trail as is_trail,c.profile_picture as customer_profile_picture, c.plan_turn_around_days as plan_turn_around_days,e.first_name as sub_first_name, e.last_name as sub_last_name");
        }
        $this->db->from('requests as r');
        $this->db->join('users as c', 'c.id = r.customer_id', 'LEFT');
        $this->db->join('users as e', 'e.id = r.created_by', 'LEFT');
        $this->db->where_in('r.status_designer', $status);
        $this->db->where("r.designer_id = '" . $designer_id . "'");
        $this->db->where('r.dummy_request', '0');
        if(!$allCountOnly){
            $this->db->group_by('r.id');
        }
        if($search){
            $this->db->group_start();
            $this->db->or_like('r.title', $search, 'both');
            $this->db->or_like('r.description', $search, 'both');
            $this->db->or_like('c.first_name', $search, 'both');
            $this->db->or_like('c.last_name', $search, 'both');
            //$this->db->or_like('d.email', $search, 'both');
            $this->db->group_end();
        }
        if(!$allCountOnly){
            $limit = ($limit == 0) ? LIMIT_DESIGNER_LIST_COUNT : $limit;
            $this->db->limit($limit, $start);
            if ($orderby == "") {
                //$this->db->order_by("DATE(r.dateinprogress)", "ASC");
                   $this->db->order_by("DATE(r.expected_date)", "ASC");
           $this->db->order_by("DATE(r.latest_update)", "ASC");
            } else {
                $this->db->order_by($orderby, "ASC");
            }
        }
        $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
        if($result){
            if($allCountOnly){
               return $result[0]['total_count']; 
            } else{
                return $result;
            }
        }
    }

	public function qa_load_more($status, $allQACustomers, $allCountOnly = false, $start = 0, $limit = 0, $orderby = "",$search="",$buckettype="") {
        if(empty($allQACustomers)){
                return $allCountOnly ? 0 : array();
        }
        $this->db->select("r.*,d.first_name as designer_first_name, d.timezone as user_timezone,d.last_name as designer_last_name, d.profile_picture as profile_picture,"
            . "c.is_trail as is_trail,c.first_name as customer_first_name,c.plan_name as current_plan_name, c.last_name as customer_last_name, c.profile_picture as customer_profile_picture, c.plan_turn_around_days as plan_turn_around_days,e.first_name as sub_first_name, e.last_name as sub_last_name");
        $this->db->from('requests as r');
        $this->db->join('users as d', 'd.id = r.designer_id', 'LEFT');
        $this->db->join('users as c', 'c.id = r.customer_id', 'LEFT');
        $this->db->join('users as e', 'e.id = r.created_by', 'LEFT');
        $this->db->where_in('r.status_qa', $status);
        $this->db->where_in('r.customer_id', $allQACustomers);
        $this->db->where('r.dummy_request', '0');
        if($buckettype){
            $this->db->where('r.category_bucket',$buckettype); 
        }
        $this->db->group_by('r.id');
        if($search){
            $this->db->group_start();
            $this->db->or_like('r.title', $search, 'both');
            $this->db->or_like('r.description', $search, 'both');
            $this->db->or_like('c.first_name', $search, 'both');
            $this->db->or_like('c.last_name', $search, 'both');
            $this->db->or_like('d.first_name', $search, 'both');
            $this->db->or_like('d.last_name', $search, 'both');
            //$this->db->or_like('d.email', $search, 'both');
            $this->db->group_end();
        }
        if(!$allCountOnly){
            $limit = ($limit == 0) ? LIMIT_QA_LIST_COUNT : $limit;
            $this->db->limit($limit, $start);
            if ($orderby == "") {
                if ($status[0] == "approved") {
                    $this->db->order_by("approvaldate", "DESC");
                } elseif ($status[0] == "pendingrevision") {
                    $this->db->order_by("modified", "DESC");
                } elseif ($status[0] == "checkforapprove") {
                    $this->db->order_by("modified", "DESC");
                }elseif ($status[0] == "assign") {
                    $this->db->order_by("priority", "ASC");
                } else {
                     $this->db->order_by("expected_date", "ASC");
           $this->db->order_by("latest_update", "ASC");
                }
            } else {
                $this->db->order_by($orderby, "ASC");
            }
        }
        $data = $this->db->get();
//        echo $this->db->last_query();exit;
        if($allCountOnly){
            return $data->num_rows();
        }else{
            $result = $data->result_array();
            if($result){
                return $result;
            }
        }
    }

        public function admin_load_more($status, $allCountOnly = false, $start = 0, $limit = 0, $orderby = "",$search="",$buckettype="",$type="",$va_projects="",$client_id='',$designer_id='') {
        $login_user_id = $this->load->get_var('login_user_id');
        if ($type !="" && $type == "download") {
            $this->db->select("r.id,r.title,r.description,r.category,r.status_admin as status,r.created as created_date,r.expected_date,r.dateinprogress as inprogress_date,r.approvaldate as approval_date,d.first_name as designer_first_name,d.last_name as designer_last_name,"
                    . "c.first_name as customer_first_name, c.last_name as customer_last_name,e.first_name as sub_first_name, e.last_name as sub_last_name");
        }else {
            $this->db->select("r.*,d.first_name as designer_first_name, d.timezone as user_timezone,d.last_name as designer_last_name, d.profile_picture as profile_picture,"
                    . "c.is_trail as is_trail,c.first_name as customer_first_name, c.last_name as customer_last_name,c.plan_name as current_plan_name, c.profile_picture as customer_profile_picture, c.plan_turn_around_days as plan_turn_around_days,e.first_name as sub_first_name, e.last_name as sub_last_name,sp.plan_color as plan_color");
        }
        $this->db->from('requests as r');
        $this->db->join('users as d', 'd.id = r.designer_id', 'LEFT');
        $this->db->join('users as c', 'c.id = r.customer_id', 'LEFT');
        $this->db->join('subscription_plan as sp', 'sp.plan_id = c.plan_name', 'LEFT');
        $this->db->join('users as e', 'e.id = r.created_by', 'LEFT');
        if($client_id!= ''){
            $this->db->where("r.customer_id ='$client_id'"); 
            $this->db->where_in('r.status_admin', $status);
        }elseif($designer_id !=''){
            $this->db->where("r.designer_id ='$designer_id'");
            $this->db->where_in('r.status_admin', $status);
        }else{
            $this->db->where_in('r.status_admin', $status);
        }
        $this->db->where('r.dummy_request', '0');
        if($buckettype){
            $this->db->where('r.category_bucket',$buckettype); 
        }
        if($va_projects == 1){
            $this->db->where('c.va_id',$login_user_id); 
        }
        //$this->db->where_in('r.customer_id', $allQACustomers);
        $this->db->group_by('r.id');
        if($search){
            $this->db->group_start();
            $this->db->or_like('r.title', $search, 'both');
            $this->db->or_like('r.description', $search, 'both');
            $this->db->or_like('c.first_name', $search, 'both');
            $this->db->or_like('c.last_name', $search, 'both');
            $this->db->or_like('d.first_name', $search, 'both');
            $this->db->or_like('d.last_name', $search, 'both');
            //$this->db->or_like('d.email', $search, 'both');
            $this->db->group_end();
            //echo $this->db->last_query();
        }
        if(!$allCountOnly && $type != "download"){
            $limit = ($limit == 0) ? LIMIT_ADMIN_LIST_COUNT : $limit;
            $this->db->limit($limit, $start);
            if ($orderby == "") {
        //echo status[0];exit;
                if ($status[0] == "approved") {
                    $this->db->order_by("approvaldate", "DESC");
                } elseif ($status[0] == "pendingrevision") {
                    $this->db->order_by("modified", "DESC");
                } elseif ($status[0] == "checkforapprove") {
                    $this->db->order_by("modified", "DESC");
                } elseif ($status[0] == "assign") {
                    $this->db->order_by("priority", "ASC");
                } else {
            $this->db->order_by("expected_date", "ASC");
           $this->db->order_by("latest_update", "ASC");
                }
            } else {
                $this->db->order_by($orderby, "ASC");
            }
        }
        $data = $this->db->get();
//  echo $this->db->last_query();exit;        
        if($allCountOnly){
            return $data->num_rows();
        }else{
            $result = $data->result_array();
            if($result){
                return $result;
            }
        }
    }

    
    /****************************end my new functions*******************************************/
    
    public function getall_request_pagination($customer_id = null, $status, $designer_id = "", $start, $limit, $orderby = "") {
        $this->db->select("*");
        $this->db->where_in('status', $status);
        if ($customer_id) {
            $this->db->where("requests.customer_id = '" . $customer_id . "'");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id = '" . $designer_id . "'");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(created)", "ASC");
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $this->db->limit($limit, $start);
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function getall_newrequest($customer_id, $status = array(), $designer_id = "", $orderby = "",$clintid="") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where("requests.customer_id = '" . $customer_id . "'");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id = '" . $designer_id . "'");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }
        if($clintid != ""){
            $this->db->where("requests.created_by = '" . $clintid . "'");
        }

        if (!empty($status)) {
            $this->db->where_in("status", $status);
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(created)", "ASC");
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $this->db->limit(LIMIT_CUSTOMER_LIST_COUNT, 0);
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
                $result[$i]['customer_email'] = $customer[0]['email'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
                $result[$i]['customer_email'] = '';
            }
        }
        return $result;
    }

    public function getallrequestaccstatus($customer_id ,$status = array(),$clntid="") {
        $this->db->select("id,status");
        $this->db->where("customer_id",$customer_id);
        if($clntid != ""){
          $this->db->where("created_by",$clntid);
        }
        $this->db->where_not_in("status",$status);
        $data = $this->db->get("requests");
        $result = $data->result_array();
        return $result;
    }
    
    public function getallrequestcount_accstatus($customer_id ,$status = array(),$createdby = "") {
        $this->db->select("id,status");
        $this->db->where("customer_id",$customer_id);
        if($createdby != ""){
         $this->db->where("created_by",$createdby);   
        }
        $this->db->where_in("status",$status);
        $result = $this->db->count_all_results('requests');
        return $result;
    }
    
    public function getuserbyid($id,$flag = "") {
        if($flag != ""){
            $this->db->select("first_name,last_name");
            $this->db->where("user_flag = '" . $flag . "'");
        }else{
        $this->db->select("*");
        }
        $this->db->where("id = '" . $id . "'");
        $data = $this->db->get("users");
        $result = $data->result_array();
        return $result;
    }

    public function getbyid($table, $id) {
        $this->db->select("*");
        $this->db->where("id = '" . $id . "'");
        $data = $this->db->get($table);
        $result = $data->result_array();
        return $result;
    }

    public function getbypriority($priority) {
        $this->db->select("*");
        $this->db->where("priority = '" . $priority . "'");
        $data = $this->db->get("requests");
        $result = $data->result_array();
        return $result;
    }

    public function get_priority_by_id($id) {
        $this->db->select("priority");
        $this->db->where("id = '" . $id . "'");
        $data = $this->db->get("requests")->row('priority');
        return $data;
    }
    
     public function update_priority($priorityfrom, $priorityto, $id = 0,$uid = 0) {
        if ($id == 0 && $uid != 0) {
            $this->db->set('priority', $priorityto)->where('priority', $priorityfrom)->where('customer_id', $uid)->update('requests');
        } else {
            $this->db->set('priority', $priorityto)->where('priority', $priorityfrom)->where('id', $id)->update('requests');
        }
        return true;
    }
    
    public function update_sub_userpriority($priorityfrom, $priorityto, $id = 0,$uid = 0) {
        if ($id == 0 && $uid != 0) {
            $this->db->set('sub_user_priority', $priorityto)->where('sub_user_priority', $priorityfrom)->where('created_by', $uid)->update('requests');
        } else {
            $this->db->set('sub_user_priority', $priorityto)->where('sub_user_priority', $priorityfrom)->where('id', $id)->update('requests');
        }
       // echo $this->db->last_query();
        return true;
    }

    public function getuserbyemail($email, $except_id = '') {
        $this->db->select("*");
        $this->db->where("email = '" . $email . "'");
        if($except_id!=''){
            $this->db->where("id != '". $except_id ."'");   
        }
        $data = $this->db->get("users");
        $result = $data->result_array();
        //echo $this->db->last_query();exit;
        return $result;
    }
    public function getuserbyparentid($id) {
        $this->db->select("*");
        $this->db->where("parent_id = '" . $id . "'");
        $data = $this->db->get("users");
        $result = $data->result_array();
    //echo $this->db->last_query();exit;
        return $result;
    }
    public function get_sub_user_permissions($id){
      $this->db->select("*");
        $this->db->where("customer_id = '" . $id . "'");
        $data = $this->db->get("user_permissions");
        $result = $data->result_array();
    //echo $this->db->last_query();exit;
        return $result;  
    }
    
    /**start subscription plan data**/
    public function getsubscriptionlistbyplanid($id) {
        $this->db->select("*");
        $this->db->where("plan_id = '" . $id . "'");
        $data = $this->db->get("subscription_plan");
        $result = $data->result_array();
        if(!empty($result)){
            $keys = 1;
            $plan_id = $result[0]['plan_id'];
            foreach ($result as $ky => $plandata) {
                if($plan_id != $plandata['plan_id']){
                    $keys = 1;
                    $plan_id = $plandata['plan_id'];
                }else{
                   $keys++; 
                }
                $result[$ky]['tier_prices'] = $this->gettierpriceofplan($plandata['plan_id']);
            }
        }
        return $result;
    }
    public function getactiveagencyplanid($type) {
        $this->db->select("plan_id,plan_price");
        $this->db->where("is_active",1);
        $this->db->where("plan_type",$type);
//        $this->db->where("plan_type_name",'agency');
        $data = $this->db->get("subscription_plan");
        $result = $data->result_array();
        return $result;
    }
    
    public function getplansforuser($type="") {
        $this->db->select("plan_id,plan_name,features,plan_type,plan_price,active_request,in_progress_request,global_inprogress_request,plan_type_name,apply_coupon");
        if($type != ""){
        $this->db->where("(plan_type = 'unlimited' and plan_id != '".FORTYNINE_REQUEST_PLAN."') or (plan_type_name = 'business' AND is_active = 1 and plan_type = 'monthly')");
        }else{
          //$this->db->where('is_active',1);  
            $this->db->where('is_active = 1 OR is_upgraded_plan = 1');  
        }
        $this->db->order_by('plan_price');
        $data = $this->db->get("subscription_plan");
        //echo $this->db->last_query();exit;
        $result = $data->result_array();
        if(!empty($result)){
            $keys = 1;
            $plan_id = $result[0]['plan_id'];
            foreach ($result as $ky => $plandata) {
                if($plan_id != $plandata['plan_id']){
                    $keys = 1;
                    $plan_id = $plandata['plan_id'];
                }else{
                   $keys++; 
                }
                $result[$ky]['tier_prices'] = $this->gettierpriceofplan($plandata['plan_id']);
            }
        }
        return $result;
    }
    
    public function getagencytierprice($type="") {
        $this->db->select("atp.plan_id,atp.quantity,atp.amount,atp.annual_price,sp.is_agency");
        $this->db->join("subscription_plan as sp","sp.plan_id = atp.plan_id");
        if($type != ""){
        $this->db->where("sp.plan_type",$type);
        }
        $this->db->where("sp.is_active",1);
        $data = $this->db->get("agency_tier_price as atp");
       // echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }
    public function gettierpriceofplan($id) {
        $this->db->select("plan_id,quantity,amount,annual_price");
        $this->db->where("plan_id",$id);
        $data = $this->db->get("agency_tier_price");
        $result = $data->result_array();
        return $result;
    }
    /**end subscription plan data**/

    public function get_request_by_data($url, $heading, $user_id, $shown) {
        $this->db->select("*");
        $this->db->where("url = '" . $url . "'");
        $this->db->where("heading = '" . $heading . "'");
        $this->db->where("user_id = '" . $user_id . "'");
        $this->db->where("shown = '" . $shown . "'");
        $data = $this->db->get("message_notification");
        $result = $data->result_array();
        return $result;
    }

    public function get_attachment_files_for_customer($request_id, $status, $role) {
        $this->db->select("*");
        $this->db->where("request_id = '" . $request_id . "'");
        $this->db->where_in('status', $status);
        $data = $this->db->get("request_files");
        $result = $data->result_array();
        if (empty($result)) {
            return array();
        }
        return $result;
    }

    public function get_new_attachment_files_for_customer($request_id, $status) {
        $this->db->select("*");
        $this->db->where("request_id = '" . $request_id . "'");
        $this->db->where_in('status', $status);
        $this->db->where('customer_seen', 0);
        $data = $this->db->get("request_files");
        $result = $data->result_array();
        if (empty($result)) {
            return array();
        }
        return $result;
    }

    public function get_request_by_id_admin($id) {
        $this->db->select("*");
        $this->db->where("id = '" . $id . "'");
        $data = $this->db->get("requests");
        $result = $data->result_array();
        // echo "<pre>";
        // print_r($result);
        // exit;
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            $qa = $this->getuserbyid($designer[$i]['qa_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
            }
            if (!empty($qa)) {
                $result[$i]['qa_first_name'] = $qa[0]['first_name'];
                $result[$i]['qa_profile_picture'] = $qa[0]['profile_picture'];
                $result[$i]['qa_last_name'] = $qa[0]['last_name'];
            } else {
                $result[$i]['qa_first_name'] = "";
                $result[$i]['qa_last_name'] = "";
                $result[$i]['qa_profile_picture'] = "";
            }
            $result[$i]['customer_attachment'] = $this->get_attachment_files($id, "customer");
            $result[$i]['designer_attachment'] = $this->get_attachment_files($id, "designer");
            $result[$i]['admin_attachment'] = $this->get_attachment_files($id, "admin");

            $style_suggestion = $this->get_style_suggestion($id);
            if (empty($style_suggestion)) {
                $result[$i]['additional_description'] = "";
                $result[$i]['color'] = "";
                $result[$i]['image'] = "";
                $result[$i]['selected_color'] = "";
            } else {
                $result[$i]['additional_description'] = $style_suggestion[0]['additional_description'];
                $result[$i]['color'] = $style_suggestion[0]['color'];
                $result[$i]['image'] = $style_suggestion[0]['image'];
                $result[$i]['selected_color'] = $style_suggestion[0]['selected_color'];
            }
        }

        return $result;
    }
    
     
    
    public function get_brandprofile_by_id($id,$reqbrndid = null) {
    //print_r($id);
        $this->db->select("*");
        if($reqbrndid == '' || $reqbrndid == NULL ){
        $this->db->where("id = '" . $id . "'" );  
        }else{
           $this->db->where_in("id" , $reqbrndid);  
        }
        $data = $this->db->get("brand_profile");
        //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
        
    }
    public function get_brand_profile_by_user_id($id,$created_by = "",$count="",$brand_permission = "") {
    $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        $login_user_id = $this->load->get_var('login_user_id');
        $this->db->select("*");
        if($brand_permission != ""){
           if($canbrand_profile_access == 0){
             $this->db->where("((id IN (".implode(',',$sub_usersbrands).")) or (created_by='".$login_user_id."'))");
            }else{
                $this->db->where("customer_id = '" . $id . "'");
            } 
        }else{
            if($created_by != ""){
                $this->db->where("created_by = '" . $id . "'");  
            }else{
                $this->db->where("customer_id = '" . $id . "'");
            }
        }
        if($count != ""){
        $result = $this->db->count_all_results('brand_profile');
        }else{
         $data = $this->db->get("brand_profile"); 
       // echo $this->db->last_query();exit; 
         $result = $data->result_array();
        }
        return $result;   
    } 
    
    public function get_brandprofile_materials_files($brand_id) {
        $this->db->select("*");
        $this->db->where("brand_id = '" . $brand_id . "'");
        $this->db->order_by("Date(created)","DESC");
        $data = $this->db->get("brand_profile_material");
        
       // echo $this->db->last_query();
        $result = $data->result_array();
        if (empty($result)) {
            return array();
        }
        return $result;
    }

    public function get_request_by_id($id,$subuserid="",$reqids=array()) {
        $this->db->select("*");
        if(!empty($reqids)){
          $this->db->where_in("id",$reqids);  
        }else{
          $this->db->where("id = '" . $id . "'");
        }
        if($subuserid != ""){
          $this->db->where("created_by = '" . $subuserid . "'");
        }
        $data = $this->db->get("requests");
        $this->db->order_by("Date(created)","DESC");
        // echo $this->db->last_query(); exit("<s></s>"); 
        $result = $data->result_array();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
            }
            $result[$i]['customer_attachment'] = $this->get_attachment_files($id, "customer");
            $result[$i]['designer_attachment'] = $this->get_attachment_files($id, "designer");
            $result[$i]['admin_attachment'] = $this->get_attachment_files($id, "admin");

            $style_suggestion = $this->get_style_suggestion($id);
            if (empty($style_suggestion)) {
                $result[$i]['additional_description'] = "";
                $result[$i]['color'] = "";
                $result[$i]['image'] = "";
                $result[$i]['selected_color'] = "";
            } else {
                $result[$i]['additional_description'] = $style_suggestion[0]['additional_description'];
                $result[$i]['color'] = $style_suggestion[0]['color'];
                $result[$i]['image'] = $style_suggestion[0]['image'];
                $result[$i]['selected_color'] = $style_suggestion[0]['selected_color'];
            }
        }

        return $result;
    }
    
    public function get_requestfiles_by_id($id,$usertype) {
        $this->db->select("file_name");
        $this->db->where("request_id = '" . $id . "'");
        $this->db->where("user_type = '" . $usertype . "'");
        $data = $this->db->get("request_files");
        $result = $data->result_array();
       // $datafile = '';
        for ($i = 0; $i < sizeof($result); $i++) {
            $datafile[$i] = $result[$i]['file_name'];
             
        }
        return $datafile;        
        
    }

    public function get_attachment_files($request_id, $role ,$status = NULL) {
       // echo $role.$status .'sdfg';exit;
        $this->db->select("*");
        $this->db->where("request_id = '" . $request_id . "'");
        $this->db->where("user_type = '" . $role . "'");
        if($status != NULL || $status != ''){
         $this->db->where("status != 'pending'");  
         $this->db->where("status != 'Reject'");  
        }
        $this->db->order_by("created","DESC");
        $data = $this->db->get("request_files");
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
       // echo "<pre>";print_r($result);exit;
        if (empty($result)) {
            return array();
        }
        return $result;
    }

    public function get_average_rating($id) {
        $sql = "SELECT avg(customer_grade) as grade FROM `request_files` WHERE user_id='" . $id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function get_customer_average_rating($customer_id, $designer_id) {
        $sql = " SELECT GROUP_CONCAT(id) as ids FROM `requests` WHERE customer_id = '" . $customer_id . "' and designer_id = '" . $designer_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        if ($result[0]['ids']) {
            $sql = "SELECT avg(customer_grade) as grade FROM `request_files` WHERE user_id='" . $designer_id . "' and request_id IN(" . $result[0]['ids'] . ")";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['grade'];
        }
        return 0;
    }

    public function get_customer_average_rating_qa_dashboard($customer_id) {
        $sql = " SELECT GROUP_CONCAT(id) as ids FROM `requests` WHERE customer_id = '" . $customer_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        if ($result[0]['ids']) {
            $sql = "SELECT avg(customer_grade) as grade FROM `request_files` WHERE request_id IN(" . $result[0]['ids'] . ")";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['grade'];
        }
        return 0;
    }

    // Get Qa Customers
    public function get_customer_list_for_qa_section($id) {
        $this->db->select('id');
        $this->db->where('qa_id', $id);
        $this->db->where('role', 'customer');
        $query = $this->db->get('users');
        $result = $query->result_array();
        return $result; 
    }

    public function get_customer_list_for_qa($type = "", $list) {
        if ($type == "array") {
            $this->db->select('*');
            $this->db->where('qa_id', $_SESSION['user_id']);
            $this->db->where('role', 'customer');
            $this->db->limit(4, 0);
            $query = $this->db->get('users');
            $result = $query->result_array();
            return $result;
        } else {
            $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE designer_id IN(" . $list . ")";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['ids'];
        }
    }

    public function get_customer_list_for_qa_scroll($type = "", $list, $start, $limit) {
        if ($type == "array") {
            $this->db->select('*');
            $this->db->where('qa_id', $_SESSION['user_id']);
            $this->db->where('role', 'customer');
            $this->db->limit($limit, $start);
            $query = $this->db->get('users');
            $result = $query->result_array();
            return $result;
        } else {
            $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE designer_id IN(" . $list . ")";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['ids'];
        }
    }

    public function get_customer_list_for_qa_count($type = "", $list) {
        if ($type == "array") {
            $this->db->select('*');
            $this->db->where('qa_id', $_SESSION['user_id']);
            $this->db->where('role', 'customer');
            $num_rows = $this->db->count_all_results('users');
            return $num_rows;
            // $query = $this->db->get('users');
            // $result = $query->result_array();
            // return $result;
        } else {
            $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE designer_id IN(" . $list . ")";
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['ids'];
        }
    }

    // public function get_designer_list_for_qa($type="")
    // {
    //  if($type == "array"){
    //      $sql = " SELECT * from `users` WHERE qa_id = '".$_SESSION['user_id']."'";
    //      $query = $this->db->query($sql);
    //      $result = $query->result_array();
    //      return $result;
    //  }else{
    //      $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '".$_SESSION['user_id']."'";
    //      $query = $this->db->query($sql);
    //      $result = $query->result_array();
    //      return $result[0]['ids'];
    //  }
    // }

    public function get_designer_list_for_qa($type = "", $id = "") {
        if ($type == "array") {
            $this->db->select('*');
            $this->db->where('qa_id', $_SESSION['user_id']);
            $this->db->where('role', 'designer');
            $this->db->limit(5, 0);
            $query = $this->db->get('users');
            $result = $query->result_array();
            return $result;
        } else {
            if ($id) {
                $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '" . $id . "'";
            } else {
                $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '" . $_SESSION['user_id'] . "'";
            }
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['ids'];
        }
    }

    public function get_designer_list_for_qa_scroll($type = "", $start, $limit) {
        if ($type == "array") {
            $this->db->select('*');
            $this->db->where('qa_id', $_SESSION['user_id']);
            $this->db->where('role', 'designer');
            $this->db->limit($limit, $start);
            $query = $this->db->get('users');
            $result = $query->result_array();
            return $result;
        } else {
            if ($id) {
                $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '" . $id . "'";
            } else {
                $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '" . $_SESSION['user_id'] . "'";
            }
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['ids'];
        }
    }

    public function get_designer_list_for_qa_count($type = "", $id = "") {
        if ($type == "array") {
            $this->db->select('*');
            $this->db->where('qa_id', $_SESSION['user_id']);
            $this->db->where('role', 'designer');
            $num_rows = $this->db->count_all_results('users');
            return $num_rows;
        } else {
            if ($id) {
                $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '" . $id . "'";
            } else {
                $sql = " SELECT GROUP_CONCAT(id) as ids FROM `users` WHERE qa_id = '" . $_SESSION['user_id'] . "'";
            }
            $query = $this->db->query($sql);
            $result = $query->result_array();
            return $result[0]['ids'];
        }
    }

    // Get Request For Qa 
    public function get_request_list_for_qa_section($customer_id = null, $status = array(), $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where_in("customer_id", $customer_id);
        }
        if (!empty($status)) {
            $this->db->where_in('status_qa', $status);
        }
       
        if ($orderby == "") {
            if ($status[0] == "approved") {
                $this->db->order_by("approvaldate", "DESC");
            }elseif($status[0] == 'active' || $status[1] == 'disapprove'){
                        $this->db->order_by("latest_update","ASC");
            }
            elseif($status[0] == 'assign' || $status[1] == 'pending'){
                        $this->db->order_by("priority","ASC");
            }elseif ($status[0] == "pendingrevision") {
                $this->db->order_by("modified", "ASC");
            } elseif ($status[0] == "checkforapprove") {
                $this->db->order_by("modified", "ASC");
            } else {
                 $this->db->order_by("modified", "DESC");
            }
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $this->db->limit(6, 0);
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                ;
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function get_request_list_for_qa_section_scroll($customer_id = null, $status = array(), $start, $limit, $orderby = "") {
        $result = array();
        $this->db->select('a.*','b.plan_name as current_plan_name');
        $this->db->from('requests as a');
        $this->db->join('users as b', 'b.id = a.customer_id','LEFT');
        if ($customer_id) {
            $this->db->where_in("a.customer_id", $customer_id);
          
            if (!empty($status)) {
                $this->db->where_in('a.status_qa', $status);
                if ($orderby == "") {
                    if ($status[0] == "approved") {
                        $this->db->order_by("a.approvaldate", "DESC");
                    } elseif ($status[0] == "pendingrevision") {
                        $this->db->order_by("a.modified", "ASC");
                    } elseif ($status[0] == "checkforapprove") {
                        $this->db->order_by("a.modified", "ASC");
                    }elseif($status[0] == "assign"){
                        $this->db->order_by("a.priority", "ASC");
                    }elseif($status[0] == 'active' || $status[1] == 'disapprove'){
                        $this->db->order_by("a.latest_update","ASC");
                    }else {
                       $this->db->order_by("a.modified", "DESC");
                    }
                    $this->db->group_by('id');
                    $this->db->limit($limit, $start);
                    $data = $this->db->get();
                    $result = $data->result_array();
                    // echo $this->db->last_query();
                    for ($i = 0; $i < sizeof($result); $i++) {
                        $designer = $this->getuserbyid($result[$i]['designer_id']);
                        $customer = $this->getuserbyid($result[$i]['customer_id']);
                        if (!empty($designer)) {
                            $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                            $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                            $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
                        } else {
                            $result[$i]['designer_first_name'] = "";
                            $result[$i]['designer_last_name'] = "";
                            $result[$i]['profile_picture'] = "";
                        }
                        if (!empty($customer)) {
                            $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                            $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                            $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                            ;
                            $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
                        } else {
                            $result[$i]['customer_first_name'] = "";
                            $result[$i]['customer_last_name'] = "";
                            $result[$i]['customer_profile_picture'] = "";
                            $result[$i]['plan_turn_around_days'] = 0;
                        }
                    }
                }
            }
        } else {
            echo '<div class="nodata">
                                <h3 class="head-b draft_no">They are no projects at this time</h3>
                            </div>';
            //$this->db->order_by($orderby,"ASC");
        }
        return $result;
    }

    public function get_request_list_for_qa_section_count($customer_id = null, $status = array(), $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where_in("customer_id", $customer_id);
        }
        if (!empty($status)) {
            $this->db->where_in('status_qa', $status);
        }
        if ($orderby == "") {
            if ($status[0] == "approved") {
                $this->db->order_by("approvaldate", "DESC");
            } elseif ($status[0] == "pendingrevision") {
                $this->db->order_by("modified", "DESC");
            } elseif ($status[0] == "checkforapprove") {
                $this->db->order_by("modified", "DESC");
            } else {
                $this->db->order_by("latest_update", "ASC");
            }
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $num_rows = $this->db->count_all_results('requests');
        return $num_rows;
    }

    public function get_request_list_for_qa($customer_id = null, $status = array(), $designer_id = "", $orderby = "") {
        $this->db->select('a.*','b.plan_name as current_plan_name');
        $this->db->from('requests as a');
        $this->db->join('users as b', 'b.id = a.customer_id','LEFT');
        $this->db->where('a.dummy_request', '0');
        if ($customer_id) {
            $this->db->where("a.customer_id IN (" . $customer_id . ")");
        } elseif ($designer_id) {
            $this->db->where("a.designer_id IN (" . $designer_id . ")");
        } else {
            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("a.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }
        if (!empty($status)) {
            $this->db->where_in('a.status_qa', $status);
        }
        if ($orderby == "") {
            if ($status[0] == "approved") {
                $this->db->order_by("a.approvaldate", "DESC");
            }elseif($status[0] == 'active' || $status[1] == 'disapprove'){
             $this->db->order_by("a.latest_update","ASC");
            }
            elseif ($status[0] == "assign") {
               $this->db->order_by("a.priority", "ASC");
            }elseif ($status[0] == "pendingrevision") {
                $this->db->order_by("a.modified", "ASC");
            } elseif ($status[0] == "checkforapprove") {
                $this->db->order_by("a.modified", "ASC");
            } else {
                $this->db->order_by("a.latest_update", "ASC");
            }
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $data = $this->db->get();
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                ;
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function get_request_list_for_admin($customer_id = null, $status = array(), $designer_id = "", $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where("requests.customer_id IN (" . $customer_id . ")");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id IN (" . $designer_id . ")");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }
        if (!empty($status)) {
            $this->db->where_in('status', $status);
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(modified)", "DESC");
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                ;
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function get_unassign_request_fist_for_qa($customer_id = null, $status = array(), $designer_id = "", $orderby = "") {
        $this->db->select("*");
        if ($customer_id) {
            $this->db->where("requests.customer_id IN (" . $customer_id . ")");
        } elseif ($designer_id) {
            $this->db->where("requests.designer_id IN (" . $designer_id . ")");
        } else {

            if (!in_array($this->session->role, array("admin", "qa", "va"))) {
                $this->db->where("requests.customer_id = '" . $_SESSION['user_id'] . "'");
            }
        }
        if (!empty($status)) {
            $this->db->where("status IN (" . $status . ")");
        }
        if ($orderby == "") {
            $this->db->order_by("DATE(created)", "ASC");
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $data = $this->db->get("requests");
        $result = $data->result_array();
        //echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                ;
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

    public function get_average_rating_for_request($request_id, $designer_id) {
        $sql = "SELECT avg(customer_grade) as grade FROM `request_files` WHERE user_id='" . $designer_id . "' and request_id='" . $request_id . "'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }

    public function get_files_count_all($request_id) {
        $this->db->select('*');
        $this->db->where('request_id', $request_id);
        $this->db->where('src_file !=', 'null');
        $query = $this->db->get('request_files');
        $result = $query->result_array();
        return $result;
    }

    public function get_files_count($request_id, $designer_id, $seen) {
        //$sql = "SELECT count(*) as fileCount FROM `request_files` WHERE request_id='".$request_id."' and src_file != 'null'";
        $this->db->select('*');
        $this->db->where('request_id', $request_id);
        $this->db->where('src_file !=', 'null');
        if ($seen == 'qa') {
            $this->db->where('qa_seen', 0);
        } elseif ($seen == 'admin') {
            $this->db->where('admin_seen', 0);
        } elseif ($seen == 'customer') {
            $this->db->where('customer_seen', 0);
        } elseif ($seen == 'designer') {
            $this->db->where('designer_seen', 0);
        }
        $query = $this->db->get('request_files');
        $result = $query->result_array();
        return $result;
    }

    public function get_style_suggestion($request_id) {
        $this->db->select("*");
        $this->db->where("request_id = '" . $request_id . "'");
        $data = $this->db->get("style_suggestion");
        $result = $data->result_array();
        if (empty($result)) {
            return array();
        }
        return $result;
    }

    public function new_designer_get_status($designer_id, $customer_id) {
        $this->db->select("*");
        $this->db->where("status='active'");
        $this->db->where("designer_id='" . $designer_id . "'");
        $this->db->where("customer_id='" . $customer_id . "'");
        $data = $this->db->get("requests");
        $result = $data->result_array();
        if (sizeof($result) == 1) {
            return "assign";
        } else {
            return "active";
        }
    }

   public function get_chat_by_id($id, $order = null) {
        $this->db->select("r.*,u.profile_picture,u.first_name");
        $this->db->from('request_file_chat as r');
        $this->db->where("r.request_file_id='" . $id . "' and r.parent_id='0' and r.is_deleted='0'");
        $this->db->join('users as u', 'r.sender_id = u.id','left');
        if($order){
            $this->db->order_by("r.created", $order);
        }else{
            $this->db->order_by("r.id", $order);
        }
        $data = $this->db->get();
       // echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }
    /* Buggy Code - due to not proper field assigning in select
    public function get_chat_by_id($id, $order = null) {
        $this->db->select("*");
        $this->db->from('request_file_chat as r');
        $this->db->where("r.request_file_id='" . $id . "'");
        $this->db->join('users as u', 'r.sender_id = u.id','left');
        $this->db->order_by("r.id", $order);
        $data = $this->db->get();
       // echo $this->db->last_query();
        $result = $data->result_array();
        return $result;
    }
    */
//    public function get_chat_request_by_id($id) {
//        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
//        $this->db->from('request_discussions as a');
//        $this->db->where("request_id='" . $id . "'");
//        $this->db->where("with_or_not_customer", 1);
//        $this->db->join('users as b', 'b.id = a.sender_id');
//        $this->db->order_by("id", "ASC");
//        $data = $this->db->get();
//        echo $this->db->last_query();
//        $result = $data->result_array();
//        return $result;
//    }
    
    public function get_chat_request_by_id($id,$orderby="") {
        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
        $this->db->from('request_discussions as a');
        $this->db->where("request_id='" . $id . "'");
        $this->db->where("with_or_not_customer", 1);
        $this->db->where("is_deleted != 1");
        $this->db->join('users as b', 'b.id = a.sender_id',"left");
        if($orderby != ""){
          $this->db->order_by("id", $orderby);
        }else{
          $this->db->order_by("id", "ASC");
        }
        $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
        return $result;
    }

    public function get_chat_request_by_id_without_customer($id) {
        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
        $this->db->from('request_discussions as a');
        $this->db->where("request_id='" . $id . "'");
        $this->db->where("with_or_not_customer", 0);
        $this->db->join('users as b', 'b.id = a.sender_id');
        $this->db->order_by("id", "DESC");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }

    public function get_notifications($user_id) {
        $this->db->select("n.*,u.first_name,u.last_name,u.profile_picture,r.title as req_title");
        $this->db->from('notification as n');
        $this->db->where("n.user_id='" . $user_id . "'");
        $this->db->where("n.shown='0'");
        $this->db->join('users as u', 'u.id = n.sender_id');
        $this->db->join('requests as r', 'r.id = n.request_id');
        $this->db->order_by("n.created", "DESC");
        $this->db->limit(LIMIT_MESSAGE_AND_NOTIFICATION);
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }

    public function get_messagenotifications($user_id) {
        $this->db->select("mn.*,u.first_name,u.last_name,u.profile_picture");
        $this->db->where("mn.user_id='" . $user_id . "'");
        $this->db->join('users as u', 'u.id = mn.user_id');
        $this->db->where("mn.shown='0'");
        $this->db->order_by("mn.id", "DESC");
        $this->db->limit(LIMIT_MESSAGE_AND_NOTIFICATION);
        $data = $this->db->get("message_notification as mn");
        $result = $data->result_array();
        return $result;
    }

    // public function get_notifications($user_id)
    // {
    //  $this->db->select("*");
    //  $this->db->where("user_id='".$user_id."'");
    //  $this->db->where("shown='0'");
    //  $this->db->order_by("shown","ASC");
    //  $data = $this->db->get("notification"); 
    //  $result = $data->result_array();
    //  return $result;
    // }
    public function get_notifications_number($user_id) {
        $this->db->select("*");
        $this->db->where("user_id='" . $user_id . "'");
        $this->db->join('users as u', 'u.id = n.sender_id');
        $this->db->join('requests as r', 'r.id = n.request_id');
        $this->db->where("shown='0'");
        $this->db->order_by("shown", "ASC");
        $num_rows = $this->db->count_all_results('notification as n');
        
       // echo $this->db->last_query();
        return $num_rows;
    }

    public function get_messagenotifications_number($user_id) {
        $this->db->select("*");
        $this->db->where("user_id='" . $user_id . "'");
        $this->db->where("shown='0'");
        $this->db->order_by("shown", "ASC");
        $num_rows = $this->db->count_all_results('message_notification');
        //echo $num_rows;
        return $num_rows;
    }

    public function get_total_chat_number_customer($request_id, $user_id, $seen, $with) {
        if ($seen == "customer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and with_or_not_customer='" . $with . "'";
        }
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return sizeof($result);
        die();
    }

    public function get_chat_number_customer($request_id, $user_id, $seen, $with) {
        if ($seen == "customer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and customer_seen='0' and with_or_not_customer='" . $with . "'";
        }

        $query = $this->db->query($sql);
        $result = $query->result_array();
        //echo "<pre>"; print_r(sizeof($result)); echo "</pre>";
        return sizeof($result);
        die();
    }

    public function get_total_chat_number($request_id, $user_id, $seen) {
        if ($seen == "designer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "'";
        }
        if ($seen == "qa") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "'";
        }
        if ($seen == "admin") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "'";
        }
        $query = $this->db->query($sql);
        $result = $query->result_array();
        return sizeof($result);
        die();
    }

    public function get_chat_number($request_id, $receiver_id, $sender_id, $seen) {
        if ($seen == "customer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and customer_seen='0'";
        }
        if ($seen == "designer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and designer_seen='0'";
        }
        if ($seen == "admin") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and admin_seen='0'";
        }
        if ($seen == "qa") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and qa_seen='0'";
        }
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //echo "<pre>"; print_r(sizeof($result)); echo "</pre>";
        return sizeof($result);
        die();
    }

    public function get_customer_chat_number($request_id, $receiver_id, $sender_id, $seen) {
        if ($seen == "customer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and customer_seen='0' and with_or_not_customer'1'";
        }
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //echo "<pre>"; print_r(sizeof($result)); echo "</pre>";
        return sizeof($result);
        die();
    }

    public function get_comment_number($request_id, $receiver_id, $sender_id, $seen) {
        if ($seen == "customer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and customer_seen='0'";
        }
        if ($seen == "designer") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and designer_seen='0'";
        }
        if ($seen == "admin") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and admin_seen='0'";
        }
        if ($seen == "qa") {
            $sql = "select * from request_discussions where request_id='" . $request_id . "' and qa_seen='0'";
        }
        $query = $this->db->query($sql);
        $result = $query->result_array();
        //echo "<pre>"; print_r(sizeof($result)); echo "</pre>";
        return sizeof($result);
        die();
    }

    // public function get_chat_number($request_id,$receiver_id,$sender_id,$seen){
    //  if($seen == "customer"){
    //      //echo "inif";
    //      $sql = "select * from request_discussions where request_id='".$request_id."' and reciever_id='".$receiver_id."' and sender_id='".$sender_id."' and customer_seen='0'";
    //  }
    //  if($seen == "designer"){
    //      $sql = "select * from request_discussions where request_id='".$request_id."' and reciever_id='".$receiver_id."' and sender_id='".$sender_id."' and designer_seen='0'";
    //  }
    //  if($seen == "admin"){
    //      $sql = "select * from request_discussions where request_id='".$request_id."' and reciever_id='".$receiver_id."' and sender_id='".$sender_id."' and admin_seen='0'";
    //  }
    //  $query = $this->db->query($sql);
    //  $result = $query->result_array();
    //   //echo "<pre>"; print_r(sizeof($result)); echo "</pre>";
    //  return sizeof($result);
    //          die();
    // }

    public function get_file_chat($request_file_id, $role) {
        if ($role == "designer") {
            $sql = "select * from request_file_chat where designer_seen='0' and request_file_id='" . $request_file_id . "' and is_deleted != 1";
        } elseif ($role == "customer") {
            $sql = "select * from request_file_chat where customer_seen='0' and request_file_id='" . $request_file_id . "' and is_deleted != 1";
        } elseif ($role == "qa") {
            $sql = "select * from request_file_chat where qa_seen='0' and request_file_id='" . $request_file_id . "' and is_deleted != 1";
        } elseif ($role == "admin") {
            $sql = "select * from request_file_chat where admin_seen='0' and request_file_id='" . $request_file_id . "' and is_deleted != 1";
        }
        $query = $this->db->query($sql);
        return $result = $query->num_rows();
    }
    
    public function get_main_messsage_chat($request_id, $role) {
        if ($role == "designer") {
            $sql = "select * from request_discussions where designer_seen='0' and request_id='" . $request_id . "' and is_deleted != 1";
        } elseif ($role == "customer") {
            $sql = "select * from request_discussions where customer_seen='0' and request_id='" . $request_id . "' and is_deleted != 1";
        } elseif ($role == "qa") {
            $sql = "select * from request_discussions where qa_seen='0' and request_id='" . $request_id . "' and is_deleted != 1";
        } elseif ($role == "admin") {
            $sql = "select * from request_discussions where admin_seen='0' and request_id='" . $request_id . "' and is_deleted != 1";
        }
        $query = $this->db->query($sql);
       // echo $this->db->last_query();
        return $result = $query->num_rows();
    }

    // public function get_file_chat_number($request_file_id,$role){
    //  if($role=="designer"){
    //      $sql="select * from request_file_chat where designer_seen='0' and request_file_id='".$request_file_id."'";
    //  }elseif($role=="customer"){
    //      $sql="select * from request_file_chat where customer_seen='0' and request_file_id='".$request_file_id."'";
    //      echo $sql;
    //  }
    //  // $query = $this->db->query($sql);
    //  // $result = $query->result_array();
    //  // //if(sizeof($result)){ echo $this->db->last_query(); }
    //  // //return sizeof($result);
    //  //   echo "<pre>"; print_r($result); echo "</pre>";
    //            // die();
    //  die();
    // }
        public function get_customer_unseen_msg_for_cron_mail($dateTimeMinutesAgo) {
        $this->db->select("r.*,u.first_name,u.last_name,u.email,req.title");
        $this->db->from('request_discussions as r');
        $this->db->where("r.created >= '".$dateTimeMinutesAgo."' and r.customer_seen = '0' and r.message != 'The designer is currently working on your project. Send messages here to share updates about the overall project. If you want to share comments about an individual design you can use the comments section within each design draft.'");
        $this->db->join('users as u', 'u.id = r.reciever_id','left');
        $this->db->join('requests as req', 'req.id = r.request_id','left');
        $this->db->group_by('r.request_id'); 
        $this->db->order_by("u.id", "ASC");
        $data = $this->db->get();
       // echo $this->db->last_query();
        $result = $data->result_array();
        return $result;

    }
    public function get_customer_unseen_msg_for_cron_mail_in_comment_chat($dateTimeMinutesAgo) {
        $this->db->select("r.*,u.first_name,u.last_name,u.email,rf.request_id,req.title,rf.status as request_file_status");
        $this->db->from('request_file_chat as r');
        $this->db->join('users as u', 'u.id = r.receiver_id','left');
        $this->db->join('request_files as rf', 'rf.id = r.request_file_id','inner');
        $this->db->join('requests as req', 'req.id = rf.request_id','left');
        $where_q = "r.created >= '".$dateTimeMinutesAgo."' and r.customer_seen = '0' and rf.status not in ('pending', 'Reject')";
        $this->db->where($where_q);
        $this->db->group_by('rf.request_id'); 
        $this->db->order_by("u.id", "ASC");
        $data = $this->db->get();
        //echo $this->db->last_query();exit;
        $result = $data->result_array();
        //echo "<pre>";print_r($result);exit;
        return $result;

    }
    public function get_customer_unseen_msg($id, $customer_id, $designer_id, $msg_seen) {
        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
        $this->db->from('request_discussions as a');
        $this->db->where("request_id='" . $id . "' and customer_seen='" . $msg_seen . "' and with_or_not_customer='1'");
        $this->db->join('users as b', 'b.id = a.sender_id');
        $this->db->order_by("id", "ASC");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;

        // $this->db->select("*");
        // $this->db->where("request_id='".$id."' and customer_seen='".$msg_seen."' and with_or_not_customer='1'");
        // $data = $this->db->get("request_discussions");
        // $result = $data->result_array();
        // return $result;
    }

    public function get_admin_unseen_msg($id, $customer_id, $designer_id, $msg_seen) {
        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
        $this->db->from('request_discussions as a');
        $this->db->where("request_id='" . $id . "' and admin_seen='" . $msg_seen . "'");
        $this->db->join('users as b', 'b.id = a.sender_id');
        $this->db->order_by("id", "ASC");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;



        // $this->db->select("*");
        // $this->db->where("request_id='".$id."' and qa_seen='".$msg_seen."'");
        // $this->db->join('users','users.id = notification.sender_id');
        // $data = $this->db->get("request_discussions");
        // $result = $data->result_array();
        // return $result;
    }

    public function get_qa_unseen_msg($id, $customer_id, $designer_id, $msg_seen) {
        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
        $this->db->from('request_discussions as a');
        $this->db->where("request_id='" . $id . "' and qa_seen='" . $msg_seen . "'");
        $this->db->join('users as b', 'b.id = a.sender_id');
        $this->db->order_by("id", "ASC");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;



        // $this->db->select("*");
        // $this->db->where("request_id='".$id."' and qa_seen='".$msg_seen."'");
        // $this->db->join('users','users.id = notification.sender_id');
        // $data = $this->db->get("request_discussions");
        // $result = $data->result_array();
        // return $result;
    }

    public function get_designer_unseen_msg($id, $customer_id, $designer_id, $msg_seen) {
        $this->db->select("a.*,b.profile_picture,b.first_name,b.last_name");
        $this->db->from('request_discussions as a');
        $this->db->where("request_id='" . $id . "' and designer_seen='" . $msg_seen . "'");
        $this->db->join('users as b', 'b.id = a.sender_id');
        $this->db->order_by("id", "ASC");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }

    public function get_all_designer_for_qa($id) {
        $this->db->select("*");
        $this->db->where('role', 'designer');
        $this->db->where('qa_id', $id);
        $data = $this->db->get('users');
        $result = $data->result_array();
        return $result;
    }

    public function get_all_clients_for_qa($name = array(), $id = null) {
        $sql = "SELECT * FROM `users` WHERE (`first_name` LIKE '%" . $name['keyword'] . "%' OR `last_name` LIKE '%" . $name['keyword'] . "%' OR `email` LIKE '%" . $name['keyword'] . "%') AND `role` = 'customer' AND `qa_id` = " . $_SESSION['user_id'];
        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function get_all_clients_for_admin($name = array()) {
        $sql = "SELECT * FROM `users` WHERE (`first_name` LIKE '%" . $name['keyword'] . "%' OR `last_name` LIKE '%" . $name['keyword'] . "%' OR `email` LIKE '%" . $name['keyword'] . "%') AND `role` = 'customer'";
        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function get_all_designer_for_admin($name = array()) {
        $sql = "SELECT * FROM `users` WHERE (`first_name` LIKE '%" . $name['keyword'] . "%' OR `last_name` LIKE '%" . $name['keyword'] . "%' OR `email` LIKE '%" . $name['keyword'] . "%') AND `role` = 'designer'";
        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function get_qa_member($name = array()) {
        $sql = "SELECT * FROM `users` WHERE (`first_name` LIKE '%" . $name['keyword'] . "%' OR `last_name` LIKE '%" . $name['keyword'] . "%' OR `email` LIKE '%" . $name['keyword'] . "%') AND `role` = 'qa'";
        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function get_va_member($name = array()) {
        $sql = "SELECT * FROM `users` WHERE (`first_name` LIKE '%" . $name['keyword'] . "%' OR `last_name` LIKE '%" . $name['keyword'] . "%') AND `role` = 'va'";
        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function get_blog_ajax($title = array()) {
        $this->db->select('*');
        $this->db->like('title', $title['keyword'], 'both');
        $query = $this->db->get('blog');
        $result = $query->result_array();
        return $result;
    }

    public function get_portfolio_ajax($title = array()) {
        $this->db->select('*');
        $this->db->like('title', $title['keyword'], 'both');
        $query = $this->db->get('protfolio');
        $result = $query->result_array();
        return $result;
    }

    public function get_all_designer_for_qa_search_ajax($name = array(), $id = null) {
        //$this->db->select("*");
        $sql = "SELECT * FROM `users` WHERE (`first_name` LIKE '%" . $name['keyword'] . "%' OR `last_name` LIKE '%" . $name['keyword'] . "%' OR `email` LIKE '%" . $name['keyword'] . "%') AND `role` = 'designer' AND `qa_id` = " . $_SESSION['user_id'];
        $data = $this->db->query($sql);
        $result = $data->result_array();
        return $result;
    }

    public function logout() {
        if(isset($_COOKIE['is_remember']) || isset($_COOKIE['rememberID'])){
                setcookie('rememberID', '', time() - 3600, '/');
                setcookie('is_remember', '', time() - 3600, '/');
        }
        $this->session->sess_destroy();
    }

    public function GetAutocomplete($options = array(), $status = array(), $customer_id, $datarole) {
        $result = array();
        // echo "<pre>";print_r($options);exit;
        $this->db->select('a.*','b.plan_name as current_plan_name');
        $this->db->from('requests as a');
        $this->db->where_in('a.status_qa', $status);
        if ($options['keyword'] != "") {
            $this->db->join('users as b', 'b.id = a.customer_id OR b.id = a.designer_id');
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            $this->db->group_by('a.id'); 
            $this->db->group_end();            
        }
        if ($datarole) {
            $this->db->where_in('a.status', $status);
        }
        if ($customer_id) {
            $this->db->where_in('a.customer_id', $customer_id);
            if ($datarole) {
                $this->db->where_in('a.status', $status);
            }
            if ($status[0] == "approved") {
                $this->db->order_by("a.approvaldate", "DESC");
            } elseif($status[0] == 'active' || $status[1] == 'disapprove'){
                        $this->db->order_by("latest_update","ASC");
                    }
            elseif ($status[0] == "pendingrevision") {
                $this->db->order_by("a.modified", "ASC");
            } elseif ($status[0] == "checkforapprove") {
                $this->db->order_by("a.modified", "ASC");
            }elseif($status[0] == "assign"){
                 $this->db->order_by("a.priority", "ASC");
            } else {
                $this->db->order_by("a.modified", "DESC");
            }
            $this->db->limit(10, 0);
            $query = $this->db->get(); 
            $result = $query->result();
           // echo $this->db->last_query();
            for ($i = 0; $i < sizeof($result); $i++) {
                $designer = $this->getuserbyid($result[$i]->designer_id);
                $customer = $this->getuserbyid($result[$i]->customer_id);
                if (!empty($designer)) {
                    $result[$i]->designer_first_name = $designer[0]['first_name'];
                    $result[$i]->designer_last_name = $designer[0]['last_name'];
                    $result[$i]->profile_picture = $designer[0]['profile_picture'];
                } else {
                    $result[$i]->designer_first_name = "";
                    $result[$i]->designer_last_name = "";
                    $result[$i]->profile_picture = "";
                }
                if (!empty($customer)) {
                    $result[$i]->customer_first_name = $customer[0]['first_name'];
                    $result[$i]->customer_last_name = $customer[0]['last_name'];
                    $result[$i]->customer_profile_picture = $customer[0]['profile_picture'];
                    $result[$i]->plan_turn_around_days = $customer[0]['plan_turn_around_days'];
                } else {
                    $result[$i]->customer_first_name = "";
                    $result[$i]->customer_last_name = "";
                    $result[$i]->customer_profile_picture = "";
                    $result[$i]->plan_turn_around_days = 0;
                }
            }
        } else {
            echo '<div class="nodata">
                                <h3 class="head-b draft_no">They are no projects at this time</h3>
                            </div>';
        }

        return $result;
    }

    public function GetAutocomplete_qa_customer($options = array(), $status = array(), $customer_id, $datarole) {
        $this->db->select('a.*','c.plan_name as current_plan_name');
        $this->db->from('requests as a');
        $this->db->join('users as c', 'c.id = a.customer_id','left');
        $this->db->join('users as b', 'b.id = a.designer_id','left');
        $this->db->where_in('a.status', $status);
        if ($options['keyword'] != "") {
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            //$this->db->or_like('b.email', $options['keyword'], 'both');
            $this->db->group_end();
        }
        if ($customer_id) {
            $this->db->where('a.customer_id', $customer_id);
        }
        if ($datarole) {
            $this->db->where_in('a.status', $status);
        }
        if($status[0] == 'active' || $status[1] == 'disapprove'){
        $this->db->order_by("latest_update","ASC");
        }elseif($status[0] == 'assign' || $status[1] == 'pending'){
            $this->db->order_by("priority","ASC");
        }elseif ($status[0] == "pendingrevision") {
            $this->db->order_by("modified", "ASC");
        } elseif ($status[0] == 'checkforapprove') {
            $this->db->order_by("modified","ASC");
        }
        elseif ($status[0] == "approved") {
        $this->db->order_by("approvaldate", "DESC");
        }  else {
         $this->db->order_by("modified", "DESC");
        }
        //echo $orderDate;
        // $sql = "SELECT * from requests ORDER BY GREATEST(".$orderDate.")";
        // $data = $this->db->query($sql);
        // $result = $data->result_array();

    
        $query = $this->db->get();
     //   echo $this->db->last_query();
        $result = $query->result();


        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]->designer_id);
            $customer = $this->getuserbyid($result[$i]->customer_id);
            if (!empty($designer)) {
                $result[$i]->designer_first_name = $designer[0]['first_name'];
                $result[$i]->designer_last_name = $designer[0]['last_name'];
                $result[$i]->profile_picture = $designer[0]['profile_picture'];
            } else {
                $result[$i]->designer_first_name = "";
                $result[$i]->designer_last_name = "";
                $result[$i]->profile_picture = "";
            }
            if (!empty($customer)) {
                $result[$i]->customer_first_name = $customer[0]['first_name'];
                $result[$i]->customer_last_name = $customer[0]['last_name'];
                $result[$i]->customer_profile_picture = $customer[0]['profile_picture'];
                ;
                $result[$i]->plan_turn_around_days = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]->customer_first_name = "";
                $result[$i]->customer_last_name = "";
                $result[$i]->customer_profile_picture = "";
                $result[$i]->plan_turn_around_days = 0;
            }
        }
        return $result;
    }

    public function GetAutocomplete_for_admin($options = array(), $status = array(), $customer_id, $datarole) {
        $this->db->select('a.*','b.plan_name as current_plan_name');
        $this->db->from('requests as a');
        $this->db->join('users as b', 'b.id = a.customer_id','left');
        $this->db->join('users as b', 'b.id = a.designer_id','left');
        if ($datarole) {
            $this->db->where_in('a.status', $status);
        } else {
            $this->db->where_in('a.status_admin', $status);
        }
        if ($options['keyword'] != "") {
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            //$this->db->or_like('b.email', $options['keyword'], 'both');
            $this->db->group_end();
        }
        if ($customer_id) {
            $this->db->where('a.customer_id', $customer_id);
        }
        if ($status[0] == "approved") {
            $this->db->order_by("a.approvaldate", "DESC");
        } elseif ($status[0] == "active") {
            $this->db->order_by("a.latest_update", "ASC");
        } elseif($status[0] == 'assign'){
            $this->db->order_by("priority","ASC");
        }
        elseif ($status[0] == "pendingrevision") {
            $this->db->order_by("modified", "ASC");
        }
        elseif ($status[0] == "checkforapprove") {
            $this->db->order_by("a.modified", "ASC");
        } else {
            $this->db->order_by("a.latest_update", "ASC");
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        $result = $query->result();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]->designer_id);
            $customer = $this->getuserbyid($result[$i]->customer_id);
            if (!empty($designer)) {
                $result[$i]->designer_first_name = $designer[0]['first_name'];
                $result[$i]->designer_last_name = $designer[0]['last_name'];
                $result[$i]->profile_picture = $designer[0]['profile_picture'];
            } else {
                $result[$i]->designer_first_name = "";
                $result[$i]->designer_last_name = "";
                $result[$i]->profile_picture = "";
            }
            if (!empty($customer)) {
                $result[$i]->customer_first_name = $customer[0]['first_name'];
                $result[$i]->customer_last_name = $customer[0]['last_name'];
                $result[$i]->customer_profile_picture = $customer[0]['profile_picture'];
                ;
                $result[$i]->plan_turn_around_days = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]->customer_first_name = "";
                $result[$i]->customer_last_name = "";
                $result[$i]->customer_profile_picture = "";
                $result[$i]->plan_turn_around_days = 0;
            }
        }
        return $result;
    }

    public function GetAutocomplete_admin($options = array(), $status = array(), $customer_id) {
        $this->db->select('a.*,b.plan_name as current_plan_name');
        $this->db->from('requests as a');
        
        $this->db->where_in('a.status_admin', $status);
        if ($options['keyword'] != "") {
            $this->db->join('users as b', 'b.id = a.customer_id OR b.id = a.designer_id');
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            //$this->db->or_like('b.email', $options['keyword'], 'both');
            $this->db->group_end();
        }
        if ($customer_id) {
            $this->db->where('a.customer_id', $customer_id);
        }
        $this->db->group_by('a.id');
        if ($status[0] == "approved") {
            $this->db->order_by("a.approvaldate", "DESC"); 
        } elseif ($status[0] == "assign") {
            $this->db->order_by("priority","ASC");
        } elseif ($status[0] == "pendingrevision") {
            $this->db->order_by("a.modified", "ASC");
        } elseif($status[0] == 'checkforapprove'){
            $this->db->order_by("a.modified", "ASC");
        }else {
            $this->db->order_by("DATE(a.latest_update)", "ASC");
        }
        $this->db->limit(10, 0);
        $query = $this->db->get(); 
        //echo $this->db->last_query();
        $result = $query->result();





        /* $this->db->select('a.*');
          $this->db->from('requests as a');
          $this->db->join('users as b','b.id = a.customer_id OR b.id = a.designer_id');
          $this->db->where_in('a.status',$status);
          $this->db->like('a.title', $options['keyword'], 'both');
          $this->db->or_like('b.first_name', $options['keyword'], 'both');
          $this->db->or_like('b.last_name', $options['keyword'], 'both');
          $this->db->or_like('b.email', $options['keyword'], 'both');
          // $this->db->join('users as b','b.id = a.designer_id');
          if($customer_id){
          $this->db->where('a.customer_id',$customer_id);
          }
          if($status[0] == "approved"){
          $this->db->order_by("DATE(a.approvaldate)","DESC");
          }elseif($status[0] == "assign"){
          //$this->db->order_by("priority","ASC");
          }elseif($status[0] == "pendingrevision"){
          $this->db->order_by("DATE(a.modified)","DESC");
          }else{
          $this->db->order_by("DATE(a.latest_update)","DESC");
          }
          $this->db->limit(10, 0);
          $query = $this->db->get();
          $result = $query->result(); */
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]->designer_id);
            $customer = $this->getuserbyid($result[$i]->customer_id);
            if (!empty($designer)) {
                $result[$i]->designer_first_name = $designer[0]['first_name'];
                $result[$i]->designer_last_name = $designer[0]['last_name'];
                $result[$i]->profile_picture = $designer[0]['profile_picture'];
            } else {
                $result[$i]->designer_first_name = "";
                $result[$i]->designer_last_name = "";
                $result[$i]->profile_picture = "";
            }
            if (!empty($customer)) {
                $result[$i]->customer_first_name = $customer[0]['first_name'];
                $result[$i]->customer_last_name = $customer[0]['last_name'];
                $result[$i]->customer_profile_picture = $customer[0]['profile_picture'];
                ;
                $result[$i]->plan_turn_around_days = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]->customer_first_name = "";
                $result[$i]->customer_last_name = "";
                $result[$i]->customer_profile_picture = "";
                $result[$i]->plan_turn_around_days = 0;
            }
        }
        return $result;
    }
    
    public function update_customer_active_status($table,$data,$where)
    {
        foreach($where as $key=>$value){
            $this->db->where($key,$value);
        }
        $data = $this->db->update($table, $data); 
        //echo $this->db->last_query();
        return $data;
    }

    public function GetAutocomplete_designer($options = array(), $status = array(), $customer_id, $datarol) {
        $this->db->select('a.*','b.plan_name as current_plan_name');
        $this->db->from('requests as a');
        $this->db->join('users as b', 'b.id = a.customer_id','LEFT');
        if ($datarol) {
            $this->db->where_in('a.status_designer', $status);
        } else {
            $this->db->where_in('a.status', $status);
        }
        if ($options['keyword'] != "") {
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            //$this->db->or_like('b.email', $options['keyword'], 'both');
            $this->db->group_end();
        }

        // $this->db->select('*');
        //      $this->db->like('title', $options['keyword'], 'both');
        //      if($datarol){
        //          $this->db->where_in('status_designer',$status);
        //      }else{
        //      $this->db->where_in('status',$status);
        //     }
        if ($customer_id) {
            $this->db->where('a.designer_id', $customer_id);
        }
        if($status[0] == 'active' || $status[1] == 'disapprove'){
         $this->db->order_by("latest_update","ASC");
        }
        elseif ($status[0] == "approved") {
            $this->db->order_by("a.approvaldate", "DESC");
        } elseif ($status[0] == "assign") {
            $this->db->order_by("a.priority", "ASC");
        } elseif ($status[0] == "pendingrevision") {
            $this->db->order_by("a.modified", "ASC");
        } elseif ($status[0] == "checkforapprove") {
                $this->db->order_by("modified", "ASC");
         } else {
            $this->db->order_by("DATE(a.latest_update)", "ASC");
        }
        //$this->db->order_by("modified","DESC");
        $query = $this->db->get();
        $result = $query->result();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]->designer_id);
            $customer = $this->getuserbyid($result[$i]->customer_id);
            if (!empty($designer)) {
                $result[$i]->designer_first_name = $designer[0]['first_name'];
                $result[$i]->designer_last_name = $designer[0]['last_name'];
                $result[$i]->profile_picture = $designer[0]['profile_picture'];
            } else {
                $result[$i]->designer_first_name = "";
                $result[$i]->designer_last_name = "";
                $result[$i]->profile_picture = "";
            }
            if (!empty($customer)) {
                $result[$i]->customer_first_name = $customer[0]['first_name'];
                $result[$i]->customer_last_name = $customer[0]['last_name'];
                $result[$i]->customer_profile_picture = $customer[0]['profile_picture'];
                ;
                $result[$i]->plan_turn_around_days = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]->customer_first_name = "";
                $result[$i]->customer_last_name = "";
                $result[$i]->customer_profile_picture = "";
                $result[$i]->plan_turn_around_days = 0;
            }
        }
        return $result;
    }

// Ajax Request for active project search

    public function ajax_getall_request($options = array(), $status = array(), $orderby = "") {
        $this->db->select('a.*');
        $this->db->from('requests as a');
        $this->db->join('users as b', 'b.id = a.designer_id');
        $this->db->where_in('a.status', $status);
        if ($options['keyword'] != "") {
            $this->db->group_start();
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            //$this->db->or_like('b.email', $options['keyword'], 'both');
            $this->db->group_end();
        }
        $this->db->where('a.customer_id', $_SESSION['user_id']);
        if ($orderby == "") {
            if(in_array('active','disapprove','assign','pending','checkforapprove',$status)){
                 $this->db->order_by("DATE(a.created)", "DESC");
            }else{
                $this->db->order_by("DATE(a.created)", "DESC");
            }
        }else {
            $this->db->order_by($orderby, "ASC");
        }
        $this->db->limit(LIMIT_CUSTOMER_LIST_COUNT, 0);
        $data = $this->db->get();
        $result = $data->result_array();
        // echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

// Ajax Request for active project search End
// Ajax Request for Designer search
    public function getall_request_designer_ajax_search($options = array(), $status = array(), $orderby = "") {
        $this->db->select('a.*,b.plan_name as current_plan_name,');
        $this->db->from('requests as a');
        
        $this->db->where_in('a.status_designer', $status);
        if ($options['keyword'] != "") {
            $this->db->group_start();
            $this->db->join('users as b', 'b.id = a.customer_id');
            $this->db->like('a.title', $options['keyword'], 'both');
            $this->db->or_like('b.first_name', $options['keyword'], 'both');
            $this->db->or_like('b.last_name', $options['keyword'], 'both');
            //$this->db->or_like('b.email', $options['keyword'], 'both');
            $this->db->group_end();
        }
        $this->db->where('a.designer_id', $_SESSION['user_id']);
        if ($orderby == "") {
            if($status[0] == 'active'){
              $this->db->order_by("DATE(a.latest_update)", "ASC");
            }elseif($status[0] =='disapprove'){
            $this->db->order_by("DATE(a.latest_update)", "ASC");
            }elseif($status[0] =='pendingrevision'){
             $this->db->order_by("DATE(a.modified)", "ASC");   
            }elseif($status[0] =='checkforapprove'){
             $this->db->order_by("a.modified", "ASC");   
            }
            elseif ($status[0] == "approved") {
                   $this->db->order_by("approvaldate", "DESC");
            } else {
                $this->db->order_by($orderby, "DESC");
            }
        }
        $data = $this->db->get();
        $result = $data->result_array();
       // echo "<pre>";print_r($status);
       // echo $this->db->last_query();
        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
                $result[$i]['plan_turn_around_days'] = $customer[0]['plan_turn_around_days'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
                $result[$i]['customer_profile_picture'] = "";
                $result[$i]['plan_turn_around_days'] = 0;
            }
        }
        return $result;
    }

// Ajax Request for Designer search End

    public function getuserbyrole($role) {
        $this->db->select('id');
        $this->db->where('role', $role);
        $this->db->order_by('id', "ASC");
        $data = $this->db->get('users');
        $result = $data->result_array();
        return $result;
    }

    public function get_blog_by_id($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $data = $this->db->get('blog');
        $result = $data->result_array();
        return $result;
    }

    public function get_portfolio_by_id($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $data = $this->db->get('protfolio');
        $result = $data->result_array();
        return $result;
    }

    public function getAllSubscriptionPlan() {
        $this->db->select("*");
        $data = $this->db->get('subscription_plan');
        $result = $data->result_array();
        return $result;
    }

    public function getallskills($user_id) {
        $this->db->select("*");
        $this->db->where('user_id', $user_id);
        $data = $this->db->get('user_skills');
        $result = $data->result_array();
        return $result;
    }

    public function get_all_designers($isdisabled = NULL) {
        $this->db->select('*');
        $this->db->where('role', 'designer');
        if($isdisabled){
          $this->db->where('is_active', '1');
        }
        $data = $this->db->get('users');
        $result = $data->result_array();
        return $result;
    }
    public function get_all_va() {
        $this->db->select('*');
        $this->db->where('role', 'admin');
        $this->db->where('full_admin_access', '0');
        $data = $this->db->get('users');
        //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }

    public function get_file_all($ids) {
        $this->db->select('*');
        $this->db->where_in('request_id', $ids);
        $data = $this->db->get('request_files');
        $result = $data->result_array();
        return $result;
    }

    public function getrequest_by_today_date($todaydate) {
        $this->db->select('*');
        $this->db->where('DATE(dateinprogress)', $todaydate);
        $data = $this->db->get('requests');
        $result = $data->result_array();
        return $result;
    }

    public function get_all_designers_for_today_request() {
        $this->db->select('id,email');
        $this->db->where('role', 'designer');
        $data = $this->db->get('users');
        $result = $data->result_array();
        //return $result;
        for ($i = 0; $i < count($result); $i++) {
            $this->db->select('id,title');
            $this->db->where_in('status', array('active', 'disapprove'));
            $this->db->where('designer_id', $result[$i]['id']);
            $rdata = $this->db->get('requests');
            $rresult = $rdata->result_array();
            $result[$i]['allrequest'] = $rresult;
        }
        return $result;
    }

    public function get_protfolio_category() {
        $this->db->select('*');
        $data = $this->db->get('protfolio_categories');
        $result = $data->result_array();
        return $result;
    }

    public function get_all_tags() {
        $this->db->select('tags');
        $this->db->group_by('tags');
        $data = $this->db->get('blog');
        $result = $data->result_array();
        return $result;
    }

    public function get_all_category() {
        $this->db->select('category');
        $this->db->group_by('category');
        $data = $this->db->get('blog');
        $result = $data->result_array();
        return $result;
    }

    public function get_all_users() {
        $this->db->select('password_reset,email');
        $data = $this->db->get('users');
        $result = $data->result_array();
        return $result;
    }

    public function get_user_by_id($id) {
          $this->db->select('first_name,last_name,profile_picture,is_trail,phone,can_trialdownload,is_trailverified');
        $this->db->where('id', $id);
        $data = $this->db->get('users');
        $result = $data->result_array();
        return $result;
    }

    public function get_designer_active_request($id) {
        $this->db->where('designer_id', $id);
        $this->db->where_not_in('status', 'approved');
        $num_rows = $this->db->count_all_results('requests');
        return $num_rows;
    }

    // Get Request By Designer id from admin dashboard controller
    public function get_request_by_designer_id($id) {
        $this->db->select('*');
        $this->db->where('designer_id', $id);
        $query = $this->db->get('requests');
        $result = $query->result_array();
        return $result;
    }

    // Get Designer Information
    public function get_designers_info($category) {
        $maping = [
            'Popular Design' => 'graphics',
            'Branding & Logos' => 'brandin_logo',
            'Social Media' => 'ads_banner',
            'Marketing' => 'email_marketing',
            'Email' => 'email_marketing',
            'Ads & Banners' => 'ads_banner',
            'Web Design' => 'webdesign',
            'App Design' => 'appdesign'
        ];
        $this->db->select('a.*');
        $this->db->from('user_skills as a');
        $this->db->where($maping[$category] . "!= 0");
        $this->db->join('users as b', 'b.id = a.user_id');
        $this->db->where('b.is_active', 1);
        $query = $this->db->get();
       // echo $this->db->last_query();
        $result = $query->result_array();
        return $result;
    }
    
    //Get request_discussions count for default qa message
    public function request_discussions_count_for_qa_message($id) {
        $this->db->where('request_id', $id);
        $this->db->where('message', 'The designer is currently working on your project. Send messages here to share updates about the overall project. If you want to share comments about an individual design you can use the comments section within each design draft.');
        $count = $this->db->count_all_results('request_discussions'); 
       //echo  $this->db->last_query();
        return $count;
    }

    //Get Designer Projects list Count
    public function get_request_count_by_designer_id($id) {
        $this->db->where('designer_id', $id);
        $count = $this->db->count_all_results('requests');
        return $count;
    }

    // For Delete Designer From Admin
    public function delete_user($table, $id) {
        $this->db->where('id', $id);
        $result = $this->db->delete($table);
        return $result;
    }

    //Get Request by Customer Id 
    public function get_request_by_customer_id($id) {
        $this->db->select('*');
        $this->db->where('customer_id', $id);
        $this->db->where('status !=', 'approved');
        $query = $this->db->get('requests');
        $result = $query->result_array();
        return $result; 
    }

    //Update Request Data from Admin or Qa
    public function update_request_admin($request_ids, $designer_id) {
        if(!empty($request_ids)){
        $this->db->where_in('id', $request_ids);
        $data = $this->db->update('requests', $designer_id);
       // echo $this->db->last_query();
        return $data;
        }
    }
    
    /*************Reports section start*******************/
    public function getCustomercount($Cdate){
        $currentdate  = isset($Cdate)?$Cdate:date('Y-m-d');
        //$yesterdaydate = date('Y-m-d h:i:s', time()-86400);
        $this->db->select("*");
        $this->db->where("date(u.created) ='" . $currentdate . "' and u.is_trail = 0 and u.role = 'customer'");
        $data = $this->db->get("users as u");
        //echo $this->db->last_query();exit;
        $result = $data->result_array();
        $count_cust = count($result);
        return $count_cust;
    }
    
    public function getfreecustomercount($Cdate){
        $currentdate  = isset($Cdate)?$Cdate:date('Y-m-d');
        //$yesterdaydate = date('Y-m-d h:i:s', time()-86400);
        $this->db->select("*");
        $this->db->where("date(u.created) ='" . $currentdate . "' and u.is_trail = 1 and u.role = 'customer'");
        $data = $this->db->get("users as u");
        //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return COUNT($result);
    }
    
    public function gettodaysrequestcount($Cdate){
        $currentdate  = isset($Cdate)?$Cdate:date('Y-m-d');
        //$yesterdaydate = date('Y-m-d h:i:s', time()-86400);
        $this->db->select("*");
        $this->db->where("date(r.created) ='" . $currentdate . "'");
        $data = $this->db->get("requests as r");
        //echo $this->db->last_query();exit;
        $result = $data->result_array();
        return COUNT($result);
    }
    
    public function gettrialcustRequestcount(){
        $this->db->select('*')->from('requests');
        $this->db->where('`customer_id` IN (SELECT `id` FROM `users` where `is_trail` = 1 and `role` = "customer")', NULL, FALSE);
        $data = $this->db->get();
        $result = $data->result_array();
        //echo "<pre/>";print_r($result);exit;
        return count($result);
       }
       
       public function getnormalcustRequestcount(){
        $this->db->select('*')->from('requests');
        $this->db->where('`customer_id` IN (SELECT `id` FROM `users` where `is_trail` = 0 and `role` = "customer")', NULL, FALSE);
        $data = $this->db->get();
        $result = $data->result_array();
        //echo "<pre/>";print_r($result);exit;
        return count($result);
       }
       
       public function gettrialapprovedrequest(){
        $this->db->select('*')->from('requests');
        $this->db->where('`customer_id` IN (SELECT `id` FROM `users` where `is_trail` = 1 and `role` = "customer") and `status` = "approved"', NULL, FALSE);
        $data = $this->db->get();
        //echo $this->db->last_query();exit;
        $result = $data->result_array();
        //echo "<pre/>";print_r($result);exit;
        return count($result);
       }
       
       public function getavgrequestcount(){
        $this->db->select('*')->from('requests');
        $this->db->where('MONTH(`created`) = MONTH(CURRENT_DATE()) AND YEAR(`created`) = YEAR(CURRENT_DATE())');
        $data = $this->db->get();
        $req = $data->result_array();
        $avgreq = count($req);
        $this->db->select('*')->from('requests');
        $this->db->where('MONTH(`created`) = MONTH(CURRENT_DATE()) AND YEAR(`created`) = YEAR(CURRENT_DATE()) GROUP BY `customer_id`');
        $custrec = $this->db->get();
        $reqcust = $custrec->result_array();
        $avgcustreq = count($reqcust);
//        echo $avgreq."<br/>";
//        echo $avgcustreq;exit;
        $finalavg = $avgreq/$avgcustreq;
        return number_format((float)$finalavg, 2, '.', '');
       }
       
       public function getavgapprovedrequestcount(){
        $this->db->select('*')->from('requests');
        $this->db->where('MONTH(`created`) = MONTH(CURRENT_DATE()) AND YEAR(`created`) = YEAR(CURRENT_DATE()) and `status` = "approved"');
        $data = $this->db->get();
        //echo $this->db->last_query();exit;
        $req = $data->result_array();
        $avgreq = count($req);
        $this->db->select('*')->from('requests');
        $this->db->where('MONTH(`created`) = MONTH(CURRENT_DATE()) AND YEAR(`created`) = YEAR(CURRENT_DATE()) and `status` = "approved" GROUP BY `customer_id`');
        $custrec = $this->db->get();
        //echo $this->db->last_query();exit;
        $reqcust = $custrec->result_array();
        $avgcustreq = count($reqcust);
//        echo $avgreq."<br/>";
//        echo $avgcustreq;exit;
        $finalavg = $avgreq/$avgcustreq;
        return number_format((float)$finalavg, 2, '.', '');
       }
       
       public function gettraildesignscount(){
        $this->db->select('*')->from('requests');
        $this->db->where('`status` IN ("checkforapprove") AND `customer_id` IN (SELECT `id` FROM `users` where `is_trail` = 1 and `role` = "customer") GROUP BY `customer_id`');
        $data = $this->db->get();
       // echo $this->db->last_query();exit;
        $reqcust = $data->result_array();
        return count($reqcust);
       }
       
    /*************Reports section END*******************/
       
       public function get_user_by_requestid($reqid){
        $this->db->select('r.customer_id as customer_id,d.is_trail as is_trial,can_trialdownload');
        $this->db->from('requests as r');
        $this->db->join('users as d', 'd.id = r.customer_id', 'LEFT');
        $this->db->where('r.id',$reqid);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $result = $query->result_array();
        return $result;
   }
   
   /**************************Move code(07jan)************/
   public function get_Allbrandprofile() {
        $this->db->select("*");
        $data = $this->db->get("brand_profile");
        $result = $data->result_array();
        return $result;
    }
    
    public function isBrandselected($cID) {
        $this->db->select("brand_profile_access");
        $this->db->where('customer_id',$cID);
        $data = $this->db->get("user_permissions");
        $result = $data->result_array();
        if($result[0]['brand_profile_access'] == 0){
            return true;
        }
    }
    
    public function selectedBrandsforuser($cID){
        $this->db->select("brand_id");
        $this->db->where('user_id',$cID);
        $data = $this->db->get("user_brand_profiles");
        $result = $data->result_array(); 
        return $result;
    }
    
    public function deleteAlluserbrands($cID){
        $this->db->where('user_id', $cID);
        $result = $this->db->delete('user_brand_profiles');
        //echo $this->db->last_query();exit;
        return $result;
    }
    
    public function getuserpermissions($uId){
        $this->db->select("*");
        $this->db->where('customer_id', $uId);
        $data = $this->db->get("user_permissions");
        //echo $this->db->last_query();exit;
        $result = $data->result_array(); 
        return $result;
    }
    public function get_user_brand_profile_by_user_id($id) {
        $this->db->select("*");
        $this->db->where("user_id = '" . $id . "'");
        $data = $this->db->get("user_brand_profiles");
        $result = $data->result_array();
//       echo $this->db->last_query();
        return $result;
        
    }
    
    public function getuserspermisionBrands($uid){
        $userID = isset($uid) ? $uid : $_SESSION['user_id'];
        $this->db->select("brand_profile_access");
        $this->db->where("customer_id = '" . $userID . "'");
        $data = $this->db->get("user_permissions");
        $result = $data->result_array();
        $brand_ids = array();
        if($result){
            if($result[0]['brand_profile_access'] == 0){
               $this->db->select("brand_id");
               $this->db->where("user_id = '" . $userID . "'");
               $data = $this->db->get("user_brand_profiles");
               $result1 = $data->result_array();
               foreach($result1 as $val){
                   $brand_ids[] = $val['brand_id'];
               }
            }
            return $brand_ids;
            //echo "<pre>";print_r($brand_ids); 
        }
        //echo "<pre>";print_r($brand_ids);  
    }
    
    public function getAllsubUsers($pID,$flag="",$is_active="",$count="",$parameter=""){
        if($parameter != ""){
            $this->db->select($parameter);
        }else{
            $this->db->select("*");
        }
        $this->db->where('parent_id', $pID);
        if($flag != ""){
            $this->db->where("user_flag = '" . $flag . "'");
        }
        if($is_active != ""){
            $this->db->where("is_active = 1");
        }
        if($count != ""){
         $result = $this->db->count_all_results('users');
//         echo $this->db->last_query();
        }else{
         $data = $this->db->get("users");
//         echo $this->db->last_query();
         $result = $data->result_array(); 
        }
        return $result; 
        
    }
/************03.01.2018**********/
    public function getAllrequestbybrandId($brandId,$reqbrndid = null){
        $this->db->select("*");
        if($reqbrndid == '' || $reqbrndid == NULL ){
        $this->db->where("brand_id = '" . $brandId . "'" );  
        }else{
           $this->db->where_in("brand_id" , $reqbrndid);  
        }
        $data = $this->db->get("requests");
       // echo $this->db->last_query();
        $result = $data->result_array();
        return $result;
    }
    public function deleteAllrequestbybrandId($brandId,$reqbrndid = null){
        $this->db->select("*");
        if($reqbrndid == '' || $reqbrndid == NULL ){
        $this->db->where("brand_id = '" . $brandId . "'" );  
        }else{
           $this->db->where_in("brand_id" , $reqbrndid);  
        }
        $data = $this->db->delete("requests");
        return $data;
    } 
    public function delete_user_brand_profile_by_brand_id($id) {
        $this->db->select("*");
        $this->db->where("brand_id = '" . $id . "'");
        $data = $this->db->delete("user_brand_profiles");
//        echo $this->db->last_query();
        return $data;
    }
    public function delete_brandprofile_materials_files($brand_id) {
        $this->db->select("*");
        $this->db->where("brand_id = '" . $brand_id . "'");
        $data = $this->db->delete("brand_profile_material");
        //echo $this->db->last_query();
        return $data;
    }
    public function update_req_brand_id($assign_brnd,$req_id) {
        $this->db->where_in("id ",$req_id);
        $data = $this->db->update("requests",$assign_brnd);
     //   echo $this->db->last_query();
        return $data;
    }
    public function getAlltimezone(){
        $this->db->select("*");
        $this->db->order_by("zone_name","ASC");
        $data = $this->db->get("timezone");
        //echo $this->db->last_query();
        $result = $data->result_array();
        return $result;
    }
    
    public function get_latestdraft_file($request_id) {
        $this->db->select("*");
        $this->db->where("request_id = '" . $request_id . "'");
        //$this->db->where("status = '" . 'customerreview' . "'");
        $this->db->where("status IN ('customerreview','Approve')");
        $this->db->order_by("created","DESC");
        $data = $this->db->get("request_files");
        //echo $this->db->last_query();
        $result = $data->result_array();
        return $result;
    }
    
    public function ifuserNotinpermision($uid) {
        $this->db->select("*");
        $this->db->where("customer_id = '" . $uid . "'");
        $data = $this->db->get("user_permissions");
//        echo $this->db->last_query();
        $result = $data->result_array();
        if($result){
            return true;
        }else{
            return false;
        }
    }
    public function delete_user_permission($id) {
        $this->db->where('customer_id', $id);
        $result = $this->db->delete('user_permissions');
        //echo $this->db->last_query();exit;
        return $result;
    }
        
        /********new functions - 14.02.2018 **********/
   public function get_all_latest_message($start = 0, $limit=150,$dateTimeMinutesAgo = '', $msgflag) {
      // echo $msgflag;exit;
       if($msgflag == 0){
          $where = " where req.dummy_request != 1";
          $where1 = "where rd.admin_seen = " .$msgflag. ""; 
          $where2 = "where rfc.admin_seen = " .$msgflag. " and rf.status != 'pending' and rf.status != 'Reject'";
          $orderby = "asc";
       }elseif($msgflag == 1){
          $where = " where subq.request_id not IN (select rdd.request_id from request_discussions as rdd where rdd.admin_seen = 0) and subq.request_id not IN (select rff.request_id from request_file_chat as rfcc inner join request_files rff on rff.id = rfcc.request_file_id where rfcc.admin_seen = 0 and rff.status != 'Reject')  and req.dummy_request != 1";
          $where1 = "where rd.admin_seen = " .$msgflag. ""; 
          $where2 = "where rfc.admin_seen = " .$msgflag. " and rf.status != 'pending' and rf.status != 'Reject'";
          $orderby = "desc";
           
       }elseif($msgflag == 2){
          $where = "where req.is_star = 1 and req.dummy_request != 1";
          $where1 = "";
          $where2 = "";
          $orderby = "desc";
       }
        $sql = "SELECT subq.request_id as rd_request_id, subq.message rd_message,subq.created,subq.typee, subq.sender_id as rd_sender_id, subq.sender_type as rd_sender_type, subq.reciever_id as rd_reciever_id, subq.reciever_type as rd_reciever_type, subq.message as rd_message, subq.customer_seen as rd_customer_seen, subq.admin_seen as rd_admin_seen,subq.created as rd_created,req.id as request_id,req.title as request_title,req.status as request_status,req.who_reject as request_who_reject,req.status_admin as req_admin_status,req.description as req_description,req.latest_update as req_last_update,req.modified as req_modified,req.approvaldate as req_approvaldate,req.is_star as is_star, u.id as userid,u.first_name as user_fname,u.last_name as user_lname,u.email as user_email,u.profile_picture as user_profile_picture,u.current_plan as user_current_plan from (
                (select rd.request_id,rd.message,rd.sender_id,rd.sender_type,rd.reciever_id,rd.reciever_type,rd.customer_seen,rd.admin_seen,rd.created,'main' as typee from request_discussions as rd ". $where1 ." order by rd.created $orderby)
                UNION
                (select rf.request_id,rfc.message,rfc.sender_id,rfc.sender_role,rfc.receiver_id,rfc.receiver_role,rfc.customer_seen,rfc.admin_seen,rfc.created,'file' as typee from request_file_chat as rfc inner join request_files rf on rf.id = rfc.request_file_id ". $where2 ." order by rfc.created $orderby)
                ) as subq
                inner join requests req on req.id = subq.request_id 
                left join users u on u.id = req.customer_id ". $where ."
                order by subq.admin_seen asc, subq.created $orderby limit $start, $limit";
        $query = $this->db->query($sql);
     //  echo $sql;
        $result = $query->result_array();
        return $result;
    }
    
    public function count_all_unseen_latest_message() {
      
        $sql = "SELECT subq.request_id,subq.admin_seen,subq.created from (
                (select rd.request_id,rd.admin_seen,rd.created from request_discussions as rd where rd.admin_seen = 0 order by rd.created desc)
                UNION
                (select rf.request_id,rfc.admin_seen,rfc.created from request_file_chat as rfc inner join request_files rf on rf.id = rfc.request_file_id where rfc.admin_seen = 0 and rf.status != 'pending' and rf.status != 'Reject' order by rfc.created desc)
                ) as subq
                inner join requests req on req.id = subq.request_id 
                left join users u on u.id = req.customer_id where req.dummy_request != 1
                GROUP BY req.id order by subq.admin_seen asc, subq.created desc";
        $query = $this->db->query($sql);
       // echo $sql;
        $result = $query->result_array();
        $countunread = count($result);
        return $countunread;
    }
    public function count_all_star_message() {
      
        $this->db->select("*");
        $this->db->where("requests.is_star = 1 and requests.dummy_request != 1");
        $data = $this->db->get("requests");
        //echo $this->db->last_query();exit; 
        $result = $data->result_array();
        $countstar = count($result);
        return $countstar;
    }
    public function get_all_unseen_message_comment_chat($dateTimeMinutesAgo) {
        $this->db->select("r.*,req.id as request_id,req.title as request_title,req.status as request_status,req.status_admin as req_admin_status,req.description as req_description,u.id as userid,u.first_name as user_fname,u.last_name as user_lname,u.email as user_email");
        $this->db->from('request_file_chat as r');
        $this->db->where("r.created >= '".$dateTimeMinutesAgo."'");
        $this->db->join('request_files as rf', 'rf.id = r.request_file_id','left');
        $this->db->join('requests as req', 'req.id = rf.request_id','left');
        $this->db->join('users as u', 'u.id = req.customer_id','left');
        $this->db->group_by('rf.request_id'); 
        $this->db->order_by("u.id", "ASC");
        $data = $this->db->get();
        //echo $this->db->last_query();
        $result = $data->result_array();
        //echo "<pre>";print_r($result);
        return $result;
    }
    
    public function get_image_by_request_file_id($id) {
        $this->db->select("*");
        $this->db->where("id = '" . $id . "'");
        $data = $this->db->get("request_files");
        $result = $data->result_array();
        return $result;
        
    }
     public function get_commentfile_chat($request_file_id, $role) {
        $this->db->select("r.*,u.profile_picture");
        $this->db->from('request_file_chat as r');
        $this->db->where("r.request_file_id='" . $request_file_id . "'");
        $this->db->where("r.is_deleted != 1");
        $this->db->join('users as u', 'r.sender_id = u.id','left');
        $this->db->order_by("created","ASC");
        $data = $this->db->get();
//        echo $this->db->last_query();
        return $result = $data->result_array();
    }
    /***** 30.01.2019 *****/
     public function mark_read_main_chat_msg($id,$admin_seen) {
        $this->db->where("request_id ",$id);
        $data = $this->db->update("request_discussions",$admin_seen);
       //echo $this->db->last_query();
        return $data;
     }
     public function mark_read_comment_chat_msg($id,$admin_seen) {
        $this->db->where_in("request_file_id ",$id);
        $data = $this->db->update("request_file_chat",$admin_seen);
      //  echo $this->db->last_query();
        return $data;
     }
     
     /**********sharing function*************/
    public function getlinkSharedprivateData($reqid,$draftid = NULL) {
         $this->db->select("ls.id,ls.user_name,ls.request_id,ls.draft_id,ls.public_link,ls.share_key,ls.email,lsp.can_view,lsp.mark_as_completed,lsp.download_source_file,lsp.commenting");
         $this->db->where("ls.share_type='" .'private'. "'");
         $this->db->where("ls.draft_id='" .$draftid. "' and ls.request_id='" .$reqid. "'");
         $this->db->join('link_sharing_permissions as lsp', 'lsp.sharing_id = ls.id','left');
         $this->db->from('link_sharing as ls');
         $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
//        echo "<pre/>";print_r($result);
        return $result;
    }
    
    public function isSheredEmaiexists($email,$request_id = NULL,$draft_id = NULL) {
        $this->db->select("*");
        if(isset($request_id) && isset($draft_id)){
            $this->db->where("email = '" . $email . "' and request_id = '" .$request_id. "' and draft_id = '" .$draft_id."'"); 
        }else{
            $this->db->where("email = '" . $email . "' and request_id = " .$request_id);
        }
        $data = $this->db->get("link_sharing");
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
//        echo "<pre/>";print_r($result);exit;
        if($result){
           return true; 
        }
    }
    
    
    public function getAllpermissionsByshareID($shareID) {
        $this->db->select("lsp.*,ls.user_name,ls.email");
        $this->db->where("sharing_id = '" . $shareID . "'");
        $this->db->join('link_sharing as ls', 'ls.id = lsp.sharing_id','left');
        $data = $this->db->get("link_sharing_permissions as lsp");
//        echo $this->db->last_query();
        $result = $data->result_array();
        return $result;
    }
    
    public function publicLinkexists($reqid,$draftId = NULL){
        $this->db->select("*");
        if($reqid && $draftId == 0){
            //$this->db->where("request_id = '" . $reqid . "' and share_type = 'public'");
            $this->db->where("request_id = '" . $reqid . "' and draft_id = 0 and share_type = 'public'");
        }
        if($reqid && $draftId != 0){
            $this->db->where("request_id = '" . $reqid . "' and draft_id = '" .$draftId. " ' and share_type = 'public'");
        }
        $data = $this->db->get("link_sharing");
//       echo $this->db->last_query();
        $result = $data->result_array();
//        echo "<pre/>";print_r($result);
        if($result){
             return $result;
        }
       
    }
    public function getPublicshareddata($reqid,$draftid = NULL) {
        $this->db->select("ls.id,ls.user_name,ls.share_key,ls.public_link,ls.email,lsp.can_view,lsp.mark_as_completed,lsp.download_source_file,lsp.commenting");
         $this->db->where("ls.share_type='" .'public'. "'");
         if($reqid){
         $this->db->where("ls.request_id='" .$reqid. "'");
         }if($reqid && $draftid){
         $this->db->where("ls.draft_id='" .$draftid. "'");
         }
         $this->db->join('link_sharing_permissions as lsp', 'lsp.sharing_id = ls.id','left');
         $this->db->from('link_sharing as ls');
         $data = $this->db->get();
       // echo $this->db->last_query();
        $result = $data->result_array();
//        echo "<pre/>";print_r($result);
        return $result;
    }
    
    public function changepublicpermissions($publink){
        $this->db->select("ls.id");
        $this->db->where("ls.share_type='" .'public'. "' and ls.public_link = ". "'$publink'");
        $this->db->from('link_sharing as ls');
        $data = $this->db->get();
        $id = $data->result_array();
//        $this->db->select("lps.*");
//        $this->db->where("lps.sharing_id='" . "'$id'");
         return $id;
    }
    public function getpublicpermissions($shareID){
        $this->db->select("*");
        $this->db->where("sharing_id = '" . $shareID . "'");
        $data = $this->db->get("link_sharing_permissions");
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }
    
    public function deleteShareduser($shareID){
        $this->db->where('id', $shareID);
        $result = $this->db->delete('link_sharing');
        if($result){
            $this->db->where('sharing_id', $shareID);
            $result2 = $this->db->delete('link_sharing_permissions');
            if($result2){
                return true;
            }
            //return true;
        }
    }
    
   public function get_customer_unseen_msg_of_mainchat($dateTimeMinutesAgo,$id){
        $this->db->select("rd.message");
        $this->db->from('request_discussions as rd');
        $this->db->where("rd.request_id='" . $id . "'");
        $this->db->where("rd.created >= '".$dateTimeMinutesAgo."' and rd.customer_seen = '0'");
        $this->db->order_by("rd.created", "ASC");
        $data = $this->db->get();
        //echo $this->db->last_query();
        $result = $data->result_array(); 
        return $result; 
     }
     public function get_customer_unseen_msg_of_commentchat($dateTimeMinutesAgo,$id){
        $this->db->select("rfc.message");
        $this->db->from('request_file_chat as rfc');
        $this->db->where("rfc.request_file_id='" . $id . "'");
        $this->db->where("rfc.created >= '".$dateTimeMinutesAgo."' and rfc.customer_seen = '0'");
        $this->db->order_by("rfc.created", "ASC");
        $data = $this->db->get();
       //echo $this->db->last_query();
        $result = $data->result_array(); 
        return $result; 
     }
     public function get_next_plan($customer_id) {
        $this->db->select("next_plan_name");
        $this->db->where("customer_id = '" . $customer_id . "'");
        $data = $this->db->get("users");
        $result = $data->result_array();
        //echo "<pre/>";print_r($result);
        return $result;
    } 
    
     public function get_userdata_bycustomerid($customer_id="") {
        $output = array();
            $this->db->select("*");
            if($customer_id != ""){
               $this->db->where("customer_id = '" . $customer_id . "'");
            }
            $data = $this->db->get("users");
            $result = $data->result_array();
            return $result;
    }
    
     public function get_userdata_bysubs_id($cusid,$sub_id="") {
        $output = array();
            $this->db->select("id");
            $this->db->where("id = '" . $cusid . "'");
            if($sub_id != ""){
             $this->db->where("current_plan = '" . $sub_id . "'");
            }
            $data = $this->db->get("users");
            $result = $data->result_array();
             if(!empty($result)){
                    $output["subs_type"] = "gz";
                    $output["result"] = $result;
             }else{
                $this->db->select("id");
                $this->db->where("user_id = '" . $cusid . "'");
                if($sub_id != ""){
                  $this->db->where("subscription_id = '" . $sub_id . "'");
                }
                $data = $this->db->get("s_users_setting");
                $s_result = $data->result_array();
                $output["subs_type"] = "saas";
                $output["result"] = $s_result;
             }  
            
    }
    
    public function getRequestCatbyid($reqId,$cat_id = NULL) {
        $this->db->select("r.category as category,rc.name as cat_name,rc.id as cat_id");
        $this->db->where("r.id = '" . $reqId . "'");
        $this->db->join('request_categories as rc', 'r.category_id = rc.id','left');
        $this->db->from("requests as r");
        $data = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
        //echo "<pre/>";print_r($result);
        return $result;
    }
    
    public function getRequestsubCatbyid($reqId,$cat_id,$sub_id = NULL) {
        $this->db->select("rc.name as subcategory");
        $this->db->where("r.id = '" . $reqId . "' and r.category_id ='" .$cat_id. "' and r.subcategory_id = '".$sub_id."'");
        $this->db->join('request_categories as rc', 'r.subcategory_id = rc.id','inner');
        $this->db->from("requests as r");
        $data = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
//        echo "<pre/>";print_r($result);
        if(isset($result[0]['subcategory']) && $result[0]['subcategory']!=''){
            return $result[0]['subcategory'];
        }
    }
    
    /*********** delete dummy request after add dummy request **********/
    public function get_dummy_request($customerid, $dummyrequest,$status=NULL) {
        $this->db->where("(customer_id ='".$customerid."' and dummy_request='".$dummyrequest."')");
        if($status != '' || $status != NULL){
         $this->db->where("(status ='".$status."')");   
        }
        $this->db->from('requests');
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
        }
        
       /*********** get active request **********/ 
       public function get_active_requestsbyuserid($status,$customerid) {
       $this->db->where("customer_id",$customerid);
        if($status != '' || $status != NULL){
         $this->db->where("(status ='".$status."')");   
        }
        $this->db->from('requests');
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;  
       }
       
       /*********************functions for dashboard report**************************/
       public function count_quality_revision_projects($status,$whoreject) {
        $this->db->where("status_designer",$status);  
        $this->db->where("who_reject",$whoreject);
        $this->db->from('requests');
        $data = $this->db->count_all_results();
        //echo $this->db->last_query();
       // $result = $data->num_row();
        return $data;  
       }
       
      public function count_all_projects($status) {
        $this->db->where_not_in('status', $status);
        $this->db->from('requests');
        $data = $this->db->count_all_results();
       // echo $this->db->last_query();
       // $result = $data->num_row();
        return $data;  
       }
       
       public function count_all_late_projects($status,$todaytime) {
          
        $this->db->where_in('status_admin', $status);
        $this->db->where("(expected_date <='".$todaytime."')"); 
        $this->db->from('requests');
        $data = $this->db->count_all_results();
        //echo $this->db->last_query();
       // $result = $data->num_row();
        return $data;  
       }
       
       public function getall_customer_have_request_designers($id) {
        $this->db->select("*");
        $this->db->from('requests as r');
        $this->db->join('users as u', 'u.id = r.customer_id','inner');
        $this->db->where("r.designer_id != ''"); 
        $this->db->where("u.is_active = 1"); 
        $this->db->group_by($id);
        $data = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
//        echo $this->db->last_query();exit;
       // $result = $data->num_row();
        return $result;  
       }
       
       
       public function getall_main_category(){
        $this->db->select("*");
        $this->db->from('request_categories as r');
        $this->db->where("parent_id = 0"); 
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;  
        
       }
       
       public function count_requests_basedon_category($status=NULL,$categoryid,$category_name){
        if($status != NULL || $status != ''){
            $this->db->where_in('status_admin', $status);
        }
        $this->db->where("category ='".$category_name."' or category_id ='".$categoryid."'"); 
        $this->db->from('requests');
        $data = $this->db->count_all_results();
//        echo $this->db->last_query();
        return $data;
       }
       
        public function getTotalrequestbyDesigner() {
            $this->db->select("r.*");
            $this->db->from('requests as r');
            $this->db->join('users as u', 'u.id = r.designer_id','inner');
            $this->db->where("r.designer_id != ''"); 
            $this->db->where("u.is_active = 1"); 
            $data = $this->db->get();
           // echo $this->db->last_query();exit;
            $result = $data->result_array();
            return $result;    
        }
       
        public function getTimeofrequestsubmission(){
            $this->db->select("r.approvaldate,r.created,DATEDIFF(r.approvaldate,r.created) as difference,SUM(DATEDIFF(r.approvaldate,r.created)) as total");
            $this->db->from('requests as r');
            $data = $this->db->get();
            // echo $this->db->last_query();exit;
            $result = $data->result_array();
            //echo "<pre>";print_r($result);
            return $result[0]['total'];  
        }

        /*********************agency user options**********************************/
    
    public function getAgencyuserinfo($userid,$domainname=NULL,$payment_mode="") {
        if($payment_mode != ""){
            $this->db->select("ag.user_id");
            $this->db->where("ag.online_payment",$payment_mode); 
        }else{
          $this->db->select("ag.*,u.company_name");
        
            if($domainname == NULL || $domainname == ''){
            $this->db->where("ag.user_id = '" . $userid . "'"); 
            }else{
             $this->db->where("ag.user_id != '" . $userid . "'"); 
             $this->db->where("ag.domain_name = '" . $domainname . "'");    
            }
        }
        $this->db->join('users as u', 'u.id = ag.user_id','inner');
        $this->db->from("agency_user_options as ag");
        $data = $this->db->get();
        $result = $data->result_array();
        if($payment_mode != ""){
        $return['user_id'] = array_column($result,'user_id');
        return $return;
        }else{
            return $result;
        }
    }
    
    public function getuserinfobydomain($domainname = NULL,$userid = NULL) {
        $this->db->select("aup.*,u.id as customer_id,u.email as useremail,u.company_name");
        if($domainname != NULL || $domainname != ''){
        $this->db->where("aup.domain_name = '" . $domainname . "'");  
        }else{
        $this->db->where("aup.user_id = '" . $userid . "'");    
        }
        $this->db->join('users as u', 'u.id = aup.user_id','inner');
        $this->db->from("agency_user_options as aup");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }
    
    public function getall_newrequestforsubuser($customer_id, $status = array(), $subuserid = array(), $orderby = "",$usertype = "") {
        $this->db->select("*");
        $this->db->where("requests.customer_id = '" . $customer_id . "'");
        $this->db->where_in("status", $status);
        if(!empty($subuserid)){
            $where = "((created_by IN (" . implode(',', $subuserid) . ")) or (created_by = 0 ) or (created_by='" . $customer_id . "'))";
        }else{
            $where = "((created_by = 0 ) or (created_by='" . $customer_id . "'))";
        }
        if ($usertype == 'manager') {
            $this->db->where($where);
        } else {
            if(!empty($subuserid)){
             $this->db->where_in("created_by", $subuserid);
            }
        }
        
        if ($orderby == "") {
            $this->db->order_by("DATE(created)", "ASC");
        } else {
            $this->db->order_by($orderby, "ASC");
        }
        $data = $this->db->get("requests");
      //  echo $this->db->last_query();
        $result = $data->result_array();
        return $result;
    }
    
    public function getmain_priorityfrom_requests($subuserpriority,$loginuserid) {
        $this->db->select("id,priority");
        $this->db->where("sub_user_priority", $subuserpriority);
        $this->db->where("created_by", $loginuserid);
        $data = $this->db->get("requests");
       // echo $this->db->last_query();
        $result = $data->result_array();
        return $result;
    }
    
    public function get_customer_mainChat_request_by_id($id,$orderby="") {
        $this->db->select("a.request_id as req_id,a.is_deleted as is_deleted,a.is_filetype as is_filetype,a.is_edited as is_edited,a.with_or_not_customer as with_or_not_customer,a.message as msg,"
                . "a.sender_id as sender_id,a.sender_type as sender_type,a.shared_user_name as shared_user_name,"
                . "a.created as chat_created_date,a.id as id,"
                . "b.id as user_id,b.profile_picture,b.first_name,b.last_name,a.is_deleted as deleted");
        $this->db->from('request_discussions as a');
        $this->db->where("a.request_id='" . $id . "'");
        $this->db->where("a.is_deleted = 0");
        $this->db->join('users as b', 'b.id = a.sender_id',"left");
        if($orderby != ""){
          $this->db->order_by("a.id", $orderby);
        }else{
          $this->db->order_by("a.id", "Desc");
        }
        $data = $this->db->get();
//      echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }
    
    function my_encrypt($data, $key) {
        // Remove the base64 encoding from our key
        $encryption_key = base64_decode($key);
        // Generate an initialization vector
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
        // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
        return base64_encode($encrypted . '::' . $iv);
    }

    function my_decrypt($data, $key) {
        // Remove the base64 encoding from our key
        $encryption_key = base64_decode($key);
        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
        list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }
    public function getSharedpeopleinfobyreqID($reqid){
        $this->db->select("ls.id,ls.user_name,ls.request_id,ls.public_link,ls.share_type,ls.email");
        $this->db->where("ls.request_id='" .$reqid. "' and ls.draft_id = 0");
        $this->db->where("share_type= 'private'");
//        $this->db->join('link_sharing_permissions as lsp', 'lsp.sharing_id = ls.id','left');
        $this->db->from('link_sharing as ls');
        $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
//        echo "<pre/>";print_r($result);
        return $result;
    }

     public function getSharedpeopleinfobyreq_draftID($reqid,$draftid){
        $this->db->select("ls.id,ls.user_name,ls.request_id,ls.public_link,ls.share_type,ls.email,ls.draft_id");
        if($draftid == 0){
            $this->db->where("ls.request_id='" .$reqid. "' and ls.draft_id ='" .$draftid."'");
        }elseif($draftid!="0"){
        {
            $this->db->where("ls.request_id='" .$reqid. "' and ls.draft_id ='" .$draftid."'");
        }
        }else{
            $this->db->where("ls.request_id='" .$reqid. "' and ls.draft_id !='0'");
        }
        $this->db->where("ls.share_type= 'private'");
//        $this->db->join('link_sharing_permissions as lsp', 'lsp.sharing_id = ls.id','left');
        $this->db->from('link_sharing as ls');
        $data = $this->db->get();
         //echo $this->db->last_query();exit;
        $result = $data->result_array();
//        echo "<pre/>";print_r($result);
        return $result;
    }
    
     public function getPubliclinkinfo($reqid){
        $this->db->select("id,public_link,is_disabled");
        $this->db->where("share_type= 'public'");
        $this->db->where("request_id='" .$reqid. "' and draft_id = 0");
        $this->db->from('link_sharing');
        $data = $this->db->get();
       // echo $this->db->last_query();exit;
        $result = $data->result_array();
//        echo "<pre/>";print_r($result);
        return $result[0];
    }
    public function getallPubliclinkinfo($reqid,$draftid=""){
        $this->db->select("id,request_id,public_link,is_disabled,draft_id");
        $this->db->where("share_type= 'public'");
        if($draftid != ""){
            $this->db->where("request_id='" .$reqid. "' and draft_id = '" .$draftid. "'");
        }else{
            $this->db->where("request_id='" .$reqid. "' and draft_id != 0");
        }
        $this->db->from('link_sharing');
        $data = $this->db->get();
      //  echo $this->db->last_query();exit;
        $result = $data->result_array();
//        echo "<pre/>";print_r($result);
        return $result;
    }
    

    public function get_chat_by_parentid($parentid, $order = null) {
        $this->db->select("r.*,u.profile_picture,u.first_name");
        $this->db->from('request_file_chat as r');
        $this->db->where("r.parent_id='" . $parentid . "'");
        $this->db->join('users as u', 'r.sender_id = u.id','left');
        if($order){
            $this->db->order_by("r.created", $order);
        }else{
            $this->db->order_by("r.id", $order);
        }
        $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
        return $result;
    }
    
    public function deleteMsg($id){
        $this->db->where("id = $id or parent_id = $id");
        $data = $this->db->update("request_file_chat",array('is_deleted' => 1));
        return true;
    }
    
    public function getuserIDbyemail($email) {
        $this->db->select("id");
        $this->db->where("email = '" . $email . "'");
        $data = $this->db->get("users");
        $result = $data->result_array();
        return $result;
    }
    
    public function getallfilefordotcomment(){
        $this->db->select("id,request_id,file_name,status,actual_image_width,actual_image_height");
        $this->db->where("status != 'Approve'");
        $this->db->where("user_type != 'customer'");
        $this->db->where("request_id != '0'");
        $this->db->from("request_files");
        $data = $this->db->get();
       // echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }
    public function getxco_ycofordotcomment($reqfileid){
        $this->db->select("id,xco,yco,request_file_id");
        $this->db->where("request_file_id",$reqfileid);
        $this->db->where("xco != 'NULL'");
        $this->db->where("yco != 'NULL'");
        $this->db->from("request_file_chat");
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }
    /**get activities timeline**/
    public function get_project_activities($request_id,$usershow=""){    
        $this->db->select("at.*,atc.message as msg,u.profile_picture as profile_picture,u.first_name as user_fisrtname,u.last_name as user_lastname,u.role as user_role,u.full_admin_access as user_adminaccess,r.designer_id as req_designer_id");
        $this->db->from('activity_timeline as at');
        $this->db->where("at.request_id='" . $request_id . "' and at.".$usershow."='1'");
        $this->db->join('activity_timeline_keys as atc', 'atc.key = at.slug','left');
        $this->db->join('users as u', 'u.id = at.action_perform_by','left');
        $this->db->join('requests as r', 'r.id = at.request_id','left');
        $this->db->order_by("at.created", 'DESC');
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }
//    public function getapprovedfiles(){    
//        $this->db->select("id,request_id,src_file,file_name");
//        $this->db->where("user_type = 'designer'");
//        $this->db->where("status = 'Approve'");
//        $this->db->where("src_file != ''");
//         $this->db->where("created >= '2019-05-01 00:00:00'");
//        $data = $this->db->get("request_files");
//       // echo $this->db->last_query();exit;
//        $result = $data->result_array();
//        return $result;
//    }
    
     public function getAllrequestscountofSubuser($pID,$subid,$status){
        $this->db->select("id,title");
        $this->db->where("customer_id", $pID);
        $this->db->where("created_by", $subid);
        $this->db->where_in('status', $status); 
        $num_rows = $this->db->count_all_results('requests');
//        echo $this->db->last_query();exit;
        return $num_rows;
    }
    
//     public function gettierpriceofplan($id) {
//        $this->db->select("plan_id,quantity,amount,annual_price");
//        $this->db->where("plan_id",$id);
//        $data = $this->db->get("agency_tier_price");
//        $result = $data->result_array();
//        return $result;
//    }
    
    
    public function countVaProjects($status,$vaID){
        $this->db->select("r.id");
        $this->db->where_in("r.status_admin",$status);
        $this->db->where("u.va_id",$vaID);
        $this->db->join('users as u', 'u.id = r.customer_id','left');
        $num_rows = $this->db->count_all_results('requests as r');
//        echo $this->db->last_query();
        return $num_rows;
    }
    
    /*************************project reports****************************************/
    
    public function projectReportAdminlisting($slug,$num,$allCountOnly = false, $start = 0, $limit = 0,$search=""){
        $todaytime = date('Y-m-d H:i:s');
        $today_time = date('Y-m-d H:i:s',mktime(0,0,0));
        $start_time =  date('Y-m-d H:i:s', strtotime($today_time. '+5 hours +00 minutes +00 seconds')); 
        $end_time = date("Y-m-d H:i:s", strtotime($start_time. '+23 hours +59 minutes +59 seconds'));
        $this->db->select("r.id,r.title,r.status_admin,r.created,r.modified,r.expected_date,r.approvaldate,r.count_customer_revision,r.count_quality_revision");
        $this->db->where("r.dummy_request",0);
        if($slug == 'revisions_count'){
        $this->db->where("r.count_customer_revision >=",$num);
        }elseif($slug == 'quality_revision_count'){
           $this->db->where("r.count_quality_revision >=",$num); 
        }elseif($slug == 'delayed_project'){
            $this->db->where("expected_date >=",$start_time);
            $this->db->where("expected_date <=",$end_time);
            $this->db->where("status_admin IN ('active','disapprove','approved')");
            $this->db->where("expected_date < approvaldate");
        }elseif($slug == 'late_project'){
            $this->db->where("expected_date <=",$todaytime);
            //$this->db->where("expected_date <=",$end_time); 
            $this->db->where("r.status_admin IN ('active','disapprove')");
        }elseif($slug == 'unverified'){
            $this->db->where("r.verified",0); 
             $this->db->where("r.status_admin IN ('active','disapprove')");
        }elseif($slug == 'activepro'){
            $this->db->where("r.status NOT IN ('cancel','hold','draft','assign')");
            $this->db->where("r.expected_date >=",$start_time); 
            $this->db->where("r.expected_date <=",$end_time);
        }elseif($slug = 'activebutcompleted'){
            $this->db->from('request_files as rf');
            $this->db->where("r.status NOT IN ('cancel','hold','draft','assign')");
            $this->db->where("r.expected_date >=",$start_time); 
            $this->db->where("r.expected_date <=",$end_time); 
            $this->db->where("rf.created >=",$start_time);
            $this->db->where("rf.created <=",$end_time);
            $this->db->where("rf.user_type = 'designer'");
            $this->db->where("rf.status IN ('Approve','customerreview')");
            $this->db->join('requests as r',"rf.request_id = r.id",'inner');
            $this->db->group_by('rf.request_id');
            $this->db->order_by("r.created", 'DESC');
            if($search){
                $this->db->group_start();
                $this->db->or_like('r.title', $search, 'both');
                $this->db->or_like('r.description', $search, 'both');
                $this->db->group_end();
            }
            if(!$allCountOnly && !$search){
                $limit = ($limit == 0) ? LIMIT_ADMIN_LIST_COUNT : $limit;
                $this->db->limit($limit, $start);
            }
            $data1 = $this->db->get();
        }
        if($slug != 'activebutcompleted'){
        $this->db->order_by("r.created", 'DESC');
        if($search){
            $this->db->group_start();
            $this->db->or_like('r.title', $search, 'both');
            $this->db->or_like('r.description', $search, 'both');
            $this->db->group_end();
        }
        if(!$allCountOnly && !$search){
            $limit = ($limit == 0) ? LIMIT_ADMIN_LIST_COUNT : $limit;
            $this->db->limit($limit, $start);
        }
        $data = $this->db->get('requests as r');
        }
//        echo $this->db->last_query();exit;
        if($data1){
            $result = $data1->result_array();
        }else{
            $result = $data->result_array();
        }
        if($result){
            if($allCountOnly){
               return count($result); 
            } else{
                return $result;
            }
        }
    }
    
    public function getrequestsid(){
         $this->db->select("id");
         $this->db->from('requests');
         $data = $this->db->get();
         $result = $data->result_array();
         return $result;
    }
    
    public function getdraftsofreq($req_id){
        $output = array();
         $this->db->select("count(id) as count");
         $this->db->from('request_files');
         $this->db->where("user_type","designer");
         $this->db->where("request_id",$req_id);
         $this->db->where("status !=",'Reject');
         $data = $this->db->get();
//         echo $this->db->last_query();exit;
         $result = $data->result_array();
         if(isset($result[0]['count']) && $result[0]['count'] != 0){
         $count = ($result[0]['count'] - 1);
         $output['status'] = 'success';
         $output['count'] = $count;
         }else{
             $output['status'] = 'fail';
         }
         return $output;
    }
    
    /************samples & questions***********/
    public function getallquestions($cat_id = NULL,$subcatid = NULL){
        $this->db->select("*");
        $this->db->from('request_questions');
        $this->db->where("((`category_id` = $cat_id AND `subcategory_id` = $subcatid ) OR (`is_default` = 1)) AND (`is_deleted` = 0 AND `is_active` = 1)"); 
        $this->db->order_by("position", 'ASC');
        $data = $this->db->get();
//        echo $this->db->last_query();
        $result = $data->result_array();
//        echo "<pre/>";print_R($result);
        return $result;
    }
    public function getSamplematerials($reqid){
        $this->db->select("rsd.id as id,rsd.material_id as material_id,smd.sample_id as sample_id,smd.sample_material");
        $this->db->from('request_sample_data as rsd');
        $this->db->where('rsd.request_id',$reqid);
        $this->db->join('sample_material_data as smd', 'smd.id = rsd.material_id','left');
        $data = $this->db->get();
//        echo $this->db->last_query();exit;
        $result = $data->result_array();
        return $result;
    }
    
        public function getallhold_request_count($brand_id = "",$client_id=""){
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $login_user_data = $this->load->get_var('login_user_data');
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $this->db->select("*");
        if($brand_id != ''){
         $this->db->where("requests.brand_id = '" . $brand_id . "'");   
        }
        if($client_id != ''){
         $this->db->where("requests.created_by = '" . $client_id . "'");   
          if($login_user_data[0]['user_flag'] == 'manager' && !empty($sub_usersbrands)){
            $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0))");
        }
        }else{
        if(!empty($sub_usersbrands)){
           // $this->db->where_in('requests.brand_id', $sub_usersbrands);
            $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0 and requests.created_by='".$login_user_id."'))");
            
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
           
            $this->db->where("requests.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("sub_user_priority", "ASC");
        }
        }
        $this->db->where("requests.customer_id = '" . $created_user . "'");
        $this->db->where("status='hold'");
        $this->db->order_by("index_number", "ASC");
        $num_rows = $this->db->count_all_results('requests');
//        echo $this->db->last_query();exit;
        return $num_rows;
    }
    
    public function getallhold_request($brand_id = "",$status = NULL,$client_id = "") {
        $created_user = $this->load->get_var('main_user');
        $login_user_id = $this->load->get_var('login_user_id');
        $login_user_data = $this->load->get_var('login_user_data');
        $sub_usersbrands = $this->load->get_var('sub_usersbrands');
        $canbrand_profile_access = $this->myfunctions->isUserPermission('brand_profile_access');
        $this->db->select("*");
        if($brand_id != ''){
         $this->db->where("requests.brand_id = '" . $brand_id . "'");   
        }
        if($client_id != ''){
         $this->db->where("requests.created_by = '" . $client_id . "'");   
           if($login_user_data[0]['user_flag'] == 'manager' && !empty($sub_usersbrands)){
            $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0))");
        }
        }else{
        if(!empty($sub_usersbrands)){
          //  $this->db->where_in('requests.brand_id', $sub_usersbrands);
            $this->db->where("((requests.brand_id IN (".implode(',',$sub_usersbrands).")) or (requests.brand_id = 0 and requests.created_by='".$login_user_id."'))");
        }elseif(empty($sub_usersbrands) && $canbrand_profile_access == 0){
           
            $this->db->where("requests.created_by = '" . $login_user_id . "'" );
            $this->db->order_by("sub_user_priority", "ASC");
            
        }
        }
        $this->db->where("requests.customer_id = '" . $created_user . "'");
        $this->db->where("status='hold'");
        if($status == 'hold'){
          $this->db->order_by("modified", "ASC");  
        }else{
            $this->db->order_by("index_number", "ASC");
        }
        $this->db->limit(LIMIT_CUSTOMER_LIST_COUNT, 0);
        $data = $this->db->get("requests");
//        echo $this->db->last_query();exit;
        $result = $data->result_array();

        for ($i = 0; $i < sizeof($result); $i++) {
            $designer = $this->getuserbyid($result[$i]['designer_id']);
            $customer = $this->getuserbyid($result[$i]['customer_id']);
            if (!empty($designer)) {
                $result[$i]['designer_first_name'] = $designer[0]['first_name'];
                $result[$i]['designer_last_name'] = $designer[0]['last_name'];
                $result[$i]['profile_picture'] = $designer[0]['profile_picture'];
            } else {
                $result[$i]['designer_first_name'] = "";
                $result[$i]['designer_last_name'] = "";
                $result[$i]['profile_picture'] = "";
            }
            if (!empty($customer)) {
                $result[$i]['customer_first_name'] = $customer[0]['first_name'];
                $result[$i]['customer_last_name'] = $customer[0]['last_name'];
                $result[$i]['customer_profile_picture'] = $customer[0]['profile_picture'];
            } else {
                $result[$i]['customer_first_name'] = "";
                $result[$i]['customer_last_name'] = "";
            }
        }
        return $result;
    }
    public function get_reviewfeedback($id,$role){
        $this->db->select("*");
        $this->db->where('draft_id',$id);
        $this->db->where('user_role',$role);
        $data = $this->db->get("request_files_review");
        $result = $data->result_array();
        return $result;
    }
    
    /*********feedback review for link sharing****/
    public function getRequestcustomerbyReqid($id){
        $this->db->select("customer_id,created_by");
        $this->db->where('id',$id);
        $data = $this->db->get("requests");
//         echo $this->db->last_query();exit;
        $result = $data->result_array();
        if($result[0]['created_by'] != 0){
            return $result[0]['created_by'];
        }else{
            return $result[0]['customer_id'];
        }
    }
}
