<html>
<head>
<title>
GraphicsZoo Maintenance 
</title>
<style>
    body{margin: 0; padding: 0; background: #f2f2f2;    height: 100vh;
    display: flex;    font-family: 'Poppins', sans-serif;
    align-items: center;
    justify-content: center;}
    section.comming-soon {
    width: 100%;
}
    .inner-box {
    max-width: 600px;
    margin:  auto;
    background-color: rgba(255,255,255,0.8);
    border-radius: 18px;
    text-align: center;
    padding: 70px;
}
h2{font-size: 26px;}
.inner-box img {
    max-width: 350px;
    margin-bottom: 30px;
}
    </style></head>
    <body>
    <section class="comming-soon">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="inner-box">
                    <img src="https://s3.us-east-2.amazonaws.com/graphics-zoo-ohio/public/assets/front_end/Updated_Design/img/logo.svg">
                    <h2>Website is under maintenance and we will be back soon.</h2>
                </div>
                </div>
            </div>
        </div>
        
        
        
    </section>   
	</body>
	</html>